graph [
  node [
    id 0
    label "jak&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "trudno"
    origin "text"
  ]
  node [
    id 2
    label "niekiedy"
    origin "text"
  ]
  node [
    id 3
    label "wena"
    origin "text"
  ]
  node [
    id 4
    label "tw&#243;rczy"
    origin "text"
  ]
  node [
    id 5
    label "taka"
    origin "text"
  ]
  node [
    id 6
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 9
    label "potok"
    origin "text"
  ]
  node [
    id 10
    label "&#380;ywy"
    origin "text"
  ]
  node [
    id 11
    label "mowa"
    origin "text"
  ]
  node [
    id 12
    label "kunszt"
    origin "text"
  ]
  node [
    id 13
    label "retor"
    origin "text"
  ]
  node [
    id 14
    label "&#347;wiadczy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wyraz"
    origin "text"
  ]
  node [
    id 16
    label "przelewa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "papier"
    origin "text"
  ]
  node [
    id 18
    label "czytelnik"
    origin "text"
  ]
  node [
    id 19
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 20
    label "&#322;atwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "&#322;apczywo&#347;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "niejaki"
    origin "text"
  ]
  node [
    id 23
    label "wzrok"
    origin "text"
  ]
  node [
    id 24
    label "po&#322;yka&#263;"
    origin "text"
  ]
  node [
    id 25
    label "czas"
    origin "text"
  ]
  node [
    id 26
    label "chcie&#263;by"
    origin "text"
  ]
  node [
    id 27
    label "si&#281;"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "s&#322;oneczny"
    origin "text"
  ]
  node [
    id 30
    label "promie&#324;"
    origin "text"
  ]
  node [
    id 31
    label "ikarowy"
    origin "text"
  ]
  node [
    id 32
    label "pod"
    origin "text"
  ]
  node [
    id 33
    label "niebiosa"
    origin "text"
  ]
  node [
    id 34
    label "pofrun&#261;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "zas&#322;ona"
    origin "text"
  ]
  node [
    id 36
    label "chmura"
    origin "text"
  ]
  node [
    id 37
    label "nieprzenikniony"
    origin "text"
  ]
  node [
    id 38
    label "stanowczo"
    origin "text"
  ]
  node [
    id 39
    label "idea"
    origin "text"
  ]
  node [
    id 40
    label "ten"
    origin "text"
  ]
  node [
    id 41
    label "wielki"
    origin "text"
  ]
  node [
    id 42
    label "porzuci&#263;"
    origin "text"
  ]
  node [
    id 43
    label "kaza&#263;"
    origin "text"
  ]
  node [
    id 44
    label "spragniony"
    origin "text"
  ]
  node [
    id 45
    label "m&#322;ode"
    origin "text"
  ]
  node [
    id 46
    label "umys&#322;"
    origin "text"
  ]
  node [
    id 47
    label "ambitny"
    origin "text"
  ]
  node [
    id 48
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 49
    label "domaga&#263;"
    origin "text"
  ]
  node [
    id 50
    label "niczym"
    origin "text"
  ]
  node [
    id 51
    label "piskl&#281;"
    origin "text"
  ]
  node [
    id 52
    label "pokarm"
    origin "text"
  ]
  node [
    id 53
    label "gniazdo"
    origin "text"
  ]
  node [
    id 54
    label "wci&#261;&#380;"
    origin "text"
  ]
  node [
    id 55
    label "nowa"
    origin "text"
  ]
  node [
    id 56
    label "coraz"
    origin "text"
  ]
  node [
    id 57
    label "doskona&#322;y"
    origin "text"
  ]
  node [
    id 58
    label "dzie&#322;o"
    origin "text"
  ]
  node [
    id 59
    label "otworzy&#263;"
    origin "text"
  ]
  node [
    id 60
    label "szeroko"
    origin "text"
  ]
  node [
    id 61
    label "wiedza"
    origin "text"
  ]
  node [
    id 62
    label "wyposa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 63
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 64
    label "narz&#281;dzie"
    origin "text"
  ]
  node [
    id 65
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 66
    label "warsztat"
    origin "text"
  ]
  node [
    id 67
    label "praca"
    origin "text"
  ]
  node [
    id 68
    label "gdzie&#380;"
    origin "text"
  ]
  node [
    id 69
    label "szuka&#263;"
    origin "text"
  ]
  node [
    id 70
    label "inspiracja"
    origin "text"
  ]
  node [
    id 71
    label "ambrozja"
    origin "text"
  ]
  node [
    id 72
    label "&#380;yciodajny"
    origin "text"
  ]
  node [
    id 73
    label "nektar"
    origin "text"
  ]
  node [
    id 74
    label "boski"
    origin "text"
  ]
  node [
    id 75
    label "duch"
    origin "text"
  ]
  node [
    id 76
    label "cia&#322;o"
    origin "text"
  ]
  node [
    id 77
    label "rozum"
    origin "text"
  ]
  node [
    id 78
    label "tchn&#261;&#263;"
    origin "text"
  ]
  node [
    id 79
    label "jaki"
    origin "text"
  ]
  node [
    id 80
    label "pod&#261;&#380;a&#263;"
    origin "text"
  ]
  node [
    id 81
    label "&#347;cie&#380;ek"
    origin "text"
  ]
  node [
    id 82
    label "zawie&#347;&#263;"
    origin "text"
  ]
  node [
    id 83
    label "srodze"
    origin "text"
  ]
  node [
    id 84
    label "wy&#380;yna"
    origin "text"
  ]
  node [
    id 85
    label "wzlecie&#263;"
    origin "text"
  ]
  node [
    id 86
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 87
    label "godne"
    origin "text"
  ]
  node [
    id 88
    label "szacunek"
    origin "text"
  ]
  node [
    id 89
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 90
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 91
    label "hard"
  ]
  node [
    id 92
    label "trudny"
  ]
  node [
    id 93
    label "ci&#281;&#380;ko"
  ]
  node [
    id 94
    label "skomplikowany"
  ]
  node [
    id 95
    label "k&#322;opotliwy"
  ]
  node [
    id 96
    label "wymagaj&#261;cy"
  ]
  node [
    id 97
    label "czasami"
  ]
  node [
    id 98
    label "enchantment"
  ]
  node [
    id 99
    label "podekscytowanie"
  ]
  node [
    id 100
    label "ch&#281;&#263;"
  ]
  node [
    id 101
    label "thinking"
  ]
  node [
    id 102
    label "inclination"
  ]
  node [
    id 103
    label "zajawka"
  ]
  node [
    id 104
    label "emocja"
  ]
  node [
    id 105
    label "wytw&#243;r"
  ]
  node [
    id 106
    label "oskoma"
  ]
  node [
    id 107
    label "agitation"
  ]
  node [
    id 108
    label "poruszenie"
  ]
  node [
    id 109
    label "excitation"
  ]
  node [
    id 110
    label "podniecenie_si&#281;"
  ]
  node [
    id 111
    label "nastr&#243;j"
  ]
  node [
    id 112
    label "tworzycielski"
  ]
  node [
    id 113
    label "kreatywny"
  ]
  node [
    id 114
    label "pracowity"
  ]
  node [
    id 115
    label "inspiruj&#261;cy"
  ]
  node [
    id 116
    label "tw&#243;rczo"
  ]
  node [
    id 117
    label "sprzyjaj&#261;cy"
  ]
  node [
    id 118
    label "oryginalny"
  ]
  node [
    id 119
    label "zaj&#281;ty"
  ]
  node [
    id 120
    label "sumienny"
  ]
  node [
    id 121
    label "aktywny"
  ]
  node [
    id 122
    label "pracowicie"
  ]
  node [
    id 123
    label "efektywny"
  ]
  node [
    id 124
    label "mozolny"
  ]
  node [
    id 125
    label "ch&#281;tny"
  ]
  node [
    id 126
    label "pomys&#322;owy"
  ]
  node [
    id 127
    label "zmienny"
  ]
  node [
    id 128
    label "kreatywnie"
  ]
  node [
    id 129
    label "niespotykany"
  ]
  node [
    id 130
    label "inny"
  ]
  node [
    id 131
    label "nowy"
  ]
  node [
    id 132
    label "prawdziwy"
  ]
  node [
    id 133
    label "warto&#347;ciowy"
  ]
  node [
    id 134
    label "o&#380;ywczy"
  ]
  node [
    id 135
    label "oryginalnie"
  ]
  node [
    id 136
    label "pierwotny"
  ]
  node [
    id 137
    label "ekscentryczny"
  ]
  node [
    id 138
    label "buduj&#261;cy"
  ]
  node [
    id 139
    label "stymuluj&#261;cy"
  ]
  node [
    id 140
    label "inspiruj&#261;co"
  ]
  node [
    id 141
    label "pomy&#347;lny"
  ]
  node [
    id 142
    label "korzystny"
  ]
  node [
    id 143
    label "przychylny"
  ]
  node [
    id 144
    label "korzystnie"
  ]
  node [
    id 145
    label "pomy&#347;lnie"
  ]
  node [
    id 146
    label "jednostka_monetarna"
  ]
  node [
    id 147
    label "Bangladesz"
  ]
  node [
    id 148
    label "Bengal"
  ]
  node [
    id 149
    label "komunikat"
  ]
  node [
    id 150
    label "jednostka_informacji"
  ]
  node [
    id 151
    label "wykrzyknik"
  ]
  node [
    id 152
    label "czasownik"
  ]
  node [
    id 153
    label "bit"
  ]
  node [
    id 154
    label "wordnet"
  ]
  node [
    id 155
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 156
    label "obietnica"
  ]
  node [
    id 157
    label "wypowiedzenie"
  ]
  node [
    id 158
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 159
    label "nag&#322;os"
  ]
  node [
    id 160
    label "morfem"
  ]
  node [
    id 161
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 162
    label "wyg&#322;os"
  ]
  node [
    id 163
    label "s&#322;ownictwo"
  ]
  node [
    id 164
    label "jednostka_leksykalna"
  ]
  node [
    id 165
    label "pole_semantyczne"
  ]
  node [
    id 166
    label "pisanie_si&#281;"
  ]
  node [
    id 167
    label "roi&#263;_si&#281;"
  ]
  node [
    id 168
    label "kreacjonista"
  ]
  node [
    id 169
    label "communication"
  ]
  node [
    id 170
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 171
    label "statement"
  ]
  node [
    id 172
    label "zapewnienie"
  ]
  node [
    id 173
    label "zapowied&#378;"
  ]
  node [
    id 174
    label "termin"
  ]
  node [
    id 175
    label "terminology"
  ]
  node [
    id 176
    label "j&#281;zyk"
  ]
  node [
    id 177
    label "rozwi&#261;zanie"
  ]
  node [
    id 178
    label "przepowiedzenie"
  ]
  node [
    id 179
    label "notice"
  ]
  node [
    id 180
    label "message"
  ]
  node [
    id 181
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 182
    label "wyra&#380;enie"
  ]
  node [
    id 183
    label "konwersja"
  ]
  node [
    id 184
    label "wydanie"
  ]
  node [
    id 185
    label "denunciation"
  ]
  node [
    id 186
    label "powiedzenie"
  ]
  node [
    id 187
    label "zwerbalizowanie"
  ]
  node [
    id 188
    label "wydobycie"
  ]
  node [
    id 189
    label "generowanie"
  ]
  node [
    id 190
    label "notification"
  ]
  node [
    id 191
    label "szyk"
  ]
  node [
    id 192
    label "generowa&#263;"
  ]
  node [
    id 193
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 194
    label "leksem"
  ]
  node [
    id 195
    label "&#347;rodek"
  ]
  node [
    id 196
    label "koniec"
  ]
  node [
    id 197
    label "pocz&#261;tek"
  ]
  node [
    id 198
    label "morpheme"
  ]
  node [
    id 199
    label "forma"
  ]
  node [
    id 200
    label "s&#322;owo_maszynowe"
  ]
  node [
    id 201
    label "rytm"
  ]
  node [
    id 202
    label "cyfra"
  ]
  node [
    id 203
    label "p&#243;&#322;bajt"
  ]
  node [
    id 204
    label "system_dw&#243;jkowy"
  ]
  node [
    id 205
    label "bajt"
  ]
  node [
    id 206
    label "oktet"
  ]
  node [
    id 207
    label "baza_danych"
  ]
  node [
    id 208
    label "WordNet"
  ]
  node [
    id 209
    label "S&#322;owosie&#263;"
  ]
  node [
    id 210
    label "exclamation_mark"
  ]
  node [
    id 211
    label "znak_interpunkcyjny"
  ]
  node [
    id 212
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 213
    label "liczba"
  ]
  node [
    id 214
    label "cecha"
  ]
  node [
    id 215
    label "rozmiar"
  ]
  node [
    id 216
    label "miejsce"
  ]
  node [
    id 217
    label "circumference"
  ]
  node [
    id 218
    label "strona"
  ]
  node [
    id 219
    label "cyrkumferencja"
  ]
  node [
    id 220
    label "nieprzechodnio&#347;&#263;"
  ]
  node [
    id 221
    label "koniugacja"
  ]
  node [
    id 222
    label "przechodnio&#347;&#263;"
  ]
  node [
    id 223
    label "flood"
  ]
  node [
    id 224
    label "ruch"
  ]
  node [
    id 225
    label "fala"
  ]
  node [
    id 226
    label "mn&#243;stwo"
  ]
  node [
    id 227
    label "ciek_wodny"
  ]
  node [
    id 228
    label "enormousness"
  ]
  node [
    id 229
    label "ilo&#347;&#263;"
  ]
  node [
    id 230
    label "stream"
  ]
  node [
    id 231
    label "rozbijanie_si&#281;"
  ]
  node [
    id 232
    label "efekt_Dopplera"
  ]
  node [
    id 233
    label "przemoc"
  ]
  node [
    id 234
    label "grzywa_fali"
  ]
  node [
    id 235
    label "strumie&#324;"
  ]
  node [
    id 236
    label "obcinka"
  ]
  node [
    id 237
    label "zafalowanie"
  ]
  node [
    id 238
    label "zjawisko"
  ]
  node [
    id 239
    label "znak_diakrytyczny"
  ]
  node [
    id 240
    label "clutter"
  ]
  node [
    id 241
    label "fit"
  ]
  node [
    id 242
    label "reakcja"
  ]
  node [
    id 243
    label "rozbicie_si&#281;"
  ]
  node [
    id 244
    label "okres"
  ]
  node [
    id 245
    label "zafalowa&#263;"
  ]
  node [
    id 246
    label "woda"
  ]
  node [
    id 247
    label "t&#322;um"
  ]
  node [
    id 248
    label "kot"
  ]
  node [
    id 249
    label "wojsko"
  ]
  node [
    id 250
    label "pasemko"
  ]
  node [
    id 251
    label "karb"
  ]
  node [
    id 252
    label "kszta&#322;t"
  ]
  node [
    id 253
    label "czo&#322;o_fali"
  ]
  node [
    id 254
    label "move"
  ]
  node [
    id 255
    label "zmiana"
  ]
  node [
    id 256
    label "model"
  ]
  node [
    id 257
    label "aktywno&#347;&#263;"
  ]
  node [
    id 258
    label "utrzymywanie"
  ]
  node [
    id 259
    label "utrzymywa&#263;"
  ]
  node [
    id 260
    label "taktyka"
  ]
  node [
    id 261
    label "d&#322;ugi"
  ]
  node [
    id 262
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 263
    label "natural_process"
  ]
  node [
    id 264
    label "kanciasty"
  ]
  node [
    id 265
    label "utrzyma&#263;"
  ]
  node [
    id 266
    label "myk"
  ]
  node [
    id 267
    label "manewr"
  ]
  node [
    id 268
    label "utrzymanie"
  ]
  node [
    id 269
    label "wydarzenie"
  ]
  node [
    id 270
    label "tumult"
  ]
  node [
    id 271
    label "stopek"
  ]
  node [
    id 272
    label "movement"
  ]
  node [
    id 273
    label "czynno&#347;&#263;"
  ]
  node [
    id 274
    label "komunikacja"
  ]
  node [
    id 275
    label "lokomocja"
  ]
  node [
    id 276
    label "drift"
  ]
  node [
    id 277
    label "commercial_enterprise"
  ]
  node [
    id 278
    label "apraksja"
  ]
  node [
    id 279
    label "proces"
  ]
  node [
    id 280
    label "mechanika"
  ]
  node [
    id 281
    label "travel"
  ]
  node [
    id 282
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 283
    label "dyssypacja_energii"
  ]
  node [
    id 284
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 285
    label "kr&#243;tki"
  ]
  node [
    id 286
    label "realistyczny"
  ]
  node [
    id 287
    label "cz&#322;owiek"
  ]
  node [
    id 288
    label "silny"
  ]
  node [
    id 289
    label "o&#380;ywianie"
  ]
  node [
    id 290
    label "zgrabny"
  ]
  node [
    id 291
    label "&#380;ycie"
  ]
  node [
    id 292
    label "g&#322;&#281;boki"
  ]
  node [
    id 293
    label "energiczny"
  ]
  node [
    id 294
    label "naturalny"
  ]
  node [
    id 295
    label "ciekawy"
  ]
  node [
    id 296
    label "&#380;ywo"
  ]
  node [
    id 297
    label "wyra&#378;ny"
  ]
  node [
    id 298
    label "&#380;ywotny"
  ]
  node [
    id 299
    label "czynny"
  ]
  node [
    id 300
    label "aktualny"
  ]
  node [
    id 301
    label "szybki"
  ]
  node [
    id 302
    label "wa&#380;ny"
  ]
  node [
    id 303
    label "aktualizowanie"
  ]
  node [
    id 304
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 305
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 306
    label "aktualnie"
  ]
  node [
    id 307
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 308
    label "uaktualnienie"
  ]
  node [
    id 309
    label "naturalnie"
  ]
  node [
    id 310
    label "zrozumia&#322;y"
  ]
  node [
    id 311
    label "prawy"
  ]
  node [
    id 312
    label "neutralny"
  ]
  node [
    id 313
    label "zwyczajny"
  ]
  node [
    id 314
    label "normalny"
  ]
  node [
    id 315
    label "immanentny"
  ]
  node [
    id 316
    label "rzeczywisty"
  ]
  node [
    id 317
    label "bezsporny"
  ]
  node [
    id 318
    label "szczery"
  ]
  node [
    id 319
    label "organicznie"
  ]
  node [
    id 320
    label "asymilowa&#263;"
  ]
  node [
    id 321
    label "nasada"
  ]
  node [
    id 322
    label "profanum"
  ]
  node [
    id 323
    label "wz&#243;r"
  ]
  node [
    id 324
    label "senior"
  ]
  node [
    id 325
    label "asymilowanie"
  ]
  node [
    id 326
    label "os&#322;abia&#263;"
  ]
  node [
    id 327
    label "homo_sapiens"
  ]
  node [
    id 328
    label "osoba"
  ]
  node [
    id 329
    label "ludzko&#347;&#263;"
  ]
  node [
    id 330
    label "Adam"
  ]
  node [
    id 331
    label "hominid"
  ]
  node [
    id 332
    label "posta&#263;"
  ]
  node [
    id 333
    label "portrecista"
  ]
  node [
    id 334
    label "polifag"
  ]
  node [
    id 335
    label "podw&#322;adny"
  ]
  node [
    id 336
    label "dwun&#243;g"
  ]
  node [
    id 337
    label "wapniak"
  ]
  node [
    id 338
    label "os&#322;abianie"
  ]
  node [
    id 339
    label "antropochoria"
  ]
  node [
    id 340
    label "figura"
  ]
  node [
    id 341
    label "g&#322;owa"
  ]
  node [
    id 342
    label "mikrokosmos"
  ]
  node [
    id 343
    label "oddzia&#322;ywanie"
  ]
  node [
    id 344
    label "zauwa&#380;alny"
  ]
  node [
    id 345
    label "wyra&#378;nie"
  ]
  node [
    id 346
    label "zdecydowany"
  ]
  node [
    id 347
    label "nieoboj&#281;tny"
  ]
  node [
    id 348
    label "g&#322;&#281;boko"
  ]
  node [
    id 349
    label "intensywny"
  ]
  node [
    id 350
    label "m&#261;dry"
  ]
  node [
    id 351
    label "wyrazisty"
  ]
  node [
    id 352
    label "daleki"
  ]
  node [
    id 353
    label "niezrozumia&#322;y"
  ]
  node [
    id 354
    label "ukryty"
  ]
  node [
    id 355
    label "gruntowny"
  ]
  node [
    id 356
    label "dog&#322;&#281;bny"
  ]
  node [
    id 357
    label "mocny"
  ]
  node [
    id 358
    label "niski"
  ]
  node [
    id 359
    label "zdrowy"
  ]
  node [
    id 360
    label "realistycznie"
  ]
  node [
    id 361
    label "przytomny"
  ]
  node [
    id 362
    label "jary"
  ]
  node [
    id 363
    label "ostry"
  ]
  node [
    id 364
    label "energicznie"
  ]
  node [
    id 365
    label "bystrolotny"
  ]
  node [
    id 366
    label "dynamiczny"
  ]
  node [
    id 367
    label "bezpo&#347;redni"
  ]
  node [
    id 368
    label "prosty"
  ]
  node [
    id 369
    label "sprawny"
  ]
  node [
    id 370
    label "temperamentny"
  ]
  node [
    id 371
    label "szybko"
  ]
  node [
    id 372
    label "skuteczny"
  ]
  node [
    id 373
    label "zr&#281;czny"
  ]
  node [
    id 374
    label "p&#322;ynny"
  ]
  node [
    id 375
    label "zwinnie"
  ]
  node [
    id 376
    label "polotny"
  ]
  node [
    id 377
    label "harmonijny"
  ]
  node [
    id 378
    label "zgrabnie"
  ]
  node [
    id 379
    label "kszta&#322;tny"
  ]
  node [
    id 380
    label "zwinny"
  ]
  node [
    id 381
    label "delikatny"
  ]
  node [
    id 382
    label "indagator"
  ]
  node [
    id 383
    label "swoisty"
  ]
  node [
    id 384
    label "interesuj&#261;cy"
  ]
  node [
    id 385
    label "nietuzinkowy"
  ]
  node [
    id 386
    label "ciekawie"
  ]
  node [
    id 387
    label "interesowanie"
  ]
  node [
    id 388
    label "dziwny"
  ]
  node [
    id 389
    label "intryguj&#261;cy"
  ]
  node [
    id 390
    label "faktyczny"
  ]
  node [
    id 391
    label "czynnie"
  ]
  node [
    id 392
    label "istotny"
  ]
  node [
    id 393
    label "zaanga&#380;owany"
  ]
  node [
    id 394
    label "aktywnie"
  ]
  node [
    id 395
    label "realny"
  ]
  node [
    id 396
    label "zdolny"
  ]
  node [
    id 397
    label "dobry"
  ]
  node [
    id 398
    label "uczynnienie"
  ]
  node [
    id 399
    label "dzia&#322;alny"
  ]
  node [
    id 400
    label "dzia&#322;anie"
  ]
  node [
    id 401
    label "uczynnianie"
  ]
  node [
    id 402
    label "zajebisty"
  ]
  node [
    id 403
    label "wytrzyma&#322;y"
  ]
  node [
    id 404
    label "mocno"
  ]
  node [
    id 405
    label "niepodwa&#380;alny"
  ]
  node [
    id 406
    label "du&#380;y"
  ]
  node [
    id 407
    label "konkretny"
  ]
  node [
    id 408
    label "meflochina"
  ]
  node [
    id 409
    label "krzepienie"
  ]
  node [
    id 410
    label "przekonuj&#261;cy"
  ]
  node [
    id 411
    label "pokrzepienie"
  ]
  node [
    id 412
    label "silnie"
  ]
  node [
    id 413
    label "zgodny"
  ]
  node [
    id 414
    label "prawdziwie"
  ]
  node [
    id 415
    label "podobny"
  ]
  node [
    id 416
    label "naprawd&#281;"
  ]
  node [
    id 417
    label "&#380;ywny"
  ]
  node [
    id 418
    label "realnie"
  ]
  node [
    id 419
    label "&#380;ywotnie"
  ]
  node [
    id 420
    label "pe&#322;ny"
  ]
  node [
    id 421
    label "biologicznie"
  ]
  node [
    id 422
    label "nasycony"
  ]
  node [
    id 423
    label "pobudzanie"
  ]
  node [
    id 424
    label "ratowanie"
  ]
  node [
    id 425
    label "wzbudzanie"
  ]
  node [
    id 426
    label "vitalization"
  ]
  node [
    id 427
    label "przywracanie"
  ]
  node [
    id 428
    label "nadawanie"
  ]
  node [
    id 429
    label "okres_noworodkowy"
  ]
  node [
    id 430
    label "umarcie"
  ]
  node [
    id 431
    label "entity"
  ]
  node [
    id 432
    label "prze&#380;ycie"
  ]
  node [
    id 433
    label "dzieci&#324;stwo"
  ]
  node [
    id 434
    label "&#347;mier&#263;"
  ]
  node [
    id 435
    label "menopauza"
  ]
  node [
    id 436
    label "warunki"
  ]
  node [
    id 437
    label "do&#380;ywanie"
  ]
  node [
    id 438
    label "power"
  ]
  node [
    id 439
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 440
    label "byt"
  ]
  node [
    id 441
    label "zegar_biologiczny"
  ]
  node [
    id 442
    label "wiek_matuzalemowy"
  ]
  node [
    id 443
    label "koleje_losu"
  ]
  node [
    id 444
    label "life"
  ]
  node [
    id 445
    label "subsistence"
  ]
  node [
    id 446
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 447
    label "umieranie"
  ]
  node [
    id 448
    label "bycie"
  ]
  node [
    id 449
    label "staro&#347;&#263;"
  ]
  node [
    id 450
    label "rozw&#243;j"
  ]
  node [
    id 451
    label "przebywanie"
  ]
  node [
    id 452
    label "niemowl&#281;ctwo"
  ]
  node [
    id 453
    label "raj_utracony"
  ]
  node [
    id 454
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 455
    label "prze&#380;ywanie"
  ]
  node [
    id 456
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 457
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 458
    label "po&#322;&#243;g"
  ]
  node [
    id 459
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 460
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 461
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 462
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 463
    label "energy"
  ]
  node [
    id 464
    label "andropauza"
  ]
  node [
    id 465
    label "szwung"
  ]
  node [
    id 466
    label "zdolno&#347;&#263;"
  ]
  node [
    id 467
    label "kod"
  ]
  node [
    id 468
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 469
    label "gramatyka"
  ]
  node [
    id 470
    label "fonetyka"
  ]
  node [
    id 471
    label "t&#322;umaczenie"
  ]
  node [
    id 472
    label "rozumienie"
  ]
  node [
    id 473
    label "address"
  ]
  node [
    id 474
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 475
    label "m&#243;wienie"
  ]
  node [
    id 476
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 477
    label "konsonantyzm"
  ]
  node [
    id 478
    label "wokalizm"
  ]
  node [
    id 479
    label "m&#243;wi&#263;"
  ]
  node [
    id 480
    label "po_koroniarsku"
  ]
  node [
    id 481
    label "rozumie&#263;"
  ]
  node [
    id 482
    label "przet&#322;umaczenie"
  ]
  node [
    id 483
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 484
    label "tongue"
  ]
  node [
    id 485
    label "wypowied&#378;"
  ]
  node [
    id 486
    label "pismo"
  ]
  node [
    id 487
    label "parafrazowanie"
  ]
  node [
    id 488
    label "stylizacja"
  ]
  node [
    id 489
    label "sparafrazowanie"
  ]
  node [
    id 490
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 491
    label "strawestowanie"
  ]
  node [
    id 492
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 493
    label "sformu&#322;owanie"
  ]
  node [
    id 494
    label "pos&#322;uchanie"
  ]
  node [
    id 495
    label "strawestowa&#263;"
  ]
  node [
    id 496
    label "parafrazowa&#263;"
  ]
  node [
    id 497
    label "delimitacja"
  ]
  node [
    id 498
    label "rezultat"
  ]
  node [
    id 499
    label "ozdobnik"
  ]
  node [
    id 500
    label "sparafrazowa&#263;"
  ]
  node [
    id 501
    label "s&#261;d"
  ]
  node [
    id 502
    label "trawestowa&#263;"
  ]
  node [
    id 503
    label "trawestowanie"
  ]
  node [
    id 504
    label "zapominanie"
  ]
  node [
    id 505
    label "zapomnie&#263;"
  ]
  node [
    id 506
    label "zapomnienie"
  ]
  node [
    id 507
    label "potencja&#322;"
  ]
  node [
    id 508
    label "obliczeniowo"
  ]
  node [
    id 509
    label "ability"
  ]
  node [
    id 510
    label "posiada&#263;"
  ]
  node [
    id 511
    label "zapomina&#263;"
  ]
  node [
    id 512
    label "transportation_system"
  ]
  node [
    id 513
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 514
    label "implicite"
  ]
  node [
    id 515
    label "explicite"
  ]
  node [
    id 516
    label "wydeptanie"
  ]
  node [
    id 517
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 518
    label "wydeptywanie"
  ]
  node [
    id 519
    label "ekspedytor"
  ]
  node [
    id 520
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 521
    label "bezproblemowy"
  ]
  node [
    id 522
    label "activity"
  ]
  node [
    id 523
    label "struktura"
  ]
  node [
    id 524
    label "ci&#261;g"
  ]
  node [
    id 525
    label "language"
  ]
  node [
    id 526
    label "szyfrowanie"
  ]
  node [
    id 527
    label "szablon"
  ]
  node [
    id 528
    label "code"
  ]
  node [
    id 529
    label "cover"
  ]
  node [
    id 530
    label "opowiedzenie"
  ]
  node [
    id 531
    label "zwracanie_si&#281;"
  ]
  node [
    id 532
    label "zapeszanie"
  ]
  node [
    id 533
    label "formu&#322;owanie"
  ]
  node [
    id 534
    label "wypowiadanie"
  ]
  node [
    id 535
    label "powiadanie"
  ]
  node [
    id 536
    label "dysfonia"
  ]
  node [
    id 537
    label "ozywanie_si&#281;"
  ]
  node [
    id 538
    label "public_speaking"
  ]
  node [
    id 539
    label "wyznawanie"
  ]
  node [
    id 540
    label "dodawanie"
  ]
  node [
    id 541
    label "wydawanie"
  ]
  node [
    id 542
    label "wygadanie"
  ]
  node [
    id 543
    label "informowanie"
  ]
  node [
    id 544
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 545
    label "dowalenie"
  ]
  node [
    id 546
    label "prawienie"
  ]
  node [
    id 547
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 548
    label "przerywanie"
  ]
  node [
    id 549
    label "dogadanie_si&#281;"
  ]
  node [
    id 550
    label "wydobywanie"
  ]
  node [
    id 551
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 552
    label "wyra&#380;anie"
  ]
  node [
    id 553
    label "opowiadanie"
  ]
  node [
    id 554
    label "mawianie"
  ]
  node [
    id 555
    label "stosowanie"
  ]
  node [
    id 556
    label "zauwa&#380;enie"
  ]
  node [
    id 557
    label "speaking"
  ]
  node [
    id 558
    label "gaworzenie"
  ]
  node [
    id 559
    label "przepowiadanie"
  ]
  node [
    id 560
    label "dogadywanie_si&#281;"
  ]
  node [
    id 561
    label "ortografia"
  ]
  node [
    id 562
    label "dzia&#322;"
  ]
  node [
    id 563
    label "paleografia"
  ]
  node [
    id 564
    label "dokument"
  ]
  node [
    id 565
    label "wk&#322;ad"
  ]
  node [
    id 566
    label "egzemplarz"
  ]
  node [
    id 567
    label "ok&#322;adka"
  ]
  node [
    id 568
    label "letter"
  ]
  node [
    id 569
    label "prasa"
  ]
  node [
    id 570
    label "psychotest"
  ]
  node [
    id 571
    label "Zwrotnica"
  ]
  node [
    id 572
    label "script"
  ]
  node [
    id 573
    label "czasopismo"
  ]
  node [
    id 574
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 575
    label "adres"
  ]
  node [
    id 576
    label "przekaz"
  ]
  node [
    id 577
    label "grafia"
  ]
  node [
    id 578
    label "interpunkcja"
  ]
  node [
    id 579
    label "handwriting"
  ]
  node [
    id 580
    label "paleograf"
  ]
  node [
    id 581
    label "list"
  ]
  node [
    id 582
    label "sk&#322;adnia"
  ]
  node [
    id 583
    label "morfologia"
  ]
  node [
    id 584
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 585
    label "kategoria_gramatyczna"
  ]
  node [
    id 586
    label "fleksja"
  ]
  node [
    id 587
    label "zasymilowa&#263;"
  ]
  node [
    id 588
    label "transkrypcja"
  ]
  node [
    id 589
    label "g&#322;osownia"
  ]
  node [
    id 590
    label "phonetics"
  ]
  node [
    id 591
    label "zasymilowanie"
  ]
  node [
    id 592
    label "palatogram"
  ]
  node [
    id 593
    label "przek&#322;adanie"
  ]
  node [
    id 594
    label "tekst"
  ]
  node [
    id 595
    label "remark"
  ]
  node [
    id 596
    label "rendition"
  ]
  node [
    id 597
    label "uzasadnianie"
  ]
  node [
    id 598
    label "rozwianie"
  ]
  node [
    id 599
    label "explanation"
  ]
  node [
    id 600
    label "kr&#281;ty"
  ]
  node [
    id 601
    label "bronienie"
  ]
  node [
    id 602
    label "robienie"
  ]
  node [
    id 603
    label "gossip"
  ]
  node [
    id 604
    label "przekonywanie"
  ]
  node [
    id 605
    label "rozwiewanie"
  ]
  node [
    id 606
    label "przedstawianie"
  ]
  node [
    id 607
    label "przekona&#263;"
  ]
  node [
    id 608
    label "zrobi&#263;"
  ]
  node [
    id 609
    label "put"
  ]
  node [
    id 610
    label "frame"
  ]
  node [
    id 611
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 612
    label "zinterpretowa&#263;"
  ]
  node [
    id 613
    label "robi&#263;"
  ]
  node [
    id 614
    label "przekonywa&#263;"
  ]
  node [
    id 615
    label "u&#322;atwia&#263;"
  ]
  node [
    id 616
    label "sprawowa&#263;"
  ]
  node [
    id 617
    label "suplikowa&#263;"
  ]
  node [
    id 618
    label "interpretowa&#263;"
  ]
  node [
    id 619
    label "uzasadnia&#263;"
  ]
  node [
    id 620
    label "give"
  ]
  node [
    id 621
    label "przedstawia&#263;"
  ]
  node [
    id 622
    label "explain"
  ]
  node [
    id 623
    label "poja&#347;nia&#263;"
  ]
  node [
    id 624
    label "broni&#263;"
  ]
  node [
    id 625
    label "przek&#322;ada&#263;"
  ]
  node [
    id 626
    label "elaborate"
  ]
  node [
    id 627
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 628
    label "zrobienie"
  ]
  node [
    id 629
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 630
    label "prawi&#263;"
  ]
  node [
    id 631
    label "express"
  ]
  node [
    id 632
    label "chew_the_fat"
  ]
  node [
    id 633
    label "talk"
  ]
  node [
    id 634
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 635
    label "say"
  ]
  node [
    id 636
    label "wyra&#380;a&#263;"
  ]
  node [
    id 637
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 638
    label "tell"
  ]
  node [
    id 639
    label "informowa&#263;"
  ]
  node [
    id 640
    label "rozmawia&#263;"
  ]
  node [
    id 641
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 642
    label "powiada&#263;"
  ]
  node [
    id 643
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 644
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 645
    label "okre&#347;la&#263;"
  ]
  node [
    id 646
    label "u&#380;ywa&#263;"
  ]
  node [
    id 647
    label "gaworzy&#263;"
  ]
  node [
    id 648
    label "formu&#322;owa&#263;"
  ]
  node [
    id 649
    label "dziama&#263;"
  ]
  node [
    id 650
    label "umie&#263;"
  ]
  node [
    id 651
    label "wydobywa&#263;"
  ]
  node [
    id 652
    label "czucie"
  ]
  node [
    id 653
    label "interpretation"
  ]
  node [
    id 654
    label "realization"
  ]
  node [
    id 655
    label "hermeneutyka"
  ]
  node [
    id 656
    label "kumanie"
  ]
  node [
    id 657
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 658
    label "apprehension"
  ]
  node [
    id 659
    label "wnioskowanie"
  ]
  node [
    id 660
    label "obja&#347;nienie"
  ]
  node [
    id 661
    label "kontekst"
  ]
  node [
    id 662
    label "match"
  ]
  node [
    id 663
    label "see"
  ]
  node [
    id 664
    label "kuma&#263;"
  ]
  node [
    id 665
    label "zna&#263;"
  ]
  node [
    id 666
    label "empatia"
  ]
  node [
    id 667
    label "czu&#263;"
  ]
  node [
    id 668
    label "wiedzie&#263;"
  ]
  node [
    id 669
    label "odbiera&#263;"
  ]
  node [
    id 670
    label "rzemios&#322;o"
  ]
  node [
    id 671
    label "wyrafinowanie"
  ]
  node [
    id 672
    label "pi&#281;kno"
  ]
  node [
    id 673
    label "trade"
  ]
  node [
    id 674
    label "sprawno&#347;&#263;"
  ]
  node [
    id 675
    label "nieszczero&#347;&#263;"
  ]
  node [
    id 676
    label "wymy&#347;lnie"
  ]
  node [
    id 677
    label "polish"
  ]
  node [
    id 678
    label "smakoszostwo"
  ]
  node [
    id 679
    label "wyrachowanie"
  ]
  node [
    id 680
    label "wykwintnie"
  ]
  node [
    id 681
    label "sophistication"
  ]
  node [
    id 682
    label "wyrafinowany"
  ]
  node [
    id 683
    label "kalokagatia"
  ]
  node [
    id 684
    label "jako&#347;&#263;"
  ]
  node [
    id 685
    label "wygl&#261;d"
  ]
  node [
    id 686
    label "beauty"
  ]
  node [
    id 687
    label "gust"
  ]
  node [
    id 688
    label "m&#243;wca"
  ]
  node [
    id 689
    label "Cyceron"
  ]
  node [
    id 690
    label "Katon"
  ]
  node [
    id 691
    label "Goebbels"
  ]
  node [
    id 692
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 693
    label "testify"
  ]
  node [
    id 694
    label "supply"
  ]
  node [
    id 695
    label "pracowa&#263;"
  ]
  node [
    id 696
    label "bespeak"
  ]
  node [
    id 697
    label "us&#322;uga"
  ]
  node [
    id 698
    label "by&#263;"
  ]
  node [
    id 699
    label "sk&#322;ada&#263;"
  ]
  node [
    id 700
    label "represent"
  ]
  node [
    id 701
    label "opowiada&#263;"
  ]
  node [
    id 702
    label "czyni&#263;_dobro"
  ]
  node [
    id 703
    label "op&#322;aca&#263;"
  ]
  node [
    id 704
    label "attest"
  ]
  node [
    id 705
    label "komunikowa&#263;"
  ]
  node [
    id 706
    label "inform"
  ]
  node [
    id 707
    label "zbiera&#263;"
  ]
  node [
    id 708
    label "opracowywa&#263;"
  ]
  node [
    id 709
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 710
    label "oddawa&#263;"
  ]
  node [
    id 711
    label "dawa&#263;"
  ]
  node [
    id 712
    label "uk&#322;ada&#263;"
  ]
  node [
    id 713
    label "convey"
  ]
  node [
    id 714
    label "zmienia&#263;"
  ]
  node [
    id 715
    label "publicize"
  ]
  node [
    id 716
    label "dzieli&#263;"
  ]
  node [
    id 717
    label "przekazywa&#263;"
  ]
  node [
    id 718
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 719
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 720
    label "zestaw"
  ]
  node [
    id 721
    label "przywraca&#263;"
  ]
  node [
    id 722
    label "render"
  ]
  node [
    id 723
    label "scala&#263;"
  ]
  node [
    id 724
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 725
    label "set"
  ]
  node [
    id 726
    label "train"
  ]
  node [
    id 727
    label "relate"
  ]
  node [
    id 728
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 729
    label "work"
  ]
  node [
    id 730
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 731
    label "dzia&#322;a&#263;"
  ]
  node [
    id 732
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 733
    label "tryb"
  ]
  node [
    id 734
    label "endeavor"
  ]
  node [
    id 735
    label "maszyna"
  ]
  node [
    id 736
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 737
    label "funkcjonowa&#263;"
  ]
  node [
    id 738
    label "do"
  ]
  node [
    id 739
    label "bangla&#263;"
  ]
  node [
    id 740
    label "mie&#263;_miejsce"
  ]
  node [
    id 741
    label "podejmowa&#263;"
  ]
  node [
    id 742
    label "osi&#261;ga&#263;"
  ]
  node [
    id 743
    label "finance"
  ]
  node [
    id 744
    label "p&#322;aci&#263;"
  ]
  node [
    id 745
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 746
    label "stan"
  ]
  node [
    id 747
    label "stand"
  ]
  node [
    id 748
    label "trwa&#263;"
  ]
  node [
    id 749
    label "equal"
  ]
  node [
    id 750
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 751
    label "chodzi&#263;"
  ]
  node [
    id 752
    label "uczestniczy&#263;"
  ]
  node [
    id 753
    label "obecno&#347;&#263;"
  ]
  node [
    id 754
    label "si&#281;ga&#263;"
  ]
  node [
    id 755
    label "produkt_gotowy"
  ]
  node [
    id 756
    label "service"
  ]
  node [
    id 757
    label "asortyment"
  ]
  node [
    id 758
    label "&#347;wiadczenie"
  ]
  node [
    id 759
    label "oznaka"
  ]
  node [
    id 760
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 761
    label "term"
  ]
  node [
    id 762
    label "element"
  ]
  node [
    id 763
    label "wytrzyma&#263;"
  ]
  node [
    id 764
    label "trim"
  ]
  node [
    id 765
    label "Osjan"
  ]
  node [
    id 766
    label "formacja"
  ]
  node [
    id 767
    label "point"
  ]
  node [
    id 768
    label "kto&#347;"
  ]
  node [
    id 769
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 770
    label "pozosta&#263;"
  ]
  node [
    id 771
    label "poby&#263;"
  ]
  node [
    id 772
    label "przedstawienie"
  ]
  node [
    id 773
    label "Aspazja"
  ]
  node [
    id 774
    label "go&#347;&#263;"
  ]
  node [
    id 775
    label "budowa"
  ]
  node [
    id 776
    label "osobowo&#347;&#263;"
  ]
  node [
    id 777
    label "charakterystyka"
  ]
  node [
    id 778
    label "kompleksja"
  ]
  node [
    id 779
    label "punkt_widzenia"
  ]
  node [
    id 780
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 781
    label "zaistnie&#263;"
  ]
  node [
    id 782
    label "m&#322;ot"
  ]
  node [
    id 783
    label "marka"
  ]
  node [
    id 784
    label "pr&#243;ba"
  ]
  node [
    id 785
    label "attribute"
  ]
  node [
    id 786
    label "drzewo"
  ]
  node [
    id 787
    label "znak"
  ]
  node [
    id 788
    label "signal"
  ]
  node [
    id 789
    label "implikowa&#263;"
  ]
  node [
    id 790
    label "fakt"
  ]
  node [
    id 791
    label "symbol"
  ]
  node [
    id 792
    label "poj&#281;cie"
  ]
  node [
    id 793
    label "przedmiot"
  ]
  node [
    id 794
    label "materia"
  ]
  node [
    id 795
    label "&#347;rodowisko"
  ]
  node [
    id 796
    label "szkodnik"
  ]
  node [
    id 797
    label "gangsterski"
  ]
  node [
    id 798
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 799
    label "underworld"
  ]
  node [
    id 800
    label "szambo"
  ]
  node [
    id 801
    label "component"
  ]
  node [
    id 802
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 803
    label "r&#243;&#380;niczka"
  ]
  node [
    id 804
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 805
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 806
    label "aspo&#322;eczny"
  ]
  node [
    id 807
    label "performance"
  ]
  node [
    id 808
    label "pracowanie"
  ]
  node [
    id 809
    label "sk&#322;adanie"
  ]
  node [
    id 810
    label "koszt_rodzajowy"
  ]
  node [
    id 811
    label "command"
  ]
  node [
    id 812
    label "znaczenie"
  ]
  node [
    id 813
    label "zobowi&#261;zanie"
  ]
  node [
    id 814
    label "czynienie_dobra"
  ]
  node [
    id 815
    label "p&#322;acenie"
  ]
  node [
    id 816
    label "przepe&#322;nia&#263;"
  ]
  node [
    id 817
    label "tip"
  ]
  node [
    id 818
    label "obdarowywa&#263;"
  ]
  node [
    id 819
    label "przenosi&#263;"
  ]
  node [
    id 820
    label "shed"
  ]
  node [
    id 821
    label "oblewa&#263;"
  ]
  node [
    id 822
    label "spill"
  ]
  node [
    id 823
    label "wlewa&#263;"
  ]
  node [
    id 824
    label "przemieszcza&#263;"
  ]
  node [
    id 825
    label "wp&#322;aca&#263;"
  ]
  node [
    id 826
    label "sygna&#322;"
  ]
  node [
    id 827
    label "powodowa&#263;"
  ]
  node [
    id 828
    label "wysy&#322;a&#263;"
  ]
  node [
    id 829
    label "podawa&#263;"
  ]
  node [
    id 830
    label "impart"
  ]
  node [
    id 831
    label "chowa&#263;"
  ]
  node [
    id 832
    label "harmonize"
  ]
  node [
    id 833
    label "donate"
  ]
  node [
    id 834
    label "udarowywa&#263;"
  ]
  node [
    id 835
    label "circulate"
  ]
  node [
    id 836
    label "kopiowa&#263;"
  ]
  node [
    id 837
    label "dostosowywa&#263;"
  ]
  node [
    id 838
    label "strzela&#263;"
  ]
  node [
    id 839
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 840
    label "go"
  ]
  node [
    id 841
    label "umieszcza&#263;"
  ]
  node [
    id 842
    label "estrange"
  ]
  node [
    id 843
    label "przelatywa&#263;"
  ]
  node [
    id 844
    label "ponosi&#263;"
  ]
  node [
    id 845
    label "rozpowszechnia&#263;"
  ]
  node [
    id 846
    label "transfer"
  ]
  node [
    id 847
    label "infest"
  ]
  node [
    id 848
    label "pocisk"
  ]
  node [
    id 849
    label "glaze"
  ]
  node [
    id 850
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 851
    label "powleka&#263;"
  ]
  node [
    id 852
    label "przegrywa&#263;"
  ]
  node [
    id 853
    label "la&#263;"
  ]
  node [
    id 854
    label "egzamin"
  ]
  node [
    id 855
    label "moczy&#263;"
  ]
  node [
    id 856
    label "barwi&#263;"
  ]
  node [
    id 857
    label "ocenia&#263;"
  ]
  node [
    id 858
    label "zalewa&#263;"
  ]
  node [
    id 859
    label "egzaminowa&#263;"
  ]
  node [
    id 860
    label "op&#322;ywa&#263;"
  ]
  node [
    id 861
    label "przesadza&#263;"
  ]
  node [
    id 862
    label "inspirowa&#263;"
  ]
  node [
    id 863
    label "pour"
  ]
  node [
    id 864
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 865
    label "wype&#322;nia&#263;"
  ]
  node [
    id 866
    label "wmusza&#263;"
  ]
  node [
    id 867
    label "inculcate"
  ]
  node [
    id 868
    label "wzbudza&#263;"
  ]
  node [
    id 869
    label "wpaja&#263;"
  ]
  node [
    id 870
    label "translokowa&#263;"
  ]
  node [
    id 871
    label "meet"
  ]
  node [
    id 872
    label "overflow"
  ]
  node [
    id 873
    label "sygnatariusz"
  ]
  node [
    id 874
    label "tworzywo"
  ]
  node [
    id 875
    label "fascyku&#322;"
  ]
  node [
    id 876
    label "dokumentacja"
  ]
  node [
    id 877
    label "writing"
  ]
  node [
    id 878
    label "artyku&#322;"
  ]
  node [
    id 879
    label "libra"
  ]
  node [
    id 880
    label "parafa"
  ]
  node [
    id 881
    label "raport&#243;wka"
  ]
  node [
    id 882
    label "registratura"
  ]
  node [
    id 883
    label "format_arkusza"
  ]
  node [
    id 884
    label "papeteria"
  ]
  node [
    id 885
    label "nak&#322;uwacz"
  ]
  node [
    id 886
    label "prawda"
  ]
  node [
    id 887
    label "wyr&#243;b"
  ]
  node [
    id 888
    label "blok"
  ]
  node [
    id 889
    label "nag&#322;&#243;wek"
  ]
  node [
    id 890
    label "szkic"
  ]
  node [
    id 891
    label "line"
  ]
  node [
    id 892
    label "rodzajnik"
  ]
  node [
    id 893
    label "fragment"
  ]
  node [
    id 894
    label "towar"
  ]
  node [
    id 895
    label "paragraf"
  ]
  node [
    id 896
    label "znak_j&#281;zykowy"
  ]
  node [
    id 897
    label "substancja"
  ]
  node [
    id 898
    label "p&#322;&#243;d"
  ]
  node [
    id 899
    label "stationery"
  ]
  node [
    id 900
    label "komplet"
  ]
  node [
    id 901
    label "quire"
  ]
  node [
    id 902
    label "jednostka"
  ]
  node [
    id 903
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 904
    label "jednostka_masy"
  ]
  node [
    id 905
    label "waga"
  ]
  node [
    id 906
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 907
    label "szpikulec"
  ]
  node [
    id 908
    label "przedstawiciel"
  ]
  node [
    id 909
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 910
    label "register"
  ]
  node [
    id 911
    label "biuro"
  ]
  node [
    id 912
    label "zbi&#243;r"
  ]
  node [
    id 913
    label "wpis"
  ]
  node [
    id 914
    label "operat"
  ]
  node [
    id 915
    label "ekscerpcja"
  ]
  node [
    id 916
    label "kosztorys"
  ]
  node [
    id 917
    label "materia&#322;"
  ]
  node [
    id 918
    label "torba"
  ]
  node [
    id 919
    label "plik"
  ]
  node [
    id 920
    label "paraph"
  ]
  node [
    id 921
    label "podpis"
  ]
  node [
    id 922
    label "biblioteka"
  ]
  node [
    id 923
    label "odbiorca"
  ]
  node [
    id 924
    label "klient"
  ]
  node [
    id 925
    label "obywatel"
  ]
  node [
    id 926
    label "us&#322;ugobiorca"
  ]
  node [
    id 927
    label "szlachcic"
  ]
  node [
    id 928
    label "agent_rozliczeniowy"
  ]
  node [
    id 929
    label "program"
  ]
  node [
    id 930
    label "bratek"
  ]
  node [
    id 931
    label "klientela"
  ]
  node [
    id 932
    label "Rzymianin"
  ]
  node [
    id 933
    label "komputer_cyfrowy"
  ]
  node [
    id 934
    label "recipient_role"
  ]
  node [
    id 935
    label "otrzymanie"
  ]
  node [
    id 936
    label "otrzymywanie"
  ]
  node [
    id 937
    label "rewers"
  ]
  node [
    id 938
    label "informatorium"
  ]
  node [
    id 939
    label "budynek"
  ]
  node [
    id 940
    label "pok&#243;j"
  ]
  node [
    id 941
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 942
    label "czytelnia"
  ]
  node [
    id 943
    label "kolekcja"
  ]
  node [
    id 944
    label "library"
  ]
  node [
    id 945
    label "programowanie"
  ]
  node [
    id 946
    label "instytucja"
  ]
  node [
    id 947
    label "p&#243;&#378;ny"
  ]
  node [
    id 948
    label "do_p&#243;&#378;na"
  ]
  node [
    id 949
    label "dyspozycja"
  ]
  node [
    id 950
    label "trudno&#347;&#263;"
  ]
  node [
    id 951
    label "adeptness"
  ]
  node [
    id 952
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 953
    label "plan"
  ]
  node [
    id 954
    label "samopoczucie"
  ]
  node [
    id 955
    label "capability"
  ]
  node [
    id 956
    label "kondycja"
  ]
  node [
    id 957
    label "prawo"
  ]
  node [
    id 958
    label "polecenie"
  ]
  node [
    id 959
    label "obstacle"
  ]
  node [
    id 960
    label "difficulty"
  ]
  node [
    id 961
    label "napotka&#263;"
  ]
  node [
    id 962
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 963
    label "subiekcja"
  ]
  node [
    id 964
    label "napotkanie"
  ]
  node [
    id 965
    label "sytuacja"
  ]
  node [
    id 966
    label "poziom"
  ]
  node [
    id 967
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 968
    label "quality"
  ]
  node [
    id 969
    label "co&#347;"
  ]
  node [
    id 970
    label "state"
  ]
  node [
    id 971
    label "warto&#347;&#263;"
  ]
  node [
    id 972
    label "syf"
  ]
  node [
    id 973
    label "smak"
  ]
  node [
    id 974
    label "zesp&#243;&#322;_Kleinego-Levina"
  ]
  node [
    id 975
    label "edacity"
  ]
  node [
    id 976
    label "chciwo&#347;&#263;"
  ]
  node [
    id 977
    label "k&#322;u&#263;_w_z&#281;by"
  ]
  node [
    id 978
    label "zakosztowa&#263;"
  ]
  node [
    id 979
    label "aromat"
  ]
  node [
    id 980
    label "kubek_smakowy"
  ]
  node [
    id 981
    label "pa&#322;aszowanie"
  ]
  node [
    id 982
    label "smakowo&#347;&#263;"
  ]
  node [
    id 983
    label "pa&#322;aszowa&#263;"
  ]
  node [
    id 984
    label "zmys&#322;"
  ]
  node [
    id 985
    label "tendency"
  ]
  node [
    id 986
    label "smakowa&#263;"
  ]
  node [
    id 987
    label "istota"
  ]
  node [
    id 988
    label "wywar"
  ]
  node [
    id 989
    label "delikatno&#347;&#263;"
  ]
  node [
    id 990
    label "feblik"
  ]
  node [
    id 991
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 992
    label "g&#322;&#243;d"
  ]
  node [
    id 993
    label "nip"
  ]
  node [
    id 994
    label "taste"
  ]
  node [
    id 995
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 996
    label "k&#322;ucie_w_z&#281;by"
  ]
  node [
    id 997
    label "smakowanie"
  ]
  node [
    id 998
    label "hunger"
  ]
  node [
    id 999
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1000
    label "chciwie"
  ]
  node [
    id 1001
    label "desire"
  ]
  node [
    id 1002
    label "chciwstwo"
  ]
  node [
    id 1003
    label "jaki&#347;"
  ]
  node [
    id 1004
    label "pewien"
  ]
  node [
    id 1005
    label "spokojny"
  ]
  node [
    id 1006
    label "mo&#380;liwy"
  ]
  node [
    id 1007
    label "ufanie"
  ]
  node [
    id 1008
    label "wierzenie"
  ]
  node [
    id 1009
    label "upewnianie_si&#281;"
  ]
  node [
    id 1010
    label "upewnienie_si&#281;"
  ]
  node [
    id 1011
    label "jako&#347;"
  ]
  node [
    id 1012
    label "niez&#322;y"
  ]
  node [
    id 1013
    label "charakterystyczny"
  ]
  node [
    id 1014
    label "jako_tako"
  ]
  node [
    id 1015
    label "przyzwoity"
  ]
  node [
    id 1016
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 1017
    label "okulista"
  ]
  node [
    id 1018
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 1019
    label "oko"
  ]
  node [
    id 1020
    label "widzenie"
  ]
  node [
    id 1021
    label "m&#281;tnienie"
  ]
  node [
    id 1022
    label "m&#281;tnie&#263;"
  ]
  node [
    id 1023
    label "widzie&#263;"
  ]
  node [
    id 1024
    label "expression"
  ]
  node [
    id 1025
    label "kontakt"
  ]
  node [
    id 1026
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 1027
    label "synestezja"
  ]
  node [
    id 1028
    label "wdarcie_si&#281;"
  ]
  node [
    id 1029
    label "doznanie"
  ]
  node [
    id 1030
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 1031
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1032
    label "flare"
  ]
  node [
    id 1033
    label "styk"
  ]
  node [
    id 1034
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 1035
    label "&#322;&#261;cznik"
  ]
  node [
    id 1036
    label "soczewka"
  ]
  node [
    id 1037
    label "association"
  ]
  node [
    id 1038
    label "zwi&#261;zek"
  ]
  node [
    id 1039
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1040
    label "socket"
  ]
  node [
    id 1041
    label "regulator"
  ]
  node [
    id 1042
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 1043
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1044
    label "instalacja_elektryczna"
  ]
  node [
    id 1045
    label "linkage"
  ]
  node [
    id 1046
    label "formacja_geologiczna"
  ]
  node [
    id 1047
    label "contact"
  ]
  node [
    id 1048
    label "katalizator"
  ]
  node [
    id 1049
    label "aprobowanie"
  ]
  node [
    id 1050
    label "wyobra&#380;anie_sobie"
  ]
  node [
    id 1051
    label "odwiedziny"
  ]
  node [
    id 1052
    label "&#347;nienie"
  ]
  node [
    id 1053
    label "widywanie"
  ]
  node [
    id 1054
    label "uznawanie"
  ]
  node [
    id 1055
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1056
    label "zmalenie"
  ]
  node [
    id 1057
    label "sojourn"
  ]
  node [
    id 1058
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1059
    label "zobaczenie"
  ]
  node [
    id 1060
    label "malenie"
  ]
  node [
    id 1061
    label "reagowanie"
  ]
  node [
    id 1062
    label "przejrzenie"
  ]
  node [
    id 1063
    label "zwi&#281;kszanie_si&#281;"
  ]
  node [
    id 1064
    label "obejrzenie"
  ]
  node [
    id 1065
    label "przywidzenie"
  ]
  node [
    id 1066
    label "visit"
  ]
  node [
    id 1067
    label "view"
  ]
  node [
    id 1068
    label "ogl&#261;danie"
  ]
  node [
    id 1069
    label "ocenianie"
  ]
  node [
    id 1070
    label "u&#322;uda"
  ]
  node [
    id 1071
    label "patrzenie"
  ]
  node [
    id 1072
    label "postrzeganie"
  ]
  node [
    id 1073
    label "dostrzeganie"
  ]
  node [
    id 1074
    label "wychodzenie"
  ]
  node [
    id 1075
    label "przegl&#261;danie"
  ]
  node [
    id 1076
    label "vision"
  ]
  node [
    id 1077
    label "pojmowanie"
  ]
  node [
    id 1078
    label "specjalista"
  ]
  node [
    id 1079
    label "okulary"
  ]
  node [
    id 1080
    label "ogl&#261;da&#263;"
  ]
  node [
    id 1081
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 1082
    label "perceive"
  ]
  node [
    id 1083
    label "zmale&#263;"
  ]
  node [
    id 1084
    label "male&#263;"
  ]
  node [
    id 1085
    label "go_steady"
  ]
  node [
    id 1086
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 1087
    label "postrzega&#263;"
  ]
  node [
    id 1088
    label "dostrzega&#263;"
  ]
  node [
    id 1089
    label "aprobowa&#263;"
  ]
  node [
    id 1090
    label "spowodowa&#263;"
  ]
  node [
    id 1091
    label "os&#261;dza&#263;"
  ]
  node [
    id 1092
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1093
    label "spotka&#263;"
  ]
  node [
    id 1094
    label "reagowa&#263;"
  ]
  node [
    id 1095
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 1096
    label "nerw_wzrokowy"
  ]
  node [
    id 1097
    label "&#347;lepie"
  ]
  node [
    id 1098
    label "&#378;renica"
  ]
  node [
    id 1099
    label "organ"
  ]
  node [
    id 1100
    label "twarz"
  ]
  node [
    id 1101
    label "&#347;lepko"
  ]
  node [
    id 1102
    label "uwaga"
  ]
  node [
    id 1103
    label "ga&#322;ka_oczna"
  ]
  node [
    id 1104
    label "oczy"
  ]
  node [
    id 1105
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 1106
    label "ros&#243;&#322;"
  ]
  node [
    id 1107
    label "net"
  ]
  node [
    id 1108
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 1109
    label "kaprawienie"
  ]
  node [
    id 1110
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 1111
    label "powieka"
  ]
  node [
    id 1112
    label "siniec"
  ]
  node [
    id 1113
    label "rzecz"
  ]
  node [
    id 1114
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 1115
    label "coloboma"
  ]
  node [
    id 1116
    label "spoj&#243;wka"
  ]
  node [
    id 1117
    label "spojrzenie"
  ]
  node [
    id 1118
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 1119
    label "kaprawie&#263;"
  ]
  node [
    id 1120
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 1121
    label "m&#261;ci&#263;_si&#281;"
  ]
  node [
    id 1122
    label "tarnish"
  ]
  node [
    id 1123
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1124
    label "zaciera&#263;_si&#281;"
  ]
  node [
    id 1125
    label "zacieranie_si&#281;"
  ]
  node [
    id 1126
    label "stawanie_si&#281;"
  ]
  node [
    id 1127
    label "niewyra&#378;ny"
  ]
  node [
    id 1128
    label "jeer"
  ]
  node [
    id 1129
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 1130
    label "gulp"
  ]
  node [
    id 1131
    label "czyta&#263;"
  ]
  node [
    id 1132
    label "przesuwa&#263;"
  ]
  node [
    id 1133
    label "zabiera&#263;"
  ]
  node [
    id 1134
    label "poch&#322;ania&#263;"
  ]
  node [
    id 1135
    label "swallow"
  ]
  node [
    id 1136
    label "przyswaja&#263;"
  ]
  node [
    id 1137
    label "odczytywa&#263;"
  ]
  node [
    id 1138
    label "przetwarza&#263;"
  ]
  node [
    id 1139
    label "obserwowa&#263;"
  ]
  node [
    id 1140
    label "dysleksja"
  ]
  node [
    id 1141
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 1142
    label "read"
  ]
  node [
    id 1143
    label "poznawa&#263;"
  ]
  node [
    id 1144
    label "konsumowa&#263;"
  ]
  node [
    id 1145
    label "rusza&#263;"
  ]
  node [
    id 1146
    label "translate"
  ]
  node [
    id 1147
    label "postpone"
  ]
  node [
    id 1148
    label "przestawia&#263;"
  ]
  node [
    id 1149
    label "chronometria"
  ]
  node [
    id 1150
    label "odczyt"
  ]
  node [
    id 1151
    label "laba"
  ]
  node [
    id 1152
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1153
    label "time_period"
  ]
  node [
    id 1154
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1155
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1156
    label "Zeitgeist"
  ]
  node [
    id 1157
    label "pochodzenie"
  ]
  node [
    id 1158
    label "przep&#322;ywanie"
  ]
  node [
    id 1159
    label "schy&#322;ek"
  ]
  node [
    id 1160
    label "czwarty_wymiar"
  ]
  node [
    id 1161
    label "poprzedzi&#263;"
  ]
  node [
    id 1162
    label "pogoda"
  ]
  node [
    id 1163
    label "czasokres"
  ]
  node [
    id 1164
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1165
    label "poprzedzenie"
  ]
  node [
    id 1166
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1167
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1168
    label "dzieje"
  ]
  node [
    id 1169
    label "zegar"
  ]
  node [
    id 1170
    label "trawi&#263;"
  ]
  node [
    id 1171
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1172
    label "poprzedza&#263;"
  ]
  node [
    id 1173
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1174
    label "trawienie"
  ]
  node [
    id 1175
    label "chwila"
  ]
  node [
    id 1176
    label "rachuba_czasu"
  ]
  node [
    id 1177
    label "poprzedzanie"
  ]
  node [
    id 1178
    label "okres_czasu"
  ]
  node [
    id 1179
    label "period"
  ]
  node [
    id 1180
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1181
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1182
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1183
    label "pochodzi&#263;"
  ]
  node [
    id 1184
    label "time"
  ]
  node [
    id 1185
    label "reading"
  ]
  node [
    id 1186
    label "handout"
  ]
  node [
    id 1187
    label "podawanie"
  ]
  node [
    id 1188
    label "wyk&#322;ad"
  ]
  node [
    id 1189
    label "lecture"
  ]
  node [
    id 1190
    label "pomiar"
  ]
  node [
    id 1191
    label "meteorology"
  ]
  node [
    id 1192
    label "weather"
  ]
  node [
    id 1193
    label "atak"
  ]
  node [
    id 1194
    label "prognoza_meteorologiczna"
  ]
  node [
    id 1195
    label "potrzyma&#263;"
  ]
  node [
    id 1196
    label "czas_wolny"
  ]
  node [
    id 1197
    label "metrologia"
  ]
  node [
    id 1198
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1199
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 1200
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 1201
    label "czasomierz"
  ]
  node [
    id 1202
    label "tyka&#263;"
  ]
  node [
    id 1203
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 1204
    label "tykn&#261;&#263;"
  ]
  node [
    id 1205
    label "nabicie"
  ]
  node [
    id 1206
    label "bicie"
  ]
  node [
    id 1207
    label "kotwica"
  ]
  node [
    id 1208
    label "godzinnik"
  ]
  node [
    id 1209
    label "werk"
  ]
  node [
    id 1210
    label "urz&#261;dzenie"
  ]
  node [
    id 1211
    label "wahad&#322;o"
  ]
  node [
    id 1212
    label "kurant"
  ]
  node [
    id 1213
    label "cyferblat"
  ]
  node [
    id 1214
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 1215
    label "coupling"
  ]
  node [
    id 1216
    label "orz&#281;sek"
  ]
  node [
    id 1217
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1218
    label "background"
  ]
  node [
    id 1219
    label "str&#243;j"
  ]
  node [
    id 1220
    label "wynikanie"
  ]
  node [
    id 1221
    label "origin"
  ]
  node [
    id 1222
    label "zaczynanie_si&#281;"
  ]
  node [
    id 1223
    label "beginning"
  ]
  node [
    id 1224
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 1225
    label "geneza"
  ]
  node [
    id 1226
    label "marnowanie"
  ]
  node [
    id 1227
    label "unicestwianie"
  ]
  node [
    id 1228
    label "sp&#281;dzanie"
  ]
  node [
    id 1229
    label "digestion"
  ]
  node [
    id 1230
    label "perystaltyka"
  ]
  node [
    id 1231
    label "proces_fizjologiczny"
  ]
  node [
    id 1232
    label "rozk&#322;adanie"
  ]
  node [
    id 1233
    label "przetrawianie"
  ]
  node [
    id 1234
    label "contemplation"
  ]
  node [
    id 1235
    label "proceed"
  ]
  node [
    id 1236
    label "mija&#263;"
  ]
  node [
    id 1237
    label "sail"
  ]
  node [
    id 1238
    label "przebywa&#263;"
  ]
  node [
    id 1239
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1240
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 1241
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 1242
    label "carry"
  ]
  node [
    id 1243
    label "go&#347;ci&#263;"
  ]
  node [
    id 1244
    label "zanikni&#281;cie"
  ]
  node [
    id 1245
    label "departure"
  ]
  node [
    id 1246
    label "odej&#347;cie"
  ]
  node [
    id 1247
    label "opuszczenie"
  ]
  node [
    id 1248
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 1249
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 1250
    label "ciecz"
  ]
  node [
    id 1251
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1252
    label "oddalenie_si&#281;"
  ]
  node [
    id 1253
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 1254
    label "cross"
  ]
  node [
    id 1255
    label "swimming"
  ]
  node [
    id 1256
    label "min&#261;&#263;"
  ]
  node [
    id 1257
    label "przeby&#263;"
  ]
  node [
    id 1258
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 1259
    label "zago&#347;ci&#263;"
  ]
  node [
    id 1260
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 1261
    label "overwhelm"
  ]
  node [
    id 1262
    label "opatrzy&#263;"
  ]
  node [
    id 1263
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1264
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1265
    label "opatrywa&#263;"
  ]
  node [
    id 1266
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1267
    label "bolt"
  ]
  node [
    id 1268
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1269
    label "date"
  ]
  node [
    id 1270
    label "fall"
  ]
  node [
    id 1271
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1272
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 1273
    label "wynika&#263;"
  ]
  node [
    id 1274
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1275
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1276
    label "progress"
  ]
  node [
    id 1277
    label "opatrzenie"
  ]
  node [
    id 1278
    label "opatrywanie"
  ]
  node [
    id 1279
    label "przebycie"
  ]
  node [
    id 1280
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1281
    label "mini&#281;cie"
  ]
  node [
    id 1282
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 1283
    label "zaistnienie"
  ]
  node [
    id 1284
    label "cruise"
  ]
  node [
    id 1285
    label "lutowa&#263;"
  ]
  node [
    id 1286
    label "metal"
  ]
  node [
    id 1287
    label "przetrawia&#263;"
  ]
  node [
    id 1288
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1289
    label "digest"
  ]
  node [
    id 1290
    label "usuwa&#263;"
  ]
  node [
    id 1291
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 1292
    label "sp&#281;dza&#263;"
  ]
  node [
    id 1293
    label "marnowa&#263;"
  ]
  node [
    id 1294
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1295
    label "zjawianie_si&#281;"
  ]
  node [
    id 1296
    label "mijanie"
  ]
  node [
    id 1297
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 1298
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1299
    label "flux"
  ]
  node [
    id 1300
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1301
    label "zaznawanie"
  ]
  node [
    id 1302
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1303
    label "charakter"
  ]
  node [
    id 1304
    label "epoka"
  ]
  node [
    id 1305
    label "ciota"
  ]
  node [
    id 1306
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1307
    label "flow"
  ]
  node [
    id 1308
    label "choroba_przyrodzona"
  ]
  node [
    id 1309
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 1310
    label "kres"
  ]
  node [
    id 1311
    label "przestrze&#324;"
  ]
  node [
    id 1312
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1313
    label "skorzysta&#263;"
  ]
  node [
    id 1314
    label "seize"
  ]
  node [
    id 1315
    label "dotrze&#263;"
  ]
  node [
    id 1316
    label "fall_upon"
  ]
  node [
    id 1317
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1318
    label "appreciation"
  ]
  node [
    id 1319
    label "allude"
  ]
  node [
    id 1320
    label "u&#380;y&#263;"
  ]
  node [
    id 1321
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 1322
    label "string"
  ]
  node [
    id 1323
    label "za&#347;piewa&#263;"
  ]
  node [
    id 1324
    label "dane"
  ]
  node [
    id 1325
    label "nak&#322;oni&#263;"
  ]
  node [
    id 1326
    label "zarobi&#263;"
  ]
  node [
    id 1327
    label "draw"
  ]
  node [
    id 1328
    label "wy&#322;udzi&#263;"
  ]
  node [
    id 1329
    label "nabra&#263;"
  ]
  node [
    id 1330
    label "obrysowa&#263;"
  ]
  node [
    id 1331
    label "wydosta&#263;"
  ]
  node [
    id 1332
    label "rozprostowa&#263;"
  ]
  node [
    id 1333
    label "ocali&#263;"
  ]
  node [
    id 1334
    label "remove"
  ]
  node [
    id 1335
    label "zmusi&#263;"
  ]
  node [
    id 1336
    label "wypomnie&#263;"
  ]
  node [
    id 1337
    label "p&#281;d"
  ]
  node [
    id 1338
    label "pozyska&#263;"
  ]
  node [
    id 1339
    label "przypomnie&#263;"
  ]
  node [
    id 1340
    label "zabra&#263;"
  ]
  node [
    id 1341
    label "mienie"
  ]
  node [
    id 1342
    label "rozci&#261;gn&#261;&#263;"
  ]
  node [
    id 1343
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1344
    label "perpetrate"
  ]
  node [
    id 1345
    label "drag"
  ]
  node [
    id 1346
    label "describe"
  ]
  node [
    id 1347
    label "make"
  ]
  node [
    id 1348
    label "score"
  ]
  node [
    id 1349
    label "uzyska&#263;"
  ]
  node [
    id 1350
    label "profit"
  ]
  node [
    id 1351
    label "wykorzysta&#263;"
  ]
  node [
    id 1352
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1353
    label "dozna&#263;"
  ]
  node [
    id 1354
    label "employment"
  ]
  node [
    id 1355
    label "utilize"
  ]
  node [
    id 1356
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1357
    label "become"
  ]
  node [
    id 1358
    label "get"
  ]
  node [
    id 1359
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 1360
    label "advance"
  ]
  node [
    id 1361
    label "silnik"
  ]
  node [
    id 1362
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 1363
    label "dorobi&#263;"
  ]
  node [
    id 1364
    label "utrze&#263;"
  ]
  node [
    id 1365
    label "dopasowa&#263;"
  ]
  node [
    id 1366
    label "catch"
  ]
  node [
    id 1367
    label "znale&#378;&#263;"
  ]
  node [
    id 1368
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 1369
    label "s&#322;onecznie"
  ]
  node [
    id 1370
    label "pogodny"
  ]
  node [
    id 1371
    label "letni"
  ]
  node [
    id 1372
    label "weso&#322;y"
  ]
  node [
    id 1373
    label "bezchmurny"
  ]
  node [
    id 1374
    label "jasny"
  ]
  node [
    id 1375
    label "bezdeszczowy"
  ]
  node [
    id 1376
    label "ciep&#322;y"
  ]
  node [
    id 1377
    label "fotowoltaiczny"
  ]
  node [
    id 1378
    label "niezm&#261;cony"
  ]
  node [
    id 1379
    label "jednoznaczny"
  ]
  node [
    id 1380
    label "o&#347;wietlenie"
  ]
  node [
    id 1381
    label "bia&#322;y"
  ]
  node [
    id 1382
    label "klarowny"
  ]
  node [
    id 1383
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 1384
    label "jasno"
  ]
  node [
    id 1385
    label "o&#347;wietlanie"
  ]
  node [
    id 1386
    label "grzanie"
  ]
  node [
    id 1387
    label "ocieplenie"
  ]
  node [
    id 1388
    label "ciep&#322;o"
  ]
  node [
    id 1389
    label "zagrzanie"
  ]
  node [
    id 1390
    label "ocieplanie"
  ]
  node [
    id 1391
    label "przyjemny"
  ]
  node [
    id 1392
    label "ocieplenie_si&#281;"
  ]
  node [
    id 1393
    label "ocieplanie_si&#281;"
  ]
  node [
    id 1394
    label "mi&#322;y"
  ]
  node [
    id 1395
    label "pozytywny"
  ]
  node [
    id 1396
    label "udany"
  ]
  node [
    id 1397
    label "&#322;adny"
  ]
  node [
    id 1398
    label "pogodnie"
  ]
  node [
    id 1399
    label "beztroski"
  ]
  node [
    id 1400
    label "pijany"
  ]
  node [
    id 1401
    label "weso&#322;o"
  ]
  node [
    id 1402
    label "wolny"
  ]
  node [
    id 1403
    label "bezchmurnie"
  ]
  node [
    id 1404
    label "bezopadowy"
  ]
  node [
    id 1405
    label "bezdeszczowo"
  ]
  node [
    id 1406
    label "ekologiczny"
  ]
  node [
    id 1407
    label "oboj&#281;tny"
  ]
  node [
    id 1408
    label "typowy"
  ]
  node [
    id 1409
    label "letnio"
  ]
  node [
    id 1410
    label "sezonowy"
  ]
  node [
    id 1411
    label "latowy"
  ]
  node [
    id 1412
    label "nijaki"
  ]
  node [
    id 1413
    label "rozeta"
  ]
  node [
    id 1414
    label "odrobina"
  ]
  node [
    id 1415
    label "pi&#243;rko"
  ]
  node [
    id 1416
    label "odcinek"
  ]
  node [
    id 1417
    label "wyrostek"
  ]
  node [
    id 1418
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1419
    label "woda_powierzchniowa"
  ]
  node [
    id 1420
    label "Ajgospotamoj"
  ]
  node [
    id 1421
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1422
    label "chordofon_szarpany"
  ]
  node [
    id 1423
    label "pi&#243;ro"
  ]
  node [
    id 1424
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 1425
    label "pole"
  ]
  node [
    id 1426
    label "part"
  ]
  node [
    id 1427
    label "kawa&#322;ek"
  ]
  node [
    id 1428
    label "teren"
  ]
  node [
    id 1429
    label "coupon"
  ]
  node [
    id 1430
    label "epizod"
  ]
  node [
    id 1431
    label "moneta"
  ]
  node [
    id 1432
    label "pokwitowanie"
  ]
  node [
    id 1433
    label "ch&#322;opiec"
  ]
  node [
    id 1434
    label "zako&#324;czenie"
  ]
  node [
    id 1435
    label "jelito_&#347;lepe"
  ]
  node [
    id 1436
    label "tw&#243;r"
  ]
  node [
    id 1437
    label "narz&#261;d_limfoidalny"
  ]
  node [
    id 1438
    label "punkt_McBurneya"
  ]
  node [
    id 1439
    label "m&#322;okos"
  ]
  node [
    id 1440
    label "grain"
  ]
  node [
    id 1441
    label "dash"
  ]
  node [
    id 1442
    label "intensywno&#347;&#263;"
  ]
  node [
    id 1443
    label "przewidywanie"
  ]
  node [
    id 1444
    label "zawiadomienie"
  ]
  node [
    id 1445
    label "declaration"
  ]
  node [
    id 1446
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 1447
    label "plama"
  ]
  node [
    id 1448
    label "b&#322;ysk"
  ]
  node [
    id 1449
    label "rzuca&#263;"
  ]
  node [
    id 1450
    label "przy&#263;miewanie"
  ]
  node [
    id 1451
    label "energia"
  ]
  node [
    id 1452
    label "przy&#263;mienie"
  ]
  node [
    id 1453
    label "radiance"
  ]
  node [
    id 1454
    label "instalacja"
  ]
  node [
    id 1455
    label "lampa"
  ]
  node [
    id 1456
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 1457
    label "rzuci&#263;"
  ]
  node [
    id 1458
    label "wpa&#347;&#263;"
  ]
  node [
    id 1459
    label "fotokataliza"
  ]
  node [
    id 1460
    label "&#347;rednica"
  ]
  node [
    id 1461
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 1462
    label "wpadanie"
  ]
  node [
    id 1463
    label "rzucanie"
  ]
  node [
    id 1464
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1465
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 1466
    label "&#347;wieci&#263;"
  ]
  node [
    id 1467
    label "wpada&#263;"
  ]
  node [
    id 1468
    label "promieniowanie_optyczne"
  ]
  node [
    id 1469
    label "interpretacja"
  ]
  node [
    id 1470
    label "&#347;wiecenie"
  ]
  node [
    id 1471
    label "odst&#281;p"
  ]
  node [
    id 1472
    label "lighter"
  ]
  node [
    id 1473
    label "ja&#347;nia"
  ]
  node [
    id 1474
    label "rzucenie"
  ]
  node [
    id 1475
    label "lighting"
  ]
  node [
    id 1476
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1477
    label "&#347;wiat&#322;y"
  ]
  node [
    id 1478
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 1479
    label "przy&#263;mi&#263;"
  ]
  node [
    id 1480
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 1481
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 1482
    label "m&#261;drze"
  ]
  node [
    id 1483
    label "wpadni&#281;cie"
  ]
  node [
    id 1484
    label "obsadnik"
  ]
  node [
    id 1485
    label "light"
  ]
  node [
    id 1486
    label "star"
  ]
  node [
    id 1487
    label "witra&#380;"
  ]
  node [
    id 1488
    label "okno"
  ]
  node [
    id 1489
    label "ornament"
  ]
  node [
    id 1490
    label "oko_opatrzno&#347;ci"
  ]
  node [
    id 1491
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1492
    label "znak_zodiaku"
  ]
  node [
    id 1493
    label "bezchmurno&#347;&#263;"
  ]
  node [
    id 1494
    label "za&#347;wiaty"
  ]
  node [
    id 1495
    label "ofiarowywa&#263;"
  ]
  node [
    id 1496
    label "si&#322;a"
  ]
  node [
    id 1497
    label "Tr&#243;jca_&#346;wi&#281;ta"
  ]
  node [
    id 1498
    label "zodiak"
  ]
  node [
    id 1499
    label "absolut"
  ]
  node [
    id 1500
    label "Kr&#243;lestwo_Niebieskie"
  ]
  node [
    id 1501
    label "Waruna"
  ]
  node [
    id 1502
    label "ofiarowa&#263;"
  ]
  node [
    id 1503
    label "opaczno&#347;&#263;"
  ]
  node [
    id 1504
    label "czczenie"
  ]
  node [
    id 1505
    label "ofiarowywanie"
  ]
  node [
    id 1506
    label "ofiarowanie"
  ]
  node [
    id 1507
    label "capacity"
  ]
  node [
    id 1508
    label "zaleta"
  ]
  node [
    id 1509
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1510
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1511
    label "parametr"
  ]
  node [
    id 1512
    label "moment_si&#322;y"
  ]
  node [
    id 1513
    label "wuchta"
  ]
  node [
    id 1514
    label "magnitude"
  ]
  node [
    id 1515
    label "potencja"
  ]
  node [
    id 1516
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1517
    label "Gargantua"
  ]
  node [
    id 1518
    label "Chocho&#322;"
  ]
  node [
    id 1519
    label "Hamlet"
  ]
  node [
    id 1520
    label "Wallenrod"
  ]
  node [
    id 1521
    label "Quasimodo"
  ]
  node [
    id 1522
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1523
    label "Plastu&#347;"
  ]
  node [
    id 1524
    label "Casanova"
  ]
  node [
    id 1525
    label "Szwejk"
  ]
  node [
    id 1526
    label "Don_Juan"
  ]
  node [
    id 1527
    label "Edyp"
  ]
  node [
    id 1528
    label "Werter"
  ]
  node [
    id 1529
    label "person"
  ]
  node [
    id 1530
    label "Harry_Potter"
  ]
  node [
    id 1531
    label "Sherlock_Holmes"
  ]
  node [
    id 1532
    label "Dwukwiat"
  ]
  node [
    id 1533
    label "Winnetou"
  ]
  node [
    id 1534
    label "Don_Kiszot"
  ]
  node [
    id 1535
    label "Herkules_Poirot"
  ]
  node [
    id 1536
    label "Faust"
  ]
  node [
    id 1537
    label "Zgredek"
  ]
  node [
    id 1538
    label "Dulcynea"
  ]
  node [
    id 1539
    label "olejek_eteryczny"
  ]
  node [
    id 1540
    label "uk&#322;ad"
  ]
  node [
    id 1541
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1542
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1543
    label "integer"
  ]
  node [
    id 1544
    label "zlewanie_si&#281;"
  ]
  node [
    id 1545
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1546
    label "punkt"
  ]
  node [
    id 1547
    label "oktant"
  ]
  node [
    id 1548
    label "przedzielenie"
  ]
  node [
    id 1549
    label "przedzieli&#263;"
  ]
  node [
    id 1550
    label "przestw&#243;r"
  ]
  node [
    id 1551
    label "rozdziela&#263;"
  ]
  node [
    id 1552
    label "nielito&#347;ciwy"
  ]
  node [
    id 1553
    label "niezmierzony"
  ]
  node [
    id 1554
    label "bezbrze&#380;e"
  ]
  node [
    id 1555
    label "rozdzielanie"
  ]
  node [
    id 1556
    label "przeciwie&#324;stwo"
  ]
  node [
    id 1557
    label "b&#322;&#281;dno&#347;&#263;"
  ]
  node [
    id 1558
    label "B&#243;g"
  ]
  node [
    id 1559
    label "bo&#380;ek"
  ]
  node [
    id 1560
    label "tender"
  ]
  node [
    id 1561
    label "volunteer"
  ]
  node [
    id 1562
    label "oferowa&#263;"
  ]
  node [
    id 1563
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 1564
    label "darowywa&#263;"
  ]
  node [
    id 1565
    label "zapewnia&#263;"
  ]
  node [
    id 1566
    label "podarowanie"
  ]
  node [
    id 1567
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 1568
    label "oferowanie"
  ]
  node [
    id 1569
    label "b&#243;g"
  ]
  node [
    id 1570
    label "forfeit"
  ]
  node [
    id 1571
    label "deklarowanie"
  ]
  node [
    id 1572
    label "zdeklarowanie"
  ]
  node [
    id 1573
    label "sacrifice"
  ]
  node [
    id 1574
    label "msza"
  ]
  node [
    id 1575
    label "crack"
  ]
  node [
    id 1576
    label "zaproponowanie"
  ]
  node [
    id 1577
    label "po&#347;wi&#281;ci&#263;"
  ]
  node [
    id 1578
    label "podarowa&#263;"
  ]
  node [
    id 1579
    label "zaproponowa&#263;"
  ]
  node [
    id 1580
    label "deklarowa&#263;"
  ]
  node [
    id 1581
    label "zdeklarowa&#263;"
  ]
  node [
    id 1582
    label "afford"
  ]
  node [
    id 1583
    label "celebration"
  ]
  node [
    id 1584
    label "fear"
  ]
  node [
    id 1585
    label "okazywanie"
  ]
  node [
    id 1586
    label "s&#322;u&#380;enie"
  ]
  node [
    id 1587
    label "szanowanie"
  ]
  node [
    id 1588
    label "devotion"
  ]
  node [
    id 1589
    label "zapewnianie"
  ]
  node [
    id 1590
    label "po&#347;wi&#281;canie"
  ]
  node [
    id 1591
    label "darowywanie"
  ]
  node [
    id 1592
    label "niebo"
  ]
  node [
    id 1593
    label "brak"
  ]
  node [
    id 1594
    label "hinduizm"
  ]
  node [
    id 1595
    label "pas"
  ]
  node [
    id 1596
    label "ekliptyka"
  ]
  node [
    id 1597
    label "zodiac"
  ]
  node [
    id 1598
    label "fly"
  ]
  node [
    id 1599
    label "induce"
  ]
  node [
    id 1600
    label "ruszy&#263;"
  ]
  node [
    id 1601
    label "zacz&#261;&#263;"
  ]
  node [
    id 1602
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1603
    label "motivate"
  ]
  node [
    id 1604
    label "cut"
  ]
  node [
    id 1605
    label "wzbudzi&#263;"
  ]
  node [
    id 1606
    label "stimulate"
  ]
  node [
    id 1607
    label "ochrona"
  ]
  node [
    id 1608
    label "przegroda"
  ]
  node [
    id 1609
    label "os&#322;ona"
  ]
  node [
    id 1610
    label "obronienie"
  ]
  node [
    id 1611
    label "przy&#322;bica"
  ]
  node [
    id 1612
    label "dekoracja_okna"
  ]
  node [
    id 1613
    label "tarcza"
  ]
  node [
    id 1614
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 1615
    label "borowiec"
  ]
  node [
    id 1616
    label "obstawienie"
  ]
  node [
    id 1617
    label "chemical_bond"
  ]
  node [
    id 1618
    label "obiekt"
  ]
  node [
    id 1619
    label "obstawianie"
  ]
  node [
    id 1620
    label "transportacja"
  ]
  node [
    id 1621
    label "obstawia&#263;"
  ]
  node [
    id 1622
    label "ubezpieczenie"
  ]
  node [
    id 1623
    label "pomieszczenie"
  ]
  node [
    id 1624
    label "przedzia&#322;"
  ]
  node [
    id 1625
    label "przeszkoda"
  ]
  node [
    id 1626
    label "preservation"
  ]
  node [
    id 1627
    label "usprawiedliwienie"
  ]
  node [
    id 1628
    label "zagranie"
  ]
  node [
    id 1629
    label "defense"
  ]
  node [
    id 1630
    label "refutation"
  ]
  node [
    id 1631
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1632
    label "oddzia&#322;"
  ]
  node [
    id 1633
    label "farmaceutyk"
  ]
  node [
    id 1634
    label "operacja"
  ]
  node [
    id 1635
    label "maska"
  ]
  node [
    id 1636
    label "he&#322;m_bojowy"
  ]
  node [
    id 1637
    label "wyraz_twarzy"
  ]
  node [
    id 1638
    label "p&#243;&#322;profil"
  ]
  node [
    id 1639
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 1640
    label "rys"
  ]
  node [
    id 1641
    label "brew"
  ]
  node [
    id 1642
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1643
    label "micha"
  ]
  node [
    id 1644
    label "ucho"
  ]
  node [
    id 1645
    label "profil"
  ]
  node [
    id 1646
    label "podbr&#243;dek"
  ]
  node [
    id 1647
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 1648
    label "policzek"
  ]
  node [
    id 1649
    label "cera"
  ]
  node [
    id 1650
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 1651
    label "czo&#322;o"
  ]
  node [
    id 1652
    label "uj&#281;cie"
  ]
  node [
    id 1653
    label "dzi&#243;b"
  ]
  node [
    id 1654
    label "maskowato&#347;&#263;"
  ]
  node [
    id 1655
    label "nos"
  ]
  node [
    id 1656
    label "wielko&#347;&#263;"
  ]
  node [
    id 1657
    label "prz&#243;d"
  ]
  node [
    id 1658
    label "twarzyczka"
  ]
  node [
    id 1659
    label "pysk"
  ]
  node [
    id 1660
    label "reputacja"
  ]
  node [
    id 1661
    label "liczko"
  ]
  node [
    id 1662
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 1663
    label "usta"
  ]
  node [
    id 1664
    label "p&#322;e&#263;"
  ]
  node [
    id 1665
    label "oberwanie_si&#281;"
  ]
  node [
    id 1666
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 1667
    label "burza"
  ]
  node [
    id 1668
    label "cloud"
  ]
  node [
    id 1669
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 1670
    label "spirala"
  ]
  node [
    id 1671
    label "miniatura"
  ]
  node [
    id 1672
    label "blaszka"
  ]
  node [
    id 1673
    label "kielich"
  ]
  node [
    id 1674
    label "p&#322;at"
  ]
  node [
    id 1675
    label "pasmo"
  ]
  node [
    id 1676
    label "comeliness"
  ]
  node [
    id 1677
    label "face"
  ]
  node [
    id 1678
    label "gwiazda"
  ]
  node [
    id 1679
    label "p&#281;tla"
  ]
  node [
    id 1680
    label "linearno&#347;&#263;"
  ]
  node [
    id 1681
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1682
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1683
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1684
    label "krajobraz"
  ]
  node [
    id 1685
    label "presence"
  ]
  node [
    id 1686
    label "zagrzmie&#263;"
  ]
  node [
    id 1687
    label "wojna"
  ]
  node [
    id 1688
    label "zagrzmienie"
  ]
  node [
    id 1689
    label "zaj&#347;cie"
  ]
  node [
    id 1690
    label "piorun"
  ]
  node [
    id 1691
    label "nieporz&#261;dek"
  ]
  node [
    id 1692
    label "konflikt"
  ]
  node [
    id 1693
    label "scene"
  ]
  node [
    id 1694
    label "nawa&#322;"
  ]
  node [
    id 1695
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 1696
    label "deszcz"
  ]
  node [
    id 1697
    label "fire"
  ]
  node [
    id 1698
    label "grzmie&#263;"
  ]
  node [
    id 1699
    label "burza_piaskowa"
  ]
  node [
    id 1700
    label "pogrzmot"
  ]
  node [
    id 1701
    label "grzmienie"
  ]
  node [
    id 1702
    label "rioting"
  ]
  node [
    id 1703
    label "tajemniczy"
  ]
  node [
    id 1704
    label "niesamowity"
  ]
  node [
    id 1705
    label "nieprzeniknienie"
  ]
  node [
    id 1706
    label "nastrojowy"
  ]
  node [
    id 1707
    label "niejednoznaczny"
  ]
  node [
    id 1708
    label "skryty"
  ]
  node [
    id 1709
    label "tajemniczo"
  ]
  node [
    id 1710
    label "nieznany"
  ]
  node [
    id 1711
    label "niezwyk&#322;y"
  ]
  node [
    id 1712
    label "niesamowicie"
  ]
  node [
    id 1713
    label "dupny"
  ]
  node [
    id 1714
    label "znaczny"
  ]
  node [
    id 1715
    label "wybitny"
  ]
  node [
    id 1716
    label "wysoce"
  ]
  node [
    id 1717
    label "nieprzeci&#281;tny"
  ]
  node [
    id 1718
    label "wyj&#261;tkowy"
  ]
  node [
    id 1719
    label "opaquely"
  ]
  node [
    id 1720
    label "intensywnie"
  ]
  node [
    id 1721
    label "stanowczy"
  ]
  node [
    id 1722
    label "zdecydowanie"
  ]
  node [
    id 1723
    label "zauwa&#380;alnie"
  ]
  node [
    id 1724
    label "judgment"
  ]
  node [
    id 1725
    label "podj&#281;cie"
  ]
  node [
    id 1726
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 1727
    label "decyzja"
  ]
  node [
    id 1728
    label "oddzia&#322;anie"
  ]
  node [
    id 1729
    label "resoluteness"
  ]
  node [
    id 1730
    label "pewnie"
  ]
  node [
    id 1731
    label "Kant"
  ]
  node [
    id 1732
    label "ideacja"
  ]
  node [
    id 1733
    label "cel"
  ]
  node [
    id 1734
    label "intelekt"
  ]
  node [
    id 1735
    label "ideologia"
  ]
  node [
    id 1736
    label "pomys&#322;"
  ]
  node [
    id 1737
    label "embryo"
  ]
  node [
    id 1738
    label "moczownik"
  ]
  node [
    id 1739
    label "latawiec"
  ]
  node [
    id 1740
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1741
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1742
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1743
    label "zarodek"
  ]
  node [
    id 1744
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1745
    label "superego"
  ]
  node [
    id 1746
    label "wn&#281;trze"
  ]
  node [
    id 1747
    label "psychika"
  ]
  node [
    id 1748
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 1749
    label "thing"
  ]
  node [
    id 1750
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1751
    label "orientacja"
  ]
  node [
    id 1752
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1753
    label "skumanie"
  ]
  node [
    id 1754
    label "teoria"
  ]
  node [
    id 1755
    label "zorientowanie"
  ]
  node [
    id 1756
    label "clasp"
  ]
  node [
    id 1757
    label "przem&#243;wienie"
  ]
  node [
    id 1758
    label "noosfera"
  ]
  node [
    id 1759
    label "ontologicznie"
  ]
  node [
    id 1760
    label "egzystencja"
  ]
  node [
    id 1761
    label "wy&#380;ywienie"
  ]
  node [
    id 1762
    label "subsystencja"
  ]
  node [
    id 1763
    label "ukradzenie"
  ]
  node [
    id 1764
    label "pocz&#261;tki"
  ]
  node [
    id 1765
    label "ukra&#347;&#263;"
  ]
  node [
    id 1766
    label "system"
  ]
  node [
    id 1767
    label "czysty_rozum"
  ]
  node [
    id 1768
    label "noumenon"
  ]
  node [
    id 1769
    label "kantysta"
  ]
  node [
    id 1770
    label "filozofia"
  ]
  node [
    id 1771
    label "fenomenologia"
  ]
  node [
    id 1772
    label "szko&#322;a"
  ]
  node [
    id 1773
    label "political_orientation"
  ]
  node [
    id 1774
    label "okre&#347;lony"
  ]
  node [
    id 1775
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1776
    label "wiadomy"
  ]
  node [
    id 1777
    label "wysoki"
  ]
  node [
    id 1778
    label "&#347;wietny"
  ]
  node [
    id 1779
    label "celny"
  ]
  node [
    id 1780
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 1781
    label "imponuj&#261;cy"
  ]
  node [
    id 1782
    label "wybitnie"
  ]
  node [
    id 1783
    label "wydatny"
  ]
  node [
    id 1784
    label "wspania&#322;y"
  ]
  node [
    id 1785
    label "nieprzeci&#281;tnie"
  ]
  node [
    id 1786
    label "wyj&#261;tkowo"
  ]
  node [
    id 1787
    label "znacznie"
  ]
  node [
    id 1788
    label "wa&#380;nie"
  ]
  node [
    id 1789
    label "eksponowany"
  ]
  node [
    id 1790
    label "wynios&#322;y"
  ]
  node [
    id 1791
    label "istotnie"
  ]
  node [
    id 1792
    label "dono&#347;ny"
  ]
  node [
    id 1793
    label "z&#322;y"
  ]
  node [
    id 1794
    label "do_dupy"
  ]
  node [
    id 1795
    label "zostawi&#263;"
  ]
  node [
    id 1796
    label "drop"
  ]
  node [
    id 1797
    label "unwrap"
  ]
  node [
    id 1798
    label "przesta&#263;"
  ]
  node [
    id 1799
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 1800
    label "zachowa&#263;"
  ]
  node [
    id 1801
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1802
    label "shelve"
  ]
  node [
    id 1803
    label "shove"
  ]
  node [
    id 1804
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1805
    label "release"
  ]
  node [
    id 1806
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1807
    label "zerwa&#263;"
  ]
  node [
    id 1808
    label "skrzywdzi&#263;"
  ]
  node [
    id 1809
    label "overhaul"
  ]
  node [
    id 1810
    label "doprowadzi&#263;"
  ]
  node [
    id 1811
    label "zrezygnowa&#263;"
  ]
  node [
    id 1812
    label "da&#263;"
  ]
  node [
    id 1813
    label "liszy&#263;"
  ]
  node [
    id 1814
    label "permit"
  ]
  node [
    id 1815
    label "zaplanowa&#263;"
  ]
  node [
    id 1816
    label "przekaza&#263;"
  ]
  node [
    id 1817
    label "stworzy&#263;"
  ]
  node [
    id 1818
    label "wyda&#263;"
  ]
  node [
    id 1819
    label "wyznaczy&#263;"
  ]
  node [
    id 1820
    label "coating"
  ]
  node [
    id 1821
    label "leave_office"
  ]
  node [
    id 1822
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1823
    label "fail"
  ]
  node [
    id 1824
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1825
    label "bustard"
  ]
  node [
    id 1826
    label "ptak"
  ]
  node [
    id 1827
    label "dropiowate"
  ]
  node [
    id 1828
    label "kania"
  ]
  node [
    id 1829
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 1830
    label "zmusza&#263;"
  ]
  node [
    id 1831
    label "order"
  ]
  node [
    id 1832
    label "nakazywa&#263;"
  ]
  node [
    id 1833
    label "wymaga&#263;"
  ]
  node [
    id 1834
    label "wyg&#322;osi&#263;"
  ]
  node [
    id 1835
    label "nakaza&#263;"
  ]
  node [
    id 1836
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1837
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 1838
    label "sandbag"
  ]
  node [
    id 1839
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 1840
    label "force"
  ]
  node [
    id 1841
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 1842
    label "take"
  ]
  node [
    id 1843
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 1844
    label "claim"
  ]
  node [
    id 1845
    label "wypowiada&#263;"
  ]
  node [
    id 1846
    label "powiedzie&#263;"
  ]
  node [
    id 1847
    label "inflict"
  ]
  node [
    id 1848
    label "poleca&#263;"
  ]
  node [
    id 1849
    label "pakowa&#263;"
  ]
  node [
    id 1850
    label "zapakowa&#263;"
  ]
  node [
    id 1851
    label "poleci&#263;"
  ]
  node [
    id 1852
    label "kawaler"
  ]
  node [
    id 1853
    label "odznaka"
  ]
  node [
    id 1854
    label "z&#322;akniony"
  ]
  node [
    id 1855
    label "ch&#281;tnie"
  ]
  node [
    id 1856
    label "gotowy"
  ]
  node [
    id 1857
    label "napalony"
  ]
  node [
    id 1858
    label "ch&#281;tliwy"
  ]
  node [
    id 1859
    label "chy&#380;y"
  ]
  node [
    id 1860
    label "&#380;yczliwy"
  ]
  node [
    id 1861
    label "zwierz&#281;"
  ]
  node [
    id 1862
    label "organizm"
  ]
  node [
    id 1863
    label "m&#322;odziak"
  ]
  node [
    id 1864
    label "potomstwo"
  ]
  node [
    id 1865
    label "fledgling"
  ]
  node [
    id 1866
    label "czeladka"
  ]
  node [
    id 1867
    label "dzietno&#347;&#263;"
  ]
  node [
    id 1868
    label "pomiot"
  ]
  node [
    id 1869
    label "bawienie_si&#281;"
  ]
  node [
    id 1870
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1871
    label "grupa"
  ]
  node [
    id 1872
    label "wyewoluowanie"
  ]
  node [
    id 1873
    label "odwadnianie"
  ]
  node [
    id 1874
    label "przyswojenie"
  ]
  node [
    id 1875
    label "starzenie_si&#281;"
  ]
  node [
    id 1876
    label "odwodni&#263;"
  ]
  node [
    id 1877
    label "ty&#322;"
  ]
  node [
    id 1878
    label "przyswajanie"
  ]
  node [
    id 1879
    label "biorytm"
  ]
  node [
    id 1880
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1881
    label "unerwienie"
  ]
  node [
    id 1882
    label "istota_&#380;ywa"
  ]
  node [
    id 1883
    label "sk&#243;ra"
  ]
  node [
    id 1884
    label "ewoluowanie"
  ]
  node [
    id 1885
    label "odwodnienie"
  ]
  node [
    id 1886
    label "ewoluowa&#263;"
  ]
  node [
    id 1887
    label "czynnik_biotyczny"
  ]
  node [
    id 1888
    label "otwieranie"
  ]
  node [
    id 1889
    label "cz&#322;onek"
  ]
  node [
    id 1890
    label "p&#322;aszczyzna"
  ]
  node [
    id 1891
    label "odwadnia&#263;"
  ]
  node [
    id 1892
    label "staw"
  ]
  node [
    id 1893
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1894
    label "wyewoluowa&#263;"
  ]
  node [
    id 1895
    label "szkielet"
  ]
  node [
    id 1896
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1897
    label "przyswoi&#263;"
  ]
  node [
    id 1898
    label "ow&#322;osienie"
  ]
  node [
    id 1899
    label "otworzenie"
  ]
  node [
    id 1900
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1901
    label "otwiera&#263;"
  ]
  node [
    id 1902
    label "individual"
  ]
  node [
    id 1903
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1904
    label "temperatura"
  ]
  node [
    id 1905
    label "monogamia"
  ]
  node [
    id 1906
    label "grzbiet"
  ]
  node [
    id 1907
    label "bestia"
  ]
  node [
    id 1908
    label "treser"
  ]
  node [
    id 1909
    label "niecz&#322;owiek"
  ]
  node [
    id 1910
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 1911
    label "agresja"
  ]
  node [
    id 1912
    label "skubni&#281;cie"
  ]
  node [
    id 1913
    label "skuba&#263;"
  ]
  node [
    id 1914
    label "tresowa&#263;"
  ]
  node [
    id 1915
    label "oz&#243;r"
  ]
  node [
    id 1916
    label "wylinka"
  ]
  node [
    id 1917
    label "poskramia&#263;"
  ]
  node [
    id 1918
    label "fukni&#281;cie"
  ]
  node [
    id 1919
    label "siedzenie"
  ]
  node [
    id 1920
    label "wios&#322;owa&#263;"
  ]
  node [
    id 1921
    label "zwyrol"
  ]
  node [
    id 1922
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 1923
    label "budowa_cia&#322;a"
  ]
  node [
    id 1924
    label "sodomita"
  ]
  node [
    id 1925
    label "wiwarium"
  ]
  node [
    id 1926
    label "oswaja&#263;"
  ]
  node [
    id 1927
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1928
    label "degenerat"
  ]
  node [
    id 1929
    label "le&#380;e&#263;"
  ]
  node [
    id 1930
    label "przyssawka"
  ]
  node [
    id 1931
    label "animalista"
  ]
  node [
    id 1932
    label "fauna"
  ]
  node [
    id 1933
    label "hodowla"
  ]
  node [
    id 1934
    label "popapraniec"
  ]
  node [
    id 1935
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 1936
    label "le&#380;enie"
  ]
  node [
    id 1937
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 1938
    label "poligamia"
  ]
  node [
    id 1939
    label "siedzie&#263;"
  ]
  node [
    id 1940
    label "napasienie_si&#281;"
  ]
  node [
    id 1941
    label "&#322;eb"
  ]
  node [
    id 1942
    label "paszcza"
  ]
  node [
    id 1943
    label "czerniak"
  ]
  node [
    id 1944
    label "zwierz&#281;ta"
  ]
  node [
    id 1945
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 1946
    label "zachowanie"
  ]
  node [
    id 1947
    label "skubn&#261;&#263;"
  ]
  node [
    id 1948
    label "wios&#322;owanie"
  ]
  node [
    id 1949
    label "skubanie"
  ]
  node [
    id 1950
    label "okrutnik"
  ]
  node [
    id 1951
    label "pasienie_si&#281;"
  ]
  node [
    id 1952
    label "farba"
  ]
  node [
    id 1953
    label "weterynarz"
  ]
  node [
    id 1954
    label "gad"
  ]
  node [
    id 1955
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 1956
    label "fukanie"
  ]
  node [
    id 1957
    label "go&#322;ow&#261;s"
  ]
  node [
    id 1958
    label "g&#243;wniarz"
  ]
  node [
    id 1959
    label "ch&#322;opta&#347;"
  ]
  node [
    id 1960
    label "niepe&#322;noletni"
  ]
  node [
    id 1961
    label "pomieszanie_si&#281;"
  ]
  node [
    id 1962
    label "pami&#281;&#263;"
  ]
  node [
    id 1963
    label "wyobra&#378;nia"
  ]
  node [
    id 1964
    label "komputer"
  ]
  node [
    id 1965
    label "hipokamp"
  ]
  node [
    id 1966
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 1967
    label "wymazanie"
  ]
  node [
    id 1968
    label "memory"
  ]
  node [
    id 1969
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 1970
    label "fondness"
  ]
  node [
    id 1971
    label "kraina"
  ]
  node [
    id 1972
    label "imagineskopia"
  ]
  node [
    id 1973
    label "psychologia"
  ]
  node [
    id 1974
    label "umeblowanie"
  ]
  node [
    id 1975
    label "esteta"
  ]
  node [
    id 1976
    label "ci&#281;&#380;ki"
  ]
  node [
    id 1977
    label "zdeterminowany"
  ]
  node [
    id 1978
    label "ambitnie"
  ]
  node [
    id 1979
    label "wysokich_lot&#243;w"
  ]
  node [
    id 1980
    label "samodzielny"
  ]
  node [
    id 1981
    label "&#347;mia&#322;y"
  ]
  node [
    id 1982
    label "wymagaj&#261;co"
  ]
  node [
    id 1983
    label "&#347;mia&#322;o"
  ]
  node [
    id 1984
    label "dziarski"
  ]
  node [
    id 1985
    label "odwa&#380;ny"
  ]
  node [
    id 1986
    label "osobny"
  ]
  node [
    id 1987
    label "samodzielnie"
  ]
  node [
    id 1988
    label "indywidualny"
  ]
  node [
    id 1989
    label "niepodleg&#322;y"
  ]
  node [
    id 1990
    label "czyj&#347;"
  ]
  node [
    id 1991
    label "autonomicznie"
  ]
  node [
    id 1992
    label "odr&#281;bny"
  ]
  node [
    id 1993
    label "sobieradzki"
  ]
  node [
    id 1994
    label "w&#322;asny"
  ]
  node [
    id 1995
    label "konsekwentny"
  ]
  node [
    id 1996
    label "skomplikowanie"
  ]
  node [
    id 1997
    label "kompletny"
  ]
  node [
    id 1998
    label "bojowy"
  ]
  node [
    id 1999
    label "niedelikatny"
  ]
  node [
    id 2000
    label "masywny"
  ]
  node [
    id 2001
    label "niezgrabny"
  ]
  node [
    id 2002
    label "zbrojny"
  ]
  node [
    id 2003
    label "gro&#378;ny"
  ]
  node [
    id 2004
    label "nieprzejrzysty"
  ]
  node [
    id 2005
    label "liczny"
  ]
  node [
    id 2006
    label "monumentalny"
  ]
  node [
    id 2007
    label "dotkliwy"
  ]
  node [
    id 2008
    label "przyswajalny"
  ]
  node [
    id 2009
    label "nieudany"
  ]
  node [
    id 2010
    label "grubo"
  ]
  node [
    id 2011
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 2012
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 2013
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 2014
    label "zwolennik"
  ]
  node [
    id 2015
    label "kontynuator"
  ]
  node [
    id 2016
    label "czeladnik"
  ]
  node [
    id 2017
    label "klasa"
  ]
  node [
    id 2018
    label "elew"
  ]
  node [
    id 2019
    label "praktykant"
  ]
  node [
    id 2020
    label "wyprawka"
  ]
  node [
    id 2021
    label "mundurek"
  ]
  node [
    id 2022
    label "rzemie&#347;lnik"
  ]
  node [
    id 2023
    label "absolwent"
  ]
  node [
    id 2024
    label "rezerwa"
  ]
  node [
    id 2025
    label "botanika"
  ]
  node [
    id 2026
    label "type"
  ]
  node [
    id 2027
    label "warstwa"
  ]
  node [
    id 2028
    label "gromada"
  ]
  node [
    id 2029
    label "mecz_mistrzowski"
  ]
  node [
    id 2030
    label "class"
  ]
  node [
    id 2031
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 2032
    label "Ekwici"
  ]
  node [
    id 2033
    label "sala"
  ]
  node [
    id 2034
    label "jednostka_systematyczna"
  ]
  node [
    id 2035
    label "wagon"
  ]
  node [
    id 2036
    label "przepisa&#263;"
  ]
  node [
    id 2037
    label "promocja"
  ]
  node [
    id 2038
    label "arrangement"
  ]
  node [
    id 2039
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 2040
    label "programowanie_obiektowe"
  ]
  node [
    id 2041
    label "obrona"
  ]
  node [
    id 2042
    label "dziennik_lekcyjny"
  ]
  node [
    id 2043
    label "typ"
  ]
  node [
    id 2044
    label "znak_jako&#347;ci"
  ]
  node [
    id 2045
    label "&#322;awka"
  ]
  node [
    id 2046
    label "organizacja"
  ]
  node [
    id 2047
    label "tablica"
  ]
  node [
    id 2048
    label "przepisanie"
  ]
  node [
    id 2049
    label "fakcja"
  ]
  node [
    id 2050
    label "pomoc"
  ]
  node [
    id 2051
    label "form"
  ]
  node [
    id 2052
    label "kurs"
  ]
  node [
    id 2053
    label "zdanie"
  ]
  node [
    id 2054
    label "lekcja"
  ]
  node [
    id 2055
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 2056
    label "skolaryzacja"
  ]
  node [
    id 2057
    label "praktyka"
  ]
  node [
    id 2058
    label "lesson"
  ]
  node [
    id 2059
    label "niepokalanki"
  ]
  node [
    id 2060
    label "kwalifikacje"
  ]
  node [
    id 2061
    label "Mickiewicz"
  ]
  node [
    id 2062
    label "muzyka"
  ]
  node [
    id 2063
    label "school"
  ]
  node [
    id 2064
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 2065
    label "&#322;awa_szkolna"
  ]
  node [
    id 2066
    label "nauka"
  ]
  node [
    id 2067
    label "siedziba"
  ]
  node [
    id 2068
    label "gabinet"
  ]
  node [
    id 2069
    label "sekretariat"
  ]
  node [
    id 2070
    label "metoda"
  ]
  node [
    id 2071
    label "szkolenie"
  ]
  node [
    id 2072
    label "sztuba"
  ]
  node [
    id 2073
    label "do&#347;wiadczenie"
  ]
  node [
    id 2074
    label "podr&#281;cznik"
  ]
  node [
    id 2075
    label "zda&#263;"
  ]
  node [
    id 2076
    label "kara"
  ]
  node [
    id 2077
    label "teren_szko&#322;y"
  ]
  node [
    id 2078
    label "urszulanki"
  ]
  node [
    id 2079
    label "remiecha"
  ]
  node [
    id 2080
    label "nowicjusz"
  ]
  node [
    id 2081
    label "na&#347;ladowca"
  ]
  node [
    id 2082
    label "nast&#281;pca"
  ]
  node [
    id 2083
    label "&#380;o&#322;nierz"
  ]
  node [
    id 2084
    label "student"
  ]
  node [
    id 2085
    label "harcerz"
  ]
  node [
    id 2086
    label "uniform"
  ]
  node [
    id 2087
    label "obszar"
  ]
  node [
    id 2088
    label "bro&#324;"
  ]
  node [
    id 2089
    label "telefon"
  ]
  node [
    id 2090
    label "bro&#324;_ochronna"
  ]
  node [
    id 2091
    label "obro&#324;ca"
  ]
  node [
    id 2092
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 2093
    label "wskaz&#243;wka"
  ]
  node [
    id 2094
    label "powierzchnia"
  ]
  node [
    id 2095
    label "naszywka"
  ]
  node [
    id 2096
    label "or&#281;&#380;"
  ]
  node [
    id 2097
    label "target"
  ]
  node [
    id 2098
    label "denture"
  ]
  node [
    id 2099
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 2100
    label "niemowl&#281;"
  ]
  node [
    id 2101
    label "zapomoga"
  ]
  node [
    id 2102
    label "layette"
  ]
  node [
    id 2103
    label "wiano"
  ]
  node [
    id 2104
    label "wyprawa"
  ]
  node [
    id 2105
    label "rzemie&#347;lniczek"
  ]
  node [
    id 2106
    label "m&#322;&#243;dka"
  ]
  node [
    id 2107
    label "dziecko"
  ]
  node [
    id 2108
    label "kruszyna"
  ]
  node [
    id 2109
    label "jajo"
  ]
  node [
    id 2110
    label "dzieciarnia"
  ]
  node [
    id 2111
    label "m&#322;odzik"
  ]
  node [
    id 2112
    label "utuli&#263;"
  ]
  node [
    id 2113
    label "pedofil"
  ]
  node [
    id 2114
    label "dzieciak"
  ]
  node [
    id 2115
    label "potomek"
  ]
  node [
    id 2116
    label "sraluch"
  ]
  node [
    id 2117
    label "utulenie"
  ]
  node [
    id 2118
    label "utulanie"
  ]
  node [
    id 2119
    label "utula&#263;"
  ]
  node [
    id 2120
    label "entliczek-pentliczek"
  ]
  node [
    id 2121
    label "cz&#322;owieczek"
  ]
  node [
    id 2122
    label "pediatra"
  ]
  node [
    id 2123
    label "krzew"
  ]
  node [
    id 2124
    label "drobina"
  ]
  node [
    id 2125
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 2126
    label "ro&#347;lina"
  ]
  node [
    id 2127
    label "ro&#347;lina_miododajna"
  ]
  node [
    id 2128
    label "szak&#322;akowate"
  ]
  node [
    id 2129
    label "odpad"
  ]
  node [
    id 2130
    label "ball"
  ]
  node [
    id 2131
    label "l&#281;gnia"
  ]
  node [
    id 2132
    label "&#380;&#243;&#322;tko"
  ]
  node [
    id 2133
    label "znoszenie"
  ]
  node [
    id 2134
    label "rozbijarka"
  ]
  node [
    id 2135
    label "j&#261;dro"
  ]
  node [
    id 2136
    label "wyt&#322;aczanka"
  ]
  node [
    id 2137
    label "nabia&#322;"
  ]
  node [
    id 2138
    label "kariogamia"
  ]
  node [
    id 2139
    label "produkt"
  ]
  node [
    id 2140
    label "zniesienie"
  ]
  node [
    id 2141
    label "ryboflawina"
  ]
  node [
    id 2142
    label "pisanka"
  ]
  node [
    id 2143
    label "kr&#243;lowa_matka"
  ]
  node [
    id 2144
    label "ovum"
  ]
  node [
    id 2145
    label "bia&#322;ko"
  ]
  node [
    id 2146
    label "gameta"
  ]
  node [
    id 2147
    label "owoskop"
  ]
  node [
    id 2148
    label "skorupka"
  ]
  node [
    id 2149
    label "faza"
  ]
  node [
    id 2150
    label "m&#322;odzie&#380;"
  ]
  node [
    id 2151
    label "dziewczyna"
  ]
  node [
    id 2152
    label "kobieta"
  ]
  node [
    id 2153
    label "upierzenie"
  ]
  node [
    id 2154
    label "mo&#322;odyca"
  ]
  node [
    id 2155
    label "samica"
  ]
  node [
    id 2156
    label "fare"
  ]
  node [
    id 2157
    label "podanie"
  ]
  node [
    id 2158
    label "poda&#263;"
  ]
  node [
    id 2159
    label "szama"
  ]
  node [
    id 2160
    label "mleko"
  ]
  node [
    id 2161
    label "koryto"
  ]
  node [
    id 2162
    label "jad&#322;o"
  ]
  node [
    id 2163
    label "wiwenda"
  ]
  node [
    id 2164
    label "ssa&#263;"
  ]
  node [
    id 2165
    label "laktacja"
  ]
  node [
    id 2166
    label "porcja"
  ]
  node [
    id 2167
    label "laktoferyna"
  ]
  node [
    id 2168
    label "milk"
  ]
  node [
    id 2169
    label "szkopek"
  ]
  node [
    id 2170
    label "jedzenie"
  ]
  node [
    id 2171
    label "zawiesina"
  ]
  node [
    id 2172
    label "warzenie_si&#281;"
  ]
  node [
    id 2173
    label "wydzielina"
  ]
  node [
    id 2174
    label "kazeina"
  ]
  node [
    id 2175
    label "mg&#322;a"
  ]
  node [
    id 2176
    label "laktoza"
  ]
  node [
    id 2177
    label "object"
  ]
  node [
    id 2178
    label "przyroda"
  ]
  node [
    id 2179
    label "kultura"
  ]
  node [
    id 2180
    label "temat"
  ]
  node [
    id 2181
    label "zawarto&#347;&#263;"
  ]
  node [
    id 2182
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 2183
    label "prze&#322;om"
  ]
  node [
    id 2184
    label "pa&#347;nik"
  ]
  node [
    id 2185
    label "sinecure"
  ]
  node [
    id 2186
    label "naczynie"
  ]
  node [
    id 2187
    label "stanowisko"
  ]
  node [
    id 2188
    label "trough"
  ]
  node [
    id 2189
    label "pojemnik"
  ]
  node [
    id 2190
    label "starorzecze"
  ]
  node [
    id 2191
    label "siatk&#243;wka"
  ]
  node [
    id 2192
    label "opowie&#347;&#263;"
  ]
  node [
    id 2193
    label "tenis"
  ]
  node [
    id 2194
    label "poinformowanie"
  ]
  node [
    id 2195
    label "narrative"
  ]
  node [
    id 2196
    label "pass"
  ]
  node [
    id 2197
    label "pi&#322;ka"
  ]
  node [
    id 2198
    label "prayer"
  ]
  node [
    id 2199
    label "ustawienie"
  ]
  node [
    id 2200
    label "nafaszerowanie"
  ]
  node [
    id 2201
    label "myth"
  ]
  node [
    id 2202
    label "danie"
  ]
  node [
    id 2203
    label "zaserwowanie"
  ]
  node [
    id 2204
    label "granie"
  ]
  node [
    id 2205
    label "faszerowanie"
  ]
  node [
    id 2206
    label "dawanie"
  ]
  node [
    id 2207
    label "administration"
  ]
  node [
    id 2208
    label "rozgrywanie"
  ]
  node [
    id 2209
    label "bufet"
  ]
  node [
    id 2210
    label "serwowanie"
  ]
  node [
    id 2211
    label "stawianie"
  ]
  node [
    id 2212
    label "nafaszerowa&#263;"
  ]
  node [
    id 2213
    label "ustawi&#263;"
  ]
  node [
    id 2214
    label "poinformowa&#263;"
  ]
  node [
    id 2215
    label "zagra&#263;"
  ]
  node [
    id 2216
    label "zaserwowa&#263;"
  ]
  node [
    id 2217
    label "introduce"
  ]
  node [
    id 2218
    label "kelner"
  ]
  node [
    id 2219
    label "faszerowa&#263;"
  ]
  node [
    id 2220
    label "serwowa&#263;"
  ]
  node [
    id 2221
    label "stawia&#263;"
  ]
  node [
    id 2222
    label "rozgrywa&#263;"
  ]
  node [
    id 2223
    label "deal"
  ]
  node [
    id 2224
    label "uwicie"
  ]
  node [
    id 2225
    label "pa&#324;stwo"
  ]
  node [
    id 2226
    label "l&#281;gowisko"
  ]
  node [
    id 2227
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 2228
    label "uwi&#263;"
  ]
  node [
    id 2229
    label "o&#347;rodek"
  ]
  node [
    id 2230
    label "wi&#263;"
  ]
  node [
    id 2231
    label "schronienie"
  ]
  node [
    id 2232
    label "patriota"
  ]
  node [
    id 2233
    label "otw&#243;r"
  ]
  node [
    id 2234
    label "skupienie"
  ]
  node [
    id 2235
    label "wicie"
  ]
  node [
    id 2236
    label "&#322;ono"
  ]
  node [
    id 2237
    label "matuszka"
  ]
  node [
    id 2238
    label "rz&#261;d"
  ]
  node [
    id 2239
    label "plac"
  ]
  node [
    id 2240
    label "location"
  ]
  node [
    id 2241
    label "warunek_lokalowy"
  ]
  node [
    id 2242
    label "status"
  ]
  node [
    id 2243
    label "zespolenie"
  ]
  node [
    id 2244
    label "kompozycja"
  ]
  node [
    id 2245
    label "spowodowanie"
  ]
  node [
    id 2246
    label "zgrzeina"
  ]
  node [
    id 2247
    label "composing"
  ]
  node [
    id 2248
    label "joining"
  ]
  node [
    id 2249
    label "junction"
  ]
  node [
    id 2250
    label "akt_p&#322;ciowy"
  ]
  node [
    id 2251
    label "zjednoczenie"
  ]
  node [
    id 2252
    label "Japonia"
  ]
  node [
    id 2253
    label "Zair"
  ]
  node [
    id 2254
    label "Belize"
  ]
  node [
    id 2255
    label "San_Marino"
  ]
  node [
    id 2256
    label "Tanzania"
  ]
  node [
    id 2257
    label "Antigua_i_Barbuda"
  ]
  node [
    id 2258
    label "granica_pa&#324;stwa"
  ]
  node [
    id 2259
    label "Senegal"
  ]
  node [
    id 2260
    label "Seszele"
  ]
  node [
    id 2261
    label "Mauretania"
  ]
  node [
    id 2262
    label "Indie"
  ]
  node [
    id 2263
    label "Filipiny"
  ]
  node [
    id 2264
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 2265
    label "Zimbabwe"
  ]
  node [
    id 2266
    label "Malezja"
  ]
  node [
    id 2267
    label "Rumunia"
  ]
  node [
    id 2268
    label "Surinam"
  ]
  node [
    id 2269
    label "Ukraina"
  ]
  node [
    id 2270
    label "Syria"
  ]
  node [
    id 2271
    label "Wyspy_Marshalla"
  ]
  node [
    id 2272
    label "Burkina_Faso"
  ]
  node [
    id 2273
    label "Grecja"
  ]
  node [
    id 2274
    label "Polska"
  ]
  node [
    id 2275
    label "Wenezuela"
  ]
  node [
    id 2276
    label "Suazi"
  ]
  node [
    id 2277
    label "Nepal"
  ]
  node [
    id 2278
    label "S&#322;owacja"
  ]
  node [
    id 2279
    label "Algieria"
  ]
  node [
    id 2280
    label "Chiny"
  ]
  node [
    id 2281
    label "Grenada"
  ]
  node [
    id 2282
    label "Barbados"
  ]
  node [
    id 2283
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 2284
    label "Pakistan"
  ]
  node [
    id 2285
    label "Niemcy"
  ]
  node [
    id 2286
    label "Bahrajn"
  ]
  node [
    id 2287
    label "Komory"
  ]
  node [
    id 2288
    label "Australia"
  ]
  node [
    id 2289
    label "Rodezja"
  ]
  node [
    id 2290
    label "Malawi"
  ]
  node [
    id 2291
    label "Gwinea"
  ]
  node [
    id 2292
    label "Wehrlen"
  ]
  node [
    id 2293
    label "Meksyk"
  ]
  node [
    id 2294
    label "Liechtenstein"
  ]
  node [
    id 2295
    label "Czarnog&#243;ra"
  ]
  node [
    id 2296
    label "Wielka_Brytania"
  ]
  node [
    id 2297
    label "Kuwejt"
  ]
  node [
    id 2298
    label "Monako"
  ]
  node [
    id 2299
    label "Angola"
  ]
  node [
    id 2300
    label "Jemen"
  ]
  node [
    id 2301
    label "Etiopia"
  ]
  node [
    id 2302
    label "Madagaskar"
  ]
  node [
    id 2303
    label "terytorium"
  ]
  node [
    id 2304
    label "Kolumbia"
  ]
  node [
    id 2305
    label "Portoryko"
  ]
  node [
    id 2306
    label "Mauritius"
  ]
  node [
    id 2307
    label "Kostaryka"
  ]
  node [
    id 2308
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 2309
    label "Tajlandia"
  ]
  node [
    id 2310
    label "Argentyna"
  ]
  node [
    id 2311
    label "Zambia"
  ]
  node [
    id 2312
    label "Sri_Lanka"
  ]
  node [
    id 2313
    label "Gwatemala"
  ]
  node [
    id 2314
    label "Kirgistan"
  ]
  node [
    id 2315
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 2316
    label "Hiszpania"
  ]
  node [
    id 2317
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 2318
    label "Salwador"
  ]
  node [
    id 2319
    label "Korea"
  ]
  node [
    id 2320
    label "Macedonia"
  ]
  node [
    id 2321
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 2322
    label "Brunei"
  ]
  node [
    id 2323
    label "Mozambik"
  ]
  node [
    id 2324
    label "Turcja"
  ]
  node [
    id 2325
    label "Kambod&#380;a"
  ]
  node [
    id 2326
    label "Benin"
  ]
  node [
    id 2327
    label "Bhutan"
  ]
  node [
    id 2328
    label "Tunezja"
  ]
  node [
    id 2329
    label "Austria"
  ]
  node [
    id 2330
    label "Izrael"
  ]
  node [
    id 2331
    label "Sierra_Leone"
  ]
  node [
    id 2332
    label "Jamajka"
  ]
  node [
    id 2333
    label "Rosja"
  ]
  node [
    id 2334
    label "Rwanda"
  ]
  node [
    id 2335
    label "holoarktyka"
  ]
  node [
    id 2336
    label "Nigeria"
  ]
  node [
    id 2337
    label "USA"
  ]
  node [
    id 2338
    label "Oman"
  ]
  node [
    id 2339
    label "Luksemburg"
  ]
  node [
    id 2340
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 2341
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 2342
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 2343
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 2344
    label "Dominikana"
  ]
  node [
    id 2345
    label "Irlandia"
  ]
  node [
    id 2346
    label "Liban"
  ]
  node [
    id 2347
    label "Hanower"
  ]
  node [
    id 2348
    label "Estonia"
  ]
  node [
    id 2349
    label "Iran"
  ]
  node [
    id 2350
    label "Nowa_Zelandia"
  ]
  node [
    id 2351
    label "Gabon"
  ]
  node [
    id 2352
    label "Samoa"
  ]
  node [
    id 2353
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 2354
    label "S&#322;owenia"
  ]
  node [
    id 2355
    label "Kiribati"
  ]
  node [
    id 2356
    label "Egipt"
  ]
  node [
    id 2357
    label "Togo"
  ]
  node [
    id 2358
    label "Mongolia"
  ]
  node [
    id 2359
    label "Sudan"
  ]
  node [
    id 2360
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 2361
    label "Bahamy"
  ]
  node [
    id 2362
    label "partia"
  ]
  node [
    id 2363
    label "Serbia"
  ]
  node [
    id 2364
    label "Czechy"
  ]
  node [
    id 2365
    label "Holandia"
  ]
  node [
    id 2366
    label "Birma"
  ]
  node [
    id 2367
    label "Albania"
  ]
  node [
    id 2368
    label "Mikronezja"
  ]
  node [
    id 2369
    label "Gambia"
  ]
  node [
    id 2370
    label "Kazachstan"
  ]
  node [
    id 2371
    label "interior"
  ]
  node [
    id 2372
    label "Uzbekistan"
  ]
  node [
    id 2373
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 2374
    label "Malta"
  ]
  node [
    id 2375
    label "Lesoto"
  ]
  node [
    id 2376
    label "para"
  ]
  node [
    id 2377
    label "Antarktis"
  ]
  node [
    id 2378
    label "Andora"
  ]
  node [
    id 2379
    label "Nauru"
  ]
  node [
    id 2380
    label "Kuba"
  ]
  node [
    id 2381
    label "Wietnam"
  ]
  node [
    id 2382
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 2383
    label "ziemia"
  ]
  node [
    id 2384
    label "Kamerun"
  ]
  node [
    id 2385
    label "Chorwacja"
  ]
  node [
    id 2386
    label "Urugwaj"
  ]
  node [
    id 2387
    label "Niger"
  ]
  node [
    id 2388
    label "Turkmenistan"
  ]
  node [
    id 2389
    label "Szwajcaria"
  ]
  node [
    id 2390
    label "zwrot"
  ]
  node [
    id 2391
    label "Palau"
  ]
  node [
    id 2392
    label "Litwa"
  ]
  node [
    id 2393
    label "Gruzja"
  ]
  node [
    id 2394
    label "Tajwan"
  ]
  node [
    id 2395
    label "Kongo"
  ]
  node [
    id 2396
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 2397
    label "Honduras"
  ]
  node [
    id 2398
    label "Boliwia"
  ]
  node [
    id 2399
    label "Uganda"
  ]
  node [
    id 2400
    label "Namibia"
  ]
  node [
    id 2401
    label "Azerbejd&#380;an"
  ]
  node [
    id 2402
    label "Erytrea"
  ]
  node [
    id 2403
    label "Gujana"
  ]
  node [
    id 2404
    label "Panama"
  ]
  node [
    id 2405
    label "Somalia"
  ]
  node [
    id 2406
    label "Burundi"
  ]
  node [
    id 2407
    label "Tuwalu"
  ]
  node [
    id 2408
    label "Libia"
  ]
  node [
    id 2409
    label "Katar"
  ]
  node [
    id 2410
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 2411
    label "Sahara_Zachodnia"
  ]
  node [
    id 2412
    label "Trynidad_i_Tobago"
  ]
  node [
    id 2413
    label "Gwinea_Bissau"
  ]
  node [
    id 2414
    label "Bu&#322;garia"
  ]
  node [
    id 2415
    label "Fid&#380;i"
  ]
  node [
    id 2416
    label "Nikaragua"
  ]
  node [
    id 2417
    label "Tonga"
  ]
  node [
    id 2418
    label "Timor_Wschodni"
  ]
  node [
    id 2419
    label "Laos"
  ]
  node [
    id 2420
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 2421
    label "Ghana"
  ]
  node [
    id 2422
    label "Brazylia"
  ]
  node [
    id 2423
    label "Belgia"
  ]
  node [
    id 2424
    label "Irak"
  ]
  node [
    id 2425
    label "Peru"
  ]
  node [
    id 2426
    label "Arabia_Saudyjska"
  ]
  node [
    id 2427
    label "Indonezja"
  ]
  node [
    id 2428
    label "Malediwy"
  ]
  node [
    id 2429
    label "Afganistan"
  ]
  node [
    id 2430
    label "Jordania"
  ]
  node [
    id 2431
    label "Kenia"
  ]
  node [
    id 2432
    label "Czad"
  ]
  node [
    id 2433
    label "Liberia"
  ]
  node [
    id 2434
    label "W&#281;gry"
  ]
  node [
    id 2435
    label "Chile"
  ]
  node [
    id 2436
    label "Mali"
  ]
  node [
    id 2437
    label "Armenia"
  ]
  node [
    id 2438
    label "Kanada"
  ]
  node [
    id 2439
    label "Cypr"
  ]
  node [
    id 2440
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 2441
    label "Ekwador"
  ]
  node [
    id 2442
    label "Mo&#322;dawia"
  ]
  node [
    id 2443
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 2444
    label "W&#322;ochy"
  ]
  node [
    id 2445
    label "Wyspy_Salomona"
  ]
  node [
    id 2446
    label "&#321;otwa"
  ]
  node [
    id 2447
    label "D&#380;ibuti"
  ]
  node [
    id 2448
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 2449
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 2450
    label "Portugalia"
  ]
  node [
    id 2451
    label "Botswana"
  ]
  node [
    id 2452
    label "Maroko"
  ]
  node [
    id 2453
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 2454
    label "Francja"
  ]
  node [
    id 2455
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 2456
    label "Dominika"
  ]
  node [
    id 2457
    label "Paragwaj"
  ]
  node [
    id 2458
    label "Tad&#380;ykistan"
  ]
  node [
    id 2459
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 2460
    label "Haiti"
  ]
  node [
    id 2461
    label "Khitai"
  ]
  node [
    id 2462
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 2463
    label "congestion"
  ]
  node [
    id 2464
    label "agglomeration"
  ]
  node [
    id 2465
    label "przegrupowanie"
  ]
  node [
    id 2466
    label "concentration"
  ]
  node [
    id 2467
    label "po&#322;&#261;czenie"
  ]
  node [
    id 2468
    label "zgromadzenie"
  ]
  node [
    id 2469
    label "kupienie"
  ]
  node [
    id 2470
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 2471
    label "przerwa"
  ]
  node [
    id 2472
    label "wiercenie"
  ]
  node [
    id 2473
    label "wybicie"
  ]
  node [
    id 2474
    label "wybijanie"
  ]
  node [
    id 2475
    label "wyd&#322;ubanie"
  ]
  node [
    id 2476
    label "powybijanie"
  ]
  node [
    id 2477
    label "pakiet_klimatyczny"
  ]
  node [
    id 2478
    label "uprawianie"
  ]
  node [
    id 2479
    label "collection"
  ]
  node [
    id 2480
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 2481
    label "gathering"
  ]
  node [
    id 2482
    label "album"
  ]
  node [
    id 2483
    label "praca_rolnicza"
  ]
  node [
    id 2484
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 2485
    label "sum"
  ]
  node [
    id 2486
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 2487
    label "series"
  ]
  node [
    id 2488
    label "ukrycie"
  ]
  node [
    id 2489
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 2490
    label "bezpieczny"
  ]
  node [
    id 2491
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 2492
    label "podbrzusze"
  ]
  node [
    id 2493
    label "pochwa"
  ]
  node [
    id 2494
    label "klatka_piersiowa"
  ]
  node [
    id 2495
    label "dziedzina"
  ]
  node [
    id 2496
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 2497
    label "brzuch"
  ]
  node [
    id 2498
    label "penis"
  ]
  node [
    id 2499
    label "macica"
  ]
  node [
    id 2500
    label "zal&#261;&#380;ek"
  ]
  node [
    id 2501
    label "skupisko"
  ]
  node [
    id 2502
    label "Hollywood"
  ]
  node [
    id 2503
    label "center"
  ]
  node [
    id 2504
    label "otoczenie"
  ]
  node [
    id 2505
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 2506
    label "siedlisko"
  ]
  node [
    id 2507
    label "przyczyna"
  ]
  node [
    id 2508
    label "ojczyzna"
  ]
  node [
    id 2509
    label "popadia"
  ]
  node [
    id 2510
    label "sple&#347;&#263;"
  ]
  node [
    id 2511
    label "zbudowa&#263;"
  ]
  node [
    id 2512
    label "wrench"
  ]
  node [
    id 2513
    label "twine"
  ]
  node [
    id 2514
    label "kinetosom"
  ]
  node [
    id 2515
    label "mastygonema"
  ]
  node [
    id 2516
    label "budowa&#263;"
  ]
  node [
    id 2517
    label "ga&#322;&#261;zka"
  ]
  node [
    id 2518
    label "splata&#263;"
  ]
  node [
    id 2519
    label "undulipodium"
  ]
  node [
    id 2520
    label "splatanie"
  ]
  node [
    id 2521
    label "budowanie"
  ]
  node [
    id 2522
    label "splecenie"
  ]
  node [
    id 2523
    label "zbudowanie"
  ]
  node [
    id 2524
    label "ci&#261;g&#322;y"
  ]
  node [
    id 2525
    label "stale"
  ]
  node [
    id 2526
    label "ci&#261;gle"
  ]
  node [
    id 2527
    label "nieprzerwany"
  ]
  node [
    id 2528
    label "sta&#322;y"
  ]
  node [
    id 2529
    label "nieustanny"
  ]
  node [
    id 2530
    label "zwykle"
  ]
  node [
    id 2531
    label "jednakowo"
  ]
  node [
    id 2532
    label "zawsze"
  ]
  node [
    id 2533
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 2534
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 2535
    label "Nibiru"
  ]
  node [
    id 2536
    label "supergrupa"
  ]
  node [
    id 2537
    label "konstelacja"
  ]
  node [
    id 2538
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 2539
    label "agregatka"
  ]
  node [
    id 2540
    label "Gwiazda_Polarna"
  ]
  node [
    id 2541
    label "Arktur"
  ]
  node [
    id 2542
    label "delta_Scuti"
  ]
  node [
    id 2543
    label "s&#322;awa"
  ]
  node [
    id 2544
    label "S&#322;o&#324;ce"
  ]
  node [
    id 2545
    label "gwiazdosz"
  ]
  node [
    id 2546
    label "asocjacja_gwiazd"
  ]
  node [
    id 2547
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 2548
    label "naj"
  ]
  node [
    id 2549
    label "doskonale"
  ]
  node [
    id 2550
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 2551
    label "nieograniczony"
  ]
  node [
    id 2552
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 2553
    label "ca&#322;y"
  ]
  node [
    id 2554
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 2555
    label "zupe&#322;ny"
  ]
  node [
    id 2556
    label "bezwzgl&#281;dny"
  ]
  node [
    id 2557
    label "satysfakcja"
  ]
  node [
    id 2558
    label "pe&#322;no"
  ]
  node [
    id 2559
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 2560
    label "r&#243;wny"
  ]
  node [
    id 2561
    label "wype&#322;nienie"
  ]
  node [
    id 2562
    label "otwarty"
  ]
  node [
    id 2563
    label "och&#281;do&#380;ny"
  ]
  node [
    id 2564
    label "&#347;wietnie"
  ]
  node [
    id 2565
    label "spania&#322;y"
  ]
  node [
    id 2566
    label "wspaniale"
  ]
  node [
    id 2567
    label "bogato"
  ]
  node [
    id 2568
    label "taki"
  ]
  node [
    id 2569
    label "stosownie"
  ]
  node [
    id 2570
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 2571
    label "zasadniczy"
  ]
  node [
    id 2572
    label "uprawniony"
  ]
  node [
    id 2573
    label "nale&#380;yty"
  ]
  node [
    id 2574
    label "nale&#380;ny"
  ]
  node [
    id 2575
    label "arcydzielny"
  ]
  node [
    id 2576
    label "superancki"
  ]
  node [
    id 2577
    label "kompletnie"
  ]
  node [
    id 2578
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 2579
    label "dorobek"
  ]
  node [
    id 2580
    label "tre&#347;&#263;"
  ]
  node [
    id 2581
    label "works"
  ]
  node [
    id 2582
    label "obrazowanie"
  ]
  node [
    id 2583
    label "retrospektywa"
  ]
  node [
    id 2584
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 2585
    label "creation"
  ]
  node [
    id 2586
    label "tetralogia"
  ]
  node [
    id 2587
    label "konto"
  ]
  node [
    id 2588
    label "wypracowa&#263;"
  ]
  node [
    id 2589
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 2590
    label "pisa&#263;"
  ]
  node [
    id 2591
    label "redakcja"
  ]
  node [
    id 2592
    label "j&#281;zykowo"
  ]
  node [
    id 2593
    label "preparacja"
  ]
  node [
    id 2594
    label "obelga"
  ]
  node [
    id 2595
    label "odmianka"
  ]
  node [
    id 2596
    label "opu&#347;ci&#263;"
  ]
  node [
    id 2597
    label "pomini&#281;cie"
  ]
  node [
    id 2598
    label "koniektura"
  ]
  node [
    id 2599
    label "zaw&#243;d"
  ]
  node [
    id 2600
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 2601
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 2602
    label "czynnik_produkcji"
  ]
  node [
    id 2603
    label "stosunek_pracy"
  ]
  node [
    id 2604
    label "kierownictwo"
  ]
  node [
    id 2605
    label "najem"
  ]
  node [
    id 2606
    label "zak&#322;ad"
  ]
  node [
    id 2607
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 2608
    label "tynkarski"
  ]
  node [
    id 2609
    label "tyrka"
  ]
  node [
    id 2610
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 2611
    label "benedykty&#324;ski"
  ]
  node [
    id 2612
    label "poda&#380;_pracy"
  ]
  node [
    id 2613
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 2614
    label "tworzenie"
  ]
  node [
    id 2615
    label "kreacja"
  ]
  node [
    id 2616
    label "imaging"
  ]
  node [
    id 2617
    label "utw&#243;r"
  ]
  node [
    id 2618
    label "informacja"
  ]
  node [
    id 2619
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 2620
    label "ornamentyka"
  ]
  node [
    id 2621
    label "formality"
  ]
  node [
    id 2622
    label "rdze&#324;"
  ]
  node [
    id 2623
    label "mode"
  ]
  node [
    id 2624
    label "odmiana"
  ]
  node [
    id 2625
    label "poznanie"
  ]
  node [
    id 2626
    label "maszyna_drukarska"
  ]
  node [
    id 2627
    label "kantyzm"
  ]
  node [
    id 2628
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 2629
    label "style"
  ]
  node [
    id 2630
    label "arystotelizm"
  ]
  node [
    id 2631
    label "zwyczaj"
  ]
  node [
    id 2632
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 2633
    label "October"
  ]
  node [
    id 2634
    label "do&#322;ek"
  ]
  node [
    id 2635
    label "wspomnienie"
  ]
  node [
    id 2636
    label "przegl&#261;d"
  ]
  node [
    id 2637
    label "cykl"
  ]
  node [
    id 2638
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 2639
    label "przeci&#261;&#263;"
  ]
  node [
    id 2640
    label "establish"
  ]
  node [
    id 2641
    label "uruchomi&#263;"
  ]
  node [
    id 2642
    label "begin"
  ]
  node [
    id 2643
    label "udost&#281;pni&#263;"
  ]
  node [
    id 2644
    label "act"
  ]
  node [
    id 2645
    label "naruszy&#263;"
  ]
  node [
    id 2646
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 2647
    label "traverse"
  ]
  node [
    id 2648
    label "przebi&#263;"
  ]
  node [
    id 2649
    label "przerwa&#263;"
  ]
  node [
    id 2650
    label "zablokowa&#263;"
  ]
  node [
    id 2651
    label "przej&#347;&#263;"
  ]
  node [
    id 2652
    label "uci&#261;&#263;"
  ]
  node [
    id 2653
    label "traversal"
  ]
  node [
    id 2654
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 2655
    label "odj&#261;&#263;"
  ]
  node [
    id 2656
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 2657
    label "post&#261;pi&#263;"
  ]
  node [
    id 2658
    label "cause"
  ]
  node [
    id 2659
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 2660
    label "trip"
  ]
  node [
    id 2661
    label "zmieni&#263;"
  ]
  node [
    id 2662
    label "stagger"
  ]
  node [
    id 2663
    label "wear"
  ]
  node [
    id 2664
    label "podzieli&#263;"
  ]
  node [
    id 2665
    label "wygra&#263;"
  ]
  node [
    id 2666
    label "zepsu&#263;"
  ]
  node [
    id 2667
    label "os&#322;abi&#263;"
  ]
  node [
    id 2668
    label "peddle"
  ]
  node [
    id 2669
    label "range"
  ]
  node [
    id 2670
    label "oddali&#263;"
  ]
  node [
    id 2671
    label "raise"
  ]
  node [
    id 2672
    label "rozmie&#347;ci&#263;"
  ]
  node [
    id 2673
    label "note"
  ]
  node [
    id 2674
    label "open"
  ]
  node [
    id 2675
    label "tanatoplastyk"
  ]
  node [
    id 2676
    label "Komitet_Region&#243;w"
  ]
  node [
    id 2677
    label "tanatoplastyka"
  ]
  node [
    id 2678
    label "pochowanie"
  ]
  node [
    id 2679
    label "zabalsamowanie"
  ]
  node [
    id 2680
    label "nieumar&#322;y"
  ]
  node [
    id 2681
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 2682
    label "balsamowanie"
  ]
  node [
    id 2683
    label "balsamowa&#263;"
  ]
  node [
    id 2684
    label "sekcja"
  ]
  node [
    id 2685
    label "pochowa&#263;"
  ]
  node [
    id 2686
    label "mi&#281;so"
  ]
  node [
    id 2687
    label "ekshumowanie"
  ]
  node [
    id 2688
    label "pogrzeb"
  ]
  node [
    id 2689
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 2690
    label "kremacja"
  ]
  node [
    id 2691
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 2692
    label "jednostka_organizacyjna"
  ]
  node [
    id 2693
    label "Izba_Konsyliarska"
  ]
  node [
    id 2694
    label "zesp&#243;&#322;"
  ]
  node [
    id 2695
    label "ekshumowa&#263;"
  ]
  node [
    id 2696
    label "zabalsamowa&#263;"
  ]
  node [
    id 2697
    label "lu&#378;ny"
  ]
  node [
    id 2698
    label "rozlegle"
  ]
  node [
    id 2699
    label "rozleg&#322;y"
  ]
  node [
    id 2700
    label "rozci&#261;gle"
  ]
  node [
    id 2701
    label "szeroki"
  ]
  node [
    id 2702
    label "nieformalny"
  ]
  node [
    id 2703
    label "dodatkowy"
  ]
  node [
    id 2704
    label "nieregularny"
  ]
  node [
    id 2705
    label "rozdeptywanie"
  ]
  node [
    id 2706
    label "swobodny"
  ]
  node [
    id 2707
    label "rozwodnienie"
  ]
  node [
    id 2708
    label "rozrzedzenie"
  ]
  node [
    id 2709
    label "nieokre&#347;lony"
  ]
  node [
    id 2710
    label "rozdeptanie"
  ]
  node [
    id 2711
    label "rozwadnianie"
  ]
  node [
    id 2712
    label "&#322;atwy"
  ]
  node [
    id 2713
    label "lu&#378;no"
  ]
  node [
    id 2714
    label "nie&#347;cis&#322;y"
  ]
  node [
    id 2715
    label "rozrzedzanie"
  ]
  node [
    id 2716
    label "zrzedni&#281;cie"
  ]
  node [
    id 2717
    label "rzedni&#281;cie"
  ]
  node [
    id 2718
    label "d&#322;ugo"
  ]
  node [
    id 2719
    label "rozci&#261;g&#322;y"
  ]
  node [
    id 2720
    label "across_the_board"
  ]
  node [
    id 2721
    label "pozwolenie"
  ]
  node [
    id 2722
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 2723
    label "zaawansowanie"
  ]
  node [
    id 2724
    label "wykszta&#322;cenie"
  ]
  node [
    id 2725
    label "cognition"
  ]
  node [
    id 2726
    label "psychoanaliza"
  ]
  node [
    id 2727
    label "ekstraspekcja"
  ]
  node [
    id 2728
    label "zemdle&#263;"
  ]
  node [
    id 2729
    label "conscience"
  ]
  node [
    id 2730
    label "feeling"
  ]
  node [
    id 2731
    label "Freud"
  ]
  node [
    id 2732
    label "rozwini&#281;cie"
  ]
  node [
    id 2733
    label "pomo&#380;enie"
  ]
  node [
    id 2734
    label "training"
  ]
  node [
    id 2735
    label "o&#347;wiecenie"
  ]
  node [
    id 2736
    label "zapoznanie"
  ]
  node [
    id 2737
    label "udoskonalenie"
  ]
  node [
    id 2738
    label "wys&#322;anie"
  ]
  node [
    id 2739
    label "stopie&#324;"
  ]
  node [
    id 2740
    label "pozwole&#324;stwo"
  ]
  node [
    id 2741
    label "umo&#380;liwienie"
  ]
  node [
    id 2742
    label "franchise"
  ]
  node [
    id 2743
    label "bycie_w_stanie"
  ]
  node [
    id 2744
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 2745
    label "uznanie"
  ]
  node [
    id 2746
    label "zwolnienie_si&#281;"
  ]
  node [
    id 2747
    label "koncesjonowanie"
  ]
  node [
    id 2748
    label "odpowied&#378;"
  ]
  node [
    id 2749
    label "pofolgowanie"
  ]
  node [
    id 2750
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 2751
    label "zwalnianie_si&#281;"
  ]
  node [
    id 2752
    label "odwieszenie"
  ]
  node [
    id 2753
    label "authorization"
  ]
  node [
    id 2754
    label "license"
  ]
  node [
    id 2755
    label "accommodate"
  ]
  node [
    id 2756
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 2757
    label "pozwoli&#263;"
  ]
  node [
    id 2758
    label "dress"
  ]
  node [
    id 2759
    label "obieca&#263;"
  ]
  node [
    id 2760
    label "przeznaczy&#263;"
  ]
  node [
    id 2761
    label "odst&#261;pi&#263;"
  ]
  node [
    id 2762
    label "zada&#263;"
  ]
  node [
    id 2763
    label "rap"
  ]
  node [
    id 2764
    label "feed"
  ]
  node [
    id 2765
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 2766
    label "picture"
  ]
  node [
    id 2767
    label "zap&#322;aci&#263;"
  ]
  node [
    id 2768
    label "sztachn&#261;&#263;"
  ]
  node [
    id 2769
    label "doda&#263;"
  ]
  node [
    id 2770
    label "dostarczy&#263;"
  ]
  node [
    id 2771
    label "przywali&#263;"
  ]
  node [
    id 2772
    label "wyrzec_si&#281;"
  ]
  node [
    id 2773
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 2774
    label "powierzy&#263;"
  ]
  node [
    id 2775
    label "zdarzony"
  ]
  node [
    id 2776
    label "odpowiednio"
  ]
  node [
    id 2777
    label "specjalny"
  ]
  node [
    id 2778
    label "odpowiadanie"
  ]
  node [
    id 2779
    label "nale&#380;nie"
  ]
  node [
    id 2780
    label "godny"
  ]
  node [
    id 2781
    label "powinny"
  ]
  node [
    id 2782
    label "przynale&#380;ny"
  ]
  node [
    id 2783
    label "przystojny"
  ]
  node [
    id 2784
    label "zadowalaj&#261;cy"
  ]
  node [
    id 2785
    label "nale&#380;ycie"
  ]
  node [
    id 2786
    label "specjalnie"
  ]
  node [
    id 2787
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 2788
    label "niedorozw&#243;j"
  ]
  node [
    id 2789
    label "nienormalny"
  ]
  node [
    id 2790
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 2791
    label "umy&#347;lnie"
  ]
  node [
    id 2792
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 2793
    label "nieetatowy"
  ]
  node [
    id 2794
    label "szczeg&#243;lny"
  ]
  node [
    id 2795
    label "intencjonalny"
  ]
  node [
    id 2796
    label "pytanie"
  ]
  node [
    id 2797
    label "picie_piwa"
  ]
  node [
    id 2798
    label "parry"
  ]
  node [
    id 2799
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 2800
    label "pokutowanie"
  ]
  node [
    id 2801
    label "powodowanie"
  ]
  node [
    id 2802
    label "dzianie_si&#281;"
  ]
  node [
    id 2803
    label "rozmawianie"
  ]
  node [
    id 2804
    label "ponoszenie"
  ]
  node [
    id 2805
    label "odpowiedzialny"
  ]
  node [
    id 2806
    label "winny"
  ]
  node [
    id 2807
    label "stosowny"
  ]
  node [
    id 2808
    label "charakterystycznie"
  ]
  node [
    id 2809
    label "dobrze"
  ]
  node [
    id 2810
    label "spos&#243;b"
  ]
  node [
    id 2811
    label "niezb&#281;dnik"
  ]
  node [
    id 2812
    label "tylec"
  ]
  node [
    id 2813
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 2814
    label "&#322;y&#380;ka_sto&#322;owa"
  ]
  node [
    id 2815
    label "widelec"
  ]
  node [
    id 2816
    label "przybornik"
  ]
  node [
    id 2817
    label "discipline"
  ]
  node [
    id 2818
    label "zboczy&#263;"
  ]
  node [
    id 2819
    label "w&#261;tek"
  ]
  node [
    id 2820
    label "sponiewiera&#263;"
  ]
  node [
    id 2821
    label "zboczenie"
  ]
  node [
    id 2822
    label "zbaczanie"
  ]
  node [
    id 2823
    label "om&#243;wi&#263;"
  ]
  node [
    id 2824
    label "kr&#261;&#380;enie"
  ]
  node [
    id 2825
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 2826
    label "zbacza&#263;"
  ]
  node [
    id 2827
    label "om&#243;wienie"
  ]
  node [
    id 2828
    label "tematyka"
  ]
  node [
    id 2829
    label "omawianie"
  ]
  node [
    id 2830
    label "omawia&#263;"
  ]
  node [
    id 2831
    label "program_nauczania"
  ]
  node [
    id 2832
    label "sponiewieranie"
  ]
  node [
    id 2833
    label "chemikalia"
  ]
  node [
    id 2834
    label "abstrakcja"
  ]
  node [
    id 2835
    label "nature"
  ]
  node [
    id 2836
    label "komora"
  ]
  node [
    id 2837
    label "wyrz&#261;dzenie"
  ]
  node [
    id 2838
    label "kom&#243;rka"
  ]
  node [
    id 2839
    label "impulsator"
  ]
  node [
    id 2840
    label "przygotowanie"
  ]
  node [
    id 2841
    label "furnishing"
  ]
  node [
    id 2842
    label "zabezpieczenie"
  ]
  node [
    id 2843
    label "sprz&#281;t"
  ]
  node [
    id 2844
    label "aparatura"
  ]
  node [
    id 2845
    label "ig&#322;a"
  ]
  node [
    id 2846
    label "wirnik"
  ]
  node [
    id 2847
    label "zablokowanie"
  ]
  node [
    id 2848
    label "blokowanie"
  ]
  node [
    id 2849
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 2850
    label "system_energetyczny"
  ]
  node [
    id 2851
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 2852
    label "zagospodarowanie"
  ]
  node [
    id 2853
    label "mechanizm"
  ]
  node [
    id 2854
    label "bli&#378;ni"
  ]
  node [
    id 2855
    label "swojak"
  ]
  node [
    id 2856
    label "wyposa&#380;enie"
  ]
  node [
    id 2857
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 2858
    label "spotkanie"
  ]
  node [
    id 2859
    label "pracownia"
  ]
  node [
    id 2860
    label "szybko&#347;&#263;"
  ]
  node [
    id 2861
    label "kondycja_fizyczna"
  ]
  node [
    id 2862
    label "harcerski"
  ]
  node [
    id 2863
    label "zdrowie"
  ]
  node [
    id 2864
    label "zinformatyzowanie"
  ]
  node [
    id 2865
    label "zainstalowanie"
  ]
  node [
    id 2866
    label "fixture"
  ]
  node [
    id 2867
    label "spotkanie_si&#281;"
  ]
  node [
    id 2868
    label "gather"
  ]
  node [
    id 2869
    label "zawarcie"
  ]
  node [
    id 2870
    label "po&#380;egnanie"
  ]
  node [
    id 2871
    label "spotykanie"
  ]
  node [
    id 2872
    label "powitanie"
  ]
  node [
    id 2873
    label "znalezienie"
  ]
  node [
    id 2874
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 2875
    label "znajomy"
  ]
  node [
    id 2876
    label "stosunek_prawny"
  ]
  node [
    id 2877
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 2878
    label "uregulowa&#263;"
  ]
  node [
    id 2879
    label "oblig"
  ]
  node [
    id 2880
    label "obowi&#261;zek"
  ]
  node [
    id 2881
    label "duty"
  ]
  node [
    id 2882
    label "occupation"
  ]
  node [
    id 2883
    label "miejsce_pracy"
  ]
  node [
    id 2884
    label "czyn"
  ]
  node [
    id 2885
    label "wyko&#324;czenie"
  ]
  node [
    id 2886
    label "umowa"
  ]
  node [
    id 2887
    label "instytut"
  ]
  node [
    id 2888
    label "zak&#322;adka"
  ]
  node [
    id 2889
    label "firma"
  ]
  node [
    id 2890
    label "company"
  ]
  node [
    id 2891
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 2892
    label "&#321;ubianka"
  ]
  node [
    id 2893
    label "Bia&#322;y_Dom"
  ]
  node [
    id 2894
    label "dzia&#322;_personalny"
  ]
  node [
    id 2895
    label "Kreml"
  ]
  node [
    id 2896
    label "sadowisko"
  ]
  node [
    id 2897
    label "odmienianie"
  ]
  node [
    id 2898
    label "zmianka"
  ]
  node [
    id 2899
    label "amendment"
  ]
  node [
    id 2900
    label "passage"
  ]
  node [
    id 2901
    label "rewizja"
  ]
  node [
    id 2902
    label "tura"
  ]
  node [
    id 2903
    label "change"
  ]
  node [
    id 2904
    label "ferment"
  ]
  node [
    id 2905
    label "anatomopatolog"
  ]
  node [
    id 2906
    label "wytrwa&#322;y"
  ]
  node [
    id 2907
    label "cierpliwy"
  ]
  node [
    id 2908
    label "benedykty&#324;sko"
  ]
  node [
    id 2909
    label "po_benedykty&#324;sku"
  ]
  node [
    id 2910
    label "craft"
  ]
  node [
    id 2911
    label "zawodoznawstwo"
  ]
  node [
    id 2912
    label "office"
  ]
  node [
    id 2913
    label "nakr&#281;canie"
  ]
  node [
    id 2914
    label "nakr&#281;cenie"
  ]
  node [
    id 2915
    label "zarz&#261;dzanie"
  ]
  node [
    id 2916
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 2917
    label "skakanie"
  ]
  node [
    id 2918
    label "d&#261;&#380;enie"
  ]
  node [
    id 2919
    label "zatrzymanie"
  ]
  node [
    id 2920
    label "postaranie_si&#281;"
  ]
  node [
    id 2921
    label "przepracowanie"
  ]
  node [
    id 2922
    label "przepracowanie_si&#281;"
  ]
  node [
    id 2923
    label "podlizanie_si&#281;"
  ]
  node [
    id 2924
    label "podlizywanie_si&#281;"
  ]
  node [
    id 2925
    label "przepracowywanie"
  ]
  node [
    id 2926
    label "w&#322;&#261;czenie"
  ]
  node [
    id 2927
    label "awansowanie"
  ]
  node [
    id 2928
    label "uruchomienie"
  ]
  node [
    id 2929
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 2930
    label "odpocz&#281;cie"
  ]
  node [
    id 2931
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 2932
    label "impact"
  ]
  node [
    id 2933
    label "podtrzymywanie"
  ]
  node [
    id 2934
    label "tr&#243;jstronny"
  ]
  node [
    id 2935
    label "courtship"
  ]
  node [
    id 2936
    label "funkcja"
  ]
  node [
    id 2937
    label "dopracowanie"
  ]
  node [
    id 2938
    label "zapracowanie"
  ]
  node [
    id 2939
    label "uruchamianie"
  ]
  node [
    id 2940
    label "wyrabianie"
  ]
  node [
    id 2941
    label "wyrobienie"
  ]
  node [
    id 2942
    label "spracowanie_si&#281;"
  ]
  node [
    id 2943
    label "poruszanie_si&#281;"
  ]
  node [
    id 2944
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 2945
    label "podejmowanie"
  ]
  node [
    id 2946
    label "funkcjonowanie"
  ]
  node [
    id 2947
    label "use"
  ]
  node [
    id 2948
    label "zaprz&#281;ganie"
  ]
  node [
    id 2949
    label "transakcja"
  ]
  node [
    id 2950
    label "lead"
  ]
  node [
    id 2951
    label "w&#322;adza"
  ]
  node [
    id 2952
    label "sprawdza&#263;"
  ]
  node [
    id 2953
    label "try"
  ]
  node [
    id 2954
    label "&#322;azi&#263;"
  ]
  node [
    id 2955
    label "ask"
  ]
  node [
    id 2956
    label "stara&#263;_si&#281;"
  ]
  node [
    id 2957
    label "szpiegowa&#263;"
  ]
  node [
    id 2958
    label "examine"
  ]
  node [
    id 2959
    label "tramp"
  ]
  node [
    id 2960
    label "narzuca&#263;_si&#281;"
  ]
  node [
    id 2961
    label "w&#322;&#243;czy&#263;_si&#281;"
  ]
  node [
    id 2962
    label "wp&#322;yw"
  ]
  node [
    id 2963
    label "porada"
  ]
  node [
    id 2964
    label "inspiration"
  ]
  node [
    id 2965
    label "zach&#281;ta"
  ]
  node [
    id 2966
    label "boost"
  ]
  node [
    id 2967
    label "czynnik"
  ]
  node [
    id 2968
    label "kibic"
  ]
  node [
    id 2969
    label "&#347;lad"
  ]
  node [
    id 2970
    label "kwota"
  ]
  node [
    id 2971
    label "lobbysta"
  ]
  node [
    id 2972
    label "doch&#243;d_narodowy"
  ]
  node [
    id 2973
    label "astrowate"
  ]
  node [
    id 2974
    label "ambrosia"
  ]
  node [
    id 2975
    label "mityczny"
  ]
  node [
    id 2976
    label "smako&#322;yk"
  ]
  node [
    id 2977
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 2978
    label "przejadanie"
  ]
  node [
    id 2979
    label "jadanie"
  ]
  node [
    id 2980
    label "posilanie"
  ]
  node [
    id 2981
    label "przejedzenie"
  ]
  node [
    id 2982
    label "odpasienie_si&#281;"
  ]
  node [
    id 2983
    label "papusianie"
  ]
  node [
    id 2984
    label "ufetowanie_si&#281;"
  ]
  node [
    id 2985
    label "wyjadanie"
  ]
  node [
    id 2986
    label "wpieprzanie"
  ]
  node [
    id 2987
    label "wmuszanie"
  ]
  node [
    id 2988
    label "objadanie"
  ]
  node [
    id 2989
    label "odpasanie_si&#281;"
  ]
  node [
    id 2990
    label "mlaskanie"
  ]
  node [
    id 2991
    label "posilenie"
  ]
  node [
    id 2992
    label "polowanie"
  ]
  node [
    id 2993
    label "&#380;arcie"
  ]
  node [
    id 2994
    label "przejadanie_si&#281;"
  ]
  node [
    id 2995
    label "przejedzenie_si&#281;"
  ]
  node [
    id 2996
    label "eating"
  ]
  node [
    id 2997
    label "wyjedzenie"
  ]
  node [
    id 2998
    label "zatruwanie_si&#281;"
  ]
  node [
    id 2999
    label "erotyka"
  ]
  node [
    id 3000
    label "gwa&#322;cenie"
  ]
  node [
    id 3001
    label "podniecanie"
  ]
  node [
    id 3002
    label "po&#380;ycie"
  ]
  node [
    id 3003
    label "baraszki"
  ]
  node [
    id 3004
    label "coexistence"
  ]
  node [
    id 3005
    label "numer"
  ]
  node [
    id 3006
    label "ruch_frykcyjny"
  ]
  node [
    id 3007
    label "wzw&#243;d"
  ]
  node [
    id 3008
    label "seks"
  ]
  node [
    id 3009
    label "pozycja_misjonarska"
  ]
  node [
    id 3010
    label "rozmna&#380;anie"
  ]
  node [
    id 3011
    label "wsp&#243;&#322;istnienie"
  ]
  node [
    id 3012
    label "imisja"
  ]
  node [
    id 3013
    label "podniecenie"
  ]
  node [
    id 3014
    label "podnieca&#263;"
  ]
  node [
    id 3015
    label "gra_wst&#281;pna"
  ]
  node [
    id 3016
    label "po&#380;&#261;danie"
  ]
  node [
    id 3017
    label "podnieci&#263;"
  ]
  node [
    id 3018
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 3019
    label "na_pieska"
  ]
  node [
    id 3020
    label "&#322;&#261;czenie"
  ]
  node [
    id 3021
    label "curio"
  ]
  node [
    id 3022
    label "pyszno&#347;ci"
  ]
  node [
    id 3023
    label "wypotnik"
  ]
  node [
    id 3024
    label "pochewka"
  ]
  node [
    id 3025
    label "strzyc"
  ]
  node [
    id 3026
    label "wegetacja"
  ]
  node [
    id 3027
    label "zadziorek"
  ]
  node [
    id 3028
    label "flawonoid"
  ]
  node [
    id 3029
    label "fitotron"
  ]
  node [
    id 3030
    label "w&#322;&#243;kno"
  ]
  node [
    id 3031
    label "zawi&#261;zek"
  ]
  node [
    id 3032
    label "pora&#380;a&#263;"
  ]
  node [
    id 3033
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 3034
    label "zbiorowisko"
  ]
  node [
    id 3035
    label "do&#322;owa&#263;"
  ]
  node [
    id 3036
    label "wegetowa&#263;"
  ]
  node [
    id 3037
    label "bulwka"
  ]
  node [
    id 3038
    label "sok"
  ]
  node [
    id 3039
    label "epiderma"
  ]
  node [
    id 3040
    label "g&#322;uszy&#263;"
  ]
  node [
    id 3041
    label "system_korzeniowy"
  ]
  node [
    id 3042
    label "g&#322;uszenie"
  ]
  node [
    id 3043
    label "owoc"
  ]
  node [
    id 3044
    label "strzy&#380;enie"
  ]
  node [
    id 3045
    label "wegetowanie"
  ]
  node [
    id 3046
    label "fotoautotrof"
  ]
  node [
    id 3047
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 3048
    label "gumoza"
  ]
  node [
    id 3049
    label "wyro&#347;le"
  ]
  node [
    id 3050
    label "fitocenoza"
  ]
  node [
    id 3051
    label "ro&#347;liny"
  ]
  node [
    id 3052
    label "odn&#243;&#380;ka"
  ]
  node [
    id 3053
    label "do&#322;owanie"
  ]
  node [
    id 3054
    label "nieuleczalnie_chory"
  ]
  node [
    id 3055
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 3056
    label "fikcyjny"
  ]
  node [
    id 3057
    label "nieprawdziwy"
  ]
  node [
    id 3058
    label "mitycznie"
  ]
  node [
    id 3059
    label "astrowce"
  ]
  node [
    id 3060
    label "plewinka"
  ]
  node [
    id 3061
    label "&#380;yciodajnie"
  ]
  node [
    id 3062
    label "dobroczynny"
  ]
  node [
    id 3063
    label "dobroczynnie"
  ]
  node [
    id 3064
    label "spo&#322;eczny"
  ]
  node [
    id 3065
    label "s&#322;odycz"
  ]
  node [
    id 3066
    label "nap&#243;j"
  ]
  node [
    id 3067
    label "amrita"
  ]
  node [
    id 3068
    label "wypitek"
  ]
  node [
    id 3069
    label "rozkosz"
  ]
  node [
    id 3070
    label "affability"
  ]
  node [
    id 3071
    label "s&#322;odziak"
  ]
  node [
    id 3072
    label "dobro&#263;"
  ]
  node [
    id 3073
    label "arcypi&#281;kny"
  ]
  node [
    id 3074
    label "cudowny"
  ]
  node [
    id 3075
    label "fantastyczny"
  ]
  node [
    id 3076
    label "nadprzyrodzony"
  ]
  node [
    id 3077
    label "bosko"
  ]
  node [
    id 3078
    label "&#347;mieszny"
  ]
  node [
    id 3079
    label "cudnie"
  ]
  node [
    id 3080
    label "bezpretensjonalny"
  ]
  node [
    id 3081
    label "niezr&#243;wnany"
  ]
  node [
    id 3082
    label "arcypi&#281;knie"
  ]
  node [
    id 3083
    label "fantastycznie"
  ]
  node [
    id 3084
    label "wymy&#347;lny"
  ]
  node [
    id 3085
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 3086
    label "fabularny"
  ]
  node [
    id 3087
    label "niewiarygodny"
  ]
  node [
    id 3088
    label "kapitalnie"
  ]
  node [
    id 3089
    label "nierealny"
  ]
  node [
    id 3090
    label "fajny"
  ]
  node [
    id 3091
    label "udanie"
  ]
  node [
    id 3092
    label "o&#347;mieszenie"
  ]
  node [
    id 3093
    label "o&#347;mieszanie"
  ]
  node [
    id 3094
    label "&#347;miesznie"
  ]
  node [
    id 3095
    label "nieadekwatny"
  ]
  node [
    id 3096
    label "bawny"
  ]
  node [
    id 3097
    label "niepowa&#380;ny"
  ]
  node [
    id 3098
    label "bezpretensjonalnie"
  ]
  node [
    id 3099
    label "nadnaturalnie"
  ]
  node [
    id 3100
    label "pozaziemski"
  ]
  node [
    id 3101
    label "pi&#281;knienie"
  ]
  node [
    id 3102
    label "przewspania&#322;y"
  ]
  node [
    id 3103
    label "wypi&#281;knienie"
  ]
  node [
    id 3104
    label "pi&#281;knie"
  ]
  node [
    id 3105
    label "cudownie"
  ]
  node [
    id 3106
    label "wonderfully"
  ]
  node [
    id 3107
    label "enormously"
  ]
  node [
    id 3108
    label "cudny"
  ]
  node [
    id 3109
    label "kunsztownie"
  ]
  node [
    id 3110
    label "deformowa&#263;"
  ]
  node [
    id 3111
    label "deformowanie"
  ]
  node [
    id 3112
    label "sfera_afektywna"
  ]
  node [
    id 3113
    label "sumienie"
  ]
  node [
    id 3114
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 3115
    label "fizjonomia"
  ]
  node [
    id 3116
    label "human_body"
  ]
  node [
    id 3117
    label "kompleks"
  ]
  node [
    id 3118
    label "piek&#322;o"
  ]
  node [
    id 3119
    label "oddech"
  ]
  node [
    id 3120
    label "nekromancja"
  ]
  node [
    id 3121
    label "seksualno&#347;&#263;"
  ]
  node [
    id 3122
    label "zjawa"
  ]
  node [
    id 3123
    label "zapalno&#347;&#263;"
  ]
  node [
    id 3124
    label "ego"
  ]
  node [
    id 3125
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 3126
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 3127
    label "Po&#347;wist"
  ]
  node [
    id 3128
    label "passion"
  ]
  node [
    id 3129
    label "zmar&#322;y"
  ]
  node [
    id 3130
    label "shape"
  ]
  node [
    id 3131
    label "T&#281;sknica"
  ]
  node [
    id 3132
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 3133
    label "odbicie"
  ]
  node [
    id 3134
    label "atom"
  ]
  node [
    id 3135
    label "kosmos"
  ]
  node [
    id 3136
    label "Ziemia"
  ]
  node [
    id 3137
    label "istota_fantastyczna"
  ]
  node [
    id 3138
    label "widziad&#322;o"
  ]
  node [
    id 3139
    label "refleksja"
  ]
  node [
    id 3140
    label "niewiedza"
  ]
  node [
    id 3141
    label "_id"
  ]
  node [
    id 3142
    label "ignorantness"
  ]
  node [
    id 3143
    label "unconsciousness"
  ]
  node [
    id 3144
    label "poczucie_w&#322;asnej_warto&#347;ci"
  ]
  node [
    id 3145
    label "self"
  ]
  node [
    id 3146
    label "podmiot"
  ]
  node [
    id 3147
    label "corrupt"
  ]
  node [
    id 3148
    label "distortion"
  ]
  node [
    id 3149
    label "contortion"
  ]
  node [
    id 3150
    label "zmienianie"
  ]
  node [
    id 3151
    label "group"
  ]
  node [
    id 3152
    label "band"
  ]
  node [
    id 3153
    label "wi&#261;zanie_koordynacyjne"
  ]
  node [
    id 3154
    label "ligand"
  ]
  node [
    id 3155
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 3156
    label "sorcery"
  ]
  node [
    id 3157
    label "wr&#243;&#380;biarstwo"
  ]
  node [
    id 3158
    label "magia"
  ]
  node [
    id 3159
    label "zw&#322;oki"
  ]
  node [
    id 3160
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 3161
    label "umarlak"
  ]
  node [
    id 3162
    label "martwy"
  ]
  node [
    id 3163
    label "chowanie"
  ]
  node [
    id 3164
    label "horror"
  ]
  node [
    id 3165
    label "szeol"
  ]
  node [
    id 3166
    label "zatka&#263;"
  ]
  node [
    id 3167
    label "zapieranie_oddechu"
  ]
  node [
    id 3168
    label "zatykanie"
  ]
  node [
    id 3169
    label "ud&#322;awienie_si&#281;"
  ]
  node [
    id 3170
    label "zapiera&#263;_oddech"
  ]
  node [
    id 3171
    label "zaparcie_oddechu"
  ]
  node [
    id 3172
    label "wdech"
  ]
  node [
    id 3173
    label "zatkanie"
  ]
  node [
    id 3174
    label "rebirthing"
  ]
  node [
    id 3175
    label "wydech"
  ]
  node [
    id 3176
    label "zaprze&#263;_oddech"
  ]
  node [
    id 3177
    label "ud&#322;awi&#263;_si&#281;"
  ]
  node [
    id 3178
    label "hipowentylacja"
  ]
  node [
    id 3179
    label "&#347;wista&#263;"
  ]
  node [
    id 3180
    label "zatyka&#263;"
  ]
  node [
    id 3181
    label "oddychanie"
  ]
  node [
    id 3182
    label "facjata"
  ]
  node [
    id 3183
    label "zapa&#322;"
  ]
  node [
    id 3184
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 3185
    label "The_Beatles"
  ]
  node [
    id 3186
    label "odm&#322;odzenie"
  ]
  node [
    id 3187
    label "odm&#322;adzanie"
  ]
  node [
    id 3188
    label "Depeche_Mode"
  ]
  node [
    id 3189
    label "odm&#322;adza&#263;"
  ]
  node [
    id 3190
    label "&#346;wietliki"
  ]
  node [
    id 3191
    label "zespolik"
  ]
  node [
    id 3192
    label "whole"
  ]
  node [
    id 3193
    label "Mazowsze"
  ]
  node [
    id 3194
    label "schorzenie"
  ]
  node [
    id 3195
    label "batch"
  ]
  node [
    id 3196
    label "zabudowania"
  ]
  node [
    id 3197
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 3198
    label "ropa"
  ]
  node [
    id 3199
    label "szczeg&#243;&#322;"
  ]
  node [
    id 3200
    label "zakonserwowa&#263;"
  ]
  node [
    id 3201
    label "spocz&#281;cie"
  ]
  node [
    id 3202
    label "poumieszczanie"
  ]
  node [
    id 3203
    label "powk&#322;adanie"
  ]
  node [
    id 3204
    label "w&#322;o&#380;enie"
  ]
  node [
    id 3205
    label "burying"
  ]
  node [
    id 3206
    label "burial"
  ]
  node [
    id 3207
    label "gr&#243;b"
  ]
  node [
    id 3208
    label "paraszyt"
  ]
  node [
    id 3209
    label "embalm"
  ]
  node [
    id 3210
    label "konserwowa&#263;"
  ]
  node [
    id 3211
    label "zakonserwowanie"
  ]
  node [
    id 3212
    label "poumieszcza&#263;"
  ]
  node [
    id 3213
    label "hide"
  ]
  node [
    id 3214
    label "znie&#347;&#263;"
  ]
  node [
    id 3215
    label "straci&#263;"
  ]
  node [
    id 3216
    label "powk&#322;ada&#263;"
  ]
  node [
    id 3217
    label "bury"
  ]
  node [
    id 3218
    label "makija&#380;ysta"
  ]
  node [
    id 3219
    label "odgrzebywa&#263;"
  ]
  node [
    id 3220
    label "odgrzeba&#263;"
  ]
  node [
    id 3221
    label "disinter"
  ]
  node [
    id 3222
    label "popio&#322;y"
  ]
  node [
    id 3223
    label "obrz&#281;d"
  ]
  node [
    id 3224
    label "zabieg"
  ]
  node [
    id 3225
    label "badanie"
  ]
  node [
    id 3226
    label "urz&#261;d"
  ]
  node [
    id 3227
    label "autopsy"
  ]
  node [
    id 3228
    label "podsekcja"
  ]
  node [
    id 3229
    label "ministerstwo"
  ]
  node [
    id 3230
    label "insourcing"
  ]
  node [
    id 3231
    label "relation"
  ]
  node [
    id 3232
    label "orkiestra"
  ]
  node [
    id 3233
    label "odgrzebanie"
  ]
  node [
    id 3234
    label "exhumation"
  ]
  node [
    id 3235
    label "odgrzebywanie"
  ]
  node [
    id 3236
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 3237
    label "grabarz"
  ]
  node [
    id 3238
    label "stypa"
  ]
  node [
    id 3239
    label "pusta_noc"
  ]
  node [
    id 3240
    label "niepowodzenie"
  ]
  node [
    id 3241
    label "konserwowanie"
  ]
  node [
    id 3242
    label "embalmment"
  ]
  node [
    id 3243
    label "termoczu&#322;y"
  ]
  node [
    id 3244
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 3245
    label "zagrza&#263;"
  ]
  node [
    id 3246
    label "denga"
  ]
  node [
    id 3247
    label "zapalenie_opon_m&#243;zgowo-rdzeniowych"
  ]
  node [
    id 3248
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 3249
    label "tautochrona"
  ]
  node [
    id 3250
    label "atmosfera"
  ]
  node [
    id 3251
    label "hotness"
  ]
  node [
    id 3252
    label "rozpalony"
  ]
  node [
    id 3253
    label "choroba_bosto&#324;ska"
  ]
  node [
    id 3254
    label "termoelektryczno&#347;&#263;"
  ]
  node [
    id 3255
    label "p&#322;aszczak"
  ]
  node [
    id 3256
    label "zakres"
  ]
  node [
    id 3257
    label "&#347;ciana"
  ]
  node [
    id 3258
    label "ukszta&#322;towanie"
  ]
  node [
    id 3259
    label "kwadrant"
  ]
  node [
    id 3260
    label "degree"
  ]
  node [
    id 3261
    label "wymiar"
  ]
  node [
    id 3262
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 3263
    label "surface"
  ]
  node [
    id 3264
    label "zaczyna&#263;"
  ]
  node [
    id 3265
    label "uruchamia&#263;"
  ]
  node [
    id 3266
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 3267
    label "przecina&#263;"
  ]
  node [
    id 3268
    label "unboxing"
  ]
  node [
    id 3269
    label "odci&#261;ga&#263;"
  ]
  node [
    id 3270
    label "drain"
  ]
  node [
    id 3271
    label "odprowadza&#263;"
  ]
  node [
    id 3272
    label "odsuwa&#263;"
  ]
  node [
    id 3273
    label "osusza&#263;"
  ]
  node [
    id 3274
    label "osuszanie"
  ]
  node [
    id 3275
    label "proces_chemiczny"
  ]
  node [
    id 3276
    label "dehydratacja"
  ]
  node [
    id 3277
    label "odprowadzanie"
  ]
  node [
    id 3278
    label "odsuwanie"
  ]
  node [
    id 3279
    label "odci&#261;ganie"
  ]
  node [
    id 3280
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 3281
    label "osuszy&#263;"
  ]
  node [
    id 3282
    label "odsun&#261;&#263;"
  ]
  node [
    id 3283
    label "odprowadzi&#263;"
  ]
  node [
    id 3284
    label "opening"
  ]
  node [
    id 3285
    label "zaczynanie"
  ]
  node [
    id 3286
    label "udost&#281;pnianie"
  ]
  node [
    id 3287
    label "przecinanie"
  ]
  node [
    id 3288
    label "operowanie"
  ]
  node [
    id 3289
    label "odprowadzenie"
  ]
  node [
    id 3290
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 3291
    label "odsuni&#281;cie"
  ]
  node [
    id 3292
    label "osuszenie"
  ]
  node [
    id 3293
    label "dehydration"
  ]
  node [
    id 3294
    label "rozpostarcie"
  ]
  node [
    id 3295
    label "pootwieranie"
  ]
  node [
    id 3296
    label "udost&#281;pnienie"
  ]
  node [
    id 3297
    label "przeci&#281;cie"
  ]
  node [
    id 3298
    label "zacz&#281;cie"
  ]
  node [
    id 3299
    label "ONZ"
  ]
  node [
    id 3300
    label "podsystem"
  ]
  node [
    id 3301
    label "NATO"
  ]
  node [
    id 3302
    label "systemat"
  ]
  node [
    id 3303
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 3304
    label "traktat_wersalski"
  ]
  node [
    id 3305
    label "przestawi&#263;"
  ]
  node [
    id 3306
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 3307
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 3308
    label "rozprz&#261;c"
  ]
  node [
    id 3309
    label "usenet"
  ]
  node [
    id 3310
    label "wi&#281;&#378;"
  ]
  node [
    id 3311
    label "treaty"
  ]
  node [
    id 3312
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 3313
    label "o&#347;"
  ]
  node [
    id 3314
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 3315
    label "cybernetyk"
  ]
  node [
    id 3316
    label "zawrze&#263;"
  ]
  node [
    id 3317
    label "alliance"
  ]
  node [
    id 3318
    label "sk&#322;ad"
  ]
  node [
    id 3319
    label "nerw"
  ]
  node [
    id 3320
    label "zaty&#322;"
  ]
  node [
    id 3321
    label "pupa"
  ]
  node [
    id 3322
    label "kierunek"
  ]
  node [
    id 3323
    label "za&#322;o&#380;enie"
  ]
  node [
    id 3324
    label "szkielet_osiowy"
  ]
  node [
    id 3325
    label "punkt_odniesienia"
  ]
  node [
    id 3326
    label "zasadzenie"
  ]
  node [
    id 3327
    label "dystraktor"
  ]
  node [
    id 3328
    label "podstawowy"
  ]
  node [
    id 3329
    label "miednica"
  ]
  node [
    id 3330
    label "skeletal_system"
  ]
  node [
    id 3331
    label "documentation"
  ]
  node [
    id 3332
    label "ko&#347;&#263;"
  ]
  node [
    id 3333
    label "konstrukcja"
  ]
  node [
    id 3334
    label "pas_barkowy"
  ]
  node [
    id 3335
    label "chrz&#261;stka"
  ]
  node [
    id 3336
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 3337
    label "zasadzi&#263;"
  ]
  node [
    id 3338
    label "wi&#281;zozrost"
  ]
  node [
    id 3339
    label "&#347;lizg_stawowy"
  ]
  node [
    id 3340
    label "kongruencja"
  ]
  node [
    id 3341
    label "koksartroza"
  ]
  node [
    id 3342
    label "ogr&#243;d_wodny"
  ]
  node [
    id 3343
    label "odprowadzalnik"
  ]
  node [
    id 3344
    label "panewka"
  ]
  node [
    id 3345
    label "zbiornik_wodny"
  ]
  node [
    id 3346
    label "kr&#261;&#380;ek_stawowy"
  ]
  node [
    id 3347
    label "t&#281;tnica_biodrowa_wewn&#281;trzna"
  ]
  node [
    id 3348
    label "t&#281;tnica_&#380;o&#322;&#261;dkowa"
  ]
  node [
    id 3349
    label "t&#281;tnica_biodrowa_wsp&#243;lna"
  ]
  node [
    id 3350
    label "t&#281;tnica_biodrowa_zewn&#281;trzna"
  ]
  node [
    id 3351
    label "patroszy&#263;"
  ]
  node [
    id 3352
    label "kiszki"
  ]
  node [
    id 3353
    label "gore"
  ]
  node [
    id 3354
    label "patroszenie"
  ]
  node [
    id 3355
    label "pie&#324;_trzewny"
  ]
  node [
    id 3356
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 3357
    label "ostrzy&#380;enie"
  ]
  node [
    id 3358
    label "sze&#347;ciopak"
  ]
  node [
    id 3359
    label "muscular_structure"
  ]
  node [
    id 3360
    label "klata"
  ]
  node [
    id 3361
    label "mi&#281;sie&#324;"
  ]
  node [
    id 3362
    label "intymny"
  ]
  node [
    id 3363
    label "genitalia"
  ]
  node [
    id 3364
    label "mi&#281;sie&#324;_l&#281;d&#378;wiowy"
  ]
  node [
    id 3365
    label "nerw_guziczny"
  ]
  node [
    id 3366
    label "plecy"
  ]
  node [
    id 3367
    label "okolica"
  ]
  node [
    id 3368
    label "okrywa"
  ]
  node [
    id 3369
    label "nask&#243;rek"
  ]
  node [
    id 3370
    label "surowiec"
  ]
  node [
    id 3371
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 3372
    label "wyprze&#263;"
  ]
  node [
    id 3373
    label "gestapowiec"
  ]
  node [
    id 3374
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 3375
    label "lico"
  ]
  node [
    id 3376
    label "pow&#322;oka"
  ]
  node [
    id 3377
    label "szczupak"
  ]
  node [
    id 3378
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 3379
    label "rockers"
  ]
  node [
    id 3380
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 3381
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 3382
    label "shell"
  ]
  node [
    id 3383
    label "p&#322;aszcz"
  ]
  node [
    id 3384
    label "wi&#243;rkownik"
  ]
  node [
    id 3385
    label "hardrockowiec"
  ]
  node [
    id 3386
    label "dupa"
  ]
  node [
    id 3387
    label "mizdra"
  ]
  node [
    id 3388
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 3389
    label "gruczo&#322;_potowy"
  ]
  node [
    id 3390
    label "harleyowiec"
  ]
  node [
    id 3391
    label "krupon"
  ]
  node [
    id 3392
    label "kurtka"
  ]
  node [
    id 3393
    label "&#322;upa"
  ]
  node [
    id 3394
    label "wej&#347;cie"
  ]
  node [
    id 3395
    label "shaft"
  ]
  node [
    id 3396
    label "ptaszek"
  ]
  node [
    id 3397
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 3398
    label "przyrodzenie"
  ]
  node [
    id 3399
    label "fiut"
  ]
  node [
    id 3400
    label "element_anatomiczny"
  ]
  node [
    id 3401
    label "wchodzenie"
  ]
  node [
    id 3402
    label "ut&#322;uczenie"
  ]
  node [
    id 3403
    label "wyluzowanie"
  ]
  node [
    id 3404
    label "obieralnia"
  ]
  node [
    id 3405
    label "potrawa"
  ]
  node [
    id 3406
    label "marynata"
  ]
  node [
    id 3407
    label "t&#322;uczenie"
  ]
  node [
    id 3408
    label "tempeh"
  ]
  node [
    id 3409
    label "skrusze&#263;"
  ]
  node [
    id 3410
    label "seitan"
  ]
  node [
    id 3411
    label "luzowa&#263;"
  ]
  node [
    id 3412
    label "luzowanie"
  ]
  node [
    id 3413
    label "panierka"
  ]
  node [
    id 3414
    label "chabanina"
  ]
  node [
    id 3415
    label "krusze&#263;"
  ]
  node [
    id 3416
    label "wyluzowa&#263;"
  ]
  node [
    id 3417
    label "rozumno&#347;&#263;"
  ]
  node [
    id 3418
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 3419
    label "inteligencja"
  ]
  node [
    id 3420
    label "emit"
  ]
  node [
    id 3421
    label "wywo&#322;a&#263;"
  ]
  node [
    id 3422
    label "inspire"
  ]
  node [
    id 3423
    label "wydziela&#263;"
  ]
  node [
    id 3424
    label "allocate"
  ]
  node [
    id 3425
    label "oddziela&#263;"
  ]
  node [
    id 3426
    label "wytwarza&#263;"
  ]
  node [
    id 3427
    label "wyznacza&#263;"
  ]
  node [
    id 3428
    label "wykrawa&#263;"
  ]
  node [
    id 3429
    label "exhaust"
  ]
  node [
    id 3430
    label "przydziela&#263;"
  ]
  node [
    id 3431
    label "wydali&#263;"
  ]
  node [
    id 3432
    label "przetworzy&#263;"
  ]
  node [
    id 3433
    label "revolutionize"
  ]
  node [
    id 3434
    label "oznajmi&#263;"
  ]
  node [
    id 3435
    label "wezwa&#263;"
  ]
  node [
    id 3436
    label "arouse"
  ]
  node [
    id 3437
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 3438
    label "moderate"
  ]
  node [
    id 3439
    label "twardo"
  ]
  node [
    id 3440
    label "surowie"
  ]
  node [
    id 3441
    label "bardzo"
  ]
  node [
    id 3442
    label "sternly"
  ]
  node [
    id 3443
    label "surowo"
  ]
  node [
    id 3444
    label "surowy"
  ]
  node [
    id 3445
    label "powa&#380;nie"
  ]
  node [
    id 3446
    label "w_chuj"
  ]
  node [
    id 3447
    label "gro&#378;nie"
  ]
  node [
    id 3448
    label "masywnie"
  ]
  node [
    id 3449
    label "&#378;le"
  ]
  node [
    id 3450
    label "monumentalnie"
  ]
  node [
    id 3451
    label "niedelikatnie"
  ]
  node [
    id 3452
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 3453
    label "heavily"
  ]
  node [
    id 3454
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 3455
    label "niezgrabnie"
  ]
  node [
    id 3456
    label "dotkliwie"
  ]
  node [
    id 3457
    label "nieudanie"
  ]
  node [
    id 3458
    label "wolno"
  ]
  node [
    id 3459
    label "g&#281;sto"
  ]
  node [
    id 3460
    label "dynamicznie"
  ]
  node [
    id 3461
    label "nieust&#281;pliwie"
  ]
  node [
    id 3462
    label "twardy"
  ]
  node [
    id 3463
    label "wytrwale"
  ]
  node [
    id 3464
    label "powa&#380;ny"
  ]
  node [
    id 3465
    label "niema&#322;o"
  ]
  node [
    id 3466
    label "oszcz&#281;dnie"
  ]
  node [
    id 3467
    label "&#347;wie&#380;y"
  ]
  node [
    id 3468
    label "srogi"
  ]
  node [
    id 3469
    label "dokuczliwy"
  ]
  node [
    id 3470
    label "oszcz&#281;dny"
  ]
  node [
    id 3471
    label "niebezpieczny"
  ]
  node [
    id 3472
    label "nad&#261;sany"
  ]
  node [
    id 3473
    label "Dekan"
  ]
  node [
    id 3474
    label "l&#261;d"
  ]
  node [
    id 3475
    label "Jura"
  ]
  node [
    id 3476
    label "Kosowo"
  ]
  node [
    id 3477
    label "zach&#243;d"
  ]
  node [
    id 3478
    label "Zabu&#380;e"
  ]
  node [
    id 3479
    label "antroposfera"
  ]
  node [
    id 3480
    label "Arktyka"
  ]
  node [
    id 3481
    label "Notogea"
  ]
  node [
    id 3482
    label "Piotrowo"
  ]
  node [
    id 3483
    label "akrecja"
  ]
  node [
    id 3484
    label "Ludwin&#243;w"
  ]
  node [
    id 3485
    label "Ruda_Pabianicka"
  ]
  node [
    id 3486
    label "po&#322;udnie"
  ]
  node [
    id 3487
    label "wsch&#243;d"
  ]
  node [
    id 3488
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 3489
    label "Pow&#261;zki"
  ]
  node [
    id 3490
    label "&#321;&#281;g"
  ]
  node [
    id 3491
    label "p&#243;&#322;noc"
  ]
  node [
    id 3492
    label "Rakowice"
  ]
  node [
    id 3493
    label "Syberia_Wschodnia"
  ]
  node [
    id 3494
    label "Zab&#322;ocie"
  ]
  node [
    id 3495
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 3496
    label "Kresy_Zachodnie"
  ]
  node [
    id 3497
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 3498
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 3499
    label "holarktyka"
  ]
  node [
    id 3500
    label "pas_planetoid"
  ]
  node [
    id 3501
    label "Antarktyka"
  ]
  node [
    id 3502
    label "Syberia_Zachodnia"
  ]
  node [
    id 3503
    label "Neogea"
  ]
  node [
    id 3504
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 3505
    label "Olszanica"
  ]
  node [
    id 3506
    label "pojazd"
  ]
  node [
    id 3507
    label "skorupa_ziemska"
  ]
  node [
    id 3508
    label "ascend"
  ]
  node [
    id 3509
    label "oderwa&#263;_si&#281;"
  ]
  node [
    id 3510
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 3511
    label "my&#347;lenie"
  ]
  node [
    id 3512
    label "proces_my&#347;lowy"
  ]
  node [
    id 3513
    label "skupianie_si&#281;"
  ]
  node [
    id 3514
    label "walczenie"
  ]
  node [
    id 3515
    label "treatment"
  ]
  node [
    id 3516
    label "pilnowanie"
  ]
  node [
    id 3517
    label "zinterpretowanie"
  ]
  node [
    id 3518
    label "troskanie_si&#281;"
  ]
  node [
    id 3519
    label "reflection"
  ]
  node [
    id 3520
    label "wzlecenie"
  ]
  node [
    id 3521
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 3522
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 3523
    label "walczy&#263;"
  ]
  node [
    id 3524
    label "s&#261;dzenie"
  ]
  node [
    id 3525
    label "belfer"
  ]
  node [
    id 3526
    label "szkolnik"
  ]
  node [
    id 3527
    label "preceptor"
  ]
  node [
    id 3528
    label "kszta&#322;ciciel"
  ]
  node [
    id 3529
    label "profesor"
  ]
  node [
    id 3530
    label "pedagog"
  ]
  node [
    id 3531
    label "popularyzator"
  ]
  node [
    id 3532
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 3533
    label "rozszerzyciel"
  ]
  node [
    id 3534
    label "autor"
  ]
  node [
    id 3535
    label "nauczyciel_akademicki"
  ]
  node [
    id 3536
    label "tytu&#322;"
  ]
  node [
    id 3537
    label "stopie&#324;_naukowy"
  ]
  node [
    id 3538
    label "konsulent"
  ]
  node [
    id 3539
    label "profesura"
  ]
  node [
    id 3540
    label "wirtuoz"
  ]
  node [
    id 3541
    label "ochotnik"
  ]
  node [
    id 3542
    label "nauczyciel_muzyki"
  ]
  node [
    id 3543
    label "pomocnik"
  ]
  node [
    id 3544
    label "zakonnik"
  ]
  node [
    id 3545
    label "zwierzchnik"
  ]
  node [
    id 3546
    label "ekspert"
  ]
  node [
    id 3547
    label "John_Dewey"
  ]
  node [
    id 3548
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 3549
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 3550
    label "J&#281;drzejewicz"
  ]
  node [
    id 3551
    label "zaimponowanie"
  ]
  node [
    id 3552
    label "postawa"
  ]
  node [
    id 3553
    label "imponowanie"
  ]
  node [
    id 3554
    label "uszanowanie"
  ]
  node [
    id 3555
    label "szanowa&#263;"
  ]
  node [
    id 3556
    label "uhonorowanie"
  ]
  node [
    id 3557
    label "uhonorowa&#263;"
  ]
  node [
    id 3558
    label "szacuneczek"
  ]
  node [
    id 3559
    label "respect"
  ]
  node [
    id 3560
    label "follow-up"
  ]
  node [
    id 3561
    label "rewerencja"
  ]
  node [
    id 3562
    label "przewidzenie"
  ]
  node [
    id 3563
    label "honorowanie"
  ]
  node [
    id 3564
    label "honorowa&#263;"
  ]
  node [
    id 3565
    label "uszanowa&#263;"
  ]
  node [
    id 3566
    label "spodziewanie_si&#281;"
  ]
  node [
    id 3567
    label "zaplanowanie"
  ]
  node [
    id 3568
    label "obliczenie"
  ]
  node [
    id 3569
    label "nastawienie"
  ]
  node [
    id 3570
    label "pozycja"
  ]
  node [
    id 3571
    label "attitude"
  ]
  node [
    id 3572
    label "powa&#380;anie"
  ]
  node [
    id 3573
    label "wyp&#322;aci&#263;"
  ]
  node [
    id 3574
    label "uczci&#263;"
  ]
  node [
    id 3575
    label "nagrodzi&#263;"
  ]
  node [
    id 3576
    label "treasure"
  ]
  node [
    id 3577
    label "respektowa&#263;"
  ]
  node [
    id 3578
    label "wzbudzenie"
  ]
  node [
    id 3579
    label "honor"
  ]
  node [
    id 3580
    label "wyrazi&#263;"
  ]
  node [
    id 3581
    label "spare_part"
  ]
  node [
    id 3582
    label "uznawa&#263;"
  ]
  node [
    id 3583
    label "wyp&#322;aca&#263;"
  ]
  node [
    id 3584
    label "czci&#263;"
  ]
  node [
    id 3585
    label "acknowledge"
  ]
  node [
    id 3586
    label "zap&#322;acenie"
  ]
  node [
    id 3587
    label "nagrodzenie"
  ]
  node [
    id 3588
    label "uprawi&#263;"
  ]
  node [
    id 3589
    label "might"
  ]
  node [
    id 3590
    label "public_treasury"
  ]
  node [
    id 3591
    label "obrobi&#263;"
  ]
  node [
    id 3592
    label "nietrze&#378;wy"
  ]
  node [
    id 3593
    label "gotowo"
  ]
  node [
    id 3594
    label "przygotowywanie"
  ]
  node [
    id 3595
    label "dyspozycyjny"
  ]
  node [
    id 3596
    label "bliski"
  ]
  node [
    id 3597
    label "zalany"
  ]
  node [
    id 3598
    label "doj&#347;cie"
  ]
  node [
    id 3599
    label "nieuchronny"
  ]
  node [
    id 3600
    label "czekanie"
  ]
  node [
    id 3601
    label "nadawa&#263;"
  ]
  node [
    id 3602
    label "mieni&#263;"
  ]
  node [
    id 3603
    label "przesy&#322;a&#263;"
  ]
  node [
    id 3604
    label "obgadywa&#263;"
  ]
  node [
    id 3605
    label "rekomendowa&#263;"
  ]
  node [
    id 3606
    label "sprawia&#263;"
  ]
  node [
    id 3607
    label "donosi&#263;"
  ]
  node [
    id 3608
    label "za&#322;atwia&#263;"
  ]
  node [
    id 3609
    label "assign"
  ]
  node [
    id 3610
    label "gada&#263;"
  ]
  node [
    id 3611
    label "decydowa&#263;"
  ]
  node [
    id 3612
    label "signify"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 273
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 332
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 287
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 620
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 254
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 498
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 287
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 684
  ]
  edge [
    source 20
    target 466
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 20
    target 967
  ]
  edge [
    source 20
    target 968
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 20
    target 971
  ]
  edge [
    source 20
    target 972
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 103
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 100
  ]
  edge [
    source 21
    target 672
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 106
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 998
  ]
  edge [
    source 21
    target 999
  ]
  edge [
    source 21
    target 1000
  ]
  edge [
    source 21
    target 1001
  ]
  edge [
    source 21
    target 1002
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 1006
  ]
  edge [
    source 22
    target 1007
  ]
  edge [
    source 22
    target 1008
  ]
  edge [
    source 22
    target 1009
  ]
  edge [
    source 22
    target 1010
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1014
  ]
  edge [
    source 22
    target 295
  ]
  edge [
    source 22
    target 388
  ]
  edge [
    source 22
    target 1015
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 466
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 169
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 654
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 779
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 485
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 864
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 827
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 650
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 837
  ]
  edge [
    source 24
    target 714
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 819
  ]
  edge [
    source 24
    target 840
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 846
  ]
  edge [
    source 24
    target 842
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1149
  ]
  edge [
    source 25
    target 1150
  ]
  edge [
    source 25
    target 1151
  ]
  edge [
    source 25
    target 1152
  ]
  edge [
    source 25
    target 1153
  ]
  edge [
    source 25
    target 1154
  ]
  edge [
    source 25
    target 1155
  ]
  edge [
    source 25
    target 1156
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 585
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1171
  ]
  edge [
    source 25
    target 1172
  ]
  edge [
    source 25
    target 1173
  ]
  edge [
    source 25
    target 1174
  ]
  edge [
    source 25
    target 1175
  ]
  edge [
    source 25
    target 1176
  ]
  edge [
    source 25
    target 1177
  ]
  edge [
    source 25
    target 1178
  ]
  edge [
    source 25
    target 1179
  ]
  edge [
    source 25
    target 1180
  ]
  edge [
    source 25
    target 1181
  ]
  edge [
    source 25
    target 1182
  ]
  edge [
    source 25
    target 1183
  ]
  edge [
    source 25
    target 1184
  ]
  edge [
    source 25
    target 888
  ]
  edge [
    source 25
    target 1185
  ]
  edge [
    source 25
    target 1186
  ]
  edge [
    source 25
    target 1187
  ]
  edge [
    source 25
    target 1188
  ]
  edge [
    source 25
    target 1189
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 436
  ]
  edge [
    source 25
    target 1192
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 25
    target 940
  ]
  edge [
    source 25
    target 1193
  ]
  edge [
    source 25
    target 1194
  ]
  edge [
    source 25
    target 1195
  ]
  edge [
    source 25
    target 929
  ]
  edge [
    source 25
    target 1196
  ]
  edge [
    source 25
    target 1197
  ]
  edge [
    source 25
    target 1198
  ]
  edge [
    source 25
    target 1199
  ]
  edge [
    source 25
    target 1200
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1208
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 1210
  ]
  edge [
    source 25
    target 1211
  ]
  edge [
    source 25
    target 1212
  ]
  edge [
    source 25
    target 1213
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 1214
  ]
  edge [
    source 25
    target 328
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 733
  ]
  edge [
    source 25
    target 1215
  ]
  edge [
    source 25
    target 586
  ]
  edge [
    source 25
    target 1216
  ]
  edge [
    source 25
    target 1217
  ]
  edge [
    source 25
    target 1218
  ]
  edge [
    source 25
    target 1219
  ]
  edge [
    source 25
    target 1220
  ]
  edge [
    source 25
    target 1221
  ]
  edge [
    source 25
    target 1222
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 1224
  ]
  edge [
    source 25
    target 1225
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 863
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 608
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
  edge [
    source 25
    target 1265
  ]
  edge [
    source 25
    target 771
  ]
  edge [
    source 25
    target 1266
  ]
  edge [
    source 25
    target 730
  ]
  edge [
    source 25
    target 1267
  ]
  edge [
    source 25
    target 1268
  ]
  edge [
    source 25
    target 1269
  ]
  edge [
    source 25
    target 1270
  ]
  edge [
    source 25
    target 1271
  ]
  edge [
    source 25
    target 1090
  ]
  edge [
    source 25
    target 1272
  ]
  edge [
    source 25
    target 1273
  ]
  edge [
    source 25
    target 1274
  ]
  edge [
    source 25
    target 1275
  ]
  edge [
    source 25
    target 1276
  ]
  edge [
    source 25
    target 1277
  ]
  edge [
    source 25
    target 1278
  ]
  edge [
    source 25
    target 1279
  ]
  edge [
    source 25
    target 1280
  ]
  edge [
    source 25
    target 1281
  ]
  edge [
    source 25
    target 1282
  ]
  edge [
    source 25
    target 1283
  ]
  edge [
    source 25
    target 1029
  ]
  edge [
    source 25
    target 1284
  ]
  edge [
    source 25
    target 1285
  ]
  edge [
    source 25
    target 1286
  ]
  edge [
    source 25
    target 1287
  ]
  edge [
    source 25
    target 1288
  ]
  edge [
    source 25
    target 1134
  ]
  edge [
    source 25
    target 1289
  ]
  edge [
    source 25
    target 1290
  ]
  edge [
    source 25
    target 1291
  ]
  edge [
    source 25
    target 1292
  ]
  edge [
    source 25
    target 1293
  ]
  edge [
    source 25
    target 1294
  ]
  edge [
    source 25
    target 1295
  ]
  edge [
    source 25
    target 1296
  ]
  edge [
    source 25
    target 1297
  ]
  edge [
    source 25
    target 451
  ]
  edge [
    source 25
    target 1298
  ]
  edge [
    source 25
    target 1299
  ]
  edge [
    source 25
    target 1300
  ]
  edge [
    source 25
    target 1301
  ]
  edge [
    source 25
    target 1302
  ]
  edge [
    source 25
    target 1303
  ]
  edge [
    source 25
    target 1304
  ]
  edge [
    source 25
    target 1305
  ]
  edge [
    source 25
    target 1306
  ]
  edge [
    source 25
    target 1307
  ]
  edge [
    source 25
    target 1308
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 1310
  ]
  edge [
    source 25
    target 1311
  ]
  edge [
    source 25
    target 461
  ]
  edge [
    source 25
    target 48
  ]
  edge [
    source 25
    target 64
  ]
  edge [
    source 25
    target 67
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 49
  ]
  edge [
    source 27
    target 50
  ]
  edge [
    source 27
    target 59
  ]
  edge [
    source 27
    target 60
  ]
  edge [
    source 27
    target 82
  ]
  edge [
    source 27
    target 83
  ]
  edge [
    source 27
    target 88
  ]
  edge [
    source 27
    target 89
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1312
  ]
  edge [
    source 28
    target 1313
  ]
  edge [
    source 28
    target 1314
  ]
  edge [
    source 28
    target 1315
  ]
  edge [
    source 28
    target 1316
  ]
  edge [
    source 28
    target 1317
  ]
  edge [
    source 28
    target 1318
  ]
  edge [
    source 28
    target 1319
  ]
  edge [
    source 28
    target 1320
  ]
  edge [
    source 28
    target 1321
  ]
  edge [
    source 28
    target 1322
  ]
  edge [
    source 28
    target 1323
  ]
  edge [
    source 28
    target 1324
  ]
  edge [
    source 28
    target 1325
  ]
  edge [
    source 28
    target 1326
  ]
  edge [
    source 28
    target 1327
  ]
  edge [
    source 28
    target 1328
  ]
  edge [
    source 28
    target 1329
  ]
  edge [
    source 28
    target 1330
  ]
  edge [
    source 28
    target 1331
  ]
  edge [
    source 28
    target 1332
  ]
  edge [
    source 28
    target 1333
  ]
  edge [
    source 28
    target 1334
  ]
  edge [
    source 28
    target 1335
  ]
  edge [
    source 28
    target 1336
  ]
  edge [
    source 28
    target 1337
  ]
  edge [
    source 28
    target 1338
  ]
  edge [
    source 28
    target 1339
  ]
  edge [
    source 28
    target 1340
  ]
  edge [
    source 28
    target 1341
  ]
  edge [
    source 28
    target 1342
  ]
  edge [
    source 28
    target 1343
  ]
  edge [
    source 28
    target 1344
  ]
  edge [
    source 28
    target 1345
  ]
  edge [
    source 28
    target 1346
  ]
  edge [
    source 28
    target 1347
  ]
  edge [
    source 28
    target 1348
  ]
  edge [
    source 28
    target 1263
  ]
  edge [
    source 28
    target 1349
  ]
  edge [
    source 28
    target 1350
  ]
  edge [
    source 28
    target 1351
  ]
  edge [
    source 28
    target 608
  ]
  edge [
    source 28
    target 1352
  ]
  edge [
    source 28
    target 1353
  ]
  edge [
    source 28
    target 1354
  ]
  edge [
    source 28
    target 1355
  ]
  edge [
    source 28
    target 1356
  ]
  edge [
    source 28
    target 1357
  ]
  edge [
    source 28
    target 1358
  ]
  edge [
    source 28
    target 1359
  ]
  edge [
    source 28
    target 1360
  ]
  edge [
    source 28
    target 1361
  ]
  edge [
    source 28
    target 1362
  ]
  edge [
    source 28
    target 1363
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1364
  ]
  edge [
    source 28
    target 1365
  ]
  edge [
    source 28
    target 1366
  ]
  edge [
    source 28
    target 1367
  ]
  edge [
    source 28
    target 1368
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1369
  ]
  edge [
    source 29
    target 1370
  ]
  edge [
    source 29
    target 1371
  ]
  edge [
    source 29
    target 1372
  ]
  edge [
    source 29
    target 1373
  ]
  edge [
    source 29
    target 1374
  ]
  edge [
    source 29
    target 1375
  ]
  edge [
    source 29
    target 1376
  ]
  edge [
    source 29
    target 1377
  ]
  edge [
    source 29
    target 310
  ]
  edge [
    source 29
    target 1378
  ]
  edge [
    source 29
    target 1379
  ]
  edge [
    source 29
    target 1380
  ]
  edge [
    source 29
    target 1381
  ]
  edge [
    source 29
    target 397
  ]
  edge [
    source 29
    target 1382
  ]
  edge [
    source 29
    target 318
  ]
  edge [
    source 29
    target 1383
  ]
  edge [
    source 29
    target 361
  ]
  edge [
    source 29
    target 1384
  ]
  edge [
    source 29
    target 1385
  ]
  edge [
    source 29
    target 1386
  ]
  edge [
    source 29
    target 1387
  ]
  edge [
    source 29
    target 142
  ]
  edge [
    source 29
    target 1388
  ]
  edge [
    source 29
    target 1389
  ]
  edge [
    source 29
    target 1390
  ]
  edge [
    source 29
    target 1391
  ]
  edge [
    source 29
    target 1392
  ]
  edge [
    source 29
    target 1393
  ]
  edge [
    source 29
    target 1394
  ]
  edge [
    source 29
    target 1005
  ]
  edge [
    source 29
    target 1395
  ]
  edge [
    source 29
    target 1396
  ]
  edge [
    source 29
    target 1397
  ]
  edge [
    source 29
    target 1398
  ]
  edge [
    source 29
    target 1399
  ]
  edge [
    source 29
    target 1400
  ]
  edge [
    source 29
    target 1401
  ]
  edge [
    source 29
    target 1402
  ]
  edge [
    source 29
    target 1403
  ]
  edge [
    source 29
    target 1404
  ]
  edge [
    source 29
    target 1405
  ]
  edge [
    source 29
    target 1406
  ]
  edge [
    source 29
    target 1407
  ]
  edge [
    source 29
    target 1408
  ]
  edge [
    source 29
    target 1409
  ]
  edge [
    source 29
    target 1410
  ]
  edge [
    source 29
    target 1411
  ]
  edge [
    source 29
    target 1412
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1413
  ]
  edge [
    source 30
    target 1414
  ]
  edge [
    source 30
    target 1415
  ]
  edge [
    source 30
    target 1416
  ]
  edge [
    source 30
    target 173
  ]
  edge [
    source 30
    target 1417
  ]
  edge [
    source 30
    target 235
  ]
  edge [
    source 30
    target 1418
  ]
  edge [
    source 30
    target 1419
  ]
  edge [
    source 30
    target 238
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 1420
  ]
  edge [
    source 30
    target 227
  ]
  edge [
    source 30
    target 1421
  ]
  edge [
    source 30
    target 1422
  ]
  edge [
    source 30
    target 1423
  ]
  edge [
    source 30
    target 1424
  ]
  edge [
    source 30
    target 1425
  ]
  edge [
    source 30
    target 1426
  ]
  edge [
    source 30
    target 891
  ]
  edge [
    source 30
    target 893
  ]
  edge [
    source 30
    target 1427
  ]
  edge [
    source 30
    target 1428
  ]
  edge [
    source 30
    target 1429
  ]
  edge [
    source 30
    target 1430
  ]
  edge [
    source 30
    target 805
  ]
  edge [
    source 30
    target 1431
  ]
  edge [
    source 30
    target 1432
  ]
  edge [
    source 30
    target 1433
  ]
  edge [
    source 30
    target 1434
  ]
  edge [
    source 30
    target 1435
  ]
  edge [
    source 30
    target 1436
  ]
  edge [
    source 30
    target 1437
  ]
  edge [
    source 30
    target 1438
  ]
  edge [
    source 30
    target 1439
  ]
  edge [
    source 30
    target 1440
  ]
  edge [
    source 30
    target 229
  ]
  edge [
    source 30
    target 1441
  ]
  edge [
    source 30
    target 1442
  ]
  edge [
    source 30
    target 759
  ]
  edge [
    source 30
    target 788
  ]
  edge [
    source 30
    target 1443
  ]
  edge [
    source 30
    target 1444
  ]
  edge [
    source 30
    target 1445
  ]
  edge [
    source 30
    target 1446
  ]
  edge [
    source 30
    target 1447
  ]
  edge [
    source 30
    target 1448
  ]
  edge [
    source 30
    target 1449
  ]
  edge [
    source 30
    target 1450
  ]
  edge [
    source 30
    target 1451
  ]
  edge [
    source 30
    target 1452
  ]
  edge [
    source 30
    target 1453
  ]
  edge [
    source 30
    target 1454
  ]
  edge [
    source 30
    target 1455
  ]
  edge [
    source 30
    target 1456
  ]
  edge [
    source 30
    target 1457
  ]
  edge [
    source 30
    target 1458
  ]
  edge [
    source 30
    target 1459
  ]
  edge [
    source 30
    target 1460
  ]
  edge [
    source 30
    target 1461
  ]
  edge [
    source 30
    target 1462
  ]
  edge [
    source 30
    target 1463
  ]
  edge [
    source 30
    target 1464
  ]
  edge [
    source 30
    target 1465
  ]
  edge [
    source 30
    target 1466
  ]
  edge [
    source 30
    target 1467
  ]
  edge [
    source 30
    target 1468
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 30
    target 1469
  ]
  edge [
    source 30
    target 1470
  ]
  edge [
    source 30
    target 1471
  ]
  edge [
    source 30
    target 1472
  ]
  edge [
    source 30
    target 1473
  ]
  edge [
    source 30
    target 1474
  ]
  edge [
    source 30
    target 1475
  ]
  edge [
    source 30
    target 1476
  ]
  edge [
    source 30
    target 1477
  ]
  edge [
    source 30
    target 1478
  ]
  edge [
    source 30
    target 1479
  ]
  edge [
    source 30
    target 1480
  ]
  edge [
    source 30
    target 1481
  ]
  edge [
    source 30
    target 1482
  ]
  edge [
    source 30
    target 1380
  ]
  edge [
    source 30
    target 1483
  ]
  edge [
    source 30
    target 779
  ]
  edge [
    source 30
    target 1484
  ]
  edge [
    source 30
    target 1485
  ]
  edge [
    source 30
    target 1486
  ]
  edge [
    source 30
    target 1487
  ]
  edge [
    source 30
    target 1488
  ]
  edge [
    source 30
    target 1489
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1490
  ]
  edge [
    source 33
    target 1491
  ]
  edge [
    source 33
    target 1311
  ]
  edge [
    source 33
    target 1492
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 1493
  ]
  edge [
    source 33
    target 1494
  ]
  edge [
    source 33
    target 1495
  ]
  edge [
    source 33
    target 1496
  ]
  edge [
    source 33
    target 1497
  ]
  edge [
    source 33
    target 1498
  ]
  edge [
    source 33
    target 804
  ]
  edge [
    source 33
    target 1499
  ]
  edge [
    source 33
    target 1500
  ]
  edge [
    source 33
    target 1501
  ]
  edge [
    source 33
    target 1502
  ]
  edge [
    source 33
    target 1503
  ]
  edge [
    source 33
    target 1504
  ]
  edge [
    source 33
    target 1505
  ]
  edge [
    source 33
    target 1506
  ]
  edge [
    source 33
    target 1507
  ]
  edge [
    source 33
    target 466
  ]
  edge [
    source 33
    target 177
  ]
  edge [
    source 33
    target 1508
  ]
  edge [
    source 33
    target 214
  ]
  edge [
    source 33
    target 238
  ]
  edge [
    source 33
    target 1509
  ]
  edge [
    source 33
    target 1510
  ]
  edge [
    source 33
    target 1451
  ]
  edge [
    source 33
    target 1511
  ]
  edge [
    source 33
    target 249
  ]
  edge [
    source 33
    target 233
  ]
  edge [
    source 33
    target 226
  ]
  edge [
    source 33
    target 1512
  ]
  edge [
    source 33
    target 1513
  ]
  edge [
    source 33
    target 1514
  ]
  edge [
    source 33
    target 1515
  ]
  edge [
    source 33
    target 1516
  ]
  edge [
    source 33
    target 1517
  ]
  edge [
    source 33
    target 1518
  ]
  edge [
    source 33
    target 1519
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 1520
  ]
  edge [
    source 33
    target 1521
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 1522
  ]
  edge [
    source 33
    target 1523
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 585
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 333
  ]
  edge [
    source 33
    target 987
  ]
  edge [
    source 33
    target 1524
  ]
  edge [
    source 33
    target 1525
  ]
  edge [
    source 33
    target 1526
  ]
  edge [
    source 33
    target 1527
  ]
  edge [
    source 33
    target 221
  ]
  edge [
    source 33
    target 1528
  ]
  edge [
    source 33
    target 75
  ]
  edge [
    source 33
    target 1529
  ]
  edge [
    source 33
    target 1530
  ]
  edge [
    source 33
    target 1531
  ]
  edge [
    source 33
    target 339
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 1532
  ]
  edge [
    source 33
    target 341
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 1533
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 1534
  ]
  edge [
    source 33
    target 1535
  ]
  edge [
    source 33
    target 1536
  ]
  edge [
    source 33
    target 1537
  ]
  edge [
    source 33
    target 1538
  ]
  edge [
    source 33
    target 1539
  ]
  edge [
    source 33
    target 440
  ]
  edge [
    source 33
    target 213
  ]
  edge [
    source 33
    target 1540
  ]
  edge [
    source 33
    target 1541
  ]
  edge [
    source 33
    target 1542
  ]
  edge [
    source 33
    target 1543
  ]
  edge [
    source 33
    target 1544
  ]
  edge [
    source 33
    target 229
  ]
  edge [
    source 33
    target 420
  ]
  edge [
    source 33
    target 1545
  ]
  edge [
    source 33
    target 1546
  ]
  edge [
    source 33
    target 1547
  ]
  edge [
    source 33
    target 1548
  ]
  edge [
    source 33
    target 1549
  ]
  edge [
    source 33
    target 912
  ]
  edge [
    source 33
    target 1550
  ]
  edge [
    source 33
    target 1551
  ]
  edge [
    source 33
    target 1552
  ]
  edge [
    source 33
    target 1152
  ]
  edge [
    source 33
    target 216
  ]
  edge [
    source 33
    target 1553
  ]
  edge [
    source 33
    target 1554
  ]
  edge [
    source 33
    target 1555
  ]
  edge [
    source 33
    target 1556
  ]
  edge [
    source 33
    target 1557
  ]
  edge [
    source 33
    target 1558
  ]
  edge [
    source 33
    target 1559
  ]
  edge [
    source 33
    target 1560
  ]
  edge [
    source 33
    target 1561
  ]
  edge [
    source 33
    target 1562
  ]
  edge [
    source 33
    target 1563
  ]
  edge [
    source 33
    target 620
  ]
  edge [
    source 33
    target 830
  ]
  edge [
    source 33
    target 1564
  ]
  edge [
    source 33
    target 1565
  ]
  edge [
    source 33
    target 1566
  ]
  edge [
    source 33
    target 1567
  ]
  edge [
    source 33
    target 1568
  ]
  edge [
    source 33
    target 1569
  ]
  edge [
    source 33
    target 1570
  ]
  edge [
    source 33
    target 1571
  ]
  edge [
    source 33
    target 1572
  ]
  edge [
    source 33
    target 1573
  ]
  edge [
    source 33
    target 1574
  ]
  edge [
    source 33
    target 1575
  ]
  edge [
    source 33
    target 1576
  ]
  edge [
    source 33
    target 1577
  ]
  edge [
    source 33
    target 1578
  ]
  edge [
    source 33
    target 1579
  ]
  edge [
    source 33
    target 1580
  ]
  edge [
    source 33
    target 1581
  ]
  edge [
    source 33
    target 1582
  ]
  edge [
    source 33
    target 1583
  ]
  edge [
    source 33
    target 1584
  ]
  edge [
    source 33
    target 1585
  ]
  edge [
    source 33
    target 602
  ]
  edge [
    source 33
    target 1586
  ]
  edge [
    source 33
    target 1587
  ]
  edge [
    source 33
    target 1588
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 1589
  ]
  edge [
    source 33
    target 1590
  ]
  edge [
    source 33
    target 1591
  ]
  edge [
    source 33
    target 1592
  ]
  edge [
    source 33
    target 1593
  ]
  edge [
    source 33
    target 246
  ]
  edge [
    source 33
    target 1594
  ]
  edge [
    source 33
    target 1595
  ]
  edge [
    source 33
    target 1596
  ]
  edge [
    source 33
    target 1597
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1264
  ]
  edge [
    source 34
    target 1598
  ]
  edge [
    source 34
    target 1599
  ]
  edge [
    source 34
    target 1600
  ]
  edge [
    source 34
    target 608
  ]
  edge [
    source 34
    target 1601
  ]
  edge [
    source 34
    target 1602
  ]
  edge [
    source 34
    target 1603
  ]
  edge [
    source 34
    target 1340
  ]
  edge [
    source 34
    target 1604
  ]
  edge [
    source 34
    target 840
  ]
  edge [
    source 34
    target 1090
  ]
  edge [
    source 34
    target 1319
  ]
  edge [
    source 34
    target 1605
  ]
  edge [
    source 34
    target 1606
  ]
  edge [
    source 34
    target 68
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1607
  ]
  edge [
    source 35
    target 1608
  ]
  edge [
    source 35
    target 1609
  ]
  edge [
    source 35
    target 1610
  ]
  edge [
    source 35
    target 1611
  ]
  edge [
    source 35
    target 1612
  ]
  edge [
    source 35
    target 1100
  ]
  edge [
    source 35
    target 1613
  ]
  edge [
    source 35
    target 513
  ]
  edge [
    source 35
    target 1614
  ]
  edge [
    source 35
    target 1615
  ]
  edge [
    source 35
    target 1616
  ]
  edge [
    source 35
    target 1617
  ]
  edge [
    source 35
    target 1618
  ]
  edge [
    source 35
    target 766
  ]
  edge [
    source 35
    target 1619
  ]
  edge [
    source 35
    target 1620
  ]
  edge [
    source 35
    target 1621
  ]
  edge [
    source 35
    target 1622
  ]
  edge [
    source 35
    target 1623
  ]
  edge [
    source 35
    target 1624
  ]
  edge [
    source 35
    target 1625
  ]
  edge [
    source 35
    target 1626
  ]
  edge [
    source 35
    target 1627
  ]
  edge [
    source 35
    target 1628
  ]
  edge [
    source 35
    target 1629
  ]
  edge [
    source 35
    target 1630
  ]
  edge [
    source 35
    target 628
  ]
  edge [
    source 35
    target 1631
  ]
  edge [
    source 35
    target 1632
  ]
  edge [
    source 35
    target 1633
  ]
  edge [
    source 35
    target 1634
  ]
  edge [
    source 35
    target 1635
  ]
  edge [
    source 35
    target 1636
  ]
  edge [
    source 35
    target 1637
  ]
  edge [
    source 35
    target 1638
  ]
  edge [
    source 35
    target 1639
  ]
  edge [
    source 35
    target 1640
  ]
  edge [
    source 35
    target 1641
  ]
  edge [
    source 35
    target 1642
  ]
  edge [
    source 35
    target 1643
  ]
  edge [
    source 35
    target 1644
  ]
  edge [
    source 35
    target 1645
  ]
  edge [
    source 35
    target 1646
  ]
  edge [
    source 35
    target 1647
  ]
  edge [
    source 35
    target 1648
  ]
  edge [
    source 35
    target 332
  ]
  edge [
    source 35
    target 1649
  ]
  edge [
    source 35
    target 287
  ]
  edge [
    source 35
    target 1650
  ]
  edge [
    source 35
    target 1651
  ]
  edge [
    source 35
    target 1019
  ]
  edge [
    source 35
    target 1652
  ]
  edge [
    source 35
    target 1653
  ]
  edge [
    source 35
    target 1654
  ]
  edge [
    source 35
    target 1111
  ]
  edge [
    source 35
    target 1655
  ]
  edge [
    source 35
    target 1656
  ]
  edge [
    source 35
    target 1657
  ]
  edge [
    source 35
    target 1658
  ]
  edge [
    source 35
    target 1659
  ]
  edge [
    source 35
    target 1660
  ]
  edge [
    source 35
    target 1661
  ]
  edge [
    source 35
    target 1662
  ]
  edge [
    source 35
    target 1663
  ]
  edge [
    source 35
    target 908
  ]
  edge [
    source 35
    target 1664
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1665
  ]
  edge [
    source 36
    target 238
  ]
  edge [
    source 36
    target 1666
  ]
  edge [
    source 36
    target 1667
  ]
  edge [
    source 36
    target 1668
  ]
  edge [
    source 36
    target 252
  ]
  edge [
    source 36
    target 1669
  ]
  edge [
    source 36
    target 1670
  ]
  edge [
    source 36
    target 1303
  ]
  edge [
    source 36
    target 1671
  ]
  edge [
    source 36
    target 1672
  ]
  edge [
    source 36
    target 341
  ]
  edge [
    source 36
    target 214
  ]
  edge [
    source 36
    target 1673
  ]
  edge [
    source 36
    target 1674
  ]
  edge [
    source 36
    target 685
  ]
  edge [
    source 36
    target 1675
  ]
  edge [
    source 36
    target 1618
  ]
  edge [
    source 36
    target 1676
  ]
  edge [
    source 36
    target 1677
  ]
  edge [
    source 36
    target 766
  ]
  edge [
    source 36
    target 1678
  ]
  edge [
    source 36
    target 779
  ]
  edge [
    source 36
    target 1679
  ]
  edge [
    source 36
    target 1680
  ]
  edge [
    source 36
    target 1681
  ]
  edge [
    source 36
    target 279
  ]
  edge [
    source 36
    target 1682
  ]
  edge [
    source 36
    target 1065
  ]
  edge [
    source 36
    target 74
  ]
  edge [
    source 36
    target 1683
  ]
  edge [
    source 36
    target 1684
  ]
  edge [
    source 36
    target 1685
  ]
  edge [
    source 36
    target 1686
  ]
  edge [
    source 36
    target 1687
  ]
  edge [
    source 36
    target 1688
  ]
  edge [
    source 36
    target 1689
  ]
  edge [
    source 36
    target 1690
  ]
  edge [
    source 36
    target 269
  ]
  edge [
    source 36
    target 1691
  ]
  edge [
    source 36
    target 1692
  ]
  edge [
    source 36
    target 1693
  ]
  edge [
    source 36
    target 1694
  ]
  edge [
    source 36
    target 1695
  ]
  edge [
    source 36
    target 1696
  ]
  edge [
    source 36
    target 1697
  ]
  edge [
    source 36
    target 1698
  ]
  edge [
    source 36
    target 1699
  ]
  edge [
    source 36
    target 1700
  ]
  edge [
    source 36
    target 1701
  ]
  edge [
    source 36
    target 1702
  ]
  edge [
    source 36
    target 226
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1703
  ]
  edge [
    source 37
    target 41
  ]
  edge [
    source 37
    target 1704
  ]
  edge [
    source 37
    target 1705
  ]
  edge [
    source 37
    target 1706
  ]
  edge [
    source 37
    target 1707
  ]
  edge [
    source 37
    target 1708
  ]
  edge [
    source 37
    target 1709
  ]
  edge [
    source 37
    target 295
  ]
  edge [
    source 37
    target 388
  ]
  edge [
    source 37
    target 389
  ]
  edge [
    source 37
    target 1710
  ]
  edge [
    source 37
    target 1711
  ]
  edge [
    source 37
    target 1712
  ]
  edge [
    source 37
    target 1713
  ]
  edge [
    source 37
    target 1714
  ]
  edge [
    source 37
    target 1715
  ]
  edge [
    source 37
    target 302
  ]
  edge [
    source 37
    target 132
  ]
  edge [
    source 37
    target 1716
  ]
  edge [
    source 37
    target 1717
  ]
  edge [
    source 37
    target 1718
  ]
  edge [
    source 37
    target 1719
  ]
  edge [
    source 37
    target 1720
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1721
  ]
  edge [
    source 38
    target 1722
  ]
  edge [
    source 38
    target 1723
  ]
  edge [
    source 38
    target 1724
  ]
  edge [
    source 38
    target 214
  ]
  edge [
    source 38
    target 1725
  ]
  edge [
    source 38
    target 1726
  ]
  edge [
    source 38
    target 1727
  ]
  edge [
    source 38
    target 1728
  ]
  edge [
    source 38
    target 1729
  ]
  edge [
    source 38
    target 628
  ]
  edge [
    source 38
    target 1730
  ]
  edge [
    source 38
    target 346
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 792
  ]
  edge [
    source 39
    target 987
  ]
  edge [
    source 39
    target 1731
  ]
  edge [
    source 39
    target 1732
  ]
  edge [
    source 39
    target 440
  ]
  edge [
    source 39
    target 898
  ]
  edge [
    source 39
    target 1733
  ]
  edge [
    source 39
    target 1734
  ]
  edge [
    source 39
    target 1735
  ]
  edge [
    source 39
    target 1736
  ]
  edge [
    source 39
    target 1737
  ]
  edge [
    source 39
    target 1738
  ]
  edge [
    source 39
    target 1739
  ]
  edge [
    source 39
    target 1740
  ]
  edge [
    source 39
    target 1741
  ]
  edge [
    source 39
    target 105
  ]
  edge [
    source 39
    target 1742
  ]
  edge [
    source 39
    target 1743
  ]
  edge [
    source 39
    target 1303
  ]
  edge [
    source 39
    target 1744
  ]
  edge [
    source 39
    target 1745
  ]
  edge [
    source 39
    target 214
  ]
  edge [
    source 39
    target 812
  ]
  edge [
    source 39
    target 1746
  ]
  edge [
    source 39
    target 1747
  ]
  edge [
    source 39
    target 1113
  ]
  edge [
    source 39
    target 1546
  ]
  edge [
    source 39
    target 1748
  ]
  edge [
    source 39
    target 1749
  ]
  edge [
    source 39
    target 1750
  ]
  edge [
    source 39
    target 498
  ]
  edge [
    source 39
    target 216
  ]
  edge [
    source 39
    target 1751
  ]
  edge [
    source 39
    target 1752
  ]
  edge [
    source 39
    target 1753
  ]
  edge [
    source 39
    target 494
  ]
  edge [
    source 39
    target 1754
  ]
  edge [
    source 39
    target 199
  ]
  edge [
    source 39
    target 1755
  ]
  edge [
    source 39
    target 1756
  ]
  edge [
    source 39
    target 1757
  ]
  edge [
    source 39
    target 61
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 39
    target 1758
  ]
  edge [
    source 39
    target 1759
  ]
  edge [
    source 39
    target 265
  ]
  edge [
    source 39
    target 448
  ]
  edge [
    source 39
    target 268
  ]
  edge [
    source 39
    target 258
  ]
  edge [
    source 39
    target 431
  ]
  edge [
    source 39
    target 259
  ]
  edge [
    source 39
    target 804
  ]
  edge [
    source 39
    target 1760
  ]
  edge [
    source 39
    target 1761
  ]
  edge [
    source 39
    target 1515
  ]
  edge [
    source 39
    target 1762
  ]
  edge [
    source 39
    target 1763
  ]
  edge [
    source 39
    target 1764
  ]
  edge [
    source 39
    target 1765
  ]
  edge [
    source 39
    target 1766
  ]
  edge [
    source 39
    target 1767
  ]
  edge [
    source 39
    target 1768
  ]
  edge [
    source 39
    target 1769
  ]
  edge [
    source 39
    target 692
  ]
  edge [
    source 39
    target 1770
  ]
  edge [
    source 39
    target 1771
  ]
  edge [
    source 39
    target 466
  ]
  edge [
    source 39
    target 1772
  ]
  edge [
    source 39
    target 1773
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1774
  ]
  edge [
    source 40
    target 1775
  ]
  edge [
    source 40
    target 1776
  ]
  edge [
    source 40
    target 57
  ]
  edge [
    source 40
    target 63
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 302
  ]
  edge [
    source 41
    target 132
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1777
  ]
  edge [
    source 41
    target 1778
  ]
  edge [
    source 41
    target 1779
  ]
  edge [
    source 41
    target 129
  ]
  edge [
    source 41
    target 1780
  ]
  edge [
    source 41
    target 1781
  ]
  edge [
    source 41
    target 1782
  ]
  edge [
    source 41
    target 1783
  ]
  edge [
    source 41
    target 1784
  ]
  edge [
    source 41
    target 1785
  ]
  edge [
    source 41
    target 130
  ]
  edge [
    source 41
    target 1786
  ]
  edge [
    source 41
    target 413
  ]
  edge [
    source 41
    target 414
  ]
  edge [
    source 41
    target 415
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 318
  ]
  edge [
    source 41
    target 416
  ]
  edge [
    source 41
    target 294
  ]
  edge [
    source 41
    target 417
  ]
  edge [
    source 41
    target 418
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 1787
  ]
  edge [
    source 41
    target 288
  ]
  edge [
    source 41
    target 1788
  ]
  edge [
    source 41
    target 1789
  ]
  edge [
    source 41
    target 1790
  ]
  edge [
    source 41
    target 397
  ]
  edge [
    source 41
    target 1791
  ]
  edge [
    source 41
    target 1792
  ]
  edge [
    source 41
    target 1793
  ]
  edge [
    source 41
    target 1794
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 74
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1795
  ]
  edge [
    source 42
    target 1796
  ]
  edge [
    source 42
    target 1797
  ]
  edge [
    source 42
    target 1798
  ]
  edge [
    source 42
    target 1799
  ]
  edge [
    source 42
    target 1800
  ]
  edge [
    source 42
    target 1801
  ]
  edge [
    source 42
    target 1802
  ]
  edge [
    source 42
    target 1803
  ]
  edge [
    source 42
    target 830
  ]
  edge [
    source 42
    target 1804
  ]
  edge [
    source 42
    target 1805
  ]
  edge [
    source 42
    target 1806
  ]
  edge [
    source 42
    target 1807
  ]
  edge [
    source 42
    target 1263
  ]
  edge [
    source 42
    target 1808
  ]
  edge [
    source 42
    target 608
  ]
  edge [
    source 42
    target 1809
  ]
  edge [
    source 42
    target 1810
  ]
  edge [
    source 42
    target 1811
  ]
  edge [
    source 42
    target 1090
  ]
  edge [
    source 42
    target 1812
  ]
  edge [
    source 42
    target 1813
  ]
  edge [
    source 42
    target 1814
  ]
  edge [
    source 42
    target 1340
  ]
  edge [
    source 42
    target 1815
  ]
  edge [
    source 42
    target 1816
  ]
  edge [
    source 42
    target 1817
  ]
  edge [
    source 42
    target 1818
  ]
  edge [
    source 42
    target 1819
  ]
  edge [
    source 42
    target 1820
  ]
  edge [
    source 42
    target 1821
  ]
  edge [
    source 42
    target 1822
  ]
  edge [
    source 42
    target 1823
  ]
  edge [
    source 42
    target 1824
  ]
  edge [
    source 42
    target 1825
  ]
  edge [
    source 42
    target 1826
  ]
  edge [
    source 42
    target 1827
  ]
  edge [
    source 42
    target 1828
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1829
  ]
  edge [
    source 43
    target 1830
  ]
  edge [
    source 43
    target 1831
  ]
  edge [
    source 43
    target 811
  ]
  edge [
    source 43
    target 1832
  ]
  edge [
    source 43
    target 1833
  ]
  edge [
    source 43
    target 1834
  ]
  edge [
    source 43
    target 1335
  ]
  edge [
    source 43
    target 1835
  ]
  edge [
    source 43
    target 635
  ]
  edge [
    source 43
    target 1836
  ]
  edge [
    source 43
    target 1837
  ]
  edge [
    source 43
    target 1838
  ]
  edge [
    source 43
    target 1090
  ]
  edge [
    source 43
    target 1839
  ]
  edge [
    source 43
    target 1840
  ]
  edge [
    source 43
    target 1841
  ]
  edge [
    source 43
    target 629
  ]
  edge [
    source 43
    target 827
  ]
  edge [
    source 43
    target 1842
  ]
  edge [
    source 43
    target 1843
  ]
  edge [
    source 43
    target 698
  ]
  edge [
    source 43
    target 1844
  ]
  edge [
    source 43
    target 633
  ]
  edge [
    source 43
    target 1845
  ]
  edge [
    source 43
    target 1846
  ]
  edge [
    source 43
    target 1847
  ]
  edge [
    source 43
    target 1848
  ]
  edge [
    source 43
    target 1849
  ]
  edge [
    source 43
    target 1850
  ]
  edge [
    source 43
    target 1851
  ]
  edge [
    source 43
    target 1852
  ]
  edge [
    source 43
    target 1853
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1854
  ]
  edge [
    source 44
    target 125
  ]
  edge [
    source 44
    target 287
  ]
  edge [
    source 44
    target 1855
  ]
  edge [
    source 44
    target 143
  ]
  edge [
    source 44
    target 1856
  ]
  edge [
    source 44
    target 1857
  ]
  edge [
    source 44
    target 1858
  ]
  edge [
    source 44
    target 1859
  ]
  edge [
    source 44
    target 1860
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1861
  ]
  edge [
    source 45
    target 1862
  ]
  edge [
    source 45
    target 1863
  ]
  edge [
    source 45
    target 1864
  ]
  edge [
    source 45
    target 1865
  ]
  edge [
    source 45
    target 1866
  ]
  edge [
    source 45
    target 912
  ]
  edge [
    source 45
    target 1867
  ]
  edge [
    source 45
    target 1868
  ]
  edge [
    source 45
    target 1869
  ]
  edge [
    source 45
    target 1870
  ]
  edge [
    source 45
    target 1871
  ]
  edge [
    source 45
    target 1872
  ]
  edge [
    source 45
    target 1873
  ]
  edge [
    source 45
    target 1874
  ]
  edge [
    source 45
    target 1875
  ]
  edge [
    source 45
    target 1876
  ]
  edge [
    source 45
    target 1618
  ]
  edge [
    source 45
    target 1877
  ]
  edge [
    source 45
    target 1878
  ]
  edge [
    source 45
    target 1879
  ]
  edge [
    source 45
    target 1880
  ]
  edge [
    source 45
    target 1881
  ]
  edge [
    source 45
    target 1882
  ]
  edge [
    source 45
    target 1540
  ]
  edge [
    source 45
    target 216
  ]
  edge [
    source 45
    target 1883
  ]
  edge [
    source 45
    target 1884
  ]
  edge [
    source 45
    target 1885
  ]
  edge [
    source 45
    target 1886
  ]
  edge [
    source 45
    target 1887
  ]
  edge [
    source 45
    target 1888
  ]
  edge [
    source 45
    target 1889
  ]
  edge [
    source 45
    target 1136
  ]
  edge [
    source 45
    target 1890
  ]
  edge [
    source 45
    target 242
  ]
  edge [
    source 45
    target 804
  ]
  edge [
    source 45
    target 59
  ]
  edge [
    source 45
    target 1891
  ]
  edge [
    source 45
    target 1892
  ]
  edge [
    source 45
    target 1893
  ]
  edge [
    source 45
    target 1894
  ]
  edge [
    source 45
    target 1657
  ]
  edge [
    source 45
    target 1895
  ]
  edge [
    source 45
    target 1896
  ]
  edge [
    source 45
    target 1897
  ]
  edge [
    source 45
    target 1898
  ]
  edge [
    source 45
    target 1899
  ]
  edge [
    source 45
    target 1900
  ]
  edge [
    source 45
    target 1901
  ]
  edge [
    source 45
    target 1902
  ]
  edge [
    source 45
    target 1903
  ]
  edge [
    source 45
    target 76
  ]
  edge [
    source 45
    target 1904
  ]
  edge [
    source 45
    target 1905
  ]
  edge [
    source 45
    target 1906
  ]
  edge [
    source 45
    target 1907
  ]
  edge [
    source 45
    target 1908
  ]
  edge [
    source 45
    target 1909
  ]
  edge [
    source 45
    target 1910
  ]
  edge [
    source 45
    target 1911
  ]
  edge [
    source 45
    target 1912
  ]
  edge [
    source 45
    target 1913
  ]
  edge [
    source 45
    target 1914
  ]
  edge [
    source 45
    target 1915
  ]
  edge [
    source 45
    target 1916
  ]
  edge [
    source 45
    target 1917
  ]
  edge [
    source 45
    target 1918
  ]
  edge [
    source 45
    target 1919
  ]
  edge [
    source 45
    target 1920
  ]
  edge [
    source 45
    target 1921
  ]
  edge [
    source 45
    target 1922
  ]
  edge [
    source 45
    target 1923
  ]
  edge [
    source 45
    target 1924
  ]
  edge [
    source 45
    target 1925
  ]
  edge [
    source 45
    target 1926
  ]
  edge [
    source 45
    target 1927
  ]
  edge [
    source 45
    target 1928
  ]
  edge [
    source 45
    target 1929
  ]
  edge [
    source 45
    target 1930
  ]
  edge [
    source 45
    target 1931
  ]
  edge [
    source 45
    target 1932
  ]
  edge [
    source 45
    target 1933
  ]
  edge [
    source 45
    target 1934
  ]
  edge [
    source 45
    target 1935
  ]
  edge [
    source 45
    target 287
  ]
  edge [
    source 45
    target 1936
  ]
  edge [
    source 45
    target 1937
  ]
  edge [
    source 45
    target 1938
  ]
  edge [
    source 45
    target 775
  ]
  edge [
    source 45
    target 1939
  ]
  edge [
    source 45
    target 1940
  ]
  edge [
    source 45
    target 1941
  ]
  edge [
    source 45
    target 1942
  ]
  edge [
    source 45
    target 1943
  ]
  edge [
    source 45
    target 1944
  ]
  edge [
    source 45
    target 1945
  ]
  edge [
    source 45
    target 1946
  ]
  edge [
    source 45
    target 1947
  ]
  edge [
    source 45
    target 1948
  ]
  edge [
    source 45
    target 1949
  ]
  edge [
    source 45
    target 1950
  ]
  edge [
    source 45
    target 1951
  ]
  edge [
    source 45
    target 1952
  ]
  edge [
    source 45
    target 1953
  ]
  edge [
    source 45
    target 1954
  ]
  edge [
    source 45
    target 1955
  ]
  edge [
    source 45
    target 1956
  ]
  edge [
    source 45
    target 1957
  ]
  edge [
    source 45
    target 1958
  ]
  edge [
    source 45
    target 1959
  ]
  edge [
    source 45
    target 1960
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 287
  ]
  edge [
    source 46
    target 1961
  ]
  edge [
    source 46
    target 1962
  ]
  edge [
    source 46
    target 1963
  ]
  edge [
    source 46
    target 1746
  ]
  edge [
    source 46
    target 1734
  ]
  edge [
    source 46
    target 320
  ]
  edge [
    source 46
    target 321
  ]
  edge [
    source 46
    target 322
  ]
  edge [
    source 46
    target 323
  ]
  edge [
    source 46
    target 324
  ]
  edge [
    source 46
    target 325
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 327
  ]
  edge [
    source 46
    target 328
  ]
  edge [
    source 46
    target 329
  ]
  edge [
    source 46
    target 330
  ]
  edge [
    source 46
    target 331
  ]
  edge [
    source 46
    target 332
  ]
  edge [
    source 46
    target 333
  ]
  edge [
    source 46
    target 334
  ]
  edge [
    source 46
    target 335
  ]
  edge [
    source 46
    target 336
  ]
  edge [
    source 46
    target 337
  ]
  edge [
    source 46
    target 75
  ]
  edge [
    source 46
    target 338
  ]
  edge [
    source 46
    target 339
  ]
  edge [
    source 46
    target 340
  ]
  edge [
    source 46
    target 341
  ]
  edge [
    source 46
    target 342
  ]
  edge [
    source 46
    target 343
  ]
  edge [
    source 46
    target 1964
  ]
  edge [
    source 46
    target 1965
  ]
  edge [
    source 46
    target 1966
  ]
  edge [
    source 46
    target 1967
  ]
  edge [
    source 46
    target 474
  ]
  edge [
    source 46
    target 1800
  ]
  edge [
    source 46
    target 105
  ]
  edge [
    source 46
    target 1968
  ]
  edge [
    source 46
    target 1969
  ]
  edge [
    source 46
    target 61
  ]
  edge [
    source 46
    target 214
  ]
  edge [
    source 46
    target 1758
  ]
  edge [
    source 46
    target 466
  ]
  edge [
    source 46
    target 1970
  ]
  edge [
    source 46
    target 1971
  ]
  edge [
    source 46
    target 1972
  ]
  edge [
    source 46
    target 1973
  ]
  edge [
    source 46
    target 912
  ]
  edge [
    source 46
    target 1974
  ]
  edge [
    source 46
    target 216
  ]
  edge [
    source 46
    target 1975
  ]
  edge [
    source 46
    target 1623
  ]
  edge [
    source 46
    target 776
  ]
  edge [
    source 46
    target 77
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1976
  ]
  edge [
    source 47
    target 1977
  ]
  edge [
    source 47
    target 1978
  ]
  edge [
    source 47
    target 96
  ]
  edge [
    source 47
    target 1979
  ]
  edge [
    source 47
    target 92
  ]
  edge [
    source 47
    target 1980
  ]
  edge [
    source 47
    target 1981
  ]
  edge [
    source 47
    target 1982
  ]
  edge [
    source 47
    target 1983
  ]
  edge [
    source 47
    target 1984
  ]
  edge [
    source 47
    target 1985
  ]
  edge [
    source 47
    target 93
  ]
  edge [
    source 47
    target 94
  ]
  edge [
    source 47
    target 95
  ]
  edge [
    source 47
    target 1986
  ]
  edge [
    source 47
    target 1987
  ]
  edge [
    source 47
    target 65
  ]
  edge [
    source 47
    target 1988
  ]
  edge [
    source 47
    target 1989
  ]
  edge [
    source 47
    target 1990
  ]
  edge [
    source 47
    target 1991
  ]
  edge [
    source 47
    target 1992
  ]
  edge [
    source 47
    target 1993
  ]
  edge [
    source 47
    target 1994
  ]
  edge [
    source 47
    target 1995
  ]
  edge [
    source 47
    target 346
  ]
  edge [
    source 47
    target 1996
  ]
  edge [
    source 47
    target 1997
  ]
  edge [
    source 47
    target 1402
  ]
  edge [
    source 47
    target 1998
  ]
  edge [
    source 47
    target 1999
  ]
  edge [
    source 47
    target 1013
  ]
  edge [
    source 47
    target 2000
  ]
  edge [
    source 47
    target 2001
  ]
  edge [
    source 47
    target 2002
  ]
  edge [
    source 47
    target 2003
  ]
  edge [
    source 47
    target 2004
  ]
  edge [
    source 47
    target 2005
  ]
  edge [
    source 47
    target 2006
  ]
  edge [
    source 47
    target 2007
  ]
  edge [
    source 47
    target 2008
  ]
  edge [
    source 47
    target 2009
  ]
  edge [
    source 47
    target 2010
  ]
  edge [
    source 47
    target 357
  ]
  edge [
    source 47
    target 2011
  ]
  edge [
    source 47
    target 2012
  ]
  edge [
    source 47
    target 2013
  ]
  edge [
    source 47
    target 349
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1772
  ]
  edge [
    source 48
    target 287
  ]
  edge [
    source 48
    target 2014
  ]
  edge [
    source 48
    target 2015
  ]
  edge [
    source 48
    target 1613
  ]
  edge [
    source 48
    target 2016
  ]
  edge [
    source 48
    target 2017
  ]
  edge [
    source 48
    target 2018
  ]
  edge [
    source 48
    target 2019
  ]
  edge [
    source 48
    target 2020
  ]
  edge [
    source 48
    target 2021
  ]
  edge [
    source 48
    target 2022
  ]
  edge [
    source 48
    target 2023
  ]
  edge [
    source 48
    target 1508
  ]
  edge [
    source 48
    target 2024
  ]
  edge [
    source 48
    target 2025
  ]
  edge [
    source 48
    target 684
  ]
  edge [
    source 48
    target 2026
  ]
  edge [
    source 48
    target 2027
  ]
  edge [
    source 48
    target 1618
  ]
  edge [
    source 48
    target 2028
  ]
  edge [
    source 48
    target 1193
  ]
  edge [
    source 48
    target 2029
  ]
  edge [
    source 48
    target 2030
  ]
  edge [
    source 48
    target 2031
  ]
  edge [
    source 48
    target 793
  ]
  edge [
    source 48
    target 912
  ]
  edge [
    source 48
    target 2032
  ]
  edge [
    source 48
    target 2033
  ]
  edge [
    source 48
    target 2034
  ]
  edge [
    source 48
    target 2035
  ]
  edge [
    source 48
    target 2036
  ]
  edge [
    source 48
    target 2037
  ]
  edge [
    source 48
    target 2038
  ]
  edge [
    source 48
    target 2039
  ]
  edge [
    source 48
    target 2040
  ]
  edge [
    source 48
    target 795
  ]
  edge [
    source 48
    target 151
  ]
  edge [
    source 48
    target 2041
  ]
  edge [
    source 48
    target 2042
  ]
  edge [
    source 48
    target 2043
  ]
  edge [
    source 48
    target 2044
  ]
  edge [
    source 48
    target 2045
  ]
  edge [
    source 48
    target 2046
  ]
  edge [
    source 48
    target 966
  ]
  edge [
    source 48
    target 1871
  ]
  edge [
    source 48
    target 2047
  ]
  edge [
    source 48
    target 2048
  ]
  edge [
    source 48
    target 2049
  ]
  edge [
    source 48
    target 2050
  ]
  edge [
    source 48
    target 2051
  ]
  edge [
    source 48
    target 2052
  ]
  edge [
    source 48
    target 2053
  ]
  edge [
    source 48
    target 2054
  ]
  edge [
    source 48
    target 2055
  ]
  edge [
    source 48
    target 2056
  ]
  edge [
    source 48
    target 2057
  ]
  edge [
    source 48
    target 61
  ]
  edge [
    source 48
    target 2058
  ]
  edge [
    source 48
    target 2059
  ]
  edge [
    source 48
    target 2060
  ]
  edge [
    source 48
    target 2061
  ]
  edge [
    source 48
    target 2062
  ]
  edge [
    source 48
    target 271
  ]
  edge [
    source 48
    target 2063
  ]
  edge [
    source 48
    target 2064
  ]
  edge [
    source 48
    target 2065
  ]
  edge [
    source 48
    target 946
  ]
  edge [
    source 48
    target 1735
  ]
  edge [
    source 48
    target 2066
  ]
  edge [
    source 48
    target 2067
  ]
  edge [
    source 48
    target 2068
  ]
  edge [
    source 48
    target 2069
  ]
  edge [
    source 48
    target 2070
  ]
  edge [
    source 48
    target 2071
  ]
  edge [
    source 48
    target 2072
  ]
  edge [
    source 48
    target 2073
  ]
  edge [
    source 48
    target 2074
  ]
  edge [
    source 48
    target 2075
  ]
  edge [
    source 48
    target 2076
  ]
  edge [
    source 48
    target 2077
  ]
  edge [
    source 48
    target 1766
  ]
  edge [
    source 48
    target 2078
  ]
  edge [
    source 48
    target 320
  ]
  edge [
    source 48
    target 321
  ]
  edge [
    source 48
    target 322
  ]
  edge [
    source 48
    target 323
  ]
  edge [
    source 48
    target 324
  ]
  edge [
    source 48
    target 325
  ]
  edge [
    source 48
    target 326
  ]
  edge [
    source 48
    target 327
  ]
  edge [
    source 48
    target 328
  ]
  edge [
    source 48
    target 329
  ]
  edge [
    source 48
    target 330
  ]
  edge [
    source 48
    target 331
  ]
  edge [
    source 48
    target 332
  ]
  edge [
    source 48
    target 333
  ]
  edge [
    source 48
    target 334
  ]
  edge [
    source 48
    target 335
  ]
  edge [
    source 48
    target 336
  ]
  edge [
    source 48
    target 337
  ]
  edge [
    source 48
    target 75
  ]
  edge [
    source 48
    target 338
  ]
  edge [
    source 48
    target 339
  ]
  edge [
    source 48
    target 340
  ]
  edge [
    source 48
    target 341
  ]
  edge [
    source 48
    target 342
  ]
  edge [
    source 48
    target 343
  ]
  edge [
    source 48
    target 2079
  ]
  edge [
    source 48
    target 2080
  ]
  edge [
    source 48
    target 2081
  ]
  edge [
    source 48
    target 2082
  ]
  edge [
    source 48
    target 2083
  ]
  edge [
    source 48
    target 2084
  ]
  edge [
    source 48
    target 2085
  ]
  edge [
    source 48
    target 2086
  ]
  edge [
    source 48
    target 2087
  ]
  edge [
    source 48
    target 2088
  ]
  edge [
    source 48
    target 2089
  ]
  edge [
    source 48
    target 2090
  ]
  edge [
    source 48
    target 2091
  ]
  edge [
    source 48
    target 1607
  ]
  edge [
    source 48
    target 2092
  ]
  edge [
    source 48
    target 2093
  ]
  edge [
    source 48
    target 2094
  ]
  edge [
    source 48
    target 2095
  ]
  edge [
    source 48
    target 2096
  ]
  edge [
    source 48
    target 903
  ]
  edge [
    source 48
    target 2097
  ]
  edge [
    source 48
    target 735
  ]
  edge [
    source 48
    target 2098
  ]
  edge [
    source 48
    target 2099
  ]
  edge [
    source 48
    target 1733
  ]
  edge [
    source 48
    target 805
  ]
  edge [
    source 48
    target 252
  ]
  edge [
    source 48
    target 1853
  ]
  edge [
    source 48
    target 2100
  ]
  edge [
    source 48
    target 900
  ]
  edge [
    source 48
    target 2101
  ]
  edge [
    source 48
    target 2102
  ]
  edge [
    source 48
    target 2103
  ]
  edge [
    source 48
    target 2104
  ]
  edge [
    source 48
    target 2105
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 2106
  ]
  edge [
    source 51
    target 2107
  ]
  edge [
    source 51
    target 2108
  ]
  edge [
    source 51
    target 2109
  ]
  edge [
    source 51
    target 287
  ]
  edge [
    source 51
    target 2110
  ]
  edge [
    source 51
    target 2111
  ]
  edge [
    source 51
    target 2112
  ]
  edge [
    source 51
    target 1861
  ]
  edge [
    source 51
    target 1862
  ]
  edge [
    source 51
    target 1863
  ]
  edge [
    source 51
    target 2113
  ]
  edge [
    source 51
    target 2114
  ]
  edge [
    source 51
    target 1864
  ]
  edge [
    source 51
    target 2115
  ]
  edge [
    source 51
    target 2116
  ]
  edge [
    source 51
    target 2117
  ]
  edge [
    source 51
    target 2118
  ]
  edge [
    source 51
    target 1865
  ]
  edge [
    source 51
    target 2119
  ]
  edge [
    source 51
    target 2120
  ]
  edge [
    source 51
    target 1960
  ]
  edge [
    source 51
    target 2121
  ]
  edge [
    source 51
    target 2122
  ]
  edge [
    source 51
    target 2123
  ]
  edge [
    source 51
    target 2124
  ]
  edge [
    source 51
    target 2125
  ]
  edge [
    source 51
    target 2126
  ]
  edge [
    source 51
    target 2127
  ]
  edge [
    source 51
    target 2128
  ]
  edge [
    source 51
    target 2129
  ]
  edge [
    source 51
    target 2130
  ]
  edge [
    source 51
    target 2131
  ]
  edge [
    source 51
    target 2132
  ]
  edge [
    source 51
    target 2133
  ]
  edge [
    source 51
    target 2134
  ]
  edge [
    source 51
    target 2135
  ]
  edge [
    source 51
    target 2136
  ]
  edge [
    source 51
    target 2137
  ]
  edge [
    source 51
    target 2138
  ]
  edge [
    source 51
    target 2139
  ]
  edge [
    source 51
    target 2140
  ]
  edge [
    source 51
    target 2141
  ]
  edge [
    source 51
    target 2142
  ]
  edge [
    source 51
    target 2143
  ]
  edge [
    source 51
    target 2144
  ]
  edge [
    source 51
    target 2145
  ]
  edge [
    source 51
    target 2146
  ]
  edge [
    source 51
    target 2147
  ]
  edge [
    source 51
    target 252
  ]
  edge [
    source 51
    target 2148
  ]
  edge [
    source 51
    target 2149
  ]
  edge [
    source 51
    target 2150
  ]
  edge [
    source 51
    target 2151
  ]
  edge [
    source 51
    target 2152
  ]
  edge [
    source 51
    target 1826
  ]
  edge [
    source 51
    target 2153
  ]
  edge [
    source 51
    target 2154
  ]
  edge [
    source 51
    target 2155
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1113
  ]
  edge [
    source 52
    target 2156
  ]
  edge [
    source 52
    target 2157
  ]
  edge [
    source 52
    target 2158
  ]
  edge [
    source 52
    target 2159
  ]
  edge [
    source 52
    target 2160
  ]
  edge [
    source 52
    target 1187
  ]
  edge [
    source 52
    target 2161
  ]
  edge [
    source 52
    target 829
  ]
  edge [
    source 52
    target 2162
  ]
  edge [
    source 52
    target 2163
  ]
  edge [
    source 52
    target 2164
  ]
  edge [
    source 52
    target 2165
  ]
  edge [
    source 52
    target 2166
  ]
  edge [
    source 52
    target 2167
  ]
  edge [
    source 52
    target 2168
  ]
  edge [
    source 52
    target 2169
  ]
  edge [
    source 52
    target 2170
  ]
  edge [
    source 52
    target 2139
  ]
  edge [
    source 52
    target 2141
  ]
  edge [
    source 52
    target 2145
  ]
  edge [
    source 52
    target 2171
  ]
  edge [
    source 52
    target 2172
  ]
  edge [
    source 52
    target 1250
  ]
  edge [
    source 52
    target 2173
  ]
  edge [
    source 52
    target 2174
  ]
  edge [
    source 52
    target 2175
  ]
  edge [
    source 52
    target 2137
  ]
  edge [
    source 52
    target 2176
  ]
  edge [
    source 52
    target 987
  ]
  edge [
    source 52
    target 793
  ]
  edge [
    source 52
    target 1467
  ]
  edge [
    source 52
    target 2177
  ]
  edge [
    source 52
    target 2178
  ]
  edge [
    source 52
    target 1458
  ]
  edge [
    source 52
    target 2179
  ]
  edge [
    source 52
    target 1341
  ]
  edge [
    source 52
    target 1618
  ]
  edge [
    source 52
    target 2180
  ]
  edge [
    source 52
    target 1483
  ]
  edge [
    source 52
    target 1462
  ]
  edge [
    source 52
    target 2181
  ]
  edge [
    source 52
    target 2182
  ]
  edge [
    source 52
    target 2183
  ]
  edge [
    source 52
    target 2184
  ]
  edge [
    source 52
    target 2185
  ]
  edge [
    source 52
    target 2186
  ]
  edge [
    source 52
    target 2187
  ]
  edge [
    source 52
    target 2188
  ]
  edge [
    source 52
    target 2189
  ]
  edge [
    source 52
    target 2190
  ]
  edge [
    source 52
    target 2191
  ]
  edge [
    source 52
    target 2192
  ]
  edge [
    source 52
    target 2193
  ]
  edge [
    source 52
    target 2194
  ]
  edge [
    source 52
    target 1628
  ]
  edge [
    source 52
    target 2195
  ]
  edge [
    source 52
    target 2196
  ]
  edge [
    source 52
    target 486
  ]
  edge [
    source 52
    target 2197
  ]
  edge [
    source 52
    target 620
  ]
  edge [
    source 52
    target 2198
  ]
  edge [
    source 52
    target 2199
  ]
  edge [
    source 52
    target 756
  ]
  edge [
    source 52
    target 2200
  ]
  edge [
    source 52
    target 2201
  ]
  edge [
    source 52
    target 2202
  ]
  edge [
    source 52
    target 2203
  ]
  edge [
    source 52
    target 2204
  ]
  edge [
    source 52
    target 2205
  ]
  edge [
    source 52
    target 2206
  ]
  edge [
    source 52
    target 2207
  ]
  edge [
    source 52
    target 2208
  ]
  edge [
    source 52
    target 543
  ]
  edge [
    source 52
    target 2209
  ]
  edge [
    source 52
    target 2210
  ]
  edge [
    source 52
    target 2211
  ]
  edge [
    source 52
    target 2212
  ]
  edge [
    source 52
    target 694
  ]
  edge [
    source 52
    target 2213
  ]
  edge [
    source 52
    target 2214
  ]
  edge [
    source 52
    target 2215
  ]
  edge [
    source 52
    target 1812
  ]
  edge [
    source 52
    target 2216
  ]
  edge [
    source 52
    target 2217
  ]
  edge [
    source 52
    target 2218
  ]
  edge [
    source 52
    target 529
  ]
  edge [
    source 52
    target 639
  ]
  edge [
    source 52
    target 1560
  ]
  edge [
    source 52
    target 711
  ]
  edge [
    source 52
    target 2219
  ]
  edge [
    source 52
    target 2220
  ]
  edge [
    source 52
    target 2221
  ]
  edge [
    source 52
    target 2222
  ]
  edge [
    source 52
    target 2223
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 2224
  ]
  edge [
    source 53
    target 2225
  ]
  edge [
    source 53
    target 912
  ]
  edge [
    source 53
    target 2226
  ]
  edge [
    source 53
    target 2227
  ]
  edge [
    source 53
    target 1039
  ]
  edge [
    source 53
    target 2228
  ]
  edge [
    source 53
    target 2229
  ]
  edge [
    source 53
    target 2230
  ]
  edge [
    source 53
    target 1045
  ]
  edge [
    source 53
    target 2231
  ]
  edge [
    source 53
    target 216
  ]
  edge [
    source 53
    target 2232
  ]
  edge [
    source 53
    target 2233
  ]
  edge [
    source 53
    target 2234
  ]
  edge [
    source 53
    target 2235
  ]
  edge [
    source 53
    target 2236
  ]
  edge [
    source 53
    target 1044
  ]
  edge [
    source 53
    target 2237
  ]
  edge [
    source 53
    target 1311
  ]
  edge [
    source 53
    target 2238
  ]
  edge [
    source 53
    target 1102
  ]
  edge [
    source 53
    target 214
  ]
  edge [
    source 53
    target 67
  ]
  edge [
    source 53
    target 2239
  ]
  edge [
    source 53
    target 2240
  ]
  edge [
    source 53
    target 2241
  ]
  edge [
    source 53
    target 161
  ]
  edge [
    source 53
    target 805
  ]
  edge [
    source 53
    target 76
  ]
  edge [
    source 53
    target 2242
  ]
  edge [
    source 53
    target 1175
  ]
  edge [
    source 53
    target 2243
  ]
  edge [
    source 53
    target 2244
  ]
  edge [
    source 53
    target 2245
  ]
  edge [
    source 53
    target 2246
  ]
  edge [
    source 53
    target 762
  ]
  edge [
    source 53
    target 238
  ]
  edge [
    source 53
    target 2247
  ]
  edge [
    source 53
    target 2248
  ]
  edge [
    source 53
    target 2249
  ]
  edge [
    source 53
    target 2250
  ]
  edge [
    source 53
    target 2251
  ]
  edge [
    source 53
    target 628
  ]
  edge [
    source 53
    target 273
  ]
  edge [
    source 53
    target 2252
  ]
  edge [
    source 53
    target 2253
  ]
  edge [
    source 53
    target 2254
  ]
  edge [
    source 53
    target 2255
  ]
  edge [
    source 53
    target 2256
  ]
  edge [
    source 53
    target 2257
  ]
  edge [
    source 53
    target 2258
  ]
  edge [
    source 53
    target 2259
  ]
  edge [
    source 53
    target 2260
  ]
  edge [
    source 53
    target 2261
  ]
  edge [
    source 53
    target 2262
  ]
  edge [
    source 53
    target 2263
  ]
  edge [
    source 53
    target 2264
  ]
  edge [
    source 53
    target 2265
  ]
  edge [
    source 53
    target 2266
  ]
  edge [
    source 53
    target 2267
  ]
  edge [
    source 53
    target 2268
  ]
  edge [
    source 53
    target 2269
  ]
  edge [
    source 53
    target 2270
  ]
  edge [
    source 53
    target 2271
  ]
  edge [
    source 53
    target 2272
  ]
  edge [
    source 53
    target 2273
  ]
  edge [
    source 53
    target 2274
  ]
  edge [
    source 53
    target 2275
  ]
  edge [
    source 53
    target 2276
  ]
  edge [
    source 53
    target 2277
  ]
  edge [
    source 53
    target 2278
  ]
  edge [
    source 53
    target 2279
  ]
  edge [
    source 53
    target 2280
  ]
  edge [
    source 53
    target 2281
  ]
  edge [
    source 53
    target 2282
  ]
  edge [
    source 53
    target 2283
  ]
  edge [
    source 53
    target 2284
  ]
  edge [
    source 53
    target 2285
  ]
  edge [
    source 53
    target 2286
  ]
  edge [
    source 53
    target 2287
  ]
  edge [
    source 53
    target 2288
  ]
  edge [
    source 53
    target 2289
  ]
  edge [
    source 53
    target 2290
  ]
  edge [
    source 53
    target 2291
  ]
  edge [
    source 53
    target 2292
  ]
  edge [
    source 53
    target 2293
  ]
  edge [
    source 53
    target 2294
  ]
  edge [
    source 53
    target 2295
  ]
  edge [
    source 53
    target 2296
  ]
  edge [
    source 53
    target 2297
  ]
  edge [
    source 53
    target 2298
  ]
  edge [
    source 53
    target 2299
  ]
  edge [
    source 53
    target 2300
  ]
  edge [
    source 53
    target 2301
  ]
  edge [
    source 53
    target 2302
  ]
  edge [
    source 53
    target 2303
  ]
  edge [
    source 53
    target 2304
  ]
  edge [
    source 53
    target 2305
  ]
  edge [
    source 53
    target 2306
  ]
  edge [
    source 53
    target 2307
  ]
  edge [
    source 53
    target 2308
  ]
  edge [
    source 53
    target 2309
  ]
  edge [
    source 53
    target 2310
  ]
  edge [
    source 53
    target 2311
  ]
  edge [
    source 53
    target 2312
  ]
  edge [
    source 53
    target 2313
  ]
  edge [
    source 53
    target 2314
  ]
  edge [
    source 53
    target 2315
  ]
  edge [
    source 53
    target 2316
  ]
  edge [
    source 53
    target 2317
  ]
  edge [
    source 53
    target 2318
  ]
  edge [
    source 53
    target 2319
  ]
  edge [
    source 53
    target 2320
  ]
  edge [
    source 53
    target 2321
  ]
  edge [
    source 53
    target 2322
  ]
  edge [
    source 53
    target 2323
  ]
  edge [
    source 53
    target 2324
  ]
  edge [
    source 53
    target 2325
  ]
  edge [
    source 53
    target 2326
  ]
  edge [
    source 53
    target 2327
  ]
  edge [
    source 53
    target 2328
  ]
  edge [
    source 53
    target 2329
  ]
  edge [
    source 53
    target 2330
  ]
  edge [
    source 53
    target 2331
  ]
  edge [
    source 53
    target 2332
  ]
  edge [
    source 53
    target 2333
  ]
  edge [
    source 53
    target 2334
  ]
  edge [
    source 53
    target 2335
  ]
  edge [
    source 53
    target 2336
  ]
  edge [
    source 53
    target 2337
  ]
  edge [
    source 53
    target 2338
  ]
  edge [
    source 53
    target 2339
  ]
  edge [
    source 53
    target 2340
  ]
  edge [
    source 53
    target 2341
  ]
  edge [
    source 53
    target 2342
  ]
  edge [
    source 53
    target 2343
  ]
  edge [
    source 53
    target 2344
  ]
  edge [
    source 53
    target 2345
  ]
  edge [
    source 53
    target 2346
  ]
  edge [
    source 53
    target 2347
  ]
  edge [
    source 53
    target 2348
  ]
  edge [
    source 53
    target 2349
  ]
  edge [
    source 53
    target 2350
  ]
  edge [
    source 53
    target 2351
  ]
  edge [
    source 53
    target 2352
  ]
  edge [
    source 53
    target 2353
  ]
  edge [
    source 53
    target 2354
  ]
  edge [
    source 53
    target 2355
  ]
  edge [
    source 53
    target 2356
  ]
  edge [
    source 53
    target 2357
  ]
  edge [
    source 53
    target 2358
  ]
  edge [
    source 53
    target 2359
  ]
  edge [
    source 53
    target 2360
  ]
  edge [
    source 53
    target 2361
  ]
  edge [
    source 53
    target 147
  ]
  edge [
    source 53
    target 2362
  ]
  edge [
    source 53
    target 2363
  ]
  edge [
    source 53
    target 2364
  ]
  edge [
    source 53
    target 2365
  ]
  edge [
    source 53
    target 2366
  ]
  edge [
    source 53
    target 2367
  ]
  edge [
    source 53
    target 2368
  ]
  edge [
    source 53
    target 2369
  ]
  edge [
    source 53
    target 2370
  ]
  edge [
    source 53
    target 2371
  ]
  edge [
    source 53
    target 2372
  ]
  edge [
    source 53
    target 2373
  ]
  edge [
    source 53
    target 2374
  ]
  edge [
    source 53
    target 2375
  ]
  edge [
    source 53
    target 2376
  ]
  edge [
    source 53
    target 2377
  ]
  edge [
    source 53
    target 2378
  ]
  edge [
    source 53
    target 2379
  ]
  edge [
    source 53
    target 2380
  ]
  edge [
    source 53
    target 2381
  ]
  edge [
    source 53
    target 2382
  ]
  edge [
    source 53
    target 2383
  ]
  edge [
    source 53
    target 2384
  ]
  edge [
    source 53
    target 2385
  ]
  edge [
    source 53
    target 2386
  ]
  edge [
    source 53
    target 2387
  ]
  edge [
    source 53
    target 2388
  ]
  edge [
    source 53
    target 2389
  ]
  edge [
    source 53
    target 2390
  ]
  edge [
    source 53
    target 2046
  ]
  edge [
    source 53
    target 1871
  ]
  edge [
    source 53
    target 2391
  ]
  edge [
    source 53
    target 2392
  ]
  edge [
    source 53
    target 2393
  ]
  edge [
    source 53
    target 2394
  ]
  edge [
    source 53
    target 2395
  ]
  edge [
    source 53
    target 2396
  ]
  edge [
    source 53
    target 2397
  ]
  edge [
    source 53
    target 2398
  ]
  edge [
    source 53
    target 2399
  ]
  edge [
    source 53
    target 2400
  ]
  edge [
    source 53
    target 2401
  ]
  edge [
    source 53
    target 2402
  ]
  edge [
    source 53
    target 2403
  ]
  edge [
    source 53
    target 2404
  ]
  edge [
    source 53
    target 2405
  ]
  edge [
    source 53
    target 2406
  ]
  edge [
    source 53
    target 2407
  ]
  edge [
    source 53
    target 2408
  ]
  edge [
    source 53
    target 2409
  ]
  edge [
    source 53
    target 2410
  ]
  edge [
    source 53
    target 2411
  ]
  edge [
    source 53
    target 2412
  ]
  edge [
    source 53
    target 2413
  ]
  edge [
    source 53
    target 2414
  ]
  edge [
    source 53
    target 2415
  ]
  edge [
    source 53
    target 2416
  ]
  edge [
    source 53
    target 2417
  ]
  edge [
    source 53
    target 2418
  ]
  edge [
    source 53
    target 2419
  ]
  edge [
    source 53
    target 2420
  ]
  edge [
    source 53
    target 2421
  ]
  edge [
    source 53
    target 2422
  ]
  edge [
    source 53
    target 2423
  ]
  edge [
    source 53
    target 2424
  ]
  edge [
    source 53
    target 2425
  ]
  edge [
    source 53
    target 2426
  ]
  edge [
    source 53
    target 2427
  ]
  edge [
    source 53
    target 2428
  ]
  edge [
    source 53
    target 2429
  ]
  edge [
    source 53
    target 2430
  ]
  edge [
    source 53
    target 2431
  ]
  edge [
    source 53
    target 2432
  ]
  edge [
    source 53
    target 2433
  ]
  edge [
    source 53
    target 2434
  ]
  edge [
    source 53
    target 2435
  ]
  edge [
    source 53
    target 2436
  ]
  edge [
    source 53
    target 2437
  ]
  edge [
    source 53
    target 2438
  ]
  edge [
    source 53
    target 2439
  ]
  edge [
    source 53
    target 2440
  ]
  edge [
    source 53
    target 2441
  ]
  edge [
    source 53
    target 2442
  ]
  edge [
    source 53
    target 2443
  ]
  edge [
    source 53
    target 2444
  ]
  edge [
    source 53
    target 2445
  ]
  edge [
    source 53
    target 2446
  ]
  edge [
    source 53
    target 2447
  ]
  edge [
    source 53
    target 2448
  ]
  edge [
    source 53
    target 1870
  ]
  edge [
    source 53
    target 2449
  ]
  edge [
    source 53
    target 2450
  ]
  edge [
    source 53
    target 2451
  ]
  edge [
    source 53
    target 2452
  ]
  edge [
    source 53
    target 2453
  ]
  edge [
    source 53
    target 2454
  ]
  edge [
    source 53
    target 2455
  ]
  edge [
    source 53
    target 2456
  ]
  edge [
    source 53
    target 2457
  ]
  edge [
    source 53
    target 2458
  ]
  edge [
    source 53
    target 2459
  ]
  edge [
    source 53
    target 2460
  ]
  edge [
    source 53
    target 2461
  ]
  edge [
    source 53
    target 2462
  ]
  edge [
    source 53
    target 1567
  ]
  edge [
    source 53
    target 2463
  ]
  edge [
    source 53
    target 2464
  ]
  edge [
    source 53
    target 2465
  ]
  edge [
    source 53
    target 2466
  ]
  edge [
    source 53
    target 2467
  ]
  edge [
    source 53
    target 2468
  ]
  edge [
    source 53
    target 2469
  ]
  edge [
    source 53
    target 2470
  ]
  edge [
    source 53
    target 2471
  ]
  edge [
    source 53
    target 2472
  ]
  edge [
    source 53
    target 2473
  ]
  edge [
    source 53
    target 2474
  ]
  edge [
    source 53
    target 2475
  ]
  edge [
    source 53
    target 2476
  ]
  edge [
    source 53
    target 792
  ]
  edge [
    source 53
    target 2477
  ]
  edge [
    source 53
    target 2478
  ]
  edge [
    source 53
    target 2479
  ]
  edge [
    source 53
    target 2480
  ]
  edge [
    source 53
    target 2481
  ]
  edge [
    source 53
    target 2482
  ]
  edge [
    source 53
    target 804
  ]
  edge [
    source 53
    target 2483
  ]
  edge [
    source 53
    target 2484
  ]
  edge [
    source 53
    target 2485
  ]
  edge [
    source 53
    target 566
  ]
  edge [
    source 53
    target 2486
  ]
  edge [
    source 53
    target 2487
  ]
  edge [
    source 53
    target 1324
  ]
  edge [
    source 53
    target 2488
  ]
  edge [
    source 53
    target 2489
  ]
  edge [
    source 53
    target 529
  ]
  edge [
    source 53
    target 354
  ]
  edge [
    source 53
    target 2490
  ]
  edge [
    source 53
    target 2094
  ]
  edge [
    source 53
    target 2491
  ]
  edge [
    source 53
    target 2492
  ]
  edge [
    source 53
    target 2178
  ]
  edge [
    source 53
    target 2493
  ]
  edge [
    source 53
    target 2494
  ]
  edge [
    source 53
    target 1900
  ]
  edge [
    source 53
    target 2152
  ]
  edge [
    source 53
    target 2495
  ]
  edge [
    source 53
    target 1746
  ]
  edge [
    source 53
    target 2496
  ]
  edge [
    source 53
    target 2497
  ]
  edge [
    source 53
    target 2498
  ]
  edge [
    source 53
    target 2499
  ]
  edge [
    source 53
    target 195
  ]
  edge [
    source 53
    target 436
  ]
  edge [
    source 53
    target 2500
  ]
  edge [
    source 53
    target 2501
  ]
  edge [
    source 53
    target 2502
  ]
  edge [
    source 53
    target 2503
  ]
  edge [
    source 53
    target 946
  ]
  edge [
    source 53
    target 2504
  ]
  edge [
    source 53
    target 2505
  ]
  edge [
    source 53
    target 2506
  ]
  edge [
    source 53
    target 2507
  ]
  edge [
    source 53
    target 2508
  ]
  edge [
    source 53
    target 2509
  ]
  edge [
    source 53
    target 2014
  ]
  edge [
    source 53
    target 925
  ]
  edge [
    source 53
    target 2510
  ]
  edge [
    source 53
    target 2511
  ]
  edge [
    source 53
    target 2512
  ]
  edge [
    source 53
    target 2513
  ]
  edge [
    source 53
    target 2514
  ]
  edge [
    source 53
    target 2515
  ]
  edge [
    source 53
    target 1337
  ]
  edge [
    source 53
    target 2516
  ]
  edge [
    source 53
    target 2517
  ]
  edge [
    source 53
    target 1322
  ]
  edge [
    source 53
    target 2518
  ]
  edge [
    source 53
    target 2519
  ]
  edge [
    source 53
    target 2520
  ]
  edge [
    source 53
    target 2521
  ]
  edge [
    source 53
    target 2522
  ]
  edge [
    source 53
    target 2523
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 2524
  ]
  edge [
    source 54
    target 2525
  ]
  edge [
    source 54
    target 2526
  ]
  edge [
    source 54
    target 2527
  ]
  edge [
    source 54
    target 2528
  ]
  edge [
    source 54
    target 2529
  ]
  edge [
    source 54
    target 2530
  ]
  edge [
    source 54
    target 2531
  ]
  edge [
    source 54
    target 2532
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 1678
  ]
  edge [
    source 55
    target 2533
  ]
  edge [
    source 55
    target 2534
  ]
  edge [
    source 55
    target 2535
  ]
  edge [
    source 55
    target 2536
  ]
  edge [
    source 55
    target 1618
  ]
  edge [
    source 55
    target 2537
  ]
  edge [
    source 55
    target 2028
  ]
  edge [
    source 55
    target 2538
  ]
  edge [
    source 55
    target 1489
  ]
  edge [
    source 55
    target 2539
  ]
  edge [
    source 55
    target 2540
  ]
  edge [
    source 55
    target 2541
  ]
  edge [
    source 55
    target 2542
  ]
  edge [
    source 55
    target 2543
  ]
  edge [
    source 55
    target 2544
  ]
  edge [
    source 55
    target 2545
  ]
  edge [
    source 55
    target 2546
  ]
  edge [
    source 55
    target 1486
  ]
  edge [
    source 55
    target 2099
  ]
  edge [
    source 55
    target 252
  ]
  edge [
    source 55
    target 1418
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 1778
  ]
  edge [
    source 57
    target 2547
  ]
  edge [
    source 57
    target 2548
  ]
  edge [
    source 57
    target 2549
  ]
  edge [
    source 57
    target 1784
  ]
  edge [
    source 57
    target 420
  ]
  edge [
    source 57
    target 1997
  ]
  edge [
    source 57
    target 2550
  ]
  edge [
    source 57
    target 2551
  ]
  edge [
    source 57
    target 2552
  ]
  edge [
    source 57
    target 2553
  ]
  edge [
    source 57
    target 2554
  ]
  edge [
    source 57
    target 2555
  ]
  edge [
    source 57
    target 2556
  ]
  edge [
    source 57
    target 2557
  ]
  edge [
    source 57
    target 2558
  ]
  edge [
    source 57
    target 804
  ]
  edge [
    source 57
    target 2559
  ]
  edge [
    source 57
    target 2560
  ]
  edge [
    source 57
    target 2561
  ]
  edge [
    source 57
    target 2562
  ]
  edge [
    source 57
    target 2563
  ]
  edge [
    source 57
    target 402
  ]
  edge [
    source 57
    target 2564
  ]
  edge [
    source 57
    target 133
  ]
  edge [
    source 57
    target 2565
  ]
  edge [
    source 57
    target 1395
  ]
  edge [
    source 57
    target 141
  ]
  edge [
    source 57
    target 397
  ]
  edge [
    source 57
    target 2566
  ]
  edge [
    source 57
    target 2567
  ]
  edge [
    source 57
    target 2568
  ]
  edge [
    source 57
    target 2569
  ]
  edge [
    source 57
    target 2570
  ]
  edge [
    source 57
    target 132
  ]
  edge [
    source 57
    target 1408
  ]
  edge [
    source 57
    target 2571
  ]
  edge [
    source 57
    target 1013
  ]
  edge [
    source 57
    target 2572
  ]
  edge [
    source 57
    target 2573
  ]
  edge [
    source 57
    target 2574
  ]
  edge [
    source 57
    target 372
  ]
  edge [
    source 57
    target 302
  ]
  edge [
    source 57
    target 2575
  ]
  edge [
    source 57
    target 2576
  ]
  edge [
    source 57
    target 2577
  ]
  edge [
    source 58
    target 149
  ]
  edge [
    source 58
    target 594
  ]
  edge [
    source 58
    target 2578
  ]
  edge [
    source 58
    target 2579
  ]
  edge [
    source 58
    target 67
  ]
  edge [
    source 58
    target 2580
  ]
  edge [
    source 58
    target 2581
  ]
  edge [
    source 58
    target 2582
  ]
  edge [
    source 58
    target 2583
  ]
  edge [
    source 58
    target 2584
  ]
  edge [
    source 58
    target 692
  ]
  edge [
    source 58
    target 199
  ]
  edge [
    source 58
    target 2585
  ]
  edge [
    source 58
    target 2586
  ]
  edge [
    source 58
    target 2587
  ]
  edge [
    source 58
    target 2588
  ]
  edge [
    source 58
    target 2589
  ]
  edge [
    source 58
    target 1341
  ]
  edge [
    source 58
    target 2590
  ]
  edge [
    source 58
    target 2591
  ]
  edge [
    source 58
    target 2592
  ]
  edge [
    source 58
    target 2593
  ]
  edge [
    source 58
    target 485
  ]
  edge [
    source 58
    target 2594
  ]
  edge [
    source 58
    target 105
  ]
  edge [
    source 58
    target 2595
  ]
  edge [
    source 58
    target 2596
  ]
  edge [
    source 58
    target 2597
  ]
  edge [
    source 58
    target 2598
  ]
  edge [
    source 58
    target 915
  ]
  edge [
    source 58
    target 167
  ]
  edge [
    source 58
    target 168
  ]
  edge [
    source 58
    target 169
  ]
  edge [
    source 58
    target 170
  ]
  edge [
    source 58
    target 2599
  ]
  edge [
    source 58
    target 255
  ]
  edge [
    source 58
    target 808
  ]
  edge [
    source 58
    target 695
  ]
  edge [
    source 58
    target 2600
  ]
  edge [
    source 58
    target 2601
  ]
  edge [
    source 58
    target 2602
  ]
  edge [
    source 58
    target 216
  ]
  edge [
    source 58
    target 2603
  ]
  edge [
    source 58
    target 2604
  ]
  edge [
    source 58
    target 2605
  ]
  edge [
    source 58
    target 1421
  ]
  edge [
    source 58
    target 273
  ]
  edge [
    source 58
    target 2067
  ]
  edge [
    source 58
    target 2606
  ]
  edge [
    source 58
    target 2607
  ]
  edge [
    source 58
    target 2608
  ]
  edge [
    source 58
    target 2609
  ]
  edge [
    source 58
    target 2610
  ]
  edge [
    source 58
    target 2611
  ]
  edge [
    source 58
    target 2612
  ]
  edge [
    source 58
    target 813
  ]
  edge [
    source 58
    target 2613
  ]
  edge [
    source 58
    target 912
  ]
  edge [
    source 58
    target 2614
  ]
  edge [
    source 58
    target 2179
  ]
  edge [
    source 58
    target 2615
  ]
  edge [
    source 58
    target 606
  ]
  edge [
    source 58
    target 2616
  ]
  edge [
    source 58
    target 2617
  ]
  edge [
    source 58
    target 987
  ]
  edge [
    source 58
    target 2181
  ]
  edge [
    source 58
    target 2618
  ]
  edge [
    source 58
    target 2180
  ]
  edge [
    source 58
    target 2619
  ]
  edge [
    source 58
    target 792
  ]
  edge [
    source 58
    target 466
  ]
  edge [
    source 58
    target 2620
  ]
  edge [
    source 58
    target 2621
  ]
  edge [
    source 58
    target 1674
  ]
  edge [
    source 58
    target 182
  ]
  edge [
    source 58
    target 1618
  ]
  edge [
    source 58
    target 766
  ]
  edge [
    source 58
    target 2622
  ]
  edge [
    source 58
    target 323
  ]
  edge [
    source 58
    target 2623
  ]
  edge [
    source 58
    target 2624
  ]
  edge [
    source 58
    target 2625
  ]
  edge [
    source 58
    target 2626
  ]
  edge [
    source 58
    target 2627
  ]
  edge [
    source 58
    target 1673
  ]
  edge [
    source 58
    target 2628
  ]
  edge [
    source 58
    target 1675
  ]
  edge [
    source 58
    target 2186
  ]
  edge [
    source 58
    target 2629
  ]
  edge [
    source 58
    target 2034
  ]
  edge [
    source 58
    target 527
  ]
  edge [
    source 58
    target 1680
  ]
  edge [
    source 58
    target 332
  ]
  edge [
    source 58
    target 1670
  ]
  edge [
    source 58
    target 194
  ]
  edge [
    source 58
    target 2630
  ]
  edge [
    source 58
    target 1671
  ]
  edge [
    source 58
    target 1672
  ]
  edge [
    source 58
    target 2631
  ]
  edge [
    source 58
    target 214
  ]
  edge [
    source 58
    target 2632
  ]
  edge [
    source 58
    target 2633
  ]
  edge [
    source 58
    target 949
  ]
  edge [
    source 58
    target 1678
  ]
  edge [
    source 58
    target 160
  ]
  edge [
    source 58
    target 523
  ]
  edge [
    source 58
    target 746
  ]
  edge [
    source 58
    target 341
  ]
  edge [
    source 58
    target 685
  ]
  edge [
    source 58
    target 2634
  ]
  edge [
    source 58
    target 779
  ]
  edge [
    source 58
    target 252
  ]
  edge [
    source 58
    target 1679
  ]
  edge [
    source 58
    target 2635
  ]
  edge [
    source 58
    target 2636
  ]
  edge [
    source 58
    target 2637
  ]
  edge [
    source 59
    target 67
  ]
  edge [
    source 59
    target 68
  ]
  edge [
    source 59
    target 1601
  ]
  edge [
    source 59
    target 2638
  ]
  edge [
    source 59
    target 2639
  ]
  edge [
    source 59
    target 2640
  ]
  edge [
    source 59
    target 76
  ]
  edge [
    source 59
    target 2641
  ]
  edge [
    source 59
    target 1090
  ]
  edge [
    source 59
    target 2642
  ]
  edge [
    source 59
    target 2643
  ]
  edge [
    source 59
    target 1263
  ]
  edge [
    source 59
    target 2644
  ]
  edge [
    source 59
    target 1549
  ]
  edge [
    source 59
    target 2645
  ]
  edge [
    source 59
    target 2646
  ]
  edge [
    source 59
    target 1266
  ]
  edge [
    source 59
    target 2647
  ]
  edge [
    source 59
    target 2648
  ]
  edge [
    source 59
    target 2649
  ]
  edge [
    source 59
    target 2650
  ]
  edge [
    source 59
    target 2651
  ]
  edge [
    source 59
    target 1264
  ]
  edge [
    source 59
    target 2652
  ]
  edge [
    source 59
    target 2653
  ]
  edge [
    source 59
    target 2654
  ]
  edge [
    source 59
    target 608
  ]
  edge [
    source 59
    target 2655
  ]
  edge [
    source 59
    target 2656
  ]
  edge [
    source 59
    target 2217
  ]
  edge [
    source 59
    target 738
  ]
  edge [
    source 59
    target 2657
  ]
  edge [
    source 59
    target 2658
  ]
  edge [
    source 59
    target 2659
  ]
  edge [
    source 59
    target 2660
  ]
  edge [
    source 59
    target 2661
  ]
  edge [
    source 59
    target 2662
  ]
  edge [
    source 59
    target 2663
  ]
  edge [
    source 59
    target 2664
  ]
  edge [
    source 59
    target 2665
  ]
  edge [
    source 59
    target 2666
  ]
  edge [
    source 59
    target 2667
  ]
  edge [
    source 59
    target 2668
  ]
  edge [
    source 59
    target 2669
  ]
  edge [
    source 59
    target 2670
  ]
  edge [
    source 59
    target 2671
  ]
  edge [
    source 59
    target 2672
  ]
  edge [
    source 59
    target 2673
  ]
  edge [
    source 59
    target 2674
  ]
  edge [
    source 59
    target 1801
  ]
  edge [
    source 59
    target 2675
  ]
  edge [
    source 59
    target 1873
  ]
  edge [
    source 59
    target 2676
  ]
  edge [
    source 59
    target 2677
  ]
  edge [
    source 59
    target 1876
  ]
  edge [
    source 59
    target 2678
  ]
  edge [
    source 59
    target 1877
  ]
  edge [
    source 59
    target 2679
  ]
  edge [
    source 59
    target 1879
  ]
  edge [
    source 59
    target 1881
  ]
  edge [
    source 59
    target 1882
  ]
  edge [
    source 59
    target 912
  ]
  edge [
    source 59
    target 2680
  ]
  edge [
    source 59
    target 2681
  ]
  edge [
    source 59
    target 1540
  ]
  edge [
    source 59
    target 2682
  ]
  edge [
    source 59
    target 2683
  ]
  edge [
    source 59
    target 2684
  ]
  edge [
    source 59
    target 216
  ]
  edge [
    source 59
    target 1883
  ]
  edge [
    source 59
    target 2685
  ]
  edge [
    source 59
    target 1885
  ]
  edge [
    source 59
    target 1888
  ]
  edge [
    source 59
    target 794
  ]
  edge [
    source 59
    target 1889
  ]
  edge [
    source 59
    target 2686
  ]
  edge [
    source 59
    target 2687
  ]
  edge [
    source 59
    target 1890
  ]
  edge [
    source 59
    target 2688
  ]
  edge [
    source 59
    target 2689
  ]
  edge [
    source 59
    target 2690
  ]
  edge [
    source 59
    target 1891
  ]
  edge [
    source 59
    target 1892
  ]
  edge [
    source 59
    target 1893
  ]
  edge [
    source 59
    target 1895
  ]
  edge [
    source 59
    target 1657
  ]
  edge [
    source 59
    target 1896
  ]
  edge [
    source 59
    target 1898
  ]
  edge [
    source 59
    target 1899
  ]
  edge [
    source 59
    target 2691
  ]
  edge [
    source 59
    target 1900
  ]
  edge [
    source 59
    target 1901
  ]
  edge [
    source 59
    target 2692
  ]
  edge [
    source 59
    target 1903
  ]
  edge [
    source 59
    target 2693
  ]
  edge [
    source 59
    target 2694
  ]
  edge [
    source 59
    target 2695
  ]
  edge [
    source 59
    target 2696
  ]
  edge [
    source 59
    target 1904
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 2697
  ]
  edge [
    source 60
    target 2698
  ]
  edge [
    source 60
    target 2699
  ]
  edge [
    source 60
    target 2700
  ]
  edge [
    source 60
    target 2701
  ]
  edge [
    source 60
    target 2702
  ]
  edge [
    source 60
    target 1986
  ]
  edge [
    source 60
    target 2703
  ]
  edge [
    source 60
    target 2704
  ]
  edge [
    source 60
    target 2705
  ]
  edge [
    source 60
    target 2706
  ]
  edge [
    source 60
    target 2707
  ]
  edge [
    source 60
    target 2708
  ]
  edge [
    source 60
    target 2709
  ]
  edge [
    source 60
    target 2710
  ]
  edge [
    source 60
    target 1399
  ]
  edge [
    source 60
    target 2711
  ]
  edge [
    source 60
    target 2712
  ]
  edge [
    source 60
    target 2713
  ]
  edge [
    source 60
    target 1391
  ]
  edge [
    source 60
    target 2714
  ]
  edge [
    source 60
    target 352
  ]
  edge [
    source 60
    target 2715
  ]
  edge [
    source 60
    target 2716
  ]
  edge [
    source 60
    target 2717
  ]
  edge [
    source 60
    target 406
  ]
  edge [
    source 60
    target 2718
  ]
  edge [
    source 60
    target 2719
  ]
  edge [
    source 60
    target 2720
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 2721
  ]
  edge [
    source 61
    target 2722
  ]
  edge [
    source 61
    target 2723
  ]
  edge [
    source 61
    target 991
  ]
  edge [
    source 61
    target 1734
  ]
  edge [
    source 61
    target 804
  ]
  edge [
    source 61
    target 2724
  ]
  edge [
    source 61
    target 2725
  ]
  edge [
    source 61
    target 214
  ]
  edge [
    source 61
    target 466
  ]
  edge [
    source 61
    target 746
  ]
  edge [
    source 61
    target 2726
  ]
  edge [
    source 61
    target 2727
  ]
  edge [
    source 61
    target 2728
  ]
  edge [
    source 61
    target 2729
  ]
  edge [
    source 61
    target 2730
  ]
  edge [
    source 61
    target 2731
  ]
  edge [
    source 61
    target 1747
  ]
  edge [
    source 61
    target 213
  ]
  edge [
    source 61
    target 1540
  ]
  edge [
    source 61
    target 1541
  ]
  edge [
    source 61
    target 1542
  ]
  edge [
    source 61
    target 1543
  ]
  edge [
    source 61
    target 1544
  ]
  edge [
    source 61
    target 229
  ]
  edge [
    source 61
    target 420
  ]
  edge [
    source 61
    target 1545
  ]
  edge [
    source 61
    target 2732
  ]
  edge [
    source 61
    target 2733
  ]
  edge [
    source 61
    target 2060
  ]
  edge [
    source 61
    target 2734
  ]
  edge [
    source 61
    target 681
  ]
  edge [
    source 61
    target 2056
  ]
  edge [
    source 61
    target 2735
  ]
  edge [
    source 61
    target 2736
  ]
  edge [
    source 61
    target 2737
  ]
  edge [
    source 61
    target 2051
  ]
  edge [
    source 61
    target 2059
  ]
  edge [
    source 61
    target 2078
  ]
  edge [
    source 61
    target 2738
  ]
  edge [
    source 61
    target 1758
  ]
  edge [
    source 61
    target 2739
  ]
  edge [
    source 61
    target 2589
  ]
  edge [
    source 61
    target 2740
  ]
  edge [
    source 61
    target 2741
  ]
  edge [
    source 61
    target 2742
  ]
  edge [
    source 61
    target 2743
  ]
  edge [
    source 61
    target 2744
  ]
  edge [
    source 61
    target 2745
  ]
  edge [
    source 61
    target 2746
  ]
  edge [
    source 61
    target 2747
  ]
  edge [
    source 61
    target 2748
  ]
  edge [
    source 61
    target 1727
  ]
  edge [
    source 61
    target 564
  ]
  edge [
    source 61
    target 2749
  ]
  edge [
    source 61
    target 628
  ]
  edge [
    source 61
    target 2750
  ]
  edge [
    source 61
    target 2751
  ]
  edge [
    source 61
    target 2752
  ]
  edge [
    source 61
    target 2753
  ]
  edge [
    source 61
    target 2754
  ]
  edge [
    source 61
    target 77
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 2755
  ]
  edge [
    source 62
    target 1090
  ]
  edge [
    source 62
    target 1812
  ]
  edge [
    source 62
    target 1263
  ]
  edge [
    source 62
    target 2644
  ]
  edge [
    source 62
    target 2756
  ]
  edge [
    source 62
    target 2757
  ]
  edge [
    source 62
    target 2758
  ]
  edge [
    source 62
    target 2646
  ]
  edge [
    source 62
    target 2759
  ]
  edge [
    source 62
    target 693
  ]
  edge [
    source 62
    target 2760
  ]
  edge [
    source 62
    target 694
  ]
  edge [
    source 62
    target 2761
  ]
  edge [
    source 62
    target 2762
  ]
  edge [
    source 62
    target 2763
  ]
  edge [
    source 62
    target 2764
  ]
  edge [
    source 62
    target 713
  ]
  edge [
    source 62
    target 2765
  ]
  edge [
    source 62
    target 2766
  ]
  edge [
    source 62
    target 2767
  ]
  edge [
    source 62
    target 2643
  ]
  edge [
    source 62
    target 2768
  ]
  edge [
    source 62
    target 2769
  ]
  edge [
    source 62
    target 2770
  ]
  edge [
    source 62
    target 608
  ]
  edge [
    source 62
    target 2771
  ]
  edge [
    source 62
    target 620
  ]
  edge [
    source 62
    target 2772
  ]
  edge [
    source 62
    target 2773
  ]
  edge [
    source 62
    target 2774
  ]
  edge [
    source 62
    target 1816
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 2547
  ]
  edge [
    source 63
    target 2569
  ]
  edge [
    source 63
    target 2570
  ]
  edge [
    source 63
    target 2573
  ]
  edge [
    source 63
    target 2775
  ]
  edge [
    source 63
    target 2776
  ]
  edge [
    source 63
    target 2777
  ]
  edge [
    source 63
    target 2778
  ]
  edge [
    source 63
    target 2574
  ]
  edge [
    source 63
    target 2568
  ]
  edge [
    source 63
    target 132
  ]
  edge [
    source 63
    target 1408
  ]
  edge [
    source 63
    target 2571
  ]
  edge [
    source 63
    target 1013
  ]
  edge [
    source 63
    target 2572
  ]
  edge [
    source 63
    target 397
  ]
  edge [
    source 63
    target 2779
  ]
  edge [
    source 63
    target 2780
  ]
  edge [
    source 63
    target 2781
  ]
  edge [
    source 63
    target 2782
  ]
  edge [
    source 63
    target 2783
  ]
  edge [
    source 63
    target 2784
  ]
  edge [
    source 63
    target 2785
  ]
  edge [
    source 63
    target 2786
  ]
  edge [
    source 63
    target 2787
  ]
  edge [
    source 63
    target 2788
  ]
  edge [
    source 63
    target 2789
  ]
  edge [
    source 63
    target 2790
  ]
  edge [
    source 63
    target 2791
  ]
  edge [
    source 63
    target 2792
  ]
  edge [
    source 63
    target 2793
  ]
  edge [
    source 63
    target 2794
  ]
  edge [
    source 63
    target 2795
  ]
  edge [
    source 63
    target 2796
  ]
  edge [
    source 63
    target 2797
  ]
  edge [
    source 63
    target 448
  ]
  edge [
    source 63
    target 2798
  ]
  edge [
    source 63
    target 2799
  ]
  edge [
    source 63
    target 596
  ]
  edge [
    source 63
    target 2800
  ]
  edge [
    source 63
    target 2801
  ]
  edge [
    source 63
    target 2206
  ]
  edge [
    source 63
    target 241
  ]
  edge [
    source 63
    target 2802
  ]
  edge [
    source 63
    target 2803
  ]
  edge [
    source 63
    target 1061
  ]
  edge [
    source 63
    target 2804
  ]
  edge [
    source 63
    target 2805
  ]
  edge [
    source 63
    target 2806
  ]
  edge [
    source 63
    target 2807
  ]
  edge [
    source 63
    target 414
  ]
  edge [
    source 63
    target 2808
  ]
  edge [
    source 63
    target 2809
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 287
  ]
  edge [
    source 64
    target 2810
  ]
  edge [
    source 64
    target 2811
  ]
  edge [
    source 64
    target 793
  ]
  edge [
    source 64
    target 195
  ]
  edge [
    source 64
    target 2812
  ]
  edge [
    source 64
    target 2813
  ]
  edge [
    source 64
    target 1210
  ]
  edge [
    source 64
    target 2814
  ]
  edge [
    source 64
    target 2815
  ]
  edge [
    source 64
    target 2816
  ]
  edge [
    source 64
    target 2817
  ]
  edge [
    source 64
    target 2818
  ]
  edge [
    source 64
    target 2819
  ]
  edge [
    source 64
    target 2179
  ]
  edge [
    source 64
    target 431
  ]
  edge [
    source 64
    target 2820
  ]
  edge [
    source 64
    target 2821
  ]
  edge [
    source 64
    target 2822
  ]
  edge [
    source 64
    target 1303
  ]
  edge [
    source 64
    target 1749
  ]
  edge [
    source 64
    target 2823
  ]
  edge [
    source 64
    target 2580
  ]
  edge [
    source 64
    target 762
  ]
  edge [
    source 64
    target 2824
  ]
  edge [
    source 64
    target 2825
  ]
  edge [
    source 64
    target 987
  ]
  edge [
    source 64
    target 2826
  ]
  edge [
    source 64
    target 2827
  ]
  edge [
    source 64
    target 1113
  ]
  edge [
    source 64
    target 2828
  ]
  edge [
    source 64
    target 2829
  ]
  edge [
    source 64
    target 2830
  ]
  edge [
    source 64
    target 602
  ]
  edge [
    source 64
    target 2831
  ]
  edge [
    source 64
    target 2832
  ]
  edge [
    source 64
    target 1546
  ]
  edge [
    source 64
    target 2833
  ]
  edge [
    source 64
    target 2834
  ]
  edge [
    source 64
    target 216
  ]
  edge [
    source 64
    target 897
  ]
  edge [
    source 64
    target 912
  ]
  edge [
    source 64
    target 256
  ]
  edge [
    source 64
    target 2835
  ]
  edge [
    source 64
    target 733
  ]
  edge [
    source 64
    target 320
  ]
  edge [
    source 64
    target 321
  ]
  edge [
    source 64
    target 322
  ]
  edge [
    source 64
    target 323
  ]
  edge [
    source 64
    target 324
  ]
  edge [
    source 64
    target 325
  ]
  edge [
    source 64
    target 326
  ]
  edge [
    source 64
    target 327
  ]
  edge [
    source 64
    target 328
  ]
  edge [
    source 64
    target 329
  ]
  edge [
    source 64
    target 330
  ]
  edge [
    source 64
    target 331
  ]
  edge [
    source 64
    target 332
  ]
  edge [
    source 64
    target 333
  ]
  edge [
    source 64
    target 334
  ]
  edge [
    source 64
    target 335
  ]
  edge [
    source 64
    target 336
  ]
  edge [
    source 64
    target 337
  ]
  edge [
    source 64
    target 75
  ]
  edge [
    source 64
    target 338
  ]
  edge [
    source 64
    target 339
  ]
  edge [
    source 64
    target 340
  ]
  edge [
    source 64
    target 341
  ]
  edge [
    source 64
    target 342
  ]
  edge [
    source 64
    target 343
  ]
  edge [
    source 64
    target 1877
  ]
  edge [
    source 64
    target 2836
  ]
  edge [
    source 64
    target 2837
  ]
  edge [
    source 64
    target 2838
  ]
  edge [
    source 64
    target 2839
  ]
  edge [
    source 64
    target 2840
  ]
  edge [
    source 64
    target 2841
  ]
  edge [
    source 64
    target 2842
  ]
  edge [
    source 64
    target 2843
  ]
  edge [
    source 64
    target 2844
  ]
  edge [
    source 64
    target 2845
  ]
  edge [
    source 64
    target 2846
  ]
  edge [
    source 64
    target 2847
  ]
  edge [
    source 64
    target 2848
  ]
  edge [
    source 64
    target 176
  ]
  edge [
    source 64
    target 273
  ]
  edge [
    source 64
    target 2849
  ]
  edge [
    source 64
    target 2850
  ]
  edge [
    source 64
    target 2851
  ]
  edge [
    source 64
    target 725
  ]
  edge [
    source 64
    target 628
  ]
  edge [
    source 64
    target 805
  ]
  edge [
    source 64
    target 2852
  ]
  edge [
    source 64
    target 2853
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 287
  ]
  edge [
    source 65
    target 2854
  ]
  edge [
    source 65
    target 2855
  ]
  edge [
    source 65
    target 1980
  ]
  edge [
    source 65
    target 1986
  ]
  edge [
    source 65
    target 1987
  ]
  edge [
    source 65
    target 1988
  ]
  edge [
    source 65
    target 1989
  ]
  edge [
    source 65
    target 1990
  ]
  edge [
    source 65
    target 1991
  ]
  edge [
    source 65
    target 1992
  ]
  edge [
    source 65
    target 1993
  ]
  edge [
    source 65
    target 1994
  ]
  edge [
    source 65
    target 320
  ]
  edge [
    source 65
    target 321
  ]
  edge [
    source 65
    target 322
  ]
  edge [
    source 65
    target 323
  ]
  edge [
    source 65
    target 324
  ]
  edge [
    source 65
    target 325
  ]
  edge [
    source 65
    target 326
  ]
  edge [
    source 65
    target 327
  ]
  edge [
    source 65
    target 328
  ]
  edge [
    source 65
    target 329
  ]
  edge [
    source 65
    target 330
  ]
  edge [
    source 65
    target 331
  ]
  edge [
    source 65
    target 332
  ]
  edge [
    source 65
    target 333
  ]
  edge [
    source 65
    target 334
  ]
  edge [
    source 65
    target 335
  ]
  edge [
    source 65
    target 336
  ]
  edge [
    source 65
    target 337
  ]
  edge [
    source 65
    target 75
  ]
  edge [
    source 65
    target 338
  ]
  edge [
    source 65
    target 339
  ]
  edge [
    source 65
    target 340
  ]
  edge [
    source 65
    target 341
  ]
  edge [
    source 65
    target 342
  ]
  edge [
    source 65
    target 343
  ]
  edge [
    source 65
    target 2547
  ]
  edge [
    source 65
    target 2569
  ]
  edge [
    source 65
    target 2570
  ]
  edge [
    source 65
    target 2573
  ]
  edge [
    source 65
    target 2775
  ]
  edge [
    source 65
    target 2776
  ]
  edge [
    source 65
    target 2777
  ]
  edge [
    source 65
    target 2778
  ]
  edge [
    source 65
    target 2574
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 66
    target 2856
  ]
  edge [
    source 66
    target 2857
  ]
  edge [
    source 66
    target 2858
  ]
  edge [
    source 66
    target 216
  ]
  edge [
    source 66
    target 2859
  ]
  edge [
    source 66
    target 674
  ]
  edge [
    source 66
    target 1311
  ]
  edge [
    source 66
    target 2238
  ]
  edge [
    source 66
    target 1102
  ]
  edge [
    source 66
    target 214
  ]
  edge [
    source 66
    target 2239
  ]
  edge [
    source 66
    target 2240
  ]
  edge [
    source 66
    target 2241
  ]
  edge [
    source 66
    target 161
  ]
  edge [
    source 66
    target 805
  ]
  edge [
    source 66
    target 76
  ]
  edge [
    source 66
    target 2242
  ]
  edge [
    source 66
    target 1175
  ]
  edge [
    source 66
    target 792
  ]
  edge [
    source 66
    target 746
  ]
  edge [
    source 66
    target 2722
  ]
  edge [
    source 66
    target 2860
  ]
  edge [
    source 66
    target 2861
  ]
  edge [
    source 66
    target 684
  ]
  edge [
    source 66
    target 2862
  ]
  edge [
    source 66
    target 2863
  ]
  edge [
    source 66
    target 1421
  ]
  edge [
    source 66
    target 1853
  ]
  edge [
    source 66
    target 2864
  ]
  edge [
    source 66
    target 2202
  ]
  edge [
    source 66
    target 912
  ]
  edge [
    source 66
    target 2245
  ]
  edge [
    source 66
    target 2865
  ]
  edge [
    source 66
    target 2866
  ]
  edge [
    source 66
    target 628
  ]
  edge [
    source 66
    target 1210
  ]
  edge [
    source 66
    target 662
  ]
  edge [
    source 66
    target 2867
  ]
  edge [
    source 66
    target 2868
  ]
  edge [
    source 66
    target 2869
  ]
  edge [
    source 66
    target 1274
  ]
  edge [
    source 66
    target 2870
  ]
  edge [
    source 66
    target 2871
  ]
  edge [
    source 66
    target 269
  ]
  edge [
    source 66
    target 1055
  ]
  edge [
    source 66
    target 2481
  ]
  edge [
    source 66
    target 2872
  ]
  edge [
    source 66
    target 1029
  ]
  edge [
    source 66
    target 2873
  ]
  edge [
    source 66
    target 1354
  ]
  edge [
    source 66
    target 2874
  ]
  edge [
    source 66
    target 2875
  ]
  edge [
    source 66
    target 1623
  ]
  edge [
    source 67
    target 2599
  ]
  edge [
    source 67
    target 255
  ]
  edge [
    source 67
    target 808
  ]
  edge [
    source 67
    target 695
  ]
  edge [
    source 67
    target 2600
  ]
  edge [
    source 67
    target 2601
  ]
  edge [
    source 67
    target 2602
  ]
  edge [
    source 67
    target 216
  ]
  edge [
    source 67
    target 2603
  ]
  edge [
    source 67
    target 2604
  ]
  edge [
    source 67
    target 2605
  ]
  edge [
    source 67
    target 1421
  ]
  edge [
    source 67
    target 273
  ]
  edge [
    source 67
    target 2067
  ]
  edge [
    source 67
    target 2606
  ]
  edge [
    source 67
    target 2607
  ]
  edge [
    source 67
    target 2608
  ]
  edge [
    source 67
    target 2609
  ]
  edge [
    source 67
    target 2610
  ]
  edge [
    source 67
    target 2611
  ]
  edge [
    source 67
    target 2612
  ]
  edge [
    source 67
    target 105
  ]
  edge [
    source 67
    target 813
  ]
  edge [
    source 67
    target 2613
  ]
  edge [
    source 67
    target 793
  ]
  edge [
    source 67
    target 498
  ]
  edge [
    source 67
    target 898
  ]
  edge [
    source 67
    target 729
  ]
  edge [
    source 67
    target 521
  ]
  edge [
    source 67
    target 269
  ]
  edge [
    source 67
    target 522
  ]
  edge [
    source 67
    target 1311
  ]
  edge [
    source 67
    target 2238
  ]
  edge [
    source 67
    target 1102
  ]
  edge [
    source 67
    target 214
  ]
  edge [
    source 67
    target 2239
  ]
  edge [
    source 67
    target 2240
  ]
  edge [
    source 67
    target 2241
  ]
  edge [
    source 67
    target 161
  ]
  edge [
    source 67
    target 805
  ]
  edge [
    source 67
    target 76
  ]
  edge [
    source 67
    target 2242
  ]
  edge [
    source 67
    target 1175
  ]
  edge [
    source 67
    target 2876
  ]
  edge [
    source 67
    target 2877
  ]
  edge [
    source 67
    target 172
  ]
  edge [
    source 67
    target 2878
  ]
  edge [
    source 67
    target 2879
  ]
  edge [
    source 67
    target 1728
  ]
  edge [
    source 67
    target 2880
  ]
  edge [
    source 67
    target 173
  ]
  edge [
    source 67
    target 171
  ]
  edge [
    source 67
    target 2881
  ]
  edge [
    source 67
    target 2882
  ]
  edge [
    source 67
    target 2883
  ]
  edge [
    source 67
    target 2884
  ]
  edge [
    source 67
    target 2885
  ]
  edge [
    source 67
    target 2886
  ]
  edge [
    source 67
    target 2887
  ]
  edge [
    source 67
    target 2692
  ]
  edge [
    source 67
    target 946
  ]
  edge [
    source 67
    target 2888
  ]
  edge [
    source 67
    target 2889
  ]
  edge [
    source 67
    target 2890
  ]
  edge [
    source 67
    target 2891
  ]
  edge [
    source 67
    target 939
  ]
  edge [
    source 67
    target 2892
  ]
  edge [
    source 67
    target 2893
  ]
  edge [
    source 67
    target 2894
  ]
  edge [
    source 67
    target 2895
  ]
  edge [
    source 67
    target 2896
  ]
  edge [
    source 67
    target 759
  ]
  edge [
    source 67
    target 2897
  ]
  edge [
    source 67
    target 2898
  ]
  edge [
    source 67
    target 2899
  ]
  edge [
    source 67
    target 2900
  ]
  edge [
    source 67
    target 2901
  ]
  edge [
    source 67
    target 238
  ]
  edge [
    source 67
    target 900
  ]
  edge [
    source 67
    target 2902
  ]
  edge [
    source 67
    target 2903
  ]
  edge [
    source 67
    target 2904
  ]
  edge [
    source 67
    target 2905
  ]
  edge [
    source 67
    target 2547
  ]
  edge [
    source 67
    target 2906
  ]
  edge [
    source 67
    target 2907
  ]
  edge [
    source 67
    target 2908
  ]
  edge [
    source 67
    target 1408
  ]
  edge [
    source 67
    target 124
  ]
  edge [
    source 67
    target 2909
  ]
  edge [
    source 67
    target 728
  ]
  edge [
    source 67
    target 730
  ]
  edge [
    source 67
    target 731
  ]
  edge [
    source 67
    target 732
  ]
  edge [
    source 67
    target 733
  ]
  edge [
    source 67
    target 734
  ]
  edge [
    source 67
    target 735
  ]
  edge [
    source 67
    target 736
  ]
  edge [
    source 67
    target 737
  ]
  edge [
    source 67
    target 738
  ]
  edge [
    source 67
    target 649
  ]
  edge [
    source 67
    target 739
  ]
  edge [
    source 67
    target 740
  ]
  edge [
    source 67
    target 741
  ]
  edge [
    source 67
    target 2910
  ]
  edge [
    source 67
    target 104
  ]
  edge [
    source 67
    target 2911
  ]
  edge [
    source 67
    target 2912
  ]
  edge [
    source 67
    target 2060
  ]
  edge [
    source 67
    target 2913
  ]
  edge [
    source 67
    target 2914
  ]
  edge [
    source 67
    target 2915
  ]
  edge [
    source 67
    target 2916
  ]
  edge [
    source 67
    target 2917
  ]
  edge [
    source 67
    target 2918
  ]
  edge [
    source 67
    target 2919
  ]
  edge [
    source 67
    target 2920
  ]
  edge [
    source 67
    target 2802
  ]
  edge [
    source 67
    target 2921
  ]
  edge [
    source 67
    target 2922
  ]
  edge [
    source 67
    target 2923
  ]
  edge [
    source 67
    target 2924
  ]
  edge [
    source 67
    target 1058
  ]
  edge [
    source 67
    target 2925
  ]
  edge [
    source 67
    target 2926
  ]
  edge [
    source 67
    target 2927
  ]
  edge [
    source 67
    target 400
  ]
  edge [
    source 67
    target 2928
  ]
  edge [
    source 67
    target 2929
  ]
  edge [
    source 67
    target 2930
  ]
  edge [
    source 67
    target 2931
  ]
  edge [
    source 67
    target 2932
  ]
  edge [
    source 67
    target 2933
  ]
  edge [
    source 67
    target 2934
  ]
  edge [
    source 67
    target 2935
  ]
  edge [
    source 67
    target 2936
  ]
  edge [
    source 67
    target 2937
  ]
  edge [
    source 67
    target 2938
  ]
  edge [
    source 67
    target 2939
  ]
  edge [
    source 67
    target 2940
  ]
  edge [
    source 67
    target 2941
  ]
  edge [
    source 67
    target 2942
  ]
  edge [
    source 67
    target 2943
  ]
  edge [
    source 67
    target 2944
  ]
  edge [
    source 67
    target 2945
  ]
  edge [
    source 67
    target 2946
  ]
  edge [
    source 67
    target 2947
  ]
  edge [
    source 67
    target 2948
  ]
  edge [
    source 67
    target 2949
  ]
  edge [
    source 67
    target 2950
  ]
  edge [
    source 67
    target 2951
  ]
  edge [
    source 67
    target 911
  ]
  edge [
    source 67
    target 2694
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 68
    target 71
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 2952
  ]
  edge [
    source 69
    target 2953
  ]
  edge [
    source 69
    target 2954
  ]
  edge [
    source 69
    target 2955
  ]
  edge [
    source 69
    target 2956
  ]
  edge [
    source 69
    target 613
  ]
  edge [
    source 69
    target 2957
  ]
  edge [
    source 69
    target 2958
  ]
  edge [
    source 69
    target 2959
  ]
  edge [
    source 69
    target 2960
  ]
  edge [
    source 69
    target 2961
  ]
  edge [
    source 69
    target 751
  ]
  edge [
    source 70
    target 2962
  ]
  edge [
    source 70
    target 2963
  ]
  edge [
    source 70
    target 485
  ]
  edge [
    source 70
    target 2964
  ]
  edge [
    source 70
    target 2965
  ]
  edge [
    source 70
    target 2966
  ]
  edge [
    source 70
    target 2967
  ]
  edge [
    source 70
    target 2968
  ]
  edge [
    source 70
    target 487
  ]
  edge [
    source 70
    target 149
  ]
  edge [
    source 70
    target 488
  ]
  edge [
    source 70
    target 489
  ]
  edge [
    source 70
    target 490
  ]
  edge [
    source 70
    target 491
  ]
  edge [
    source 70
    target 492
  ]
  edge [
    source 70
    target 493
  ]
  edge [
    source 70
    target 494
  ]
  edge [
    source 70
    target 495
  ]
  edge [
    source 70
    target 496
  ]
  edge [
    source 70
    target 497
  ]
  edge [
    source 70
    target 498
  ]
  edge [
    source 70
    target 499
  ]
  edge [
    source 70
    target 500
  ]
  edge [
    source 70
    target 501
  ]
  edge [
    source 70
    target 502
  ]
  edge [
    source 70
    target 503
  ]
  edge [
    source 70
    target 2093
  ]
  edge [
    source 70
    target 238
  ]
  edge [
    source 70
    target 2969
  ]
  edge [
    source 70
    target 2970
  ]
  edge [
    source 70
    target 2971
  ]
  edge [
    source 70
    target 2972
  ]
  edge [
    source 70
    target 81
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 2973
  ]
  edge [
    source 71
    target 2170
  ]
  edge [
    source 71
    target 2126
  ]
  edge [
    source 71
    target 2974
  ]
  edge [
    source 71
    target 2975
  ]
  edge [
    source 71
    target 2976
  ]
  edge [
    source 71
    target 2977
  ]
  edge [
    source 71
    target 2978
  ]
  edge [
    source 71
    target 2979
  ]
  edge [
    source 71
    target 2157
  ]
  edge [
    source 71
    target 2980
  ]
  edge [
    source 71
    target 2981
  ]
  edge [
    source 71
    target 2159
  ]
  edge [
    source 71
    target 2982
  ]
  edge [
    source 71
    target 2983
  ]
  edge [
    source 71
    target 2984
  ]
  edge [
    source 71
    target 2985
  ]
  edge [
    source 71
    target 2986
  ]
  edge [
    source 71
    target 2987
  ]
  edge [
    source 71
    target 2988
  ]
  edge [
    source 71
    target 2989
  ]
  edge [
    source 71
    target 2990
  ]
  edge [
    source 71
    target 273
  ]
  edge [
    source 71
    target 2991
  ]
  edge [
    source 71
    target 2992
  ]
  edge [
    source 71
    target 2993
  ]
  edge [
    source 71
    target 2994
  ]
  edge [
    source 71
    target 1187
  ]
  edge [
    source 71
    target 2161
  ]
  edge [
    source 71
    target 829
  ]
  edge [
    source 71
    target 2162
  ]
  edge [
    source 71
    target 2995
  ]
  edge [
    source 71
    target 2996
  ]
  edge [
    source 71
    target 2163
  ]
  edge [
    source 71
    target 1113
  ]
  edge [
    source 71
    target 2997
  ]
  edge [
    source 71
    target 2158
  ]
  edge [
    source 71
    target 602
  ]
  edge [
    source 71
    target 997
  ]
  edge [
    source 71
    target 2998
  ]
  edge [
    source 71
    target 2999
  ]
  edge [
    source 71
    target 3000
  ]
  edge [
    source 71
    target 3001
  ]
  edge [
    source 71
    target 3002
  ]
  edge [
    source 71
    target 3003
  ]
  edge [
    source 71
    target 3004
  ]
  edge [
    source 71
    target 3005
  ]
  edge [
    source 71
    target 3006
  ]
  edge [
    source 71
    target 3007
  ]
  edge [
    source 71
    target 445
  ]
  edge [
    source 71
    target 3008
  ]
  edge [
    source 71
    target 3009
  ]
  edge [
    source 71
    target 3010
  ]
  edge [
    source 71
    target 1039
  ]
  edge [
    source 71
    target 2250
  ]
  edge [
    source 71
    target 3011
  ]
  edge [
    source 71
    target 3012
  ]
  edge [
    source 71
    target 291
  ]
  edge [
    source 71
    target 3013
  ]
  edge [
    source 71
    target 3014
  ]
  edge [
    source 71
    target 3015
  ]
  edge [
    source 71
    target 3016
  ]
  edge [
    source 71
    target 3017
  ]
  edge [
    source 71
    target 3018
  ]
  edge [
    source 71
    target 3019
  ]
  edge [
    source 71
    target 3020
  ]
  edge [
    source 71
    target 3021
  ]
  edge [
    source 71
    target 3022
  ]
  edge [
    source 71
    target 3023
  ]
  edge [
    source 71
    target 3024
  ]
  edge [
    source 71
    target 3025
  ]
  edge [
    source 71
    target 3026
  ]
  edge [
    source 71
    target 3027
  ]
  edge [
    source 71
    target 3028
  ]
  edge [
    source 71
    target 3029
  ]
  edge [
    source 71
    target 3030
  ]
  edge [
    source 71
    target 3031
  ]
  edge [
    source 71
    target 2373
  ]
  edge [
    source 71
    target 3032
  ]
  edge [
    source 71
    target 3033
  ]
  edge [
    source 71
    target 3034
  ]
  edge [
    source 71
    target 3035
  ]
  edge [
    source 71
    target 1927
  ]
  edge [
    source 71
    target 1933
  ]
  edge [
    source 71
    target 3036
  ]
  edge [
    source 71
    target 3037
  ]
  edge [
    source 71
    target 3038
  ]
  edge [
    source 71
    target 3039
  ]
  edge [
    source 71
    target 3040
  ]
  edge [
    source 71
    target 3041
  ]
  edge [
    source 71
    target 3042
  ]
  edge [
    source 71
    target 3043
  ]
  edge [
    source 71
    target 3044
  ]
  edge [
    source 71
    target 1337
  ]
  edge [
    source 71
    target 3045
  ]
  edge [
    source 71
    target 3046
  ]
  edge [
    source 71
    target 3047
  ]
  edge [
    source 71
    target 3048
  ]
  edge [
    source 71
    target 3049
  ]
  edge [
    source 71
    target 3050
  ]
  edge [
    source 71
    target 3051
  ]
  edge [
    source 71
    target 3052
  ]
  edge [
    source 71
    target 3053
  ]
  edge [
    source 71
    target 3054
  ]
  edge [
    source 71
    target 3055
  ]
  edge [
    source 71
    target 3056
  ]
  edge [
    source 71
    target 3057
  ]
  edge [
    source 71
    target 3058
  ]
  edge [
    source 71
    target 3059
  ]
  edge [
    source 71
    target 3060
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 3061
  ]
  edge [
    source 72
    target 3062
  ]
  edge [
    source 72
    target 3063
  ]
  edge [
    source 72
    target 397
  ]
  edge [
    source 72
    target 142
  ]
  edge [
    source 72
    target 3064
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 3065
  ]
  edge [
    source 73
    target 2975
  ]
  edge [
    source 73
    target 3038
  ]
  edge [
    source 73
    target 3066
  ]
  edge [
    source 73
    target 3067
  ]
  edge [
    source 73
    target 1250
  ]
  edge [
    source 73
    target 2166
  ]
  edge [
    source 73
    target 897
  ]
  edge [
    source 73
    target 3068
  ]
  edge [
    source 73
    target 973
  ]
  edge [
    source 73
    target 3069
  ]
  edge [
    source 73
    target 989
  ]
  edge [
    source 73
    target 685
  ]
  edge [
    source 73
    target 979
  ]
  edge [
    source 73
    target 3070
  ]
  edge [
    source 73
    target 3071
  ]
  edge [
    source 73
    target 3072
  ]
  edge [
    source 73
    target 2126
  ]
  edge [
    source 73
    target 2825
  ]
  edge [
    source 73
    target 3056
  ]
  edge [
    source 73
    target 3057
  ]
  edge [
    source 73
    target 3058
  ]
  edge [
    source 73
    target 1594
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 3073
  ]
  edge [
    source 74
    target 3074
  ]
  edge [
    source 74
    target 2574
  ]
  edge [
    source 74
    target 3075
  ]
  edge [
    source 74
    target 3076
  ]
  edge [
    source 74
    target 3077
  ]
  edge [
    source 74
    target 1396
  ]
  edge [
    source 74
    target 3078
  ]
  edge [
    source 74
    target 1784
  ]
  edge [
    source 74
    target 3079
  ]
  edge [
    source 74
    target 1718
  ]
  edge [
    source 74
    target 3080
  ]
  edge [
    source 74
    target 3081
  ]
  edge [
    source 74
    target 3082
  ]
  edge [
    source 74
    target 1778
  ]
  edge [
    source 74
    target 3083
  ]
  edge [
    source 74
    target 3084
  ]
  edge [
    source 74
    target 3085
  ]
  edge [
    source 74
    target 3086
  ]
  edge [
    source 74
    target 3087
  ]
  edge [
    source 74
    target 397
  ]
  edge [
    source 74
    target 3088
  ]
  edge [
    source 74
    target 3089
  ]
  edge [
    source 74
    target 130
  ]
  edge [
    source 74
    target 1786
  ]
  edge [
    source 74
    target 2563
  ]
  edge [
    source 74
    target 2547
  ]
  edge [
    source 74
    target 402
  ]
  edge [
    source 74
    target 2564
  ]
  edge [
    source 74
    target 133
  ]
  edge [
    source 74
    target 2565
  ]
  edge [
    source 74
    target 1395
  ]
  edge [
    source 74
    target 141
  ]
  edge [
    source 74
    target 2566
  ]
  edge [
    source 74
    target 2567
  ]
  edge [
    source 74
    target 1391
  ]
  edge [
    source 74
    target 3090
  ]
  edge [
    source 74
    target 3091
  ]
  edge [
    source 74
    target 3092
  ]
  edge [
    source 74
    target 3093
  ]
  edge [
    source 74
    target 3094
  ]
  edge [
    source 74
    target 3095
  ]
  edge [
    source 74
    target 3096
  ]
  edge [
    source 74
    target 3097
  ]
  edge [
    source 74
    target 388
  ]
  edge [
    source 74
    target 3098
  ]
  edge [
    source 74
    target 368
  ]
  edge [
    source 74
    target 294
  ]
  edge [
    source 74
    target 1713
  ]
  edge [
    source 74
    target 1714
  ]
  edge [
    source 74
    target 1715
  ]
  edge [
    source 74
    target 302
  ]
  edge [
    source 74
    target 132
  ]
  edge [
    source 74
    target 1716
  ]
  edge [
    source 74
    target 1717
  ]
  edge [
    source 74
    target 2779
  ]
  edge [
    source 74
    target 2780
  ]
  edge [
    source 74
    target 2573
  ]
  edge [
    source 74
    target 2781
  ]
  edge [
    source 74
    target 2782
  ]
  edge [
    source 74
    target 3099
  ]
  edge [
    source 74
    target 3100
  ]
  edge [
    source 74
    target 3101
  ]
  edge [
    source 74
    target 1711
  ]
  edge [
    source 74
    target 3102
  ]
  edge [
    source 74
    target 3103
  ]
  edge [
    source 74
    target 3104
  ]
  edge [
    source 74
    target 3105
  ]
  edge [
    source 74
    target 3106
  ]
  edge [
    source 74
    target 3107
  ]
  edge [
    source 74
    target 3108
  ]
  edge [
    source 74
    target 3109
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 3110
  ]
  edge [
    source 75
    target 3111
  ]
  edge [
    source 75
    target 3112
  ]
  edge [
    source 75
    target 3113
  ]
  edge [
    source 75
    target 431
  ]
  edge [
    source 75
    target 1747
  ]
  edge [
    source 75
    target 1491
  ]
  edge [
    source 75
    target 3114
  ]
  edge [
    source 75
    target 1303
  ]
  edge [
    source 75
    target 3115
  ]
  edge [
    source 75
    target 328
  ]
  edge [
    source 75
    target 438
  ]
  edge [
    source 75
    target 440
  ]
  edge [
    source 75
    target 999
  ]
  edge [
    source 75
    target 3116
  ]
  edge [
    source 75
    target 99
  ]
  edge [
    source 75
    target 3117
  ]
  edge [
    source 75
    target 3118
  ]
  edge [
    source 75
    target 3119
  ]
  edge [
    source 75
    target 287
  ]
  edge [
    source 75
    target 1495
  ]
  edge [
    source 75
    target 3120
  ]
  edge [
    source 75
    target 1496
  ]
  edge [
    source 75
    target 214
  ]
  edge [
    source 75
    target 3121
  ]
  edge [
    source 75
    target 3122
  ]
  edge [
    source 75
    target 3123
  ]
  edge [
    source 75
    target 3124
  ]
  edge [
    source 75
    target 1502
  ]
  edge [
    source 75
    target 776
  ]
  edge [
    source 75
    target 3125
  ]
  edge [
    source 75
    target 3126
  ]
  edge [
    source 75
    target 3127
  ]
  edge [
    source 75
    target 3128
  ]
  edge [
    source 75
    target 342
  ]
  edge [
    source 75
    target 778
  ]
  edge [
    source 75
    target 3129
  ]
  edge [
    source 75
    target 3130
  ]
  edge [
    source 75
    target 1506
  ]
  edge [
    source 75
    target 1505
  ]
  edge [
    source 75
    target 3131
  ]
  edge [
    source 75
    target 3132
  ]
  edge [
    source 75
    target 1671
  ]
  edge [
    source 75
    target 2178
  ]
  edge [
    source 75
    target 3133
  ]
  edge [
    source 75
    target 3134
  ]
  edge [
    source 75
    target 3135
  ]
  edge [
    source 75
    target 3136
  ]
  edge [
    source 75
    target 3137
  ]
  edge [
    source 75
    target 3138
  ]
  edge [
    source 75
    target 3139
  ]
  edge [
    source 75
    target 1759
  ]
  edge [
    source 75
    target 265
  ]
  edge [
    source 75
    target 448
  ]
  edge [
    source 75
    target 268
  ]
  edge [
    source 75
    target 258
  ]
  edge [
    source 75
    target 259
  ]
  edge [
    source 75
    target 804
  ]
  edge [
    source 75
    target 1760
  ]
  edge [
    source 75
    target 1761
  ]
  edge [
    source 75
    target 1515
  ]
  edge [
    source 75
    target 1762
  ]
  edge [
    source 75
    target 777
  ]
  edge [
    source 75
    target 782
  ]
  edge [
    source 75
    target 783
  ]
  edge [
    source 75
    target 784
  ]
  edge [
    source 75
    target 785
  ]
  edge [
    source 75
    target 786
  ]
  edge [
    source 75
    target 787
  ]
  edge [
    source 75
    target 107
  ]
  edge [
    source 75
    target 108
  ]
  edge [
    source 75
    target 109
  ]
  edge [
    source 75
    target 110
  ]
  edge [
    source 75
    target 111
  ]
  edge [
    source 75
    target 912
  ]
  edge [
    source 75
    target 793
  ]
  edge [
    source 75
    target 269
  ]
  edge [
    source 75
    target 238
  ]
  edge [
    source 75
    target 332
  ]
  edge [
    source 75
    target 1516
  ]
  edge [
    source 75
    target 1517
  ]
  edge [
    source 75
    target 1518
  ]
  edge [
    source 75
    target 1519
  ]
  edge [
    source 75
    target 322
  ]
  edge [
    source 75
    target 1520
  ]
  edge [
    source 75
    target 1521
  ]
  edge [
    source 75
    target 327
  ]
  edge [
    source 75
    target 1522
  ]
  edge [
    source 75
    target 1523
  ]
  edge [
    source 75
    target 329
  ]
  edge [
    source 75
    target 585
  ]
  edge [
    source 75
    target 333
  ]
  edge [
    source 75
    target 987
  ]
  edge [
    source 75
    target 1524
  ]
  edge [
    source 75
    target 1525
  ]
  edge [
    source 75
    target 1526
  ]
  edge [
    source 75
    target 1527
  ]
  edge [
    source 75
    target 221
  ]
  edge [
    source 75
    target 1528
  ]
  edge [
    source 75
    target 1529
  ]
  edge [
    source 75
    target 1530
  ]
  edge [
    source 75
    target 1531
  ]
  edge [
    source 75
    target 339
  ]
  edge [
    source 75
    target 340
  ]
  edge [
    source 75
    target 1532
  ]
  edge [
    source 75
    target 341
  ]
  edge [
    source 75
    target 1533
  ]
  edge [
    source 75
    target 343
  ]
  edge [
    source 75
    target 1534
  ]
  edge [
    source 75
    target 1535
  ]
  edge [
    source 75
    target 1536
  ]
  edge [
    source 75
    target 1537
  ]
  edge [
    source 75
    target 1538
  ]
  edge [
    source 75
    target 3140
  ]
  edge [
    source 75
    target 746
  ]
  edge [
    source 75
    target 3141
  ]
  edge [
    source 75
    target 3142
  ]
  edge [
    source 75
    target 3143
  ]
  edge [
    source 75
    target 2726
  ]
  edge [
    source 75
    target 3144
  ]
  edge [
    source 75
    target 2731
  ]
  edge [
    source 75
    target 991
  ]
  edge [
    source 75
    target 1744
  ]
  edge [
    source 75
    target 1745
  ]
  edge [
    source 75
    target 3145
  ]
  edge [
    source 75
    target 1746
  ]
  edge [
    source 75
    target 3146
  ]
  edge [
    source 75
    target 1718
  ]
  edge [
    source 75
    target 320
  ]
  edge [
    source 75
    target 321
  ]
  edge [
    source 75
    target 323
  ]
  edge [
    source 75
    target 324
  ]
  edge [
    source 75
    target 325
  ]
  edge [
    source 75
    target 326
  ]
  edge [
    source 75
    target 330
  ]
  edge [
    source 75
    target 331
  ]
  edge [
    source 75
    target 334
  ]
  edge [
    source 75
    target 335
  ]
  edge [
    source 75
    target 336
  ]
  edge [
    source 75
    target 337
  ]
  edge [
    source 75
    target 338
  ]
  edge [
    source 75
    target 714
  ]
  edge [
    source 75
    target 629
  ]
  edge [
    source 75
    target 3147
  ]
  edge [
    source 75
    target 3148
  ]
  edge [
    source 75
    target 3149
  ]
  edge [
    source 75
    target 3150
  ]
  edge [
    source 75
    target 3151
  ]
  edge [
    source 75
    target 523
  ]
  edge [
    source 75
    target 3152
  ]
  edge [
    source 75
    target 3153
  ]
  edge [
    source 75
    target 3154
  ]
  edge [
    source 75
    target 3155
  ]
  edge [
    source 75
    target 2485
  ]
  edge [
    source 75
    target 2680
  ]
  edge [
    source 75
    target 3156
  ]
  edge [
    source 75
    target 3157
  ]
  edge [
    source 75
    target 3158
  ]
  edge [
    source 75
    target 3159
  ]
  edge [
    source 75
    target 3160
  ]
  edge [
    source 75
    target 3161
  ]
  edge [
    source 75
    target 3162
  ]
  edge [
    source 75
    target 3163
  ]
  edge [
    source 75
    target 1494
  ]
  edge [
    source 75
    target 3164
  ]
  edge [
    source 75
    target 3165
  ]
  edge [
    source 75
    target 2800
  ]
  edge [
    source 75
    target 1577
  ]
  edge [
    source 75
    target 1569
  ]
  edge [
    source 75
    target 1558
  ]
  edge [
    source 75
    target 1561
  ]
  edge [
    source 75
    target 1578
  ]
  edge [
    source 75
    target 1562
  ]
  edge [
    source 75
    target 620
  ]
  edge [
    source 75
    target 1579
  ]
  edge [
    source 75
    target 830
  ]
  edge [
    source 75
    target 1580
  ]
  edge [
    source 75
    target 1581
  ]
  edge [
    source 75
    target 1582
  ]
  edge [
    source 75
    target 1559
  ]
  edge [
    source 75
    target 1560
  ]
  edge [
    source 75
    target 1563
  ]
  edge [
    source 75
    target 1564
  ]
  edge [
    source 75
    target 1565
  ]
  edge [
    source 75
    target 1566
  ]
  edge [
    source 75
    target 1567
  ]
  edge [
    source 75
    target 1568
  ]
  edge [
    source 75
    target 1570
  ]
  edge [
    source 75
    target 1571
  ]
  edge [
    source 75
    target 1572
  ]
  edge [
    source 75
    target 1573
  ]
  edge [
    source 75
    target 1574
  ]
  edge [
    source 75
    target 1575
  ]
  edge [
    source 75
    target 1576
  ]
  edge [
    source 75
    target 1589
  ]
  edge [
    source 75
    target 1590
  ]
  edge [
    source 75
    target 1591
  ]
  edge [
    source 75
    target 3166
  ]
  edge [
    source 75
    target 3167
  ]
  edge [
    source 75
    target 3168
  ]
  edge [
    source 75
    target 3169
  ]
  edge [
    source 75
    target 3170
  ]
  edge [
    source 75
    target 3171
  ]
  edge [
    source 75
    target 3172
  ]
  edge [
    source 75
    target 3173
  ]
  edge [
    source 75
    target 3174
  ]
  edge [
    source 75
    target 3175
  ]
  edge [
    source 75
    target 3176
  ]
  edge [
    source 75
    target 3177
  ]
  edge [
    source 75
    target 3178
  ]
  edge [
    source 75
    target 3179
  ]
  edge [
    source 75
    target 3180
  ]
  edge [
    source 75
    target 3181
  ]
  edge [
    source 75
    target 273
  ]
  edge [
    source 75
    target 3182
  ]
  edge [
    source 75
    target 685
  ]
  edge [
    source 75
    target 1100
  ]
  edge [
    source 75
    target 1451
  ]
  edge [
    source 75
    target 3183
  ]
  edge [
    source 75
    target 1507
  ]
  edge [
    source 75
    target 466
  ]
  edge [
    source 75
    target 177
  ]
  edge [
    source 75
    target 1508
  ]
  edge [
    source 75
    target 1509
  ]
  edge [
    source 75
    target 1510
  ]
  edge [
    source 75
    target 1511
  ]
  edge [
    source 75
    target 249
  ]
  edge [
    source 75
    target 233
  ]
  edge [
    source 75
    target 226
  ]
  edge [
    source 75
    target 1512
  ]
  edge [
    source 75
    target 1513
  ]
  edge [
    source 75
    target 1514
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 2675
  ]
  edge [
    source 76
    target 1873
  ]
  edge [
    source 76
    target 2676
  ]
  edge [
    source 76
    target 2677
  ]
  edge [
    source 76
    target 1876
  ]
  edge [
    source 76
    target 2678
  ]
  edge [
    source 76
    target 1877
  ]
  edge [
    source 76
    target 2679
  ]
  edge [
    source 76
    target 1879
  ]
  edge [
    source 76
    target 1881
  ]
  edge [
    source 76
    target 1882
  ]
  edge [
    source 76
    target 912
  ]
  edge [
    source 76
    target 2680
  ]
  edge [
    source 76
    target 2681
  ]
  edge [
    source 76
    target 1540
  ]
  edge [
    source 76
    target 2682
  ]
  edge [
    source 76
    target 2683
  ]
  edge [
    source 76
    target 2684
  ]
  edge [
    source 76
    target 216
  ]
  edge [
    source 76
    target 1883
  ]
  edge [
    source 76
    target 2685
  ]
  edge [
    source 76
    target 1885
  ]
  edge [
    source 76
    target 1888
  ]
  edge [
    source 76
    target 794
  ]
  edge [
    source 76
    target 1889
  ]
  edge [
    source 76
    target 2686
  ]
  edge [
    source 76
    target 2687
  ]
  edge [
    source 76
    target 1890
  ]
  edge [
    source 76
    target 2688
  ]
  edge [
    source 76
    target 2689
  ]
  edge [
    source 76
    target 2690
  ]
  edge [
    source 76
    target 1891
  ]
  edge [
    source 76
    target 1892
  ]
  edge [
    source 76
    target 1893
  ]
  edge [
    source 76
    target 1895
  ]
  edge [
    source 76
    target 1657
  ]
  edge [
    source 76
    target 1896
  ]
  edge [
    source 76
    target 1898
  ]
  edge [
    source 76
    target 1899
  ]
  edge [
    source 76
    target 2691
  ]
  edge [
    source 76
    target 1900
  ]
  edge [
    source 76
    target 1901
  ]
  edge [
    source 76
    target 2692
  ]
  edge [
    source 76
    target 1903
  ]
  edge [
    source 76
    target 2693
  ]
  edge [
    source 76
    target 2694
  ]
  edge [
    source 76
    target 2695
  ]
  edge [
    source 76
    target 2696
  ]
  edge [
    source 76
    target 1904
  ]
  edge [
    source 76
    target 3151
  ]
  edge [
    source 76
    target 3184
  ]
  edge [
    source 76
    target 3185
  ]
  edge [
    source 76
    target 3186
  ]
  edge [
    source 76
    target 1871
  ]
  edge [
    source 76
    target 2126
  ]
  edge [
    source 76
    target 3187
  ]
  edge [
    source 76
    target 3188
  ]
  edge [
    source 76
    target 3189
  ]
  edge [
    source 76
    target 3190
  ]
  edge [
    source 76
    target 3191
  ]
  edge [
    source 76
    target 3192
  ]
  edge [
    source 76
    target 3193
  ]
  edge [
    source 76
    target 3194
  ]
  edge [
    source 76
    target 2234
  ]
  edge [
    source 76
    target 3195
  ]
  edge [
    source 76
    target 3196
  ]
  edge [
    source 76
    target 1113
  ]
  edge [
    source 76
    target 3197
  ]
  edge [
    source 76
    target 440
  ]
  edge [
    source 76
    target 2618
  ]
  edge [
    source 76
    target 3198
  ]
  edge [
    source 76
    target 2180
  ]
  edge [
    source 76
    target 3199
  ]
  edge [
    source 76
    target 917
  ]
  edge [
    source 76
    target 792
  ]
  edge [
    source 76
    target 2477
  ]
  edge [
    source 76
    target 2478
  ]
  edge [
    source 76
    target 2479
  ]
  edge [
    source 76
    target 2480
  ]
  edge [
    source 76
    target 2481
  ]
  edge [
    source 76
    target 2482
  ]
  edge [
    source 76
    target 804
  ]
  edge [
    source 76
    target 2483
  ]
  edge [
    source 76
    target 2484
  ]
  edge [
    source 76
    target 2485
  ]
  edge [
    source 76
    target 566
  ]
  edge [
    source 76
    target 2486
  ]
  edge [
    source 76
    target 2487
  ]
  edge [
    source 76
    target 1324
  ]
  edge [
    source 76
    target 3129
  ]
  edge [
    source 76
    target 3137
  ]
  edge [
    source 76
    target 3159
  ]
  edge [
    source 76
    target 3120
  ]
  edge [
    source 76
    target 3200
  ]
  edge [
    source 76
    target 3201
  ]
  edge [
    source 76
    target 3202
  ]
  edge [
    source 76
    target 3203
  ]
  edge [
    source 76
    target 3204
  ]
  edge [
    source 76
    target 3205
  ]
  edge [
    source 76
    target 3206
  ]
  edge [
    source 76
    target 3207
  ]
  edge [
    source 76
    target 3208
  ]
  edge [
    source 76
    target 3209
  ]
  edge [
    source 76
    target 3210
  ]
  edge [
    source 76
    target 3211
  ]
  edge [
    source 76
    target 1806
  ]
  edge [
    source 76
    target 3212
  ]
  edge [
    source 76
    target 3213
  ]
  edge [
    source 76
    target 3214
  ]
  edge [
    source 76
    target 3215
  ]
  edge [
    source 76
    target 3216
  ]
  edge [
    source 76
    target 3217
  ]
  edge [
    source 76
    target 1078
  ]
  edge [
    source 76
    target 3218
  ]
  edge [
    source 76
    target 3219
  ]
  edge [
    source 76
    target 3220
  ]
  edge [
    source 76
    target 3221
  ]
  edge [
    source 76
    target 3222
  ]
  edge [
    source 76
    target 3223
  ]
  edge [
    source 76
    target 3224
  ]
  edge [
    source 76
    target 3225
  ]
  edge [
    source 76
    target 3226
  ]
  edge [
    source 76
    target 562
  ]
  edge [
    source 76
    target 2883
  ]
  edge [
    source 76
    target 3227
  ]
  edge [
    source 76
    target 3228
  ]
  edge [
    source 76
    target 3229
  ]
  edge [
    source 76
    target 3230
  ]
  edge [
    source 76
    target 3231
  ]
  edge [
    source 76
    target 3232
  ]
  edge [
    source 76
    target 805
  ]
  edge [
    source 76
    target 3233
  ]
  edge [
    source 76
    target 3234
  ]
  edge [
    source 76
    target 3235
  ]
  edge [
    source 76
    target 3236
  ]
  edge [
    source 76
    target 3237
  ]
  edge [
    source 76
    target 3238
  ]
  edge [
    source 76
    target 434
  ]
  edge [
    source 76
    target 3239
  ]
  edge [
    source 76
    target 3240
  ]
  edge [
    source 76
    target 3241
  ]
  edge [
    source 76
    target 3242
  ]
  edge [
    source 76
    target 201
  ]
  edge [
    source 76
    target 3243
  ]
  edge [
    source 76
    target 759
  ]
  edge [
    source 76
    target 3244
  ]
  edge [
    source 76
    target 3245
  ]
  edge [
    source 76
    target 3246
  ]
  edge [
    source 76
    target 3247
  ]
  edge [
    source 76
    target 3248
  ]
  edge [
    source 76
    target 104
  ]
  edge [
    source 76
    target 3249
  ]
  edge [
    source 76
    target 3250
  ]
  edge [
    source 76
    target 3251
  ]
  edge [
    source 76
    target 3252
  ]
  edge [
    source 76
    target 3253
  ]
  edge [
    source 76
    target 1421
  ]
  edge [
    source 76
    target 3254
  ]
  edge [
    source 76
    target 3255
  ]
  edge [
    source 76
    target 3256
  ]
  edge [
    source 76
    target 3257
  ]
  edge [
    source 76
    target 3258
  ]
  edge [
    source 76
    target 3259
  ]
  edge [
    source 76
    target 3260
  ]
  edge [
    source 76
    target 3261
  ]
  edge [
    source 76
    target 2094
  ]
  edge [
    source 76
    target 3262
  ]
  edge [
    source 76
    target 3263
  ]
  edge [
    source 76
    target 613
  ]
  edge [
    source 76
    target 3264
  ]
  edge [
    source 76
    target 3265
  ]
  edge [
    source 76
    target 827
  ]
  edge [
    source 76
    target 3266
  ]
  edge [
    source 76
    target 3267
  ]
  edge [
    source 76
    target 1291
  ]
  edge [
    source 76
    target 3268
  ]
  edge [
    source 76
    target 726
  ]
  edge [
    source 76
    target 2642
  ]
  edge [
    source 76
    target 1601
  ]
  edge [
    source 76
    target 2638
  ]
  edge [
    source 76
    target 2639
  ]
  edge [
    source 76
    target 2640
  ]
  edge [
    source 76
    target 2641
  ]
  edge [
    source 76
    target 1090
  ]
  edge [
    source 76
    target 2643
  ]
  edge [
    source 76
    target 3269
  ]
  edge [
    source 76
    target 3270
  ]
  edge [
    source 76
    target 3271
  ]
  edge [
    source 76
    target 3272
  ]
  edge [
    source 76
    target 3155
  ]
  edge [
    source 76
    target 3273
  ]
  edge [
    source 76
    target 3274
  ]
  edge [
    source 76
    target 3275
  ]
  edge [
    source 76
    target 3276
  ]
  edge [
    source 76
    target 2801
  ]
  edge [
    source 76
    target 3277
  ]
  edge [
    source 76
    target 3278
  ]
  edge [
    source 76
    target 3279
  ]
  edge [
    source 76
    target 3280
  ]
  edge [
    source 76
    target 3281
  ]
  edge [
    source 76
    target 3282
  ]
  edge [
    source 76
    target 3283
  ]
  edge [
    source 76
    target 3284
  ]
  edge [
    source 76
    target 3285
  ]
  edge [
    source 76
    target 3286
  ]
  edge [
    source 76
    target 1232
  ]
  edge [
    source 76
    target 3287
  ]
  edge [
    source 76
    target 3288
  ]
  edge [
    source 76
    target 3289
  ]
  edge [
    source 76
    target 2245
  ]
  edge [
    source 76
    target 3290
  ]
  edge [
    source 76
    target 3291
  ]
  edge [
    source 76
    target 3292
  ]
  edge [
    source 76
    target 3293
  ]
  edge [
    source 76
    target 3294
  ]
  edge [
    source 76
    target 3295
  ]
  edge [
    source 76
    target 3296
  ]
  edge [
    source 76
    target 273
  ]
  edge [
    source 76
    target 3297
  ]
  edge [
    source 76
    target 3298
  ]
  edge [
    source 76
    target 3299
  ]
  edge [
    source 76
    target 3300
  ]
  edge [
    source 76
    target 3301
  ]
  edge [
    source 76
    target 3302
  ]
  edge [
    source 76
    target 3303
  ]
  edge [
    source 76
    target 3304
  ]
  edge [
    source 76
    target 3305
  ]
  edge [
    source 76
    target 2537
  ]
  edge [
    source 76
    target 3306
  ]
  edge [
    source 76
    target 1099
  ]
  edge [
    source 76
    target 2869
  ]
  edge [
    source 76
    target 3307
  ]
  edge [
    source 76
    target 3308
  ]
  edge [
    source 76
    target 3309
  ]
  edge [
    source 76
    target 3310
  ]
  edge [
    source 76
    target 3311
  ]
  edge [
    source 76
    target 3312
  ]
  edge [
    source 76
    target 523
  ]
  edge [
    source 76
    target 3313
  ]
  edge [
    source 76
    target 1946
  ]
  edge [
    source 76
    target 2886
  ]
  edge [
    source 76
    target 3314
  ]
  edge [
    source 76
    target 3315
  ]
  edge [
    source 76
    target 1766
  ]
  edge [
    source 76
    target 3316
  ]
  edge [
    source 76
    target 3317
  ]
  edge [
    source 76
    target 3318
  ]
  edge [
    source 76
    target 1311
  ]
  edge [
    source 76
    target 2238
  ]
  edge [
    source 76
    target 1102
  ]
  edge [
    source 76
    target 214
  ]
  edge [
    source 76
    target 2239
  ]
  edge [
    source 76
    target 2240
  ]
  edge [
    source 76
    target 2241
  ]
  edge [
    source 76
    target 161
  ]
  edge [
    source 76
    target 2242
  ]
  edge [
    source 76
    target 1175
  ]
  edge [
    source 76
    target 3319
  ]
  edge [
    source 76
    target 3320
  ]
  edge [
    source 76
    target 3321
  ]
  edge [
    source 76
    target 218
  ]
  edge [
    source 76
    target 3322
  ]
  edge [
    source 76
    target 3323
  ]
  edge [
    source 76
    target 287
  ]
  edge [
    source 76
    target 3324
  ]
  edge [
    source 76
    target 3325
  ]
  edge [
    source 76
    target 3326
  ]
  edge [
    source 76
    target 3327
  ]
  edge [
    source 76
    target 3328
  ]
  edge [
    source 76
    target 3329
  ]
  edge [
    source 76
    target 3330
  ]
  edge [
    source 76
    target 3331
  ]
  edge [
    source 76
    target 3332
  ]
  edge [
    source 76
    target 3333
  ]
  edge [
    source 76
    target 3334
  ]
  edge [
    source 76
    target 3335
  ]
  edge [
    source 76
    target 3336
  ]
  edge [
    source 76
    target 3337
  ]
  edge [
    source 76
    target 3338
  ]
  edge [
    source 76
    target 3339
  ]
  edge [
    source 76
    target 3340
  ]
  edge [
    source 76
    target 3341
  ]
  edge [
    source 76
    target 3342
  ]
  edge [
    source 76
    target 3343
  ]
  edge [
    source 76
    target 3344
  ]
  edge [
    source 76
    target 3345
  ]
  edge [
    source 76
    target 3346
  ]
  edge [
    source 76
    target 3347
  ]
  edge [
    source 76
    target 3348
  ]
  edge [
    source 76
    target 3349
  ]
  edge [
    source 76
    target 3350
  ]
  edge [
    source 76
    target 3351
  ]
  edge [
    source 76
    target 3352
  ]
  edge [
    source 76
    target 3353
  ]
  edge [
    source 76
    target 3354
  ]
  edge [
    source 76
    target 3355
  ]
  edge [
    source 76
    target 3025
  ]
  edge [
    source 76
    target 3356
  ]
  edge [
    source 76
    target 3357
  ]
  edge [
    source 76
    target 3044
  ]
  edge [
    source 76
    target 3358
  ]
  edge [
    source 76
    target 3359
  ]
  edge [
    source 76
    target 3360
  ]
  edge [
    source 76
    target 3361
  ]
  edge [
    source 76
    target 3362
  ]
  edge [
    source 76
    target 3363
  ]
  edge [
    source 76
    target 3364
  ]
  edge [
    source 76
    target 3365
  ]
  edge [
    source 76
    target 3366
  ]
  edge [
    source 76
    target 3367
  ]
  edge [
    source 76
    target 1820
  ]
  edge [
    source 76
    target 3368
  ]
  edge [
    source 76
    target 3369
  ]
  edge [
    source 76
    target 3370
  ]
  edge [
    source 76
    target 3371
  ]
  edge [
    source 76
    target 3372
  ]
  edge [
    source 76
    target 3373
  ]
  edge [
    source 76
    target 3374
  ]
  edge [
    source 76
    target 3375
  ]
  edge [
    source 76
    target 3376
  ]
  edge [
    source 76
    target 3377
  ]
  edge [
    source 76
    target 3378
  ]
  edge [
    source 76
    target 3379
  ]
  edge [
    source 76
    target 3380
  ]
  edge [
    source 76
    target 3381
  ]
  edge [
    source 76
    target 3382
  ]
  edge [
    source 76
    target 3383
  ]
  edge [
    source 76
    target 3384
  ]
  edge [
    source 76
    target 3385
  ]
  edge [
    source 76
    target 3386
  ]
  edge [
    source 76
    target 2936
  ]
  edge [
    source 76
    target 3387
  ]
  edge [
    source 76
    target 3388
  ]
  edge [
    source 76
    target 3389
  ]
  edge [
    source 76
    target 291
  ]
  edge [
    source 76
    target 2104
  ]
  edge [
    source 76
    target 2863
  ]
  edge [
    source 76
    target 3390
  ]
  edge [
    source 76
    target 3391
  ]
  edge [
    source 76
    target 1286
  ]
  edge [
    source 76
    target 3392
  ]
  edge [
    source 76
    target 3393
  ]
  edge [
    source 76
    target 3394
  ]
  edge [
    source 76
    target 3395
  ]
  edge [
    source 76
    target 3396
  ]
  edge [
    source 76
    target 3397
  ]
  edge [
    source 76
    target 3398
  ]
  edge [
    source 76
    target 3399
  ]
  edge [
    source 76
    target 3400
  ]
  edge [
    source 76
    target 908
  ]
  edge [
    source 76
    target 3146
  ]
  edge [
    source 76
    target 2046
  ]
  edge [
    source 76
    target 3401
  ]
  edge [
    source 76
    target 3402
  ]
  edge [
    source 76
    target 3403
  ]
  edge [
    source 76
    target 2170
  ]
  edge [
    source 76
    target 2139
  ]
  edge [
    source 76
    target 3404
  ]
  edge [
    source 76
    target 3405
  ]
  edge [
    source 76
    target 3406
  ]
  edge [
    source 76
    target 3407
  ]
  edge [
    source 76
    target 3408
  ]
  edge [
    source 76
    target 3409
  ]
  edge [
    source 76
    target 3410
  ]
  edge [
    source 76
    target 3411
  ]
  edge [
    source 76
    target 3412
  ]
  edge [
    source 76
    target 3413
  ]
  edge [
    source 76
    target 3414
  ]
  edge [
    source 76
    target 3415
  ]
  edge [
    source 76
    target 3416
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 3417
  ]
  edge [
    source 77
    target 214
  ]
  edge [
    source 77
    target 3418
  ]
  edge [
    source 77
    target 1758
  ]
  edge [
    source 77
    target 999
  ]
  edge [
    source 77
    target 3419
  ]
  edge [
    source 77
    target 777
  ]
  edge [
    source 77
    target 782
  ]
  edge [
    source 77
    target 783
  ]
  edge [
    source 77
    target 784
  ]
  edge [
    source 77
    target 785
  ]
  edge [
    source 77
    target 786
  ]
  edge [
    source 77
    target 787
  ]
  edge [
    source 77
    target 1734
  ]
  edge [
    source 77
    target 287
  ]
  edge [
    source 77
    target 1961
  ]
  edge [
    source 77
    target 1962
  ]
  edge [
    source 77
    target 1963
  ]
  edge [
    source 77
    target 1746
  ]
  edge [
    source 77
    target 2721
  ]
  edge [
    source 77
    target 2722
  ]
  edge [
    source 77
    target 2723
  ]
  edge [
    source 77
    target 991
  ]
  edge [
    source 77
    target 804
  ]
  edge [
    source 77
    target 2724
  ]
  edge [
    source 77
    target 2725
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 3420
  ]
  edge [
    source 78
    target 3421
  ]
  edge [
    source 78
    target 750
  ]
  edge [
    source 78
    target 3422
  ]
  edge [
    source 78
    target 3423
  ]
  edge [
    source 78
    target 3424
  ]
  edge [
    source 78
    target 1551
  ]
  edge [
    source 78
    target 3425
  ]
  edge [
    source 78
    target 2662
  ]
  edge [
    source 78
    target 3426
  ]
  edge [
    source 78
    target 3427
  ]
  edge [
    source 78
    target 3428
  ]
  edge [
    source 78
    target 3429
  ]
  edge [
    source 78
    target 3430
  ]
  edge [
    source 78
    target 3431
  ]
  edge [
    source 78
    target 3432
  ]
  edge [
    source 78
    target 3433
  ]
  edge [
    source 78
    target 3434
  ]
  edge [
    source 78
    target 3435
  ]
  edge [
    source 78
    target 1090
  ]
  edge [
    source 78
    target 3436
  ]
  edge [
    source 78
    target 2660
  ]
  edge [
    source 78
    target 1851
  ]
  edge [
    source 78
    target 726
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 3437
  ]
  edge [
    source 80
    target 1346
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 82
    target 1605
  ]
  edge [
    source 82
    target 3438
  ]
  edge [
    source 82
    target 3436
  ]
  edge [
    source 82
    target 3421
  ]
  edge [
    source 82
    target 85
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 3439
  ]
  edge [
    source 83
    target 93
  ]
  edge [
    source 83
    target 2003
  ]
  edge [
    source 83
    target 3440
  ]
  edge [
    source 83
    target 3441
  ]
  edge [
    source 83
    target 3442
  ]
  edge [
    source 83
    target 3443
  ]
  edge [
    source 83
    target 3444
  ]
  edge [
    source 83
    target 1720
  ]
  edge [
    source 83
    target 3445
  ]
  edge [
    source 83
    target 3446
  ]
  edge [
    source 83
    target 3447
  ]
  edge [
    source 83
    target 3448
  ]
  edge [
    source 83
    target 1976
  ]
  edge [
    source 83
    target 92
  ]
  edge [
    source 83
    target 3449
  ]
  edge [
    source 83
    target 2577
  ]
  edge [
    source 83
    target 404
  ]
  edge [
    source 83
    target 3450
  ]
  edge [
    source 83
    target 3451
  ]
  edge [
    source 83
    target 3452
  ]
  edge [
    source 83
    target 3453
  ]
  edge [
    source 83
    target 3454
  ]
  edge [
    source 83
    target 91
  ]
  edge [
    source 83
    target 3455
  ]
  edge [
    source 83
    target 2808
  ]
  edge [
    source 83
    target 3456
  ]
  edge [
    source 83
    target 3457
  ]
  edge [
    source 83
    target 3458
  ]
  edge [
    source 83
    target 3459
  ]
  edge [
    source 83
    target 349
  ]
  edge [
    source 83
    target 3460
  ]
  edge [
    source 83
    target 3461
  ]
  edge [
    source 83
    target 3462
  ]
  edge [
    source 83
    target 3463
  ]
  edge [
    source 83
    target 3464
  ]
  edge [
    source 83
    target 3465
  ]
  edge [
    source 83
    target 3466
  ]
  edge [
    source 83
    target 3467
  ]
  edge [
    source 83
    target 3468
  ]
  edge [
    source 83
    target 3469
  ]
  edge [
    source 83
    target 3470
  ]
  edge [
    source 83
    target 3471
  ]
  edge [
    source 83
    target 3472
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 3473
  ]
  edge [
    source 84
    target 3474
  ]
  edge [
    source 84
    target 2087
  ]
  edge [
    source 84
    target 3475
  ]
  edge [
    source 84
    target 3476
  ]
  edge [
    source 84
    target 3477
  ]
  edge [
    source 84
    target 3478
  ]
  edge [
    source 84
    target 3261
  ]
  edge [
    source 84
    target 3479
  ]
  edge [
    source 84
    target 3480
  ]
  edge [
    source 84
    target 3481
  ]
  edge [
    source 84
    target 1311
  ]
  edge [
    source 84
    target 3482
  ]
  edge [
    source 84
    target 912
  ]
  edge [
    source 84
    target 3483
  ]
  edge [
    source 84
    target 3256
  ]
  edge [
    source 84
    target 3484
  ]
  edge [
    source 84
    target 3485
  ]
  edge [
    source 84
    target 3486
  ]
  edge [
    source 84
    target 216
  ]
  edge [
    source 84
    target 3487
  ]
  edge [
    source 84
    target 3488
  ]
  edge [
    source 84
    target 3489
  ]
  edge [
    source 84
    target 3490
  ]
  edge [
    source 84
    target 3491
  ]
  edge [
    source 84
    target 3492
  ]
  edge [
    source 84
    target 3493
  ]
  edge [
    source 84
    target 3494
  ]
  edge [
    source 84
    target 3495
  ]
  edge [
    source 84
    target 3496
  ]
  edge [
    source 84
    target 804
  ]
  edge [
    source 84
    target 3497
  ]
  edge [
    source 84
    target 3498
  ]
  edge [
    source 84
    target 3499
  ]
  edge [
    source 84
    target 2303
  ]
  edge [
    source 84
    target 3500
  ]
  edge [
    source 84
    target 3501
  ]
  edge [
    source 84
    target 3502
  ]
  edge [
    source 84
    target 3503
  ]
  edge [
    source 84
    target 3504
  ]
  edge [
    source 84
    target 3505
  ]
  edge [
    source 84
    target 3506
  ]
  edge [
    source 84
    target 3507
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 3508
  ]
  edge [
    source 85
    target 3509
  ]
  edge [
    source 85
    target 3510
  ]
  edge [
    source 85
    target 3511
  ]
  edge [
    source 85
    target 3512
  ]
  edge [
    source 85
    target 3513
  ]
  edge [
    source 85
    target 3514
  ]
  edge [
    source 85
    target 3515
  ]
  edge [
    source 85
    target 3516
  ]
  edge [
    source 85
    target 3517
  ]
  edge [
    source 85
    target 3518
  ]
  edge [
    source 85
    target 3519
  ]
  edge [
    source 85
    target 474
  ]
  edge [
    source 85
    target 273
  ]
  edge [
    source 85
    target 3520
  ]
  edge [
    source 85
    target 238
  ]
  edge [
    source 85
    target 3521
  ]
  edge [
    source 85
    target 3522
  ]
  edge [
    source 85
    target 3523
  ]
  edge [
    source 85
    target 279
  ]
  edge [
    source 85
    target 1724
  ]
  edge [
    source 85
    target 602
  ]
  edge [
    source 85
    target 659
  ]
  edge [
    source 85
    target 3524
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 3525
  ]
  edge [
    source 86
    target 3526
  ]
  edge [
    source 86
    target 3527
  ]
  edge [
    source 86
    target 3528
  ]
  edge [
    source 86
    target 3529
  ]
  edge [
    source 86
    target 3530
  ]
  edge [
    source 86
    target 3531
  ]
  edge [
    source 86
    target 3532
  ]
  edge [
    source 86
    target 3533
  ]
  edge [
    source 86
    target 287
  ]
  edge [
    source 86
    target 3534
  ]
  edge [
    source 86
    target 1772
  ]
  edge [
    source 86
    target 1613
  ]
  edge [
    source 86
    target 2017
  ]
  edge [
    source 86
    target 2018
  ]
  edge [
    source 86
    target 2020
  ]
  edge [
    source 86
    target 2021
  ]
  edge [
    source 86
    target 2023
  ]
  edge [
    source 86
    target 3535
  ]
  edge [
    source 86
    target 3536
  ]
  edge [
    source 86
    target 3537
  ]
  edge [
    source 86
    target 3538
  ]
  edge [
    source 86
    target 3539
  ]
  edge [
    source 86
    target 3540
  ]
  edge [
    source 86
    target 3541
  ]
  edge [
    source 86
    target 3542
  ]
  edge [
    source 86
    target 3543
  ]
  edge [
    source 86
    target 3544
  ]
  edge [
    source 86
    target 3545
  ]
  edge [
    source 86
    target 2084
  ]
  edge [
    source 86
    target 3546
  ]
  edge [
    source 86
    target 3547
  ]
  edge [
    source 86
    target 3548
  ]
  edge [
    source 86
    target 3549
  ]
  edge [
    source 86
    target 3550
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 88
    target 3551
  ]
  edge [
    source 88
    target 3552
  ]
  edge [
    source 88
    target 3553
  ]
  edge [
    source 88
    target 3554
  ]
  edge [
    source 88
    target 3555
  ]
  edge [
    source 88
    target 3556
  ]
  edge [
    source 88
    target 3557
  ]
  edge [
    source 88
    target 3558
  ]
  edge [
    source 88
    target 3559
  ]
  edge [
    source 88
    target 3560
  ]
  edge [
    source 88
    target 3561
  ]
  edge [
    source 88
    target 3562
  ]
  edge [
    source 88
    target 3563
  ]
  edge [
    source 88
    target 3564
  ]
  edge [
    source 88
    target 3565
  ]
  edge [
    source 88
    target 3566
  ]
  edge [
    source 88
    target 3567
  ]
  edge [
    source 88
    target 3568
  ]
  edge [
    source 88
    target 628
  ]
  edge [
    source 88
    target 1076
  ]
  edge [
    source 88
    target 3569
  ]
  edge [
    source 88
    target 3570
  ]
  edge [
    source 88
    target 3571
  ]
  edge [
    source 88
    target 746
  ]
  edge [
    source 88
    target 485
  ]
  edge [
    source 88
    target 3572
  ]
  edge [
    source 88
    target 3573
  ]
  edge [
    source 88
    target 3574
  ]
  edge [
    source 88
    target 3575
  ]
  edge [
    source 88
    target 425
  ]
  edge [
    source 88
    target 1587
  ]
  edge [
    source 88
    target 831
  ]
  edge [
    source 88
    target 3576
  ]
  edge [
    source 88
    target 3577
  ]
  edge [
    source 88
    target 667
  ]
  edge [
    source 88
    target 636
  ]
  edge [
    source 88
    target 182
  ]
  edge [
    source 88
    target 3578
  ]
  edge [
    source 88
    target 3579
  ]
  edge [
    source 88
    target 1054
  ]
  edge [
    source 88
    target 1585
  ]
  edge [
    source 88
    target 815
  ]
  edge [
    source 88
    target 3580
  ]
  edge [
    source 88
    target 3581
  ]
  edge [
    source 88
    target 3582
  ]
  edge [
    source 88
    target 3583
  ]
  edge [
    source 88
    target 241
  ]
  edge [
    source 88
    target 3584
  ]
  edge [
    source 88
    target 3585
  ]
  edge [
    source 88
    target 179
  ]
  edge [
    source 88
    target 3586
  ]
  edge [
    source 88
    target 3587
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 698
  ]
  edge [
    source 89
    target 3588
  ]
  edge [
    source 89
    target 1856
  ]
  edge [
    source 89
    target 3589
  ]
  edge [
    source 89
    target 1425
  ]
  edge [
    source 89
    target 3590
  ]
  edge [
    source 89
    target 3591
  ]
  edge [
    source 89
    target 3592
  ]
  edge [
    source 89
    target 3593
  ]
  edge [
    source 89
    target 3594
  ]
  edge [
    source 89
    target 3595
  ]
  edge [
    source 89
    target 2840
  ]
  edge [
    source 89
    target 3596
  ]
  edge [
    source 89
    target 3162
  ]
  edge [
    source 89
    target 3597
  ]
  edge [
    source 89
    target 3598
  ]
  edge [
    source 89
    target 3599
  ]
  edge [
    source 89
    target 3600
  ]
  edge [
    source 89
    target 745
  ]
  edge [
    source 89
    target 746
  ]
  edge [
    source 89
    target 747
  ]
  edge [
    source 89
    target 748
  ]
  edge [
    source 89
    target 749
  ]
  edge [
    source 89
    target 750
  ]
  edge [
    source 89
    target 751
  ]
  edge [
    source 89
    target 752
  ]
  edge [
    source 89
    target 753
  ]
  edge [
    source 89
    target 754
  ]
  edge [
    source 89
    target 740
  ]
  edge [
    source 90
    target 3601
  ]
  edge [
    source 90
    target 620
  ]
  edge [
    source 90
    target 645
  ]
  edge [
    source 90
    target 3602
  ]
  edge [
    source 90
    target 3603
  ]
  edge [
    source 90
    target 711
  ]
  edge [
    source 90
    target 3604
  ]
  edge [
    source 90
    target 3605
  ]
  edge [
    source 90
    target 3606
  ]
  edge [
    source 90
    target 3607
  ]
  edge [
    source 90
    target 3608
  ]
  edge [
    source 90
    target 3609
  ]
  edge [
    source 90
    target 3610
  ]
  edge [
    source 90
    target 2629
  ]
  edge [
    source 90
    target 3611
  ]
  edge [
    source 90
    target 827
  ]
  edge [
    source 90
    target 3612
  ]
]
