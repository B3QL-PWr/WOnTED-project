graph [
  node [
    id 0
    label "nieudacznik"
    origin "text"
  ]
  node [
    id 1
    label "izraelski"
    origin "text"
  ]
  node [
    id 2
    label "mossadu"
    origin "text"
  ]
  node [
    id 3
    label "miernota"
  ]
  node [
    id 4
    label "oferma"
  ]
  node [
    id 5
    label "przegraniec"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "istota_&#380;ywa"
  ]
  node [
    id 8
    label "po&#347;miewisko"
  ]
  node [
    id 9
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 10
    label "dupa_wo&#322;owa"
  ]
  node [
    id 11
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 12
    label "jako&#347;&#263;"
  ]
  node [
    id 13
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 14
    label "tandetno&#347;&#263;"
  ]
  node [
    id 15
    label "parias"
  ]
  node [
    id 16
    label "po_izraelsku"
  ]
  node [
    id 17
    label "moszaw"
  ]
  node [
    id 18
    label "azjatycki"
  ]
  node [
    id 19
    label "bliskowschodni"
  ]
  node [
    id 20
    label "zachodnioazjatycki"
  ]
  node [
    id 21
    label "amba"
  ]
  node [
    id 22
    label "po_bliskowschodniemu"
  ]
  node [
    id 23
    label "ka&#322;mucki"
  ]
  node [
    id 24
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 25
    label "kampong"
  ]
  node [
    id 26
    label "balut"
  ]
  node [
    id 27
    label "typowy"
  ]
  node [
    id 28
    label "charakterystyczny"
  ]
  node [
    id 29
    label "ghaty"
  ]
  node [
    id 30
    label "azjatycko"
  ]
  node [
    id 31
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 32
    label "&#380;ydowski"
  ]
  node [
    id 33
    label "Palestyna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
]
