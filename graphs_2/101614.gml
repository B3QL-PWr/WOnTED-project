graph [
  node [
    id 0
    label "zwyci&#281;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 1
    label "jeden"
    origin "text"
  ]
  node [
    id 2
    label "poradzi&#263;_sobie"
  ]
  node [
    id 3
    label "znie&#347;&#263;"
  ]
  node [
    id 4
    label "zdecydowa&#263;"
  ]
  node [
    id 5
    label "score"
  ]
  node [
    id 6
    label "zrobi&#263;"
  ]
  node [
    id 7
    label "rule"
  ]
  node [
    id 8
    label "zwojowa&#263;"
  ]
  node [
    id 9
    label "overwhelm"
  ]
  node [
    id 10
    label "post&#261;pi&#263;"
  ]
  node [
    id 11
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 12
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 13
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 14
    label "zorganizowa&#263;"
  ]
  node [
    id 15
    label "appoint"
  ]
  node [
    id 16
    label "wystylizowa&#263;"
  ]
  node [
    id 17
    label "cause"
  ]
  node [
    id 18
    label "przerobi&#263;"
  ]
  node [
    id 19
    label "nabra&#263;"
  ]
  node [
    id 20
    label "make"
  ]
  node [
    id 21
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 22
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 23
    label "wydali&#263;"
  ]
  node [
    id 24
    label "sta&#263;_si&#281;"
  ]
  node [
    id 25
    label "podj&#261;&#263;"
  ]
  node [
    id 26
    label "decide"
  ]
  node [
    id 27
    label "determine"
  ]
  node [
    id 28
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 29
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 30
    label "wygra&#263;"
  ]
  node [
    id 31
    label "zgromadzi&#263;"
  ]
  node [
    id 32
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 33
    label "float"
  ]
  node [
    id 34
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 35
    label "revoke"
  ]
  node [
    id 36
    label "ranny"
  ]
  node [
    id 37
    label "usun&#261;&#263;"
  ]
  node [
    id 38
    label "zebra&#263;"
  ]
  node [
    id 39
    label "wytrzyma&#263;"
  ]
  node [
    id 40
    label "digest"
  ]
  node [
    id 41
    label "lift"
  ]
  node [
    id 42
    label "podda&#263;_si&#281;"
  ]
  node [
    id 43
    label "przenie&#347;&#263;"
  ]
  node [
    id 44
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 45
    label "&#347;cierpie&#263;"
  ]
  node [
    id 46
    label "porwa&#263;"
  ]
  node [
    id 47
    label "abolicjonista"
  ]
  node [
    id 48
    label "raise"
  ]
  node [
    id 49
    label "zniszczy&#263;"
  ]
  node [
    id 50
    label "shot"
  ]
  node [
    id 51
    label "jednakowy"
  ]
  node [
    id 52
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 53
    label "ujednolicenie"
  ]
  node [
    id 54
    label "jaki&#347;"
  ]
  node [
    id 55
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 56
    label "jednolicie"
  ]
  node [
    id 57
    label "kieliszek"
  ]
  node [
    id 58
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 59
    label "w&#243;dka"
  ]
  node [
    id 60
    label "ten"
  ]
  node [
    id 61
    label "szk&#322;o"
  ]
  node [
    id 62
    label "zawarto&#347;&#263;"
  ]
  node [
    id 63
    label "naczynie"
  ]
  node [
    id 64
    label "alkohol"
  ]
  node [
    id 65
    label "sznaps"
  ]
  node [
    id 66
    label "nap&#243;j"
  ]
  node [
    id 67
    label "gorza&#322;ka"
  ]
  node [
    id 68
    label "mohorycz"
  ]
  node [
    id 69
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 70
    label "mundurowanie"
  ]
  node [
    id 71
    label "zr&#243;wnanie"
  ]
  node [
    id 72
    label "taki&#380;"
  ]
  node [
    id 73
    label "mundurowa&#263;"
  ]
  node [
    id 74
    label "jednakowo"
  ]
  node [
    id 75
    label "zr&#243;wnywanie"
  ]
  node [
    id 76
    label "identyczny"
  ]
  node [
    id 77
    label "okre&#347;lony"
  ]
  node [
    id 78
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 79
    label "z&#322;o&#380;ony"
  ]
  node [
    id 80
    label "przyzwoity"
  ]
  node [
    id 81
    label "ciekawy"
  ]
  node [
    id 82
    label "jako&#347;"
  ]
  node [
    id 83
    label "jako_tako"
  ]
  node [
    id 84
    label "niez&#322;y"
  ]
  node [
    id 85
    label "dziwny"
  ]
  node [
    id 86
    label "charakterystyczny"
  ]
  node [
    id 87
    label "g&#322;&#281;bszy"
  ]
  node [
    id 88
    label "drink"
  ]
  node [
    id 89
    label "jednolity"
  ]
  node [
    id 90
    label "upodobnienie"
  ]
  node [
    id 91
    label "calibration"
  ]
  node [
    id 92
    label "Fejdolosa"
  ]
  node [
    id 93
    label "Koryntianin"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 92
    target 93
  ]
]
