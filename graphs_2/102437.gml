graph [
  node [
    id 0
    label "m&#322;ody"
    origin "text"
  ]
  node [
    id 1
    label "media"
    origin "text"
  ]
  node [
    id 2
    label "premier"
    origin "text"
  ]
  node [
    id 3
    label "raport"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "wczesny"
  ]
  node [
    id 6
    label "nowo&#380;eniec"
  ]
  node [
    id 7
    label "nowy"
  ]
  node [
    id 8
    label "nie&#380;onaty"
  ]
  node [
    id 9
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 10
    label "charakterystyczny"
  ]
  node [
    id 11
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 12
    label "m&#261;&#380;"
  ]
  node [
    id 13
    label "m&#322;odo"
  ]
  node [
    id 14
    label "pa&#324;stwo"
  ]
  node [
    id 15
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 16
    label "andropauza"
  ]
  node [
    id 17
    label "twardziel"
  ]
  node [
    id 18
    label "jegomo&#347;&#263;"
  ]
  node [
    id 19
    label "doros&#322;y"
  ]
  node [
    id 20
    label "ojciec"
  ]
  node [
    id 21
    label "samiec"
  ]
  node [
    id 22
    label "androlog"
  ]
  node [
    id 23
    label "bratek"
  ]
  node [
    id 24
    label "ch&#322;opina"
  ]
  node [
    id 25
    label "pocz&#261;tkowy"
  ]
  node [
    id 26
    label "wcze&#347;nie"
  ]
  node [
    id 27
    label "wyj&#261;tkowy"
  ]
  node [
    id 28
    label "podobny"
  ]
  node [
    id 29
    label "typowy"
  ]
  node [
    id 30
    label "charakterystycznie"
  ]
  node [
    id 31
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 32
    label "szczeg&#243;lny"
  ]
  node [
    id 33
    label "nowotny"
  ]
  node [
    id 34
    label "drugi"
  ]
  node [
    id 35
    label "narybek"
  ]
  node [
    id 36
    label "obcy"
  ]
  node [
    id 37
    label "nowo"
  ]
  node [
    id 38
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 39
    label "bie&#380;&#261;cy"
  ]
  node [
    id 40
    label "kolejny"
  ]
  node [
    id 41
    label "asymilowa&#263;"
  ]
  node [
    id 42
    label "nasada"
  ]
  node [
    id 43
    label "profanum"
  ]
  node [
    id 44
    label "wz&#243;r"
  ]
  node [
    id 45
    label "senior"
  ]
  node [
    id 46
    label "asymilowanie"
  ]
  node [
    id 47
    label "os&#322;abia&#263;"
  ]
  node [
    id 48
    label "homo_sapiens"
  ]
  node [
    id 49
    label "osoba"
  ]
  node [
    id 50
    label "ludzko&#347;&#263;"
  ]
  node [
    id 51
    label "Adam"
  ]
  node [
    id 52
    label "hominid"
  ]
  node [
    id 53
    label "posta&#263;"
  ]
  node [
    id 54
    label "portrecista"
  ]
  node [
    id 55
    label "polifag"
  ]
  node [
    id 56
    label "podw&#322;adny"
  ]
  node [
    id 57
    label "dwun&#243;g"
  ]
  node [
    id 58
    label "wapniak"
  ]
  node [
    id 59
    label "duch"
  ]
  node [
    id 60
    label "os&#322;abianie"
  ]
  node [
    id 61
    label "antropochoria"
  ]
  node [
    id 62
    label "figura"
  ]
  node [
    id 63
    label "g&#322;owa"
  ]
  node [
    id 64
    label "mikrokosmos"
  ]
  node [
    id 65
    label "oddzia&#322;ywanie"
  ]
  node [
    id 66
    label "samotny"
  ]
  node [
    id 67
    label "pan_m&#322;ody"
  ]
  node [
    id 68
    label "pan_i_w&#322;adca"
  ]
  node [
    id 69
    label "ch&#322;op"
  ]
  node [
    id 70
    label "&#347;lubny"
  ]
  node [
    id 71
    label "m&#243;j"
  ]
  node [
    id 72
    label "pan_domu"
  ]
  node [
    id 73
    label "ma&#322;&#380;onek"
  ]
  node [
    id 74
    label "stary"
  ]
  node [
    id 75
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 76
    label "nowo&#380;e&#324;cy"
  ]
  node [
    id 77
    label "m&#322;odo&#380;eniec"
  ]
  node [
    id 78
    label "&#380;onko&#347;"
  ]
  node [
    id 79
    label "uzbrajanie"
  ]
  node [
    id 80
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 81
    label "mass-media"
  ]
  node [
    id 82
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 83
    label "medium"
  ]
  node [
    id 84
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 85
    label "przekazior"
  ]
  node [
    id 86
    label "hipnoza"
  ]
  node [
    id 87
    label "&#347;rodek"
  ]
  node [
    id 88
    label "warunki"
  ]
  node [
    id 89
    label "publikator"
  ]
  node [
    id 90
    label "spirytysta"
  ]
  node [
    id 91
    label "strona"
  ]
  node [
    id 92
    label "otoczenie"
  ]
  node [
    id 93
    label "jasnowidz"
  ]
  node [
    id 94
    label "&#347;rodek_przekazu"
  ]
  node [
    id 95
    label "przeka&#378;nik"
  ]
  node [
    id 96
    label "arming"
  ]
  node [
    id 97
    label "wyposa&#380;anie"
  ]
  node [
    id 98
    label "dozbrojenie"
  ]
  node [
    id 99
    label "armament"
  ]
  node [
    id 100
    label "dozbrajanie"
  ]
  node [
    id 101
    label "instalacja"
  ]
  node [
    id 102
    label "montowanie"
  ]
  node [
    id 103
    label "Sto&#322;ypin"
  ]
  node [
    id 104
    label "Bismarck"
  ]
  node [
    id 105
    label "Jelcyn"
  ]
  node [
    id 106
    label "zwierzchnik"
  ]
  node [
    id 107
    label "Miko&#322;ajczyk"
  ]
  node [
    id 108
    label "rz&#261;d"
  ]
  node [
    id 109
    label "Chruszczow"
  ]
  node [
    id 110
    label "dostojnik"
  ]
  node [
    id 111
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 112
    label "kierowa&#263;"
  ]
  node [
    id 113
    label "pryncypa&#322;"
  ]
  node [
    id 114
    label "kierownictwo"
  ]
  node [
    id 115
    label "w&#322;adza"
  ]
  node [
    id 116
    label "urz&#281;dnik"
  ]
  node [
    id 117
    label "oficja&#322;"
  ]
  node [
    id 118
    label "notabl"
  ]
  node [
    id 119
    label "szpaler"
  ]
  node [
    id 120
    label "number"
  ]
  node [
    id 121
    label "Londyn"
  ]
  node [
    id 122
    label "przybli&#380;enie"
  ]
  node [
    id 123
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 124
    label "tract"
  ]
  node [
    id 125
    label "uporz&#261;dkowanie"
  ]
  node [
    id 126
    label "egzekutywa"
  ]
  node [
    id 127
    label "klasa"
  ]
  node [
    id 128
    label "Konsulat"
  ]
  node [
    id 129
    label "gabinet_cieni"
  ]
  node [
    id 130
    label "lon&#380;a"
  ]
  node [
    id 131
    label "gromada"
  ]
  node [
    id 132
    label "jednostka_systematyczna"
  ]
  node [
    id 133
    label "kategoria"
  ]
  node [
    id 134
    label "instytucja"
  ]
  node [
    id 135
    label "relacja"
  ]
  node [
    id 136
    label "statement"
  ]
  node [
    id 137
    label "raport_Beveridge'a"
  ]
  node [
    id 138
    label "raport_Fischlera"
  ]
  node [
    id 139
    label "zwi&#261;zanie"
  ]
  node [
    id 140
    label "ustosunkowywa&#263;"
  ]
  node [
    id 141
    label "podzbi&#243;r"
  ]
  node [
    id 142
    label "zwi&#261;zek"
  ]
  node [
    id 143
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 144
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 145
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 146
    label "marriage"
  ]
  node [
    id 147
    label "bratnia_dusza"
  ]
  node [
    id 148
    label "ustosunkowywanie"
  ]
  node [
    id 149
    label "zwi&#261;za&#263;"
  ]
  node [
    id 150
    label "sprawko"
  ]
  node [
    id 151
    label "korespondent"
  ]
  node [
    id 152
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 153
    label "wi&#261;zanie"
  ]
  node [
    id 154
    label "message"
  ]
  node [
    id 155
    label "trasa"
  ]
  node [
    id 156
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 157
    label "ustosunkowanie"
  ]
  node [
    id 158
    label "ustosunkowa&#263;"
  ]
  node [
    id 159
    label "wypowied&#378;"
  ]
  node [
    id 160
    label "zwi&#261;za&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
]
