graph [
  node [
    id 0
    label "j&#243;zef"
    origin "text"
  ]
  node [
    id 1
    label "sare"
    origin "text"
  ]
  node [
    id 2
    label "J&#243;zefa"
  ]
  node [
    id 3
    label "Sare"
  ]
  node [
    id 4
    label "sejm"
  ]
  node [
    id 5
    label "krajowy"
  ]
  node [
    id 6
    label "danieli"
  ]
  node [
    id 7
    label "zeszyt"
  ]
  node [
    id 8
    label "Kleinberger&#243;w"
  ]
  node [
    id 9
    label "i"
  ]
  node [
    id 10
    label "wojna"
  ]
  node [
    id 11
    label "&#347;wiatowy"
  ]
  node [
    id 12
    label "naczelny"
  ]
  node [
    id 13
    label "komitet"
  ]
  node [
    id 14
    label "narodowy"
  ]
  node [
    id 15
    label "Henryka"
  ]
  node [
    id 16
    label "Szarskiego"
  ]
  node [
    id 17
    label "Juliusz"
  ]
  node [
    id 18
    label "Leo"
  ]
  node [
    id 19
    label "Haller"
  ]
  node [
    id 20
    label "Jan"
  ]
  node [
    id 21
    label "Kanty"
  ]
  node [
    id 22
    label "Federowicza"
  ]
  node [
    id 23
    label "Karol"
  ]
  node [
    id 24
    label "Rollego"
  ]
  node [
    id 25
    label "Witolda"
  ]
  node [
    id 26
    label "ostrowski"
  ]
  node [
    id 27
    label "rada"
  ]
  node [
    id 28
    label "zjednoczy&#263;"
  ]
  node [
    id 29
    label "polak"
  ]
  node [
    id 30
    label "wyzna&#263;"
  ]
  node [
    id 31
    label "moj&#380;eszowy"
  ]
  node [
    id 32
    label "ziemia"
  ]
  node [
    id 33
    label "polskie"
  ]
  node [
    id 34
    label "klinika"
  ]
  node [
    id 35
    label "psychiatryczny"
  ]
  node [
    id 36
    label "collegium"
  ]
  node [
    id 37
    label "Medicum"
  ]
  node [
    id 38
    label "chirurgiczny"
  ]
  node [
    id 39
    label "okulistyczny"
  ]
  node [
    id 40
    label "gimnazjum"
  ]
  node [
    id 41
    label "on"
  ]
  node [
    id 42
    label "Sobieski"
  ]
  node [
    id 43
    label "liceum"
  ]
  node [
    id 44
    label "by&#322;y"
  ]
  node [
    id 45
    label "nowodworski"
  ]
  node [
    id 46
    label "plac"
  ]
  node [
    id 47
    label "na"
  ]
  node [
    id 48
    label "grobla"
  ]
  node [
    id 49
    label "lasa"
  ]
  node [
    id 50
    label "wolski"
  ]
  node [
    id 51
    label "muzeum"
  ]
  node [
    id 52
    label "Jack"
  ]
  node [
    id 53
    label "Malczewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 52
    target 53
  ]
]
