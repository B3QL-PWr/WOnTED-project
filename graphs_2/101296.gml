graph [
  node [
    id 0
    label "dzwonek"
    origin "text"
  ]
  node [
    id 1
    label "teatralny"
    origin "text"
  ]
  node [
    id 2
    label "zadzwoni&#263;"
  ]
  node [
    id 3
    label "&#322;&#261;cznik_instalacyjny"
  ]
  node [
    id 4
    label "ro&#347;lina_zielna"
  ]
  node [
    id 5
    label "kolor"
  ]
  node [
    id 6
    label "dzwoni&#263;"
  ]
  node [
    id 7
    label "dzwonkowate"
  ]
  node [
    id 8
    label "karo"
  ]
  node [
    id 9
    label "sygna&#322;_d&#378;wi&#281;kowy"
  ]
  node [
    id 10
    label "dzwonienie"
  ]
  node [
    id 11
    label "przycisk"
  ]
  node [
    id 12
    label "campanula"
  ]
  node [
    id 13
    label "sygnalizator"
  ]
  node [
    id 14
    label "karta"
  ]
  node [
    id 15
    label "kartka"
  ]
  node [
    id 16
    label "danie"
  ]
  node [
    id 17
    label "menu"
  ]
  node [
    id 18
    label "zezwolenie"
  ]
  node [
    id 19
    label "restauracja"
  ]
  node [
    id 20
    label "chart"
  ]
  node [
    id 21
    label "p&#322;ytka"
  ]
  node [
    id 22
    label "formularz"
  ]
  node [
    id 23
    label "ticket"
  ]
  node [
    id 24
    label "cennik"
  ]
  node [
    id 25
    label "oferta"
  ]
  node [
    id 26
    label "komputer"
  ]
  node [
    id 27
    label "charter"
  ]
  node [
    id 28
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 29
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 30
    label "kartonik"
  ]
  node [
    id 31
    label "urz&#261;dzenie"
  ]
  node [
    id 32
    label "circuit_board"
  ]
  node [
    id 33
    label "liczba_kwantowa"
  ]
  node [
    id 34
    label "&#347;wieci&#263;"
  ]
  node [
    id 35
    label "poker"
  ]
  node [
    id 36
    label "cecha"
  ]
  node [
    id 37
    label "ubarwienie"
  ]
  node [
    id 38
    label "blakn&#261;&#263;"
  ]
  node [
    id 39
    label "struktura"
  ]
  node [
    id 40
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 41
    label "zblakni&#281;cie"
  ]
  node [
    id 42
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 43
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 44
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 45
    label "prze&#322;amanie"
  ]
  node [
    id 46
    label "prze&#322;amywanie"
  ]
  node [
    id 47
    label "&#347;wiecenie"
  ]
  node [
    id 48
    label "prze&#322;ama&#263;"
  ]
  node [
    id 49
    label "zblakn&#261;&#263;"
  ]
  node [
    id 50
    label "symbol"
  ]
  node [
    id 51
    label "blakni&#281;cie"
  ]
  node [
    id 52
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 53
    label "interfejs"
  ]
  node [
    id 54
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 55
    label "pole"
  ]
  node [
    id 56
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 57
    label "nacisk"
  ]
  node [
    id 58
    label "wymowa"
  ]
  node [
    id 59
    label "d&#378;wi&#281;k"
  ]
  node [
    id 60
    label "przedmiot"
  ]
  node [
    id 61
    label "indicator"
  ]
  node [
    id 62
    label "dekolt"
  ]
  node [
    id 63
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 64
    label "jingle"
  ]
  node [
    id 65
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 66
    label "wydzwanianie"
  ]
  node [
    id 67
    label "naciskanie"
  ]
  node [
    id 68
    label "sound"
  ]
  node [
    id 69
    label "telefon"
  ]
  node [
    id 70
    label "brzmienie"
  ]
  node [
    id 71
    label "wybijanie"
  ]
  node [
    id 72
    label "dryndanie"
  ]
  node [
    id 73
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 74
    label "wydzwonienie"
  ]
  node [
    id 75
    label "call"
  ]
  node [
    id 76
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 78
    label "zabi&#263;"
  ]
  node [
    id 79
    label "zadrynda&#263;"
  ]
  node [
    id 80
    label "zabrzmie&#263;"
  ]
  node [
    id 81
    label "nacisn&#261;&#263;"
  ]
  node [
    id 82
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 83
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 84
    label "bi&#263;"
  ]
  node [
    id 85
    label "brzmie&#263;"
  ]
  node [
    id 86
    label "drynda&#263;"
  ]
  node [
    id 87
    label "brz&#281;cze&#263;"
  ]
  node [
    id 88
    label "astrowce"
  ]
  node [
    id 89
    label "Campanulaceae"
  ]
  node [
    id 90
    label "nienaturalny"
  ]
  node [
    id 91
    label "teatralnie"
  ]
  node [
    id 92
    label "nadmierny"
  ]
  node [
    id 93
    label "nadmiernie"
  ]
  node [
    id 94
    label "artystycznie"
  ]
  node [
    id 95
    label "stagily"
  ]
  node [
    id 96
    label "theatrically"
  ]
  node [
    id 97
    label "nienaturalnie"
  ]
  node [
    id 98
    label "niespotykany"
  ]
  node [
    id 99
    label "sztucznie"
  ]
  node [
    id 100
    label "niepodobny"
  ]
  node [
    id 101
    label "nieprzekonuj&#261;cy"
  ]
  node [
    id 102
    label "nienormalny"
  ]
  node [
    id 103
    label "nietypowy"
  ]
  node [
    id 104
    label "dziwny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
]
