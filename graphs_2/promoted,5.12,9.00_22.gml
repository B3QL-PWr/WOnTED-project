graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "temat"
    origin "text"
  ]
  node [
    id 2
    label "sprawa"
  ]
  node [
    id 3
    label "wyraz_pochodny"
  ]
  node [
    id 4
    label "zboczenie"
  ]
  node [
    id 5
    label "om&#243;wienie"
  ]
  node [
    id 6
    label "cecha"
  ]
  node [
    id 7
    label "rzecz"
  ]
  node [
    id 8
    label "omawia&#263;"
  ]
  node [
    id 9
    label "fraza"
  ]
  node [
    id 10
    label "tre&#347;&#263;"
  ]
  node [
    id 11
    label "entity"
  ]
  node [
    id 12
    label "forum"
  ]
  node [
    id 13
    label "topik"
  ]
  node [
    id 14
    label "tematyka"
  ]
  node [
    id 15
    label "w&#261;tek"
  ]
  node [
    id 16
    label "zbaczanie"
  ]
  node [
    id 17
    label "forma"
  ]
  node [
    id 18
    label "om&#243;wi&#263;"
  ]
  node [
    id 19
    label "omawianie"
  ]
  node [
    id 20
    label "melodia"
  ]
  node [
    id 21
    label "otoczka"
  ]
  node [
    id 22
    label "istota"
  ]
  node [
    id 23
    label "zbacza&#263;"
  ]
  node [
    id 24
    label "zboczy&#263;"
  ]
  node [
    id 25
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "wypowiedzenie"
  ]
  node [
    id 28
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 29
    label "zdanie"
  ]
  node [
    id 30
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 31
    label "motyw"
  ]
  node [
    id 32
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 33
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 34
    label "informacja"
  ]
  node [
    id 35
    label "zawarto&#347;&#263;"
  ]
  node [
    id 36
    label "kszta&#322;t"
  ]
  node [
    id 37
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 38
    label "jednostka_systematyczna"
  ]
  node [
    id 39
    label "poznanie"
  ]
  node [
    id 40
    label "leksem"
  ]
  node [
    id 41
    label "dzie&#322;o"
  ]
  node [
    id 42
    label "stan"
  ]
  node [
    id 43
    label "blaszka"
  ]
  node [
    id 44
    label "poj&#281;cie"
  ]
  node [
    id 45
    label "kantyzm"
  ]
  node [
    id 46
    label "zdolno&#347;&#263;"
  ]
  node [
    id 47
    label "do&#322;ek"
  ]
  node [
    id 48
    label "gwiazda"
  ]
  node [
    id 49
    label "formality"
  ]
  node [
    id 50
    label "struktura"
  ]
  node [
    id 51
    label "wygl&#261;d"
  ]
  node [
    id 52
    label "mode"
  ]
  node [
    id 53
    label "morfem"
  ]
  node [
    id 54
    label "rdze&#324;"
  ]
  node [
    id 55
    label "posta&#263;"
  ]
  node [
    id 56
    label "kielich"
  ]
  node [
    id 57
    label "ornamentyka"
  ]
  node [
    id 58
    label "pasmo"
  ]
  node [
    id 59
    label "zwyczaj"
  ]
  node [
    id 60
    label "punkt_widzenia"
  ]
  node [
    id 61
    label "g&#322;owa"
  ]
  node [
    id 62
    label "naczynie"
  ]
  node [
    id 63
    label "p&#322;at"
  ]
  node [
    id 64
    label "maszyna_drukarska"
  ]
  node [
    id 65
    label "obiekt"
  ]
  node [
    id 66
    label "style"
  ]
  node [
    id 67
    label "linearno&#347;&#263;"
  ]
  node [
    id 68
    label "wyra&#380;enie"
  ]
  node [
    id 69
    label "formacja"
  ]
  node [
    id 70
    label "spirala"
  ]
  node [
    id 71
    label "dyspozycja"
  ]
  node [
    id 72
    label "odmiana"
  ]
  node [
    id 73
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 74
    label "wz&#243;r"
  ]
  node [
    id 75
    label "October"
  ]
  node [
    id 76
    label "creation"
  ]
  node [
    id 77
    label "p&#281;tla"
  ]
  node [
    id 78
    label "arystotelizm"
  ]
  node [
    id 79
    label "szablon"
  ]
  node [
    id 80
    label "miniatura"
  ]
  node [
    id 81
    label "zanucenie"
  ]
  node [
    id 82
    label "nuta"
  ]
  node [
    id 83
    label "zakosztowa&#263;"
  ]
  node [
    id 84
    label "zajawka"
  ]
  node [
    id 85
    label "zanuci&#263;"
  ]
  node [
    id 86
    label "emocja"
  ]
  node [
    id 87
    label "oskoma"
  ]
  node [
    id 88
    label "melika"
  ]
  node [
    id 89
    label "nucenie"
  ]
  node [
    id 90
    label "nuci&#263;"
  ]
  node [
    id 91
    label "brzmienie"
  ]
  node [
    id 92
    label "zjawisko"
  ]
  node [
    id 93
    label "taste"
  ]
  node [
    id 94
    label "muzyka"
  ]
  node [
    id 95
    label "inclination"
  ]
  node [
    id 96
    label "charakterystyka"
  ]
  node [
    id 97
    label "m&#322;ot"
  ]
  node [
    id 98
    label "znak"
  ]
  node [
    id 99
    label "drzewo"
  ]
  node [
    id 100
    label "pr&#243;ba"
  ]
  node [
    id 101
    label "attribute"
  ]
  node [
    id 102
    label "marka"
  ]
  node [
    id 103
    label "mentalno&#347;&#263;"
  ]
  node [
    id 104
    label "superego"
  ]
  node [
    id 105
    label "psychika"
  ]
  node [
    id 106
    label "znaczenie"
  ]
  node [
    id 107
    label "wn&#281;trze"
  ]
  node [
    id 108
    label "charakter"
  ]
  node [
    id 109
    label "matter"
  ]
  node [
    id 110
    label "splot"
  ]
  node [
    id 111
    label "wytw&#243;r"
  ]
  node [
    id 112
    label "ceg&#322;a"
  ]
  node [
    id 113
    label "socket"
  ]
  node [
    id 114
    label "rozmieszczenie"
  ]
  node [
    id 115
    label "fabu&#322;a"
  ]
  node [
    id 116
    label "okrywa"
  ]
  node [
    id 117
    label "kontekst"
  ]
  node [
    id 118
    label "object"
  ]
  node [
    id 119
    label "przedmiot"
  ]
  node [
    id 120
    label "wpadni&#281;cie"
  ]
  node [
    id 121
    label "mienie"
  ]
  node [
    id 122
    label "przyroda"
  ]
  node [
    id 123
    label "kultura"
  ]
  node [
    id 124
    label "wpa&#347;&#263;"
  ]
  node [
    id 125
    label "wpadanie"
  ]
  node [
    id 126
    label "wpada&#263;"
  ]
  node [
    id 127
    label "discussion"
  ]
  node [
    id 128
    label "rozpatrywanie"
  ]
  node [
    id 129
    label "dyskutowanie"
  ]
  node [
    id 130
    label "omowny"
  ]
  node [
    id 131
    label "figura_stylistyczna"
  ]
  node [
    id 132
    label "sformu&#322;owanie"
  ]
  node [
    id 133
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 134
    label "odchodzenie"
  ]
  node [
    id 135
    label "aberrance"
  ]
  node [
    id 136
    label "swerve"
  ]
  node [
    id 137
    label "kierunek"
  ]
  node [
    id 138
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 139
    label "distract"
  ]
  node [
    id 140
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 141
    label "odej&#347;&#263;"
  ]
  node [
    id 142
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 143
    label "twist"
  ]
  node [
    id 144
    label "zmieni&#263;"
  ]
  node [
    id 145
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 146
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 147
    label "przedyskutowa&#263;"
  ]
  node [
    id 148
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 149
    label "publicize"
  ]
  node [
    id 150
    label "digress"
  ]
  node [
    id 151
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 152
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 153
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 154
    label "odchodzi&#263;"
  ]
  node [
    id 155
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 156
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 157
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 158
    label "perversion"
  ]
  node [
    id 159
    label "death"
  ]
  node [
    id 160
    label "odej&#347;cie"
  ]
  node [
    id 161
    label "turn"
  ]
  node [
    id 162
    label "k&#261;t"
  ]
  node [
    id 163
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 164
    label "odchylenie_si&#281;"
  ]
  node [
    id 165
    label "deviation"
  ]
  node [
    id 166
    label "patologia"
  ]
  node [
    id 167
    label "dyskutowa&#263;"
  ]
  node [
    id 168
    label "formu&#322;owa&#263;"
  ]
  node [
    id 169
    label "discourse"
  ]
  node [
    id 170
    label "kognicja"
  ]
  node [
    id 171
    label "rozprawa"
  ]
  node [
    id 172
    label "wydarzenie"
  ]
  node [
    id 173
    label "szczeg&#243;&#322;"
  ]
  node [
    id 174
    label "proposition"
  ]
  node [
    id 175
    label "przes&#322;anka"
  ]
  node [
    id 176
    label "idea"
  ]
  node [
    id 177
    label "paj&#261;k"
  ]
  node [
    id 178
    label "przewodnik"
  ]
  node [
    id 179
    label "odcinek"
  ]
  node [
    id 180
    label "topikowate"
  ]
  node [
    id 181
    label "grupa_dyskusyjna"
  ]
  node [
    id 182
    label "s&#261;d"
  ]
  node [
    id 183
    label "plac"
  ]
  node [
    id 184
    label "bazylika"
  ]
  node [
    id 185
    label "przestrze&#324;"
  ]
  node [
    id 186
    label "miejsce"
  ]
  node [
    id 187
    label "portal"
  ]
  node [
    id 188
    label "konferencja"
  ]
  node [
    id 189
    label "agora"
  ]
  node [
    id 190
    label "grupa"
  ]
  node [
    id 191
    label "strona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
]
