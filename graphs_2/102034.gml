graph [
  node [
    id 0
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "podstawowy"
    origin "text"
  ]
  node [
    id 2
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "swoje"
    origin "text"
  ]
  node [
    id 4
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 5
    label "k&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 6
    label "teatralny"
    origin "text"
  ]
  node [
    id 7
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 8
    label "j&#281;zykowy"
    origin "text"
  ]
  node [
    id 9
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 10
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "radio"
    origin "text"
  ]
  node [
    id 12
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 13
    label "lista"
    origin "text"
  ]
  node [
    id 14
    label "przeboje"
    origin "text"
  ]
  node [
    id 15
    label "adam"
    origin "text"
  ]
  node [
    id 16
    label "ko&#322;aci&#324;ski"
    origin "text"
  ]
  node [
    id 17
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 20
    label "podszewka"
    origin "text"
  ]
  node [
    id 21
    label "zainteresowania"
    origin "text"
  ]
  node [
    id 22
    label "humanistyczny"
    origin "text"
  ]
  node [
    id 23
    label "przeszkodzi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "uko&#324;czenie"
    origin "text"
  ]
  node [
    id 25
    label "studia"
    origin "text"
  ]
  node [
    id 26
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 27
    label "do&#347;wiadczenie"
  ]
  node [
    id 28
    label "teren_szko&#322;y"
  ]
  node [
    id 29
    label "wiedza"
  ]
  node [
    id 30
    label "Mickiewicz"
  ]
  node [
    id 31
    label "kwalifikacje"
  ]
  node [
    id 32
    label "podr&#281;cznik"
  ]
  node [
    id 33
    label "absolwent"
  ]
  node [
    id 34
    label "praktyka"
  ]
  node [
    id 35
    label "school"
  ]
  node [
    id 36
    label "system"
  ]
  node [
    id 37
    label "zda&#263;"
  ]
  node [
    id 38
    label "gabinet"
  ]
  node [
    id 39
    label "urszulanki"
  ]
  node [
    id 40
    label "sztuba"
  ]
  node [
    id 41
    label "&#322;awa_szkolna"
  ]
  node [
    id 42
    label "nauka"
  ]
  node [
    id 43
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 44
    label "przepisa&#263;"
  ]
  node [
    id 45
    label "muzyka"
  ]
  node [
    id 46
    label "grupa"
  ]
  node [
    id 47
    label "form"
  ]
  node [
    id 48
    label "klasa"
  ]
  node [
    id 49
    label "lekcja"
  ]
  node [
    id 50
    label "metoda"
  ]
  node [
    id 51
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 52
    label "przepisanie"
  ]
  node [
    id 53
    label "czas"
  ]
  node [
    id 54
    label "skolaryzacja"
  ]
  node [
    id 55
    label "zdanie"
  ]
  node [
    id 56
    label "stopek"
  ]
  node [
    id 57
    label "sekretariat"
  ]
  node [
    id 58
    label "ideologia"
  ]
  node [
    id 59
    label "lesson"
  ]
  node [
    id 60
    label "instytucja"
  ]
  node [
    id 61
    label "niepokalanki"
  ]
  node [
    id 62
    label "siedziba"
  ]
  node [
    id 63
    label "szkolenie"
  ]
  node [
    id 64
    label "kara"
  ]
  node [
    id 65
    label "tablica"
  ]
  node [
    id 66
    label "wyprawka"
  ]
  node [
    id 67
    label "pomoc_naukowa"
  ]
  node [
    id 68
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 69
    label "odm&#322;adzanie"
  ]
  node [
    id 70
    label "liga"
  ]
  node [
    id 71
    label "jednostka_systematyczna"
  ]
  node [
    id 72
    label "asymilowanie"
  ]
  node [
    id 73
    label "gromada"
  ]
  node [
    id 74
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 75
    label "asymilowa&#263;"
  ]
  node [
    id 76
    label "egzemplarz"
  ]
  node [
    id 77
    label "Entuzjastki"
  ]
  node [
    id 78
    label "zbi&#243;r"
  ]
  node [
    id 79
    label "kompozycja"
  ]
  node [
    id 80
    label "Terranie"
  ]
  node [
    id 81
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 82
    label "category"
  ]
  node [
    id 83
    label "pakiet_klimatyczny"
  ]
  node [
    id 84
    label "oddzia&#322;"
  ]
  node [
    id 85
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 86
    label "cz&#261;steczka"
  ]
  node [
    id 87
    label "stage_set"
  ]
  node [
    id 88
    label "type"
  ]
  node [
    id 89
    label "specgrupa"
  ]
  node [
    id 90
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 91
    label "&#346;wietliki"
  ]
  node [
    id 92
    label "odm&#322;odzenie"
  ]
  node [
    id 93
    label "Eurogrupa"
  ]
  node [
    id 94
    label "odm&#322;adza&#263;"
  ]
  node [
    id 95
    label "formacja_geologiczna"
  ]
  node [
    id 96
    label "harcerze_starsi"
  ]
  node [
    id 97
    label "course"
  ]
  node [
    id 98
    label "pomaganie"
  ]
  node [
    id 99
    label "training"
  ]
  node [
    id 100
    label "zapoznawanie"
  ]
  node [
    id 101
    label "seria"
  ]
  node [
    id 102
    label "zaj&#281;cia"
  ]
  node [
    id 103
    label "pouczenie"
  ]
  node [
    id 104
    label "o&#347;wiecanie"
  ]
  node [
    id 105
    label "Lira"
  ]
  node [
    id 106
    label "kliker"
  ]
  node [
    id 107
    label "miasteczko_rowerowe"
  ]
  node [
    id 108
    label "porada"
  ]
  node [
    id 109
    label "fotowoltaika"
  ]
  node [
    id 110
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 111
    label "przem&#243;wienie"
  ]
  node [
    id 112
    label "nauki_o_poznaniu"
  ]
  node [
    id 113
    label "nomotetyczny"
  ]
  node [
    id 114
    label "systematyka"
  ]
  node [
    id 115
    label "proces"
  ]
  node [
    id 116
    label "typologia"
  ]
  node [
    id 117
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 118
    label "kultura_duchowa"
  ]
  node [
    id 119
    label "nauki_penalne"
  ]
  node [
    id 120
    label "dziedzina"
  ]
  node [
    id 121
    label "imagineskopia"
  ]
  node [
    id 122
    label "teoria_naukowa"
  ]
  node [
    id 123
    label "inwentyka"
  ]
  node [
    id 124
    label "metodologia"
  ]
  node [
    id 125
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 126
    label "nauki_o_Ziemi"
  ]
  node [
    id 127
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 128
    label "eliminacje"
  ]
  node [
    id 129
    label "osoba_prawna"
  ]
  node [
    id 130
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 131
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 132
    label "poj&#281;cie"
  ]
  node [
    id 133
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 134
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 135
    label "biuro"
  ]
  node [
    id 136
    label "organizacja"
  ]
  node [
    id 137
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 138
    label "Fundusze_Unijne"
  ]
  node [
    id 139
    label "zamyka&#263;"
  ]
  node [
    id 140
    label "establishment"
  ]
  node [
    id 141
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 142
    label "urz&#261;d"
  ]
  node [
    id 143
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 144
    label "afiliowa&#263;"
  ]
  node [
    id 145
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 146
    label "standard"
  ]
  node [
    id 147
    label "zamykanie"
  ]
  node [
    id 148
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 149
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 150
    label "kwota"
  ]
  node [
    id 151
    label "nemezis"
  ]
  node [
    id 152
    label "konsekwencja"
  ]
  node [
    id 153
    label "punishment"
  ]
  node [
    id 154
    label "klacz"
  ]
  node [
    id 155
    label "forfeit"
  ]
  node [
    id 156
    label "roboty_przymusowe"
  ]
  node [
    id 157
    label "materia&#322;"
  ]
  node [
    id 158
    label "spos&#243;b"
  ]
  node [
    id 159
    label "obrz&#261;dek"
  ]
  node [
    id 160
    label "Biblia"
  ]
  node [
    id 161
    label "tekst"
  ]
  node [
    id 162
    label "lektor"
  ]
  node [
    id 163
    label "poprzedzanie"
  ]
  node [
    id 164
    label "czasoprzestrze&#324;"
  ]
  node [
    id 165
    label "laba"
  ]
  node [
    id 166
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 167
    label "chronometria"
  ]
  node [
    id 168
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 169
    label "rachuba_czasu"
  ]
  node [
    id 170
    label "przep&#322;ywanie"
  ]
  node [
    id 171
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 172
    label "czasokres"
  ]
  node [
    id 173
    label "odczyt"
  ]
  node [
    id 174
    label "chwila"
  ]
  node [
    id 175
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 176
    label "dzieje"
  ]
  node [
    id 177
    label "kategoria_gramatyczna"
  ]
  node [
    id 178
    label "poprzedzenie"
  ]
  node [
    id 179
    label "trawienie"
  ]
  node [
    id 180
    label "pochodzi&#263;"
  ]
  node [
    id 181
    label "period"
  ]
  node [
    id 182
    label "okres_czasu"
  ]
  node [
    id 183
    label "poprzedza&#263;"
  ]
  node [
    id 184
    label "schy&#322;ek"
  ]
  node [
    id 185
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 186
    label "odwlekanie_si&#281;"
  ]
  node [
    id 187
    label "zegar"
  ]
  node [
    id 188
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 189
    label "czwarty_wymiar"
  ]
  node [
    id 190
    label "pochodzenie"
  ]
  node [
    id 191
    label "koniugacja"
  ]
  node [
    id 192
    label "Zeitgeist"
  ]
  node [
    id 193
    label "trawi&#263;"
  ]
  node [
    id 194
    label "pogoda"
  ]
  node [
    id 195
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 196
    label "poprzedzi&#263;"
  ]
  node [
    id 197
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 198
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 199
    label "time_period"
  ]
  node [
    id 200
    label "practice"
  ]
  node [
    id 201
    label "znawstwo"
  ]
  node [
    id 202
    label "skill"
  ]
  node [
    id 203
    label "czyn"
  ]
  node [
    id 204
    label "zwyczaj"
  ]
  node [
    id 205
    label "eksperiencja"
  ]
  node [
    id 206
    label "praca"
  ]
  node [
    id 207
    label "j&#261;dro"
  ]
  node [
    id 208
    label "systemik"
  ]
  node [
    id 209
    label "rozprz&#261;c"
  ]
  node [
    id 210
    label "oprogramowanie"
  ]
  node [
    id 211
    label "systemat"
  ]
  node [
    id 212
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 213
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 214
    label "model"
  ]
  node [
    id 215
    label "struktura"
  ]
  node [
    id 216
    label "usenet"
  ]
  node [
    id 217
    label "s&#261;d"
  ]
  node [
    id 218
    label "porz&#261;dek"
  ]
  node [
    id 219
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 220
    label "przyn&#281;ta"
  ]
  node [
    id 221
    label "p&#322;&#243;d"
  ]
  node [
    id 222
    label "net"
  ]
  node [
    id 223
    label "w&#281;dkarstwo"
  ]
  node [
    id 224
    label "eratem"
  ]
  node [
    id 225
    label "doktryna"
  ]
  node [
    id 226
    label "pulpit"
  ]
  node [
    id 227
    label "konstelacja"
  ]
  node [
    id 228
    label "jednostka_geologiczna"
  ]
  node [
    id 229
    label "o&#347;"
  ]
  node [
    id 230
    label "podsystem"
  ]
  node [
    id 231
    label "ryba"
  ]
  node [
    id 232
    label "Leopard"
  ]
  node [
    id 233
    label "Android"
  ]
  node [
    id 234
    label "zachowanie"
  ]
  node [
    id 235
    label "cybernetyk"
  ]
  node [
    id 236
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 237
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 238
    label "method"
  ]
  node [
    id 239
    label "sk&#322;ad"
  ]
  node [
    id 240
    label "podstawa"
  ]
  node [
    id 241
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 242
    label "&#321;ubianka"
  ]
  node [
    id 243
    label "miejsce_pracy"
  ]
  node [
    id 244
    label "dzia&#322;_personalny"
  ]
  node [
    id 245
    label "Kreml"
  ]
  node [
    id 246
    label "Bia&#322;y_Dom"
  ]
  node [
    id 247
    label "budynek"
  ]
  node [
    id 248
    label "miejsce"
  ]
  node [
    id 249
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 250
    label "sadowisko"
  ]
  node [
    id 251
    label "wokalistyka"
  ]
  node [
    id 252
    label "przedmiot"
  ]
  node [
    id 253
    label "wykonywanie"
  ]
  node [
    id 254
    label "muza"
  ]
  node [
    id 255
    label "wykonywa&#263;"
  ]
  node [
    id 256
    label "zjawisko"
  ]
  node [
    id 257
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 258
    label "beatbox"
  ]
  node [
    id 259
    label "komponowa&#263;"
  ]
  node [
    id 260
    label "komponowanie"
  ]
  node [
    id 261
    label "wytw&#243;r"
  ]
  node [
    id 262
    label "pasa&#380;"
  ]
  node [
    id 263
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 264
    label "notacja_muzyczna"
  ]
  node [
    id 265
    label "kontrapunkt"
  ]
  node [
    id 266
    label "sztuka"
  ]
  node [
    id 267
    label "instrumentalistyka"
  ]
  node [
    id 268
    label "harmonia"
  ]
  node [
    id 269
    label "set"
  ]
  node [
    id 270
    label "wys&#322;uchanie"
  ]
  node [
    id 271
    label "kapela"
  ]
  node [
    id 272
    label "britpop"
  ]
  node [
    id 273
    label "badanie"
  ]
  node [
    id 274
    label "obserwowanie"
  ]
  node [
    id 275
    label "wy&#347;wiadczenie"
  ]
  node [
    id 276
    label "wydarzenie"
  ]
  node [
    id 277
    label "assay"
  ]
  node [
    id 278
    label "checkup"
  ]
  node [
    id 279
    label "spotkanie"
  ]
  node [
    id 280
    label "do&#347;wiadczanie"
  ]
  node [
    id 281
    label "zbadanie"
  ]
  node [
    id 282
    label "potraktowanie"
  ]
  node [
    id 283
    label "poczucie"
  ]
  node [
    id 284
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 285
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 286
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 287
    label "wykszta&#322;cenie"
  ]
  node [
    id 288
    label "urszulanki_szare"
  ]
  node [
    id 289
    label "proporcja"
  ]
  node [
    id 290
    label "cognition"
  ]
  node [
    id 291
    label "intelekt"
  ]
  node [
    id 292
    label "pozwolenie"
  ]
  node [
    id 293
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 294
    label "zaawansowanie"
  ]
  node [
    id 295
    label "przekazanie"
  ]
  node [
    id 296
    label "skopiowanie"
  ]
  node [
    id 297
    label "arrangement"
  ]
  node [
    id 298
    label "przeniesienie"
  ]
  node [
    id 299
    label "testament"
  ]
  node [
    id 300
    label "lekarstwo"
  ]
  node [
    id 301
    label "zadanie"
  ]
  node [
    id 302
    label "answer"
  ]
  node [
    id 303
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 304
    label "transcription"
  ]
  node [
    id 305
    label "zalecenie"
  ]
  node [
    id 306
    label "ucze&#324;"
  ]
  node [
    id 307
    label "student"
  ]
  node [
    id 308
    label "cz&#322;owiek"
  ]
  node [
    id 309
    label "zaliczy&#263;"
  ]
  node [
    id 310
    label "przekaza&#263;"
  ]
  node [
    id 311
    label "powierzy&#263;"
  ]
  node [
    id 312
    label "zmusi&#263;"
  ]
  node [
    id 313
    label "translate"
  ]
  node [
    id 314
    label "give"
  ]
  node [
    id 315
    label "picture"
  ]
  node [
    id 316
    label "przedstawi&#263;"
  ]
  node [
    id 317
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 318
    label "convey"
  ]
  node [
    id 319
    label "fraza"
  ]
  node [
    id 320
    label "stanowisko"
  ]
  node [
    id 321
    label "wypowiedzenie"
  ]
  node [
    id 322
    label "prison_term"
  ]
  node [
    id 323
    label "okres"
  ]
  node [
    id 324
    label "przedstawienie"
  ]
  node [
    id 325
    label "wyra&#380;enie"
  ]
  node [
    id 326
    label "zaliczenie"
  ]
  node [
    id 327
    label "antylogizm"
  ]
  node [
    id 328
    label "zmuszenie"
  ]
  node [
    id 329
    label "konektyw"
  ]
  node [
    id 330
    label "attitude"
  ]
  node [
    id 331
    label "powierzenie"
  ]
  node [
    id 332
    label "adjudication"
  ]
  node [
    id 333
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 334
    label "pass"
  ]
  node [
    id 335
    label "supply"
  ]
  node [
    id 336
    label "zaleci&#263;"
  ]
  node [
    id 337
    label "rewrite"
  ]
  node [
    id 338
    label "zrzec_si&#281;"
  ]
  node [
    id 339
    label "skopiowa&#263;"
  ]
  node [
    id 340
    label "przenie&#347;&#263;"
  ]
  node [
    id 341
    label "political_orientation"
  ]
  node [
    id 342
    label "idea"
  ]
  node [
    id 343
    label "stra&#380;nik"
  ]
  node [
    id 344
    label "przedszkole"
  ]
  node [
    id 345
    label "opiekun"
  ]
  node [
    id 346
    label "ruch"
  ]
  node [
    id 347
    label "rozmiar&#243;wka"
  ]
  node [
    id 348
    label "p&#322;aszczyzna"
  ]
  node [
    id 349
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 350
    label "tarcza"
  ]
  node [
    id 351
    label "kosz"
  ]
  node [
    id 352
    label "transparent"
  ]
  node [
    id 353
    label "uk&#322;ad"
  ]
  node [
    id 354
    label "rubryka"
  ]
  node [
    id 355
    label "kontener"
  ]
  node [
    id 356
    label "spis"
  ]
  node [
    id 357
    label "plate"
  ]
  node [
    id 358
    label "konstrukcja"
  ]
  node [
    id 359
    label "szachownica_Punnetta"
  ]
  node [
    id 360
    label "chart"
  ]
  node [
    id 361
    label "izba"
  ]
  node [
    id 362
    label "biurko"
  ]
  node [
    id 363
    label "boks"
  ]
  node [
    id 364
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 365
    label "egzekutywa"
  ]
  node [
    id 366
    label "premier"
  ]
  node [
    id 367
    label "Londyn"
  ]
  node [
    id 368
    label "palestra"
  ]
  node [
    id 369
    label "pok&#243;j"
  ]
  node [
    id 370
    label "pracownia"
  ]
  node [
    id 371
    label "gabinet_cieni"
  ]
  node [
    id 372
    label "pomieszczenie"
  ]
  node [
    id 373
    label "Konsulat"
  ]
  node [
    id 374
    label "wagon"
  ]
  node [
    id 375
    label "mecz_mistrzowski"
  ]
  node [
    id 376
    label "class"
  ]
  node [
    id 377
    label "&#322;awka"
  ]
  node [
    id 378
    label "wykrzyknik"
  ]
  node [
    id 379
    label "zaleta"
  ]
  node [
    id 380
    label "programowanie_obiektowe"
  ]
  node [
    id 381
    label "warstwa"
  ]
  node [
    id 382
    label "rezerwa"
  ]
  node [
    id 383
    label "Ekwici"
  ]
  node [
    id 384
    label "&#347;rodowisko"
  ]
  node [
    id 385
    label "sala"
  ]
  node [
    id 386
    label "pomoc"
  ]
  node [
    id 387
    label "jako&#347;&#263;"
  ]
  node [
    id 388
    label "znak_jako&#347;ci"
  ]
  node [
    id 389
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 390
    label "poziom"
  ]
  node [
    id 391
    label "promocja"
  ]
  node [
    id 392
    label "kurs"
  ]
  node [
    id 393
    label "obiekt"
  ]
  node [
    id 394
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 395
    label "dziennik_lekcyjny"
  ]
  node [
    id 396
    label "typ"
  ]
  node [
    id 397
    label "fakcja"
  ]
  node [
    id 398
    label "obrona"
  ]
  node [
    id 399
    label "atak"
  ]
  node [
    id 400
    label "botanika"
  ]
  node [
    id 401
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 402
    label "Wallenrod"
  ]
  node [
    id 403
    label "niezaawansowany"
  ]
  node [
    id 404
    label "najwa&#380;niejszy"
  ]
  node [
    id 405
    label "pocz&#261;tkowy"
  ]
  node [
    id 406
    label "podstawowo"
  ]
  node [
    id 407
    label "dzieci&#281;cy"
  ]
  node [
    id 408
    label "pierwszy"
  ]
  node [
    id 409
    label "elementarny"
  ]
  node [
    id 410
    label "pocz&#261;tkowo"
  ]
  node [
    id 411
    label "stara&#263;_si&#281;"
  ]
  node [
    id 412
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 413
    label "sprawdza&#263;"
  ]
  node [
    id 414
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 415
    label "feel"
  ]
  node [
    id 416
    label "try"
  ]
  node [
    id 417
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 418
    label "kosztowa&#263;"
  ]
  node [
    id 419
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 420
    label "examine"
  ]
  node [
    id 421
    label "robi&#263;"
  ]
  node [
    id 422
    label "szpiegowa&#263;"
  ]
  node [
    id 423
    label "konsumowa&#263;"
  ]
  node [
    id 424
    label "by&#263;"
  ]
  node [
    id 425
    label "savor"
  ]
  node [
    id 426
    label "cena"
  ]
  node [
    id 427
    label "doznawa&#263;"
  ]
  node [
    id 428
    label "essay"
  ]
  node [
    id 429
    label "pr&#243;bowanie"
  ]
  node [
    id 430
    label "zademonstrowanie"
  ]
  node [
    id 431
    label "report"
  ]
  node [
    id 432
    label "obgadanie"
  ]
  node [
    id 433
    label "realizacja"
  ]
  node [
    id 434
    label "scena"
  ]
  node [
    id 435
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 436
    label "narration"
  ]
  node [
    id 437
    label "cyrk"
  ]
  node [
    id 438
    label "posta&#263;"
  ]
  node [
    id 439
    label "theatrical_performance"
  ]
  node [
    id 440
    label "opisanie"
  ]
  node [
    id 441
    label "malarstwo"
  ]
  node [
    id 442
    label "scenografia"
  ]
  node [
    id 443
    label "teatr"
  ]
  node [
    id 444
    label "ukazanie"
  ]
  node [
    id 445
    label "zapoznanie"
  ]
  node [
    id 446
    label "pokaz"
  ]
  node [
    id 447
    label "podanie"
  ]
  node [
    id 448
    label "ods&#322;ona"
  ]
  node [
    id 449
    label "exhibit"
  ]
  node [
    id 450
    label "pokazanie"
  ]
  node [
    id 451
    label "wyst&#261;pienie"
  ]
  node [
    id 452
    label "przedstawianie"
  ]
  node [
    id 453
    label "przedstawia&#263;"
  ]
  node [
    id 454
    label "rola"
  ]
  node [
    id 455
    label "energia"
  ]
  node [
    id 456
    label "parametr"
  ]
  node [
    id 457
    label "rozwi&#261;zanie"
  ]
  node [
    id 458
    label "wojsko"
  ]
  node [
    id 459
    label "cecha"
  ]
  node [
    id 460
    label "wuchta"
  ]
  node [
    id 461
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 462
    label "moment_si&#322;y"
  ]
  node [
    id 463
    label "mn&#243;stwo"
  ]
  node [
    id 464
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 465
    label "zdolno&#347;&#263;"
  ]
  node [
    id 466
    label "capacity"
  ]
  node [
    id 467
    label "magnitude"
  ]
  node [
    id 468
    label "potencja"
  ]
  node [
    id 469
    label "przemoc"
  ]
  node [
    id 470
    label "boski"
  ]
  node [
    id 471
    label "krajobraz"
  ]
  node [
    id 472
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 473
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 474
    label "przywidzenie"
  ]
  node [
    id 475
    label "presence"
  ]
  node [
    id 476
    label "charakter"
  ]
  node [
    id 477
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 478
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 479
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 480
    label "emitowa&#263;"
  ]
  node [
    id 481
    label "egzergia"
  ]
  node [
    id 482
    label "kwant_energii"
  ]
  node [
    id 483
    label "szwung"
  ]
  node [
    id 484
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 485
    label "power"
  ]
  node [
    id 486
    label "emitowanie"
  ]
  node [
    id 487
    label "energy"
  ]
  node [
    id 488
    label "wymiar"
  ]
  node [
    id 489
    label "zmienna"
  ]
  node [
    id 490
    label "charakterystyka"
  ]
  node [
    id 491
    label "wielko&#347;&#263;"
  ]
  node [
    id 492
    label "patologia"
  ]
  node [
    id 493
    label "agresja"
  ]
  node [
    id 494
    label "przewaga"
  ]
  node [
    id 495
    label "drastyczny"
  ]
  node [
    id 496
    label "po&#322;&#243;g"
  ]
  node [
    id 497
    label "spe&#322;nienie"
  ]
  node [
    id 498
    label "dula"
  ]
  node [
    id 499
    label "usuni&#281;cie"
  ]
  node [
    id 500
    label "wymy&#347;lenie"
  ]
  node [
    id 501
    label "po&#322;o&#380;na"
  ]
  node [
    id 502
    label "wyj&#347;cie"
  ]
  node [
    id 503
    label "uniewa&#380;nienie"
  ]
  node [
    id 504
    label "proces_fizjologiczny"
  ]
  node [
    id 505
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 506
    label "pomys&#322;"
  ]
  node [
    id 507
    label "szok_poporodowy"
  ]
  node [
    id 508
    label "event"
  ]
  node [
    id 509
    label "marc&#243;wka"
  ]
  node [
    id 510
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 511
    label "birth"
  ]
  node [
    id 512
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 513
    label "wynik"
  ]
  node [
    id 514
    label "przestanie"
  ]
  node [
    id 515
    label "ilo&#347;&#263;"
  ]
  node [
    id 516
    label "enormousness"
  ]
  node [
    id 517
    label "posiada&#263;"
  ]
  node [
    id 518
    label "potencja&#322;"
  ]
  node [
    id 519
    label "zapomina&#263;"
  ]
  node [
    id 520
    label "zapomnienie"
  ]
  node [
    id 521
    label "zapominanie"
  ]
  node [
    id 522
    label "ability"
  ]
  node [
    id 523
    label "obliczeniowo"
  ]
  node [
    id 524
    label "zapomnie&#263;"
  ]
  node [
    id 525
    label "facylitacja"
  ]
  node [
    id 526
    label "warto&#347;&#263;"
  ]
  node [
    id 527
    label "zrewaluowa&#263;"
  ]
  node [
    id 528
    label "rewaluowanie"
  ]
  node [
    id 529
    label "korzy&#347;&#263;"
  ]
  node [
    id 530
    label "strona"
  ]
  node [
    id 531
    label "rewaluowa&#263;"
  ]
  node [
    id 532
    label "wabik"
  ]
  node [
    id 533
    label "zrewaluowanie"
  ]
  node [
    id 534
    label "m&#322;ot"
  ]
  node [
    id 535
    label "znak"
  ]
  node [
    id 536
    label "drzewo"
  ]
  node [
    id 537
    label "pr&#243;ba"
  ]
  node [
    id 538
    label "attribute"
  ]
  node [
    id 539
    label "marka"
  ]
  node [
    id 540
    label "moc"
  ]
  node [
    id 541
    label "potency"
  ]
  node [
    id 542
    label "byt"
  ]
  node [
    id 543
    label "tomizm"
  ]
  node [
    id 544
    label "wydolno&#347;&#263;"
  ]
  node [
    id 545
    label "arystotelizm"
  ]
  node [
    id 546
    label "gotowo&#347;&#263;"
  ]
  node [
    id 547
    label "zrejterowanie"
  ]
  node [
    id 548
    label "zmobilizowa&#263;"
  ]
  node [
    id 549
    label "dezerter"
  ]
  node [
    id 550
    label "oddzia&#322;_karny"
  ]
  node [
    id 551
    label "tabor"
  ]
  node [
    id 552
    label "wermacht"
  ]
  node [
    id 553
    label "cofni&#281;cie"
  ]
  node [
    id 554
    label "fala"
  ]
  node [
    id 555
    label "korpus"
  ]
  node [
    id 556
    label "soldateska"
  ]
  node [
    id 557
    label "ods&#322;ugiwanie"
  ]
  node [
    id 558
    label "werbowanie_si&#281;"
  ]
  node [
    id 559
    label "zdemobilizowanie"
  ]
  node [
    id 560
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 561
    label "s&#322;u&#380;ba"
  ]
  node [
    id 562
    label "or&#281;&#380;"
  ]
  node [
    id 563
    label "Legia_Cudzoziemska"
  ]
  node [
    id 564
    label "Armia_Czerwona"
  ]
  node [
    id 565
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 566
    label "rejterowanie"
  ]
  node [
    id 567
    label "Czerwona_Gwardia"
  ]
  node [
    id 568
    label "zrejterowa&#263;"
  ]
  node [
    id 569
    label "sztabslekarz"
  ]
  node [
    id 570
    label "zmobilizowanie"
  ]
  node [
    id 571
    label "wojo"
  ]
  node [
    id 572
    label "pospolite_ruszenie"
  ]
  node [
    id 573
    label "Eurokorpus"
  ]
  node [
    id 574
    label "mobilizowanie"
  ]
  node [
    id 575
    label "rejterowa&#263;"
  ]
  node [
    id 576
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 577
    label "mobilizowa&#263;"
  ]
  node [
    id 578
    label "Armia_Krajowa"
  ]
  node [
    id 579
    label "dryl"
  ]
  node [
    id 580
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 581
    label "petarda"
  ]
  node [
    id 582
    label "pozycja"
  ]
  node [
    id 583
    label "zdemobilizowa&#263;"
  ]
  node [
    id 584
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 585
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 586
    label "nienaturalny"
  ]
  node [
    id 587
    label "teatralnie"
  ]
  node [
    id 588
    label "nadmierny"
  ]
  node [
    id 589
    label "nadmiernie"
  ]
  node [
    id 590
    label "artystycznie"
  ]
  node [
    id 591
    label "stagily"
  ]
  node [
    id 592
    label "theatrically"
  ]
  node [
    id 593
    label "nienaturalnie"
  ]
  node [
    id 594
    label "niespotykany"
  ]
  node [
    id 595
    label "sztucznie"
  ]
  node [
    id 596
    label "niepodobny"
  ]
  node [
    id 597
    label "nieprzekonuj&#261;cy"
  ]
  node [
    id 598
    label "nienormalny"
  ]
  node [
    id 599
    label "nietypowy"
  ]
  node [
    id 600
    label "dziwny"
  ]
  node [
    id 601
    label "zawodowy"
  ]
  node [
    id 602
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 603
    label "typowy"
  ]
  node [
    id 604
    label "dziennikarsko"
  ]
  node [
    id 605
    label "tre&#347;ciwy"
  ]
  node [
    id 606
    label "po_dziennikarsku"
  ]
  node [
    id 607
    label "wzorowy"
  ]
  node [
    id 608
    label "obiektywny"
  ]
  node [
    id 609
    label "rzetelny"
  ]
  node [
    id 610
    label "doskona&#322;y"
  ]
  node [
    id 611
    label "przyk&#322;adny"
  ]
  node [
    id 612
    label "&#322;adny"
  ]
  node [
    id 613
    label "dobry"
  ]
  node [
    id 614
    label "wzorowo"
  ]
  node [
    id 615
    label "czadowy"
  ]
  node [
    id 616
    label "fachowy"
  ]
  node [
    id 617
    label "fajny"
  ]
  node [
    id 618
    label "klawy"
  ]
  node [
    id 619
    label "zawodowo"
  ]
  node [
    id 620
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 621
    label "formalny"
  ]
  node [
    id 622
    label "zawo&#322;any"
  ]
  node [
    id 623
    label "profesjonalny"
  ]
  node [
    id 624
    label "zwyczajny"
  ]
  node [
    id 625
    label "typowo"
  ]
  node [
    id 626
    label "cz&#281;sty"
  ]
  node [
    id 627
    label "zwyk&#322;y"
  ]
  node [
    id 628
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 629
    label "nale&#380;ny"
  ]
  node [
    id 630
    label "nale&#380;yty"
  ]
  node [
    id 631
    label "uprawniony"
  ]
  node [
    id 632
    label "zasadniczy"
  ]
  node [
    id 633
    label "stosownie"
  ]
  node [
    id 634
    label "taki"
  ]
  node [
    id 635
    label "charakterystyczny"
  ]
  node [
    id 636
    label "prawdziwy"
  ]
  node [
    id 637
    label "ten"
  ]
  node [
    id 638
    label "syc&#261;cy"
  ]
  node [
    id 639
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 640
    label "tre&#347;ciwie"
  ]
  node [
    id 641
    label "zgrabny"
  ]
  node [
    id 642
    label "g&#281;sty"
  ]
  node [
    id 643
    label "rzetelnie"
  ]
  node [
    id 644
    label "przekonuj&#261;cy"
  ]
  node [
    id 645
    label "porz&#261;dny"
  ]
  node [
    id 646
    label "uczciwy"
  ]
  node [
    id 647
    label "obiektywizowanie"
  ]
  node [
    id 648
    label "zobiektywizowanie"
  ]
  node [
    id 649
    label "niezale&#380;ny"
  ]
  node [
    id 650
    label "bezsporny"
  ]
  node [
    id 651
    label "obiektywnie"
  ]
  node [
    id 652
    label "neutralny"
  ]
  node [
    id 653
    label "faktyczny"
  ]
  node [
    id 654
    label "komunikacyjny"
  ]
  node [
    id 655
    label "dogodny"
  ]
  node [
    id 656
    label "p&#243;&#378;ny"
  ]
  node [
    id 657
    label "do_p&#243;&#378;na"
  ]
  node [
    id 658
    label "line_up"
  ]
  node [
    id 659
    label "sta&#263;_si&#281;"
  ]
  node [
    id 660
    label "przyby&#263;"
  ]
  node [
    id 661
    label "zaistnie&#263;"
  ]
  node [
    id 662
    label "doj&#347;&#263;"
  ]
  node [
    id 663
    label "become"
  ]
  node [
    id 664
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 665
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 666
    label "appear"
  ]
  node [
    id 667
    label "get"
  ]
  node [
    id 668
    label "dotrze&#263;"
  ]
  node [
    id 669
    label "zyska&#263;"
  ]
  node [
    id 670
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 671
    label "supervene"
  ]
  node [
    id 672
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 673
    label "zaj&#347;&#263;"
  ]
  node [
    id 674
    label "catch"
  ]
  node [
    id 675
    label "bodziec"
  ]
  node [
    id 676
    label "informacja"
  ]
  node [
    id 677
    label "przesy&#322;ka"
  ]
  node [
    id 678
    label "dodatek"
  ]
  node [
    id 679
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 680
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 681
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 682
    label "heed"
  ]
  node [
    id 683
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 684
    label "spowodowa&#263;"
  ]
  node [
    id 685
    label "dozna&#263;"
  ]
  node [
    id 686
    label "dokoptowa&#263;"
  ]
  node [
    id 687
    label "postrzega&#263;"
  ]
  node [
    id 688
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 689
    label "orgazm"
  ]
  node [
    id 690
    label "dolecie&#263;"
  ]
  node [
    id 691
    label "drive"
  ]
  node [
    id 692
    label "uzyska&#263;"
  ]
  node [
    id 693
    label "dop&#322;ata"
  ]
  node [
    id 694
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 695
    label "paj&#281;czarz"
  ]
  node [
    id 696
    label "radiola"
  ]
  node [
    id 697
    label "programowiec"
  ]
  node [
    id 698
    label "redakcja"
  ]
  node [
    id 699
    label "spot"
  ]
  node [
    id 700
    label "stacja"
  ]
  node [
    id 701
    label "odbiornik"
  ]
  node [
    id 702
    label "eliminator"
  ]
  node [
    id 703
    label "radiolinia"
  ]
  node [
    id 704
    label "media"
  ]
  node [
    id 705
    label "fala_radiowa"
  ]
  node [
    id 706
    label "radiofonia"
  ]
  node [
    id 707
    label "odbieranie"
  ]
  node [
    id 708
    label "studio"
  ]
  node [
    id 709
    label "dyskryminator"
  ]
  node [
    id 710
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 711
    label "odbiera&#263;"
  ]
  node [
    id 712
    label "treaty"
  ]
  node [
    id 713
    label "umowa"
  ]
  node [
    id 714
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 715
    label "przestawi&#263;"
  ]
  node [
    id 716
    label "alliance"
  ]
  node [
    id 717
    label "ONZ"
  ]
  node [
    id 718
    label "NATO"
  ]
  node [
    id 719
    label "zawarcie"
  ]
  node [
    id 720
    label "zawrze&#263;"
  ]
  node [
    id 721
    label "organ"
  ]
  node [
    id 722
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 723
    label "wi&#281;&#378;"
  ]
  node [
    id 724
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 725
    label "traktat_wersalski"
  ]
  node [
    id 726
    label "cia&#322;o"
  ]
  node [
    id 727
    label "mass-media"
  ]
  node [
    id 728
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 729
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 730
    label "przekazior"
  ]
  node [
    id 731
    label "uzbrajanie"
  ]
  node [
    id 732
    label "medium"
  ]
  node [
    id 733
    label "punkt"
  ]
  node [
    id 734
    label "droga_krzy&#380;owa"
  ]
  node [
    id 735
    label "urz&#261;dzenie"
  ]
  node [
    id 736
    label "antena"
  ]
  node [
    id 737
    label "amplituner"
  ]
  node [
    id 738
    label "tuner"
  ]
  node [
    id 739
    label "telewizja"
  ]
  node [
    id 740
    label "redaktor"
  ]
  node [
    id 741
    label "zesp&#243;&#322;"
  ]
  node [
    id 742
    label "composition"
  ]
  node [
    id 743
    label "wydawnictwo"
  ]
  node [
    id 744
    label "redaction"
  ]
  node [
    id 745
    label "obr&#243;bka"
  ]
  node [
    id 746
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 747
    label "radiokomunikacja"
  ]
  node [
    id 748
    label "infrastruktura"
  ]
  node [
    id 749
    label "radiofonizacja"
  ]
  node [
    id 750
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 751
    label "lampa"
  ]
  node [
    id 752
    label "film"
  ]
  node [
    id 753
    label "pomiar"
  ]
  node [
    id 754
    label "booklet"
  ]
  node [
    id 755
    label "transakcja"
  ]
  node [
    id 756
    label "ekspozycja"
  ]
  node [
    id 757
    label "reklama"
  ]
  node [
    id 758
    label "fotografia"
  ]
  node [
    id 759
    label "u&#380;ytkownik"
  ]
  node [
    id 760
    label "oszust"
  ]
  node [
    id 761
    label "telewizor"
  ]
  node [
    id 762
    label "pirat"
  ]
  node [
    id 763
    label "dochodzenie"
  ]
  node [
    id 764
    label "rozsi&#261;dni&#281;cie_si&#281;"
  ]
  node [
    id 765
    label "powodowanie"
  ]
  node [
    id 766
    label "wpadni&#281;cie"
  ]
  node [
    id 767
    label "collection"
  ]
  node [
    id 768
    label "konfiskowanie"
  ]
  node [
    id 769
    label "rozsiadanie_si&#281;"
  ]
  node [
    id 770
    label "zabieranie"
  ]
  node [
    id 771
    label "zlecenie"
  ]
  node [
    id 772
    label "przyjmowanie"
  ]
  node [
    id 773
    label "solicitation"
  ]
  node [
    id 774
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 775
    label "robienie"
  ]
  node [
    id 776
    label "zniewalanie"
  ]
  node [
    id 777
    label "doj&#347;cie"
  ]
  node [
    id 778
    label "przyp&#322;ywanie"
  ]
  node [
    id 779
    label "odzyskiwanie"
  ]
  node [
    id 780
    label "czynno&#347;&#263;"
  ]
  node [
    id 781
    label "branie"
  ]
  node [
    id 782
    label "perception"
  ]
  node [
    id 783
    label "odp&#322;ywanie"
  ]
  node [
    id 784
    label "wpadanie"
  ]
  node [
    id 785
    label "zabiera&#263;"
  ]
  node [
    id 786
    label "odzyskiwa&#263;"
  ]
  node [
    id 787
    label "przyjmowa&#263;"
  ]
  node [
    id 788
    label "bra&#263;"
  ]
  node [
    id 789
    label "fall"
  ]
  node [
    id 790
    label "liszy&#263;"
  ]
  node [
    id 791
    label "pozbawia&#263;"
  ]
  node [
    id 792
    label "konfiskowa&#263;"
  ]
  node [
    id 793
    label "deprive"
  ]
  node [
    id 794
    label "accept"
  ]
  node [
    id 795
    label "magnetofon"
  ]
  node [
    id 796
    label "lnowate"
  ]
  node [
    id 797
    label "zestaw_elektroakustyczny"
  ]
  node [
    id 798
    label "gramofon"
  ]
  node [
    id 799
    label "wzmacniacz"
  ]
  node [
    id 800
    label "radiow&#281;ze&#322;"
  ]
  node [
    id 801
    label "ro&#347;lina"
  ]
  node [
    id 802
    label "spalin&#243;wka"
  ]
  node [
    id 803
    label "pojazd_niemechaniczny"
  ]
  node [
    id 804
    label "statek"
  ]
  node [
    id 805
    label "regaty"
  ]
  node [
    id 806
    label "kratownica"
  ]
  node [
    id 807
    label "pok&#322;ad"
  ]
  node [
    id 808
    label "drzewce"
  ]
  node [
    id 809
    label "ster"
  ]
  node [
    id 810
    label "dobija&#263;"
  ]
  node [
    id 811
    label "zakotwiczenie"
  ]
  node [
    id 812
    label "odcumowywa&#263;"
  ]
  node [
    id 813
    label "p&#322;ywa&#263;"
  ]
  node [
    id 814
    label "odkotwicza&#263;"
  ]
  node [
    id 815
    label "zwodowanie"
  ]
  node [
    id 816
    label "odkotwiczy&#263;"
  ]
  node [
    id 817
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 818
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 819
    label "odcumowanie"
  ]
  node [
    id 820
    label "odcumowa&#263;"
  ]
  node [
    id 821
    label "zacumowanie"
  ]
  node [
    id 822
    label "kotwiczenie"
  ]
  node [
    id 823
    label "kad&#322;ub"
  ]
  node [
    id 824
    label "reling"
  ]
  node [
    id 825
    label "kabina"
  ]
  node [
    id 826
    label "dokowanie"
  ]
  node [
    id 827
    label "kotwiczy&#263;"
  ]
  node [
    id 828
    label "szkutnictwo"
  ]
  node [
    id 829
    label "korab"
  ]
  node [
    id 830
    label "odbijacz"
  ]
  node [
    id 831
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 832
    label "dobi&#263;"
  ]
  node [
    id 833
    label "dobijanie"
  ]
  node [
    id 834
    label "proporczyk"
  ]
  node [
    id 835
    label "odkotwiczenie"
  ]
  node [
    id 836
    label "kabestan"
  ]
  node [
    id 837
    label "cumowanie"
  ]
  node [
    id 838
    label "zaw&#243;r_denny"
  ]
  node [
    id 839
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 840
    label "flota"
  ]
  node [
    id 841
    label "rostra"
  ]
  node [
    id 842
    label "zr&#281;bnica"
  ]
  node [
    id 843
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 844
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 845
    label "bumsztak"
  ]
  node [
    id 846
    label "nadbud&#243;wka"
  ]
  node [
    id 847
    label "sterownik_automatyczny"
  ]
  node [
    id 848
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 849
    label "cumowa&#263;"
  ]
  node [
    id 850
    label "armator"
  ]
  node [
    id 851
    label "odcumowywanie"
  ]
  node [
    id 852
    label "zakotwiczy&#263;"
  ]
  node [
    id 853
    label "zacumowa&#263;"
  ]
  node [
    id 854
    label "dokowa&#263;"
  ]
  node [
    id 855
    label "wodowanie"
  ]
  node [
    id 856
    label "zadokowanie"
  ]
  node [
    id 857
    label "dobicie"
  ]
  node [
    id 858
    label "trap"
  ]
  node [
    id 859
    label "kotwica"
  ]
  node [
    id 860
    label "odkotwiczanie"
  ]
  node [
    id 861
    label "luk"
  ]
  node [
    id 862
    label "dzi&#243;b"
  ]
  node [
    id 863
    label "armada"
  ]
  node [
    id 864
    label "&#380;yroskop"
  ]
  node [
    id 865
    label "futr&#243;wka"
  ]
  node [
    id 866
    label "pojazd"
  ]
  node [
    id 867
    label "sztormtrap"
  ]
  node [
    id 868
    label "skrajnik"
  ]
  node [
    id 869
    label "zadokowa&#263;"
  ]
  node [
    id 870
    label "zwodowa&#263;"
  ]
  node [
    id 871
    label "grobla"
  ]
  node [
    id 872
    label "metryczna_jednostka_obj&#281;to&#347;ci"
  ]
  node [
    id 873
    label "sterownica"
  ]
  node [
    id 874
    label "kilometr_sze&#347;cienny"
  ]
  node [
    id 875
    label "wolant"
  ]
  node [
    id 876
    label "powierzchnia_sterowa"
  ]
  node [
    id 877
    label "sterolotka"
  ]
  node [
    id 878
    label "&#380;agl&#243;wka"
  ]
  node [
    id 879
    label "statek_powietrzny"
  ]
  node [
    id 880
    label "rumpel"
  ]
  node [
    id 881
    label "mechanizm"
  ]
  node [
    id 882
    label "przyw&#243;dztwo"
  ]
  node [
    id 883
    label "jacht"
  ]
  node [
    id 884
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 885
    label "pr&#281;t"
  ]
  node [
    id 886
    label "omasztowanie"
  ]
  node [
    id 887
    label "pi&#281;ta"
  ]
  node [
    id 888
    label "bro&#324;_obuchowa"
  ]
  node [
    id 889
    label "uchwyt"
  ]
  node [
    id 890
    label "dr&#261;&#380;ek"
  ]
  node [
    id 891
    label "belka"
  ]
  node [
    id 892
    label "przyrz&#261;d_naukowy"
  ]
  node [
    id 893
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 894
    label "d&#378;wigar"
  ]
  node [
    id 895
    label "krata"
  ]
  node [
    id 896
    label "bar"
  ]
  node [
    id 897
    label "wicket"
  ]
  node [
    id 898
    label "okratowanie"
  ]
  node [
    id 899
    label "ochrona"
  ]
  node [
    id 900
    label "sp&#261;g"
  ]
  node [
    id 901
    label "przestrze&#324;"
  ]
  node [
    id 902
    label "pok&#322;adnik"
  ]
  node [
    id 903
    label "powierzchnia"
  ]
  node [
    id 904
    label "strop"
  ]
  node [
    id 905
    label "poszycie_pok&#322;adowe"
  ]
  node [
    id 906
    label "kipa"
  ]
  node [
    id 907
    label "p&#243;&#322;pok&#322;ad"
  ]
  node [
    id 908
    label "samolot"
  ]
  node [
    id 909
    label "jut"
  ]
  node [
    id 910
    label "z&#322;o&#380;e"
  ]
  node [
    id 911
    label "wy&#347;cig"
  ]
  node [
    id 912
    label "kosiarka"
  ]
  node [
    id 913
    label "pi&#322;a_mechaniczna"
  ]
  node [
    id 914
    label "lokomotywa"
  ]
  node [
    id 915
    label "przew&#243;d"
  ]
  node [
    id 916
    label "szarpanka"
  ]
  node [
    id 917
    label "rura"
  ]
  node [
    id 918
    label "catalog"
  ]
  node [
    id 919
    label "sumariusz"
  ]
  node [
    id 920
    label "book"
  ]
  node [
    id 921
    label "stock"
  ]
  node [
    id 922
    label "figurowa&#263;"
  ]
  node [
    id 923
    label "wyliczanka"
  ]
  node [
    id 924
    label "ekscerpcja"
  ]
  node [
    id 925
    label "j&#281;zykowo"
  ]
  node [
    id 926
    label "wypowied&#378;"
  ]
  node [
    id 927
    label "pomini&#281;cie"
  ]
  node [
    id 928
    label "dzie&#322;o"
  ]
  node [
    id 929
    label "preparacja"
  ]
  node [
    id 930
    label "odmianka"
  ]
  node [
    id 931
    label "opu&#347;ci&#263;"
  ]
  node [
    id 932
    label "koniektura"
  ]
  node [
    id 933
    label "pisa&#263;"
  ]
  node [
    id 934
    label "obelga"
  ]
  node [
    id 935
    label "series"
  ]
  node [
    id 936
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 937
    label "uprawianie"
  ]
  node [
    id 938
    label "praca_rolnicza"
  ]
  node [
    id 939
    label "dane"
  ]
  node [
    id 940
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 941
    label "sum"
  ]
  node [
    id 942
    label "gathering"
  ]
  node [
    id 943
    label "album"
  ]
  node [
    id 944
    label "po&#322;o&#380;enie"
  ]
  node [
    id 945
    label "debit"
  ]
  node [
    id 946
    label "druk"
  ]
  node [
    id 947
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 948
    label "szata_graficzna"
  ]
  node [
    id 949
    label "wydawa&#263;"
  ]
  node [
    id 950
    label "szermierka"
  ]
  node [
    id 951
    label "wyda&#263;"
  ]
  node [
    id 952
    label "ustawienie"
  ]
  node [
    id 953
    label "publikacja"
  ]
  node [
    id 954
    label "status"
  ]
  node [
    id 955
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 956
    label "adres"
  ]
  node [
    id 957
    label "rozmieszczenie"
  ]
  node [
    id 958
    label "sytuacja"
  ]
  node [
    id 959
    label "rz&#261;d"
  ]
  node [
    id 960
    label "awansowa&#263;"
  ]
  node [
    id 961
    label "bearing"
  ]
  node [
    id 962
    label "znaczenie"
  ]
  node [
    id 963
    label "awans"
  ]
  node [
    id 964
    label "awansowanie"
  ]
  node [
    id 965
    label "poster"
  ]
  node [
    id 966
    label "le&#380;e&#263;"
  ]
  node [
    id 967
    label "entliczek"
  ]
  node [
    id 968
    label "zabawa"
  ]
  node [
    id 969
    label "wiersz"
  ]
  node [
    id 970
    label "pentliczek"
  ]
  node [
    id 971
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 972
    label "bonanza"
  ]
  node [
    id 973
    label "komplikacja"
  ]
  node [
    id 974
    label "k&#322;opot"
  ]
  node [
    id 975
    label "&#378;r&#243;d&#322;o_dochodu"
  ]
  node [
    id 976
    label "bieganina"
  ]
  node [
    id 977
    label "jazda"
  ]
  node [
    id 978
    label "heca"
  ]
  node [
    id 979
    label "interes"
  ]
  node [
    id 980
    label "&#380;y&#322;a_z&#322;ota"
  ]
  node [
    id 981
    label "warto"
  ]
  node [
    id 982
    label "problem"
  ]
  node [
    id 983
    label "subiekcja"
  ]
  node [
    id 984
    label "utrudnienie"
  ]
  node [
    id 985
    label "rozw&#243;j"
  ]
  node [
    id 986
    label "schorzenie"
  ]
  node [
    id 987
    label "hindrance"
  ]
  node [
    id 988
    label "turbacja"
  ]
  node [
    id 989
    label "zrozumie&#263;"
  ]
  node [
    id 990
    label "topographic_point"
  ]
  node [
    id 991
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 992
    label "visualize"
  ]
  node [
    id 993
    label "przyswoi&#263;"
  ]
  node [
    id 994
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 995
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 996
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 997
    label "teach"
  ]
  node [
    id 998
    label "experience"
  ]
  node [
    id 999
    label "permit"
  ]
  node [
    id 1000
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 1001
    label "dostrzec"
  ]
  node [
    id 1002
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 1003
    label "organizm"
  ]
  node [
    id 1004
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1005
    label "kultura"
  ]
  node [
    id 1006
    label "pobra&#263;"
  ]
  node [
    id 1007
    label "thrill"
  ]
  node [
    id 1008
    label "oceni&#263;"
  ]
  node [
    id 1009
    label "skuma&#263;"
  ]
  node [
    id 1010
    label "poczu&#263;"
  ]
  node [
    id 1011
    label "do"
  ]
  node [
    id 1012
    label "zacz&#261;&#263;"
  ]
  node [
    id 1013
    label "think"
  ]
  node [
    id 1014
    label "tkanina"
  ]
  node [
    id 1015
    label "str&#243;j"
  ]
  node [
    id 1016
    label "podszycie"
  ]
  node [
    id 1017
    label "sp&#243;d"
  ]
  node [
    id 1018
    label "placek"
  ]
  node [
    id 1019
    label "bielizna"
  ]
  node [
    id 1020
    label "d&#243;&#322;"
  ]
  node [
    id 1021
    label "pru&#263;_si&#281;"
  ]
  node [
    id 1022
    label "maglownia"
  ]
  node [
    id 1023
    label "opalarnia"
  ]
  node [
    id 1024
    label "prucie_si&#281;"
  ]
  node [
    id 1025
    label "splot"
  ]
  node [
    id 1026
    label "karbonizowa&#263;"
  ]
  node [
    id 1027
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 1028
    label "karbonizacja"
  ]
  node [
    id 1029
    label "rozprucie_si&#281;"
  ]
  node [
    id 1030
    label "towar"
  ]
  node [
    id 1031
    label "apretura"
  ]
  node [
    id 1032
    label "las"
  ]
  node [
    id 1033
    label "dar&#324;"
  ]
  node [
    id 1034
    label "wyko&#324;czenie"
  ]
  node [
    id 1035
    label "przyszycie"
  ]
  node [
    id 1036
    label "zszycie"
  ]
  node [
    id 1037
    label "underbrush"
  ]
  node [
    id 1038
    label "pi&#281;tro"
  ]
  node [
    id 1039
    label "gorset"
  ]
  node [
    id 1040
    label "zrzucenie"
  ]
  node [
    id 1041
    label "znoszenie"
  ]
  node [
    id 1042
    label "kr&#243;j"
  ]
  node [
    id 1043
    label "ubranie_si&#281;"
  ]
  node [
    id 1044
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1045
    label "znosi&#263;"
  ]
  node [
    id 1046
    label "zrzuci&#263;"
  ]
  node [
    id 1047
    label "pasmanteria"
  ]
  node [
    id 1048
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 1049
    label "odzie&#380;"
  ]
  node [
    id 1050
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1051
    label "nosi&#263;"
  ]
  node [
    id 1052
    label "zasada"
  ]
  node [
    id 1053
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1054
    label "garderoba"
  ]
  node [
    id 1055
    label "odziewek"
  ]
  node [
    id 1056
    label "zajawka"
  ]
  node [
    id 1057
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 1058
    label "feblik"
  ]
  node [
    id 1059
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1060
    label "tendency"
  ]
  node [
    id 1061
    label "sympatia"
  ]
  node [
    id 1062
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1063
    label "podatno&#347;&#263;"
  ]
  node [
    id 1064
    label "zami&#322;owanie"
  ]
  node [
    id 1065
    label "streszczenie"
  ]
  node [
    id 1066
    label "harbinger"
  ]
  node [
    id 1067
    label "ch&#281;&#263;"
  ]
  node [
    id 1068
    label "zapowied&#378;"
  ]
  node [
    id 1069
    label "czasopismo"
  ]
  node [
    id 1070
    label "gadka"
  ]
  node [
    id 1071
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 1072
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 1073
    label "wyci&#261;&#263;"
  ]
  node [
    id 1074
    label "spa&#347;&#263;"
  ]
  node [
    id 1075
    label "wybi&#263;"
  ]
  node [
    id 1076
    label "uderzy&#263;"
  ]
  node [
    id 1077
    label "zrobi&#263;"
  ]
  node [
    id 1078
    label "slaughter"
  ]
  node [
    id 1079
    label "overwhelm"
  ]
  node [
    id 1080
    label "wyrze&#378;bi&#263;"
  ]
  node [
    id 1081
    label "przewr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1082
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 1083
    label "humanistycznie"
  ]
  node [
    id 1084
    label "utrudni&#263;"
  ]
  node [
    id 1085
    label "intervene"
  ]
  node [
    id 1086
    label "embarrass"
  ]
  node [
    id 1087
    label "uczenie_si&#281;"
  ]
  node [
    id 1088
    label "dzia&#322;anie"
  ]
  node [
    id 1089
    label "termination"
  ]
  node [
    id 1090
    label "completion"
  ]
  node [
    id 1091
    label "zako&#324;czenie"
  ]
  node [
    id 1092
    label "zrobienie"
  ]
  node [
    id 1093
    label "closing"
  ]
  node [
    id 1094
    label "zrezygnowanie"
  ]
  node [
    id 1095
    label "closure"
  ]
  node [
    id 1096
    label "ukszta&#322;towanie"
  ]
  node [
    id 1097
    label "conclusion"
  ]
  node [
    id 1098
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1099
    label "koniec"
  ]
  node [
    id 1100
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 1101
    label "adjustment"
  ]
  node [
    id 1102
    label "narobienie"
  ]
  node [
    id 1103
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1104
    label "creation"
  ]
  node [
    id 1105
    label "porobienie"
  ]
  node [
    id 1106
    label "infimum"
  ]
  node [
    id 1107
    label "liczenie"
  ]
  node [
    id 1108
    label "skutek"
  ]
  node [
    id 1109
    label "podzia&#322;anie"
  ]
  node [
    id 1110
    label "supremum"
  ]
  node [
    id 1111
    label "kampania"
  ]
  node [
    id 1112
    label "uruchamianie"
  ]
  node [
    id 1113
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1114
    label "operacja"
  ]
  node [
    id 1115
    label "jednostka"
  ]
  node [
    id 1116
    label "hipnotyzowanie"
  ]
  node [
    id 1117
    label "uruchomienie"
  ]
  node [
    id 1118
    label "nakr&#281;canie"
  ]
  node [
    id 1119
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1120
    label "matematyka"
  ]
  node [
    id 1121
    label "reakcja_chemiczna"
  ]
  node [
    id 1122
    label "tr&#243;jstronny"
  ]
  node [
    id 1123
    label "natural_process"
  ]
  node [
    id 1124
    label "nakr&#281;cenie"
  ]
  node [
    id 1125
    label "zatrzymanie"
  ]
  node [
    id 1126
    label "wp&#322;yw"
  ]
  node [
    id 1127
    label "rzut"
  ]
  node [
    id 1128
    label "podtrzymywanie"
  ]
  node [
    id 1129
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1130
    label "liczy&#263;"
  ]
  node [
    id 1131
    label "operation"
  ]
  node [
    id 1132
    label "rezultat"
  ]
  node [
    id 1133
    label "dzianie_si&#281;"
  ]
  node [
    id 1134
    label "zadzia&#322;anie"
  ]
  node [
    id 1135
    label "priorytet"
  ]
  node [
    id 1136
    label "bycie"
  ]
  node [
    id 1137
    label "kres"
  ]
  node [
    id 1138
    label "rozpocz&#281;cie"
  ]
  node [
    id 1139
    label "docieranie"
  ]
  node [
    id 1140
    label "funkcja"
  ]
  node [
    id 1141
    label "czynny"
  ]
  node [
    id 1142
    label "impact"
  ]
  node [
    id 1143
    label "oferta"
  ]
  node [
    id 1144
    label "act"
  ]
  node [
    id 1145
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1146
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1147
    label "zrecenzowanie"
  ]
  node [
    id 1148
    label "kontrola"
  ]
  node [
    id 1149
    label "analysis"
  ]
  node [
    id 1150
    label "rektalny"
  ]
  node [
    id 1151
    label "ustalenie"
  ]
  node [
    id 1152
    label "macanie"
  ]
  node [
    id 1153
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1154
    label "usi&#322;owanie"
  ]
  node [
    id 1155
    label "udowadnianie"
  ]
  node [
    id 1156
    label "bia&#322;a_niedziela"
  ]
  node [
    id 1157
    label "diagnostyka"
  ]
  node [
    id 1158
    label "dociekanie"
  ]
  node [
    id 1159
    label "sprawdzanie"
  ]
  node [
    id 1160
    label "penetrowanie"
  ]
  node [
    id 1161
    label "krytykowanie"
  ]
  node [
    id 1162
    label "omawianie"
  ]
  node [
    id 1163
    label "ustalanie"
  ]
  node [
    id 1164
    label "rozpatrywanie"
  ]
  node [
    id 1165
    label "investigation"
  ]
  node [
    id 1166
    label "wziernikowanie"
  ]
  node [
    id 1167
    label "examination"
  ]
  node [
    id 1168
    label "ekonomicznie"
  ]
  node [
    id 1169
    label "oszcz&#281;dny"
  ]
  node [
    id 1170
    label "korzystny"
  ]
  node [
    id 1171
    label "korzystnie"
  ]
  node [
    id 1172
    label "prosty"
  ]
  node [
    id 1173
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 1174
    label "rozwa&#380;ny"
  ]
  node [
    id 1175
    label "oszcz&#281;dnie"
  ]
  node [
    id 1176
    label "przeb&#243;j"
  ]
  node [
    id 1177
    label "Adam"
  ]
  node [
    id 1178
    label "Edyta"
  ]
  node [
    id 1179
    label "flis"
  ]
  node [
    id 1180
    label "&#347;wiatowy"
  ]
  node [
    id 1181
    label "przegl&#261;d"
  ]
  node [
    id 1182
    label "folklor"
  ]
  node [
    id 1183
    label "&#8222;"
  ]
  node [
    id 1184
    label "integracja"
  ]
  node [
    id 1185
    label "&#8221;"
  ]
  node [
    id 1186
    label "wyspa"
  ]
  node [
    id 1187
    label "fundacja"
  ]
  node [
    id 1188
    label "nowy"
  ]
  node [
    id 1189
    label "internetowy"
  ]
  node [
    id 1190
    label "wirtualny"
  ]
  node [
    id 1191
    label "konkurs"
  ]
  node [
    id 1192
    label "chopinowski"
  ]
  node [
    id 1193
    label "&#8211;"
  ]
  node [
    id 1194
    label "Chopin"
  ]
  node [
    id 1195
    label "Garage"
  ]
  node [
    id 1196
    label "bando"
  ]
  node [
    id 1197
    label "Marcin"
  ]
  node [
    id 1198
    label "grudzie&#324;"
  ]
  node [
    id 1199
    label "m&#322;odzie&#380;owy"
  ]
  node [
    id 1200
    label "akcja"
  ]
  node [
    id 1201
    label "multimedialny"
  ]
  node [
    id 1202
    label "stowarzyszy&#263;"
  ]
  node [
    id 1203
    label "m&#322;odzi"
  ]
  node [
    id 1204
    label "dziennikarz"
  ]
  node [
    id 1205
    label "polisa"
  ]
  node [
    id 1206
    label "unia"
  ]
  node [
    id 1207
    label "polski"
  ]
  node [
    id 1208
    label "PKO"
  ]
  node [
    id 1209
    label "b&#322;ogos&#322;awionej&#160;pami&#281;ci"
  ]
  node [
    id 1210
    label "ergo"
  ]
  node [
    id 1211
    label "Hestii"
  ]
  node [
    id 1212
    label "Micha&#322;"
  ]
  node [
    id 1213
    label "Horbulewicz"
  ]
  node [
    id 1214
    label "pod"
  ]
  node [
    id 1215
    label "wiatr"
  ]
  node [
    id 1216
    label "plus"
  ]
  node [
    id 1217
    label "Bydgoszcz"
  ]
  node [
    id 1218
    label "TVP"
  ]
  node [
    id 1219
    label "monitor"
  ]
  node [
    id 1220
    label "wiadomo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 1216
  ]
  edge [
    source 11
    target 1217
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 1176
  ]
  edge [
    source 13
    target 1177
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 374
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1176
  ]
  edge [
    source 16
    target 1177
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 688
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 415
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 684
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 313
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1014
  ]
  edge [
    source 20
    target 1015
  ]
  edge [
    source 20
    target 1016
  ]
  edge [
    source 20
    target 1017
  ]
  edge [
    source 20
    target 1018
  ]
  edge [
    source 20
    target 1019
  ]
  edge [
    source 20
    target 530
  ]
  edge [
    source 20
    target 1020
  ]
  edge [
    source 20
    target 1021
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 1022
  ]
  edge [
    source 20
    target 1023
  ]
  edge [
    source 20
    target 1024
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 20
    target 1027
  ]
  edge [
    source 20
    target 1028
  ]
  edge [
    source 20
    target 1029
  ]
  edge [
    source 20
    target 1030
  ]
  edge [
    source 20
    target 1031
  ]
  edge [
    source 20
    target 1032
  ]
  edge [
    source 20
    target 1033
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 1035
  ]
  edge [
    source 20
    target 1036
  ]
  edge [
    source 20
    target 1037
  ]
  edge [
    source 20
    target 1038
  ]
  edge [
    source 20
    target 1039
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 1041
  ]
  edge [
    source 20
    target 1042
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 20
    target 1045
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 1046
  ]
  edge [
    source 20
    target 1047
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 1048
  ]
  edge [
    source 20
    target 1049
  ]
  edge [
    source 20
    target 1050
  ]
  edge [
    source 20
    target 1051
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 757
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1087
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1090
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 1094
  ]
  edge [
    source 24
    target 1095
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1097
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1104
  ]
  edge [
    source 24
    target 1105
  ]
  edge [
    source 24
    target 780
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 765
  ]
  edge [
    source 24
    target 1107
  ]
  edge [
    source 24
    target 308
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1109
  ]
  edge [
    source 24
    target 1110
  ]
  edge [
    source 24
    target 1111
  ]
  edge [
    source 24
    target 1112
  ]
  edge [
    source 24
    target 1113
  ]
  edge [
    source 24
    target 1114
  ]
  edge [
    source 24
    target 1115
  ]
  edge [
    source 24
    target 1116
  ]
  edge [
    source 24
    target 775
  ]
  edge [
    source 24
    target 1117
  ]
  edge [
    source 24
    target 1118
  ]
  edge [
    source 24
    target 1119
  ]
  edge [
    source 24
    target 1120
  ]
  edge [
    source 24
    target 1121
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 42
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 1147
  ]
  edge [
    source 25
    target 1148
  ]
  edge [
    source 25
    target 1149
  ]
  edge [
    source 25
    target 1150
  ]
  edge [
    source 25
    target 1151
  ]
  edge [
    source 25
    target 1152
  ]
  edge [
    source 25
    target 1153
  ]
  edge [
    source 25
    target 1154
  ]
  edge [
    source 25
    target 1155
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 1156
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1132
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 780
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 107
  ]
  edge [
    source 25
    target 108
  ]
  edge [
    source 25
    target 109
  ]
  edge [
    source 25
    target 110
  ]
  edge [
    source 25
    target 111
  ]
  edge [
    source 25
    target 112
  ]
  edge [
    source 25
    target 113
  ]
  edge [
    source 25
    target 114
  ]
  edge [
    source 25
    target 115
  ]
  edge [
    source 25
    target 116
  ]
  edge [
    source 25
    target 117
  ]
  edge [
    source 25
    target 118
  ]
  edge [
    source 25
    target 41
  ]
  edge [
    source 25
    target 119
  ]
  edge [
    source 25
    target 120
  ]
  edge [
    source 25
    target 121
  ]
  edge [
    source 25
    target 122
  ]
  edge [
    source 25
    target 123
  ]
  edge [
    source 25
    target 124
  ]
  edge [
    source 25
    target 125
  ]
  edge [
    source 25
    target 126
  ]
  edge [
    source 26
    target 1168
  ]
  edge [
    source 26
    target 1169
  ]
  edge [
    source 26
    target 1170
  ]
  edge [
    source 26
    target 1171
  ]
  edge [
    source 26
    target 613
  ]
  edge [
    source 26
    target 1172
  ]
  edge [
    source 26
    target 1173
  ]
  edge [
    source 26
    target 1174
  ]
  edge [
    source 26
    target 1175
  ]
  edge [
    source 732
    target 1187
  ]
  edge [
    source 732
    target 1188
  ]
  edge [
    source 1176
    target 1177
  ]
  edge [
    source 1178
    target 1179
  ]
  edge [
    source 1180
    target 1181
  ]
  edge [
    source 1180
    target 1182
  ]
  edge [
    source 1180
    target 1183
  ]
  edge [
    source 1180
    target 1184
  ]
  edge [
    source 1180
    target 1185
  ]
  edge [
    source 1180
    target 1186
  ]
  edge [
    source 1181
    target 1182
  ]
  edge [
    source 1181
    target 1183
  ]
  edge [
    source 1181
    target 1184
  ]
  edge [
    source 1181
    target 1185
  ]
  edge [
    source 1181
    target 1186
  ]
  edge [
    source 1182
    target 1183
  ]
  edge [
    source 1182
    target 1184
  ]
  edge [
    source 1182
    target 1185
  ]
  edge [
    source 1182
    target 1186
  ]
  edge [
    source 1183
    target 1184
  ]
  edge [
    source 1183
    target 1185
  ]
  edge [
    source 1183
    target 1186
  ]
  edge [
    source 1183
    target 1202
  ]
  edge [
    source 1183
    target 1203
  ]
  edge [
    source 1183
    target 1204
  ]
  edge [
    source 1183
    target 1205
  ]
  edge [
    source 1184
    target 1185
  ]
  edge [
    source 1184
    target 1186
  ]
  edge [
    source 1185
    target 1186
  ]
  edge [
    source 1185
    target 1202
  ]
  edge [
    source 1185
    target 1203
  ]
  edge [
    source 1185
    target 1204
  ]
  edge [
    source 1185
    target 1205
  ]
  edge [
    source 1187
    target 1188
  ]
  edge [
    source 1189
    target 1190
  ]
  edge [
    source 1189
    target 1191
  ]
  edge [
    source 1189
    target 1192
  ]
  edge [
    source 1189
    target 1193
  ]
  edge [
    source 1189
    target 1194
  ]
  edge [
    source 1189
    target 1195
  ]
  edge [
    source 1189
    target 1196
  ]
  edge [
    source 1190
    target 1191
  ]
  edge [
    source 1190
    target 1192
  ]
  edge [
    source 1190
    target 1193
  ]
  edge [
    source 1190
    target 1194
  ]
  edge [
    source 1190
    target 1195
  ]
  edge [
    source 1190
    target 1196
  ]
  edge [
    source 1191
    target 1192
  ]
  edge [
    source 1191
    target 1193
  ]
  edge [
    source 1191
    target 1194
  ]
  edge [
    source 1191
    target 1195
  ]
  edge [
    source 1191
    target 1196
  ]
  edge [
    source 1192
    target 1193
  ]
  edge [
    source 1192
    target 1194
  ]
  edge [
    source 1192
    target 1195
  ]
  edge [
    source 1192
    target 1196
  ]
  edge [
    source 1193
    target 1194
  ]
  edge [
    source 1193
    target 1195
  ]
  edge [
    source 1193
    target 1196
  ]
  edge [
    source 1194
    target 1195
  ]
  edge [
    source 1194
    target 1196
  ]
  edge [
    source 1195
    target 1196
  ]
  edge [
    source 1197
    target 1198
  ]
  edge [
    source 1199
    target 1200
  ]
  edge [
    source 1199
    target 1201
  ]
  edge [
    source 1200
    target 1201
  ]
  edge [
    source 1202
    target 1203
  ]
  edge [
    source 1202
    target 1204
  ]
  edge [
    source 1202
    target 1205
  ]
  edge [
    source 1203
    target 1204
  ]
  edge [
    source 1203
    target 1205
  ]
  edge [
    source 1204
    target 1205
  ]
  edge [
    source 1206
    target 1207
  ]
  edge [
    source 1208
    target 1209
  ]
  edge [
    source 1210
    target 1211
  ]
  edge [
    source 1212
    target 1213
  ]
  edge [
    source 1214
    target 1215
  ]
  edge [
    source 1216
    target 1217
  ]
  edge [
    source 1217
    target 1218
  ]
  edge [
    source 1219
    target 1220
  ]
]
