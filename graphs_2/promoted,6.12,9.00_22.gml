graph [
  node [
    id 0
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 1
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "niesamowity"
    origin "text"
  ]
  node [
    id 3
    label "historia"
    origin "text"
  ]
  node [
    id 4
    label "czego"
    origin "text"
  ]
  node [
    id 5
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "poni&#380;szy"
    origin "text"
  ]
  node [
    id 8
    label "raj_utracony"
  ]
  node [
    id 9
    label "umieranie"
  ]
  node [
    id 10
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 11
    label "prze&#380;ywanie"
  ]
  node [
    id 12
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 13
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 14
    label "po&#322;&#243;g"
  ]
  node [
    id 15
    label "umarcie"
  ]
  node [
    id 16
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 17
    label "subsistence"
  ]
  node [
    id 18
    label "power"
  ]
  node [
    id 19
    label "okres_noworodkowy"
  ]
  node [
    id 20
    label "prze&#380;ycie"
  ]
  node [
    id 21
    label "wiek_matuzalemowy"
  ]
  node [
    id 22
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 23
    label "entity"
  ]
  node [
    id 24
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 25
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 26
    label "do&#380;ywanie"
  ]
  node [
    id 27
    label "byt"
  ]
  node [
    id 28
    label "andropauza"
  ]
  node [
    id 29
    label "dzieci&#324;stwo"
  ]
  node [
    id 30
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 31
    label "rozw&#243;j"
  ]
  node [
    id 32
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 33
    label "czas"
  ]
  node [
    id 34
    label "menopauza"
  ]
  node [
    id 35
    label "&#347;mier&#263;"
  ]
  node [
    id 36
    label "koleje_losu"
  ]
  node [
    id 37
    label "bycie"
  ]
  node [
    id 38
    label "zegar_biologiczny"
  ]
  node [
    id 39
    label "szwung"
  ]
  node [
    id 40
    label "przebywanie"
  ]
  node [
    id 41
    label "warunki"
  ]
  node [
    id 42
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 43
    label "niemowl&#281;ctwo"
  ]
  node [
    id 44
    label "&#380;ywy"
  ]
  node [
    id 45
    label "life"
  ]
  node [
    id 46
    label "staro&#347;&#263;"
  ]
  node [
    id 47
    label "energy"
  ]
  node [
    id 48
    label "trwanie"
  ]
  node [
    id 49
    label "wra&#380;enie"
  ]
  node [
    id 50
    label "przej&#347;cie"
  ]
  node [
    id 51
    label "doznanie"
  ]
  node [
    id 52
    label "poradzenie_sobie"
  ]
  node [
    id 53
    label "przetrwanie"
  ]
  node [
    id 54
    label "survival"
  ]
  node [
    id 55
    label "przechodzenie"
  ]
  node [
    id 56
    label "wytrzymywanie"
  ]
  node [
    id 57
    label "zaznawanie"
  ]
  node [
    id 58
    label "obejrzenie"
  ]
  node [
    id 59
    label "widzenie"
  ]
  node [
    id 60
    label "urzeczywistnianie"
  ]
  node [
    id 61
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 62
    label "przeszkodzenie"
  ]
  node [
    id 63
    label "produkowanie"
  ]
  node [
    id 64
    label "being"
  ]
  node [
    id 65
    label "znikni&#281;cie"
  ]
  node [
    id 66
    label "robienie"
  ]
  node [
    id 67
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 68
    label "przeszkadzanie"
  ]
  node [
    id 69
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 70
    label "wyprodukowanie"
  ]
  node [
    id 71
    label "utrzymywanie"
  ]
  node [
    id 72
    label "subsystencja"
  ]
  node [
    id 73
    label "utrzyma&#263;"
  ]
  node [
    id 74
    label "egzystencja"
  ]
  node [
    id 75
    label "wy&#380;ywienie"
  ]
  node [
    id 76
    label "ontologicznie"
  ]
  node [
    id 77
    label "utrzymanie"
  ]
  node [
    id 78
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 79
    label "potencja"
  ]
  node [
    id 80
    label "utrzymywa&#263;"
  ]
  node [
    id 81
    label "status"
  ]
  node [
    id 82
    label "sytuacja"
  ]
  node [
    id 83
    label "poprzedzanie"
  ]
  node [
    id 84
    label "czasoprzestrze&#324;"
  ]
  node [
    id 85
    label "laba"
  ]
  node [
    id 86
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 87
    label "chronometria"
  ]
  node [
    id 88
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 89
    label "rachuba_czasu"
  ]
  node [
    id 90
    label "przep&#322;ywanie"
  ]
  node [
    id 91
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 92
    label "czasokres"
  ]
  node [
    id 93
    label "odczyt"
  ]
  node [
    id 94
    label "chwila"
  ]
  node [
    id 95
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 96
    label "dzieje"
  ]
  node [
    id 97
    label "kategoria_gramatyczna"
  ]
  node [
    id 98
    label "poprzedzenie"
  ]
  node [
    id 99
    label "trawienie"
  ]
  node [
    id 100
    label "pochodzi&#263;"
  ]
  node [
    id 101
    label "period"
  ]
  node [
    id 102
    label "okres_czasu"
  ]
  node [
    id 103
    label "poprzedza&#263;"
  ]
  node [
    id 104
    label "schy&#322;ek"
  ]
  node [
    id 105
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 106
    label "odwlekanie_si&#281;"
  ]
  node [
    id 107
    label "zegar"
  ]
  node [
    id 108
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 109
    label "czwarty_wymiar"
  ]
  node [
    id 110
    label "pochodzenie"
  ]
  node [
    id 111
    label "koniugacja"
  ]
  node [
    id 112
    label "Zeitgeist"
  ]
  node [
    id 113
    label "trawi&#263;"
  ]
  node [
    id 114
    label "pogoda"
  ]
  node [
    id 115
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 116
    label "poprzedzi&#263;"
  ]
  node [
    id 117
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 118
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 119
    label "time_period"
  ]
  node [
    id 120
    label "ocieranie_si&#281;"
  ]
  node [
    id 121
    label "otoczenie_si&#281;"
  ]
  node [
    id 122
    label "posiedzenie"
  ]
  node [
    id 123
    label "otarcie_si&#281;"
  ]
  node [
    id 124
    label "atakowanie"
  ]
  node [
    id 125
    label "otaczanie_si&#281;"
  ]
  node [
    id 126
    label "wyj&#347;cie"
  ]
  node [
    id 127
    label "zmierzanie"
  ]
  node [
    id 128
    label "residency"
  ]
  node [
    id 129
    label "sojourn"
  ]
  node [
    id 130
    label "wychodzenie"
  ]
  node [
    id 131
    label "tkwienie"
  ]
  node [
    id 132
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 133
    label "absolutorium"
  ]
  node [
    id 134
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 135
    label "dzia&#322;anie"
  ]
  node [
    id 136
    label "activity"
  ]
  node [
    id 137
    label "ton"
  ]
  node [
    id 138
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 139
    label "cecha"
  ]
  node [
    id 140
    label "korkowanie"
  ]
  node [
    id 141
    label "death"
  ]
  node [
    id 142
    label "zabijanie"
  ]
  node [
    id 143
    label "martwy"
  ]
  node [
    id 144
    label "przestawanie"
  ]
  node [
    id 145
    label "odumieranie"
  ]
  node [
    id 146
    label "zdychanie"
  ]
  node [
    id 147
    label "stan"
  ]
  node [
    id 148
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 149
    label "zanikanie"
  ]
  node [
    id 150
    label "ko&#324;czenie"
  ]
  node [
    id 151
    label "nieuleczalnie_chory"
  ]
  node [
    id 152
    label "ciekawy"
  ]
  node [
    id 153
    label "szybki"
  ]
  node [
    id 154
    label "&#380;ywotny"
  ]
  node [
    id 155
    label "naturalny"
  ]
  node [
    id 156
    label "&#380;ywo"
  ]
  node [
    id 157
    label "cz&#322;owiek"
  ]
  node [
    id 158
    label "o&#380;ywianie"
  ]
  node [
    id 159
    label "silny"
  ]
  node [
    id 160
    label "g&#322;&#281;boki"
  ]
  node [
    id 161
    label "wyra&#378;ny"
  ]
  node [
    id 162
    label "czynny"
  ]
  node [
    id 163
    label "aktualny"
  ]
  node [
    id 164
    label "zgrabny"
  ]
  node [
    id 165
    label "prawdziwy"
  ]
  node [
    id 166
    label "realistyczny"
  ]
  node [
    id 167
    label "energiczny"
  ]
  node [
    id 168
    label "odumarcie"
  ]
  node [
    id 169
    label "przestanie"
  ]
  node [
    id 170
    label "dysponowanie_si&#281;"
  ]
  node [
    id 171
    label "pomarcie"
  ]
  node [
    id 172
    label "die"
  ]
  node [
    id 173
    label "sko&#324;czenie"
  ]
  node [
    id 174
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 175
    label "zdechni&#281;cie"
  ]
  node [
    id 176
    label "zabicie"
  ]
  node [
    id 177
    label "procedura"
  ]
  node [
    id 178
    label "proces"
  ]
  node [
    id 179
    label "proces_biologiczny"
  ]
  node [
    id 180
    label "z&#322;ote_czasy"
  ]
  node [
    id 181
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 182
    label "process"
  ]
  node [
    id 183
    label "cycle"
  ]
  node [
    id 184
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 185
    label "adolescence"
  ]
  node [
    id 186
    label "wiek"
  ]
  node [
    id 187
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 188
    label "zielone_lata"
  ]
  node [
    id 189
    label "rozwi&#261;zanie"
  ]
  node [
    id 190
    label "zlec"
  ]
  node [
    id 191
    label "zlegni&#281;cie"
  ]
  node [
    id 192
    label "defenestracja"
  ]
  node [
    id 193
    label "agonia"
  ]
  node [
    id 194
    label "kres"
  ]
  node [
    id 195
    label "mogi&#322;a"
  ]
  node [
    id 196
    label "kres_&#380;ycia"
  ]
  node [
    id 197
    label "upadek"
  ]
  node [
    id 198
    label "szeol"
  ]
  node [
    id 199
    label "pogrzebanie"
  ]
  node [
    id 200
    label "istota_nadprzyrodzona"
  ]
  node [
    id 201
    label "&#380;a&#322;oba"
  ]
  node [
    id 202
    label "pogrzeb"
  ]
  node [
    id 203
    label "majority"
  ]
  node [
    id 204
    label "osiemnastoletni"
  ]
  node [
    id 205
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 206
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 207
    label "age"
  ]
  node [
    id 208
    label "kobieta"
  ]
  node [
    id 209
    label "przekwitanie"
  ]
  node [
    id 210
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 211
    label "dzieci&#281;ctwo"
  ]
  node [
    id 212
    label "energia"
  ]
  node [
    id 213
    label "zapa&#322;"
  ]
  node [
    id 214
    label "formu&#322;owa&#263;"
  ]
  node [
    id 215
    label "ozdabia&#263;"
  ]
  node [
    id 216
    label "stawia&#263;"
  ]
  node [
    id 217
    label "spell"
  ]
  node [
    id 218
    label "styl"
  ]
  node [
    id 219
    label "skryba"
  ]
  node [
    id 220
    label "read"
  ]
  node [
    id 221
    label "donosi&#263;"
  ]
  node [
    id 222
    label "code"
  ]
  node [
    id 223
    label "tekst"
  ]
  node [
    id 224
    label "dysgrafia"
  ]
  node [
    id 225
    label "dysortografia"
  ]
  node [
    id 226
    label "tworzy&#263;"
  ]
  node [
    id 227
    label "prasa"
  ]
  node [
    id 228
    label "robi&#263;"
  ]
  node [
    id 229
    label "pope&#322;nia&#263;"
  ]
  node [
    id 230
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 231
    label "wytwarza&#263;"
  ]
  node [
    id 232
    label "get"
  ]
  node [
    id 233
    label "consist"
  ]
  node [
    id 234
    label "stanowi&#263;"
  ]
  node [
    id 235
    label "raise"
  ]
  node [
    id 236
    label "spill_the_beans"
  ]
  node [
    id 237
    label "przeby&#263;"
  ]
  node [
    id 238
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 239
    label "zanosi&#263;"
  ]
  node [
    id 240
    label "inform"
  ]
  node [
    id 241
    label "give"
  ]
  node [
    id 242
    label "zu&#380;y&#263;"
  ]
  node [
    id 243
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 244
    label "introduce"
  ]
  node [
    id 245
    label "render"
  ]
  node [
    id 246
    label "ci&#261;&#380;a"
  ]
  node [
    id 247
    label "informowa&#263;"
  ]
  node [
    id 248
    label "komunikowa&#263;"
  ]
  node [
    id 249
    label "convey"
  ]
  node [
    id 250
    label "pozostawia&#263;"
  ]
  node [
    id 251
    label "czyni&#263;"
  ]
  node [
    id 252
    label "wydawa&#263;"
  ]
  node [
    id 253
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 254
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 255
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 256
    label "przewidywa&#263;"
  ]
  node [
    id 257
    label "przyznawa&#263;"
  ]
  node [
    id 258
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 259
    label "go"
  ]
  node [
    id 260
    label "obstawia&#263;"
  ]
  node [
    id 261
    label "umieszcza&#263;"
  ]
  node [
    id 262
    label "ocenia&#263;"
  ]
  node [
    id 263
    label "zastawia&#263;"
  ]
  node [
    id 264
    label "stanowisko"
  ]
  node [
    id 265
    label "znak"
  ]
  node [
    id 266
    label "wskazywa&#263;"
  ]
  node [
    id 267
    label "uruchamia&#263;"
  ]
  node [
    id 268
    label "fundowa&#263;"
  ]
  node [
    id 269
    label "zmienia&#263;"
  ]
  node [
    id 270
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 271
    label "deliver"
  ]
  node [
    id 272
    label "powodowa&#263;"
  ]
  node [
    id 273
    label "wyznacza&#263;"
  ]
  node [
    id 274
    label "przedstawia&#263;"
  ]
  node [
    id 275
    label "wydobywa&#263;"
  ]
  node [
    id 276
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 277
    label "trim"
  ]
  node [
    id 278
    label "gryzipi&#243;rek"
  ]
  node [
    id 279
    label "pisarz"
  ]
  node [
    id 280
    label "ekscerpcja"
  ]
  node [
    id 281
    label "j&#281;zykowo"
  ]
  node [
    id 282
    label "wypowied&#378;"
  ]
  node [
    id 283
    label "redakcja"
  ]
  node [
    id 284
    label "wytw&#243;r"
  ]
  node [
    id 285
    label "pomini&#281;cie"
  ]
  node [
    id 286
    label "dzie&#322;o"
  ]
  node [
    id 287
    label "preparacja"
  ]
  node [
    id 288
    label "odmianka"
  ]
  node [
    id 289
    label "opu&#347;ci&#263;"
  ]
  node [
    id 290
    label "koniektura"
  ]
  node [
    id 291
    label "obelga"
  ]
  node [
    id 292
    label "zesp&#243;&#322;"
  ]
  node [
    id 293
    label "t&#322;oczysko"
  ]
  node [
    id 294
    label "depesza"
  ]
  node [
    id 295
    label "maszyna"
  ]
  node [
    id 296
    label "media"
  ]
  node [
    id 297
    label "napisa&#263;"
  ]
  node [
    id 298
    label "czasopismo"
  ]
  node [
    id 299
    label "dziennikarz_prasowy"
  ]
  node [
    id 300
    label "kiosk"
  ]
  node [
    id 301
    label "maszyna_rolnicza"
  ]
  node [
    id 302
    label "gazeta"
  ]
  node [
    id 303
    label "trzonek"
  ]
  node [
    id 304
    label "reakcja"
  ]
  node [
    id 305
    label "narz&#281;dzie"
  ]
  node [
    id 306
    label "spos&#243;b"
  ]
  node [
    id 307
    label "zbi&#243;r"
  ]
  node [
    id 308
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 309
    label "zachowanie"
  ]
  node [
    id 310
    label "stylik"
  ]
  node [
    id 311
    label "dyscyplina_sportowa"
  ]
  node [
    id 312
    label "handle"
  ]
  node [
    id 313
    label "stroke"
  ]
  node [
    id 314
    label "line"
  ]
  node [
    id 315
    label "charakter"
  ]
  node [
    id 316
    label "natural_language"
  ]
  node [
    id 317
    label "kanon"
  ]
  node [
    id 318
    label "behawior"
  ]
  node [
    id 319
    label "dysleksja"
  ]
  node [
    id 320
    label "pisanie"
  ]
  node [
    id 321
    label "dysgraphia"
  ]
  node [
    id 322
    label "niesamowicie"
  ]
  node [
    id 323
    label "niezwyk&#322;y"
  ]
  node [
    id 324
    label "niezwykle"
  ]
  node [
    id 325
    label "inny"
  ]
  node [
    id 326
    label "niesamowito"
  ]
  node [
    id 327
    label "bardzo"
  ]
  node [
    id 328
    label "historiografia"
  ]
  node [
    id 329
    label "nauka_humanistyczna"
  ]
  node [
    id 330
    label "nautologia"
  ]
  node [
    id 331
    label "przedmiot"
  ]
  node [
    id 332
    label "epigrafika"
  ]
  node [
    id 333
    label "muzealnictwo"
  ]
  node [
    id 334
    label "report"
  ]
  node [
    id 335
    label "hista"
  ]
  node [
    id 336
    label "przebiec"
  ]
  node [
    id 337
    label "zabytkoznawstwo"
  ]
  node [
    id 338
    label "historia_gospodarcza"
  ]
  node [
    id 339
    label "motyw"
  ]
  node [
    id 340
    label "kierunek"
  ]
  node [
    id 341
    label "varsavianistyka"
  ]
  node [
    id 342
    label "filigranistyka"
  ]
  node [
    id 343
    label "neografia"
  ]
  node [
    id 344
    label "prezentyzm"
  ]
  node [
    id 345
    label "genealogia"
  ]
  node [
    id 346
    label "ikonografia"
  ]
  node [
    id 347
    label "bizantynistyka"
  ]
  node [
    id 348
    label "epoka"
  ]
  node [
    id 349
    label "historia_sztuki"
  ]
  node [
    id 350
    label "ruralistyka"
  ]
  node [
    id 351
    label "annalistyka"
  ]
  node [
    id 352
    label "papirologia"
  ]
  node [
    id 353
    label "heraldyka"
  ]
  node [
    id 354
    label "archiwistyka"
  ]
  node [
    id 355
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 356
    label "dyplomatyka"
  ]
  node [
    id 357
    label "czynno&#347;&#263;"
  ]
  node [
    id 358
    label "numizmatyka"
  ]
  node [
    id 359
    label "chronologia"
  ]
  node [
    id 360
    label "historyka"
  ]
  node [
    id 361
    label "prozopografia"
  ]
  node [
    id 362
    label "sfragistyka"
  ]
  node [
    id 363
    label "weksylologia"
  ]
  node [
    id 364
    label "paleografia"
  ]
  node [
    id 365
    label "mediewistyka"
  ]
  node [
    id 366
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 367
    label "przebiegni&#281;cie"
  ]
  node [
    id 368
    label "fabu&#322;a"
  ]
  node [
    id 369
    label "zboczenie"
  ]
  node [
    id 370
    label "om&#243;wienie"
  ]
  node [
    id 371
    label "sponiewieranie"
  ]
  node [
    id 372
    label "discipline"
  ]
  node [
    id 373
    label "rzecz"
  ]
  node [
    id 374
    label "omawia&#263;"
  ]
  node [
    id 375
    label "kr&#261;&#380;enie"
  ]
  node [
    id 376
    label "tre&#347;&#263;"
  ]
  node [
    id 377
    label "sponiewiera&#263;"
  ]
  node [
    id 378
    label "element"
  ]
  node [
    id 379
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 380
    label "tematyka"
  ]
  node [
    id 381
    label "w&#261;tek"
  ]
  node [
    id 382
    label "zbaczanie"
  ]
  node [
    id 383
    label "program_nauczania"
  ]
  node [
    id 384
    label "om&#243;wi&#263;"
  ]
  node [
    id 385
    label "omawianie"
  ]
  node [
    id 386
    label "thing"
  ]
  node [
    id 387
    label "kultura"
  ]
  node [
    id 388
    label "istota"
  ]
  node [
    id 389
    label "zbacza&#263;"
  ]
  node [
    id 390
    label "zboczy&#263;"
  ]
  node [
    id 391
    label "pos&#322;uchanie"
  ]
  node [
    id 392
    label "s&#261;d"
  ]
  node [
    id 393
    label "sparafrazowanie"
  ]
  node [
    id 394
    label "strawestowa&#263;"
  ]
  node [
    id 395
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 396
    label "trawestowa&#263;"
  ]
  node [
    id 397
    label "sparafrazowa&#263;"
  ]
  node [
    id 398
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 399
    label "sformu&#322;owanie"
  ]
  node [
    id 400
    label "parafrazowanie"
  ]
  node [
    id 401
    label "ozdobnik"
  ]
  node [
    id 402
    label "delimitacja"
  ]
  node [
    id 403
    label "parafrazowa&#263;"
  ]
  node [
    id 404
    label "stylizacja"
  ]
  node [
    id 405
    label "komunikat"
  ]
  node [
    id 406
    label "trawestowanie"
  ]
  node [
    id 407
    label "strawestowanie"
  ]
  node [
    id 408
    label "rezultat"
  ]
  node [
    id 409
    label "przebieg"
  ]
  node [
    id 410
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 411
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 412
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 413
    label "praktyka"
  ]
  node [
    id 414
    label "system"
  ]
  node [
    id 415
    label "przeorientowywanie"
  ]
  node [
    id 416
    label "studia"
  ]
  node [
    id 417
    label "linia"
  ]
  node [
    id 418
    label "bok"
  ]
  node [
    id 419
    label "skr&#281;canie"
  ]
  node [
    id 420
    label "skr&#281;ca&#263;"
  ]
  node [
    id 421
    label "przeorientowywa&#263;"
  ]
  node [
    id 422
    label "orientowanie"
  ]
  node [
    id 423
    label "skr&#281;ci&#263;"
  ]
  node [
    id 424
    label "przeorientowanie"
  ]
  node [
    id 425
    label "zorientowanie"
  ]
  node [
    id 426
    label "przeorientowa&#263;"
  ]
  node [
    id 427
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 428
    label "metoda"
  ]
  node [
    id 429
    label "ty&#322;"
  ]
  node [
    id 430
    label "zorientowa&#263;"
  ]
  node [
    id 431
    label "g&#243;ra"
  ]
  node [
    id 432
    label "orientowa&#263;"
  ]
  node [
    id 433
    label "ideologia"
  ]
  node [
    id 434
    label "orientacja"
  ]
  node [
    id 435
    label "prz&#243;d"
  ]
  node [
    id 436
    label "bearing"
  ]
  node [
    id 437
    label "skr&#281;cenie"
  ]
  node [
    id 438
    label "aalen"
  ]
  node [
    id 439
    label "jura_wczesna"
  ]
  node [
    id 440
    label "holocen"
  ]
  node [
    id 441
    label "pliocen"
  ]
  node [
    id 442
    label "plejstocen"
  ]
  node [
    id 443
    label "paleocen"
  ]
  node [
    id 444
    label "bajos"
  ]
  node [
    id 445
    label "kelowej"
  ]
  node [
    id 446
    label "eocen"
  ]
  node [
    id 447
    label "jednostka_geologiczna"
  ]
  node [
    id 448
    label "okres"
  ]
  node [
    id 449
    label "miocen"
  ]
  node [
    id 450
    label "&#347;rodkowy_trias"
  ]
  node [
    id 451
    label "term"
  ]
  node [
    id 452
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 453
    label "wczesny_trias"
  ]
  node [
    id 454
    label "jura_&#347;rodkowa"
  ]
  node [
    id 455
    label "oligocen"
  ]
  node [
    id 456
    label "w&#281;ze&#322;"
  ]
  node [
    id 457
    label "perypetia"
  ]
  node [
    id 458
    label "opowiadanie"
  ]
  node [
    id 459
    label "datacja"
  ]
  node [
    id 460
    label "dendrochronologia"
  ]
  node [
    id 461
    label "kolejno&#347;&#263;"
  ]
  node [
    id 462
    label "plastyka"
  ]
  node [
    id 463
    label "&#347;redniowiecze"
  ]
  node [
    id 464
    label "descendencja"
  ]
  node [
    id 465
    label "drzewo_genealogiczne"
  ]
  node [
    id 466
    label "procedencja"
  ]
  node [
    id 467
    label "medal"
  ]
  node [
    id 468
    label "kolekcjonerstwo"
  ]
  node [
    id 469
    label "numismatics"
  ]
  node [
    id 470
    label "archeologia"
  ]
  node [
    id 471
    label "archiwoznawstwo"
  ]
  node [
    id 472
    label "Byzantine_Empire"
  ]
  node [
    id 473
    label "pismo"
  ]
  node [
    id 474
    label "brachygrafia"
  ]
  node [
    id 475
    label "architektura"
  ]
  node [
    id 476
    label "nauka"
  ]
  node [
    id 477
    label "oksza"
  ]
  node [
    id 478
    label "pas"
  ]
  node [
    id 479
    label "s&#322;up"
  ]
  node [
    id 480
    label "barwa_heraldyczna"
  ]
  node [
    id 481
    label "herb"
  ]
  node [
    id 482
    label "or&#281;&#380;"
  ]
  node [
    id 483
    label "museum"
  ]
  node [
    id 484
    label "bibliologia"
  ]
  node [
    id 485
    label "historiography"
  ]
  node [
    id 486
    label "pi&#347;miennictwo"
  ]
  node [
    id 487
    label "metodologia"
  ]
  node [
    id 488
    label "fraza"
  ]
  node [
    id 489
    label "temat"
  ]
  node [
    id 490
    label "wydarzenie"
  ]
  node [
    id 491
    label "melodia"
  ]
  node [
    id 492
    label "przyczyna"
  ]
  node [
    id 493
    label "ozdoba"
  ]
  node [
    id 494
    label "umowa"
  ]
  node [
    id 495
    label "cover"
  ]
  node [
    id 496
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 497
    label "osobowo&#347;&#263;"
  ]
  node [
    id 498
    label "psychika"
  ]
  node [
    id 499
    label "posta&#263;"
  ]
  node [
    id 500
    label "kompleksja"
  ]
  node [
    id 501
    label "fizjonomia"
  ]
  node [
    id 502
    label "zjawisko"
  ]
  node [
    id 503
    label "bezproblemowy"
  ]
  node [
    id 504
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 505
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 506
    label "run"
  ]
  node [
    id 507
    label "proceed"
  ]
  node [
    id 508
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 509
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 510
    label "przemierzy&#263;"
  ]
  node [
    id 511
    label "fly"
  ]
  node [
    id 512
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 513
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 514
    label "przesun&#261;&#263;"
  ]
  node [
    id 515
    label "przemkni&#281;cie"
  ]
  node [
    id 516
    label "zabrzmienie"
  ]
  node [
    id 517
    label "przebycie"
  ]
  node [
    id 518
    label "zdarzenie_si&#281;"
  ]
  node [
    id 519
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 520
    label "fakt"
  ]
  node [
    id 521
    label "czyn"
  ]
  node [
    id 522
    label "ilustracja"
  ]
  node [
    id 523
    label "przedstawiciel"
  ]
  node [
    id 524
    label "materia&#322;"
  ]
  node [
    id 525
    label "szata_graficzna"
  ]
  node [
    id 526
    label "photograph"
  ]
  node [
    id 527
    label "obrazek"
  ]
  node [
    id 528
    label "bia&#322;e_plamy"
  ]
  node [
    id 529
    label "ludzko&#347;&#263;"
  ]
  node [
    id 530
    label "asymilowanie"
  ]
  node [
    id 531
    label "wapniak"
  ]
  node [
    id 532
    label "asymilowa&#263;"
  ]
  node [
    id 533
    label "os&#322;abia&#263;"
  ]
  node [
    id 534
    label "hominid"
  ]
  node [
    id 535
    label "podw&#322;adny"
  ]
  node [
    id 536
    label "os&#322;abianie"
  ]
  node [
    id 537
    label "g&#322;owa"
  ]
  node [
    id 538
    label "figura"
  ]
  node [
    id 539
    label "portrecista"
  ]
  node [
    id 540
    label "dwun&#243;g"
  ]
  node [
    id 541
    label "profanum"
  ]
  node [
    id 542
    label "mikrokosmos"
  ]
  node [
    id 543
    label "nasada"
  ]
  node [
    id 544
    label "duch"
  ]
  node [
    id 545
    label "antropochoria"
  ]
  node [
    id 546
    label "osoba"
  ]
  node [
    id 547
    label "wz&#243;r"
  ]
  node [
    id 548
    label "senior"
  ]
  node [
    id 549
    label "oddzia&#322;ywanie"
  ]
  node [
    id 550
    label "Adam"
  ]
  node [
    id 551
    label "homo_sapiens"
  ]
  node [
    id 552
    label "polifag"
  ]
  node [
    id 553
    label "funkcja"
  ]
  node [
    id 554
    label "act"
  ]
  node [
    id 555
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 556
    label "cz&#322;onek"
  ]
  node [
    id 557
    label "substytuowa&#263;"
  ]
  node [
    id 558
    label "substytuowanie"
  ]
  node [
    id 559
    label "zast&#281;pca"
  ]
  node [
    id 560
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 561
    label "mie&#263;_miejsce"
  ]
  node [
    id 562
    label "equal"
  ]
  node [
    id 563
    label "trwa&#263;"
  ]
  node [
    id 564
    label "chodzi&#263;"
  ]
  node [
    id 565
    label "si&#281;ga&#263;"
  ]
  node [
    id 566
    label "obecno&#347;&#263;"
  ]
  node [
    id 567
    label "stand"
  ]
  node [
    id 568
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 569
    label "uczestniczy&#263;"
  ]
  node [
    id 570
    label "participate"
  ]
  node [
    id 571
    label "istnie&#263;"
  ]
  node [
    id 572
    label "pozostawa&#263;"
  ]
  node [
    id 573
    label "zostawa&#263;"
  ]
  node [
    id 574
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 575
    label "adhere"
  ]
  node [
    id 576
    label "compass"
  ]
  node [
    id 577
    label "korzysta&#263;"
  ]
  node [
    id 578
    label "appreciation"
  ]
  node [
    id 579
    label "osi&#261;ga&#263;"
  ]
  node [
    id 580
    label "dociera&#263;"
  ]
  node [
    id 581
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 582
    label "mierzy&#263;"
  ]
  node [
    id 583
    label "u&#380;ywa&#263;"
  ]
  node [
    id 584
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 585
    label "exsert"
  ]
  node [
    id 586
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 587
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 588
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 589
    label "p&#322;ywa&#263;"
  ]
  node [
    id 590
    label "bangla&#263;"
  ]
  node [
    id 591
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 592
    label "przebiega&#263;"
  ]
  node [
    id 593
    label "wk&#322;ada&#263;"
  ]
  node [
    id 594
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 595
    label "carry"
  ]
  node [
    id 596
    label "bywa&#263;"
  ]
  node [
    id 597
    label "dziama&#263;"
  ]
  node [
    id 598
    label "stara&#263;_si&#281;"
  ]
  node [
    id 599
    label "para"
  ]
  node [
    id 600
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 601
    label "str&#243;j"
  ]
  node [
    id 602
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 603
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 604
    label "krok"
  ]
  node [
    id 605
    label "tryb"
  ]
  node [
    id 606
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 607
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 608
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 609
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 610
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 611
    label "continue"
  ]
  node [
    id 612
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 613
    label "Ohio"
  ]
  node [
    id 614
    label "wci&#281;cie"
  ]
  node [
    id 615
    label "Nowy_York"
  ]
  node [
    id 616
    label "warstwa"
  ]
  node [
    id 617
    label "samopoczucie"
  ]
  node [
    id 618
    label "Illinois"
  ]
  node [
    id 619
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 620
    label "state"
  ]
  node [
    id 621
    label "Jukatan"
  ]
  node [
    id 622
    label "Kalifornia"
  ]
  node [
    id 623
    label "Wirginia"
  ]
  node [
    id 624
    label "wektor"
  ]
  node [
    id 625
    label "Teksas"
  ]
  node [
    id 626
    label "Goa"
  ]
  node [
    id 627
    label "Waszyngton"
  ]
  node [
    id 628
    label "miejsce"
  ]
  node [
    id 629
    label "Massachusetts"
  ]
  node [
    id 630
    label "Alaska"
  ]
  node [
    id 631
    label "Arakan"
  ]
  node [
    id 632
    label "Hawaje"
  ]
  node [
    id 633
    label "Maryland"
  ]
  node [
    id 634
    label "punkt"
  ]
  node [
    id 635
    label "Michigan"
  ]
  node [
    id 636
    label "Arizona"
  ]
  node [
    id 637
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 638
    label "Georgia"
  ]
  node [
    id 639
    label "poziom"
  ]
  node [
    id 640
    label "Pensylwania"
  ]
  node [
    id 641
    label "shape"
  ]
  node [
    id 642
    label "Luizjana"
  ]
  node [
    id 643
    label "Nowy_Meksyk"
  ]
  node [
    id 644
    label "Alabama"
  ]
  node [
    id 645
    label "ilo&#347;&#263;"
  ]
  node [
    id 646
    label "Kansas"
  ]
  node [
    id 647
    label "Oregon"
  ]
  node [
    id 648
    label "Floryda"
  ]
  node [
    id 649
    label "Oklahoma"
  ]
  node [
    id 650
    label "jednostka_administracyjna"
  ]
  node [
    id 651
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 652
    label "kolejny"
  ]
  node [
    id 653
    label "poni&#380;ej"
  ]
  node [
    id 654
    label "ten"
  ]
  node [
    id 655
    label "okre&#347;lony"
  ]
  node [
    id 656
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 657
    label "nast&#281;pnie"
  ]
  node [
    id 658
    label "nastopny"
  ]
  node [
    id 659
    label "kolejno"
  ]
  node [
    id 660
    label "kt&#243;ry&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
]
