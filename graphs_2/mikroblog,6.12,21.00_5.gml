graph [
  node [
    id 0
    label "ida"
    origin "text"
  ]
  node [
    id 1
    label "opowiada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzieciak"
    origin "text"
  ]
  node [
    id 3
    label "mineralach"
    origin "text"
  ]
  node [
    id 4
    label "potem"
    origin "text"
  ]
  node [
    id 5
    label "pensja"
    origin "text"
  ]
  node [
    id 6
    label "wasi"
    origin "text"
  ]
  node [
    id 7
    label "podatek"
    origin "text"
  ]
  node [
    id 8
    label "prawi&#263;"
  ]
  node [
    id 9
    label "relate"
  ]
  node [
    id 10
    label "przedstawia&#263;"
  ]
  node [
    id 11
    label "teatr"
  ]
  node [
    id 12
    label "exhibit"
  ]
  node [
    id 13
    label "podawa&#263;"
  ]
  node [
    id 14
    label "display"
  ]
  node [
    id 15
    label "pokazywa&#263;"
  ]
  node [
    id 16
    label "demonstrowa&#263;"
  ]
  node [
    id 17
    label "przedstawienie"
  ]
  node [
    id 18
    label "zapoznawa&#263;"
  ]
  node [
    id 19
    label "opisywa&#263;"
  ]
  node [
    id 20
    label "ukazywa&#263;"
  ]
  node [
    id 21
    label "represent"
  ]
  node [
    id 22
    label "zg&#322;asza&#263;"
  ]
  node [
    id 23
    label "typify"
  ]
  node [
    id 24
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 25
    label "attest"
  ]
  node [
    id 26
    label "stanowi&#263;"
  ]
  node [
    id 27
    label "m&#243;wi&#263;"
  ]
  node [
    id 28
    label "bech"
  ]
  node [
    id 29
    label "dziecinny"
  ]
  node [
    id 30
    label "naiwniak"
  ]
  node [
    id 31
    label "dziecko"
  ]
  node [
    id 32
    label "utulenie"
  ]
  node [
    id 33
    label "pediatra"
  ]
  node [
    id 34
    label "utulanie"
  ]
  node [
    id 35
    label "dzieciarnia"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "niepe&#322;noletni"
  ]
  node [
    id 38
    label "organizm"
  ]
  node [
    id 39
    label "utula&#263;"
  ]
  node [
    id 40
    label "cz&#322;owieczek"
  ]
  node [
    id 41
    label "fledgling"
  ]
  node [
    id 42
    label "zwierz&#281;"
  ]
  node [
    id 43
    label "utuli&#263;"
  ]
  node [
    id 44
    label "m&#322;odzik"
  ]
  node [
    id 45
    label "pedofil"
  ]
  node [
    id 46
    label "m&#322;odziak"
  ]
  node [
    id 47
    label "potomek"
  ]
  node [
    id 48
    label "entliczek-pentliczek"
  ]
  node [
    id 49
    label "potomstwo"
  ]
  node [
    id 50
    label "sraluch"
  ]
  node [
    id 51
    label "wa&#322;"
  ]
  node [
    id 52
    label "g&#322;upiec"
  ]
  node [
    id 53
    label "oferma"
  ]
  node [
    id 54
    label "dziecinnie"
  ]
  node [
    id 55
    label "pocz&#261;tkowy"
  ]
  node [
    id 56
    label "typowy"
  ]
  node [
    id 57
    label "dzieci&#281;co"
  ]
  node [
    id 58
    label "dziecinnienie"
  ]
  node [
    id 59
    label "dzieci&#324;ski"
  ]
  node [
    id 60
    label "zdziecinnienie"
  ]
  node [
    id 61
    label "niedojrza&#322;y"
  ]
  node [
    id 62
    label "salariat"
  ]
  node [
    id 63
    label "szko&#322;a"
  ]
  node [
    id 64
    label "kategoria_urz&#281;dnicza"
  ]
  node [
    id 65
    label "wynagrodzenie"
  ]
  node [
    id 66
    label "salarium"
  ]
  node [
    id 67
    label "do&#347;wiadczenie"
  ]
  node [
    id 68
    label "teren_szko&#322;y"
  ]
  node [
    id 69
    label "wiedza"
  ]
  node [
    id 70
    label "Mickiewicz"
  ]
  node [
    id 71
    label "kwalifikacje"
  ]
  node [
    id 72
    label "podr&#281;cznik"
  ]
  node [
    id 73
    label "absolwent"
  ]
  node [
    id 74
    label "praktyka"
  ]
  node [
    id 75
    label "school"
  ]
  node [
    id 76
    label "system"
  ]
  node [
    id 77
    label "zda&#263;"
  ]
  node [
    id 78
    label "gabinet"
  ]
  node [
    id 79
    label "urszulanki"
  ]
  node [
    id 80
    label "sztuba"
  ]
  node [
    id 81
    label "&#322;awa_szkolna"
  ]
  node [
    id 82
    label "nauka"
  ]
  node [
    id 83
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 84
    label "przepisa&#263;"
  ]
  node [
    id 85
    label "muzyka"
  ]
  node [
    id 86
    label "grupa"
  ]
  node [
    id 87
    label "form"
  ]
  node [
    id 88
    label "klasa"
  ]
  node [
    id 89
    label "lekcja"
  ]
  node [
    id 90
    label "metoda"
  ]
  node [
    id 91
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 92
    label "przepisanie"
  ]
  node [
    id 93
    label "czas"
  ]
  node [
    id 94
    label "skolaryzacja"
  ]
  node [
    id 95
    label "zdanie"
  ]
  node [
    id 96
    label "stopek"
  ]
  node [
    id 97
    label "sekretariat"
  ]
  node [
    id 98
    label "ideologia"
  ]
  node [
    id 99
    label "lesson"
  ]
  node [
    id 100
    label "instytucja"
  ]
  node [
    id 101
    label "niepokalanki"
  ]
  node [
    id 102
    label "siedziba"
  ]
  node [
    id 103
    label "szkolenie"
  ]
  node [
    id 104
    label "kara"
  ]
  node [
    id 105
    label "tablica"
  ]
  node [
    id 106
    label "danie"
  ]
  node [
    id 107
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 108
    label "return"
  ]
  node [
    id 109
    label "refund"
  ]
  node [
    id 110
    label "liczenie"
  ]
  node [
    id 111
    label "liczy&#263;"
  ]
  node [
    id 112
    label "doch&#243;d"
  ]
  node [
    id 113
    label "wynagrodzenie_brutto"
  ]
  node [
    id 114
    label "koszt_rodzajowy"
  ]
  node [
    id 115
    label "policzy&#263;"
  ]
  node [
    id 116
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 117
    label "ordynaria"
  ]
  node [
    id 118
    label "bud&#380;et_domowy"
  ]
  node [
    id 119
    label "policzenie"
  ]
  node [
    id 120
    label "pay"
  ]
  node [
    id 121
    label "zap&#322;ata"
  ]
  node [
    id 122
    label "p&#322;aca"
  ]
  node [
    id 123
    label "warstwa"
  ]
  node [
    id 124
    label "op&#322;ata"
  ]
  node [
    id 125
    label "danina"
  ]
  node [
    id 126
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 127
    label "trybut"
  ]
  node [
    id 128
    label "bilans_handlowy"
  ]
  node [
    id 129
    label "kwota"
  ]
  node [
    id 130
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 131
    label "&#347;wiadczenie"
  ]
  node [
    id 132
    label "weso&#322;y"
  ]
  node [
    id 133
    label "Miko&#322;ajek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 132
    target 133
  ]
]
