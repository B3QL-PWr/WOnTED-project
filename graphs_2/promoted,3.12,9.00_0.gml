graph [
  node [
    id 0
    label "informacja"
    origin "text"
  ]
  node [
    id 1
    label "ile"
    origin "text"
  ]
  node [
    id 2
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "&#347;ci&#347;le"
    origin "text"
  ]
  node [
    id 7
    label "strzec"
    origin "text"
  ]
  node [
    id 8
    label "punkt"
  ]
  node [
    id 9
    label "publikacja"
  ]
  node [
    id 10
    label "wiedza"
  ]
  node [
    id 11
    label "obiega&#263;"
  ]
  node [
    id 12
    label "powzi&#281;cie"
  ]
  node [
    id 13
    label "dane"
  ]
  node [
    id 14
    label "obiegni&#281;cie"
  ]
  node [
    id 15
    label "sygna&#322;"
  ]
  node [
    id 16
    label "obieganie"
  ]
  node [
    id 17
    label "powzi&#261;&#263;"
  ]
  node [
    id 18
    label "obiec"
  ]
  node [
    id 19
    label "doj&#347;cie"
  ]
  node [
    id 20
    label "doj&#347;&#263;"
  ]
  node [
    id 21
    label "po&#322;o&#380;enie"
  ]
  node [
    id 22
    label "sprawa"
  ]
  node [
    id 23
    label "ust&#281;p"
  ]
  node [
    id 24
    label "plan"
  ]
  node [
    id 25
    label "obiekt_matematyczny"
  ]
  node [
    id 26
    label "problemat"
  ]
  node [
    id 27
    label "plamka"
  ]
  node [
    id 28
    label "stopie&#324;_pisma"
  ]
  node [
    id 29
    label "jednostka"
  ]
  node [
    id 30
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 31
    label "miejsce"
  ]
  node [
    id 32
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 33
    label "mark"
  ]
  node [
    id 34
    label "chwila"
  ]
  node [
    id 35
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 36
    label "prosta"
  ]
  node [
    id 37
    label "problematyka"
  ]
  node [
    id 38
    label "obiekt"
  ]
  node [
    id 39
    label "zapunktowa&#263;"
  ]
  node [
    id 40
    label "podpunkt"
  ]
  node [
    id 41
    label "wojsko"
  ]
  node [
    id 42
    label "kres"
  ]
  node [
    id 43
    label "przestrze&#324;"
  ]
  node [
    id 44
    label "point"
  ]
  node [
    id 45
    label "pozycja"
  ]
  node [
    id 46
    label "cognition"
  ]
  node [
    id 47
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 48
    label "intelekt"
  ]
  node [
    id 49
    label "pozwolenie"
  ]
  node [
    id 50
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 51
    label "zaawansowanie"
  ]
  node [
    id 52
    label "wykszta&#322;cenie"
  ]
  node [
    id 53
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 54
    label "przekazywa&#263;"
  ]
  node [
    id 55
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 56
    label "pulsation"
  ]
  node [
    id 57
    label "przekazywanie"
  ]
  node [
    id 58
    label "przewodzenie"
  ]
  node [
    id 59
    label "d&#378;wi&#281;k"
  ]
  node [
    id 60
    label "po&#322;&#261;czenie"
  ]
  node [
    id 61
    label "fala"
  ]
  node [
    id 62
    label "przekazanie"
  ]
  node [
    id 63
    label "przewodzi&#263;"
  ]
  node [
    id 64
    label "znak"
  ]
  node [
    id 65
    label "zapowied&#378;"
  ]
  node [
    id 66
    label "medium_transmisyjne"
  ]
  node [
    id 67
    label "demodulacja"
  ]
  node [
    id 68
    label "przekaza&#263;"
  ]
  node [
    id 69
    label "czynnik"
  ]
  node [
    id 70
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 71
    label "aliasing"
  ]
  node [
    id 72
    label "wizja"
  ]
  node [
    id 73
    label "modulacja"
  ]
  node [
    id 74
    label "drift"
  ]
  node [
    id 75
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 76
    label "tekst"
  ]
  node [
    id 77
    label "druk"
  ]
  node [
    id 78
    label "produkcja"
  ]
  node [
    id 79
    label "notification"
  ]
  node [
    id 80
    label "edytowa&#263;"
  ]
  node [
    id 81
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 82
    label "spakowanie"
  ]
  node [
    id 83
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 84
    label "pakowa&#263;"
  ]
  node [
    id 85
    label "rekord"
  ]
  node [
    id 86
    label "korelator"
  ]
  node [
    id 87
    label "wyci&#261;ganie"
  ]
  node [
    id 88
    label "pakowanie"
  ]
  node [
    id 89
    label "sekwencjonowa&#263;"
  ]
  node [
    id 90
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 91
    label "jednostka_informacji"
  ]
  node [
    id 92
    label "zbi&#243;r"
  ]
  node [
    id 93
    label "evidence"
  ]
  node [
    id 94
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 95
    label "rozpakowywanie"
  ]
  node [
    id 96
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 97
    label "rozpakowanie"
  ]
  node [
    id 98
    label "rozpakowywa&#263;"
  ]
  node [
    id 99
    label "konwersja"
  ]
  node [
    id 100
    label "nap&#322;ywanie"
  ]
  node [
    id 101
    label "rozpakowa&#263;"
  ]
  node [
    id 102
    label "spakowa&#263;"
  ]
  node [
    id 103
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 104
    label "edytowanie"
  ]
  node [
    id 105
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 106
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 107
    label "sekwencjonowanie"
  ]
  node [
    id 108
    label "dochodzenie"
  ]
  node [
    id 109
    label "uzyskanie"
  ]
  node [
    id 110
    label "skill"
  ]
  node [
    id 111
    label "dochrapanie_si&#281;"
  ]
  node [
    id 112
    label "znajomo&#347;ci"
  ]
  node [
    id 113
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 114
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 115
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 116
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 117
    label "powi&#261;zanie"
  ]
  node [
    id 118
    label "entrance"
  ]
  node [
    id 119
    label "affiliation"
  ]
  node [
    id 120
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 121
    label "dor&#281;czenie"
  ]
  node [
    id 122
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 123
    label "spowodowanie"
  ]
  node [
    id 124
    label "bodziec"
  ]
  node [
    id 125
    label "dost&#281;p"
  ]
  node [
    id 126
    label "przesy&#322;ka"
  ]
  node [
    id 127
    label "gotowy"
  ]
  node [
    id 128
    label "avenue"
  ]
  node [
    id 129
    label "postrzeganie"
  ]
  node [
    id 130
    label "dodatek"
  ]
  node [
    id 131
    label "doznanie"
  ]
  node [
    id 132
    label "dojrza&#322;y"
  ]
  node [
    id 133
    label "dojechanie"
  ]
  node [
    id 134
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 135
    label "ingress"
  ]
  node [
    id 136
    label "czynno&#347;&#263;"
  ]
  node [
    id 137
    label "strzelenie"
  ]
  node [
    id 138
    label "orzekni&#281;cie"
  ]
  node [
    id 139
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 140
    label "orgazm"
  ]
  node [
    id 141
    label "dolecenie"
  ]
  node [
    id 142
    label "rozpowszechnienie"
  ]
  node [
    id 143
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 144
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 145
    label "stanie_si&#281;"
  ]
  node [
    id 146
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 147
    label "dop&#322;ata"
  ]
  node [
    id 148
    label "zrobienie"
  ]
  node [
    id 149
    label "odwiedzi&#263;"
  ]
  node [
    id 150
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 151
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 152
    label "orb"
  ]
  node [
    id 153
    label "sta&#263;_si&#281;"
  ]
  node [
    id 154
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 155
    label "supervene"
  ]
  node [
    id 156
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 157
    label "zaj&#347;&#263;"
  ]
  node [
    id 158
    label "catch"
  ]
  node [
    id 159
    label "get"
  ]
  node [
    id 160
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 161
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 162
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 163
    label "heed"
  ]
  node [
    id 164
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 165
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 166
    label "spowodowa&#263;"
  ]
  node [
    id 167
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 168
    label "dozna&#263;"
  ]
  node [
    id 169
    label "dokoptowa&#263;"
  ]
  node [
    id 170
    label "postrzega&#263;"
  ]
  node [
    id 171
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 172
    label "dolecie&#263;"
  ]
  node [
    id 173
    label "drive"
  ]
  node [
    id 174
    label "dotrze&#263;"
  ]
  node [
    id 175
    label "uzyska&#263;"
  ]
  node [
    id 176
    label "become"
  ]
  node [
    id 177
    label "podj&#261;&#263;"
  ]
  node [
    id 178
    label "zacz&#261;&#263;"
  ]
  node [
    id 179
    label "otrzyma&#263;"
  ]
  node [
    id 180
    label "flow"
  ]
  node [
    id 181
    label "odwiedza&#263;"
  ]
  node [
    id 182
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 183
    label "rotate"
  ]
  node [
    id 184
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 185
    label "authorize"
  ]
  node [
    id 186
    label "odwiedzanie"
  ]
  node [
    id 187
    label "biegni&#281;cie"
  ]
  node [
    id 188
    label "zakre&#347;lanie"
  ]
  node [
    id 189
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 190
    label "okr&#261;&#380;anie"
  ]
  node [
    id 191
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 192
    label "zakre&#347;lenie"
  ]
  node [
    id 193
    label "odwiedzenie"
  ]
  node [
    id 194
    label "okr&#261;&#380;enie"
  ]
  node [
    id 195
    label "podj&#281;cie"
  ]
  node [
    id 196
    label "otrzymanie"
  ]
  node [
    id 197
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 198
    label "sell"
  ]
  node [
    id 199
    label "zach&#281;ci&#263;"
  ]
  node [
    id 200
    label "op&#281;dzi&#263;"
  ]
  node [
    id 201
    label "odda&#263;"
  ]
  node [
    id 202
    label "give_birth"
  ]
  node [
    id 203
    label "zdradzi&#263;"
  ]
  node [
    id 204
    label "zhandlowa&#263;"
  ]
  node [
    id 205
    label "umie&#347;ci&#263;"
  ]
  node [
    id 206
    label "sacrifice"
  ]
  node [
    id 207
    label "da&#263;"
  ]
  node [
    id 208
    label "transfer"
  ]
  node [
    id 209
    label "give"
  ]
  node [
    id 210
    label "picture"
  ]
  node [
    id 211
    label "przedstawi&#263;"
  ]
  node [
    id 212
    label "zrobi&#263;"
  ]
  node [
    id 213
    label "reflect"
  ]
  node [
    id 214
    label "odst&#261;pi&#263;"
  ]
  node [
    id 215
    label "deliver"
  ]
  node [
    id 216
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 217
    label "restore"
  ]
  node [
    id 218
    label "odpowiedzie&#263;"
  ]
  node [
    id 219
    label "convey"
  ]
  node [
    id 220
    label "dostarczy&#263;"
  ]
  node [
    id 221
    label "z_powrotem"
  ]
  node [
    id 222
    label "rogi"
  ]
  node [
    id 223
    label "objawi&#263;"
  ]
  node [
    id 224
    label "poinformowa&#263;"
  ]
  node [
    id 225
    label "naruszy&#263;"
  ]
  node [
    id 226
    label "nabra&#263;"
  ]
  node [
    id 227
    label "denounce"
  ]
  node [
    id 228
    label "invite"
  ]
  node [
    id 229
    label "pozyska&#263;"
  ]
  node [
    id 230
    label "wymieni&#263;"
  ]
  node [
    id 231
    label "skorzysta&#263;"
  ]
  node [
    id 232
    label "poradzi&#263;_sobie"
  ]
  node [
    id 233
    label "spo&#380;y&#263;"
  ]
  node [
    id 234
    label "egzemplarz"
  ]
  node [
    id 235
    label "rozdzia&#322;"
  ]
  node [
    id 236
    label "wk&#322;ad"
  ]
  node [
    id 237
    label "tytu&#322;"
  ]
  node [
    id 238
    label "zak&#322;adka"
  ]
  node [
    id 239
    label "nomina&#322;"
  ]
  node [
    id 240
    label "ok&#322;adka"
  ]
  node [
    id 241
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 242
    label "wydawnictwo"
  ]
  node [
    id 243
    label "ekslibris"
  ]
  node [
    id 244
    label "przek&#322;adacz"
  ]
  node [
    id 245
    label "bibliofilstwo"
  ]
  node [
    id 246
    label "falc"
  ]
  node [
    id 247
    label "pagina"
  ]
  node [
    id 248
    label "zw&#243;j"
  ]
  node [
    id 249
    label "ekscerpcja"
  ]
  node [
    id 250
    label "j&#281;zykowo"
  ]
  node [
    id 251
    label "wypowied&#378;"
  ]
  node [
    id 252
    label "redakcja"
  ]
  node [
    id 253
    label "wytw&#243;r"
  ]
  node [
    id 254
    label "pomini&#281;cie"
  ]
  node [
    id 255
    label "dzie&#322;o"
  ]
  node [
    id 256
    label "preparacja"
  ]
  node [
    id 257
    label "odmianka"
  ]
  node [
    id 258
    label "opu&#347;ci&#263;"
  ]
  node [
    id 259
    label "koniektura"
  ]
  node [
    id 260
    label "pisa&#263;"
  ]
  node [
    id 261
    label "obelga"
  ]
  node [
    id 262
    label "czynnik_biotyczny"
  ]
  node [
    id 263
    label "wyewoluowanie"
  ]
  node [
    id 264
    label "reakcja"
  ]
  node [
    id 265
    label "individual"
  ]
  node [
    id 266
    label "przyswoi&#263;"
  ]
  node [
    id 267
    label "starzenie_si&#281;"
  ]
  node [
    id 268
    label "wyewoluowa&#263;"
  ]
  node [
    id 269
    label "okaz"
  ]
  node [
    id 270
    label "part"
  ]
  node [
    id 271
    label "przyswojenie"
  ]
  node [
    id 272
    label "ewoluowanie"
  ]
  node [
    id 273
    label "ewoluowa&#263;"
  ]
  node [
    id 274
    label "sztuka"
  ]
  node [
    id 275
    label "agent"
  ]
  node [
    id 276
    label "przyswaja&#263;"
  ]
  node [
    id 277
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 278
    label "nicpo&#324;"
  ]
  node [
    id 279
    label "przyswajanie"
  ]
  node [
    id 280
    label "debit"
  ]
  node [
    id 281
    label "redaktor"
  ]
  node [
    id 282
    label "szata_graficzna"
  ]
  node [
    id 283
    label "firma"
  ]
  node [
    id 284
    label "wydawa&#263;"
  ]
  node [
    id 285
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 286
    label "wyda&#263;"
  ]
  node [
    id 287
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 288
    label "poster"
  ]
  node [
    id 289
    label "nadtytu&#322;"
  ]
  node [
    id 290
    label "tytulatura"
  ]
  node [
    id 291
    label "elevation"
  ]
  node [
    id 292
    label "mianowaniec"
  ]
  node [
    id 293
    label "nazwa"
  ]
  node [
    id 294
    label "podtytu&#322;"
  ]
  node [
    id 295
    label "wydarzenie"
  ]
  node [
    id 296
    label "faza"
  ]
  node [
    id 297
    label "interruption"
  ]
  node [
    id 298
    label "podzia&#322;"
  ]
  node [
    id 299
    label "podrozdzia&#322;"
  ]
  node [
    id 300
    label "fragment"
  ]
  node [
    id 301
    label "pagination"
  ]
  node [
    id 302
    label "strona"
  ]
  node [
    id 303
    label "numer"
  ]
  node [
    id 304
    label "kartka"
  ]
  node [
    id 305
    label "kwota"
  ]
  node [
    id 306
    label "uczestnictwo"
  ]
  node [
    id 307
    label "element"
  ]
  node [
    id 308
    label "input"
  ]
  node [
    id 309
    label "czasopismo"
  ]
  node [
    id 310
    label "lokata"
  ]
  node [
    id 311
    label "zeszyt"
  ]
  node [
    id 312
    label "blok"
  ]
  node [
    id 313
    label "oprawa"
  ]
  node [
    id 314
    label "boarding"
  ]
  node [
    id 315
    label "oprawianie"
  ]
  node [
    id 316
    label "os&#322;ona"
  ]
  node [
    id 317
    label "oprawia&#263;"
  ]
  node [
    id 318
    label "blacha"
  ]
  node [
    id 319
    label "z&#322;&#261;czenie"
  ]
  node [
    id 320
    label "grzbiet"
  ]
  node [
    id 321
    label "kszta&#322;t"
  ]
  node [
    id 322
    label "wrench"
  ]
  node [
    id 323
    label "m&#243;zg"
  ]
  node [
    id 324
    label "kink"
  ]
  node [
    id 325
    label "plik"
  ]
  node [
    id 326
    label "manuskrypt"
  ]
  node [
    id 327
    label "rolka"
  ]
  node [
    id 328
    label "warto&#347;&#263;"
  ]
  node [
    id 329
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 330
    label "pieni&#261;dz"
  ]
  node [
    id 331
    label "par_value"
  ]
  node [
    id 332
    label "cena"
  ]
  node [
    id 333
    label "znaczek"
  ]
  node [
    id 334
    label "kolekcjonerstwo"
  ]
  node [
    id 335
    label "bibliomania"
  ]
  node [
    id 336
    label "t&#322;umacz"
  ]
  node [
    id 337
    label "urz&#261;dzenie"
  ]
  node [
    id 338
    label "cz&#322;owiek"
  ]
  node [
    id 339
    label "bookmark"
  ]
  node [
    id 340
    label "fa&#322;da"
  ]
  node [
    id 341
    label "znacznik"
  ]
  node [
    id 342
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 343
    label "widok"
  ]
  node [
    id 344
    label "program"
  ]
  node [
    id 345
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 346
    label "oznaczenie"
  ]
  node [
    id 347
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 348
    label "mie&#263;_miejsce"
  ]
  node [
    id 349
    label "equal"
  ]
  node [
    id 350
    label "trwa&#263;"
  ]
  node [
    id 351
    label "chodzi&#263;"
  ]
  node [
    id 352
    label "si&#281;ga&#263;"
  ]
  node [
    id 353
    label "stan"
  ]
  node [
    id 354
    label "obecno&#347;&#263;"
  ]
  node [
    id 355
    label "stand"
  ]
  node [
    id 356
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 357
    label "uczestniczy&#263;"
  ]
  node [
    id 358
    label "participate"
  ]
  node [
    id 359
    label "robi&#263;"
  ]
  node [
    id 360
    label "istnie&#263;"
  ]
  node [
    id 361
    label "pozostawa&#263;"
  ]
  node [
    id 362
    label "zostawa&#263;"
  ]
  node [
    id 363
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 364
    label "adhere"
  ]
  node [
    id 365
    label "compass"
  ]
  node [
    id 366
    label "korzysta&#263;"
  ]
  node [
    id 367
    label "appreciation"
  ]
  node [
    id 368
    label "osi&#261;ga&#263;"
  ]
  node [
    id 369
    label "dociera&#263;"
  ]
  node [
    id 370
    label "mierzy&#263;"
  ]
  node [
    id 371
    label "u&#380;ywa&#263;"
  ]
  node [
    id 372
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 373
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 374
    label "exsert"
  ]
  node [
    id 375
    label "being"
  ]
  node [
    id 376
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 377
    label "cecha"
  ]
  node [
    id 378
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 379
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 380
    label "p&#322;ywa&#263;"
  ]
  node [
    id 381
    label "run"
  ]
  node [
    id 382
    label "bangla&#263;"
  ]
  node [
    id 383
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 384
    label "przebiega&#263;"
  ]
  node [
    id 385
    label "wk&#322;ada&#263;"
  ]
  node [
    id 386
    label "proceed"
  ]
  node [
    id 387
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 388
    label "carry"
  ]
  node [
    id 389
    label "bywa&#263;"
  ]
  node [
    id 390
    label "dziama&#263;"
  ]
  node [
    id 391
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 392
    label "stara&#263;_si&#281;"
  ]
  node [
    id 393
    label "para"
  ]
  node [
    id 394
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 395
    label "str&#243;j"
  ]
  node [
    id 396
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 397
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 398
    label "krok"
  ]
  node [
    id 399
    label "tryb"
  ]
  node [
    id 400
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 401
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 402
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 403
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 404
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 405
    label "continue"
  ]
  node [
    id 406
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 407
    label "Ohio"
  ]
  node [
    id 408
    label "wci&#281;cie"
  ]
  node [
    id 409
    label "Nowy_York"
  ]
  node [
    id 410
    label "warstwa"
  ]
  node [
    id 411
    label "samopoczucie"
  ]
  node [
    id 412
    label "Illinois"
  ]
  node [
    id 413
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 414
    label "state"
  ]
  node [
    id 415
    label "Jukatan"
  ]
  node [
    id 416
    label "Kalifornia"
  ]
  node [
    id 417
    label "Wirginia"
  ]
  node [
    id 418
    label "wektor"
  ]
  node [
    id 419
    label "Teksas"
  ]
  node [
    id 420
    label "Goa"
  ]
  node [
    id 421
    label "Waszyngton"
  ]
  node [
    id 422
    label "Massachusetts"
  ]
  node [
    id 423
    label "Alaska"
  ]
  node [
    id 424
    label "Arakan"
  ]
  node [
    id 425
    label "Hawaje"
  ]
  node [
    id 426
    label "Maryland"
  ]
  node [
    id 427
    label "Michigan"
  ]
  node [
    id 428
    label "Arizona"
  ]
  node [
    id 429
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 430
    label "Georgia"
  ]
  node [
    id 431
    label "poziom"
  ]
  node [
    id 432
    label "Pensylwania"
  ]
  node [
    id 433
    label "shape"
  ]
  node [
    id 434
    label "Luizjana"
  ]
  node [
    id 435
    label "Nowy_Meksyk"
  ]
  node [
    id 436
    label "Alabama"
  ]
  node [
    id 437
    label "ilo&#347;&#263;"
  ]
  node [
    id 438
    label "Kansas"
  ]
  node [
    id 439
    label "Oregon"
  ]
  node [
    id 440
    label "Floryda"
  ]
  node [
    id 441
    label "Oklahoma"
  ]
  node [
    id 442
    label "jednostka_administracyjna"
  ]
  node [
    id 443
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 444
    label "blisko"
  ]
  node [
    id 445
    label "rygorystyczny"
  ]
  node [
    id 446
    label "zwarto"
  ]
  node [
    id 447
    label "dok&#322;adnie"
  ]
  node [
    id 448
    label "logicznie"
  ]
  node [
    id 449
    label "surowo"
  ]
  node [
    id 450
    label "przylegle"
  ]
  node [
    id 451
    label "g&#281;sto"
  ]
  node [
    id 452
    label "&#347;cis&#322;y"
  ]
  node [
    id 453
    label "zwarty"
  ]
  node [
    id 454
    label "powa&#380;nie"
  ]
  node [
    id 455
    label "surowy"
  ]
  node [
    id 456
    label "srodze"
  ]
  node [
    id 457
    label "sternly"
  ]
  node [
    id 458
    label "gro&#378;ny"
  ]
  node [
    id 459
    label "oszcz&#281;dnie"
  ]
  node [
    id 460
    label "twardo"
  ]
  node [
    id 461
    label "surowie"
  ]
  node [
    id 462
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 463
    label "bliski"
  ]
  node [
    id 464
    label "silnie"
  ]
  node [
    id 465
    label "logiczny"
  ]
  node [
    id 466
    label "naukowo"
  ]
  node [
    id 467
    label "rozumowo"
  ]
  node [
    id 468
    label "sensownie"
  ]
  node [
    id 469
    label "logically"
  ]
  node [
    id 470
    label "punctiliously"
  ]
  node [
    id 471
    label "meticulously"
  ]
  node [
    id 472
    label "precyzyjnie"
  ]
  node [
    id 473
    label "dok&#322;adny"
  ]
  node [
    id 474
    label "rzetelnie"
  ]
  node [
    id 475
    label "densely"
  ]
  node [
    id 476
    label "g&#281;sty"
  ]
  node [
    id 477
    label "gor&#261;czkowo"
  ]
  node [
    id 478
    label "ci&#281;&#380;ki"
  ]
  node [
    id 479
    label "obficie"
  ]
  node [
    id 480
    label "ci&#281;&#380;ko"
  ]
  node [
    id 481
    label "intensywnie"
  ]
  node [
    id 482
    label "bezpo&#347;rednio"
  ]
  node [
    id 483
    label "przyleg&#322;y"
  ]
  node [
    id 484
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 485
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 486
    label "rygorystycznie"
  ]
  node [
    id 487
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 488
    label "w&#261;ski"
  ]
  node [
    id 489
    label "konkretny"
  ]
  node [
    id 490
    label "zwarcie"
  ]
  node [
    id 491
    label "rzetelny"
  ]
  node [
    id 492
    label "szybki"
  ]
  node [
    id 493
    label "solidarny"
  ]
  node [
    id 494
    label "sprawny"
  ]
  node [
    id 495
    label "sp&#243;jny"
  ]
  node [
    id 496
    label "czuwa&#263;"
  ]
  node [
    id 497
    label "chroni&#263;"
  ]
  node [
    id 498
    label "rebuff"
  ]
  node [
    id 499
    label "broni&#263;"
  ]
  node [
    id 500
    label "sprawowa&#263;"
  ]
  node [
    id 501
    label "zachowywa&#263;"
  ]
  node [
    id 502
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 503
    label "tajemnica"
  ]
  node [
    id 504
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 505
    label "zdyscyplinowanie"
  ]
  node [
    id 506
    label "podtrzymywa&#263;"
  ]
  node [
    id 507
    label "post"
  ]
  node [
    id 508
    label "control"
  ]
  node [
    id 509
    label "przechowywa&#263;"
  ]
  node [
    id 510
    label "behave"
  ]
  node [
    id 511
    label "dieta"
  ]
  node [
    id 512
    label "hold"
  ]
  node [
    id 513
    label "post&#281;powa&#263;"
  ]
  node [
    id 514
    label "fend"
  ]
  node [
    id 515
    label "s&#261;d"
  ]
  node [
    id 516
    label "reprezentowa&#263;"
  ]
  node [
    id 517
    label "zdawa&#263;"
  ]
  node [
    id 518
    label "preach"
  ]
  node [
    id 519
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 520
    label "walczy&#263;"
  ]
  node [
    id 521
    label "resist"
  ]
  node [
    id 522
    label "adwokatowa&#263;"
  ]
  node [
    id 523
    label "udowadnia&#263;"
  ]
  node [
    id 524
    label "gra&#263;"
  ]
  node [
    id 525
    label "refuse"
  ]
  node [
    id 526
    label "kultywowa&#263;"
  ]
  node [
    id 527
    label "report"
  ]
  node [
    id 528
    label "prosecute"
  ]
  node [
    id 529
    label "organizowa&#263;"
  ]
  node [
    id 530
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 531
    label "czyni&#263;"
  ]
  node [
    id 532
    label "stylizowa&#263;"
  ]
  node [
    id 533
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 534
    label "falowa&#263;"
  ]
  node [
    id 535
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 536
    label "peddle"
  ]
  node [
    id 537
    label "praca"
  ]
  node [
    id 538
    label "wydala&#263;"
  ]
  node [
    id 539
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 540
    label "tentegowa&#263;"
  ]
  node [
    id 541
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 542
    label "urz&#261;dza&#263;"
  ]
  node [
    id 543
    label "oszukiwa&#263;"
  ]
  node [
    id 544
    label "work"
  ]
  node [
    id 545
    label "ukazywa&#263;"
  ]
  node [
    id 546
    label "przerabia&#263;"
  ]
  node [
    id 547
    label "act"
  ]
  node [
    id 548
    label "troszczy&#263;_si&#281;"
  ]
  node [
    id 549
    label "odpowiada&#263;"
  ]
  node [
    id 550
    label "funkcjonowa&#263;"
  ]
  node [
    id 551
    label "guard"
  ]
  node [
    id 552
    label "czeka&#263;"
  ]
  node [
    id 553
    label "cover"
  ]
  node [
    id 554
    label "pilnowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
]
