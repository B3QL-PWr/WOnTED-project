graph [
  node [
    id 0
    label "malediwy"
    origin "text"
  ]
  node [
    id 1
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 3
    label "lekkoatletyka"
    origin "text"
  ]
  node [
    id 4
    label "championship"
  ]
  node [
    id 5
    label "zawody"
  ]
  node [
    id 6
    label "Formu&#322;a_1"
  ]
  node [
    id 7
    label "tysi&#281;cznik"
  ]
  node [
    id 8
    label "impreza"
  ]
  node [
    id 9
    label "walczenie"
  ]
  node [
    id 10
    label "contest"
  ]
  node [
    id 11
    label "rywalizacja"
  ]
  node [
    id 12
    label "spadochroniarstwo"
  ]
  node [
    id 13
    label "kategoria_open"
  ]
  node [
    id 14
    label "champion"
  ]
  node [
    id 15
    label "walczy&#263;"
  ]
  node [
    id 16
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "przyroda"
  ]
  node [
    id 18
    label "Wsch&#243;d"
  ]
  node [
    id 19
    label "obszar"
  ]
  node [
    id 20
    label "morze"
  ]
  node [
    id 21
    label "kuchnia"
  ]
  node [
    id 22
    label "biosfera"
  ]
  node [
    id 23
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 24
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 25
    label "geotermia"
  ]
  node [
    id 26
    label "atmosfera"
  ]
  node [
    id 27
    label "ekosystem"
  ]
  node [
    id 28
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 29
    label "p&#243;&#322;kula"
  ]
  node [
    id 30
    label "class"
  ]
  node [
    id 31
    label "huczek"
  ]
  node [
    id 32
    label "planeta"
  ]
  node [
    id 33
    label "makrokosmos"
  ]
  node [
    id 34
    label "przestrze&#324;"
  ]
  node [
    id 35
    label "przej&#281;cie"
  ]
  node [
    id 36
    label "przej&#261;&#263;"
  ]
  node [
    id 37
    label "universe"
  ]
  node [
    id 38
    label "przedmiot"
  ]
  node [
    id 39
    label "biegun"
  ]
  node [
    id 40
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 41
    label "wszechstworzenie"
  ]
  node [
    id 42
    label "populace"
  ]
  node [
    id 43
    label "po&#322;udnie"
  ]
  node [
    id 44
    label "magnetosfera"
  ]
  node [
    id 45
    label "przejmowa&#263;"
  ]
  node [
    id 46
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 47
    label "zagranica"
  ]
  node [
    id 48
    label "fauna"
  ]
  node [
    id 49
    label "p&#243;&#322;noc"
  ]
  node [
    id 50
    label "stw&#243;r"
  ]
  node [
    id 51
    label "ekosfera"
  ]
  node [
    id 52
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 53
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 54
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 55
    label "litosfera"
  ]
  node [
    id 56
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 57
    label "zjawisko"
  ]
  node [
    id 58
    label "ciemna_materia"
  ]
  node [
    id 59
    label "teren"
  ]
  node [
    id 60
    label "asymilowanie_si&#281;"
  ]
  node [
    id 61
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 62
    label "Nowy_&#346;wiat"
  ]
  node [
    id 63
    label "barysfera"
  ]
  node [
    id 64
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 65
    label "grupa"
  ]
  node [
    id 66
    label "Ziemia"
  ]
  node [
    id 67
    label "hydrosfera"
  ]
  node [
    id 68
    label "rzecz"
  ]
  node [
    id 69
    label "Stary_&#346;wiat"
  ]
  node [
    id 70
    label "przejmowanie"
  ]
  node [
    id 71
    label "geosfera"
  ]
  node [
    id 72
    label "mikrokosmos"
  ]
  node [
    id 73
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 74
    label "woda"
  ]
  node [
    id 75
    label "biota"
  ]
  node [
    id 76
    label "rze&#378;ba"
  ]
  node [
    id 77
    label "environment"
  ]
  node [
    id 78
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 79
    label "obiekt_naturalny"
  ]
  node [
    id 80
    label "czarna_dziura"
  ]
  node [
    id 81
    label "ozonosfera"
  ]
  node [
    id 82
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 83
    label "geoida"
  ]
  node [
    id 84
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "asymilowa&#263;"
  ]
  node [
    id 86
    label "kompozycja"
  ]
  node [
    id 87
    label "pakiet_klimatyczny"
  ]
  node [
    id 88
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 89
    label "type"
  ]
  node [
    id 90
    label "cz&#261;steczka"
  ]
  node [
    id 91
    label "gromada"
  ]
  node [
    id 92
    label "specgrupa"
  ]
  node [
    id 93
    label "egzemplarz"
  ]
  node [
    id 94
    label "stage_set"
  ]
  node [
    id 95
    label "asymilowanie"
  ]
  node [
    id 96
    label "zbi&#243;r"
  ]
  node [
    id 97
    label "odm&#322;odzenie"
  ]
  node [
    id 98
    label "odm&#322;adza&#263;"
  ]
  node [
    id 99
    label "harcerze_starsi"
  ]
  node [
    id 100
    label "jednostka_systematyczna"
  ]
  node [
    id 101
    label "oddzia&#322;"
  ]
  node [
    id 102
    label "category"
  ]
  node [
    id 103
    label "liga"
  ]
  node [
    id 104
    label "&#346;wietliki"
  ]
  node [
    id 105
    label "formacja_geologiczna"
  ]
  node [
    id 106
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 107
    label "Eurogrupa"
  ]
  node [
    id 108
    label "Terranie"
  ]
  node [
    id 109
    label "odm&#322;adzanie"
  ]
  node [
    id 110
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 111
    label "Entuzjastki"
  ]
  node [
    id 112
    label "Kosowo"
  ]
  node [
    id 113
    label "zach&#243;d"
  ]
  node [
    id 114
    label "Zabu&#380;e"
  ]
  node [
    id 115
    label "wymiar"
  ]
  node [
    id 116
    label "antroposfera"
  ]
  node [
    id 117
    label "Arktyka"
  ]
  node [
    id 118
    label "Notogea"
  ]
  node [
    id 119
    label "Piotrowo"
  ]
  node [
    id 120
    label "akrecja"
  ]
  node [
    id 121
    label "zakres"
  ]
  node [
    id 122
    label "Ludwin&#243;w"
  ]
  node [
    id 123
    label "Ruda_Pabianicka"
  ]
  node [
    id 124
    label "miejsce"
  ]
  node [
    id 125
    label "wsch&#243;d"
  ]
  node [
    id 126
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 127
    label "Pow&#261;zki"
  ]
  node [
    id 128
    label "&#321;&#281;g"
  ]
  node [
    id 129
    label "Rakowice"
  ]
  node [
    id 130
    label "Syberia_Wschodnia"
  ]
  node [
    id 131
    label "Zab&#322;ocie"
  ]
  node [
    id 132
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 133
    label "Kresy_Zachodnie"
  ]
  node [
    id 134
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 135
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 136
    label "holarktyka"
  ]
  node [
    id 137
    label "terytorium"
  ]
  node [
    id 138
    label "pas_planetoid"
  ]
  node [
    id 139
    label "Antarktyka"
  ]
  node [
    id 140
    label "Syberia_Zachodnia"
  ]
  node [
    id 141
    label "Neogea"
  ]
  node [
    id 142
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 143
    label "Olszanica"
  ]
  node [
    id 144
    label "liczba"
  ]
  node [
    id 145
    label "uk&#322;ad"
  ]
  node [
    id 146
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 147
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 148
    label "integer"
  ]
  node [
    id 149
    label "zlewanie_si&#281;"
  ]
  node [
    id 150
    label "ilo&#347;&#263;"
  ]
  node [
    id 151
    label "pe&#322;ny"
  ]
  node [
    id 152
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 153
    label "charakter"
  ]
  node [
    id 154
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 155
    label "proces"
  ]
  node [
    id 156
    label "przywidzenie"
  ]
  node [
    id 157
    label "boski"
  ]
  node [
    id 158
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 159
    label "krajobraz"
  ]
  node [
    id 160
    label "presence"
  ]
  node [
    id 161
    label "punkt"
  ]
  node [
    id 162
    label "oktant"
  ]
  node [
    id 163
    label "przedzielenie"
  ]
  node [
    id 164
    label "przedzieli&#263;"
  ]
  node [
    id 165
    label "przestw&#243;r"
  ]
  node [
    id 166
    label "rozdziela&#263;"
  ]
  node [
    id 167
    label "nielito&#347;ciwy"
  ]
  node [
    id 168
    label "czasoprzestrze&#324;"
  ]
  node [
    id 169
    label "niezmierzony"
  ]
  node [
    id 170
    label "bezbrze&#380;e"
  ]
  node [
    id 171
    label "rozdzielanie"
  ]
  node [
    id 172
    label "rura"
  ]
  node [
    id 173
    label "&#347;rodowisko"
  ]
  node [
    id 174
    label "grzebiuszka"
  ]
  node [
    id 175
    label "cz&#322;owiek"
  ]
  node [
    id 176
    label "miniatura"
  ]
  node [
    id 177
    label "odbicie"
  ]
  node [
    id 178
    label "atom"
  ]
  node [
    id 179
    label "kosmos"
  ]
  node [
    id 180
    label "potw&#243;r"
  ]
  node [
    id 181
    label "istota_&#380;ywa"
  ]
  node [
    id 182
    label "monster"
  ]
  node [
    id 183
    label "istota_fantastyczna"
  ]
  node [
    id 184
    label "niecz&#322;owiek"
  ]
  node [
    id 185
    label "smok_wawelski"
  ]
  node [
    id 186
    label "kultura"
  ]
  node [
    id 187
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 188
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 189
    label "aspekt"
  ]
  node [
    id 190
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 191
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 192
    label "powietrznia"
  ]
  node [
    id 193
    label "egzosfera"
  ]
  node [
    id 194
    label "mezopauza"
  ]
  node [
    id 195
    label "mezosfera"
  ]
  node [
    id 196
    label "powietrze"
  ]
  node [
    id 197
    label "pow&#322;oka"
  ]
  node [
    id 198
    label "termosfera"
  ]
  node [
    id 199
    label "stratosfera"
  ]
  node [
    id 200
    label "kwas"
  ]
  node [
    id 201
    label "atmosphere"
  ]
  node [
    id 202
    label "homosfera"
  ]
  node [
    id 203
    label "cecha"
  ]
  node [
    id 204
    label "metasfera"
  ]
  node [
    id 205
    label "tropopauza"
  ]
  node [
    id 206
    label "heterosfera"
  ]
  node [
    id 207
    label "klimat"
  ]
  node [
    id 208
    label "atmosferyki"
  ]
  node [
    id 209
    label "jonosfera"
  ]
  node [
    id 210
    label "troposfera"
  ]
  node [
    id 211
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 212
    label "energia_termiczna"
  ]
  node [
    id 213
    label "ciep&#322;o"
  ]
  node [
    id 214
    label "sferoida"
  ]
  node [
    id 215
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 216
    label "istota"
  ]
  node [
    id 217
    label "wpada&#263;"
  ]
  node [
    id 218
    label "object"
  ]
  node [
    id 219
    label "wpa&#347;&#263;"
  ]
  node [
    id 220
    label "mienie"
  ]
  node [
    id 221
    label "obiekt"
  ]
  node [
    id 222
    label "temat"
  ]
  node [
    id 223
    label "wpadni&#281;cie"
  ]
  node [
    id 224
    label "wpadanie"
  ]
  node [
    id 225
    label "interception"
  ]
  node [
    id 226
    label "zaczerpni&#281;cie"
  ]
  node [
    id 227
    label "wzbudzenie"
  ]
  node [
    id 228
    label "wzi&#281;cie"
  ]
  node [
    id 229
    label "movement"
  ]
  node [
    id 230
    label "wra&#380;enie"
  ]
  node [
    id 231
    label "emotion"
  ]
  node [
    id 232
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 233
    label "thrill"
  ]
  node [
    id 234
    label "ogarn&#261;&#263;"
  ]
  node [
    id 235
    label "bang"
  ]
  node [
    id 236
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 237
    label "wzi&#261;&#263;"
  ]
  node [
    id 238
    label "wzbudzi&#263;"
  ]
  node [
    id 239
    label "stimulate"
  ]
  node [
    id 240
    label "ogarnia&#263;"
  ]
  node [
    id 241
    label "handle"
  ]
  node [
    id 242
    label "treat"
  ]
  node [
    id 243
    label "czerpa&#263;"
  ]
  node [
    id 244
    label "go"
  ]
  node [
    id 245
    label "bra&#263;"
  ]
  node [
    id 246
    label "wzbudza&#263;"
  ]
  node [
    id 247
    label "branie"
  ]
  node [
    id 248
    label "acquisition"
  ]
  node [
    id 249
    label "czerpanie"
  ]
  node [
    id 250
    label "ogarnianie"
  ]
  node [
    id 251
    label "wzbudzanie"
  ]
  node [
    id 252
    label "caparison"
  ]
  node [
    id 253
    label "czynno&#347;&#263;"
  ]
  node [
    id 254
    label "discipline"
  ]
  node [
    id 255
    label "zboczy&#263;"
  ]
  node [
    id 256
    label "w&#261;tek"
  ]
  node [
    id 257
    label "entity"
  ]
  node [
    id 258
    label "sponiewiera&#263;"
  ]
  node [
    id 259
    label "zboczenie"
  ]
  node [
    id 260
    label "zbaczanie"
  ]
  node [
    id 261
    label "thing"
  ]
  node [
    id 262
    label "om&#243;wi&#263;"
  ]
  node [
    id 263
    label "tre&#347;&#263;"
  ]
  node [
    id 264
    label "element"
  ]
  node [
    id 265
    label "kr&#261;&#380;enie"
  ]
  node [
    id 266
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 267
    label "zbacza&#263;"
  ]
  node [
    id 268
    label "om&#243;wienie"
  ]
  node [
    id 269
    label "tematyka"
  ]
  node [
    id 270
    label "omawianie"
  ]
  node [
    id 271
    label "omawia&#263;"
  ]
  node [
    id 272
    label "robienie"
  ]
  node [
    id 273
    label "program_nauczania"
  ]
  node [
    id 274
    label "sponiewieranie"
  ]
  node [
    id 275
    label "performance"
  ]
  node [
    id 276
    label "sztuka"
  ]
  node [
    id 277
    label "granica_pa&#324;stwa"
  ]
  node [
    id 278
    label "strona_&#347;wiata"
  ]
  node [
    id 279
    label "p&#243;&#322;nocek"
  ]
  node [
    id 280
    label "noc"
  ]
  node [
    id 281
    label "Boreasz"
  ]
  node [
    id 282
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 283
    label "godzina"
  ]
  node [
    id 284
    label "&#347;rodek"
  ]
  node [
    id 285
    label "dwunasta"
  ]
  node [
    id 286
    label "dzie&#324;"
  ]
  node [
    id 287
    label "pora"
  ]
  node [
    id 288
    label "zawiasy"
  ]
  node [
    id 289
    label "brzeg"
  ]
  node [
    id 290
    label "p&#322;oza"
  ]
  node [
    id 291
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 292
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 293
    label "element_anatomiczny"
  ]
  node [
    id 294
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 295
    label "organ"
  ]
  node [
    id 296
    label "marina"
  ]
  node [
    id 297
    label "reda"
  ]
  node [
    id 298
    label "pe&#322;ne_morze"
  ]
  node [
    id 299
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 300
    label "Morze_Bia&#322;e"
  ]
  node [
    id 301
    label "przymorze"
  ]
  node [
    id 302
    label "Morze_Adriatyckie"
  ]
  node [
    id 303
    label "paliszcze"
  ]
  node [
    id 304
    label "talasoterapia"
  ]
  node [
    id 305
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 306
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 307
    label "bezmiar"
  ]
  node [
    id 308
    label "Morze_Egejskie"
  ]
  node [
    id 309
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 310
    label "latarnia_morska"
  ]
  node [
    id 311
    label "Neptun"
  ]
  node [
    id 312
    label "Morze_Czarne"
  ]
  node [
    id 313
    label "nereida"
  ]
  node [
    id 314
    label "laguna"
  ]
  node [
    id 315
    label "okeanida"
  ]
  node [
    id 316
    label "Morze_Czerwone"
  ]
  node [
    id 317
    label "zbiornik_wodny"
  ]
  node [
    id 318
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 319
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 320
    label "rze&#378;biarstwo"
  ]
  node [
    id 321
    label "planacja"
  ]
  node [
    id 322
    label "plastyka"
  ]
  node [
    id 323
    label "relief"
  ]
  node [
    id 324
    label "bozzetto"
  ]
  node [
    id 325
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 326
    label "sfera"
  ]
  node [
    id 327
    label "warstwa_perydotytowa"
  ]
  node [
    id 328
    label "gleba"
  ]
  node [
    id 329
    label "skorupa_ziemska"
  ]
  node [
    id 330
    label "warstwa"
  ]
  node [
    id 331
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 332
    label "warstwa_granitowa"
  ]
  node [
    id 333
    label "sialma"
  ]
  node [
    id 334
    label "kriosfera"
  ]
  node [
    id 335
    label "j&#261;dro"
  ]
  node [
    id 336
    label "lej_polarny"
  ]
  node [
    id 337
    label "kresom&#243;zgowie"
  ]
  node [
    id 338
    label "kula"
  ]
  node [
    id 339
    label "ozon"
  ]
  node [
    id 340
    label "przyra"
  ]
  node [
    id 341
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 342
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 343
    label "miejsce_pracy"
  ]
  node [
    id 344
    label "nation"
  ]
  node [
    id 345
    label "w&#322;adza"
  ]
  node [
    id 346
    label "kontekst"
  ]
  node [
    id 347
    label "cyprysowate"
  ]
  node [
    id 348
    label "iglak"
  ]
  node [
    id 349
    label "plant"
  ]
  node [
    id 350
    label "ro&#347;lina"
  ]
  node [
    id 351
    label "formacja_ro&#347;linna"
  ]
  node [
    id 352
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 353
    label "biom"
  ]
  node [
    id 354
    label "geosystem"
  ]
  node [
    id 355
    label "szata_ro&#347;linna"
  ]
  node [
    id 356
    label "zielono&#347;&#263;"
  ]
  node [
    id 357
    label "pi&#281;tro"
  ]
  node [
    id 358
    label "przybieranie"
  ]
  node [
    id 359
    label "pustka"
  ]
  node [
    id 360
    label "przybrze&#380;e"
  ]
  node [
    id 361
    label "woda_s&#322;odka"
  ]
  node [
    id 362
    label "utylizator"
  ]
  node [
    id 363
    label "spi&#281;trzenie"
  ]
  node [
    id 364
    label "wodnik"
  ]
  node [
    id 365
    label "water"
  ]
  node [
    id 366
    label "fala"
  ]
  node [
    id 367
    label "kryptodepresja"
  ]
  node [
    id 368
    label "klarownik"
  ]
  node [
    id 369
    label "tlenek"
  ]
  node [
    id 370
    label "l&#243;d"
  ]
  node [
    id 371
    label "nabranie"
  ]
  node [
    id 372
    label "chlastanie"
  ]
  node [
    id 373
    label "zrzut"
  ]
  node [
    id 374
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 375
    label "uci&#261;g"
  ]
  node [
    id 376
    label "nabra&#263;"
  ]
  node [
    id 377
    label "wybrze&#380;e"
  ]
  node [
    id 378
    label "p&#322;ycizna"
  ]
  node [
    id 379
    label "uj&#281;cie_wody"
  ]
  node [
    id 380
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 381
    label "ciecz"
  ]
  node [
    id 382
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 383
    label "Waruna"
  ]
  node [
    id 384
    label "chlasta&#263;"
  ]
  node [
    id 385
    label "bicie"
  ]
  node [
    id 386
    label "deklamacja"
  ]
  node [
    id 387
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 388
    label "spi&#281;trzanie"
  ]
  node [
    id 389
    label "wypowied&#378;"
  ]
  node [
    id 390
    label "spi&#281;trza&#263;"
  ]
  node [
    id 391
    label "wysi&#281;k"
  ]
  node [
    id 392
    label "dotleni&#263;"
  ]
  node [
    id 393
    label "pojazd"
  ]
  node [
    id 394
    label "nap&#243;j"
  ]
  node [
    id 395
    label "bombast"
  ]
  node [
    id 396
    label "biotop"
  ]
  node [
    id 397
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 398
    label "biocenoza"
  ]
  node [
    id 399
    label "awifauna"
  ]
  node [
    id 400
    label "ichtiofauna"
  ]
  node [
    id 401
    label "zlewozmywak"
  ]
  node [
    id 402
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 403
    label "zaplecze"
  ]
  node [
    id 404
    label "gotowa&#263;"
  ]
  node [
    id 405
    label "jedzenie"
  ]
  node [
    id 406
    label "tajniki"
  ]
  node [
    id 407
    label "zaj&#281;cie"
  ]
  node [
    id 408
    label "instytucja"
  ]
  node [
    id 409
    label "pomieszczenie"
  ]
  node [
    id 410
    label "Jowisz"
  ]
  node [
    id 411
    label "syzygia"
  ]
  node [
    id 412
    label "Uran"
  ]
  node [
    id 413
    label "Saturn"
  ]
  node [
    id 414
    label "strefa"
  ]
  node [
    id 415
    label "message"
  ]
  node [
    id 416
    label "dar"
  ]
  node [
    id 417
    label "real"
  ]
  node [
    id 418
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 419
    label "Ukraina"
  ]
  node [
    id 420
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 421
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 422
    label "blok_wschodni"
  ]
  node [
    id 423
    label "Europa_Wschodnia"
  ]
  node [
    id 424
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 425
    label "tr&#243;jskok"
  ]
  node [
    id 426
    label "sport"
  ]
  node [
    id 427
    label "skok_wzwy&#380;"
  ]
  node [
    id 428
    label "rzut_oszczepem"
  ]
  node [
    id 429
    label "pchni&#281;cie_kul&#261;"
  ]
  node [
    id 430
    label "skok_o_tyczce"
  ]
  node [
    id 431
    label "rzut_m&#322;otem"
  ]
  node [
    id 432
    label "dziesi&#281;ciob&#243;j"
  ]
  node [
    id 433
    label "bieg"
  ]
  node [
    id 434
    label "skok_w_dal"
  ]
  node [
    id 435
    label "ch&#243;d"
  ]
  node [
    id 436
    label "atakowanie"
  ]
  node [
    id 437
    label "zaatakowanie"
  ]
  node [
    id 438
    label "kultura_fizyczna"
  ]
  node [
    id 439
    label "zgrupowanie"
  ]
  node [
    id 440
    label "atakowa&#263;"
  ]
  node [
    id 441
    label "usportowienie"
  ]
  node [
    id 442
    label "sokolstwo"
  ]
  node [
    id 443
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 444
    label "zaatakowa&#263;"
  ]
  node [
    id 445
    label "usportowi&#263;"
  ]
  node [
    id 446
    label "krok"
  ]
  node [
    id 447
    label "wy&#347;cig"
  ]
  node [
    id 448
    label "step"
  ]
  node [
    id 449
    label "ruch"
  ]
  node [
    id 450
    label "czerwona_kartka"
  ]
  node [
    id 451
    label "konkurencja"
  ]
  node [
    id 452
    label "skoki"
  ]
  node [
    id 453
    label "linia"
  ]
  node [
    id 454
    label "pozycja"
  ]
  node [
    id 455
    label "przedbieg"
  ]
  node [
    id 456
    label "d&#261;&#380;enie"
  ]
  node [
    id 457
    label "syfon"
  ]
  node [
    id 458
    label "bystrzyca"
  ]
  node [
    id 459
    label "parametr"
  ]
  node [
    id 460
    label "ciek_wodny"
  ]
  node [
    id 461
    label "pr&#261;d"
  ]
  node [
    id 462
    label "cycle"
  ]
  node [
    id 463
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 464
    label "tryb"
  ]
  node [
    id 465
    label "czo&#322;&#243;wka"
  ]
  node [
    id 466
    label "roll"
  ]
  node [
    id 467
    label "kierunek"
  ]
  node [
    id 468
    label "kurs"
  ]
  node [
    id 469
    label "procedura"
  ]
  node [
    id 470
    label "mistrzostwo"
  ]
  node [
    id 471
    label "wyspa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 470
    target 471
  ]
]
