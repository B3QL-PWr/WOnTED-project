graph [
  node [
    id 0
    label "eksploatacja"
    origin "text"
  ]
  node [
    id 1
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "telekomunikacyjny"
    origin "text"
  ]
  node [
    id 3
    label "utilization"
  ]
  node [
    id 4
    label "czynno&#347;&#263;"
  ]
  node [
    id 5
    label "wyzysk"
  ]
  node [
    id 6
    label "proces"
  ]
  node [
    id 7
    label "kognicja"
  ]
  node [
    id 8
    label "przebieg"
  ]
  node [
    id 9
    label "rozprawa"
  ]
  node [
    id 10
    label "wydarzenie"
  ]
  node [
    id 11
    label "legislacyjnie"
  ]
  node [
    id 12
    label "przes&#322;anka"
  ]
  node [
    id 13
    label "zjawisko"
  ]
  node [
    id 14
    label "nast&#281;pstwo"
  ]
  node [
    id 15
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 16
    label "activity"
  ]
  node [
    id 17
    label "bezproblemowy"
  ]
  node [
    id 18
    label "rozb&#243;j"
  ]
  node [
    id 19
    label "exploitation"
  ]
  node [
    id 20
    label "krzywda"
  ]
  node [
    id 21
    label "kszta&#322;t"
  ]
  node [
    id 22
    label "provider"
  ]
  node [
    id 23
    label "biznes_elektroniczny"
  ]
  node [
    id 24
    label "zasadzka"
  ]
  node [
    id 25
    label "mesh"
  ]
  node [
    id 26
    label "plecionka"
  ]
  node [
    id 27
    label "gauze"
  ]
  node [
    id 28
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 29
    label "struktura"
  ]
  node [
    id 30
    label "web"
  ]
  node [
    id 31
    label "organizacja"
  ]
  node [
    id 32
    label "gra_sieciowa"
  ]
  node [
    id 33
    label "net"
  ]
  node [
    id 34
    label "media"
  ]
  node [
    id 35
    label "sie&#263;_komputerowa"
  ]
  node [
    id 36
    label "nitka"
  ]
  node [
    id 37
    label "snu&#263;"
  ]
  node [
    id 38
    label "vane"
  ]
  node [
    id 39
    label "instalacja"
  ]
  node [
    id 40
    label "wysnu&#263;"
  ]
  node [
    id 41
    label "organization"
  ]
  node [
    id 42
    label "obiekt"
  ]
  node [
    id 43
    label "us&#322;uga_internetowa"
  ]
  node [
    id 44
    label "rozmieszczenie"
  ]
  node [
    id 45
    label "podcast"
  ]
  node [
    id 46
    label "hipertekst"
  ]
  node [
    id 47
    label "cyberprzestrze&#324;"
  ]
  node [
    id 48
    label "mem"
  ]
  node [
    id 49
    label "grooming"
  ]
  node [
    id 50
    label "punkt_dost&#281;pu"
  ]
  node [
    id 51
    label "netbook"
  ]
  node [
    id 52
    label "e-hazard"
  ]
  node [
    id 53
    label "strona"
  ]
  node [
    id 54
    label "zastawia&#263;"
  ]
  node [
    id 55
    label "miejsce"
  ]
  node [
    id 56
    label "zastawi&#263;"
  ]
  node [
    id 57
    label "ambush"
  ]
  node [
    id 58
    label "atak"
  ]
  node [
    id 59
    label "podst&#281;p"
  ]
  node [
    id 60
    label "sytuacja"
  ]
  node [
    id 61
    label "formacja"
  ]
  node [
    id 62
    label "punkt_widzenia"
  ]
  node [
    id 63
    label "wygl&#261;d"
  ]
  node [
    id 64
    label "g&#322;owa"
  ]
  node [
    id 65
    label "spirala"
  ]
  node [
    id 66
    label "p&#322;at"
  ]
  node [
    id 67
    label "comeliness"
  ]
  node [
    id 68
    label "kielich"
  ]
  node [
    id 69
    label "face"
  ]
  node [
    id 70
    label "blaszka"
  ]
  node [
    id 71
    label "charakter"
  ]
  node [
    id 72
    label "p&#281;tla"
  ]
  node [
    id 73
    label "pasmo"
  ]
  node [
    id 74
    label "cecha"
  ]
  node [
    id 75
    label "linearno&#347;&#263;"
  ]
  node [
    id 76
    label "gwiazda"
  ]
  node [
    id 77
    label "miniatura"
  ]
  node [
    id 78
    label "integer"
  ]
  node [
    id 79
    label "liczba"
  ]
  node [
    id 80
    label "zlewanie_si&#281;"
  ]
  node [
    id 81
    label "ilo&#347;&#263;"
  ]
  node [
    id 82
    label "uk&#322;ad"
  ]
  node [
    id 83
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 84
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 85
    label "pe&#322;ny"
  ]
  node [
    id 86
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 87
    label "u&#322;o&#380;enie"
  ]
  node [
    id 88
    label "porozmieszczanie"
  ]
  node [
    id 89
    label "wyst&#281;powanie"
  ]
  node [
    id 90
    label "layout"
  ]
  node [
    id 91
    label "umieszczenie"
  ]
  node [
    id 92
    label "mechanika"
  ]
  node [
    id 93
    label "o&#347;"
  ]
  node [
    id 94
    label "usenet"
  ]
  node [
    id 95
    label "rozprz&#261;c"
  ]
  node [
    id 96
    label "zachowanie"
  ]
  node [
    id 97
    label "cybernetyk"
  ]
  node [
    id 98
    label "podsystem"
  ]
  node [
    id 99
    label "system"
  ]
  node [
    id 100
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 101
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 102
    label "sk&#322;ad"
  ]
  node [
    id 103
    label "systemat"
  ]
  node [
    id 104
    label "konstrukcja"
  ]
  node [
    id 105
    label "konstelacja"
  ]
  node [
    id 106
    label "podmiot"
  ]
  node [
    id 107
    label "jednostka_organizacyjna"
  ]
  node [
    id 108
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 109
    label "TOPR"
  ]
  node [
    id 110
    label "endecki"
  ]
  node [
    id 111
    label "zesp&#243;&#322;"
  ]
  node [
    id 112
    label "przedstawicielstwo"
  ]
  node [
    id 113
    label "od&#322;am"
  ]
  node [
    id 114
    label "Cepelia"
  ]
  node [
    id 115
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 116
    label "ZBoWiD"
  ]
  node [
    id 117
    label "centrala"
  ]
  node [
    id 118
    label "GOPR"
  ]
  node [
    id 119
    label "ZOMO"
  ]
  node [
    id 120
    label "ZMP"
  ]
  node [
    id 121
    label "komitet_koordynacyjny"
  ]
  node [
    id 122
    label "przybud&#243;wka"
  ]
  node [
    id 123
    label "boj&#243;wka"
  ]
  node [
    id 124
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 125
    label "kompozycja"
  ]
  node [
    id 126
    label "uzbrajanie"
  ]
  node [
    id 127
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 128
    label "co&#347;"
  ]
  node [
    id 129
    label "budynek"
  ]
  node [
    id 130
    label "thing"
  ]
  node [
    id 131
    label "poj&#281;cie"
  ]
  node [
    id 132
    label "program"
  ]
  node [
    id 133
    label "rzecz"
  ]
  node [
    id 134
    label "mass-media"
  ]
  node [
    id 135
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 136
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 137
    label "przekazior"
  ]
  node [
    id 138
    label "medium"
  ]
  node [
    id 139
    label "tekst"
  ]
  node [
    id 140
    label "ornament"
  ]
  node [
    id 141
    label "przedmiot"
  ]
  node [
    id 142
    label "splot"
  ]
  node [
    id 143
    label "braid"
  ]
  node [
    id 144
    label "szachulec"
  ]
  node [
    id 145
    label "b&#322;&#261;d"
  ]
  node [
    id 146
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 147
    label "nawijad&#322;o"
  ]
  node [
    id 148
    label "sznur"
  ]
  node [
    id 149
    label "motowid&#322;o"
  ]
  node [
    id 150
    label "makaron"
  ]
  node [
    id 151
    label "internet"
  ]
  node [
    id 152
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 153
    label "kartka"
  ]
  node [
    id 154
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 155
    label "logowanie"
  ]
  node [
    id 156
    label "plik"
  ]
  node [
    id 157
    label "s&#261;d"
  ]
  node [
    id 158
    label "adres_internetowy"
  ]
  node [
    id 159
    label "linia"
  ]
  node [
    id 160
    label "serwis_internetowy"
  ]
  node [
    id 161
    label "posta&#263;"
  ]
  node [
    id 162
    label "bok"
  ]
  node [
    id 163
    label "skr&#281;canie"
  ]
  node [
    id 164
    label "skr&#281;ca&#263;"
  ]
  node [
    id 165
    label "orientowanie"
  ]
  node [
    id 166
    label "skr&#281;ci&#263;"
  ]
  node [
    id 167
    label "uj&#281;cie"
  ]
  node [
    id 168
    label "zorientowanie"
  ]
  node [
    id 169
    label "ty&#322;"
  ]
  node [
    id 170
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 171
    label "fragment"
  ]
  node [
    id 172
    label "zorientowa&#263;"
  ]
  node [
    id 173
    label "pagina"
  ]
  node [
    id 174
    label "g&#243;ra"
  ]
  node [
    id 175
    label "orientowa&#263;"
  ]
  node [
    id 176
    label "voice"
  ]
  node [
    id 177
    label "orientacja"
  ]
  node [
    id 178
    label "prz&#243;d"
  ]
  node [
    id 179
    label "powierzchnia"
  ]
  node [
    id 180
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 181
    label "forma"
  ]
  node [
    id 182
    label "skr&#281;cenie"
  ]
  node [
    id 183
    label "paj&#261;k"
  ]
  node [
    id 184
    label "devise"
  ]
  node [
    id 185
    label "wyjmowa&#263;"
  ]
  node [
    id 186
    label "project"
  ]
  node [
    id 187
    label "my&#347;le&#263;"
  ]
  node [
    id 188
    label "produkowa&#263;"
  ]
  node [
    id 189
    label "uk&#322;ada&#263;"
  ]
  node [
    id 190
    label "tworzy&#263;"
  ]
  node [
    id 191
    label "wyj&#261;&#263;"
  ]
  node [
    id 192
    label "stworzy&#263;"
  ]
  node [
    id 193
    label "zasadzi&#263;"
  ]
  node [
    id 194
    label "dostawca"
  ]
  node [
    id 195
    label "telefonia"
  ]
  node [
    id 196
    label "wydawnictwo"
  ]
  node [
    id 197
    label "meme"
  ]
  node [
    id 198
    label "hazard"
  ]
  node [
    id 199
    label "molestowanie_seksualne"
  ]
  node [
    id 200
    label "piel&#281;gnacja"
  ]
  node [
    id 201
    label "zwierz&#281;_domowe"
  ]
  node [
    id 202
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 203
    label "ma&#322;y"
  ]
  node [
    id 204
    label "prawo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 2
    target 204
  ]
]
