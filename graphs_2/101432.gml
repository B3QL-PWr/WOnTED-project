graph [
  node [
    id 0
    label "newman"
    origin "text"
  ]
  node [
    id 1
    label "australia"
    origin "text"
  ]
  node [
    id 2
    label "Australia"
  ]
  node [
    id 3
    label "zachodni"
  ]
  node [
    id 4
    label "East"
  ]
  node [
    id 5
    label "Pilbara"
  ]
  node [
    id 6
    label "porto"
  ]
  node [
    id 7
    label "Hedland"
  ]
  node [
    id 8
    label "albo"
  ]
  node [
    id 9
    label "wyspa"
  ]
  node [
    id 10
    label "Newman"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
]
