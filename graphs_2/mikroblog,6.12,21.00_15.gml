graph [
  node [
    id 0
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "apteka"
    origin "text"
  ]
  node [
    id 4
    label "dziewczynka"
  ]
  node [
    id 5
    label "dziewczyna"
  ]
  node [
    id 6
    label "prostytutka"
  ]
  node [
    id 7
    label "dziecko"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "potomkini"
  ]
  node [
    id 10
    label "dziewka"
  ]
  node [
    id 11
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 12
    label "sikorka"
  ]
  node [
    id 13
    label "kora"
  ]
  node [
    id 14
    label "dziewcz&#281;"
  ]
  node [
    id 15
    label "dziecina"
  ]
  node [
    id 16
    label "m&#322;&#243;dka"
  ]
  node [
    id 17
    label "sympatia"
  ]
  node [
    id 18
    label "dziunia"
  ]
  node [
    id 19
    label "dziewczynina"
  ]
  node [
    id 20
    label "partnerka"
  ]
  node [
    id 21
    label "siksa"
  ]
  node [
    id 22
    label "dziewoja"
  ]
  node [
    id 23
    label "Aurignac"
  ]
  node [
    id 24
    label "Sabaudia"
  ]
  node [
    id 25
    label "Cecora"
  ]
  node [
    id 26
    label "Saint-Acheul"
  ]
  node [
    id 27
    label "Boulogne"
  ]
  node [
    id 28
    label "Opat&#243;wek"
  ]
  node [
    id 29
    label "osiedle"
  ]
  node [
    id 30
    label "Levallois-Perret"
  ]
  node [
    id 31
    label "Szcz&#281;&#347;liwice"
  ]
  node [
    id 32
    label "Jelcz"
  ]
  node [
    id 33
    label "Kaw&#281;czyn"
  ]
  node [
    id 34
    label "Br&#243;dno"
  ]
  node [
    id 35
    label "Marysin"
  ]
  node [
    id 36
    label "Ochock"
  ]
  node [
    id 37
    label "Kabaty"
  ]
  node [
    id 38
    label "Paw&#322;owice"
  ]
  node [
    id 39
    label "Falenica"
  ]
  node [
    id 40
    label "Osobowice"
  ]
  node [
    id 41
    label "Wielopole"
  ]
  node [
    id 42
    label "Boryszew"
  ]
  node [
    id 43
    label "Chojny"
  ]
  node [
    id 44
    label "Szack"
  ]
  node [
    id 45
    label "Powsin"
  ]
  node [
    id 46
    label "Bielice"
  ]
  node [
    id 47
    label "Wi&#347;niowiec"
  ]
  node [
    id 48
    label "Branice"
  ]
  node [
    id 49
    label "Rej&#243;w"
  ]
  node [
    id 50
    label "Zerze&#324;"
  ]
  node [
    id 51
    label "Rakowiec"
  ]
  node [
    id 52
    label "osadnictwo"
  ]
  node [
    id 53
    label "Jelonki"
  ]
  node [
    id 54
    label "Gronik"
  ]
  node [
    id 55
    label "Horodyszcze"
  ]
  node [
    id 56
    label "S&#281;polno"
  ]
  node [
    id 57
    label "Salwator"
  ]
  node [
    id 58
    label "Mariensztat"
  ]
  node [
    id 59
    label "Lubiesz&#243;w"
  ]
  node [
    id 60
    label "Izborsk"
  ]
  node [
    id 61
    label "Orunia"
  ]
  node [
    id 62
    label "Opor&#243;w"
  ]
  node [
    id 63
    label "Miedzeszyn"
  ]
  node [
    id 64
    label "Nadodrze"
  ]
  node [
    id 65
    label "Natolin"
  ]
  node [
    id 66
    label "Wi&#347;niewo"
  ]
  node [
    id 67
    label "Wojn&#243;w"
  ]
  node [
    id 68
    label "Ujazd&#243;w"
  ]
  node [
    id 69
    label "Solec"
  ]
  node [
    id 70
    label "Biskupin"
  ]
  node [
    id 71
    label "G&#243;rce"
  ]
  node [
    id 72
    label "Siersza"
  ]
  node [
    id 73
    label "Wawrzyszew"
  ]
  node [
    id 74
    label "&#321;agiewniki"
  ]
  node [
    id 75
    label "Azory"
  ]
  node [
    id 76
    label "&#379;erniki"
  ]
  node [
    id 77
    label "jednostka_administracyjna"
  ]
  node [
    id 78
    label "Goc&#322;aw"
  ]
  node [
    id 79
    label "Latycz&#243;w"
  ]
  node [
    id 80
    label "Micha&#322;owo"
  ]
  node [
    id 81
    label "zesp&#243;&#322;"
  ]
  node [
    id 82
    label "Broch&#243;w"
  ]
  node [
    id 83
    label "jednostka_osadnicza"
  ]
  node [
    id 84
    label "M&#322;ociny"
  ]
  node [
    id 85
    label "Groch&#243;w"
  ]
  node [
    id 86
    label "dzielnica"
  ]
  node [
    id 87
    label "Laskowice_O&#322;awskie"
  ]
  node [
    id 88
    label "Marysin_Wawerski"
  ]
  node [
    id 89
    label "Le&#347;nica"
  ]
  node [
    id 90
    label "Kortowo"
  ]
  node [
    id 91
    label "G&#322;uszyna"
  ]
  node [
    id 92
    label "Kar&#322;owice"
  ]
  node [
    id 93
    label "Kujbyszewe"
  ]
  node [
    id 94
    label "Tarchomin"
  ]
  node [
    id 95
    label "&#379;era&#324;"
  ]
  node [
    id 96
    label "Jasienica"
  ]
  node [
    id 97
    label "Ok&#281;cie"
  ]
  node [
    id 98
    label "Zakrz&#243;w"
  ]
  node [
    id 99
    label "G&#243;rczyn"
  ]
  node [
    id 100
    label "Powi&#347;le"
  ]
  node [
    id 101
    label "Lewin&#243;w"
  ]
  node [
    id 102
    label "Gutkowo"
  ]
  node [
    id 103
    label "Wad&#243;w"
  ]
  node [
    id 104
    label "grupa"
  ]
  node [
    id 105
    label "Gr&#281;ba&#322;&#243;w"
  ]
  node [
    id 106
    label "Dojlidy"
  ]
  node [
    id 107
    label "Marymont"
  ]
  node [
    id 108
    label "Rataje"
  ]
  node [
    id 109
    label "Grabiszyn"
  ]
  node [
    id 110
    label "Szczytniki"
  ]
  node [
    id 111
    label "Anin"
  ]
  node [
    id 112
    label "Imielin"
  ]
  node [
    id 113
    label "siedziba"
  ]
  node [
    id 114
    label "Zalesie"
  ]
  node [
    id 115
    label "Arsk"
  ]
  node [
    id 116
    label "Bogucice"
  ]
  node [
    id 117
    label "kultura_aszelska"
  ]
  node [
    id 118
    label "Francja"
  ]
  node [
    id 119
    label "shot"
  ]
  node [
    id 120
    label "jednakowy"
  ]
  node [
    id 121
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 122
    label "ujednolicenie"
  ]
  node [
    id 123
    label "jaki&#347;"
  ]
  node [
    id 124
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 125
    label "jednolicie"
  ]
  node [
    id 126
    label "kieliszek"
  ]
  node [
    id 127
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 128
    label "w&#243;dka"
  ]
  node [
    id 129
    label "ten"
  ]
  node [
    id 130
    label "szk&#322;o"
  ]
  node [
    id 131
    label "zawarto&#347;&#263;"
  ]
  node [
    id 132
    label "naczynie"
  ]
  node [
    id 133
    label "alkohol"
  ]
  node [
    id 134
    label "sznaps"
  ]
  node [
    id 135
    label "nap&#243;j"
  ]
  node [
    id 136
    label "gorza&#322;ka"
  ]
  node [
    id 137
    label "mohorycz"
  ]
  node [
    id 138
    label "okre&#347;lony"
  ]
  node [
    id 139
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 140
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 141
    label "mundurowanie"
  ]
  node [
    id 142
    label "zr&#243;wnanie"
  ]
  node [
    id 143
    label "taki&#380;"
  ]
  node [
    id 144
    label "mundurowa&#263;"
  ]
  node [
    id 145
    label "jednakowo"
  ]
  node [
    id 146
    label "zr&#243;wnywanie"
  ]
  node [
    id 147
    label "identyczny"
  ]
  node [
    id 148
    label "z&#322;o&#380;ony"
  ]
  node [
    id 149
    label "przyzwoity"
  ]
  node [
    id 150
    label "ciekawy"
  ]
  node [
    id 151
    label "jako&#347;"
  ]
  node [
    id 152
    label "jako_tako"
  ]
  node [
    id 153
    label "niez&#322;y"
  ]
  node [
    id 154
    label "dziwny"
  ]
  node [
    id 155
    label "charakterystyczny"
  ]
  node [
    id 156
    label "g&#322;&#281;bszy"
  ]
  node [
    id 157
    label "drink"
  ]
  node [
    id 158
    label "upodobnienie"
  ]
  node [
    id 159
    label "jednolity"
  ]
  node [
    id 160
    label "calibration"
  ]
  node [
    id 161
    label "sklep"
  ]
  node [
    id 162
    label "punkt_apteczny"
  ]
  node [
    id 163
    label "p&#243;&#322;ka"
  ]
  node [
    id 164
    label "firma"
  ]
  node [
    id 165
    label "stoisko"
  ]
  node [
    id 166
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 167
    label "sk&#322;ad"
  ]
  node [
    id 168
    label "obiekt_handlowy"
  ]
  node [
    id 169
    label "zaplecze"
  ]
  node [
    id 170
    label "witryna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
]
