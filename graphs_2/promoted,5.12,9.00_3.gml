graph [
  node [
    id 0
    label "ubieg&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "sp&#322;on&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kolejny"
    origin "text"
  ]
  node [
    id 4
    label "wysypisko"
    origin "text"
  ]
  node [
    id 5
    label "odpad"
    origin "text"
  ]
  node [
    id 6
    label "stara"
    origin "text"
  ]
  node [
    id 7
    label "opona"
    origin "text"
  ]
  node [
    id 8
    label "ostatni"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "niedawno"
  ]
  node [
    id 11
    label "poprzedni"
  ]
  node [
    id 12
    label "pozosta&#322;y"
  ]
  node [
    id 13
    label "ostatnio"
  ]
  node [
    id 14
    label "sko&#324;czony"
  ]
  node [
    id 15
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 16
    label "aktualny"
  ]
  node [
    id 17
    label "najgorszy"
  ]
  node [
    id 18
    label "istota_&#380;ywa"
  ]
  node [
    id 19
    label "w&#261;tpliwy"
  ]
  node [
    id 20
    label "doba"
  ]
  node [
    id 21
    label "weekend"
  ]
  node [
    id 22
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 23
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 24
    label "czas"
  ]
  node [
    id 25
    label "miesi&#261;c"
  ]
  node [
    id 26
    label "poprzedzanie"
  ]
  node [
    id 27
    label "czasoprzestrze&#324;"
  ]
  node [
    id 28
    label "laba"
  ]
  node [
    id 29
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 30
    label "chronometria"
  ]
  node [
    id 31
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 32
    label "rachuba_czasu"
  ]
  node [
    id 33
    label "przep&#322;ywanie"
  ]
  node [
    id 34
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 35
    label "czasokres"
  ]
  node [
    id 36
    label "odczyt"
  ]
  node [
    id 37
    label "chwila"
  ]
  node [
    id 38
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 39
    label "dzieje"
  ]
  node [
    id 40
    label "kategoria_gramatyczna"
  ]
  node [
    id 41
    label "poprzedzenie"
  ]
  node [
    id 42
    label "trawienie"
  ]
  node [
    id 43
    label "pochodzi&#263;"
  ]
  node [
    id 44
    label "period"
  ]
  node [
    id 45
    label "okres_czasu"
  ]
  node [
    id 46
    label "poprzedza&#263;"
  ]
  node [
    id 47
    label "schy&#322;ek"
  ]
  node [
    id 48
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 49
    label "odwlekanie_si&#281;"
  ]
  node [
    id 50
    label "zegar"
  ]
  node [
    id 51
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 52
    label "czwarty_wymiar"
  ]
  node [
    id 53
    label "pochodzenie"
  ]
  node [
    id 54
    label "koniugacja"
  ]
  node [
    id 55
    label "Zeitgeist"
  ]
  node [
    id 56
    label "trawi&#263;"
  ]
  node [
    id 57
    label "pogoda"
  ]
  node [
    id 58
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 59
    label "poprzedzi&#263;"
  ]
  node [
    id 60
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 61
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 62
    label "time_period"
  ]
  node [
    id 63
    label "noc"
  ]
  node [
    id 64
    label "dzie&#324;"
  ]
  node [
    id 65
    label "godzina"
  ]
  node [
    id 66
    label "long_time"
  ]
  node [
    id 67
    label "jednostka_geologiczna"
  ]
  node [
    id 68
    label "niedziela"
  ]
  node [
    id 69
    label "sobota"
  ]
  node [
    id 70
    label "miech"
  ]
  node [
    id 71
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 72
    label "rok"
  ]
  node [
    id 73
    label "kalendy"
  ]
  node [
    id 74
    label "ulec"
  ]
  node [
    id 75
    label "erupt"
  ]
  node [
    id 76
    label "zgorze&#263;"
  ]
  node [
    id 77
    label "zjara&#263;_si&#281;"
  ]
  node [
    id 78
    label "przegorze&#263;"
  ]
  node [
    id 79
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 80
    label "sta&#263;_si&#281;"
  ]
  node [
    id 81
    label "fall"
  ]
  node [
    id 82
    label "give"
  ]
  node [
    id 83
    label "kobieta"
  ]
  node [
    id 84
    label "pozwoli&#263;"
  ]
  node [
    id 85
    label "podda&#263;"
  ]
  node [
    id 86
    label "put_in"
  ]
  node [
    id 87
    label "podda&#263;_si&#281;"
  ]
  node [
    id 88
    label "nast&#281;pnie"
  ]
  node [
    id 89
    label "inny"
  ]
  node [
    id 90
    label "nastopny"
  ]
  node [
    id 91
    label "kolejno"
  ]
  node [
    id 92
    label "kt&#243;ry&#347;"
  ]
  node [
    id 93
    label "osobno"
  ]
  node [
    id 94
    label "r&#243;&#380;ny"
  ]
  node [
    id 95
    label "inszy"
  ]
  node [
    id 96
    label "inaczej"
  ]
  node [
    id 97
    label "sk&#322;adowisko"
  ]
  node [
    id 98
    label "bomba_ekologiczna"
  ]
  node [
    id 99
    label "miejsce"
  ]
  node [
    id 100
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 101
    label "reszta"
  ]
  node [
    id 102
    label "trace"
  ]
  node [
    id 103
    label "obiekt"
  ]
  node [
    id 104
    label "&#347;wiadectwo"
  ]
  node [
    id 105
    label "starzy"
  ]
  node [
    id 106
    label "&#380;ona"
  ]
  node [
    id 107
    label "partnerka"
  ]
  node [
    id 108
    label "matka"
  ]
  node [
    id 109
    label "doros&#322;y"
  ]
  node [
    id 110
    label "samica"
  ]
  node [
    id 111
    label "uleganie"
  ]
  node [
    id 112
    label "m&#281;&#380;yna"
  ]
  node [
    id 113
    label "ulegni&#281;cie"
  ]
  node [
    id 114
    label "pa&#324;stwo"
  ]
  node [
    id 115
    label "&#322;ono"
  ]
  node [
    id 116
    label "menopauza"
  ]
  node [
    id 117
    label "przekwitanie"
  ]
  node [
    id 118
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 119
    label "babka"
  ]
  node [
    id 120
    label "ulega&#263;"
  ]
  node [
    id 121
    label "ma&#322;&#380;onek"
  ]
  node [
    id 122
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 123
    label "&#347;lubna"
  ]
  node [
    id 124
    label "kobita"
  ]
  node [
    id 125
    label "panna_m&#322;oda"
  ]
  node [
    id 126
    label "aktorka"
  ]
  node [
    id 127
    label "partner"
  ]
  node [
    id 128
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 129
    label "dwa_ognie"
  ]
  node [
    id 130
    label "gracz"
  ]
  node [
    id 131
    label "rozsadnik"
  ]
  node [
    id 132
    label "rodzice"
  ]
  node [
    id 133
    label "staruszka"
  ]
  node [
    id 134
    label "ro&#347;lina"
  ]
  node [
    id 135
    label "przyczyna"
  ]
  node [
    id 136
    label "macocha"
  ]
  node [
    id 137
    label "matka_zast&#281;pcza"
  ]
  node [
    id 138
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 139
    label "zawodnik"
  ]
  node [
    id 140
    label "matczysko"
  ]
  node [
    id 141
    label "macierz"
  ]
  node [
    id 142
    label "Matka_Boska"
  ]
  node [
    id 143
    label "przodkini"
  ]
  node [
    id 144
    label "zakonnica"
  ]
  node [
    id 145
    label "rodzic"
  ]
  node [
    id 146
    label "owad"
  ]
  node [
    id 147
    label "stary"
  ]
  node [
    id 148
    label "bie&#380;nik"
  ]
  node [
    id 149
    label "ko&#322;o"
  ]
  node [
    id 150
    label "meninx"
  ]
  node [
    id 151
    label "ogumienie"
  ]
  node [
    id 152
    label "wa&#322;ek"
  ]
  node [
    id 153
    label "b&#322;ona"
  ]
  node [
    id 154
    label "tkanka"
  ]
  node [
    id 155
    label "m&#243;zgoczaszka"
  ]
  node [
    id 156
    label "wytw&#243;r"
  ]
  node [
    id 157
    label "narz&#281;dzie"
  ]
  node [
    id 158
    label "przewa&#322;"
  ]
  node [
    id 159
    label "wy&#380;ymaczka"
  ]
  node [
    id 160
    label "chutzpa"
  ]
  node [
    id 161
    label "wa&#322;kowanie"
  ]
  node [
    id 162
    label "maszyna"
  ]
  node [
    id 163
    label "fa&#322;da"
  ]
  node [
    id 164
    label "p&#243;&#322;wa&#322;ek"
  ]
  node [
    id 165
    label "poduszka"
  ]
  node [
    id 166
    label "cylinder"
  ]
  node [
    id 167
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 168
    label "post&#281;pek"
  ]
  node [
    id 169
    label "walec"
  ]
  node [
    id 170
    label "blok"
  ]
  node [
    id 171
    label "chodnik"
  ]
  node [
    id 172
    label "lina"
  ]
  node [
    id 173
    label "serweta"
  ]
  node [
    id 174
    label "powierzchnia"
  ]
  node [
    id 175
    label "urz&#261;dzenie"
  ]
  node [
    id 176
    label "d&#281;tka"
  ]
  node [
    id 177
    label "przedmiot"
  ]
  node [
    id 178
    label "gang"
  ]
  node [
    id 179
    label "&#322;ama&#263;"
  ]
  node [
    id 180
    label "zabawa"
  ]
  node [
    id 181
    label "&#322;amanie"
  ]
  node [
    id 182
    label "obr&#281;cz"
  ]
  node [
    id 183
    label "piasta"
  ]
  node [
    id 184
    label "lap"
  ]
  node [
    id 185
    label "figura_geometryczna"
  ]
  node [
    id 186
    label "sphere"
  ]
  node [
    id 187
    label "grupa"
  ]
  node [
    id 188
    label "o&#347;"
  ]
  node [
    id 189
    label "kolokwium"
  ]
  node [
    id 190
    label "pi"
  ]
  node [
    id 191
    label "zwolnica"
  ]
  node [
    id 192
    label "p&#243;&#322;kole"
  ]
  node [
    id 193
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 194
    label "sejmik"
  ]
  node [
    id 195
    label "pojazd"
  ]
  node [
    id 196
    label "figura_ograniczona"
  ]
  node [
    id 197
    label "whip"
  ]
  node [
    id 198
    label "okr&#261;g"
  ]
  node [
    id 199
    label "odcinek_ko&#322;a"
  ]
  node [
    id 200
    label "stowarzyszenie"
  ]
  node [
    id 201
    label "podwozie"
  ]
  node [
    id 202
    label "rybnicki"
  ]
  node [
    id 203
    label "alarm"
  ]
  node [
    id 204
    label "smogowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 202
    target 203
  ]
  edge [
    source 202
    target 204
  ]
  edge [
    source 203
    target 204
  ]
]
