graph [
  node [
    id 0
    label "komisja"
    origin "text"
  ]
  node [
    id 1
    label "europejski"
    origin "text"
  ]
  node [
    id 2
    label "obrady"
  ]
  node [
    id 3
    label "podkomisja"
  ]
  node [
    id 4
    label "Komisja_Europejska"
  ]
  node [
    id 5
    label "zesp&#243;&#322;"
  ]
  node [
    id 6
    label "organ"
  ]
  node [
    id 7
    label "konsylium"
  ]
  node [
    id 8
    label "conference"
  ]
  node [
    id 9
    label "dyskusja"
  ]
  node [
    id 10
    label "group"
  ]
  node [
    id 11
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 12
    label "zbi&#243;r"
  ]
  node [
    id 13
    label "The_Beatles"
  ]
  node [
    id 14
    label "odm&#322;odzenie"
  ]
  node [
    id 15
    label "grupa"
  ]
  node [
    id 16
    label "ro&#347;lina"
  ]
  node [
    id 17
    label "odm&#322;adzanie"
  ]
  node [
    id 18
    label "Depeche_Mode"
  ]
  node [
    id 19
    label "odm&#322;adza&#263;"
  ]
  node [
    id 20
    label "&#346;wietliki"
  ]
  node [
    id 21
    label "zespolik"
  ]
  node [
    id 22
    label "whole"
  ]
  node [
    id 23
    label "Mazowsze"
  ]
  node [
    id 24
    label "schorzenie"
  ]
  node [
    id 25
    label "skupienie"
  ]
  node [
    id 26
    label "batch"
  ]
  node [
    id 27
    label "zabudowania"
  ]
  node [
    id 28
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 29
    label "uk&#322;ad"
  ]
  node [
    id 30
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 31
    label "Komitet_Region&#243;w"
  ]
  node [
    id 32
    label "struktura_anatomiczna"
  ]
  node [
    id 33
    label "organogeneza"
  ]
  node [
    id 34
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 35
    label "tw&#243;r"
  ]
  node [
    id 36
    label "tkanka"
  ]
  node [
    id 37
    label "stomia"
  ]
  node [
    id 38
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 39
    label "budowa"
  ]
  node [
    id 40
    label "dekortykacja"
  ]
  node [
    id 41
    label "okolica"
  ]
  node [
    id 42
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 43
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 44
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 45
    label "Izba_Konsyliarska"
  ]
  node [
    id 46
    label "jednostka_organizacyjna"
  ]
  node [
    id 47
    label "subcommittee"
  ]
  node [
    id 48
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 49
    label "europejsko"
  ]
  node [
    id 50
    label "typowy"
  ]
  node [
    id 51
    label "charakterystyczny"
  ]
  node [
    id 52
    label "po_europejsku"
  ]
  node [
    id 53
    label "European"
  ]
  node [
    id 54
    label "wyj&#261;tkowy"
  ]
  node [
    id 55
    label "podobny"
  ]
  node [
    id 56
    label "charakterystycznie"
  ]
  node [
    id 57
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 58
    label "szczeg&#243;lny"
  ]
  node [
    id 59
    label "typowo"
  ]
  node [
    id 60
    label "zwyk&#322;y"
  ]
  node [
    id 61
    label "zwyczajny"
  ]
  node [
    id 62
    label "cz&#281;sty"
  ]
  node [
    id 63
    label "taki"
  ]
  node [
    id 64
    label "stosownie"
  ]
  node [
    id 65
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 66
    label "prawdziwy"
  ]
  node [
    id 67
    label "zasadniczy"
  ]
  node [
    id 68
    label "uprawniony"
  ]
  node [
    id 69
    label "nale&#380;yty"
  ]
  node [
    id 70
    label "ten"
  ]
  node [
    id 71
    label "dobry"
  ]
  node [
    id 72
    label "nale&#380;ny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
]
