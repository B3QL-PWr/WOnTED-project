graph [
  node [
    id 0
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 2
    label "oferowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "darczy&#324;ca"
    origin "text"
  ]
  node [
    id 4
    label "wskazanie"
    origin "text"
  ]
  node [
    id 5
    label "sprawca"
    origin "text"
  ]
  node [
    id 6
    label "bestialski"
    origin "text"
  ]
  node [
    id 7
    label "powiesi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pies"
    origin "text"
  ]
  node [
    id 9
    label "jaki"
    origin "text"
  ]
  node [
    id 10
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "przed"
    origin "text"
  ]
  node [
    id 12
    label "kilka"
    origin "text"
  ]
  node [
    id 13
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 14
    label "teren"
    origin "text"
  ]
  node [
    id 15
    label "wa&#322;brzych"
    origin "text"
  ]
  node [
    id 16
    label "nieopodal"
    origin "text"
  ]
  node [
    id 17
    label "schronisko"
    origin "text"
  ]
  node [
    id 18
    label "licytacja"
  ]
  node [
    id 19
    label "kwota"
  ]
  node [
    id 20
    label "liczba"
  ]
  node [
    id 21
    label "molarity"
  ]
  node [
    id 22
    label "tauzen"
  ]
  node [
    id 23
    label "gra_w_karty"
  ]
  node [
    id 24
    label "patyk"
  ]
  node [
    id 25
    label "musik"
  ]
  node [
    id 26
    label "wynie&#347;&#263;"
  ]
  node [
    id 27
    label "pieni&#261;dze"
  ]
  node [
    id 28
    label "ilo&#347;&#263;"
  ]
  node [
    id 29
    label "limit"
  ]
  node [
    id 30
    label "wynosi&#263;"
  ]
  node [
    id 31
    label "kategoria"
  ]
  node [
    id 32
    label "pierwiastek"
  ]
  node [
    id 33
    label "rozmiar"
  ]
  node [
    id 34
    label "wyra&#380;enie"
  ]
  node [
    id 35
    label "poj&#281;cie"
  ]
  node [
    id 36
    label "number"
  ]
  node [
    id 37
    label "cecha"
  ]
  node [
    id 38
    label "kategoria_gramatyczna"
  ]
  node [
    id 39
    label "grupa"
  ]
  node [
    id 40
    label "kwadrat_magiczny"
  ]
  node [
    id 41
    label "koniugacja"
  ]
  node [
    id 42
    label "przymus"
  ]
  node [
    id 43
    label "przetarg"
  ]
  node [
    id 44
    label "rozdanie"
  ]
  node [
    id 45
    label "faza"
  ]
  node [
    id 46
    label "pas"
  ]
  node [
    id 47
    label "sprzeda&#380;"
  ]
  node [
    id 48
    label "bryd&#380;"
  ]
  node [
    id 49
    label "skat"
  ]
  node [
    id 50
    label "kij"
  ]
  node [
    id 51
    label "obiekt_naturalny"
  ]
  node [
    id 52
    label "pr&#281;t"
  ]
  node [
    id 53
    label "chudzielec"
  ]
  node [
    id 54
    label "rod"
  ]
  node [
    id 55
    label "jednostka_monetarna"
  ]
  node [
    id 56
    label "wspania&#322;y"
  ]
  node [
    id 57
    label "metaliczny"
  ]
  node [
    id 58
    label "Polska"
  ]
  node [
    id 59
    label "szlachetny"
  ]
  node [
    id 60
    label "kochany"
  ]
  node [
    id 61
    label "doskona&#322;y"
  ]
  node [
    id 62
    label "grosz"
  ]
  node [
    id 63
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 64
    label "poz&#322;ocenie"
  ]
  node [
    id 65
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 66
    label "utytu&#322;owany"
  ]
  node [
    id 67
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 68
    label "z&#322;ocenie"
  ]
  node [
    id 69
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 70
    label "prominentny"
  ]
  node [
    id 71
    label "znany"
  ]
  node [
    id 72
    label "wybitny"
  ]
  node [
    id 73
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 74
    label "naj"
  ]
  node [
    id 75
    label "&#347;wietny"
  ]
  node [
    id 76
    label "pe&#322;ny"
  ]
  node [
    id 77
    label "doskonale"
  ]
  node [
    id 78
    label "szlachetnie"
  ]
  node [
    id 79
    label "uczciwy"
  ]
  node [
    id 80
    label "zacny"
  ]
  node [
    id 81
    label "harmonijny"
  ]
  node [
    id 82
    label "gatunkowy"
  ]
  node [
    id 83
    label "pi&#281;kny"
  ]
  node [
    id 84
    label "dobry"
  ]
  node [
    id 85
    label "typowy"
  ]
  node [
    id 86
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 87
    label "metaloplastyczny"
  ]
  node [
    id 88
    label "metalicznie"
  ]
  node [
    id 89
    label "kochanek"
  ]
  node [
    id 90
    label "wybranek"
  ]
  node [
    id 91
    label "umi&#322;owany"
  ]
  node [
    id 92
    label "drogi"
  ]
  node [
    id 93
    label "kochanie"
  ]
  node [
    id 94
    label "wspaniale"
  ]
  node [
    id 95
    label "pomy&#347;lny"
  ]
  node [
    id 96
    label "pozytywny"
  ]
  node [
    id 97
    label "&#347;wietnie"
  ]
  node [
    id 98
    label "spania&#322;y"
  ]
  node [
    id 99
    label "och&#281;do&#380;ny"
  ]
  node [
    id 100
    label "warto&#347;ciowy"
  ]
  node [
    id 101
    label "zajebisty"
  ]
  node [
    id 102
    label "bogato"
  ]
  node [
    id 103
    label "typ_mongoloidalny"
  ]
  node [
    id 104
    label "kolorowy"
  ]
  node [
    id 105
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 106
    label "ciep&#322;y"
  ]
  node [
    id 107
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 108
    label "jasny"
  ]
  node [
    id 109
    label "groszak"
  ]
  node [
    id 110
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 111
    label "szyling_austryjacki"
  ]
  node [
    id 112
    label "moneta"
  ]
  node [
    id 113
    label "Mazowsze"
  ]
  node [
    id 114
    label "Pa&#322;uki"
  ]
  node [
    id 115
    label "Pomorze_Zachodnie"
  ]
  node [
    id 116
    label "Powi&#347;le"
  ]
  node [
    id 117
    label "Wolin"
  ]
  node [
    id 118
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 119
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 120
    label "So&#322;a"
  ]
  node [
    id 121
    label "Unia_Europejska"
  ]
  node [
    id 122
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 123
    label "Opolskie"
  ]
  node [
    id 124
    label "Suwalszczyzna"
  ]
  node [
    id 125
    label "Krajna"
  ]
  node [
    id 126
    label "barwy_polskie"
  ]
  node [
    id 127
    label "Nadbu&#380;e"
  ]
  node [
    id 128
    label "Podlasie"
  ]
  node [
    id 129
    label "Izera"
  ]
  node [
    id 130
    label "Ma&#322;opolska"
  ]
  node [
    id 131
    label "Warmia"
  ]
  node [
    id 132
    label "Mazury"
  ]
  node [
    id 133
    label "NATO"
  ]
  node [
    id 134
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 135
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 136
    label "Lubelszczyzna"
  ]
  node [
    id 137
    label "Kaczawa"
  ]
  node [
    id 138
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 139
    label "Kielecczyzna"
  ]
  node [
    id 140
    label "Lubuskie"
  ]
  node [
    id 141
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 142
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 143
    label "&#321;&#243;dzkie"
  ]
  node [
    id 144
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 145
    label "Kujawy"
  ]
  node [
    id 146
    label "Podkarpacie"
  ]
  node [
    id 147
    label "Wielkopolska"
  ]
  node [
    id 148
    label "Wis&#322;a"
  ]
  node [
    id 149
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 150
    label "Bory_Tucholskie"
  ]
  node [
    id 151
    label "platerowanie"
  ]
  node [
    id 152
    label "z&#322;ocisty"
  ]
  node [
    id 153
    label "barwienie"
  ]
  node [
    id 154
    label "gilt"
  ]
  node [
    id 155
    label "plating"
  ]
  node [
    id 156
    label "zdobienie"
  ]
  node [
    id 157
    label "club"
  ]
  node [
    id 158
    label "powleczenie"
  ]
  node [
    id 159
    label "zabarwienie"
  ]
  node [
    id 160
    label "zach&#281;ca&#263;"
  ]
  node [
    id 161
    label "volunteer"
  ]
  node [
    id 162
    label "pozyskiwa&#263;"
  ]
  node [
    id 163
    label "act"
  ]
  node [
    id 164
    label "cz&#322;owiek"
  ]
  node [
    id 165
    label "ludzko&#347;&#263;"
  ]
  node [
    id 166
    label "asymilowanie"
  ]
  node [
    id 167
    label "wapniak"
  ]
  node [
    id 168
    label "asymilowa&#263;"
  ]
  node [
    id 169
    label "os&#322;abia&#263;"
  ]
  node [
    id 170
    label "posta&#263;"
  ]
  node [
    id 171
    label "hominid"
  ]
  node [
    id 172
    label "podw&#322;adny"
  ]
  node [
    id 173
    label "os&#322;abianie"
  ]
  node [
    id 174
    label "g&#322;owa"
  ]
  node [
    id 175
    label "figura"
  ]
  node [
    id 176
    label "portrecista"
  ]
  node [
    id 177
    label "dwun&#243;g"
  ]
  node [
    id 178
    label "profanum"
  ]
  node [
    id 179
    label "mikrokosmos"
  ]
  node [
    id 180
    label "nasada"
  ]
  node [
    id 181
    label "duch"
  ]
  node [
    id 182
    label "antropochoria"
  ]
  node [
    id 183
    label "osoba"
  ]
  node [
    id 184
    label "wz&#243;r"
  ]
  node [
    id 185
    label "senior"
  ]
  node [
    id 186
    label "oddzia&#322;ywanie"
  ]
  node [
    id 187
    label "Adam"
  ]
  node [
    id 188
    label "homo_sapiens"
  ]
  node [
    id 189
    label "polifag"
  ]
  node [
    id 190
    label "podanie"
  ]
  node [
    id 191
    label "pokazanie"
  ]
  node [
    id 192
    label "wskaz&#243;wka"
  ]
  node [
    id 193
    label "wyja&#347;nienie"
  ]
  node [
    id 194
    label "appointment"
  ]
  node [
    id 195
    label "meaning"
  ]
  node [
    id 196
    label "education"
  ]
  node [
    id 197
    label "wybranie"
  ]
  node [
    id 198
    label "podkre&#347;lenie"
  ]
  node [
    id 199
    label "przyczyna"
  ]
  node [
    id 200
    label "wynik"
  ]
  node [
    id 201
    label "zaokr&#261;glenie"
  ]
  node [
    id 202
    label "dzia&#322;anie"
  ]
  node [
    id 203
    label "typ"
  ]
  node [
    id 204
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 205
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 206
    label "rezultat"
  ]
  node [
    id 207
    label "event"
  ]
  node [
    id 208
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 209
    label "subject"
  ]
  node [
    id 210
    label "czynnik"
  ]
  node [
    id 211
    label "matuszka"
  ]
  node [
    id 212
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 213
    label "geneza"
  ]
  node [
    id 214
    label "poci&#261;ganie"
  ]
  node [
    id 215
    label "ustawienie"
  ]
  node [
    id 216
    label "danie"
  ]
  node [
    id 217
    label "narrative"
  ]
  node [
    id 218
    label "pismo"
  ]
  node [
    id 219
    label "nafaszerowanie"
  ]
  node [
    id 220
    label "tenis"
  ]
  node [
    id 221
    label "prayer"
  ]
  node [
    id 222
    label "siatk&#243;wka"
  ]
  node [
    id 223
    label "pi&#322;ka"
  ]
  node [
    id 224
    label "give"
  ]
  node [
    id 225
    label "myth"
  ]
  node [
    id 226
    label "service"
  ]
  node [
    id 227
    label "jedzenie"
  ]
  node [
    id 228
    label "zagranie"
  ]
  node [
    id 229
    label "poinformowanie"
  ]
  node [
    id 230
    label "zaserwowanie"
  ]
  node [
    id 231
    label "opowie&#347;&#263;"
  ]
  node [
    id 232
    label "pass"
  ]
  node [
    id 233
    label "udowodnienie"
  ]
  node [
    id 234
    label "exhibit"
  ]
  node [
    id 235
    label "obniesienie"
  ]
  node [
    id 236
    label "przedstawienie"
  ]
  node [
    id 237
    label "spowodowanie"
  ]
  node [
    id 238
    label "nauczenie"
  ]
  node [
    id 239
    label "wczytanie"
  ]
  node [
    id 240
    label "zrobienie"
  ]
  node [
    id 241
    label "zu&#380;ycie"
  ]
  node [
    id 242
    label "powo&#322;anie"
  ]
  node [
    id 243
    label "powybieranie"
  ]
  node [
    id 244
    label "ustalenie"
  ]
  node [
    id 245
    label "sie&#263;_rybacka"
  ]
  node [
    id 246
    label "wyj&#281;cie"
  ]
  node [
    id 247
    label "optowanie"
  ]
  node [
    id 248
    label "kotwica"
  ]
  node [
    id 249
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 250
    label "linia"
  ]
  node [
    id 251
    label "kreska"
  ]
  node [
    id 252
    label "stress"
  ]
  node [
    id 253
    label "accent"
  ]
  node [
    id 254
    label "narysowanie"
  ]
  node [
    id 255
    label "uzasadnienie"
  ]
  node [
    id 256
    label "fakt"
  ]
  node [
    id 257
    label "tarcza"
  ]
  node [
    id 258
    label "zegar"
  ]
  node [
    id 259
    label "solucja"
  ]
  node [
    id 260
    label "informacja"
  ]
  node [
    id 261
    label "implikowa&#263;"
  ]
  node [
    id 262
    label "explanation"
  ]
  node [
    id 263
    label "report"
  ]
  node [
    id 264
    label "zrozumia&#322;y"
  ]
  node [
    id 265
    label "apologetyk"
  ]
  node [
    id 266
    label "justyfikacja"
  ]
  node [
    id 267
    label "gossip"
  ]
  node [
    id 268
    label "sprawiciel"
  ]
  node [
    id 269
    label "zwierz&#281;cy"
  ]
  node [
    id 270
    label "bestialsko"
  ]
  node [
    id 271
    label "okrutny"
  ]
  node [
    id 272
    label "z&#322;y"
  ]
  node [
    id 273
    label "nieludzki"
  ]
  node [
    id 274
    label "straszny"
  ]
  node [
    id 275
    label "mocny"
  ]
  node [
    id 276
    label "pod&#322;y"
  ]
  node [
    id 277
    label "bezlito&#347;ny"
  ]
  node [
    id 278
    label "gro&#378;ny"
  ]
  node [
    id 279
    label "okrutnie"
  ]
  node [
    id 280
    label "zwierz&#281;co"
  ]
  node [
    id 281
    label "wzorzysty"
  ]
  node [
    id 282
    label "organiczny"
  ]
  node [
    id 283
    label "pierwotny"
  ]
  node [
    id 284
    label "aintelektualny"
  ]
  node [
    id 285
    label "pieski"
  ]
  node [
    id 286
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 287
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 288
    label "niekorzystny"
  ]
  node [
    id 289
    label "z&#322;oszczenie"
  ]
  node [
    id 290
    label "sierdzisty"
  ]
  node [
    id 291
    label "niegrzeczny"
  ]
  node [
    id 292
    label "zez&#322;oszczenie"
  ]
  node [
    id 293
    label "zdenerwowany"
  ]
  node [
    id 294
    label "negatywny"
  ]
  node [
    id 295
    label "rozgniewanie"
  ]
  node [
    id 296
    label "gniewanie"
  ]
  node [
    id 297
    label "niemoralny"
  ]
  node [
    id 298
    label "&#378;le"
  ]
  node [
    id 299
    label "niepomy&#347;lny"
  ]
  node [
    id 300
    label "syf"
  ]
  node [
    id 301
    label "beastly"
  ]
  node [
    id 302
    label "suspend"
  ]
  node [
    id 303
    label "zabi&#263;"
  ]
  node [
    id 304
    label "bent"
  ]
  node [
    id 305
    label "szubienica"
  ]
  node [
    id 306
    label "umie&#347;ci&#263;"
  ]
  node [
    id 307
    label "stryczek"
  ]
  node [
    id 308
    label "set"
  ]
  node [
    id 309
    label "put"
  ]
  node [
    id 310
    label "uplasowa&#263;"
  ]
  node [
    id 311
    label "wpierniczy&#263;"
  ]
  node [
    id 312
    label "okre&#347;li&#263;"
  ]
  node [
    id 313
    label "zrobi&#263;"
  ]
  node [
    id 314
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 315
    label "zmieni&#263;"
  ]
  node [
    id 316
    label "umieszcza&#263;"
  ]
  node [
    id 317
    label "zadzwoni&#263;"
  ]
  node [
    id 318
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 319
    label "skarci&#263;"
  ]
  node [
    id 320
    label "skrzywdzi&#263;"
  ]
  node [
    id 321
    label "os&#322;oni&#263;"
  ]
  node [
    id 322
    label "przybi&#263;"
  ]
  node [
    id 323
    label "rozbroi&#263;"
  ]
  node [
    id 324
    label "uderzy&#263;"
  ]
  node [
    id 325
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 326
    label "skrzywi&#263;"
  ]
  node [
    id 327
    label "dispatch"
  ]
  node [
    id 328
    label "zmordowa&#263;"
  ]
  node [
    id 329
    label "zakry&#263;"
  ]
  node [
    id 330
    label "zbi&#263;"
  ]
  node [
    id 331
    label "zapulsowa&#263;"
  ]
  node [
    id 332
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 333
    label "break"
  ]
  node [
    id 334
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 335
    label "zastrzeli&#263;"
  ]
  node [
    id 336
    label "u&#347;mierci&#263;"
  ]
  node [
    id 337
    label "zwalczy&#263;"
  ]
  node [
    id 338
    label "pomacha&#263;"
  ]
  node [
    id 339
    label "kill"
  ]
  node [
    id 340
    label "zako&#324;czy&#263;"
  ]
  node [
    id 341
    label "zniszczy&#263;"
  ]
  node [
    id 342
    label "powieszenie"
  ]
  node [
    id 343
    label "pohybel"
  ]
  node [
    id 344
    label "wieszanie"
  ]
  node [
    id 345
    label "wiesza&#263;"
  ]
  node [
    id 346
    label "narz&#281;dzie_&#347;mierci"
  ]
  node [
    id 347
    label "hangman's_rope"
  ]
  node [
    id 348
    label "kara_&#347;mierci"
  ]
  node [
    id 349
    label "suspension"
  ]
  node [
    id 350
    label "sznur"
  ]
  node [
    id 351
    label "piese&#322;"
  ]
  node [
    id 352
    label "Cerber"
  ]
  node [
    id 353
    label "szczeka&#263;"
  ]
  node [
    id 354
    label "&#322;ajdak"
  ]
  node [
    id 355
    label "kabanos"
  ]
  node [
    id 356
    label "wyzwisko"
  ]
  node [
    id 357
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 358
    label "samiec"
  ]
  node [
    id 359
    label "spragniony"
  ]
  node [
    id 360
    label "policjant"
  ]
  node [
    id 361
    label "rakarz"
  ]
  node [
    id 362
    label "szczu&#263;"
  ]
  node [
    id 363
    label "wycie"
  ]
  node [
    id 364
    label "istota_&#380;ywa"
  ]
  node [
    id 365
    label "trufla"
  ]
  node [
    id 366
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 367
    label "zawy&#263;"
  ]
  node [
    id 368
    label "sobaka"
  ]
  node [
    id 369
    label "dogoterapia"
  ]
  node [
    id 370
    label "s&#322;u&#380;enie"
  ]
  node [
    id 371
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 372
    label "psowate"
  ]
  node [
    id 373
    label "wy&#263;"
  ]
  node [
    id 374
    label "szczucie"
  ]
  node [
    id 375
    label "czworon&#243;g"
  ]
  node [
    id 376
    label "sympatyk"
  ]
  node [
    id 377
    label "entuzjasta"
  ]
  node [
    id 378
    label "critter"
  ]
  node [
    id 379
    label "zwierz&#281;_domowe"
  ]
  node [
    id 380
    label "kr&#281;gowiec"
  ]
  node [
    id 381
    label "tetrapody"
  ]
  node [
    id 382
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 383
    label "zwierz&#281;"
  ]
  node [
    id 384
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 385
    label "palconogie"
  ]
  node [
    id 386
    label "stra&#380;nik"
  ]
  node [
    id 387
    label "wielog&#322;owy"
  ]
  node [
    id 388
    label "zooterapia"
  ]
  node [
    id 389
    label "przek&#261;ska"
  ]
  node [
    id 390
    label "w&#281;dzi&#263;"
  ]
  node [
    id 391
    label "przysmak"
  ]
  node [
    id 392
    label "kie&#322;basa"
  ]
  node [
    id 393
    label "cygaro"
  ]
  node [
    id 394
    label "kot"
  ]
  node [
    id 395
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 396
    label "bark"
  ]
  node [
    id 397
    label "m&#243;wi&#263;"
  ]
  node [
    id 398
    label "hum"
  ]
  node [
    id 399
    label "obgadywa&#263;"
  ]
  node [
    id 400
    label "kozio&#322;"
  ]
  node [
    id 401
    label "szcz&#281;ka&#263;"
  ]
  node [
    id 402
    label "karabin"
  ]
  node [
    id 403
    label "wymy&#347;la&#263;"
  ]
  node [
    id 404
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 405
    label "wydoby&#263;"
  ]
  node [
    id 406
    label "wilk"
  ]
  node [
    id 407
    label "rant"
  ]
  node [
    id 408
    label "rave"
  ]
  node [
    id 409
    label "zabrzmie&#263;"
  ]
  node [
    id 410
    label "&#380;o&#322;nierz"
  ]
  node [
    id 411
    label "robi&#263;"
  ]
  node [
    id 412
    label "by&#263;"
  ]
  node [
    id 413
    label "trwa&#263;"
  ]
  node [
    id 414
    label "use"
  ]
  node [
    id 415
    label "suffice"
  ]
  node [
    id 416
    label "cel"
  ]
  node [
    id 417
    label "pracowa&#263;"
  ]
  node [
    id 418
    label "match"
  ]
  node [
    id 419
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 420
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 421
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 422
    label "wait"
  ]
  node [
    id 423
    label "pomaga&#263;"
  ]
  node [
    id 424
    label "tease"
  ]
  node [
    id 425
    label "pod&#380;ega&#263;"
  ]
  node [
    id 426
    label "podjudza&#263;"
  ]
  node [
    id 427
    label "pracownik_komunalny"
  ]
  node [
    id 428
    label "powodowanie"
  ]
  node [
    id 429
    label "pod&#380;eganie"
  ]
  node [
    id 430
    label "atakowanie"
  ]
  node [
    id 431
    label "fomentation"
  ]
  node [
    id 432
    label "uczynny"
  ]
  node [
    id 433
    label "s&#322;ugiwanie"
  ]
  node [
    id 434
    label "pomaganie"
  ]
  node [
    id 435
    label "bycie"
  ]
  node [
    id 436
    label "wys&#322;u&#380;enie_si&#281;"
  ]
  node [
    id 437
    label "request"
  ]
  node [
    id 438
    label "trwanie"
  ]
  node [
    id 439
    label "robienie"
  ]
  node [
    id 440
    label "przydawanie_si&#281;"
  ]
  node [
    id 441
    label "czynno&#347;&#263;"
  ]
  node [
    id 442
    label "pracowanie"
  ]
  node [
    id 443
    label "czekoladka"
  ]
  node [
    id 444
    label "afrodyzjak"
  ]
  node [
    id 445
    label "workowiec"
  ]
  node [
    id 446
    label "nos"
  ]
  node [
    id 447
    label "grzyb_owocnikowy"
  ]
  node [
    id 448
    label "truflowate"
  ]
  node [
    id 449
    label "grzyb_mikoryzowy"
  ]
  node [
    id 450
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 451
    label "wo&#322;anie"
  ]
  node [
    id 452
    label "wydobywanie"
  ]
  node [
    id 453
    label "brzmienie"
  ]
  node [
    id 454
    label "wydawanie"
  ]
  node [
    id 455
    label "d&#378;wi&#281;k"
  ]
  node [
    id 456
    label "whimper"
  ]
  node [
    id 457
    label "yip"
  ]
  node [
    id 458
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 459
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 460
    label "p&#322;aka&#263;"
  ]
  node [
    id 461
    label "snivel"
  ]
  node [
    id 462
    label "cholera"
  ]
  node [
    id 463
    label "wypowied&#378;"
  ]
  node [
    id 464
    label "chuj"
  ]
  node [
    id 465
    label "bluzg"
  ]
  node [
    id 466
    label "chujowy"
  ]
  node [
    id 467
    label "obelga"
  ]
  node [
    id 468
    label "szmata"
  ]
  node [
    id 469
    label "ch&#281;tny"
  ]
  node [
    id 470
    label "z&#322;akniony"
  ]
  node [
    id 471
    label "upodlenie_si&#281;"
  ]
  node [
    id 472
    label "skurwysyn"
  ]
  node [
    id 473
    label "upadlanie_si&#281;"
  ]
  node [
    id 474
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 475
    label "psubrat"
  ]
  node [
    id 476
    label "policja"
  ]
  node [
    id 477
    label "blacharz"
  ]
  node [
    id 478
    label "str&#243;&#380;"
  ]
  node [
    id 479
    label "pa&#322;a"
  ]
  node [
    id 480
    label "mundurowy"
  ]
  node [
    id 481
    label "glina"
  ]
  node [
    id 482
    label "sta&#263;_si&#281;"
  ]
  node [
    id 483
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 484
    label "supervene"
  ]
  node [
    id 485
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 486
    label "zaj&#347;&#263;"
  ]
  node [
    id 487
    label "catch"
  ]
  node [
    id 488
    label "get"
  ]
  node [
    id 489
    label "bodziec"
  ]
  node [
    id 490
    label "przesy&#322;ka"
  ]
  node [
    id 491
    label "dodatek"
  ]
  node [
    id 492
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 493
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 494
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 495
    label "heed"
  ]
  node [
    id 496
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 497
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 498
    label "spowodowa&#263;"
  ]
  node [
    id 499
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 500
    label "dozna&#263;"
  ]
  node [
    id 501
    label "dokoptowa&#263;"
  ]
  node [
    id 502
    label "postrzega&#263;"
  ]
  node [
    id 503
    label "orgazm"
  ]
  node [
    id 504
    label "dolecie&#263;"
  ]
  node [
    id 505
    label "drive"
  ]
  node [
    id 506
    label "dotrze&#263;"
  ]
  node [
    id 507
    label "uzyska&#263;"
  ]
  node [
    id 508
    label "dop&#322;ata"
  ]
  node [
    id 509
    label "become"
  ]
  node [
    id 510
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 511
    label "spotka&#263;"
  ]
  node [
    id 512
    label "range"
  ]
  node [
    id 513
    label "fall_upon"
  ]
  node [
    id 514
    label "profit"
  ]
  node [
    id 515
    label "score"
  ]
  node [
    id 516
    label "make"
  ]
  node [
    id 517
    label "utrze&#263;"
  ]
  node [
    id 518
    label "znale&#378;&#263;"
  ]
  node [
    id 519
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 520
    label "silnik"
  ]
  node [
    id 521
    label "dopasowa&#263;"
  ]
  node [
    id 522
    label "advance"
  ]
  node [
    id 523
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 524
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 525
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 526
    label "dorobi&#263;"
  ]
  node [
    id 527
    label "realize"
  ]
  node [
    id 528
    label "promocja"
  ]
  node [
    id 529
    label "wytworzy&#263;"
  ]
  node [
    id 530
    label "give_birth"
  ]
  node [
    id 531
    label "feel"
  ]
  node [
    id 532
    label "punkt"
  ]
  node [
    id 533
    label "publikacja"
  ]
  node [
    id 534
    label "wiedza"
  ]
  node [
    id 535
    label "obiega&#263;"
  ]
  node [
    id 536
    label "powzi&#281;cie"
  ]
  node [
    id 537
    label "dane"
  ]
  node [
    id 538
    label "obiegni&#281;cie"
  ]
  node [
    id 539
    label "sygna&#322;"
  ]
  node [
    id 540
    label "obieganie"
  ]
  node [
    id 541
    label "powzi&#261;&#263;"
  ]
  node [
    id 542
    label "obiec"
  ]
  node [
    id 543
    label "doj&#347;cie"
  ]
  node [
    id 544
    label "pobudka"
  ]
  node [
    id 545
    label "ankus"
  ]
  node [
    id 546
    label "przedmiot"
  ]
  node [
    id 547
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 548
    label "przewodzi&#263;"
  ]
  node [
    id 549
    label "zach&#281;ta"
  ]
  node [
    id 550
    label "drift"
  ]
  node [
    id 551
    label "przewodzenie"
  ]
  node [
    id 552
    label "o&#347;cie&#324;"
  ]
  node [
    id 553
    label "perceive"
  ]
  node [
    id 554
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 555
    label "punkt_widzenia"
  ]
  node [
    id 556
    label "obacza&#263;"
  ]
  node [
    id 557
    label "widzie&#263;"
  ]
  node [
    id 558
    label "dochodzi&#263;"
  ]
  node [
    id 559
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 560
    label "notice"
  ]
  node [
    id 561
    label "os&#261;dza&#263;"
  ]
  node [
    id 562
    label "doch&#243;d"
  ]
  node [
    id 563
    label "dochodzenie"
  ]
  node [
    id 564
    label "dziennik"
  ]
  node [
    id 565
    label "element"
  ]
  node [
    id 566
    label "rzecz"
  ]
  node [
    id 567
    label "galanteria"
  ]
  node [
    id 568
    label "aneks"
  ]
  node [
    id 569
    label "akt_p&#322;ciowy"
  ]
  node [
    id 570
    label "posy&#322;ka"
  ]
  node [
    id 571
    label "nadawca"
  ]
  node [
    id 572
    label "adres"
  ]
  node [
    id 573
    label "jell"
  ]
  node [
    id 574
    label "przys&#322;oni&#263;"
  ]
  node [
    id 575
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 576
    label "zaistnie&#263;"
  ]
  node [
    id 577
    label "surprise"
  ]
  node [
    id 578
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 579
    label "ukry&#263;_si&#281;"
  ]
  node [
    id 580
    label "podej&#347;&#263;"
  ]
  node [
    id 581
    label "wpa&#347;&#263;"
  ]
  node [
    id 582
    label "pokry&#263;_si&#281;"
  ]
  node [
    id 583
    label "wsp&#243;&#322;wyst&#261;pi&#263;"
  ]
  node [
    id 584
    label "przesta&#263;"
  ]
  node [
    id 585
    label "dokooptowa&#263;"
  ]
  node [
    id 586
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 587
    label "flow"
  ]
  node [
    id 588
    label "przyp&#322;yn&#261;&#263;"
  ]
  node [
    id 589
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 590
    label "swimming"
  ]
  node [
    id 591
    label "ryba"
  ]
  node [
    id 592
    label "&#347;ledziowate"
  ]
  node [
    id 593
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 594
    label "systemik"
  ]
  node [
    id 595
    label "doniczkowiec"
  ]
  node [
    id 596
    label "mi&#281;so"
  ]
  node [
    id 597
    label "system"
  ]
  node [
    id 598
    label "patroszy&#263;"
  ]
  node [
    id 599
    label "rakowato&#347;&#263;"
  ]
  node [
    id 600
    label "w&#281;dkarstwo"
  ]
  node [
    id 601
    label "ryby"
  ]
  node [
    id 602
    label "fish"
  ]
  node [
    id 603
    label "linia_boczna"
  ]
  node [
    id 604
    label "tar&#322;o"
  ]
  node [
    id 605
    label "wyrostek_filtracyjny"
  ]
  node [
    id 606
    label "m&#281;tnooki"
  ]
  node [
    id 607
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 608
    label "pokrywa_skrzelowa"
  ]
  node [
    id 609
    label "ikra"
  ]
  node [
    id 610
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 611
    label "szczelina_skrzelowa"
  ]
  node [
    id 612
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 613
    label "ranek"
  ]
  node [
    id 614
    label "doba"
  ]
  node [
    id 615
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 616
    label "noc"
  ]
  node [
    id 617
    label "podwiecz&#243;r"
  ]
  node [
    id 618
    label "po&#322;udnie"
  ]
  node [
    id 619
    label "godzina"
  ]
  node [
    id 620
    label "przedpo&#322;udnie"
  ]
  node [
    id 621
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 622
    label "long_time"
  ]
  node [
    id 623
    label "wiecz&#243;r"
  ]
  node [
    id 624
    label "t&#322;usty_czwartek"
  ]
  node [
    id 625
    label "popo&#322;udnie"
  ]
  node [
    id 626
    label "walentynki"
  ]
  node [
    id 627
    label "czynienie_si&#281;"
  ]
  node [
    id 628
    label "s&#322;o&#324;ce"
  ]
  node [
    id 629
    label "rano"
  ]
  node [
    id 630
    label "tydzie&#324;"
  ]
  node [
    id 631
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 632
    label "wzej&#347;cie"
  ]
  node [
    id 633
    label "czas"
  ]
  node [
    id 634
    label "wsta&#263;"
  ]
  node [
    id 635
    label "day"
  ]
  node [
    id 636
    label "termin"
  ]
  node [
    id 637
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 638
    label "wstanie"
  ]
  node [
    id 639
    label "przedwiecz&#243;r"
  ]
  node [
    id 640
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 641
    label "Sylwester"
  ]
  node [
    id 642
    label "poprzedzanie"
  ]
  node [
    id 643
    label "czasoprzestrze&#324;"
  ]
  node [
    id 644
    label "laba"
  ]
  node [
    id 645
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 646
    label "chronometria"
  ]
  node [
    id 647
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 648
    label "rachuba_czasu"
  ]
  node [
    id 649
    label "przep&#322;ywanie"
  ]
  node [
    id 650
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 651
    label "czasokres"
  ]
  node [
    id 652
    label "odczyt"
  ]
  node [
    id 653
    label "chwila"
  ]
  node [
    id 654
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 655
    label "dzieje"
  ]
  node [
    id 656
    label "poprzedzenie"
  ]
  node [
    id 657
    label "trawienie"
  ]
  node [
    id 658
    label "pochodzi&#263;"
  ]
  node [
    id 659
    label "period"
  ]
  node [
    id 660
    label "okres_czasu"
  ]
  node [
    id 661
    label "poprzedza&#263;"
  ]
  node [
    id 662
    label "schy&#322;ek"
  ]
  node [
    id 663
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 664
    label "odwlekanie_si&#281;"
  ]
  node [
    id 665
    label "czwarty_wymiar"
  ]
  node [
    id 666
    label "pochodzenie"
  ]
  node [
    id 667
    label "Zeitgeist"
  ]
  node [
    id 668
    label "trawi&#263;"
  ]
  node [
    id 669
    label "pogoda"
  ]
  node [
    id 670
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 671
    label "poprzedzi&#263;"
  ]
  node [
    id 672
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 673
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 674
    label "time_period"
  ]
  node [
    id 675
    label "nazewnictwo"
  ]
  node [
    id 676
    label "term"
  ]
  node [
    id 677
    label "przypadni&#281;cie"
  ]
  node [
    id 678
    label "ekspiracja"
  ]
  node [
    id 679
    label "przypa&#347;&#263;"
  ]
  node [
    id 680
    label "chronogram"
  ]
  node [
    id 681
    label "praktyka"
  ]
  node [
    id 682
    label "nazwa"
  ]
  node [
    id 683
    label "przyj&#281;cie"
  ]
  node [
    id 684
    label "spotkanie"
  ]
  node [
    id 685
    label "night"
  ]
  node [
    id 686
    label "zach&#243;d"
  ]
  node [
    id 687
    label "vesper"
  ]
  node [
    id 688
    label "pora"
  ]
  node [
    id 689
    label "odwieczerz"
  ]
  node [
    id 690
    label "blady_&#347;wit"
  ]
  node [
    id 691
    label "podkurek"
  ]
  node [
    id 692
    label "aurora"
  ]
  node [
    id 693
    label "wsch&#243;d"
  ]
  node [
    id 694
    label "zjawisko"
  ]
  node [
    id 695
    label "&#347;rodek"
  ]
  node [
    id 696
    label "obszar"
  ]
  node [
    id 697
    label "Ziemia"
  ]
  node [
    id 698
    label "dwunasta"
  ]
  node [
    id 699
    label "strona_&#347;wiata"
  ]
  node [
    id 700
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 701
    label "dopo&#322;udnie"
  ]
  node [
    id 702
    label "p&#243;&#322;noc"
  ]
  node [
    id 703
    label "nokturn"
  ]
  node [
    id 704
    label "time"
  ]
  node [
    id 705
    label "p&#243;&#322;godzina"
  ]
  node [
    id 706
    label "jednostka_czasu"
  ]
  node [
    id 707
    label "minuta"
  ]
  node [
    id 708
    label "kwadrans"
  ]
  node [
    id 709
    label "jednostka_geologiczna"
  ]
  node [
    id 710
    label "weekend"
  ]
  node [
    id 711
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 712
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 713
    label "miesi&#261;c"
  ]
  node [
    id 714
    label "S&#322;o&#324;ce"
  ]
  node [
    id 715
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 716
    label "&#347;wiat&#322;o"
  ]
  node [
    id 717
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 718
    label "sunlight"
  ]
  node [
    id 719
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 720
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 721
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 722
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 723
    label "mount"
  ]
  node [
    id 724
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 725
    label "wzej&#347;&#263;"
  ]
  node [
    id 726
    label "ascend"
  ]
  node [
    id 727
    label "kuca&#263;"
  ]
  node [
    id 728
    label "wyzdrowie&#263;"
  ]
  node [
    id 729
    label "opu&#347;ci&#263;"
  ]
  node [
    id 730
    label "rise"
  ]
  node [
    id 731
    label "arise"
  ]
  node [
    id 732
    label "stan&#261;&#263;"
  ]
  node [
    id 733
    label "wyzdrowienie"
  ]
  node [
    id 734
    label "le&#380;enie"
  ]
  node [
    id 735
    label "kl&#281;czenie"
  ]
  node [
    id 736
    label "opuszczenie"
  ]
  node [
    id 737
    label "uniesienie_si&#281;"
  ]
  node [
    id 738
    label "siedzenie"
  ]
  node [
    id 739
    label "beginning"
  ]
  node [
    id 740
    label "przestanie"
  ]
  node [
    id 741
    label "grudzie&#324;"
  ]
  node [
    id 742
    label "luty"
  ]
  node [
    id 743
    label "wymiar"
  ]
  node [
    id 744
    label "zakres"
  ]
  node [
    id 745
    label "kontekst"
  ]
  node [
    id 746
    label "miejsce_pracy"
  ]
  node [
    id 747
    label "nation"
  ]
  node [
    id 748
    label "krajobraz"
  ]
  node [
    id 749
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 750
    label "przyroda"
  ]
  node [
    id 751
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 752
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 753
    label "w&#322;adza"
  ]
  node [
    id 754
    label "integer"
  ]
  node [
    id 755
    label "zlewanie_si&#281;"
  ]
  node [
    id 756
    label "uk&#322;ad"
  ]
  node [
    id 757
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 758
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 759
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 760
    label "parametr"
  ]
  node [
    id 761
    label "warunek_lokalowy"
  ]
  node [
    id 762
    label "poziom"
  ]
  node [
    id 763
    label "znaczenie"
  ]
  node [
    id 764
    label "wielko&#347;&#263;"
  ]
  node [
    id 765
    label "dymensja"
  ]
  node [
    id 766
    label "strona"
  ]
  node [
    id 767
    label "Kosowo"
  ]
  node [
    id 768
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 769
    label "Zab&#322;ocie"
  ]
  node [
    id 770
    label "Pow&#261;zki"
  ]
  node [
    id 771
    label "Piotrowo"
  ]
  node [
    id 772
    label "Olszanica"
  ]
  node [
    id 773
    label "zbi&#243;r"
  ]
  node [
    id 774
    label "holarktyka"
  ]
  node [
    id 775
    label "Ruda_Pabianicka"
  ]
  node [
    id 776
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 777
    label "Ludwin&#243;w"
  ]
  node [
    id 778
    label "Arktyka"
  ]
  node [
    id 779
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 780
    label "Zabu&#380;e"
  ]
  node [
    id 781
    label "miejsce"
  ]
  node [
    id 782
    label "antroposfera"
  ]
  node [
    id 783
    label "terytorium"
  ]
  node [
    id 784
    label "Neogea"
  ]
  node [
    id 785
    label "Syberia_Zachodnia"
  ]
  node [
    id 786
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 787
    label "pas_planetoid"
  ]
  node [
    id 788
    label "Syberia_Wschodnia"
  ]
  node [
    id 789
    label "Antarktyka"
  ]
  node [
    id 790
    label "Rakowice"
  ]
  node [
    id 791
    label "akrecja"
  ]
  node [
    id 792
    label "&#321;&#281;g"
  ]
  node [
    id 793
    label "Kresy_Zachodnie"
  ]
  node [
    id 794
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 795
    label "przestrze&#324;"
  ]
  node [
    id 796
    label "Notogea"
  ]
  node [
    id 797
    label "&#347;rodowisko"
  ]
  node [
    id 798
    label "odniesienie"
  ]
  node [
    id 799
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 800
    label "otoczenie"
  ]
  node [
    id 801
    label "background"
  ]
  node [
    id 802
    label "causal_agent"
  ]
  node [
    id 803
    label "context"
  ]
  node [
    id 804
    label "warunki"
  ]
  node [
    id 805
    label "fragment"
  ]
  node [
    id 806
    label "interpretacja"
  ]
  node [
    id 807
    label "struktura"
  ]
  node [
    id 808
    label "prawo"
  ]
  node [
    id 809
    label "rz&#261;dzenie"
  ]
  node [
    id 810
    label "panowanie"
  ]
  node [
    id 811
    label "Kreml"
  ]
  node [
    id 812
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 813
    label "wydolno&#347;&#263;"
  ]
  node [
    id 814
    label "rz&#261;d"
  ]
  node [
    id 815
    label "granica"
  ]
  node [
    id 816
    label "sfera"
  ]
  node [
    id 817
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 818
    label "podzakres"
  ]
  node [
    id 819
    label "dziedzina"
  ]
  node [
    id 820
    label "desygnat"
  ]
  node [
    id 821
    label "circle"
  ]
  node [
    id 822
    label "human_body"
  ]
  node [
    id 823
    label "dzie&#322;o"
  ]
  node [
    id 824
    label "obraz"
  ]
  node [
    id 825
    label "widok"
  ]
  node [
    id 826
    label "zaj&#347;cie"
  ]
  node [
    id 827
    label "woda"
  ]
  node [
    id 828
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 829
    label "ekosystem"
  ]
  node [
    id 830
    label "stw&#243;r"
  ]
  node [
    id 831
    label "environment"
  ]
  node [
    id 832
    label "przyra"
  ]
  node [
    id 833
    label "wszechstworzenie"
  ]
  node [
    id 834
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 835
    label "fauna"
  ]
  node [
    id 836
    label "biota"
  ]
  node [
    id 837
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 838
    label "bliski"
  ]
  node [
    id 839
    label "blisko"
  ]
  node [
    id 840
    label "znajomy"
  ]
  node [
    id 841
    label "zwi&#261;zany"
  ]
  node [
    id 842
    label "przesz&#322;y"
  ]
  node [
    id 843
    label "silny"
  ]
  node [
    id 844
    label "zbli&#380;enie"
  ]
  node [
    id 845
    label "kr&#243;tki"
  ]
  node [
    id 846
    label "oddalony"
  ]
  node [
    id 847
    label "dok&#322;adny"
  ]
  node [
    id 848
    label "nieodleg&#322;y"
  ]
  node [
    id 849
    label "przysz&#322;y"
  ]
  node [
    id 850
    label "gotowy"
  ]
  node [
    id 851
    label "ma&#322;y"
  ]
  node [
    id 852
    label "nocleg"
  ]
  node [
    id 853
    label "budynek_zamieszkania_zbiorowego"
  ]
  node [
    id 854
    label "jaskinia"
  ]
  node [
    id 855
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  node [
    id 856
    label "lokal"
  ]
  node [
    id 857
    label "dom"
  ]
  node [
    id 858
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 859
    label "rodzina"
  ]
  node [
    id 860
    label "substancja_mieszkaniowa"
  ]
  node [
    id 861
    label "instytucja"
  ]
  node [
    id 862
    label "siedziba"
  ]
  node [
    id 863
    label "dom_rodzinny"
  ]
  node [
    id 864
    label "budynek"
  ]
  node [
    id 865
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 866
    label "stead"
  ]
  node [
    id 867
    label "garderoba"
  ]
  node [
    id 868
    label "wiecha"
  ]
  node [
    id 869
    label "fratria"
  ]
  node [
    id 870
    label "strop"
  ]
  node [
    id 871
    label "korytarz"
  ]
  node [
    id 872
    label "komora"
  ]
  node [
    id 873
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 874
    label "gastronomia"
  ]
  node [
    id 875
    label "zak&#322;ad"
  ]
  node [
    id 876
    label "room"
  ]
  node [
    id 877
    label "baza_noclegowa"
  ]
  node [
    id 878
    label "pobyt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 41
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 37
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 787
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 789
  ]
  edge [
    source 14
    target 790
  ]
  edge [
    source 14
    target 791
  ]
  edge [
    source 14
    target 792
  ]
  edge [
    source 14
    target 793
  ]
  edge [
    source 14
    target 794
  ]
  edge [
    source 14
    target 795
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 796
  ]
  edge [
    source 14
    target 797
  ]
  edge [
    source 14
    target 798
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 802
  ]
  edge [
    source 14
    target 803
  ]
  edge [
    source 14
    target 804
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 14
    target 808
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 14
    target 810
  ]
  edge [
    source 14
    target 811
  ]
  edge [
    source 14
    target 812
  ]
  edge [
    source 14
    target 813
  ]
  edge [
    source 14
    target 39
  ]
  edge [
    source 14
    target 814
  ]
  edge [
    source 14
    target 815
  ]
  edge [
    source 14
    target 816
  ]
  edge [
    source 14
    target 817
  ]
  edge [
    source 14
    target 818
  ]
  edge [
    source 14
    target 819
  ]
  edge [
    source 14
    target 820
  ]
  edge [
    source 14
    target 821
  ]
  edge [
    source 14
    target 822
  ]
  edge [
    source 14
    target 823
  ]
  edge [
    source 14
    target 824
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 825
  ]
  edge [
    source 14
    target 826
  ]
  edge [
    source 14
    target 827
  ]
  edge [
    source 14
    target 828
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 829
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 830
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 14
    target 831
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 832
  ]
  edge [
    source 14
    target 833
  ]
  edge [
    source 14
    target 834
  ]
  edge [
    source 14
    target 835
  ]
  edge [
    source 14
    target 836
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
]
