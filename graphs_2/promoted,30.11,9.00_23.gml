graph [
  node [
    id 0
    label "wysoce"
    origin "text"
  ]
  node [
    id 1
    label "wysoki"
  ]
  node [
    id 2
    label "intensywnie"
  ]
  node [
    id 3
    label "wielki"
  ]
  node [
    id 4
    label "intensywny"
  ]
  node [
    id 5
    label "g&#281;sto"
  ]
  node [
    id 6
    label "dynamicznie"
  ]
  node [
    id 7
    label "znaczny"
  ]
  node [
    id 8
    label "wyj&#261;tkowy"
  ]
  node [
    id 9
    label "nieprzeci&#281;tny"
  ]
  node [
    id 10
    label "wa&#380;ny"
  ]
  node [
    id 11
    label "prawdziwy"
  ]
  node [
    id 12
    label "wybitny"
  ]
  node [
    id 13
    label "dupny"
  ]
  node [
    id 14
    label "wyrafinowany"
  ]
  node [
    id 15
    label "niepo&#347;ledni"
  ]
  node [
    id 16
    label "du&#380;y"
  ]
  node [
    id 17
    label "chwalebny"
  ]
  node [
    id 18
    label "z_wysoka"
  ]
  node [
    id 19
    label "wznios&#322;y"
  ]
  node [
    id 20
    label "daleki"
  ]
  node [
    id 21
    label "szczytnie"
  ]
  node [
    id 22
    label "warto&#347;ciowy"
  ]
  node [
    id 23
    label "wysoko"
  ]
  node [
    id 24
    label "uprzywilejowany"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
]
