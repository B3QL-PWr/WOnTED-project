graph [
  node [
    id 0
    label "dlaczego"
    origin "text"
  ]
  node [
    id 1
    label "pszczelarz"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 3
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zgoda"
    origin "text"
  ]
  node [
    id 5
    label "hodowca"
  ]
  node [
    id 6
    label "rolnik"
  ]
  node [
    id 7
    label "by&#263;"
  ]
  node [
    id 8
    label "gotowy"
  ]
  node [
    id 9
    label "might"
  ]
  node [
    id 10
    label "uprawi&#263;"
  ]
  node [
    id 11
    label "public_treasury"
  ]
  node [
    id 12
    label "pole"
  ]
  node [
    id 13
    label "obrobi&#263;"
  ]
  node [
    id 14
    label "nietrze&#378;wy"
  ]
  node [
    id 15
    label "czekanie"
  ]
  node [
    id 16
    label "martwy"
  ]
  node [
    id 17
    label "bliski"
  ]
  node [
    id 18
    label "gotowo"
  ]
  node [
    id 19
    label "przygotowanie"
  ]
  node [
    id 20
    label "przygotowywanie"
  ]
  node [
    id 21
    label "dyspozycyjny"
  ]
  node [
    id 22
    label "zalany"
  ]
  node [
    id 23
    label "nieuchronny"
  ]
  node [
    id 24
    label "doj&#347;cie"
  ]
  node [
    id 25
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 26
    label "mie&#263;_miejsce"
  ]
  node [
    id 27
    label "equal"
  ]
  node [
    id 28
    label "trwa&#263;"
  ]
  node [
    id 29
    label "chodzi&#263;"
  ]
  node [
    id 30
    label "si&#281;ga&#263;"
  ]
  node [
    id 31
    label "stan"
  ]
  node [
    id 32
    label "obecno&#347;&#263;"
  ]
  node [
    id 33
    label "stand"
  ]
  node [
    id 34
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "uczestniczy&#263;"
  ]
  node [
    id 36
    label "istnie&#263;"
  ]
  node [
    id 37
    label "pause"
  ]
  node [
    id 38
    label "stay"
  ]
  node [
    id 39
    label "consist"
  ]
  node [
    id 40
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 41
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 42
    label "odst&#281;powa&#263;"
  ]
  node [
    id 43
    label "perform"
  ]
  node [
    id 44
    label "wychodzi&#263;"
  ]
  node [
    id 45
    label "seclude"
  ]
  node [
    id 46
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 47
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 48
    label "nak&#322;ania&#263;"
  ]
  node [
    id 49
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 50
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 51
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 52
    label "dzia&#322;a&#263;"
  ]
  node [
    id 53
    label "act"
  ]
  node [
    id 54
    label "appear"
  ]
  node [
    id 55
    label "unwrap"
  ]
  node [
    id 56
    label "rezygnowa&#263;"
  ]
  node [
    id 57
    label "overture"
  ]
  node [
    id 58
    label "decyzja"
  ]
  node [
    id 59
    label "wiedza"
  ]
  node [
    id 60
    label "consensus"
  ]
  node [
    id 61
    label "zwalnianie_si&#281;"
  ]
  node [
    id 62
    label "odpowied&#378;"
  ]
  node [
    id 63
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 64
    label "jednomy&#347;lno&#347;&#263;"
  ]
  node [
    id 65
    label "spok&#243;j"
  ]
  node [
    id 66
    label "license"
  ]
  node [
    id 67
    label "agreement"
  ]
  node [
    id 68
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 69
    label "zwolnienie_si&#281;"
  ]
  node [
    id 70
    label "entity"
  ]
  node [
    id 71
    label "pozwole&#324;stwo"
  ]
  node [
    id 72
    label "slowness"
  ]
  node [
    id 73
    label "cecha"
  ]
  node [
    id 74
    label "cisza"
  ]
  node [
    id 75
    label "control"
  ]
  node [
    id 76
    label "tajemno&#347;&#263;"
  ]
  node [
    id 77
    label "czas"
  ]
  node [
    id 78
    label "ci&#261;g"
  ]
  node [
    id 79
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 80
    label "management"
  ]
  node [
    id 81
    label "resolution"
  ]
  node [
    id 82
    label "wytw&#243;r"
  ]
  node [
    id 83
    label "zdecydowanie"
  ]
  node [
    id 84
    label "dokument"
  ]
  node [
    id 85
    label "react"
  ]
  node [
    id 86
    label "replica"
  ]
  node [
    id 87
    label "rozmowa"
  ]
  node [
    id 88
    label "wyj&#347;cie"
  ]
  node [
    id 89
    label "respondent"
  ]
  node [
    id 90
    label "reakcja"
  ]
  node [
    id 91
    label "podobie&#324;stwo"
  ]
  node [
    id 92
    label "unit"
  ]
  node [
    id 93
    label "cognition"
  ]
  node [
    id 94
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 95
    label "intelekt"
  ]
  node [
    id 96
    label "pozwolenie"
  ]
  node [
    id 97
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 98
    label "zaawansowanie"
  ]
  node [
    id 99
    label "wykszta&#322;cenie"
  ]
  node [
    id 100
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 101
    label "porozumienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
]
