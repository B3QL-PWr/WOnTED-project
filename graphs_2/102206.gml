graph [
  node [
    id 0
    label "doby&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "szpada"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "fechmistrz"
    origin "text"
  ]
  node [
    id 4
    label "anglia"
    origin "text"
  ]
  node [
    id 5
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 6
    label "poda&#263;"
    origin "text"
  ]
  node [
    id 7
    label "s&#322;omka"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "robi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pika"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "ten"
    origin "text"
  ]
  node [
    id 13
    label "sztuka"
    origin "text"
  ]
  node [
    id 14
    label "m&#322;odo&#347;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "pokazywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "dwana&#347;cie"
    origin "text"
  ]
  node [
    id 19
    label "raz"
    origin "text"
  ]
  node [
    id 20
    label "zawsze"
    origin "text"
  ]
  node [
    id 21
    label "to&#380;"
    origin "text"
  ]
  node [
    id 22
    label "sam"
    origin "text"
  ]
  node [
    id 23
    label "powtarza&#263;"
    origin "text"
  ]
  node [
    id 24
    label "prawie"
    origin "text"
  ]
  node [
    id 25
    label "kona&#263;by&#263;"
    origin "text"
  ]
  node [
    id 26
    label "trud"
    origin "text"
  ]
  node [
    id 27
    label "przykro&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "nudno&#347;ci"
    origin "text"
  ]
  node [
    id 29
    label "bro&#324;_osobista"
  ]
  node [
    id 30
    label "sword"
  ]
  node [
    id 31
    label "szermierka"
  ]
  node [
    id 32
    label "punta"
  ]
  node [
    id 33
    label "bro&#324;_sportowa"
  ]
  node [
    id 34
    label "bro&#324;"
  ]
  node [
    id 35
    label "amunicja"
  ]
  node [
    id 36
    label "karta_przetargowa"
  ]
  node [
    id 37
    label "rozbrojenie"
  ]
  node [
    id 38
    label "rozbroi&#263;"
  ]
  node [
    id 39
    label "osprz&#281;t"
  ]
  node [
    id 40
    label "uzbrojenie"
  ]
  node [
    id 41
    label "przyrz&#261;d"
  ]
  node [
    id 42
    label "rozbrajanie"
  ]
  node [
    id 43
    label "rozbraja&#263;"
  ]
  node [
    id 44
    label "or&#281;&#380;"
  ]
  node [
    id 45
    label "plansza"
  ]
  node [
    id 46
    label "wi&#261;zanie"
  ]
  node [
    id 47
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 48
    label "ripostowa&#263;"
  ]
  node [
    id 49
    label "czerwona_kartka"
  ]
  node [
    id 50
    label "czarna_kartka"
  ]
  node [
    id 51
    label "pozycja"
  ]
  node [
    id 52
    label "fight"
  ]
  node [
    id 53
    label "sport_walki"
  ]
  node [
    id 54
    label "apel"
  ]
  node [
    id 55
    label "manszeta"
  ]
  node [
    id 56
    label "ripostowanie"
  ]
  node [
    id 57
    label "tusz"
  ]
  node [
    id 58
    label "element"
  ]
  node [
    id 59
    label "floret"
  ]
  node [
    id 60
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 61
    label "zobo"
  ]
  node [
    id 62
    label "yakalo"
  ]
  node [
    id 63
    label "byd&#322;o"
  ]
  node [
    id 64
    label "dzo"
  ]
  node [
    id 65
    label "kr&#281;torogie"
  ]
  node [
    id 66
    label "zbi&#243;r"
  ]
  node [
    id 67
    label "g&#322;owa"
  ]
  node [
    id 68
    label "czochrad&#322;o"
  ]
  node [
    id 69
    label "posp&#243;lstwo"
  ]
  node [
    id 70
    label "kraal"
  ]
  node [
    id 71
    label "livestock"
  ]
  node [
    id 72
    label "prze&#380;uwacz"
  ]
  node [
    id 73
    label "bizon"
  ]
  node [
    id 74
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 75
    label "zebu"
  ]
  node [
    id 76
    label "byd&#322;o_domowe"
  ]
  node [
    id 77
    label "szermierz"
  ]
  node [
    id 78
    label "instruktor"
  ]
  node [
    id 79
    label "sportowiec"
  ]
  node [
    id 80
    label "zgrupowanie"
  ]
  node [
    id 81
    label "cz&#322;owiek"
  ]
  node [
    id 82
    label "nauczyciel"
  ]
  node [
    id 83
    label "harcerz"
  ]
  node [
    id 84
    label "opiekun"
  ]
  node [
    id 85
    label "trener"
  ]
  node [
    id 86
    label "organizacja_harcerska"
  ]
  node [
    id 87
    label "bojownik"
  ]
  node [
    id 88
    label "orator"
  ]
  node [
    id 89
    label "dziewka"
  ]
  node [
    id 90
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 91
    label "sikorka"
  ]
  node [
    id 92
    label "kora"
  ]
  node [
    id 93
    label "dziewcz&#281;"
  ]
  node [
    id 94
    label "dziecina"
  ]
  node [
    id 95
    label "m&#322;&#243;dka"
  ]
  node [
    id 96
    label "sympatia"
  ]
  node [
    id 97
    label "dziunia"
  ]
  node [
    id 98
    label "dziewczynina"
  ]
  node [
    id 99
    label "partnerka"
  ]
  node [
    id 100
    label "siksa"
  ]
  node [
    id 101
    label "dziewoja"
  ]
  node [
    id 102
    label "aktorka"
  ]
  node [
    id 103
    label "kobieta"
  ]
  node [
    id 104
    label "partner"
  ]
  node [
    id 105
    label "kobita"
  ]
  node [
    id 106
    label "emocja"
  ]
  node [
    id 107
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 108
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 109
    label "love"
  ]
  node [
    id 110
    label "ludzko&#347;&#263;"
  ]
  node [
    id 111
    label "asymilowanie"
  ]
  node [
    id 112
    label "wapniak"
  ]
  node [
    id 113
    label "asymilowa&#263;"
  ]
  node [
    id 114
    label "os&#322;abia&#263;"
  ]
  node [
    id 115
    label "posta&#263;"
  ]
  node [
    id 116
    label "hominid"
  ]
  node [
    id 117
    label "podw&#322;adny"
  ]
  node [
    id 118
    label "os&#322;abianie"
  ]
  node [
    id 119
    label "figura"
  ]
  node [
    id 120
    label "portrecista"
  ]
  node [
    id 121
    label "dwun&#243;g"
  ]
  node [
    id 122
    label "profanum"
  ]
  node [
    id 123
    label "mikrokosmos"
  ]
  node [
    id 124
    label "nasada"
  ]
  node [
    id 125
    label "duch"
  ]
  node [
    id 126
    label "antropochoria"
  ]
  node [
    id 127
    label "osoba"
  ]
  node [
    id 128
    label "wz&#243;r"
  ]
  node [
    id 129
    label "senior"
  ]
  node [
    id 130
    label "oddzia&#322;ywanie"
  ]
  node [
    id 131
    label "Adam"
  ]
  node [
    id 132
    label "homo_sapiens"
  ]
  node [
    id 133
    label "polifag"
  ]
  node [
    id 134
    label "s&#322;u&#380;ba"
  ]
  node [
    id 135
    label "ochmistrzyni"
  ]
  node [
    id 136
    label "s&#322;u&#380;ebnica"
  ]
  node [
    id 137
    label "prostytutka"
  ]
  node [
    id 138
    label "ma&#322;olata"
  ]
  node [
    id 139
    label "sikora"
  ]
  node [
    id 140
    label "panna"
  ]
  node [
    id 141
    label "laska"
  ]
  node [
    id 142
    label "dziecko"
  ]
  node [
    id 143
    label "zwrot"
  ]
  node [
    id 144
    label "crust"
  ]
  node [
    id 145
    label "ciasto"
  ]
  node [
    id 146
    label "szabla"
  ]
  node [
    id 147
    label "drzewko"
  ]
  node [
    id 148
    label "drzewo"
  ]
  node [
    id 149
    label "po&#347;ciel&#243;wka"
  ]
  node [
    id 150
    label "harfa"
  ]
  node [
    id 151
    label "bawe&#322;na"
  ]
  node [
    id 152
    label "tkanka_sta&#322;a"
  ]
  node [
    id 153
    label "piskl&#281;"
  ]
  node [
    id 154
    label "samica"
  ]
  node [
    id 155
    label "ptak"
  ]
  node [
    id 156
    label "upierzenie"
  ]
  node [
    id 157
    label "m&#322;odzie&#380;"
  ]
  node [
    id 158
    label "mo&#322;odyca"
  ]
  node [
    id 159
    label "tenis"
  ]
  node [
    id 160
    label "supply"
  ]
  node [
    id 161
    label "da&#263;"
  ]
  node [
    id 162
    label "ustawi&#263;"
  ]
  node [
    id 163
    label "siatk&#243;wka"
  ]
  node [
    id 164
    label "give"
  ]
  node [
    id 165
    label "zagra&#263;"
  ]
  node [
    id 166
    label "jedzenie"
  ]
  node [
    id 167
    label "poinformowa&#263;"
  ]
  node [
    id 168
    label "introduce"
  ]
  node [
    id 169
    label "nafaszerowa&#263;"
  ]
  node [
    id 170
    label "zaserwowa&#263;"
  ]
  node [
    id 171
    label "pi&#322;ka"
  ]
  node [
    id 172
    label "poprawi&#263;"
  ]
  node [
    id 173
    label "nada&#263;"
  ]
  node [
    id 174
    label "peddle"
  ]
  node [
    id 175
    label "marshal"
  ]
  node [
    id 176
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 177
    label "wyznaczy&#263;"
  ]
  node [
    id 178
    label "stanowisko"
  ]
  node [
    id 179
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 180
    label "spowodowa&#263;"
  ]
  node [
    id 181
    label "zabezpieczy&#263;"
  ]
  node [
    id 182
    label "umie&#347;ci&#263;"
  ]
  node [
    id 183
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 184
    label "zinterpretowa&#263;"
  ]
  node [
    id 185
    label "wskaza&#263;"
  ]
  node [
    id 186
    label "set"
  ]
  node [
    id 187
    label "przyzna&#263;"
  ]
  node [
    id 188
    label "sk&#322;oni&#263;"
  ]
  node [
    id 189
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 190
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 191
    label "zdecydowa&#263;"
  ]
  node [
    id 192
    label "accommodate"
  ]
  node [
    id 193
    label "ustali&#263;"
  ]
  node [
    id 194
    label "situate"
  ]
  node [
    id 195
    label "rola"
  ]
  node [
    id 196
    label "inform"
  ]
  node [
    id 197
    label "zakomunikowa&#263;"
  ]
  node [
    id 198
    label "powierzy&#263;"
  ]
  node [
    id 199
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 200
    label "obieca&#263;"
  ]
  node [
    id 201
    label "pozwoli&#263;"
  ]
  node [
    id 202
    label "odst&#261;pi&#263;"
  ]
  node [
    id 203
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 204
    label "przywali&#263;"
  ]
  node [
    id 205
    label "wyrzec_si&#281;"
  ]
  node [
    id 206
    label "sztachn&#261;&#263;"
  ]
  node [
    id 207
    label "rap"
  ]
  node [
    id 208
    label "feed"
  ]
  node [
    id 209
    label "zrobi&#263;"
  ]
  node [
    id 210
    label "convey"
  ]
  node [
    id 211
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 212
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 213
    label "testify"
  ]
  node [
    id 214
    label "udost&#281;pni&#263;"
  ]
  node [
    id 215
    label "przeznaczy&#263;"
  ]
  node [
    id 216
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 217
    label "picture"
  ]
  node [
    id 218
    label "zada&#263;"
  ]
  node [
    id 219
    label "dress"
  ]
  node [
    id 220
    label "dostarczy&#263;"
  ]
  node [
    id 221
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 222
    label "przekaza&#263;"
  ]
  node [
    id 223
    label "doda&#263;"
  ]
  node [
    id 224
    label "zap&#322;aci&#263;"
  ]
  node [
    id 225
    label "play"
  ]
  node [
    id 226
    label "zabrzmie&#263;"
  ]
  node [
    id 227
    label "leave"
  ]
  node [
    id 228
    label "instrument_muzyczny"
  ]
  node [
    id 229
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 230
    label "flare"
  ]
  node [
    id 231
    label "rozegra&#263;"
  ]
  node [
    id 232
    label "zaszczeka&#263;"
  ]
  node [
    id 233
    label "sound"
  ]
  node [
    id 234
    label "represent"
  ]
  node [
    id 235
    label "wykorzysta&#263;"
  ]
  node [
    id 236
    label "zatokowa&#263;"
  ]
  node [
    id 237
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 238
    label "uda&#263;_si&#281;"
  ]
  node [
    id 239
    label "zacz&#261;&#263;"
  ]
  node [
    id 240
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 241
    label "wykona&#263;"
  ]
  node [
    id 242
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 243
    label "typify"
  ]
  node [
    id 244
    label "blok"
  ]
  node [
    id 245
    label "lobowanie"
  ]
  node [
    id 246
    label "&#347;cina&#263;"
  ]
  node [
    id 247
    label "retinopatia"
  ]
  node [
    id 248
    label "podanie"
  ]
  node [
    id 249
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 250
    label "cia&#322;o_szkliste"
  ]
  node [
    id 251
    label "&#347;cinanie"
  ]
  node [
    id 252
    label "zeaksantyna"
  ]
  node [
    id 253
    label "podawa&#263;"
  ]
  node [
    id 254
    label "przelobowa&#263;"
  ]
  node [
    id 255
    label "lobowa&#263;"
  ]
  node [
    id 256
    label "dno_oka"
  ]
  node [
    id 257
    label "przelobowanie"
  ]
  node [
    id 258
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 259
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 260
    label "&#347;ci&#281;cie"
  ]
  node [
    id 261
    label "podawanie"
  ]
  node [
    id 262
    label "pelota"
  ]
  node [
    id 263
    label "sport_rakietowy"
  ]
  node [
    id 264
    label "wolej"
  ]
  node [
    id 265
    label "supervisor"
  ]
  node [
    id 266
    label "ubrani&#243;wka"
  ]
  node [
    id 267
    label "singlista"
  ]
  node [
    id 268
    label "bekhend"
  ]
  node [
    id 269
    label "forhend"
  ]
  node [
    id 270
    label "p&#243;&#322;wolej"
  ]
  node [
    id 271
    label "pr&#261;&#380;kowany"
  ]
  node [
    id 272
    label "singlowy"
  ]
  node [
    id 273
    label "tkanina_we&#322;niana"
  ]
  node [
    id 274
    label "deblowy"
  ]
  node [
    id 275
    label "tkanina"
  ]
  node [
    id 276
    label "mikst"
  ]
  node [
    id 277
    label "slajs"
  ]
  node [
    id 278
    label "deblista"
  ]
  node [
    id 279
    label "miksista"
  ]
  node [
    id 280
    label "Wimbledon"
  ]
  node [
    id 281
    label "zatruwanie_si&#281;"
  ]
  node [
    id 282
    label "przejadanie_si&#281;"
  ]
  node [
    id 283
    label "szama"
  ]
  node [
    id 284
    label "koryto"
  ]
  node [
    id 285
    label "rzecz"
  ]
  node [
    id 286
    label "odpasanie_si&#281;"
  ]
  node [
    id 287
    label "eating"
  ]
  node [
    id 288
    label "jadanie"
  ]
  node [
    id 289
    label "posilenie"
  ]
  node [
    id 290
    label "wpieprzanie"
  ]
  node [
    id 291
    label "wmuszanie"
  ]
  node [
    id 292
    label "robienie"
  ]
  node [
    id 293
    label "wiwenda"
  ]
  node [
    id 294
    label "polowanie"
  ]
  node [
    id 295
    label "ufetowanie_si&#281;"
  ]
  node [
    id 296
    label "wyjadanie"
  ]
  node [
    id 297
    label "smakowanie"
  ]
  node [
    id 298
    label "przejedzenie"
  ]
  node [
    id 299
    label "jad&#322;o"
  ]
  node [
    id 300
    label "mlaskanie"
  ]
  node [
    id 301
    label "papusianie"
  ]
  node [
    id 302
    label "posilanie"
  ]
  node [
    id 303
    label "czynno&#347;&#263;"
  ]
  node [
    id 304
    label "przejedzenie_si&#281;"
  ]
  node [
    id 305
    label "&#380;arcie"
  ]
  node [
    id 306
    label "odpasienie_si&#281;"
  ]
  node [
    id 307
    label "wyjedzenie"
  ]
  node [
    id 308
    label "przejadanie"
  ]
  node [
    id 309
    label "objadanie"
  ]
  node [
    id 310
    label "przesadzi&#263;"
  ]
  node [
    id 311
    label "nadzia&#263;"
  ]
  node [
    id 312
    label "stuff"
  ]
  node [
    id 313
    label "rurka"
  ]
  node [
    id 314
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 315
    label "straw"
  ]
  node [
    id 316
    label "osklepek"
  ]
  node [
    id 317
    label "kwiat"
  ]
  node [
    id 318
    label "tube"
  ]
  node [
    id 319
    label "rura"
  ]
  node [
    id 320
    label "makaron"
  ]
  node [
    id 321
    label "kolanko"
  ]
  node [
    id 322
    label "&#322;odyga"
  ]
  node [
    id 323
    label "lot"
  ]
  node [
    id 324
    label "grot"
  ]
  node [
    id 325
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 326
    label "ratyszcze"
  ]
  node [
    id 327
    label "chronometra&#380;ysta"
  ]
  node [
    id 328
    label "odlot"
  ]
  node [
    id 329
    label "l&#261;dowanie"
  ]
  node [
    id 330
    label "start"
  ]
  node [
    id 331
    label "podr&#243;&#380;"
  ]
  node [
    id 332
    label "ruch"
  ]
  node [
    id 333
    label "ci&#261;g"
  ]
  node [
    id 334
    label "flight"
  ]
  node [
    id 335
    label "w&#322;&#243;kno_naturalne"
  ]
  node [
    id 336
    label "targanie"
  ]
  node [
    id 337
    label "rafinoza"
  ]
  node [
    id 338
    label "ro&#347;lina"
  ]
  node [
    id 339
    label "targa&#263;"
  ]
  node [
    id 340
    label "ro&#347;lina_w&#322;&#243;knista"
  ]
  node [
    id 341
    label "haczyk"
  ]
  node [
    id 342
    label "&#380;ele&#378;ce"
  ]
  node [
    id 343
    label "&#380;agiel"
  ]
  node [
    id 344
    label "ostrze"
  ]
  node [
    id 345
    label "zadzior"
  ]
  node [
    id 346
    label "p&#322;oszczyk"
  ]
  node [
    id 347
    label "okre&#347;lony"
  ]
  node [
    id 348
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 349
    label "wiadomy"
  ]
  node [
    id 350
    label "pr&#243;bowanie"
  ]
  node [
    id 351
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 352
    label "przedmiot"
  ]
  node [
    id 353
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 354
    label "realizacja"
  ]
  node [
    id 355
    label "scena"
  ]
  node [
    id 356
    label "didaskalia"
  ]
  node [
    id 357
    label "czyn"
  ]
  node [
    id 358
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 359
    label "environment"
  ]
  node [
    id 360
    label "head"
  ]
  node [
    id 361
    label "scenariusz"
  ]
  node [
    id 362
    label "egzemplarz"
  ]
  node [
    id 363
    label "jednostka"
  ]
  node [
    id 364
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 365
    label "utw&#243;r"
  ]
  node [
    id 366
    label "kultura_duchowa"
  ]
  node [
    id 367
    label "fortel"
  ]
  node [
    id 368
    label "theatrical_performance"
  ]
  node [
    id 369
    label "ambala&#380;"
  ]
  node [
    id 370
    label "sprawno&#347;&#263;"
  ]
  node [
    id 371
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 372
    label "Faust"
  ]
  node [
    id 373
    label "scenografia"
  ]
  node [
    id 374
    label "ods&#322;ona"
  ]
  node [
    id 375
    label "turn"
  ]
  node [
    id 376
    label "pokaz"
  ]
  node [
    id 377
    label "ilo&#347;&#263;"
  ]
  node [
    id 378
    label "przedstawienie"
  ]
  node [
    id 379
    label "przedstawi&#263;"
  ]
  node [
    id 380
    label "Apollo"
  ]
  node [
    id 381
    label "kultura"
  ]
  node [
    id 382
    label "przedstawianie"
  ]
  node [
    id 383
    label "przedstawia&#263;"
  ]
  node [
    id 384
    label "towar"
  ]
  node [
    id 385
    label "przyswoi&#263;"
  ]
  node [
    id 386
    label "one"
  ]
  node [
    id 387
    label "poj&#281;cie"
  ]
  node [
    id 388
    label "ewoluowanie"
  ]
  node [
    id 389
    label "supremum"
  ]
  node [
    id 390
    label "skala"
  ]
  node [
    id 391
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 392
    label "przyswajanie"
  ]
  node [
    id 393
    label "wyewoluowanie"
  ]
  node [
    id 394
    label "reakcja"
  ]
  node [
    id 395
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 396
    label "przeliczy&#263;"
  ]
  node [
    id 397
    label "wyewoluowa&#263;"
  ]
  node [
    id 398
    label "ewoluowa&#263;"
  ]
  node [
    id 399
    label "matematyka"
  ]
  node [
    id 400
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 401
    label "rzut"
  ]
  node [
    id 402
    label "liczba_naturalna"
  ]
  node [
    id 403
    label "czynnik_biotyczny"
  ]
  node [
    id 404
    label "individual"
  ]
  node [
    id 405
    label "obiekt"
  ]
  node [
    id 406
    label "przyswaja&#263;"
  ]
  node [
    id 407
    label "przyswojenie"
  ]
  node [
    id 408
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 409
    label "starzenie_si&#281;"
  ]
  node [
    id 410
    label "przeliczanie"
  ]
  node [
    id 411
    label "funkcja"
  ]
  node [
    id 412
    label "przelicza&#263;"
  ]
  node [
    id 413
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 414
    label "infimum"
  ]
  node [
    id 415
    label "przeliczenie"
  ]
  node [
    id 416
    label "dorobek"
  ]
  node [
    id 417
    label "tworzenie"
  ]
  node [
    id 418
    label "kreacja"
  ]
  node [
    id 419
    label "creation"
  ]
  node [
    id 420
    label "act"
  ]
  node [
    id 421
    label "rozmiar"
  ]
  node [
    id 422
    label "part"
  ]
  node [
    id 423
    label "pokaz&#243;wka"
  ]
  node [
    id 424
    label "prezenter"
  ]
  node [
    id 425
    label "wydarzenie"
  ]
  node [
    id 426
    label "wyraz"
  ]
  node [
    id 427
    label "impreza"
  ]
  node [
    id 428
    label "show"
  ]
  node [
    id 429
    label "fabrication"
  ]
  node [
    id 430
    label "scheduling"
  ]
  node [
    id 431
    label "operacja"
  ]
  node [
    id 432
    label "proces"
  ]
  node [
    id 433
    label "dzie&#322;o"
  ]
  node [
    id 434
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 435
    label "monta&#380;"
  ]
  node [
    id 436
    label "postprodukcja"
  ]
  node [
    id 437
    label "performance"
  ]
  node [
    id 438
    label "zboczenie"
  ]
  node [
    id 439
    label "om&#243;wienie"
  ]
  node [
    id 440
    label "sponiewieranie"
  ]
  node [
    id 441
    label "discipline"
  ]
  node [
    id 442
    label "omawia&#263;"
  ]
  node [
    id 443
    label "kr&#261;&#380;enie"
  ]
  node [
    id 444
    label "tre&#347;&#263;"
  ]
  node [
    id 445
    label "sponiewiera&#263;"
  ]
  node [
    id 446
    label "entity"
  ]
  node [
    id 447
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 448
    label "tematyka"
  ]
  node [
    id 449
    label "w&#261;tek"
  ]
  node [
    id 450
    label "charakter"
  ]
  node [
    id 451
    label "zbaczanie"
  ]
  node [
    id 452
    label "program_nauczania"
  ]
  node [
    id 453
    label "om&#243;wi&#263;"
  ]
  node [
    id 454
    label "omawianie"
  ]
  node [
    id 455
    label "thing"
  ]
  node [
    id 456
    label "istota"
  ]
  node [
    id 457
    label "zbacza&#263;"
  ]
  node [
    id 458
    label "zboczy&#263;"
  ]
  node [
    id 459
    label "metka"
  ]
  node [
    id 460
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 461
    label "szprycowa&#263;"
  ]
  node [
    id 462
    label "naszprycowa&#263;"
  ]
  node [
    id 463
    label "rzuca&#263;"
  ]
  node [
    id 464
    label "tandeta"
  ]
  node [
    id 465
    label "obr&#243;t_handlowy"
  ]
  node [
    id 466
    label "wyr&#243;b"
  ]
  node [
    id 467
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 468
    label "rzuci&#263;"
  ]
  node [
    id 469
    label "naszprycowanie"
  ]
  node [
    id 470
    label "szprycowanie"
  ]
  node [
    id 471
    label "za&#322;adownia"
  ]
  node [
    id 472
    label "asortyment"
  ]
  node [
    id 473
    label "&#322;&#243;dzki"
  ]
  node [
    id 474
    label "narkobiznes"
  ]
  node [
    id 475
    label "rzucenie"
  ]
  node [
    id 476
    label "rzucanie"
  ]
  node [
    id 477
    label "doros&#322;y"
  ]
  node [
    id 478
    label "&#380;ona"
  ]
  node [
    id 479
    label "uleganie"
  ]
  node [
    id 480
    label "ulec"
  ]
  node [
    id 481
    label "m&#281;&#380;yna"
  ]
  node [
    id 482
    label "ulegni&#281;cie"
  ]
  node [
    id 483
    label "pa&#324;stwo"
  ]
  node [
    id 484
    label "&#322;ono"
  ]
  node [
    id 485
    label "menopauza"
  ]
  node [
    id 486
    label "przekwitanie"
  ]
  node [
    id 487
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 488
    label "babka"
  ]
  node [
    id 489
    label "ulega&#263;"
  ]
  node [
    id 490
    label "jako&#347;&#263;"
  ]
  node [
    id 491
    label "szybko&#347;&#263;"
  ]
  node [
    id 492
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 493
    label "kondycja_fizyczna"
  ]
  node [
    id 494
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 495
    label "zdrowie"
  ]
  node [
    id 496
    label "stan"
  ]
  node [
    id 497
    label "harcerski"
  ]
  node [
    id 498
    label "cecha"
  ]
  node [
    id 499
    label "odznaka"
  ]
  node [
    id 500
    label "obrazowanie"
  ]
  node [
    id 501
    label "organ"
  ]
  node [
    id 502
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 503
    label "element_anatomiczny"
  ]
  node [
    id 504
    label "tekst"
  ]
  node [
    id 505
    label "komunikat"
  ]
  node [
    id 506
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 507
    label "wytw&#243;r"
  ]
  node [
    id 508
    label "okaz"
  ]
  node [
    id 509
    label "agent"
  ]
  node [
    id 510
    label "nicpo&#324;"
  ]
  node [
    id 511
    label "pean"
  ]
  node [
    id 512
    label "teatr"
  ]
  node [
    id 513
    label "exhibit"
  ]
  node [
    id 514
    label "display"
  ]
  node [
    id 515
    label "demonstrowa&#263;"
  ]
  node [
    id 516
    label "zapoznawa&#263;"
  ]
  node [
    id 517
    label "opisywa&#263;"
  ]
  node [
    id 518
    label "ukazywa&#263;"
  ]
  node [
    id 519
    label "zg&#322;asza&#263;"
  ]
  node [
    id 520
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 521
    label "attest"
  ]
  node [
    id 522
    label "stanowi&#263;"
  ]
  node [
    id 523
    label "zademonstrowanie"
  ]
  node [
    id 524
    label "report"
  ]
  node [
    id 525
    label "obgadanie"
  ]
  node [
    id 526
    label "narration"
  ]
  node [
    id 527
    label "cyrk"
  ]
  node [
    id 528
    label "opisanie"
  ]
  node [
    id 529
    label "malarstwo"
  ]
  node [
    id 530
    label "ukazanie"
  ]
  node [
    id 531
    label "zapoznanie"
  ]
  node [
    id 532
    label "spos&#243;b"
  ]
  node [
    id 533
    label "pokazanie"
  ]
  node [
    id 534
    label "wyst&#261;pienie"
  ]
  node [
    id 535
    label "stara&#263;_si&#281;"
  ]
  node [
    id 536
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 537
    label "sprawdza&#263;"
  ]
  node [
    id 538
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 539
    label "feel"
  ]
  node [
    id 540
    label "try"
  ]
  node [
    id 541
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 542
    label "kosztowa&#263;"
  ]
  node [
    id 543
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 544
    label "medialno&#347;&#263;"
  ]
  node [
    id 545
    label "ukaza&#263;"
  ]
  node [
    id 546
    label "pokaza&#263;"
  ]
  node [
    id 547
    label "zapozna&#263;"
  ]
  node [
    id 548
    label "express"
  ]
  node [
    id 549
    label "zaproponowa&#263;"
  ]
  node [
    id 550
    label "zademonstrowa&#263;"
  ]
  node [
    id 551
    label "opisa&#263;"
  ]
  node [
    id 552
    label "badanie"
  ]
  node [
    id 553
    label "podejmowanie"
  ]
  node [
    id 554
    label "usi&#322;owanie"
  ]
  node [
    id 555
    label "tasting"
  ]
  node [
    id 556
    label "kiperstwo"
  ]
  node [
    id 557
    label "staranie_si&#281;"
  ]
  node [
    id 558
    label "zaznawanie"
  ]
  node [
    id 559
    label "przygotowywanie_si&#281;"
  ]
  node [
    id 560
    label "essay"
  ]
  node [
    id 561
    label "opisywanie"
  ]
  node [
    id 562
    label "bycie"
  ]
  node [
    id 563
    label "representation"
  ]
  node [
    id 564
    label "obgadywanie"
  ]
  node [
    id 565
    label "zapoznawanie"
  ]
  node [
    id 566
    label "wyst&#281;powanie"
  ]
  node [
    id 567
    label "ukazywanie"
  ]
  node [
    id 568
    label "pokazywanie"
  ]
  node [
    id 569
    label "demonstrowanie"
  ]
  node [
    id 570
    label "presentation"
  ]
  node [
    id 571
    label "dramat"
  ]
  node [
    id 572
    label "plan"
  ]
  node [
    id 573
    label "prognoza"
  ]
  node [
    id 574
    label "scenario"
  ]
  node [
    id 575
    label "podwy&#380;szenie"
  ]
  node [
    id 576
    label "kurtyna"
  ]
  node [
    id 577
    label "akt"
  ]
  node [
    id 578
    label "widzownia"
  ]
  node [
    id 579
    label "sznurownia"
  ]
  node [
    id 580
    label "dramaturgy"
  ]
  node [
    id 581
    label "sphere"
  ]
  node [
    id 582
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 583
    label "budka_suflera"
  ]
  node [
    id 584
    label "epizod"
  ]
  node [
    id 585
    label "film"
  ]
  node [
    id 586
    label "fragment"
  ]
  node [
    id 587
    label "k&#322;&#243;tnia"
  ]
  node [
    id 588
    label "kiesze&#324;"
  ]
  node [
    id 589
    label "stadium"
  ]
  node [
    id 590
    label "podest"
  ]
  node [
    id 591
    label "horyzont"
  ]
  node [
    id 592
    label "teren"
  ]
  node [
    id 593
    label "instytucja"
  ]
  node [
    id 594
    label "proscenium"
  ]
  node [
    id 595
    label "nadscenie"
  ]
  node [
    id 596
    label "antyteatr"
  ]
  node [
    id 597
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 598
    label "mansjon"
  ]
  node [
    id 599
    label "modelatornia"
  ]
  node [
    id 600
    label "dekoracja"
  ]
  node [
    id 601
    label "uprawienie"
  ]
  node [
    id 602
    label "kszta&#322;t"
  ]
  node [
    id 603
    label "dialog"
  ]
  node [
    id 604
    label "p&#322;osa"
  ]
  node [
    id 605
    label "wykonywanie"
  ]
  node [
    id 606
    label "plik"
  ]
  node [
    id 607
    label "ziemia"
  ]
  node [
    id 608
    label "wykonywa&#263;"
  ]
  node [
    id 609
    label "ustawienie"
  ]
  node [
    id 610
    label "pole"
  ]
  node [
    id 611
    label "gospodarstwo"
  ]
  node [
    id 612
    label "uprawi&#263;"
  ]
  node [
    id 613
    label "function"
  ]
  node [
    id 614
    label "zreinterpretowa&#263;"
  ]
  node [
    id 615
    label "zastosowanie"
  ]
  node [
    id 616
    label "reinterpretowa&#263;"
  ]
  node [
    id 617
    label "wrench"
  ]
  node [
    id 618
    label "irygowanie"
  ]
  node [
    id 619
    label "irygowa&#263;"
  ]
  node [
    id 620
    label "zreinterpretowanie"
  ]
  node [
    id 621
    label "cel"
  ]
  node [
    id 622
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 623
    label "gra&#263;"
  ]
  node [
    id 624
    label "aktorstwo"
  ]
  node [
    id 625
    label "kostium"
  ]
  node [
    id 626
    label "zagon"
  ]
  node [
    id 627
    label "znaczenie"
  ]
  node [
    id 628
    label "reinterpretowanie"
  ]
  node [
    id 629
    label "sk&#322;ad"
  ]
  node [
    id 630
    label "zagranie"
  ]
  node [
    id 631
    label "radlina"
  ]
  node [
    id 632
    label "granie"
  ]
  node [
    id 633
    label "stage_direction"
  ]
  node [
    id 634
    label "obja&#347;nienie"
  ]
  node [
    id 635
    label "asymilowanie_si&#281;"
  ]
  node [
    id 636
    label "Wsch&#243;d"
  ]
  node [
    id 637
    label "praca_rolnicza"
  ]
  node [
    id 638
    label "przejmowanie"
  ]
  node [
    id 639
    label "zjawisko"
  ]
  node [
    id 640
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 641
    label "makrokosmos"
  ]
  node [
    id 642
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 643
    label "konwencja"
  ]
  node [
    id 644
    label "propriety"
  ]
  node [
    id 645
    label "przejmowa&#263;"
  ]
  node [
    id 646
    label "brzoskwiniarnia"
  ]
  node [
    id 647
    label "zwyczaj"
  ]
  node [
    id 648
    label "kuchnia"
  ]
  node [
    id 649
    label "tradycja"
  ]
  node [
    id 650
    label "populace"
  ]
  node [
    id 651
    label "hodowla"
  ]
  node [
    id 652
    label "religia"
  ]
  node [
    id 653
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 654
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 655
    label "przej&#281;cie"
  ]
  node [
    id 656
    label "przej&#261;&#263;"
  ]
  node [
    id 657
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 658
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 659
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 660
    label "&#347;rodek"
  ]
  node [
    id 661
    label "manewr"
  ]
  node [
    id 662
    label "chwyt"
  ]
  node [
    id 663
    label "game"
  ]
  node [
    id 664
    label "podchwyt"
  ]
  node [
    id 665
    label "&#380;ycie"
  ]
  node [
    id 666
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 667
    label "adolescence"
  ]
  node [
    id 668
    label "wiek"
  ]
  node [
    id 669
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 670
    label "zielone_lata"
  ]
  node [
    id 671
    label "period"
  ]
  node [
    id 672
    label "choroba_wieku"
  ]
  node [
    id 673
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 674
    label "chron"
  ]
  node [
    id 675
    label "czas"
  ]
  node [
    id 676
    label "rok"
  ]
  node [
    id 677
    label "long_time"
  ]
  node [
    id 678
    label "jednostka_geologiczna"
  ]
  node [
    id 679
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 680
    label "raj_utracony"
  ]
  node [
    id 681
    label "umieranie"
  ]
  node [
    id 682
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 683
    label "prze&#380;ywanie"
  ]
  node [
    id 684
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 685
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 686
    label "po&#322;&#243;g"
  ]
  node [
    id 687
    label "umarcie"
  ]
  node [
    id 688
    label "subsistence"
  ]
  node [
    id 689
    label "power"
  ]
  node [
    id 690
    label "okres_noworodkowy"
  ]
  node [
    id 691
    label "prze&#380;ycie"
  ]
  node [
    id 692
    label "wiek_matuzalemowy"
  ]
  node [
    id 693
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 694
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 695
    label "do&#380;ywanie"
  ]
  node [
    id 696
    label "byt"
  ]
  node [
    id 697
    label "dzieci&#324;stwo"
  ]
  node [
    id 698
    label "andropauza"
  ]
  node [
    id 699
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 700
    label "rozw&#243;j"
  ]
  node [
    id 701
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 702
    label "&#347;mier&#263;"
  ]
  node [
    id 703
    label "koleje_losu"
  ]
  node [
    id 704
    label "zegar_biologiczny"
  ]
  node [
    id 705
    label "szwung"
  ]
  node [
    id 706
    label "przebywanie"
  ]
  node [
    id 707
    label "warunki"
  ]
  node [
    id 708
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 709
    label "niemowl&#281;ctwo"
  ]
  node [
    id 710
    label "&#380;ywy"
  ]
  node [
    id 711
    label "life"
  ]
  node [
    id 712
    label "staro&#347;&#263;"
  ]
  node [
    id 713
    label "energy"
  ]
  node [
    id 714
    label "podobie&#324;stwo"
  ]
  node [
    id 715
    label "sympatyczno&#347;&#263;"
  ]
  node [
    id 716
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 717
    label "szczeg&#243;lno&#347;&#263;"
  ]
  node [
    id 718
    label "youngness"
  ]
  node [
    id 719
    label "ranek"
  ]
  node [
    id 720
    label "doba"
  ]
  node [
    id 721
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 722
    label "noc"
  ]
  node [
    id 723
    label "podwiecz&#243;r"
  ]
  node [
    id 724
    label "po&#322;udnie"
  ]
  node [
    id 725
    label "godzina"
  ]
  node [
    id 726
    label "przedpo&#322;udnie"
  ]
  node [
    id 727
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 728
    label "wiecz&#243;r"
  ]
  node [
    id 729
    label "t&#322;usty_czwartek"
  ]
  node [
    id 730
    label "popo&#322;udnie"
  ]
  node [
    id 731
    label "walentynki"
  ]
  node [
    id 732
    label "czynienie_si&#281;"
  ]
  node [
    id 733
    label "s&#322;o&#324;ce"
  ]
  node [
    id 734
    label "rano"
  ]
  node [
    id 735
    label "tydzie&#324;"
  ]
  node [
    id 736
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 737
    label "wzej&#347;cie"
  ]
  node [
    id 738
    label "wsta&#263;"
  ]
  node [
    id 739
    label "day"
  ]
  node [
    id 740
    label "termin"
  ]
  node [
    id 741
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 742
    label "wstanie"
  ]
  node [
    id 743
    label "przedwiecz&#243;r"
  ]
  node [
    id 744
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 745
    label "Sylwester"
  ]
  node [
    id 746
    label "poprzedzanie"
  ]
  node [
    id 747
    label "czasoprzestrze&#324;"
  ]
  node [
    id 748
    label "laba"
  ]
  node [
    id 749
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 750
    label "chronometria"
  ]
  node [
    id 751
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 752
    label "rachuba_czasu"
  ]
  node [
    id 753
    label "przep&#322;ywanie"
  ]
  node [
    id 754
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 755
    label "czasokres"
  ]
  node [
    id 756
    label "odczyt"
  ]
  node [
    id 757
    label "chwila"
  ]
  node [
    id 758
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 759
    label "dzieje"
  ]
  node [
    id 760
    label "kategoria_gramatyczna"
  ]
  node [
    id 761
    label "poprzedzenie"
  ]
  node [
    id 762
    label "trawienie"
  ]
  node [
    id 763
    label "pochodzi&#263;"
  ]
  node [
    id 764
    label "okres_czasu"
  ]
  node [
    id 765
    label "poprzedza&#263;"
  ]
  node [
    id 766
    label "schy&#322;ek"
  ]
  node [
    id 767
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 768
    label "odwlekanie_si&#281;"
  ]
  node [
    id 769
    label "zegar"
  ]
  node [
    id 770
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 771
    label "czwarty_wymiar"
  ]
  node [
    id 772
    label "pochodzenie"
  ]
  node [
    id 773
    label "koniugacja"
  ]
  node [
    id 774
    label "Zeitgeist"
  ]
  node [
    id 775
    label "trawi&#263;"
  ]
  node [
    id 776
    label "pogoda"
  ]
  node [
    id 777
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 778
    label "poprzedzi&#263;"
  ]
  node [
    id 779
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 780
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 781
    label "time_period"
  ]
  node [
    id 782
    label "nazewnictwo"
  ]
  node [
    id 783
    label "term"
  ]
  node [
    id 784
    label "przypadni&#281;cie"
  ]
  node [
    id 785
    label "ekspiracja"
  ]
  node [
    id 786
    label "przypa&#347;&#263;"
  ]
  node [
    id 787
    label "chronogram"
  ]
  node [
    id 788
    label "praktyka"
  ]
  node [
    id 789
    label "nazwa"
  ]
  node [
    id 790
    label "odwieczerz"
  ]
  node [
    id 791
    label "pora"
  ]
  node [
    id 792
    label "przyj&#281;cie"
  ]
  node [
    id 793
    label "spotkanie"
  ]
  node [
    id 794
    label "night"
  ]
  node [
    id 795
    label "zach&#243;d"
  ]
  node [
    id 796
    label "vesper"
  ]
  node [
    id 797
    label "aurora"
  ]
  node [
    id 798
    label "wsch&#243;d"
  ]
  node [
    id 799
    label "obszar"
  ]
  node [
    id 800
    label "Ziemia"
  ]
  node [
    id 801
    label "dwunasta"
  ]
  node [
    id 802
    label "strona_&#347;wiata"
  ]
  node [
    id 803
    label "dopo&#322;udnie"
  ]
  node [
    id 804
    label "blady_&#347;wit"
  ]
  node [
    id 805
    label "podkurek"
  ]
  node [
    id 806
    label "time"
  ]
  node [
    id 807
    label "p&#243;&#322;godzina"
  ]
  node [
    id 808
    label "jednostka_czasu"
  ]
  node [
    id 809
    label "minuta"
  ]
  node [
    id 810
    label "kwadrans"
  ]
  node [
    id 811
    label "p&#243;&#322;noc"
  ]
  node [
    id 812
    label "nokturn"
  ]
  node [
    id 813
    label "weekend"
  ]
  node [
    id 814
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 815
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 816
    label "miesi&#261;c"
  ]
  node [
    id 817
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 818
    label "mount"
  ]
  node [
    id 819
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 820
    label "wzej&#347;&#263;"
  ]
  node [
    id 821
    label "ascend"
  ]
  node [
    id 822
    label "kuca&#263;"
  ]
  node [
    id 823
    label "wyzdrowie&#263;"
  ]
  node [
    id 824
    label "opu&#347;ci&#263;"
  ]
  node [
    id 825
    label "rise"
  ]
  node [
    id 826
    label "arise"
  ]
  node [
    id 827
    label "stan&#261;&#263;"
  ]
  node [
    id 828
    label "przesta&#263;"
  ]
  node [
    id 829
    label "wyzdrowienie"
  ]
  node [
    id 830
    label "le&#380;enie"
  ]
  node [
    id 831
    label "kl&#281;czenie"
  ]
  node [
    id 832
    label "opuszczenie"
  ]
  node [
    id 833
    label "uniesienie_si&#281;"
  ]
  node [
    id 834
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 835
    label "siedzenie"
  ]
  node [
    id 836
    label "beginning"
  ]
  node [
    id 837
    label "przestanie"
  ]
  node [
    id 838
    label "S&#322;o&#324;ce"
  ]
  node [
    id 839
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 840
    label "&#347;wiat&#322;o"
  ]
  node [
    id 841
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 842
    label "kochanie"
  ]
  node [
    id 843
    label "sunlight"
  ]
  node [
    id 844
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 845
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 846
    label "grudzie&#324;"
  ]
  node [
    id 847
    label "luty"
  ]
  node [
    id 848
    label "warto&#347;&#263;"
  ]
  node [
    id 849
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 850
    label "by&#263;"
  ]
  node [
    id 851
    label "wyra&#380;a&#263;"
  ]
  node [
    id 852
    label "przeszkala&#263;"
  ]
  node [
    id 853
    label "powodowa&#263;"
  ]
  node [
    id 854
    label "exsert"
  ]
  node [
    id 855
    label "bespeak"
  ]
  node [
    id 856
    label "informowa&#263;"
  ]
  node [
    id 857
    label "indicate"
  ]
  node [
    id 858
    label "szkoli&#263;"
  ]
  node [
    id 859
    label "powiada&#263;"
  ]
  node [
    id 860
    label "komunikowa&#263;"
  ]
  node [
    id 861
    label "mie&#263;_miejsce"
  ]
  node [
    id 862
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 863
    label "motywowa&#263;"
  ]
  node [
    id 864
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 865
    label "op&#322;aca&#263;"
  ]
  node [
    id 866
    label "sk&#322;ada&#263;"
  ]
  node [
    id 867
    label "pracowa&#263;"
  ]
  node [
    id 868
    label "us&#322;uga"
  ]
  node [
    id 869
    label "opowiada&#263;"
  ]
  node [
    id 870
    label "czyni&#263;_dobro"
  ]
  node [
    id 871
    label "znaczy&#263;"
  ]
  node [
    id 872
    label "give_voice"
  ]
  node [
    id 873
    label "oznacza&#263;"
  ]
  node [
    id 874
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 875
    label "arouse"
  ]
  node [
    id 876
    label "deal"
  ]
  node [
    id 877
    label "dawa&#263;"
  ]
  node [
    id 878
    label "stawia&#263;"
  ]
  node [
    id 879
    label "rozgrywa&#263;"
  ]
  node [
    id 880
    label "kelner"
  ]
  node [
    id 881
    label "cover"
  ]
  node [
    id 882
    label "tender"
  ]
  node [
    id 883
    label "faszerowa&#263;"
  ]
  node [
    id 884
    label "serwowa&#263;"
  ]
  node [
    id 885
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 886
    label "equal"
  ]
  node [
    id 887
    label "trwa&#263;"
  ]
  node [
    id 888
    label "chodzi&#263;"
  ]
  node [
    id 889
    label "si&#281;ga&#263;"
  ]
  node [
    id 890
    label "obecno&#347;&#263;"
  ]
  node [
    id 891
    label "stand"
  ]
  node [
    id 892
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 893
    label "uczestniczy&#263;"
  ]
  node [
    id 894
    label "rewaluowa&#263;"
  ]
  node [
    id 895
    label "zrewaluowa&#263;"
  ]
  node [
    id 896
    label "zmienna"
  ]
  node [
    id 897
    label "wskazywanie"
  ]
  node [
    id 898
    label "rewaluowanie"
  ]
  node [
    id 899
    label "wskazywa&#263;"
  ]
  node [
    id 900
    label "korzy&#347;&#263;"
  ]
  node [
    id 901
    label "worth"
  ]
  node [
    id 902
    label "zrewaluowanie"
  ]
  node [
    id 903
    label "wabik"
  ]
  node [
    id 904
    label "strona"
  ]
  node [
    id 905
    label "oznaka"
  ]
  node [
    id 906
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 907
    label "leksem"
  ]
  node [
    id 908
    label "&#347;wiadczenie"
  ]
  node [
    id 909
    label "cios"
  ]
  node [
    id 910
    label "uderzenie"
  ]
  node [
    id 911
    label "shot"
  ]
  node [
    id 912
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 913
    label "struktura_geologiczna"
  ]
  node [
    id 914
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 915
    label "pr&#243;ba"
  ]
  node [
    id 916
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 917
    label "coup"
  ]
  node [
    id 918
    label "siekacz"
  ]
  node [
    id 919
    label "instrumentalizacja"
  ]
  node [
    id 920
    label "trafienie"
  ]
  node [
    id 921
    label "walka"
  ]
  node [
    id 922
    label "zdarzenie_si&#281;"
  ]
  node [
    id 923
    label "wdarcie_si&#281;"
  ]
  node [
    id 924
    label "pogorszenie"
  ]
  node [
    id 925
    label "d&#378;wi&#281;k"
  ]
  node [
    id 926
    label "poczucie"
  ]
  node [
    id 927
    label "contact"
  ]
  node [
    id 928
    label "stukni&#281;cie"
  ]
  node [
    id 929
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 930
    label "bat"
  ]
  node [
    id 931
    label "spowodowanie"
  ]
  node [
    id 932
    label "rush"
  ]
  node [
    id 933
    label "odbicie"
  ]
  node [
    id 934
    label "dawka"
  ]
  node [
    id 935
    label "zadanie"
  ]
  node [
    id 936
    label "st&#322;uczenie"
  ]
  node [
    id 937
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 938
    label "odbicie_si&#281;"
  ]
  node [
    id 939
    label "dotkni&#281;cie"
  ]
  node [
    id 940
    label "charge"
  ]
  node [
    id 941
    label "dostanie"
  ]
  node [
    id 942
    label "skrytykowanie"
  ]
  node [
    id 943
    label "zagrywka"
  ]
  node [
    id 944
    label "nast&#261;pienie"
  ]
  node [
    id 945
    label "uderzanie"
  ]
  node [
    id 946
    label "stroke"
  ]
  node [
    id 947
    label "pobicie"
  ]
  node [
    id 948
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 949
    label "flap"
  ]
  node [
    id 950
    label "dotyk"
  ]
  node [
    id 951
    label "zrobienie"
  ]
  node [
    id 952
    label "cz&#281;sto"
  ]
  node [
    id 953
    label "ci&#261;gle"
  ]
  node [
    id 954
    label "zaw&#380;dy"
  ]
  node [
    id 955
    label "na_zawsze"
  ]
  node [
    id 956
    label "cz&#281;sty"
  ]
  node [
    id 957
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 958
    label "stale"
  ]
  node [
    id 959
    label "ci&#261;g&#322;y"
  ]
  node [
    id 960
    label "nieprzerwanie"
  ]
  node [
    id 961
    label "sklep"
  ]
  node [
    id 962
    label "p&#243;&#322;ka"
  ]
  node [
    id 963
    label "firma"
  ]
  node [
    id 964
    label "stoisko"
  ]
  node [
    id 965
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 966
    label "obiekt_handlowy"
  ]
  node [
    id 967
    label "zaplecze"
  ]
  node [
    id 968
    label "witryna"
  ]
  node [
    id 969
    label "robi&#263;"
  ]
  node [
    id 970
    label "repeat"
  ]
  node [
    id 971
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 972
    label "impart"
  ]
  node [
    id 973
    label "organizowa&#263;"
  ]
  node [
    id 974
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 975
    label "czyni&#263;"
  ]
  node [
    id 976
    label "stylizowa&#263;"
  ]
  node [
    id 977
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 978
    label "falowa&#263;"
  ]
  node [
    id 979
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 980
    label "praca"
  ]
  node [
    id 981
    label "wydala&#263;"
  ]
  node [
    id 982
    label "tentegowa&#263;"
  ]
  node [
    id 983
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 984
    label "urz&#261;dza&#263;"
  ]
  node [
    id 985
    label "oszukiwa&#263;"
  ]
  node [
    id 986
    label "work"
  ]
  node [
    id 987
    label "przerabia&#263;"
  ]
  node [
    id 988
    label "post&#281;powa&#263;"
  ]
  node [
    id 989
    label "wleczenie_si&#281;"
  ]
  node [
    id 990
    label "przytaczanie_si&#281;"
  ]
  node [
    id 991
    label "trudzenie"
  ]
  node [
    id 992
    label "trudzi&#263;"
  ]
  node [
    id 993
    label "przytoczenie_si&#281;"
  ]
  node [
    id 994
    label "wlec_si&#281;"
  ]
  node [
    id 995
    label "charakterystyka"
  ]
  node [
    id 996
    label "m&#322;ot"
  ]
  node [
    id 997
    label "znak"
  ]
  node [
    id 998
    label "attribute"
  ]
  node [
    id 999
    label "marka"
  ]
  node [
    id 1000
    label "sunset"
  ]
  node [
    id 1001
    label "szar&#243;wka"
  ]
  node [
    id 1002
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1003
    label "dispatch"
  ]
  node [
    id 1004
    label "m&#281;czy&#263;"
  ]
  node [
    id 1005
    label "zajmowa&#263;"
  ]
  node [
    id 1006
    label "ora&#263;"
  ]
  node [
    id 1007
    label "trouble_oneself"
  ]
  node [
    id 1008
    label "fatygowanie_si&#281;"
  ]
  node [
    id 1009
    label "murder"
  ]
  node [
    id 1010
    label "m&#281;czenie"
  ]
  node [
    id 1011
    label "spracowanie_si&#281;"
  ]
  node [
    id 1012
    label "oranie"
  ]
  node [
    id 1013
    label "zajmowanie"
  ]
  node [
    id 1014
    label "disagreeableness"
  ]
  node [
    id 1015
    label "doznanie"
  ]
  node [
    id 1016
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1017
    label "zmys&#322;"
  ]
  node [
    id 1018
    label "czucie"
  ]
  node [
    id 1019
    label "przeczulica"
  ]
  node [
    id 1020
    label "wra&#380;enie"
  ]
  node [
    id 1021
    label "przej&#347;cie"
  ]
  node [
    id 1022
    label "poradzenie_sobie"
  ]
  node [
    id 1023
    label "przetrwanie"
  ]
  node [
    id 1024
    label "survival"
  ]
  node [
    id 1025
    label "mdlenie"
  ]
  node [
    id 1026
    label "implikowa&#263;"
  ]
  node [
    id 1027
    label "signal"
  ]
  node [
    id 1028
    label "fakt"
  ]
  node [
    id 1029
    label "symbol"
  ]
  node [
    id 1030
    label "md&#322;o&#347;ci"
  ]
  node [
    id 1031
    label "sag"
  ]
  node [
    id 1032
    label "powodowanie"
  ]
  node [
    id 1033
    label "zemdlenie"
  ]
  node [
    id 1034
    label "dzianie_si&#281;"
  ]
  node [
    id 1035
    label "s&#322;abni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 44
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 513
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 383
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 420
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 521
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 512
  ]
  edge [
    source 17
    target 514
  ]
  edge [
    source 17
    target 515
  ]
  edge [
    source 17
    target 378
  ]
  edge [
    source 17
    target 516
  ]
  edge [
    source 17
    target 517
  ]
  edge [
    source 17
    target 518
  ]
  edge [
    source 17
    target 519
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 520
  ]
  edge [
    source 17
    target 522
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 496
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 421
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 498
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 757
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 394
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 260
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 661
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 332
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 675
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 961
  ]
  edge [
    source 22
    target 962
  ]
  edge [
    source 22
    target 963
  ]
  edge [
    source 22
    target 964
  ]
  edge [
    source 22
    target 965
  ]
  edge [
    source 22
    target 629
  ]
  edge [
    source 22
    target 966
  ]
  edge [
    source 22
    target 967
  ]
  edge [
    source 22
    target 968
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 969
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 970
  ]
  edge [
    source 23
    target 971
  ]
  edge [
    source 23
    target 972
  ]
  edge [
    source 23
    target 973
  ]
  edge [
    source 23
    target 974
  ]
  edge [
    source 23
    target 975
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 976
  ]
  edge [
    source 23
    target 977
  ]
  edge [
    source 23
    target 978
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 174
  ]
  edge [
    source 23
    target 980
  ]
  edge [
    source 23
    target 981
  ]
  edge [
    source 23
    target 538
  ]
  edge [
    source 23
    target 982
  ]
  edge [
    source 23
    target 983
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 518
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 420
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 159
  ]
  edge [
    source 23
    target 876
  ]
  edge [
    source 23
    target 877
  ]
  edge [
    source 23
    target 878
  ]
  edge [
    source 23
    target 879
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 881
  ]
  edge [
    source 23
    target 882
  ]
  edge [
    source 23
    target 166
  ]
  edge [
    source 23
    target 883
  ]
  edge [
    source 23
    target 168
  ]
  edge [
    source 23
    target 856
  ]
  edge [
    source 23
    target 884
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 989
  ]
  edge [
    source 26
    target 990
  ]
  edge [
    source 26
    target 991
  ]
  edge [
    source 26
    target 795
  ]
  edge [
    source 26
    target 498
  ]
  edge [
    source 26
    target 992
  ]
  edge [
    source 26
    target 993
  ]
  edge [
    source 26
    target 994
  ]
  edge [
    source 26
    target 995
  ]
  edge [
    source 26
    target 996
  ]
  edge [
    source 26
    target 997
  ]
  edge [
    source 26
    target 148
  ]
  edge [
    source 26
    target 915
  ]
  edge [
    source 26
    target 998
  ]
  edge [
    source 26
    target 999
  ]
  edge [
    source 26
    target 728
  ]
  edge [
    source 26
    target 799
  ]
  edge [
    source 26
    target 1000
  ]
  edge [
    source 26
    target 1001
  ]
  edge [
    source 26
    target 554
  ]
  edge [
    source 26
    target 802
  ]
  edge [
    source 26
    target 1002
  ]
  edge [
    source 26
    target 639
  ]
  edge [
    source 26
    target 791
  ]
  edge [
    source 26
    target 733
  ]
  edge [
    source 26
    target 413
  ]
  edge [
    source 26
    target 1003
  ]
  edge [
    source 26
    target 1004
  ]
  edge [
    source 26
    target 1005
  ]
  edge [
    source 26
    target 1006
  ]
  edge [
    source 26
    target 1007
  ]
  edge [
    source 26
    target 1008
  ]
  edge [
    source 26
    target 1009
  ]
  edge [
    source 26
    target 1010
  ]
  edge [
    source 26
    target 1011
  ]
  edge [
    source 26
    target 1012
  ]
  edge [
    source 26
    target 1013
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 691
  ]
  edge [
    source 27
    target 1014
  ]
  edge [
    source 27
    target 1015
  ]
  edge [
    source 27
    target 754
  ]
  edge [
    source 27
    target 1016
  ]
  edge [
    source 27
    target 1017
  ]
  edge [
    source 27
    target 793
  ]
  edge [
    source 27
    target 1018
  ]
  edge [
    source 27
    target 1019
  ]
  edge [
    source 27
    target 926
  ]
  edge [
    source 27
    target 1020
  ]
  edge [
    source 27
    target 1021
  ]
  edge [
    source 27
    target 1022
  ]
  edge [
    source 27
    target 1023
  ]
  edge [
    source 27
    target 1024
  ]
  edge [
    source 28
    target 905
  ]
  edge [
    source 28
    target 1015
  ]
  edge [
    source 28
    target 1025
  ]
  edge [
    source 28
    target 754
  ]
  edge [
    source 28
    target 1016
  ]
  edge [
    source 28
    target 1017
  ]
  edge [
    source 28
    target 793
  ]
  edge [
    source 28
    target 1018
  ]
  edge [
    source 28
    target 1019
  ]
  edge [
    source 28
    target 926
  ]
  edge [
    source 28
    target 1026
  ]
  edge [
    source 28
    target 1027
  ]
  edge [
    source 28
    target 1028
  ]
  edge [
    source 28
    target 1029
  ]
  edge [
    source 28
    target 1030
  ]
  edge [
    source 28
    target 1031
  ]
  edge [
    source 28
    target 1032
  ]
  edge [
    source 28
    target 1033
  ]
  edge [
    source 28
    target 1034
  ]
  edge [
    source 28
    target 1035
  ]
]
