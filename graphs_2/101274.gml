graph [
  node [
    id 0
    label "serowy"
    origin "text"
  ]
  node [
    id 1
    label "ciep&#322;y"
  ]
  node [
    id 2
    label "mi&#322;y"
  ]
  node [
    id 3
    label "ocieplanie_si&#281;"
  ]
  node [
    id 4
    label "ocieplanie"
  ]
  node [
    id 5
    label "grzanie"
  ]
  node [
    id 6
    label "ocieplenie_si&#281;"
  ]
  node [
    id 7
    label "zagrzanie"
  ]
  node [
    id 8
    label "ocieplenie"
  ]
  node [
    id 9
    label "korzystny"
  ]
  node [
    id 10
    label "przyjemny"
  ]
  node [
    id 11
    label "ciep&#322;o"
  ]
  node [
    id 12
    label "dobry"
  ]
  node [
    id 13
    label "dystrykt"
  ]
  node [
    id 14
    label "centralny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 13
    target 14
  ]
]
