graph [
  node [
    id 0
    label "osiek"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "o&#347;wi&#281;cimski"
    origin "text"
  ]
  node [
    id 3
    label "wojew&#243;dztwo"
  ]
  node [
    id 4
    label "gmina"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "urz&#261;d"
  ]
  node [
    id 7
    label "Karlsbad"
  ]
  node [
    id 8
    label "Dobro&#324;"
  ]
  node [
    id 9
    label "rada_gminy"
  ]
  node [
    id 10
    label "Wielka_Wie&#347;"
  ]
  node [
    id 11
    label "radny"
  ]
  node [
    id 12
    label "organizacja_religijna"
  ]
  node [
    id 13
    label "Biskupice"
  ]
  node [
    id 14
    label "mikroregion"
  ]
  node [
    id 15
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 16
    label "pa&#324;stwo"
  ]
  node [
    id 17
    label "makroregion"
  ]
  node [
    id 18
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 19
    label "typowy"
  ]
  node [
    id 20
    label "po_o&#347;wi&#281;cimsku"
  ]
  node [
    id 21
    label "obozowy"
  ]
  node [
    id 22
    label "ma&#322;opolski"
  ]
  node [
    id 23
    label "polski"
  ]
  node [
    id 24
    label "sznycel"
  ]
  node [
    id 25
    label "po_ma&#322;opolsku"
  ]
  node [
    id 26
    label "regionalny"
  ]
  node [
    id 27
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 28
    label "typowo"
  ]
  node [
    id 29
    label "zwyk&#322;y"
  ]
  node [
    id 30
    label "zwyczajny"
  ]
  node [
    id 31
    label "cz&#281;sty"
  ]
  node [
    id 32
    label "&#347;wi&#281;ty"
  ]
  node [
    id 33
    label "ojciec"
  ]
  node [
    id 34
    label "Pio"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 34
  ]
]
