graph [
  node [
    id 0
    label "straci&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ostatni"
    origin "text"
  ]
  node [
    id 2
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 3
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 4
    label "dosy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "g&#322;upi"
    origin "text"
  ]
  node [
    id 6
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 7
    label "ten"
    origin "text"
  ]
  node [
    id 8
    label "rok"
    origin "text"
  ]
  node [
    id 9
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 10
    label "przepatrze&#263;"
    origin "text"
  ]
  node [
    id 11
    label "spokojnie"
    origin "text"
  ]
  node [
    id 12
    label "jak"
    origin "text"
  ]
  node [
    id 13
    label "ga&#378;dzina"
    origin "text"
  ]
  node [
    id 14
    label "oszukiwa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 16
    label "poza"
    origin "text"
  ]
  node [
    id 17
    label "oko"
    origin "text"
  ]
  node [
    id 18
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 21
    label "ca&#322;kiem"
    origin "text"
  ]
  node [
    id 22
    label "bezradny"
    origin "text"
  ]
  node [
    id 23
    label "jeszcze"
    origin "text"
  ]
  node [
    id 24
    label "bardzo"
    origin "text"
  ]
  node [
    id 25
    label "wszystko"
    origin "text"
  ]
  node [
    id 26
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 27
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "zwykn&#261;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 30
    label "tak"
    origin "text"
  ]
  node [
    id 31
    label "pomiesza&#263;"
    origin "text"
  ]
  node [
    id 32
    label "znak"
    origin "text"
  ]
  node [
    id 33
    label "omami&#263;"
    origin "text"
  ]
  node [
    id 34
    label "mg&#322;a"
    origin "text"
  ]
  node [
    id 35
    label "siad&#322;o"
    origin "text"
  ]
  node [
    id 36
    label "ujrze&#263;"
    origin "text"
  ]
  node [
    id 37
    label "niby"
    origin "text"
  ]
  node [
    id 38
    label "przed"
    origin "text"
  ]
  node [
    id 39
    label "czarna"
    origin "text"
  ]
  node [
    id 40
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 41
    label "przebi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "czu&#263;"
    origin "text"
  ]
  node [
    id 43
    label "przy"
    origin "text"
  ]
  node [
    id 44
    label "tym"
    origin "text"
  ]
  node [
    id 45
    label "rozwidni&#263;"
    origin "text"
  ]
  node [
    id 46
    label "potem"
    origin "text"
  ]
  node [
    id 47
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 48
    label "sen"
    origin "text"
  ]
  node [
    id 49
    label "stracenie"
  ]
  node [
    id 50
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 51
    label "leave_office"
  ]
  node [
    id 52
    label "zabi&#263;"
  ]
  node [
    id 53
    label "forfeit"
  ]
  node [
    id 54
    label "wytraci&#263;"
  ]
  node [
    id 55
    label "waste"
  ]
  node [
    id 56
    label "spowodowa&#263;"
  ]
  node [
    id 57
    label "przegra&#263;"
  ]
  node [
    id 58
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 59
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 60
    label "execute"
  ]
  node [
    id 61
    label "omin&#261;&#263;"
  ]
  node [
    id 62
    label "act"
  ]
  node [
    id 63
    label "ponie&#347;&#263;"
  ]
  node [
    id 64
    label "play"
  ]
  node [
    id 65
    label "zadzwoni&#263;"
  ]
  node [
    id 66
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 67
    label "skarci&#263;"
  ]
  node [
    id 68
    label "skrzywdzi&#263;"
  ]
  node [
    id 69
    label "os&#322;oni&#263;"
  ]
  node [
    id 70
    label "przybi&#263;"
  ]
  node [
    id 71
    label "rozbroi&#263;"
  ]
  node [
    id 72
    label "uderzy&#263;"
  ]
  node [
    id 73
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 74
    label "skrzywi&#263;"
  ]
  node [
    id 75
    label "dispatch"
  ]
  node [
    id 76
    label "zmordowa&#263;"
  ]
  node [
    id 77
    label "zakry&#263;"
  ]
  node [
    id 78
    label "zbi&#263;"
  ]
  node [
    id 79
    label "zapulsowa&#263;"
  ]
  node [
    id 80
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 81
    label "break"
  ]
  node [
    id 82
    label "zastrzeli&#263;"
  ]
  node [
    id 83
    label "u&#347;mierci&#263;"
  ]
  node [
    id 84
    label "zwalczy&#263;"
  ]
  node [
    id 85
    label "pomacha&#263;"
  ]
  node [
    id 86
    label "kill"
  ]
  node [
    id 87
    label "zako&#324;czy&#263;"
  ]
  node [
    id 88
    label "zniszczy&#263;"
  ]
  node [
    id 89
    label "pomin&#261;&#263;"
  ]
  node [
    id 90
    label "wymin&#261;&#263;"
  ]
  node [
    id 91
    label "sidestep"
  ]
  node [
    id 92
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 93
    label "unikn&#261;&#263;"
  ]
  node [
    id 94
    label "przej&#347;&#263;"
  ]
  node [
    id 95
    label "obej&#347;&#263;"
  ]
  node [
    id 96
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 97
    label "opu&#347;ci&#263;"
  ]
  node [
    id 98
    label "shed"
  ]
  node [
    id 99
    label "rozstrzela&#263;"
  ]
  node [
    id 100
    label "rozstrzeliwa&#263;"
  ]
  node [
    id 101
    label "przegranie"
  ]
  node [
    id 102
    label "rozstrzeliwanie"
  ]
  node [
    id 103
    label "wytracenie"
  ]
  node [
    id 104
    label "przep&#322;acenie"
  ]
  node [
    id 105
    label "rozstrzelanie"
  ]
  node [
    id 106
    label "zdarzenie_si&#281;"
  ]
  node [
    id 107
    label "pogorszenie_si&#281;"
  ]
  node [
    id 108
    label "pomarnowanie"
  ]
  node [
    id 109
    label "potracenie"
  ]
  node [
    id 110
    label "tracenie"
  ]
  node [
    id 111
    label "performance"
  ]
  node [
    id 112
    label "zabicie"
  ]
  node [
    id 113
    label "pozabija&#263;"
  ]
  node [
    id 114
    label "kolejny"
  ]
  node [
    id 115
    label "cz&#322;owiek"
  ]
  node [
    id 116
    label "niedawno"
  ]
  node [
    id 117
    label "poprzedni"
  ]
  node [
    id 118
    label "pozosta&#322;y"
  ]
  node [
    id 119
    label "ostatnio"
  ]
  node [
    id 120
    label "sko&#324;czony"
  ]
  node [
    id 121
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 122
    label "aktualny"
  ]
  node [
    id 123
    label "najgorszy"
  ]
  node [
    id 124
    label "istota_&#380;ywa"
  ]
  node [
    id 125
    label "w&#261;tpliwy"
  ]
  node [
    id 126
    label "nast&#281;pnie"
  ]
  node [
    id 127
    label "inny"
  ]
  node [
    id 128
    label "nastopny"
  ]
  node [
    id 129
    label "kolejno"
  ]
  node [
    id 130
    label "kt&#243;ry&#347;"
  ]
  node [
    id 131
    label "przesz&#322;y"
  ]
  node [
    id 132
    label "wcze&#347;niejszy"
  ]
  node [
    id 133
    label "poprzednio"
  ]
  node [
    id 134
    label "w&#261;tpliwie"
  ]
  node [
    id 135
    label "pozorny"
  ]
  node [
    id 136
    label "&#380;ywy"
  ]
  node [
    id 137
    label "ostateczny"
  ]
  node [
    id 138
    label "wa&#380;ny"
  ]
  node [
    id 139
    label "ludzko&#347;&#263;"
  ]
  node [
    id 140
    label "asymilowanie"
  ]
  node [
    id 141
    label "wapniak"
  ]
  node [
    id 142
    label "asymilowa&#263;"
  ]
  node [
    id 143
    label "os&#322;abia&#263;"
  ]
  node [
    id 144
    label "posta&#263;"
  ]
  node [
    id 145
    label "hominid"
  ]
  node [
    id 146
    label "podw&#322;adny"
  ]
  node [
    id 147
    label "os&#322;abianie"
  ]
  node [
    id 148
    label "g&#322;owa"
  ]
  node [
    id 149
    label "figura"
  ]
  node [
    id 150
    label "portrecista"
  ]
  node [
    id 151
    label "dwun&#243;g"
  ]
  node [
    id 152
    label "profanum"
  ]
  node [
    id 153
    label "mikrokosmos"
  ]
  node [
    id 154
    label "nasada"
  ]
  node [
    id 155
    label "duch"
  ]
  node [
    id 156
    label "antropochoria"
  ]
  node [
    id 157
    label "osoba"
  ]
  node [
    id 158
    label "wz&#243;r"
  ]
  node [
    id 159
    label "senior"
  ]
  node [
    id 160
    label "oddzia&#322;ywanie"
  ]
  node [
    id 161
    label "Adam"
  ]
  node [
    id 162
    label "homo_sapiens"
  ]
  node [
    id 163
    label "polifag"
  ]
  node [
    id 164
    label "wykszta&#322;cony"
  ]
  node [
    id 165
    label "dyplomowany"
  ]
  node [
    id 166
    label "wykwalifikowany"
  ]
  node [
    id 167
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 168
    label "kompletny"
  ]
  node [
    id 169
    label "sko&#324;czenie"
  ]
  node [
    id 170
    label "okre&#347;lony"
  ]
  node [
    id 171
    label "wielki"
  ]
  node [
    id 172
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 173
    label "aktualnie"
  ]
  node [
    id 174
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 175
    label "aktualizowanie"
  ]
  node [
    id 176
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 177
    label "uaktualnienie"
  ]
  node [
    id 178
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 179
    label "zesp&#243;&#322;"
  ]
  node [
    id 180
    label "instytucja"
  ]
  node [
    id 181
    label "wys&#322;uga"
  ]
  node [
    id 182
    label "service"
  ]
  node [
    id 183
    label "czworak"
  ]
  node [
    id 184
    label "ZOMO"
  ]
  node [
    id 185
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 186
    label "praca"
  ]
  node [
    id 187
    label "moneta"
  ]
  node [
    id 188
    label "szesnastowieczny"
  ]
  node [
    id 189
    label "dom_wielorodzinny"
  ]
  node [
    id 190
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 191
    label "Mazowsze"
  ]
  node [
    id 192
    label "odm&#322;adzanie"
  ]
  node [
    id 193
    label "&#346;wietliki"
  ]
  node [
    id 194
    label "zbi&#243;r"
  ]
  node [
    id 195
    label "whole"
  ]
  node [
    id 196
    label "skupienie"
  ]
  node [
    id 197
    label "The_Beatles"
  ]
  node [
    id 198
    label "odm&#322;adza&#263;"
  ]
  node [
    id 199
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 200
    label "zabudowania"
  ]
  node [
    id 201
    label "group"
  ]
  node [
    id 202
    label "zespolik"
  ]
  node [
    id 203
    label "schorzenie"
  ]
  node [
    id 204
    label "ro&#347;lina"
  ]
  node [
    id 205
    label "grupa"
  ]
  node [
    id 206
    label "Depeche_Mode"
  ]
  node [
    id 207
    label "batch"
  ]
  node [
    id 208
    label "odm&#322;odzenie"
  ]
  node [
    id 209
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 210
    label "najem"
  ]
  node [
    id 211
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 212
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 213
    label "zak&#322;ad"
  ]
  node [
    id 214
    label "stosunek_pracy"
  ]
  node [
    id 215
    label "benedykty&#324;ski"
  ]
  node [
    id 216
    label "poda&#380;_pracy"
  ]
  node [
    id 217
    label "pracowanie"
  ]
  node [
    id 218
    label "tyrka"
  ]
  node [
    id 219
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 220
    label "wytw&#243;r"
  ]
  node [
    id 221
    label "miejsce"
  ]
  node [
    id 222
    label "zaw&#243;d"
  ]
  node [
    id 223
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 224
    label "tynkarski"
  ]
  node [
    id 225
    label "pracowa&#263;"
  ]
  node [
    id 226
    label "czynno&#347;&#263;"
  ]
  node [
    id 227
    label "zmiana"
  ]
  node [
    id 228
    label "czynnik_produkcji"
  ]
  node [
    id 229
    label "zobowi&#261;zanie"
  ]
  node [
    id 230
    label "kierownictwo"
  ]
  node [
    id 231
    label "siedziba"
  ]
  node [
    id 232
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 233
    label "osoba_prawna"
  ]
  node [
    id 234
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 235
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 236
    label "poj&#281;cie"
  ]
  node [
    id 237
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 238
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 239
    label "biuro"
  ]
  node [
    id 240
    label "organizacja"
  ]
  node [
    id 241
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 242
    label "Fundusze_Unijne"
  ]
  node [
    id 243
    label "zamyka&#263;"
  ]
  node [
    id 244
    label "establishment"
  ]
  node [
    id 245
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 246
    label "urz&#261;d"
  ]
  node [
    id 247
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 248
    label "afiliowa&#263;"
  ]
  node [
    id 249
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 250
    label "standard"
  ]
  node [
    id 251
    label "zamykanie"
  ]
  node [
    id 252
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 253
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 254
    label "ochmistrzyni"
  ]
  node [
    id 255
    label "s&#322;u&#380;ebnica"
  ]
  node [
    id 256
    label "s&#322;uga"
  ]
  node [
    id 257
    label "s&#322;u&#380;ebnik"
  ]
  node [
    id 258
    label "milicja_obywatelska"
  ]
  node [
    id 259
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 260
    label "subject"
  ]
  node [
    id 261
    label "czynnik"
  ]
  node [
    id 262
    label "matuszka"
  ]
  node [
    id 263
    label "poci&#261;ganie"
  ]
  node [
    id 264
    label "rezultat"
  ]
  node [
    id 265
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 266
    label "przyczyna"
  ]
  node [
    id 267
    label "geneza"
  ]
  node [
    id 268
    label "strona"
  ]
  node [
    id 269
    label "uprz&#261;&#380;"
  ]
  node [
    id 270
    label "kartka"
  ]
  node [
    id 271
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 272
    label "logowanie"
  ]
  node [
    id 273
    label "plik"
  ]
  node [
    id 274
    label "s&#261;d"
  ]
  node [
    id 275
    label "adres_internetowy"
  ]
  node [
    id 276
    label "linia"
  ]
  node [
    id 277
    label "serwis_internetowy"
  ]
  node [
    id 278
    label "bok"
  ]
  node [
    id 279
    label "skr&#281;canie"
  ]
  node [
    id 280
    label "skr&#281;ca&#263;"
  ]
  node [
    id 281
    label "orientowanie"
  ]
  node [
    id 282
    label "skr&#281;ci&#263;"
  ]
  node [
    id 283
    label "uj&#281;cie"
  ]
  node [
    id 284
    label "zorientowanie"
  ]
  node [
    id 285
    label "ty&#322;"
  ]
  node [
    id 286
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 287
    label "fragment"
  ]
  node [
    id 288
    label "layout"
  ]
  node [
    id 289
    label "obiekt"
  ]
  node [
    id 290
    label "zorientowa&#263;"
  ]
  node [
    id 291
    label "pagina"
  ]
  node [
    id 292
    label "podmiot"
  ]
  node [
    id 293
    label "g&#243;ra"
  ]
  node [
    id 294
    label "orientowa&#263;"
  ]
  node [
    id 295
    label "voice"
  ]
  node [
    id 296
    label "orientacja"
  ]
  node [
    id 297
    label "prz&#243;d"
  ]
  node [
    id 298
    label "internet"
  ]
  node [
    id 299
    label "powierzchnia"
  ]
  node [
    id 300
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 301
    label "forma"
  ]
  node [
    id 302
    label "skr&#281;cenie"
  ]
  node [
    id 303
    label "u&#378;dzienica"
  ]
  node [
    id 304
    label "postronek"
  ]
  node [
    id 305
    label "uzda"
  ]
  node [
    id 306
    label "chom&#261;to"
  ]
  node [
    id 307
    label "naszelnik"
  ]
  node [
    id 308
    label "nakarcznik"
  ]
  node [
    id 309
    label "janczary"
  ]
  node [
    id 310
    label "moderunek"
  ]
  node [
    id 311
    label "podogonie"
  ]
  node [
    id 312
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 313
    label "divisor"
  ]
  node [
    id 314
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 315
    label "faktor"
  ]
  node [
    id 316
    label "agent"
  ]
  node [
    id 317
    label "ekspozycja"
  ]
  node [
    id 318
    label "iloczyn"
  ]
  node [
    id 319
    label "popadia"
  ]
  node [
    id 320
    label "ojczyzna"
  ]
  node [
    id 321
    label "proces"
  ]
  node [
    id 322
    label "rodny"
  ]
  node [
    id 323
    label "powstanie"
  ]
  node [
    id 324
    label "monogeneza"
  ]
  node [
    id 325
    label "zaistnienie"
  ]
  node [
    id 326
    label "give"
  ]
  node [
    id 327
    label "pocz&#261;tek"
  ]
  node [
    id 328
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 329
    label "upicie"
  ]
  node [
    id 330
    label "pull"
  ]
  node [
    id 331
    label "move"
  ]
  node [
    id 332
    label "ruszenie"
  ]
  node [
    id 333
    label "wyszarpanie"
  ]
  node [
    id 334
    label "pokrycie"
  ]
  node [
    id 335
    label "myk"
  ]
  node [
    id 336
    label "wywo&#322;anie"
  ]
  node [
    id 337
    label "si&#261;kanie"
  ]
  node [
    id 338
    label "zainstalowanie"
  ]
  node [
    id 339
    label "przechylenie"
  ]
  node [
    id 340
    label "przesuni&#281;cie"
  ]
  node [
    id 341
    label "zaci&#261;ganie"
  ]
  node [
    id 342
    label "wessanie"
  ]
  node [
    id 343
    label "powianie"
  ]
  node [
    id 344
    label "posuni&#281;cie"
  ]
  node [
    id 345
    label "p&#243;j&#347;cie"
  ]
  node [
    id 346
    label "nos"
  ]
  node [
    id 347
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 348
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 349
    label "dzia&#322;anie"
  ]
  node [
    id 350
    label "typ"
  ]
  node [
    id 351
    label "event"
  ]
  node [
    id 352
    label "implikacja"
  ]
  node [
    id 353
    label "powodowanie"
  ]
  node [
    id 354
    label "powiewanie"
  ]
  node [
    id 355
    label "powleczenie"
  ]
  node [
    id 356
    label "interesowanie"
  ]
  node [
    id 357
    label "manienie"
  ]
  node [
    id 358
    label "upijanie"
  ]
  node [
    id 359
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 360
    label "przechylanie"
  ]
  node [
    id 361
    label "temptation"
  ]
  node [
    id 362
    label "pokrywanie"
  ]
  node [
    id 363
    label "oddzieranie"
  ]
  node [
    id 364
    label "dzianie_si&#281;"
  ]
  node [
    id 365
    label "urwanie"
  ]
  node [
    id 366
    label "oddarcie"
  ]
  node [
    id 367
    label "przesuwanie"
  ]
  node [
    id 368
    label "zerwanie"
  ]
  node [
    id 369
    label "ruszanie"
  ]
  node [
    id 370
    label "traction"
  ]
  node [
    id 371
    label "urywanie"
  ]
  node [
    id 372
    label "powlekanie"
  ]
  node [
    id 373
    label "wsysanie"
  ]
  node [
    id 374
    label "&#347;mieszny"
  ]
  node [
    id 375
    label "bezwolny"
  ]
  node [
    id 376
    label "g&#322;upienie"
  ]
  node [
    id 377
    label "mondzio&#322;"
  ]
  node [
    id 378
    label "niewa&#380;ny"
  ]
  node [
    id 379
    label "bezmy&#347;lny"
  ]
  node [
    id 380
    label "bezsensowny"
  ]
  node [
    id 381
    label "nadaremny"
  ]
  node [
    id 382
    label "niem&#261;dry"
  ]
  node [
    id 383
    label "nierozwa&#380;ny"
  ]
  node [
    id 384
    label "niezr&#281;czny"
  ]
  node [
    id 385
    label "g&#322;uptas"
  ]
  node [
    id 386
    label "zg&#322;upienie"
  ]
  node [
    id 387
    label "g&#322;upiec"
  ]
  node [
    id 388
    label "uprzykrzony"
  ]
  node [
    id 389
    label "bezcelowy"
  ]
  node [
    id 390
    label "ma&#322;y"
  ]
  node [
    id 391
    label "g&#322;upio"
  ]
  node [
    id 392
    label "bezmy&#347;lnie"
  ]
  node [
    id 393
    label "nieswojo"
  ]
  node [
    id 394
    label "niem&#261;drze"
  ]
  node [
    id 395
    label "bezcelowo"
  ]
  node [
    id 396
    label "nierozwa&#380;nie"
  ]
  node [
    id 397
    label "niezr&#281;cznie"
  ]
  node [
    id 398
    label "nadaremnie"
  ]
  node [
    id 399
    label "bezsensownie"
  ]
  node [
    id 400
    label "durnienie"
  ]
  node [
    id 401
    label "czucie"
  ]
  node [
    id 402
    label "g&#322;upek"
  ]
  node [
    id 403
    label "stawanie_si&#281;"
  ]
  node [
    id 404
    label "hebetude"
  ]
  node [
    id 405
    label "zdurnienie"
  ]
  node [
    id 406
    label "os&#322;upienie"
  ]
  node [
    id 407
    label "stanie_si&#281;"
  ]
  node [
    id 408
    label "przekl&#281;ty"
  ]
  node [
    id 409
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 410
    label "nieszcz&#281;sny"
  ]
  node [
    id 411
    label "pos&#322;uszny"
  ]
  node [
    id 412
    label "uleg&#322;y"
  ]
  node [
    id 413
    label "poniewolny"
  ]
  node [
    id 414
    label "bezwolnie"
  ]
  node [
    id 415
    label "ja&#322;owy"
  ]
  node [
    id 416
    label "nieskuteczny"
  ]
  node [
    id 417
    label "bezkrytyczny"
  ]
  node [
    id 418
    label "bezm&#243;zgi"
  ]
  node [
    id 419
    label "nierozumny"
  ]
  node [
    id 420
    label "bezrefleksyjny"
  ]
  node [
    id 421
    label "bezwiedny"
  ]
  node [
    id 422
    label "bezsensowy"
  ]
  node [
    id 423
    label "nielogiczny"
  ]
  node [
    id 424
    label "niezrozumia&#322;y"
  ]
  node [
    id 425
    label "nieuzasadniony"
  ]
  node [
    id 426
    label "p&#322;onny"
  ]
  node [
    id 427
    label "niekonstruktywny"
  ]
  node [
    id 428
    label "zb&#281;dny"
  ]
  node [
    id 429
    label "z&#322;y"
  ]
  node [
    id 430
    label "szybki"
  ]
  node [
    id 431
    label "nieznaczny"
  ]
  node [
    id 432
    label "przeci&#281;tny"
  ]
  node [
    id 433
    label "wstydliwy"
  ]
  node [
    id 434
    label "s&#322;aby"
  ]
  node [
    id 435
    label "ch&#322;opiec"
  ]
  node [
    id 436
    label "m&#322;ody"
  ]
  node [
    id 437
    label "ma&#322;o"
  ]
  node [
    id 438
    label "marny"
  ]
  node [
    id 439
    label "nieliczny"
  ]
  node [
    id 440
    label "n&#281;dznie"
  ]
  node [
    id 441
    label "uniewa&#380;nianie_si&#281;"
  ]
  node [
    id 442
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 443
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 444
    label "nieistotnie"
  ]
  node [
    id 445
    label "niepowa&#380;ny"
  ]
  node [
    id 446
    label "o&#347;mieszanie"
  ]
  node [
    id 447
    label "&#347;miesznie"
  ]
  node [
    id 448
    label "bawny"
  ]
  node [
    id 449
    label "o&#347;mieszenie"
  ]
  node [
    id 450
    label "dziwny"
  ]
  node [
    id 451
    label "nieadekwatny"
  ]
  node [
    id 452
    label "nieumiej&#281;tny"
  ]
  node [
    id 453
    label "k&#322;opotliwy"
  ]
  node [
    id 454
    label "nieudany"
  ]
  node [
    id 455
    label "niewygodny"
  ]
  node [
    id 456
    label "niestosowny"
  ]
  node [
    id 457
    label "niezgrabnie"
  ]
  node [
    id 458
    label "poczciwiec"
  ]
  node [
    id 459
    label "by&#263;"
  ]
  node [
    id 460
    label "gotowy"
  ]
  node [
    id 461
    label "might"
  ]
  node [
    id 462
    label "uprawi&#263;"
  ]
  node [
    id 463
    label "public_treasury"
  ]
  node [
    id 464
    label "pole"
  ]
  node [
    id 465
    label "obrobi&#263;"
  ]
  node [
    id 466
    label "nietrze&#378;wy"
  ]
  node [
    id 467
    label "czekanie"
  ]
  node [
    id 468
    label "martwy"
  ]
  node [
    id 469
    label "bliski"
  ]
  node [
    id 470
    label "gotowo"
  ]
  node [
    id 471
    label "przygotowywanie"
  ]
  node [
    id 472
    label "przygotowanie"
  ]
  node [
    id 473
    label "dyspozycyjny"
  ]
  node [
    id 474
    label "zalany"
  ]
  node [
    id 475
    label "nieuchronny"
  ]
  node [
    id 476
    label "doj&#347;cie"
  ]
  node [
    id 477
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 478
    label "mie&#263;_miejsce"
  ]
  node [
    id 479
    label "equal"
  ]
  node [
    id 480
    label "trwa&#263;"
  ]
  node [
    id 481
    label "chodzi&#263;"
  ]
  node [
    id 482
    label "si&#281;ga&#263;"
  ]
  node [
    id 483
    label "stan"
  ]
  node [
    id 484
    label "obecno&#347;&#263;"
  ]
  node [
    id 485
    label "stand"
  ]
  node [
    id 486
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 487
    label "uczestniczy&#263;"
  ]
  node [
    id 488
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 489
    label "wiadomy"
  ]
  node [
    id 490
    label "p&#243;&#322;rocze"
  ]
  node [
    id 491
    label "martwy_sezon"
  ]
  node [
    id 492
    label "kalendarz"
  ]
  node [
    id 493
    label "cykl_astronomiczny"
  ]
  node [
    id 494
    label "lata"
  ]
  node [
    id 495
    label "pora_roku"
  ]
  node [
    id 496
    label "stulecie"
  ]
  node [
    id 497
    label "kurs"
  ]
  node [
    id 498
    label "czas"
  ]
  node [
    id 499
    label "jubileusz"
  ]
  node [
    id 500
    label "kwarta&#322;"
  ]
  node [
    id 501
    label "miesi&#261;c"
  ]
  node [
    id 502
    label "summer"
  ]
  node [
    id 503
    label "liga"
  ]
  node [
    id 504
    label "jednostka_systematyczna"
  ]
  node [
    id 505
    label "gromada"
  ]
  node [
    id 506
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 507
    label "egzemplarz"
  ]
  node [
    id 508
    label "Entuzjastki"
  ]
  node [
    id 509
    label "kompozycja"
  ]
  node [
    id 510
    label "Terranie"
  ]
  node [
    id 511
    label "category"
  ]
  node [
    id 512
    label "pakiet_klimatyczny"
  ]
  node [
    id 513
    label "oddzia&#322;"
  ]
  node [
    id 514
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 515
    label "cz&#261;steczka"
  ]
  node [
    id 516
    label "stage_set"
  ]
  node [
    id 517
    label "type"
  ]
  node [
    id 518
    label "specgrupa"
  ]
  node [
    id 519
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 520
    label "Eurogrupa"
  ]
  node [
    id 521
    label "formacja_geologiczna"
  ]
  node [
    id 522
    label "harcerze_starsi"
  ]
  node [
    id 523
    label "poprzedzanie"
  ]
  node [
    id 524
    label "czasoprzestrze&#324;"
  ]
  node [
    id 525
    label "laba"
  ]
  node [
    id 526
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 527
    label "chronometria"
  ]
  node [
    id 528
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 529
    label "rachuba_czasu"
  ]
  node [
    id 530
    label "przep&#322;ywanie"
  ]
  node [
    id 531
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 532
    label "czasokres"
  ]
  node [
    id 533
    label "odczyt"
  ]
  node [
    id 534
    label "chwila"
  ]
  node [
    id 535
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 536
    label "dzieje"
  ]
  node [
    id 537
    label "kategoria_gramatyczna"
  ]
  node [
    id 538
    label "poprzedzenie"
  ]
  node [
    id 539
    label "trawienie"
  ]
  node [
    id 540
    label "pochodzi&#263;"
  ]
  node [
    id 541
    label "period"
  ]
  node [
    id 542
    label "okres_czasu"
  ]
  node [
    id 543
    label "poprzedza&#263;"
  ]
  node [
    id 544
    label "schy&#322;ek"
  ]
  node [
    id 545
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 546
    label "odwlekanie_si&#281;"
  ]
  node [
    id 547
    label "zegar"
  ]
  node [
    id 548
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 549
    label "czwarty_wymiar"
  ]
  node [
    id 550
    label "pochodzenie"
  ]
  node [
    id 551
    label "koniugacja"
  ]
  node [
    id 552
    label "Zeitgeist"
  ]
  node [
    id 553
    label "trawi&#263;"
  ]
  node [
    id 554
    label "pogoda"
  ]
  node [
    id 555
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 556
    label "poprzedzi&#263;"
  ]
  node [
    id 557
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 558
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 559
    label "time_period"
  ]
  node [
    id 560
    label "term"
  ]
  node [
    id 561
    label "rok_akademicki"
  ]
  node [
    id 562
    label "rok_szkolny"
  ]
  node [
    id 563
    label "semester"
  ]
  node [
    id 564
    label "anniwersarz"
  ]
  node [
    id 565
    label "rocznica"
  ]
  node [
    id 566
    label "obszar"
  ]
  node [
    id 567
    label "tydzie&#324;"
  ]
  node [
    id 568
    label "miech"
  ]
  node [
    id 569
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 570
    label "kalendy"
  ]
  node [
    id 571
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 572
    label "long_time"
  ]
  node [
    id 573
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 574
    label "almanac"
  ]
  node [
    id 575
    label "rozk&#322;ad"
  ]
  node [
    id 576
    label "wydawnictwo"
  ]
  node [
    id 577
    label "Juliusz_Cezar"
  ]
  node [
    id 578
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 579
    label "zwy&#380;kowanie"
  ]
  node [
    id 580
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 581
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 582
    label "zaj&#281;cia"
  ]
  node [
    id 583
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 584
    label "trasa"
  ]
  node [
    id 585
    label "przeorientowywanie"
  ]
  node [
    id 586
    label "przejazd"
  ]
  node [
    id 587
    label "kierunek"
  ]
  node [
    id 588
    label "przeorientowywa&#263;"
  ]
  node [
    id 589
    label "nauka"
  ]
  node [
    id 590
    label "przeorientowanie"
  ]
  node [
    id 591
    label "klasa"
  ]
  node [
    id 592
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 593
    label "przeorientowa&#263;"
  ]
  node [
    id 594
    label "manner"
  ]
  node [
    id 595
    label "course"
  ]
  node [
    id 596
    label "passage"
  ]
  node [
    id 597
    label "zni&#380;kowanie"
  ]
  node [
    id 598
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 599
    label "seria"
  ]
  node [
    id 600
    label "stawka"
  ]
  node [
    id 601
    label "way"
  ]
  node [
    id 602
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 603
    label "spos&#243;b"
  ]
  node [
    id 604
    label "deprecjacja"
  ]
  node [
    id 605
    label "cedu&#322;a"
  ]
  node [
    id 606
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 607
    label "drive"
  ]
  node [
    id 608
    label "bearing"
  ]
  node [
    id 609
    label "Lira"
  ]
  node [
    id 610
    label "przyzwoity"
  ]
  node [
    id 611
    label "ciekawy"
  ]
  node [
    id 612
    label "jako&#347;"
  ]
  node [
    id 613
    label "jako_tako"
  ]
  node [
    id 614
    label "niez&#322;y"
  ]
  node [
    id 615
    label "charakterystyczny"
  ]
  node [
    id 616
    label "intensywny"
  ]
  node [
    id 617
    label "udolny"
  ]
  node [
    id 618
    label "skuteczny"
  ]
  node [
    id 619
    label "niczegowaty"
  ]
  node [
    id 620
    label "dobrze"
  ]
  node [
    id 621
    label "nieszpetny"
  ]
  node [
    id 622
    label "spory"
  ]
  node [
    id 623
    label "pozytywny"
  ]
  node [
    id 624
    label "korzystny"
  ]
  node [
    id 625
    label "nie&#378;le"
  ]
  node [
    id 626
    label "skromny"
  ]
  node [
    id 627
    label "kulturalny"
  ]
  node [
    id 628
    label "grzeczny"
  ]
  node [
    id 629
    label "stosowny"
  ]
  node [
    id 630
    label "przystojny"
  ]
  node [
    id 631
    label "nale&#380;yty"
  ]
  node [
    id 632
    label "moralny"
  ]
  node [
    id 633
    label "przyzwoicie"
  ]
  node [
    id 634
    label "wystarczaj&#261;cy"
  ]
  node [
    id 635
    label "nietuzinkowy"
  ]
  node [
    id 636
    label "intryguj&#261;cy"
  ]
  node [
    id 637
    label "ch&#281;tny"
  ]
  node [
    id 638
    label "swoisty"
  ]
  node [
    id 639
    label "interesuj&#261;cy"
  ]
  node [
    id 640
    label "ciekawie"
  ]
  node [
    id 641
    label "indagator"
  ]
  node [
    id 642
    label "charakterystycznie"
  ]
  node [
    id 643
    label "szczeg&#243;lny"
  ]
  node [
    id 644
    label "wyj&#261;tkowy"
  ]
  node [
    id 645
    label "typowy"
  ]
  node [
    id 646
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 647
    label "podobny"
  ]
  node [
    id 648
    label "dziwnie"
  ]
  node [
    id 649
    label "dziwy"
  ]
  node [
    id 650
    label "w_miar&#281;"
  ]
  node [
    id 651
    label "jako_taki"
  ]
  node [
    id 652
    label "spokojny"
  ]
  node [
    id 653
    label "bezproblemowo"
  ]
  node [
    id 654
    label "przyjemnie"
  ]
  node [
    id 655
    label "cichy"
  ]
  node [
    id 656
    label "wolno"
  ]
  node [
    id 657
    label "niespiesznie"
  ]
  node [
    id 658
    label "wolny"
  ]
  node [
    id 659
    label "thinly"
  ]
  node [
    id 660
    label "wolniej"
  ]
  node [
    id 661
    label "swobodny"
  ]
  node [
    id 662
    label "wolnie"
  ]
  node [
    id 663
    label "free"
  ]
  node [
    id 664
    label "lu&#378;ny"
  ]
  node [
    id 665
    label "lu&#378;no"
  ]
  node [
    id 666
    label "pleasantly"
  ]
  node [
    id 667
    label "deliciously"
  ]
  node [
    id 668
    label "przyjemny"
  ]
  node [
    id 669
    label "gratifyingly"
  ]
  node [
    id 670
    label "udanie"
  ]
  node [
    id 671
    label "bezproblemowy"
  ]
  node [
    id 672
    label "uspokajanie_si&#281;"
  ]
  node [
    id 673
    label "uspokojenie_si&#281;"
  ]
  node [
    id 674
    label "cicho"
  ]
  node [
    id 675
    label "uspokojenie"
  ]
  node [
    id 676
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 677
    label "nietrudny"
  ]
  node [
    id 678
    label "uspokajanie"
  ]
  node [
    id 679
    label "niezauwa&#380;alny"
  ]
  node [
    id 680
    label "niemy"
  ]
  node [
    id 681
    label "tajemniczy"
  ]
  node [
    id 682
    label "ucichni&#281;cie"
  ]
  node [
    id 683
    label "uciszenie"
  ]
  node [
    id 684
    label "zamazywanie"
  ]
  node [
    id 685
    label "zamazanie"
  ]
  node [
    id 686
    label "trusia"
  ]
  node [
    id 687
    label "uciszanie"
  ]
  node [
    id 688
    label "przycichni&#281;cie"
  ]
  node [
    id 689
    label "podst&#281;pny"
  ]
  node [
    id 690
    label "t&#322;umienie"
  ]
  node [
    id 691
    label "cichni&#281;cie"
  ]
  node [
    id 692
    label "skryty"
  ]
  node [
    id 693
    label "przycichanie"
  ]
  node [
    id 694
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 695
    label "zobo"
  ]
  node [
    id 696
    label "yakalo"
  ]
  node [
    id 697
    label "byd&#322;o"
  ]
  node [
    id 698
    label "dzo"
  ]
  node [
    id 699
    label "kr&#281;torogie"
  ]
  node [
    id 700
    label "czochrad&#322;o"
  ]
  node [
    id 701
    label "posp&#243;lstwo"
  ]
  node [
    id 702
    label "kraal"
  ]
  node [
    id 703
    label "livestock"
  ]
  node [
    id 704
    label "prze&#380;uwacz"
  ]
  node [
    id 705
    label "bizon"
  ]
  node [
    id 706
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 707
    label "zebu"
  ]
  node [
    id 708
    label "byd&#322;o_domowe"
  ]
  node [
    id 709
    label "gazdy"
  ]
  node [
    id 710
    label "m&#281;&#380;atka"
  ]
  node [
    id 711
    label "gospodyni"
  ]
  node [
    id 712
    label "kobieta"
  ]
  node [
    id 713
    label "pomoc_domowa"
  ]
  node [
    id 714
    label "stan_cywilny"
  ]
  node [
    id 715
    label "&#380;ona"
  ]
  node [
    id 716
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 717
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 718
    label "orzyna&#263;"
  ]
  node [
    id 719
    label "oszwabia&#263;"
  ]
  node [
    id 720
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 721
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 722
    label "cheat"
  ]
  node [
    id 723
    label "ogrywa&#263;"
  ]
  node [
    id 724
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 725
    label "trenowa&#263;"
  ]
  node [
    id 726
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 727
    label "opieprza&#263;"
  ]
  node [
    id 728
    label "zje&#380;d&#380;a&#263;"
  ]
  node [
    id 729
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 730
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 731
    label "osacza&#263;"
  ]
  node [
    id 732
    label "wyprzedza&#263;"
  ]
  node [
    id 733
    label "omija&#263;"
  ]
  node [
    id 734
    label "evade"
  ]
  node [
    id 735
    label "przesuwa&#263;"
  ]
  node [
    id 736
    label "zapewnia&#263;"
  ]
  node [
    id 737
    label "wywodzi&#263;"
  ]
  node [
    id 738
    label "przeje&#380;d&#380;a&#263;"
  ]
  node [
    id 739
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 740
    label "za&#322;atwia&#263;"
  ]
  node [
    id 741
    label "umieszcza&#263;"
  ]
  node [
    id 742
    label "wk&#322;ada&#263;"
  ]
  node [
    id 743
    label "ma&#322;&#380;onek"
  ]
  node [
    id 744
    label "m&#243;j"
  ]
  node [
    id 745
    label "ch&#322;op"
  ]
  node [
    id 746
    label "pan_m&#322;ody"
  ]
  node [
    id 747
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 748
    label "&#347;lubny"
  ]
  node [
    id 749
    label "pan_domu"
  ]
  node [
    id 750
    label "pan_i_w&#322;adca"
  ]
  node [
    id 751
    label "stary"
  ]
  node [
    id 752
    label "doros&#322;y"
  ]
  node [
    id 753
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 754
    label "ojciec"
  ]
  node [
    id 755
    label "jegomo&#347;&#263;"
  ]
  node [
    id 756
    label "andropauza"
  ]
  node [
    id 757
    label "pa&#324;stwo"
  ]
  node [
    id 758
    label "bratek"
  ]
  node [
    id 759
    label "ch&#322;opina"
  ]
  node [
    id 760
    label "samiec"
  ]
  node [
    id 761
    label "twardziel"
  ]
  node [
    id 762
    label "androlog"
  ]
  node [
    id 763
    label "partner"
  ]
  node [
    id 764
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 765
    label "para"
  ]
  node [
    id 766
    label "matrymonialny"
  ]
  node [
    id 767
    label "lewirat"
  ]
  node [
    id 768
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 769
    label "sakrament"
  ]
  node [
    id 770
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 771
    label "zwi&#261;zek"
  ]
  node [
    id 772
    label "partia"
  ]
  node [
    id 773
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 774
    label "nienowoczesny"
  ]
  node [
    id 775
    label "gruba_ryba"
  ]
  node [
    id 776
    label "zestarzenie_si&#281;"
  ]
  node [
    id 777
    label "dawno"
  ]
  node [
    id 778
    label "staro"
  ]
  node [
    id 779
    label "starzy"
  ]
  node [
    id 780
    label "dotychczasowy"
  ]
  node [
    id 781
    label "p&#243;&#378;ny"
  ]
  node [
    id 782
    label "d&#322;ugoletni"
  ]
  node [
    id 783
    label "brat"
  ]
  node [
    id 784
    label "po_staro&#347;wiecku"
  ]
  node [
    id 785
    label "zwierzchnik"
  ]
  node [
    id 786
    label "znajomy"
  ]
  node [
    id 787
    label "odleg&#322;y"
  ]
  node [
    id 788
    label "starzenie_si&#281;"
  ]
  node [
    id 789
    label "starczo"
  ]
  node [
    id 790
    label "dawniej"
  ]
  node [
    id 791
    label "niegdysiejszy"
  ]
  node [
    id 792
    label "dojrza&#322;y"
  ]
  node [
    id 793
    label "szlubny"
  ]
  node [
    id 794
    label "&#347;lubnie"
  ]
  node [
    id 795
    label "legalny"
  ]
  node [
    id 796
    label "czyj&#347;"
  ]
  node [
    id 797
    label "uw&#322;aszczy&#263;"
  ]
  node [
    id 798
    label "uw&#322;aszcza&#263;"
  ]
  node [
    id 799
    label "rolnik"
  ]
  node [
    id 800
    label "ch&#322;opstwo"
  ]
  node [
    id 801
    label "cham"
  ]
  node [
    id 802
    label "bamber"
  ]
  node [
    id 803
    label "uw&#322;aszczanie"
  ]
  node [
    id 804
    label "prawo_wychodu"
  ]
  node [
    id 805
    label "w&#322;o&#347;cianin"
  ]
  node [
    id 806
    label "przedstawiciel"
  ]
  node [
    id 807
    label "facet"
  ]
  node [
    id 808
    label "ustawienie"
  ]
  node [
    id 809
    label "mode"
  ]
  node [
    id 810
    label "przesada"
  ]
  node [
    id 811
    label "gra"
  ]
  node [
    id 812
    label "u&#322;o&#380;enie"
  ]
  node [
    id 813
    label "ustalenie"
  ]
  node [
    id 814
    label "erection"
  ]
  node [
    id 815
    label "setup"
  ]
  node [
    id 816
    label "spowodowanie"
  ]
  node [
    id 817
    label "erecting"
  ]
  node [
    id 818
    label "rozmieszczenie"
  ]
  node [
    id 819
    label "poustawianie"
  ]
  node [
    id 820
    label "zinterpretowanie"
  ]
  node [
    id 821
    label "porozstawianie"
  ]
  node [
    id 822
    label "rola"
  ]
  node [
    id 823
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 824
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 825
    label "nadmiar"
  ]
  node [
    id 826
    label "zmienno&#347;&#263;"
  ]
  node [
    id 827
    label "rozgrywka"
  ]
  node [
    id 828
    label "apparent_motion"
  ]
  node [
    id 829
    label "wydarzenie"
  ]
  node [
    id 830
    label "contest"
  ]
  node [
    id 831
    label "akcja"
  ]
  node [
    id 832
    label "komplet"
  ]
  node [
    id 833
    label "zabawa"
  ]
  node [
    id 834
    label "zasada"
  ]
  node [
    id 835
    label "rywalizacja"
  ]
  node [
    id 836
    label "zbijany"
  ]
  node [
    id 837
    label "post&#281;powanie"
  ]
  node [
    id 838
    label "game"
  ]
  node [
    id 839
    label "odg&#322;os"
  ]
  node [
    id 840
    label "Pok&#233;mon"
  ]
  node [
    id 841
    label "synteza"
  ]
  node [
    id 842
    label "odtworzenie"
  ]
  node [
    id 843
    label "rekwizyt_do_gry"
  ]
  node [
    id 844
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 845
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 846
    label "rzecz"
  ]
  node [
    id 847
    label "oczy"
  ]
  node [
    id 848
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 849
    label "&#378;renica"
  ]
  node [
    id 850
    label "uwaga"
  ]
  node [
    id 851
    label "spojrzenie"
  ]
  node [
    id 852
    label "&#347;lepko"
  ]
  node [
    id 853
    label "net"
  ]
  node [
    id 854
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 855
    label "twarz"
  ]
  node [
    id 856
    label "siniec"
  ]
  node [
    id 857
    label "wzrok"
  ]
  node [
    id 858
    label "powieka"
  ]
  node [
    id 859
    label "kaprawie&#263;"
  ]
  node [
    id 860
    label "spoj&#243;wka"
  ]
  node [
    id 861
    label "organ"
  ]
  node [
    id 862
    label "ga&#322;ka_oczna"
  ]
  node [
    id 863
    label "kaprawienie"
  ]
  node [
    id 864
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 865
    label "ros&#243;&#322;"
  ]
  node [
    id 866
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 867
    label "wypowied&#378;"
  ]
  node [
    id 868
    label "&#347;lepie"
  ]
  node [
    id 869
    label "nerw_wzrokowy"
  ]
  node [
    id 870
    label "coloboma"
  ]
  node [
    id 871
    label "tkanka"
  ]
  node [
    id 872
    label "jednostka_organizacyjna"
  ]
  node [
    id 873
    label "budowa"
  ]
  node [
    id 874
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 875
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 876
    label "tw&#243;r"
  ]
  node [
    id 877
    label "organogeneza"
  ]
  node [
    id 878
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 879
    label "struktura_anatomiczna"
  ]
  node [
    id 880
    label "uk&#322;ad"
  ]
  node [
    id 881
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 882
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 883
    label "Izba_Konsyliarska"
  ]
  node [
    id 884
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 885
    label "stomia"
  ]
  node [
    id 886
    label "dekortykacja"
  ]
  node [
    id 887
    label "okolica"
  ]
  node [
    id 888
    label "Komitet_Region&#243;w"
  ]
  node [
    id 889
    label "m&#281;tnienie"
  ]
  node [
    id 890
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 891
    label "widzenie"
  ]
  node [
    id 892
    label "okulista"
  ]
  node [
    id 893
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 894
    label "zmys&#322;"
  ]
  node [
    id 895
    label "expression"
  ]
  node [
    id 896
    label "widzie&#263;"
  ]
  node [
    id 897
    label "m&#281;tnie&#263;"
  ]
  node [
    id 898
    label "kontakt"
  ]
  node [
    id 899
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 900
    label "nagana"
  ]
  node [
    id 901
    label "tekst"
  ]
  node [
    id 902
    label "upomnienie"
  ]
  node [
    id 903
    label "dzienniczek"
  ]
  node [
    id 904
    label "wzgl&#261;d"
  ]
  node [
    id 905
    label "gossip"
  ]
  node [
    id 906
    label "patrzenie"
  ]
  node [
    id 907
    label "patrze&#263;"
  ]
  node [
    id 908
    label "expectation"
  ]
  node [
    id 909
    label "popatrzenie"
  ]
  node [
    id 910
    label "pojmowanie"
  ]
  node [
    id 911
    label "stare"
  ]
  node [
    id 912
    label "decentracja"
  ]
  node [
    id 913
    label "object"
  ]
  node [
    id 914
    label "przedmiot"
  ]
  node [
    id 915
    label "temat"
  ]
  node [
    id 916
    label "wpadni&#281;cie"
  ]
  node [
    id 917
    label "mienie"
  ]
  node [
    id 918
    label "przyroda"
  ]
  node [
    id 919
    label "istota"
  ]
  node [
    id 920
    label "kultura"
  ]
  node [
    id 921
    label "wpa&#347;&#263;"
  ]
  node [
    id 922
    label "wpadanie"
  ]
  node [
    id 923
    label "wpada&#263;"
  ]
  node [
    id 924
    label "pos&#322;uchanie"
  ]
  node [
    id 925
    label "sparafrazowanie"
  ]
  node [
    id 926
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 927
    label "strawestowa&#263;"
  ]
  node [
    id 928
    label "sparafrazowa&#263;"
  ]
  node [
    id 929
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 930
    label "trawestowa&#263;"
  ]
  node [
    id 931
    label "sformu&#322;owanie"
  ]
  node [
    id 932
    label "parafrazowanie"
  ]
  node [
    id 933
    label "ozdobnik"
  ]
  node [
    id 934
    label "delimitacja"
  ]
  node [
    id 935
    label "parafrazowa&#263;"
  ]
  node [
    id 936
    label "stylizacja"
  ]
  node [
    id 937
    label "komunikat"
  ]
  node [
    id 938
    label "trawestowanie"
  ]
  node [
    id 939
    label "strawestowanie"
  ]
  node [
    id 940
    label "cera"
  ]
  node [
    id 941
    label "wielko&#347;&#263;"
  ]
  node [
    id 942
    label "rys"
  ]
  node [
    id 943
    label "profil"
  ]
  node [
    id 944
    label "p&#322;e&#263;"
  ]
  node [
    id 945
    label "zas&#322;ona"
  ]
  node [
    id 946
    label "p&#243;&#322;profil"
  ]
  node [
    id 947
    label "policzek"
  ]
  node [
    id 948
    label "brew"
  ]
  node [
    id 949
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 950
    label "micha"
  ]
  node [
    id 951
    label "reputacja"
  ]
  node [
    id 952
    label "wyraz_twarzy"
  ]
  node [
    id 953
    label "czo&#322;o"
  ]
  node [
    id 954
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 955
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 956
    label "twarzyczka"
  ]
  node [
    id 957
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 958
    label "ucho"
  ]
  node [
    id 959
    label "usta"
  ]
  node [
    id 960
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 961
    label "dzi&#243;b"
  ]
  node [
    id 962
    label "podbr&#243;dek"
  ]
  node [
    id 963
    label "liczko"
  ]
  node [
    id 964
    label "pysk"
  ]
  node [
    id 965
    label "maskowato&#347;&#263;"
  ]
  node [
    id 966
    label "eyeliner"
  ]
  node [
    id 967
    label "ga&#322;y"
  ]
  node [
    id 968
    label "zupa"
  ]
  node [
    id 969
    label "consomme"
  ]
  node [
    id 970
    label "sk&#243;rzak"
  ]
  node [
    id 971
    label "tarczka"
  ]
  node [
    id 972
    label "mruganie"
  ]
  node [
    id 973
    label "mruga&#263;"
  ]
  node [
    id 974
    label "entropion"
  ]
  node [
    id 975
    label "ptoza"
  ]
  node [
    id 976
    label "mrugni&#281;cie"
  ]
  node [
    id 977
    label "mrugn&#261;&#263;"
  ]
  node [
    id 978
    label "wrodzone_zw&#281;&#380;enie_szpary_powiekowej"
  ]
  node [
    id 979
    label "grad&#243;wka"
  ]
  node [
    id 980
    label "j&#281;czmie&#324;"
  ]
  node [
    id 981
    label "rz&#281;sa"
  ]
  node [
    id 982
    label "ektropion"
  ]
  node [
    id 983
    label "&#347;luz&#243;wka"
  ]
  node [
    id 984
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 985
    label "st&#322;uczenie"
  ]
  node [
    id 986
    label "effusion"
  ]
  node [
    id 987
    label "oznaka"
  ]
  node [
    id 988
    label "karpiowate"
  ]
  node [
    id 989
    label "obw&#243;dka"
  ]
  node [
    id 990
    label "ryba"
  ]
  node [
    id 991
    label "przebarwienie"
  ]
  node [
    id 992
    label "zm&#281;czenie"
  ]
  node [
    id 993
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 994
    label "szczelina"
  ]
  node [
    id 995
    label "wada_wrodzona"
  ]
  node [
    id 996
    label "ropie&#263;"
  ]
  node [
    id 997
    label "ropienie"
  ]
  node [
    id 998
    label "provider"
  ]
  node [
    id 999
    label "b&#322;&#261;d"
  ]
  node [
    id 1000
    label "hipertekst"
  ]
  node [
    id 1001
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1002
    label "mem"
  ]
  node [
    id 1003
    label "gra_sieciowa"
  ]
  node [
    id 1004
    label "grooming"
  ]
  node [
    id 1005
    label "media"
  ]
  node [
    id 1006
    label "biznes_elektroniczny"
  ]
  node [
    id 1007
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1008
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1009
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1010
    label "netbook"
  ]
  node [
    id 1011
    label "e-hazard"
  ]
  node [
    id 1012
    label "podcast"
  ]
  node [
    id 1013
    label "wystarczy&#263;"
  ]
  node [
    id 1014
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 1015
    label "przebywa&#263;"
  ]
  node [
    id 1016
    label "pozostawa&#263;"
  ]
  node [
    id 1017
    label "kosztowa&#263;"
  ]
  node [
    id 1018
    label "undertaking"
  ]
  node [
    id 1019
    label "digest"
  ]
  node [
    id 1020
    label "wystawa&#263;"
  ]
  node [
    id 1021
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1022
    label "wystarcza&#263;"
  ]
  node [
    id 1023
    label "base"
  ]
  node [
    id 1024
    label "mieszka&#263;"
  ]
  node [
    id 1025
    label "sprawowa&#263;"
  ]
  node [
    id 1026
    label "czeka&#263;"
  ]
  node [
    id 1027
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 1028
    label "istnie&#263;"
  ]
  node [
    id 1029
    label "zostawa&#263;"
  ]
  node [
    id 1030
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1031
    label "adhere"
  ]
  node [
    id 1032
    label "function"
  ]
  node [
    id 1033
    label "bind"
  ]
  node [
    id 1034
    label "panowa&#263;"
  ]
  node [
    id 1035
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 1036
    label "zjednywa&#263;"
  ]
  node [
    id 1037
    label "tkwi&#263;"
  ]
  node [
    id 1038
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1039
    label "pause"
  ]
  node [
    id 1040
    label "przestawa&#263;"
  ]
  node [
    id 1041
    label "hesitate"
  ]
  node [
    id 1042
    label "try"
  ]
  node [
    id 1043
    label "savor"
  ]
  node [
    id 1044
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 1045
    label "cena"
  ]
  node [
    id 1046
    label "doznawa&#263;"
  ]
  node [
    id 1047
    label "essay"
  ]
  node [
    id 1048
    label "suffice"
  ]
  node [
    id 1049
    label "stan&#261;&#263;"
  ]
  node [
    id 1050
    label "zaspokoi&#263;"
  ]
  node [
    id 1051
    label "dosta&#263;"
  ]
  node [
    id 1052
    label "zaspokaja&#263;"
  ]
  node [
    id 1053
    label "dostawa&#263;"
  ]
  node [
    id 1054
    label "stawa&#263;"
  ]
  node [
    id 1055
    label "pauzowa&#263;"
  ]
  node [
    id 1056
    label "oczekiwa&#263;"
  ]
  node [
    id 1057
    label "decydowa&#263;"
  ]
  node [
    id 1058
    label "sp&#281;dza&#263;"
  ]
  node [
    id 1059
    label "look"
  ]
  node [
    id 1060
    label "hold"
  ]
  node [
    id 1061
    label "anticipate"
  ]
  node [
    id 1062
    label "blend"
  ]
  node [
    id 1063
    label "stop"
  ]
  node [
    id 1064
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 1065
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 1066
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1067
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 1068
    label "support"
  ]
  node [
    id 1069
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 1070
    label "prosecute"
  ]
  node [
    id 1071
    label "zajmowa&#263;"
  ]
  node [
    id 1072
    label "room"
  ]
  node [
    id 1073
    label "fall"
  ]
  node [
    id 1074
    label "zupe&#322;ny"
  ]
  node [
    id 1075
    label "wniwecz"
  ]
  node [
    id 1076
    label "zupe&#322;nie"
  ]
  node [
    id 1077
    label "og&#243;lnie"
  ]
  node [
    id 1078
    label "w_pizdu"
  ]
  node [
    id 1079
    label "kompletnie"
  ]
  node [
    id 1080
    label "&#322;&#261;czny"
  ]
  node [
    id 1081
    label "pe&#322;ny"
  ]
  node [
    id 1082
    label "bezradnie"
  ]
  node [
    id 1083
    label "bezskutecznie"
  ]
  node [
    id 1084
    label "&#347;redni"
  ]
  node [
    id 1085
    label "ci&#261;gle"
  ]
  node [
    id 1086
    label "stale"
  ]
  node [
    id 1087
    label "ci&#261;g&#322;y"
  ]
  node [
    id 1088
    label "nieprzerwanie"
  ]
  node [
    id 1089
    label "w_chuj"
  ]
  node [
    id 1090
    label "lock"
  ]
  node [
    id 1091
    label "absolut"
  ]
  node [
    id 1092
    label "integer"
  ]
  node [
    id 1093
    label "liczba"
  ]
  node [
    id 1094
    label "zlewanie_si&#281;"
  ]
  node [
    id 1095
    label "ilo&#347;&#263;"
  ]
  node [
    id 1096
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1097
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1098
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1099
    label "olejek_eteryczny"
  ]
  node [
    id 1100
    label "byt"
  ]
  node [
    id 1101
    label "raj_utracony"
  ]
  node [
    id 1102
    label "umieranie"
  ]
  node [
    id 1103
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1104
    label "prze&#380;ywanie"
  ]
  node [
    id 1105
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1106
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1107
    label "po&#322;&#243;g"
  ]
  node [
    id 1108
    label "umarcie"
  ]
  node [
    id 1109
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1110
    label "subsistence"
  ]
  node [
    id 1111
    label "power"
  ]
  node [
    id 1112
    label "okres_noworodkowy"
  ]
  node [
    id 1113
    label "prze&#380;ycie"
  ]
  node [
    id 1114
    label "wiek_matuzalemowy"
  ]
  node [
    id 1115
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1116
    label "entity"
  ]
  node [
    id 1117
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1118
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1119
    label "do&#380;ywanie"
  ]
  node [
    id 1120
    label "dzieci&#324;stwo"
  ]
  node [
    id 1121
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1122
    label "rozw&#243;j"
  ]
  node [
    id 1123
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1124
    label "menopauza"
  ]
  node [
    id 1125
    label "&#347;mier&#263;"
  ]
  node [
    id 1126
    label "koleje_losu"
  ]
  node [
    id 1127
    label "bycie"
  ]
  node [
    id 1128
    label "zegar_biologiczny"
  ]
  node [
    id 1129
    label "szwung"
  ]
  node [
    id 1130
    label "przebywanie"
  ]
  node [
    id 1131
    label "warunki"
  ]
  node [
    id 1132
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1133
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1134
    label "life"
  ]
  node [
    id 1135
    label "staro&#347;&#263;"
  ]
  node [
    id 1136
    label "energy"
  ]
  node [
    id 1137
    label "trwanie"
  ]
  node [
    id 1138
    label "wra&#380;enie"
  ]
  node [
    id 1139
    label "przej&#347;cie"
  ]
  node [
    id 1140
    label "doznanie"
  ]
  node [
    id 1141
    label "poradzenie_sobie"
  ]
  node [
    id 1142
    label "przetrwanie"
  ]
  node [
    id 1143
    label "survival"
  ]
  node [
    id 1144
    label "przechodzenie"
  ]
  node [
    id 1145
    label "wytrzymywanie"
  ]
  node [
    id 1146
    label "zaznawanie"
  ]
  node [
    id 1147
    label "obejrzenie"
  ]
  node [
    id 1148
    label "urzeczywistnianie"
  ]
  node [
    id 1149
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1150
    label "przeszkodzenie"
  ]
  node [
    id 1151
    label "produkowanie"
  ]
  node [
    id 1152
    label "being"
  ]
  node [
    id 1153
    label "znikni&#281;cie"
  ]
  node [
    id 1154
    label "robienie"
  ]
  node [
    id 1155
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1156
    label "przeszkadzanie"
  ]
  node [
    id 1157
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1158
    label "wyprodukowanie"
  ]
  node [
    id 1159
    label "utrzymywanie"
  ]
  node [
    id 1160
    label "subsystencja"
  ]
  node [
    id 1161
    label "utrzyma&#263;"
  ]
  node [
    id 1162
    label "egzystencja"
  ]
  node [
    id 1163
    label "wy&#380;ywienie"
  ]
  node [
    id 1164
    label "ontologicznie"
  ]
  node [
    id 1165
    label "utrzymanie"
  ]
  node [
    id 1166
    label "potencja"
  ]
  node [
    id 1167
    label "utrzymywa&#263;"
  ]
  node [
    id 1168
    label "status"
  ]
  node [
    id 1169
    label "sytuacja"
  ]
  node [
    id 1170
    label "ocieranie_si&#281;"
  ]
  node [
    id 1171
    label "otoczenie_si&#281;"
  ]
  node [
    id 1172
    label "posiedzenie"
  ]
  node [
    id 1173
    label "otarcie_si&#281;"
  ]
  node [
    id 1174
    label "atakowanie"
  ]
  node [
    id 1175
    label "otaczanie_si&#281;"
  ]
  node [
    id 1176
    label "wyj&#347;cie"
  ]
  node [
    id 1177
    label "zmierzanie"
  ]
  node [
    id 1178
    label "residency"
  ]
  node [
    id 1179
    label "sojourn"
  ]
  node [
    id 1180
    label "wychodzenie"
  ]
  node [
    id 1181
    label "tkwienie"
  ]
  node [
    id 1182
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1183
    label "absolutorium"
  ]
  node [
    id 1184
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1185
    label "activity"
  ]
  node [
    id 1186
    label "ton"
  ]
  node [
    id 1187
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 1188
    label "cecha"
  ]
  node [
    id 1189
    label "korkowanie"
  ]
  node [
    id 1190
    label "death"
  ]
  node [
    id 1191
    label "zabijanie"
  ]
  node [
    id 1192
    label "przestawanie"
  ]
  node [
    id 1193
    label "odumieranie"
  ]
  node [
    id 1194
    label "zdychanie"
  ]
  node [
    id 1195
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1196
    label "zanikanie"
  ]
  node [
    id 1197
    label "ko&#324;czenie"
  ]
  node [
    id 1198
    label "nieuleczalnie_chory"
  ]
  node [
    id 1199
    label "&#380;ywotny"
  ]
  node [
    id 1200
    label "naturalny"
  ]
  node [
    id 1201
    label "&#380;ywo"
  ]
  node [
    id 1202
    label "o&#380;ywianie"
  ]
  node [
    id 1203
    label "silny"
  ]
  node [
    id 1204
    label "g&#322;&#281;boki"
  ]
  node [
    id 1205
    label "wyra&#378;ny"
  ]
  node [
    id 1206
    label "czynny"
  ]
  node [
    id 1207
    label "zgrabny"
  ]
  node [
    id 1208
    label "prawdziwy"
  ]
  node [
    id 1209
    label "realistyczny"
  ]
  node [
    id 1210
    label "energiczny"
  ]
  node [
    id 1211
    label "odumarcie"
  ]
  node [
    id 1212
    label "przestanie"
  ]
  node [
    id 1213
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1214
    label "pomarcie"
  ]
  node [
    id 1215
    label "die"
  ]
  node [
    id 1216
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1217
    label "zdechni&#281;cie"
  ]
  node [
    id 1218
    label "procedura"
  ]
  node [
    id 1219
    label "proces_biologiczny"
  ]
  node [
    id 1220
    label "z&#322;ote_czasy"
  ]
  node [
    id 1221
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1222
    label "process"
  ]
  node [
    id 1223
    label "cycle"
  ]
  node [
    id 1224
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 1225
    label "adolescence"
  ]
  node [
    id 1226
    label "wiek"
  ]
  node [
    id 1227
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 1228
    label "zielone_lata"
  ]
  node [
    id 1229
    label "rozwi&#261;zanie"
  ]
  node [
    id 1230
    label "zlec"
  ]
  node [
    id 1231
    label "zlegni&#281;cie"
  ]
  node [
    id 1232
    label "defenestracja"
  ]
  node [
    id 1233
    label "agonia"
  ]
  node [
    id 1234
    label "kres"
  ]
  node [
    id 1235
    label "mogi&#322;a"
  ]
  node [
    id 1236
    label "kres_&#380;ycia"
  ]
  node [
    id 1237
    label "upadek"
  ]
  node [
    id 1238
    label "szeol"
  ]
  node [
    id 1239
    label "pogrzebanie"
  ]
  node [
    id 1240
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1241
    label "&#380;a&#322;oba"
  ]
  node [
    id 1242
    label "pogrzeb"
  ]
  node [
    id 1243
    label "majority"
  ]
  node [
    id 1244
    label "osiemnastoletni"
  ]
  node [
    id 1245
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1246
    label "age"
  ]
  node [
    id 1247
    label "przekwitanie"
  ]
  node [
    id 1248
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 1249
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1250
    label "energia"
  ]
  node [
    id 1251
    label "zapa&#322;"
  ]
  node [
    id 1252
    label "mieni&#263;"
  ]
  node [
    id 1253
    label "nadawa&#263;"
  ]
  node [
    id 1254
    label "okre&#347;la&#263;"
  ]
  node [
    id 1255
    label "dawa&#263;"
  ]
  node [
    id 1256
    label "assign"
  ]
  node [
    id 1257
    label "gada&#263;"
  ]
  node [
    id 1258
    label "donosi&#263;"
  ]
  node [
    id 1259
    label "rekomendowa&#263;"
  ]
  node [
    id 1260
    label "obgadywa&#263;"
  ]
  node [
    id 1261
    label "sprawia&#263;"
  ]
  node [
    id 1262
    label "przesy&#322;a&#263;"
  ]
  node [
    id 1263
    label "signify"
  ]
  node [
    id 1264
    label "style"
  ]
  node [
    id 1265
    label "powodowa&#263;"
  ]
  node [
    id 1266
    label "przyzwyczai&#263;_si&#281;"
  ]
  node [
    id 1267
    label "wiedzie&#263;"
  ]
  node [
    id 1268
    label "kuma&#263;"
  ]
  node [
    id 1269
    label "dziama&#263;"
  ]
  node [
    id 1270
    label "match"
  ]
  node [
    id 1271
    label "empatia"
  ]
  node [
    id 1272
    label "j&#281;zyk"
  ]
  node [
    id 1273
    label "odbiera&#263;"
  ]
  node [
    id 1274
    label "see"
  ]
  node [
    id 1275
    label "zna&#263;"
  ]
  node [
    id 1276
    label "cognizance"
  ]
  node [
    id 1277
    label "postrzega&#263;"
  ]
  node [
    id 1278
    label "przewidywa&#263;"
  ]
  node [
    id 1279
    label "smell"
  ]
  node [
    id 1280
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 1281
    label "uczuwa&#263;"
  ]
  node [
    id 1282
    label "spirit"
  ]
  node [
    id 1283
    label "zabiera&#263;"
  ]
  node [
    id 1284
    label "zlecenie"
  ]
  node [
    id 1285
    label "odzyskiwa&#263;"
  ]
  node [
    id 1286
    label "radio"
  ]
  node [
    id 1287
    label "przyjmowa&#263;"
  ]
  node [
    id 1288
    label "bra&#263;"
  ]
  node [
    id 1289
    label "antena"
  ]
  node [
    id 1290
    label "liszy&#263;"
  ]
  node [
    id 1291
    label "pozbawia&#263;"
  ]
  node [
    id 1292
    label "telewizor"
  ]
  node [
    id 1293
    label "konfiskowa&#263;"
  ]
  node [
    id 1294
    label "deprive"
  ]
  node [
    id 1295
    label "accept"
  ]
  node [
    id 1296
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1297
    label "artykulator"
  ]
  node [
    id 1298
    label "kod"
  ]
  node [
    id 1299
    label "kawa&#322;ek"
  ]
  node [
    id 1300
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1301
    label "gramatyka"
  ]
  node [
    id 1302
    label "stylik"
  ]
  node [
    id 1303
    label "przet&#322;umaczenie"
  ]
  node [
    id 1304
    label "formalizowanie"
  ]
  node [
    id 1305
    label "ssa&#263;"
  ]
  node [
    id 1306
    label "ssanie"
  ]
  node [
    id 1307
    label "language"
  ]
  node [
    id 1308
    label "liza&#263;"
  ]
  node [
    id 1309
    label "napisa&#263;"
  ]
  node [
    id 1310
    label "konsonantyzm"
  ]
  node [
    id 1311
    label "wokalizm"
  ]
  node [
    id 1312
    label "pisa&#263;"
  ]
  node [
    id 1313
    label "fonetyka"
  ]
  node [
    id 1314
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1315
    label "jeniec"
  ]
  node [
    id 1316
    label "but"
  ]
  node [
    id 1317
    label "po_koroniarsku"
  ]
  node [
    id 1318
    label "kultura_duchowa"
  ]
  node [
    id 1319
    label "t&#322;umaczenie"
  ]
  node [
    id 1320
    label "m&#243;wienie"
  ]
  node [
    id 1321
    label "pype&#263;"
  ]
  node [
    id 1322
    label "lizanie"
  ]
  node [
    id 1323
    label "pismo"
  ]
  node [
    id 1324
    label "formalizowa&#263;"
  ]
  node [
    id 1325
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1326
    label "rozumienie"
  ]
  node [
    id 1327
    label "makroglosja"
  ]
  node [
    id 1328
    label "m&#243;wi&#263;"
  ]
  node [
    id 1329
    label "jama_ustna"
  ]
  node [
    id 1330
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1331
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1332
    label "natural_language"
  ]
  node [
    id 1333
    label "s&#322;ownictwo"
  ]
  node [
    id 1334
    label "urz&#261;dzenie"
  ]
  node [
    id 1335
    label "zrozumienie"
  ]
  node [
    id 1336
    label "empathy"
  ]
  node [
    id 1337
    label "lito&#347;&#263;"
  ]
  node [
    id 1338
    label "wczuwa&#263;_si&#281;"
  ]
  node [
    id 1339
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1340
    label "szczeka&#263;"
  ]
  node [
    id 1341
    label "rozmawia&#263;"
  ]
  node [
    id 1342
    label "funkcjonowa&#263;"
  ]
  node [
    id 1343
    label "sprawi&#263;"
  ]
  node [
    id 1344
    label "wykona&#263;"
  ]
  node [
    id 1345
    label "popsu&#263;"
  ]
  node [
    id 1346
    label "pomi&#281;sza&#263;"
  ]
  node [
    id 1347
    label "zmi&#281;sza&#263;"
  ]
  node [
    id 1348
    label "powi&#261;za&#263;"
  ]
  node [
    id 1349
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 1350
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1351
    label "confuse"
  ]
  node [
    id 1352
    label "wprowadzi&#263;"
  ]
  node [
    id 1353
    label "entangle"
  ]
  node [
    id 1354
    label "popieprzy&#263;"
  ]
  node [
    id 1355
    label "zam&#261;ci&#263;"
  ]
  node [
    id 1356
    label "wzburzy&#263;"
  ]
  node [
    id 1357
    label "clutter"
  ]
  node [
    id 1358
    label "naruszy&#263;"
  ]
  node [
    id 1359
    label "zak&#322;ama&#263;"
  ]
  node [
    id 1360
    label "interrupt"
  ]
  node [
    id 1361
    label "namiesza&#263;"
  ]
  node [
    id 1362
    label "zaciemni&#263;"
  ]
  node [
    id 1363
    label "zaszkodzi&#263;"
  ]
  node [
    id 1364
    label "zjeba&#263;"
  ]
  node [
    id 1365
    label "pogorszy&#263;"
  ]
  node [
    id 1366
    label "zrobi&#263;"
  ]
  node [
    id 1367
    label "drop_the_ball"
  ]
  node [
    id 1368
    label "zdemoralizowa&#263;"
  ]
  node [
    id 1369
    label "uszkodzi&#263;"
  ]
  node [
    id 1370
    label "wypierdoli&#263;_si&#281;"
  ]
  node [
    id 1371
    label "damage"
  ]
  node [
    id 1372
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 1373
    label "spapra&#263;"
  ]
  node [
    id 1374
    label "skopa&#263;"
  ]
  node [
    id 1375
    label "rynek"
  ]
  node [
    id 1376
    label "doprowadzi&#263;"
  ]
  node [
    id 1377
    label "testify"
  ]
  node [
    id 1378
    label "insert"
  ]
  node [
    id 1379
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1380
    label "wpisa&#263;"
  ]
  node [
    id 1381
    label "picture"
  ]
  node [
    id 1382
    label "zapozna&#263;"
  ]
  node [
    id 1383
    label "wej&#347;&#263;"
  ]
  node [
    id 1384
    label "zej&#347;&#263;"
  ]
  node [
    id 1385
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1386
    label "zacz&#261;&#263;"
  ]
  node [
    id 1387
    label "indicate"
  ]
  node [
    id 1388
    label "wyrobi&#263;"
  ]
  node [
    id 1389
    label "wzi&#261;&#263;"
  ]
  node [
    id 1390
    label "catch"
  ]
  node [
    id 1391
    label "frame"
  ]
  node [
    id 1392
    label "przygotowa&#263;"
  ]
  node [
    id 1393
    label "wytworzy&#263;"
  ]
  node [
    id 1394
    label "manufacture"
  ]
  node [
    id 1395
    label "relate"
  ]
  node [
    id 1396
    label "incorporate"
  ]
  node [
    id 1397
    label "wi&#281;&#378;"
  ]
  node [
    id 1398
    label "articulation"
  ]
  node [
    id 1399
    label "zjednoczy&#263;"
  ]
  node [
    id 1400
    label "stworzy&#263;"
  ]
  node [
    id 1401
    label "connect"
  ]
  node [
    id 1402
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1403
    label "zawstydzi&#263;"
  ]
  node [
    id 1404
    label "pomyli&#263;"
  ]
  node [
    id 1405
    label "pogada&#263;"
  ]
  node [
    id 1406
    label "pepper"
  ]
  node [
    id 1407
    label "porobi&#263;"
  ]
  node [
    id 1408
    label "przyprawi&#263;"
  ]
  node [
    id 1409
    label "dow&#243;d"
  ]
  node [
    id 1410
    label "oznakowanie"
  ]
  node [
    id 1411
    label "fakt"
  ]
  node [
    id 1412
    label "stawia&#263;"
  ]
  node [
    id 1413
    label "point"
  ]
  node [
    id 1414
    label "kodzik"
  ]
  node [
    id 1415
    label "postawi&#263;"
  ]
  node [
    id 1416
    label "mark"
  ]
  node [
    id 1417
    label "herb"
  ]
  node [
    id 1418
    label "attribute"
  ]
  node [
    id 1419
    label "implikowa&#263;"
  ]
  node [
    id 1420
    label "reszta"
  ]
  node [
    id 1421
    label "trace"
  ]
  node [
    id 1422
    label "&#347;wiadectwo"
  ]
  node [
    id 1423
    label "p&#322;&#243;d"
  ]
  node [
    id 1424
    label "work"
  ]
  node [
    id 1425
    label "&#347;rodek"
  ]
  node [
    id 1426
    label "rewizja"
  ]
  node [
    id 1427
    label "certificate"
  ]
  node [
    id 1428
    label "argument"
  ]
  node [
    id 1429
    label "forsing"
  ]
  node [
    id 1430
    label "dokument"
  ]
  node [
    id 1431
    label "uzasadnienie"
  ]
  node [
    id 1432
    label "bia&#322;e_plamy"
  ]
  node [
    id 1433
    label "klejnot_herbowy"
  ]
  node [
    id 1434
    label "barwy"
  ]
  node [
    id 1435
    label "blazonowa&#263;"
  ]
  node [
    id 1436
    label "symbol"
  ]
  node [
    id 1437
    label "blazonowanie"
  ]
  node [
    id 1438
    label "korona_rangowa"
  ]
  node [
    id 1439
    label "heraldyka"
  ]
  node [
    id 1440
    label "tarcza_herbowa"
  ]
  node [
    id 1441
    label "trzymacz"
  ]
  node [
    id 1442
    label "marking"
  ]
  node [
    id 1443
    label "oznaczenie"
  ]
  node [
    id 1444
    label "pozostawia&#263;"
  ]
  node [
    id 1445
    label "czyni&#263;"
  ]
  node [
    id 1446
    label "wydawa&#263;"
  ]
  node [
    id 1447
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 1448
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1449
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 1450
    label "raise"
  ]
  node [
    id 1451
    label "przyznawa&#263;"
  ]
  node [
    id 1452
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1453
    label "go"
  ]
  node [
    id 1454
    label "obstawia&#263;"
  ]
  node [
    id 1455
    label "ocenia&#263;"
  ]
  node [
    id 1456
    label "zastawia&#263;"
  ]
  node [
    id 1457
    label "stanowisko"
  ]
  node [
    id 1458
    label "wskazywa&#263;"
  ]
  node [
    id 1459
    label "introduce"
  ]
  node [
    id 1460
    label "uruchamia&#263;"
  ]
  node [
    id 1461
    label "wytwarza&#263;"
  ]
  node [
    id 1462
    label "fundowa&#263;"
  ]
  node [
    id 1463
    label "zmienia&#263;"
  ]
  node [
    id 1464
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 1465
    label "deliver"
  ]
  node [
    id 1466
    label "wyznacza&#263;"
  ]
  node [
    id 1467
    label "przedstawia&#263;"
  ]
  node [
    id 1468
    label "wydobywa&#263;"
  ]
  node [
    id 1469
    label "zafundowa&#263;"
  ]
  node [
    id 1470
    label "budowla"
  ]
  node [
    id 1471
    label "wyda&#263;"
  ]
  node [
    id 1472
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 1473
    label "plant"
  ]
  node [
    id 1474
    label "uruchomi&#263;"
  ]
  node [
    id 1475
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 1476
    label "pozostawi&#263;"
  ]
  node [
    id 1477
    label "obra&#263;"
  ]
  node [
    id 1478
    label "peddle"
  ]
  node [
    id 1479
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 1480
    label "obstawi&#263;"
  ]
  node [
    id 1481
    label "zmieni&#263;"
  ]
  node [
    id 1482
    label "post"
  ]
  node [
    id 1483
    label "wyznaczy&#263;"
  ]
  node [
    id 1484
    label "oceni&#263;"
  ]
  node [
    id 1485
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 1486
    label "uczyni&#263;"
  ]
  node [
    id 1487
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1488
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 1489
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 1490
    label "set"
  ]
  node [
    id 1491
    label "wskaza&#263;"
  ]
  node [
    id 1492
    label "przyzna&#263;"
  ]
  node [
    id 1493
    label "wydoby&#263;"
  ]
  node [
    id 1494
    label "przedstawi&#263;"
  ]
  node [
    id 1495
    label "establish"
  ]
  node [
    id 1496
    label "stawi&#263;"
  ]
  node [
    id 1497
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1498
    label "zapis"
  ]
  node [
    id 1499
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1500
    label "imply"
  ]
  node [
    id 1501
    label "gull"
  ]
  node [
    id 1502
    label "nabra&#263;"
  ]
  node [
    id 1503
    label "zba&#322;amuci&#263;"
  ]
  node [
    id 1504
    label "woda"
  ]
  node [
    id 1505
    label "hoax"
  ]
  node [
    id 1506
    label "deceive"
  ]
  node [
    id 1507
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 1508
    label "oszwabi&#263;"
  ]
  node [
    id 1509
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 1510
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 1511
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 1512
    label "naby&#263;"
  ]
  node [
    id 1513
    label "fraud"
  ]
  node [
    id 1514
    label "kupi&#263;"
  ]
  node [
    id 1515
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1516
    label "objecha&#263;"
  ]
  node [
    id 1517
    label "omyli&#263;"
  ]
  node [
    id 1518
    label "uwie&#347;&#263;"
  ]
  node [
    id 1519
    label "woal"
  ]
  node [
    id 1520
    label "tajemnica"
  ]
  node [
    id 1521
    label "zapomnienie"
  ]
  node [
    id 1522
    label "smoke"
  ]
  node [
    id 1523
    label "zjawisko"
  ]
  node [
    id 1524
    label "niepogoda"
  ]
  node [
    id 1525
    label "wypaplanie"
  ]
  node [
    id 1526
    label "enigmat"
  ]
  node [
    id 1527
    label "wiedza"
  ]
  node [
    id 1528
    label "zachowanie"
  ]
  node [
    id 1529
    label "zachowywanie"
  ]
  node [
    id 1530
    label "secret"
  ]
  node [
    id 1531
    label "obowi&#261;zek"
  ]
  node [
    id 1532
    label "dyskrecja"
  ]
  node [
    id 1533
    label "informacja"
  ]
  node [
    id 1534
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1535
    label "taj&#324;"
  ]
  node [
    id 1536
    label "zachowa&#263;"
  ]
  node [
    id 1537
    label "zachowywa&#263;"
  ]
  node [
    id 1538
    label "boski"
  ]
  node [
    id 1539
    label "krajobraz"
  ]
  node [
    id 1540
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1541
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1542
    label "przywidzenie"
  ]
  node [
    id 1543
    label "presence"
  ]
  node [
    id 1544
    label "charakter"
  ]
  node [
    id 1545
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1546
    label "forgetfulness"
  ]
  node [
    id 1547
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1548
    label "obscurity"
  ]
  node [
    id 1549
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1550
    label "zagojenie_si&#281;"
  ]
  node [
    id 1551
    label "zaniedbany"
  ]
  node [
    id 1552
    label "zostawienie"
  ]
  node [
    id 1553
    label "zapominanie"
  ]
  node [
    id 1554
    label "pami&#281;tanie"
  ]
  node [
    id 1555
    label "ulecenie"
  ]
  node [
    id 1556
    label "zlekcewa&#380;enie"
  ]
  node [
    id 1557
    label "omission"
  ]
  node [
    id 1558
    label "roztargnienie"
  ]
  node [
    id 1559
    label "izolacja"
  ]
  node [
    id 1560
    label "pomroka"
  ]
  node [
    id 1561
    label "utrudnienie"
  ]
  node [
    id 1562
    label "tkanina"
  ]
  node [
    id 1563
    label "os&#322;ona"
  ]
  node [
    id 1564
    label "zasnu&#263;"
  ]
  node [
    id 1565
    label "szal"
  ]
  node [
    id 1566
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 1567
    label "postrzec"
  ]
  node [
    id 1568
    label "zobaczy&#263;"
  ]
  node [
    id 1569
    label "notice"
  ]
  node [
    id 1570
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 1571
    label "respect"
  ]
  node [
    id 1572
    label "spojrze&#263;"
  ]
  node [
    id 1573
    label "czarny"
  ]
  node [
    id 1574
    label "kawa"
  ]
  node [
    id 1575
    label "murzynek"
  ]
  node [
    id 1576
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 1577
    label "dripper"
  ]
  node [
    id 1578
    label "ziarno"
  ]
  node [
    id 1579
    label "u&#380;ywka"
  ]
  node [
    id 1580
    label "egzotyk"
  ]
  node [
    id 1581
    label "marzanowate"
  ]
  node [
    id 1582
    label "nap&#243;j"
  ]
  node [
    id 1583
    label "jedzenie"
  ]
  node [
    id 1584
    label "produkt"
  ]
  node [
    id 1585
    label "pestkowiec"
  ]
  node [
    id 1586
    label "porcja"
  ]
  node [
    id 1587
    label "kofeina"
  ]
  node [
    id 1588
    label "chemex"
  ]
  node [
    id 1589
    label "czarna_kawa"
  ]
  node [
    id 1590
    label "ma&#347;lak_pstry"
  ]
  node [
    id 1591
    label "ciasto"
  ]
  node [
    id 1592
    label "czarne"
  ]
  node [
    id 1593
    label "kolorowy"
  ]
  node [
    id 1594
    label "bierka_szachowa"
  ]
  node [
    id 1595
    label "gorzki"
  ]
  node [
    id 1596
    label "murzy&#324;ski"
  ]
  node [
    id 1597
    label "ksi&#261;dz"
  ]
  node [
    id 1598
    label "przewrotny"
  ]
  node [
    id 1599
    label "ponury"
  ]
  node [
    id 1600
    label "beznadziejny"
  ]
  node [
    id 1601
    label "wedel"
  ]
  node [
    id 1602
    label "czarnuch"
  ]
  node [
    id 1603
    label "granatowo"
  ]
  node [
    id 1604
    label "ciemny"
  ]
  node [
    id 1605
    label "negatywny"
  ]
  node [
    id 1606
    label "ciemnienie"
  ]
  node [
    id 1607
    label "czernienie"
  ]
  node [
    id 1608
    label "zaczernienie"
  ]
  node [
    id 1609
    label "pesymistycznie"
  ]
  node [
    id 1610
    label "abolicjonista"
  ]
  node [
    id 1611
    label "brudny"
  ]
  node [
    id 1612
    label "zaczernianie_si&#281;"
  ]
  node [
    id 1613
    label "kafar"
  ]
  node [
    id 1614
    label "czarnuchowaty"
  ]
  node [
    id 1615
    label "pessimistic"
  ]
  node [
    id 1616
    label "czarniawy"
  ]
  node [
    id 1617
    label "ciemnosk&#243;ry"
  ]
  node [
    id 1618
    label "okrutny"
  ]
  node [
    id 1619
    label "czarno"
  ]
  node [
    id 1620
    label "zaczernienie_si&#281;"
  ]
  node [
    id 1621
    label "niepomy&#347;lny"
  ]
  node [
    id 1622
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 1623
    label "zdeklasowa&#263;"
  ]
  node [
    id 1624
    label "przeszy&#263;"
  ]
  node [
    id 1625
    label "tear"
  ]
  node [
    id 1626
    label "wybi&#263;"
  ]
  node [
    id 1627
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 1628
    label "przekrzycze&#263;"
  ]
  node [
    id 1629
    label "przerzuci&#263;"
  ]
  node [
    id 1630
    label "rozgromi&#263;"
  ]
  node [
    id 1631
    label "dopowiedzie&#263;"
  ]
  node [
    id 1632
    label "doj&#261;&#263;"
  ]
  node [
    id 1633
    label "zarysowa&#263;_si&#281;"
  ]
  node [
    id 1634
    label "stick"
  ]
  node [
    id 1635
    label "zaproponowa&#263;"
  ]
  node [
    id 1636
    label "pokona&#263;"
  ]
  node [
    id 1637
    label "zm&#261;ci&#263;"
  ]
  node [
    id 1638
    label "przenikn&#261;&#263;"
  ]
  node [
    id 1639
    label "beat"
  ]
  node [
    id 1640
    label "przedziurawi&#263;"
  ]
  node [
    id 1641
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 1642
    label "tug"
  ]
  node [
    id 1643
    label "przewierci&#263;"
  ]
  node [
    id 1644
    label "strickle"
  ]
  node [
    id 1645
    label "broach"
  ]
  node [
    id 1646
    label "zszy&#263;"
  ]
  node [
    id 1647
    label "przerobi&#263;"
  ]
  node [
    id 1648
    label "spike"
  ]
  node [
    id 1649
    label "przelecie&#263;"
  ]
  node [
    id 1650
    label "fascinate"
  ]
  node [
    id 1651
    label "blast"
  ]
  node [
    id 1652
    label "strike"
  ]
  node [
    id 1653
    label "sypn&#261;&#263;_si&#281;"
  ]
  node [
    id 1654
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1655
    label "usun&#261;&#263;"
  ]
  node [
    id 1656
    label "obi&#263;"
  ]
  node [
    id 1657
    label "nast&#261;pi&#263;"
  ]
  node [
    id 1658
    label "przegoni&#263;"
  ]
  node [
    id 1659
    label "pozbija&#263;"
  ]
  node [
    id 1660
    label "thrash"
  ]
  node [
    id 1661
    label "wyperswadowa&#263;"
  ]
  node [
    id 1662
    label "crush"
  ]
  node [
    id 1663
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 1664
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 1665
    label "po&#322;ama&#263;"
  ]
  node [
    id 1666
    label "wystuka&#263;"
  ]
  node [
    id 1667
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 1668
    label "zagra&#263;"
  ]
  node [
    id 1669
    label "transgress"
  ]
  node [
    id 1670
    label "precipitate"
  ]
  node [
    id 1671
    label "wyt&#322;oczy&#263;"
  ]
  node [
    id 1672
    label "nabi&#263;"
  ]
  node [
    id 1673
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 1674
    label "wy&#322;oi&#263;"
  ]
  node [
    id 1675
    label "obni&#380;y&#263;"
  ]
  node [
    id 1676
    label "ubi&#263;"
  ]
  node [
    id 1677
    label "&#347;cie&#347;ni&#263;"
  ]
  node [
    id 1678
    label "str&#261;ci&#263;"
  ]
  node [
    id 1679
    label "zebra&#263;"
  ]
  node [
    id 1680
    label "obali&#263;"
  ]
  node [
    id 1681
    label "zgromadzi&#263;"
  ]
  node [
    id 1682
    label "pobi&#263;"
  ]
  node [
    id 1683
    label "sku&#263;"
  ]
  node [
    id 1684
    label "rozbi&#263;"
  ]
  node [
    id 1685
    label "change"
  ]
  node [
    id 1686
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1687
    label "come_up"
  ]
  node [
    id 1688
    label "zyska&#263;"
  ]
  node [
    id 1689
    label "zach&#281;ci&#263;"
  ]
  node [
    id 1690
    label "volunteer"
  ]
  node [
    id 1691
    label "poinformowa&#263;"
  ]
  node [
    id 1692
    label "kandydatura"
  ]
  node [
    id 1693
    label "announce"
  ]
  node [
    id 1694
    label "podra&#380;ni&#263;"
  ]
  node [
    id 1695
    label "wywierci&#263;"
  ]
  node [
    id 1696
    label "bang"
  ]
  node [
    id 1697
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1698
    label "zdegradowa&#263;"
  ]
  node [
    id 1699
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1700
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1701
    label "przemyci&#263;"
  ]
  node [
    id 1702
    label "throw"
  ]
  node [
    id 1703
    label "przejrze&#263;"
  ]
  node [
    id 1704
    label "bewilder"
  ]
  node [
    id 1705
    label "zarzuci&#263;"
  ]
  node [
    id 1706
    label "przeszuka&#263;"
  ]
  node [
    id 1707
    label "przesun&#261;&#263;"
  ]
  node [
    id 1708
    label "poradzi&#263;_sobie"
  ]
  node [
    id 1709
    label "zapobiec"
  ]
  node [
    id 1710
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1711
    label "z&#322;oi&#263;"
  ]
  node [
    id 1712
    label "zaatakowa&#263;"
  ]
  node [
    id 1713
    label "overwhelm"
  ]
  node [
    id 1714
    label "wygra&#263;"
  ]
  node [
    id 1715
    label "manipulate"
  ]
  node [
    id 1716
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 1717
    label "erupt"
  ]
  node [
    id 1718
    label "absorb"
  ]
  node [
    id 1719
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 1720
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 1721
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 1722
    label "infiltrate"
  ]
  node [
    id 1723
    label "zag&#322;uszy&#263;"
  ]
  node [
    id 1724
    label "zawt&#243;rowa&#263;"
  ]
  node [
    id 1725
    label "doda&#263;"
  ]
  node [
    id 1726
    label "ujawni&#263;"
  ]
  node [
    id 1727
    label "powiedzie&#263;"
  ]
  node [
    id 1728
    label "dorobi&#263;"
  ]
  node [
    id 1729
    label "rytm"
  ]
  node [
    id 1730
    label "perceive"
  ]
  node [
    id 1731
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 1732
    label "punkt_widzenia"
  ]
  node [
    id 1733
    label "obacza&#263;"
  ]
  node [
    id 1734
    label "dochodzi&#263;"
  ]
  node [
    id 1735
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 1736
    label "doj&#347;&#263;"
  ]
  node [
    id 1737
    label "os&#261;dza&#263;"
  ]
  node [
    id 1738
    label "zamierza&#263;"
  ]
  node [
    id 1739
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1740
    label "hurt"
  ]
  node [
    id 1741
    label "wali&#263;"
  ]
  node [
    id 1742
    label "jeba&#263;"
  ]
  node [
    id 1743
    label "pachnie&#263;"
  ]
  node [
    id 1744
    label "proceed"
  ]
  node [
    id 1745
    label "rozja&#347;ni&#263;"
  ]
  node [
    id 1746
    label "o&#347;wietli&#263;"
  ]
  node [
    id 1747
    label "clear"
  ]
  node [
    id 1748
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 1749
    label "poja&#347;ni&#263;"
  ]
  node [
    id 1750
    label "light"
  ]
  node [
    id 1751
    label "clarify"
  ]
  node [
    id 1752
    label "pom&#243;c"
  ]
  node [
    id 1753
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 1754
    label "odmalowa&#263;_si&#281;"
  ]
  node [
    id 1755
    label "jedyny"
  ]
  node [
    id 1756
    label "du&#380;y"
  ]
  node [
    id 1757
    label "zdr&#243;w"
  ]
  node [
    id 1758
    label "calu&#347;ko"
  ]
  node [
    id 1759
    label "ca&#322;o"
  ]
  node [
    id 1760
    label "przypominanie"
  ]
  node [
    id 1761
    label "podobnie"
  ]
  node [
    id 1762
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1763
    label "upodobnienie"
  ]
  node [
    id 1764
    label "drugi"
  ]
  node [
    id 1765
    label "taki"
  ]
  node [
    id 1766
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1767
    label "zasymilowanie"
  ]
  node [
    id 1768
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1769
    label "ukochany"
  ]
  node [
    id 1770
    label "najlepszy"
  ]
  node [
    id 1771
    label "optymalnie"
  ]
  node [
    id 1772
    label "znaczny"
  ]
  node [
    id 1773
    label "niema&#322;o"
  ]
  node [
    id 1774
    label "wiele"
  ]
  node [
    id 1775
    label "rozwini&#281;ty"
  ]
  node [
    id 1776
    label "dorodny"
  ]
  node [
    id 1777
    label "du&#380;o"
  ]
  node [
    id 1778
    label "zdrowy"
  ]
  node [
    id 1779
    label "nieograniczony"
  ]
  node [
    id 1780
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1781
    label "satysfakcja"
  ]
  node [
    id 1782
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1783
    label "otwarty"
  ]
  node [
    id 1784
    label "wype&#322;nienie"
  ]
  node [
    id 1785
    label "pe&#322;no"
  ]
  node [
    id 1786
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1787
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1788
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1789
    label "r&#243;wny"
  ]
  node [
    id 1790
    label "nieuszkodzony"
  ]
  node [
    id 1791
    label "odpowiednio"
  ]
  node [
    id 1792
    label "jen"
  ]
  node [
    id 1793
    label "relaxation"
  ]
  node [
    id 1794
    label "wymys&#322;"
  ]
  node [
    id 1795
    label "fun"
  ]
  node [
    id 1796
    label "hipersomnia"
  ]
  node [
    id 1797
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1798
    label "marzenie_senne"
  ]
  node [
    id 1799
    label "proces_fizjologiczny"
  ]
  node [
    id 1800
    label "sen_paradoksalny"
  ]
  node [
    id 1801
    label "nokturn"
  ]
  node [
    id 1802
    label "odpoczynek"
  ]
  node [
    id 1803
    label "sen_wolnofalowy"
  ]
  node [
    id 1804
    label "kima"
  ]
  node [
    id 1805
    label "inicjatywa"
  ]
  node [
    id 1806
    label "pomys&#322;"
  ]
  node [
    id 1807
    label "concoction"
  ]
  node [
    id 1808
    label "rozrywka"
  ]
  node [
    id 1809
    label "wyraj"
  ]
  node [
    id 1810
    label "wczas"
  ]
  node [
    id 1811
    label "diversion"
  ]
  node [
    id 1812
    label "rado&#347;&#263;"
  ]
  node [
    id 1813
    label "rin"
  ]
  node [
    id 1814
    label "kszta&#322;townik"
  ]
  node [
    id 1815
    label "zaburzenie"
  ]
  node [
    id 1816
    label "liryczny"
  ]
  node [
    id 1817
    label "nocturne"
  ]
  node [
    id 1818
    label "noc"
  ]
  node [
    id 1819
    label "dzie&#322;o"
  ]
  node [
    id 1820
    label "wiersz"
  ]
  node [
    id 1821
    label "utw&#243;r"
  ]
  node [
    id 1822
    label "pejza&#380;"
  ]
  node [
    id 1823
    label "Japonia"
  ]
  node [
    id 1824
    label "jednostka_monetarna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 271
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 226
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 64
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 41
  ]
  edge [
    source 17
    target 45
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 300
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 289
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 274
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 264
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 283
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 297
  ]
  edge [
    source 17
    target 346
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 25
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 47
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 416
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 429
  ]
  edge [
    source 22
    target 46
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 46
  ]
  edge [
    source 25
    target 47
  ]
  edge [
    source 25
    target 1090
  ]
  edge [
    source 25
    target 1091
  ]
  edge [
    source 25
    target 506
  ]
  edge [
    source 25
    target 1092
  ]
  edge [
    source 25
    target 1093
  ]
  edge [
    source 25
    target 1094
  ]
  edge [
    source 25
    target 1095
  ]
  edge [
    source 25
    target 880
  ]
  edge [
    source 25
    target 1096
  ]
  edge [
    source 25
    target 1097
  ]
  edge [
    source 25
    target 1081
  ]
  edge [
    source 25
    target 1098
  ]
  edge [
    source 25
    target 1099
  ]
  edge [
    source 25
    target 1100
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 47
  ]
  edge [
    source 26
    target 1101
  ]
  edge [
    source 26
    target 1102
  ]
  edge [
    source 26
    target 1103
  ]
  edge [
    source 26
    target 1104
  ]
  edge [
    source 26
    target 1105
  ]
  edge [
    source 26
    target 1106
  ]
  edge [
    source 26
    target 1107
  ]
  edge [
    source 26
    target 1108
  ]
  edge [
    source 26
    target 1109
  ]
  edge [
    source 26
    target 1110
  ]
  edge [
    source 26
    target 1111
  ]
  edge [
    source 26
    target 1112
  ]
  edge [
    source 26
    target 1113
  ]
  edge [
    source 26
    target 1114
  ]
  edge [
    source 26
    target 1115
  ]
  edge [
    source 26
    target 1116
  ]
  edge [
    source 26
    target 1117
  ]
  edge [
    source 26
    target 1118
  ]
  edge [
    source 26
    target 1119
  ]
  edge [
    source 26
    target 1100
  ]
  edge [
    source 26
    target 756
  ]
  edge [
    source 26
    target 1120
  ]
  edge [
    source 26
    target 1121
  ]
  edge [
    source 26
    target 1122
  ]
  edge [
    source 26
    target 1123
  ]
  edge [
    source 26
    target 498
  ]
  edge [
    source 26
    target 1124
  ]
  edge [
    source 26
    target 1125
  ]
  edge [
    source 26
    target 1126
  ]
  edge [
    source 26
    target 1127
  ]
  edge [
    source 26
    target 1128
  ]
  edge [
    source 26
    target 1129
  ]
  edge [
    source 26
    target 1130
  ]
  edge [
    source 26
    target 1131
  ]
  edge [
    source 26
    target 1132
  ]
  edge [
    source 26
    target 1133
  ]
  edge [
    source 26
    target 136
  ]
  edge [
    source 26
    target 1134
  ]
  edge [
    source 26
    target 1135
  ]
  edge [
    source 26
    target 1136
  ]
  edge [
    source 26
    target 1137
  ]
  edge [
    source 26
    target 1138
  ]
  edge [
    source 26
    target 1139
  ]
  edge [
    source 26
    target 1140
  ]
  edge [
    source 26
    target 1141
  ]
  edge [
    source 26
    target 1142
  ]
  edge [
    source 26
    target 1143
  ]
  edge [
    source 26
    target 1144
  ]
  edge [
    source 26
    target 1145
  ]
  edge [
    source 26
    target 1146
  ]
  edge [
    source 26
    target 1147
  ]
  edge [
    source 26
    target 891
  ]
  edge [
    source 26
    target 1148
  ]
  edge [
    source 26
    target 1149
  ]
  edge [
    source 26
    target 1150
  ]
  edge [
    source 26
    target 1151
  ]
  edge [
    source 26
    target 1152
  ]
  edge [
    source 26
    target 1153
  ]
  edge [
    source 26
    target 1154
  ]
  edge [
    source 26
    target 1155
  ]
  edge [
    source 26
    target 1156
  ]
  edge [
    source 26
    target 1157
  ]
  edge [
    source 26
    target 1158
  ]
  edge [
    source 26
    target 1159
  ]
  edge [
    source 26
    target 1160
  ]
  edge [
    source 26
    target 1161
  ]
  edge [
    source 26
    target 1162
  ]
  edge [
    source 26
    target 1163
  ]
  edge [
    source 26
    target 1164
  ]
  edge [
    source 26
    target 1165
  ]
  edge [
    source 26
    target 506
  ]
  edge [
    source 26
    target 1166
  ]
  edge [
    source 26
    target 1167
  ]
  edge [
    source 26
    target 1168
  ]
  edge [
    source 26
    target 1169
  ]
  edge [
    source 26
    target 523
  ]
  edge [
    source 26
    target 524
  ]
  edge [
    source 26
    target 525
  ]
  edge [
    source 26
    target 526
  ]
  edge [
    source 26
    target 527
  ]
  edge [
    source 26
    target 528
  ]
  edge [
    source 26
    target 529
  ]
  edge [
    source 26
    target 530
  ]
  edge [
    source 26
    target 531
  ]
  edge [
    source 26
    target 532
  ]
  edge [
    source 26
    target 533
  ]
  edge [
    source 26
    target 534
  ]
  edge [
    source 26
    target 535
  ]
  edge [
    source 26
    target 536
  ]
  edge [
    source 26
    target 537
  ]
  edge [
    source 26
    target 538
  ]
  edge [
    source 26
    target 539
  ]
  edge [
    source 26
    target 540
  ]
  edge [
    source 26
    target 541
  ]
  edge [
    source 26
    target 542
  ]
  edge [
    source 26
    target 543
  ]
  edge [
    source 26
    target 544
  ]
  edge [
    source 26
    target 545
  ]
  edge [
    source 26
    target 546
  ]
  edge [
    source 26
    target 547
  ]
  edge [
    source 26
    target 548
  ]
  edge [
    source 26
    target 549
  ]
  edge [
    source 26
    target 550
  ]
  edge [
    source 26
    target 551
  ]
  edge [
    source 26
    target 552
  ]
  edge [
    source 26
    target 553
  ]
  edge [
    source 26
    target 554
  ]
  edge [
    source 26
    target 555
  ]
  edge [
    source 26
    target 556
  ]
  edge [
    source 26
    target 557
  ]
  edge [
    source 26
    target 558
  ]
  edge [
    source 26
    target 559
  ]
  edge [
    source 26
    target 1170
  ]
  edge [
    source 26
    target 1171
  ]
  edge [
    source 26
    target 1172
  ]
  edge [
    source 26
    target 1173
  ]
  edge [
    source 26
    target 1174
  ]
  edge [
    source 26
    target 1175
  ]
  edge [
    source 26
    target 1176
  ]
  edge [
    source 26
    target 1177
  ]
  edge [
    source 26
    target 1178
  ]
  edge [
    source 26
    target 1179
  ]
  edge [
    source 26
    target 1180
  ]
  edge [
    source 26
    target 1181
  ]
  edge [
    source 26
    target 1182
  ]
  edge [
    source 26
    target 1183
  ]
  edge [
    source 26
    target 1184
  ]
  edge [
    source 26
    target 349
  ]
  edge [
    source 26
    target 1185
  ]
  edge [
    source 26
    target 1186
  ]
  edge [
    source 26
    target 1187
  ]
  edge [
    source 26
    target 1188
  ]
  edge [
    source 26
    target 1189
  ]
  edge [
    source 26
    target 1190
  ]
  edge [
    source 26
    target 1191
  ]
  edge [
    source 26
    target 468
  ]
  edge [
    source 26
    target 1192
  ]
  edge [
    source 26
    target 1193
  ]
  edge [
    source 26
    target 1194
  ]
  edge [
    source 26
    target 483
  ]
  edge [
    source 26
    target 1195
  ]
  edge [
    source 26
    target 1196
  ]
  edge [
    source 26
    target 1197
  ]
  edge [
    source 26
    target 1198
  ]
  edge [
    source 26
    target 611
  ]
  edge [
    source 26
    target 430
  ]
  edge [
    source 26
    target 1199
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 115
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 1204
  ]
  edge [
    source 26
    target 1205
  ]
  edge [
    source 26
    target 1206
  ]
  edge [
    source 26
    target 122
  ]
  edge [
    source 26
    target 1207
  ]
  edge [
    source 26
    target 1208
  ]
  edge [
    source 26
    target 1209
  ]
  edge [
    source 26
    target 1210
  ]
  edge [
    source 26
    target 1211
  ]
  edge [
    source 26
    target 1212
  ]
  edge [
    source 26
    target 1213
  ]
  edge [
    source 26
    target 1214
  ]
  edge [
    source 26
    target 1215
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 1216
  ]
  edge [
    source 26
    target 1217
  ]
  edge [
    source 26
    target 112
  ]
  edge [
    source 26
    target 1218
  ]
  edge [
    source 26
    target 321
  ]
  edge [
    source 26
    target 1219
  ]
  edge [
    source 26
    target 1220
  ]
  edge [
    source 26
    target 1221
  ]
  edge [
    source 26
    target 1222
  ]
  edge [
    source 26
    target 1223
  ]
  edge [
    source 26
    target 1224
  ]
  edge [
    source 26
    target 1225
  ]
  edge [
    source 26
    target 1226
  ]
  edge [
    source 26
    target 1227
  ]
  edge [
    source 26
    target 1228
  ]
  edge [
    source 26
    target 1229
  ]
  edge [
    source 26
    target 1230
  ]
  edge [
    source 26
    target 1231
  ]
  edge [
    source 26
    target 1232
  ]
  edge [
    source 26
    target 1233
  ]
  edge [
    source 26
    target 1234
  ]
  edge [
    source 26
    target 1235
  ]
  edge [
    source 26
    target 1236
  ]
  edge [
    source 26
    target 1237
  ]
  edge [
    source 26
    target 1238
  ]
  edge [
    source 26
    target 1239
  ]
  edge [
    source 26
    target 1240
  ]
  edge [
    source 26
    target 1241
  ]
  edge [
    source 26
    target 1242
  ]
  edge [
    source 26
    target 1243
  ]
  edge [
    source 26
    target 1244
  ]
  edge [
    source 26
    target 1245
  ]
  edge [
    source 26
    target 747
  ]
  edge [
    source 26
    target 1246
  ]
  edge [
    source 26
    target 712
  ]
  edge [
    source 26
    target 1247
  ]
  edge [
    source 26
    target 1248
  ]
  edge [
    source 26
    target 1249
  ]
  edge [
    source 26
    target 1250
  ]
  edge [
    source 26
    target 1251
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 326
  ]
  edge [
    source 27
    target 1253
  ]
  edge [
    source 27
    target 1254
  ]
  edge [
    source 27
    target 1255
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1257
  ]
  edge [
    source 27
    target 1258
  ]
  edge [
    source 27
    target 1259
  ]
  edge [
    source 27
    target 740
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 1261
  ]
  edge [
    source 27
    target 1262
  ]
  edge [
    source 27
    target 1057
  ]
  edge [
    source 27
    target 1263
  ]
  edge [
    source 27
    target 1264
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1266
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1267
  ]
  edge [
    source 29
    target 1268
  ]
  edge [
    source 29
    target 42
  ]
  edge [
    source 29
    target 326
  ]
  edge [
    source 29
    target 1269
  ]
  edge [
    source 29
    target 1270
  ]
  edge [
    source 29
    target 1271
  ]
  edge [
    source 29
    target 1272
  ]
  edge [
    source 29
    target 1273
  ]
  edge [
    source 29
    target 1274
  ]
  edge [
    source 29
    target 1275
  ]
  edge [
    source 29
    target 1276
  ]
  edge [
    source 29
    target 1277
  ]
  edge [
    source 29
    target 1278
  ]
  edge [
    source 29
    target 459
  ]
  edge [
    source 29
    target 1279
  ]
  edge [
    source 29
    target 1280
  ]
  edge [
    source 29
    target 1281
  ]
  edge [
    source 29
    target 1282
  ]
  edge [
    source 29
    target 1046
  ]
  edge [
    source 29
    target 1061
  ]
  edge [
    source 29
    target 1283
  ]
  edge [
    source 29
    target 1284
  ]
  edge [
    source 29
    target 1285
  ]
  edge [
    source 29
    target 1286
  ]
  edge [
    source 29
    target 1287
  ]
  edge [
    source 29
    target 1288
  ]
  edge [
    source 29
    target 1289
  ]
  edge [
    source 29
    target 1073
  ]
  edge [
    source 29
    target 1290
  ]
  edge [
    source 29
    target 1291
  ]
  edge [
    source 29
    target 1292
  ]
  edge [
    source 29
    target 1293
  ]
  edge [
    source 29
    target 1294
  ]
  edge [
    source 29
    target 1295
  ]
  edge [
    source 29
    target 1296
  ]
  edge [
    source 29
    target 1297
  ]
  edge [
    source 29
    target 1298
  ]
  edge [
    source 29
    target 1299
  ]
  edge [
    source 29
    target 914
  ]
  edge [
    source 29
    target 1300
  ]
  edge [
    source 29
    target 1301
  ]
  edge [
    source 29
    target 1302
  ]
  edge [
    source 29
    target 1303
  ]
  edge [
    source 29
    target 1304
  ]
  edge [
    source 29
    target 1305
  ]
  edge [
    source 29
    target 1306
  ]
  edge [
    source 29
    target 1307
  ]
  edge [
    source 29
    target 1308
  ]
  edge [
    source 29
    target 1309
  ]
  edge [
    source 29
    target 1310
  ]
  edge [
    source 29
    target 1311
  ]
  edge [
    source 29
    target 1312
  ]
  edge [
    source 29
    target 1313
  ]
  edge [
    source 29
    target 1314
  ]
  edge [
    source 29
    target 1315
  ]
  edge [
    source 29
    target 1316
  ]
  edge [
    source 29
    target 899
  ]
  edge [
    source 29
    target 1317
  ]
  edge [
    source 29
    target 1318
  ]
  edge [
    source 29
    target 1319
  ]
  edge [
    source 29
    target 1320
  ]
  edge [
    source 29
    target 1321
  ]
  edge [
    source 29
    target 1322
  ]
  edge [
    source 29
    target 1323
  ]
  edge [
    source 29
    target 1324
  ]
  edge [
    source 29
    target 861
  ]
  edge [
    source 29
    target 1325
  ]
  edge [
    source 29
    target 1326
  ]
  edge [
    source 29
    target 603
  ]
  edge [
    source 29
    target 1327
  ]
  edge [
    source 29
    target 1328
  ]
  edge [
    source 29
    target 1329
  ]
  edge [
    source 29
    target 1330
  ]
  edge [
    source 29
    target 521
  ]
  edge [
    source 29
    target 1331
  ]
  edge [
    source 29
    target 1332
  ]
  edge [
    source 29
    target 1333
  ]
  edge [
    source 29
    target 1334
  ]
  edge [
    source 29
    target 1335
  ]
  edge [
    source 29
    target 1336
  ]
  edge [
    source 29
    target 1337
  ]
  edge [
    source 29
    target 1338
  ]
  edge [
    source 29
    target 1339
  ]
  edge [
    source 29
    target 1340
  ]
  edge [
    source 29
    target 1341
  ]
  edge [
    source 29
    target 1342
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 31
    target 1343
  ]
  edge [
    source 31
    target 1344
  ]
  edge [
    source 31
    target 1345
  ]
  edge [
    source 31
    target 1346
  ]
  edge [
    source 31
    target 1347
  ]
  edge [
    source 31
    target 1348
  ]
  edge [
    source 31
    target 1349
  ]
  edge [
    source 31
    target 1350
  ]
  edge [
    source 31
    target 1351
  ]
  edge [
    source 31
    target 1352
  ]
  edge [
    source 31
    target 1353
  ]
  edge [
    source 31
    target 1354
  ]
  edge [
    source 31
    target 1355
  ]
  edge [
    source 31
    target 1356
  ]
  edge [
    source 31
    target 1357
  ]
  edge [
    source 31
    target 1358
  ]
  edge [
    source 31
    target 1359
  ]
  edge [
    source 31
    target 1360
  ]
  edge [
    source 31
    target 1361
  ]
  edge [
    source 31
    target 1362
  ]
  edge [
    source 31
    target 1363
  ]
  edge [
    source 31
    target 1364
  ]
  edge [
    source 31
    target 1365
  ]
  edge [
    source 31
    target 1366
  ]
  edge [
    source 31
    target 1367
  ]
  edge [
    source 31
    target 1368
  ]
  edge [
    source 31
    target 1369
  ]
  edge [
    source 31
    target 1370
  ]
  edge [
    source 31
    target 1371
  ]
  edge [
    source 31
    target 1372
  ]
  edge [
    source 31
    target 1373
  ]
  edge [
    source 31
    target 1374
  ]
  edge [
    source 31
    target 1375
  ]
  edge [
    source 31
    target 1376
  ]
  edge [
    source 31
    target 1377
  ]
  edge [
    source 31
    target 1378
  ]
  edge [
    source 31
    target 1379
  ]
  edge [
    source 31
    target 1380
  ]
  edge [
    source 31
    target 1381
  ]
  edge [
    source 31
    target 1382
  ]
  edge [
    source 31
    target 1383
  ]
  edge [
    source 31
    target 56
  ]
  edge [
    source 31
    target 1384
  ]
  edge [
    source 31
    target 1385
  ]
  edge [
    source 31
    target 1386
  ]
  edge [
    source 31
    target 1387
  ]
  edge [
    source 31
    target 1388
  ]
  edge [
    source 31
    target 1389
  ]
  edge [
    source 31
    target 1390
  ]
  edge [
    source 31
    target 1391
  ]
  edge [
    source 31
    target 1392
  ]
  edge [
    source 31
    target 1393
  ]
  edge [
    source 31
    target 1394
  ]
  edge [
    source 31
    target 1395
  ]
  edge [
    source 31
    target 1396
  ]
  edge [
    source 31
    target 1397
  ]
  edge [
    source 31
    target 1398
  ]
  edge [
    source 31
    target 1399
  ]
  edge [
    source 31
    target 1400
  ]
  edge [
    source 31
    target 1401
  ]
  edge [
    source 31
    target 1402
  ]
  edge [
    source 31
    target 1403
  ]
  edge [
    source 31
    target 1404
  ]
  edge [
    source 31
    target 1405
  ]
  edge [
    source 31
    target 1406
  ]
  edge [
    source 31
    target 1407
  ]
  edge [
    source 31
    target 1408
  ]
  edge [
    source 31
    target 41
  ]
  edge [
    source 32
    target 1409
  ]
  edge [
    source 32
    target 1410
  ]
  edge [
    source 32
    target 1411
  ]
  edge [
    source 32
    target 1412
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1414
  ]
  edge [
    source 32
    target 1415
  ]
  edge [
    source 32
    target 1416
  ]
  edge [
    source 32
    target 1417
  ]
  edge [
    source 32
    target 993
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 32
    target 1419
  ]
  edge [
    source 32
    target 1420
  ]
  edge [
    source 32
    target 1421
  ]
  edge [
    source 32
    target 289
  ]
  edge [
    source 32
    target 1422
  ]
  edge [
    source 32
    target 914
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1424
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 1425
  ]
  edge [
    source 32
    target 1426
  ]
  edge [
    source 32
    target 1427
  ]
  edge [
    source 32
    target 1428
  ]
  edge [
    source 32
    target 62
  ]
  edge [
    source 32
    target 1429
  ]
  edge [
    source 32
    target 846
  ]
  edge [
    source 32
    target 1430
  ]
  edge [
    source 32
    target 1431
  ]
  edge [
    source 32
    target 1432
  ]
  edge [
    source 32
    target 829
  ]
  edge [
    source 32
    target 1433
  ]
  edge [
    source 32
    target 1434
  ]
  edge [
    source 32
    target 1435
  ]
  edge [
    source 32
    target 1436
  ]
  edge [
    source 32
    target 1437
  ]
  edge [
    source 32
    target 1438
  ]
  edge [
    source 32
    target 1439
  ]
  edge [
    source 32
    target 1440
  ]
  edge [
    source 32
    target 1441
  ]
  edge [
    source 32
    target 1442
  ]
  edge [
    source 32
    target 1443
  ]
  edge [
    source 32
    target 1444
  ]
  edge [
    source 32
    target 1445
  ]
  edge [
    source 32
    target 1446
  ]
  edge [
    source 32
    target 1447
  ]
  edge [
    source 32
    target 1448
  ]
  edge [
    source 32
    target 1449
  ]
  edge [
    source 32
    target 1450
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1451
  ]
  edge [
    source 32
    target 1452
  ]
  edge [
    source 32
    target 1453
  ]
  edge [
    source 32
    target 1454
  ]
  edge [
    source 32
    target 741
  ]
  edge [
    source 32
    target 1455
  ]
  edge [
    source 32
    target 1456
  ]
  edge [
    source 32
    target 1457
  ]
  edge [
    source 32
    target 1458
  ]
  edge [
    source 32
    target 1459
  ]
  edge [
    source 32
    target 1460
  ]
  edge [
    source 32
    target 1461
  ]
  edge [
    source 32
    target 1462
  ]
  edge [
    source 32
    target 1463
  ]
  edge [
    source 32
    target 1464
  ]
  edge [
    source 32
    target 1465
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1466
  ]
  edge [
    source 32
    target 1467
  ]
  edge [
    source 32
    target 1468
  ]
  edge [
    source 32
    target 1469
  ]
  edge [
    source 32
    target 1470
  ]
  edge [
    source 32
    target 1471
  ]
  edge [
    source 32
    target 1472
  ]
  edge [
    source 32
    target 1473
  ]
  edge [
    source 32
    target 1474
  ]
  edge [
    source 32
    target 1475
  ]
  edge [
    source 32
    target 1476
  ]
  edge [
    source 32
    target 1477
  ]
  edge [
    source 32
    target 1478
  ]
  edge [
    source 32
    target 1479
  ]
  edge [
    source 32
    target 1480
  ]
  edge [
    source 32
    target 1481
  ]
  edge [
    source 32
    target 1482
  ]
  edge [
    source 32
    target 1483
  ]
  edge [
    source 32
    target 1484
  ]
  edge [
    source 32
    target 1485
  ]
  edge [
    source 32
    target 1486
  ]
  edge [
    source 32
    target 56
  ]
  edge [
    source 32
    target 1487
  ]
  edge [
    source 32
    target 1393
  ]
  edge [
    source 32
    target 1488
  ]
  edge [
    source 32
    target 1385
  ]
  edge [
    source 32
    target 1489
  ]
  edge [
    source 32
    target 1490
  ]
  edge [
    source 32
    target 1491
  ]
  edge [
    source 32
    target 1492
  ]
  edge [
    source 32
    target 1493
  ]
  edge [
    source 32
    target 1494
  ]
  edge [
    source 32
    target 1495
  ]
  edge [
    source 32
    target 1496
  ]
  edge [
    source 32
    target 1497
  ]
  edge [
    source 32
    target 1498
  ]
  edge [
    source 32
    target 987
  ]
  edge [
    source 32
    target 1499
  ]
  edge [
    source 32
    target 1500
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1501
  ]
  edge [
    source 33
    target 1502
  ]
  edge [
    source 33
    target 1503
  ]
  edge [
    source 33
    target 1504
  ]
  edge [
    source 33
    target 1505
  ]
  edge [
    source 33
    target 1506
  ]
  edge [
    source 33
    target 1507
  ]
  edge [
    source 33
    target 1508
  ]
  edge [
    source 33
    target 1509
  ]
  edge [
    source 33
    target 1510
  ]
  edge [
    source 33
    target 1511
  ]
  edge [
    source 33
    target 1389
  ]
  edge [
    source 33
    target 1512
  ]
  edge [
    source 33
    target 1513
  ]
  edge [
    source 33
    target 1514
  ]
  edge [
    source 33
    target 1515
  ]
  edge [
    source 33
    target 1516
  ]
  edge [
    source 33
    target 1517
  ]
  edge [
    source 33
    target 1518
  ]
  edge [
    source 34
    target 1519
  ]
  edge [
    source 34
    target 1520
  ]
  edge [
    source 34
    target 1521
  ]
  edge [
    source 34
    target 1522
  ]
  edge [
    source 34
    target 1523
  ]
  edge [
    source 34
    target 1524
  ]
  edge [
    source 34
    target 1525
  ]
  edge [
    source 34
    target 1526
  ]
  edge [
    source 34
    target 1527
  ]
  edge [
    source 34
    target 603
  ]
  edge [
    source 34
    target 1528
  ]
  edge [
    source 34
    target 1529
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1446
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1471
  ]
  edge [
    source 34
    target 846
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 1169
  ]
  edge [
    source 34
    target 554
  ]
  edge [
    source 34
    target 321
  ]
  edge [
    source 34
    target 1538
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1541
  ]
  edge [
    source 34
    target 1542
  ]
  edge [
    source 34
    target 1543
  ]
  edge [
    source 34
    target 1544
  ]
  edge [
    source 34
    target 1545
  ]
  edge [
    source 34
    target 49
  ]
  edge [
    source 34
    target 1546
  ]
  edge [
    source 34
    target 1547
  ]
  edge [
    source 34
    target 1548
  ]
  edge [
    source 34
    target 1549
  ]
  edge [
    source 34
    target 1550
  ]
  edge [
    source 34
    target 1551
  ]
  edge [
    source 34
    target 1552
  ]
  edge [
    source 34
    target 1553
  ]
  edge [
    source 34
    target 106
  ]
  edge [
    source 34
    target 1554
  ]
  edge [
    source 34
    target 1555
  ]
  edge [
    source 34
    target 1556
  ]
  edge [
    source 34
    target 1557
  ]
  edge [
    source 34
    target 1558
  ]
  edge [
    source 34
    target 1559
  ]
  edge [
    source 34
    target 1212
  ]
  edge [
    source 34
    target 1560
  ]
  edge [
    source 34
    target 1561
  ]
  edge [
    source 34
    target 1562
  ]
  edge [
    source 34
    target 1563
  ]
  edge [
    source 34
    target 1564
  ]
  edge [
    source 34
    target 1565
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1276
  ]
  edge [
    source 36
    target 1566
  ]
  edge [
    source 36
    target 1567
  ]
  edge [
    source 36
    target 1568
  ]
  edge [
    source 36
    target 50
  ]
  edge [
    source 36
    target 1569
  ]
  edge [
    source 36
    target 1484
  ]
  edge [
    source 36
    target 1570
  ]
  edge [
    source 36
    target 1571
  ]
  edge [
    source 36
    target 1572
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1573
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 39
    target 1576
  ]
  edge [
    source 39
    target 1577
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 1579
  ]
  edge [
    source 39
    target 1580
  ]
  edge [
    source 39
    target 1581
  ]
  edge [
    source 39
    target 1582
  ]
  edge [
    source 39
    target 1583
  ]
  edge [
    source 39
    target 1584
  ]
  edge [
    source 39
    target 1585
  ]
  edge [
    source 39
    target 204
  ]
  edge [
    source 39
    target 1586
  ]
  edge [
    source 39
    target 1587
  ]
  edge [
    source 39
    target 1588
  ]
  edge [
    source 39
    target 1589
  ]
  edge [
    source 39
    target 1590
  ]
  edge [
    source 39
    target 1591
  ]
  edge [
    source 39
    target 1592
  ]
  edge [
    source 39
    target 1593
  ]
  edge [
    source 39
    target 1594
  ]
  edge [
    source 39
    target 1595
  ]
  edge [
    source 39
    target 1596
  ]
  edge [
    source 39
    target 1597
  ]
  edge [
    source 39
    target 168
  ]
  edge [
    source 39
    target 1598
  ]
  edge [
    source 39
    target 1599
  ]
  edge [
    source 39
    target 1600
  ]
  edge [
    source 39
    target 429
  ]
  edge [
    source 39
    target 1601
  ]
  edge [
    source 39
    target 1602
  ]
  edge [
    source 39
    target 1603
  ]
  edge [
    source 39
    target 1604
  ]
  edge [
    source 39
    target 1605
  ]
  edge [
    source 39
    target 1606
  ]
  edge [
    source 39
    target 1607
  ]
  edge [
    source 39
    target 1608
  ]
  edge [
    source 39
    target 1609
  ]
  edge [
    source 39
    target 1610
  ]
  edge [
    source 39
    target 1611
  ]
  edge [
    source 39
    target 1612
  ]
  edge [
    source 39
    target 1613
  ]
  edge [
    source 39
    target 1614
  ]
  edge [
    source 39
    target 1615
  ]
  edge [
    source 39
    target 1616
  ]
  edge [
    source 39
    target 1617
  ]
  edge [
    source 39
    target 1618
  ]
  edge [
    source 39
    target 1619
  ]
  edge [
    source 39
    target 1620
  ]
  edge [
    source 39
    target 1621
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1622
  ]
  edge [
    source 41
    target 1623
  ]
  edge [
    source 41
    target 1624
  ]
  edge [
    source 41
    target 1625
  ]
  edge [
    source 41
    target 1626
  ]
  edge [
    source 41
    target 1627
  ]
  edge [
    source 41
    target 1628
  ]
  edge [
    source 41
    target 1629
  ]
  edge [
    source 41
    target 1630
  ]
  edge [
    source 41
    target 1481
  ]
  edge [
    source 41
    target 1631
  ]
  edge [
    source 41
    target 1632
  ]
  edge [
    source 41
    target 78
  ]
  edge [
    source 41
    target 1633
  ]
  edge [
    source 41
    target 1634
  ]
  edge [
    source 41
    target 1635
  ]
  edge [
    source 41
    target 1636
  ]
  edge [
    source 41
    target 1637
  ]
  edge [
    source 41
    target 1638
  ]
  edge [
    source 41
    target 1639
  ]
  edge [
    source 41
    target 1640
  ]
  edge [
    source 41
    target 1641
  ]
  edge [
    source 41
    target 1642
  ]
  edge [
    source 41
    target 1643
  ]
  edge [
    source 41
    target 1644
  ]
  edge [
    source 41
    target 56
  ]
  edge [
    source 41
    target 1645
  ]
  edge [
    source 41
    target 1343
  ]
  edge [
    source 41
    target 1345
  ]
  edge [
    source 41
    target 1356
  ]
  edge [
    source 41
    target 1357
  ]
  edge [
    source 41
    target 1358
  ]
  edge [
    source 41
    target 1359
  ]
  edge [
    source 41
    target 1360
  ]
  edge [
    source 41
    target 1361
  ]
  edge [
    source 41
    target 1646
  ]
  edge [
    source 41
    target 94
  ]
  edge [
    source 41
    target 1647
  ]
  edge [
    source 41
    target 1648
  ]
  edge [
    source 41
    target 1649
  ]
  edge [
    source 41
    target 1650
  ]
  edge [
    source 41
    target 1651
  ]
  edge [
    source 41
    target 330
  ]
  edge [
    source 41
    target 1652
  ]
  edge [
    source 41
    target 1653
  ]
  edge [
    source 41
    target 1654
  ]
  edge [
    source 41
    target 1655
  ]
  edge [
    source 41
    target 1656
  ]
  edge [
    source 41
    target 1657
  ]
  edge [
    source 41
    target 1658
  ]
  edge [
    source 41
    target 1659
  ]
  edge [
    source 41
    target 1660
  ]
  edge [
    source 41
    target 1661
  ]
  edge [
    source 41
    target 1369
  ]
  edge [
    source 41
    target 1662
  ]
  edge [
    source 41
    target 1663
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 548
  ]
  edge [
    source 41
    target 1664
  ]
  edge [
    source 41
    target 1393
  ]
  edge [
    source 41
    target 50
  ]
  edge [
    source 41
    target 1665
  ]
  edge [
    source 41
    target 1666
  ]
  edge [
    source 41
    target 1491
  ]
  edge [
    source 41
    target 113
  ]
  edge [
    source 41
    target 1667
  ]
  edge [
    source 41
    target 1668
  ]
  edge [
    source 41
    target 1669
  ]
  edge [
    source 41
    target 1670
  ]
  edge [
    source 41
    target 1671
  ]
  edge [
    source 41
    target 1672
  ]
  edge [
    source 41
    target 1673
  ]
  edge [
    source 41
    target 1674
  ]
  edge [
    source 41
    target 1388
  ]
  edge [
    source 41
    target 1675
  ]
  edge [
    source 41
    target 1676
  ]
  edge [
    source 41
    target 1677
  ]
  edge [
    source 41
    target 1678
  ]
  edge [
    source 41
    target 1679
  ]
  edge [
    source 41
    target 81
  ]
  edge [
    source 41
    target 1680
  ]
  edge [
    source 41
    target 1681
  ]
  edge [
    source 41
    target 1682
  ]
  edge [
    source 41
    target 1683
  ]
  edge [
    source 41
    target 1350
  ]
  edge [
    source 41
    target 1684
  ]
  edge [
    source 41
    target 1685
  ]
  edge [
    source 41
    target 1366
  ]
  edge [
    source 41
    target 1686
  ]
  edge [
    source 41
    target 1687
  ]
  edge [
    source 41
    target 1688
  ]
  edge [
    source 41
    target 1689
  ]
  edge [
    source 41
    target 1690
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1387
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 326
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1383
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1490
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1277
  ]
  edge [
    source 42
    target 1278
  ]
  edge [
    source 42
    target 459
  ]
  edge [
    source 42
    target 1279
  ]
  edge [
    source 42
    target 1280
  ]
  edge [
    source 42
    target 1281
  ]
  edge [
    source 42
    target 1282
  ]
  edge [
    source 42
    target 1046
  ]
  edge [
    source 42
    target 1061
  ]
  edge [
    source 42
    target 1730
  ]
  edge [
    source 42
    target 1731
  ]
  edge [
    source 42
    target 1732
  ]
  edge [
    source 42
    target 1733
  ]
  edge [
    source 42
    target 896
  ]
  edge [
    source 42
    target 1734
  ]
  edge [
    source 42
    target 1735
  ]
  edge [
    source 42
    target 1569
  ]
  edge [
    source 42
    target 1736
  ]
  edge [
    source 42
    target 1737
  ]
  edge [
    source 42
    target 477
  ]
  edge [
    source 42
    target 478
  ]
  edge [
    source 42
    target 479
  ]
  edge [
    source 42
    target 480
  ]
  edge [
    source 42
    target 481
  ]
  edge [
    source 42
    target 482
  ]
  edge [
    source 42
    target 483
  ]
  edge [
    source 42
    target 484
  ]
  edge [
    source 42
    target 485
  ]
  edge [
    source 42
    target 486
  ]
  edge [
    source 42
    target 487
  ]
  edge [
    source 42
    target 1738
  ]
  edge [
    source 42
    target 1739
  ]
  edge [
    source 42
    target 1740
  ]
  edge [
    source 42
    target 1741
  ]
  edge [
    source 42
    target 1742
  ]
  edge [
    source 42
    target 1743
  ]
  edge [
    source 42
    target 1744
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1745
  ]
  edge [
    source 45
    target 1746
  ]
  edge [
    source 45
    target 1747
  ]
  edge [
    source 45
    target 1748
  ]
  edge [
    source 45
    target 1749
  ]
  edge [
    source 45
    target 1750
  ]
  edge [
    source 45
    target 56
  ]
  edge [
    source 45
    target 1751
  ]
  edge [
    source 45
    target 1752
  ]
  edge [
    source 45
    target 1753
  ]
  edge [
    source 45
    target 1754
  ]
  edge [
    source 47
    target 1755
  ]
  edge [
    source 47
    target 1756
  ]
  edge [
    source 47
    target 1757
  ]
  edge [
    source 47
    target 1758
  ]
  edge [
    source 47
    target 168
  ]
  edge [
    source 47
    target 136
  ]
  edge [
    source 47
    target 1081
  ]
  edge [
    source 47
    target 647
  ]
  edge [
    source 47
    target 1759
  ]
  edge [
    source 47
    target 1079
  ]
  edge [
    source 47
    target 1074
  ]
  edge [
    source 47
    target 1078
  ]
  edge [
    source 47
    target 1760
  ]
  edge [
    source 47
    target 1761
  ]
  edge [
    source 47
    target 140
  ]
  edge [
    source 47
    target 1762
  ]
  edge [
    source 47
    target 1763
  ]
  edge [
    source 47
    target 1764
  ]
  edge [
    source 47
    target 1765
  ]
  edge [
    source 47
    target 615
  ]
  edge [
    source 47
    target 1766
  ]
  edge [
    source 47
    target 1767
  ]
  edge [
    source 47
    target 1768
  ]
  edge [
    source 47
    target 1769
  ]
  edge [
    source 47
    target 747
  ]
  edge [
    source 47
    target 1770
  ]
  edge [
    source 47
    target 1771
  ]
  edge [
    source 47
    target 752
  ]
  edge [
    source 47
    target 1772
  ]
  edge [
    source 47
    target 1773
  ]
  edge [
    source 47
    target 1774
  ]
  edge [
    source 47
    target 1775
  ]
  edge [
    source 47
    target 1776
  ]
  edge [
    source 47
    target 138
  ]
  edge [
    source 47
    target 1208
  ]
  edge [
    source 47
    target 1777
  ]
  edge [
    source 47
    target 611
  ]
  edge [
    source 47
    target 430
  ]
  edge [
    source 47
    target 1199
  ]
  edge [
    source 47
    target 1200
  ]
  edge [
    source 47
    target 1201
  ]
  edge [
    source 47
    target 115
  ]
  edge [
    source 47
    target 1202
  ]
  edge [
    source 47
    target 1203
  ]
  edge [
    source 47
    target 1204
  ]
  edge [
    source 47
    target 1205
  ]
  edge [
    source 47
    target 1206
  ]
  edge [
    source 47
    target 122
  ]
  edge [
    source 47
    target 1207
  ]
  edge [
    source 47
    target 1209
  ]
  edge [
    source 47
    target 1210
  ]
  edge [
    source 47
    target 1778
  ]
  edge [
    source 47
    target 1779
  ]
  edge [
    source 47
    target 1780
  ]
  edge [
    source 47
    target 1781
  ]
  edge [
    source 47
    target 1782
  ]
  edge [
    source 47
    target 1783
  ]
  edge [
    source 47
    target 1784
  ]
  edge [
    source 47
    target 506
  ]
  edge [
    source 47
    target 1785
  ]
  edge [
    source 47
    target 1786
  ]
  edge [
    source 47
    target 1787
  ]
  edge [
    source 47
    target 1788
  ]
  edge [
    source 47
    target 1789
  ]
  edge [
    source 47
    target 1790
  ]
  edge [
    source 47
    target 1791
  ]
  edge [
    source 48
    target 1792
  ]
  edge [
    source 48
    target 1793
  ]
  edge [
    source 48
    target 1794
  ]
  edge [
    source 48
    target 220
  ]
  edge [
    source 48
    target 1795
  ]
  edge [
    source 48
    target 1796
  ]
  edge [
    source 48
    target 1797
  ]
  edge [
    source 48
    target 1798
  ]
  edge [
    source 48
    target 1799
  ]
  edge [
    source 48
    target 1800
  ]
  edge [
    source 48
    target 1801
  ]
  edge [
    source 48
    target 1802
  ]
  edge [
    source 48
    target 1803
  ]
  edge [
    source 48
    target 1804
  ]
  edge [
    source 48
    target 1805
  ]
  edge [
    source 48
    target 1806
  ]
  edge [
    source 48
    target 1807
  ]
  edge [
    source 48
    target 1808
  ]
  edge [
    source 48
    target 483
  ]
  edge [
    source 48
    target 1809
  ]
  edge [
    source 48
    target 1810
  ]
  edge [
    source 48
    target 1811
  ]
  edge [
    source 48
    target 914
  ]
  edge [
    source 48
    target 1423
  ]
  edge [
    source 48
    target 1424
  ]
  edge [
    source 48
    target 264
  ]
  edge [
    source 48
    target 1812
  ]
  edge [
    source 48
    target 1813
  ]
  edge [
    source 48
    target 1814
  ]
  edge [
    source 48
    target 1815
  ]
  edge [
    source 48
    target 1816
  ]
  edge [
    source 48
    target 1817
  ]
  edge [
    source 48
    target 1818
  ]
  edge [
    source 48
    target 1819
  ]
  edge [
    source 48
    target 1820
  ]
  edge [
    source 48
    target 1821
  ]
  edge [
    source 48
    target 1822
  ]
  edge [
    source 48
    target 1823
  ]
  edge [
    source 48
    target 1824
  ]
]
