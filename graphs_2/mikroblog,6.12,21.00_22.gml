graph [
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "religia"
    origin "text"
  ]
  node [
    id 2
    label "bekazkatoli"
    origin "text"
  ]
  node [
    id 3
    label "humorinformatykow"
    origin "text"
  ]
  node [
    id 4
    label "kult"
  ]
  node [
    id 5
    label "wyznanie"
  ]
  node [
    id 6
    label "mitologia"
  ]
  node [
    id 7
    label "przedmiot"
  ]
  node [
    id 8
    label "ideologia"
  ]
  node [
    id 9
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 10
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 11
    label "nawracanie_si&#281;"
  ]
  node [
    id 12
    label "duchowny"
  ]
  node [
    id 13
    label "rela"
  ]
  node [
    id 14
    label "kultura_duchowa"
  ]
  node [
    id 15
    label "kosmologia"
  ]
  node [
    id 16
    label "kultura"
  ]
  node [
    id 17
    label "kosmogonia"
  ]
  node [
    id 18
    label "nawraca&#263;"
  ]
  node [
    id 19
    label "mistyka"
  ]
  node [
    id 20
    label "political_orientation"
  ]
  node [
    id 21
    label "idea"
  ]
  node [
    id 22
    label "system"
  ]
  node [
    id 23
    label "szko&#322;a"
  ]
  node [
    id 24
    label "zboczenie"
  ]
  node [
    id 25
    label "om&#243;wienie"
  ]
  node [
    id 26
    label "sponiewieranie"
  ]
  node [
    id 27
    label "discipline"
  ]
  node [
    id 28
    label "rzecz"
  ]
  node [
    id 29
    label "omawia&#263;"
  ]
  node [
    id 30
    label "kr&#261;&#380;enie"
  ]
  node [
    id 31
    label "tre&#347;&#263;"
  ]
  node [
    id 32
    label "robienie"
  ]
  node [
    id 33
    label "sponiewiera&#263;"
  ]
  node [
    id 34
    label "element"
  ]
  node [
    id 35
    label "entity"
  ]
  node [
    id 36
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 37
    label "tematyka"
  ]
  node [
    id 38
    label "w&#261;tek"
  ]
  node [
    id 39
    label "charakter"
  ]
  node [
    id 40
    label "zbaczanie"
  ]
  node [
    id 41
    label "program_nauczania"
  ]
  node [
    id 42
    label "om&#243;wi&#263;"
  ]
  node [
    id 43
    label "omawianie"
  ]
  node [
    id 44
    label "thing"
  ]
  node [
    id 45
    label "istota"
  ]
  node [
    id 46
    label "zbacza&#263;"
  ]
  node [
    id 47
    label "zboczy&#263;"
  ]
  node [
    id 48
    label "wypowied&#378;"
  ]
  node [
    id 49
    label "acknowledgment"
  ]
  node [
    id 50
    label "zwierzenie_si&#281;"
  ]
  node [
    id 51
    label "ujawnienie"
  ]
  node [
    id 52
    label "uwielbienie"
  ]
  node [
    id 53
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 54
    label "translacja"
  ]
  node [
    id 55
    label "postawa"
  ]
  node [
    id 56
    label "egzegeta"
  ]
  node [
    id 57
    label "worship"
  ]
  node [
    id 58
    label "obrz&#281;d"
  ]
  node [
    id 59
    label "hipoteza_planetozymalna"
  ]
  node [
    id 60
    label "mikrofalowe_promieniowanie_t&#322;a"
  ]
  node [
    id 61
    label "ciemna_materia"
  ]
  node [
    id 62
    label "struna"
  ]
  node [
    id 63
    label "hipoteza_mg&#322;awicy_s&#322;onecznej"
  ]
  node [
    id 64
    label "zasada_antropiczna"
  ]
  node [
    id 65
    label "inflacja"
  ]
  node [
    id 66
    label "Wielki_Wybuch"
  ]
  node [
    id 67
    label "sta&#322;a_Hubble'a"
  ]
  node [
    id 68
    label "wszech&#347;wiat_zamkni&#281;ty"
  ]
  node [
    id 69
    label "astronomia"
  ]
  node [
    id 70
    label "teoria_stanu_stacjonarnego"
  ]
  node [
    id 71
    label "ylem"
  ]
  node [
    id 72
    label "teoria_Wielkiego_Wybuchu"
  ]
  node [
    id 73
    label "model_kosmologiczny"
  ]
  node [
    id 74
    label "nauka_humanistyczna"
  ]
  node [
    id 75
    label "kolekcja"
  ]
  node [
    id 76
    label "mit"
  ]
  node [
    id 77
    label "teogonia"
  ]
  node [
    id 78
    label "wimana"
  ]
  node [
    id 79
    label "mythology"
  ]
  node [
    id 80
    label "amfisbena"
  ]
  node [
    id 81
    label "pobo&#380;no&#347;&#263;"
  ]
  node [
    id 82
    label "mysticism"
  ]
  node [
    id 83
    label "tajemniczo&#347;&#263;"
  ]
  node [
    id 84
    label "asymilowanie_si&#281;"
  ]
  node [
    id 85
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 86
    label "Wsch&#243;d"
  ]
  node [
    id 87
    label "praca_rolnicza"
  ]
  node [
    id 88
    label "przejmowanie"
  ]
  node [
    id 89
    label "zjawisko"
  ]
  node [
    id 90
    label "cecha"
  ]
  node [
    id 91
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 92
    label "makrokosmos"
  ]
  node [
    id 93
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "konwencja"
  ]
  node [
    id 95
    label "propriety"
  ]
  node [
    id 96
    label "przejmowa&#263;"
  ]
  node [
    id 97
    label "brzoskwiniarnia"
  ]
  node [
    id 98
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 99
    label "sztuka"
  ]
  node [
    id 100
    label "zwyczaj"
  ]
  node [
    id 101
    label "jako&#347;&#263;"
  ]
  node [
    id 102
    label "kuchnia"
  ]
  node [
    id 103
    label "tradycja"
  ]
  node [
    id 104
    label "populace"
  ]
  node [
    id 105
    label "hodowla"
  ]
  node [
    id 106
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 107
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 108
    label "przej&#281;cie"
  ]
  node [
    id 109
    label "przej&#261;&#263;"
  ]
  node [
    id 110
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 111
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 112
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 113
    label "return"
  ]
  node [
    id 114
    label "pogl&#261;dy"
  ]
  node [
    id 115
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 116
    label "nak&#322;ania&#263;"
  ]
  node [
    id 117
    label "Luter"
  ]
  node [
    id 118
    label "cz&#322;owiek"
  ]
  node [
    id 119
    label "eklezjasta"
  ]
  node [
    id 120
    label "Bayes"
  ]
  node [
    id 121
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 122
    label "sekularyzacja"
  ]
  node [
    id 123
    label "tonsura"
  ]
  node [
    id 124
    label "seminarzysta"
  ]
  node [
    id 125
    label "Hus"
  ]
  node [
    id 126
    label "wyznawca"
  ]
  node [
    id 127
    label "duchowie&#324;stwo"
  ]
  node [
    id 128
    label "religijny"
  ]
  node [
    id 129
    label "przedstawiciel"
  ]
  node [
    id 130
    label "&#347;w"
  ]
  node [
    id 131
    label "kongregacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 2
    target 3
  ]
]
