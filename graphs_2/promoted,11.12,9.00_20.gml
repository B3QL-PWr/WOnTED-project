graph [
  node [
    id 0
    label "rekordowy"
    origin "text"
  ]
  node [
    id 1
    label "godzina"
    origin "text"
  ]
  node [
    id 2
    label "tyle"
    origin "text"
  ]
  node [
    id 3
    label "bez"
    origin "text"
  ]
  node [
    id 4
    label "przerwa"
    origin "text"
  ]
  node [
    id 5
    label "przepracowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "lekarz"
    origin "text"
  ]
  node [
    id 8
    label "maksymalny"
  ]
  node [
    id 9
    label "najlepszy"
  ]
  node [
    id 10
    label "rekordowo"
  ]
  node [
    id 11
    label "wyj&#261;tkowy"
  ]
  node [
    id 12
    label "lider"
  ]
  node [
    id 13
    label "maksymalizowanie"
  ]
  node [
    id 14
    label "zmaksymalizowanie"
  ]
  node [
    id 15
    label "maxymalny"
  ]
  node [
    id 16
    label "graniczny"
  ]
  node [
    id 17
    label "maksymalnie"
  ]
  node [
    id 18
    label "time"
  ]
  node [
    id 19
    label "doba"
  ]
  node [
    id 20
    label "p&#243;&#322;godzina"
  ]
  node [
    id 21
    label "jednostka_czasu"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "minuta"
  ]
  node [
    id 24
    label "kwadrans"
  ]
  node [
    id 25
    label "poprzedzanie"
  ]
  node [
    id 26
    label "czasoprzestrze&#324;"
  ]
  node [
    id 27
    label "laba"
  ]
  node [
    id 28
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 29
    label "chronometria"
  ]
  node [
    id 30
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 31
    label "rachuba_czasu"
  ]
  node [
    id 32
    label "przep&#322;ywanie"
  ]
  node [
    id 33
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 34
    label "czasokres"
  ]
  node [
    id 35
    label "odczyt"
  ]
  node [
    id 36
    label "chwila"
  ]
  node [
    id 37
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 38
    label "dzieje"
  ]
  node [
    id 39
    label "kategoria_gramatyczna"
  ]
  node [
    id 40
    label "poprzedzenie"
  ]
  node [
    id 41
    label "trawienie"
  ]
  node [
    id 42
    label "pochodzi&#263;"
  ]
  node [
    id 43
    label "period"
  ]
  node [
    id 44
    label "okres_czasu"
  ]
  node [
    id 45
    label "poprzedza&#263;"
  ]
  node [
    id 46
    label "schy&#322;ek"
  ]
  node [
    id 47
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 48
    label "odwlekanie_si&#281;"
  ]
  node [
    id 49
    label "zegar"
  ]
  node [
    id 50
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 51
    label "czwarty_wymiar"
  ]
  node [
    id 52
    label "pochodzenie"
  ]
  node [
    id 53
    label "koniugacja"
  ]
  node [
    id 54
    label "Zeitgeist"
  ]
  node [
    id 55
    label "trawi&#263;"
  ]
  node [
    id 56
    label "pogoda"
  ]
  node [
    id 57
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 58
    label "poprzedzi&#263;"
  ]
  node [
    id 59
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 60
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 61
    label "time_period"
  ]
  node [
    id 62
    label "zapis"
  ]
  node [
    id 63
    label "sekunda"
  ]
  node [
    id 64
    label "jednostka"
  ]
  node [
    id 65
    label "stopie&#324;"
  ]
  node [
    id 66
    label "design"
  ]
  node [
    id 67
    label "tydzie&#324;"
  ]
  node [
    id 68
    label "noc"
  ]
  node [
    id 69
    label "dzie&#324;"
  ]
  node [
    id 70
    label "long_time"
  ]
  node [
    id 71
    label "jednostka_geologiczna"
  ]
  node [
    id 72
    label "wiele"
  ]
  node [
    id 73
    label "konkretnie"
  ]
  node [
    id 74
    label "nieznacznie"
  ]
  node [
    id 75
    label "wiela"
  ]
  node [
    id 76
    label "du&#380;y"
  ]
  node [
    id 77
    label "nieistotnie"
  ]
  node [
    id 78
    label "nieznaczny"
  ]
  node [
    id 79
    label "jasno"
  ]
  node [
    id 80
    label "posilnie"
  ]
  node [
    id 81
    label "dok&#322;adnie"
  ]
  node [
    id 82
    label "tre&#347;ciwie"
  ]
  node [
    id 83
    label "po&#380;ywnie"
  ]
  node [
    id 84
    label "konkretny"
  ]
  node [
    id 85
    label "solidny"
  ]
  node [
    id 86
    label "&#322;adnie"
  ]
  node [
    id 87
    label "nie&#378;le"
  ]
  node [
    id 88
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 89
    label "krzew"
  ]
  node [
    id 90
    label "delfinidyna"
  ]
  node [
    id 91
    label "pi&#380;maczkowate"
  ]
  node [
    id 92
    label "ki&#347;&#263;"
  ]
  node [
    id 93
    label "hy&#263;ka"
  ]
  node [
    id 94
    label "pestkowiec"
  ]
  node [
    id 95
    label "kwiat"
  ]
  node [
    id 96
    label "ro&#347;lina"
  ]
  node [
    id 97
    label "owoc"
  ]
  node [
    id 98
    label "oliwkowate"
  ]
  node [
    id 99
    label "lilac"
  ]
  node [
    id 100
    label "flakon"
  ]
  node [
    id 101
    label "przykoronek"
  ]
  node [
    id 102
    label "kielich"
  ]
  node [
    id 103
    label "dno_kwiatowe"
  ]
  node [
    id 104
    label "organ_ro&#347;linny"
  ]
  node [
    id 105
    label "ogon"
  ]
  node [
    id 106
    label "warga"
  ]
  node [
    id 107
    label "korona"
  ]
  node [
    id 108
    label "rurka"
  ]
  node [
    id 109
    label "ozdoba"
  ]
  node [
    id 110
    label "kostka"
  ]
  node [
    id 111
    label "kita"
  ]
  node [
    id 112
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 113
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 114
    label "d&#322;o&#324;"
  ]
  node [
    id 115
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 116
    label "powerball"
  ]
  node [
    id 117
    label "&#380;ubr"
  ]
  node [
    id 118
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 119
    label "p&#281;k"
  ]
  node [
    id 120
    label "r&#281;ka"
  ]
  node [
    id 121
    label "zako&#324;czenie"
  ]
  node [
    id 122
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 123
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 124
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 125
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 126
    label "&#322;yko"
  ]
  node [
    id 127
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 128
    label "karczowa&#263;"
  ]
  node [
    id 129
    label "wykarczowanie"
  ]
  node [
    id 130
    label "skupina"
  ]
  node [
    id 131
    label "wykarczowa&#263;"
  ]
  node [
    id 132
    label "karczowanie"
  ]
  node [
    id 133
    label "fanerofit"
  ]
  node [
    id 134
    label "zbiorowisko"
  ]
  node [
    id 135
    label "ro&#347;liny"
  ]
  node [
    id 136
    label "p&#281;d"
  ]
  node [
    id 137
    label "wegetowanie"
  ]
  node [
    id 138
    label "zadziorek"
  ]
  node [
    id 139
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 140
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 141
    label "do&#322;owa&#263;"
  ]
  node [
    id 142
    label "wegetacja"
  ]
  node [
    id 143
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 144
    label "strzyc"
  ]
  node [
    id 145
    label "w&#322;&#243;kno"
  ]
  node [
    id 146
    label "g&#322;uszenie"
  ]
  node [
    id 147
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 148
    label "fitotron"
  ]
  node [
    id 149
    label "bulwka"
  ]
  node [
    id 150
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 151
    label "odn&#243;&#380;ka"
  ]
  node [
    id 152
    label "epiderma"
  ]
  node [
    id 153
    label "gumoza"
  ]
  node [
    id 154
    label "strzy&#380;enie"
  ]
  node [
    id 155
    label "wypotnik"
  ]
  node [
    id 156
    label "flawonoid"
  ]
  node [
    id 157
    label "wyro&#347;le"
  ]
  node [
    id 158
    label "do&#322;owanie"
  ]
  node [
    id 159
    label "g&#322;uszy&#263;"
  ]
  node [
    id 160
    label "pora&#380;a&#263;"
  ]
  node [
    id 161
    label "fitocenoza"
  ]
  node [
    id 162
    label "hodowla"
  ]
  node [
    id 163
    label "fotoautotrof"
  ]
  node [
    id 164
    label "nieuleczalnie_chory"
  ]
  node [
    id 165
    label "wegetowa&#263;"
  ]
  node [
    id 166
    label "pochewka"
  ]
  node [
    id 167
    label "sok"
  ]
  node [
    id 168
    label "system_korzeniowy"
  ]
  node [
    id 169
    label "zawi&#261;zek"
  ]
  node [
    id 170
    label "mi&#261;&#380;sz"
  ]
  node [
    id 171
    label "frukt"
  ]
  node [
    id 172
    label "drylowanie"
  ]
  node [
    id 173
    label "produkt"
  ]
  node [
    id 174
    label "owocnia"
  ]
  node [
    id 175
    label "fruktoza"
  ]
  node [
    id 176
    label "obiekt"
  ]
  node [
    id 177
    label "gniazdo_nasienne"
  ]
  node [
    id 178
    label "rezultat"
  ]
  node [
    id 179
    label "glukoza"
  ]
  node [
    id 180
    label "pestka"
  ]
  node [
    id 181
    label "antocyjanidyn"
  ]
  node [
    id 182
    label "szczeciowce"
  ]
  node [
    id 183
    label "jasnotowce"
  ]
  node [
    id 184
    label "Oleaceae"
  ]
  node [
    id 185
    label "wielkopolski"
  ]
  node [
    id 186
    label "bez_czarny"
  ]
  node [
    id 187
    label "miejsce"
  ]
  node [
    id 188
    label "pauza"
  ]
  node [
    id 189
    label "przedzia&#322;"
  ]
  node [
    id 190
    label "warunek_lokalowy"
  ]
  node [
    id 191
    label "plac"
  ]
  node [
    id 192
    label "location"
  ]
  node [
    id 193
    label "uwaga"
  ]
  node [
    id 194
    label "przestrze&#324;"
  ]
  node [
    id 195
    label "status"
  ]
  node [
    id 196
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 197
    label "cia&#322;o"
  ]
  node [
    id 198
    label "cecha"
  ]
  node [
    id 199
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 200
    label "praca"
  ]
  node [
    id 201
    label "rz&#261;d"
  ]
  node [
    id 202
    label "przegroda"
  ]
  node [
    id 203
    label "zbi&#243;r"
  ]
  node [
    id 204
    label "miejsce_le&#380;&#261;ce"
  ]
  node [
    id 205
    label "part"
  ]
  node [
    id 206
    label "podzia&#322;"
  ]
  node [
    id 207
    label "pomieszczenie"
  ]
  node [
    id 208
    label "skala"
  ]
  node [
    id 209
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 210
    label "farewell"
  ]
  node [
    id 211
    label "hyphen"
  ]
  node [
    id 212
    label "znak_muzyczny"
  ]
  node [
    id 213
    label "znak_graficzny"
  ]
  node [
    id 214
    label "overwork"
  ]
  node [
    id 215
    label "prze&#380;y&#263;"
  ]
  node [
    id 216
    label "upora&#263;_si&#281;"
  ]
  node [
    id 217
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 218
    label "poradzi&#263;_sobie"
  ]
  node [
    id 219
    label "visualize"
  ]
  node [
    id 220
    label "dozna&#263;"
  ]
  node [
    id 221
    label "wytrzyma&#263;"
  ]
  node [
    id 222
    label "przej&#347;&#263;"
  ]
  node [
    id 223
    label "see"
  ]
  node [
    id 224
    label "przedmiot"
  ]
  node [
    id 225
    label "Polish"
  ]
  node [
    id 226
    label "goniony"
  ]
  node [
    id 227
    label "oberek"
  ]
  node [
    id 228
    label "ryba_po_grecku"
  ]
  node [
    id 229
    label "sztajer"
  ]
  node [
    id 230
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 231
    label "krakowiak"
  ]
  node [
    id 232
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 233
    label "pierogi_ruskie"
  ]
  node [
    id 234
    label "lacki"
  ]
  node [
    id 235
    label "polak"
  ]
  node [
    id 236
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 237
    label "chodzony"
  ]
  node [
    id 238
    label "po_polsku"
  ]
  node [
    id 239
    label "mazur"
  ]
  node [
    id 240
    label "polsko"
  ]
  node [
    id 241
    label "skoczny"
  ]
  node [
    id 242
    label "drabant"
  ]
  node [
    id 243
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 244
    label "j&#281;zyk"
  ]
  node [
    id 245
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 246
    label "artykulator"
  ]
  node [
    id 247
    label "kod"
  ]
  node [
    id 248
    label "kawa&#322;ek"
  ]
  node [
    id 249
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 250
    label "gramatyka"
  ]
  node [
    id 251
    label "stylik"
  ]
  node [
    id 252
    label "przet&#322;umaczenie"
  ]
  node [
    id 253
    label "formalizowanie"
  ]
  node [
    id 254
    label "ssa&#263;"
  ]
  node [
    id 255
    label "ssanie"
  ]
  node [
    id 256
    label "language"
  ]
  node [
    id 257
    label "liza&#263;"
  ]
  node [
    id 258
    label "napisa&#263;"
  ]
  node [
    id 259
    label "konsonantyzm"
  ]
  node [
    id 260
    label "wokalizm"
  ]
  node [
    id 261
    label "pisa&#263;"
  ]
  node [
    id 262
    label "fonetyka"
  ]
  node [
    id 263
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 264
    label "jeniec"
  ]
  node [
    id 265
    label "but"
  ]
  node [
    id 266
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 267
    label "po_koroniarsku"
  ]
  node [
    id 268
    label "kultura_duchowa"
  ]
  node [
    id 269
    label "t&#322;umaczenie"
  ]
  node [
    id 270
    label "m&#243;wienie"
  ]
  node [
    id 271
    label "pype&#263;"
  ]
  node [
    id 272
    label "lizanie"
  ]
  node [
    id 273
    label "pismo"
  ]
  node [
    id 274
    label "formalizowa&#263;"
  ]
  node [
    id 275
    label "rozumie&#263;"
  ]
  node [
    id 276
    label "organ"
  ]
  node [
    id 277
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 278
    label "rozumienie"
  ]
  node [
    id 279
    label "spos&#243;b"
  ]
  node [
    id 280
    label "makroglosja"
  ]
  node [
    id 281
    label "m&#243;wi&#263;"
  ]
  node [
    id 282
    label "jama_ustna"
  ]
  node [
    id 283
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 284
    label "formacja_geologiczna"
  ]
  node [
    id 285
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 286
    label "natural_language"
  ]
  node [
    id 287
    label "s&#322;ownictwo"
  ]
  node [
    id 288
    label "urz&#261;dzenie"
  ]
  node [
    id 289
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 290
    label "wschodnioeuropejski"
  ]
  node [
    id 291
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 292
    label "poga&#324;ski"
  ]
  node [
    id 293
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 294
    label "topielec"
  ]
  node [
    id 295
    label "europejski"
  ]
  node [
    id 296
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 297
    label "langosz"
  ]
  node [
    id 298
    label "zboczenie"
  ]
  node [
    id 299
    label "om&#243;wienie"
  ]
  node [
    id 300
    label "sponiewieranie"
  ]
  node [
    id 301
    label "discipline"
  ]
  node [
    id 302
    label "rzecz"
  ]
  node [
    id 303
    label "omawia&#263;"
  ]
  node [
    id 304
    label "kr&#261;&#380;enie"
  ]
  node [
    id 305
    label "tre&#347;&#263;"
  ]
  node [
    id 306
    label "robienie"
  ]
  node [
    id 307
    label "sponiewiera&#263;"
  ]
  node [
    id 308
    label "element"
  ]
  node [
    id 309
    label "entity"
  ]
  node [
    id 310
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 311
    label "tematyka"
  ]
  node [
    id 312
    label "w&#261;tek"
  ]
  node [
    id 313
    label "charakter"
  ]
  node [
    id 314
    label "zbaczanie"
  ]
  node [
    id 315
    label "program_nauczania"
  ]
  node [
    id 316
    label "om&#243;wi&#263;"
  ]
  node [
    id 317
    label "omawianie"
  ]
  node [
    id 318
    label "thing"
  ]
  node [
    id 319
    label "kultura"
  ]
  node [
    id 320
    label "istota"
  ]
  node [
    id 321
    label "zbacza&#263;"
  ]
  node [
    id 322
    label "zboczy&#263;"
  ]
  node [
    id 323
    label "gwardzista"
  ]
  node [
    id 324
    label "melodia"
  ]
  node [
    id 325
    label "taniec"
  ]
  node [
    id 326
    label "taniec_ludowy"
  ]
  node [
    id 327
    label "&#347;redniowieczny"
  ]
  node [
    id 328
    label "specjalny"
  ]
  node [
    id 329
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 330
    label "weso&#322;y"
  ]
  node [
    id 331
    label "sprawny"
  ]
  node [
    id 332
    label "rytmiczny"
  ]
  node [
    id 333
    label "skocznie"
  ]
  node [
    id 334
    label "energiczny"
  ]
  node [
    id 335
    label "lendler"
  ]
  node [
    id 336
    label "austriacki"
  ]
  node [
    id 337
    label "polka"
  ]
  node [
    id 338
    label "europejsko"
  ]
  node [
    id 339
    label "przytup"
  ]
  node [
    id 340
    label "ho&#322;ubiec"
  ]
  node [
    id 341
    label "wodzi&#263;"
  ]
  node [
    id 342
    label "ludowy"
  ]
  node [
    id 343
    label "pie&#347;&#324;"
  ]
  node [
    id 344
    label "mieszkaniec"
  ]
  node [
    id 345
    label "centu&#347;"
  ]
  node [
    id 346
    label "lalka"
  ]
  node [
    id 347
    label "Ma&#322;opolanin"
  ]
  node [
    id 348
    label "krakauer"
  ]
  node [
    id 349
    label "Mesmer"
  ]
  node [
    id 350
    label "pracownik"
  ]
  node [
    id 351
    label "Galen"
  ]
  node [
    id 352
    label "zbada&#263;"
  ]
  node [
    id 353
    label "medyk"
  ]
  node [
    id 354
    label "eskulap"
  ]
  node [
    id 355
    label "lekarze"
  ]
  node [
    id 356
    label "Hipokrates"
  ]
  node [
    id 357
    label "dokt&#243;r"
  ]
  node [
    id 358
    label "&#347;rodowisko"
  ]
  node [
    id 359
    label "student"
  ]
  node [
    id 360
    label "praktyk"
  ]
  node [
    id 361
    label "salariat"
  ]
  node [
    id 362
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 363
    label "cz&#322;owiek"
  ]
  node [
    id 364
    label "delegowanie"
  ]
  node [
    id 365
    label "pracu&#347;"
  ]
  node [
    id 366
    label "delegowa&#263;"
  ]
  node [
    id 367
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 368
    label "Aesculapius"
  ]
  node [
    id 369
    label "sprawdzi&#263;"
  ]
  node [
    id 370
    label "pozna&#263;"
  ]
  node [
    id 371
    label "zdecydowa&#263;"
  ]
  node [
    id 372
    label "zrobi&#263;"
  ]
  node [
    id 373
    label "wybada&#263;"
  ]
  node [
    id 374
    label "examine"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
]
