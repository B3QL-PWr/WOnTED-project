graph [
  node [
    id 0
    label "chrostkowo"
    origin "text"
  ]
  node [
    id 1
    label "kujawsko"
  ]
  node [
    id 2
    label "pomorski"
  ]
  node [
    id 3
    label "powsta&#263;"
  ]
  node [
    id 4
    label "styczniowy"
  ]
  node [
    id 5
    label "i"
  ]
  node [
    id 6
    label "ii"
  ]
  node [
    id 7
    label "wojna"
  ]
  node [
    id 8
    label "&#347;wiatowy"
  ]
  node [
    id 9
    label "ziemia"
  ]
  node [
    id 10
    label "dobrzy&#324;ski"
  ]
  node [
    id 11
    label "front"
  ]
  node [
    id 12
    label "bia&#322;oruski"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
]
