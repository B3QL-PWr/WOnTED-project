graph [
  node [
    id 0
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "serdecznie"
    origin "text"
  ]
  node [
    id 2
    label "zakazhandlu"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "pis"
    origin "text"
  ]
  node [
    id 5
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 6
    label "sk&#322;ada&#263;"
  ]
  node [
    id 7
    label "odmawia&#263;"
  ]
  node [
    id 8
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 9
    label "wyra&#380;a&#263;"
  ]
  node [
    id 10
    label "thank"
  ]
  node [
    id 11
    label "etykieta"
  ]
  node [
    id 12
    label "przekazywa&#263;"
  ]
  node [
    id 13
    label "zbiera&#263;"
  ]
  node [
    id 14
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 15
    label "przywraca&#263;"
  ]
  node [
    id 16
    label "dawa&#263;"
  ]
  node [
    id 17
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 18
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 19
    label "convey"
  ]
  node [
    id 20
    label "publicize"
  ]
  node [
    id 21
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 22
    label "render"
  ]
  node [
    id 23
    label "uk&#322;ada&#263;"
  ]
  node [
    id 24
    label "opracowywa&#263;"
  ]
  node [
    id 25
    label "set"
  ]
  node [
    id 26
    label "oddawa&#263;"
  ]
  node [
    id 27
    label "train"
  ]
  node [
    id 28
    label "zmienia&#263;"
  ]
  node [
    id 29
    label "dzieli&#263;"
  ]
  node [
    id 30
    label "scala&#263;"
  ]
  node [
    id 31
    label "zestaw"
  ]
  node [
    id 32
    label "znaczy&#263;"
  ]
  node [
    id 33
    label "give_voice"
  ]
  node [
    id 34
    label "oznacza&#263;"
  ]
  node [
    id 35
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 36
    label "represent"
  ]
  node [
    id 37
    label "komunikowa&#263;"
  ]
  node [
    id 38
    label "arouse"
  ]
  node [
    id 39
    label "contest"
  ]
  node [
    id 40
    label "wypowiada&#263;"
  ]
  node [
    id 41
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 42
    label "odpowiada&#263;"
  ]
  node [
    id 43
    label "odrzuca&#263;"
  ]
  node [
    id 44
    label "frame"
  ]
  node [
    id 45
    label "os&#261;dza&#263;"
  ]
  node [
    id 46
    label "tab"
  ]
  node [
    id 47
    label "naklejka"
  ]
  node [
    id 48
    label "zwyczaj"
  ]
  node [
    id 49
    label "tabliczka"
  ]
  node [
    id 50
    label "formality"
  ]
  node [
    id 51
    label "szczerze"
  ]
  node [
    id 52
    label "serdeczny"
  ]
  node [
    id 53
    label "mi&#322;o"
  ]
  node [
    id 54
    label "siarczy&#347;cie"
  ]
  node [
    id 55
    label "przyja&#378;nie"
  ]
  node [
    id 56
    label "przychylnie"
  ]
  node [
    id 57
    label "mi&#322;y"
  ]
  node [
    id 58
    label "dobrze"
  ]
  node [
    id 59
    label "pleasantly"
  ]
  node [
    id 60
    label "deliciously"
  ]
  node [
    id 61
    label "przyjemny"
  ]
  node [
    id 62
    label "gratifyingly"
  ]
  node [
    id 63
    label "szczery"
  ]
  node [
    id 64
    label "s&#322;usznie"
  ]
  node [
    id 65
    label "szczero"
  ]
  node [
    id 66
    label "hojnie"
  ]
  node [
    id 67
    label "honestly"
  ]
  node [
    id 68
    label "przekonuj&#261;co"
  ]
  node [
    id 69
    label "outspokenly"
  ]
  node [
    id 70
    label "artlessly"
  ]
  node [
    id 71
    label "uczciwie"
  ]
  node [
    id 72
    label "bluffly"
  ]
  node [
    id 73
    label "dosadnie"
  ]
  node [
    id 74
    label "silnie"
  ]
  node [
    id 75
    label "siarczysty"
  ]
  node [
    id 76
    label "drogi"
  ]
  node [
    id 77
    label "ciep&#322;y"
  ]
  node [
    id 78
    label "&#380;yczliwy"
  ]
  node [
    id 79
    label "belfer"
  ]
  node [
    id 80
    label "murza"
  ]
  node [
    id 81
    label "cz&#322;owiek"
  ]
  node [
    id 82
    label "ojciec"
  ]
  node [
    id 83
    label "samiec"
  ]
  node [
    id 84
    label "androlog"
  ]
  node [
    id 85
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 86
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 87
    label "efendi"
  ]
  node [
    id 88
    label "opiekun"
  ]
  node [
    id 89
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 90
    label "pa&#324;stwo"
  ]
  node [
    id 91
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 92
    label "bratek"
  ]
  node [
    id 93
    label "Mieszko_I"
  ]
  node [
    id 94
    label "Midas"
  ]
  node [
    id 95
    label "m&#261;&#380;"
  ]
  node [
    id 96
    label "bogaty"
  ]
  node [
    id 97
    label "popularyzator"
  ]
  node [
    id 98
    label "pracodawca"
  ]
  node [
    id 99
    label "kszta&#322;ciciel"
  ]
  node [
    id 100
    label "preceptor"
  ]
  node [
    id 101
    label "nabab"
  ]
  node [
    id 102
    label "pupil"
  ]
  node [
    id 103
    label "andropauza"
  ]
  node [
    id 104
    label "zwrot"
  ]
  node [
    id 105
    label "przyw&#243;dca"
  ]
  node [
    id 106
    label "doros&#322;y"
  ]
  node [
    id 107
    label "pedagog"
  ]
  node [
    id 108
    label "rz&#261;dzenie"
  ]
  node [
    id 109
    label "jegomo&#347;&#263;"
  ]
  node [
    id 110
    label "szkolnik"
  ]
  node [
    id 111
    label "ch&#322;opina"
  ]
  node [
    id 112
    label "w&#322;odarz"
  ]
  node [
    id 113
    label "profesor"
  ]
  node [
    id 114
    label "gra_w_karty"
  ]
  node [
    id 115
    label "w&#322;adza"
  ]
  node [
    id 116
    label "Fidel_Castro"
  ]
  node [
    id 117
    label "Anders"
  ]
  node [
    id 118
    label "Ko&#347;ciuszko"
  ]
  node [
    id 119
    label "Tito"
  ]
  node [
    id 120
    label "Miko&#322;ajczyk"
  ]
  node [
    id 121
    label "Sabataj_Cwi"
  ]
  node [
    id 122
    label "lider"
  ]
  node [
    id 123
    label "Mao"
  ]
  node [
    id 124
    label "p&#322;atnik"
  ]
  node [
    id 125
    label "zwierzchnik"
  ]
  node [
    id 126
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 127
    label "nadzorca"
  ]
  node [
    id 128
    label "funkcjonariusz"
  ]
  node [
    id 129
    label "podmiot"
  ]
  node [
    id 130
    label "wykupienie"
  ]
  node [
    id 131
    label "bycie_w_posiadaniu"
  ]
  node [
    id 132
    label "wykupywanie"
  ]
  node [
    id 133
    label "rozszerzyciel"
  ]
  node [
    id 134
    label "ludzko&#347;&#263;"
  ]
  node [
    id 135
    label "asymilowanie"
  ]
  node [
    id 136
    label "wapniak"
  ]
  node [
    id 137
    label "asymilowa&#263;"
  ]
  node [
    id 138
    label "os&#322;abia&#263;"
  ]
  node [
    id 139
    label "posta&#263;"
  ]
  node [
    id 140
    label "hominid"
  ]
  node [
    id 141
    label "podw&#322;adny"
  ]
  node [
    id 142
    label "os&#322;abianie"
  ]
  node [
    id 143
    label "g&#322;owa"
  ]
  node [
    id 144
    label "figura"
  ]
  node [
    id 145
    label "portrecista"
  ]
  node [
    id 146
    label "dwun&#243;g"
  ]
  node [
    id 147
    label "profanum"
  ]
  node [
    id 148
    label "mikrokosmos"
  ]
  node [
    id 149
    label "nasada"
  ]
  node [
    id 150
    label "duch"
  ]
  node [
    id 151
    label "antropochoria"
  ]
  node [
    id 152
    label "osoba"
  ]
  node [
    id 153
    label "wz&#243;r"
  ]
  node [
    id 154
    label "senior"
  ]
  node [
    id 155
    label "oddzia&#322;ywanie"
  ]
  node [
    id 156
    label "Adam"
  ]
  node [
    id 157
    label "homo_sapiens"
  ]
  node [
    id 158
    label "polifag"
  ]
  node [
    id 159
    label "wydoro&#347;lenie"
  ]
  node [
    id 160
    label "du&#380;y"
  ]
  node [
    id 161
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 162
    label "doro&#347;lenie"
  ]
  node [
    id 163
    label "&#378;ra&#322;y"
  ]
  node [
    id 164
    label "doro&#347;le"
  ]
  node [
    id 165
    label "dojrzale"
  ]
  node [
    id 166
    label "dojrza&#322;y"
  ]
  node [
    id 167
    label "m&#261;dry"
  ]
  node [
    id 168
    label "doletni"
  ]
  node [
    id 169
    label "punkt"
  ]
  node [
    id 170
    label "turn"
  ]
  node [
    id 171
    label "turning"
  ]
  node [
    id 172
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 173
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 174
    label "skr&#281;t"
  ]
  node [
    id 175
    label "obr&#243;t"
  ]
  node [
    id 176
    label "fraza_czasownikowa"
  ]
  node [
    id 177
    label "jednostka_leksykalna"
  ]
  node [
    id 178
    label "zmiana"
  ]
  node [
    id 179
    label "wyra&#380;enie"
  ]
  node [
    id 180
    label "starosta"
  ]
  node [
    id 181
    label "zarz&#261;dca"
  ]
  node [
    id 182
    label "w&#322;adca"
  ]
  node [
    id 183
    label "nauczyciel"
  ]
  node [
    id 184
    label "autor"
  ]
  node [
    id 185
    label "wyprawka"
  ]
  node [
    id 186
    label "mundurek"
  ]
  node [
    id 187
    label "szko&#322;a"
  ]
  node [
    id 188
    label "tarcza"
  ]
  node [
    id 189
    label "elew"
  ]
  node [
    id 190
    label "absolwent"
  ]
  node [
    id 191
    label "klasa"
  ]
  node [
    id 192
    label "stopie&#324;_naukowy"
  ]
  node [
    id 193
    label "nauczyciel_akademicki"
  ]
  node [
    id 194
    label "tytu&#322;"
  ]
  node [
    id 195
    label "profesura"
  ]
  node [
    id 196
    label "konsulent"
  ]
  node [
    id 197
    label "wirtuoz"
  ]
  node [
    id 198
    label "ekspert"
  ]
  node [
    id 199
    label "ochotnik"
  ]
  node [
    id 200
    label "pomocnik"
  ]
  node [
    id 201
    label "student"
  ]
  node [
    id 202
    label "nauczyciel_muzyki"
  ]
  node [
    id 203
    label "zakonnik"
  ]
  node [
    id 204
    label "urz&#281;dnik"
  ]
  node [
    id 205
    label "bogacz"
  ]
  node [
    id 206
    label "dostojnik"
  ]
  node [
    id 207
    label "mo&#347;&#263;"
  ]
  node [
    id 208
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 209
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 210
    label "kuwada"
  ]
  node [
    id 211
    label "tworzyciel"
  ]
  node [
    id 212
    label "rodzice"
  ]
  node [
    id 213
    label "&#347;w"
  ]
  node [
    id 214
    label "pomys&#322;odawca"
  ]
  node [
    id 215
    label "rodzic"
  ]
  node [
    id 216
    label "wykonawca"
  ]
  node [
    id 217
    label "ojczym"
  ]
  node [
    id 218
    label "przodek"
  ]
  node [
    id 219
    label "papa"
  ]
  node [
    id 220
    label "stary"
  ]
  node [
    id 221
    label "zwierz&#281;"
  ]
  node [
    id 222
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 223
    label "kochanek"
  ]
  node [
    id 224
    label "fio&#322;ek"
  ]
  node [
    id 225
    label "facet"
  ]
  node [
    id 226
    label "brat"
  ]
  node [
    id 227
    label "ma&#322;&#380;onek"
  ]
  node [
    id 228
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 229
    label "m&#243;j"
  ]
  node [
    id 230
    label "ch&#322;op"
  ]
  node [
    id 231
    label "pan_m&#322;ody"
  ]
  node [
    id 232
    label "&#347;lubny"
  ]
  node [
    id 233
    label "pan_domu"
  ]
  node [
    id 234
    label "pan_i_w&#322;adca"
  ]
  node [
    id 235
    label "Frygia"
  ]
  node [
    id 236
    label "sprawowanie"
  ]
  node [
    id 237
    label "dominion"
  ]
  node [
    id 238
    label "dominowanie"
  ]
  node [
    id 239
    label "reign"
  ]
  node [
    id 240
    label "rule"
  ]
  node [
    id 241
    label "zwierz&#281;_domowe"
  ]
  node [
    id 242
    label "J&#281;drzejewicz"
  ]
  node [
    id 243
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 244
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 245
    label "John_Dewey"
  ]
  node [
    id 246
    label "specjalista"
  ]
  node [
    id 247
    label "&#380;ycie"
  ]
  node [
    id 248
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 249
    label "Turek"
  ]
  node [
    id 250
    label "effendi"
  ]
  node [
    id 251
    label "obfituj&#261;cy"
  ]
  node [
    id 252
    label "r&#243;&#380;norodny"
  ]
  node [
    id 253
    label "spania&#322;y"
  ]
  node [
    id 254
    label "obficie"
  ]
  node [
    id 255
    label "sytuowany"
  ]
  node [
    id 256
    label "och&#281;do&#380;ny"
  ]
  node [
    id 257
    label "forsiasty"
  ]
  node [
    id 258
    label "zapa&#347;ny"
  ]
  node [
    id 259
    label "bogato"
  ]
  node [
    id 260
    label "Katar"
  ]
  node [
    id 261
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 262
    label "Libia"
  ]
  node [
    id 263
    label "Gwatemala"
  ]
  node [
    id 264
    label "Afganistan"
  ]
  node [
    id 265
    label "Ekwador"
  ]
  node [
    id 266
    label "Tad&#380;ykistan"
  ]
  node [
    id 267
    label "Bhutan"
  ]
  node [
    id 268
    label "Argentyna"
  ]
  node [
    id 269
    label "D&#380;ibuti"
  ]
  node [
    id 270
    label "Wenezuela"
  ]
  node [
    id 271
    label "Ukraina"
  ]
  node [
    id 272
    label "Gabon"
  ]
  node [
    id 273
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 274
    label "Rwanda"
  ]
  node [
    id 275
    label "Liechtenstein"
  ]
  node [
    id 276
    label "organizacja"
  ]
  node [
    id 277
    label "Sri_Lanka"
  ]
  node [
    id 278
    label "Madagaskar"
  ]
  node [
    id 279
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 280
    label "Tonga"
  ]
  node [
    id 281
    label "Kongo"
  ]
  node [
    id 282
    label "Bangladesz"
  ]
  node [
    id 283
    label "Kanada"
  ]
  node [
    id 284
    label "Wehrlen"
  ]
  node [
    id 285
    label "Algieria"
  ]
  node [
    id 286
    label "Surinam"
  ]
  node [
    id 287
    label "Chile"
  ]
  node [
    id 288
    label "Sahara_Zachodnia"
  ]
  node [
    id 289
    label "Uganda"
  ]
  node [
    id 290
    label "W&#281;gry"
  ]
  node [
    id 291
    label "Birma"
  ]
  node [
    id 292
    label "Kazachstan"
  ]
  node [
    id 293
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 294
    label "Armenia"
  ]
  node [
    id 295
    label "Tuwalu"
  ]
  node [
    id 296
    label "Timor_Wschodni"
  ]
  node [
    id 297
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 298
    label "Izrael"
  ]
  node [
    id 299
    label "Estonia"
  ]
  node [
    id 300
    label "Komory"
  ]
  node [
    id 301
    label "Kamerun"
  ]
  node [
    id 302
    label "Haiti"
  ]
  node [
    id 303
    label "Belize"
  ]
  node [
    id 304
    label "Sierra_Leone"
  ]
  node [
    id 305
    label "Luksemburg"
  ]
  node [
    id 306
    label "USA"
  ]
  node [
    id 307
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 308
    label "Barbados"
  ]
  node [
    id 309
    label "San_Marino"
  ]
  node [
    id 310
    label "Bu&#322;garia"
  ]
  node [
    id 311
    label "Wietnam"
  ]
  node [
    id 312
    label "Indonezja"
  ]
  node [
    id 313
    label "Malawi"
  ]
  node [
    id 314
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 315
    label "Francja"
  ]
  node [
    id 316
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 317
    label "partia"
  ]
  node [
    id 318
    label "Zambia"
  ]
  node [
    id 319
    label "Angola"
  ]
  node [
    id 320
    label "Grenada"
  ]
  node [
    id 321
    label "Nepal"
  ]
  node [
    id 322
    label "Panama"
  ]
  node [
    id 323
    label "Rumunia"
  ]
  node [
    id 324
    label "Czarnog&#243;ra"
  ]
  node [
    id 325
    label "Malediwy"
  ]
  node [
    id 326
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 327
    label "S&#322;owacja"
  ]
  node [
    id 328
    label "para"
  ]
  node [
    id 329
    label "Egipt"
  ]
  node [
    id 330
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 331
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 332
    label "Kolumbia"
  ]
  node [
    id 333
    label "Mozambik"
  ]
  node [
    id 334
    label "Laos"
  ]
  node [
    id 335
    label "Burundi"
  ]
  node [
    id 336
    label "Suazi"
  ]
  node [
    id 337
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 338
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 339
    label "Czechy"
  ]
  node [
    id 340
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 341
    label "Wyspy_Marshalla"
  ]
  node [
    id 342
    label "Trynidad_i_Tobago"
  ]
  node [
    id 343
    label "Dominika"
  ]
  node [
    id 344
    label "Palau"
  ]
  node [
    id 345
    label "Syria"
  ]
  node [
    id 346
    label "Gwinea_Bissau"
  ]
  node [
    id 347
    label "Liberia"
  ]
  node [
    id 348
    label "Zimbabwe"
  ]
  node [
    id 349
    label "Polska"
  ]
  node [
    id 350
    label "Jamajka"
  ]
  node [
    id 351
    label "Dominikana"
  ]
  node [
    id 352
    label "Senegal"
  ]
  node [
    id 353
    label "Gruzja"
  ]
  node [
    id 354
    label "Togo"
  ]
  node [
    id 355
    label "Chorwacja"
  ]
  node [
    id 356
    label "Meksyk"
  ]
  node [
    id 357
    label "Macedonia"
  ]
  node [
    id 358
    label "Gujana"
  ]
  node [
    id 359
    label "Zair"
  ]
  node [
    id 360
    label "Albania"
  ]
  node [
    id 361
    label "Kambod&#380;a"
  ]
  node [
    id 362
    label "Mauritius"
  ]
  node [
    id 363
    label "Monako"
  ]
  node [
    id 364
    label "Gwinea"
  ]
  node [
    id 365
    label "Mali"
  ]
  node [
    id 366
    label "Nigeria"
  ]
  node [
    id 367
    label "Kostaryka"
  ]
  node [
    id 368
    label "Hanower"
  ]
  node [
    id 369
    label "Paragwaj"
  ]
  node [
    id 370
    label "W&#322;ochy"
  ]
  node [
    id 371
    label "Wyspy_Salomona"
  ]
  node [
    id 372
    label "Seszele"
  ]
  node [
    id 373
    label "Hiszpania"
  ]
  node [
    id 374
    label "Boliwia"
  ]
  node [
    id 375
    label "Kirgistan"
  ]
  node [
    id 376
    label "Irlandia"
  ]
  node [
    id 377
    label "Czad"
  ]
  node [
    id 378
    label "Irak"
  ]
  node [
    id 379
    label "Lesoto"
  ]
  node [
    id 380
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 381
    label "Malta"
  ]
  node [
    id 382
    label "Andora"
  ]
  node [
    id 383
    label "Chiny"
  ]
  node [
    id 384
    label "Filipiny"
  ]
  node [
    id 385
    label "Antarktis"
  ]
  node [
    id 386
    label "Niemcy"
  ]
  node [
    id 387
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 388
    label "Brazylia"
  ]
  node [
    id 389
    label "terytorium"
  ]
  node [
    id 390
    label "Nikaragua"
  ]
  node [
    id 391
    label "Pakistan"
  ]
  node [
    id 392
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 393
    label "Kenia"
  ]
  node [
    id 394
    label "Niger"
  ]
  node [
    id 395
    label "Tunezja"
  ]
  node [
    id 396
    label "Portugalia"
  ]
  node [
    id 397
    label "Fid&#380;i"
  ]
  node [
    id 398
    label "Maroko"
  ]
  node [
    id 399
    label "Botswana"
  ]
  node [
    id 400
    label "Tajlandia"
  ]
  node [
    id 401
    label "Australia"
  ]
  node [
    id 402
    label "Burkina_Faso"
  ]
  node [
    id 403
    label "interior"
  ]
  node [
    id 404
    label "Benin"
  ]
  node [
    id 405
    label "Tanzania"
  ]
  node [
    id 406
    label "Indie"
  ]
  node [
    id 407
    label "&#321;otwa"
  ]
  node [
    id 408
    label "Kiribati"
  ]
  node [
    id 409
    label "Antigua_i_Barbuda"
  ]
  node [
    id 410
    label "Rodezja"
  ]
  node [
    id 411
    label "Cypr"
  ]
  node [
    id 412
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 413
    label "Peru"
  ]
  node [
    id 414
    label "Austria"
  ]
  node [
    id 415
    label "Urugwaj"
  ]
  node [
    id 416
    label "Jordania"
  ]
  node [
    id 417
    label "Grecja"
  ]
  node [
    id 418
    label "Azerbejd&#380;an"
  ]
  node [
    id 419
    label "Turcja"
  ]
  node [
    id 420
    label "Samoa"
  ]
  node [
    id 421
    label "Sudan"
  ]
  node [
    id 422
    label "Oman"
  ]
  node [
    id 423
    label "ziemia"
  ]
  node [
    id 424
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 425
    label "Uzbekistan"
  ]
  node [
    id 426
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 427
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 428
    label "Honduras"
  ]
  node [
    id 429
    label "Mongolia"
  ]
  node [
    id 430
    label "Portoryko"
  ]
  node [
    id 431
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 432
    label "Serbia"
  ]
  node [
    id 433
    label "Tajwan"
  ]
  node [
    id 434
    label "Wielka_Brytania"
  ]
  node [
    id 435
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 436
    label "Liban"
  ]
  node [
    id 437
    label "Japonia"
  ]
  node [
    id 438
    label "Ghana"
  ]
  node [
    id 439
    label "Bahrajn"
  ]
  node [
    id 440
    label "Belgia"
  ]
  node [
    id 441
    label "Etiopia"
  ]
  node [
    id 442
    label "Mikronezja"
  ]
  node [
    id 443
    label "Kuwejt"
  ]
  node [
    id 444
    label "grupa"
  ]
  node [
    id 445
    label "Bahamy"
  ]
  node [
    id 446
    label "Rosja"
  ]
  node [
    id 447
    label "Mo&#322;dawia"
  ]
  node [
    id 448
    label "Litwa"
  ]
  node [
    id 449
    label "S&#322;owenia"
  ]
  node [
    id 450
    label "Szwajcaria"
  ]
  node [
    id 451
    label "Erytrea"
  ]
  node [
    id 452
    label "Kuba"
  ]
  node [
    id 453
    label "Arabia_Saudyjska"
  ]
  node [
    id 454
    label "granica_pa&#324;stwa"
  ]
  node [
    id 455
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 456
    label "Malezja"
  ]
  node [
    id 457
    label "Korea"
  ]
  node [
    id 458
    label "Jemen"
  ]
  node [
    id 459
    label "Nowa_Zelandia"
  ]
  node [
    id 460
    label "Namibia"
  ]
  node [
    id 461
    label "Nauru"
  ]
  node [
    id 462
    label "holoarktyka"
  ]
  node [
    id 463
    label "Brunei"
  ]
  node [
    id 464
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 465
    label "Khitai"
  ]
  node [
    id 466
    label "Mauretania"
  ]
  node [
    id 467
    label "Iran"
  ]
  node [
    id 468
    label "Gambia"
  ]
  node [
    id 469
    label "Somalia"
  ]
  node [
    id 470
    label "Holandia"
  ]
  node [
    id 471
    label "Turkmenistan"
  ]
  node [
    id 472
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 473
    label "Salwador"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
]
