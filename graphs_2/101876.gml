graph [
  node [
    id 0
    label "czyszczenie"
    origin "text"
  ]
  node [
    id 1
    label "&#322;o&#380;ysko"
    origin "text"
  ]
  node [
    id 2
    label "purge"
  ]
  node [
    id 3
    label "czyszczenie_si&#281;"
  ]
  node [
    id 4
    label "usuwanie"
  ]
  node [
    id 5
    label "przeczyszczenie"
  ]
  node [
    id 6
    label "uk&#322;adanie"
  ]
  node [
    id 7
    label "cleaning"
  ]
  node [
    id 8
    label "klarownia"
  ]
  node [
    id 9
    label "draft"
  ]
  node [
    id 10
    label "k&#322;adzenie"
  ]
  node [
    id 11
    label "kszta&#322;towanie"
  ]
  node [
    id 12
    label "writing"
  ]
  node [
    id 13
    label "tworzenie"
  ]
  node [
    id 14
    label "powodowanie"
  ]
  node [
    id 15
    label "kszta&#322;cenie"
  ]
  node [
    id 16
    label "composing"
  ]
  node [
    id 17
    label "szykowanie"
  ]
  node [
    id 18
    label "obk&#322;adanie"
  ]
  node [
    id 19
    label "gentil"
  ]
  node [
    id 20
    label "scheduling"
  ]
  node [
    id 21
    label "urz&#261;dzanie"
  ]
  node [
    id 22
    label "ordination"
  ]
  node [
    id 23
    label "uczenie"
  ]
  node [
    id 24
    label "my&#347;lenie"
  ]
  node [
    id 25
    label "przenoszenie"
  ]
  node [
    id 26
    label "rugowanie"
  ]
  node [
    id 27
    label "odchodzenie"
  ]
  node [
    id 28
    label "wyci&#261;ganie"
  ]
  node [
    id 29
    label "pozbywanie_si&#281;"
  ]
  node [
    id 30
    label "removal"
  ]
  node [
    id 31
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 32
    label "przesuwanie"
  ]
  node [
    id 33
    label "disposal"
  ]
  node [
    id 34
    label "elimination"
  ]
  node [
    id 35
    label "spowodowanie"
  ]
  node [
    id 36
    label "purification"
  ]
  node [
    id 37
    label "usuni&#281;cie"
  ]
  node [
    id 38
    label "oczyszczenie"
  ]
  node [
    id 39
    label "wypr&#243;&#380;nienie_si&#281;"
  ]
  node [
    id 40
    label "rozwolnienie"
  ]
  node [
    id 41
    label "instalacja"
  ]
  node [
    id 42
    label "kana&#322;"
  ]
  node [
    id 43
    label "trofoblast"
  ]
  node [
    id 44
    label "layer"
  ]
  node [
    id 45
    label "panew"
  ]
  node [
    id 46
    label "bariera_&#322;o&#380;yskowa"
  ]
  node [
    id 47
    label "placenta"
  ]
  node [
    id 48
    label "zal&#261;&#380;nia"
  ]
  node [
    id 49
    label "dolina"
  ]
  node [
    id 50
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 51
    label "katarakta"
  ]
  node [
    id 52
    label "organ"
  ]
  node [
    id 53
    label "komora"
  ]
  node [
    id 54
    label "s&#322;upek"
  ]
  node [
    id 55
    label "obni&#380;enie"
  ]
  node [
    id 56
    label "kiesze&#324;"
  ]
  node [
    id 57
    label "prze&#322;om"
  ]
  node [
    id 58
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 59
    label "uk&#322;ad"
  ]
  node [
    id 60
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 61
    label "Komitet_Region&#243;w"
  ]
  node [
    id 62
    label "struktura_anatomiczna"
  ]
  node [
    id 63
    label "organogeneza"
  ]
  node [
    id 64
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 65
    label "tw&#243;r"
  ]
  node [
    id 66
    label "tkanka"
  ]
  node [
    id 67
    label "stomia"
  ]
  node [
    id 68
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 69
    label "budowa"
  ]
  node [
    id 70
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 71
    label "okolica"
  ]
  node [
    id 72
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 73
    label "dekortykacja"
  ]
  node [
    id 74
    label "Izba_Konsyliarska"
  ]
  node [
    id 75
    label "zesp&#243;&#322;"
  ]
  node [
    id 76
    label "jednostka_organizacyjna"
  ]
  node [
    id 77
    label "Rzym_Zachodni"
  ]
  node [
    id 78
    label "Rzym_Wschodni"
  ]
  node [
    id 79
    label "element"
  ]
  node [
    id 80
    label "ilo&#347;&#263;"
  ]
  node [
    id 81
    label "whole"
  ]
  node [
    id 82
    label "urz&#261;dzenie"
  ]
  node [
    id 83
    label "pr&#243;g"
  ]
  node [
    id 84
    label "fakoemulsyfikacja"
  ]
  node [
    id 85
    label "schorzenie"
  ]
  node [
    id 86
    label "fakoemulsyfikator"
  ]
  node [
    id 87
    label "naczynie"
  ]
  node [
    id 88
    label "pan"
  ]
  node [
    id 89
    label "warzelnia"
  ]
  node [
    id 90
    label "warstwa"
  ]
  node [
    id 91
    label "kosm&#243;wka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
]
