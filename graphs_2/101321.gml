graph [
  node [
    id 0
    label "kisiel&#243;wka"
    origin "text"
  ]
  node [
    id 1
    label "wiejski"
  ]
  node [
    id 2
    label "kluba"
  ]
  node [
    id 3
    label "kultura"
  ]
  node [
    id 4
    label "ochotniczy"
  ]
  node [
    id 5
    label "stra&#380;a"
  ]
  node [
    id 6
    label "po&#380;arny"
  ]
  node [
    id 7
    label "ospa"
  ]
  node [
    id 8
    label "Kisiel&#243;wka"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
]
