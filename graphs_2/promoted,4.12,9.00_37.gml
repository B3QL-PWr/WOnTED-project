graph [
  node [
    id 0
    label "dla"
    origin "text"
  ]
  node [
    id 1
    label "benzo"
    origin "text"
  ]
  node [
    id 2
    label "pirenu"
    origin "text"
  ]
  node [
    id 3
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 4
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "st&#281;&#380;enie"
    origin "text"
  ]
  node [
    id 6
    label "dopuszczalny"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wiadomy"
  ]
  node [
    id 10
    label "konkretny"
  ]
  node [
    id 11
    label "znany"
  ]
  node [
    id 12
    label "ten"
  ]
  node [
    id 13
    label "wiadomie"
  ]
  node [
    id 14
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 15
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 16
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "osta&#263;_si&#281;"
  ]
  node [
    id 18
    label "change"
  ]
  node [
    id 19
    label "pozosta&#263;"
  ]
  node [
    id 20
    label "catch"
  ]
  node [
    id 21
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 22
    label "proceed"
  ]
  node [
    id 23
    label "support"
  ]
  node [
    id 24
    label "prze&#380;y&#263;"
  ]
  node [
    id 25
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 26
    label "stwardnienie"
  ]
  node [
    id 27
    label "&#347;ci&#281;cie"
  ]
  node [
    id 28
    label "zg&#281;stnienie"
  ]
  node [
    id 29
    label "nasycenie"
  ]
  node [
    id 30
    label "concentration"
  ]
  node [
    id 31
    label "opanowanie"
  ]
  node [
    id 32
    label "izotonia"
  ]
  node [
    id 33
    label "przybranie_na_sile"
  ]
  node [
    id 34
    label "wezbranie"
  ]
  node [
    id 35
    label "stanie_si&#281;"
  ]
  node [
    id 36
    label "znieruchomienie"
  ]
  node [
    id 37
    label "immunity"
  ]
  node [
    id 38
    label "nefelometria"
  ]
  node [
    id 39
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 40
    label "doznanie"
  ]
  node [
    id 41
    label "zaimpregnowanie"
  ]
  node [
    id 42
    label "zadowolenie"
  ]
  node [
    id 43
    label "satysfakcja"
  ]
  node [
    id 44
    label "saturation"
  ]
  node [
    id 45
    label "wprowadzenie"
  ]
  node [
    id 46
    label "syty"
  ]
  node [
    id 47
    label "fertilization"
  ]
  node [
    id 48
    label "stan"
  ]
  node [
    id 49
    label "przenikni&#281;cie"
  ]
  node [
    id 50
    label "napojenie_si&#281;"
  ]
  node [
    id 51
    label "satisfaction"
  ]
  node [
    id 52
    label "impregnation"
  ]
  node [
    id 53
    label "zaspokojenie"
  ]
  node [
    id 54
    label "nieruchomy"
  ]
  node [
    id 55
    label "tableau"
  ]
  node [
    id 56
    label "zjawisko_fonetyczne"
  ]
  node [
    id 57
    label "twardszy"
  ]
  node [
    id 58
    label "hardening"
  ]
  node [
    id 59
    label "zmiana"
  ]
  node [
    id 60
    label "g&#281;sty"
  ]
  node [
    id 61
    label "condensation"
  ]
  node [
    id 62
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 63
    label "nasilenie_si&#281;"
  ]
  node [
    id 64
    label "bulge"
  ]
  node [
    id 65
    label "podniesienie_si&#281;"
  ]
  node [
    id 66
    label "zjawisko"
  ]
  node [
    id 67
    label "wyniesienie"
  ]
  node [
    id 68
    label "podporz&#261;dkowanie"
  ]
  node [
    id 69
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 70
    label "dostanie"
  ]
  node [
    id 71
    label "zr&#243;wnowa&#380;enie"
  ]
  node [
    id 72
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 73
    label "spowodowanie"
  ]
  node [
    id 74
    label "nauczenie_si&#281;"
  ]
  node [
    id 75
    label "control"
  ]
  node [
    id 76
    label "zdarzenie_si&#281;"
  ]
  node [
    id 77
    label "powstrzymanie"
  ]
  node [
    id 78
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 79
    label "poczucie"
  ]
  node [
    id 80
    label "convention"
  ]
  node [
    id 81
    label "r&#243;wno&#347;&#263;"
  ]
  node [
    id 82
    label "elektrolit"
  ]
  node [
    id 83
    label "analiza_chemiczna"
  ]
  node [
    id 84
    label "zawiesina"
  ]
  node [
    id 85
    label "roztw&#243;r_koloidowy"
  ]
  node [
    id 86
    label "obci&#281;cie"
  ]
  node [
    id 87
    label "decapitation"
  ]
  node [
    id 88
    label "ow&#322;osienie"
  ]
  node [
    id 89
    label "opitolenie"
  ]
  node [
    id 90
    label "poobcinanie"
  ]
  node [
    id 91
    label "zmro&#380;enie"
  ]
  node [
    id 92
    label "snub"
  ]
  node [
    id 93
    label "kr&#243;j"
  ]
  node [
    id 94
    label "oblanie"
  ]
  node [
    id 95
    label "przeegzaminowanie"
  ]
  node [
    id 96
    label "odbicie"
  ]
  node [
    id 97
    label "uderzenie"
  ]
  node [
    id 98
    label "w&#322;osy"
  ]
  node [
    id 99
    label "ping-pong"
  ]
  node [
    id 100
    label "cut"
  ]
  node [
    id 101
    label "gilotyna"
  ]
  node [
    id 102
    label "szafot"
  ]
  node [
    id 103
    label "skr&#243;cenie"
  ]
  node [
    id 104
    label "zniszczenie"
  ]
  node [
    id 105
    label "kara_&#347;mierci"
  ]
  node [
    id 106
    label "g&#322;owa"
  ]
  node [
    id 107
    label "siatk&#243;wka"
  ]
  node [
    id 108
    label "k&#322;&#243;tnia"
  ]
  node [
    id 109
    label "ukszta&#322;towanie"
  ]
  node [
    id 110
    label "splay"
  ]
  node [
    id 111
    label "zabicie"
  ]
  node [
    id 112
    label "tenis"
  ]
  node [
    id 113
    label "usuni&#281;cie"
  ]
  node [
    id 114
    label "odci&#281;cie"
  ]
  node [
    id 115
    label "chop"
  ]
  node [
    id 116
    label "mo&#380;liwy"
  ]
  node [
    id 117
    label "dopuszczalnie"
  ]
  node [
    id 118
    label "urealnianie"
  ]
  node [
    id 119
    label "mo&#380;ebny"
  ]
  node [
    id 120
    label "umo&#380;liwianie"
  ]
  node [
    id 121
    label "zno&#347;ny"
  ]
  node [
    id 122
    label "umo&#380;liwienie"
  ]
  node [
    id 123
    label "mo&#380;liwie"
  ]
  node [
    id 124
    label "urealnienie"
  ]
  node [
    id 125
    label "dost&#281;pny"
  ]
  node [
    id 126
    label "podnosi&#263;"
  ]
  node [
    id 127
    label "kwota"
  ]
  node [
    id 128
    label "liczy&#263;"
  ]
  node [
    id 129
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 130
    label "zanosi&#263;"
  ]
  node [
    id 131
    label "rozpowszechnia&#263;"
  ]
  node [
    id 132
    label "ujawnia&#263;"
  ]
  node [
    id 133
    label "otrzymywa&#263;"
  ]
  node [
    id 134
    label "kra&#347;&#263;"
  ]
  node [
    id 135
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 136
    label "raise"
  ]
  node [
    id 137
    label "odsuwa&#263;"
  ]
  node [
    id 138
    label "nagradza&#263;"
  ]
  node [
    id 139
    label "forytowa&#263;"
  ]
  node [
    id 140
    label "traktowa&#263;"
  ]
  node [
    id 141
    label "sign"
  ]
  node [
    id 142
    label "robi&#263;"
  ]
  node [
    id 143
    label "podpierdala&#263;"
  ]
  node [
    id 144
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 145
    label "r&#261;ba&#263;"
  ]
  node [
    id 146
    label "podsuwa&#263;"
  ]
  node [
    id 147
    label "overcharge"
  ]
  node [
    id 148
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 149
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 150
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 151
    label "przejmowa&#263;"
  ]
  node [
    id 152
    label "saturate"
  ]
  node [
    id 153
    label "return"
  ]
  node [
    id 154
    label "dostawa&#263;"
  ]
  node [
    id 155
    label "take"
  ]
  node [
    id 156
    label "wytwarza&#263;"
  ]
  node [
    id 157
    label "report"
  ]
  node [
    id 158
    label "dyskalkulia"
  ]
  node [
    id 159
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 160
    label "wynagrodzenie"
  ]
  node [
    id 161
    label "osi&#261;ga&#263;"
  ]
  node [
    id 162
    label "wymienia&#263;"
  ]
  node [
    id 163
    label "posiada&#263;"
  ]
  node [
    id 164
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 165
    label "wycenia&#263;"
  ]
  node [
    id 166
    label "bra&#263;"
  ]
  node [
    id 167
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 168
    label "mierzy&#263;"
  ]
  node [
    id 169
    label "rachowa&#263;"
  ]
  node [
    id 170
    label "count"
  ]
  node [
    id 171
    label "tell"
  ]
  node [
    id 172
    label "odlicza&#263;"
  ]
  node [
    id 173
    label "dodawa&#263;"
  ]
  node [
    id 174
    label "wyznacza&#263;"
  ]
  node [
    id 175
    label "admit"
  ]
  node [
    id 176
    label "policza&#263;"
  ]
  node [
    id 177
    label "okre&#347;la&#263;"
  ]
  node [
    id 178
    label "dostarcza&#263;"
  ]
  node [
    id 179
    label "kry&#263;"
  ]
  node [
    id 180
    label "get"
  ]
  node [
    id 181
    label "przenosi&#263;"
  ]
  node [
    id 182
    label "usi&#322;owa&#263;"
  ]
  node [
    id 183
    label "remove"
  ]
  node [
    id 184
    label "seclude"
  ]
  node [
    id 185
    label "przestawa&#263;"
  ]
  node [
    id 186
    label "przemieszcza&#263;"
  ]
  node [
    id 187
    label "przesuwa&#263;"
  ]
  node [
    id 188
    label "oddala&#263;"
  ]
  node [
    id 189
    label "dissolve"
  ]
  node [
    id 190
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 191
    label "retard"
  ]
  node [
    id 192
    label "blurt_out"
  ]
  node [
    id 193
    label "odci&#261;ga&#263;"
  ]
  node [
    id 194
    label "odk&#322;ada&#263;"
  ]
  node [
    id 195
    label "generalize"
  ]
  node [
    id 196
    label "sprawia&#263;"
  ]
  node [
    id 197
    label "demaskator"
  ]
  node [
    id 198
    label "dostrzega&#263;"
  ]
  node [
    id 199
    label "objawia&#263;"
  ]
  node [
    id 200
    label "unwrap"
  ]
  node [
    id 201
    label "informowa&#263;"
  ]
  node [
    id 202
    label "indicate"
  ]
  node [
    id 203
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 204
    label "zaczyna&#263;"
  ]
  node [
    id 205
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 206
    label "escalate"
  ]
  node [
    id 207
    label "pia&#263;"
  ]
  node [
    id 208
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 209
    label "przybli&#380;a&#263;"
  ]
  node [
    id 210
    label "ulepsza&#263;"
  ]
  node [
    id 211
    label "tire"
  ]
  node [
    id 212
    label "pomaga&#263;"
  ]
  node [
    id 213
    label "express"
  ]
  node [
    id 214
    label "chwali&#263;"
  ]
  node [
    id 215
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 216
    label "rise"
  ]
  node [
    id 217
    label "os&#322;awia&#263;"
  ]
  node [
    id 218
    label "odbudowywa&#263;"
  ]
  node [
    id 219
    label "drive"
  ]
  node [
    id 220
    label "zmienia&#263;"
  ]
  node [
    id 221
    label "enhance"
  ]
  node [
    id 222
    label "za&#322;apywa&#263;"
  ]
  node [
    id 223
    label "lift"
  ]
  node [
    id 224
    label "wynie&#347;&#263;"
  ]
  node [
    id 225
    label "pieni&#261;dze"
  ]
  node [
    id 226
    label "ilo&#347;&#263;"
  ]
  node [
    id 227
    label "limit"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
]
