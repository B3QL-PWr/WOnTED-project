graph [
  node [
    id 0
    label "gdyby"
    origin "text"
  ]
  node [
    id 1
    label "krytyk"
    origin "text"
  ]
  node [
    id 2
    label "jahus&#243;w"
    origin "text"
  ]
  node [
    id 3
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 4
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 5
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "mie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zupe&#322;ny"
    origin "text"
  ]
  node [
    id 8
    label "s&#322;uszno&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wiele"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "u&#380;ala&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "tyle"
    origin "text"
  ]
  node [
    id 14
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 15
    label "bezczelny"
    origin "text"
  ]
  node [
    id 16
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 17
    label "utrzymywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "moja"
    origin "text"
  ]
  node [
    id 19
    label "by&#263;"
    origin "text"
  ]
  node [
    id 20
    label "czysta"
    origin "text"
  ]
  node [
    id 21
    label "bajka"
    origin "text"
  ]
  node [
    id 22
    label "m&#243;zg"
    origin "text"
  ]
  node [
    id 23
    label "moi"
    origin "text"
  ]
  node [
    id 24
    label "wyleg&#322;y"
    origin "text"
  ]
  node [
    id 25
    label "tak"
    origin "text"
  ]
  node [
    id 26
    label "dalece"
    origin "text"
  ]
  node [
    id 27
    label "zuchwa&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 29
    label "posun&#261;&#263;"
    origin "text"
  ]
  node [
    id 30
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 31
    label "nie"
    origin "text"
  ]
  node [
    id 32
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 33
    label "jak"
    origin "text"
  ]
  node [
    id 34
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 35
    label "krytyka"
  ]
  node [
    id 36
    label "przeciwnik"
  ]
  node [
    id 37
    label "publicysta"
  ]
  node [
    id 38
    label "cz&#322;owiek"
  ]
  node [
    id 39
    label "sp&#243;&#322;zawodnik"
  ]
  node [
    id 40
    label "wojna"
  ]
  node [
    id 41
    label "posta&#263;"
  ]
  node [
    id 42
    label "konkurencja"
  ]
  node [
    id 43
    label "Conrad"
  ]
  node [
    id 44
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 45
    label "Gogol"
  ]
  node [
    id 46
    label "intelektualista"
  ]
  node [
    id 47
    label "autor"
  ]
  node [
    id 48
    label "Korwin"
  ]
  node [
    id 49
    label "Michnik"
  ]
  node [
    id 50
    label "diatryba"
  ]
  node [
    id 51
    label "ocena"
  ]
  node [
    id 52
    label "streszczenie"
  ]
  node [
    id 53
    label "tekst"
  ]
  node [
    id 54
    label "krytyka_literacka"
  ]
  node [
    id 55
    label "cenzura"
  ]
  node [
    id 56
    label "publiczno&#347;&#263;"
  ]
  node [
    id 57
    label "review"
  ]
  node [
    id 58
    label "criticism"
  ]
  node [
    id 59
    label "publicystyka"
  ]
  node [
    id 60
    label "sake"
  ]
  node [
    id 61
    label "rozciekawia&#263;"
  ]
  node [
    id 62
    label "alkohol"
  ]
  node [
    id 63
    label "&#322;&#261;czny"
  ]
  node [
    id 64
    label "zupe&#322;nie"
  ]
  node [
    id 65
    label "ca&#322;y"
  ]
  node [
    id 66
    label "kompletnie"
  ]
  node [
    id 67
    label "og&#243;lnie"
  ]
  node [
    id 68
    label "w_pizdu"
  ]
  node [
    id 69
    label "pe&#322;ny"
  ]
  node [
    id 70
    label "kompletny"
  ]
  node [
    id 71
    label "zdr&#243;w"
  ]
  node [
    id 72
    label "ca&#322;o"
  ]
  node [
    id 73
    label "du&#380;y"
  ]
  node [
    id 74
    label "calu&#347;ko"
  ]
  node [
    id 75
    label "podobny"
  ]
  node [
    id 76
    label "&#380;ywy"
  ]
  node [
    id 77
    label "jedyny"
  ]
  node [
    id 78
    label "&#322;&#261;cznie"
  ]
  node [
    id 79
    label "zbiorczy"
  ]
  node [
    id 80
    label "posp&#243;lnie"
  ]
  node [
    id 81
    label "generalny"
  ]
  node [
    id 82
    label "nadrz&#281;dnie"
  ]
  node [
    id 83
    label "og&#243;lny"
  ]
  node [
    id 84
    label "zbiorowo"
  ]
  node [
    id 85
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 86
    label "nieograniczony"
  ]
  node [
    id 87
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 88
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 89
    label "wype&#322;nienie"
  ]
  node [
    id 90
    label "bezwzgl&#281;dny"
  ]
  node [
    id 91
    label "satysfakcja"
  ]
  node [
    id 92
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 93
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 94
    label "r&#243;wny"
  ]
  node [
    id 95
    label "pe&#322;no"
  ]
  node [
    id 96
    label "otwarty"
  ]
  node [
    id 97
    label "wniwecz"
  ]
  node [
    id 98
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 99
    label "prawda"
  ]
  node [
    id 100
    label "zasadno&#347;&#263;"
  ]
  node [
    id 101
    label "prawdziwo&#347;&#263;"
  ]
  node [
    id 102
    label "cecha"
  ]
  node [
    id 103
    label "podobie&#324;stwo"
  ]
  node [
    id 104
    label "zgodno&#347;&#263;"
  ]
  node [
    id 105
    label "za&#322;o&#380;enie"
  ]
  node [
    id 106
    label "prawdziwy"
  ]
  node [
    id 107
    label "truth"
  ]
  node [
    id 108
    label "nieprawdziwy"
  ]
  node [
    id 109
    label "s&#261;d"
  ]
  node [
    id 110
    label "realia"
  ]
  node [
    id 111
    label "wyregulowanie"
  ]
  node [
    id 112
    label "charakterystyka"
  ]
  node [
    id 113
    label "zaleta"
  ]
  node [
    id 114
    label "kompetencja"
  ]
  node [
    id 115
    label "regulowa&#263;"
  ]
  node [
    id 116
    label "regulowanie"
  ]
  node [
    id 117
    label "feature"
  ]
  node [
    id 118
    label "standard"
  ]
  node [
    id 119
    label "attribute"
  ]
  node [
    id 120
    label "wyregulowa&#263;"
  ]
  node [
    id 121
    label "warto&#347;&#263;"
  ]
  node [
    id 122
    label "wiela"
  ]
  node [
    id 123
    label "du&#380;o"
  ]
  node [
    id 124
    label "znaczny"
  ]
  node [
    id 125
    label "wa&#380;ny"
  ]
  node [
    id 126
    label "niema&#322;o"
  ]
  node [
    id 127
    label "rozwini&#281;ty"
  ]
  node [
    id 128
    label "doros&#322;y"
  ]
  node [
    id 129
    label "dorodny"
  ]
  node [
    id 130
    label "konkretnie"
  ]
  node [
    id 131
    label "nieznacznie"
  ]
  node [
    id 132
    label "nieznaczny"
  ]
  node [
    id 133
    label "nieistotnie"
  ]
  node [
    id 134
    label "posilnie"
  ]
  node [
    id 135
    label "po&#380;ywnie"
  ]
  node [
    id 136
    label "&#322;adnie"
  ]
  node [
    id 137
    label "solidny"
  ]
  node [
    id 138
    label "tre&#347;ciwie"
  ]
  node [
    id 139
    label "konkretny"
  ]
  node [
    id 140
    label "nie&#378;le"
  ]
  node [
    id 141
    label "jasno"
  ]
  node [
    id 142
    label "dok&#322;adnie"
  ]
  node [
    id 143
    label "rozw&#243;d"
  ]
  node [
    id 144
    label "partner"
  ]
  node [
    id 145
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 146
    label "eksprezydent"
  ]
  node [
    id 147
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 148
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 149
    label "wcze&#347;niejszy"
  ]
  node [
    id 150
    label "dawny"
  ]
  node [
    id 151
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 152
    label "aktor"
  ]
  node [
    id 153
    label "sp&#243;lnik"
  ]
  node [
    id 154
    label "kolaborator"
  ]
  node [
    id 155
    label "prowadzi&#263;"
  ]
  node [
    id 156
    label "uczestniczenie"
  ]
  node [
    id 157
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 158
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 159
    label "pracownik"
  ]
  node [
    id 160
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 161
    label "przedsi&#281;biorca"
  ]
  node [
    id 162
    label "wcze&#347;niej"
  ]
  node [
    id 163
    label "od_dawna"
  ]
  node [
    id 164
    label "anachroniczny"
  ]
  node [
    id 165
    label "dawniej"
  ]
  node [
    id 166
    label "odleg&#322;y"
  ]
  node [
    id 167
    label "przesz&#322;y"
  ]
  node [
    id 168
    label "d&#322;ugoletni"
  ]
  node [
    id 169
    label "poprzedni"
  ]
  node [
    id 170
    label "dawno"
  ]
  node [
    id 171
    label "przestarza&#322;y"
  ]
  node [
    id 172
    label "kombatant"
  ]
  node [
    id 173
    label "niegdysiejszy"
  ]
  node [
    id 174
    label "stary"
  ]
  node [
    id 175
    label "rozbita_rodzina"
  ]
  node [
    id 176
    label "rozstanie"
  ]
  node [
    id 177
    label "separation"
  ]
  node [
    id 178
    label "uniewa&#380;nienie"
  ]
  node [
    id 179
    label "ekspartner"
  ]
  node [
    id 180
    label "prezydent"
  ]
  node [
    id 181
    label "niepokorny"
  ]
  node [
    id 182
    label "rozbestwienie_si&#281;"
  ]
  node [
    id 183
    label "rozzuchwalenie_si&#281;"
  ]
  node [
    id 184
    label "rozzuchwalanie_si&#281;"
  ]
  node [
    id 185
    label "rozbestwianie_si&#281;"
  ]
  node [
    id 186
    label "rozpanoszenie_si&#281;"
  ]
  node [
    id 187
    label "rozzuchwalenie"
  ]
  node [
    id 188
    label "zuchwale"
  ]
  node [
    id 189
    label "czelny"
  ]
  node [
    id 190
    label "pyszny"
  ]
  node [
    id 191
    label "harny"
  ]
  node [
    id 192
    label "rozzuchwalanie"
  ]
  node [
    id 193
    label "zuchwalczy"
  ]
  node [
    id 194
    label "odwa&#380;ny"
  ]
  node [
    id 195
    label "przedni"
  ]
  node [
    id 196
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 197
    label "pysznie"
  ]
  node [
    id 198
    label "napuszenie_si&#281;"
  ]
  node [
    id 199
    label "wynios&#322;y"
  ]
  node [
    id 200
    label "przesmaczny"
  ]
  node [
    id 201
    label "udany"
  ]
  node [
    id 202
    label "rozdymanie_si&#281;"
  ]
  node [
    id 203
    label "podufa&#322;y"
  ]
  node [
    id 204
    label "napuszanie_si&#281;"
  ]
  node [
    id 205
    label "wspania&#322;y"
  ]
  node [
    id 206
    label "dufny"
  ]
  node [
    id 207
    label "niestandardowy"
  ]
  node [
    id 208
    label "odwa&#380;nie"
  ]
  node [
    id 209
    label "niepos&#322;uszny"
  ]
  node [
    id 210
    label "niezale&#380;ny"
  ]
  node [
    id 211
    label "niepokornie"
  ]
  node [
    id 212
    label "zuchwa&#322;y"
  ]
  node [
    id 213
    label "&#322;adny"
  ]
  node [
    id 214
    label "czo&#322;owo"
  ]
  node [
    id 215
    label "gotowy"
  ]
  node [
    id 216
    label "czelnie"
  ]
  node [
    id 217
    label "powodowanie"
  ]
  node [
    id 218
    label "pob&#322;a&#380;anie"
  ]
  node [
    id 219
    label "spowodowanie"
  ]
  node [
    id 220
    label "czynno&#347;&#263;"
  ]
  node [
    id 221
    label "panowa&#263;"
  ]
  node [
    id 222
    label "manewr"
  ]
  node [
    id 223
    label "zachowywa&#263;"
  ]
  node [
    id 224
    label "twierdzi&#263;"
  ]
  node [
    id 225
    label "argue"
  ]
  node [
    id 226
    label "sprawowa&#263;"
  ]
  node [
    id 227
    label "s&#261;dzi&#263;"
  ]
  node [
    id 228
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 229
    label "byt"
  ]
  node [
    id 230
    label "podtrzymywa&#263;"
  ]
  node [
    id 231
    label "corroborate"
  ]
  node [
    id 232
    label "cope"
  ]
  node [
    id 233
    label "trzyma&#263;"
  ]
  node [
    id 234
    label "broni&#263;"
  ]
  node [
    id 235
    label "defy"
  ]
  node [
    id 236
    label "zapewnia&#263;"
  ]
  node [
    id 237
    label "robi&#263;"
  ]
  node [
    id 238
    label "przechowywa&#263;"
  ]
  node [
    id 239
    label "hold"
  ]
  node [
    id 240
    label "dieta"
  ]
  node [
    id 241
    label "zdyscyplinowanie"
  ]
  node [
    id 242
    label "behave"
  ]
  node [
    id 243
    label "post&#281;powa&#263;"
  ]
  node [
    id 244
    label "control"
  ]
  node [
    id 245
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 246
    label "tajemnica"
  ]
  node [
    id 247
    label "post"
  ]
  node [
    id 248
    label "pociesza&#263;"
  ]
  node [
    id 249
    label "back"
  ]
  node [
    id 250
    label "patronize"
  ]
  node [
    id 251
    label "reinforce"
  ]
  node [
    id 252
    label "adhere"
  ]
  node [
    id 253
    label "wychowywa&#263;"
  ]
  node [
    id 254
    label "hodowa&#263;"
  ]
  node [
    id 255
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 256
    label "sympatyzowa&#263;"
  ]
  node [
    id 257
    label "pozostawa&#263;"
  ]
  node [
    id 258
    label "zmusza&#263;"
  ]
  node [
    id 259
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 260
    label "continue"
  ]
  node [
    id 261
    label "treat"
  ]
  node [
    id 262
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 263
    label "administrowa&#263;"
  ]
  node [
    id 264
    label "przetrzymywa&#263;"
  ]
  node [
    id 265
    label "dzier&#380;y&#263;"
  ]
  node [
    id 266
    label "komunikowa&#263;"
  ]
  node [
    id 267
    label "oznajmia&#263;"
  ]
  node [
    id 268
    label "attest"
  ]
  node [
    id 269
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 270
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 271
    label "powodowa&#263;"
  ]
  node [
    id 272
    label "zas&#261;dza&#263;"
  ]
  node [
    id 273
    label "deliver"
  ]
  node [
    id 274
    label "os&#261;dza&#263;"
  ]
  node [
    id 275
    label "my&#347;le&#263;"
  ]
  node [
    id 276
    label "defray"
  ]
  node [
    id 277
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 278
    label "reprezentowa&#263;"
  ]
  node [
    id 279
    label "chroni&#263;"
  ]
  node [
    id 280
    label "refuse"
  ]
  node [
    id 281
    label "gra&#263;"
  ]
  node [
    id 282
    label "walczy&#263;"
  ]
  node [
    id 283
    label "fend"
  ]
  node [
    id 284
    label "resist"
  ]
  node [
    id 285
    label "czuwa&#263;"
  ]
  node [
    id 286
    label "udowadnia&#263;"
  ]
  node [
    id 287
    label "rebuff"
  ]
  node [
    id 288
    label "preach"
  ]
  node [
    id 289
    label "adwokatowa&#263;"
  ]
  node [
    id 290
    label "zdawa&#263;"
  ]
  node [
    id 291
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 292
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 293
    label "prosecute"
  ]
  node [
    id 294
    label "dominowa&#263;"
  ]
  node [
    id 295
    label "kontrolowa&#263;"
  ]
  node [
    id 296
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 297
    label "kierowa&#263;"
  ]
  node [
    id 298
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 299
    label "istnie&#263;"
  ]
  node [
    id 300
    label "przewa&#380;a&#263;"
  ]
  node [
    id 301
    label "manipulate"
  ]
  node [
    id 302
    label "informowa&#263;"
  ]
  node [
    id 303
    label "dostarcza&#263;"
  ]
  node [
    id 304
    label "move"
  ]
  node [
    id 305
    label "maneuver"
  ]
  node [
    id 306
    label "utrzyma&#263;"
  ]
  node [
    id 307
    label "myk"
  ]
  node [
    id 308
    label "utrzymanie"
  ]
  node [
    id 309
    label "wydarzenie"
  ]
  node [
    id 310
    label "utrzymywanie"
  ]
  node [
    id 311
    label "ruch"
  ]
  node [
    id 312
    label "taktyka"
  ]
  node [
    id 313
    label "movement"
  ]
  node [
    id 314
    label "posuni&#281;cie"
  ]
  node [
    id 315
    label "ontologicznie"
  ]
  node [
    id 316
    label "bycie"
  ]
  node [
    id 317
    label "entity"
  ]
  node [
    id 318
    label "egzystencja"
  ]
  node [
    id 319
    label "wy&#380;ywienie"
  ]
  node [
    id 320
    label "potencja"
  ]
  node [
    id 321
    label "subsystencja"
  ]
  node [
    id 322
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 323
    label "stan"
  ]
  node [
    id 324
    label "stand"
  ]
  node [
    id 325
    label "trwa&#263;"
  ]
  node [
    id 326
    label "equal"
  ]
  node [
    id 327
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 328
    label "chodzi&#263;"
  ]
  node [
    id 329
    label "uczestniczy&#263;"
  ]
  node [
    id 330
    label "obecno&#347;&#263;"
  ]
  node [
    id 331
    label "si&#281;ga&#263;"
  ]
  node [
    id 332
    label "mie&#263;_miejsce"
  ]
  node [
    id 333
    label "participate"
  ]
  node [
    id 334
    label "zostawa&#263;"
  ]
  node [
    id 335
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 336
    label "compass"
  ]
  node [
    id 337
    label "exsert"
  ]
  node [
    id 338
    label "get"
  ]
  node [
    id 339
    label "u&#380;ywa&#263;"
  ]
  node [
    id 340
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 341
    label "osi&#261;ga&#263;"
  ]
  node [
    id 342
    label "korzysta&#263;"
  ]
  node [
    id 343
    label "appreciation"
  ]
  node [
    id 344
    label "dociera&#263;"
  ]
  node [
    id 345
    label "mierzy&#263;"
  ]
  node [
    id 346
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 347
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 348
    label "being"
  ]
  node [
    id 349
    label "proceed"
  ]
  node [
    id 350
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 351
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 352
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 353
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 354
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 355
    label "str&#243;j"
  ]
  node [
    id 356
    label "para"
  ]
  node [
    id 357
    label "krok"
  ]
  node [
    id 358
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 359
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 360
    label "przebiega&#263;"
  ]
  node [
    id 361
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 362
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 363
    label "carry"
  ]
  node [
    id 364
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 365
    label "wk&#322;ada&#263;"
  ]
  node [
    id 366
    label "p&#322;ywa&#263;"
  ]
  node [
    id 367
    label "bangla&#263;"
  ]
  node [
    id 368
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 369
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 370
    label "bywa&#263;"
  ]
  node [
    id 371
    label "tryb"
  ]
  node [
    id 372
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 373
    label "dziama&#263;"
  ]
  node [
    id 374
    label "run"
  ]
  node [
    id 375
    label "stara&#263;_si&#281;"
  ]
  node [
    id 376
    label "Arakan"
  ]
  node [
    id 377
    label "Teksas"
  ]
  node [
    id 378
    label "Georgia"
  ]
  node [
    id 379
    label "Maryland"
  ]
  node [
    id 380
    label "warstwa"
  ]
  node [
    id 381
    label "Luizjana"
  ]
  node [
    id 382
    label "Massachusetts"
  ]
  node [
    id 383
    label "Michigan"
  ]
  node [
    id 384
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 385
    label "samopoczucie"
  ]
  node [
    id 386
    label "Floryda"
  ]
  node [
    id 387
    label "Ohio"
  ]
  node [
    id 388
    label "Alaska"
  ]
  node [
    id 389
    label "Nowy_Meksyk"
  ]
  node [
    id 390
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 391
    label "wci&#281;cie"
  ]
  node [
    id 392
    label "Kansas"
  ]
  node [
    id 393
    label "Alabama"
  ]
  node [
    id 394
    label "miejsce"
  ]
  node [
    id 395
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 396
    label "Kalifornia"
  ]
  node [
    id 397
    label "Wirginia"
  ]
  node [
    id 398
    label "punkt"
  ]
  node [
    id 399
    label "Nowy_York"
  ]
  node [
    id 400
    label "Waszyngton"
  ]
  node [
    id 401
    label "Pensylwania"
  ]
  node [
    id 402
    label "wektor"
  ]
  node [
    id 403
    label "Hawaje"
  ]
  node [
    id 404
    label "state"
  ]
  node [
    id 405
    label "poziom"
  ]
  node [
    id 406
    label "jednostka_administracyjna"
  ]
  node [
    id 407
    label "Illinois"
  ]
  node [
    id 408
    label "Oklahoma"
  ]
  node [
    id 409
    label "Jukatan"
  ]
  node [
    id 410
    label "Arizona"
  ]
  node [
    id 411
    label "ilo&#347;&#263;"
  ]
  node [
    id 412
    label "Oregon"
  ]
  node [
    id 413
    label "shape"
  ]
  node [
    id 414
    label "Goa"
  ]
  node [
    id 415
    label "w&#243;dka"
  ]
  node [
    id 416
    label "czy&#347;ciocha"
  ]
  node [
    id 417
    label "mohorycz"
  ]
  node [
    id 418
    label "gorza&#322;ka"
  ]
  node [
    id 419
    label "sznaps"
  ]
  node [
    id 420
    label "nap&#243;j"
  ]
  node [
    id 421
    label "higienistka"
  ]
  node [
    id 422
    label "mora&#322;"
  ]
  node [
    id 423
    label "opowie&#347;&#263;"
  ]
  node [
    id 424
    label "Pok&#233;mon"
  ]
  node [
    id 425
    label "g&#322;upstwo"
  ]
  node [
    id 426
    label "narrative"
  ]
  node [
    id 427
    label "film"
  ]
  node [
    id 428
    label "utw&#243;r"
  ]
  node [
    id 429
    label "morfing"
  ]
  node [
    id 430
    label "komfort"
  ]
  node [
    id 431
    label "apolog"
  ]
  node [
    id 432
    label "sytuacja"
  ]
  node [
    id 433
    label "epika"
  ]
  node [
    id 434
    label "warunki"
  ]
  node [
    id 435
    label "szczeg&#243;&#322;"
  ]
  node [
    id 436
    label "motyw"
  ]
  node [
    id 437
    label "b&#322;ona"
  ]
  node [
    id 438
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 439
    label "trawiarnia"
  ]
  node [
    id 440
    label "sztuka"
  ]
  node [
    id 441
    label "emulsja_fotograficzna"
  ]
  node [
    id 442
    label "rozbieg&#243;wka"
  ]
  node [
    id 443
    label "sklejarka"
  ]
  node [
    id 444
    label "odczuli&#263;"
  ]
  node [
    id 445
    label "filmoteka"
  ]
  node [
    id 446
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 447
    label "dorobek"
  ]
  node [
    id 448
    label "animatronika"
  ]
  node [
    id 449
    label "napisy"
  ]
  node [
    id 450
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 451
    label "blik"
  ]
  node [
    id 452
    label "odczula&#263;"
  ]
  node [
    id 453
    label "odczulenie"
  ]
  node [
    id 454
    label "photograph"
  ]
  node [
    id 455
    label "scena"
  ]
  node [
    id 456
    label "klatka"
  ]
  node [
    id 457
    label "ta&#347;ma"
  ]
  node [
    id 458
    label "uj&#281;cie"
  ]
  node [
    id 459
    label "anamorfoza"
  ]
  node [
    id 460
    label "czo&#322;&#243;wka"
  ]
  node [
    id 461
    label "odczulanie"
  ]
  node [
    id 462
    label "postprodukcja"
  ]
  node [
    id 463
    label "muza"
  ]
  node [
    id 464
    label "block"
  ]
  node [
    id 465
    label "rola"
  ]
  node [
    id 466
    label "ty&#322;&#243;wka"
  ]
  node [
    id 467
    label "komunikat"
  ]
  node [
    id 468
    label "part"
  ]
  node [
    id 469
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 470
    label "tre&#347;&#263;"
  ]
  node [
    id 471
    label "obrazowanie"
  ]
  node [
    id 472
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 473
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 474
    label "element_anatomiczny"
  ]
  node [
    id 475
    label "organ"
  ]
  node [
    id 476
    label "wypowied&#378;"
  ]
  node [
    id 477
    label "opowiadanie"
  ]
  node [
    id 478
    label "report"
  ]
  node [
    id 479
    label "fabu&#322;a"
  ]
  node [
    id 480
    label "czyn"
  ]
  node [
    id 481
    label "stupidity"
  ]
  node [
    id 482
    label "g&#243;wno"
  ]
  node [
    id 483
    label "sofcik"
  ]
  node [
    id 484
    label "nonsense"
  ]
  node [
    id 485
    label "banalny"
  ]
  node [
    id 486
    label "baj&#281;da"
  ]
  node [
    id 487
    label "furda"
  ]
  node [
    id 488
    label "farmazon"
  ]
  node [
    id 489
    label "g&#322;upota"
  ]
  node [
    id 490
    label "emocja"
  ]
  node [
    id 491
    label "ease"
  ]
  node [
    id 492
    label "przypowie&#347;&#263;"
  ]
  node [
    id 493
    label "wniosek"
  ]
  node [
    id 494
    label "moral"
  ]
  node [
    id 495
    label "nauka"
  ]
  node [
    id 496
    label "animacja"
  ]
  node [
    id 497
    label "przeobra&#380;anie"
  ]
  node [
    id 498
    label "epos"
  ]
  node [
    id 499
    label "literatura"
  ]
  node [
    id 500
    label "utw&#243;r_epicki"
  ]
  node [
    id 501
    label "nowelistyka"
  ]
  node [
    id 502
    label "rodzaj_literacki"
  ]
  node [
    id 503
    label "romans"
  ]
  node [
    id 504
    label "fantastyka"
  ]
  node [
    id 505
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 506
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 507
    label "przedmurze"
  ]
  node [
    id 508
    label "umys&#322;"
  ]
  node [
    id 509
    label "wiedza"
  ]
  node [
    id 510
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 511
    label "podwzg&#243;rze"
  ]
  node [
    id 512
    label "encefalografia"
  ]
  node [
    id 513
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 514
    label "przysadka"
  ]
  node [
    id 515
    label "poduszka"
  ]
  node [
    id 516
    label "elektroencefalogram"
  ]
  node [
    id 517
    label "przodom&#243;zgowie"
  ]
  node [
    id 518
    label "projektodawca"
  ]
  node [
    id 519
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 520
    label "kora_m&#243;zgowa"
  ]
  node [
    id 521
    label "bruzda"
  ]
  node [
    id 522
    label "kresom&#243;zgowie"
  ]
  node [
    id 523
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 524
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 525
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 526
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 527
    label "zw&#243;j"
  ]
  node [
    id 528
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 529
    label "substancja_szara"
  ]
  node [
    id 530
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 531
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 532
    label "g&#322;owa"
  ]
  node [
    id 533
    label "most"
  ]
  node [
    id 534
    label "noosfera"
  ]
  node [
    id 535
    label "wzg&#243;rze"
  ]
  node [
    id 536
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 537
    label "uk&#322;ad"
  ]
  node [
    id 538
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 539
    label "Komitet_Region&#243;w"
  ]
  node [
    id 540
    label "struktura_anatomiczna"
  ]
  node [
    id 541
    label "organogeneza"
  ]
  node [
    id 542
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 543
    label "tw&#243;r"
  ]
  node [
    id 544
    label "tkanka"
  ]
  node [
    id 545
    label "stomia"
  ]
  node [
    id 546
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 547
    label "budowa"
  ]
  node [
    id 548
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 549
    label "okolica"
  ]
  node [
    id 550
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 551
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 552
    label "dekortykacja"
  ]
  node [
    id 553
    label "Izba_Konsyliarska"
  ]
  node [
    id 554
    label "zesp&#243;&#322;"
  ]
  node [
    id 555
    label "jednostka_organizacyjna"
  ]
  node [
    id 556
    label "inicjator"
  ]
  node [
    id 557
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 558
    label "zdolno&#347;&#263;"
  ]
  node [
    id 559
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 560
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 561
    label "obiekt"
  ]
  node [
    id 562
    label "czaszka"
  ]
  node [
    id 563
    label "g&#243;ra"
  ]
  node [
    id 564
    label "fryzura"
  ]
  node [
    id 565
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 566
    label "pryncypa&#322;"
  ]
  node [
    id 567
    label "ro&#347;lina"
  ]
  node [
    id 568
    label "ucho"
  ]
  node [
    id 569
    label "byd&#322;o"
  ]
  node [
    id 570
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 571
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 572
    label "kierownictwo"
  ]
  node [
    id 573
    label "&#347;ci&#281;cie"
  ]
  node [
    id 574
    label "cz&#322;onek"
  ]
  node [
    id 575
    label "makrocefalia"
  ]
  node [
    id 576
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 577
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 578
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 579
    label "&#380;ycie"
  ]
  node [
    id 580
    label "dekiel"
  ]
  node [
    id 581
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 582
    label "&#347;ci&#281;gno"
  ]
  node [
    id 583
    label "cia&#322;o"
  ]
  node [
    id 584
    label "kszta&#322;t"
  ]
  node [
    id 585
    label "m&#322;ot"
  ]
  node [
    id 586
    label "marka"
  ]
  node [
    id 587
    label "pr&#243;ba"
  ]
  node [
    id 588
    label "drzewo"
  ]
  node [
    id 589
    label "znak"
  ]
  node [
    id 590
    label "melanotropina"
  ]
  node [
    id 591
    label "gruczo&#322;_dokrewny"
  ]
  node [
    id 592
    label "li&#347;&#263;"
  ]
  node [
    id 593
    label "p&#243;&#322;kula_m&#243;zgu"
  ]
  node [
    id 594
    label "p&#322;at_skroniowy"
  ]
  node [
    id 595
    label "ga&#322;ka_blada"
  ]
  node [
    id 596
    label "p&#322;at_potyliczny"
  ]
  node [
    id 597
    label "w&#281;chom&#243;zgowie"
  ]
  node [
    id 598
    label "cia&#322;o_modzelowate"
  ]
  node [
    id 599
    label "kresom&#243;zgowie_&#347;rodkowe"
  ]
  node [
    id 600
    label "wyspa"
  ]
  node [
    id 601
    label "bruzda_przed&#347;rodkowa"
  ]
  node [
    id 602
    label "cerebrum"
  ]
  node [
    id 603
    label "p&#322;at_ciemieniowy"
  ]
  node [
    id 604
    label "j&#261;dro_podstawne"
  ]
  node [
    id 605
    label "p&#322;at_czo&#322;owy"
  ]
  node [
    id 606
    label "podroby"
  ]
  node [
    id 607
    label "cerebellum"
  ]
  node [
    id 608
    label "intelekt"
  ]
  node [
    id 609
    label "j&#261;dro_z&#281;bate"
  ]
  node [
    id 610
    label "kom&#243;rka_Purkyniego"
  ]
  node [
    id 611
    label "zam&#243;zgowie"
  ]
  node [
    id 612
    label "robak"
  ]
  node [
    id 613
    label "kink"
  ]
  node [
    id 614
    label "rolka"
  ]
  node [
    id 615
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 616
    label "wrench"
  ]
  node [
    id 617
    label "manuskrypt"
  ]
  node [
    id 618
    label "egzemplarz"
  ]
  node [
    id 619
    label "plik"
  ]
  node [
    id 620
    label "fa&#322;da"
  ]
  node [
    id 621
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 622
    label "szczelina"
  ]
  node [
    id 623
    label "line"
  ]
  node [
    id 624
    label "rowkowa&#263;"
  ]
  node [
    id 625
    label "zmarszczka"
  ]
  node [
    id 626
    label "diencephalon"
  ]
  node [
    id 627
    label "lejek"
  ]
  node [
    id 628
    label "zawzg&#243;rze"
  ]
  node [
    id 629
    label "niskowzg&#243;rze"
  ]
  node [
    id 630
    label "cia&#322;o_suteczkowate"
  ]
  node [
    id 631
    label "wyko&#324;czenie"
  ]
  node [
    id 632
    label "wype&#322;niacz"
  ]
  node [
    id 633
    label "przedmiot"
  ]
  node [
    id 634
    label "kanapa"
  ]
  node [
    id 635
    label "&#322;apa"
  ]
  node [
    id 636
    label "piecz&#261;tka"
  ]
  node [
    id 637
    label "po&#347;ciel"
  ]
  node [
    id 638
    label "fotel"
  ]
  node [
    id 639
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 640
    label "palec"
  ]
  node [
    id 641
    label "podpora"
  ]
  node [
    id 642
    label "d&#322;o&#324;"
  ]
  node [
    id 643
    label "wyst&#281;p"
  ]
  node [
    id 644
    label "mur"
  ]
  node [
    id 645
    label "teren"
  ]
  node [
    id 646
    label "ostoja"
  ]
  node [
    id 647
    label "suwnica"
  ]
  node [
    id 648
    label "nap&#281;d"
  ]
  node [
    id 649
    label "prz&#281;s&#322;o"
  ]
  node [
    id 650
    label "pylon"
  ]
  node [
    id 651
    label "rzuci&#263;"
  ]
  node [
    id 652
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 653
    label "rzuca&#263;"
  ]
  node [
    id 654
    label "samoch&#243;d"
  ]
  node [
    id 655
    label "obiekt_mostowy"
  ]
  node [
    id 656
    label "bridge"
  ]
  node [
    id 657
    label "szczelina_dylatacyjna"
  ]
  node [
    id 658
    label "jarzmo_mostowe"
  ]
  node [
    id 659
    label "rzucanie"
  ]
  node [
    id 660
    label "porozumienie"
  ]
  node [
    id 661
    label "rzucenie"
  ]
  node [
    id 662
    label "trasa"
  ]
  node [
    id 663
    label "urz&#261;dzenie"
  ]
  node [
    id 664
    label "hindbrain"
  ]
  node [
    id 665
    label "Wawel"
  ]
  node [
    id 666
    label "Awentyn"
  ]
  node [
    id 667
    label "Kapitol"
  ]
  node [
    id 668
    label "Eskwilin"
  ]
  node [
    id 669
    label "Kwiryna&#322;"
  ]
  node [
    id 670
    label "Palatyn"
  ]
  node [
    id 671
    label "wzniesienie"
  ]
  node [
    id 672
    label "Syjon"
  ]
  node [
    id 673
    label "istota_czarna"
  ]
  node [
    id 674
    label "pokrywa"
  ]
  node [
    id 675
    label "hipoplazja_cia&#322;a_modzelowatego"
  ]
  node [
    id 676
    label "holoprozencefalia"
  ]
  node [
    id 677
    label "forebrain"
  ]
  node [
    id 678
    label "agenezja_cia&#322;a_modzelowatego"
  ]
  node [
    id 679
    label "pomieszanie_si&#281;"
  ]
  node [
    id 680
    label "pami&#281;&#263;"
  ]
  node [
    id 681
    label "wyobra&#378;nia"
  ]
  node [
    id 682
    label "wn&#281;trze"
  ]
  node [
    id 683
    label "wada_wrodzona"
  ]
  node [
    id 684
    label "badanie"
  ]
  node [
    id 685
    label "wynik_badania"
  ]
  node [
    id 686
    label "electroencephalogram"
  ]
  node [
    id 687
    label "elektroencefalografia"
  ]
  node [
    id 688
    label "encephalography"
  ]
  node [
    id 689
    label "pozwolenie"
  ]
  node [
    id 690
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 691
    label "zaawansowanie"
  ]
  node [
    id 692
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 693
    label "wykszta&#322;cenie"
  ]
  node [
    id 694
    label "cognition"
  ]
  node [
    id 695
    label "zniszczony"
  ]
  node [
    id 696
    label "niedobry"
  ]
  node [
    id 697
    label "znacznie"
  ]
  node [
    id 698
    label "mocno"
  ]
  node [
    id 699
    label "cz&#281;sto"
  ]
  node [
    id 700
    label "bardzo"
  ]
  node [
    id 701
    label "zauwa&#380;alnie"
  ]
  node [
    id 702
    label "wynios&#322;o&#347;&#263;"
  ]
  node [
    id 703
    label "niepokorno&#347;&#263;"
  ]
  node [
    id 704
    label "charakterek"
  ]
  node [
    id 705
    label "chojractwo"
  ]
  node [
    id 706
    label "pewno&#347;&#263;_siebie"
  ]
  node [
    id 707
    label "niegrzeczno&#347;&#263;"
  ]
  node [
    id 708
    label "heart"
  ]
  node [
    id 709
    label "pyszno&#347;&#263;"
  ]
  node [
    id 710
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 711
    label "Sikornik"
  ]
  node [
    id 712
    label "Izera"
  ]
  node [
    id 713
    label "Bukowiec"
  ]
  node [
    id 714
    label "Bielec"
  ]
  node [
    id 715
    label "Skalnik"
  ]
  node [
    id 716
    label "pogardliwo&#347;&#263;"
  ]
  node [
    id 717
    label "nastawienie"
  ]
  node [
    id 718
    label "Zwalisko"
  ]
  node [
    id 719
    label "wierzchowina"
  ]
  node [
    id 720
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 721
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 722
    label "niepos&#322;usze&#324;stwo"
  ]
  node [
    id 723
    label "postawa"
  ]
  node [
    id 724
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 725
    label "zachowanie"
  ]
  node [
    id 726
    label "niestosowno&#347;&#263;"
  ]
  node [
    id 727
    label "tupet"
  ]
  node [
    id 728
    label "temperament"
  ]
  node [
    id 729
    label "bli&#378;ni"
  ]
  node [
    id 730
    label "swojak"
  ]
  node [
    id 731
    label "odpowiedni"
  ]
  node [
    id 732
    label "samodzielny"
  ]
  node [
    id 733
    label "osobny"
  ]
  node [
    id 734
    label "samodzielnie"
  ]
  node [
    id 735
    label "indywidualny"
  ]
  node [
    id 736
    label "niepodleg&#322;y"
  ]
  node [
    id 737
    label "czyj&#347;"
  ]
  node [
    id 738
    label "autonomicznie"
  ]
  node [
    id 739
    label "odr&#281;bny"
  ]
  node [
    id 740
    label "sobieradzki"
  ]
  node [
    id 741
    label "w&#322;asny"
  ]
  node [
    id 742
    label "asymilowa&#263;"
  ]
  node [
    id 743
    label "nasada"
  ]
  node [
    id 744
    label "profanum"
  ]
  node [
    id 745
    label "wz&#243;r"
  ]
  node [
    id 746
    label "senior"
  ]
  node [
    id 747
    label "asymilowanie"
  ]
  node [
    id 748
    label "os&#322;abia&#263;"
  ]
  node [
    id 749
    label "homo_sapiens"
  ]
  node [
    id 750
    label "osoba"
  ]
  node [
    id 751
    label "ludzko&#347;&#263;"
  ]
  node [
    id 752
    label "Adam"
  ]
  node [
    id 753
    label "hominid"
  ]
  node [
    id 754
    label "portrecista"
  ]
  node [
    id 755
    label "polifag"
  ]
  node [
    id 756
    label "podw&#322;adny"
  ]
  node [
    id 757
    label "dwun&#243;g"
  ]
  node [
    id 758
    label "wapniak"
  ]
  node [
    id 759
    label "duch"
  ]
  node [
    id 760
    label "os&#322;abianie"
  ]
  node [
    id 761
    label "antropochoria"
  ]
  node [
    id 762
    label "figura"
  ]
  node [
    id 763
    label "mikrokosmos"
  ]
  node [
    id 764
    label "oddzia&#322;ywanie"
  ]
  node [
    id 765
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 766
    label "stosownie"
  ]
  node [
    id 767
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 768
    label "nale&#380;yty"
  ]
  node [
    id 769
    label "zdarzony"
  ]
  node [
    id 770
    label "odpowiednio"
  ]
  node [
    id 771
    label "specjalny"
  ]
  node [
    id 772
    label "odpowiadanie"
  ]
  node [
    id 773
    label "nale&#380;ny"
  ]
  node [
    id 774
    label "shift"
  ]
  node [
    id 775
    label "advance"
  ]
  node [
    id 776
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 777
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 778
    label "przyspieszy&#263;"
  ]
  node [
    id 779
    label "seize"
  ]
  node [
    id 780
    label "pozyska&#263;"
  ]
  node [
    id 781
    label "wyciupcia&#263;"
  ]
  node [
    id 782
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 783
    label "give_birth"
  ]
  node [
    id 784
    label "wyrucha&#263;"
  ]
  node [
    id 785
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 786
    label "obskoczy&#263;"
  ]
  node [
    id 787
    label "go"
  ]
  node [
    id 788
    label "spowodowa&#263;"
  ]
  node [
    id 789
    label "travel"
  ]
  node [
    id 790
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 791
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 792
    label "heat"
  ]
  node [
    id 793
    label "klawisz"
  ]
  node [
    id 794
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 795
    label "okre&#347;li&#263;"
  ]
  node [
    id 796
    label "express"
  ]
  node [
    id 797
    label "wydoby&#263;"
  ]
  node [
    id 798
    label "wyrazi&#263;"
  ]
  node [
    id 799
    label "poda&#263;"
  ]
  node [
    id 800
    label "unwrap"
  ]
  node [
    id 801
    label "rzekn&#261;&#263;"
  ]
  node [
    id 802
    label "discover"
  ]
  node [
    id 803
    label "convey"
  ]
  node [
    id 804
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 805
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 806
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 807
    label "siatk&#243;wka"
  ]
  node [
    id 808
    label "nafaszerowa&#263;"
  ]
  node [
    id 809
    label "supply"
  ]
  node [
    id 810
    label "tenis"
  ]
  node [
    id 811
    label "jedzenie"
  ]
  node [
    id 812
    label "ustawi&#263;"
  ]
  node [
    id 813
    label "poinformowa&#263;"
  ]
  node [
    id 814
    label "give"
  ]
  node [
    id 815
    label "zagra&#263;"
  ]
  node [
    id 816
    label "da&#263;"
  ]
  node [
    id 817
    label "zaserwowa&#263;"
  ]
  node [
    id 818
    label "introduce"
  ]
  node [
    id 819
    label "wydosta&#263;"
  ]
  node [
    id 820
    label "wyj&#261;&#263;"
  ]
  node [
    id 821
    label "ocali&#263;"
  ]
  node [
    id 822
    label "g&#243;rnictwo"
  ]
  node [
    id 823
    label "distill"
  ]
  node [
    id 824
    label "extract"
  ]
  node [
    id 825
    label "obtain"
  ]
  node [
    id 826
    label "uwydatni&#263;"
  ]
  node [
    id 827
    label "draw"
  ]
  node [
    id 828
    label "raise"
  ]
  node [
    id 829
    label "wyeksploatowa&#263;"
  ]
  node [
    id 830
    label "wyda&#263;"
  ]
  node [
    id 831
    label "uzyska&#263;"
  ]
  node [
    id 832
    label "doby&#263;"
  ]
  node [
    id 833
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 834
    label "testify"
  ]
  node [
    id 835
    label "oznaczy&#263;"
  ]
  node [
    id 836
    label "zakomunikowa&#263;"
  ]
  node [
    id 837
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 838
    label "vent"
  ]
  node [
    id 839
    label "situate"
  ]
  node [
    id 840
    label "zdecydowa&#263;"
  ]
  node [
    id 841
    label "zrobi&#263;"
  ]
  node [
    id 842
    label "nominate"
  ]
  node [
    id 843
    label "sprzeciw"
  ]
  node [
    id 844
    label "reakcja"
  ]
  node [
    id 845
    label "czerwona_kartka"
  ]
  node [
    id 846
    label "protestacja"
  ]
  node [
    id 847
    label "m&#261;&#380;"
  ]
  node [
    id 848
    label "prywatny"
  ]
  node [
    id 849
    label "pan_i_w&#322;adca"
  ]
  node [
    id 850
    label "pan_m&#322;ody"
  ]
  node [
    id 851
    label "ch&#322;op"
  ]
  node [
    id 852
    label "&#347;lubny"
  ]
  node [
    id 853
    label "pan_domu"
  ]
  node [
    id 854
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 855
    label "ma&#322;&#380;onek"
  ]
  node [
    id 856
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 857
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 858
    label "zobo"
  ]
  node [
    id 859
    label "dzo"
  ]
  node [
    id 860
    label "yakalo"
  ]
  node [
    id 861
    label "zbi&#243;r"
  ]
  node [
    id 862
    label "kr&#281;torogie"
  ]
  node [
    id 863
    label "livestock"
  ]
  node [
    id 864
    label "posp&#243;lstwo"
  ]
  node [
    id 865
    label "kraal"
  ]
  node [
    id 866
    label "czochrad&#322;o"
  ]
  node [
    id 867
    label "prze&#380;uwacz"
  ]
  node [
    id 868
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 869
    label "bizon"
  ]
  node [
    id 870
    label "zebu"
  ]
  node [
    id 871
    label "byd&#322;o_domowe"
  ]
  node [
    id 872
    label "zwierz&#281;"
  ]
  node [
    id 873
    label "ludno&#347;&#263;"
  ]
  node [
    id 874
    label "monogamia"
  ]
  node [
    id 875
    label "grzbiet"
  ]
  node [
    id 876
    label "bestia"
  ]
  node [
    id 877
    label "treser"
  ]
  node [
    id 878
    label "agresja"
  ]
  node [
    id 879
    label "niecz&#322;owiek"
  ]
  node [
    id 880
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 881
    label "skubni&#281;cie"
  ]
  node [
    id 882
    label "skuba&#263;"
  ]
  node [
    id 883
    label "tresowa&#263;"
  ]
  node [
    id 884
    label "oz&#243;r"
  ]
  node [
    id 885
    label "istota_&#380;ywa"
  ]
  node [
    id 886
    label "wylinka"
  ]
  node [
    id 887
    label "poskramia&#263;"
  ]
  node [
    id 888
    label "fukni&#281;cie"
  ]
  node [
    id 889
    label "siedzenie"
  ]
  node [
    id 890
    label "wios&#322;owa&#263;"
  ]
  node [
    id 891
    label "zwyrol"
  ]
  node [
    id 892
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 893
    label "budowa_cia&#322;a"
  ]
  node [
    id 894
    label "wiwarium"
  ]
  node [
    id 895
    label "sodomita"
  ]
  node [
    id 896
    label "oswaja&#263;"
  ]
  node [
    id 897
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 898
    label "degenerat"
  ]
  node [
    id 899
    label "le&#380;e&#263;"
  ]
  node [
    id 900
    label "przyssawka"
  ]
  node [
    id 901
    label "animalista"
  ]
  node [
    id 902
    label "fauna"
  ]
  node [
    id 903
    label "hodowla"
  ]
  node [
    id 904
    label "popapraniec"
  ]
  node [
    id 905
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 906
    label "le&#380;enie"
  ]
  node [
    id 907
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 908
    label "poligamia"
  ]
  node [
    id 909
    label "siedzie&#263;"
  ]
  node [
    id 910
    label "napasienie_si&#281;"
  ]
  node [
    id 911
    label "&#322;eb"
  ]
  node [
    id 912
    label "paszcza"
  ]
  node [
    id 913
    label "czerniak"
  ]
  node [
    id 914
    label "zwierz&#281;ta"
  ]
  node [
    id 915
    label "wios&#322;owanie"
  ]
  node [
    id 916
    label "skubn&#261;&#263;"
  ]
  node [
    id 917
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 918
    label "skubanie"
  ]
  node [
    id 919
    label "okrutnik"
  ]
  node [
    id 920
    label "pasienie_si&#281;"
  ]
  node [
    id 921
    label "farba"
  ]
  node [
    id 922
    label "weterynarz"
  ]
  node [
    id 923
    label "gad"
  ]
  node [
    id 924
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 925
    label "fukanie"
  ]
  node [
    id 926
    label "ch&#322;opstwo"
  ]
  node [
    id 927
    label "innowierstwo"
  ]
  node [
    id 928
    label "zbiorowo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 29
  ]
  edge [
    source 10
    target 30
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 229
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 237
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 239
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 256
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 259
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 262
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 264
  ]
  edge [
    source 17
    target 265
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 267
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 17
    target 269
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 17
    target 271
  ]
  edge [
    source 17
    target 272
  ]
  edge [
    source 17
    target 273
  ]
  edge [
    source 17
    target 274
  ]
  edge [
    source 17
    target 275
  ]
  edge [
    source 17
    target 276
  ]
  edge [
    source 17
    target 277
  ]
  edge [
    source 17
    target 278
  ]
  edge [
    source 17
    target 279
  ]
  edge [
    source 17
    target 280
  ]
  edge [
    source 17
    target 281
  ]
  edge [
    source 17
    target 282
  ]
  edge [
    source 17
    target 283
  ]
  edge [
    source 17
    target 284
  ]
  edge [
    source 17
    target 285
  ]
  edge [
    source 17
    target 286
  ]
  edge [
    source 17
    target 287
  ]
  edge [
    source 17
    target 288
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 289
  ]
  edge [
    source 17
    target 290
  ]
  edge [
    source 17
    target 291
  ]
  edge [
    source 17
    target 292
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 293
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 295
  ]
  edge [
    source 17
    target 296
  ]
  edge [
    source 17
    target 297
  ]
  edge [
    source 17
    target 298
  ]
  edge [
    source 17
    target 299
  ]
  edge [
    source 17
    target 300
  ]
  edge [
    source 17
    target 301
  ]
  edge [
    source 17
    target 302
  ]
  edge [
    source 17
    target 303
  ]
  edge [
    source 17
    target 304
  ]
  edge [
    source 17
    target 305
  ]
  edge [
    source 17
    target 306
  ]
  edge [
    source 17
    target 307
  ]
  edge [
    source 17
    target 308
  ]
  edge [
    source 17
    target 309
  ]
  edge [
    source 17
    target 310
  ]
  edge [
    source 17
    target 311
  ]
  edge [
    source 17
    target 312
  ]
  edge [
    source 17
    target 313
  ]
  edge [
    source 17
    target 314
  ]
  edge [
    source 17
    target 315
  ]
  edge [
    source 17
    target 316
  ]
  edge [
    source 17
    target 317
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 319
  ]
  edge [
    source 17
    target 320
  ]
  edge [
    source 17
    target 321
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 322
  ]
  edge [
    source 19
    target 323
  ]
  edge [
    source 19
    target 324
  ]
  edge [
    source 19
    target 325
  ]
  edge [
    source 19
    target 326
  ]
  edge [
    source 19
    target 327
  ]
  edge [
    source 19
    target 328
  ]
  edge [
    source 19
    target 329
  ]
  edge [
    source 19
    target 330
  ]
  edge [
    source 19
    target 331
  ]
  edge [
    source 19
    target 332
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 333
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 257
  ]
  edge [
    source 19
    target 334
  ]
  edge [
    source 19
    target 335
  ]
  edge [
    source 19
    target 299
  ]
  edge [
    source 19
    target 336
  ]
  edge [
    source 19
    target 337
  ]
  edge [
    source 19
    target 338
  ]
  edge [
    source 19
    target 339
  ]
  edge [
    source 19
    target 340
  ]
  edge [
    source 19
    target 341
  ]
  edge [
    source 19
    target 342
  ]
  edge [
    source 19
    target 343
  ]
  edge [
    source 19
    target 344
  ]
  edge [
    source 19
    target 345
  ]
  edge [
    source 19
    target 346
  ]
  edge [
    source 19
    target 347
  ]
  edge [
    source 19
    target 348
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 291
  ]
  edge [
    source 19
    target 349
  ]
  edge [
    source 19
    target 350
  ]
  edge [
    source 19
    target 351
  ]
  edge [
    source 19
    target 352
  ]
  edge [
    source 19
    target 353
  ]
  edge [
    source 19
    target 354
  ]
  edge [
    source 19
    target 355
  ]
  edge [
    source 19
    target 356
  ]
  edge [
    source 19
    target 357
  ]
  edge [
    source 19
    target 358
  ]
  edge [
    source 19
    target 359
  ]
  edge [
    source 19
    target 360
  ]
  edge [
    source 19
    target 361
  ]
  edge [
    source 19
    target 362
  ]
  edge [
    source 19
    target 260
  ]
  edge [
    source 19
    target 363
  ]
  edge [
    source 19
    target 364
  ]
  edge [
    source 19
    target 365
  ]
  edge [
    source 19
    target 366
  ]
  edge [
    source 19
    target 262
  ]
  edge [
    source 19
    target 367
  ]
  edge [
    source 19
    target 368
  ]
  edge [
    source 19
    target 369
  ]
  edge [
    source 19
    target 370
  ]
  edge [
    source 19
    target 371
  ]
  edge [
    source 19
    target 372
  ]
  edge [
    source 19
    target 373
  ]
  edge [
    source 19
    target 374
  ]
  edge [
    source 19
    target 375
  ]
  edge [
    source 19
    target 376
  ]
  edge [
    source 19
    target 377
  ]
  edge [
    source 19
    target 378
  ]
  edge [
    source 19
    target 379
  ]
  edge [
    source 19
    target 380
  ]
  edge [
    source 19
    target 381
  ]
  edge [
    source 19
    target 382
  ]
  edge [
    source 19
    target 383
  ]
  edge [
    source 19
    target 384
  ]
  edge [
    source 19
    target 385
  ]
  edge [
    source 19
    target 386
  ]
  edge [
    source 19
    target 387
  ]
  edge [
    source 19
    target 388
  ]
  edge [
    source 19
    target 389
  ]
  edge [
    source 19
    target 390
  ]
  edge [
    source 19
    target 391
  ]
  edge [
    source 19
    target 392
  ]
  edge [
    source 19
    target 393
  ]
  edge [
    source 19
    target 394
  ]
  edge [
    source 19
    target 395
  ]
  edge [
    source 19
    target 396
  ]
  edge [
    source 19
    target 397
  ]
  edge [
    source 19
    target 398
  ]
  edge [
    source 19
    target 399
  ]
  edge [
    source 19
    target 400
  ]
  edge [
    source 19
    target 401
  ]
  edge [
    source 19
    target 402
  ]
  edge [
    source 19
    target 403
  ]
  edge [
    source 19
    target 404
  ]
  edge [
    source 19
    target 405
  ]
  edge [
    source 19
    target 406
  ]
  edge [
    source 19
    target 407
  ]
  edge [
    source 19
    target 408
  ]
  edge [
    source 19
    target 409
  ]
  edge [
    source 19
    target 410
  ]
  edge [
    source 19
    target 411
  ]
  edge [
    source 19
    target 412
  ]
  edge [
    source 19
    target 413
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 415
  ]
  edge [
    source 20
    target 416
  ]
  edge [
    source 20
    target 417
  ]
  edge [
    source 20
    target 418
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 419
  ]
  edge [
    source 20
    target 420
  ]
  edge [
    source 20
    target 421
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 422
  ]
  edge [
    source 21
    target 423
  ]
  edge [
    source 21
    target 424
  ]
  edge [
    source 21
    target 425
  ]
  edge [
    source 21
    target 426
  ]
  edge [
    source 21
    target 427
  ]
  edge [
    source 21
    target 428
  ]
  edge [
    source 21
    target 429
  ]
  edge [
    source 21
    target 430
  ]
  edge [
    source 21
    target 431
  ]
  edge [
    source 21
    target 432
  ]
  edge [
    source 21
    target 433
  ]
  edge [
    source 21
    target 434
  ]
  edge [
    source 21
    target 435
  ]
  edge [
    source 21
    target 110
  ]
  edge [
    source 21
    target 395
  ]
  edge [
    source 21
    target 404
  ]
  edge [
    source 21
    target 436
  ]
  edge [
    source 21
    target 437
  ]
  edge [
    source 21
    target 438
  ]
  edge [
    source 21
    target 439
  ]
  edge [
    source 21
    target 440
  ]
  edge [
    source 21
    target 441
  ]
  edge [
    source 21
    target 442
  ]
  edge [
    source 21
    target 443
  ]
  edge [
    source 21
    target 444
  ]
  edge [
    source 21
    target 445
  ]
  edge [
    source 21
    target 446
  ]
  edge [
    source 21
    target 447
  ]
  edge [
    source 21
    target 448
  ]
  edge [
    source 21
    target 449
  ]
  edge [
    source 21
    target 450
  ]
  edge [
    source 21
    target 451
  ]
  edge [
    source 21
    target 452
  ]
  edge [
    source 21
    target 453
  ]
  edge [
    source 21
    target 454
  ]
  edge [
    source 21
    target 455
  ]
  edge [
    source 21
    target 456
  ]
  edge [
    source 21
    target 457
  ]
  edge [
    source 21
    target 458
  ]
  edge [
    source 21
    target 459
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 21
    target 461
  ]
  edge [
    source 21
    target 462
  ]
  edge [
    source 21
    target 463
  ]
  edge [
    source 21
    target 464
  ]
  edge [
    source 21
    target 465
  ]
  edge [
    source 21
    target 466
  ]
  edge [
    source 21
    target 467
  ]
  edge [
    source 21
    target 468
  ]
  edge [
    source 21
    target 53
  ]
  edge [
    source 21
    target 469
  ]
  edge [
    source 21
    target 470
  ]
  edge [
    source 21
    target 471
  ]
  edge [
    source 21
    target 472
  ]
  edge [
    source 21
    target 473
  ]
  edge [
    source 21
    target 474
  ]
  edge [
    source 21
    target 475
  ]
  edge [
    source 21
    target 476
  ]
  edge [
    source 21
    target 477
  ]
  edge [
    source 21
    target 478
  ]
  edge [
    source 21
    target 479
  ]
  edge [
    source 21
    target 480
  ]
  edge [
    source 21
    target 481
  ]
  edge [
    source 21
    target 482
  ]
  edge [
    source 21
    target 483
  ]
  edge [
    source 21
    target 484
  ]
  edge [
    source 21
    target 485
  ]
  edge [
    source 21
    target 486
  ]
  edge [
    source 21
    target 487
  ]
  edge [
    source 21
    target 488
  ]
  edge [
    source 21
    target 489
  ]
  edge [
    source 21
    target 490
  ]
  edge [
    source 21
    target 113
  ]
  edge [
    source 21
    target 491
  ]
  edge [
    source 21
    target 492
  ]
  edge [
    source 21
    target 493
  ]
  edge [
    source 21
    target 494
  ]
  edge [
    source 21
    target 495
  ]
  edge [
    source 21
    target 496
  ]
  edge [
    source 21
    target 497
  ]
  edge [
    source 21
    target 498
  ]
  edge [
    source 21
    target 499
  ]
  edge [
    source 21
    target 500
  ]
  edge [
    source 21
    target 501
  ]
  edge [
    source 21
    target 502
  ]
  edge [
    source 21
    target 503
  ]
  edge [
    source 21
    target 504
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 505
  ]
  edge [
    source 22
    target 506
  ]
  edge [
    source 22
    target 507
  ]
  edge [
    source 22
    target 508
  ]
  edge [
    source 22
    target 509
  ]
  edge [
    source 22
    target 510
  ]
  edge [
    source 22
    target 511
  ]
  edge [
    source 22
    target 475
  ]
  edge [
    source 22
    target 512
  ]
  edge [
    source 22
    target 513
  ]
  edge [
    source 22
    target 514
  ]
  edge [
    source 22
    target 515
  ]
  edge [
    source 22
    target 516
  ]
  edge [
    source 22
    target 517
  ]
  edge [
    source 22
    target 518
  ]
  edge [
    source 22
    target 519
  ]
  edge [
    source 22
    target 520
  ]
  edge [
    source 22
    target 521
  ]
  edge [
    source 22
    target 522
  ]
  edge [
    source 22
    target 523
  ]
  edge [
    source 22
    target 524
  ]
  edge [
    source 22
    target 102
  ]
  edge [
    source 22
    target 525
  ]
  edge [
    source 22
    target 526
  ]
  edge [
    source 22
    target 527
  ]
  edge [
    source 22
    target 528
  ]
  edge [
    source 22
    target 529
  ]
  edge [
    source 22
    target 530
  ]
  edge [
    source 22
    target 531
  ]
  edge [
    source 22
    target 532
  ]
  edge [
    source 22
    target 533
  ]
  edge [
    source 22
    target 534
  ]
  edge [
    source 22
    target 535
  ]
  edge [
    source 22
    target 536
  ]
  edge [
    source 22
    target 537
  ]
  edge [
    source 22
    target 538
  ]
  edge [
    source 22
    target 539
  ]
  edge [
    source 22
    target 540
  ]
  edge [
    source 22
    target 541
  ]
  edge [
    source 22
    target 542
  ]
  edge [
    source 22
    target 543
  ]
  edge [
    source 22
    target 544
  ]
  edge [
    source 22
    target 545
  ]
  edge [
    source 22
    target 546
  ]
  edge [
    source 22
    target 547
  ]
  edge [
    source 22
    target 548
  ]
  edge [
    source 22
    target 549
  ]
  edge [
    source 22
    target 550
  ]
  edge [
    source 22
    target 551
  ]
  edge [
    source 22
    target 552
  ]
  edge [
    source 22
    target 553
  ]
  edge [
    source 22
    target 554
  ]
  edge [
    source 22
    target 555
  ]
  edge [
    source 22
    target 556
  ]
  edge [
    source 22
    target 557
  ]
  edge [
    source 22
    target 558
  ]
  edge [
    source 22
    target 559
  ]
  edge [
    source 22
    target 560
  ]
  edge [
    source 22
    target 297
  ]
  edge [
    source 22
    target 561
  ]
  edge [
    source 22
    target 440
  ]
  edge [
    source 22
    target 562
  ]
  edge [
    source 22
    target 563
  ]
  edge [
    source 22
    target 564
  ]
  edge [
    source 22
    target 565
  ]
  edge [
    source 22
    target 566
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 568
  ]
  edge [
    source 22
    target 569
  ]
  edge [
    source 22
    target 570
  ]
  edge [
    source 22
    target 62
  ]
  edge [
    source 22
    target 571
  ]
  edge [
    source 22
    target 572
  ]
  edge [
    source 22
    target 573
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 22
    target 574
  ]
  edge [
    source 22
    target 575
  ]
  edge [
    source 22
    target 576
  ]
  edge [
    source 22
    target 577
  ]
  edge [
    source 22
    target 578
  ]
  edge [
    source 22
    target 579
  ]
  edge [
    source 22
    target 580
  ]
  edge [
    source 22
    target 581
  ]
  edge [
    source 22
    target 582
  ]
  edge [
    source 22
    target 583
  ]
  edge [
    source 22
    target 584
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 22
    target 585
  ]
  edge [
    source 22
    target 586
  ]
  edge [
    source 22
    target 587
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 22
    target 588
  ]
  edge [
    source 22
    target 589
  ]
  edge [
    source 22
    target 590
  ]
  edge [
    source 22
    target 591
  ]
  edge [
    source 22
    target 592
  ]
  edge [
    source 22
    target 593
  ]
  edge [
    source 22
    target 594
  ]
  edge [
    source 22
    target 595
  ]
  edge [
    source 22
    target 596
  ]
  edge [
    source 22
    target 597
  ]
  edge [
    source 22
    target 598
  ]
  edge [
    source 22
    target 599
  ]
  edge [
    source 22
    target 600
  ]
  edge [
    source 22
    target 601
  ]
  edge [
    source 22
    target 602
  ]
  edge [
    source 22
    target 603
  ]
  edge [
    source 22
    target 604
  ]
  edge [
    source 22
    target 605
  ]
  edge [
    source 22
    target 606
  ]
  edge [
    source 22
    target 607
  ]
  edge [
    source 22
    target 608
  ]
  edge [
    source 22
    target 609
  ]
  edge [
    source 22
    target 610
  ]
  edge [
    source 22
    target 611
  ]
  edge [
    source 22
    target 612
  ]
  edge [
    source 22
    target 613
  ]
  edge [
    source 22
    target 614
  ]
  edge [
    source 22
    target 615
  ]
  edge [
    source 22
    target 616
  ]
  edge [
    source 22
    target 617
  ]
  edge [
    source 22
    target 618
  ]
  edge [
    source 22
    target 619
  ]
  edge [
    source 22
    target 620
  ]
  edge [
    source 22
    target 621
  ]
  edge [
    source 22
    target 622
  ]
  edge [
    source 22
    target 623
  ]
  edge [
    source 22
    target 624
  ]
  edge [
    source 22
    target 625
  ]
  edge [
    source 22
    target 626
  ]
  edge [
    source 22
    target 627
  ]
  edge [
    source 22
    target 628
  ]
  edge [
    source 22
    target 629
  ]
  edge [
    source 22
    target 630
  ]
  edge [
    source 22
    target 631
  ]
  edge [
    source 22
    target 632
  ]
  edge [
    source 22
    target 633
  ]
  edge [
    source 22
    target 634
  ]
  edge [
    source 22
    target 635
  ]
  edge [
    source 22
    target 636
  ]
  edge [
    source 22
    target 637
  ]
  edge [
    source 22
    target 638
  ]
  edge [
    source 22
    target 639
  ]
  edge [
    source 22
    target 640
  ]
  edge [
    source 22
    target 641
  ]
  edge [
    source 22
    target 642
  ]
  edge [
    source 22
    target 643
  ]
  edge [
    source 22
    target 644
  ]
  edge [
    source 22
    target 645
  ]
  edge [
    source 22
    target 380
  ]
  edge [
    source 22
    target 646
  ]
  edge [
    source 22
    target 647
  ]
  edge [
    source 22
    target 648
  ]
  edge [
    source 22
    target 649
  ]
  edge [
    source 22
    target 650
  ]
  edge [
    source 22
    target 651
  ]
  edge [
    source 22
    target 652
  ]
  edge [
    source 22
    target 653
  ]
  edge [
    source 22
    target 654
  ]
  edge [
    source 22
    target 655
  ]
  edge [
    source 22
    target 656
  ]
  edge [
    source 22
    target 657
  ]
  edge [
    source 22
    target 658
  ]
  edge [
    source 22
    target 659
  ]
  edge [
    source 22
    target 660
  ]
  edge [
    source 22
    target 661
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 663
  ]
  edge [
    source 22
    target 664
  ]
  edge [
    source 22
    target 665
  ]
  edge [
    source 22
    target 666
  ]
  edge [
    source 22
    target 667
  ]
  edge [
    source 22
    target 668
  ]
  edge [
    source 22
    target 669
  ]
  edge [
    source 22
    target 670
  ]
  edge [
    source 22
    target 671
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 673
  ]
  edge [
    source 22
    target 674
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 676
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 678
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 680
  ]
  edge [
    source 22
    target 681
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 683
  ]
  edge [
    source 22
    target 684
  ]
  edge [
    source 22
    target 685
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 687
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 692
  ]
  edge [
    source 22
    target 92
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 695
  ]
  edge [
    source 24
    target 696
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 697
  ]
  edge [
    source 26
    target 123
  ]
  edge [
    source 26
    target 698
  ]
  edge [
    source 26
    target 73
  ]
  edge [
    source 26
    target 699
  ]
  edge [
    source 26
    target 700
  ]
  edge [
    source 26
    target 122
  ]
  edge [
    source 26
    target 124
  ]
  edge [
    source 26
    target 701
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 702
  ]
  edge [
    source 27
    target 703
  ]
  edge [
    source 27
    target 704
  ]
  edge [
    source 27
    target 705
  ]
  edge [
    source 27
    target 706
  ]
  edge [
    source 27
    target 707
  ]
  edge [
    source 27
    target 708
  ]
  edge [
    source 27
    target 709
  ]
  edge [
    source 27
    target 710
  ]
  edge [
    source 27
    target 711
  ]
  edge [
    source 27
    target 712
  ]
  edge [
    source 27
    target 713
  ]
  edge [
    source 27
    target 714
  ]
  edge [
    source 27
    target 715
  ]
  edge [
    source 27
    target 716
  ]
  edge [
    source 27
    target 717
  ]
  edge [
    source 27
    target 394
  ]
  edge [
    source 27
    target 718
  ]
  edge [
    source 27
    target 719
  ]
  edge [
    source 27
    target 551
  ]
  edge [
    source 27
    target 584
  ]
  edge [
    source 27
    target 720
  ]
  edge [
    source 27
    target 721
  ]
  edge [
    source 27
    target 722
  ]
  edge [
    source 27
    target 723
  ]
  edge [
    source 27
    target 724
  ]
  edge [
    source 27
    target 480
  ]
  edge [
    source 27
    target 725
  ]
  edge [
    source 27
    target 102
  ]
  edge [
    source 27
    target 726
  ]
  edge [
    source 27
    target 727
  ]
  edge [
    source 27
    target 728
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 729
  ]
  edge [
    source 28
    target 730
  ]
  edge [
    source 28
    target 731
  ]
  edge [
    source 28
    target 732
  ]
  edge [
    source 28
    target 733
  ]
  edge [
    source 28
    target 734
  ]
  edge [
    source 28
    target 735
  ]
  edge [
    source 28
    target 736
  ]
  edge [
    source 28
    target 737
  ]
  edge [
    source 28
    target 738
  ]
  edge [
    source 28
    target 739
  ]
  edge [
    source 28
    target 740
  ]
  edge [
    source 28
    target 741
  ]
  edge [
    source 28
    target 742
  ]
  edge [
    source 28
    target 743
  ]
  edge [
    source 28
    target 744
  ]
  edge [
    source 28
    target 745
  ]
  edge [
    source 28
    target 746
  ]
  edge [
    source 28
    target 747
  ]
  edge [
    source 28
    target 748
  ]
  edge [
    source 28
    target 749
  ]
  edge [
    source 28
    target 750
  ]
  edge [
    source 28
    target 751
  ]
  edge [
    source 28
    target 752
  ]
  edge [
    source 28
    target 753
  ]
  edge [
    source 28
    target 41
  ]
  edge [
    source 28
    target 754
  ]
  edge [
    source 28
    target 755
  ]
  edge [
    source 28
    target 756
  ]
  edge [
    source 28
    target 757
  ]
  edge [
    source 28
    target 758
  ]
  edge [
    source 28
    target 759
  ]
  edge [
    source 28
    target 760
  ]
  edge [
    source 28
    target 761
  ]
  edge [
    source 28
    target 762
  ]
  edge [
    source 28
    target 532
  ]
  edge [
    source 28
    target 763
  ]
  edge [
    source 28
    target 764
  ]
  edge [
    source 28
    target 765
  ]
  edge [
    source 28
    target 766
  ]
  edge [
    source 28
    target 767
  ]
  edge [
    source 28
    target 768
  ]
  edge [
    source 28
    target 769
  ]
  edge [
    source 28
    target 770
  ]
  edge [
    source 28
    target 771
  ]
  edge [
    source 28
    target 772
  ]
  edge [
    source 28
    target 773
  ]
  edge [
    source 29
    target 774
  ]
  edge [
    source 29
    target 775
  ]
  edge [
    source 29
    target 776
  ]
  edge [
    source 29
    target 777
  ]
  edge [
    source 29
    target 778
  ]
  edge [
    source 29
    target 779
  ]
  edge [
    source 29
    target 780
  ]
  edge [
    source 29
    target 781
  ]
  edge [
    source 29
    target 782
  ]
  edge [
    source 29
    target 783
  ]
  edge [
    source 29
    target 784
  ]
  edge [
    source 29
    target 785
  ]
  edge [
    source 29
    target 786
  ]
  edge [
    source 29
    target 787
  ]
  edge [
    source 29
    target 788
  ]
  edge [
    source 29
    target 789
  ]
  edge [
    source 29
    target 790
  ]
  edge [
    source 29
    target 791
  ]
  edge [
    source 29
    target 792
  ]
  edge [
    source 29
    target 793
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 794
  ]
  edge [
    source 30
    target 795
  ]
  edge [
    source 30
    target 796
  ]
  edge [
    source 30
    target 797
  ]
  edge [
    source 30
    target 798
  ]
  edge [
    source 30
    target 799
  ]
  edge [
    source 30
    target 800
  ]
  edge [
    source 30
    target 801
  ]
  edge [
    source 30
    target 802
  ]
  edge [
    source 30
    target 803
  ]
  edge [
    source 30
    target 804
  ]
  edge [
    source 30
    target 805
  ]
  edge [
    source 30
    target 806
  ]
  edge [
    source 30
    target 807
  ]
  edge [
    source 30
    target 808
  ]
  edge [
    source 30
    target 809
  ]
  edge [
    source 30
    target 810
  ]
  edge [
    source 30
    target 811
  ]
  edge [
    source 30
    target 812
  ]
  edge [
    source 30
    target 813
  ]
  edge [
    source 30
    target 814
  ]
  edge [
    source 30
    target 815
  ]
  edge [
    source 30
    target 816
  ]
  edge [
    source 30
    target 817
  ]
  edge [
    source 30
    target 818
  ]
  edge [
    source 30
    target 819
  ]
  edge [
    source 30
    target 820
  ]
  edge [
    source 30
    target 821
  ]
  edge [
    source 30
    target 822
  ]
  edge [
    source 30
    target 823
  ]
  edge [
    source 30
    target 824
  ]
  edge [
    source 30
    target 825
  ]
  edge [
    source 30
    target 826
  ]
  edge [
    source 30
    target 827
  ]
  edge [
    source 30
    target 828
  ]
  edge [
    source 30
    target 829
  ]
  edge [
    source 30
    target 830
  ]
  edge [
    source 30
    target 831
  ]
  edge [
    source 30
    target 832
  ]
  edge [
    source 30
    target 833
  ]
  edge [
    source 30
    target 834
  ]
  edge [
    source 30
    target 835
  ]
  edge [
    source 30
    target 836
  ]
  edge [
    source 30
    target 837
  ]
  edge [
    source 30
    target 838
  ]
  edge [
    source 30
    target 839
  ]
  edge [
    source 30
    target 840
  ]
  edge [
    source 30
    target 841
  ]
  edge [
    source 30
    target 842
  ]
  edge [
    source 30
    target 788
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 843
  ]
  edge [
    source 31
    target 844
  ]
  edge [
    source 31
    target 845
  ]
  edge [
    source 31
    target 846
  ]
  edge [
    source 32
    target 847
  ]
  edge [
    source 32
    target 737
  ]
  edge [
    source 32
    target 848
  ]
  edge [
    source 32
    target 849
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 850
  ]
  edge [
    source 32
    target 851
  ]
  edge [
    source 32
    target 852
  ]
  edge [
    source 32
    target 853
  ]
  edge [
    source 32
    target 854
  ]
  edge [
    source 32
    target 855
  ]
  edge [
    source 32
    target 174
  ]
  edge [
    source 32
    target 856
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 857
  ]
  edge [
    source 33
    target 858
  ]
  edge [
    source 33
    target 569
  ]
  edge [
    source 33
    target 859
  ]
  edge [
    source 33
    target 860
  ]
  edge [
    source 33
    target 861
  ]
  edge [
    source 33
    target 862
  ]
  edge [
    source 33
    target 532
  ]
  edge [
    source 33
    target 863
  ]
  edge [
    source 33
    target 864
  ]
  edge [
    source 33
    target 865
  ]
  edge [
    source 33
    target 866
  ]
  edge [
    source 33
    target 867
  ]
  edge [
    source 33
    target 868
  ]
  edge [
    source 33
    target 869
  ]
  edge [
    source 33
    target 870
  ]
  edge [
    source 33
    target 871
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 872
  ]
  edge [
    source 34
    target 873
  ]
  edge [
    source 34
    target 874
  ]
  edge [
    source 34
    target 875
  ]
  edge [
    source 34
    target 876
  ]
  edge [
    source 34
    target 877
  ]
  edge [
    source 34
    target 878
  ]
  edge [
    source 34
    target 879
  ]
  edge [
    source 34
    target 880
  ]
  edge [
    source 34
    target 881
  ]
  edge [
    source 34
    target 882
  ]
  edge [
    source 34
    target 883
  ]
  edge [
    source 34
    target 884
  ]
  edge [
    source 34
    target 885
  ]
  edge [
    source 34
    target 886
  ]
  edge [
    source 34
    target 887
  ]
  edge [
    source 34
    target 888
  ]
  edge [
    source 34
    target 889
  ]
  edge [
    source 34
    target 890
  ]
  edge [
    source 34
    target 891
  ]
  edge [
    source 34
    target 892
  ]
  edge [
    source 34
    target 893
  ]
  edge [
    source 34
    target 894
  ]
  edge [
    source 34
    target 895
  ]
  edge [
    source 34
    target 896
  ]
  edge [
    source 34
    target 897
  ]
  edge [
    source 34
    target 898
  ]
  edge [
    source 34
    target 899
  ]
  edge [
    source 34
    target 900
  ]
  edge [
    source 34
    target 901
  ]
  edge [
    source 34
    target 902
  ]
  edge [
    source 34
    target 903
  ]
  edge [
    source 34
    target 904
  ]
  edge [
    source 34
    target 905
  ]
  edge [
    source 34
    target 906
  ]
  edge [
    source 34
    target 907
  ]
  edge [
    source 34
    target 908
  ]
  edge [
    source 34
    target 547
  ]
  edge [
    source 34
    target 909
  ]
  edge [
    source 34
    target 910
  ]
  edge [
    source 34
    target 911
  ]
  edge [
    source 34
    target 912
  ]
  edge [
    source 34
    target 913
  ]
  edge [
    source 34
    target 914
  ]
  edge [
    source 34
    target 915
  ]
  edge [
    source 34
    target 725
  ]
  edge [
    source 34
    target 916
  ]
  edge [
    source 34
    target 917
  ]
  edge [
    source 34
    target 918
  ]
  edge [
    source 34
    target 919
  ]
  edge [
    source 34
    target 920
  ]
  edge [
    source 34
    target 921
  ]
  edge [
    source 34
    target 922
  ]
  edge [
    source 34
    target 923
  ]
  edge [
    source 34
    target 924
  ]
  edge [
    source 34
    target 925
  ]
  edge [
    source 34
    target 742
  ]
  edge [
    source 34
    target 743
  ]
  edge [
    source 34
    target 744
  ]
  edge [
    source 34
    target 745
  ]
  edge [
    source 34
    target 746
  ]
  edge [
    source 34
    target 747
  ]
  edge [
    source 34
    target 748
  ]
  edge [
    source 34
    target 749
  ]
  edge [
    source 34
    target 750
  ]
  edge [
    source 34
    target 751
  ]
  edge [
    source 34
    target 752
  ]
  edge [
    source 34
    target 753
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 754
  ]
  edge [
    source 34
    target 755
  ]
  edge [
    source 34
    target 756
  ]
  edge [
    source 34
    target 757
  ]
  edge [
    source 34
    target 758
  ]
  edge [
    source 34
    target 759
  ]
  edge [
    source 34
    target 760
  ]
  edge [
    source 34
    target 761
  ]
  edge [
    source 34
    target 762
  ]
  edge [
    source 34
    target 532
  ]
  edge [
    source 34
    target 763
  ]
  edge [
    source 34
    target 764
  ]
  edge [
    source 34
    target 926
  ]
  edge [
    source 34
    target 927
  ]
  edge [
    source 34
    target 928
  ]
]
