graph [
  node [
    id 0
    label "letni"
    origin "text"
  ]
  node [
    id 1
    label "yara"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "szkolny"
    origin "text"
  ]
  node [
    id 5
    label "klasa"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "niemiecki"
    origin "text"
  ]
  node [
    id 9
    label "latowy"
  ]
  node [
    id 10
    label "typowy"
  ]
  node [
    id 11
    label "weso&#322;y"
  ]
  node [
    id 12
    label "s&#322;oneczny"
  ]
  node [
    id 13
    label "sezonowy"
  ]
  node [
    id 14
    label "ciep&#322;y"
  ]
  node [
    id 15
    label "letnio"
  ]
  node [
    id 16
    label "oboj&#281;tny"
  ]
  node [
    id 17
    label "nijaki"
  ]
  node [
    id 18
    label "nijak"
  ]
  node [
    id 19
    label "niezabawny"
  ]
  node [
    id 20
    label "&#380;aden"
  ]
  node [
    id 21
    label "zwyczajny"
  ]
  node [
    id 22
    label "poszarzenie"
  ]
  node [
    id 23
    label "neutralny"
  ]
  node [
    id 24
    label "szarzenie"
  ]
  node [
    id 25
    label "bezbarwnie"
  ]
  node [
    id 26
    label "nieciekawy"
  ]
  node [
    id 27
    label "czasowy"
  ]
  node [
    id 28
    label "sezonowo"
  ]
  node [
    id 29
    label "zoboj&#281;tnienie"
  ]
  node [
    id 30
    label "nieszkodliwy"
  ]
  node [
    id 31
    label "&#347;ni&#281;ty"
  ]
  node [
    id 32
    label "oboj&#281;tnie"
  ]
  node [
    id 33
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 34
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 35
    label "niewa&#380;ny"
  ]
  node [
    id 36
    label "neutralizowanie"
  ]
  node [
    id 37
    label "bierny"
  ]
  node [
    id 38
    label "zneutralizowanie"
  ]
  node [
    id 39
    label "pijany"
  ]
  node [
    id 40
    label "weso&#322;o"
  ]
  node [
    id 41
    label "pozytywny"
  ]
  node [
    id 42
    label "beztroski"
  ]
  node [
    id 43
    label "dobry"
  ]
  node [
    id 44
    label "mi&#322;y"
  ]
  node [
    id 45
    label "ocieplanie_si&#281;"
  ]
  node [
    id 46
    label "ocieplanie"
  ]
  node [
    id 47
    label "grzanie"
  ]
  node [
    id 48
    label "ocieplenie_si&#281;"
  ]
  node [
    id 49
    label "zagrzanie"
  ]
  node [
    id 50
    label "ocieplenie"
  ]
  node [
    id 51
    label "korzystny"
  ]
  node [
    id 52
    label "przyjemny"
  ]
  node [
    id 53
    label "ciep&#322;o"
  ]
  node [
    id 54
    label "s&#322;onecznie"
  ]
  node [
    id 55
    label "bezdeszczowy"
  ]
  node [
    id 56
    label "bezchmurny"
  ]
  node [
    id 57
    label "pogodny"
  ]
  node [
    id 58
    label "fotowoltaiczny"
  ]
  node [
    id 59
    label "jasny"
  ]
  node [
    id 60
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 61
    label "typowo"
  ]
  node [
    id 62
    label "cz&#281;sty"
  ]
  node [
    id 63
    label "zwyk&#322;y"
  ]
  node [
    id 64
    label "post&#261;pi&#263;"
  ]
  node [
    id 65
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 66
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 67
    label "odj&#261;&#263;"
  ]
  node [
    id 68
    label "zrobi&#263;"
  ]
  node [
    id 69
    label "cause"
  ]
  node [
    id 70
    label "introduce"
  ]
  node [
    id 71
    label "begin"
  ]
  node [
    id 72
    label "do"
  ]
  node [
    id 73
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 74
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 75
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 76
    label "zorganizowa&#263;"
  ]
  node [
    id 77
    label "appoint"
  ]
  node [
    id 78
    label "wystylizowa&#263;"
  ]
  node [
    id 79
    label "przerobi&#263;"
  ]
  node [
    id 80
    label "nabra&#263;"
  ]
  node [
    id 81
    label "make"
  ]
  node [
    id 82
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 83
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 84
    label "wydali&#263;"
  ]
  node [
    id 85
    label "withdraw"
  ]
  node [
    id 86
    label "zabra&#263;"
  ]
  node [
    id 87
    label "oddzieli&#263;"
  ]
  node [
    id 88
    label "policzy&#263;"
  ]
  node [
    id 89
    label "reduce"
  ]
  node [
    id 90
    label "oddali&#263;"
  ]
  node [
    id 91
    label "separate"
  ]
  node [
    id 92
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 93
    label "advance"
  ]
  node [
    id 94
    label "act"
  ]
  node [
    id 95
    label "see"
  ]
  node [
    id 96
    label "ut"
  ]
  node [
    id 97
    label "d&#378;wi&#281;k"
  ]
  node [
    id 98
    label "C"
  ]
  node [
    id 99
    label "his"
  ]
  node [
    id 100
    label "p&#243;&#322;rocze"
  ]
  node [
    id 101
    label "martwy_sezon"
  ]
  node [
    id 102
    label "kalendarz"
  ]
  node [
    id 103
    label "cykl_astronomiczny"
  ]
  node [
    id 104
    label "lata"
  ]
  node [
    id 105
    label "pora_roku"
  ]
  node [
    id 106
    label "stulecie"
  ]
  node [
    id 107
    label "kurs"
  ]
  node [
    id 108
    label "czas"
  ]
  node [
    id 109
    label "jubileusz"
  ]
  node [
    id 110
    label "grupa"
  ]
  node [
    id 111
    label "kwarta&#322;"
  ]
  node [
    id 112
    label "miesi&#261;c"
  ]
  node [
    id 113
    label "summer"
  ]
  node [
    id 114
    label "odm&#322;adzanie"
  ]
  node [
    id 115
    label "liga"
  ]
  node [
    id 116
    label "jednostka_systematyczna"
  ]
  node [
    id 117
    label "asymilowanie"
  ]
  node [
    id 118
    label "gromada"
  ]
  node [
    id 119
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 120
    label "asymilowa&#263;"
  ]
  node [
    id 121
    label "egzemplarz"
  ]
  node [
    id 122
    label "Entuzjastki"
  ]
  node [
    id 123
    label "zbi&#243;r"
  ]
  node [
    id 124
    label "kompozycja"
  ]
  node [
    id 125
    label "Terranie"
  ]
  node [
    id 126
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 127
    label "category"
  ]
  node [
    id 128
    label "pakiet_klimatyczny"
  ]
  node [
    id 129
    label "oddzia&#322;"
  ]
  node [
    id 130
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 131
    label "cz&#261;steczka"
  ]
  node [
    id 132
    label "stage_set"
  ]
  node [
    id 133
    label "type"
  ]
  node [
    id 134
    label "specgrupa"
  ]
  node [
    id 135
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 136
    label "&#346;wietliki"
  ]
  node [
    id 137
    label "odm&#322;odzenie"
  ]
  node [
    id 138
    label "Eurogrupa"
  ]
  node [
    id 139
    label "odm&#322;adza&#263;"
  ]
  node [
    id 140
    label "formacja_geologiczna"
  ]
  node [
    id 141
    label "harcerze_starsi"
  ]
  node [
    id 142
    label "poprzedzanie"
  ]
  node [
    id 143
    label "czasoprzestrze&#324;"
  ]
  node [
    id 144
    label "laba"
  ]
  node [
    id 145
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 146
    label "chronometria"
  ]
  node [
    id 147
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 148
    label "rachuba_czasu"
  ]
  node [
    id 149
    label "przep&#322;ywanie"
  ]
  node [
    id 150
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 151
    label "czasokres"
  ]
  node [
    id 152
    label "odczyt"
  ]
  node [
    id 153
    label "chwila"
  ]
  node [
    id 154
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 155
    label "dzieje"
  ]
  node [
    id 156
    label "kategoria_gramatyczna"
  ]
  node [
    id 157
    label "poprzedzenie"
  ]
  node [
    id 158
    label "trawienie"
  ]
  node [
    id 159
    label "pochodzi&#263;"
  ]
  node [
    id 160
    label "period"
  ]
  node [
    id 161
    label "okres_czasu"
  ]
  node [
    id 162
    label "poprzedza&#263;"
  ]
  node [
    id 163
    label "schy&#322;ek"
  ]
  node [
    id 164
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 165
    label "odwlekanie_si&#281;"
  ]
  node [
    id 166
    label "zegar"
  ]
  node [
    id 167
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 168
    label "czwarty_wymiar"
  ]
  node [
    id 169
    label "pochodzenie"
  ]
  node [
    id 170
    label "koniugacja"
  ]
  node [
    id 171
    label "Zeitgeist"
  ]
  node [
    id 172
    label "trawi&#263;"
  ]
  node [
    id 173
    label "pogoda"
  ]
  node [
    id 174
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 175
    label "poprzedzi&#263;"
  ]
  node [
    id 176
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 177
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 178
    label "time_period"
  ]
  node [
    id 179
    label "term"
  ]
  node [
    id 180
    label "rok_akademicki"
  ]
  node [
    id 181
    label "rok_szkolny"
  ]
  node [
    id 182
    label "semester"
  ]
  node [
    id 183
    label "anniwersarz"
  ]
  node [
    id 184
    label "rocznica"
  ]
  node [
    id 185
    label "obszar"
  ]
  node [
    id 186
    label "tydzie&#324;"
  ]
  node [
    id 187
    label "miech"
  ]
  node [
    id 188
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 189
    label "kalendy"
  ]
  node [
    id 190
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 191
    label "long_time"
  ]
  node [
    id 192
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 193
    label "almanac"
  ]
  node [
    id 194
    label "rozk&#322;ad"
  ]
  node [
    id 195
    label "wydawnictwo"
  ]
  node [
    id 196
    label "Juliusz_Cezar"
  ]
  node [
    id 197
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 198
    label "zwy&#380;kowanie"
  ]
  node [
    id 199
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 200
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 201
    label "zaj&#281;cia"
  ]
  node [
    id 202
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 203
    label "trasa"
  ]
  node [
    id 204
    label "przeorientowywanie"
  ]
  node [
    id 205
    label "przejazd"
  ]
  node [
    id 206
    label "kierunek"
  ]
  node [
    id 207
    label "przeorientowywa&#263;"
  ]
  node [
    id 208
    label "nauka"
  ]
  node [
    id 209
    label "przeorientowanie"
  ]
  node [
    id 210
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 211
    label "przeorientowa&#263;"
  ]
  node [
    id 212
    label "manner"
  ]
  node [
    id 213
    label "course"
  ]
  node [
    id 214
    label "passage"
  ]
  node [
    id 215
    label "zni&#380;kowanie"
  ]
  node [
    id 216
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 217
    label "seria"
  ]
  node [
    id 218
    label "stawka"
  ]
  node [
    id 219
    label "way"
  ]
  node [
    id 220
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 221
    label "spos&#243;b"
  ]
  node [
    id 222
    label "deprecjacja"
  ]
  node [
    id 223
    label "cedu&#322;a"
  ]
  node [
    id 224
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 225
    label "drive"
  ]
  node [
    id 226
    label "bearing"
  ]
  node [
    id 227
    label "Lira"
  ]
  node [
    id 228
    label "szkolnie"
  ]
  node [
    id 229
    label "podstawowy"
  ]
  node [
    id 230
    label "prosty"
  ]
  node [
    id 231
    label "szkoleniowy"
  ]
  node [
    id 232
    label "skromny"
  ]
  node [
    id 233
    label "po_prostu"
  ]
  node [
    id 234
    label "naturalny"
  ]
  node [
    id 235
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 236
    label "rozprostowanie"
  ]
  node [
    id 237
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 238
    label "prosto"
  ]
  node [
    id 239
    label "prostowanie_si&#281;"
  ]
  node [
    id 240
    label "niepozorny"
  ]
  node [
    id 241
    label "cios"
  ]
  node [
    id 242
    label "prostoduszny"
  ]
  node [
    id 243
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 244
    label "naiwny"
  ]
  node [
    id 245
    label "&#322;atwy"
  ]
  node [
    id 246
    label "prostowanie"
  ]
  node [
    id 247
    label "naukowy"
  ]
  node [
    id 248
    label "niezaawansowany"
  ]
  node [
    id 249
    label "najwa&#380;niejszy"
  ]
  node [
    id 250
    label "pocz&#261;tkowy"
  ]
  node [
    id 251
    label "podstawowo"
  ]
  node [
    id 252
    label "wagon"
  ]
  node [
    id 253
    label "mecz_mistrzowski"
  ]
  node [
    id 254
    label "przedmiot"
  ]
  node [
    id 255
    label "arrangement"
  ]
  node [
    id 256
    label "class"
  ]
  node [
    id 257
    label "&#322;awka"
  ]
  node [
    id 258
    label "wykrzyknik"
  ]
  node [
    id 259
    label "zaleta"
  ]
  node [
    id 260
    label "programowanie_obiektowe"
  ]
  node [
    id 261
    label "tablica"
  ]
  node [
    id 262
    label "warstwa"
  ]
  node [
    id 263
    label "rezerwa"
  ]
  node [
    id 264
    label "Ekwici"
  ]
  node [
    id 265
    label "&#347;rodowisko"
  ]
  node [
    id 266
    label "szko&#322;a"
  ]
  node [
    id 267
    label "organizacja"
  ]
  node [
    id 268
    label "sala"
  ]
  node [
    id 269
    label "pomoc"
  ]
  node [
    id 270
    label "form"
  ]
  node [
    id 271
    label "przepisa&#263;"
  ]
  node [
    id 272
    label "jako&#347;&#263;"
  ]
  node [
    id 273
    label "znak_jako&#347;ci"
  ]
  node [
    id 274
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 275
    label "poziom"
  ]
  node [
    id 276
    label "promocja"
  ]
  node [
    id 277
    label "przepisanie"
  ]
  node [
    id 278
    label "obiekt"
  ]
  node [
    id 279
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 280
    label "dziennik_lekcyjny"
  ]
  node [
    id 281
    label "typ"
  ]
  node [
    id 282
    label "fakcja"
  ]
  node [
    id 283
    label "obrona"
  ]
  node [
    id 284
    label "atak"
  ]
  node [
    id 285
    label "botanika"
  ]
  node [
    id 286
    label "wypowiedzenie"
  ]
  node [
    id 287
    label "leksem"
  ]
  node [
    id 288
    label "exclamation_mark"
  ]
  node [
    id 289
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 290
    label "znak_interpunkcyjny"
  ]
  node [
    id 291
    label "warto&#347;&#263;"
  ]
  node [
    id 292
    label "quality"
  ]
  node [
    id 293
    label "co&#347;"
  ]
  node [
    id 294
    label "state"
  ]
  node [
    id 295
    label "syf"
  ]
  node [
    id 296
    label "p&#322;aszczyzna"
  ]
  node [
    id 297
    label "przek&#322;adaniec"
  ]
  node [
    id 298
    label "covering"
  ]
  node [
    id 299
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 300
    label "podwarstwa"
  ]
  node [
    id 301
    label "zesp&#243;&#322;"
  ]
  node [
    id 302
    label "obiekt_naturalny"
  ]
  node [
    id 303
    label "otoczenie"
  ]
  node [
    id 304
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 305
    label "environment"
  ]
  node [
    id 306
    label "rzecz"
  ]
  node [
    id 307
    label "huczek"
  ]
  node [
    id 308
    label "ekosystem"
  ]
  node [
    id 309
    label "wszechstworzenie"
  ]
  node [
    id 310
    label "woda"
  ]
  node [
    id 311
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 312
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 313
    label "teren"
  ]
  node [
    id 314
    label "mikrokosmos"
  ]
  node [
    id 315
    label "stw&#243;r"
  ]
  node [
    id 316
    label "warunki"
  ]
  node [
    id 317
    label "Ziemia"
  ]
  node [
    id 318
    label "fauna"
  ]
  node [
    id 319
    label "biota"
  ]
  node [
    id 320
    label "zboczenie"
  ]
  node [
    id 321
    label "om&#243;wienie"
  ]
  node [
    id 322
    label "sponiewieranie"
  ]
  node [
    id 323
    label "discipline"
  ]
  node [
    id 324
    label "omawia&#263;"
  ]
  node [
    id 325
    label "kr&#261;&#380;enie"
  ]
  node [
    id 326
    label "tre&#347;&#263;"
  ]
  node [
    id 327
    label "robienie"
  ]
  node [
    id 328
    label "sponiewiera&#263;"
  ]
  node [
    id 329
    label "element"
  ]
  node [
    id 330
    label "entity"
  ]
  node [
    id 331
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 332
    label "tematyka"
  ]
  node [
    id 333
    label "w&#261;tek"
  ]
  node [
    id 334
    label "charakter"
  ]
  node [
    id 335
    label "zbaczanie"
  ]
  node [
    id 336
    label "program_nauczania"
  ]
  node [
    id 337
    label "om&#243;wi&#263;"
  ]
  node [
    id 338
    label "omawianie"
  ]
  node [
    id 339
    label "thing"
  ]
  node [
    id 340
    label "kultura"
  ]
  node [
    id 341
    label "istota"
  ]
  node [
    id 342
    label "zbacza&#263;"
  ]
  node [
    id 343
    label "zboczy&#263;"
  ]
  node [
    id 344
    label "po&#322;o&#380;enie"
  ]
  node [
    id 345
    label "punkt_widzenia"
  ]
  node [
    id 346
    label "wyk&#322;adnik"
  ]
  node [
    id 347
    label "faza"
  ]
  node [
    id 348
    label "szczebel"
  ]
  node [
    id 349
    label "budynek"
  ]
  node [
    id 350
    label "wysoko&#347;&#263;"
  ]
  node [
    id 351
    label "ranga"
  ]
  node [
    id 352
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 353
    label "zrewaluowa&#263;"
  ]
  node [
    id 354
    label "rewaluowanie"
  ]
  node [
    id 355
    label "korzy&#347;&#263;"
  ]
  node [
    id 356
    label "strona"
  ]
  node [
    id 357
    label "rewaluowa&#263;"
  ]
  node [
    id 358
    label "wabik"
  ]
  node [
    id 359
    label "zrewaluowanie"
  ]
  node [
    id 360
    label "cz&#322;owiek"
  ]
  node [
    id 361
    label "kr&#243;lestwo"
  ]
  node [
    id 362
    label "autorament"
  ]
  node [
    id 363
    label "variety"
  ]
  node [
    id 364
    label "antycypacja"
  ]
  node [
    id 365
    label "przypuszczenie"
  ]
  node [
    id 366
    label "cynk"
  ]
  node [
    id 367
    label "obstawia&#263;"
  ]
  node [
    id 368
    label "sztuka"
  ]
  node [
    id 369
    label "rezultat"
  ]
  node [
    id 370
    label "facet"
  ]
  node [
    id 371
    label "design"
  ]
  node [
    id 372
    label "series"
  ]
  node [
    id 373
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 374
    label "uprawianie"
  ]
  node [
    id 375
    label "praca_rolnicza"
  ]
  node [
    id 376
    label "collection"
  ]
  node [
    id 377
    label "dane"
  ]
  node [
    id 378
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 379
    label "poj&#281;cie"
  ]
  node [
    id 380
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 381
    label "sum"
  ]
  node [
    id 382
    label "gathering"
  ]
  node [
    id 383
    label "album"
  ]
  node [
    id 384
    label "audience"
  ]
  node [
    id 385
    label "zgromadzenie"
  ]
  node [
    id 386
    label "publiczno&#347;&#263;"
  ]
  node [
    id 387
    label "pomieszczenie"
  ]
  node [
    id 388
    label "program"
  ]
  node [
    id 389
    label "podmiot"
  ]
  node [
    id 390
    label "jednostka_organizacyjna"
  ]
  node [
    id 391
    label "struktura"
  ]
  node [
    id 392
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 393
    label "TOPR"
  ]
  node [
    id 394
    label "endecki"
  ]
  node [
    id 395
    label "przedstawicielstwo"
  ]
  node [
    id 396
    label "od&#322;am"
  ]
  node [
    id 397
    label "Cepelia"
  ]
  node [
    id 398
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 399
    label "ZBoWiD"
  ]
  node [
    id 400
    label "organization"
  ]
  node [
    id 401
    label "centrala"
  ]
  node [
    id 402
    label "GOPR"
  ]
  node [
    id 403
    label "ZOMO"
  ]
  node [
    id 404
    label "ZMP"
  ]
  node [
    id 405
    label "komitet_koordynacyjny"
  ]
  node [
    id 406
    label "przybud&#243;wka"
  ]
  node [
    id 407
    label "boj&#243;wka"
  ]
  node [
    id 408
    label "poci&#261;g"
  ]
  node [
    id 409
    label "karton"
  ]
  node [
    id 410
    label "czo&#322;ownica"
  ]
  node [
    id 411
    label "harmonijka"
  ]
  node [
    id 412
    label "tramwaj"
  ]
  node [
    id 413
    label "pteridologia"
  ]
  node [
    id 414
    label "fitosocjologia"
  ]
  node [
    id 415
    label "embriologia_ro&#347;lin"
  ]
  node [
    id 416
    label "biologia"
  ]
  node [
    id 417
    label "dendrologia"
  ]
  node [
    id 418
    label "fitopatologia"
  ]
  node [
    id 419
    label "palinologia"
  ]
  node [
    id 420
    label "hylobiologia"
  ]
  node [
    id 421
    label "herboryzowa&#263;"
  ]
  node [
    id 422
    label "herboryzowanie"
  ]
  node [
    id 423
    label "algologia"
  ]
  node [
    id 424
    label "botanika_farmaceutyczna"
  ]
  node [
    id 425
    label "lichenologia"
  ]
  node [
    id 426
    label "organologia"
  ]
  node [
    id 427
    label "fitogeografia"
  ]
  node [
    id 428
    label "etnobotanika"
  ]
  node [
    id 429
    label "geobotanika"
  ]
  node [
    id 430
    label "damka"
  ]
  node [
    id 431
    label "warcaby"
  ]
  node [
    id 432
    label "promotion"
  ]
  node [
    id 433
    label "impreza"
  ]
  node [
    id 434
    label "sprzeda&#380;"
  ]
  node [
    id 435
    label "zamiana"
  ]
  node [
    id 436
    label "udzieli&#263;"
  ]
  node [
    id 437
    label "brief"
  ]
  node [
    id 438
    label "decyzja"
  ]
  node [
    id 439
    label "&#347;wiadectwo"
  ]
  node [
    id 440
    label "akcja"
  ]
  node [
    id 441
    label "bran&#380;a"
  ]
  node [
    id 442
    label "commencement"
  ]
  node [
    id 443
    label "okazja"
  ]
  node [
    id 444
    label "informacja"
  ]
  node [
    id 445
    label "promowa&#263;"
  ]
  node [
    id 446
    label "graduacja"
  ]
  node [
    id 447
    label "nominacja"
  ]
  node [
    id 448
    label "szachy"
  ]
  node [
    id 449
    label "popularyzacja"
  ]
  node [
    id 450
    label "wypromowa&#263;"
  ]
  node [
    id 451
    label "gradation"
  ]
  node [
    id 452
    label "uzyska&#263;"
  ]
  node [
    id 453
    label "przekazanie"
  ]
  node [
    id 454
    label "skopiowanie"
  ]
  node [
    id 455
    label "przeniesienie"
  ]
  node [
    id 456
    label "testament"
  ]
  node [
    id 457
    label "lekarstwo"
  ]
  node [
    id 458
    label "zadanie"
  ]
  node [
    id 459
    label "answer"
  ]
  node [
    id 460
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 461
    label "transcription"
  ]
  node [
    id 462
    label "zalecenie"
  ]
  node [
    id 463
    label "przekaza&#263;"
  ]
  node [
    id 464
    label "supply"
  ]
  node [
    id 465
    label "zaleci&#263;"
  ]
  node [
    id 466
    label "rewrite"
  ]
  node [
    id 467
    label "zrzec_si&#281;"
  ]
  node [
    id 468
    label "skopiowa&#263;"
  ]
  node [
    id 469
    label "przenie&#347;&#263;"
  ]
  node [
    id 470
    label "kategoria"
  ]
  node [
    id 471
    label "blat"
  ]
  node [
    id 472
    label "krzes&#322;o"
  ]
  node [
    id 473
    label "mebel"
  ]
  node [
    id 474
    label "siedzenie"
  ]
  node [
    id 475
    label "rozmiar&#243;wka"
  ]
  node [
    id 476
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 477
    label "tarcza"
  ]
  node [
    id 478
    label "kosz"
  ]
  node [
    id 479
    label "transparent"
  ]
  node [
    id 480
    label "uk&#322;ad"
  ]
  node [
    id 481
    label "rubryka"
  ]
  node [
    id 482
    label "kontener"
  ]
  node [
    id 483
    label "spis"
  ]
  node [
    id 484
    label "plate"
  ]
  node [
    id 485
    label "konstrukcja"
  ]
  node [
    id 486
    label "szachownica_Punnetta"
  ]
  node [
    id 487
    label "chart"
  ]
  node [
    id 488
    label "&#347;rodek"
  ]
  node [
    id 489
    label "darowizna"
  ]
  node [
    id 490
    label "doch&#243;d"
  ]
  node [
    id 491
    label "telefon_zaufania"
  ]
  node [
    id 492
    label "pomocnik"
  ]
  node [
    id 493
    label "zgodzi&#263;"
  ]
  node [
    id 494
    label "property"
  ]
  node [
    id 495
    label "wojsko"
  ]
  node [
    id 496
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 497
    label "zas&#243;b"
  ]
  node [
    id 498
    label "nieufno&#347;&#263;"
  ]
  node [
    id 499
    label "zapasy"
  ]
  node [
    id 500
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 501
    label "resource"
  ]
  node [
    id 502
    label "egzamin"
  ]
  node [
    id 503
    label "walka"
  ]
  node [
    id 504
    label "gracz"
  ]
  node [
    id 505
    label "protection"
  ]
  node [
    id 506
    label "poparcie"
  ]
  node [
    id 507
    label "mecz"
  ]
  node [
    id 508
    label "reakcja"
  ]
  node [
    id 509
    label "defense"
  ]
  node [
    id 510
    label "s&#261;d"
  ]
  node [
    id 511
    label "auspices"
  ]
  node [
    id 512
    label "gra"
  ]
  node [
    id 513
    label "ochrona"
  ]
  node [
    id 514
    label "sp&#243;r"
  ]
  node [
    id 515
    label "post&#281;powanie"
  ]
  node [
    id 516
    label "manewr"
  ]
  node [
    id 517
    label "defensive_structure"
  ]
  node [
    id 518
    label "guard_duty"
  ]
  node [
    id 519
    label "oznaka"
  ]
  node [
    id 520
    label "pogorszenie"
  ]
  node [
    id 521
    label "przemoc"
  ]
  node [
    id 522
    label "krytyka"
  ]
  node [
    id 523
    label "bat"
  ]
  node [
    id 524
    label "kaszel"
  ]
  node [
    id 525
    label "fit"
  ]
  node [
    id 526
    label "rzuci&#263;"
  ]
  node [
    id 527
    label "spasm"
  ]
  node [
    id 528
    label "zagrywka"
  ]
  node [
    id 529
    label "wypowied&#378;"
  ]
  node [
    id 530
    label "&#380;&#261;danie"
  ]
  node [
    id 531
    label "przyp&#322;yw"
  ]
  node [
    id 532
    label "ofensywa"
  ]
  node [
    id 533
    label "stroke"
  ]
  node [
    id 534
    label "pozycja"
  ]
  node [
    id 535
    label "rzucenie"
  ]
  node [
    id 536
    label "knock"
  ]
  node [
    id 537
    label "cywilizacja"
  ]
  node [
    id 538
    label "pole"
  ]
  node [
    id 539
    label "elita"
  ]
  node [
    id 540
    label "status"
  ]
  node [
    id 541
    label "aspo&#322;eczny"
  ]
  node [
    id 542
    label "ludzie_pracy"
  ]
  node [
    id 543
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 544
    label "pozaklasowy"
  ]
  node [
    id 545
    label "uwarstwienie"
  ]
  node [
    id 546
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 547
    label "community"
  ]
  node [
    id 548
    label "kastowo&#347;&#263;"
  ]
  node [
    id 549
    label "do&#347;wiadczenie"
  ]
  node [
    id 550
    label "teren_szko&#322;y"
  ]
  node [
    id 551
    label "wiedza"
  ]
  node [
    id 552
    label "Mickiewicz"
  ]
  node [
    id 553
    label "kwalifikacje"
  ]
  node [
    id 554
    label "podr&#281;cznik"
  ]
  node [
    id 555
    label "absolwent"
  ]
  node [
    id 556
    label "praktyka"
  ]
  node [
    id 557
    label "school"
  ]
  node [
    id 558
    label "system"
  ]
  node [
    id 559
    label "zda&#263;"
  ]
  node [
    id 560
    label "gabinet"
  ]
  node [
    id 561
    label "urszulanki"
  ]
  node [
    id 562
    label "sztuba"
  ]
  node [
    id 563
    label "&#322;awa_szkolna"
  ]
  node [
    id 564
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 565
    label "muzyka"
  ]
  node [
    id 566
    label "lekcja"
  ]
  node [
    id 567
    label "metoda"
  ]
  node [
    id 568
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 569
    label "skolaryzacja"
  ]
  node [
    id 570
    label "zdanie"
  ]
  node [
    id 571
    label "stopek"
  ]
  node [
    id 572
    label "sekretariat"
  ]
  node [
    id 573
    label "ideologia"
  ]
  node [
    id 574
    label "lesson"
  ]
  node [
    id 575
    label "instytucja"
  ]
  node [
    id 576
    label "niepokalanki"
  ]
  node [
    id 577
    label "siedziba"
  ]
  node [
    id 578
    label "szkolenie"
  ]
  node [
    id 579
    label "kara"
  ]
  node [
    id 580
    label "jednostka_administracyjna"
  ]
  node [
    id 581
    label "zoologia"
  ]
  node [
    id 582
    label "skupienie"
  ]
  node [
    id 583
    label "tribe"
  ]
  node [
    id 584
    label "hurma"
  ]
  node [
    id 585
    label "gaworzy&#263;"
  ]
  node [
    id 586
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 587
    label "remark"
  ]
  node [
    id 588
    label "rozmawia&#263;"
  ]
  node [
    id 589
    label "wyra&#380;a&#263;"
  ]
  node [
    id 590
    label "umie&#263;"
  ]
  node [
    id 591
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 592
    label "dziama&#263;"
  ]
  node [
    id 593
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 594
    label "formu&#322;owa&#263;"
  ]
  node [
    id 595
    label "dysfonia"
  ]
  node [
    id 596
    label "express"
  ]
  node [
    id 597
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 598
    label "talk"
  ]
  node [
    id 599
    label "u&#380;ywa&#263;"
  ]
  node [
    id 600
    label "prawi&#263;"
  ]
  node [
    id 601
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 602
    label "powiada&#263;"
  ]
  node [
    id 603
    label "tell"
  ]
  node [
    id 604
    label "chew_the_fat"
  ]
  node [
    id 605
    label "say"
  ]
  node [
    id 606
    label "j&#281;zyk"
  ]
  node [
    id 607
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 608
    label "informowa&#263;"
  ]
  node [
    id 609
    label "wydobywa&#263;"
  ]
  node [
    id 610
    label "okre&#347;la&#263;"
  ]
  node [
    id 611
    label "korzysta&#263;"
  ]
  node [
    id 612
    label "distribute"
  ]
  node [
    id 613
    label "give"
  ]
  node [
    id 614
    label "bash"
  ]
  node [
    id 615
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 616
    label "doznawa&#263;"
  ]
  node [
    id 617
    label "decydowa&#263;"
  ]
  node [
    id 618
    label "signify"
  ]
  node [
    id 619
    label "style"
  ]
  node [
    id 620
    label "powodowa&#263;"
  ]
  node [
    id 621
    label "komunikowa&#263;"
  ]
  node [
    id 622
    label "inform"
  ]
  node [
    id 623
    label "znaczy&#263;"
  ]
  node [
    id 624
    label "give_voice"
  ]
  node [
    id 625
    label "oznacza&#263;"
  ]
  node [
    id 626
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 627
    label "represent"
  ]
  node [
    id 628
    label "convey"
  ]
  node [
    id 629
    label "arouse"
  ]
  node [
    id 630
    label "robi&#263;"
  ]
  node [
    id 631
    label "determine"
  ]
  node [
    id 632
    label "work"
  ]
  node [
    id 633
    label "reakcja_chemiczna"
  ]
  node [
    id 634
    label "uwydatnia&#263;"
  ]
  node [
    id 635
    label "eksploatowa&#263;"
  ]
  node [
    id 636
    label "uzyskiwa&#263;"
  ]
  node [
    id 637
    label "wydostawa&#263;"
  ]
  node [
    id 638
    label "wyjmowa&#263;"
  ]
  node [
    id 639
    label "train"
  ]
  node [
    id 640
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 641
    label "wydawa&#263;"
  ]
  node [
    id 642
    label "dobywa&#263;"
  ]
  node [
    id 643
    label "ocala&#263;"
  ]
  node [
    id 644
    label "excavate"
  ]
  node [
    id 645
    label "g&#243;rnictwo"
  ]
  node [
    id 646
    label "raise"
  ]
  node [
    id 647
    label "wiedzie&#263;"
  ]
  node [
    id 648
    label "can"
  ]
  node [
    id 649
    label "m&#243;c"
  ]
  node [
    id 650
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 651
    label "rozumie&#263;"
  ]
  node [
    id 652
    label "szczeka&#263;"
  ]
  node [
    id 653
    label "funkcjonowa&#263;"
  ]
  node [
    id 654
    label "mawia&#263;"
  ]
  node [
    id 655
    label "opowiada&#263;"
  ]
  node [
    id 656
    label "chatter"
  ]
  node [
    id 657
    label "niemowl&#281;"
  ]
  node [
    id 658
    label "kosmetyk"
  ]
  node [
    id 659
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 660
    label "stanowisko_archeologiczne"
  ]
  node [
    id 661
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 662
    label "artykulator"
  ]
  node [
    id 663
    label "kod"
  ]
  node [
    id 664
    label "kawa&#322;ek"
  ]
  node [
    id 665
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 666
    label "gramatyka"
  ]
  node [
    id 667
    label "stylik"
  ]
  node [
    id 668
    label "przet&#322;umaczenie"
  ]
  node [
    id 669
    label "formalizowanie"
  ]
  node [
    id 670
    label "ssa&#263;"
  ]
  node [
    id 671
    label "ssanie"
  ]
  node [
    id 672
    label "language"
  ]
  node [
    id 673
    label "liza&#263;"
  ]
  node [
    id 674
    label "napisa&#263;"
  ]
  node [
    id 675
    label "konsonantyzm"
  ]
  node [
    id 676
    label "wokalizm"
  ]
  node [
    id 677
    label "pisa&#263;"
  ]
  node [
    id 678
    label "fonetyka"
  ]
  node [
    id 679
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 680
    label "jeniec"
  ]
  node [
    id 681
    label "but"
  ]
  node [
    id 682
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 683
    label "po_koroniarsku"
  ]
  node [
    id 684
    label "kultura_duchowa"
  ]
  node [
    id 685
    label "t&#322;umaczenie"
  ]
  node [
    id 686
    label "m&#243;wienie"
  ]
  node [
    id 687
    label "pype&#263;"
  ]
  node [
    id 688
    label "lizanie"
  ]
  node [
    id 689
    label "pismo"
  ]
  node [
    id 690
    label "formalizowa&#263;"
  ]
  node [
    id 691
    label "organ"
  ]
  node [
    id 692
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 693
    label "rozumienie"
  ]
  node [
    id 694
    label "makroglosja"
  ]
  node [
    id 695
    label "jama_ustna"
  ]
  node [
    id 696
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 697
    label "natural_language"
  ]
  node [
    id 698
    label "s&#322;ownictwo"
  ]
  node [
    id 699
    label "urz&#261;dzenie"
  ]
  node [
    id 700
    label "dysphonia"
  ]
  node [
    id 701
    label "dysleksja"
  ]
  node [
    id 702
    label "po_niemiecku"
  ]
  node [
    id 703
    label "German"
  ]
  node [
    id 704
    label "niemiecko"
  ]
  node [
    id 705
    label "cenar"
  ]
  node [
    id 706
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 707
    label "europejski"
  ]
  node [
    id 708
    label "strudel"
  ]
  node [
    id 709
    label "niemiec"
  ]
  node [
    id 710
    label "pionier"
  ]
  node [
    id 711
    label "zachodnioeuropejski"
  ]
  node [
    id 712
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 713
    label "junkers"
  ]
  node [
    id 714
    label "szwabski"
  ]
  node [
    id 715
    label "szwabsko"
  ]
  node [
    id 716
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 717
    label "po_szwabsku"
  ]
  node [
    id 718
    label "platt"
  ]
  node [
    id 719
    label "europejsko"
  ]
  node [
    id 720
    label "&#380;o&#322;nierz"
  ]
  node [
    id 721
    label "saper"
  ]
  node [
    id 722
    label "prekursor"
  ]
  node [
    id 723
    label "osadnik"
  ]
  node [
    id 724
    label "skaut"
  ]
  node [
    id 725
    label "g&#322;osiciel"
  ]
  node [
    id 726
    label "ciasto"
  ]
  node [
    id 727
    label "taniec_ludowy"
  ]
  node [
    id 728
    label "melodia"
  ]
  node [
    id 729
    label "taniec"
  ]
  node [
    id 730
    label "podgrzewacz"
  ]
  node [
    id 731
    label "samolot_wojskowy"
  ]
  node [
    id 732
    label "moreska"
  ]
  node [
    id 733
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 734
    label "zachodni"
  ]
  node [
    id 735
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 736
    label "langosz"
  ]
  node [
    id 737
    label "po_europejsku"
  ]
  node [
    id 738
    label "European"
  ]
  node [
    id 739
    label "charakterystyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
]
