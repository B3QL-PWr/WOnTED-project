graph [
  node [
    id 0
    label "konwencja"
    origin "text"
  ]
  node [
    id 1
    label "ochrona"
    origin "text"
  ]
  node [
    id 2
    label "podwodne"
    origin "text"
  ]
  node [
    id 3
    label "dziedzictwo"
    origin "text"
  ]
  node [
    id 4
    label "kulturowy"
    origin "text"
  ]
  node [
    id 5
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 6
    label "zbi&#243;r"
  ]
  node [
    id 7
    label "uk&#322;ad"
  ]
  node [
    id 8
    label "styl"
  ]
  node [
    id 9
    label "line"
  ]
  node [
    id 10
    label "zwyczaj"
  ]
  node [
    id 11
    label "kanon"
  ]
  node [
    id 12
    label "zjazd"
  ]
  node [
    id 13
    label "egzemplarz"
  ]
  node [
    id 14
    label "series"
  ]
  node [
    id 15
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 16
    label "uprawianie"
  ]
  node [
    id 17
    label "praca_rolnicza"
  ]
  node [
    id 18
    label "collection"
  ]
  node [
    id 19
    label "dane"
  ]
  node [
    id 20
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 21
    label "pakiet_klimatyczny"
  ]
  node [
    id 22
    label "poj&#281;cie"
  ]
  node [
    id 23
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 24
    label "sum"
  ]
  node [
    id 25
    label "gathering"
  ]
  node [
    id 26
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 27
    label "album"
  ]
  node [
    id 28
    label "rozprz&#261;c"
  ]
  node [
    id 29
    label "treaty"
  ]
  node [
    id 30
    label "systemat"
  ]
  node [
    id 31
    label "system"
  ]
  node [
    id 32
    label "umowa"
  ]
  node [
    id 33
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 34
    label "struktura"
  ]
  node [
    id 35
    label "usenet"
  ]
  node [
    id 36
    label "przestawi&#263;"
  ]
  node [
    id 37
    label "alliance"
  ]
  node [
    id 38
    label "ONZ"
  ]
  node [
    id 39
    label "NATO"
  ]
  node [
    id 40
    label "konstelacja"
  ]
  node [
    id 41
    label "o&#347;"
  ]
  node [
    id 42
    label "podsystem"
  ]
  node [
    id 43
    label "zawarcie"
  ]
  node [
    id 44
    label "zawrze&#263;"
  ]
  node [
    id 45
    label "organ"
  ]
  node [
    id 46
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 47
    label "wi&#281;&#378;"
  ]
  node [
    id 48
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 49
    label "zachowanie"
  ]
  node [
    id 50
    label "cybernetyk"
  ]
  node [
    id 51
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 52
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 53
    label "sk&#322;ad"
  ]
  node [
    id 54
    label "traktat_wersalski"
  ]
  node [
    id 55
    label "cia&#322;o"
  ]
  node [
    id 56
    label "kombinacja_alpejska"
  ]
  node [
    id 57
    label "rally"
  ]
  node [
    id 58
    label "pochy&#322;o&#347;&#263;"
  ]
  node [
    id 59
    label "manewr"
  ]
  node [
    id 60
    label "przyjazd"
  ]
  node [
    id 61
    label "spotkanie"
  ]
  node [
    id 62
    label "dojazd"
  ]
  node [
    id 63
    label "jazda"
  ]
  node [
    id 64
    label "wy&#347;cig"
  ]
  node [
    id 65
    label "odjazd"
  ]
  node [
    id 66
    label "meeting"
  ]
  node [
    id 67
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 68
    label "kultura_duchowa"
  ]
  node [
    id 69
    label "kultura"
  ]
  node [
    id 70
    label "ceremony"
  ]
  node [
    id 71
    label "model"
  ]
  node [
    id 72
    label "stopie&#324;_pisma"
  ]
  node [
    id 73
    label "dekalog"
  ]
  node [
    id 74
    label "&#347;wi&#281;to&#347;&#263;"
  ]
  node [
    id 75
    label "msza"
  ]
  node [
    id 76
    label "zasada"
  ]
  node [
    id 77
    label "utw&#243;r"
  ]
  node [
    id 78
    label "criterion"
  ]
  node [
    id 79
    label "prawo"
  ]
  node [
    id 80
    label "trzonek"
  ]
  node [
    id 81
    label "reakcja"
  ]
  node [
    id 82
    label "narz&#281;dzie"
  ]
  node [
    id 83
    label "spos&#243;b"
  ]
  node [
    id 84
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 85
    label "stylik"
  ]
  node [
    id 86
    label "dyscyplina_sportowa"
  ]
  node [
    id 87
    label "handle"
  ]
  node [
    id 88
    label "stroke"
  ]
  node [
    id 89
    label "napisa&#263;"
  ]
  node [
    id 90
    label "charakter"
  ]
  node [
    id 91
    label "natural_language"
  ]
  node [
    id 92
    label "pisa&#263;"
  ]
  node [
    id 93
    label "behawior"
  ]
  node [
    id 94
    label "formacja"
  ]
  node [
    id 95
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 96
    label "obstawianie"
  ]
  node [
    id 97
    label "obstawienie"
  ]
  node [
    id 98
    label "tarcza"
  ]
  node [
    id 99
    label "ubezpieczenie"
  ]
  node [
    id 100
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 101
    label "transportacja"
  ]
  node [
    id 102
    label "obstawia&#263;"
  ]
  node [
    id 103
    label "obiekt"
  ]
  node [
    id 104
    label "borowiec"
  ]
  node [
    id 105
    label "chemical_bond"
  ]
  node [
    id 106
    label "co&#347;"
  ]
  node [
    id 107
    label "budynek"
  ]
  node [
    id 108
    label "thing"
  ]
  node [
    id 109
    label "program"
  ]
  node [
    id 110
    label "rzecz"
  ]
  node [
    id 111
    label "strona"
  ]
  node [
    id 112
    label "Bund"
  ]
  node [
    id 113
    label "Mazowsze"
  ]
  node [
    id 114
    label "PPR"
  ]
  node [
    id 115
    label "Jakobici"
  ]
  node [
    id 116
    label "zesp&#243;&#322;"
  ]
  node [
    id 117
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 118
    label "leksem"
  ]
  node [
    id 119
    label "SLD"
  ]
  node [
    id 120
    label "zespolik"
  ]
  node [
    id 121
    label "Razem"
  ]
  node [
    id 122
    label "PiS"
  ]
  node [
    id 123
    label "zjawisko"
  ]
  node [
    id 124
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 125
    label "partia"
  ]
  node [
    id 126
    label "Kuomintang"
  ]
  node [
    id 127
    label "ZSL"
  ]
  node [
    id 128
    label "szko&#322;a"
  ]
  node [
    id 129
    label "jednostka"
  ]
  node [
    id 130
    label "proces"
  ]
  node [
    id 131
    label "organizacja"
  ]
  node [
    id 132
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 133
    label "rugby"
  ]
  node [
    id 134
    label "AWS"
  ]
  node [
    id 135
    label "posta&#263;"
  ]
  node [
    id 136
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 137
    label "blok"
  ]
  node [
    id 138
    label "PO"
  ]
  node [
    id 139
    label "si&#322;a"
  ]
  node [
    id 140
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 141
    label "Federali&#347;ci"
  ]
  node [
    id 142
    label "PSL"
  ]
  node [
    id 143
    label "czynno&#347;&#263;"
  ]
  node [
    id 144
    label "wojsko"
  ]
  node [
    id 145
    label "Wigowie"
  ]
  node [
    id 146
    label "ZChN"
  ]
  node [
    id 147
    label "egzekutywa"
  ]
  node [
    id 148
    label "rocznik"
  ]
  node [
    id 149
    label "The_Beatles"
  ]
  node [
    id 150
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 151
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 152
    label "unit"
  ]
  node [
    id 153
    label "Depeche_Mode"
  ]
  node [
    id 154
    label "forma"
  ]
  node [
    id 155
    label "naszywka"
  ]
  node [
    id 156
    label "przedmiot"
  ]
  node [
    id 157
    label "kszta&#322;t"
  ]
  node [
    id 158
    label "wskaz&#243;wka"
  ]
  node [
    id 159
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 160
    label "obro&#324;ca"
  ]
  node [
    id 161
    label "bro&#324;_ochronna"
  ]
  node [
    id 162
    label "odznaka"
  ]
  node [
    id 163
    label "bro&#324;"
  ]
  node [
    id 164
    label "denture"
  ]
  node [
    id 165
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 166
    label "telefon"
  ]
  node [
    id 167
    label "or&#281;&#380;"
  ]
  node [
    id 168
    label "target"
  ]
  node [
    id 169
    label "cel"
  ]
  node [
    id 170
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 171
    label "maszyna"
  ]
  node [
    id 172
    label "obszar"
  ]
  node [
    id 173
    label "ucze&#324;"
  ]
  node [
    id 174
    label "powierzchnia"
  ]
  node [
    id 175
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 176
    label "tablica"
  ]
  node [
    id 177
    label "op&#322;ata"
  ]
  node [
    id 178
    label "ubezpieczalnia"
  ]
  node [
    id 179
    label "insurance"
  ]
  node [
    id 180
    label "cover"
  ]
  node [
    id 181
    label "franszyza"
  ]
  node [
    id 182
    label "screen"
  ]
  node [
    id 183
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 184
    label "suma_ubezpieczenia"
  ]
  node [
    id 185
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 186
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 187
    label "oddzia&#322;"
  ]
  node [
    id 188
    label "zapewnienie"
  ]
  node [
    id 189
    label "przyznanie"
  ]
  node [
    id 190
    label "uchronienie"
  ]
  node [
    id 191
    label "transport"
  ]
  node [
    id 192
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 193
    label "Irish_pound"
  ]
  node [
    id 194
    label "otoczenie"
  ]
  node [
    id 195
    label "bramka"
  ]
  node [
    id 196
    label "wytypowanie"
  ]
  node [
    id 197
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 198
    label "otaczanie"
  ]
  node [
    id 199
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 200
    label "os&#322;anianie"
  ]
  node [
    id 201
    label "typowanie"
  ]
  node [
    id 202
    label "ubezpieczanie"
  ]
  node [
    id 203
    label "ubezpiecza&#263;"
  ]
  node [
    id 204
    label "venture"
  ]
  node [
    id 205
    label "przewidywa&#263;"
  ]
  node [
    id 206
    label "zapewnia&#263;"
  ]
  node [
    id 207
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 208
    label "typowa&#263;"
  ]
  node [
    id 209
    label "zastawia&#263;"
  ]
  node [
    id 210
    label "budowa&#263;"
  ]
  node [
    id 211
    label "zajmowa&#263;"
  ]
  node [
    id 212
    label "obejmowa&#263;"
  ]
  node [
    id 213
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 214
    label "os&#322;ania&#263;"
  ]
  node [
    id 215
    label "otacza&#263;"
  ]
  node [
    id 216
    label "broni&#263;"
  ]
  node [
    id 217
    label "powierza&#263;"
  ]
  node [
    id 218
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 219
    label "frame"
  ]
  node [
    id 220
    label "wysy&#322;a&#263;"
  ]
  node [
    id 221
    label "typ"
  ]
  node [
    id 222
    label "borowce"
  ]
  node [
    id 223
    label "ochroniarz"
  ]
  node [
    id 224
    label "duch"
  ]
  node [
    id 225
    label "borowik"
  ]
  node [
    id 226
    label "nietoperz"
  ]
  node [
    id 227
    label "kozio&#322;ek"
  ]
  node [
    id 228
    label "owado&#380;erca"
  ]
  node [
    id 229
    label "funkcjonariusz"
  ]
  node [
    id 230
    label "BOR"
  ]
  node [
    id 231
    label "mroczkowate"
  ]
  node [
    id 232
    label "pierwiastek"
  ]
  node [
    id 233
    label "zachowek"
  ]
  node [
    id 234
    label "mienie"
  ]
  node [
    id 235
    label "wydziedziczenie"
  ]
  node [
    id 236
    label "scheda_spadkowa"
  ]
  node [
    id 237
    label "sukcesja"
  ]
  node [
    id 238
    label "wydziedziczy&#263;"
  ]
  node [
    id 239
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 240
    label "umocowa&#263;"
  ]
  node [
    id 241
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 242
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 243
    label "procesualistyka"
  ]
  node [
    id 244
    label "regu&#322;a_Allena"
  ]
  node [
    id 245
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 246
    label "kryminalistyka"
  ]
  node [
    id 247
    label "kierunek"
  ]
  node [
    id 248
    label "zasada_d'Alemberta"
  ]
  node [
    id 249
    label "obserwacja"
  ]
  node [
    id 250
    label "normatywizm"
  ]
  node [
    id 251
    label "jurisprudence"
  ]
  node [
    id 252
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 253
    label "przepis"
  ]
  node [
    id 254
    label "prawo_karne_procesowe"
  ]
  node [
    id 255
    label "kazuistyka"
  ]
  node [
    id 256
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 257
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 258
    label "kryminologia"
  ]
  node [
    id 259
    label "opis"
  ]
  node [
    id 260
    label "regu&#322;a_Glogera"
  ]
  node [
    id 261
    label "prawo_Mendla"
  ]
  node [
    id 262
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 263
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 264
    label "prawo_karne"
  ]
  node [
    id 265
    label "legislacyjnie"
  ]
  node [
    id 266
    label "twierdzenie"
  ]
  node [
    id 267
    label "cywilistyka"
  ]
  node [
    id 268
    label "judykatura"
  ]
  node [
    id 269
    label "kanonistyka"
  ]
  node [
    id 270
    label "standard"
  ]
  node [
    id 271
    label "nauka_prawa"
  ]
  node [
    id 272
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 273
    label "podmiot"
  ]
  node [
    id 274
    label "law"
  ]
  node [
    id 275
    label "qualification"
  ]
  node [
    id 276
    label "dominion"
  ]
  node [
    id 277
    label "wykonawczy"
  ]
  node [
    id 278
    label "normalizacja"
  ]
  node [
    id 279
    label "przej&#347;cie"
  ]
  node [
    id 280
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 281
    label "rodowo&#347;&#263;"
  ]
  node [
    id 282
    label "patent"
  ]
  node [
    id 283
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 284
    label "dobra"
  ]
  node [
    id 285
    label "stan"
  ]
  node [
    id 286
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 287
    label "przej&#347;&#263;"
  ]
  node [
    id 288
    label "possession"
  ]
  node [
    id 289
    label "spadek"
  ]
  node [
    id 290
    label "zabranie"
  ]
  node [
    id 291
    label "disinheritance"
  ]
  node [
    id 292
    label "zabra&#263;"
  ]
  node [
    id 293
    label "disinherit"
  ]
  node [
    id 294
    label "proces_biologiczny"
  ]
  node [
    id 295
    label "seniorat"
  ]
  node [
    id 296
    label "kulturowo"
  ]
  node [
    id 297
    label "ojciec"
  ]
  node [
    id 298
    label "podwodny"
  ]
  node [
    id 299
    label "morze"
  ]
  node [
    id 300
    label "polskie"
  ]
  node [
    id 301
    label "komitet"
  ]
  node [
    id 302
    label "do&#160;spraw"
  ]
  node [
    id 303
    label "UNESCO"
  ]
  node [
    id 304
    label "Zbigniew"
  ]
  node [
    id 305
    label "kobyli&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 79
    target 297
  ]
  edge [
    source 79
    target 299
  ]
  edge [
    source 297
    target 298
  ]
  edge [
    source 297
    target 299
  ]
  edge [
    source 300
    target 301
  ]
  edge [
    source 300
    target 302
  ]
  edge [
    source 300
    target 303
  ]
  edge [
    source 301
    target 302
  ]
  edge [
    source 301
    target 303
  ]
  edge [
    source 302
    target 303
  ]
  edge [
    source 304
    target 305
  ]
]
