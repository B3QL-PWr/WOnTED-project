graph [
  node [
    id 0
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "koszt"
    origin "text"
  ]
  node [
    id 3
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 4
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 6
    label "kolejny"
  ]
  node [
    id 7
    label "nast&#281;pnie"
  ]
  node [
    id 8
    label "inny"
  ]
  node [
    id 9
    label "nastopny"
  ]
  node [
    id 10
    label "kolejno"
  ]
  node [
    id 11
    label "kt&#243;ry&#347;"
  ]
  node [
    id 12
    label "p&#243;&#322;rocze"
  ]
  node [
    id 13
    label "martwy_sezon"
  ]
  node [
    id 14
    label "kalendarz"
  ]
  node [
    id 15
    label "cykl_astronomiczny"
  ]
  node [
    id 16
    label "lata"
  ]
  node [
    id 17
    label "pora_roku"
  ]
  node [
    id 18
    label "stulecie"
  ]
  node [
    id 19
    label "kurs"
  ]
  node [
    id 20
    label "czas"
  ]
  node [
    id 21
    label "jubileusz"
  ]
  node [
    id 22
    label "grupa"
  ]
  node [
    id 23
    label "kwarta&#322;"
  ]
  node [
    id 24
    label "miesi&#261;c"
  ]
  node [
    id 25
    label "summer"
  ]
  node [
    id 26
    label "odm&#322;adzanie"
  ]
  node [
    id 27
    label "liga"
  ]
  node [
    id 28
    label "jednostka_systematyczna"
  ]
  node [
    id 29
    label "asymilowanie"
  ]
  node [
    id 30
    label "gromada"
  ]
  node [
    id 31
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 32
    label "asymilowa&#263;"
  ]
  node [
    id 33
    label "egzemplarz"
  ]
  node [
    id 34
    label "Entuzjastki"
  ]
  node [
    id 35
    label "zbi&#243;r"
  ]
  node [
    id 36
    label "kompozycja"
  ]
  node [
    id 37
    label "Terranie"
  ]
  node [
    id 38
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 39
    label "category"
  ]
  node [
    id 40
    label "pakiet_klimatyczny"
  ]
  node [
    id 41
    label "oddzia&#322;"
  ]
  node [
    id 42
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 43
    label "cz&#261;steczka"
  ]
  node [
    id 44
    label "stage_set"
  ]
  node [
    id 45
    label "type"
  ]
  node [
    id 46
    label "specgrupa"
  ]
  node [
    id 47
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 48
    label "&#346;wietliki"
  ]
  node [
    id 49
    label "odm&#322;odzenie"
  ]
  node [
    id 50
    label "Eurogrupa"
  ]
  node [
    id 51
    label "odm&#322;adza&#263;"
  ]
  node [
    id 52
    label "formacja_geologiczna"
  ]
  node [
    id 53
    label "harcerze_starsi"
  ]
  node [
    id 54
    label "poprzedzanie"
  ]
  node [
    id 55
    label "czasoprzestrze&#324;"
  ]
  node [
    id 56
    label "laba"
  ]
  node [
    id 57
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 58
    label "chronometria"
  ]
  node [
    id 59
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 60
    label "rachuba_czasu"
  ]
  node [
    id 61
    label "przep&#322;ywanie"
  ]
  node [
    id 62
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 63
    label "czasokres"
  ]
  node [
    id 64
    label "odczyt"
  ]
  node [
    id 65
    label "chwila"
  ]
  node [
    id 66
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 67
    label "dzieje"
  ]
  node [
    id 68
    label "kategoria_gramatyczna"
  ]
  node [
    id 69
    label "poprzedzenie"
  ]
  node [
    id 70
    label "trawienie"
  ]
  node [
    id 71
    label "pochodzi&#263;"
  ]
  node [
    id 72
    label "period"
  ]
  node [
    id 73
    label "okres_czasu"
  ]
  node [
    id 74
    label "poprzedza&#263;"
  ]
  node [
    id 75
    label "schy&#322;ek"
  ]
  node [
    id 76
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 77
    label "odwlekanie_si&#281;"
  ]
  node [
    id 78
    label "zegar"
  ]
  node [
    id 79
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 80
    label "czwarty_wymiar"
  ]
  node [
    id 81
    label "pochodzenie"
  ]
  node [
    id 82
    label "koniugacja"
  ]
  node [
    id 83
    label "Zeitgeist"
  ]
  node [
    id 84
    label "trawi&#263;"
  ]
  node [
    id 85
    label "pogoda"
  ]
  node [
    id 86
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 87
    label "poprzedzi&#263;"
  ]
  node [
    id 88
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 89
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 90
    label "time_period"
  ]
  node [
    id 91
    label "tydzie&#324;"
  ]
  node [
    id 92
    label "miech"
  ]
  node [
    id 93
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 94
    label "kalendy"
  ]
  node [
    id 95
    label "term"
  ]
  node [
    id 96
    label "rok_akademicki"
  ]
  node [
    id 97
    label "rok_szkolny"
  ]
  node [
    id 98
    label "semester"
  ]
  node [
    id 99
    label "anniwersarz"
  ]
  node [
    id 100
    label "rocznica"
  ]
  node [
    id 101
    label "obszar"
  ]
  node [
    id 102
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 103
    label "long_time"
  ]
  node [
    id 104
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 105
    label "almanac"
  ]
  node [
    id 106
    label "rozk&#322;ad"
  ]
  node [
    id 107
    label "wydawnictwo"
  ]
  node [
    id 108
    label "Juliusz_Cezar"
  ]
  node [
    id 109
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 110
    label "zwy&#380;kowanie"
  ]
  node [
    id 111
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 112
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 113
    label "zaj&#281;cia"
  ]
  node [
    id 114
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 115
    label "trasa"
  ]
  node [
    id 116
    label "przeorientowywanie"
  ]
  node [
    id 117
    label "przejazd"
  ]
  node [
    id 118
    label "kierunek"
  ]
  node [
    id 119
    label "przeorientowywa&#263;"
  ]
  node [
    id 120
    label "nauka"
  ]
  node [
    id 121
    label "przeorientowanie"
  ]
  node [
    id 122
    label "klasa"
  ]
  node [
    id 123
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 124
    label "przeorientowa&#263;"
  ]
  node [
    id 125
    label "manner"
  ]
  node [
    id 126
    label "course"
  ]
  node [
    id 127
    label "passage"
  ]
  node [
    id 128
    label "zni&#380;kowanie"
  ]
  node [
    id 129
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 130
    label "seria"
  ]
  node [
    id 131
    label "stawka"
  ]
  node [
    id 132
    label "way"
  ]
  node [
    id 133
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 134
    label "spos&#243;b"
  ]
  node [
    id 135
    label "deprecjacja"
  ]
  node [
    id 136
    label "cedu&#322;a"
  ]
  node [
    id 137
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 138
    label "drive"
  ]
  node [
    id 139
    label "bearing"
  ]
  node [
    id 140
    label "Lira"
  ]
  node [
    id 141
    label "wydatek"
  ]
  node [
    id 142
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 143
    label "sumpt"
  ]
  node [
    id 144
    label "nak&#322;ad"
  ]
  node [
    id 145
    label "ilo&#347;&#263;"
  ]
  node [
    id 146
    label "kwota"
  ]
  node [
    id 147
    label "liczba"
  ]
  node [
    id 148
    label "wych&#243;d"
  ]
  node [
    id 149
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 150
    label "energia"
  ]
  node [
    id 151
    label "przep&#322;yw"
  ]
  node [
    id 152
    label "ideologia"
  ]
  node [
    id 153
    label "apparent_motion"
  ]
  node [
    id 154
    label "przyp&#322;yw"
  ]
  node [
    id 155
    label "metoda"
  ]
  node [
    id 156
    label "electricity"
  ]
  node [
    id 157
    label "dreszcz"
  ]
  node [
    id 158
    label "ruch"
  ]
  node [
    id 159
    label "zjawisko"
  ]
  node [
    id 160
    label "praktyka"
  ]
  node [
    id 161
    label "system"
  ]
  node [
    id 162
    label "flow"
  ]
  node [
    id 163
    label "p&#322;yw"
  ]
  node [
    id 164
    label "wzrost"
  ]
  node [
    id 165
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 166
    label "fit"
  ]
  node [
    id 167
    label "reakcja"
  ]
  node [
    id 168
    label "obieg"
  ]
  node [
    id 169
    label "flux"
  ]
  node [
    id 170
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 171
    label "egzergia"
  ]
  node [
    id 172
    label "emitowa&#263;"
  ]
  node [
    id 173
    label "kwant_energii"
  ]
  node [
    id 174
    label "szwung"
  ]
  node [
    id 175
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 176
    label "power"
  ]
  node [
    id 177
    label "cecha"
  ]
  node [
    id 178
    label "emitowanie"
  ]
  node [
    id 179
    label "energy"
  ]
  node [
    id 180
    label "proces"
  ]
  node [
    id 181
    label "boski"
  ]
  node [
    id 182
    label "krajobraz"
  ]
  node [
    id 183
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 184
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 185
    label "przywidzenie"
  ]
  node [
    id 186
    label "presence"
  ]
  node [
    id 187
    label "charakter"
  ]
  node [
    id 188
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 189
    label "practice"
  ]
  node [
    id 190
    label "wiedza"
  ]
  node [
    id 191
    label "znawstwo"
  ]
  node [
    id 192
    label "skill"
  ]
  node [
    id 193
    label "czyn"
  ]
  node [
    id 194
    label "zwyczaj"
  ]
  node [
    id 195
    label "eksperiencja"
  ]
  node [
    id 196
    label "praca"
  ]
  node [
    id 197
    label "j&#261;dro"
  ]
  node [
    id 198
    label "systemik"
  ]
  node [
    id 199
    label "rozprz&#261;c"
  ]
  node [
    id 200
    label "oprogramowanie"
  ]
  node [
    id 201
    label "poj&#281;cie"
  ]
  node [
    id 202
    label "systemat"
  ]
  node [
    id 203
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 204
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 205
    label "model"
  ]
  node [
    id 206
    label "struktura"
  ]
  node [
    id 207
    label "usenet"
  ]
  node [
    id 208
    label "s&#261;d"
  ]
  node [
    id 209
    label "porz&#261;dek"
  ]
  node [
    id 210
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 211
    label "przyn&#281;ta"
  ]
  node [
    id 212
    label "p&#322;&#243;d"
  ]
  node [
    id 213
    label "net"
  ]
  node [
    id 214
    label "w&#281;dkarstwo"
  ]
  node [
    id 215
    label "eratem"
  ]
  node [
    id 216
    label "doktryna"
  ]
  node [
    id 217
    label "pulpit"
  ]
  node [
    id 218
    label "konstelacja"
  ]
  node [
    id 219
    label "jednostka_geologiczna"
  ]
  node [
    id 220
    label "o&#347;"
  ]
  node [
    id 221
    label "podsystem"
  ]
  node [
    id 222
    label "ryba"
  ]
  node [
    id 223
    label "Leopard"
  ]
  node [
    id 224
    label "Android"
  ]
  node [
    id 225
    label "zachowanie"
  ]
  node [
    id 226
    label "cybernetyk"
  ]
  node [
    id 227
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 228
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 229
    label "method"
  ]
  node [
    id 230
    label "sk&#322;ad"
  ]
  node [
    id 231
    label "podstawa"
  ]
  node [
    id 232
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 233
    label "mechanika"
  ]
  node [
    id 234
    label "utrzymywanie"
  ]
  node [
    id 235
    label "move"
  ]
  node [
    id 236
    label "poruszenie"
  ]
  node [
    id 237
    label "movement"
  ]
  node [
    id 238
    label "myk"
  ]
  node [
    id 239
    label "utrzyma&#263;"
  ]
  node [
    id 240
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 241
    label "utrzymanie"
  ]
  node [
    id 242
    label "travel"
  ]
  node [
    id 243
    label "kanciasty"
  ]
  node [
    id 244
    label "commercial_enterprise"
  ]
  node [
    id 245
    label "strumie&#324;"
  ]
  node [
    id 246
    label "aktywno&#347;&#263;"
  ]
  node [
    id 247
    label "kr&#243;tki"
  ]
  node [
    id 248
    label "taktyka"
  ]
  node [
    id 249
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 250
    label "apraksja"
  ]
  node [
    id 251
    label "natural_process"
  ]
  node [
    id 252
    label "utrzymywa&#263;"
  ]
  node [
    id 253
    label "d&#322;ugi"
  ]
  node [
    id 254
    label "wydarzenie"
  ]
  node [
    id 255
    label "dyssypacja_energii"
  ]
  node [
    id 256
    label "tumult"
  ]
  node [
    id 257
    label "stopek"
  ]
  node [
    id 258
    label "czynno&#347;&#263;"
  ]
  node [
    id 259
    label "zmiana"
  ]
  node [
    id 260
    label "manewr"
  ]
  node [
    id 261
    label "lokomocja"
  ]
  node [
    id 262
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 263
    label "komunikacja"
  ]
  node [
    id 264
    label "drift"
  ]
  node [
    id 265
    label "oznaka"
  ]
  node [
    id 266
    label "doznanie"
  ]
  node [
    id 267
    label "throb"
  ]
  node [
    id 268
    label "ci&#261;goty"
  ]
  node [
    id 269
    label "political_orientation"
  ]
  node [
    id 270
    label "idea"
  ]
  node [
    id 271
    label "szko&#322;a"
  ]
  node [
    id 272
    label "sail"
  ]
  node [
    id 273
    label "leave"
  ]
  node [
    id 274
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 275
    label "proceed"
  ]
  node [
    id 276
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 277
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 278
    label "zrobi&#263;"
  ]
  node [
    id 279
    label "zmieni&#263;"
  ]
  node [
    id 280
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 281
    label "zosta&#263;"
  ]
  node [
    id 282
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 283
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 284
    label "przyj&#261;&#263;"
  ]
  node [
    id 285
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 286
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 287
    label "uda&#263;_si&#281;"
  ]
  node [
    id 288
    label "zacz&#261;&#263;"
  ]
  node [
    id 289
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 290
    label "play_along"
  ]
  node [
    id 291
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 292
    label "opu&#347;ci&#263;"
  ]
  node [
    id 293
    label "become"
  ]
  node [
    id 294
    label "post&#261;pi&#263;"
  ]
  node [
    id 295
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 296
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 297
    label "odj&#261;&#263;"
  ]
  node [
    id 298
    label "cause"
  ]
  node [
    id 299
    label "introduce"
  ]
  node [
    id 300
    label "begin"
  ]
  node [
    id 301
    label "do"
  ]
  node [
    id 302
    label "sprawi&#263;"
  ]
  node [
    id 303
    label "change"
  ]
  node [
    id 304
    label "zast&#261;pi&#263;"
  ]
  node [
    id 305
    label "come_up"
  ]
  node [
    id 306
    label "przej&#347;&#263;"
  ]
  node [
    id 307
    label "straci&#263;"
  ]
  node [
    id 308
    label "zyska&#263;"
  ]
  node [
    id 309
    label "przybra&#263;"
  ]
  node [
    id 310
    label "strike"
  ]
  node [
    id 311
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 312
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 313
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 314
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 315
    label "receive"
  ]
  node [
    id 316
    label "obra&#263;"
  ]
  node [
    id 317
    label "uzna&#263;"
  ]
  node [
    id 318
    label "draw"
  ]
  node [
    id 319
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 320
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 321
    label "przyj&#281;cie"
  ]
  node [
    id 322
    label "fall"
  ]
  node [
    id 323
    label "swallow"
  ]
  node [
    id 324
    label "odebra&#263;"
  ]
  node [
    id 325
    label "dostarczy&#263;"
  ]
  node [
    id 326
    label "umie&#347;ci&#263;"
  ]
  node [
    id 327
    label "wzi&#261;&#263;"
  ]
  node [
    id 328
    label "absorb"
  ]
  node [
    id 329
    label "undertake"
  ]
  node [
    id 330
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 331
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 332
    label "osta&#263;_si&#281;"
  ]
  node [
    id 333
    label "pozosta&#263;"
  ]
  node [
    id 334
    label "catch"
  ]
  node [
    id 335
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 336
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 337
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 338
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 339
    label "zorganizowa&#263;"
  ]
  node [
    id 340
    label "appoint"
  ]
  node [
    id 341
    label "wystylizowa&#263;"
  ]
  node [
    id 342
    label "przerobi&#263;"
  ]
  node [
    id 343
    label "nabra&#263;"
  ]
  node [
    id 344
    label "make"
  ]
  node [
    id 345
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 346
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 347
    label "wydali&#263;"
  ]
  node [
    id 348
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 349
    label "pozostawi&#263;"
  ]
  node [
    id 350
    label "obni&#380;y&#263;"
  ]
  node [
    id 351
    label "zostawi&#263;"
  ]
  node [
    id 352
    label "przesta&#263;"
  ]
  node [
    id 353
    label "potani&#263;"
  ]
  node [
    id 354
    label "drop"
  ]
  node [
    id 355
    label "evacuate"
  ]
  node [
    id 356
    label "humiliate"
  ]
  node [
    id 357
    label "tekst"
  ]
  node [
    id 358
    label "authorize"
  ]
  node [
    id 359
    label "omin&#261;&#263;"
  ]
  node [
    id 360
    label "loom"
  ]
  node [
    id 361
    label "result"
  ]
  node [
    id 362
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 363
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 364
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 365
    label "appear"
  ]
  node [
    id 366
    label "zgin&#261;&#263;"
  ]
  node [
    id 367
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 368
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 369
    label "rise"
  ]
  node [
    id 370
    label "przedmiot"
  ]
  node [
    id 371
    label "przelezienie"
  ]
  node [
    id 372
    label "&#347;piew"
  ]
  node [
    id 373
    label "Synaj"
  ]
  node [
    id 374
    label "Kreml"
  ]
  node [
    id 375
    label "d&#378;wi&#281;k"
  ]
  node [
    id 376
    label "wysoki"
  ]
  node [
    id 377
    label "element"
  ]
  node [
    id 378
    label "wzniesienie"
  ]
  node [
    id 379
    label "pi&#281;tro"
  ]
  node [
    id 380
    label "Ropa"
  ]
  node [
    id 381
    label "kupa"
  ]
  node [
    id 382
    label "przele&#378;&#263;"
  ]
  node [
    id 383
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 384
    label "karczek"
  ]
  node [
    id 385
    label "rami&#261;czko"
  ]
  node [
    id 386
    label "Jaworze"
  ]
  node [
    id 387
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 388
    label "Rzym_Zachodni"
  ]
  node [
    id 389
    label "whole"
  ]
  node [
    id 390
    label "Rzym_Wschodni"
  ]
  node [
    id 391
    label "urz&#261;dzenie"
  ]
  node [
    id 392
    label "kszta&#322;t"
  ]
  node [
    id 393
    label "nabudowanie"
  ]
  node [
    id 394
    label "Skalnik"
  ]
  node [
    id 395
    label "budowla"
  ]
  node [
    id 396
    label "raise"
  ]
  node [
    id 397
    label "wierzchowina"
  ]
  node [
    id 398
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 399
    label "Sikornik"
  ]
  node [
    id 400
    label "miejsce"
  ]
  node [
    id 401
    label "Bukowiec"
  ]
  node [
    id 402
    label "Izera"
  ]
  node [
    id 403
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 404
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 405
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 406
    label "podniesienie"
  ]
  node [
    id 407
    label "Zwalisko"
  ]
  node [
    id 408
    label "Bielec"
  ]
  node [
    id 409
    label "construction"
  ]
  node [
    id 410
    label "zrobienie"
  ]
  node [
    id 411
    label "phone"
  ]
  node [
    id 412
    label "wpadni&#281;cie"
  ]
  node [
    id 413
    label "wydawa&#263;"
  ]
  node [
    id 414
    label "wyda&#263;"
  ]
  node [
    id 415
    label "intonacja"
  ]
  node [
    id 416
    label "wpa&#347;&#263;"
  ]
  node [
    id 417
    label "note"
  ]
  node [
    id 418
    label "onomatopeja"
  ]
  node [
    id 419
    label "modalizm"
  ]
  node [
    id 420
    label "nadlecenie"
  ]
  node [
    id 421
    label "sound"
  ]
  node [
    id 422
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 423
    label "wpada&#263;"
  ]
  node [
    id 424
    label "solmizacja"
  ]
  node [
    id 425
    label "dobiec"
  ]
  node [
    id 426
    label "transmiter"
  ]
  node [
    id 427
    label "heksachord"
  ]
  node [
    id 428
    label "akcent"
  ]
  node [
    id 429
    label "wydanie"
  ]
  node [
    id 430
    label "repetycja"
  ]
  node [
    id 431
    label "brzmienie"
  ]
  node [
    id 432
    label "wpadanie"
  ]
  node [
    id 433
    label "zboczenie"
  ]
  node [
    id 434
    label "om&#243;wienie"
  ]
  node [
    id 435
    label "sponiewieranie"
  ]
  node [
    id 436
    label "discipline"
  ]
  node [
    id 437
    label "rzecz"
  ]
  node [
    id 438
    label "omawia&#263;"
  ]
  node [
    id 439
    label "kr&#261;&#380;enie"
  ]
  node [
    id 440
    label "tre&#347;&#263;"
  ]
  node [
    id 441
    label "robienie"
  ]
  node [
    id 442
    label "sponiewiera&#263;"
  ]
  node [
    id 443
    label "entity"
  ]
  node [
    id 444
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 445
    label "tematyka"
  ]
  node [
    id 446
    label "w&#261;tek"
  ]
  node [
    id 447
    label "zbaczanie"
  ]
  node [
    id 448
    label "program_nauczania"
  ]
  node [
    id 449
    label "om&#243;wi&#263;"
  ]
  node [
    id 450
    label "omawianie"
  ]
  node [
    id 451
    label "thing"
  ]
  node [
    id 452
    label "kultura"
  ]
  node [
    id 453
    label "istota"
  ]
  node [
    id 454
    label "zbacza&#263;"
  ]
  node [
    id 455
    label "zboczy&#263;"
  ]
  node [
    id 456
    label "r&#243;&#380;niczka"
  ]
  node [
    id 457
    label "&#347;rodowisko"
  ]
  node [
    id 458
    label "materia"
  ]
  node [
    id 459
    label "szambo"
  ]
  node [
    id 460
    label "aspo&#322;eczny"
  ]
  node [
    id 461
    label "component"
  ]
  node [
    id 462
    label "szkodnik"
  ]
  node [
    id 463
    label "gangsterski"
  ]
  node [
    id 464
    label "underworld"
  ]
  node [
    id 465
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 466
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 467
    label "p&#322;aszczyzna"
  ]
  node [
    id 468
    label "chronozona"
  ]
  node [
    id 469
    label "kondygnacja"
  ]
  node [
    id 470
    label "budynek"
  ]
  node [
    id 471
    label "eta&#380;"
  ]
  node [
    id 472
    label "floor"
  ]
  node [
    id 473
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 474
    label "tragedia"
  ]
  node [
    id 475
    label "wydalina"
  ]
  node [
    id 476
    label "stool"
  ]
  node [
    id 477
    label "koprofilia"
  ]
  node [
    id 478
    label "odchody"
  ]
  node [
    id 479
    label "mn&#243;stwo"
  ]
  node [
    id 480
    label "knoll"
  ]
  node [
    id 481
    label "balas"
  ]
  node [
    id 482
    label "g&#243;wno"
  ]
  node [
    id 483
    label "fekalia"
  ]
  node [
    id 484
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 485
    label "Moj&#380;esz"
  ]
  node [
    id 486
    label "Egipt"
  ]
  node [
    id 487
    label "Beskid_Niski"
  ]
  node [
    id 488
    label "Tatry"
  ]
  node [
    id 489
    label "Ma&#322;opolska"
  ]
  node [
    id 490
    label "przebieg"
  ]
  node [
    id 491
    label "studia"
  ]
  node [
    id 492
    label "linia"
  ]
  node [
    id 493
    label "bok"
  ]
  node [
    id 494
    label "skr&#281;canie"
  ]
  node [
    id 495
    label "skr&#281;ca&#263;"
  ]
  node [
    id 496
    label "orientowanie"
  ]
  node [
    id 497
    label "skr&#281;ci&#263;"
  ]
  node [
    id 498
    label "zorientowanie"
  ]
  node [
    id 499
    label "ty&#322;"
  ]
  node [
    id 500
    label "zorientowa&#263;"
  ]
  node [
    id 501
    label "orientowa&#263;"
  ]
  node [
    id 502
    label "orientacja"
  ]
  node [
    id 503
    label "prz&#243;d"
  ]
  node [
    id 504
    label "skr&#281;cenie"
  ]
  node [
    id 505
    label "ascent"
  ]
  node [
    id 506
    label "przeby&#263;"
  ]
  node [
    id 507
    label "beat"
  ]
  node [
    id 508
    label "min&#261;&#263;"
  ]
  node [
    id 509
    label "przekroczy&#263;"
  ]
  node [
    id 510
    label "pique"
  ]
  node [
    id 511
    label "mini&#281;cie"
  ]
  node [
    id 512
    label "przepuszczenie"
  ]
  node [
    id 513
    label "przebycie"
  ]
  node [
    id 514
    label "traversal"
  ]
  node [
    id 515
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 516
    label "prze&#322;a&#380;enie"
  ]
  node [
    id 517
    label "offense"
  ]
  node [
    id 518
    label "przekroczenie"
  ]
  node [
    id 519
    label "breeze"
  ]
  node [
    id 520
    label "wokal"
  ]
  node [
    id 521
    label "muzyka"
  ]
  node [
    id 522
    label "d&#243;&#322;"
  ]
  node [
    id 523
    label "impostacja"
  ]
  node [
    id 524
    label "g&#322;os"
  ]
  node [
    id 525
    label "odg&#322;os"
  ]
  node [
    id 526
    label "pienie"
  ]
  node [
    id 527
    label "wyrafinowany"
  ]
  node [
    id 528
    label "niepo&#347;ledni"
  ]
  node [
    id 529
    label "du&#380;y"
  ]
  node [
    id 530
    label "chwalebny"
  ]
  node [
    id 531
    label "z_wysoka"
  ]
  node [
    id 532
    label "wznios&#322;y"
  ]
  node [
    id 533
    label "daleki"
  ]
  node [
    id 534
    label "wysoce"
  ]
  node [
    id 535
    label "szczytnie"
  ]
  node [
    id 536
    label "znaczny"
  ]
  node [
    id 537
    label "warto&#347;ciowy"
  ]
  node [
    id 538
    label "wysoko"
  ]
  node [
    id 539
    label "uprzywilejowany"
  ]
  node [
    id 540
    label "tusza"
  ]
  node [
    id 541
    label "mi&#281;so"
  ]
  node [
    id 542
    label "strap"
  ]
  node [
    id 543
    label "pasek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
]
