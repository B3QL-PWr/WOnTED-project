graph [
  node [
    id 0
    label "wjazd"
    origin "text"
  ]
  node [
    id 1
    label "p&#243;&#378;ny"
    origin "text"
  ]
  node [
    id 2
    label "pomara&#324;czowy"
    origin "text"
  ]
  node [
    id 3
    label "zako&#324;czony"
    origin "text"
  ]
  node [
    id 4
    label "ucieczka"
    origin "text"
  ]
  node [
    id 5
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "pieszy"
    origin "text"
  ]
  node [
    id 8
    label "droga"
  ]
  node [
    id 9
    label "wydarzenie"
  ]
  node [
    id 10
    label "zawiasy"
  ]
  node [
    id 11
    label "antaba"
  ]
  node [
    id 12
    label "ogrodzenie"
  ]
  node [
    id 13
    label "zamek"
  ]
  node [
    id 14
    label "budowa"
  ]
  node [
    id 15
    label "wrzeci&#261;dz"
  ]
  node [
    id 16
    label "dost&#281;p"
  ]
  node [
    id 17
    label "wej&#347;cie"
  ]
  node [
    id 18
    label "informatyka"
  ]
  node [
    id 19
    label "operacja"
  ]
  node [
    id 20
    label "konto"
  ]
  node [
    id 21
    label "miejsce"
  ]
  node [
    id 22
    label "has&#322;o"
  ]
  node [
    id 23
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 24
    label "wnikni&#281;cie"
  ]
  node [
    id 25
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 26
    label "spotkanie"
  ]
  node [
    id 27
    label "poznanie"
  ]
  node [
    id 28
    label "pojawienie_si&#281;"
  ]
  node [
    id 29
    label "zdarzenie_si&#281;"
  ]
  node [
    id 30
    label "przenikni&#281;cie"
  ]
  node [
    id 31
    label "wpuszczenie"
  ]
  node [
    id 32
    label "zaatakowanie"
  ]
  node [
    id 33
    label "trespass"
  ]
  node [
    id 34
    label "doj&#347;cie"
  ]
  node [
    id 35
    label "przekroczenie"
  ]
  node [
    id 36
    label "otw&#243;r"
  ]
  node [
    id 37
    label "wzi&#281;cie"
  ]
  node [
    id 38
    label "vent"
  ]
  node [
    id 39
    label "stimulation"
  ]
  node [
    id 40
    label "dostanie_si&#281;"
  ]
  node [
    id 41
    label "pocz&#261;tek"
  ]
  node [
    id 42
    label "approach"
  ]
  node [
    id 43
    label "release"
  ]
  node [
    id 44
    label "wnij&#347;cie"
  ]
  node [
    id 45
    label "bramka"
  ]
  node [
    id 46
    label "wzniesienie_si&#281;"
  ]
  node [
    id 47
    label "podw&#243;rze"
  ]
  node [
    id 48
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 49
    label "dom"
  ]
  node [
    id 50
    label "wch&#243;d"
  ]
  node [
    id 51
    label "nast&#261;pienie"
  ]
  node [
    id 52
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 53
    label "zacz&#281;cie"
  ]
  node [
    id 54
    label "cz&#322;onek"
  ]
  node [
    id 55
    label "stanie_si&#281;"
  ]
  node [
    id 56
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 57
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 58
    label "urz&#261;dzenie"
  ]
  node [
    id 59
    label "przebiec"
  ]
  node [
    id 60
    label "charakter"
  ]
  node [
    id 61
    label "czynno&#347;&#263;"
  ]
  node [
    id 62
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 63
    label "motyw"
  ]
  node [
    id 64
    label "przebiegni&#281;cie"
  ]
  node [
    id 65
    label "fabu&#322;a"
  ]
  node [
    id 66
    label "ekskursja"
  ]
  node [
    id 67
    label "bezsilnikowy"
  ]
  node [
    id 68
    label "budowla"
  ]
  node [
    id 69
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 70
    label "trasa"
  ]
  node [
    id 71
    label "podbieg"
  ]
  node [
    id 72
    label "turystyka"
  ]
  node [
    id 73
    label "nawierzchnia"
  ]
  node [
    id 74
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 75
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 76
    label "rajza"
  ]
  node [
    id 77
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 78
    label "korona_drogi"
  ]
  node [
    id 79
    label "passage"
  ]
  node [
    id 80
    label "wylot"
  ]
  node [
    id 81
    label "ekwipunek"
  ]
  node [
    id 82
    label "zbior&#243;wka"
  ]
  node [
    id 83
    label "marszrutyzacja"
  ]
  node [
    id 84
    label "wyb&#243;j"
  ]
  node [
    id 85
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 86
    label "drogowskaz"
  ]
  node [
    id 87
    label "spos&#243;b"
  ]
  node [
    id 88
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 89
    label "pobocze"
  ]
  node [
    id 90
    label "journey"
  ]
  node [
    id 91
    label "ruch"
  ]
  node [
    id 92
    label "mechanika"
  ]
  node [
    id 93
    label "struktura"
  ]
  node [
    id 94
    label "figura"
  ]
  node [
    id 95
    label "miejsce_pracy"
  ]
  node [
    id 96
    label "cecha"
  ]
  node [
    id 97
    label "organ"
  ]
  node [
    id 98
    label "kreacja"
  ]
  node [
    id 99
    label "zwierz&#281;"
  ]
  node [
    id 100
    label "r&#243;w"
  ]
  node [
    id 101
    label "posesja"
  ]
  node [
    id 102
    label "konstrukcja"
  ]
  node [
    id 103
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "constitution"
  ]
  node [
    id 106
    label "grodza"
  ]
  node [
    id 107
    label "okalanie"
  ]
  node [
    id 108
    label "railing"
  ]
  node [
    id 109
    label "przeszkoda"
  ]
  node [
    id 110
    label "przestrze&#324;"
  ]
  node [
    id 111
    label "reservation"
  ]
  node [
    id 112
    label "otoczenie"
  ]
  node [
    id 113
    label "wskok"
  ]
  node [
    id 114
    label "kraal"
  ]
  node [
    id 115
    label "limitation"
  ]
  node [
    id 116
    label "rzecz"
  ]
  node [
    id 117
    label "drzwi"
  ]
  node [
    id 118
    label "klaps"
  ]
  node [
    id 119
    label "okucie"
  ]
  node [
    id 120
    label "sztaba"
  ]
  node [
    id 121
    label "brama"
  ]
  node [
    id 122
    label "skrzynia"
  ]
  node [
    id 123
    label "handle"
  ]
  node [
    id 124
    label "uchwyt"
  ]
  node [
    id 125
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 126
    label "blokada"
  ]
  node [
    id 127
    label "zamkni&#281;cie"
  ]
  node [
    id 128
    label "bro&#324;_palna"
  ]
  node [
    id 129
    label "blockage"
  ]
  node [
    id 130
    label "zapi&#281;cie"
  ]
  node [
    id 131
    label "tercja"
  ]
  node [
    id 132
    label "budynek"
  ]
  node [
    id 133
    label "ekspres"
  ]
  node [
    id 134
    label "mechanizm"
  ]
  node [
    id 135
    label "komora_zamkowa"
  ]
  node [
    id 136
    label "Windsor"
  ]
  node [
    id 137
    label "stra&#380;nica"
  ]
  node [
    id 138
    label "fortyfikacja"
  ]
  node [
    id 139
    label "rezydencja"
  ]
  node [
    id 140
    label "iglica"
  ]
  node [
    id 141
    label "zagrywka"
  ]
  node [
    id 142
    label "hokej"
  ]
  node [
    id 143
    label "baszta"
  ]
  node [
    id 144
    label "fastener"
  ]
  node [
    id 145
    label "Wawel"
  ]
  node [
    id 146
    label "do_p&#243;&#378;na"
  ]
  node [
    id 147
    label "p&#243;&#378;no"
  ]
  node [
    id 148
    label "pomara&#324;czowo"
  ]
  node [
    id 149
    label "owocowy"
  ]
  node [
    id 150
    label "ciep&#322;y"
  ]
  node [
    id 151
    label "cytrusowy"
  ]
  node [
    id 152
    label "mi&#322;y"
  ]
  node [
    id 153
    label "ocieplanie_si&#281;"
  ]
  node [
    id 154
    label "ocieplanie"
  ]
  node [
    id 155
    label "grzanie"
  ]
  node [
    id 156
    label "ocieplenie_si&#281;"
  ]
  node [
    id 157
    label "zagrzanie"
  ]
  node [
    id 158
    label "ocieplenie"
  ]
  node [
    id 159
    label "korzystny"
  ]
  node [
    id 160
    label "przyjemny"
  ]
  node [
    id 161
    label "ciep&#322;o"
  ]
  node [
    id 162
    label "dobry"
  ]
  node [
    id 163
    label "kwa&#347;ny"
  ]
  node [
    id 164
    label "kwaskowy"
  ]
  node [
    id 165
    label "&#347;wie&#380;y"
  ]
  node [
    id 166
    label "ro&#347;linny"
  ]
  node [
    id 167
    label "u&#380;ytkowy"
  ]
  node [
    id 168
    label "przypominaj&#261;cy"
  ]
  node [
    id 169
    label "s&#322;odki"
  ]
  node [
    id 170
    label "owocowo"
  ]
  node [
    id 171
    label "kwaskowato"
  ]
  node [
    id 172
    label "&#347;wie&#380;o"
  ]
  node [
    id 173
    label "czyn"
  ]
  node [
    id 174
    label "apostasy"
  ]
  node [
    id 175
    label "postawa"
  ]
  node [
    id 176
    label "withdrawal"
  ]
  node [
    id 177
    label "activity"
  ]
  node [
    id 178
    label "bezproblemowy"
  ]
  node [
    id 179
    label "stan"
  ]
  node [
    id 180
    label "nastawienie"
  ]
  node [
    id 181
    label "pozycja"
  ]
  node [
    id 182
    label "attitude"
  ]
  node [
    id 183
    label "funkcja"
  ]
  node [
    id 184
    label "act"
  ]
  node [
    id 185
    label "mini&#281;cie"
  ]
  node [
    id 186
    label "ustawa"
  ]
  node [
    id 187
    label "wymienienie"
  ]
  node [
    id 188
    label "zaliczenie"
  ]
  node [
    id 189
    label "traversal"
  ]
  node [
    id 190
    label "przewy&#380;szenie"
  ]
  node [
    id 191
    label "experience"
  ]
  node [
    id 192
    label "przepuszczenie"
  ]
  node [
    id 193
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 194
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 195
    label "strain"
  ]
  node [
    id 196
    label "faza"
  ]
  node [
    id 197
    label "przerobienie"
  ]
  node [
    id 198
    label "wydeptywanie"
  ]
  node [
    id 199
    label "crack"
  ]
  node [
    id 200
    label "wydeptanie"
  ]
  node [
    id 201
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 202
    label "wstawka"
  ]
  node [
    id 203
    label "prze&#380;ycie"
  ]
  node [
    id 204
    label "uznanie"
  ]
  node [
    id 205
    label "doznanie"
  ]
  node [
    id 206
    label "trwanie"
  ]
  node [
    id 207
    label "przebycie"
  ]
  node [
    id 208
    label "wytyczenie"
  ]
  node [
    id 209
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 210
    label "przepojenie"
  ]
  node [
    id 211
    label "nas&#261;czenie"
  ]
  node [
    id 212
    label "nale&#380;enie"
  ]
  node [
    id 213
    label "mienie"
  ]
  node [
    id 214
    label "odmienienie"
  ]
  node [
    id 215
    label "przedostanie_si&#281;"
  ]
  node [
    id 216
    label "przemokni&#281;cie"
  ]
  node [
    id 217
    label "nasycenie_si&#281;"
  ]
  node [
    id 218
    label "offense"
  ]
  node [
    id 219
    label "przestanie"
  ]
  node [
    id 220
    label "discourtesy"
  ]
  node [
    id 221
    label "odj&#281;cie"
  ]
  node [
    id 222
    label "post&#261;pienie"
  ]
  node [
    id 223
    label "opening"
  ]
  node [
    id 224
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 225
    label "wyra&#380;enie"
  ]
  node [
    id 226
    label "zrobienie"
  ]
  node [
    id 227
    label "conversion"
  ]
  node [
    id 228
    label "podanie"
  ]
  node [
    id 229
    label "exchange"
  ]
  node [
    id 230
    label "spisanie"
  ]
  node [
    id 231
    label "zape&#322;nienie"
  ]
  node [
    id 232
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 233
    label "skontaktowanie_si&#281;"
  ]
  node [
    id 234
    label "policzenie"
  ]
  node [
    id 235
    label "substytuowanie"
  ]
  node [
    id 236
    label "odbycie"
  ]
  node [
    id 237
    label "theodolite"
  ]
  node [
    id 238
    label "wy&#347;wiadczenie"
  ]
  node [
    id 239
    label "zmys&#322;"
  ]
  node [
    id 240
    label "przeczulica"
  ]
  node [
    id 241
    label "czucie"
  ]
  node [
    id 242
    label "poczucie"
  ]
  node [
    id 243
    label "oduczenie"
  ]
  node [
    id 244
    label "disavowal"
  ]
  node [
    id 245
    label "zako&#324;czenie"
  ]
  node [
    id 246
    label "cessation"
  ]
  node [
    id 247
    label "przeczekanie"
  ]
  node [
    id 248
    label "spe&#322;nienie"
  ]
  node [
    id 249
    label "wliczenie"
  ]
  node [
    id 250
    label "zaliczanie"
  ]
  node [
    id 251
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 252
    label "zadanie"
  ]
  node [
    id 253
    label "odb&#281;bnienie"
  ]
  node [
    id 254
    label "ocenienie"
  ]
  node [
    id 255
    label "number"
  ]
  node [
    id 256
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 257
    label "przeklasyfikowanie"
  ]
  node [
    id 258
    label "zaliczanie_si&#281;"
  ]
  node [
    id 259
    label "warunek_lokalowy"
  ]
  node [
    id 260
    label "plac"
  ]
  node [
    id 261
    label "location"
  ]
  node [
    id 262
    label "uwaga"
  ]
  node [
    id 263
    label "status"
  ]
  node [
    id 264
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 265
    label "chwila"
  ]
  node [
    id 266
    label "cia&#322;o"
  ]
  node [
    id 267
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 268
    label "rz&#261;d"
  ]
  node [
    id 269
    label "dodatek"
  ]
  node [
    id 270
    label "tekst"
  ]
  node [
    id 271
    label "cykl_astronomiczny"
  ]
  node [
    id 272
    label "coil"
  ]
  node [
    id 273
    label "zjawisko"
  ]
  node [
    id 274
    label "fotoelement"
  ]
  node [
    id 275
    label "komutowanie"
  ]
  node [
    id 276
    label "stan_skupienia"
  ]
  node [
    id 277
    label "nastr&#243;j"
  ]
  node [
    id 278
    label "przerywacz"
  ]
  node [
    id 279
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 280
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 281
    label "kraw&#281;d&#378;"
  ]
  node [
    id 282
    label "obsesja"
  ]
  node [
    id 283
    label "dw&#243;jnik"
  ]
  node [
    id 284
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 285
    label "okres"
  ]
  node [
    id 286
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 287
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 288
    label "przew&#243;d"
  ]
  node [
    id 289
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 290
    label "czas"
  ]
  node [
    id 291
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 292
    label "obw&#243;d"
  ]
  node [
    id 293
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 294
    label "degree"
  ]
  node [
    id 295
    label "komutowa&#263;"
  ]
  node [
    id 296
    label "wra&#380;enie"
  ]
  node [
    id 297
    label "poradzenie_sobie"
  ]
  node [
    id 298
    label "przetrwanie"
  ]
  node [
    id 299
    label "survival"
  ]
  node [
    id 300
    label "battery"
  ]
  node [
    id 301
    label "wygranie"
  ]
  node [
    id 302
    label "lepszy"
  ]
  node [
    id 303
    label "breakdown"
  ]
  node [
    id 304
    label "gap"
  ]
  node [
    id 305
    label "kokaina"
  ]
  node [
    id 306
    label "program"
  ]
  node [
    id 307
    label "po&#322;o&#380;enie"
  ]
  node [
    id 308
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 309
    label "rodowo&#347;&#263;"
  ]
  node [
    id 310
    label "patent"
  ]
  node [
    id 311
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 312
    label "dobra"
  ]
  node [
    id 313
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 314
    label "przej&#347;&#263;"
  ]
  node [
    id 315
    label "possession"
  ]
  node [
    id 316
    label "organizacyjnie"
  ]
  node [
    id 317
    label "zwi&#261;zek"
  ]
  node [
    id 318
    label "przyjmowanie"
  ]
  node [
    id 319
    label "przechodzenie"
  ]
  node [
    id 320
    label "puszczenie"
  ]
  node [
    id 321
    label "przeoczenie"
  ]
  node [
    id 322
    label "obrobienie"
  ]
  node [
    id 323
    label "oddzia&#322;anie"
  ]
  node [
    id 324
    label "spowodowanie"
  ]
  node [
    id 325
    label "ust&#261;pienie"
  ]
  node [
    id 326
    label "darowanie"
  ]
  node [
    id 327
    label "roztrwonienie"
  ]
  node [
    id 328
    label "zaistnienie"
  ]
  node [
    id 329
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 330
    label "cruise"
  ]
  node [
    id 331
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 332
    label "przep&#322;ywanie"
  ]
  node [
    id 333
    label "wlanie"
  ]
  node [
    id 334
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 335
    label "nasycenie"
  ]
  node [
    id 336
    label "przesycenie"
  ]
  node [
    id 337
    label "saturation"
  ]
  node [
    id 338
    label "impregnation"
  ]
  node [
    id 339
    label "opanowanie"
  ]
  node [
    id 340
    label "zmoczenie"
  ]
  node [
    id 341
    label "nadanie"
  ]
  node [
    id 342
    label "mokry"
  ]
  node [
    id 343
    label "zmokni&#281;cie"
  ]
  node [
    id 344
    label "bycie"
  ]
  node [
    id 345
    label "upieranie_si&#281;"
  ]
  node [
    id 346
    label "pozostawanie"
  ]
  node [
    id 347
    label "presence"
  ]
  node [
    id 348
    label "imperativeness"
  ]
  node [
    id 349
    label "standing"
  ]
  node [
    id 350
    label "zostawanie"
  ]
  node [
    id 351
    label "wytkni&#281;cie"
  ]
  node [
    id 352
    label "ustalenie"
  ]
  node [
    id 353
    label "wyznaczenie"
  ]
  node [
    id 354
    label "trace"
  ]
  node [
    id 355
    label "przeprowadzenie"
  ]
  node [
    id 356
    label "niszczenie"
  ]
  node [
    id 357
    label "kszta&#322;towanie"
  ]
  node [
    id 358
    label "pozyskiwanie"
  ]
  node [
    id 359
    label "zniszczenie"
  ]
  node [
    id 360
    label "egress"
  ]
  node [
    id 361
    label "ukszta&#322;towanie"
  ]
  node [
    id 362
    label "skombinowanie"
  ]
  node [
    id 363
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 364
    label "change"
  ]
  node [
    id 365
    label "reengineering"
  ]
  node [
    id 366
    label "zmienienie"
  ]
  node [
    id 367
    label "przeformu&#322;owanie"
  ]
  node [
    id 368
    label "przeformu&#322;owywanie"
  ]
  node [
    id 369
    label "Karta_Nauczyciela"
  ]
  node [
    id 370
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 371
    label "akt"
  ]
  node [
    id 372
    label "charter"
  ]
  node [
    id 373
    label "marc&#243;wka"
  ]
  node [
    id 374
    label "zaimponowanie"
  ]
  node [
    id 375
    label "honorowanie"
  ]
  node [
    id 376
    label "uszanowanie"
  ]
  node [
    id 377
    label "uhonorowa&#263;"
  ]
  node [
    id 378
    label "oznajmienie"
  ]
  node [
    id 379
    label "imponowanie"
  ]
  node [
    id 380
    label "uhonorowanie"
  ]
  node [
    id 381
    label "honorowa&#263;"
  ]
  node [
    id 382
    label "uszanowa&#263;"
  ]
  node [
    id 383
    label "mniemanie"
  ]
  node [
    id 384
    label "szacuneczek"
  ]
  node [
    id 385
    label "recognition"
  ]
  node [
    id 386
    label "rewerencja"
  ]
  node [
    id 387
    label "szanowa&#263;"
  ]
  node [
    id 388
    label "acclaim"
  ]
  node [
    id 389
    label "zachwyt"
  ]
  node [
    id 390
    label "respect"
  ]
  node [
    id 391
    label "fame"
  ]
  node [
    id 392
    label "rework"
  ]
  node [
    id 393
    label "zg&#322;&#281;bienie"
  ]
  node [
    id 394
    label "cz&#322;owiek"
  ]
  node [
    id 395
    label "pieszo"
  ]
  node [
    id 396
    label "chodnik"
  ]
  node [
    id 397
    label "piechotny"
  ]
  node [
    id 398
    label "w&#281;drowiec"
  ]
  node [
    id 399
    label "specjalny"
  ]
  node [
    id 400
    label "intencjonalny"
  ]
  node [
    id 401
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 402
    label "niedorozw&#243;j"
  ]
  node [
    id 403
    label "szczeg&#243;lny"
  ]
  node [
    id 404
    label "specjalnie"
  ]
  node [
    id 405
    label "nieetatowy"
  ]
  node [
    id 406
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 407
    label "nienormalny"
  ]
  node [
    id 408
    label "umy&#347;lnie"
  ]
  node [
    id 409
    label "odpowiedni"
  ]
  node [
    id 410
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 411
    label "ludzko&#347;&#263;"
  ]
  node [
    id 412
    label "asymilowanie"
  ]
  node [
    id 413
    label "wapniak"
  ]
  node [
    id 414
    label "asymilowa&#263;"
  ]
  node [
    id 415
    label "os&#322;abia&#263;"
  ]
  node [
    id 416
    label "posta&#263;"
  ]
  node [
    id 417
    label "hominid"
  ]
  node [
    id 418
    label "podw&#322;adny"
  ]
  node [
    id 419
    label "os&#322;abianie"
  ]
  node [
    id 420
    label "g&#322;owa"
  ]
  node [
    id 421
    label "portrecista"
  ]
  node [
    id 422
    label "dwun&#243;g"
  ]
  node [
    id 423
    label "profanum"
  ]
  node [
    id 424
    label "mikrokosmos"
  ]
  node [
    id 425
    label "nasada"
  ]
  node [
    id 426
    label "duch"
  ]
  node [
    id 427
    label "antropochoria"
  ]
  node [
    id 428
    label "osoba"
  ]
  node [
    id 429
    label "wz&#243;r"
  ]
  node [
    id 430
    label "senior"
  ]
  node [
    id 431
    label "oddzia&#322;ywanie"
  ]
  node [
    id 432
    label "Adam"
  ]
  node [
    id 433
    label "homo_sapiens"
  ]
  node [
    id 434
    label "polifag"
  ]
  node [
    id 435
    label "na_pieszo"
  ]
  node [
    id 436
    label "z_buta"
  ]
  node [
    id 437
    label "na_pieszk&#281;"
  ]
  node [
    id 438
    label "kraw&#281;&#380;nik"
  ]
  node [
    id 439
    label "chody"
  ]
  node [
    id 440
    label "sztreka"
  ]
  node [
    id 441
    label "kostka_brukowa"
  ]
  node [
    id 442
    label "drzewo"
  ]
  node [
    id 443
    label "wyrobisko"
  ]
  node [
    id 444
    label "kornik"
  ]
  node [
    id 445
    label "dywanik"
  ]
  node [
    id 446
    label "ulica"
  ]
  node [
    id 447
    label "przodek"
  ]
  node [
    id 448
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 449
    label "podr&#243;&#380;nik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
]
