graph [
  node [
    id 0
    label "odk&#261;d"
    origin "text"
  ]
  node [
    id 1
    label "og&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "robert"
    origin "text"
  ]
  node [
    id 3
    label "kubica"
    origin "text"
  ]
  node [
    id 4
    label "wraca&#263;"
    origin "text"
  ]
  node [
    id 5
    label "formu&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "wszyscy"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "historia"
    origin "text"
  ]
  node [
    id 9
    label "sportowiec"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "&#347;wietny"
    origin "text"
  ]
  node [
    id 12
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 13
    label "stworzenie"
    origin "text"
  ]
  node [
    id 14
    label "film"
    origin "text"
  ]
  node [
    id 15
    label "declare"
  ]
  node [
    id 16
    label "poda&#263;"
  ]
  node [
    id 17
    label "opublikowa&#263;"
  ]
  node [
    id 18
    label "communicate"
  ]
  node [
    id 19
    label "obwo&#322;a&#263;"
  ]
  node [
    id 20
    label "publish"
  ]
  node [
    id 21
    label "wprowadzi&#263;"
  ]
  node [
    id 22
    label "wydawnictwo"
  ]
  node [
    id 23
    label "upubliczni&#263;"
  ]
  node [
    id 24
    label "picture"
  ]
  node [
    id 25
    label "siatk&#243;wka"
  ]
  node [
    id 26
    label "nafaszerowa&#263;"
  ]
  node [
    id 27
    label "supply"
  ]
  node [
    id 28
    label "tenis"
  ]
  node [
    id 29
    label "jedzenie"
  ]
  node [
    id 30
    label "ustawi&#263;"
  ]
  node [
    id 31
    label "poinformowa&#263;"
  ]
  node [
    id 32
    label "give"
  ]
  node [
    id 33
    label "zagra&#263;"
  ]
  node [
    id 34
    label "da&#263;"
  ]
  node [
    id 35
    label "zaserwowa&#263;"
  ]
  node [
    id 36
    label "introduce"
  ]
  node [
    id 37
    label "okre&#347;li&#263;"
  ]
  node [
    id 38
    label "zakomunikowa&#263;"
  ]
  node [
    id 39
    label "nawi&#261;zywa&#263;"
  ]
  node [
    id 40
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 41
    label "zaczyna&#263;"
  ]
  node [
    id 42
    label "tax_return"
  ]
  node [
    id 43
    label "zostawa&#263;"
  ]
  node [
    id 44
    label "powodowa&#263;"
  ]
  node [
    id 45
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 46
    label "przychodzi&#263;"
  ]
  node [
    id 47
    label "przybywa&#263;"
  ]
  node [
    id 48
    label "return"
  ]
  node [
    id 49
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 50
    label "recur"
  ]
  node [
    id 51
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 52
    label "bankrupt"
  ]
  node [
    id 53
    label "open"
  ]
  node [
    id 54
    label "post&#281;powa&#263;"
  ]
  node [
    id 55
    label "odejmowa&#263;"
  ]
  node [
    id 56
    label "set_about"
  ]
  node [
    id 57
    label "begin"
  ]
  node [
    id 58
    label "mie&#263;_miejsce"
  ]
  node [
    id 59
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 60
    label "blend"
  ]
  node [
    id 61
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 62
    label "pozostawa&#263;"
  ]
  node [
    id 63
    label "przebywa&#263;"
  ]
  node [
    id 64
    label "change"
  ]
  node [
    id 65
    label "stop"
  ]
  node [
    id 66
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 67
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 68
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 69
    label "dochodzi&#263;"
  ]
  node [
    id 70
    label "czerpa&#263;"
  ]
  node [
    id 71
    label "bind"
  ]
  node [
    id 72
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 73
    label "motywowa&#263;"
  ]
  node [
    id 74
    label "act"
  ]
  node [
    id 75
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 76
    label "dociera&#263;"
  ]
  node [
    id 77
    label "get"
  ]
  node [
    id 78
    label "zyskiwa&#263;"
  ]
  node [
    id 79
    label "formularz"
  ]
  node [
    id 80
    label "kultura_duchowa"
  ]
  node [
    id 81
    label "sformu&#322;owanie"
  ]
  node [
    id 82
    label "zapis"
  ]
  node [
    id 83
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 84
    label "kultura"
  ]
  node [
    id 85
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 86
    label "rule"
  ]
  node [
    id 87
    label "ceremony"
  ]
  node [
    id 88
    label "tradycja"
  ]
  node [
    id 89
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 90
    label "Wsch&#243;d"
  ]
  node [
    id 91
    label "kuchnia"
  ]
  node [
    id 92
    label "jako&#347;&#263;"
  ]
  node [
    id 93
    label "sztuka"
  ]
  node [
    id 94
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 95
    label "praca_rolnicza"
  ]
  node [
    id 96
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 97
    label "makrokosmos"
  ]
  node [
    id 98
    label "przej&#281;cie"
  ]
  node [
    id 99
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 100
    label "przej&#261;&#263;"
  ]
  node [
    id 101
    label "przedmiot"
  ]
  node [
    id 102
    label "populace"
  ]
  node [
    id 103
    label "przejmowa&#263;"
  ]
  node [
    id 104
    label "hodowla"
  ]
  node [
    id 105
    label "religia"
  ]
  node [
    id 106
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 107
    label "propriety"
  ]
  node [
    id 108
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 109
    label "zwyczaj"
  ]
  node [
    id 110
    label "cecha"
  ]
  node [
    id 111
    label "zjawisko"
  ]
  node [
    id 112
    label "brzoskwiniarnia"
  ]
  node [
    id 113
    label "asymilowanie_si&#281;"
  ]
  node [
    id 114
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 115
    label "rzecz"
  ]
  node [
    id 116
    label "konwencja"
  ]
  node [
    id 117
    label "przejmowanie"
  ]
  node [
    id 118
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 119
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 120
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 121
    label "zapisanie"
  ]
  node [
    id 122
    label "poinformowanie"
  ]
  node [
    id 123
    label "wypowied&#378;"
  ]
  node [
    id 124
    label "wording"
  ]
  node [
    id 125
    label "rzucenie"
  ]
  node [
    id 126
    label "statement"
  ]
  node [
    id 127
    label "spos&#243;b"
  ]
  node [
    id 128
    label "normalizacja"
  ]
  node [
    id 129
    label "entrance"
  ]
  node [
    id 130
    label "wytw&#243;r"
  ]
  node [
    id 131
    label "wpis"
  ]
  node [
    id 132
    label "czynno&#347;&#263;"
  ]
  node [
    id 133
    label "dokument"
  ]
  node [
    id 134
    label "zestaw"
  ]
  node [
    id 135
    label "dysfonia"
  ]
  node [
    id 136
    label "prawi&#263;"
  ]
  node [
    id 137
    label "remark"
  ]
  node [
    id 138
    label "express"
  ]
  node [
    id 139
    label "chew_the_fat"
  ]
  node [
    id 140
    label "talk"
  ]
  node [
    id 141
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 142
    label "say"
  ]
  node [
    id 143
    label "wyra&#380;a&#263;"
  ]
  node [
    id 144
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 145
    label "j&#281;zyk"
  ]
  node [
    id 146
    label "tell"
  ]
  node [
    id 147
    label "informowa&#263;"
  ]
  node [
    id 148
    label "rozmawia&#263;"
  ]
  node [
    id 149
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 150
    label "powiada&#263;"
  ]
  node [
    id 151
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 152
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 153
    label "okre&#347;la&#263;"
  ]
  node [
    id 154
    label "u&#380;ywa&#263;"
  ]
  node [
    id 155
    label "gaworzy&#263;"
  ]
  node [
    id 156
    label "formu&#322;owa&#263;"
  ]
  node [
    id 157
    label "dziama&#263;"
  ]
  node [
    id 158
    label "umie&#263;"
  ]
  node [
    id 159
    label "wydobywa&#263;"
  ]
  node [
    id 160
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 161
    label "korzysta&#263;"
  ]
  node [
    id 162
    label "doznawa&#263;"
  ]
  node [
    id 163
    label "distribute"
  ]
  node [
    id 164
    label "bash"
  ]
  node [
    id 165
    label "style"
  ]
  node [
    id 166
    label "decydowa&#263;"
  ]
  node [
    id 167
    label "signify"
  ]
  node [
    id 168
    label "komunikowa&#263;"
  ]
  node [
    id 169
    label "inform"
  ]
  node [
    id 170
    label "oznacza&#263;"
  ]
  node [
    id 171
    label "znaczy&#263;"
  ]
  node [
    id 172
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 173
    label "convey"
  ]
  node [
    id 174
    label "arouse"
  ]
  node [
    id 175
    label "represent"
  ]
  node [
    id 176
    label "give_voice"
  ]
  node [
    id 177
    label "robi&#263;"
  ]
  node [
    id 178
    label "reakcja_chemiczna"
  ]
  node [
    id 179
    label "determine"
  ]
  node [
    id 180
    label "work"
  ]
  node [
    id 181
    label "wyjmowa&#263;"
  ]
  node [
    id 182
    label "wydostawa&#263;"
  ]
  node [
    id 183
    label "wydawa&#263;"
  ]
  node [
    id 184
    label "g&#243;rnictwo"
  ]
  node [
    id 185
    label "dobywa&#263;"
  ]
  node [
    id 186
    label "uwydatnia&#263;"
  ]
  node [
    id 187
    label "eksploatowa&#263;"
  ]
  node [
    id 188
    label "excavate"
  ]
  node [
    id 189
    label "raise"
  ]
  node [
    id 190
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 191
    label "train"
  ]
  node [
    id 192
    label "ocala&#263;"
  ]
  node [
    id 193
    label "uzyskiwa&#263;"
  ]
  node [
    id 194
    label "m&#243;c"
  ]
  node [
    id 195
    label "can"
  ]
  node [
    id 196
    label "wiedzie&#263;"
  ]
  node [
    id 197
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 198
    label "szczeka&#263;"
  ]
  node [
    id 199
    label "rozumie&#263;"
  ]
  node [
    id 200
    label "funkcjonowa&#263;"
  ]
  node [
    id 201
    label "mawia&#263;"
  ]
  node [
    id 202
    label "opowiada&#263;"
  ]
  node [
    id 203
    label "chatter"
  ]
  node [
    id 204
    label "niemowl&#281;"
  ]
  node [
    id 205
    label "kosmetyk"
  ]
  node [
    id 206
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 207
    label "stanowisko_archeologiczne"
  ]
  node [
    id 208
    label "pisa&#263;"
  ]
  node [
    id 209
    label "kod"
  ]
  node [
    id 210
    label "pype&#263;"
  ]
  node [
    id 211
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 212
    label "gramatyka"
  ]
  node [
    id 213
    label "language"
  ]
  node [
    id 214
    label "fonetyka"
  ]
  node [
    id 215
    label "t&#322;umaczenie"
  ]
  node [
    id 216
    label "artykulator"
  ]
  node [
    id 217
    label "rozumienie"
  ]
  node [
    id 218
    label "jama_ustna"
  ]
  node [
    id 219
    label "urz&#261;dzenie"
  ]
  node [
    id 220
    label "organ"
  ]
  node [
    id 221
    label "ssanie"
  ]
  node [
    id 222
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 223
    label "lizanie"
  ]
  node [
    id 224
    label "liza&#263;"
  ]
  node [
    id 225
    label "makroglosja"
  ]
  node [
    id 226
    label "natural_language"
  ]
  node [
    id 227
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 228
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 229
    label "napisa&#263;"
  ]
  node [
    id 230
    label "m&#243;wienie"
  ]
  node [
    id 231
    label "s&#322;ownictwo"
  ]
  node [
    id 232
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 233
    label "konsonantyzm"
  ]
  node [
    id 234
    label "ssa&#263;"
  ]
  node [
    id 235
    label "wokalizm"
  ]
  node [
    id 236
    label "formalizowanie"
  ]
  node [
    id 237
    label "jeniec"
  ]
  node [
    id 238
    label "kawa&#322;ek"
  ]
  node [
    id 239
    label "po_koroniarsku"
  ]
  node [
    id 240
    label "stylik"
  ]
  node [
    id 241
    label "przet&#322;umaczenie"
  ]
  node [
    id 242
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 243
    label "formacja_geologiczna"
  ]
  node [
    id 244
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 245
    label "but"
  ]
  node [
    id 246
    label "pismo"
  ]
  node [
    id 247
    label "formalizowa&#263;"
  ]
  node [
    id 248
    label "dysleksja"
  ]
  node [
    id 249
    label "dysphonia"
  ]
  node [
    id 250
    label "bizantynistyka"
  ]
  node [
    id 251
    label "genealogia"
  ]
  node [
    id 252
    label "epoka"
  ]
  node [
    id 253
    label "historia_gospodarcza"
  ]
  node [
    id 254
    label "paleografia"
  ]
  node [
    id 255
    label "numizmatyka"
  ]
  node [
    id 256
    label "annalistyka"
  ]
  node [
    id 257
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 258
    label "historyka"
  ]
  node [
    id 259
    label "muzealnictwo"
  ]
  node [
    id 260
    label "nautologia"
  ]
  node [
    id 261
    label "filigranistyka"
  ]
  node [
    id 262
    label "hista"
  ]
  node [
    id 263
    label "weksylologia"
  ]
  node [
    id 264
    label "historiografia"
  ]
  node [
    id 265
    label "charakter"
  ]
  node [
    id 266
    label "chronologia"
  ]
  node [
    id 267
    label "historia_sztuki"
  ]
  node [
    id 268
    label "przebiec"
  ]
  node [
    id 269
    label "przebiegni&#281;cie"
  ]
  node [
    id 270
    label "ikonografia"
  ]
  node [
    id 271
    label "prezentyzm"
  ]
  node [
    id 272
    label "mediewistyka"
  ]
  node [
    id 273
    label "dyplomatyka"
  ]
  node [
    id 274
    label "neografia"
  ]
  node [
    id 275
    label "ruralistyka"
  ]
  node [
    id 276
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 277
    label "prozopografia"
  ]
  node [
    id 278
    label "motyw"
  ]
  node [
    id 279
    label "epigrafika"
  ]
  node [
    id 280
    label "sfragistyka"
  ]
  node [
    id 281
    label "papirologia"
  ]
  node [
    id 282
    label "nauka_humanistyczna"
  ]
  node [
    id 283
    label "report"
  ]
  node [
    id 284
    label "zabytkoznawstwo"
  ]
  node [
    id 285
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 286
    label "archiwistyka"
  ]
  node [
    id 287
    label "heraldyka"
  ]
  node [
    id 288
    label "varsavianistyka"
  ]
  node [
    id 289
    label "kierunek"
  ]
  node [
    id 290
    label "fabu&#322;a"
  ]
  node [
    id 291
    label "czas"
  ]
  node [
    id 292
    label "&#380;ycie"
  ]
  node [
    id 293
    label "koleje_losu"
  ]
  node [
    id 294
    label "discipline"
  ]
  node [
    id 295
    label "zboczy&#263;"
  ]
  node [
    id 296
    label "w&#261;tek"
  ]
  node [
    id 297
    label "entity"
  ]
  node [
    id 298
    label "sponiewiera&#263;"
  ]
  node [
    id 299
    label "zboczenie"
  ]
  node [
    id 300
    label "zbaczanie"
  ]
  node [
    id 301
    label "thing"
  ]
  node [
    id 302
    label "om&#243;wi&#263;"
  ]
  node [
    id 303
    label "tre&#347;&#263;"
  ]
  node [
    id 304
    label "element"
  ]
  node [
    id 305
    label "kr&#261;&#380;enie"
  ]
  node [
    id 306
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 307
    label "istota"
  ]
  node [
    id 308
    label "zbacza&#263;"
  ]
  node [
    id 309
    label "om&#243;wienie"
  ]
  node [
    id 310
    label "tematyka"
  ]
  node [
    id 311
    label "omawianie"
  ]
  node [
    id 312
    label "omawia&#263;"
  ]
  node [
    id 313
    label "robienie"
  ]
  node [
    id 314
    label "program_nauczania"
  ]
  node [
    id 315
    label "sponiewieranie"
  ]
  node [
    id 316
    label "parafrazowanie"
  ]
  node [
    id 317
    label "komunikat"
  ]
  node [
    id 318
    label "stylizacja"
  ]
  node [
    id 319
    label "sparafrazowanie"
  ]
  node [
    id 320
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 321
    label "strawestowanie"
  ]
  node [
    id 322
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 323
    label "pos&#322;uchanie"
  ]
  node [
    id 324
    label "strawestowa&#263;"
  ]
  node [
    id 325
    label "parafrazowa&#263;"
  ]
  node [
    id 326
    label "delimitacja"
  ]
  node [
    id 327
    label "rezultat"
  ]
  node [
    id 328
    label "ozdobnik"
  ]
  node [
    id 329
    label "sparafrazowa&#263;"
  ]
  node [
    id 330
    label "s&#261;d"
  ]
  node [
    id 331
    label "trawestowa&#263;"
  ]
  node [
    id 332
    label "trawestowanie"
  ]
  node [
    id 333
    label "linia"
  ]
  node [
    id 334
    label "przebieg"
  ]
  node [
    id 335
    label "zorientowa&#263;"
  ]
  node [
    id 336
    label "orientowa&#263;"
  ]
  node [
    id 337
    label "praktyka"
  ]
  node [
    id 338
    label "skr&#281;cenie"
  ]
  node [
    id 339
    label "skr&#281;ci&#263;"
  ]
  node [
    id 340
    label "przeorientowanie"
  ]
  node [
    id 341
    label "g&#243;ra"
  ]
  node [
    id 342
    label "orientowanie"
  ]
  node [
    id 343
    label "zorientowanie"
  ]
  node [
    id 344
    label "ty&#322;"
  ]
  node [
    id 345
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 346
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 347
    label "przeorientowywanie"
  ]
  node [
    id 348
    label "bok"
  ]
  node [
    id 349
    label "ideologia"
  ]
  node [
    id 350
    label "skr&#281;canie"
  ]
  node [
    id 351
    label "orientacja"
  ]
  node [
    id 352
    label "metoda"
  ]
  node [
    id 353
    label "studia"
  ]
  node [
    id 354
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 355
    label "przeorientowa&#263;"
  ]
  node [
    id 356
    label "bearing"
  ]
  node [
    id 357
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 358
    label "prz&#243;d"
  ]
  node [
    id 359
    label "skr&#281;ca&#263;"
  ]
  node [
    id 360
    label "system"
  ]
  node [
    id 361
    label "przeorientowywa&#263;"
  ]
  node [
    id 362
    label "bajos"
  ]
  node [
    id 363
    label "kelowej"
  ]
  node [
    id 364
    label "paleocen"
  ]
  node [
    id 365
    label "&#347;rodkowy_trias"
  ]
  node [
    id 366
    label "miocen"
  ]
  node [
    id 367
    label "Zeitgeist"
  ]
  node [
    id 368
    label "schy&#322;ek"
  ]
  node [
    id 369
    label "plejstocen"
  ]
  node [
    id 370
    label "aalen"
  ]
  node [
    id 371
    label "jura_&#347;rodkowa"
  ]
  node [
    id 372
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 373
    label "jura_wczesna"
  ]
  node [
    id 374
    label "jednostka_geologiczna"
  ]
  node [
    id 375
    label "eocen"
  ]
  node [
    id 376
    label "term"
  ]
  node [
    id 377
    label "dzieje"
  ]
  node [
    id 378
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 379
    label "wczesny_trias"
  ]
  node [
    id 380
    label "okres"
  ]
  node [
    id 381
    label "holocen"
  ]
  node [
    id 382
    label "pliocen"
  ]
  node [
    id 383
    label "oligocen"
  ]
  node [
    id 384
    label "perypetia"
  ]
  node [
    id 385
    label "w&#281;ze&#322;"
  ]
  node [
    id 386
    label "opowiadanie"
  ]
  node [
    id 387
    label "Byzantine_Empire"
  ]
  node [
    id 388
    label "metodologia"
  ]
  node [
    id 389
    label "dendrochronologia"
  ]
  node [
    id 390
    label "kolejno&#347;&#263;"
  ]
  node [
    id 391
    label "datacja"
  ]
  node [
    id 392
    label "bibliologia"
  ]
  node [
    id 393
    label "brachygrafia"
  ]
  node [
    id 394
    label "architektura"
  ]
  node [
    id 395
    label "nauka"
  ]
  node [
    id 396
    label "museum"
  ]
  node [
    id 397
    label "archiwoznawstwo"
  ]
  node [
    id 398
    label "pi&#347;miennictwo"
  ]
  node [
    id 399
    label "historiography"
  ]
  node [
    id 400
    label "archeologia"
  ]
  node [
    id 401
    label "plastyka"
  ]
  node [
    id 402
    label "or&#281;&#380;"
  ]
  node [
    id 403
    label "s&#322;up"
  ]
  node [
    id 404
    label "pas"
  ]
  node [
    id 405
    label "barwa_heraldyczna"
  ]
  node [
    id 406
    label "herb"
  ]
  node [
    id 407
    label "oksza"
  ]
  node [
    id 408
    label "&#347;redniowiecze"
  ]
  node [
    id 409
    label "descendencja"
  ]
  node [
    id 410
    label "pochodzenie"
  ]
  node [
    id 411
    label "drzewo_genealogiczne"
  ]
  node [
    id 412
    label "procedencja"
  ]
  node [
    id 413
    label "numismatics"
  ]
  node [
    id 414
    label "kolekcjonerstwo"
  ]
  node [
    id 415
    label "medal"
  ]
  node [
    id 416
    label "fraza"
  ]
  node [
    id 417
    label "ozdoba"
  ]
  node [
    id 418
    label "przyczyna"
  ]
  node [
    id 419
    label "wydarzenie"
  ]
  node [
    id 420
    label "temat"
  ]
  node [
    id 421
    label "melodia"
  ]
  node [
    id 422
    label "sytuacja"
  ]
  node [
    id 423
    label "umowa"
  ]
  node [
    id 424
    label "cover"
  ]
  node [
    id 425
    label "proceed"
  ]
  node [
    id 426
    label "fly"
  ]
  node [
    id 427
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 428
    label "przeby&#263;"
  ]
  node [
    id 429
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 430
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 431
    label "przesun&#261;&#263;"
  ]
  node [
    id 432
    label "przemierzy&#263;"
  ]
  node [
    id 433
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 434
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 435
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 436
    label "run"
  ]
  node [
    id 437
    label "bezproblemowy"
  ]
  node [
    id 438
    label "activity"
  ]
  node [
    id 439
    label "cz&#322;owiek"
  ]
  node [
    id 440
    label "zbi&#243;r"
  ]
  node [
    id 441
    label "fizjonomia"
  ]
  node [
    id 442
    label "kompleksja"
  ]
  node [
    id 443
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 444
    label "psychika"
  ]
  node [
    id 445
    label "posta&#263;"
  ]
  node [
    id 446
    label "osobowo&#347;&#263;"
  ]
  node [
    id 447
    label "przebycie"
  ]
  node [
    id 448
    label "przemkni&#281;cie"
  ]
  node [
    id 449
    label "zdarzenie_si&#281;"
  ]
  node [
    id 450
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 451
    label "zabrzmienie"
  ]
  node [
    id 452
    label "zgrupowanie"
  ]
  node [
    id 453
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 454
    label "podzielenie"
  ]
  node [
    id 455
    label "sport"
  ]
  node [
    id 456
    label "spowodowanie"
  ]
  node [
    id 457
    label "jednostka"
  ]
  node [
    id 458
    label "set"
  ]
  node [
    id 459
    label "armia"
  ]
  node [
    id 460
    label "concentration"
  ]
  node [
    id 461
    label "trening"
  ]
  node [
    id 462
    label "ob&#243;z"
  ]
  node [
    id 463
    label "skupienie"
  ]
  node [
    id 464
    label "organizacja"
  ]
  node [
    id 465
    label "zesp&#243;&#322;"
  ]
  node [
    id 466
    label "concourse"
  ]
  node [
    id 467
    label "asymilowa&#263;"
  ]
  node [
    id 468
    label "nasada"
  ]
  node [
    id 469
    label "profanum"
  ]
  node [
    id 470
    label "wz&#243;r"
  ]
  node [
    id 471
    label "senior"
  ]
  node [
    id 472
    label "asymilowanie"
  ]
  node [
    id 473
    label "os&#322;abia&#263;"
  ]
  node [
    id 474
    label "homo_sapiens"
  ]
  node [
    id 475
    label "osoba"
  ]
  node [
    id 476
    label "ludzko&#347;&#263;"
  ]
  node [
    id 477
    label "Adam"
  ]
  node [
    id 478
    label "hominid"
  ]
  node [
    id 479
    label "portrecista"
  ]
  node [
    id 480
    label "polifag"
  ]
  node [
    id 481
    label "podw&#322;adny"
  ]
  node [
    id 482
    label "dwun&#243;g"
  ]
  node [
    id 483
    label "wapniak"
  ]
  node [
    id 484
    label "duch"
  ]
  node [
    id 485
    label "os&#322;abianie"
  ]
  node [
    id 486
    label "antropochoria"
  ]
  node [
    id 487
    label "figura"
  ]
  node [
    id 488
    label "g&#322;owa"
  ]
  node [
    id 489
    label "mikrokosmos"
  ]
  node [
    id 490
    label "oddzia&#322;ywanie"
  ]
  node [
    id 491
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 492
    label "stan"
  ]
  node [
    id 493
    label "stand"
  ]
  node [
    id 494
    label "trwa&#263;"
  ]
  node [
    id 495
    label "equal"
  ]
  node [
    id 496
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 497
    label "chodzi&#263;"
  ]
  node [
    id 498
    label "uczestniczy&#263;"
  ]
  node [
    id 499
    label "obecno&#347;&#263;"
  ]
  node [
    id 500
    label "si&#281;ga&#263;"
  ]
  node [
    id 501
    label "participate"
  ]
  node [
    id 502
    label "adhere"
  ]
  node [
    id 503
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 504
    label "istnie&#263;"
  ]
  node [
    id 505
    label "compass"
  ]
  node [
    id 506
    label "exsert"
  ]
  node [
    id 507
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 508
    label "osi&#261;ga&#263;"
  ]
  node [
    id 509
    label "appreciation"
  ]
  node [
    id 510
    label "mierzy&#263;"
  ]
  node [
    id 511
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 512
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 513
    label "being"
  ]
  node [
    id 514
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 515
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 516
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 517
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 518
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 519
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 520
    label "str&#243;j"
  ]
  node [
    id 521
    label "para"
  ]
  node [
    id 522
    label "krok"
  ]
  node [
    id 523
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 524
    label "przebiega&#263;"
  ]
  node [
    id 525
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 526
    label "continue"
  ]
  node [
    id 527
    label "carry"
  ]
  node [
    id 528
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 529
    label "wk&#322;ada&#263;"
  ]
  node [
    id 530
    label "p&#322;ywa&#263;"
  ]
  node [
    id 531
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 532
    label "bangla&#263;"
  ]
  node [
    id 533
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 534
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 535
    label "bywa&#263;"
  ]
  node [
    id 536
    label "tryb"
  ]
  node [
    id 537
    label "stara&#263;_si&#281;"
  ]
  node [
    id 538
    label "Arakan"
  ]
  node [
    id 539
    label "Teksas"
  ]
  node [
    id 540
    label "Georgia"
  ]
  node [
    id 541
    label "Maryland"
  ]
  node [
    id 542
    label "warstwa"
  ]
  node [
    id 543
    label "Michigan"
  ]
  node [
    id 544
    label "Massachusetts"
  ]
  node [
    id 545
    label "Luizjana"
  ]
  node [
    id 546
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 547
    label "samopoczucie"
  ]
  node [
    id 548
    label "Floryda"
  ]
  node [
    id 549
    label "Ohio"
  ]
  node [
    id 550
    label "Alaska"
  ]
  node [
    id 551
    label "Nowy_Meksyk"
  ]
  node [
    id 552
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 553
    label "wci&#281;cie"
  ]
  node [
    id 554
    label "Kansas"
  ]
  node [
    id 555
    label "Alabama"
  ]
  node [
    id 556
    label "miejsce"
  ]
  node [
    id 557
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 558
    label "Kalifornia"
  ]
  node [
    id 559
    label "Wirginia"
  ]
  node [
    id 560
    label "punkt"
  ]
  node [
    id 561
    label "Nowy_York"
  ]
  node [
    id 562
    label "Waszyngton"
  ]
  node [
    id 563
    label "Pensylwania"
  ]
  node [
    id 564
    label "wektor"
  ]
  node [
    id 565
    label "Hawaje"
  ]
  node [
    id 566
    label "state"
  ]
  node [
    id 567
    label "poziom"
  ]
  node [
    id 568
    label "jednostka_administracyjna"
  ]
  node [
    id 569
    label "Illinois"
  ]
  node [
    id 570
    label "Oklahoma"
  ]
  node [
    id 571
    label "Oregon"
  ]
  node [
    id 572
    label "Arizona"
  ]
  node [
    id 573
    label "ilo&#347;&#263;"
  ]
  node [
    id 574
    label "Jukatan"
  ]
  node [
    id 575
    label "shape"
  ]
  node [
    id 576
    label "Goa"
  ]
  node [
    id 577
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 578
    label "skuteczny"
  ]
  node [
    id 579
    label "wa&#380;ny"
  ]
  node [
    id 580
    label "zajebisty"
  ]
  node [
    id 581
    label "pozytywny"
  ]
  node [
    id 582
    label "spania&#322;y"
  ]
  node [
    id 583
    label "pomy&#347;lny"
  ]
  node [
    id 584
    label "arcydzielny"
  ]
  node [
    id 585
    label "dobry"
  ]
  node [
    id 586
    label "wspaniale"
  ]
  node [
    id 587
    label "superancki"
  ]
  node [
    id 588
    label "&#347;wietnie"
  ]
  node [
    id 589
    label "znaczny"
  ]
  node [
    id 590
    label "silny"
  ]
  node [
    id 591
    label "wa&#380;nie"
  ]
  node [
    id 592
    label "eksponowany"
  ]
  node [
    id 593
    label "wynios&#322;y"
  ]
  node [
    id 594
    label "istotnie"
  ]
  node [
    id 595
    label "dono&#347;ny"
  ]
  node [
    id 596
    label "ca&#322;y"
  ]
  node [
    id 597
    label "czw&#243;rka"
  ]
  node [
    id 598
    label "spokojny"
  ]
  node [
    id 599
    label "pos&#322;uszny"
  ]
  node [
    id 600
    label "korzystny"
  ]
  node [
    id 601
    label "drogi"
  ]
  node [
    id 602
    label "moralny"
  ]
  node [
    id 603
    label "powitanie"
  ]
  node [
    id 604
    label "grzeczny"
  ]
  node [
    id 605
    label "&#347;mieszny"
  ]
  node [
    id 606
    label "odpowiedni"
  ]
  node [
    id 607
    label "zwrot"
  ]
  node [
    id 608
    label "dobrze"
  ]
  node [
    id 609
    label "dobroczynny"
  ]
  node [
    id 610
    label "mi&#322;y"
  ]
  node [
    id 611
    label "taki"
  ]
  node [
    id 612
    label "stosownie"
  ]
  node [
    id 613
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 614
    label "prawdziwy"
  ]
  node [
    id 615
    label "typowy"
  ]
  node [
    id 616
    label "zasadniczy"
  ]
  node [
    id 617
    label "charakterystyczny"
  ]
  node [
    id 618
    label "uprawniony"
  ]
  node [
    id 619
    label "nale&#380;yty"
  ]
  node [
    id 620
    label "ten"
  ]
  node [
    id 621
    label "nale&#380;ny"
  ]
  node [
    id 622
    label "po&#380;&#261;dany"
  ]
  node [
    id 623
    label "pomy&#347;lnie"
  ]
  node [
    id 624
    label "sprawny"
  ]
  node [
    id 625
    label "skutkowanie"
  ]
  node [
    id 626
    label "poskutkowanie"
  ]
  node [
    id 627
    label "skutecznie"
  ]
  node [
    id 628
    label "pozytywnie"
  ]
  node [
    id 629
    label "fajny"
  ]
  node [
    id 630
    label "przyjemny"
  ]
  node [
    id 631
    label "dodatnio"
  ]
  node [
    id 632
    label "zajebi&#347;cie"
  ]
  node [
    id 633
    label "wspania&#322;y"
  ]
  node [
    id 634
    label "och&#281;do&#380;nie"
  ]
  node [
    id 635
    label "bogaty"
  ]
  node [
    id 636
    label "kapitalny"
  ]
  node [
    id 637
    label "superancko"
  ]
  node [
    id 638
    label "doskona&#322;y"
  ]
  node [
    id 639
    label "nieustraszony"
  ]
  node [
    id 640
    label "zadzier&#380;ysty"
  ]
  node [
    id 641
    label "tworzywo"
  ]
  node [
    id 642
    label "substancja"
  ]
  node [
    id 643
    label "materia"
  ]
  node [
    id 644
    label "bielarnia"
  ]
  node [
    id 645
    label "dyspozycja"
  ]
  node [
    id 646
    label "archiwum"
  ]
  node [
    id 647
    label "krajalno&#347;&#263;"
  ]
  node [
    id 648
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 649
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 650
    label "kandydat"
  ]
  node [
    id 651
    label "krajka"
  ]
  node [
    id 652
    label "nawil&#380;arka"
  ]
  node [
    id 653
    label "dane"
  ]
  node [
    id 654
    label "jednostka_informacji"
  ]
  node [
    id 655
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 656
    label "pakowanie"
  ]
  node [
    id 657
    label "edytowanie"
  ]
  node [
    id 658
    label "sekwencjonowa&#263;"
  ]
  node [
    id 659
    label "rozpakowywanie"
  ]
  node [
    id 660
    label "rozpakowa&#263;"
  ]
  node [
    id 661
    label "nap&#322;ywanie"
  ]
  node [
    id 662
    label "spakowa&#263;"
  ]
  node [
    id 663
    label "edytowa&#263;"
  ]
  node [
    id 664
    label "evidence"
  ]
  node [
    id 665
    label "sekwencjonowanie"
  ]
  node [
    id 666
    label "rozpakowanie"
  ]
  node [
    id 667
    label "wyci&#261;ganie"
  ]
  node [
    id 668
    label "korelator"
  ]
  node [
    id 669
    label "rekord"
  ]
  node [
    id 670
    label "informacja"
  ]
  node [
    id 671
    label "spakowanie"
  ]
  node [
    id 672
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 673
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 674
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 675
    label "konwersja"
  ]
  node [
    id 676
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 677
    label "rozpakowywa&#263;"
  ]
  node [
    id 678
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 679
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 680
    label "pakowa&#263;"
  ]
  node [
    id 681
    label "temperatura_krytyczna"
  ]
  node [
    id 682
    label "smolisty"
  ]
  node [
    id 683
    label "byt"
  ]
  node [
    id 684
    label "przenika&#263;"
  ]
  node [
    id 685
    label "cz&#261;steczka"
  ]
  node [
    id 686
    label "przenikanie"
  ]
  node [
    id 687
    label "wst&#261;&#380;ka"
  ]
  node [
    id 688
    label "pasmanteria"
  ]
  node [
    id 689
    label "pasek"
  ]
  node [
    id 690
    label "bardko"
  ]
  node [
    id 691
    label "obszycie"
  ]
  node [
    id 692
    label "okrajka"
  ]
  node [
    id 693
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 694
    label "ropa"
  ]
  node [
    id 695
    label "szczeg&#243;&#322;"
  ]
  node [
    id 696
    label "maszyna_w&#322;&#243;kiennicza"
  ]
  node [
    id 697
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 698
    label "szk&#322;o"
  ]
  node [
    id 699
    label "blacha"
  ]
  node [
    id 700
    label "aspirowanie"
  ]
  node [
    id 701
    label "bierne_prawo_wyborcze"
  ]
  node [
    id 702
    label "lista_wyborcza"
  ]
  node [
    id 703
    label "kolekcja"
  ]
  node [
    id 704
    label "instytucja"
  ]
  node [
    id 705
    label "archive"
  ]
  node [
    id 706
    label "dokumentacja"
  ]
  node [
    id 707
    label "zdolno&#347;&#263;"
  ]
  node [
    id 708
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 709
    label "plan"
  ]
  node [
    id 710
    label "capability"
  ]
  node [
    id 711
    label "potencja&#322;"
  ]
  node [
    id 712
    label "kondycja"
  ]
  node [
    id 713
    label "prawo"
  ]
  node [
    id 714
    label "polecenie"
  ]
  node [
    id 715
    label "wizerunek"
  ]
  node [
    id 716
    label "powstanie"
  ]
  node [
    id 717
    label "organizm"
  ]
  node [
    id 718
    label "ekosystem"
  ]
  node [
    id 719
    label "wszechstworzenie"
  ]
  node [
    id 720
    label "fauna"
  ]
  node [
    id 721
    label "stw&#243;r"
  ]
  node [
    id 722
    label "pope&#322;nienie"
  ]
  node [
    id 723
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 724
    label "erecting"
  ]
  node [
    id 725
    label "teren"
  ]
  node [
    id 726
    label "Ziemia"
  ]
  node [
    id 727
    label "woda"
  ]
  node [
    id 728
    label "biota"
  ]
  node [
    id 729
    label "environment"
  ]
  node [
    id 730
    label "obiekt_naturalny"
  ]
  node [
    id 731
    label "zrobienie"
  ]
  node [
    id 732
    label "cia&#322;o"
  ]
  node [
    id 733
    label "potworzenie"
  ]
  node [
    id 734
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 735
    label "narobienie"
  ]
  node [
    id 736
    label "porobienie"
  ]
  node [
    id 737
    label "creation"
  ]
  node [
    id 738
    label "wyewoluowanie"
  ]
  node [
    id 739
    label "odwadnianie"
  ]
  node [
    id 740
    label "przyswojenie"
  ]
  node [
    id 741
    label "starzenie_si&#281;"
  ]
  node [
    id 742
    label "odwodni&#263;"
  ]
  node [
    id 743
    label "obiekt"
  ]
  node [
    id 744
    label "przyswajanie"
  ]
  node [
    id 745
    label "biorytm"
  ]
  node [
    id 746
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 747
    label "unerwienie"
  ]
  node [
    id 748
    label "istota_&#380;ywa"
  ]
  node [
    id 749
    label "uk&#322;ad"
  ]
  node [
    id 750
    label "sk&#243;ra"
  ]
  node [
    id 751
    label "ewoluowanie"
  ]
  node [
    id 752
    label "odwodnienie"
  ]
  node [
    id 753
    label "ewoluowa&#263;"
  ]
  node [
    id 754
    label "czynnik_biotyczny"
  ]
  node [
    id 755
    label "otwieranie"
  ]
  node [
    id 756
    label "cz&#322;onek"
  ]
  node [
    id 757
    label "przyswaja&#263;"
  ]
  node [
    id 758
    label "p&#322;aszczyzna"
  ]
  node [
    id 759
    label "reakcja"
  ]
  node [
    id 760
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 761
    label "otworzy&#263;"
  ]
  node [
    id 762
    label "odwadnia&#263;"
  ]
  node [
    id 763
    label "staw"
  ]
  node [
    id 764
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 765
    label "wyewoluowa&#263;"
  ]
  node [
    id 766
    label "szkielet"
  ]
  node [
    id 767
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 768
    label "przyswoi&#263;"
  ]
  node [
    id 769
    label "ow&#322;osienie"
  ]
  node [
    id 770
    label "otworzenie"
  ]
  node [
    id 771
    label "l&#281;d&#378;wie"
  ]
  node [
    id 772
    label "otwiera&#263;"
  ]
  node [
    id 773
    label "individual"
  ]
  node [
    id 774
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 775
    label "temperatura"
  ]
  node [
    id 776
    label "mentalno&#347;&#263;"
  ]
  node [
    id 777
    label "superego"
  ]
  node [
    id 778
    label "znaczenie"
  ]
  node [
    id 779
    label "wn&#281;trze"
  ]
  node [
    id 780
    label "committee"
  ]
  node [
    id 781
    label "potw&#243;r"
  ]
  node [
    id 782
    label "monster"
  ]
  node [
    id 783
    label "przyroda"
  ]
  node [
    id 784
    label "istota_fantastyczna"
  ]
  node [
    id 785
    label "niecz&#322;owiek"
  ]
  node [
    id 786
    label "smok_wawelski"
  ]
  node [
    id 787
    label "performance"
  ]
  node [
    id 788
    label "kl&#281;czenie"
  ]
  node [
    id 789
    label "powstanie_listopadowe"
  ]
  node [
    id 790
    label "potworzenie_si&#281;"
  ]
  node [
    id 791
    label "beginning"
  ]
  node [
    id 792
    label "powstanie_tambowskie"
  ]
  node [
    id 793
    label "origin"
  ]
  node [
    id 794
    label "siedzenie"
  ]
  node [
    id 795
    label "odbudowanie_si&#281;"
  ]
  node [
    id 796
    label "walka"
  ]
  node [
    id 797
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 798
    label "pierwocina"
  ]
  node [
    id 799
    label "chmielnicczyzna"
  ]
  node [
    id 800
    label "le&#380;enie"
  ]
  node [
    id 801
    label "&#380;akieria"
  ]
  node [
    id 802
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 803
    label "geneza"
  ]
  node [
    id 804
    label "utworzenie"
  ]
  node [
    id 805
    label "orgy"
  ]
  node [
    id 806
    label "zaistnienie"
  ]
  node [
    id 807
    label "powstanie_warszawskie"
  ]
  node [
    id 808
    label "koliszczyzna"
  ]
  node [
    id 809
    label "uniesienie_si&#281;"
  ]
  node [
    id 810
    label "Ko&#347;ciuszko"
  ]
  node [
    id 811
    label "wpada&#263;"
  ]
  node [
    id 812
    label "object"
  ]
  node [
    id 813
    label "wpa&#347;&#263;"
  ]
  node [
    id 814
    label "mienie"
  ]
  node [
    id 815
    label "wpadni&#281;cie"
  ]
  node [
    id 816
    label "wpadanie"
  ]
  node [
    id 817
    label "wykreowanie"
  ]
  node [
    id 818
    label "wykreowa&#263;"
  ]
  node [
    id 819
    label "wygl&#261;d"
  ]
  node [
    id 820
    label "kreowanie"
  ]
  node [
    id 821
    label "appearance"
  ]
  node [
    id 822
    label "kreacja"
  ]
  node [
    id 823
    label "kreowa&#263;"
  ]
  node [
    id 824
    label "awifauna"
  ]
  node [
    id 825
    label "ichtiofauna"
  ]
  node [
    id 826
    label "biom"
  ]
  node [
    id 827
    label "geosystem"
  ]
  node [
    id 828
    label "przybieranie"
  ]
  node [
    id 829
    label "pustka"
  ]
  node [
    id 830
    label "przybrze&#380;e"
  ]
  node [
    id 831
    label "woda_s&#322;odka"
  ]
  node [
    id 832
    label "utylizator"
  ]
  node [
    id 833
    label "spi&#281;trzenie"
  ]
  node [
    id 834
    label "wodnik"
  ]
  node [
    id 835
    label "water"
  ]
  node [
    id 836
    label "fala"
  ]
  node [
    id 837
    label "kryptodepresja"
  ]
  node [
    id 838
    label "klarownik"
  ]
  node [
    id 839
    label "tlenek"
  ]
  node [
    id 840
    label "l&#243;d"
  ]
  node [
    id 841
    label "nabranie"
  ]
  node [
    id 842
    label "chlastanie"
  ]
  node [
    id 843
    label "zrzut"
  ]
  node [
    id 844
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 845
    label "uci&#261;g"
  ]
  node [
    id 846
    label "nabra&#263;"
  ]
  node [
    id 847
    label "wybrze&#380;e"
  ]
  node [
    id 848
    label "p&#322;ycizna"
  ]
  node [
    id 849
    label "uj&#281;cie_wody"
  ]
  node [
    id 850
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 851
    label "ciecz"
  ]
  node [
    id 852
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 853
    label "Waruna"
  ]
  node [
    id 854
    label "chlasta&#263;"
  ]
  node [
    id 855
    label "bicie"
  ]
  node [
    id 856
    label "deklamacja"
  ]
  node [
    id 857
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 858
    label "spi&#281;trzanie"
  ]
  node [
    id 859
    label "spi&#281;trza&#263;"
  ]
  node [
    id 860
    label "wysi&#281;k"
  ]
  node [
    id 861
    label "dotleni&#263;"
  ]
  node [
    id 862
    label "pojazd"
  ]
  node [
    id 863
    label "nap&#243;j"
  ]
  node [
    id 864
    label "bombast"
  ]
  node [
    id 865
    label "miniatura"
  ]
  node [
    id 866
    label "odbicie"
  ]
  node [
    id 867
    label "atom"
  ]
  node [
    id 868
    label "kosmos"
  ]
  node [
    id 869
    label "biotop"
  ]
  node [
    id 870
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 871
    label "biocenoza"
  ]
  node [
    id 872
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 873
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 874
    label "zakres"
  ]
  node [
    id 875
    label "miejsce_pracy"
  ]
  node [
    id 876
    label "obszar"
  ]
  node [
    id 877
    label "wymiar"
  ]
  node [
    id 878
    label "nation"
  ]
  node [
    id 879
    label "w&#322;adza"
  ]
  node [
    id 880
    label "kontekst"
  ]
  node [
    id 881
    label "krajobraz"
  ]
  node [
    id 882
    label "plant"
  ]
  node [
    id 883
    label "ro&#347;lina"
  ]
  node [
    id 884
    label "formacja_ro&#347;linna"
  ]
  node [
    id 885
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 886
    label "szata_ro&#347;linna"
  ]
  node [
    id 887
    label "zielono&#347;&#263;"
  ]
  node [
    id 888
    label "pi&#281;tro"
  ]
  node [
    id 889
    label "cyprysowate"
  ]
  node [
    id 890
    label "iglak"
  ]
  node [
    id 891
    label "tanatoplastyk"
  ]
  node [
    id 892
    label "Komitet_Region&#243;w"
  ]
  node [
    id 893
    label "tanatoplastyka"
  ]
  node [
    id 894
    label "pochowanie"
  ]
  node [
    id 895
    label "zabalsamowanie"
  ]
  node [
    id 896
    label "nieumar&#322;y"
  ]
  node [
    id 897
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 898
    label "balsamowanie"
  ]
  node [
    id 899
    label "balsamowa&#263;"
  ]
  node [
    id 900
    label "sekcja"
  ]
  node [
    id 901
    label "pochowa&#263;"
  ]
  node [
    id 902
    label "mi&#281;so"
  ]
  node [
    id 903
    label "ekshumowanie"
  ]
  node [
    id 904
    label "pogrzeb"
  ]
  node [
    id 905
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 906
    label "kremacja"
  ]
  node [
    id 907
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 908
    label "jednostka_organizacyjna"
  ]
  node [
    id 909
    label "Izba_Konsyliarska"
  ]
  node [
    id 910
    label "ekshumowa&#263;"
  ]
  node [
    id 911
    label "zabalsamowa&#263;"
  ]
  node [
    id 912
    label "morze"
  ]
  node [
    id 913
    label "biosfera"
  ]
  node [
    id 914
    label "geotermia"
  ]
  node [
    id 915
    label "atmosfera"
  ]
  node [
    id 916
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 917
    label "p&#243;&#322;kula"
  ]
  node [
    id 918
    label "biegun"
  ]
  node [
    id 919
    label "po&#322;udnie"
  ]
  node [
    id 920
    label "magnetosfera"
  ]
  node [
    id 921
    label "p&#243;&#322;noc"
  ]
  node [
    id 922
    label "litosfera"
  ]
  node [
    id 923
    label "Nowy_&#346;wiat"
  ]
  node [
    id 924
    label "barysfera"
  ]
  node [
    id 925
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 926
    label "hydrosfera"
  ]
  node [
    id 927
    label "Stary_&#346;wiat"
  ]
  node [
    id 928
    label "geosfera"
  ]
  node [
    id 929
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 930
    label "rze&#378;ba"
  ]
  node [
    id 931
    label "ozonosfera"
  ]
  node [
    id 932
    label "geoida"
  ]
  node [
    id 933
    label "b&#322;ona"
  ]
  node [
    id 934
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 935
    label "trawiarnia"
  ]
  node [
    id 936
    label "emulsja_fotograficzna"
  ]
  node [
    id 937
    label "rozbieg&#243;wka"
  ]
  node [
    id 938
    label "sklejarka"
  ]
  node [
    id 939
    label "odczuli&#263;"
  ]
  node [
    id 940
    label "filmoteka"
  ]
  node [
    id 941
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 942
    label "dorobek"
  ]
  node [
    id 943
    label "animatronika"
  ]
  node [
    id 944
    label "napisy"
  ]
  node [
    id 945
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 946
    label "blik"
  ]
  node [
    id 947
    label "odczula&#263;"
  ]
  node [
    id 948
    label "odczulenie"
  ]
  node [
    id 949
    label "photograph"
  ]
  node [
    id 950
    label "scena"
  ]
  node [
    id 951
    label "klatka"
  ]
  node [
    id 952
    label "ta&#347;ma"
  ]
  node [
    id 953
    label "uj&#281;cie"
  ]
  node [
    id 954
    label "anamorfoza"
  ]
  node [
    id 955
    label "czo&#322;&#243;wka"
  ]
  node [
    id 956
    label "odczulanie"
  ]
  node [
    id 957
    label "postprodukcja"
  ]
  node [
    id 958
    label "muza"
  ]
  node [
    id 959
    label "block"
  ]
  node [
    id 960
    label "rola"
  ]
  node [
    id 961
    label "ty&#322;&#243;wka"
  ]
  node [
    id 962
    label "transporter"
  ]
  node [
    id 963
    label "&#347;cie&#380;ka"
  ]
  node [
    id 964
    label "watkowce"
  ]
  node [
    id 965
    label "kula"
  ]
  node [
    id 966
    label "ta&#347;moteka"
  ]
  node [
    id 967
    label "klaps"
  ]
  node [
    id 968
    label "artyku&#322;"
  ]
  node [
    id 969
    label "p&#243;&#322;produkt"
  ]
  node [
    id 970
    label "przewijanie_si&#281;"
  ]
  node [
    id 971
    label "hutnictwo"
  ]
  node [
    id 972
    label "wodorost"
  ]
  node [
    id 973
    label "zielenica"
  ]
  node [
    id 974
    label "webbing"
  ]
  node [
    id 975
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 976
    label "no&#347;nik_danych"
  ]
  node [
    id 977
    label "nagranie"
  ]
  node [
    id 978
    label "m&#243;zgoczaszka"
  ]
  node [
    id 979
    label "tkanka"
  ]
  node [
    id 980
    label "konto"
  ]
  node [
    id 981
    label "wypracowa&#263;"
  ]
  node [
    id 982
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 983
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 984
    label "ods&#322;ona"
  ]
  node [
    id 985
    label "scenariusz"
  ]
  node [
    id 986
    label "fortel"
  ]
  node [
    id 987
    label "utw&#243;r"
  ]
  node [
    id 988
    label "kobieta"
  ]
  node [
    id 989
    label "ambala&#380;"
  ]
  node [
    id 990
    label "Apollo"
  ]
  node [
    id 991
    label "egzemplarz"
  ]
  node [
    id 992
    label "didaskalia"
  ]
  node [
    id 993
    label "czyn"
  ]
  node [
    id 994
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 995
    label "turn"
  ]
  node [
    id 996
    label "towar"
  ]
  node [
    id 997
    label "przedstawia&#263;"
  ]
  node [
    id 998
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 999
    label "head"
  ]
  node [
    id 1000
    label "przedstawienie"
  ]
  node [
    id 1001
    label "theatrical_performance"
  ]
  node [
    id 1002
    label "pokaz"
  ]
  node [
    id 1003
    label "pr&#243;bowanie"
  ]
  node [
    id 1004
    label "przedstawianie"
  ]
  node [
    id 1005
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1006
    label "scenografia"
  ]
  node [
    id 1007
    label "realizacja"
  ]
  node [
    id 1008
    label "Faust"
  ]
  node [
    id 1009
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1010
    label "przedstawi&#263;"
  ]
  node [
    id 1011
    label "Melpomena"
  ]
  node [
    id 1012
    label "muzyka"
  ]
  node [
    id 1013
    label "bogini"
  ]
  node [
    id 1014
    label "talent"
  ]
  node [
    id 1015
    label "natchnienie"
  ]
  node [
    id 1016
    label "banan"
  ]
  node [
    id 1017
    label "inspiratorka"
  ]
  node [
    id 1018
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 1019
    label "palma"
  ]
  node [
    id 1020
    label "pocz&#261;tek"
  ]
  node [
    id 1021
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 1022
    label "kopalnia"
  ]
  node [
    id 1023
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 1024
    label "ogranicza&#263;"
  ]
  node [
    id 1025
    label "akwarium"
  ]
  node [
    id 1026
    label "kle&#263;"
  ]
  node [
    id 1027
    label "konstrukcja"
  ]
  node [
    id 1028
    label "human_body"
  ]
  node [
    id 1029
    label "pr&#281;t"
  ]
  node [
    id 1030
    label "pomieszczenie"
  ]
  node [
    id 1031
    label "d&#378;wig"
  ]
  node [
    id 1032
    label "kinematografia"
  ]
  node [
    id 1033
    label "technika"
  ]
  node [
    id 1034
    label "sznurownia"
  ]
  node [
    id 1035
    label "nadscenie"
  ]
  node [
    id 1036
    label "fragment"
  ]
  node [
    id 1037
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1038
    label "horyzont"
  ]
  node [
    id 1039
    label "epizod"
  ]
  node [
    id 1040
    label "sphere"
  ]
  node [
    id 1041
    label "kiesze&#324;"
  ]
  node [
    id 1042
    label "podest"
  ]
  node [
    id 1043
    label "antyteatr"
  ]
  node [
    id 1044
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 1045
    label "budka_suflera"
  ]
  node [
    id 1046
    label "akt"
  ]
  node [
    id 1047
    label "proscenium"
  ]
  node [
    id 1048
    label "podwy&#380;szenie"
  ]
  node [
    id 1049
    label "stadium"
  ]
  node [
    id 1050
    label "dramaturgy"
  ]
  node [
    id 1051
    label "widzownia"
  ]
  node [
    id 1052
    label "kurtyna"
  ]
  node [
    id 1053
    label "zamkni&#281;cie"
  ]
  node [
    id 1054
    label "prezentacja"
  ]
  node [
    id 1055
    label "withdrawal"
  ]
  node [
    id 1056
    label "podniesienie"
  ]
  node [
    id 1057
    label "strona"
  ]
  node [
    id 1058
    label "wzbudzenie"
  ]
  node [
    id 1059
    label "wzi&#281;cie"
  ]
  node [
    id 1060
    label "zaaresztowanie"
  ]
  node [
    id 1061
    label "capture"
  ]
  node [
    id 1062
    label "pochwytanie"
  ]
  node [
    id 1063
    label "zabranie"
  ]
  node [
    id 1064
    label "poligrafia"
  ]
  node [
    id 1065
    label "latarka_czo&#322;owa"
  ]
  node [
    id 1066
    label "&#347;ciana"
  ]
  node [
    id 1067
    label "pododdzia&#322;"
  ]
  node [
    id 1068
    label "rajd"
  ]
  node [
    id 1069
    label "alpinizm"
  ]
  node [
    id 1070
    label "bieg"
  ]
  node [
    id 1071
    label "front"
  ]
  node [
    id 1072
    label "rz&#261;d"
  ]
  node [
    id 1073
    label "grupa"
  ]
  node [
    id 1074
    label "wst&#281;p"
  ]
  node [
    id 1075
    label "elita"
  ]
  node [
    id 1076
    label "zderzenie"
  ]
  node [
    id 1077
    label "fina&#322;"
  ]
  node [
    id 1078
    label "kostium"
  ]
  node [
    id 1079
    label "radlina"
  ]
  node [
    id 1080
    label "gospodarstwo"
  ]
  node [
    id 1081
    label "zastosowanie"
  ]
  node [
    id 1082
    label "uprawienie"
  ]
  node [
    id 1083
    label "irygowanie"
  ]
  node [
    id 1084
    label "wykonywa&#263;"
  ]
  node [
    id 1085
    label "reinterpretowanie"
  ]
  node [
    id 1086
    label "pole"
  ]
  node [
    id 1087
    label "tekst"
  ]
  node [
    id 1088
    label "aktorstwo"
  ]
  node [
    id 1089
    label "ziemia"
  ]
  node [
    id 1090
    label "ustawienie"
  ]
  node [
    id 1091
    label "zreinterpretowa&#263;"
  ]
  node [
    id 1092
    label "reinterpretowa&#263;"
  ]
  node [
    id 1093
    label "irygowa&#263;"
  ]
  node [
    id 1094
    label "wykonywanie"
  ]
  node [
    id 1095
    label "p&#322;osa"
  ]
  node [
    id 1096
    label "gra&#263;"
  ]
  node [
    id 1097
    label "function"
  ]
  node [
    id 1098
    label "wrench"
  ]
  node [
    id 1099
    label "dialog"
  ]
  node [
    id 1100
    label "zreinterpretowanie"
  ]
  node [
    id 1101
    label "granie"
  ]
  node [
    id 1102
    label "sk&#322;ad"
  ]
  node [
    id 1103
    label "zagranie"
  ]
  node [
    id 1104
    label "uprawi&#263;"
  ]
  node [
    id 1105
    label "cel"
  ]
  node [
    id 1106
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 1107
    label "kszta&#322;t"
  ]
  node [
    id 1108
    label "zagon"
  ]
  node [
    id 1109
    label "plik"
  ]
  node [
    id 1110
    label "odblask"
  ]
  node [
    id 1111
    label "plama"
  ]
  node [
    id 1112
    label "farba"
  ]
  node [
    id 1113
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 1114
    label "wyleczenie"
  ]
  node [
    id 1115
    label "usuni&#281;cie"
  ]
  node [
    id 1116
    label "desensitization"
  ]
  node [
    id 1117
    label "zmniejszenie"
  ]
  node [
    id 1118
    label "alergia"
  ]
  node [
    id 1119
    label "usuwanie"
  ]
  node [
    id 1120
    label "zmniejszanie"
  ]
  node [
    id 1121
    label "terapia"
  ]
  node [
    id 1122
    label "zmniejszy&#263;"
  ]
  node [
    id 1123
    label "wyleczy&#263;"
  ]
  node [
    id 1124
    label "usun&#261;&#263;"
  ]
  node [
    id 1125
    label "zmniejsza&#263;"
  ]
  node [
    id 1126
    label "usuwa&#263;"
  ]
  node [
    id 1127
    label "leczy&#263;"
  ]
  node [
    id 1128
    label "dialogista"
  ]
  node [
    id 1129
    label "przek&#322;ad"
  ]
  node [
    id 1130
    label "deformacja"
  ]
  node [
    id 1131
    label "zamiana"
  ]
  node [
    id 1132
    label "proces_biologiczny"
  ]
  node [
    id 1133
    label "faza"
  ]
  node [
    id 1134
    label "Robert"
  ]
  node [
    id 1135
    label "Kubica"
  ]
  node [
    id 1136
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 1136
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 404
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 814
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 993
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 994
  ]
  edge [
    source 14
    target 995
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 998
  ]
  edge [
    source 14
    target 999
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 1000
  ]
  edge [
    source 14
    target 1001
  ]
  edge [
    source 14
    target 1002
  ]
  edge [
    source 14
    target 1003
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 1005
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 1006
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1008
  ]
  edge [
    source 14
    target 1009
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1011
  ]
  edge [
    source 14
    target 1012
  ]
  edge [
    source 14
    target 1013
  ]
  edge [
    source 14
    target 1014
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1016
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 1018
  ]
  edge [
    source 14
    target 1019
  ]
  edge [
    source 14
    target 1020
  ]
  edge [
    source 14
    target 1021
  ]
  edge [
    source 14
    target 1022
  ]
  edge [
    source 14
    target 1023
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 1025
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 1029
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 1030
  ]
  edge [
    source 14
    target 1031
  ]
  edge [
    source 14
    target 1032
  ]
  edge [
    source 14
    target 1033
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 33
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 219
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 1134
    target 1135
  ]
]
