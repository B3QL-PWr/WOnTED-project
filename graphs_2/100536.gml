graph [
  node [
    id 0
    label "szczepionka"
    origin "text"
  ]
  node [
    id 1
    label "pre"
    origin "text"
  ]
  node [
    id 2
    label "pandemiczny"
    origin "text"
  ]
  node [
    id 3
    label "przeciw"
    origin "text"
  ]
  node [
    id 4
    label "grypa"
    origin "text"
  ]
  node [
    id 5
    label "biopreparat"
  ]
  node [
    id 6
    label "tiomersal"
  ]
  node [
    id 7
    label "przyjmowa&#263;_si&#281;"
  ]
  node [
    id 8
    label "przyj&#261;&#263;_si&#281;"
  ]
  node [
    id 9
    label "vaccine"
  ]
  node [
    id 10
    label "naw&#243;z"
  ]
  node [
    id 11
    label "lekarstwo"
  ]
  node [
    id 12
    label "antyseptyk"
  ]
  node [
    id 13
    label "rt&#281;&#263;"
  ]
  node [
    id 14
    label "metaloorganiczny"
  ]
  node [
    id 15
    label "konserwant"
  ]
  node [
    id 16
    label "epidemiczny"
  ]
  node [
    id 17
    label "nagminny"
  ]
  node [
    id 18
    label "epidemicznie"
  ]
  node [
    id 19
    label "choroba_wirusowa"
  ]
  node [
    id 20
    label "influenca"
  ]
  node [
    id 21
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 22
    label "kaszel"
  ]
  node [
    id 23
    label "wirus_grypy"
  ]
  node [
    id 24
    label "alergia"
  ]
  node [
    id 25
    label "oznaka"
  ]
  node [
    id 26
    label "ekspulsja"
  ]
  node [
    id 27
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 28
    label "atak"
  ]
  node [
    id 29
    label "czynno&#347;&#263;"
  ]
  node [
    id 30
    label "albo"
  ]
  node [
    id 31
    label "H5N1"
  ]
  node [
    id 32
    label "komitet"
  ]
  node [
    id 33
    label "do&#160;spraw"
  ]
  node [
    id 34
    label "produkt"
  ]
  node [
    id 35
    label "leczniczy"
  ]
  node [
    id 36
    label "stosowa&#263;"
  ]
  node [
    id 37
    label "u"
  ]
  node [
    id 38
    label "ludzie"
  ]
  node [
    id 39
    label "Prepandrix"
  ]
  node [
    id 40
    label "&#174;"
  ]
  node [
    id 41
    label "Pandemrix"
  ]
  node [
    id 42
    label "GSK"
  ]
  node [
    id 43
    label "Biologicals"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
]
