graph [
  node [
    id 0
    label "brajanek"
    origin "text"
  ]
  node [
    id 1
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "siusiu"
    origin "text"
  ]
  node [
    id 3
    label "karynka"
    origin "text"
  ]
  node [
    id 4
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "upilnowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "czu&#263;"
  ]
  node [
    id 7
    label "desire"
  ]
  node [
    id 8
    label "kcie&#263;"
  ]
  node [
    id 9
    label "postrzega&#263;"
  ]
  node [
    id 10
    label "przewidywa&#263;"
  ]
  node [
    id 11
    label "by&#263;"
  ]
  node [
    id 12
    label "smell"
  ]
  node [
    id 13
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 14
    label "uczuwa&#263;"
  ]
  node [
    id 15
    label "spirit"
  ]
  node [
    id 16
    label "doznawa&#263;"
  ]
  node [
    id 17
    label "anticipate"
  ]
  node [
    id 18
    label "can"
  ]
  node [
    id 19
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 20
    label "umie&#263;"
  ]
  node [
    id 21
    label "cope"
  ]
  node [
    id 22
    label "potrafia&#263;"
  ]
  node [
    id 23
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 24
    label "wiedzie&#263;"
  ]
  node [
    id 25
    label "m&#243;c"
  ]
  node [
    id 26
    label "visualize"
  ]
  node [
    id 27
    label "zrobi&#263;"
  ]
  node [
    id 28
    label "post&#261;pi&#263;"
  ]
  node [
    id 29
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 30
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 31
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 32
    label "zorganizowa&#263;"
  ]
  node [
    id 33
    label "appoint"
  ]
  node [
    id 34
    label "wystylizowa&#263;"
  ]
  node [
    id 35
    label "cause"
  ]
  node [
    id 36
    label "przerobi&#263;"
  ]
  node [
    id 37
    label "nabra&#263;"
  ]
  node [
    id 38
    label "make"
  ]
  node [
    id 39
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 40
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 41
    label "wydali&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
]
