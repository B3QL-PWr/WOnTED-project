graph [
  node [
    id 0
    label "powt&#243;rka"
    origin "text"
  ]
  node [
    id 1
    label "rozrywka"
    origin "text"
  ]
  node [
    id 2
    label "czynno&#347;&#263;"
  ]
  node [
    id 3
    label "program"
  ]
  node [
    id 4
    label "nauka"
  ]
  node [
    id 5
    label "activity"
  ]
  node [
    id 6
    label "bezproblemowy"
  ]
  node [
    id 7
    label "wydarzenie"
  ]
  node [
    id 8
    label "wiedza"
  ]
  node [
    id 9
    label "miasteczko_rowerowe"
  ]
  node [
    id 10
    label "porada"
  ]
  node [
    id 11
    label "fotowoltaika"
  ]
  node [
    id 12
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 13
    label "przem&#243;wienie"
  ]
  node [
    id 14
    label "nauki_o_poznaniu"
  ]
  node [
    id 15
    label "nomotetyczny"
  ]
  node [
    id 16
    label "systematyka"
  ]
  node [
    id 17
    label "proces"
  ]
  node [
    id 18
    label "typologia"
  ]
  node [
    id 19
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 20
    label "kultura_duchowa"
  ]
  node [
    id 21
    label "&#322;awa_szkolna"
  ]
  node [
    id 22
    label "nauki_penalne"
  ]
  node [
    id 23
    label "dziedzina"
  ]
  node [
    id 24
    label "imagineskopia"
  ]
  node [
    id 25
    label "teoria_naukowa"
  ]
  node [
    id 26
    label "inwentyka"
  ]
  node [
    id 27
    label "metodologia"
  ]
  node [
    id 28
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 29
    label "nauki_o_Ziemi"
  ]
  node [
    id 30
    label "instalowa&#263;"
  ]
  node [
    id 31
    label "oprogramowanie"
  ]
  node [
    id 32
    label "odinstalowywa&#263;"
  ]
  node [
    id 33
    label "spis"
  ]
  node [
    id 34
    label "zaprezentowanie"
  ]
  node [
    id 35
    label "podprogram"
  ]
  node [
    id 36
    label "ogranicznik_referencyjny"
  ]
  node [
    id 37
    label "course_of_study"
  ]
  node [
    id 38
    label "booklet"
  ]
  node [
    id 39
    label "dzia&#322;"
  ]
  node [
    id 40
    label "odinstalowanie"
  ]
  node [
    id 41
    label "broszura"
  ]
  node [
    id 42
    label "wytw&#243;r"
  ]
  node [
    id 43
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 44
    label "kana&#322;"
  ]
  node [
    id 45
    label "teleferie"
  ]
  node [
    id 46
    label "zainstalowanie"
  ]
  node [
    id 47
    label "struktura_organizacyjna"
  ]
  node [
    id 48
    label "pirat"
  ]
  node [
    id 49
    label "zaprezentowa&#263;"
  ]
  node [
    id 50
    label "prezentowanie"
  ]
  node [
    id 51
    label "prezentowa&#263;"
  ]
  node [
    id 52
    label "interfejs"
  ]
  node [
    id 53
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 54
    label "okno"
  ]
  node [
    id 55
    label "blok"
  ]
  node [
    id 56
    label "punkt"
  ]
  node [
    id 57
    label "folder"
  ]
  node [
    id 58
    label "zainstalowa&#263;"
  ]
  node [
    id 59
    label "za&#322;o&#380;enie"
  ]
  node [
    id 60
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 61
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 62
    label "ram&#243;wka"
  ]
  node [
    id 63
    label "tryb"
  ]
  node [
    id 64
    label "emitowa&#263;"
  ]
  node [
    id 65
    label "emitowanie"
  ]
  node [
    id 66
    label "odinstalowywanie"
  ]
  node [
    id 67
    label "instrukcja"
  ]
  node [
    id 68
    label "informatyka"
  ]
  node [
    id 69
    label "deklaracja"
  ]
  node [
    id 70
    label "sekcja_krytyczna"
  ]
  node [
    id 71
    label "menu"
  ]
  node [
    id 72
    label "furkacja"
  ]
  node [
    id 73
    label "podstawa"
  ]
  node [
    id 74
    label "instalowanie"
  ]
  node [
    id 75
    label "oferta"
  ]
  node [
    id 76
    label "odinstalowa&#263;"
  ]
  node [
    id 77
    label "czasoumilacz"
  ]
  node [
    id 78
    label "odpoczynek"
  ]
  node [
    id 79
    label "game"
  ]
  node [
    id 80
    label "urozmaicenie"
  ]
  node [
    id 81
    label "stan"
  ]
  node [
    id 82
    label "wyraj"
  ]
  node [
    id 83
    label "wczas"
  ]
  node [
    id 84
    label "diversion"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
]
