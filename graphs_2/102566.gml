graph [
  node [
    id 0
    label "zakres"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 2
    label "gabinet"
    origin "text"
  ]
  node [
    id 3
    label "prezydent"
    origin "text"
  ]
  node [
    id 4
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 5
    label "szczeg&#243;lno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wielko&#347;&#263;"
  ]
  node [
    id 7
    label "zbi&#243;r"
  ]
  node [
    id 8
    label "podzakres"
  ]
  node [
    id 9
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 10
    label "granica"
  ]
  node [
    id 11
    label "circle"
  ]
  node [
    id 12
    label "desygnat"
  ]
  node [
    id 13
    label "dziedzina"
  ]
  node [
    id 14
    label "sfera"
  ]
  node [
    id 15
    label "zasi&#261;g"
  ]
  node [
    id 16
    label "distribution"
  ]
  node [
    id 17
    label "rozmiar"
  ]
  node [
    id 18
    label "bridge"
  ]
  node [
    id 19
    label "izochronizm"
  ]
  node [
    id 20
    label "poj&#281;cie"
  ]
  node [
    id 21
    label "zdolno&#347;&#263;"
  ]
  node [
    id 22
    label "liczba"
  ]
  node [
    id 23
    label "zaleta"
  ]
  node [
    id 24
    label "property"
  ]
  node [
    id 25
    label "dymensja"
  ]
  node [
    id 26
    label "measure"
  ]
  node [
    id 27
    label "opinia"
  ]
  node [
    id 28
    label "cecha"
  ]
  node [
    id 29
    label "ilo&#347;&#263;"
  ]
  node [
    id 30
    label "znaczenie"
  ]
  node [
    id 31
    label "potencja"
  ]
  node [
    id 32
    label "rzadko&#347;&#263;"
  ]
  node [
    id 33
    label "warunek_lokalowy"
  ]
  node [
    id 34
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 35
    label "pakiet_klimatyczny"
  ]
  node [
    id 36
    label "uprawianie"
  ]
  node [
    id 37
    label "collection"
  ]
  node [
    id 38
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 39
    label "gathering"
  ]
  node [
    id 40
    label "album"
  ]
  node [
    id 41
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 42
    label "praca_rolnicza"
  ]
  node [
    id 43
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 44
    label "sum"
  ]
  node [
    id 45
    label "egzemplarz"
  ]
  node [
    id 46
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 47
    label "series"
  ]
  node [
    id 48
    label "dane"
  ]
  node [
    id 49
    label "miara"
  ]
  node [
    id 50
    label "kres"
  ]
  node [
    id 51
    label "frontier"
  ]
  node [
    id 52
    label "koniec"
  ]
  node [
    id 53
    label "granice"
  ]
  node [
    id 54
    label "pu&#322;ap"
  ]
  node [
    id 55
    label "end"
  ]
  node [
    id 56
    label "Ural"
  ]
  node [
    id 57
    label "granica_pa&#324;stwa"
  ]
  node [
    id 58
    label "przej&#347;cie"
  ]
  node [
    id 59
    label "funkcja"
  ]
  node [
    id 60
    label "bezdro&#380;e"
  ]
  node [
    id 61
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 62
    label "poddzia&#322;"
  ]
  node [
    id 63
    label "przestrze&#324;"
  ]
  node [
    id 64
    label "class"
  ]
  node [
    id 65
    label "sector"
  ]
  node [
    id 66
    label "kula"
  ]
  node [
    id 67
    label "p&#243;&#322;kula"
  ]
  node [
    id 68
    label "wymiar"
  ]
  node [
    id 69
    label "strefa"
  ]
  node [
    id 70
    label "kolur"
  ]
  node [
    id 71
    label "huczek"
  ]
  node [
    id 72
    label "powierzchnia"
  ]
  node [
    id 73
    label "grupa"
  ]
  node [
    id 74
    label "p&#243;&#322;sfera"
  ]
  node [
    id 75
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 76
    label "denotacja"
  ]
  node [
    id 77
    label "designatum"
  ]
  node [
    id 78
    label "nazwa_rzetelna"
  ]
  node [
    id 79
    label "odpowiednik"
  ]
  node [
    id 80
    label "nazwa_pozorna"
  ]
  node [
    id 81
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 82
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 83
    label "strategia"
  ]
  node [
    id 84
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 85
    label "operacja"
  ]
  node [
    id 86
    label "doktryna"
  ]
  node [
    id 87
    label "plan"
  ]
  node [
    id 88
    label "gra"
  ]
  node [
    id 89
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 90
    label "pocz&#261;tki"
  ]
  node [
    id 91
    label "dokument"
  ]
  node [
    id 92
    label "metoda"
  ]
  node [
    id 93
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 94
    label "program"
  ]
  node [
    id 95
    label "wrinkle"
  ]
  node [
    id 96
    label "wzorzec_projektowy"
  ]
  node [
    id 97
    label "activity"
  ]
  node [
    id 98
    label "absolutorium"
  ]
  node [
    id 99
    label "dzia&#322;anie"
  ]
  node [
    id 100
    label "szko&#322;a"
  ]
  node [
    id 101
    label "Londyn"
  ]
  node [
    id 102
    label "boks"
  ]
  node [
    id 103
    label "premier"
  ]
  node [
    id 104
    label "biurko"
  ]
  node [
    id 105
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 106
    label "egzekutywa"
  ]
  node [
    id 107
    label "pok&#243;j"
  ]
  node [
    id 108
    label "Konsulat"
  ]
  node [
    id 109
    label "palestra"
  ]
  node [
    id 110
    label "gabinet_cieni"
  ]
  node [
    id 111
    label "pracownia"
  ]
  node [
    id 112
    label "s&#261;d"
  ]
  node [
    id 113
    label "instytucja"
  ]
  node [
    id 114
    label "pomieszczenie"
  ]
  node [
    id 115
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 116
    label "afiliowa&#263;"
  ]
  node [
    id 117
    label "establishment"
  ]
  node [
    id 118
    label "zamyka&#263;"
  ]
  node [
    id 119
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 120
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 121
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 122
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 123
    label "standard"
  ]
  node [
    id 124
    label "Fundusze_Unijne"
  ]
  node [
    id 125
    label "biuro"
  ]
  node [
    id 126
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 127
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 128
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 129
    label "zamykanie"
  ]
  node [
    id 130
    label "organizacja"
  ]
  node [
    id 131
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 132
    label "osoba_prawna"
  ]
  node [
    id 133
    label "urz&#261;d"
  ]
  node [
    id 134
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 135
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 136
    label "federacja"
  ]
  node [
    id 137
    label "partia"
  ]
  node [
    id 138
    label "w&#322;adza"
  ]
  node [
    id 139
    label "executive"
  ]
  node [
    id 140
    label "obrady"
  ]
  node [
    id 141
    label "organ"
  ]
  node [
    id 142
    label "warsztat"
  ]
  node [
    id 143
    label "uk&#322;ad"
  ]
  node [
    id 144
    label "preliminarium_pokojowe"
  ]
  node [
    id 145
    label "spok&#243;j"
  ]
  node [
    id 146
    label "pacyfista"
  ]
  node [
    id 147
    label "mir"
  ]
  node [
    id 148
    label "zakamarek"
  ]
  node [
    id 149
    label "amfilada"
  ]
  node [
    id 150
    label "sklepienie"
  ]
  node [
    id 151
    label "apartment"
  ]
  node [
    id 152
    label "udost&#281;pnienie"
  ]
  node [
    id 153
    label "front"
  ]
  node [
    id 154
    label "umieszczenie"
  ]
  node [
    id 155
    label "miejsce"
  ]
  node [
    id 156
    label "sufit"
  ]
  node [
    id 157
    label "pod&#322;oga"
  ]
  node [
    id 158
    label "Sto&#322;ypin"
  ]
  node [
    id 159
    label "Bismarck"
  ]
  node [
    id 160
    label "Jelcyn"
  ]
  node [
    id 161
    label "zwierzchnik"
  ]
  node [
    id 162
    label "Miko&#322;ajczyk"
  ]
  node [
    id 163
    label "rz&#261;d"
  ]
  node [
    id 164
    label "Chruszczow"
  ]
  node [
    id 165
    label "dostojnik"
  ]
  node [
    id 166
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 167
    label "blat"
  ]
  node [
    id 168
    label "mebel"
  ]
  node [
    id 169
    label "szuflada"
  ]
  node [
    id 170
    label "sport_walki"
  ]
  node [
    id 171
    label "hak"
  ]
  node [
    id 172
    label "stajnia"
  ]
  node [
    id 173
    label "cholewka"
  ]
  node [
    id 174
    label "sekundant"
  ]
  node [
    id 175
    label "sk&#243;ra"
  ]
  node [
    id 176
    label "Londek"
  ]
  node [
    id 177
    label "Westminster"
  ]
  node [
    id 178
    label "Wimbledon"
  ]
  node [
    id 179
    label "zdanie"
  ]
  node [
    id 180
    label "lekcja"
  ]
  node [
    id 181
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 182
    label "skolaryzacja"
  ]
  node [
    id 183
    label "praktyka"
  ]
  node [
    id 184
    label "wiedza"
  ]
  node [
    id 185
    label "lesson"
  ]
  node [
    id 186
    label "niepokalanki"
  ]
  node [
    id 187
    label "kwalifikacje"
  ]
  node [
    id 188
    label "Mickiewicz"
  ]
  node [
    id 189
    label "muzyka"
  ]
  node [
    id 190
    label "klasa"
  ]
  node [
    id 191
    label "stopek"
  ]
  node [
    id 192
    label "school"
  ]
  node [
    id 193
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 194
    label "&#322;awa_szkolna"
  ]
  node [
    id 195
    label "przepisa&#263;"
  ]
  node [
    id 196
    label "ideologia"
  ]
  node [
    id 197
    label "nauka"
  ]
  node [
    id 198
    label "siedziba"
  ]
  node [
    id 199
    label "sekretariat"
  ]
  node [
    id 200
    label "szkolenie"
  ]
  node [
    id 201
    label "sztuba"
  ]
  node [
    id 202
    label "do&#347;wiadczenie"
  ]
  node [
    id 203
    label "podr&#281;cznik"
  ]
  node [
    id 204
    label "zda&#263;"
  ]
  node [
    id 205
    label "tablica"
  ]
  node [
    id 206
    label "przepisanie"
  ]
  node [
    id 207
    label "kara"
  ]
  node [
    id 208
    label "teren_szko&#322;y"
  ]
  node [
    id 209
    label "system"
  ]
  node [
    id 210
    label "form"
  ]
  node [
    id 211
    label "czas"
  ]
  node [
    id 212
    label "urszulanki"
  ]
  node [
    id 213
    label "absolwent"
  ]
  node [
    id 214
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 215
    label "forum"
  ]
  node [
    id 216
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 217
    label "s&#261;downictwo"
  ]
  node [
    id 218
    label "wydarzenie"
  ]
  node [
    id 219
    label "podejrzany"
  ]
  node [
    id 220
    label "&#347;wiadek"
  ]
  node [
    id 221
    label "post&#281;powanie"
  ]
  node [
    id 222
    label "court"
  ]
  node [
    id 223
    label "my&#347;l"
  ]
  node [
    id 224
    label "obrona"
  ]
  node [
    id 225
    label "broni&#263;"
  ]
  node [
    id 226
    label "antylogizm"
  ]
  node [
    id 227
    label "strona"
  ]
  node [
    id 228
    label "oskar&#380;yciel"
  ]
  node [
    id 229
    label "skazany"
  ]
  node [
    id 230
    label "konektyw"
  ]
  node [
    id 231
    label "wypowied&#378;"
  ]
  node [
    id 232
    label "bronienie"
  ]
  node [
    id 233
    label "wytw&#243;r"
  ]
  node [
    id 234
    label "pods&#261;dny"
  ]
  node [
    id 235
    label "zesp&#243;&#322;"
  ]
  node [
    id 236
    label "procesowicz"
  ]
  node [
    id 237
    label "legal_profession"
  ]
  node [
    id 238
    label "regent"
  ]
  node [
    id 239
    label "budynek"
  ]
  node [
    id 240
    label "prawnicy"
  ]
  node [
    id 241
    label "chancellery"
  ]
  node [
    id 242
    label "Grecja"
  ]
  node [
    id 243
    label "Clinton"
  ]
  node [
    id 244
    label "Putin"
  ]
  node [
    id 245
    label "samorz&#261;dowiec"
  ]
  node [
    id 246
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 247
    label "Nixon"
  ]
  node [
    id 248
    label "Naser"
  ]
  node [
    id 249
    label "Bierut"
  ]
  node [
    id 250
    label "Kemal"
  ]
  node [
    id 251
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 252
    label "gruba_ryba"
  ]
  node [
    id 253
    label "de_Gaulle"
  ]
  node [
    id 254
    label "Tito"
  ]
  node [
    id 255
    label "Gorbaczow"
  ]
  node [
    id 256
    label "Roosevelt"
  ]
  node [
    id 257
    label "kierowa&#263;"
  ]
  node [
    id 258
    label "cz&#322;owiek"
  ]
  node [
    id 259
    label "pryncypa&#322;"
  ]
  node [
    id 260
    label "kierownictwo"
  ]
  node [
    id 261
    label "urz&#281;dnik"
  ]
  node [
    id 262
    label "oficja&#322;"
  ]
  node [
    id 263
    label "notabl"
  ]
  node [
    id 264
    label "samorz&#261;d"
  ]
  node [
    id 265
    label "polityk"
  ]
  node [
    id 266
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 267
    label "komuna"
  ]
  node [
    id 268
    label "para"
  ]
  node [
    id 269
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 270
    label "necessity"
  ]
  node [
    id 271
    label "uczestniczy&#263;"
  ]
  node [
    id 272
    label "trza"
  ]
  node [
    id 273
    label "robi&#263;"
  ]
  node [
    id 274
    label "by&#263;"
  ]
  node [
    id 275
    label "participate"
  ]
  node [
    id 276
    label "trzeba"
  ]
  node [
    id 277
    label "poker"
  ]
  node [
    id 278
    label "odparowanie"
  ]
  node [
    id 279
    label "sztuka"
  ]
  node [
    id 280
    label "smoke"
  ]
  node [
    id 281
    label "Albania"
  ]
  node [
    id 282
    label "odparowa&#263;"
  ]
  node [
    id 283
    label "parowanie"
  ]
  node [
    id 284
    label "chodzi&#263;"
  ]
  node [
    id 285
    label "pair"
  ]
  node [
    id 286
    label "odparowywa&#263;"
  ]
  node [
    id 287
    label "dodatek"
  ]
  node [
    id 288
    label "odparowywanie"
  ]
  node [
    id 289
    label "jednostka_monetarna"
  ]
  node [
    id 290
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 291
    label "moneta"
  ]
  node [
    id 292
    label "damp"
  ]
  node [
    id 293
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 294
    label "wyparowanie"
  ]
  node [
    id 295
    label "gaz_cieplarniany"
  ]
  node [
    id 296
    label "gaz"
  ]
  node [
    id 297
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  node [
    id 298
    label "inno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
]
