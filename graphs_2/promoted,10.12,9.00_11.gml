graph [
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "swoje"
    origin "text"
  ]
  node [
    id 3
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 4
    label "jaki&#347;"
  ]
  node [
    id 5
    label "przyzwoity"
  ]
  node [
    id 6
    label "ciekawy"
  ]
  node [
    id 7
    label "jako&#347;"
  ]
  node [
    id 8
    label "jako_tako"
  ]
  node [
    id 9
    label "niez&#322;y"
  ]
  node [
    id 10
    label "dziwny"
  ]
  node [
    id 11
    label "charakterystyczny"
  ]
  node [
    id 12
    label "czyj&#347;"
  ]
  node [
    id 13
    label "m&#261;&#380;"
  ]
  node [
    id 14
    label "prywatny"
  ]
  node [
    id 15
    label "ma&#322;&#380;onek"
  ]
  node [
    id 16
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 17
    label "ch&#322;op"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "pan_m&#322;ody"
  ]
  node [
    id 20
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 21
    label "&#347;lubny"
  ]
  node [
    id 22
    label "pan_domu"
  ]
  node [
    id 23
    label "pan_i_w&#322;adca"
  ]
  node [
    id 24
    label "stary"
  ]
  node [
    id 25
    label "wytw&#243;r"
  ]
  node [
    id 26
    label "pocz&#261;tki"
  ]
  node [
    id 27
    label "ukra&#347;&#263;"
  ]
  node [
    id 28
    label "ukradzenie"
  ]
  node [
    id 29
    label "idea"
  ]
  node [
    id 30
    label "system"
  ]
  node [
    id 31
    label "przedmiot"
  ]
  node [
    id 32
    label "p&#322;&#243;d"
  ]
  node [
    id 33
    label "work"
  ]
  node [
    id 34
    label "rezultat"
  ]
  node [
    id 35
    label "strategia"
  ]
  node [
    id 36
    label "background"
  ]
  node [
    id 37
    label "dzieci&#281;ctwo"
  ]
  node [
    id 38
    label "podpierdoli&#263;"
  ]
  node [
    id 39
    label "dash_off"
  ]
  node [
    id 40
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 41
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 42
    label "zabra&#263;"
  ]
  node [
    id 43
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 44
    label "overcharge"
  ]
  node [
    id 45
    label "podpierdolenie"
  ]
  node [
    id 46
    label "zgini&#281;cie"
  ]
  node [
    id 47
    label "przyw&#322;aszczenie"
  ]
  node [
    id 48
    label "larceny"
  ]
  node [
    id 49
    label "zaczerpni&#281;cie"
  ]
  node [
    id 50
    label "zw&#281;dzenie"
  ]
  node [
    id 51
    label "okradzenie"
  ]
  node [
    id 52
    label "nakradzenie"
  ]
  node [
    id 53
    label "ideologia"
  ]
  node [
    id 54
    label "byt"
  ]
  node [
    id 55
    label "intelekt"
  ]
  node [
    id 56
    label "Kant"
  ]
  node [
    id 57
    label "cel"
  ]
  node [
    id 58
    label "poj&#281;cie"
  ]
  node [
    id 59
    label "istota"
  ]
  node [
    id 60
    label "ideacja"
  ]
  node [
    id 61
    label "j&#261;dro"
  ]
  node [
    id 62
    label "systemik"
  ]
  node [
    id 63
    label "rozprz&#261;c"
  ]
  node [
    id 64
    label "oprogramowanie"
  ]
  node [
    id 65
    label "systemat"
  ]
  node [
    id 66
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 67
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 68
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 69
    label "model"
  ]
  node [
    id 70
    label "struktura"
  ]
  node [
    id 71
    label "usenet"
  ]
  node [
    id 72
    label "s&#261;d"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "porz&#261;dek"
  ]
  node [
    id 75
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 76
    label "przyn&#281;ta"
  ]
  node [
    id 77
    label "net"
  ]
  node [
    id 78
    label "w&#281;dkarstwo"
  ]
  node [
    id 79
    label "eratem"
  ]
  node [
    id 80
    label "oddzia&#322;"
  ]
  node [
    id 81
    label "doktryna"
  ]
  node [
    id 82
    label "pulpit"
  ]
  node [
    id 83
    label "konstelacja"
  ]
  node [
    id 84
    label "jednostka_geologiczna"
  ]
  node [
    id 85
    label "o&#347;"
  ]
  node [
    id 86
    label "podsystem"
  ]
  node [
    id 87
    label "metoda"
  ]
  node [
    id 88
    label "ryba"
  ]
  node [
    id 89
    label "Leopard"
  ]
  node [
    id 90
    label "spos&#243;b"
  ]
  node [
    id 91
    label "Android"
  ]
  node [
    id 92
    label "zachowanie"
  ]
  node [
    id 93
    label "cybernetyk"
  ]
  node [
    id 94
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 95
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 96
    label "method"
  ]
  node [
    id 97
    label "sk&#322;ad"
  ]
  node [
    id 98
    label "podstawa"
  ]
  node [
    id 99
    label "oprzyrz&#261;dowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
]
