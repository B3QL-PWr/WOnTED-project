graph [
  node [
    id 0
    label "bielawa"
    origin "text"
  ]
  node [
    id 1
    label "toru&#324;"
    origin "text"
  ]
  node [
    id 2
    label "na"
  ]
  node [
    id 3
    label "skarpa"
  ]
  node [
    id 4
    label "lasa"
  ]
  node [
    id 5
    label "bielawski"
  ]
  node [
    id 6
    label "struga&#263;"
  ]
  node [
    id 7
    label "Lubicka"
  ]
  node [
    id 8
    label "Lubiczem"
  ]
  node [
    id 9
    label "dolny"
  ]
  node [
    id 10
    label "stra&#380;"
  ]
  node [
    id 11
    label "po&#380;arny"
  ]
  node [
    id 12
    label "toru&#324;ski"
  ]
  node [
    id 13
    label "centrum"
  ]
  node [
    id 14
    label "handlowy"
  ]
  node [
    id 15
    label "Leroy"
  ]
  node [
    id 16
    label "Merlin"
  ]
  node [
    id 17
    label "Curie"
  ]
  node [
    id 18
    label "Sk&#322;odowska"
  ]
  node [
    id 19
    label "stra&#380;a"
  ]
  node [
    id 20
    label "drogi"
  ]
  node [
    id 21
    label "krajowy"
  ]
  node [
    id 22
    label "nr"
  ]
  node [
    id 23
    label "15"
  ]
  node [
    id 24
    label "80"
  ]
  node [
    id 25
    label "wojew&#243;dzki"
  ]
  node [
    id 26
    label "654"
  ]
  node [
    id 27
    label "Toru&#324;"
  ]
  node [
    id 28
    label "Gr&#281;bocin"
  ]
  node [
    id 29
    label "Lubicz"
  ]
  node [
    id 30
    label "szosa"
  ]
  node [
    id 31
    label "Lubickiej"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 30
    target 31
  ]
]
