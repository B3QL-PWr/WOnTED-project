graph [
  node [
    id 0
    label "george"
    origin "text"
  ]
  node [
    id 1
    label "olah"
    origin "text"
  ]
  node [
    id 2
    label "George"
  ]
  node [
    id 3
    label "albo"
  ]
  node [
    id 4
    label "Olah"
  ]
  node [
    id 5
    label "Andrew"
  ]
  node [
    id 6
    label "Gy&#246;rgy"
  ]
  node [
    id 7
    label "Ol&#225;h"
  ]
  node [
    id 8
    label "nagroda"
  ]
  node [
    id 9
    label "nobel"
  ]
  node [
    id 10
    label "uniwersytet"
  ]
  node [
    id 11
    label "technologia"
  ]
  node [
    id 12
    label "i"
  ]
  node [
    id 13
    label "ekonomia"
  ]
  node [
    id 14
    label "powsta&#263;"
  ]
  node [
    id 15
    label "w&#281;gierski"
  ]
  node [
    id 16
    label "wielki"
  ]
  node [
    id 17
    label "brytania"
  ]
  node [
    id 18
    label "Dow"
  ]
  node [
    id 19
    label "Chemical"
  ]
  node [
    id 20
    label "Case"
  ]
  node [
    id 21
    label "western"
  ]
  node [
    id 22
    label "Reserve"
  ]
  node [
    id 23
    label "University"
  ]
  node [
    id 24
    label "po&#322;udniowy"
  ]
  node [
    id 25
    label "Kalifornia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
]
