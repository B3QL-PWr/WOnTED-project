graph [
  node [
    id 0
    label "paradygmat"
    origin "text"
  ]
  node [
    id 1
    label "obiektowy"
    origin "text"
  ]
  node [
    id 2
    label "opiera&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "cztery"
    origin "text"
  ]
  node [
    id 5
    label "zasada"
    origin "text"
  ]
  node [
    id 6
    label "spos&#243;b"
  ]
  node [
    id 7
    label "mildew"
  ]
  node [
    id 8
    label "ideal"
  ]
  node [
    id 9
    label "ruch"
  ]
  node [
    id 10
    label "fleksja"
  ]
  node [
    id 11
    label "gramatyka"
  ]
  node [
    id 12
    label "odmiennia"
  ]
  node [
    id 13
    label "morfologia"
  ]
  node [
    id 14
    label "model"
  ]
  node [
    id 15
    label "narz&#281;dzie"
  ]
  node [
    id 16
    label "zbi&#243;r"
  ]
  node [
    id 17
    label "tryb"
  ]
  node [
    id 18
    label "nature"
  ]
  node [
    id 19
    label "mechanika"
  ]
  node [
    id 20
    label "utrzymywanie"
  ]
  node [
    id 21
    label "move"
  ]
  node [
    id 22
    label "poruszenie"
  ]
  node [
    id 23
    label "movement"
  ]
  node [
    id 24
    label "myk"
  ]
  node [
    id 25
    label "utrzyma&#263;"
  ]
  node [
    id 26
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 27
    label "zjawisko"
  ]
  node [
    id 28
    label "utrzymanie"
  ]
  node [
    id 29
    label "travel"
  ]
  node [
    id 30
    label "kanciasty"
  ]
  node [
    id 31
    label "commercial_enterprise"
  ]
  node [
    id 32
    label "strumie&#324;"
  ]
  node [
    id 33
    label "proces"
  ]
  node [
    id 34
    label "aktywno&#347;&#263;"
  ]
  node [
    id 35
    label "kr&#243;tki"
  ]
  node [
    id 36
    label "taktyka"
  ]
  node [
    id 37
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 38
    label "apraksja"
  ]
  node [
    id 39
    label "natural_process"
  ]
  node [
    id 40
    label "utrzymywa&#263;"
  ]
  node [
    id 41
    label "d&#322;ugi"
  ]
  node [
    id 42
    label "wydarzenie"
  ]
  node [
    id 43
    label "dyssypacja_energii"
  ]
  node [
    id 44
    label "tumult"
  ]
  node [
    id 45
    label "stopek"
  ]
  node [
    id 46
    label "czynno&#347;&#263;"
  ]
  node [
    id 47
    label "zmiana"
  ]
  node [
    id 48
    label "manewr"
  ]
  node [
    id 49
    label "lokomocja"
  ]
  node [
    id 50
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 51
    label "komunikacja"
  ]
  node [
    id 52
    label "drift"
  ]
  node [
    id 53
    label "osnowywa&#263;"
  ]
  node [
    id 54
    label "czerpa&#263;"
  ]
  node [
    id 55
    label "stawia&#263;"
  ]
  node [
    id 56
    label "digest"
  ]
  node [
    id 57
    label "back"
  ]
  node [
    id 58
    label "pozostawia&#263;"
  ]
  node [
    id 59
    label "czyni&#263;"
  ]
  node [
    id 60
    label "wydawa&#263;"
  ]
  node [
    id 61
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 62
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 63
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 64
    label "raise"
  ]
  node [
    id 65
    label "przewidywa&#263;"
  ]
  node [
    id 66
    label "przyznawa&#263;"
  ]
  node [
    id 67
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 68
    label "go"
  ]
  node [
    id 69
    label "obstawia&#263;"
  ]
  node [
    id 70
    label "umieszcza&#263;"
  ]
  node [
    id 71
    label "ocenia&#263;"
  ]
  node [
    id 72
    label "zastawia&#263;"
  ]
  node [
    id 73
    label "stanowisko"
  ]
  node [
    id 74
    label "znak"
  ]
  node [
    id 75
    label "wskazywa&#263;"
  ]
  node [
    id 76
    label "introduce"
  ]
  node [
    id 77
    label "uruchamia&#263;"
  ]
  node [
    id 78
    label "wytwarza&#263;"
  ]
  node [
    id 79
    label "fundowa&#263;"
  ]
  node [
    id 80
    label "zmienia&#263;"
  ]
  node [
    id 81
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 82
    label "deliver"
  ]
  node [
    id 83
    label "powodowa&#263;"
  ]
  node [
    id 84
    label "wyznacza&#263;"
  ]
  node [
    id 85
    label "przedstawia&#263;"
  ]
  node [
    id 86
    label "wydobywa&#263;"
  ]
  node [
    id 87
    label "bra&#263;"
  ]
  node [
    id 88
    label "korzysta&#263;"
  ]
  node [
    id 89
    label "wch&#322;ania&#263;"
  ]
  node [
    id 90
    label "get"
  ]
  node [
    id 91
    label "cover"
  ]
  node [
    id 92
    label "okr&#281;ca&#263;_si&#281;"
  ]
  node [
    id 93
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 94
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 95
    label "regu&#322;a_Allena"
  ]
  node [
    id 96
    label "base"
  ]
  node [
    id 97
    label "umowa"
  ]
  node [
    id 98
    label "obserwacja"
  ]
  node [
    id 99
    label "zasada_d'Alemberta"
  ]
  node [
    id 100
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 101
    label "normalizacja"
  ]
  node [
    id 102
    label "moralno&#347;&#263;"
  ]
  node [
    id 103
    label "criterion"
  ]
  node [
    id 104
    label "opis"
  ]
  node [
    id 105
    label "regu&#322;a_Glogera"
  ]
  node [
    id 106
    label "prawo_Mendla"
  ]
  node [
    id 107
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 108
    label "twierdzenie"
  ]
  node [
    id 109
    label "prawo"
  ]
  node [
    id 110
    label "standard"
  ]
  node [
    id 111
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 112
    label "dominion"
  ]
  node [
    id 113
    label "qualification"
  ]
  node [
    id 114
    label "occupation"
  ]
  node [
    id 115
    label "podstawa"
  ]
  node [
    id 116
    label "substancja"
  ]
  node [
    id 117
    label "prawid&#322;o"
  ]
  node [
    id 118
    label "dobro&#263;"
  ]
  node [
    id 119
    label "aretologia"
  ]
  node [
    id 120
    label "zesp&#243;&#322;"
  ]
  node [
    id 121
    label "morality"
  ]
  node [
    id 122
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 123
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 124
    label "honesty"
  ]
  node [
    id 125
    label "cecha"
  ]
  node [
    id 126
    label "organizowa&#263;"
  ]
  node [
    id 127
    label "ordinariness"
  ]
  node [
    id 128
    label "instytucja"
  ]
  node [
    id 129
    label "zorganizowa&#263;"
  ]
  node [
    id 130
    label "taniec_towarzyski"
  ]
  node [
    id 131
    label "organizowanie"
  ]
  node [
    id 132
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 133
    label "zorganizowanie"
  ]
  node [
    id 134
    label "wypowied&#378;"
  ]
  node [
    id 135
    label "exposition"
  ]
  node [
    id 136
    label "obja&#347;nienie"
  ]
  node [
    id 137
    label "zawarcie"
  ]
  node [
    id 138
    label "zawrze&#263;"
  ]
  node [
    id 139
    label "czyn"
  ]
  node [
    id 140
    label "warunek"
  ]
  node [
    id 141
    label "gestia_transportowa"
  ]
  node [
    id 142
    label "contract"
  ]
  node [
    id 143
    label "porozumienie"
  ]
  node [
    id 144
    label "klauzula"
  ]
  node [
    id 145
    label "przenikanie"
  ]
  node [
    id 146
    label "byt"
  ]
  node [
    id 147
    label "materia"
  ]
  node [
    id 148
    label "cz&#261;steczka"
  ]
  node [
    id 149
    label "temperatura_krytyczna"
  ]
  node [
    id 150
    label "przenika&#263;"
  ]
  node [
    id 151
    label "smolisty"
  ]
  node [
    id 152
    label "pot&#281;ga"
  ]
  node [
    id 153
    label "documentation"
  ]
  node [
    id 154
    label "przedmiot"
  ]
  node [
    id 155
    label "column"
  ]
  node [
    id 156
    label "zasadzenie"
  ]
  node [
    id 157
    label "za&#322;o&#380;enie"
  ]
  node [
    id 158
    label "punkt_odniesienia"
  ]
  node [
    id 159
    label "zasadzi&#263;"
  ]
  node [
    id 160
    label "bok"
  ]
  node [
    id 161
    label "d&#243;&#322;"
  ]
  node [
    id 162
    label "dzieci&#281;ctwo"
  ]
  node [
    id 163
    label "background"
  ]
  node [
    id 164
    label "podstawowy"
  ]
  node [
    id 165
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 166
    label "strategia"
  ]
  node [
    id 167
    label "pomys&#322;"
  ]
  node [
    id 168
    label "&#347;ciana"
  ]
  node [
    id 169
    label "shoetree"
  ]
  node [
    id 170
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 171
    label "alternatywa_Fredholma"
  ]
  node [
    id 172
    label "oznajmianie"
  ]
  node [
    id 173
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 174
    label "teoria"
  ]
  node [
    id 175
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 176
    label "paradoks_Leontiefa"
  ]
  node [
    id 177
    label "s&#261;d"
  ]
  node [
    id 178
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 179
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 180
    label "teza"
  ]
  node [
    id 181
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 182
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 183
    label "twierdzenie_Pettisa"
  ]
  node [
    id 184
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 185
    label "twierdzenie_Maya"
  ]
  node [
    id 186
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 187
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 188
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 189
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 190
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 191
    label "zapewnianie"
  ]
  node [
    id 192
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 193
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 194
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 195
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 196
    label "twierdzenie_Stokesa"
  ]
  node [
    id 197
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 198
    label "twierdzenie_Cevy"
  ]
  node [
    id 199
    label "twierdzenie_Pascala"
  ]
  node [
    id 200
    label "proposition"
  ]
  node [
    id 201
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 202
    label "komunikowanie"
  ]
  node [
    id 203
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 204
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 205
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 206
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 207
    label "relacja"
  ]
  node [
    id 208
    label "badanie"
  ]
  node [
    id 209
    label "proces_my&#347;lowy"
  ]
  node [
    id 210
    label "remark"
  ]
  node [
    id 211
    label "metoda"
  ]
  node [
    id 212
    label "stwierdzenie"
  ]
  node [
    id 213
    label "observation"
  ]
  node [
    id 214
    label "calibration"
  ]
  node [
    id 215
    label "operacja"
  ]
  node [
    id 216
    label "porz&#261;dek"
  ]
  node [
    id 217
    label "dominance"
  ]
  node [
    id 218
    label "zabieg"
  ]
  node [
    id 219
    label "standardization"
  ]
  node [
    id 220
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 221
    label "umocowa&#263;"
  ]
  node [
    id 222
    label "procesualistyka"
  ]
  node [
    id 223
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 224
    label "kryminalistyka"
  ]
  node [
    id 225
    label "struktura"
  ]
  node [
    id 226
    label "szko&#322;a"
  ]
  node [
    id 227
    label "kierunek"
  ]
  node [
    id 228
    label "normatywizm"
  ]
  node [
    id 229
    label "jurisprudence"
  ]
  node [
    id 230
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 231
    label "kultura_duchowa"
  ]
  node [
    id 232
    label "przepis"
  ]
  node [
    id 233
    label "prawo_karne_procesowe"
  ]
  node [
    id 234
    label "kazuistyka"
  ]
  node [
    id 235
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 236
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 237
    label "kryminologia"
  ]
  node [
    id 238
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 239
    label "prawo_karne"
  ]
  node [
    id 240
    label "legislacyjnie"
  ]
  node [
    id 241
    label "cywilistyka"
  ]
  node [
    id 242
    label "judykatura"
  ]
  node [
    id 243
    label "kanonistyka"
  ]
  node [
    id 244
    label "nauka_prawa"
  ]
  node [
    id 245
    label "podmiot"
  ]
  node [
    id 246
    label "law"
  ]
  node [
    id 247
    label "wykonawczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
]
