graph [
  node [
    id 0
    label "pomnik"
    origin "text"
  ]
  node [
    id 1
    label "karol"
    origin "text"
  ]
  node [
    id 2
    label "linneusz"
    origin "text"
  ]
  node [
    id 3
    label "rzecz"
  ]
  node [
    id 4
    label "&#347;wiadectwo"
  ]
  node [
    id 5
    label "dzie&#322;o"
  ]
  node [
    id 6
    label "dow&#243;d"
  ]
  node [
    id 7
    label "gr&#243;b"
  ]
  node [
    id 8
    label "cok&#243;&#322;"
  ]
  node [
    id 9
    label "p&#322;yta"
  ]
  node [
    id 10
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 11
    label "produkcja"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "phonograph_record"
  ]
  node [
    id 14
    label "nagranie"
  ]
  node [
    id 15
    label "kuchnia"
  ]
  node [
    id 16
    label "p&#322;ytoteka"
  ]
  node [
    id 17
    label "AGD"
  ]
  node [
    id 18
    label "plate"
  ]
  node [
    id 19
    label "miejsce"
  ]
  node [
    id 20
    label "dysk"
  ]
  node [
    id 21
    label "no&#347;nik_danych"
  ]
  node [
    id 22
    label "sheet"
  ]
  node [
    id 23
    label "komunikat"
  ]
  node [
    id 24
    label "tekst"
  ]
  node [
    id 25
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 26
    label "dorobek"
  ]
  node [
    id 27
    label "praca"
  ]
  node [
    id 28
    label "tre&#347;&#263;"
  ]
  node [
    id 29
    label "works"
  ]
  node [
    id 30
    label "obrazowanie"
  ]
  node [
    id 31
    label "retrospektywa"
  ]
  node [
    id 32
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 33
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 34
    label "forma"
  ]
  node [
    id 35
    label "creation"
  ]
  node [
    id 36
    label "tetralogia"
  ]
  node [
    id 37
    label "istota"
  ]
  node [
    id 38
    label "wpada&#263;"
  ]
  node [
    id 39
    label "object"
  ]
  node [
    id 40
    label "przyroda"
  ]
  node [
    id 41
    label "wpa&#347;&#263;"
  ]
  node [
    id 42
    label "kultura"
  ]
  node [
    id 43
    label "mienie"
  ]
  node [
    id 44
    label "obiekt"
  ]
  node [
    id 45
    label "temat"
  ]
  node [
    id 46
    label "wpadni&#281;cie"
  ]
  node [
    id 47
    label "wpadanie"
  ]
  node [
    id 48
    label "certificate"
  ]
  node [
    id 49
    label "o&#347;wiadczenie"
  ]
  node [
    id 50
    label "dokument"
  ]
  node [
    id 51
    label "za&#347;wiadczenie"
  ]
  node [
    id 52
    label "promocja"
  ]
  node [
    id 53
    label "uzasadnienie"
  ]
  node [
    id 54
    label "&#347;rodek"
  ]
  node [
    id 55
    label "act"
  ]
  node [
    id 56
    label "rewizja"
  ]
  node [
    id 57
    label "forsing"
  ]
  node [
    id 58
    label "argument"
  ]
  node [
    id 59
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 60
    label "prochowisko"
  ]
  node [
    id 61
    label "kres"
  ]
  node [
    id 62
    label "&#380;a&#322;oba"
  ]
  node [
    id 63
    label "pogrzebanie"
  ]
  node [
    id 64
    label "zabicie"
  ]
  node [
    id 65
    label "spocz&#281;cie"
  ]
  node [
    id 66
    label "spoczywa&#263;"
  ]
  node [
    id 67
    label "nagrobek"
  ]
  node [
    id 68
    label "pochowanie"
  ]
  node [
    id 69
    label "spoczywanie"
  ]
  node [
    id 70
    label "park_sztywnych"
  ]
  node [
    id 71
    label "agonia"
  ]
  node [
    id 72
    label "szeol"
  ]
  node [
    id 73
    label "mogi&#322;a"
  ]
  node [
    id 74
    label "chowanie"
  ]
  node [
    id 75
    label "kres_&#380;ycia"
  ]
  node [
    id 76
    label "spocz&#261;&#263;"
  ]
  node [
    id 77
    label "defenestracja"
  ]
  node [
    id 78
    label "listwa"
  ]
  node [
    id 79
    label "podstawa"
  ]
  node [
    id 80
    label "&#380;ar&#243;wka"
  ]
  node [
    id 81
    label "lampa_elektronowa"
  ]
  node [
    id 82
    label "uk&#322;ad_elektryczny"
  ]
  node [
    id 83
    label "bottom"
  ]
  node [
    id 84
    label "piedesta&#322;"
  ]
  node [
    id 85
    label "Karol"
  ]
  node [
    id 86
    label "Linneusz"
  ]
  node [
    id 87
    label "albert"
  ]
  node [
    id 88
    label "Rachner"
  ]
  node [
    id 89
    label "Moritz"
  ]
  node [
    id 90
    label "weseli&#263;"
  ]
  node [
    id 91
    label "ogr&#243;d"
  ]
  node [
    id 92
    label "botaniczny"
  ]
  node [
    id 93
    label "ii"
  ]
  node [
    id 94
    label "wojna"
  ]
  node [
    id 95
    label "&#347;wiatowy"
  ]
  node [
    id 96
    label "Antonio"
  ]
  node [
    id 97
    label "Comolli"
  ]
  node [
    id 98
    label "bohdan"
  ]
  node [
    id 99
    label "Chmielewski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 98
    target 99
  ]
]
