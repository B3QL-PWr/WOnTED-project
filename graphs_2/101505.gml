graph [
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "wiesia"
    origin "text"
  ]
  node [
    id 2
    label "powiat"
    origin "text"
  ]
  node [
    id 3
    label "w&#261;growiecki"
    origin "text"
  ]
  node [
    id 4
    label "gwiazda"
  ]
  node [
    id 5
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 6
    label "Arktur"
  ]
  node [
    id 7
    label "kszta&#322;t"
  ]
  node [
    id 8
    label "Gwiazda_Polarna"
  ]
  node [
    id 9
    label "agregatka"
  ]
  node [
    id 10
    label "gromada"
  ]
  node [
    id 11
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 12
    label "S&#322;o&#324;ce"
  ]
  node [
    id 13
    label "Nibiru"
  ]
  node [
    id 14
    label "konstelacja"
  ]
  node [
    id 15
    label "ornament"
  ]
  node [
    id 16
    label "delta_Scuti"
  ]
  node [
    id 17
    label "&#347;wiat&#322;o"
  ]
  node [
    id 18
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 19
    label "obiekt"
  ]
  node [
    id 20
    label "s&#322;awa"
  ]
  node [
    id 21
    label "promie&#324;"
  ]
  node [
    id 22
    label "star"
  ]
  node [
    id 23
    label "gwiazdosz"
  ]
  node [
    id 24
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 25
    label "asocjacja_gwiazd"
  ]
  node [
    id 26
    label "supergrupa"
  ]
  node [
    id 27
    label "wojew&#243;dztwo"
  ]
  node [
    id 28
    label "jednostka_administracyjna"
  ]
  node [
    id 29
    label "gmina"
  ]
  node [
    id 30
    label "Biskupice"
  ]
  node [
    id 31
    label "radny"
  ]
  node [
    id 32
    label "urz&#261;d"
  ]
  node [
    id 33
    label "rada_gminy"
  ]
  node [
    id 34
    label "Dobro&#324;"
  ]
  node [
    id 35
    label "organizacja_religijna"
  ]
  node [
    id 36
    label "Karlsbad"
  ]
  node [
    id 37
    label "Wielka_Wie&#347;"
  ]
  node [
    id 38
    label "mikroregion"
  ]
  node [
    id 39
    label "makroregion"
  ]
  node [
    id 40
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 41
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 42
    label "pa&#324;stwo"
  ]
  node [
    id 43
    label "nowy"
  ]
  node [
    id 44
    label "wie&#347;"
  ]
  node [
    id 45
    label "janowiec"
  ]
  node [
    id 46
    label "wielkopolski"
  ]
  node [
    id 47
    label "RSP"
  ]
  node [
    id 48
    label "&#322;azisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
]
