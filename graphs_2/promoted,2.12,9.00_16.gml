graph [
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 2
    label "masowy"
    origin "text"
  ]
  node [
    id 3
    label "przew&#243;z"
    origin "text"
  ]
  node [
    id 4
    label "towar"
    origin "text"
  ]
  node [
    id 5
    label "trasa"
    origin "text"
  ]
  node [
    id 6
    label "drogowy"
    origin "text"
  ]
  node [
    id 7
    label "chiny"
    origin "text"
  ]
  node [
    id 8
    label "polska"
    origin "text"
  ]
  node [
    id 9
    label "ucieszy&#263;"
    origin "text"
  ]
  node [
    id 10
    label "organizator"
    origin "text"
  ]
  node [
    id 11
    label "test"
    origin "text"
  ]
  node [
    id 12
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 13
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 14
    label "godzina"
  ]
  node [
    id 15
    label "time"
  ]
  node [
    id 16
    label "kwadrans"
  ]
  node [
    id 17
    label "p&#243;&#322;godzina"
  ]
  node [
    id 18
    label "doba"
  ]
  node [
    id 19
    label "czas"
  ]
  node [
    id 20
    label "jednostka_czasu"
  ]
  node [
    id 21
    label "minuta"
  ]
  node [
    id 22
    label "analiza_chemiczna"
  ]
  node [
    id 23
    label "probiernictwo"
  ]
  node [
    id 24
    label "sytuacja"
  ]
  node [
    id 25
    label "spotkanie"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "pobra&#263;"
  ]
  node [
    id 28
    label "effort"
  ]
  node [
    id 29
    label "rezultat"
  ]
  node [
    id 30
    label "usi&#322;owanie"
  ]
  node [
    id 31
    label "czynno&#347;&#263;"
  ]
  node [
    id 32
    label "pobieranie"
  ]
  node [
    id 33
    label "metal_szlachetny"
  ]
  node [
    id 34
    label "pobiera&#263;"
  ]
  node [
    id 35
    label "do&#347;wiadczenie"
  ]
  node [
    id 36
    label "znak"
  ]
  node [
    id 37
    label "item"
  ]
  node [
    id 38
    label "ilo&#347;&#263;"
  ]
  node [
    id 39
    label "pobranie"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 41
    label "postawi&#263;"
  ]
  node [
    id 42
    label "mark"
  ]
  node [
    id 43
    label "kodzik"
  ]
  node [
    id 44
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 45
    label "oznakowanie"
  ]
  node [
    id 46
    label "implikowa&#263;"
  ]
  node [
    id 47
    label "attribute"
  ]
  node [
    id 48
    label "wytw&#243;r"
  ]
  node [
    id 49
    label "point"
  ]
  node [
    id 50
    label "fakt"
  ]
  node [
    id 51
    label "dow&#243;d"
  ]
  node [
    id 52
    label "herb"
  ]
  node [
    id 53
    label "stawia&#263;"
  ]
  node [
    id 54
    label "badanie"
  ]
  node [
    id 55
    label "przechodzenie"
  ]
  node [
    id 56
    label "quiz"
  ]
  node [
    id 57
    label "przechodzi&#263;"
  ]
  node [
    id 58
    label "sprawdzian"
  ]
  node [
    id 59
    label "arkusz"
  ]
  node [
    id 60
    label "narz&#281;dzie"
  ]
  node [
    id 61
    label "bezproblemowy"
  ]
  node [
    id 62
    label "wydarzenie"
  ]
  node [
    id 63
    label "activity"
  ]
  node [
    id 64
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 65
    label "part"
  ]
  node [
    id 66
    label "rozmiar"
  ]
  node [
    id 67
    label "Rzym_Zachodni"
  ]
  node [
    id 68
    label "Rzym_Wschodni"
  ]
  node [
    id 69
    label "element"
  ]
  node [
    id 70
    label "whole"
  ]
  node [
    id 71
    label "urz&#261;dzenie"
  ]
  node [
    id 72
    label "szko&#322;a"
  ]
  node [
    id 73
    label "potraktowanie"
  ]
  node [
    id 74
    label "assay"
  ]
  node [
    id 75
    label "checkup"
  ]
  node [
    id 76
    label "znawstwo"
  ]
  node [
    id 77
    label "skill"
  ]
  node [
    id 78
    label "do&#347;wiadczanie"
  ]
  node [
    id 79
    label "obserwowanie"
  ]
  node [
    id 80
    label "poczucie"
  ]
  node [
    id 81
    label "wiedza"
  ]
  node [
    id 82
    label "eksperiencja"
  ]
  node [
    id 83
    label "zbadanie"
  ]
  node [
    id 84
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 85
    label "wy&#347;wiadczenie"
  ]
  node [
    id 86
    label "warunki"
  ]
  node [
    id 87
    label "szczeg&#243;&#322;"
  ]
  node [
    id 88
    label "realia"
  ]
  node [
    id 89
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 90
    label "state"
  ]
  node [
    id 91
    label "motyw"
  ]
  node [
    id 92
    label "match"
  ]
  node [
    id 93
    label "spotkanie_si&#281;"
  ]
  node [
    id 94
    label "gather"
  ]
  node [
    id 95
    label "spowodowanie"
  ]
  node [
    id 96
    label "zawarcie"
  ]
  node [
    id 97
    label "zdarzenie_si&#281;"
  ]
  node [
    id 98
    label "po&#380;egnanie"
  ]
  node [
    id 99
    label "spotykanie"
  ]
  node [
    id 100
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 101
    label "gathering"
  ]
  node [
    id 102
    label "powitanie"
  ]
  node [
    id 103
    label "doznanie"
  ]
  node [
    id 104
    label "znalezienie"
  ]
  node [
    id 105
    label "employment"
  ]
  node [
    id 106
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 107
    label "znajomy"
  ]
  node [
    id 108
    label "poj&#281;cie"
  ]
  node [
    id 109
    label "pakiet_klimatyczny"
  ]
  node [
    id 110
    label "uprawianie"
  ]
  node [
    id 111
    label "collection"
  ]
  node [
    id 112
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 113
    label "album"
  ]
  node [
    id 114
    label "praca_rolnicza"
  ]
  node [
    id 115
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 116
    label "sum"
  ]
  node [
    id 117
    label "egzemplarz"
  ]
  node [
    id 118
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 119
    label "series"
  ]
  node [
    id 120
    label "dane"
  ]
  node [
    id 121
    label "przyczyna"
  ]
  node [
    id 122
    label "typ"
  ]
  node [
    id 123
    label "dzia&#322;anie"
  ]
  node [
    id 124
    label "event"
  ]
  node [
    id 125
    label "podejmowanie"
  ]
  node [
    id 126
    label "essay"
  ]
  node [
    id 127
    label "staranie_si&#281;"
  ]
  node [
    id 128
    label "kontrola"
  ]
  node [
    id 129
    label "wch&#322;anianie"
  ]
  node [
    id 130
    label "branie"
  ]
  node [
    id 131
    label "przeszczepianie"
  ]
  node [
    id 132
    label "wymienianie_si&#281;"
  ]
  node [
    id 133
    label "pr&#243;bka"
  ]
  node [
    id 134
    label "wycinanie"
  ]
  node [
    id 135
    label "levy"
  ]
  node [
    id 136
    label "otrzymywanie"
  ]
  node [
    id 137
    label "bite"
  ]
  node [
    id 138
    label "wyci&#281;cie"
  ]
  node [
    id 139
    label "przeszczepienie"
  ]
  node [
    id 140
    label "wzi&#281;cie"
  ]
  node [
    id 141
    label "otrzymanie"
  ]
  node [
    id 142
    label "capture"
  ]
  node [
    id 143
    label "wymienienie_si&#281;"
  ]
  node [
    id 144
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 145
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 146
    label "uzyskanie"
  ]
  node [
    id 147
    label "wyci&#261;&#263;"
  ]
  node [
    id 148
    label "skopiowa&#263;"
  ]
  node [
    id 149
    label "get"
  ]
  node [
    id 150
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 151
    label "otrzyma&#263;"
  ]
  node [
    id 152
    label "catch"
  ]
  node [
    id 153
    label "wzi&#261;&#263;"
  ]
  node [
    id 154
    label "uzyska&#263;"
  ]
  node [
    id 155
    label "arise"
  ]
  node [
    id 156
    label "kopiowa&#263;"
  ]
  node [
    id 157
    label "wch&#322;ania&#263;"
  ]
  node [
    id 158
    label "otrzymywa&#263;"
  ]
  node [
    id 159
    label "wycina&#263;"
  ]
  node [
    id 160
    label "open"
  ]
  node [
    id 161
    label "bra&#263;"
  ]
  node [
    id 162
    label "raise"
  ]
  node [
    id 163
    label "popularny"
  ]
  node [
    id 164
    label "niski"
  ]
  node [
    id 165
    label "seryjny"
  ]
  node [
    id 166
    label "masowo"
  ]
  node [
    id 167
    label "nieoryginalny"
  ]
  node [
    id 168
    label "notoryczny"
  ]
  node [
    id 169
    label "powa&#380;ny"
  ]
  node [
    id 170
    label "powtarzalny"
  ]
  node [
    id 171
    label "prawdziwy"
  ]
  node [
    id 172
    label "wielocz&#281;&#347;ciowy"
  ]
  node [
    id 173
    label "seryjnie"
  ]
  node [
    id 174
    label "nisko"
  ]
  node [
    id 175
    label "marny"
  ]
  node [
    id 176
    label "obni&#380;enie"
  ]
  node [
    id 177
    label "ma&#322;y"
  ]
  node [
    id 178
    label "pospolity"
  ]
  node [
    id 179
    label "uni&#380;ony"
  ]
  node [
    id 180
    label "po&#347;ledni"
  ]
  node [
    id 181
    label "n&#281;dznie"
  ]
  node [
    id 182
    label "wstydliwy"
  ]
  node [
    id 183
    label "bliski"
  ]
  node [
    id 184
    label "gorszy"
  ]
  node [
    id 185
    label "obni&#380;anie"
  ]
  node [
    id 186
    label "pomierny"
  ]
  node [
    id 187
    label "nieznaczny"
  ]
  node [
    id 188
    label "s&#322;aby"
  ]
  node [
    id 189
    label "&#322;atwy"
  ]
  node [
    id 190
    label "popularnie"
  ]
  node [
    id 191
    label "przyst&#281;pny"
  ]
  node [
    id 192
    label "znany"
  ]
  node [
    id 193
    label "komunikacja"
  ]
  node [
    id 194
    label "jednoszynowy"
  ]
  node [
    id 195
    label "prze&#322;adunek"
  ]
  node [
    id 196
    label "cedu&#322;a"
  ]
  node [
    id 197
    label "za&#322;adunek"
  ]
  node [
    id 198
    label "us&#322;uga"
  ]
  node [
    id 199
    label "roz&#322;adunek"
  ]
  node [
    id 200
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 201
    label "produkt_gotowy"
  ]
  node [
    id 202
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 203
    label "service"
  ]
  node [
    id 204
    label "asortyment"
  ]
  node [
    id 205
    label "&#347;wiadczenie"
  ]
  node [
    id 206
    label "transportation_system"
  ]
  node [
    id 207
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 208
    label "implicite"
  ]
  node [
    id 209
    label "explicite"
  ]
  node [
    id 210
    label "wydeptanie"
  ]
  node [
    id 211
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 212
    label "miejsce"
  ]
  node [
    id 213
    label "wydeptywanie"
  ]
  node [
    id 214
    label "ekspedytor"
  ]
  node [
    id 215
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 216
    label "transport"
  ]
  node [
    id 217
    label "relokacja"
  ]
  node [
    id 218
    label "bilet"
  ]
  node [
    id 219
    label "kolej"
  ]
  node [
    id 220
    label "spis"
  ]
  node [
    id 221
    label "raport"
  ]
  node [
    id 222
    label "kurs"
  ]
  node [
    id 223
    label "rzuca&#263;"
  ]
  node [
    id 224
    label "rzuci&#263;"
  ]
  node [
    id 225
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 226
    label "szprycowa&#263;"
  ]
  node [
    id 227
    label "rzucanie"
  ]
  node [
    id 228
    label "&#322;&#243;dzki"
  ]
  node [
    id 229
    label "za&#322;adownia"
  ]
  node [
    id 230
    label "cz&#322;owiek"
  ]
  node [
    id 231
    label "wyr&#243;b"
  ]
  node [
    id 232
    label "naszprycowanie"
  ]
  node [
    id 233
    label "rzucenie"
  ]
  node [
    id 234
    label "tkanina"
  ]
  node [
    id 235
    label "szprycowanie"
  ]
  node [
    id 236
    label "obr&#243;t_handlowy"
  ]
  node [
    id 237
    label "narkobiznes"
  ]
  node [
    id 238
    label "metka"
  ]
  node [
    id 239
    label "tandeta"
  ]
  node [
    id 240
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 241
    label "naszprycowa&#263;"
  ]
  node [
    id 242
    label "asymilowa&#263;"
  ]
  node [
    id 243
    label "nasada"
  ]
  node [
    id 244
    label "profanum"
  ]
  node [
    id 245
    label "wz&#243;r"
  ]
  node [
    id 246
    label "senior"
  ]
  node [
    id 247
    label "asymilowanie"
  ]
  node [
    id 248
    label "os&#322;abia&#263;"
  ]
  node [
    id 249
    label "homo_sapiens"
  ]
  node [
    id 250
    label "osoba"
  ]
  node [
    id 251
    label "ludzko&#347;&#263;"
  ]
  node [
    id 252
    label "Adam"
  ]
  node [
    id 253
    label "hominid"
  ]
  node [
    id 254
    label "posta&#263;"
  ]
  node [
    id 255
    label "portrecista"
  ]
  node [
    id 256
    label "polifag"
  ]
  node [
    id 257
    label "podw&#322;adny"
  ]
  node [
    id 258
    label "dwun&#243;g"
  ]
  node [
    id 259
    label "wapniak"
  ]
  node [
    id 260
    label "duch"
  ]
  node [
    id 261
    label "os&#322;abianie"
  ]
  node [
    id 262
    label "antropochoria"
  ]
  node [
    id 263
    label "figura"
  ]
  node [
    id 264
    label "g&#322;owa"
  ]
  node [
    id 265
    label "mikrokosmos"
  ]
  node [
    id 266
    label "oddzia&#322;ywanie"
  ]
  node [
    id 267
    label "produkcja"
  ]
  node [
    id 268
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 269
    label "produkt"
  ]
  node [
    id 270
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 271
    label "p&#322;uczkarnia"
  ]
  node [
    id 272
    label "znakowarka"
  ]
  node [
    id 273
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 274
    label "creation"
  ]
  node [
    id 275
    label "szara_strefa"
  ]
  node [
    id 276
    label "handel"
  ]
  node [
    id 277
    label "narkotyk"
  ]
  node [
    id 278
    label "biznes"
  ]
  node [
    id 279
    label "lekarstwo"
  ]
  node [
    id 280
    label "nafaszerowa&#263;"
  ]
  node [
    id 281
    label "faszerowanie"
  ]
  node [
    id 282
    label "nafaszerowanie"
  ]
  node [
    id 283
    label "faszerowa&#263;"
  ]
  node [
    id 284
    label "syringe"
  ]
  node [
    id 285
    label "port"
  ]
  node [
    id 286
    label "kopalnia"
  ]
  node [
    id 287
    label "zmieni&#263;"
  ]
  node [
    id 288
    label "zdecydowa&#263;"
  ]
  node [
    id 289
    label "poruszy&#263;"
  ]
  node [
    id 290
    label "wywo&#322;a&#263;"
  ]
  node [
    id 291
    label "atak"
  ]
  node [
    id 292
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 293
    label "cie&#324;"
  ]
  node [
    id 294
    label "majdn&#261;&#263;"
  ]
  node [
    id 295
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 296
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 297
    label "ruszy&#263;"
  ]
  node [
    id 298
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 299
    label "rush"
  ]
  node [
    id 300
    label "czar"
  ]
  node [
    id 301
    label "skonstruowa&#263;"
  ]
  node [
    id 302
    label "przeznaczenie"
  ]
  node [
    id 303
    label "bewilder"
  ]
  node [
    id 304
    label "odej&#347;&#263;"
  ]
  node [
    id 305
    label "podejrzenie"
  ]
  node [
    id 306
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 307
    label "project"
  ]
  node [
    id 308
    label "wyzwanie"
  ]
  node [
    id 309
    label "spowodowa&#263;"
  ]
  node [
    id 310
    label "da&#263;"
  ]
  node [
    id 311
    label "peddle"
  ]
  node [
    id 312
    label "powiedzie&#263;"
  ]
  node [
    id 313
    label "most"
  ]
  node [
    id 314
    label "frame"
  ]
  node [
    id 315
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 316
    label "opu&#347;ci&#263;"
  ]
  node [
    id 317
    label "sygn&#261;&#263;"
  ]
  node [
    id 318
    label "konwulsja"
  ]
  node [
    id 319
    label "&#347;wiat&#322;o"
  ]
  node [
    id 320
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 321
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 322
    label "porusza&#263;"
  ]
  node [
    id 323
    label "flip"
  ]
  node [
    id 324
    label "bequeath"
  ]
  node [
    id 325
    label "rusza&#263;"
  ]
  node [
    id 326
    label "odchodzi&#263;"
  ]
  node [
    id 327
    label "grzmoci&#263;"
  ]
  node [
    id 328
    label "spring"
  ]
  node [
    id 329
    label "syga&#263;"
  ]
  node [
    id 330
    label "zmienia&#263;"
  ]
  node [
    id 331
    label "przestawa&#263;"
  ]
  node [
    id 332
    label "opuszcza&#263;"
  ]
  node [
    id 333
    label "przewraca&#263;"
  ]
  node [
    id 334
    label "m&#243;wi&#263;"
  ]
  node [
    id 335
    label "unwrap"
  ]
  node [
    id 336
    label "tug"
  ]
  node [
    id 337
    label "przemieszcza&#263;"
  ]
  node [
    id 338
    label "konstruowa&#263;"
  ]
  node [
    id 339
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 340
    label "kie&#322;basa"
  ]
  node [
    id 341
    label "kawa&#322;ek"
  ]
  node [
    id 342
    label "informacja"
  ]
  node [
    id 343
    label "kartonik"
  ]
  node [
    id 344
    label "label"
  ]
  node [
    id 345
    label "wyposa&#380;enie"
  ]
  node [
    id 346
    label "shy"
  ]
  node [
    id 347
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 348
    label "porzucenie"
  ]
  node [
    id 349
    label "odej&#347;cie"
  ]
  node [
    id 350
    label "oddzia&#322;anie"
  ]
  node [
    id 351
    label "powiedzenie"
  ]
  node [
    id 352
    label "skonstruowanie"
  ]
  node [
    id 353
    label "przewr&#243;cenie"
  ]
  node [
    id 354
    label "ruszenie"
  ]
  node [
    id 355
    label "zrezygnowanie"
  ]
  node [
    id 356
    label "przemieszczenie"
  ]
  node [
    id 357
    label "grzmotni&#281;cie"
  ]
  node [
    id 358
    label "zdecydowanie"
  ]
  node [
    id 359
    label "opuszczenie"
  ]
  node [
    id 360
    label "wywo&#322;anie"
  ]
  node [
    id 361
    label "poruszenie"
  ]
  node [
    id 362
    label "pierdolni&#281;cie"
  ]
  node [
    id 363
    label "przewracanie"
  ]
  node [
    id 364
    label "grzmocenie"
  ]
  node [
    id 365
    label "rezygnowanie"
  ]
  node [
    id 366
    label "przemieszczanie"
  ]
  node [
    id 367
    label "chow"
  ]
  node [
    id 368
    label "decydowanie"
  ]
  node [
    id 369
    label "wywo&#322;ywanie"
  ]
  node [
    id 370
    label "powodowanie"
  ]
  node [
    id 371
    label "m&#243;wienie"
  ]
  node [
    id 372
    label "trafianie"
  ]
  node [
    id 373
    label "konstruowanie"
  ]
  node [
    id 374
    label "wyposa&#380;anie"
  ]
  node [
    id 375
    label "wrzucanie"
  ]
  node [
    id 376
    label "poruszanie"
  ]
  node [
    id 377
    label "odchodzenie"
  ]
  node [
    id 378
    label "narzucanie"
  ]
  node [
    id 379
    label "odrzucenie"
  ]
  node [
    id 380
    label "opuszczanie"
  ]
  node [
    id 381
    label "ruszanie"
  ]
  node [
    id 382
    label "porzucanie"
  ]
  node [
    id 383
    label "zamachiwanie_si&#281;"
  ]
  node [
    id 384
    label "odrzucanie"
  ]
  node [
    id 385
    label "przestawanie"
  ]
  node [
    id 386
    label "przerzucanie"
  ]
  node [
    id 387
    label "wyb&#243;r"
  ]
  node [
    id 388
    label "range"
  ]
  node [
    id 389
    label "mierno&#347;&#263;"
  ]
  node [
    id 390
    label "pisanina"
  ]
  node [
    id 391
    label "g&#243;wno"
  ]
  node [
    id 392
    label "ta&#322;atajstwo"
  ]
  node [
    id 393
    label "plewa"
  ]
  node [
    id 394
    label "utw&#243;r"
  ]
  node [
    id 395
    label "lichota"
  ]
  node [
    id 396
    label "marno&#347;&#263;"
  ]
  node [
    id 397
    label "lipa"
  ]
  node [
    id 398
    label "migawka"
  ]
  node [
    id 399
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 400
    label "polski"
  ]
  node [
    id 401
    label "brzuszek"
  ]
  node [
    id 402
    label "czarne"
  ]
  node [
    id 403
    label "kluski_&#380;elazne"
  ]
  node [
    id 404
    label "angielka"
  ]
  node [
    id 405
    label "po_&#322;&#243;dzku"
  ]
  node [
    id 406
    label "&#380;ulik"
  ]
  node [
    id 407
    label "famu&#322;a"
  ]
  node [
    id 408
    label "remiza"
  ]
  node [
    id 409
    label "dziad"
  ]
  node [
    id 410
    label "siaja"
  ]
  node [
    id 411
    label "poci&#261;gaj&#261;co"
  ]
  node [
    id 412
    label "uatrakcyjnianie"
  ]
  node [
    id 413
    label "uatrakcyjnienie"
  ]
  node [
    id 414
    label "ciekawy"
  ]
  node [
    id 415
    label "apretura"
  ]
  node [
    id 416
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 417
    label "karbonizacja"
  ]
  node [
    id 418
    label "rozprucie_si&#281;"
  ]
  node [
    id 419
    label "splot"
  ]
  node [
    id 420
    label "prucie_si&#281;"
  ]
  node [
    id 421
    label "materia&#322;"
  ]
  node [
    id 422
    label "karbonizowa&#263;"
  ]
  node [
    id 423
    label "maglownia"
  ]
  node [
    id 424
    label "opalarnia"
  ]
  node [
    id 425
    label "pru&#263;_si&#281;"
  ]
  node [
    id 426
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 427
    label "przebieg"
  ]
  node [
    id 428
    label "infrastruktura"
  ]
  node [
    id 429
    label "w&#281;ze&#322;"
  ]
  node [
    id 430
    label "podbieg"
  ]
  node [
    id 431
    label "marszrutyzacja"
  ]
  node [
    id 432
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 433
    label "droga"
  ]
  node [
    id 434
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 435
    label "cycle"
  ]
  node [
    id 436
    label "linia"
  ]
  node [
    id 437
    label "proces"
  ]
  node [
    id 438
    label "praca"
  ]
  node [
    id 439
    label "sequence"
  ]
  node [
    id 440
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 441
    label "room"
  ]
  node [
    id 442
    label "procedura"
  ]
  node [
    id 443
    label "odcinek"
  ]
  node [
    id 444
    label "ton"
  ]
  node [
    id 445
    label "ambitus"
  ]
  node [
    id 446
    label "skala"
  ]
  node [
    id 447
    label "ekwipunek"
  ]
  node [
    id 448
    label "wyb&#243;j"
  ]
  node [
    id 449
    label "journey"
  ]
  node [
    id 450
    label "pobocze"
  ]
  node [
    id 451
    label "ekskursja"
  ]
  node [
    id 452
    label "drogowskaz"
  ]
  node [
    id 453
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 454
    label "budowla"
  ]
  node [
    id 455
    label "rajza"
  ]
  node [
    id 456
    label "passage"
  ]
  node [
    id 457
    label "zbior&#243;wka"
  ]
  node [
    id 458
    label "spos&#243;b"
  ]
  node [
    id 459
    label "turystyka"
  ]
  node [
    id 460
    label "wylot"
  ]
  node [
    id 461
    label "ruch"
  ]
  node [
    id 462
    label "bezsilnikowy"
  ]
  node [
    id 463
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 464
    label "nawierzchnia"
  ]
  node [
    id 465
    label "korona_drogi"
  ]
  node [
    id 466
    label "operacja"
  ]
  node [
    id 467
    label "bieg"
  ]
  node [
    id 468
    label "stray"
  ]
  node [
    id 469
    label "pozostawa&#263;"
  ]
  node [
    id 470
    label "s&#261;dzi&#263;"
  ]
  node [
    id 471
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 472
    label "digress"
  ]
  node [
    id 473
    label "chodzi&#263;"
  ]
  node [
    id 474
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 475
    label "mieszanie_si&#281;"
  ]
  node [
    id 476
    label "chodzenie"
  ]
  node [
    id 477
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 478
    label "zwi&#261;zanie"
  ]
  node [
    id 479
    label "graf"
  ]
  node [
    id 480
    label "struktura_anatomiczna"
  ]
  node [
    id 481
    label "band"
  ]
  node [
    id 482
    label "zwi&#261;zek"
  ]
  node [
    id 483
    label "argument"
  ]
  node [
    id 484
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 485
    label "marriage"
  ]
  node [
    id 486
    label "bratnia_dusza"
  ]
  node [
    id 487
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 488
    label "fala_stoj&#261;ca"
  ]
  node [
    id 489
    label "mila_morska"
  ]
  node [
    id 490
    label "zwi&#261;za&#263;"
  ]
  node [
    id 491
    label "uczesanie"
  ]
  node [
    id 492
    label "punkt"
  ]
  node [
    id 493
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 494
    label "zgrubienie"
  ]
  node [
    id 495
    label "pismo_klinowe"
  ]
  node [
    id 496
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 497
    label "o&#347;rodek"
  ]
  node [
    id 498
    label "problem"
  ]
  node [
    id 499
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 500
    label "wi&#261;zanie"
  ]
  node [
    id 501
    label "akcja"
  ]
  node [
    id 502
    label "przeci&#281;cie"
  ]
  node [
    id 503
    label "skupienie"
  ]
  node [
    id 504
    label "zawi&#261;za&#263;"
  ]
  node [
    id 505
    label "tying"
  ]
  node [
    id 506
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 507
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 508
    label "hitch"
  ]
  node [
    id 509
    label "orbita"
  ]
  node [
    id 510
    label "kryszta&#322;"
  ]
  node [
    id 511
    label "ekliptyka"
  ]
  node [
    id 512
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 513
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 514
    label "fabu&#322;a"
  ]
  node [
    id 515
    label "zaplecze"
  ]
  node [
    id 516
    label "radiofonia"
  ]
  node [
    id 517
    label "telefonia"
  ]
  node [
    id 518
    label "dro&#380;ny"
  ]
  node [
    id 519
    label "permeable"
  ]
  node [
    id 520
    label "udro&#380;nienie"
  ]
  node [
    id 521
    label "wydolny"
  ]
  node [
    id 522
    label "wzbudzi&#263;"
  ]
  node [
    id 523
    label "obradowa&#263;"
  ]
  node [
    id 524
    label "delight"
  ]
  node [
    id 525
    label "arouse"
  ]
  node [
    id 526
    label "sit"
  ]
  node [
    id 527
    label "dyskutowa&#263;"
  ]
  node [
    id 528
    label "realizator"
  ]
  node [
    id 529
    label "spiritus_movens"
  ]
  node [
    id 530
    label "wykonawca"
  ]
  node [
    id 531
    label "niezb&#281;dnik"
  ]
  node [
    id 532
    label "przedmiot"
  ]
  node [
    id 533
    label "tylec"
  ]
  node [
    id 534
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 535
    label "pytanie"
  ]
  node [
    id 536
    label "praca_pisemna"
  ]
  node [
    id 537
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 538
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 539
    label "examination"
  ]
  node [
    id 540
    label "&#263;wiczenie"
  ]
  node [
    id 541
    label "podchodzi&#263;"
  ]
  node [
    id 542
    label "dydaktyka"
  ]
  node [
    id 543
    label "faza"
  ]
  node [
    id 544
    label "krytykowanie"
  ]
  node [
    id 545
    label "sprawdzanie"
  ]
  node [
    id 546
    label "udowadnianie"
  ]
  node [
    id 547
    label "ustalenie"
  ]
  node [
    id 548
    label "macanie"
  ]
  node [
    id 549
    label "analysis"
  ]
  node [
    id 550
    label "investigation"
  ]
  node [
    id 551
    label "dociekanie"
  ]
  node [
    id 552
    label "bia&#322;a_niedziela"
  ]
  node [
    id 553
    label "penetrowanie"
  ]
  node [
    id 554
    label "zrecenzowanie"
  ]
  node [
    id 555
    label "diagnostyka"
  ]
  node [
    id 556
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 557
    label "ustalanie"
  ]
  node [
    id 558
    label "omawianie"
  ]
  node [
    id 559
    label "wziernikowanie"
  ]
  node [
    id 560
    label "rozpatrywanie"
  ]
  node [
    id 561
    label "rektalny"
  ]
  node [
    id 562
    label "p&#322;at"
  ]
  node [
    id 563
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 564
    label "konkurs"
  ]
  node [
    id 565
    label "zgaduj-zgadula"
  ]
  node [
    id 566
    label "program"
  ]
  node [
    id 567
    label "zmierzanie"
  ]
  node [
    id 568
    label "popchni&#281;cie"
  ]
  node [
    id 569
    label "dzianie_si&#281;"
  ]
  node [
    id 570
    label "zaznawanie"
  ]
  node [
    id 571
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 572
    label "popychanie"
  ]
  node [
    id 573
    label "trwanie"
  ]
  node [
    id 574
    label "zaczynanie"
  ]
  node [
    id 575
    label "przep&#322;ywanie"
  ]
  node [
    id 576
    label "nas&#261;czanie"
  ]
  node [
    id 577
    label "przemakanie"
  ]
  node [
    id 578
    label "passing"
  ]
  node [
    id 579
    label "zaliczanie"
  ]
  node [
    id 580
    label "przerabianie"
  ]
  node [
    id 581
    label "nasycanie_si&#281;"
  ]
  node [
    id 582
    label "wytyczenie"
  ]
  node [
    id 583
    label "bycie"
  ]
  node [
    id 584
    label "mijanie"
  ]
  node [
    id 585
    label "pass"
  ]
  node [
    id 586
    label "uznanie"
  ]
  node [
    id 587
    label "przebywanie"
  ]
  node [
    id 588
    label "potr&#261;canie"
  ]
  node [
    id 589
    label "dostawanie_si&#281;"
  ]
  node [
    id 590
    label "zast&#281;powanie"
  ]
  node [
    id 591
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 592
    label "odmienianie"
  ]
  node [
    id 593
    label "przedostawanie_si&#281;"
  ]
  node [
    id 594
    label "nale&#380;enie"
  ]
  node [
    id 595
    label "stawanie_si&#281;"
  ]
  node [
    id 596
    label "impregnation"
  ]
  node [
    id 597
    label "przepuszczanie"
  ]
  node [
    id 598
    label "move"
  ]
  node [
    id 599
    label "proceed"
  ]
  node [
    id 600
    label "conflict"
  ]
  node [
    id 601
    label "doznawa&#263;"
  ]
  node [
    id 602
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 603
    label "mie&#263;_miejsce"
  ]
  node [
    id 604
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 605
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 606
    label "przebywa&#263;"
  ]
  node [
    id 607
    label "continue"
  ]
  node [
    id 608
    label "go"
  ]
  node [
    id 609
    label "mija&#263;"
  ]
  node [
    id 610
    label "przerabia&#263;"
  ]
  node [
    id 611
    label "saturate"
  ]
  node [
    id 612
    label "zaczyna&#263;"
  ]
  node [
    id 613
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 614
    label "zalicza&#263;"
  ]
  node [
    id 615
    label "i&#347;&#263;"
  ]
  node [
    id 616
    label "podlega&#263;"
  ]
  node [
    id 617
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 618
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 619
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 620
    label "Japonia"
  ]
  node [
    id 621
    label "Zair"
  ]
  node [
    id 622
    label "Belize"
  ]
  node [
    id 623
    label "San_Marino"
  ]
  node [
    id 624
    label "Tanzania"
  ]
  node [
    id 625
    label "Antigua_i_Barbuda"
  ]
  node [
    id 626
    label "granica_pa&#324;stwa"
  ]
  node [
    id 627
    label "Senegal"
  ]
  node [
    id 628
    label "Seszele"
  ]
  node [
    id 629
    label "Mauretania"
  ]
  node [
    id 630
    label "Indie"
  ]
  node [
    id 631
    label "Filipiny"
  ]
  node [
    id 632
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 633
    label "Zimbabwe"
  ]
  node [
    id 634
    label "Malezja"
  ]
  node [
    id 635
    label "Rumunia"
  ]
  node [
    id 636
    label "Surinam"
  ]
  node [
    id 637
    label "Ukraina"
  ]
  node [
    id 638
    label "Syria"
  ]
  node [
    id 639
    label "Wyspy_Marshalla"
  ]
  node [
    id 640
    label "Burkina_Faso"
  ]
  node [
    id 641
    label "Grecja"
  ]
  node [
    id 642
    label "Polska"
  ]
  node [
    id 643
    label "Wenezuela"
  ]
  node [
    id 644
    label "Suazi"
  ]
  node [
    id 645
    label "Nepal"
  ]
  node [
    id 646
    label "S&#322;owacja"
  ]
  node [
    id 647
    label "Algieria"
  ]
  node [
    id 648
    label "Chiny"
  ]
  node [
    id 649
    label "Grenada"
  ]
  node [
    id 650
    label "Barbados"
  ]
  node [
    id 651
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 652
    label "Pakistan"
  ]
  node [
    id 653
    label "Niemcy"
  ]
  node [
    id 654
    label "Bahrajn"
  ]
  node [
    id 655
    label "Komory"
  ]
  node [
    id 656
    label "Australia"
  ]
  node [
    id 657
    label "Rodezja"
  ]
  node [
    id 658
    label "Malawi"
  ]
  node [
    id 659
    label "Gwinea"
  ]
  node [
    id 660
    label "Wehrlen"
  ]
  node [
    id 661
    label "Meksyk"
  ]
  node [
    id 662
    label "Liechtenstein"
  ]
  node [
    id 663
    label "Czarnog&#243;ra"
  ]
  node [
    id 664
    label "Wielka_Brytania"
  ]
  node [
    id 665
    label "Kuwejt"
  ]
  node [
    id 666
    label "Monako"
  ]
  node [
    id 667
    label "Angola"
  ]
  node [
    id 668
    label "Jemen"
  ]
  node [
    id 669
    label "Etiopia"
  ]
  node [
    id 670
    label "Madagaskar"
  ]
  node [
    id 671
    label "terytorium"
  ]
  node [
    id 672
    label "Kolumbia"
  ]
  node [
    id 673
    label "Portoryko"
  ]
  node [
    id 674
    label "Mauritius"
  ]
  node [
    id 675
    label "Kostaryka"
  ]
  node [
    id 676
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 677
    label "Tajlandia"
  ]
  node [
    id 678
    label "Argentyna"
  ]
  node [
    id 679
    label "Zambia"
  ]
  node [
    id 680
    label "Sri_Lanka"
  ]
  node [
    id 681
    label "Gwatemala"
  ]
  node [
    id 682
    label "Kirgistan"
  ]
  node [
    id 683
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 684
    label "Hiszpania"
  ]
  node [
    id 685
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 686
    label "Salwador"
  ]
  node [
    id 687
    label "Korea"
  ]
  node [
    id 688
    label "Macedonia"
  ]
  node [
    id 689
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 690
    label "Brunei"
  ]
  node [
    id 691
    label "Mozambik"
  ]
  node [
    id 692
    label "Turcja"
  ]
  node [
    id 693
    label "Kambod&#380;a"
  ]
  node [
    id 694
    label "Benin"
  ]
  node [
    id 695
    label "Bhutan"
  ]
  node [
    id 696
    label "Tunezja"
  ]
  node [
    id 697
    label "Austria"
  ]
  node [
    id 698
    label "Izrael"
  ]
  node [
    id 699
    label "Sierra_Leone"
  ]
  node [
    id 700
    label "Jamajka"
  ]
  node [
    id 701
    label "Rosja"
  ]
  node [
    id 702
    label "Rwanda"
  ]
  node [
    id 703
    label "holoarktyka"
  ]
  node [
    id 704
    label "Nigeria"
  ]
  node [
    id 705
    label "USA"
  ]
  node [
    id 706
    label "Oman"
  ]
  node [
    id 707
    label "Luksemburg"
  ]
  node [
    id 708
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 709
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 710
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 711
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 712
    label "Dominikana"
  ]
  node [
    id 713
    label "Irlandia"
  ]
  node [
    id 714
    label "Liban"
  ]
  node [
    id 715
    label "Hanower"
  ]
  node [
    id 716
    label "Estonia"
  ]
  node [
    id 717
    label "Iran"
  ]
  node [
    id 718
    label "Nowa_Zelandia"
  ]
  node [
    id 719
    label "Gabon"
  ]
  node [
    id 720
    label "Samoa"
  ]
  node [
    id 721
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 722
    label "S&#322;owenia"
  ]
  node [
    id 723
    label "Kiribati"
  ]
  node [
    id 724
    label "Egipt"
  ]
  node [
    id 725
    label "Togo"
  ]
  node [
    id 726
    label "Mongolia"
  ]
  node [
    id 727
    label "Sudan"
  ]
  node [
    id 728
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 729
    label "Bahamy"
  ]
  node [
    id 730
    label "Bangladesz"
  ]
  node [
    id 731
    label "partia"
  ]
  node [
    id 732
    label "Serbia"
  ]
  node [
    id 733
    label "Czechy"
  ]
  node [
    id 734
    label "Holandia"
  ]
  node [
    id 735
    label "Birma"
  ]
  node [
    id 736
    label "Albania"
  ]
  node [
    id 737
    label "Mikronezja"
  ]
  node [
    id 738
    label "Gambia"
  ]
  node [
    id 739
    label "Kazachstan"
  ]
  node [
    id 740
    label "interior"
  ]
  node [
    id 741
    label "Uzbekistan"
  ]
  node [
    id 742
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 743
    label "Malta"
  ]
  node [
    id 744
    label "Lesoto"
  ]
  node [
    id 745
    label "para"
  ]
  node [
    id 746
    label "Antarktis"
  ]
  node [
    id 747
    label "Andora"
  ]
  node [
    id 748
    label "Nauru"
  ]
  node [
    id 749
    label "Kuba"
  ]
  node [
    id 750
    label "Wietnam"
  ]
  node [
    id 751
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 752
    label "ziemia"
  ]
  node [
    id 753
    label "Kamerun"
  ]
  node [
    id 754
    label "Chorwacja"
  ]
  node [
    id 755
    label "Urugwaj"
  ]
  node [
    id 756
    label "Niger"
  ]
  node [
    id 757
    label "Turkmenistan"
  ]
  node [
    id 758
    label "Szwajcaria"
  ]
  node [
    id 759
    label "zwrot"
  ]
  node [
    id 760
    label "organizacja"
  ]
  node [
    id 761
    label "grupa"
  ]
  node [
    id 762
    label "Palau"
  ]
  node [
    id 763
    label "Litwa"
  ]
  node [
    id 764
    label "Gruzja"
  ]
  node [
    id 765
    label "Tajwan"
  ]
  node [
    id 766
    label "Kongo"
  ]
  node [
    id 767
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 768
    label "Honduras"
  ]
  node [
    id 769
    label "Boliwia"
  ]
  node [
    id 770
    label "Uganda"
  ]
  node [
    id 771
    label "Namibia"
  ]
  node [
    id 772
    label "Azerbejd&#380;an"
  ]
  node [
    id 773
    label "Erytrea"
  ]
  node [
    id 774
    label "Gujana"
  ]
  node [
    id 775
    label "Panama"
  ]
  node [
    id 776
    label "Somalia"
  ]
  node [
    id 777
    label "Burundi"
  ]
  node [
    id 778
    label "Tuwalu"
  ]
  node [
    id 779
    label "Libia"
  ]
  node [
    id 780
    label "Katar"
  ]
  node [
    id 781
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 782
    label "Sahara_Zachodnia"
  ]
  node [
    id 783
    label "Trynidad_i_Tobago"
  ]
  node [
    id 784
    label "Gwinea_Bissau"
  ]
  node [
    id 785
    label "Bu&#322;garia"
  ]
  node [
    id 786
    label "Fid&#380;i"
  ]
  node [
    id 787
    label "Nikaragua"
  ]
  node [
    id 788
    label "Tonga"
  ]
  node [
    id 789
    label "Timor_Wschodni"
  ]
  node [
    id 790
    label "Laos"
  ]
  node [
    id 791
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 792
    label "Ghana"
  ]
  node [
    id 793
    label "Brazylia"
  ]
  node [
    id 794
    label "Belgia"
  ]
  node [
    id 795
    label "Irak"
  ]
  node [
    id 796
    label "Peru"
  ]
  node [
    id 797
    label "Arabia_Saudyjska"
  ]
  node [
    id 798
    label "Indonezja"
  ]
  node [
    id 799
    label "Malediwy"
  ]
  node [
    id 800
    label "Afganistan"
  ]
  node [
    id 801
    label "Jordania"
  ]
  node [
    id 802
    label "Kenia"
  ]
  node [
    id 803
    label "Czad"
  ]
  node [
    id 804
    label "Liberia"
  ]
  node [
    id 805
    label "W&#281;gry"
  ]
  node [
    id 806
    label "Chile"
  ]
  node [
    id 807
    label "Mali"
  ]
  node [
    id 808
    label "Armenia"
  ]
  node [
    id 809
    label "Kanada"
  ]
  node [
    id 810
    label "Cypr"
  ]
  node [
    id 811
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 812
    label "Ekwador"
  ]
  node [
    id 813
    label "Mo&#322;dawia"
  ]
  node [
    id 814
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 815
    label "W&#322;ochy"
  ]
  node [
    id 816
    label "Wyspy_Salomona"
  ]
  node [
    id 817
    label "&#321;otwa"
  ]
  node [
    id 818
    label "D&#380;ibuti"
  ]
  node [
    id 819
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 820
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 821
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 822
    label "Portugalia"
  ]
  node [
    id 823
    label "Botswana"
  ]
  node [
    id 824
    label "Maroko"
  ]
  node [
    id 825
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 826
    label "Francja"
  ]
  node [
    id 827
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 828
    label "Dominika"
  ]
  node [
    id 829
    label "Paragwaj"
  ]
  node [
    id 830
    label "Tad&#380;ykistan"
  ]
  node [
    id 831
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 832
    label "Haiti"
  ]
  node [
    id 833
    label "Khitai"
  ]
  node [
    id 834
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 835
    label "poker"
  ]
  node [
    id 836
    label "nale&#380;e&#263;"
  ]
  node [
    id 837
    label "odparowanie"
  ]
  node [
    id 838
    label "sztuka"
  ]
  node [
    id 839
    label "smoke"
  ]
  node [
    id 840
    label "odparowa&#263;"
  ]
  node [
    id 841
    label "parowanie"
  ]
  node [
    id 842
    label "pair"
  ]
  node [
    id 843
    label "uk&#322;ad"
  ]
  node [
    id 844
    label "odparowywa&#263;"
  ]
  node [
    id 845
    label "dodatek"
  ]
  node [
    id 846
    label "odparowywanie"
  ]
  node [
    id 847
    label "jednostka_monetarna"
  ]
  node [
    id 848
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 849
    label "moneta"
  ]
  node [
    id 850
    label "damp"
  ]
  node [
    id 851
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 852
    label "wyparowanie"
  ]
  node [
    id 853
    label "gaz_cieplarniany"
  ]
  node [
    id 854
    label "gaz"
  ]
  node [
    id 855
    label "zesp&#243;&#322;"
  ]
  node [
    id 856
    label "jednostka_administracyjna"
  ]
  node [
    id 857
    label "Wile&#324;szczyzna"
  ]
  node [
    id 858
    label "obszar"
  ]
  node [
    id 859
    label "Jukon"
  ]
  node [
    id 860
    label "przybud&#243;wka"
  ]
  node [
    id 861
    label "struktura"
  ]
  node [
    id 862
    label "organization"
  ]
  node [
    id 863
    label "od&#322;am"
  ]
  node [
    id 864
    label "TOPR"
  ]
  node [
    id 865
    label "komitet_koordynacyjny"
  ]
  node [
    id 866
    label "przedstawicielstwo"
  ]
  node [
    id 867
    label "ZMP"
  ]
  node [
    id 868
    label "Cepelia"
  ]
  node [
    id 869
    label "GOPR"
  ]
  node [
    id 870
    label "endecki"
  ]
  node [
    id 871
    label "ZBoWiD"
  ]
  node [
    id 872
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 873
    label "podmiot"
  ]
  node [
    id 874
    label "boj&#243;wka"
  ]
  node [
    id 875
    label "ZOMO"
  ]
  node [
    id 876
    label "jednostka_organizacyjna"
  ]
  node [
    id 877
    label "centrala"
  ]
  node [
    id 878
    label "zmiana"
  ]
  node [
    id 879
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 880
    label "turn"
  ]
  node [
    id 881
    label "wyra&#380;enie"
  ]
  node [
    id 882
    label "fraza_czasownikowa"
  ]
  node [
    id 883
    label "turning"
  ]
  node [
    id 884
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 885
    label "skr&#281;t"
  ]
  node [
    id 886
    label "jednostka_leksykalna"
  ]
  node [
    id 887
    label "obr&#243;t"
  ]
  node [
    id 888
    label "kompozycja"
  ]
  node [
    id 889
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 890
    label "type"
  ]
  node [
    id 891
    label "cz&#261;steczka"
  ]
  node [
    id 892
    label "gromada"
  ]
  node [
    id 893
    label "specgrupa"
  ]
  node [
    id 894
    label "stage_set"
  ]
  node [
    id 895
    label "odm&#322;odzenie"
  ]
  node [
    id 896
    label "odm&#322;adza&#263;"
  ]
  node [
    id 897
    label "harcerze_starsi"
  ]
  node [
    id 898
    label "jednostka_systematyczna"
  ]
  node [
    id 899
    label "oddzia&#322;"
  ]
  node [
    id 900
    label "category"
  ]
  node [
    id 901
    label "liga"
  ]
  node [
    id 902
    label "&#346;wietliki"
  ]
  node [
    id 903
    label "formacja_geologiczna"
  ]
  node [
    id 904
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 905
    label "Eurogrupa"
  ]
  node [
    id 906
    label "Terranie"
  ]
  node [
    id 907
    label "odm&#322;adzanie"
  ]
  node [
    id 908
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 909
    label "Entuzjastki"
  ]
  node [
    id 910
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 911
    label "AWS"
  ]
  node [
    id 912
    label "ZChN"
  ]
  node [
    id 913
    label "Bund"
  ]
  node [
    id 914
    label "PPR"
  ]
  node [
    id 915
    label "blok"
  ]
  node [
    id 916
    label "egzekutywa"
  ]
  node [
    id 917
    label "Wigowie"
  ]
  node [
    id 918
    label "aktyw"
  ]
  node [
    id 919
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 920
    label "Razem"
  ]
  node [
    id 921
    label "unit"
  ]
  node [
    id 922
    label "wybranka"
  ]
  node [
    id 923
    label "SLD"
  ]
  node [
    id 924
    label "ZSL"
  ]
  node [
    id 925
    label "Kuomintang"
  ]
  node [
    id 926
    label "si&#322;a"
  ]
  node [
    id 927
    label "PiS"
  ]
  node [
    id 928
    label "gra"
  ]
  node [
    id 929
    label "Jakobici"
  ]
  node [
    id 930
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 931
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 932
    label "package"
  ]
  node [
    id 933
    label "PO"
  ]
  node [
    id 934
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 935
    label "game"
  ]
  node [
    id 936
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 937
    label "wybranek"
  ]
  node [
    id 938
    label "niedoczas"
  ]
  node [
    id 939
    label "Federali&#347;ci"
  ]
  node [
    id 940
    label "PSL"
  ]
  node [
    id 941
    label "plant"
  ]
  node [
    id 942
    label "przyroda"
  ]
  node [
    id 943
    label "ro&#347;lina"
  ]
  node [
    id 944
    label "formacja_ro&#347;linna"
  ]
  node [
    id 945
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 946
    label "biom"
  ]
  node [
    id 947
    label "geosystem"
  ]
  node [
    id 948
    label "szata_ro&#347;linna"
  ]
  node [
    id 949
    label "zielono&#347;&#263;"
  ]
  node [
    id 950
    label "pi&#281;tro"
  ]
  node [
    id 951
    label "teren"
  ]
  node [
    id 952
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 953
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 954
    label "Kaszmir"
  ]
  node [
    id 955
    label "Pend&#380;ab"
  ]
  node [
    id 956
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 957
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 958
    label "funt_liba&#324;ski"
  ]
  node [
    id 959
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 960
    label "Pozna&#324;"
  ]
  node [
    id 961
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 962
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 963
    label "Gozo"
  ]
  node [
    id 964
    label "lira_malta&#324;ska"
  ]
  node [
    id 965
    label "strefa_euro"
  ]
  node [
    id 966
    label "Afryka_Zachodnia"
  ]
  node [
    id 967
    label "Afryka_Wschodnia"
  ]
  node [
    id 968
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 969
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 970
    label "dolar_namibijski"
  ]
  node [
    id 971
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 972
    label "NATO"
  ]
  node [
    id 973
    label "escudo_portugalskie"
  ]
  node [
    id 974
    label "milrejs"
  ]
  node [
    id 975
    label "Wielka_Bahama"
  ]
  node [
    id 976
    label "dolar_bahamski"
  ]
  node [
    id 977
    label "Karaiby"
  ]
  node [
    id 978
    label "dolar_liberyjski"
  ]
  node [
    id 979
    label "riel"
  ]
  node [
    id 980
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 981
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 982
    label "&#321;adoga"
  ]
  node [
    id 983
    label "Dniepr"
  ]
  node [
    id 984
    label "Kamczatka"
  ]
  node [
    id 985
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 986
    label "Witim"
  ]
  node [
    id 987
    label "Tuwa"
  ]
  node [
    id 988
    label "Czeczenia"
  ]
  node [
    id 989
    label "Ajon"
  ]
  node [
    id 990
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 991
    label "car"
  ]
  node [
    id 992
    label "Karelia"
  ]
  node [
    id 993
    label "Don"
  ]
  node [
    id 994
    label "Mordowia"
  ]
  node [
    id 995
    label "Czuwaszja"
  ]
  node [
    id 996
    label "Udmurcja"
  ]
  node [
    id 997
    label "Jama&#322;"
  ]
  node [
    id 998
    label "Azja"
  ]
  node [
    id 999
    label "Newa"
  ]
  node [
    id 1000
    label "Adygeja"
  ]
  node [
    id 1001
    label "Inguszetia"
  ]
  node [
    id 1002
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1003
    label "Mari_El"
  ]
  node [
    id 1004
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1005
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1006
    label "Wszechrosja"
  ]
  node [
    id 1007
    label "rubel_rosyjski"
  ]
  node [
    id 1008
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1009
    label "Dagestan"
  ]
  node [
    id 1010
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1011
    label "Komi"
  ]
  node [
    id 1012
    label "Tatarstan"
  ]
  node [
    id 1013
    label "Baszkiria"
  ]
  node [
    id 1014
    label "Perm"
  ]
  node [
    id 1015
    label "Chakasja"
  ]
  node [
    id 1016
    label "Syberia"
  ]
  node [
    id 1017
    label "Europa_Wschodnia"
  ]
  node [
    id 1018
    label "Anadyr"
  ]
  node [
    id 1019
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1020
    label "Unia_Europejska"
  ]
  node [
    id 1021
    label "Dobrud&#380;a"
  ]
  node [
    id 1022
    label "lew"
  ]
  node [
    id 1023
    label "Judea"
  ]
  node [
    id 1024
    label "lira_izraelska"
  ]
  node [
    id 1025
    label "Galilea"
  ]
  node [
    id 1026
    label "szekel"
  ]
  node [
    id 1027
    label "Luksemburgia"
  ]
  node [
    id 1028
    label "Flandria"
  ]
  node [
    id 1029
    label "Brabancja"
  ]
  node [
    id 1030
    label "Limburgia"
  ]
  node [
    id 1031
    label "Walonia"
  ]
  node [
    id 1032
    label "frank_belgijski"
  ]
  node [
    id 1033
    label "Niderlandy"
  ]
  node [
    id 1034
    label "dinar_iracki"
  ]
  node [
    id 1035
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1036
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1037
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1038
    label "Maghreb"
  ]
  node [
    id 1039
    label "szyling_ugandyjski"
  ]
  node [
    id 1040
    label "kafar"
  ]
  node [
    id 1041
    label "dolar_jamajski"
  ]
  node [
    id 1042
    label "Borneo"
  ]
  node [
    id 1043
    label "ringgit"
  ]
  node [
    id 1044
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1045
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1046
    label "dolar_surinamski"
  ]
  node [
    id 1047
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1048
    label "funt_suda&#324;ski"
  ]
  node [
    id 1049
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1050
    label "Manica"
  ]
  node [
    id 1051
    label "Inhambane"
  ]
  node [
    id 1052
    label "Maputo"
  ]
  node [
    id 1053
    label "Nampula"
  ]
  node [
    id 1054
    label "metical"
  ]
  node [
    id 1055
    label "escudo_mozambickie"
  ]
  node [
    id 1056
    label "Gaza"
  ]
  node [
    id 1057
    label "Niasa"
  ]
  node [
    id 1058
    label "Cabo_Delgado"
  ]
  node [
    id 1059
    label "Sahara"
  ]
  node [
    id 1060
    label "sol"
  ]
  node [
    id 1061
    label "inti"
  ]
  node [
    id 1062
    label "kip"
  ]
  node [
    id 1063
    label "Pireneje"
  ]
  node [
    id 1064
    label "euro"
  ]
  node [
    id 1065
    label "kwacha_zambijska"
  ]
  node [
    id 1066
    label "tugrik"
  ]
  node [
    id 1067
    label "Azja_Wschodnia"
  ]
  node [
    id 1068
    label "ajmak"
  ]
  node [
    id 1069
    label "Buriaci"
  ]
  node [
    id 1070
    label "Ameryka_Centralna"
  ]
  node [
    id 1071
    label "balboa"
  ]
  node [
    id 1072
    label "dolar"
  ]
  node [
    id 1073
    label "Zelandia"
  ]
  node [
    id 1074
    label "gulden"
  ]
  node [
    id 1075
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1076
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1077
    label "dolar_Tuvalu"
  ]
  node [
    id 1078
    label "Polinezja"
  ]
  node [
    id 1079
    label "Katanga"
  ]
  node [
    id 1080
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1081
    label "zair"
  ]
  node [
    id 1082
    label "frank_szwajcarski"
  ]
  node [
    id 1083
    label "Europa_Zachodnia"
  ]
  node [
    id 1084
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1085
    label "dolar_Belize"
  ]
  node [
    id 1086
    label "Jukatan"
  ]
  node [
    id 1087
    label "colon"
  ]
  node [
    id 1088
    label "Dyja"
  ]
  node [
    id 1089
    label "Izera"
  ]
  node [
    id 1090
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1091
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1092
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1093
    label "Lasko"
  ]
  node [
    id 1094
    label "korona_czeska"
  ]
  node [
    id 1095
    label "ugija"
  ]
  node [
    id 1096
    label "szyling_kenijski"
  ]
  node [
    id 1097
    label "Karabach"
  ]
  node [
    id 1098
    label "manat_azerski"
  ]
  node [
    id 1099
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1100
    label "Nachiczewan"
  ]
  node [
    id 1101
    label "Bengal"
  ]
  node [
    id 1102
    label "taka"
  ]
  node [
    id 1103
    label "dolar_Kiribati"
  ]
  node [
    id 1104
    label "Ocean_Spokojny"
  ]
  node [
    id 1105
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1106
    label "Cebu"
  ]
  node [
    id 1107
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1108
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1109
    label "Ulster"
  ]
  node [
    id 1110
    label "Atlantyk"
  ]
  node [
    id 1111
    label "funt_irlandzki"
  ]
  node [
    id 1112
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1113
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1114
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1115
    label "cedi"
  ]
  node [
    id 1116
    label "ariary"
  ]
  node [
    id 1117
    label "Ocean_Indyjski"
  ]
  node [
    id 1118
    label "frank_malgaski"
  ]
  node [
    id 1119
    label "Walencja"
  ]
  node [
    id 1120
    label "Galicja"
  ]
  node [
    id 1121
    label "Aragonia"
  ]
  node [
    id 1122
    label "Estremadura"
  ]
  node [
    id 1123
    label "Baskonia"
  ]
  node [
    id 1124
    label "Andaluzja"
  ]
  node [
    id 1125
    label "Katalonia"
  ]
  node [
    id 1126
    label "Majorka"
  ]
  node [
    id 1127
    label "Asturia"
  ]
  node [
    id 1128
    label "Kastylia"
  ]
  node [
    id 1129
    label "peseta"
  ]
  node [
    id 1130
    label "hacjender"
  ]
  node [
    id 1131
    label "peso_chilijskie"
  ]
  node [
    id 1132
    label "rupia_indyjska"
  ]
  node [
    id 1133
    label "Kerala"
  ]
  node [
    id 1134
    label "Indie_Zachodnie"
  ]
  node [
    id 1135
    label "Indie_Wschodnie"
  ]
  node [
    id 1136
    label "Asam"
  ]
  node [
    id 1137
    label "Indie_Portugalskie"
  ]
  node [
    id 1138
    label "Bollywood"
  ]
  node [
    id 1139
    label "Sikkim"
  ]
  node [
    id 1140
    label "jen"
  ]
  node [
    id 1141
    label "Okinawa"
  ]
  node [
    id 1142
    label "jinja"
  ]
  node [
    id 1143
    label "Japonica"
  ]
  node [
    id 1144
    label "Karlsbad"
  ]
  node [
    id 1145
    label "Turyngia"
  ]
  node [
    id 1146
    label "Brandenburgia"
  ]
  node [
    id 1147
    label "marka"
  ]
  node [
    id 1148
    label "Saksonia"
  ]
  node [
    id 1149
    label "Szlezwik"
  ]
  node [
    id 1150
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1151
    label "Frankonia"
  ]
  node [
    id 1152
    label "Rugia"
  ]
  node [
    id 1153
    label "Helgoland"
  ]
  node [
    id 1154
    label "Bawaria"
  ]
  node [
    id 1155
    label "Holsztyn"
  ]
  node [
    id 1156
    label "Badenia"
  ]
  node [
    id 1157
    label "Wirtembergia"
  ]
  node [
    id 1158
    label "Nadrenia"
  ]
  node [
    id 1159
    label "Anglosas"
  ]
  node [
    id 1160
    label "Hesja"
  ]
  node [
    id 1161
    label "Dolna_Saksonia"
  ]
  node [
    id 1162
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1163
    label "Germania"
  ]
  node [
    id 1164
    label "Po&#322;abie"
  ]
  node [
    id 1165
    label "Szwabia"
  ]
  node [
    id 1166
    label "Westfalia"
  ]
  node [
    id 1167
    label "Romania"
  ]
  node [
    id 1168
    label "Ok&#281;cie"
  ]
  node [
    id 1169
    label "Apulia"
  ]
  node [
    id 1170
    label "Liguria"
  ]
  node [
    id 1171
    label "Kalabria"
  ]
  node [
    id 1172
    label "Piemont"
  ]
  node [
    id 1173
    label "Umbria"
  ]
  node [
    id 1174
    label "lir"
  ]
  node [
    id 1175
    label "Lombardia"
  ]
  node [
    id 1176
    label "Warszawa"
  ]
  node [
    id 1177
    label "Karyntia"
  ]
  node [
    id 1178
    label "Sardynia"
  ]
  node [
    id 1179
    label "Toskania"
  ]
  node [
    id 1180
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1181
    label "Italia"
  ]
  node [
    id 1182
    label "Kampania"
  ]
  node [
    id 1183
    label "Sycylia"
  ]
  node [
    id 1184
    label "Dacja"
  ]
  node [
    id 1185
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1186
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1187
    label "Ba&#322;kany"
  ]
  node [
    id 1188
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1189
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1190
    label "alawizm"
  ]
  node [
    id 1191
    label "funt_syryjski"
  ]
  node [
    id 1192
    label "frank_rwandyjski"
  ]
  node [
    id 1193
    label "dinar_Bahrajnu"
  ]
  node [
    id 1194
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1195
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1196
    label "frank_luksemburski"
  ]
  node [
    id 1197
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1198
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1199
    label "frank_monakijski"
  ]
  node [
    id 1200
    label "dinar_algierski"
  ]
  node [
    id 1201
    label "Kabylia"
  ]
  node [
    id 1202
    label "Oceania"
  ]
  node [
    id 1203
    label "Sand&#380;ak"
  ]
  node [
    id 1204
    label "Wojwodina"
  ]
  node [
    id 1205
    label "dinar_serbski"
  ]
  node [
    id 1206
    label "boliwar"
  ]
  node [
    id 1207
    label "Orinoko"
  ]
  node [
    id 1208
    label "tenge"
  ]
  node [
    id 1209
    label "lek"
  ]
  node [
    id 1210
    label "frank_alba&#324;ski"
  ]
  node [
    id 1211
    label "dolar_Barbadosu"
  ]
  node [
    id 1212
    label "Antyle"
  ]
  node [
    id 1213
    label "Arakan"
  ]
  node [
    id 1214
    label "kyat"
  ]
  node [
    id 1215
    label "c&#243;rdoba"
  ]
  node [
    id 1216
    label "Lesbos"
  ]
  node [
    id 1217
    label "Tesalia"
  ]
  node [
    id 1218
    label "Eolia"
  ]
  node [
    id 1219
    label "panhellenizm"
  ]
  node [
    id 1220
    label "Achaja"
  ]
  node [
    id 1221
    label "Kreta"
  ]
  node [
    id 1222
    label "Peloponez"
  ]
  node [
    id 1223
    label "Olimp"
  ]
  node [
    id 1224
    label "drachma"
  ]
  node [
    id 1225
    label "Rodos"
  ]
  node [
    id 1226
    label "Termopile"
  ]
  node [
    id 1227
    label "palestra"
  ]
  node [
    id 1228
    label "Eubea"
  ]
  node [
    id 1229
    label "Paros"
  ]
  node [
    id 1230
    label "Hellada"
  ]
  node [
    id 1231
    label "Beocja"
  ]
  node [
    id 1232
    label "Parnas"
  ]
  node [
    id 1233
    label "Etolia"
  ]
  node [
    id 1234
    label "Attyka"
  ]
  node [
    id 1235
    label "Epir"
  ]
  node [
    id 1236
    label "Mariany"
  ]
  node [
    id 1237
    label "Tyrol"
  ]
  node [
    id 1238
    label "Salzburg"
  ]
  node [
    id 1239
    label "konsulent"
  ]
  node [
    id 1240
    label "Rakuzy"
  ]
  node [
    id 1241
    label "szyling_austryjacki"
  ]
  node [
    id 1242
    label "Amhara"
  ]
  node [
    id 1243
    label "negus"
  ]
  node [
    id 1244
    label "birr"
  ]
  node [
    id 1245
    label "Syjon"
  ]
  node [
    id 1246
    label "rupia_indonezyjska"
  ]
  node [
    id 1247
    label "Jawa"
  ]
  node [
    id 1248
    label "Moluki"
  ]
  node [
    id 1249
    label "Nowa_Gwinea"
  ]
  node [
    id 1250
    label "Sumatra"
  ]
  node [
    id 1251
    label "boliviano"
  ]
  node [
    id 1252
    label "frank_francuski"
  ]
  node [
    id 1253
    label "Lotaryngia"
  ]
  node [
    id 1254
    label "Alzacja"
  ]
  node [
    id 1255
    label "Gwadelupa"
  ]
  node [
    id 1256
    label "Bordeaux"
  ]
  node [
    id 1257
    label "Pikardia"
  ]
  node [
    id 1258
    label "Sabaudia"
  ]
  node [
    id 1259
    label "Korsyka"
  ]
  node [
    id 1260
    label "Bretania"
  ]
  node [
    id 1261
    label "Masyw_Centralny"
  ]
  node [
    id 1262
    label "Armagnac"
  ]
  node [
    id 1263
    label "Akwitania"
  ]
  node [
    id 1264
    label "Wandea"
  ]
  node [
    id 1265
    label "Martynika"
  ]
  node [
    id 1266
    label "Prowansja"
  ]
  node [
    id 1267
    label "Sekwana"
  ]
  node [
    id 1268
    label "Normandia"
  ]
  node [
    id 1269
    label "Burgundia"
  ]
  node [
    id 1270
    label "Gaskonia"
  ]
  node [
    id 1271
    label "Langwedocja"
  ]
  node [
    id 1272
    label "somoni"
  ]
  node [
    id 1273
    label "Melanezja"
  ]
  node [
    id 1274
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1275
    label "Afrodyzje"
  ]
  node [
    id 1276
    label "funt_cypryjski"
  ]
  node [
    id 1277
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1278
    label "Fryburg"
  ]
  node [
    id 1279
    label "Bazylea"
  ]
  node [
    id 1280
    label "Helwecja"
  ]
  node [
    id 1281
    label "Alpy"
  ]
  node [
    id 1282
    label "Berno"
  ]
  node [
    id 1283
    label "Karaka&#322;pacja"
  ]
  node [
    id 1284
    label "Liwonia"
  ]
  node [
    id 1285
    label "Kurlandia"
  ]
  node [
    id 1286
    label "rubel_&#322;otewski"
  ]
  node [
    id 1287
    label "&#322;at"
  ]
  node [
    id 1288
    label "Windawa"
  ]
  node [
    id 1289
    label "Inflanty"
  ]
  node [
    id 1290
    label "&#379;mud&#378;"
  ]
  node [
    id 1291
    label "lit"
  ]
  node [
    id 1292
    label "dinar_tunezyjski"
  ]
  node [
    id 1293
    label "frank_tunezyjski"
  ]
  node [
    id 1294
    label "lempira"
  ]
  node [
    id 1295
    label "Lipt&#243;w"
  ]
  node [
    id 1296
    label "korona_w&#281;gierska"
  ]
  node [
    id 1297
    label "forint"
  ]
  node [
    id 1298
    label "dong"
  ]
  node [
    id 1299
    label "Annam"
  ]
  node [
    id 1300
    label "Tonkin"
  ]
  node [
    id 1301
    label "lud"
  ]
  node [
    id 1302
    label "frank_kongijski"
  ]
  node [
    id 1303
    label "szyling_somalijski"
  ]
  node [
    id 1304
    label "real"
  ]
  node [
    id 1305
    label "cruzado"
  ]
  node [
    id 1306
    label "Ma&#322;orosja"
  ]
  node [
    id 1307
    label "Podole"
  ]
  node [
    id 1308
    label "Nadbu&#380;e"
  ]
  node [
    id 1309
    label "Przykarpacie"
  ]
  node [
    id 1310
    label "Naddnieprze"
  ]
  node [
    id 1311
    label "Zaporo&#380;e"
  ]
  node [
    id 1312
    label "Kozaczyzna"
  ]
  node [
    id 1313
    label "Wsch&#243;d"
  ]
  node [
    id 1314
    label "karbowaniec"
  ]
  node [
    id 1315
    label "hrywna"
  ]
  node [
    id 1316
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1317
    label "Dniestr"
  ]
  node [
    id 1318
    label "Krym"
  ]
  node [
    id 1319
    label "Zakarpacie"
  ]
  node [
    id 1320
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1321
    label "Tasmania"
  ]
  node [
    id 1322
    label "dolar_australijski"
  ]
  node [
    id 1323
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1324
    label "gourde"
  ]
  node [
    id 1325
    label "kwanza"
  ]
  node [
    id 1326
    label "escudo_angolskie"
  ]
  node [
    id 1327
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1328
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1329
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1330
    label "lari"
  ]
  node [
    id 1331
    label "Ad&#380;aria"
  ]
  node [
    id 1332
    label "naira"
  ]
  node [
    id 1333
    label "Hudson"
  ]
  node [
    id 1334
    label "Teksas"
  ]
  node [
    id 1335
    label "Georgia"
  ]
  node [
    id 1336
    label "Maryland"
  ]
  node [
    id 1337
    label "Michigan"
  ]
  node [
    id 1338
    label "Massachusetts"
  ]
  node [
    id 1339
    label "Luizjana"
  ]
  node [
    id 1340
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1341
    label "stan_wolny"
  ]
  node [
    id 1342
    label "Floryda"
  ]
  node [
    id 1343
    label "Po&#322;udnie"
  ]
  node [
    id 1344
    label "Ohio"
  ]
  node [
    id 1345
    label "Alaska"
  ]
  node [
    id 1346
    label "Nowy_Meksyk"
  ]
  node [
    id 1347
    label "Wuj_Sam"
  ]
  node [
    id 1348
    label "Kansas"
  ]
  node [
    id 1349
    label "Alabama"
  ]
  node [
    id 1350
    label "Kalifornia"
  ]
  node [
    id 1351
    label "Wirginia"
  ]
  node [
    id 1352
    label "Nowy_York"
  ]
  node [
    id 1353
    label "Waszyngton"
  ]
  node [
    id 1354
    label "Pensylwania"
  ]
  node [
    id 1355
    label "zielona_karta"
  ]
  node [
    id 1356
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1357
    label "P&#243;&#322;noc"
  ]
  node [
    id 1358
    label "Hawaje"
  ]
  node [
    id 1359
    label "Zach&#243;d"
  ]
  node [
    id 1360
    label "Illinois"
  ]
  node [
    id 1361
    label "Oklahoma"
  ]
  node [
    id 1362
    label "Oregon"
  ]
  node [
    id 1363
    label "Arizona"
  ]
  node [
    id 1364
    label "som"
  ]
  node [
    id 1365
    label "peso_urugwajskie"
  ]
  node [
    id 1366
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1367
    label "dolar_Brunei"
  ]
  node [
    id 1368
    label "Persja"
  ]
  node [
    id 1369
    label "rial_ira&#324;ski"
  ]
  node [
    id 1370
    label "mu&#322;&#322;a"
  ]
  node [
    id 1371
    label "dinar_libijski"
  ]
  node [
    id 1372
    label "d&#380;amahirijja"
  ]
  node [
    id 1373
    label "nakfa"
  ]
  node [
    id 1374
    label "rial_katarski"
  ]
  node [
    id 1375
    label "quetzal"
  ]
  node [
    id 1376
    label "won"
  ]
  node [
    id 1377
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1378
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1379
    label "guarani"
  ]
  node [
    id 1380
    label "perper"
  ]
  node [
    id 1381
    label "dinar_kuwejcki"
  ]
  node [
    id 1382
    label "dalasi"
  ]
  node [
    id 1383
    label "dolar_Zimbabwe"
  ]
  node [
    id 1384
    label "Szantung"
  ]
  node [
    id 1385
    label "Mand&#380;uria"
  ]
  node [
    id 1386
    label "Hongkong"
  ]
  node [
    id 1387
    label "Guangdong"
  ]
  node [
    id 1388
    label "yuan"
  ]
  node [
    id 1389
    label "D&#380;ungaria"
  ]
  node [
    id 1390
    label "Chiny_Zachodnie"
  ]
  node [
    id 1391
    label "Junnan"
  ]
  node [
    id 1392
    label "Kuantung"
  ]
  node [
    id 1393
    label "Chiny_Wschodnie"
  ]
  node [
    id 1394
    label "Syczuan"
  ]
  node [
    id 1395
    label "Krajna"
  ]
  node [
    id 1396
    label "Kielecczyzna"
  ]
  node [
    id 1397
    label "Opolskie"
  ]
  node [
    id 1398
    label "Lubuskie"
  ]
  node [
    id 1399
    label "Mazowsze"
  ]
  node [
    id 1400
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1401
    label "Kaczawa"
  ]
  node [
    id 1402
    label "Podlasie"
  ]
  node [
    id 1403
    label "Wolin"
  ]
  node [
    id 1404
    label "Wielkopolska"
  ]
  node [
    id 1405
    label "Lubelszczyzna"
  ]
  node [
    id 1406
    label "So&#322;a"
  ]
  node [
    id 1407
    label "Wis&#322;a"
  ]
  node [
    id 1408
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1409
    label "Pa&#322;uki"
  ]
  node [
    id 1410
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1411
    label "Podkarpacie"
  ]
  node [
    id 1412
    label "barwy_polskie"
  ]
  node [
    id 1413
    label "Kujawy"
  ]
  node [
    id 1414
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1415
    label "Warmia"
  ]
  node [
    id 1416
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1417
    label "Suwalszczyzna"
  ]
  node [
    id 1418
    label "Bory_Tucholskie"
  ]
  node [
    id 1419
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1420
    label "z&#322;oty"
  ]
  node [
    id 1421
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1422
    label "Ma&#322;opolska"
  ]
  node [
    id 1423
    label "Mazury"
  ]
  node [
    id 1424
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1425
    label "Powi&#347;le"
  ]
  node [
    id 1426
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1427
    label "Azja_Mniejsza"
  ]
  node [
    id 1428
    label "lira_turecka"
  ]
  node [
    id 1429
    label "Ujgur"
  ]
  node [
    id 1430
    label "kuna"
  ]
  node [
    id 1431
    label "dram"
  ]
  node [
    id 1432
    label "tala"
  ]
  node [
    id 1433
    label "korona_s&#322;owacka"
  ]
  node [
    id 1434
    label "Turiec"
  ]
  node [
    id 1435
    label "rupia_nepalska"
  ]
  node [
    id 1436
    label "Himalaje"
  ]
  node [
    id 1437
    label "frank_gwinejski"
  ]
  node [
    id 1438
    label "marka_esto&#324;ska"
  ]
  node [
    id 1439
    label "korona_esto&#324;ska"
  ]
  node [
    id 1440
    label "Skandynawia"
  ]
  node [
    id 1441
    label "Nowa_Fundlandia"
  ]
  node [
    id 1442
    label "Quebec"
  ]
  node [
    id 1443
    label "dolar_kanadyjski"
  ]
  node [
    id 1444
    label "Zanzibar"
  ]
  node [
    id 1445
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1446
    label "&#346;wite&#378;"
  ]
  node [
    id 1447
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1448
    label "peso_kolumbijskie"
  ]
  node [
    id 1449
    label "funt_egipski"
  ]
  node [
    id 1450
    label "Synaj"
  ]
  node [
    id 1451
    label "paraszyt"
  ]
  node [
    id 1452
    label "afgani"
  ]
  node [
    id 1453
    label "Baktria"
  ]
  node [
    id 1454
    label "szach"
  ]
  node [
    id 1455
    label "baht"
  ]
  node [
    id 1456
    label "tolar"
  ]
  node [
    id 1457
    label "Naddniestrze"
  ]
  node [
    id 1458
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1459
    label "Gagauzja"
  ]
  node [
    id 1460
    label "posadzka"
  ]
  node [
    id 1461
    label "podglebie"
  ]
  node [
    id 1462
    label "Ko&#322;yma"
  ]
  node [
    id 1463
    label "Indochiny"
  ]
  node [
    id 1464
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1465
    label "Bo&#347;nia"
  ]
  node [
    id 1466
    label "Kaukaz"
  ]
  node [
    id 1467
    label "Opolszczyzna"
  ]
  node [
    id 1468
    label "czynnik_produkcji"
  ]
  node [
    id 1469
    label "kort"
  ]
  node [
    id 1470
    label "Polesie"
  ]
  node [
    id 1471
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1472
    label "Yorkshire"
  ]
  node [
    id 1473
    label "zapadnia"
  ]
  node [
    id 1474
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1475
    label "Noworosja"
  ]
  node [
    id 1476
    label "glinowa&#263;"
  ]
  node [
    id 1477
    label "litosfera"
  ]
  node [
    id 1478
    label "Kurpie"
  ]
  node [
    id 1479
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1480
    label "Kociewie"
  ]
  node [
    id 1481
    label "Anglia"
  ]
  node [
    id 1482
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1483
    label "Laponia"
  ]
  node [
    id 1484
    label "Amazonia"
  ]
  node [
    id 1485
    label "Hercegowina"
  ]
  node [
    id 1486
    label "przestrze&#324;"
  ]
  node [
    id 1487
    label "Pamir"
  ]
  node [
    id 1488
    label "powierzchnia"
  ]
  node [
    id 1489
    label "p&#322;aszczyzna"
  ]
  node [
    id 1490
    label "Podhale"
  ]
  node [
    id 1491
    label "pomieszczenie"
  ]
  node [
    id 1492
    label "plantowa&#263;"
  ]
  node [
    id 1493
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1494
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1495
    label "dotleni&#263;"
  ]
  node [
    id 1496
    label "Zabajkale"
  ]
  node [
    id 1497
    label "skorupa_ziemska"
  ]
  node [
    id 1498
    label "glinowanie"
  ]
  node [
    id 1499
    label "Kaszuby"
  ]
  node [
    id 1500
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1501
    label "Oksytania"
  ]
  node [
    id 1502
    label "Mezoameryka"
  ]
  node [
    id 1503
    label "Turkiestan"
  ]
  node [
    id 1504
    label "Kurdystan"
  ]
  node [
    id 1505
    label "glej"
  ]
  node [
    id 1506
    label "Biskupizna"
  ]
  node [
    id 1507
    label "Podbeskidzie"
  ]
  node [
    id 1508
    label "Zag&#243;rze"
  ]
  node [
    id 1509
    label "Szkocja"
  ]
  node [
    id 1510
    label "domain"
  ]
  node [
    id 1511
    label "Huculszczyzna"
  ]
  node [
    id 1512
    label "pojazd"
  ]
  node [
    id 1513
    label "budynek"
  ]
  node [
    id 1514
    label "S&#261;decczyzna"
  ]
  node [
    id 1515
    label "Palestyna"
  ]
  node [
    id 1516
    label "Lauda"
  ]
  node [
    id 1517
    label "penetrator"
  ]
  node [
    id 1518
    label "Bojkowszczyzna"
  ]
  node [
    id 1519
    label "ryzosfera"
  ]
  node [
    id 1520
    label "Zamojszczyzna"
  ]
  node [
    id 1521
    label "Walia"
  ]
  node [
    id 1522
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1523
    label "martwica"
  ]
  node [
    id 1524
    label "pr&#243;chnica"
  ]
  node [
    id 1525
    label "chemikalia"
  ]
  node [
    id 1526
    label "abstrakcja"
  ]
  node [
    id 1527
    label "substancja"
  ]
  node [
    id 1528
    label "chronometria"
  ]
  node [
    id 1529
    label "odczyt"
  ]
  node [
    id 1530
    label "laba"
  ]
  node [
    id 1531
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1532
    label "time_period"
  ]
  node [
    id 1533
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1534
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1535
    label "Zeitgeist"
  ]
  node [
    id 1536
    label "pochodzenie"
  ]
  node [
    id 1537
    label "schy&#322;ek"
  ]
  node [
    id 1538
    label "czwarty_wymiar"
  ]
  node [
    id 1539
    label "kategoria_gramatyczna"
  ]
  node [
    id 1540
    label "poprzedzi&#263;"
  ]
  node [
    id 1541
    label "pogoda"
  ]
  node [
    id 1542
    label "czasokres"
  ]
  node [
    id 1543
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1544
    label "poprzedzenie"
  ]
  node [
    id 1545
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1546
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1547
    label "dzieje"
  ]
  node [
    id 1548
    label "zegar"
  ]
  node [
    id 1549
    label "koniugacja"
  ]
  node [
    id 1550
    label "trawi&#263;"
  ]
  node [
    id 1551
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1552
    label "poprzedza&#263;"
  ]
  node [
    id 1553
    label "trawienie"
  ]
  node [
    id 1554
    label "chwila"
  ]
  node [
    id 1555
    label "rachuba_czasu"
  ]
  node [
    id 1556
    label "poprzedzanie"
  ]
  node [
    id 1557
    label "okres_czasu"
  ]
  node [
    id 1558
    label "period"
  ]
  node [
    id 1559
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1560
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1561
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1562
    label "pochodzi&#263;"
  ]
  node [
    id 1563
    label "model"
  ]
  node [
    id 1564
    label "nature"
  ]
  node [
    id 1565
    label "tryb"
  ]
  node [
    id 1566
    label "obiekt_matematyczny"
  ]
  node [
    id 1567
    label "stopie&#324;_pisma"
  ]
  node [
    id 1568
    label "pozycja"
  ]
  node [
    id 1569
    label "problemat"
  ]
  node [
    id 1570
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1571
    label "obiekt"
  ]
  node [
    id 1572
    label "plamka"
  ]
  node [
    id 1573
    label "ust&#281;p"
  ]
  node [
    id 1574
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1575
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1576
    label "kres"
  ]
  node [
    id 1577
    label "plan"
  ]
  node [
    id 1578
    label "podpunkt"
  ]
  node [
    id 1579
    label "jednostka"
  ]
  node [
    id 1580
    label "sprawa"
  ]
  node [
    id 1581
    label "problematyka"
  ]
  node [
    id 1582
    label "prosta"
  ]
  node [
    id 1583
    label "wojsko"
  ]
  node [
    id 1584
    label "zapunktowa&#263;"
  ]
  node [
    id 1585
    label "rz&#261;d"
  ]
  node [
    id 1586
    label "uwaga"
  ]
  node [
    id 1587
    label "cecha"
  ]
  node [
    id 1588
    label "plac"
  ]
  node [
    id 1589
    label "location"
  ]
  node [
    id 1590
    label "warunek_lokalowy"
  ]
  node [
    id 1591
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1592
    label "cia&#322;o"
  ]
  node [
    id 1593
    label "status"
  ]
  node [
    id 1594
    label "temperatura_krytyczna"
  ]
  node [
    id 1595
    label "materia"
  ]
  node [
    id 1596
    label "smolisty"
  ]
  node [
    id 1597
    label "byt"
  ]
  node [
    id 1598
    label "przenika&#263;"
  ]
  node [
    id 1599
    label "przenikanie"
  ]
  node [
    id 1600
    label "proces_my&#347;lowy"
  ]
  node [
    id 1601
    label "abstractedness"
  ]
  node [
    id 1602
    label "obraz"
  ]
  node [
    id 1603
    label "abstraction"
  ]
  node [
    id 1604
    label "spalanie"
  ]
  node [
    id 1605
    label "spalenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 242
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 12
    target 1077
  ]
  edge [
    source 12
    target 1078
  ]
  edge [
    source 12
    target 1079
  ]
  edge [
    source 12
    target 1080
  ]
  edge [
    source 12
    target 1081
  ]
  edge [
    source 12
    target 1082
  ]
  edge [
    source 12
    target 1083
  ]
  edge [
    source 12
    target 1084
  ]
  edge [
    source 12
    target 1085
  ]
  edge [
    source 12
    target 1086
  ]
  edge [
    source 12
    target 1087
  ]
  edge [
    source 12
    target 1088
  ]
  edge [
    source 12
    target 1089
  ]
  edge [
    source 12
    target 1090
  ]
  edge [
    source 12
    target 1091
  ]
  edge [
    source 12
    target 1092
  ]
  edge [
    source 12
    target 1093
  ]
  edge [
    source 12
    target 1094
  ]
  edge [
    source 12
    target 1095
  ]
  edge [
    source 12
    target 1096
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 1098
  ]
  edge [
    source 12
    target 1099
  ]
  edge [
    source 12
    target 1100
  ]
  edge [
    source 12
    target 1101
  ]
  edge [
    source 12
    target 1102
  ]
  edge [
    source 12
    target 1103
  ]
  edge [
    source 12
    target 1104
  ]
  edge [
    source 12
    target 1105
  ]
  edge [
    source 12
    target 1106
  ]
  edge [
    source 12
    target 1107
  ]
  edge [
    source 12
    target 1108
  ]
  edge [
    source 12
    target 1109
  ]
  edge [
    source 12
    target 1110
  ]
  edge [
    source 12
    target 1111
  ]
  edge [
    source 12
    target 1112
  ]
  edge [
    source 12
    target 1113
  ]
  edge [
    source 12
    target 1114
  ]
  edge [
    source 12
    target 1115
  ]
  edge [
    source 12
    target 1116
  ]
  edge [
    source 12
    target 1117
  ]
  edge [
    source 12
    target 1118
  ]
  edge [
    source 12
    target 1119
  ]
  edge [
    source 12
    target 1120
  ]
  edge [
    source 12
    target 1121
  ]
  edge [
    source 12
    target 1122
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 1123
  ]
  edge [
    source 12
    target 1124
  ]
  edge [
    source 12
    target 1125
  ]
  edge [
    source 12
    target 1126
  ]
  edge [
    source 12
    target 1127
  ]
  edge [
    source 12
    target 1128
  ]
  edge [
    source 12
    target 1129
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1131
  ]
  edge [
    source 12
    target 1132
  ]
  edge [
    source 12
    target 1133
  ]
  edge [
    source 12
    target 1134
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1153
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1155
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1165
  ]
  edge [
    source 12
    target 1166
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 1229
  ]
  edge [
    source 12
    target 1230
  ]
  edge [
    source 12
    target 1231
  ]
  edge [
    source 12
    target 1232
  ]
  edge [
    source 12
    target 1233
  ]
  edge [
    source 12
    target 1234
  ]
  edge [
    source 12
    target 1235
  ]
  edge [
    source 12
    target 1236
  ]
  edge [
    source 12
    target 1237
  ]
  edge [
    source 12
    target 1238
  ]
  edge [
    source 12
    target 1239
  ]
  edge [
    source 12
    target 1240
  ]
  edge [
    source 12
    target 1241
  ]
  edge [
    source 12
    target 1242
  ]
  edge [
    source 12
    target 1243
  ]
  edge [
    source 12
    target 1244
  ]
  edge [
    source 12
    target 1245
  ]
  edge [
    source 12
    target 1246
  ]
  edge [
    source 12
    target 1247
  ]
  edge [
    source 12
    target 1248
  ]
  edge [
    source 12
    target 1249
  ]
  edge [
    source 12
    target 1250
  ]
  edge [
    source 12
    target 1251
  ]
  edge [
    source 12
    target 1252
  ]
  edge [
    source 12
    target 1253
  ]
  edge [
    source 12
    target 1254
  ]
  edge [
    source 12
    target 1255
  ]
  edge [
    source 12
    target 1256
  ]
  edge [
    source 12
    target 1257
  ]
  edge [
    source 12
    target 1258
  ]
  edge [
    source 12
    target 1259
  ]
  edge [
    source 12
    target 1260
  ]
  edge [
    source 12
    target 1261
  ]
  edge [
    source 12
    target 1262
  ]
  edge [
    source 12
    target 1263
  ]
  edge [
    source 12
    target 1264
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 12
    target 1309
  ]
  edge [
    source 12
    target 1310
  ]
  edge [
    source 12
    target 1311
  ]
  edge [
    source 12
    target 1312
  ]
  edge [
    source 12
    target 1313
  ]
  edge [
    source 12
    target 1314
  ]
  edge [
    source 12
    target 1315
  ]
  edge [
    source 12
    target 1316
  ]
  edge [
    source 12
    target 1317
  ]
  edge [
    source 12
    target 1318
  ]
  edge [
    source 12
    target 1319
  ]
  edge [
    source 12
    target 1320
  ]
  edge [
    source 12
    target 1321
  ]
  edge [
    source 12
    target 1322
  ]
  edge [
    source 12
    target 1323
  ]
  edge [
    source 12
    target 1324
  ]
  edge [
    source 12
    target 1325
  ]
  edge [
    source 12
    target 1326
  ]
  edge [
    source 12
    target 1327
  ]
  edge [
    source 12
    target 1328
  ]
  edge [
    source 12
    target 1329
  ]
  edge [
    source 12
    target 1330
  ]
  edge [
    source 12
    target 1331
  ]
  edge [
    source 12
    target 1332
  ]
  edge [
    source 12
    target 1333
  ]
  edge [
    source 12
    target 1334
  ]
  edge [
    source 12
    target 1335
  ]
  edge [
    source 12
    target 1336
  ]
  edge [
    source 12
    target 1337
  ]
  edge [
    source 12
    target 1338
  ]
  edge [
    source 12
    target 1339
  ]
  edge [
    source 12
    target 1340
  ]
  edge [
    source 12
    target 1341
  ]
  edge [
    source 12
    target 1342
  ]
  edge [
    source 12
    target 1343
  ]
  edge [
    source 12
    target 1344
  ]
  edge [
    source 12
    target 1345
  ]
  edge [
    source 12
    target 1346
  ]
  edge [
    source 12
    target 1347
  ]
  edge [
    source 12
    target 1348
  ]
  edge [
    source 12
    target 1349
  ]
  edge [
    source 12
    target 1350
  ]
  edge [
    source 12
    target 1351
  ]
  edge [
    source 12
    target 1352
  ]
  edge [
    source 12
    target 1353
  ]
  edge [
    source 12
    target 1354
  ]
  edge [
    source 12
    target 1355
  ]
  edge [
    source 12
    target 1356
  ]
  edge [
    source 12
    target 1357
  ]
  edge [
    source 12
    target 1358
  ]
  edge [
    source 12
    target 1359
  ]
  edge [
    source 12
    target 1360
  ]
  edge [
    source 12
    target 1361
  ]
  edge [
    source 12
    target 1362
  ]
  edge [
    source 12
    target 1363
  ]
  edge [
    source 12
    target 1364
  ]
  edge [
    source 12
    target 1365
  ]
  edge [
    source 12
    target 1366
  ]
  edge [
    source 12
    target 1367
  ]
  edge [
    source 12
    target 1368
  ]
  edge [
    source 12
    target 1369
  ]
  edge [
    source 12
    target 1370
  ]
  edge [
    source 12
    target 1371
  ]
  edge [
    source 12
    target 1372
  ]
  edge [
    source 12
    target 1373
  ]
  edge [
    source 12
    target 1374
  ]
  edge [
    source 12
    target 1375
  ]
  edge [
    source 12
    target 1376
  ]
  edge [
    source 12
    target 1377
  ]
  edge [
    source 12
    target 1378
  ]
  edge [
    source 12
    target 1379
  ]
  edge [
    source 12
    target 1380
  ]
  edge [
    source 12
    target 1381
  ]
  edge [
    source 12
    target 1382
  ]
  edge [
    source 12
    target 1383
  ]
  edge [
    source 12
    target 1384
  ]
  edge [
    source 12
    target 1385
  ]
  edge [
    source 12
    target 1386
  ]
  edge [
    source 12
    target 1387
  ]
  edge [
    source 12
    target 1388
  ]
  edge [
    source 12
    target 1389
  ]
  edge [
    source 12
    target 1390
  ]
  edge [
    source 12
    target 1391
  ]
  edge [
    source 12
    target 1392
  ]
  edge [
    source 12
    target 1393
  ]
  edge [
    source 12
    target 1394
  ]
  edge [
    source 12
    target 1395
  ]
  edge [
    source 12
    target 1396
  ]
  edge [
    source 12
    target 1397
  ]
  edge [
    source 12
    target 1398
  ]
  edge [
    source 12
    target 1399
  ]
  edge [
    source 12
    target 1400
  ]
  edge [
    source 12
    target 1401
  ]
  edge [
    source 12
    target 1402
  ]
  edge [
    source 12
    target 1403
  ]
  edge [
    source 12
    target 1404
  ]
  edge [
    source 12
    target 1405
  ]
  edge [
    source 12
    target 1406
  ]
  edge [
    source 12
    target 1407
  ]
  edge [
    source 12
    target 1408
  ]
  edge [
    source 12
    target 1409
  ]
  edge [
    source 12
    target 1410
  ]
  edge [
    source 12
    target 1411
  ]
  edge [
    source 12
    target 1412
  ]
  edge [
    source 12
    target 1413
  ]
  edge [
    source 12
    target 1414
  ]
  edge [
    source 12
    target 1415
  ]
  edge [
    source 12
    target 1416
  ]
  edge [
    source 12
    target 1417
  ]
  edge [
    source 12
    target 1418
  ]
  edge [
    source 12
    target 1419
  ]
  edge [
    source 12
    target 1420
  ]
  edge [
    source 12
    target 1421
  ]
  edge [
    source 12
    target 1422
  ]
  edge [
    source 12
    target 1423
  ]
  edge [
    source 12
    target 1424
  ]
  edge [
    source 12
    target 1425
  ]
  edge [
    source 12
    target 1426
  ]
  edge [
    source 12
    target 1427
  ]
  edge [
    source 12
    target 1428
  ]
  edge [
    source 12
    target 1429
  ]
  edge [
    source 12
    target 1430
  ]
  edge [
    source 12
    target 1431
  ]
  edge [
    source 12
    target 1432
  ]
  edge [
    source 12
    target 1433
  ]
  edge [
    source 12
    target 1434
  ]
  edge [
    source 12
    target 1435
  ]
  edge [
    source 12
    target 1436
  ]
  edge [
    source 12
    target 1437
  ]
  edge [
    source 12
    target 1438
  ]
  edge [
    source 12
    target 1439
  ]
  edge [
    source 12
    target 1440
  ]
  edge [
    source 12
    target 1441
  ]
  edge [
    source 12
    target 1442
  ]
  edge [
    source 12
    target 1443
  ]
  edge [
    source 12
    target 1444
  ]
  edge [
    source 12
    target 1445
  ]
  edge [
    source 12
    target 1446
  ]
  edge [
    source 12
    target 1447
  ]
  edge [
    source 12
    target 1448
  ]
  edge [
    source 12
    target 1449
  ]
  edge [
    source 12
    target 1450
  ]
  edge [
    source 12
    target 1451
  ]
  edge [
    source 12
    target 1452
  ]
  edge [
    source 12
    target 1453
  ]
  edge [
    source 12
    target 1454
  ]
  edge [
    source 12
    target 1455
  ]
  edge [
    source 12
    target 1456
  ]
  edge [
    source 12
    target 1457
  ]
  edge [
    source 12
    target 1458
  ]
  edge [
    source 12
    target 1459
  ]
  edge [
    source 12
    target 1460
  ]
  edge [
    source 12
    target 1461
  ]
  edge [
    source 12
    target 1462
  ]
  edge [
    source 12
    target 1463
  ]
  edge [
    source 12
    target 1464
  ]
  edge [
    source 12
    target 1465
  ]
  edge [
    source 12
    target 1466
  ]
  edge [
    source 12
    target 1467
  ]
  edge [
    source 12
    target 1468
  ]
  edge [
    source 12
    target 1469
  ]
  edge [
    source 12
    target 1470
  ]
  edge [
    source 12
    target 1471
  ]
  edge [
    source 12
    target 1472
  ]
  edge [
    source 12
    target 1473
  ]
  edge [
    source 12
    target 1474
  ]
  edge [
    source 12
    target 1475
  ]
  edge [
    source 12
    target 1476
  ]
  edge [
    source 12
    target 1477
  ]
  edge [
    source 12
    target 1478
  ]
  edge [
    source 12
    target 1479
  ]
  edge [
    source 12
    target 1480
  ]
  edge [
    source 12
    target 1481
  ]
  edge [
    source 12
    target 1482
  ]
  edge [
    source 12
    target 1483
  ]
  edge [
    source 12
    target 1484
  ]
  edge [
    source 12
    target 1485
  ]
  edge [
    source 12
    target 1486
  ]
  edge [
    source 12
    target 1487
  ]
  edge [
    source 12
    target 1488
  ]
  edge [
    source 12
    target 1489
  ]
  edge [
    source 12
    target 1490
  ]
  edge [
    source 12
    target 1491
  ]
  edge [
    source 12
    target 1492
  ]
  edge [
    source 12
    target 1493
  ]
  edge [
    source 12
    target 1494
  ]
  edge [
    source 12
    target 1495
  ]
  edge [
    source 12
    target 1496
  ]
  edge [
    source 12
    target 1497
  ]
  edge [
    source 12
    target 1498
  ]
  edge [
    source 12
    target 1499
  ]
  edge [
    source 12
    target 1500
  ]
  edge [
    source 12
    target 1501
  ]
  edge [
    source 12
    target 1502
  ]
  edge [
    source 12
    target 1503
  ]
  edge [
    source 12
    target 1504
  ]
  edge [
    source 12
    target 1505
  ]
  edge [
    source 12
    target 1506
  ]
  edge [
    source 12
    target 1507
  ]
  edge [
    source 12
    target 1508
  ]
  edge [
    source 12
    target 1509
  ]
  edge [
    source 12
    target 1510
  ]
  edge [
    source 12
    target 1511
  ]
  edge [
    source 12
    target 1512
  ]
  edge [
    source 12
    target 1513
  ]
  edge [
    source 12
    target 1514
  ]
  edge [
    source 12
    target 1515
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 1516
  ]
  edge [
    source 12
    target 1517
  ]
  edge [
    source 12
    target 1518
  ]
  edge [
    source 12
    target 1519
  ]
  edge [
    source 12
    target 1520
  ]
  edge [
    source 12
    target 1521
  ]
  edge [
    source 12
    target 1522
  ]
  edge [
    source 12
    target 1523
  ]
  edge [
    source 12
    target 1524
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 1525
  ]
  edge [
    source 13
    target 1526
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 1527
  ]
  edge [
    source 13
    target 1528
  ]
  edge [
    source 13
    target 1529
  ]
  edge [
    source 13
    target 1530
  ]
  edge [
    source 13
    target 1531
  ]
  edge [
    source 13
    target 1532
  ]
  edge [
    source 13
    target 1533
  ]
  edge [
    source 13
    target 1534
  ]
  edge [
    source 13
    target 1535
  ]
  edge [
    source 13
    target 1536
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 1537
  ]
  edge [
    source 13
    target 1538
  ]
  edge [
    source 13
    target 1539
  ]
  edge [
    source 13
    target 1540
  ]
  edge [
    source 13
    target 1541
  ]
  edge [
    source 13
    target 1542
  ]
  edge [
    source 13
    target 1543
  ]
  edge [
    source 13
    target 1544
  ]
  edge [
    source 13
    target 1545
  ]
  edge [
    source 13
    target 1546
  ]
  edge [
    source 13
    target 1547
  ]
  edge [
    source 13
    target 1548
  ]
  edge [
    source 13
    target 1549
  ]
  edge [
    source 13
    target 1550
  ]
  edge [
    source 13
    target 1551
  ]
  edge [
    source 13
    target 1552
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 1553
  ]
  edge [
    source 13
    target 1554
  ]
  edge [
    source 13
    target 1555
  ]
  edge [
    source 13
    target 1556
  ]
  edge [
    source 13
    target 1557
  ]
  edge [
    source 13
    target 1558
  ]
  edge [
    source 13
    target 1559
  ]
  edge [
    source 13
    target 1560
  ]
  edge [
    source 13
    target 1561
  ]
  edge [
    source 13
    target 1562
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 1563
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 1564
  ]
  edge [
    source 13
    target 1565
  ]
  edge [
    source 13
    target 1566
  ]
  edge [
    source 13
    target 1567
  ]
  edge [
    source 13
    target 1568
  ]
  edge [
    source 13
    target 1569
  ]
  edge [
    source 13
    target 1570
  ]
  edge [
    source 13
    target 1571
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 1572
  ]
  edge [
    source 13
    target 1486
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 1573
  ]
  edge [
    source 13
    target 1574
  ]
  edge [
    source 13
    target 1575
  ]
  edge [
    source 13
    target 1576
  ]
  edge [
    source 13
    target 1577
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 1578
  ]
  edge [
    source 13
    target 1579
  ]
  edge [
    source 13
    target 1580
  ]
  edge [
    source 13
    target 1581
  ]
  edge [
    source 13
    target 1582
  ]
  edge [
    source 13
    target 1583
  ]
  edge [
    source 13
    target 1584
  ]
  edge [
    source 13
    target 1585
  ]
  edge [
    source 13
    target 1586
  ]
  edge [
    source 13
    target 1587
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 1588
  ]
  edge [
    source 13
    target 1589
  ]
  edge [
    source 13
    target 1590
  ]
  edge [
    source 13
    target 1591
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 1592
  ]
  edge [
    source 13
    target 1593
  ]
  edge [
    source 13
    target 1594
  ]
  edge [
    source 13
    target 1595
  ]
  edge [
    source 13
    target 1596
  ]
  edge [
    source 13
    target 1597
  ]
  edge [
    source 13
    target 1598
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 1599
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 1600
  ]
  edge [
    source 13
    target 1601
  ]
  edge [
    source 13
    target 1602
  ]
  edge [
    source 13
    target 1603
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 1604
  ]
  edge [
    source 13
    target 1605
  ]
]
