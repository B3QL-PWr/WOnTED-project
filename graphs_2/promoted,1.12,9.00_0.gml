graph [
  node [
    id 0
    label "zemrze&#263;"
    origin "text"
  ]
  node [
    id 1
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "prezydent"
    origin "text"
  ]
  node [
    id 3
    label "stany"
    origin "text"
  ]
  node [
    id 4
    label "zjednoczy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "george"
    origin "text"
  ]
  node [
    id 6
    label "bush"
    origin "text"
  ]
  node [
    id 7
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "usa"
    origin "text"
  ]
  node [
    id 9
    label "today"
    origin "text"
  ]
  node [
    id 10
    label "umrze&#263;"
  ]
  node [
    id 11
    label "znikn&#261;&#263;"
  ]
  node [
    id 12
    label "die"
  ]
  node [
    id 13
    label "pa&#347;&#263;"
  ]
  node [
    id 14
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 15
    label "przesta&#263;"
  ]
  node [
    id 16
    label "dawny"
  ]
  node [
    id 17
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 18
    label "eksprezydent"
  ]
  node [
    id 19
    label "partner"
  ]
  node [
    id 20
    label "rozw&#243;d"
  ]
  node [
    id 21
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 22
    label "wcze&#347;niejszy"
  ]
  node [
    id 23
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 24
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 25
    label "pracownik"
  ]
  node [
    id 26
    label "przedsi&#281;biorca"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 29
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 30
    label "kolaborator"
  ]
  node [
    id 31
    label "prowadzi&#263;"
  ]
  node [
    id 32
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 33
    label "sp&#243;lnik"
  ]
  node [
    id 34
    label "aktor"
  ]
  node [
    id 35
    label "uczestniczenie"
  ]
  node [
    id 36
    label "wcze&#347;niej"
  ]
  node [
    id 37
    label "przestarza&#322;y"
  ]
  node [
    id 38
    label "odleg&#322;y"
  ]
  node [
    id 39
    label "przesz&#322;y"
  ]
  node [
    id 40
    label "od_dawna"
  ]
  node [
    id 41
    label "poprzedni"
  ]
  node [
    id 42
    label "dawno"
  ]
  node [
    id 43
    label "d&#322;ugoletni"
  ]
  node [
    id 44
    label "anachroniczny"
  ]
  node [
    id 45
    label "dawniej"
  ]
  node [
    id 46
    label "niegdysiejszy"
  ]
  node [
    id 47
    label "kombatant"
  ]
  node [
    id 48
    label "stary"
  ]
  node [
    id 49
    label "rozstanie"
  ]
  node [
    id 50
    label "ekspartner"
  ]
  node [
    id 51
    label "rozbita_rodzina"
  ]
  node [
    id 52
    label "uniewa&#380;nienie"
  ]
  node [
    id 53
    label "separation"
  ]
  node [
    id 54
    label "gruba_ryba"
  ]
  node [
    id 55
    label "Gorbaczow"
  ]
  node [
    id 56
    label "zwierzchnik"
  ]
  node [
    id 57
    label "Putin"
  ]
  node [
    id 58
    label "Tito"
  ]
  node [
    id 59
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 60
    label "Naser"
  ]
  node [
    id 61
    label "de_Gaulle"
  ]
  node [
    id 62
    label "Nixon"
  ]
  node [
    id 63
    label "Kemal"
  ]
  node [
    id 64
    label "Clinton"
  ]
  node [
    id 65
    label "Bierut"
  ]
  node [
    id 66
    label "Roosevelt"
  ]
  node [
    id 67
    label "samorz&#261;dowiec"
  ]
  node [
    id 68
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 69
    label "Jelcyn"
  ]
  node [
    id 70
    label "dostojnik"
  ]
  node [
    id 71
    label "pryncypa&#322;"
  ]
  node [
    id 72
    label "kierowa&#263;"
  ]
  node [
    id 73
    label "kierownictwo"
  ]
  node [
    id 74
    label "urz&#281;dnik"
  ]
  node [
    id 75
    label "notabl"
  ]
  node [
    id 76
    label "oficja&#322;"
  ]
  node [
    id 77
    label "samorz&#261;d"
  ]
  node [
    id 78
    label "polityk"
  ]
  node [
    id 79
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 80
    label "komuna"
  ]
  node [
    id 81
    label "consort"
  ]
  node [
    id 82
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 83
    label "stworzy&#263;"
  ]
  node [
    id 84
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 85
    label "incorporate"
  ]
  node [
    id 86
    label "zrobi&#263;"
  ]
  node [
    id 87
    label "connect"
  ]
  node [
    id 88
    label "spowodowa&#263;"
  ]
  node [
    id 89
    label "relate"
  ]
  node [
    id 90
    label "po&#322;&#261;czenie"
  ]
  node [
    id 91
    label "tenis"
  ]
  node [
    id 92
    label "deal"
  ]
  node [
    id 93
    label "dawa&#263;"
  ]
  node [
    id 94
    label "stawia&#263;"
  ]
  node [
    id 95
    label "rozgrywa&#263;"
  ]
  node [
    id 96
    label "kelner"
  ]
  node [
    id 97
    label "siatk&#243;wka"
  ]
  node [
    id 98
    label "cover"
  ]
  node [
    id 99
    label "tender"
  ]
  node [
    id 100
    label "jedzenie"
  ]
  node [
    id 101
    label "faszerowa&#263;"
  ]
  node [
    id 102
    label "introduce"
  ]
  node [
    id 103
    label "informowa&#263;"
  ]
  node [
    id 104
    label "serwowa&#263;"
  ]
  node [
    id 105
    label "powiada&#263;"
  ]
  node [
    id 106
    label "komunikowa&#263;"
  ]
  node [
    id 107
    label "inform"
  ]
  node [
    id 108
    label "zaczyna&#263;"
  ]
  node [
    id 109
    label "pi&#322;ka"
  ]
  node [
    id 110
    label "give"
  ]
  node [
    id 111
    label "przeprowadza&#263;"
  ]
  node [
    id 112
    label "gra&#263;"
  ]
  node [
    id 113
    label "play"
  ]
  node [
    id 114
    label "robi&#263;"
  ]
  node [
    id 115
    label "przekazywa&#263;"
  ]
  node [
    id 116
    label "dostarcza&#263;"
  ]
  node [
    id 117
    label "mie&#263;_miejsce"
  ]
  node [
    id 118
    label "&#322;adowa&#263;"
  ]
  node [
    id 119
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 120
    label "przeznacza&#263;"
  ]
  node [
    id 121
    label "surrender"
  ]
  node [
    id 122
    label "traktowa&#263;"
  ]
  node [
    id 123
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 124
    label "obiecywa&#263;"
  ]
  node [
    id 125
    label "odst&#281;powa&#263;"
  ]
  node [
    id 126
    label "rap"
  ]
  node [
    id 127
    label "umieszcza&#263;"
  ]
  node [
    id 128
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 129
    label "t&#322;uc"
  ]
  node [
    id 130
    label "powierza&#263;"
  ]
  node [
    id 131
    label "render"
  ]
  node [
    id 132
    label "wpiernicza&#263;"
  ]
  node [
    id 133
    label "exsert"
  ]
  node [
    id 134
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 135
    label "train"
  ]
  node [
    id 136
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 137
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 138
    label "p&#322;aci&#263;"
  ]
  node [
    id 139
    label "hold_out"
  ]
  node [
    id 140
    label "nalewa&#263;"
  ]
  node [
    id 141
    label "zezwala&#263;"
  ]
  node [
    id 142
    label "hold"
  ]
  node [
    id 143
    label "pozostawia&#263;"
  ]
  node [
    id 144
    label "czyni&#263;"
  ]
  node [
    id 145
    label "wydawa&#263;"
  ]
  node [
    id 146
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 147
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 148
    label "raise"
  ]
  node [
    id 149
    label "przewidywa&#263;"
  ]
  node [
    id 150
    label "przyznawa&#263;"
  ]
  node [
    id 151
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 152
    label "go"
  ]
  node [
    id 153
    label "obstawia&#263;"
  ]
  node [
    id 154
    label "ocenia&#263;"
  ]
  node [
    id 155
    label "zastawia&#263;"
  ]
  node [
    id 156
    label "stanowisko"
  ]
  node [
    id 157
    label "znak"
  ]
  node [
    id 158
    label "wskazywa&#263;"
  ]
  node [
    id 159
    label "uruchamia&#263;"
  ]
  node [
    id 160
    label "wytwarza&#263;"
  ]
  node [
    id 161
    label "fundowa&#263;"
  ]
  node [
    id 162
    label "zmienia&#263;"
  ]
  node [
    id 163
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 164
    label "deliver"
  ]
  node [
    id 165
    label "powodowa&#263;"
  ]
  node [
    id 166
    label "wyznacza&#263;"
  ]
  node [
    id 167
    label "przedstawia&#263;"
  ]
  node [
    id 168
    label "wydobywa&#263;"
  ]
  node [
    id 169
    label "charge"
  ]
  node [
    id 170
    label "nadziewa&#263;"
  ]
  node [
    id 171
    label "przesadza&#263;"
  ]
  node [
    id 172
    label "pelota"
  ]
  node [
    id 173
    label "sport_rakietowy"
  ]
  node [
    id 174
    label "&#347;cina&#263;"
  ]
  node [
    id 175
    label "wolej"
  ]
  node [
    id 176
    label "&#347;cinanie"
  ]
  node [
    id 177
    label "supervisor"
  ]
  node [
    id 178
    label "ubrani&#243;wka"
  ]
  node [
    id 179
    label "singlista"
  ]
  node [
    id 180
    label "lobowanie"
  ]
  node [
    id 181
    label "bekhend"
  ]
  node [
    id 182
    label "lobowa&#263;"
  ]
  node [
    id 183
    label "forhend"
  ]
  node [
    id 184
    label "p&#243;&#322;wolej"
  ]
  node [
    id 185
    label "pr&#261;&#380;kowany"
  ]
  node [
    id 186
    label "singlowy"
  ]
  node [
    id 187
    label "tkanina_we&#322;niana"
  ]
  node [
    id 188
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 189
    label "&#347;ci&#281;cie"
  ]
  node [
    id 190
    label "deblowy"
  ]
  node [
    id 191
    label "tkanina"
  ]
  node [
    id 192
    label "mikst"
  ]
  node [
    id 193
    label "slajs"
  ]
  node [
    id 194
    label "poda&#263;"
  ]
  node [
    id 195
    label "podawanie"
  ]
  node [
    id 196
    label "podanie"
  ]
  node [
    id 197
    label "deblista"
  ]
  node [
    id 198
    label "miksista"
  ]
  node [
    id 199
    label "Wimbledon"
  ]
  node [
    id 200
    label "blok"
  ]
  node [
    id 201
    label "cia&#322;o_szkliste"
  ]
  node [
    id 202
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 203
    label "retinopatia"
  ]
  node [
    id 204
    label "zeaksantyna"
  ]
  node [
    id 205
    label "przelobowa&#263;"
  ]
  node [
    id 206
    label "dno_oka"
  ]
  node [
    id 207
    label "przelobowanie"
  ]
  node [
    id 208
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 209
    label "ober"
  ]
  node [
    id 210
    label "zatruwanie_si&#281;"
  ]
  node [
    id 211
    label "przejadanie_si&#281;"
  ]
  node [
    id 212
    label "szama"
  ]
  node [
    id 213
    label "koryto"
  ]
  node [
    id 214
    label "rzecz"
  ]
  node [
    id 215
    label "odpasanie_si&#281;"
  ]
  node [
    id 216
    label "eating"
  ]
  node [
    id 217
    label "jadanie"
  ]
  node [
    id 218
    label "posilenie"
  ]
  node [
    id 219
    label "wpieprzanie"
  ]
  node [
    id 220
    label "wmuszanie"
  ]
  node [
    id 221
    label "robienie"
  ]
  node [
    id 222
    label "wiwenda"
  ]
  node [
    id 223
    label "polowanie"
  ]
  node [
    id 224
    label "ufetowanie_si&#281;"
  ]
  node [
    id 225
    label "wyjadanie"
  ]
  node [
    id 226
    label "smakowanie"
  ]
  node [
    id 227
    label "przejedzenie"
  ]
  node [
    id 228
    label "jad&#322;o"
  ]
  node [
    id 229
    label "mlaskanie"
  ]
  node [
    id 230
    label "papusianie"
  ]
  node [
    id 231
    label "posilanie"
  ]
  node [
    id 232
    label "czynno&#347;&#263;"
  ]
  node [
    id 233
    label "przejedzenie_si&#281;"
  ]
  node [
    id 234
    label "&#380;arcie"
  ]
  node [
    id 235
    label "odpasienie_si&#281;"
  ]
  node [
    id 236
    label "wyjedzenie"
  ]
  node [
    id 237
    label "przejadanie"
  ]
  node [
    id 238
    label "objadanie"
  ]
  node [
    id 239
    label "kawa&#322;ek"
  ]
  node [
    id 240
    label "aran&#380;acja"
  ]
  node [
    id 241
    label "pojazd_kolejowy"
  ]
  node [
    id 242
    label "wagon"
  ]
  node [
    id 243
    label "poci&#261;g"
  ]
  node [
    id 244
    label "statek"
  ]
  node [
    id 245
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 246
    label "okr&#281;t"
  ]
  node [
    id 247
    label "Stany"
  ]
  node [
    id 248
    label "George"
  ]
  node [
    id 249
    label "H"
  ]
  node [
    id 250
    label "w"
  ]
  node [
    id 251
    label "Bush"
  ]
  node [
    id 252
    label "USA"
  ]
  node [
    id 253
    label "Today"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 247
    target 248
  ]
  edge [
    source 247
    target 249
  ]
  edge [
    source 247
    target 250
  ]
  edge [
    source 247
    target 251
  ]
  edge [
    source 248
    target 249
  ]
  edge [
    source 248
    target 250
  ]
  edge [
    source 248
    target 251
  ]
  edge [
    source 249
    target 250
  ]
  edge [
    source 249
    target 251
  ]
  edge [
    source 250
    target 251
  ]
  edge [
    source 252
    target 253
  ]
]
