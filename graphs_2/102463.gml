graph [
  node [
    id 0
    label "turniej"
    origin "text"
  ]
  node [
    id 1
    label "fina&#322;owy"
    origin "text"
  ]
  node [
    id 2
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 3
    label "siatkowy"
    origin "text"
  ]
  node [
    id 4
    label "grupa"
    origin "text"
  ]
  node [
    id 5
    label "kadet"
    origin "text"
  ]
  node [
    id 6
    label "dni"
    origin "text"
  ]
  node [
    id 7
    label "lut"
    origin "text"
  ]
  node [
    id 8
    label "hala"
    origin "text"
  ]
  node [
    id 9
    label "sportowy"
    origin "text"
  ]
  node [
    id 10
    label "mosir"
    origin "text"
  ]
  node [
    id 11
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "wej&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "&#263;wier&#263;fina&#322;"
    origin "text"
  ]
  node [
    id 15
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 16
    label "polska"
    origin "text"
  ]
  node [
    id 17
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 18
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 19
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 20
    label "zapewni&#263;"
    origin "text"
  ]
  node [
    id 21
    label "siebie"
    origin "text"
  ]
  node [
    id 22
    label "dru&#380;yna"
    origin "text"
  ]
  node [
    id 23
    label "bzura"
    origin "text"
  ]
  node [
    id 24
    label "ozorek"
    origin "text"
  ]
  node [
    id 25
    label "wiking"
    origin "text"
  ]
  node [
    id 26
    label "tomasz"
    origin "text"
  ]
  node [
    id 27
    label "mazowiecki"
    origin "text"
  ]
  node [
    id 28
    label "skra"
    origin "text"
  ]
  node [
    id 29
    label "be&#322;chat&#243;w"
    origin "text"
  ]
  node [
    id 30
    label "wifama"
    origin "text"
  ]
  node [
    id 31
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 32
    label "mecz"
    origin "text"
  ]
  node [
    id 33
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 34
    label "termin"
    origin "text"
  ]
  node [
    id 35
    label "rocznik"
    origin "text"
  ]
  node [
    id 36
    label "godz"
    origin "text"
  ]
  node [
    id 37
    label "be&#322;cht&#243;w"
    origin "text"
  ]
  node [
    id 38
    label "trener"
    origin "text"
  ]
  node [
    id 39
    label "nasze"
    origin "text"
  ]
  node [
    id 40
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 41
    label "by&#263;"
    origin "text"
  ]
  node [
    id 42
    label "mgr"
    origin "text"
  ]
  node [
    id 43
    label "s&#322;awomir"
    origin "text"
  ]
  node [
    id 44
    label "pachnowski"
    origin "text"
  ]
  node [
    id 45
    label "impreza"
  ]
  node [
    id 46
    label "Wielki_Szlem"
  ]
  node [
    id 47
    label "pojedynek"
  ]
  node [
    id 48
    label "drive"
  ]
  node [
    id 49
    label "runda"
  ]
  node [
    id 50
    label "rywalizacja"
  ]
  node [
    id 51
    label "zawody"
  ]
  node [
    id 52
    label "eliminacje"
  ]
  node [
    id 53
    label "tournament"
  ]
  node [
    id 54
    label "contest"
  ]
  node [
    id 55
    label "wydarzenie"
  ]
  node [
    id 56
    label "impra"
  ]
  node [
    id 57
    label "rozrywka"
  ]
  node [
    id 58
    label "przyj&#281;cie"
  ]
  node [
    id 59
    label "okazja"
  ]
  node [
    id 60
    label "party"
  ]
  node [
    id 61
    label "walczy&#263;"
  ]
  node [
    id 62
    label "champion"
  ]
  node [
    id 63
    label "walczenie"
  ]
  node [
    id 64
    label "tysi&#281;cznik"
  ]
  node [
    id 65
    label "spadochroniarstwo"
  ]
  node [
    id 66
    label "kategoria_open"
  ]
  node [
    id 67
    label "engagement"
  ]
  node [
    id 68
    label "walka"
  ]
  node [
    id 69
    label "wyzwanie"
  ]
  node [
    id 70
    label "wyzwa&#263;"
  ]
  node [
    id 71
    label "odyniec"
  ]
  node [
    id 72
    label "wyzywa&#263;"
  ]
  node [
    id 73
    label "sekundant"
  ]
  node [
    id 74
    label "competitiveness"
  ]
  node [
    id 75
    label "sp&#243;r"
  ]
  node [
    id 76
    label "bout"
  ]
  node [
    id 77
    label "wyzywanie"
  ]
  node [
    id 78
    label "konkurs"
  ]
  node [
    id 79
    label "faza"
  ]
  node [
    id 80
    label "retirement"
  ]
  node [
    id 81
    label "rozgrywka"
  ]
  node [
    id 82
    label "seria"
  ]
  node [
    id 83
    label "rhythm"
  ]
  node [
    id 84
    label "czas"
  ]
  node [
    id 85
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 86
    label "okr&#261;&#380;enie"
  ]
  node [
    id 87
    label "ostateczny"
  ]
  node [
    id 88
    label "ko&#324;cowy"
  ]
  node [
    id 89
    label "finalnie"
  ]
  node [
    id 90
    label "ko&#324;cowo"
  ]
  node [
    id 91
    label "ostatni"
  ]
  node [
    id 92
    label "konieczny"
  ]
  node [
    id 93
    label "ostatecznie"
  ]
  node [
    id 94
    label "zupe&#322;ny"
  ]
  node [
    id 95
    label "skrajny"
  ]
  node [
    id 96
    label "finalny"
  ]
  node [
    id 97
    label "kula"
  ]
  node [
    id 98
    label "zagrywka"
  ]
  node [
    id 99
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 100
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 101
    label "do&#347;rodkowywanie"
  ]
  node [
    id 102
    label "odbicie"
  ]
  node [
    id 103
    label "gra"
  ]
  node [
    id 104
    label "musket_ball"
  ]
  node [
    id 105
    label "aut"
  ]
  node [
    id 106
    label "serwowa&#263;"
  ]
  node [
    id 107
    label "sport_zespo&#322;owy"
  ]
  node [
    id 108
    label "sport"
  ]
  node [
    id 109
    label "serwowanie"
  ]
  node [
    id 110
    label "orb"
  ]
  node [
    id 111
    label "&#347;wieca"
  ]
  node [
    id 112
    label "zaserwowanie"
  ]
  node [
    id 113
    label "zaserwowa&#263;"
  ]
  node [
    id 114
    label "rzucanka"
  ]
  node [
    id 115
    label "&#322;uska"
  ]
  node [
    id 116
    label "przedmiot"
  ]
  node [
    id 117
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 118
    label "figura_heraldyczna"
  ]
  node [
    id 119
    label "podpora"
  ]
  node [
    id 120
    label "bry&#322;a"
  ]
  node [
    id 121
    label "pile"
  ]
  node [
    id 122
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 123
    label "p&#243;&#322;sfera"
  ]
  node [
    id 124
    label "sfera"
  ]
  node [
    id 125
    label "warstwa_kulista"
  ]
  node [
    id 126
    label "wycinek_kuli"
  ]
  node [
    id 127
    label "amunicja"
  ]
  node [
    id 128
    label "ta&#347;ma"
  ]
  node [
    id 129
    label "o&#322;&#243;w"
  ]
  node [
    id 130
    label "ball"
  ]
  node [
    id 131
    label "kartuza"
  ]
  node [
    id 132
    label "czasza"
  ]
  node [
    id 133
    label "p&#243;&#322;kula"
  ]
  node [
    id 134
    label "nab&#243;j"
  ]
  node [
    id 135
    label "pocisk"
  ]
  node [
    id 136
    label "komora_nabojowa"
  ]
  node [
    id 137
    label "akwarium"
  ]
  node [
    id 138
    label "gambit"
  ]
  node [
    id 139
    label "move"
  ]
  node [
    id 140
    label "manewr"
  ]
  node [
    id 141
    label "uderzenie"
  ]
  node [
    id 142
    label "posuni&#281;cie"
  ]
  node [
    id 143
    label "myk"
  ]
  node [
    id 144
    label "gra_w_karty"
  ]
  node [
    id 145
    label "travel"
  ]
  node [
    id 146
    label "zmienno&#347;&#263;"
  ]
  node [
    id 147
    label "play"
  ]
  node [
    id 148
    label "apparent_motion"
  ]
  node [
    id 149
    label "akcja"
  ]
  node [
    id 150
    label "komplet"
  ]
  node [
    id 151
    label "zabawa"
  ]
  node [
    id 152
    label "zasada"
  ]
  node [
    id 153
    label "zbijany"
  ]
  node [
    id 154
    label "post&#281;powanie"
  ]
  node [
    id 155
    label "game"
  ]
  node [
    id 156
    label "odg&#322;os"
  ]
  node [
    id 157
    label "Pok&#233;mon"
  ]
  node [
    id 158
    label "czynno&#347;&#263;"
  ]
  node [
    id 159
    label "synteza"
  ]
  node [
    id 160
    label "odtworzenie"
  ]
  node [
    id 161
    label "rekwizyt_do_gry"
  ]
  node [
    id 162
    label "zgrupowanie"
  ]
  node [
    id 163
    label "kultura_fizyczna"
  ]
  node [
    id 164
    label "usportowienie"
  ]
  node [
    id 165
    label "atakowa&#263;"
  ]
  node [
    id 166
    label "zaatakowanie"
  ]
  node [
    id 167
    label "atakowanie"
  ]
  node [
    id 168
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 169
    label "zaatakowa&#263;"
  ]
  node [
    id 170
    label "usportowi&#263;"
  ]
  node [
    id 171
    label "sokolstwo"
  ]
  node [
    id 172
    label "stawianie"
  ]
  node [
    id 173
    label "administration"
  ]
  node [
    id 174
    label "jedzenie"
  ]
  node [
    id 175
    label "faszerowanie"
  ]
  node [
    id 176
    label "zaczynanie"
  ]
  node [
    id 177
    label "bufet"
  ]
  node [
    id 178
    label "podawanie"
  ]
  node [
    id 179
    label "podawa&#263;"
  ]
  node [
    id 180
    label "center"
  ]
  node [
    id 181
    label "kierowa&#263;"
  ]
  node [
    id 182
    label "ustawi&#263;"
  ]
  node [
    id 183
    label "give"
  ]
  node [
    id 184
    label "poda&#263;"
  ]
  node [
    id 185
    label "nafaszerowa&#263;"
  ]
  node [
    id 186
    label "zwracanie"
  ]
  node [
    id 187
    label "centering"
  ]
  node [
    id 188
    label "stamp"
  ]
  node [
    id 189
    label "wyswobodzenie"
  ]
  node [
    id 190
    label "skopiowanie"
  ]
  node [
    id 191
    label "wymienienie"
  ]
  node [
    id 192
    label "wynagrodzenie"
  ]
  node [
    id 193
    label "zdarzenie_si&#281;"
  ]
  node [
    id 194
    label "zwierciad&#322;o"
  ]
  node [
    id 195
    label "obraz"
  ]
  node [
    id 196
    label "odegranie_si&#281;"
  ]
  node [
    id 197
    label "oddalenie_si&#281;"
  ]
  node [
    id 198
    label "impression"
  ]
  node [
    id 199
    label "uszkodzenie"
  ]
  node [
    id 200
    label "spowodowanie"
  ]
  node [
    id 201
    label "stracenie_g&#322;owy"
  ]
  node [
    id 202
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 203
    label "zata&#324;czenie"
  ]
  node [
    id 204
    label "lustro"
  ]
  node [
    id 205
    label "odskoczenie"
  ]
  node [
    id 206
    label "odbicie_si&#281;"
  ]
  node [
    id 207
    label "ut&#322;uczenie"
  ]
  node [
    id 208
    label "reproduction"
  ]
  node [
    id 209
    label "wybicie"
  ]
  node [
    id 210
    label "picture"
  ]
  node [
    id 211
    label "zostawienie"
  ]
  node [
    id 212
    label "prototype"
  ]
  node [
    id 213
    label "zachowanie"
  ]
  node [
    id 214
    label "recoil"
  ]
  node [
    id 215
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 216
    label "przelobowanie"
  ]
  node [
    id 217
    label "reflection"
  ]
  node [
    id 218
    label "blask"
  ]
  node [
    id 219
    label "zabranie"
  ]
  node [
    id 220
    label "zrobienie"
  ]
  node [
    id 221
    label "ustawienie"
  ]
  node [
    id 222
    label "podanie"
  ]
  node [
    id 223
    label "service"
  ]
  node [
    id 224
    label "nafaszerowanie"
  ]
  node [
    id 225
    label "lot"
  ]
  node [
    id 226
    label "profitka"
  ]
  node [
    id 227
    label "jednostka_nat&#281;&#380;enia_&#347;wiat&#322;a"
  ]
  node [
    id 228
    label "akrobacja_lotnicza"
  ]
  node [
    id 229
    label "&#347;wiat&#322;o"
  ]
  node [
    id 230
    label "&#263;wiczenie"
  ]
  node [
    id 231
    label "s&#322;up"
  ]
  node [
    id 232
    label "gasid&#322;o"
  ]
  node [
    id 233
    label "silnik_spalinowy"
  ]
  node [
    id 234
    label "sygnalizator"
  ]
  node [
    id 235
    label "knot"
  ]
  node [
    id 236
    label "deal"
  ]
  node [
    id 237
    label "stawia&#263;"
  ]
  node [
    id 238
    label "zaczyna&#263;"
  ]
  node [
    id 239
    label "kelner"
  ]
  node [
    id 240
    label "faszerowa&#263;"
  ]
  node [
    id 241
    label "out"
  ]
  node [
    id 242
    label "przestrze&#324;"
  ]
  node [
    id 243
    label "przedostanie_si&#281;"
  ]
  node [
    id 244
    label "boisko"
  ]
  node [
    id 245
    label "wrzut"
  ]
  node [
    id 246
    label "sieciowo"
  ]
  node [
    id 247
    label "elektronicznie"
  ]
  node [
    id 248
    label "internetowy"
  ]
  node [
    id 249
    label "sieciowy"
  ]
  node [
    id 250
    label "odm&#322;adzanie"
  ]
  node [
    id 251
    label "liga"
  ]
  node [
    id 252
    label "jednostka_systematyczna"
  ]
  node [
    id 253
    label "asymilowanie"
  ]
  node [
    id 254
    label "gromada"
  ]
  node [
    id 255
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 256
    label "asymilowa&#263;"
  ]
  node [
    id 257
    label "egzemplarz"
  ]
  node [
    id 258
    label "Entuzjastki"
  ]
  node [
    id 259
    label "zbi&#243;r"
  ]
  node [
    id 260
    label "kompozycja"
  ]
  node [
    id 261
    label "Terranie"
  ]
  node [
    id 262
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 263
    label "category"
  ]
  node [
    id 264
    label "pakiet_klimatyczny"
  ]
  node [
    id 265
    label "oddzia&#322;"
  ]
  node [
    id 266
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 267
    label "cz&#261;steczka"
  ]
  node [
    id 268
    label "stage_set"
  ]
  node [
    id 269
    label "type"
  ]
  node [
    id 270
    label "specgrupa"
  ]
  node [
    id 271
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 272
    label "&#346;wietliki"
  ]
  node [
    id 273
    label "odm&#322;odzenie"
  ]
  node [
    id 274
    label "Eurogrupa"
  ]
  node [
    id 275
    label "odm&#322;adza&#263;"
  ]
  node [
    id 276
    label "formacja_geologiczna"
  ]
  node [
    id 277
    label "harcerze_starsi"
  ]
  node [
    id 278
    label "konfiguracja"
  ]
  node [
    id 279
    label "cz&#261;stka"
  ]
  node [
    id 280
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 281
    label "diadochia"
  ]
  node [
    id 282
    label "substancja"
  ]
  node [
    id 283
    label "grupa_funkcyjna"
  ]
  node [
    id 284
    label "integer"
  ]
  node [
    id 285
    label "liczba"
  ]
  node [
    id 286
    label "zlewanie_si&#281;"
  ]
  node [
    id 287
    label "ilo&#347;&#263;"
  ]
  node [
    id 288
    label "uk&#322;ad"
  ]
  node [
    id 289
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 290
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 291
    label "pe&#322;ny"
  ]
  node [
    id 292
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 293
    label "series"
  ]
  node [
    id 294
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 295
    label "uprawianie"
  ]
  node [
    id 296
    label "praca_rolnicza"
  ]
  node [
    id 297
    label "collection"
  ]
  node [
    id 298
    label "dane"
  ]
  node [
    id 299
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 300
    label "poj&#281;cie"
  ]
  node [
    id 301
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 302
    label "sum"
  ]
  node [
    id 303
    label "gathering"
  ]
  node [
    id 304
    label "album"
  ]
  node [
    id 305
    label "dzia&#322;"
  ]
  node [
    id 306
    label "system"
  ]
  node [
    id 307
    label "lias"
  ]
  node [
    id 308
    label "jednostka"
  ]
  node [
    id 309
    label "pi&#281;tro"
  ]
  node [
    id 310
    label "klasa"
  ]
  node [
    id 311
    label "jednostka_geologiczna"
  ]
  node [
    id 312
    label "filia"
  ]
  node [
    id 313
    label "malm"
  ]
  node [
    id 314
    label "whole"
  ]
  node [
    id 315
    label "dogger"
  ]
  node [
    id 316
    label "poziom"
  ]
  node [
    id 317
    label "promocja"
  ]
  node [
    id 318
    label "kurs"
  ]
  node [
    id 319
    label "bank"
  ]
  node [
    id 320
    label "formacja"
  ]
  node [
    id 321
    label "ajencja"
  ]
  node [
    id 322
    label "wojsko"
  ]
  node [
    id 323
    label "siedziba"
  ]
  node [
    id 324
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 325
    label "agencja"
  ]
  node [
    id 326
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 327
    label "szpital"
  ]
  node [
    id 328
    label "blend"
  ]
  node [
    id 329
    label "struktura"
  ]
  node [
    id 330
    label "prawo_karne"
  ]
  node [
    id 331
    label "leksem"
  ]
  node [
    id 332
    label "dzie&#322;o"
  ]
  node [
    id 333
    label "figuracja"
  ]
  node [
    id 334
    label "chwyt"
  ]
  node [
    id 335
    label "okup"
  ]
  node [
    id 336
    label "muzykologia"
  ]
  node [
    id 337
    label "&#347;redniowiecze"
  ]
  node [
    id 338
    label "czynnik_biotyczny"
  ]
  node [
    id 339
    label "wyewoluowanie"
  ]
  node [
    id 340
    label "reakcja"
  ]
  node [
    id 341
    label "individual"
  ]
  node [
    id 342
    label "przyswoi&#263;"
  ]
  node [
    id 343
    label "wytw&#243;r"
  ]
  node [
    id 344
    label "starzenie_si&#281;"
  ]
  node [
    id 345
    label "wyewoluowa&#263;"
  ]
  node [
    id 346
    label "okaz"
  ]
  node [
    id 347
    label "part"
  ]
  node [
    id 348
    label "przyswojenie"
  ]
  node [
    id 349
    label "ewoluowanie"
  ]
  node [
    id 350
    label "ewoluowa&#263;"
  ]
  node [
    id 351
    label "obiekt"
  ]
  node [
    id 352
    label "sztuka"
  ]
  node [
    id 353
    label "agent"
  ]
  node [
    id 354
    label "przyswaja&#263;"
  ]
  node [
    id 355
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 356
    label "nicpo&#324;"
  ]
  node [
    id 357
    label "przyswajanie"
  ]
  node [
    id 358
    label "feminizm"
  ]
  node [
    id 359
    label "Unia_Europejska"
  ]
  node [
    id 360
    label "uatrakcyjni&#263;"
  ]
  node [
    id 361
    label "przewietrzy&#263;"
  ]
  node [
    id 362
    label "regenerate"
  ]
  node [
    id 363
    label "odtworzy&#263;"
  ]
  node [
    id 364
    label "wymieni&#263;"
  ]
  node [
    id 365
    label "odbudowa&#263;"
  ]
  node [
    id 366
    label "odbudowywa&#263;"
  ]
  node [
    id 367
    label "m&#322;odzi&#263;"
  ]
  node [
    id 368
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 369
    label "przewietrza&#263;"
  ]
  node [
    id 370
    label "wymienia&#263;"
  ]
  node [
    id 371
    label "odtwarza&#263;"
  ]
  node [
    id 372
    label "odtwarzanie"
  ]
  node [
    id 373
    label "uatrakcyjnianie"
  ]
  node [
    id 374
    label "zast&#281;powanie"
  ]
  node [
    id 375
    label "odbudowywanie"
  ]
  node [
    id 376
    label "rejuvenation"
  ]
  node [
    id 377
    label "m&#322;odszy"
  ]
  node [
    id 378
    label "uatrakcyjnienie"
  ]
  node [
    id 379
    label "odbudowanie"
  ]
  node [
    id 380
    label "asymilowanie_si&#281;"
  ]
  node [
    id 381
    label "absorption"
  ]
  node [
    id 382
    label "pobieranie"
  ]
  node [
    id 383
    label "czerpanie"
  ]
  node [
    id 384
    label "acquisition"
  ]
  node [
    id 385
    label "zmienianie"
  ]
  node [
    id 386
    label "cz&#322;owiek"
  ]
  node [
    id 387
    label "organizm"
  ]
  node [
    id 388
    label "assimilation"
  ]
  node [
    id 389
    label "upodabnianie"
  ]
  node [
    id 390
    label "g&#322;oska"
  ]
  node [
    id 391
    label "kultura"
  ]
  node [
    id 392
    label "podobny"
  ]
  node [
    id 393
    label "fonetyka"
  ]
  node [
    id 394
    label "mecz_mistrzowski"
  ]
  node [
    id 395
    label "&#347;rodowisko"
  ]
  node [
    id 396
    label "arrangement"
  ]
  node [
    id 397
    label "obrona"
  ]
  node [
    id 398
    label "pomoc"
  ]
  node [
    id 399
    label "organizacja"
  ]
  node [
    id 400
    label "rezerwa"
  ]
  node [
    id 401
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 402
    label "pr&#243;ba"
  ]
  node [
    id 403
    label "atak"
  ]
  node [
    id 404
    label "moneta"
  ]
  node [
    id 405
    label "union"
  ]
  node [
    id 406
    label "assimilate"
  ]
  node [
    id 407
    label "dostosowywa&#263;"
  ]
  node [
    id 408
    label "dostosowa&#263;"
  ]
  node [
    id 409
    label "przejmowa&#263;"
  ]
  node [
    id 410
    label "upodobni&#263;"
  ]
  node [
    id 411
    label "przej&#261;&#263;"
  ]
  node [
    id 412
    label "upodabnia&#263;"
  ]
  node [
    id 413
    label "pobiera&#263;"
  ]
  node [
    id 414
    label "pobra&#263;"
  ]
  node [
    id 415
    label "typ"
  ]
  node [
    id 416
    label "jednostka_administracyjna"
  ]
  node [
    id 417
    label "zoologia"
  ]
  node [
    id 418
    label "skupienie"
  ]
  node [
    id 419
    label "kr&#243;lestwo"
  ]
  node [
    id 420
    label "tribe"
  ]
  node [
    id 421
    label "hurma"
  ]
  node [
    id 422
    label "botanika"
  ]
  node [
    id 423
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 424
    label "elew"
  ]
  node [
    id 425
    label "samoch&#243;d"
  ]
  node [
    id 426
    label "opel"
  ]
  node [
    id 427
    label "Kadett"
  ]
  node [
    id 428
    label "pojazd_drogowy"
  ]
  node [
    id 429
    label "spryskiwacz"
  ]
  node [
    id 430
    label "most"
  ]
  node [
    id 431
    label "baga&#380;nik"
  ]
  node [
    id 432
    label "silnik"
  ]
  node [
    id 433
    label "dachowanie"
  ]
  node [
    id 434
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 435
    label "pompa_wodna"
  ]
  node [
    id 436
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 437
    label "poduszka_powietrzna"
  ]
  node [
    id 438
    label "tempomat"
  ]
  node [
    id 439
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 440
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 441
    label "deska_rozdzielcza"
  ]
  node [
    id 442
    label "immobilizer"
  ]
  node [
    id 443
    label "t&#322;umik"
  ]
  node [
    id 444
    label "kierownica"
  ]
  node [
    id 445
    label "ABS"
  ]
  node [
    id 446
    label "bak"
  ]
  node [
    id 447
    label "dwu&#347;lad"
  ]
  node [
    id 448
    label "poci&#261;g_drogowy"
  ]
  node [
    id 449
    label "wycieraczka"
  ]
  node [
    id 450
    label "Opel"
  ]
  node [
    id 451
    label "ucze&#324;"
  ]
  node [
    id 452
    label "&#380;o&#322;nierz"
  ]
  node [
    id 453
    label "poprzedzanie"
  ]
  node [
    id 454
    label "czasoprzestrze&#324;"
  ]
  node [
    id 455
    label "laba"
  ]
  node [
    id 456
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 457
    label "chronometria"
  ]
  node [
    id 458
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 459
    label "rachuba_czasu"
  ]
  node [
    id 460
    label "przep&#322;ywanie"
  ]
  node [
    id 461
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 462
    label "czasokres"
  ]
  node [
    id 463
    label "odczyt"
  ]
  node [
    id 464
    label "chwila"
  ]
  node [
    id 465
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 466
    label "dzieje"
  ]
  node [
    id 467
    label "kategoria_gramatyczna"
  ]
  node [
    id 468
    label "poprzedzenie"
  ]
  node [
    id 469
    label "trawienie"
  ]
  node [
    id 470
    label "pochodzi&#263;"
  ]
  node [
    id 471
    label "period"
  ]
  node [
    id 472
    label "okres_czasu"
  ]
  node [
    id 473
    label "poprzedza&#263;"
  ]
  node [
    id 474
    label "schy&#322;ek"
  ]
  node [
    id 475
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 476
    label "odwlekanie_si&#281;"
  ]
  node [
    id 477
    label "zegar"
  ]
  node [
    id 478
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 479
    label "czwarty_wymiar"
  ]
  node [
    id 480
    label "pochodzenie"
  ]
  node [
    id 481
    label "koniugacja"
  ]
  node [
    id 482
    label "Zeitgeist"
  ]
  node [
    id 483
    label "trawi&#263;"
  ]
  node [
    id 484
    label "pogoda"
  ]
  node [
    id 485
    label "poprzedzi&#263;"
  ]
  node [
    id 486
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 487
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 488
    label "time_period"
  ]
  node [
    id 489
    label "stop"
  ]
  node [
    id 490
    label "spoiwo"
  ]
  node [
    id 491
    label "przesyca&#263;"
  ]
  node [
    id 492
    label "przesycanie"
  ]
  node [
    id 493
    label "przesycenie"
  ]
  node [
    id 494
    label "struktura_metalu"
  ]
  node [
    id 495
    label "mieszanina"
  ]
  node [
    id 496
    label "znak_nakazu"
  ]
  node [
    id 497
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 498
    label "reflektor"
  ]
  node [
    id 499
    label "alia&#380;"
  ]
  node [
    id 500
    label "przesyci&#263;"
  ]
  node [
    id 501
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 502
    label "czynnik"
  ]
  node [
    id 503
    label "ska&#322;a"
  ]
  node [
    id 504
    label "masa"
  ]
  node [
    id 505
    label "stopiwo"
  ]
  node [
    id 506
    label "adhesiveness"
  ]
  node [
    id 507
    label "kontakt"
  ]
  node [
    id 508
    label "dworzec"
  ]
  node [
    id 509
    label "oczyszczalnia"
  ]
  node [
    id 510
    label "huta"
  ]
  node [
    id 511
    label "budynek"
  ]
  node [
    id 512
    label "lotnisko"
  ]
  node [
    id 513
    label "pomieszczenie"
  ]
  node [
    id 514
    label "pastwisko"
  ]
  node [
    id 515
    label "kopalnia"
  ]
  node [
    id 516
    label "halizna"
  ]
  node [
    id 517
    label "fabryka"
  ]
  node [
    id 518
    label "p&#322;aszczyzna"
  ]
  node [
    id 519
    label "chronozona"
  ]
  node [
    id 520
    label "kondygnacja"
  ]
  node [
    id 521
    label "eta&#380;"
  ]
  node [
    id 522
    label "floor"
  ]
  node [
    id 523
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 524
    label "amfilada"
  ]
  node [
    id 525
    label "front"
  ]
  node [
    id 526
    label "apartment"
  ]
  node [
    id 527
    label "pod&#322;oga"
  ]
  node [
    id 528
    label "udost&#281;pnienie"
  ]
  node [
    id 529
    label "miejsce"
  ]
  node [
    id 530
    label "sklepienie"
  ]
  node [
    id 531
    label "sufit"
  ]
  node [
    id 532
    label "umieszczenie"
  ]
  node [
    id 533
    label "zakamarek"
  ]
  node [
    id 534
    label "&#322;&#261;ka"
  ]
  node [
    id 535
    label "balkon"
  ]
  node [
    id 536
    label "budowla"
  ]
  node [
    id 537
    label "skrzyd&#322;o"
  ]
  node [
    id 538
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 539
    label "dach"
  ]
  node [
    id 540
    label "strop"
  ]
  node [
    id 541
    label "klatka_schodowa"
  ]
  node [
    id 542
    label "przedpro&#380;e"
  ]
  node [
    id 543
    label "Pentagon"
  ]
  node [
    id 544
    label "alkierz"
  ]
  node [
    id 545
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 546
    label "prz&#281;dzalnia"
  ]
  node [
    id 547
    label "rurownia"
  ]
  node [
    id 548
    label "wytrawialnia"
  ]
  node [
    id 549
    label "ucieralnia"
  ]
  node [
    id 550
    label "tkalnia"
  ]
  node [
    id 551
    label "farbiarnia"
  ]
  node [
    id 552
    label "szwalnia"
  ]
  node [
    id 553
    label "szlifiernia"
  ]
  node [
    id 554
    label "probiernia"
  ]
  node [
    id 555
    label "fryzernia"
  ]
  node [
    id 556
    label "celulozownia"
  ]
  node [
    id 557
    label "magazyn"
  ]
  node [
    id 558
    label "gospodarka"
  ]
  node [
    id 559
    label "dziewiarnia"
  ]
  node [
    id 560
    label "bicie"
  ]
  node [
    id 561
    label "mina"
  ]
  node [
    id 562
    label "miejsce_pracy"
  ]
  node [
    id 563
    label "ucinka"
  ]
  node [
    id 564
    label "cechownia"
  ]
  node [
    id 565
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 566
    label "w&#281;giel_kopalny"
  ]
  node [
    id 567
    label "wyrobisko"
  ]
  node [
    id 568
    label "klatka"
  ]
  node [
    id 569
    label "g&#243;rnik"
  ]
  node [
    id 570
    label "za&#322;adownia"
  ]
  node [
    id 571
    label "lutnioci&#261;g"
  ]
  node [
    id 572
    label "zag&#322;&#281;bie"
  ]
  node [
    id 573
    label "gmina_g&#243;rnicza"
  ]
  node [
    id 574
    label "slabing"
  ]
  node [
    id 575
    label "odlewnia"
  ]
  node [
    id 576
    label "stalownia"
  ]
  node [
    id 577
    label "piec_hutniczy"
  ]
  node [
    id 578
    label "walcownia"
  ]
  node [
    id 579
    label "pra&#380;alnia"
  ]
  node [
    id 580
    label "terminal"
  ]
  node [
    id 581
    label "droga_ko&#322;owania"
  ]
  node [
    id 582
    label "p&#322;yta_postojowa"
  ]
  node [
    id 583
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 584
    label "aerodrom"
  ]
  node [
    id 585
    label "pas_startowy"
  ]
  node [
    id 586
    label "baza"
  ]
  node [
    id 587
    label "betonka"
  ]
  node [
    id 588
    label "peron"
  ]
  node [
    id 589
    label "poczekalnia"
  ]
  node [
    id 590
    label "majdaniarz"
  ]
  node [
    id 591
    label "stacja"
  ]
  node [
    id 592
    label "przechowalnia"
  ]
  node [
    id 593
    label "miedza"
  ]
  node [
    id 594
    label "las"
  ]
  node [
    id 595
    label "sportowo"
  ]
  node [
    id 596
    label "uczciwy"
  ]
  node [
    id 597
    label "wygodny"
  ]
  node [
    id 598
    label "na_sportowo"
  ]
  node [
    id 599
    label "specjalny"
  ]
  node [
    id 600
    label "intencjonalny"
  ]
  node [
    id 601
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 602
    label "niedorozw&#243;j"
  ]
  node [
    id 603
    label "szczeg&#243;lny"
  ]
  node [
    id 604
    label "specjalnie"
  ]
  node [
    id 605
    label "nieetatowy"
  ]
  node [
    id 606
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 607
    label "nienormalny"
  ]
  node [
    id 608
    label "umy&#347;lnie"
  ]
  node [
    id 609
    label "odpowiedni"
  ]
  node [
    id 610
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 611
    label "leniwy"
  ]
  node [
    id 612
    label "dogodnie"
  ]
  node [
    id 613
    label "wygodnie"
  ]
  node [
    id 614
    label "przyjemny"
  ]
  node [
    id 615
    label "nieograniczony"
  ]
  node [
    id 616
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 617
    label "satysfakcja"
  ]
  node [
    id 618
    label "bezwzgl&#281;dny"
  ]
  node [
    id 619
    label "ca&#322;y"
  ]
  node [
    id 620
    label "otwarty"
  ]
  node [
    id 621
    label "wype&#322;nienie"
  ]
  node [
    id 622
    label "kompletny"
  ]
  node [
    id 623
    label "pe&#322;no"
  ]
  node [
    id 624
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 625
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 626
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 627
    label "r&#243;wny"
  ]
  node [
    id 628
    label "intensywny"
  ]
  node [
    id 629
    label "szczery"
  ]
  node [
    id 630
    label "s&#322;uszny"
  ]
  node [
    id 631
    label "s&#322;usznie"
  ]
  node [
    id 632
    label "nale&#380;yty"
  ]
  node [
    id 633
    label "moralny"
  ]
  node [
    id 634
    label "porz&#261;dnie"
  ]
  node [
    id 635
    label "uczciwie"
  ]
  node [
    id 636
    label "prawdziwy"
  ]
  node [
    id 637
    label "zgodny"
  ]
  node [
    id 638
    label "solidny"
  ]
  node [
    id 639
    label "rzetelny"
  ]
  node [
    id 640
    label "reserve"
  ]
  node [
    id 641
    label "przej&#347;&#263;"
  ]
  node [
    id 642
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 643
    label "ustawa"
  ]
  node [
    id 644
    label "podlec"
  ]
  node [
    id 645
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 646
    label "min&#261;&#263;"
  ]
  node [
    id 647
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 648
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 649
    label "zaliczy&#263;"
  ]
  node [
    id 650
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 651
    label "zmieni&#263;"
  ]
  node [
    id 652
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 653
    label "przeby&#263;"
  ]
  node [
    id 654
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 655
    label "die"
  ]
  node [
    id 656
    label "dozna&#263;"
  ]
  node [
    id 657
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 658
    label "zacz&#261;&#263;"
  ]
  node [
    id 659
    label "happen"
  ]
  node [
    id 660
    label "pass"
  ]
  node [
    id 661
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 662
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 663
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 664
    label "beat"
  ]
  node [
    id 665
    label "mienie"
  ]
  node [
    id 666
    label "absorb"
  ]
  node [
    id 667
    label "przerobi&#263;"
  ]
  node [
    id 668
    label "pique"
  ]
  node [
    id 669
    label "przesta&#263;"
  ]
  node [
    id 670
    label "wnikni&#281;cie"
  ]
  node [
    id 671
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 672
    label "spotkanie"
  ]
  node [
    id 673
    label "poznanie"
  ]
  node [
    id 674
    label "pojawienie_si&#281;"
  ]
  node [
    id 675
    label "przenikni&#281;cie"
  ]
  node [
    id 676
    label "wpuszczenie"
  ]
  node [
    id 677
    label "trespass"
  ]
  node [
    id 678
    label "dost&#281;p"
  ]
  node [
    id 679
    label "doj&#347;cie"
  ]
  node [
    id 680
    label "przekroczenie"
  ]
  node [
    id 681
    label "otw&#243;r"
  ]
  node [
    id 682
    label "wzi&#281;cie"
  ]
  node [
    id 683
    label "vent"
  ]
  node [
    id 684
    label "stimulation"
  ]
  node [
    id 685
    label "dostanie_si&#281;"
  ]
  node [
    id 686
    label "pocz&#261;tek"
  ]
  node [
    id 687
    label "approach"
  ]
  node [
    id 688
    label "release"
  ]
  node [
    id 689
    label "wnij&#347;cie"
  ]
  node [
    id 690
    label "bramka"
  ]
  node [
    id 691
    label "wzniesienie_si&#281;"
  ]
  node [
    id 692
    label "podw&#243;rze"
  ]
  node [
    id 693
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 694
    label "dom"
  ]
  node [
    id 695
    label "wch&#243;d"
  ]
  node [
    id 696
    label "nast&#261;pienie"
  ]
  node [
    id 697
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 698
    label "zacz&#281;cie"
  ]
  node [
    id 699
    label "cz&#322;onek"
  ]
  node [
    id 700
    label "stanie_si&#281;"
  ]
  node [
    id 701
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 702
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 703
    label "urz&#261;dzenie"
  ]
  node [
    id 704
    label "discourtesy"
  ]
  node [
    id 705
    label "odj&#281;cie"
  ]
  node [
    id 706
    label "post&#261;pienie"
  ]
  node [
    id 707
    label "opening"
  ]
  node [
    id 708
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 709
    label "wyra&#380;enie"
  ]
  node [
    id 710
    label "porobienie_si&#281;"
  ]
  node [
    id 711
    label "naci&#347;ni&#281;cie"
  ]
  node [
    id 712
    label "chodzenie"
  ]
  node [
    id 713
    label "event"
  ]
  node [
    id 714
    label "advent"
  ]
  node [
    id 715
    label "uzyskanie"
  ]
  node [
    id 716
    label "dochrapanie_si&#281;"
  ]
  node [
    id 717
    label "skill"
  ]
  node [
    id 718
    label "accomplishment"
  ]
  node [
    id 719
    label "sukces"
  ]
  node [
    id 720
    label "zaawansowanie"
  ]
  node [
    id 721
    label "dotarcie"
  ]
  node [
    id 722
    label "act"
  ]
  node [
    id 723
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 724
    label "doznanie"
  ]
  node [
    id 725
    label "zawarcie"
  ]
  node [
    id 726
    label "znajomy"
  ]
  node [
    id 727
    label "powitanie"
  ]
  node [
    id 728
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 729
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 730
    label "znalezienie"
  ]
  node [
    id 731
    label "match"
  ]
  node [
    id 732
    label "employment"
  ]
  node [
    id 733
    label "po&#380;egnanie"
  ]
  node [
    id 734
    label "gather"
  ]
  node [
    id 735
    label "spotykanie"
  ]
  node [
    id 736
    label "spotkanie_si&#281;"
  ]
  node [
    id 737
    label "dochodzenie"
  ]
  node [
    id 738
    label "znajomo&#347;ci"
  ]
  node [
    id 739
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 740
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 741
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 742
    label "powi&#261;zanie"
  ]
  node [
    id 743
    label "entrance"
  ]
  node [
    id 744
    label "affiliation"
  ]
  node [
    id 745
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 746
    label "dor&#281;czenie"
  ]
  node [
    id 747
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 748
    label "bodziec"
  ]
  node [
    id 749
    label "informacja"
  ]
  node [
    id 750
    label "przesy&#322;ka"
  ]
  node [
    id 751
    label "gotowy"
  ]
  node [
    id 752
    label "avenue"
  ]
  node [
    id 753
    label "postrzeganie"
  ]
  node [
    id 754
    label "dodatek"
  ]
  node [
    id 755
    label "dojrza&#322;y"
  ]
  node [
    id 756
    label "dojechanie"
  ]
  node [
    id 757
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 758
    label "ingress"
  ]
  node [
    id 759
    label "strzelenie"
  ]
  node [
    id 760
    label "orzekni&#281;cie"
  ]
  node [
    id 761
    label "orgazm"
  ]
  node [
    id 762
    label "dolecenie"
  ]
  node [
    id 763
    label "rozpowszechnienie"
  ]
  node [
    id 764
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 765
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 766
    label "dop&#322;ata"
  ]
  node [
    id 767
    label "informatyka"
  ]
  node [
    id 768
    label "operacja"
  ]
  node [
    id 769
    label "konto"
  ]
  node [
    id 770
    label "has&#322;o"
  ]
  node [
    id 771
    label "pierworodztwo"
  ]
  node [
    id 772
    label "upgrade"
  ]
  node [
    id 773
    label "nast&#281;pstwo"
  ]
  node [
    id 774
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 775
    label "skrytykowanie"
  ]
  node [
    id 776
    label "time"
  ]
  node [
    id 777
    label "oddzia&#322;anie"
  ]
  node [
    id 778
    label "przebycie"
  ]
  node [
    id 779
    label "upolowanie"
  ]
  node [
    id 780
    label "wdarcie_si&#281;"
  ]
  node [
    id 781
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 782
    label "progress"
  ]
  node [
    id 783
    label "spr&#243;bowanie"
  ]
  node [
    id 784
    label "powiedzenie"
  ]
  node [
    id 785
    label "rozegranie"
  ]
  node [
    id 786
    label "mini&#281;cie"
  ]
  node [
    id 787
    label "ograniczenie"
  ]
  node [
    id 788
    label "przepuszczenie"
  ]
  node [
    id 789
    label "transgression"
  ]
  node [
    id 790
    label "transgresja"
  ]
  node [
    id 791
    label "emergence"
  ]
  node [
    id 792
    label "offense"
  ]
  node [
    id 793
    label "kom&#243;rka"
  ]
  node [
    id 794
    label "furnishing"
  ]
  node [
    id 795
    label "zabezpieczenie"
  ]
  node [
    id 796
    label "wyrz&#261;dzenie"
  ]
  node [
    id 797
    label "zagospodarowanie"
  ]
  node [
    id 798
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 799
    label "ig&#322;a"
  ]
  node [
    id 800
    label "narz&#281;dzie"
  ]
  node [
    id 801
    label "wirnik"
  ]
  node [
    id 802
    label "aparatura"
  ]
  node [
    id 803
    label "system_energetyczny"
  ]
  node [
    id 804
    label "impulsator"
  ]
  node [
    id 805
    label "mechanizm"
  ]
  node [
    id 806
    label "sprz&#281;t"
  ]
  node [
    id 807
    label "blokowanie"
  ]
  node [
    id 808
    label "set"
  ]
  node [
    id 809
    label "zablokowanie"
  ]
  node [
    id 810
    label "przygotowanie"
  ]
  node [
    id 811
    label "komora"
  ]
  node [
    id 812
    label "j&#281;zyk"
  ]
  node [
    id 813
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 814
    label "wsi&#261;kni&#281;cie"
  ]
  node [
    id 815
    label "infiltration"
  ]
  node [
    id 816
    label "penetration"
  ]
  node [
    id 817
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 818
    label "wyd&#322;ubanie"
  ]
  node [
    id 819
    label "przerwa"
  ]
  node [
    id 820
    label "powybijanie"
  ]
  node [
    id 821
    label "wybijanie"
  ]
  node [
    id 822
    label "wiercenie"
  ]
  node [
    id 823
    label "acquaintance"
  ]
  node [
    id 824
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 825
    label "nauczenie_si&#281;"
  ]
  node [
    id 826
    label "poczucie"
  ]
  node [
    id 827
    label "knowing"
  ]
  node [
    id 828
    label "zapoznanie_si&#281;"
  ]
  node [
    id 829
    label "wy&#347;wiadczenie"
  ]
  node [
    id 830
    label "inclusion"
  ]
  node [
    id 831
    label "zrozumienie"
  ]
  node [
    id 832
    label "designation"
  ]
  node [
    id 833
    label "umo&#380;liwienie"
  ]
  node [
    id 834
    label "sensing"
  ]
  node [
    id 835
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 836
    label "zapoznanie"
  ]
  node [
    id 837
    label "forma"
  ]
  node [
    id 838
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 839
    label "insekt"
  ]
  node [
    id 840
    label "puszczenie"
  ]
  node [
    id 841
    label "entree"
  ]
  node [
    id 842
    label "wprowadzenie"
  ]
  node [
    id 843
    label "dmuchni&#281;cie"
  ]
  node [
    id 844
    label "niesienie"
  ]
  node [
    id 845
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 846
    label "nakazanie"
  ]
  node [
    id 847
    label "ruszenie"
  ]
  node [
    id 848
    label "pokonanie"
  ]
  node [
    id 849
    label "take"
  ]
  node [
    id 850
    label "wywiezienie"
  ]
  node [
    id 851
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 852
    label "wymienienie_si&#281;"
  ]
  node [
    id 853
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 854
    label "uciekni&#281;cie"
  ]
  node [
    id 855
    label "pobranie"
  ]
  node [
    id 856
    label "poczytanie"
  ]
  node [
    id 857
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 858
    label "pozabieranie"
  ]
  node [
    id 859
    label "u&#380;ycie"
  ]
  node [
    id 860
    label "powodzenie"
  ]
  node [
    id 861
    label "pickings"
  ]
  node [
    id 862
    label "zniesienie"
  ]
  node [
    id 863
    label "kupienie"
  ]
  node [
    id 864
    label "bite"
  ]
  node [
    id 865
    label "dostanie"
  ]
  node [
    id 866
    label "wyruchanie"
  ]
  node [
    id 867
    label "odziedziczenie"
  ]
  node [
    id 868
    label "capture"
  ]
  node [
    id 869
    label "otrzymanie"
  ]
  node [
    id 870
    label "branie"
  ]
  node [
    id 871
    label "wygranie"
  ]
  node [
    id 872
    label "wzi&#261;&#263;"
  ]
  node [
    id 873
    label "obj&#281;cie"
  ]
  node [
    id 874
    label "w&#322;o&#380;enie"
  ]
  node [
    id 875
    label "udanie_si&#281;"
  ]
  node [
    id 876
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 877
    label "podmiot"
  ]
  node [
    id 878
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 879
    label "organ"
  ]
  node [
    id 880
    label "ptaszek"
  ]
  node [
    id 881
    label "element_anatomiczny"
  ]
  node [
    id 882
    label "cia&#322;o"
  ]
  node [
    id 883
    label "przyrodzenie"
  ]
  node [
    id 884
    label "fiut"
  ]
  node [
    id 885
    label "shaft"
  ]
  node [
    id 886
    label "wchodzenie"
  ]
  node [
    id 887
    label "przedstawiciel"
  ]
  node [
    id 888
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 889
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 890
    label "nas&#261;czenie"
  ]
  node [
    id 891
    label "strain"
  ]
  node [
    id 892
    label "control"
  ]
  node [
    id 893
    label "nasycenie_si&#281;"
  ]
  node [
    id 894
    label "permeation"
  ]
  node [
    id 895
    label "nasilenie_si&#281;"
  ]
  node [
    id 896
    label "przemokni&#281;cie"
  ]
  node [
    id 897
    label "przepojenie"
  ]
  node [
    id 898
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 899
    label "obstawianie"
  ]
  node [
    id 900
    label "trafienie"
  ]
  node [
    id 901
    label "obstawienie"
  ]
  node [
    id 902
    label "przeszkoda"
  ]
  node [
    id 903
    label "zawiasy"
  ]
  node [
    id 904
    label "s&#322;upek"
  ]
  node [
    id 905
    label "siatka"
  ]
  node [
    id 906
    label "obstawia&#263;"
  ]
  node [
    id 907
    label "ogrodzenie"
  ]
  node [
    id 908
    label "zamek"
  ]
  node [
    id 909
    label "goal"
  ]
  node [
    id 910
    label "poprzeczka"
  ]
  node [
    id 911
    label "p&#322;ot"
  ]
  node [
    id 912
    label "obstawi&#263;"
  ]
  node [
    id 913
    label "brama"
  ]
  node [
    id 914
    label "plac"
  ]
  node [
    id 915
    label "podjazd"
  ]
  node [
    id 916
    label "ogr&#243;d"
  ]
  node [
    id 917
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 918
    label "rodzina"
  ]
  node [
    id 919
    label "substancja_mieszkaniowa"
  ]
  node [
    id 920
    label "instytucja"
  ]
  node [
    id 921
    label "dom_rodzinny"
  ]
  node [
    id 922
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 923
    label "stead"
  ]
  node [
    id 924
    label "garderoba"
  ]
  node [
    id 925
    label "wiecha"
  ]
  node [
    id 926
    label "fratria"
  ]
  node [
    id 927
    label "quarter"
  ]
  node [
    id 928
    label "Formu&#322;a_1"
  ]
  node [
    id 929
    label "championship"
  ]
  node [
    id 930
    label "obecno&#347;&#263;"
  ]
  node [
    id 931
    label "kwota"
  ]
  node [
    id 932
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 933
    label "stan"
  ]
  node [
    id 934
    label "being"
  ]
  node [
    id 935
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 936
    label "cecha"
  ]
  node [
    id 937
    label "wynie&#347;&#263;"
  ]
  node [
    id 938
    label "pieni&#261;dze"
  ]
  node [
    id 939
    label "limit"
  ]
  node [
    id 940
    label "wynosi&#263;"
  ]
  node [
    id 941
    label "rozmiar"
  ]
  node [
    id 942
    label "powiat"
  ]
  node [
    id 943
    label "mikroregion"
  ]
  node [
    id 944
    label "makroregion"
  ]
  node [
    id 945
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 946
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 947
    label "pa&#324;stwo"
  ]
  node [
    id 948
    label "region"
  ]
  node [
    id 949
    label "gmina"
  ]
  node [
    id 950
    label "Katar"
  ]
  node [
    id 951
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 952
    label "Libia"
  ]
  node [
    id 953
    label "Gwatemala"
  ]
  node [
    id 954
    label "Afganistan"
  ]
  node [
    id 955
    label "Ekwador"
  ]
  node [
    id 956
    label "Tad&#380;ykistan"
  ]
  node [
    id 957
    label "Bhutan"
  ]
  node [
    id 958
    label "Argentyna"
  ]
  node [
    id 959
    label "D&#380;ibuti"
  ]
  node [
    id 960
    label "Wenezuela"
  ]
  node [
    id 961
    label "Ukraina"
  ]
  node [
    id 962
    label "Gabon"
  ]
  node [
    id 963
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 964
    label "Rwanda"
  ]
  node [
    id 965
    label "Liechtenstein"
  ]
  node [
    id 966
    label "Sri_Lanka"
  ]
  node [
    id 967
    label "Madagaskar"
  ]
  node [
    id 968
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 969
    label "Tonga"
  ]
  node [
    id 970
    label "Kongo"
  ]
  node [
    id 971
    label "Bangladesz"
  ]
  node [
    id 972
    label "Kanada"
  ]
  node [
    id 973
    label "Wehrlen"
  ]
  node [
    id 974
    label "Algieria"
  ]
  node [
    id 975
    label "Surinam"
  ]
  node [
    id 976
    label "Chile"
  ]
  node [
    id 977
    label "Sahara_Zachodnia"
  ]
  node [
    id 978
    label "Uganda"
  ]
  node [
    id 979
    label "W&#281;gry"
  ]
  node [
    id 980
    label "Birma"
  ]
  node [
    id 981
    label "Kazachstan"
  ]
  node [
    id 982
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 983
    label "Armenia"
  ]
  node [
    id 984
    label "Tuwalu"
  ]
  node [
    id 985
    label "Timor_Wschodni"
  ]
  node [
    id 986
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 987
    label "Izrael"
  ]
  node [
    id 988
    label "Estonia"
  ]
  node [
    id 989
    label "Komory"
  ]
  node [
    id 990
    label "Kamerun"
  ]
  node [
    id 991
    label "Haiti"
  ]
  node [
    id 992
    label "Belize"
  ]
  node [
    id 993
    label "Sierra_Leone"
  ]
  node [
    id 994
    label "Luksemburg"
  ]
  node [
    id 995
    label "USA"
  ]
  node [
    id 996
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 997
    label "Barbados"
  ]
  node [
    id 998
    label "San_Marino"
  ]
  node [
    id 999
    label "Bu&#322;garia"
  ]
  node [
    id 1000
    label "Wietnam"
  ]
  node [
    id 1001
    label "Indonezja"
  ]
  node [
    id 1002
    label "Malawi"
  ]
  node [
    id 1003
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1004
    label "Francja"
  ]
  node [
    id 1005
    label "partia"
  ]
  node [
    id 1006
    label "Zambia"
  ]
  node [
    id 1007
    label "Angola"
  ]
  node [
    id 1008
    label "Grenada"
  ]
  node [
    id 1009
    label "Nepal"
  ]
  node [
    id 1010
    label "Panama"
  ]
  node [
    id 1011
    label "Rumunia"
  ]
  node [
    id 1012
    label "Czarnog&#243;ra"
  ]
  node [
    id 1013
    label "Malediwy"
  ]
  node [
    id 1014
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1015
    label "S&#322;owacja"
  ]
  node [
    id 1016
    label "para"
  ]
  node [
    id 1017
    label "Egipt"
  ]
  node [
    id 1018
    label "zwrot"
  ]
  node [
    id 1019
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1020
    label "Kolumbia"
  ]
  node [
    id 1021
    label "Mozambik"
  ]
  node [
    id 1022
    label "Laos"
  ]
  node [
    id 1023
    label "Burundi"
  ]
  node [
    id 1024
    label "Suazi"
  ]
  node [
    id 1025
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1026
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1027
    label "Czechy"
  ]
  node [
    id 1028
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1029
    label "Wyspy_Marshalla"
  ]
  node [
    id 1030
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1031
    label "Dominika"
  ]
  node [
    id 1032
    label "Palau"
  ]
  node [
    id 1033
    label "Syria"
  ]
  node [
    id 1034
    label "Gwinea_Bissau"
  ]
  node [
    id 1035
    label "Liberia"
  ]
  node [
    id 1036
    label "Zimbabwe"
  ]
  node [
    id 1037
    label "Polska"
  ]
  node [
    id 1038
    label "Jamajka"
  ]
  node [
    id 1039
    label "Dominikana"
  ]
  node [
    id 1040
    label "Senegal"
  ]
  node [
    id 1041
    label "Gruzja"
  ]
  node [
    id 1042
    label "Togo"
  ]
  node [
    id 1043
    label "Chorwacja"
  ]
  node [
    id 1044
    label "Meksyk"
  ]
  node [
    id 1045
    label "Macedonia"
  ]
  node [
    id 1046
    label "Gujana"
  ]
  node [
    id 1047
    label "Zair"
  ]
  node [
    id 1048
    label "Albania"
  ]
  node [
    id 1049
    label "Kambod&#380;a"
  ]
  node [
    id 1050
    label "Mauritius"
  ]
  node [
    id 1051
    label "Monako"
  ]
  node [
    id 1052
    label "Gwinea"
  ]
  node [
    id 1053
    label "Mali"
  ]
  node [
    id 1054
    label "Nigeria"
  ]
  node [
    id 1055
    label "Kostaryka"
  ]
  node [
    id 1056
    label "Hanower"
  ]
  node [
    id 1057
    label "Paragwaj"
  ]
  node [
    id 1058
    label "W&#322;ochy"
  ]
  node [
    id 1059
    label "Wyspy_Salomona"
  ]
  node [
    id 1060
    label "Seszele"
  ]
  node [
    id 1061
    label "Hiszpania"
  ]
  node [
    id 1062
    label "Boliwia"
  ]
  node [
    id 1063
    label "Kirgistan"
  ]
  node [
    id 1064
    label "Irlandia"
  ]
  node [
    id 1065
    label "Czad"
  ]
  node [
    id 1066
    label "Irak"
  ]
  node [
    id 1067
    label "Lesoto"
  ]
  node [
    id 1068
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1069
    label "Malta"
  ]
  node [
    id 1070
    label "Andora"
  ]
  node [
    id 1071
    label "Chiny"
  ]
  node [
    id 1072
    label "Filipiny"
  ]
  node [
    id 1073
    label "Antarktis"
  ]
  node [
    id 1074
    label "Niemcy"
  ]
  node [
    id 1075
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1076
    label "Brazylia"
  ]
  node [
    id 1077
    label "terytorium"
  ]
  node [
    id 1078
    label "Nikaragua"
  ]
  node [
    id 1079
    label "Pakistan"
  ]
  node [
    id 1080
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1081
    label "Kenia"
  ]
  node [
    id 1082
    label "Niger"
  ]
  node [
    id 1083
    label "Tunezja"
  ]
  node [
    id 1084
    label "Portugalia"
  ]
  node [
    id 1085
    label "Fid&#380;i"
  ]
  node [
    id 1086
    label "Maroko"
  ]
  node [
    id 1087
    label "Botswana"
  ]
  node [
    id 1088
    label "Tajlandia"
  ]
  node [
    id 1089
    label "Australia"
  ]
  node [
    id 1090
    label "Burkina_Faso"
  ]
  node [
    id 1091
    label "interior"
  ]
  node [
    id 1092
    label "Benin"
  ]
  node [
    id 1093
    label "Tanzania"
  ]
  node [
    id 1094
    label "Indie"
  ]
  node [
    id 1095
    label "&#321;otwa"
  ]
  node [
    id 1096
    label "Kiribati"
  ]
  node [
    id 1097
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1098
    label "Rodezja"
  ]
  node [
    id 1099
    label "Cypr"
  ]
  node [
    id 1100
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1101
    label "Peru"
  ]
  node [
    id 1102
    label "Austria"
  ]
  node [
    id 1103
    label "Urugwaj"
  ]
  node [
    id 1104
    label "Jordania"
  ]
  node [
    id 1105
    label "Grecja"
  ]
  node [
    id 1106
    label "Azerbejd&#380;an"
  ]
  node [
    id 1107
    label "Turcja"
  ]
  node [
    id 1108
    label "Samoa"
  ]
  node [
    id 1109
    label "Sudan"
  ]
  node [
    id 1110
    label "Oman"
  ]
  node [
    id 1111
    label "ziemia"
  ]
  node [
    id 1112
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1113
    label "Uzbekistan"
  ]
  node [
    id 1114
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1115
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1116
    label "Honduras"
  ]
  node [
    id 1117
    label "Mongolia"
  ]
  node [
    id 1118
    label "Portoryko"
  ]
  node [
    id 1119
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1120
    label "Serbia"
  ]
  node [
    id 1121
    label "Tajwan"
  ]
  node [
    id 1122
    label "Wielka_Brytania"
  ]
  node [
    id 1123
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1124
    label "Liban"
  ]
  node [
    id 1125
    label "Japonia"
  ]
  node [
    id 1126
    label "Ghana"
  ]
  node [
    id 1127
    label "Bahrajn"
  ]
  node [
    id 1128
    label "Belgia"
  ]
  node [
    id 1129
    label "Etiopia"
  ]
  node [
    id 1130
    label "Mikronezja"
  ]
  node [
    id 1131
    label "Kuwejt"
  ]
  node [
    id 1132
    label "Bahamy"
  ]
  node [
    id 1133
    label "Rosja"
  ]
  node [
    id 1134
    label "Mo&#322;dawia"
  ]
  node [
    id 1135
    label "Litwa"
  ]
  node [
    id 1136
    label "S&#322;owenia"
  ]
  node [
    id 1137
    label "Szwajcaria"
  ]
  node [
    id 1138
    label "Erytrea"
  ]
  node [
    id 1139
    label "Kuba"
  ]
  node [
    id 1140
    label "Arabia_Saudyjska"
  ]
  node [
    id 1141
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1142
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1143
    label "Malezja"
  ]
  node [
    id 1144
    label "Korea"
  ]
  node [
    id 1145
    label "Jemen"
  ]
  node [
    id 1146
    label "Nowa_Zelandia"
  ]
  node [
    id 1147
    label "Namibia"
  ]
  node [
    id 1148
    label "Nauru"
  ]
  node [
    id 1149
    label "holoarktyka"
  ]
  node [
    id 1150
    label "Brunei"
  ]
  node [
    id 1151
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1152
    label "Khitai"
  ]
  node [
    id 1153
    label "Mauretania"
  ]
  node [
    id 1154
    label "Iran"
  ]
  node [
    id 1155
    label "Gambia"
  ]
  node [
    id 1156
    label "Somalia"
  ]
  node [
    id 1157
    label "Holandia"
  ]
  node [
    id 1158
    label "Turkmenistan"
  ]
  node [
    id 1159
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1160
    label "Salwador"
  ]
  node [
    id 1161
    label "mezoregion"
  ]
  node [
    id 1162
    label "Jura"
  ]
  node [
    id 1163
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1164
    label "poinformowa&#263;"
  ]
  node [
    id 1165
    label "translate"
  ]
  node [
    id 1166
    label "spowodowa&#263;"
  ]
  node [
    id 1167
    label "inform"
  ]
  node [
    id 1168
    label "zakomunikowa&#263;"
  ]
  node [
    id 1169
    label "dublet"
  ]
  node [
    id 1170
    label "szczep"
  ]
  node [
    id 1171
    label "zast&#281;p"
  ]
  node [
    id 1172
    label "pododdzia&#322;"
  ]
  node [
    id 1173
    label "pluton"
  ]
  node [
    id 1174
    label "force"
  ]
  node [
    id 1175
    label "Bund"
  ]
  node [
    id 1176
    label "Mazowsze"
  ]
  node [
    id 1177
    label "PPR"
  ]
  node [
    id 1178
    label "Jakobici"
  ]
  node [
    id 1179
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1180
    label "SLD"
  ]
  node [
    id 1181
    label "zespolik"
  ]
  node [
    id 1182
    label "Razem"
  ]
  node [
    id 1183
    label "PiS"
  ]
  node [
    id 1184
    label "zjawisko"
  ]
  node [
    id 1185
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1186
    label "Kuomintang"
  ]
  node [
    id 1187
    label "ZSL"
  ]
  node [
    id 1188
    label "szko&#322;a"
  ]
  node [
    id 1189
    label "proces"
  ]
  node [
    id 1190
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1191
    label "rugby"
  ]
  node [
    id 1192
    label "AWS"
  ]
  node [
    id 1193
    label "posta&#263;"
  ]
  node [
    id 1194
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1195
    label "blok"
  ]
  node [
    id 1196
    label "PO"
  ]
  node [
    id 1197
    label "si&#322;a"
  ]
  node [
    id 1198
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1199
    label "Federali&#347;ci"
  ]
  node [
    id 1200
    label "PSL"
  ]
  node [
    id 1201
    label "Wigowie"
  ]
  node [
    id 1202
    label "ZChN"
  ]
  node [
    id 1203
    label "egzekutywa"
  ]
  node [
    id 1204
    label "The_Beatles"
  ]
  node [
    id 1205
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 1206
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1207
    label "unit"
  ]
  node [
    id 1208
    label "Depeche_Mode"
  ]
  node [
    id 1209
    label "zabudowania"
  ]
  node [
    id 1210
    label "group"
  ]
  node [
    id 1211
    label "schorzenie"
  ]
  node [
    id 1212
    label "ro&#347;lina"
  ]
  node [
    id 1213
    label "batch"
  ]
  node [
    id 1214
    label "pu&#322;k"
  ]
  node [
    id 1215
    label "dru&#380;yna_harcerska"
  ]
  node [
    id 1216
    label "t&#322;um"
  ]
  node [
    id 1217
    label "zoosocjologia"
  ]
  node [
    id 1218
    label "Tagalowie"
  ]
  node [
    id 1219
    label "Ugrowie"
  ]
  node [
    id 1220
    label "Retowie"
  ]
  node [
    id 1221
    label "podgromada"
  ]
  node [
    id 1222
    label "Negryci"
  ]
  node [
    id 1223
    label "Ladynowie"
  ]
  node [
    id 1224
    label "Wizygoci"
  ]
  node [
    id 1225
    label "Dogonowie"
  ]
  node [
    id 1226
    label "linia"
  ]
  node [
    id 1227
    label "mikrobiologia"
  ]
  node [
    id 1228
    label "plemi&#281;"
  ]
  node [
    id 1229
    label "linia_filogenetyczna"
  ]
  node [
    id 1230
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1231
    label "gatunek"
  ]
  node [
    id 1232
    label "Do&#322;ganie"
  ]
  node [
    id 1233
    label "podrodzina"
  ]
  node [
    id 1234
    label "Indoira&#324;czycy"
  ]
  node [
    id 1235
    label "paleontologia"
  ]
  node [
    id 1236
    label "Kozacy"
  ]
  node [
    id 1237
    label "Indoariowie"
  ]
  node [
    id 1238
    label "Maroni"
  ]
  node [
    id 1239
    label "Kumbrowie"
  ]
  node [
    id 1240
    label "Po&#322;owcy"
  ]
  node [
    id 1241
    label "Nogajowie"
  ]
  node [
    id 1242
    label "Nawahowie"
  ]
  node [
    id 1243
    label "ornitologia"
  ]
  node [
    id 1244
    label "podk&#322;ad"
  ]
  node [
    id 1245
    label "Wenedowie"
  ]
  node [
    id 1246
    label "Majowie"
  ]
  node [
    id 1247
    label "Kipczacy"
  ]
  node [
    id 1248
    label "odmiana"
  ]
  node [
    id 1249
    label "Frygijczycy"
  ]
  node [
    id 1250
    label "grupa_etniczna"
  ]
  node [
    id 1251
    label "Paleoazjaci"
  ]
  node [
    id 1252
    label "teriologia"
  ]
  node [
    id 1253
    label "hufiec"
  ]
  node [
    id 1254
    label "Tocharowie"
  ]
  node [
    id 1255
    label "jednostka_organizacyjna"
  ]
  node [
    id 1256
    label "transuranowiec"
  ]
  node [
    id 1257
    label "dzia&#322;on"
  ]
  node [
    id 1258
    label "aktynowiec"
  ]
  node [
    id 1259
    label "bateria"
  ]
  node [
    id 1260
    label "kompania"
  ]
  node [
    id 1261
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 1262
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 1263
    label "kaftan"
  ]
  node [
    id 1264
    label "zwyci&#281;stwo"
  ]
  node [
    id 1265
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 1266
    label "podroby"
  ]
  node [
    id 1267
    label "grzyb"
  ]
  node [
    id 1268
    label "ozorkowate"
  ]
  node [
    id 1269
    label "paso&#380;yt"
  ]
  node [
    id 1270
    label "saprotrof"
  ]
  node [
    id 1271
    label "pieczarkowiec"
  ]
  node [
    id 1272
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 1273
    label "towar"
  ]
  node [
    id 1274
    label "mi&#281;so"
  ]
  node [
    id 1275
    label "saprofit"
  ]
  node [
    id 1276
    label "odwszawianie"
  ]
  node [
    id 1277
    label "odrobaczanie"
  ]
  node [
    id 1278
    label "odrobacza&#263;"
  ]
  node [
    id 1279
    label "konsument"
  ]
  node [
    id 1280
    label "istota_&#380;ywa"
  ]
  node [
    id 1281
    label "agaric"
  ]
  node [
    id 1282
    label "bed&#322;ka"
  ]
  node [
    id 1283
    label "pieczarniak"
  ]
  node [
    id 1284
    label "pieczarkowce"
  ]
  node [
    id 1285
    label "kszta&#322;t"
  ]
  node [
    id 1286
    label "starzec"
  ]
  node [
    id 1287
    label "papierzak"
  ]
  node [
    id 1288
    label "choroba_somatyczna"
  ]
  node [
    id 1289
    label "fungus"
  ]
  node [
    id 1290
    label "grzyby"
  ]
  node [
    id 1291
    label "blanszownik"
  ]
  node [
    id 1292
    label "zrz&#281;da"
  ]
  node [
    id 1293
    label "tetryk"
  ]
  node [
    id 1294
    label "ramolenie"
  ]
  node [
    id 1295
    label "borowiec"
  ]
  node [
    id 1296
    label "fungal_infection"
  ]
  node [
    id 1297
    label "pierdo&#322;a"
  ]
  node [
    id 1298
    label "ko&#378;larz"
  ]
  node [
    id 1299
    label "zramolenie"
  ]
  node [
    id 1300
    label "gametangium"
  ]
  node [
    id 1301
    label "plechowiec"
  ]
  node [
    id 1302
    label "borowikowate"
  ]
  node [
    id 1303
    label "plemnia"
  ]
  node [
    id 1304
    label "zarodnia"
  ]
  node [
    id 1305
    label "snekkar"
  ]
  node [
    id 1306
    label "drakkar"
  ]
  node [
    id 1307
    label "skeid"
  ]
  node [
    id 1308
    label "byrding"
  ]
  node [
    id 1309
    label "knara"
  ]
  node [
    id 1310
    label "rozb&#243;jnik"
  ]
  node [
    id 1311
    label "Skandynaw"
  ]
  node [
    id 1312
    label "wojownik"
  ]
  node [
    id 1313
    label "mieszkaniec"
  ]
  node [
    id 1314
    label "Europejczyk"
  ]
  node [
    id 1315
    label "brygant"
  ]
  node [
    id 1316
    label "bandyta"
  ]
  node [
    id 1317
    label "osada"
  ]
  node [
    id 1318
    label "walcz&#261;cy"
  ]
  node [
    id 1319
    label "langskip"
  ]
  node [
    id 1320
    label "statek"
  ]
  node [
    id 1321
    label "jednomasztowiec"
  ]
  node [
    id 1322
    label "ster_wios&#322;owy"
  ]
  node [
    id 1323
    label "poszycie_zak&#322;adkowe"
  ]
  node [
    id 1324
    label "frachtowiec"
  ]
  node [
    id 1325
    label "polski"
  ]
  node [
    id 1326
    label "po_mazowiecku"
  ]
  node [
    id 1327
    label "regionalny"
  ]
  node [
    id 1328
    label "Polish"
  ]
  node [
    id 1329
    label "goniony"
  ]
  node [
    id 1330
    label "oberek"
  ]
  node [
    id 1331
    label "ryba_po_grecku"
  ]
  node [
    id 1332
    label "sztajer"
  ]
  node [
    id 1333
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 1334
    label "krakowiak"
  ]
  node [
    id 1335
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 1336
    label "pierogi_ruskie"
  ]
  node [
    id 1337
    label "lacki"
  ]
  node [
    id 1338
    label "polak"
  ]
  node [
    id 1339
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 1340
    label "chodzony"
  ]
  node [
    id 1341
    label "po_polsku"
  ]
  node [
    id 1342
    label "mazur"
  ]
  node [
    id 1343
    label "polsko"
  ]
  node [
    id 1344
    label "skoczny"
  ]
  node [
    id 1345
    label "drabant"
  ]
  node [
    id 1346
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 1347
    label "tradycyjny"
  ]
  node [
    id 1348
    label "regionalnie"
  ]
  node [
    id 1349
    label "lokalny"
  ]
  node [
    id 1350
    label "typowy"
  ]
  node [
    id 1351
    label "flicker"
  ]
  node [
    id 1352
    label "odrobina"
  ]
  node [
    id 1353
    label "b&#322;ysk"
  ]
  node [
    id 1354
    label "odblask"
  ]
  node [
    id 1355
    label "glint"
  ]
  node [
    id 1356
    label "discharge"
  ]
  node [
    id 1357
    label "&#322;ysk"
  ]
  node [
    id 1358
    label "porz&#261;dek"
  ]
  node [
    id 1359
    label "wyraz"
  ]
  node [
    id 1360
    label "oznaka"
  ]
  node [
    id 1361
    label "b&#322;ystka"
  ]
  node [
    id 1362
    label "radiance"
  ]
  node [
    id 1363
    label "ostentation"
  ]
  node [
    id 1364
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1365
    label "luminosity"
  ]
  node [
    id 1366
    label "wspania&#322;o&#347;&#263;"
  ]
  node [
    id 1367
    label "light"
  ]
  node [
    id 1368
    label "znaczek"
  ]
  node [
    id 1369
    label "dash"
  ]
  node [
    id 1370
    label "grain"
  ]
  node [
    id 1371
    label "intensywno&#347;&#263;"
  ]
  node [
    id 1372
    label "spalin&#243;wka"
  ]
  node [
    id 1373
    label "pojazd_niemechaniczny"
  ]
  node [
    id 1374
    label "regaty"
  ]
  node [
    id 1375
    label "kratownica"
  ]
  node [
    id 1376
    label "pok&#322;ad"
  ]
  node [
    id 1377
    label "drzewce"
  ]
  node [
    id 1378
    label "ster"
  ]
  node [
    id 1379
    label "dobija&#263;"
  ]
  node [
    id 1380
    label "zakotwiczenie"
  ]
  node [
    id 1381
    label "odcumowywa&#263;"
  ]
  node [
    id 1382
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1383
    label "odkotwicza&#263;"
  ]
  node [
    id 1384
    label "zwodowanie"
  ]
  node [
    id 1385
    label "odkotwiczy&#263;"
  ]
  node [
    id 1386
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 1387
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 1388
    label "odcumowanie"
  ]
  node [
    id 1389
    label "odcumowa&#263;"
  ]
  node [
    id 1390
    label "zacumowanie"
  ]
  node [
    id 1391
    label "kotwiczenie"
  ]
  node [
    id 1392
    label "kad&#322;ub"
  ]
  node [
    id 1393
    label "reling"
  ]
  node [
    id 1394
    label "kabina"
  ]
  node [
    id 1395
    label "kotwiczy&#263;"
  ]
  node [
    id 1396
    label "szkutnictwo"
  ]
  node [
    id 1397
    label "korab"
  ]
  node [
    id 1398
    label "odbijacz"
  ]
  node [
    id 1399
    label "dobijanie"
  ]
  node [
    id 1400
    label "dobi&#263;"
  ]
  node [
    id 1401
    label "proporczyk"
  ]
  node [
    id 1402
    label "odkotwiczenie"
  ]
  node [
    id 1403
    label "kabestan"
  ]
  node [
    id 1404
    label "cumowanie"
  ]
  node [
    id 1405
    label "zaw&#243;r_denny"
  ]
  node [
    id 1406
    label "zadokowa&#263;"
  ]
  node [
    id 1407
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 1408
    label "flota"
  ]
  node [
    id 1409
    label "rostra"
  ]
  node [
    id 1410
    label "zr&#281;bnica"
  ]
  node [
    id 1411
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1412
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 1413
    label "bumsztak"
  ]
  node [
    id 1414
    label "sterownik_automatyczny"
  ]
  node [
    id 1415
    label "nadbud&#243;wka"
  ]
  node [
    id 1416
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 1417
    label "cumowa&#263;"
  ]
  node [
    id 1418
    label "armator"
  ]
  node [
    id 1419
    label "odcumowywanie"
  ]
  node [
    id 1420
    label "zakotwiczy&#263;"
  ]
  node [
    id 1421
    label "zacumowa&#263;"
  ]
  node [
    id 1422
    label "wodowanie"
  ]
  node [
    id 1423
    label "dobicie"
  ]
  node [
    id 1424
    label "zadokowanie"
  ]
  node [
    id 1425
    label "dokowa&#263;"
  ]
  node [
    id 1426
    label "trap"
  ]
  node [
    id 1427
    label "kotwica"
  ]
  node [
    id 1428
    label "odkotwiczanie"
  ]
  node [
    id 1429
    label "luk"
  ]
  node [
    id 1430
    label "dzi&#243;b"
  ]
  node [
    id 1431
    label "armada"
  ]
  node [
    id 1432
    label "&#380;yroskop"
  ]
  node [
    id 1433
    label "futr&#243;wka"
  ]
  node [
    id 1434
    label "pojazd"
  ]
  node [
    id 1435
    label "sztormtrap"
  ]
  node [
    id 1436
    label "skrajnik"
  ]
  node [
    id 1437
    label "dokowanie"
  ]
  node [
    id 1438
    label "zwodowa&#263;"
  ]
  node [
    id 1439
    label "grobla"
  ]
  node [
    id 1440
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 1441
    label "pr&#281;t"
  ]
  node [
    id 1442
    label "omasztowanie"
  ]
  node [
    id 1443
    label "pi&#281;ta"
  ]
  node [
    id 1444
    label "bro&#324;_obuchowa"
  ]
  node [
    id 1445
    label "uchwyt"
  ]
  node [
    id 1446
    label "dr&#261;&#380;ek"
  ]
  node [
    id 1447
    label "belka"
  ]
  node [
    id 1448
    label "przyrz&#261;d_naukowy"
  ]
  node [
    id 1449
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 1450
    label "d&#378;wigar"
  ]
  node [
    id 1451
    label "krata"
  ]
  node [
    id 1452
    label "bar"
  ]
  node [
    id 1453
    label "wicket"
  ]
  node [
    id 1454
    label "okratowanie"
  ]
  node [
    id 1455
    label "konstrukcja"
  ]
  node [
    id 1456
    label "ochrona"
  ]
  node [
    id 1457
    label "metryczna_jednostka_obj&#281;to&#347;ci"
  ]
  node [
    id 1458
    label "sterownica"
  ]
  node [
    id 1459
    label "kilometr_sze&#347;cienny"
  ]
  node [
    id 1460
    label "wolant"
  ]
  node [
    id 1461
    label "powierzchnia_sterowa"
  ]
  node [
    id 1462
    label "sterolotka"
  ]
  node [
    id 1463
    label "&#380;agl&#243;wka"
  ]
  node [
    id 1464
    label "statek_powietrzny"
  ]
  node [
    id 1465
    label "rumpel"
  ]
  node [
    id 1466
    label "przyw&#243;dztwo"
  ]
  node [
    id 1467
    label "jacht"
  ]
  node [
    id 1468
    label "sp&#261;g"
  ]
  node [
    id 1469
    label "pok&#322;adnik"
  ]
  node [
    id 1470
    label "warstwa"
  ]
  node [
    id 1471
    label "powierzchnia"
  ]
  node [
    id 1472
    label "poszycie_pok&#322;adowe"
  ]
  node [
    id 1473
    label "p&#243;&#322;pok&#322;ad"
  ]
  node [
    id 1474
    label "kipa"
  ]
  node [
    id 1475
    label "samolot"
  ]
  node [
    id 1476
    label "jut"
  ]
  node [
    id 1477
    label "z&#322;o&#380;e"
  ]
  node [
    id 1478
    label "model"
  ]
  node [
    id 1479
    label "kosiarka"
  ]
  node [
    id 1480
    label "pi&#322;a_mechaniczna"
  ]
  node [
    id 1481
    label "lokomotywa"
  ]
  node [
    id 1482
    label "przew&#243;d"
  ]
  node [
    id 1483
    label "szarpanka"
  ]
  node [
    id 1484
    label "rura"
  ]
  node [
    id 1485
    label "wy&#347;cig"
  ]
  node [
    id 1486
    label "serw"
  ]
  node [
    id 1487
    label "dwumecz"
  ]
  node [
    id 1488
    label "egzamin"
  ]
  node [
    id 1489
    label "gracz"
  ]
  node [
    id 1490
    label "protection"
  ]
  node [
    id 1491
    label "poparcie"
  ]
  node [
    id 1492
    label "defense"
  ]
  node [
    id 1493
    label "s&#261;d"
  ]
  node [
    id 1494
    label "auspices"
  ]
  node [
    id 1495
    label "defensive_structure"
  ]
  node [
    id 1496
    label "guard_duty"
  ]
  node [
    id 1497
    label "strona"
  ]
  node [
    id 1498
    label "okre&#347;lony"
  ]
  node [
    id 1499
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1500
    label "wiadomy"
  ]
  node [
    id 1501
    label "ten"
  ]
  node [
    id 1502
    label "nazewnictwo"
  ]
  node [
    id 1503
    label "term"
  ]
  node [
    id 1504
    label "przypadni&#281;cie"
  ]
  node [
    id 1505
    label "ekspiracja"
  ]
  node [
    id 1506
    label "przypa&#347;&#263;"
  ]
  node [
    id 1507
    label "chronogram"
  ]
  node [
    id 1508
    label "praktyka"
  ]
  node [
    id 1509
    label "nazwa"
  ]
  node [
    id 1510
    label "wezwanie"
  ]
  node [
    id 1511
    label "patron"
  ]
  node [
    id 1512
    label "practice"
  ]
  node [
    id 1513
    label "wiedza"
  ]
  node [
    id 1514
    label "znawstwo"
  ]
  node [
    id 1515
    label "czyn"
  ]
  node [
    id 1516
    label "nauka"
  ]
  node [
    id 1517
    label "zwyczaj"
  ]
  node [
    id 1518
    label "eksperiencja"
  ]
  node [
    id 1519
    label "praca"
  ]
  node [
    id 1520
    label "s&#322;ownictwo"
  ]
  node [
    id 1521
    label "terminology"
  ]
  node [
    id 1522
    label "wydech"
  ]
  node [
    id 1523
    label "ekspirowanie"
  ]
  node [
    id 1524
    label "spodoba&#263;_si&#281;"
  ]
  node [
    id 1525
    label "trafi&#263;_si&#281;"
  ]
  node [
    id 1526
    label "fall"
  ]
  node [
    id 1527
    label "pa&#347;&#263;"
  ]
  node [
    id 1528
    label "dotrze&#263;"
  ]
  node [
    id 1529
    label "wypa&#347;&#263;"
  ]
  node [
    id 1530
    label "przywrze&#263;"
  ]
  node [
    id 1531
    label "rzuci&#263;_si&#281;"
  ]
  node [
    id 1532
    label "zapis"
  ]
  node [
    id 1533
    label "barok"
  ]
  node [
    id 1534
    label "przytulenie_si&#281;"
  ]
  node [
    id 1535
    label "spadni&#281;cie"
  ]
  node [
    id 1536
    label "okrojenie_si&#281;"
  ]
  node [
    id 1537
    label "prolapse"
  ]
  node [
    id 1538
    label "yearbook"
  ]
  node [
    id 1539
    label "czasopismo"
  ]
  node [
    id 1540
    label "kronika"
  ]
  node [
    id 1541
    label "chronograf"
  ]
  node [
    id 1542
    label "latopis"
  ]
  node [
    id 1543
    label "ksi&#281;ga"
  ]
  node [
    id 1544
    label "psychotest"
  ]
  node [
    id 1545
    label "pismo"
  ]
  node [
    id 1546
    label "communication"
  ]
  node [
    id 1547
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1548
    label "wk&#322;ad"
  ]
  node [
    id 1549
    label "zajawka"
  ]
  node [
    id 1550
    label "ok&#322;adka"
  ]
  node [
    id 1551
    label "Zwrotnica"
  ]
  node [
    id 1552
    label "prasa"
  ]
  node [
    id 1553
    label "nauczyciel"
  ]
  node [
    id 1554
    label "opiekun"
  ]
  node [
    id 1555
    label "konsultant"
  ]
  node [
    id 1556
    label "Daniel_Dubicki"
  ]
  node [
    id 1557
    label "mentor"
  ]
  node [
    id 1558
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1559
    label "wapniak"
  ]
  node [
    id 1560
    label "os&#322;abia&#263;"
  ]
  node [
    id 1561
    label "hominid"
  ]
  node [
    id 1562
    label "podw&#322;adny"
  ]
  node [
    id 1563
    label "os&#322;abianie"
  ]
  node [
    id 1564
    label "g&#322;owa"
  ]
  node [
    id 1565
    label "figura"
  ]
  node [
    id 1566
    label "portrecista"
  ]
  node [
    id 1567
    label "dwun&#243;g"
  ]
  node [
    id 1568
    label "profanum"
  ]
  node [
    id 1569
    label "mikrokosmos"
  ]
  node [
    id 1570
    label "nasada"
  ]
  node [
    id 1571
    label "duch"
  ]
  node [
    id 1572
    label "antropochoria"
  ]
  node [
    id 1573
    label "osoba"
  ]
  node [
    id 1574
    label "wz&#243;r"
  ]
  node [
    id 1575
    label "senior"
  ]
  node [
    id 1576
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1577
    label "Adam"
  ]
  node [
    id 1578
    label "homo_sapiens"
  ]
  node [
    id 1579
    label "polifag"
  ]
  node [
    id 1580
    label "nadzorca"
  ]
  node [
    id 1581
    label "funkcjonariusz"
  ]
  node [
    id 1582
    label "autorytet"
  ]
  node [
    id 1583
    label "sensat"
  ]
  node [
    id 1584
    label "diagnosta"
  ]
  node [
    id 1585
    label "sztywniak"
  ]
  node [
    id 1586
    label "Towia&#324;ski"
  ]
  node [
    id 1587
    label "doradca"
  ]
  node [
    id 1588
    label "belfer"
  ]
  node [
    id 1589
    label "kszta&#322;ciciel"
  ]
  node [
    id 1590
    label "preceptor"
  ]
  node [
    id 1591
    label "pedagog"
  ]
  node [
    id 1592
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1593
    label "szkolnik"
  ]
  node [
    id 1594
    label "profesor"
  ]
  node [
    id 1595
    label "popularyzator"
  ]
  node [
    id 1596
    label "pracownik"
  ]
  node [
    id 1597
    label "ekspert"
  ]
  node [
    id 1598
    label "ognisko"
  ]
  node [
    id 1599
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1600
    label "powalenie"
  ]
  node [
    id 1601
    label "odezwanie_si&#281;"
  ]
  node [
    id 1602
    label "grupa_ryzyka"
  ]
  node [
    id 1603
    label "przypadek"
  ]
  node [
    id 1604
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1605
    label "nabawienie_si&#281;"
  ]
  node [
    id 1606
    label "inkubacja"
  ]
  node [
    id 1607
    label "kryzys"
  ]
  node [
    id 1608
    label "powali&#263;"
  ]
  node [
    id 1609
    label "remisja"
  ]
  node [
    id 1610
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1611
    label "zajmowa&#263;"
  ]
  node [
    id 1612
    label "zaburzenie"
  ]
  node [
    id 1613
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1614
    label "badanie_histopatologiczne"
  ]
  node [
    id 1615
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1616
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1617
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1618
    label "odzywanie_si&#281;"
  ]
  node [
    id 1619
    label "diagnoza"
  ]
  node [
    id 1620
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1621
    label "nabawianie_si&#281;"
  ]
  node [
    id 1622
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1623
    label "zajmowanie"
  ]
  node [
    id 1624
    label "agglomeration"
  ]
  node [
    id 1625
    label "uwaga"
  ]
  node [
    id 1626
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 1627
    label "przegrupowanie"
  ]
  node [
    id 1628
    label "congestion"
  ]
  node [
    id 1629
    label "zgromadzenie"
  ]
  node [
    id 1630
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1631
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1632
    label "concentration"
  ]
  node [
    id 1633
    label "kompleks"
  ]
  node [
    id 1634
    label "obszar"
  ]
  node [
    id 1635
    label "Kurpie"
  ]
  node [
    id 1636
    label "Mogielnica"
  ]
  node [
    id 1637
    label "zbiorowisko"
  ]
  node [
    id 1638
    label "ro&#347;liny"
  ]
  node [
    id 1639
    label "p&#281;d"
  ]
  node [
    id 1640
    label "wegetowanie"
  ]
  node [
    id 1641
    label "zadziorek"
  ]
  node [
    id 1642
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1643
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1644
    label "do&#322;owa&#263;"
  ]
  node [
    id 1645
    label "wegetacja"
  ]
  node [
    id 1646
    label "owoc"
  ]
  node [
    id 1647
    label "strzyc"
  ]
  node [
    id 1648
    label "w&#322;&#243;kno"
  ]
  node [
    id 1649
    label "g&#322;uszenie"
  ]
  node [
    id 1650
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1651
    label "fitotron"
  ]
  node [
    id 1652
    label "bulwka"
  ]
  node [
    id 1653
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1654
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1655
    label "epiderma"
  ]
  node [
    id 1656
    label "gumoza"
  ]
  node [
    id 1657
    label "strzy&#380;enie"
  ]
  node [
    id 1658
    label "wypotnik"
  ]
  node [
    id 1659
    label "flawonoid"
  ]
  node [
    id 1660
    label "wyro&#347;le"
  ]
  node [
    id 1661
    label "do&#322;owanie"
  ]
  node [
    id 1662
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1663
    label "pora&#380;a&#263;"
  ]
  node [
    id 1664
    label "fitocenoza"
  ]
  node [
    id 1665
    label "hodowla"
  ]
  node [
    id 1666
    label "fotoautotrof"
  ]
  node [
    id 1667
    label "nieuleczalnie_chory"
  ]
  node [
    id 1668
    label "wegetowa&#263;"
  ]
  node [
    id 1669
    label "pochewka"
  ]
  node [
    id 1670
    label "sok"
  ]
  node [
    id 1671
    label "system_korzeniowy"
  ]
  node [
    id 1672
    label "zawi&#261;zek"
  ]
  node [
    id 1673
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1674
    label "mie&#263;_miejsce"
  ]
  node [
    id 1675
    label "equal"
  ]
  node [
    id 1676
    label "trwa&#263;"
  ]
  node [
    id 1677
    label "chodzi&#263;"
  ]
  node [
    id 1678
    label "si&#281;ga&#263;"
  ]
  node [
    id 1679
    label "stand"
  ]
  node [
    id 1680
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1681
    label "uczestniczy&#263;"
  ]
  node [
    id 1682
    label "participate"
  ]
  node [
    id 1683
    label "robi&#263;"
  ]
  node [
    id 1684
    label "istnie&#263;"
  ]
  node [
    id 1685
    label "pozostawa&#263;"
  ]
  node [
    id 1686
    label "zostawa&#263;"
  ]
  node [
    id 1687
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1688
    label "adhere"
  ]
  node [
    id 1689
    label "compass"
  ]
  node [
    id 1690
    label "korzysta&#263;"
  ]
  node [
    id 1691
    label "appreciation"
  ]
  node [
    id 1692
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1693
    label "dociera&#263;"
  ]
  node [
    id 1694
    label "get"
  ]
  node [
    id 1695
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1696
    label "mierzy&#263;"
  ]
  node [
    id 1697
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1698
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1699
    label "exsert"
  ]
  node [
    id 1700
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1701
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1702
    label "run"
  ]
  node [
    id 1703
    label "bangla&#263;"
  ]
  node [
    id 1704
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1705
    label "przebiega&#263;"
  ]
  node [
    id 1706
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1707
    label "proceed"
  ]
  node [
    id 1708
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1709
    label "carry"
  ]
  node [
    id 1710
    label "bywa&#263;"
  ]
  node [
    id 1711
    label "dziama&#263;"
  ]
  node [
    id 1712
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1713
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1714
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1715
    label "str&#243;j"
  ]
  node [
    id 1716
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1717
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1718
    label "krok"
  ]
  node [
    id 1719
    label "tryb"
  ]
  node [
    id 1720
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1721
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1722
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1723
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1724
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1725
    label "continue"
  ]
  node [
    id 1726
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1727
    label "Ohio"
  ]
  node [
    id 1728
    label "wci&#281;cie"
  ]
  node [
    id 1729
    label "Nowy_York"
  ]
  node [
    id 1730
    label "samopoczucie"
  ]
  node [
    id 1731
    label "Illinois"
  ]
  node [
    id 1732
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1733
    label "state"
  ]
  node [
    id 1734
    label "Jukatan"
  ]
  node [
    id 1735
    label "Kalifornia"
  ]
  node [
    id 1736
    label "Wirginia"
  ]
  node [
    id 1737
    label "wektor"
  ]
  node [
    id 1738
    label "Goa"
  ]
  node [
    id 1739
    label "Teksas"
  ]
  node [
    id 1740
    label "Waszyngton"
  ]
  node [
    id 1741
    label "Massachusetts"
  ]
  node [
    id 1742
    label "Alaska"
  ]
  node [
    id 1743
    label "Arakan"
  ]
  node [
    id 1744
    label "Hawaje"
  ]
  node [
    id 1745
    label "Maryland"
  ]
  node [
    id 1746
    label "punkt"
  ]
  node [
    id 1747
    label "Michigan"
  ]
  node [
    id 1748
    label "Arizona"
  ]
  node [
    id 1749
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1750
    label "Georgia"
  ]
  node [
    id 1751
    label "Pensylwania"
  ]
  node [
    id 1752
    label "shape"
  ]
  node [
    id 1753
    label "Luizjana"
  ]
  node [
    id 1754
    label "Nowy_Meksyk"
  ]
  node [
    id 1755
    label "Alabama"
  ]
  node [
    id 1756
    label "Kansas"
  ]
  node [
    id 1757
    label "Oregon"
  ]
  node [
    id 1758
    label "Oklahoma"
  ]
  node [
    id 1759
    label "Floryda"
  ]
  node [
    id 1760
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1761
    label "stopie&#324;_naukowy"
  ]
  node [
    id 1762
    label "tytu&#322;"
  ]
  node [
    id 1763
    label "debit"
  ]
  node [
    id 1764
    label "redaktor"
  ]
  node [
    id 1765
    label "druk"
  ]
  node [
    id 1766
    label "publikacja"
  ]
  node [
    id 1767
    label "nadtytu&#322;"
  ]
  node [
    id 1768
    label "szata_graficzna"
  ]
  node [
    id 1769
    label "tytulatura"
  ]
  node [
    id 1770
    label "wydawa&#263;"
  ]
  node [
    id 1771
    label "elevation"
  ]
  node [
    id 1772
    label "wyda&#263;"
  ]
  node [
    id 1773
    label "mianowaniec"
  ]
  node [
    id 1774
    label "poster"
  ]
  node [
    id 1775
    label "podtytu&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 242
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 244
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 45
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 15
    target 61
  ]
  edge [
    source 15
    target 62
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 287
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 347
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 416
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 399
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 523
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 18
    target 1085
  ]
  edge [
    source 18
    target 1086
  ]
  edge [
    source 18
    target 1087
  ]
  edge [
    source 18
    target 1088
  ]
  edge [
    source 18
    target 1089
  ]
  edge [
    source 18
    target 1090
  ]
  edge [
    source 18
    target 1091
  ]
  edge [
    source 18
    target 1092
  ]
  edge [
    source 18
    target 1093
  ]
  edge [
    source 18
    target 1094
  ]
  edge [
    source 18
    target 1095
  ]
  edge [
    source 18
    target 1096
  ]
  edge [
    source 18
    target 1097
  ]
  edge [
    source 18
    target 1098
  ]
  edge [
    source 18
    target 1099
  ]
  edge [
    source 18
    target 1100
  ]
  edge [
    source 18
    target 1101
  ]
  edge [
    source 18
    target 1102
  ]
  edge [
    source 18
    target 1103
  ]
  edge [
    source 18
    target 1104
  ]
  edge [
    source 18
    target 1105
  ]
  edge [
    source 18
    target 1106
  ]
  edge [
    source 18
    target 1107
  ]
  edge [
    source 18
    target 1108
  ]
  edge [
    source 18
    target 1109
  ]
  edge [
    source 18
    target 1110
  ]
  edge [
    source 18
    target 1111
  ]
  edge [
    source 18
    target 1112
  ]
  edge [
    source 18
    target 1113
  ]
  edge [
    source 18
    target 1114
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 18
    target 1161
  ]
  edge [
    source 18
    target 1162
  ]
  edge [
    source 18
    target 1163
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1164
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 1165
  ]
  edge [
    source 20
    target 1166
  ]
  edge [
    source 20
    target 1167
  ]
  edge [
    source 20
    target 1168
  ]
  edge [
    source 20
    target 662
  ]
  edge [
    source 20
    target 722
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 320
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 314
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 308
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 399
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 22
    target 1199
  ]
  edge [
    source 22
    target 1200
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 322
  ]
  edge [
    source 22
    target 1201
  ]
  edge [
    source 22
    target 1202
  ]
  edge [
    source 22
    target 1203
  ]
  edge [
    source 22
    target 35
  ]
  edge [
    source 22
    target 1204
  ]
  edge [
    source 22
    target 1205
  ]
  edge [
    source 22
    target 1206
  ]
  edge [
    source 22
    target 1207
  ]
  edge [
    source 22
    target 1208
  ]
  edge [
    source 22
    target 837
  ]
  edge [
    source 22
    target 250
  ]
  edge [
    source 22
    target 272
  ]
  edge [
    source 22
    target 259
  ]
  edge [
    source 22
    target 418
  ]
  edge [
    source 22
    target 262
  ]
  edge [
    source 22
    target 275
  ]
  edge [
    source 22
    target 1209
  ]
  edge [
    source 22
    target 1210
  ]
  edge [
    source 22
    target 1211
  ]
  edge [
    source 22
    target 1212
  ]
  edge [
    source 22
    target 1213
  ]
  edge [
    source 22
    target 273
  ]
  edge [
    source 22
    target 265
  ]
  edge [
    source 22
    target 1214
  ]
  edge [
    source 22
    target 1215
  ]
  edge [
    source 22
    target 1216
  ]
  edge [
    source 22
    target 1217
  ]
  edge [
    source 22
    target 252
  ]
  edge [
    source 22
    target 1218
  ]
  edge [
    source 22
    target 1219
  ]
  edge [
    source 22
    target 1220
  ]
  edge [
    source 22
    target 1221
  ]
  edge [
    source 22
    target 1222
  ]
  edge [
    source 22
    target 1223
  ]
  edge [
    source 22
    target 1224
  ]
  edge [
    source 22
    target 1225
  ]
  edge [
    source 22
    target 1226
  ]
  edge [
    source 22
    target 891
  ]
  edge [
    source 22
    target 1227
  ]
  edge [
    source 22
    target 1228
  ]
  edge [
    source 22
    target 1229
  ]
  edge [
    source 22
    target 1230
  ]
  edge [
    source 22
    target 1231
  ]
  edge [
    source 22
    target 1232
  ]
  edge [
    source 22
    target 1233
  ]
  edge [
    source 22
    target 1234
  ]
  edge [
    source 22
    target 1235
  ]
  edge [
    source 22
    target 1236
  ]
  edge [
    source 22
    target 1237
  ]
  edge [
    source 22
    target 1238
  ]
  edge [
    source 22
    target 1239
  ]
  edge [
    source 22
    target 1240
  ]
  edge [
    source 22
    target 1241
  ]
  edge [
    source 22
    target 1242
  ]
  edge [
    source 22
    target 1243
  ]
  edge [
    source 22
    target 1244
  ]
  edge [
    source 22
    target 1245
  ]
  edge [
    source 22
    target 1246
  ]
  edge [
    source 22
    target 1247
  ]
  edge [
    source 22
    target 1248
  ]
  edge [
    source 22
    target 1249
  ]
  edge [
    source 22
    target 1250
  ]
  edge [
    source 22
    target 1251
  ]
  edge [
    source 22
    target 1252
  ]
  edge [
    source 22
    target 1253
  ]
  edge [
    source 22
    target 1254
  ]
  edge [
    source 22
    target 1255
  ]
  edge [
    source 22
    target 1256
  ]
  edge [
    source 22
    target 1257
  ]
  edge [
    source 22
    target 1258
  ]
  edge [
    source 22
    target 1259
  ]
  edge [
    source 22
    target 1260
  ]
  edge [
    source 22
    target 257
  ]
  edge [
    source 22
    target 1261
  ]
  edge [
    source 22
    target 1262
  ]
  edge [
    source 22
    target 1263
  ]
  edge [
    source 22
    target 1264
  ]
  edge [
    source 22
    target 1265
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 35
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 1266
  ]
  edge [
    source 24
    target 1267
  ]
  edge [
    source 24
    target 1268
  ]
  edge [
    source 24
    target 1269
  ]
  edge [
    source 24
    target 1270
  ]
  edge [
    source 24
    target 1271
  ]
  edge [
    source 24
    target 1272
  ]
  edge [
    source 24
    target 1273
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 1274
  ]
  edge [
    source 24
    target 1275
  ]
  edge [
    source 24
    target 1276
  ]
  edge [
    source 24
    target 386
  ]
  edge [
    source 24
    target 1277
  ]
  edge [
    source 24
    target 1278
  ]
  edge [
    source 24
    target 1279
  ]
  edge [
    source 24
    target 1280
  ]
  edge [
    source 24
    target 1281
  ]
  edge [
    source 24
    target 1282
  ]
  edge [
    source 24
    target 1283
  ]
  edge [
    source 24
    target 1284
  ]
  edge [
    source 24
    target 1285
  ]
  edge [
    source 24
    target 1286
  ]
  edge [
    source 24
    target 1287
  ]
  edge [
    source 24
    target 1288
  ]
  edge [
    source 24
    target 1289
  ]
  edge [
    source 24
    target 1290
  ]
  edge [
    source 24
    target 1291
  ]
  edge [
    source 24
    target 1292
  ]
  edge [
    source 24
    target 1293
  ]
  edge [
    source 24
    target 1294
  ]
  edge [
    source 24
    target 1295
  ]
  edge [
    source 24
    target 1296
  ]
  edge [
    source 24
    target 1297
  ]
  edge [
    source 24
    target 1298
  ]
  edge [
    source 24
    target 1299
  ]
  edge [
    source 24
    target 1300
  ]
  edge [
    source 24
    target 1301
  ]
  edge [
    source 24
    target 1302
  ]
  edge [
    source 24
    target 1303
  ]
  edge [
    source 24
    target 1304
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 36
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 25
    target 1305
  ]
  edge [
    source 25
    target 1306
  ]
  edge [
    source 25
    target 1307
  ]
  edge [
    source 25
    target 1308
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 1310
  ]
  edge [
    source 25
    target 1311
  ]
  edge [
    source 25
    target 1312
  ]
  edge [
    source 25
    target 1313
  ]
  edge [
    source 25
    target 1314
  ]
  edge [
    source 25
    target 1315
  ]
  edge [
    source 25
    target 1316
  ]
  edge [
    source 25
    target 1317
  ]
  edge [
    source 25
    target 452
  ]
  edge [
    source 25
    target 1318
  ]
  edge [
    source 25
    target 386
  ]
  edge [
    source 25
    target 1319
  ]
  edge [
    source 25
    target 1320
  ]
  edge [
    source 25
    target 1321
  ]
  edge [
    source 25
    target 1322
  ]
  edge [
    source 25
    target 1323
  ]
  edge [
    source 25
    target 1324
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1325
  ]
  edge [
    source 27
    target 1326
  ]
  edge [
    source 27
    target 1327
  ]
  edge [
    source 27
    target 116
  ]
  edge [
    source 27
    target 1328
  ]
  edge [
    source 27
    target 1329
  ]
  edge [
    source 27
    target 1330
  ]
  edge [
    source 27
    target 1331
  ]
  edge [
    source 27
    target 1332
  ]
  edge [
    source 27
    target 1333
  ]
  edge [
    source 27
    target 1334
  ]
  edge [
    source 27
    target 1335
  ]
  edge [
    source 27
    target 1336
  ]
  edge [
    source 27
    target 1337
  ]
  edge [
    source 27
    target 1338
  ]
  edge [
    source 27
    target 1339
  ]
  edge [
    source 27
    target 1340
  ]
  edge [
    source 27
    target 1341
  ]
  edge [
    source 27
    target 1342
  ]
  edge [
    source 27
    target 1343
  ]
  edge [
    source 27
    target 1344
  ]
  edge [
    source 27
    target 1345
  ]
  edge [
    source 27
    target 1346
  ]
  edge [
    source 27
    target 812
  ]
  edge [
    source 27
    target 1347
  ]
  edge [
    source 27
    target 1348
  ]
  edge [
    source 27
    target 1349
  ]
  edge [
    source 27
    target 1350
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 1351
  ]
  edge [
    source 28
    target 1352
  ]
  edge [
    source 28
    target 1353
  ]
  edge [
    source 28
    target 1354
  ]
  edge [
    source 28
    target 1355
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 1356
  ]
  edge [
    source 28
    target 1357
  ]
  edge [
    source 28
    target 1358
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 1359
  ]
  edge [
    source 28
    target 1360
  ]
  edge [
    source 28
    target 1361
  ]
  edge [
    source 28
    target 1362
  ]
  edge [
    source 28
    target 464
  ]
  edge [
    source 28
    target 1363
  ]
  edge [
    source 28
    target 1364
  ]
  edge [
    source 28
    target 1365
  ]
  edge [
    source 28
    target 1366
  ]
  edge [
    source 28
    target 1367
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 28
    target 1368
  ]
  edge [
    source 28
    target 1369
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 1370
  ]
  edge [
    source 28
    target 1371
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 44
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1372
  ]
  edge [
    source 31
    target 1373
  ]
  edge [
    source 31
    target 1320
  ]
  edge [
    source 31
    target 1374
  ]
  edge [
    source 31
    target 1375
  ]
  edge [
    source 31
    target 1376
  ]
  edge [
    source 31
    target 1377
  ]
  edge [
    source 31
    target 1378
  ]
  edge [
    source 31
    target 1379
  ]
  edge [
    source 31
    target 1380
  ]
  edge [
    source 31
    target 1381
  ]
  edge [
    source 31
    target 1382
  ]
  edge [
    source 31
    target 1383
  ]
  edge [
    source 31
    target 1384
  ]
  edge [
    source 31
    target 1385
  ]
  edge [
    source 31
    target 1386
  ]
  edge [
    source 31
    target 1387
  ]
  edge [
    source 31
    target 1388
  ]
  edge [
    source 31
    target 1389
  ]
  edge [
    source 31
    target 1390
  ]
  edge [
    source 31
    target 1391
  ]
  edge [
    source 31
    target 1392
  ]
  edge [
    source 31
    target 1393
  ]
  edge [
    source 31
    target 1394
  ]
  edge [
    source 31
    target 1395
  ]
  edge [
    source 31
    target 1396
  ]
  edge [
    source 31
    target 1397
  ]
  edge [
    source 31
    target 1398
  ]
  edge [
    source 31
    target 436
  ]
  edge [
    source 31
    target 1399
  ]
  edge [
    source 31
    target 1400
  ]
  edge [
    source 31
    target 1401
  ]
  edge [
    source 31
    target 1402
  ]
  edge [
    source 31
    target 1403
  ]
  edge [
    source 31
    target 1404
  ]
  edge [
    source 31
    target 1405
  ]
  edge [
    source 31
    target 1406
  ]
  edge [
    source 31
    target 1407
  ]
  edge [
    source 31
    target 1408
  ]
  edge [
    source 31
    target 1409
  ]
  edge [
    source 31
    target 1410
  ]
  edge [
    source 31
    target 1411
  ]
  edge [
    source 31
    target 1412
  ]
  edge [
    source 31
    target 1413
  ]
  edge [
    source 31
    target 1414
  ]
  edge [
    source 31
    target 1415
  ]
  edge [
    source 31
    target 1416
  ]
  edge [
    source 31
    target 1417
  ]
  edge [
    source 31
    target 1418
  ]
  edge [
    source 31
    target 1419
  ]
  edge [
    source 31
    target 1420
  ]
  edge [
    source 31
    target 1421
  ]
  edge [
    source 31
    target 1422
  ]
  edge [
    source 31
    target 1423
  ]
  edge [
    source 31
    target 1424
  ]
  edge [
    source 31
    target 1425
  ]
  edge [
    source 31
    target 1426
  ]
  edge [
    source 31
    target 1427
  ]
  edge [
    source 31
    target 1428
  ]
  edge [
    source 31
    target 1429
  ]
  edge [
    source 31
    target 1430
  ]
  edge [
    source 31
    target 1431
  ]
  edge [
    source 31
    target 1432
  ]
  edge [
    source 31
    target 1433
  ]
  edge [
    source 31
    target 1434
  ]
  edge [
    source 31
    target 1435
  ]
  edge [
    source 31
    target 1436
  ]
  edge [
    source 31
    target 1437
  ]
  edge [
    source 31
    target 1438
  ]
  edge [
    source 31
    target 1439
  ]
  edge [
    source 31
    target 1440
  ]
  edge [
    source 31
    target 1441
  ]
  edge [
    source 31
    target 1442
  ]
  edge [
    source 31
    target 1443
  ]
  edge [
    source 31
    target 1444
  ]
  edge [
    source 31
    target 1445
  ]
  edge [
    source 31
    target 1446
  ]
  edge [
    source 31
    target 1447
  ]
  edge [
    source 31
    target 1448
  ]
  edge [
    source 31
    target 1449
  ]
  edge [
    source 31
    target 1450
  ]
  edge [
    source 31
    target 1451
  ]
  edge [
    source 31
    target 1452
  ]
  edge [
    source 31
    target 1453
  ]
  edge [
    source 31
    target 1454
  ]
  edge [
    source 31
    target 1455
  ]
  edge [
    source 31
    target 1456
  ]
  edge [
    source 31
    target 1457
  ]
  edge [
    source 31
    target 1458
  ]
  edge [
    source 31
    target 1459
  ]
  edge [
    source 31
    target 1460
  ]
  edge [
    source 31
    target 1461
  ]
  edge [
    source 31
    target 1462
  ]
  edge [
    source 31
    target 1463
  ]
  edge [
    source 31
    target 1464
  ]
  edge [
    source 31
    target 1465
  ]
  edge [
    source 31
    target 805
  ]
  edge [
    source 31
    target 1466
  ]
  edge [
    source 31
    target 1467
  ]
  edge [
    source 31
    target 518
  ]
  edge [
    source 31
    target 1468
  ]
  edge [
    source 31
    target 242
  ]
  edge [
    source 31
    target 1469
  ]
  edge [
    source 31
    target 1470
  ]
  edge [
    source 31
    target 1471
  ]
  edge [
    source 31
    target 540
  ]
  edge [
    source 31
    target 1472
  ]
  edge [
    source 31
    target 1473
  ]
  edge [
    source 31
    target 1474
  ]
  edge [
    source 31
    target 1475
  ]
  edge [
    source 31
    target 1476
  ]
  edge [
    source 31
    target 1477
  ]
  edge [
    source 31
    target 1478
  ]
  edge [
    source 31
    target 1479
  ]
  edge [
    source 31
    target 1480
  ]
  edge [
    source 31
    target 1481
  ]
  edge [
    source 31
    target 1482
  ]
  edge [
    source 31
    target 1483
  ]
  edge [
    source 31
    target 1484
  ]
  edge [
    source 31
    target 1485
  ]
  edge [
    source 32
    target 397
  ]
  edge [
    source 32
    target 103
  ]
  edge [
    source 32
    target 155
  ]
  edge [
    source 32
    target 1486
  ]
  edge [
    source 32
    target 1487
  ]
  edge [
    source 32
    target 146
  ]
  edge [
    source 32
    target 147
  ]
  edge [
    source 32
    target 81
  ]
  edge [
    source 32
    target 148
  ]
  edge [
    source 32
    target 55
  ]
  edge [
    source 32
    target 54
  ]
  edge [
    source 32
    target 149
  ]
  edge [
    source 32
    target 150
  ]
  edge [
    source 32
    target 151
  ]
  edge [
    source 32
    target 152
  ]
  edge [
    source 32
    target 50
  ]
  edge [
    source 32
    target 153
  ]
  edge [
    source 32
    target 154
  ]
  edge [
    source 32
    target 156
  ]
  edge [
    source 32
    target 157
  ]
  edge [
    source 32
    target 158
  ]
  edge [
    source 32
    target 159
  ]
  edge [
    source 32
    target 160
  ]
  edge [
    source 32
    target 161
  ]
  edge [
    source 32
    target 1488
  ]
  edge [
    source 32
    target 68
  ]
  edge [
    source 32
    target 251
  ]
  edge [
    source 32
    target 1489
  ]
  edge [
    source 32
    target 300
  ]
  edge [
    source 32
    target 1490
  ]
  edge [
    source 32
    target 1491
  ]
  edge [
    source 32
    target 340
  ]
  edge [
    source 32
    target 1492
  ]
  edge [
    source 32
    target 1493
  ]
  edge [
    source 32
    target 1494
  ]
  edge [
    source 32
    target 1456
  ]
  edge [
    source 32
    target 75
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 140
  ]
  edge [
    source 32
    target 1495
  ]
  edge [
    source 32
    target 1496
  ]
  edge [
    source 32
    target 1497
  ]
  edge [
    source 32
    target 141
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1498
  ]
  edge [
    source 33
    target 1499
  ]
  edge [
    source 33
    target 1500
  ]
  edge [
    source 33
    target 1501
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1502
  ]
  edge [
    source 34
    target 1503
  ]
  edge [
    source 34
    target 1504
  ]
  edge [
    source 34
    target 1505
  ]
  edge [
    source 34
    target 1506
  ]
  edge [
    source 34
    target 1507
  ]
  edge [
    source 34
    target 84
  ]
  edge [
    source 34
    target 1508
  ]
  edge [
    source 34
    target 1509
  ]
  edge [
    source 34
    target 1510
  ]
  edge [
    source 34
    target 1511
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 453
  ]
  edge [
    source 34
    target 454
  ]
  edge [
    source 34
    target 455
  ]
  edge [
    source 34
    target 456
  ]
  edge [
    source 34
    target 457
  ]
  edge [
    source 34
    target 458
  ]
  edge [
    source 34
    target 459
  ]
  edge [
    source 34
    target 460
  ]
  edge [
    source 34
    target 461
  ]
  edge [
    source 34
    target 462
  ]
  edge [
    source 34
    target 463
  ]
  edge [
    source 34
    target 464
  ]
  edge [
    source 34
    target 465
  ]
  edge [
    source 34
    target 466
  ]
  edge [
    source 34
    target 467
  ]
  edge [
    source 34
    target 468
  ]
  edge [
    source 34
    target 469
  ]
  edge [
    source 34
    target 470
  ]
  edge [
    source 34
    target 471
  ]
  edge [
    source 34
    target 472
  ]
  edge [
    source 34
    target 473
  ]
  edge [
    source 34
    target 474
  ]
  edge [
    source 34
    target 475
  ]
  edge [
    source 34
    target 476
  ]
  edge [
    source 34
    target 477
  ]
  edge [
    source 34
    target 478
  ]
  edge [
    source 34
    target 479
  ]
  edge [
    source 34
    target 480
  ]
  edge [
    source 34
    target 481
  ]
  edge [
    source 34
    target 482
  ]
  edge [
    source 34
    target 483
  ]
  edge [
    source 34
    target 484
  ]
  edge [
    source 34
    target 215
  ]
  edge [
    source 34
    target 485
  ]
  edge [
    source 34
    target 486
  ]
  edge [
    source 34
    target 487
  ]
  edge [
    source 34
    target 488
  ]
  edge [
    source 34
    target 1512
  ]
  edge [
    source 34
    target 1513
  ]
  edge [
    source 34
    target 1514
  ]
  edge [
    source 34
    target 717
  ]
  edge [
    source 34
    target 1515
  ]
  edge [
    source 34
    target 1516
  ]
  edge [
    source 34
    target 1517
  ]
  edge [
    source 34
    target 1518
  ]
  edge [
    source 34
    target 1519
  ]
  edge [
    source 34
    target 1520
  ]
  edge [
    source 34
    target 1521
  ]
  edge [
    source 34
    target 300
  ]
  edge [
    source 34
    target 1522
  ]
  edge [
    source 34
    target 1523
  ]
  edge [
    source 34
    target 662
  ]
  edge [
    source 34
    target 1524
  ]
  edge [
    source 34
    target 1525
  ]
  edge [
    source 34
    target 1526
  ]
  edge [
    source 34
    target 1527
  ]
  edge [
    source 34
    target 1528
  ]
  edge [
    source 34
    target 1529
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 193
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 1538
  ]
  edge [
    source 35
    target 1539
  ]
  edge [
    source 35
    target 1540
  ]
  edge [
    source 35
    target 1175
  ]
  edge [
    source 35
    target 1176
  ]
  edge [
    source 35
    target 1177
  ]
  edge [
    source 35
    target 1178
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 35
    target 1179
  ]
  edge [
    source 35
    target 331
  ]
  edge [
    source 35
    target 1180
  ]
  edge [
    source 35
    target 1181
  ]
  edge [
    source 35
    target 1182
  ]
  edge [
    source 35
    target 1183
  ]
  edge [
    source 35
    target 1184
  ]
  edge [
    source 35
    target 1185
  ]
  edge [
    source 35
    target 1005
  ]
  edge [
    source 35
    target 1186
  ]
  edge [
    source 35
    target 1187
  ]
  edge [
    source 35
    target 1188
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 1189
  ]
  edge [
    source 35
    target 399
  ]
  edge [
    source 35
    target 1190
  ]
  edge [
    source 35
    target 1191
  ]
  edge [
    source 35
    target 1192
  ]
  edge [
    source 35
    target 1193
  ]
  edge [
    source 35
    target 1194
  ]
  edge [
    source 35
    target 1195
  ]
  edge [
    source 35
    target 1196
  ]
  edge [
    source 35
    target 1197
  ]
  edge [
    source 35
    target 1198
  ]
  edge [
    source 35
    target 1199
  ]
  edge [
    source 35
    target 1200
  ]
  edge [
    source 35
    target 158
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 35
    target 1201
  ]
  edge [
    source 35
    target 1202
  ]
  edge [
    source 35
    target 1203
  ]
  edge [
    source 35
    target 1204
  ]
  edge [
    source 35
    target 1205
  ]
  edge [
    source 35
    target 1206
  ]
  edge [
    source 35
    target 1207
  ]
  edge [
    source 35
    target 1208
  ]
  edge [
    source 35
    target 837
  ]
  edge [
    source 35
    target 1532
  ]
  edge [
    source 35
    target 1541
  ]
  edge [
    source 35
    target 1542
  ]
  edge [
    source 35
    target 1543
  ]
  edge [
    source 35
    target 257
  ]
  edge [
    source 35
    target 1544
  ]
  edge [
    source 35
    target 1545
  ]
  edge [
    source 35
    target 1546
  ]
  edge [
    source 35
    target 1547
  ]
  edge [
    source 35
    target 1548
  ]
  edge [
    source 35
    target 1549
  ]
  edge [
    source 35
    target 1550
  ]
  edge [
    source 35
    target 1551
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 1552
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1553
  ]
  edge [
    source 38
    target 1554
  ]
  edge [
    source 38
    target 386
  ]
  edge [
    source 38
    target 1555
  ]
  edge [
    source 38
    target 1556
  ]
  edge [
    source 38
    target 1557
  ]
  edge [
    source 38
    target 1558
  ]
  edge [
    source 38
    target 253
  ]
  edge [
    source 38
    target 1559
  ]
  edge [
    source 38
    target 256
  ]
  edge [
    source 38
    target 1560
  ]
  edge [
    source 38
    target 1193
  ]
  edge [
    source 38
    target 1561
  ]
  edge [
    source 38
    target 1562
  ]
  edge [
    source 38
    target 1563
  ]
  edge [
    source 38
    target 1564
  ]
  edge [
    source 38
    target 1565
  ]
  edge [
    source 38
    target 1566
  ]
  edge [
    source 38
    target 1567
  ]
  edge [
    source 38
    target 1568
  ]
  edge [
    source 38
    target 1569
  ]
  edge [
    source 38
    target 1570
  ]
  edge [
    source 38
    target 1571
  ]
  edge [
    source 38
    target 1572
  ]
  edge [
    source 38
    target 1573
  ]
  edge [
    source 38
    target 1574
  ]
  edge [
    source 38
    target 1575
  ]
  edge [
    source 38
    target 1576
  ]
  edge [
    source 38
    target 1577
  ]
  edge [
    source 38
    target 1578
  ]
  edge [
    source 38
    target 1579
  ]
  edge [
    source 38
    target 1580
  ]
  edge [
    source 38
    target 1581
  ]
  edge [
    source 38
    target 1582
  ]
  edge [
    source 38
    target 1583
  ]
  edge [
    source 38
    target 1584
  ]
  edge [
    source 38
    target 1212
  ]
  edge [
    source 38
    target 1585
  ]
  edge [
    source 38
    target 1586
  ]
  edge [
    source 38
    target 1587
  ]
  edge [
    source 38
    target 1588
  ]
  edge [
    source 38
    target 1589
  ]
  edge [
    source 38
    target 1590
  ]
  edge [
    source 38
    target 1591
  ]
  edge [
    source 38
    target 1592
  ]
  edge [
    source 38
    target 1593
  ]
  edge [
    source 38
    target 1594
  ]
  edge [
    source 38
    target 1595
  ]
  edge [
    source 38
    target 1596
  ]
  edge [
    source 38
    target 1597
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1176
  ]
  edge [
    source 40
    target 250
  ]
  edge [
    source 40
    target 272
  ]
  edge [
    source 40
    target 259
  ]
  edge [
    source 40
    target 314
  ]
  edge [
    source 40
    target 418
  ]
  edge [
    source 40
    target 1204
  ]
  edge [
    source 40
    target 262
  ]
  edge [
    source 40
    target 275
  ]
  edge [
    source 40
    target 1209
  ]
  edge [
    source 40
    target 1210
  ]
  edge [
    source 40
    target 1181
  ]
  edge [
    source 40
    target 1211
  ]
  edge [
    source 40
    target 1212
  ]
  edge [
    source 40
    target 1208
  ]
  edge [
    source 40
    target 1213
  ]
  edge [
    source 40
    target 273
  ]
  edge [
    source 40
    target 251
  ]
  edge [
    source 40
    target 252
  ]
  edge [
    source 40
    target 253
  ]
  edge [
    source 40
    target 254
  ]
  edge [
    source 40
    target 255
  ]
  edge [
    source 40
    target 256
  ]
  edge [
    source 40
    target 257
  ]
  edge [
    source 40
    target 258
  ]
  edge [
    source 40
    target 260
  ]
  edge [
    source 40
    target 261
  ]
  edge [
    source 40
    target 263
  ]
  edge [
    source 40
    target 264
  ]
  edge [
    source 40
    target 265
  ]
  edge [
    source 40
    target 266
  ]
  edge [
    source 40
    target 267
  ]
  edge [
    source 40
    target 268
  ]
  edge [
    source 40
    target 269
  ]
  edge [
    source 40
    target 270
  ]
  edge [
    source 40
    target 271
  ]
  edge [
    source 40
    target 274
  ]
  edge [
    source 40
    target 276
  ]
  edge [
    source 40
    target 277
  ]
  edge [
    source 40
    target 1598
  ]
  edge [
    source 40
    target 1599
  ]
  edge [
    source 40
    target 1600
  ]
  edge [
    source 40
    target 1601
  ]
  edge [
    source 40
    target 167
  ]
  edge [
    source 40
    target 1602
  ]
  edge [
    source 40
    target 1603
  ]
  edge [
    source 40
    target 1604
  ]
  edge [
    source 40
    target 1605
  ]
  edge [
    source 40
    target 1606
  ]
  edge [
    source 40
    target 1607
  ]
  edge [
    source 40
    target 1608
  ]
  edge [
    source 40
    target 1609
  ]
  edge [
    source 40
    target 1610
  ]
  edge [
    source 40
    target 1611
  ]
  edge [
    source 40
    target 1612
  ]
  edge [
    source 40
    target 1613
  ]
  edge [
    source 40
    target 1614
  ]
  edge [
    source 40
    target 1615
  ]
  edge [
    source 40
    target 1616
  ]
  edge [
    source 40
    target 1617
  ]
  edge [
    source 40
    target 1618
  ]
  edge [
    source 40
    target 1619
  ]
  edge [
    source 40
    target 165
  ]
  edge [
    source 40
    target 1620
  ]
  edge [
    source 40
    target 1621
  ]
  edge [
    source 40
    target 1622
  ]
  edge [
    source 40
    target 1623
  ]
  edge [
    source 40
    target 293
  ]
  edge [
    source 40
    target 294
  ]
  edge [
    source 40
    target 295
  ]
  edge [
    source 40
    target 296
  ]
  edge [
    source 40
    target 297
  ]
  edge [
    source 40
    target 298
  ]
  edge [
    source 40
    target 299
  ]
  edge [
    source 40
    target 300
  ]
  edge [
    source 40
    target 301
  ]
  edge [
    source 40
    target 302
  ]
  edge [
    source 40
    target 303
  ]
  edge [
    source 40
    target 304
  ]
  edge [
    source 40
    target 1624
  ]
  edge [
    source 40
    target 1625
  ]
  edge [
    source 40
    target 1626
  ]
  edge [
    source 40
    target 1627
  ]
  edge [
    source 40
    target 200
  ]
  edge [
    source 40
    target 1628
  ]
  edge [
    source 40
    target 1629
  ]
  edge [
    source 40
    target 863
  ]
  edge [
    source 40
    target 1630
  ]
  edge [
    source 40
    target 158
  ]
  edge [
    source 40
    target 1631
  ]
  edge [
    source 40
    target 1632
  ]
  edge [
    source 40
    target 1633
  ]
  edge [
    source 40
    target 1634
  ]
  edge [
    source 40
    target 1037
  ]
  edge [
    source 40
    target 1635
  ]
  edge [
    source 40
    target 1636
  ]
  edge [
    source 40
    target 372
  ]
  edge [
    source 40
    target 373
  ]
  edge [
    source 40
    target 374
  ]
  edge [
    source 40
    target 375
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 40
    target 377
  ]
  edge [
    source 40
    target 366
  ]
  edge [
    source 40
    target 367
  ]
  edge [
    source 40
    target 368
  ]
  edge [
    source 40
    target 369
  ]
  edge [
    source 40
    target 370
  ]
  edge [
    source 40
    target 371
  ]
  edge [
    source 40
    target 360
  ]
  edge [
    source 40
    target 361
  ]
  edge [
    source 40
    target 362
  ]
  edge [
    source 40
    target 363
  ]
  edge [
    source 40
    target 364
  ]
  edge [
    source 40
    target 365
  ]
  edge [
    source 40
    target 191
  ]
  edge [
    source 40
    target 378
  ]
  edge [
    source 40
    target 379
  ]
  edge [
    source 40
    target 160
  ]
  edge [
    source 40
    target 1637
  ]
  edge [
    source 40
    target 1638
  ]
  edge [
    source 40
    target 1639
  ]
  edge [
    source 40
    target 1640
  ]
  edge [
    source 40
    target 1641
  ]
  edge [
    source 40
    target 1642
  ]
  edge [
    source 40
    target 1643
  ]
  edge [
    source 40
    target 1644
  ]
  edge [
    source 40
    target 1645
  ]
  edge [
    source 40
    target 1646
  ]
  edge [
    source 40
    target 523
  ]
  edge [
    source 40
    target 1647
  ]
  edge [
    source 40
    target 1648
  ]
  edge [
    source 40
    target 1649
  ]
  edge [
    source 40
    target 1650
  ]
  edge [
    source 40
    target 1651
  ]
  edge [
    source 40
    target 1652
  ]
  edge [
    source 40
    target 1653
  ]
  edge [
    source 40
    target 1654
  ]
  edge [
    source 40
    target 1655
  ]
  edge [
    source 40
    target 1656
  ]
  edge [
    source 40
    target 1657
  ]
  edge [
    source 40
    target 1658
  ]
  edge [
    source 40
    target 1659
  ]
  edge [
    source 40
    target 1660
  ]
  edge [
    source 40
    target 1661
  ]
  edge [
    source 40
    target 1662
  ]
  edge [
    source 40
    target 1663
  ]
  edge [
    source 40
    target 1664
  ]
  edge [
    source 40
    target 1665
  ]
  edge [
    source 40
    target 1666
  ]
  edge [
    source 40
    target 1667
  ]
  edge [
    source 40
    target 1668
  ]
  edge [
    source 40
    target 1669
  ]
  edge [
    source 40
    target 1670
  ]
  edge [
    source 40
    target 1671
  ]
  edge [
    source 40
    target 1672
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1673
  ]
  edge [
    source 41
    target 1674
  ]
  edge [
    source 41
    target 1675
  ]
  edge [
    source 41
    target 1676
  ]
  edge [
    source 41
    target 1677
  ]
  edge [
    source 41
    target 1678
  ]
  edge [
    source 41
    target 933
  ]
  edge [
    source 41
    target 930
  ]
  edge [
    source 41
    target 1679
  ]
  edge [
    source 41
    target 1680
  ]
  edge [
    source 41
    target 1681
  ]
  edge [
    source 41
    target 1682
  ]
  edge [
    source 41
    target 1683
  ]
  edge [
    source 41
    target 1684
  ]
  edge [
    source 41
    target 1685
  ]
  edge [
    source 41
    target 1686
  ]
  edge [
    source 41
    target 1687
  ]
  edge [
    source 41
    target 1688
  ]
  edge [
    source 41
    target 1689
  ]
  edge [
    source 41
    target 1690
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 458
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 934
  ]
  edge [
    source 41
    target 935
  ]
  edge [
    source 41
    target 936
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 1382
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1016
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 41
    target 1470
  ]
  edge [
    source 41
    target 1730
  ]
  edge [
    source 41
    target 1731
  ]
  edge [
    source 41
    target 1732
  ]
  edge [
    source 41
    target 1733
  ]
  edge [
    source 41
    target 1734
  ]
  edge [
    source 41
    target 1735
  ]
  edge [
    source 41
    target 1736
  ]
  edge [
    source 41
    target 1737
  ]
  edge [
    source 41
    target 1738
  ]
  edge [
    source 41
    target 1739
  ]
  edge [
    source 41
    target 1740
  ]
  edge [
    source 41
    target 529
  ]
  edge [
    source 41
    target 1741
  ]
  edge [
    source 41
    target 1742
  ]
  edge [
    source 41
    target 1743
  ]
  edge [
    source 41
    target 1744
  ]
  edge [
    source 41
    target 1745
  ]
  edge [
    source 41
    target 1746
  ]
  edge [
    source 41
    target 1747
  ]
  edge [
    source 41
    target 1748
  ]
  edge [
    source 41
    target 1749
  ]
  edge [
    source 41
    target 1750
  ]
  edge [
    source 41
    target 316
  ]
  edge [
    source 41
    target 1751
  ]
  edge [
    source 41
    target 1752
  ]
  edge [
    source 41
    target 1753
  ]
  edge [
    source 41
    target 1754
  ]
  edge [
    source 41
    target 1755
  ]
  edge [
    source 41
    target 287
  ]
  edge [
    source 41
    target 1756
  ]
  edge [
    source 41
    target 1757
  ]
  edge [
    source 41
    target 1758
  ]
  edge [
    source 41
    target 1759
  ]
  edge [
    source 41
    target 416
  ]
  edge [
    source 41
    target 1760
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1761
  ]
  edge [
    source 42
    target 1762
  ]
  edge [
    source 42
    target 1763
  ]
  edge [
    source 42
    target 1764
  ]
  edge [
    source 42
    target 1765
  ]
  edge [
    source 42
    target 1766
  ]
  edge [
    source 42
    target 1767
  ]
  edge [
    source 42
    target 1768
  ]
  edge [
    source 42
    target 1769
  ]
  edge [
    source 42
    target 1770
  ]
  edge [
    source 42
    target 1771
  ]
  edge [
    source 42
    target 1772
  ]
  edge [
    source 42
    target 1773
  ]
  edge [
    source 42
    target 1774
  ]
  edge [
    source 42
    target 1509
  ]
  edge [
    source 42
    target 1775
  ]
  edge [
    source 43
    target 44
  ]
]
