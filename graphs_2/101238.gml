graph [
  node [
    id 0
    label "batalion"
    origin "text"
  ]
  node [
    id 1
    label "brze&#380;any"
    origin "text"
  ]
  node [
    id 2
    label "formacja"
  ]
  node [
    id 3
    label "s&#322;onki"
  ]
  node [
    id 4
    label "ptak_w&#281;drowny"
  ]
  node [
    id 5
    label "pododdzia&#322;"
  ]
  node [
    id 6
    label "brygada"
  ]
  node [
    id 7
    label "pu&#322;k"
  ]
  node [
    id 8
    label "kompania"
  ]
  node [
    id 9
    label "dywizjon"
  ]
  node [
    id 10
    label "ptak_wodno-b&#322;otny"
  ]
  node [
    id 11
    label "Bund"
  ]
  node [
    id 12
    label "Mazowsze"
  ]
  node [
    id 13
    label "PPR"
  ]
  node [
    id 14
    label "Jakobici"
  ]
  node [
    id 15
    label "zesp&#243;&#322;"
  ]
  node [
    id 16
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 17
    label "leksem"
  ]
  node [
    id 18
    label "SLD"
  ]
  node [
    id 19
    label "zespolik"
  ]
  node [
    id 20
    label "Razem"
  ]
  node [
    id 21
    label "PiS"
  ]
  node [
    id 22
    label "zjawisko"
  ]
  node [
    id 23
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 24
    label "partia"
  ]
  node [
    id 25
    label "Kuomintang"
  ]
  node [
    id 26
    label "ZSL"
  ]
  node [
    id 27
    label "szko&#322;a"
  ]
  node [
    id 28
    label "jednostka"
  ]
  node [
    id 29
    label "proces"
  ]
  node [
    id 30
    label "organizacja"
  ]
  node [
    id 31
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 32
    label "rugby"
  ]
  node [
    id 33
    label "AWS"
  ]
  node [
    id 34
    label "posta&#263;"
  ]
  node [
    id 35
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 36
    label "blok"
  ]
  node [
    id 37
    label "PO"
  ]
  node [
    id 38
    label "si&#322;a"
  ]
  node [
    id 39
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 40
    label "Federali&#347;ci"
  ]
  node [
    id 41
    label "PSL"
  ]
  node [
    id 42
    label "czynno&#347;&#263;"
  ]
  node [
    id 43
    label "wojsko"
  ]
  node [
    id 44
    label "Wigowie"
  ]
  node [
    id 45
    label "ZChN"
  ]
  node [
    id 46
    label "egzekutywa"
  ]
  node [
    id 47
    label "rocznik"
  ]
  node [
    id 48
    label "The_Beatles"
  ]
  node [
    id 49
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 50
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 51
    label "unit"
  ]
  node [
    id 52
    label "Depeche_Mode"
  ]
  node [
    id 53
    label "forma"
  ]
  node [
    id 54
    label "oddzia&#322;"
  ]
  node [
    id 55
    label "bekasowate"
  ]
  node [
    id 56
    label "szwadron"
  ]
  node [
    id 57
    label "firma"
  ]
  node [
    id 58
    label "pluton"
  ]
  node [
    id 59
    label "institution"
  ]
  node [
    id 60
    label "grono"
  ]
  node [
    id 61
    label "armia"
  ]
  node [
    id 62
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 63
    label "crew"
  ]
  node [
    id 64
    label "dywizja"
  ]
  node [
    id 65
    label "D&#261;browszczacy"
  ]
  node [
    id 66
    label "za&#322;oga"
  ]
  node [
    id 67
    label "dywizjon_lotniczy"
  ]
  node [
    id 68
    label "dywizjon_artylerii"
  ]
  node [
    id 69
    label "regiment"
  ]
  node [
    id 70
    label "on"
  ]
  node [
    id 71
    label "Brze&#380;any"
  ]
  node [
    id 72
    label "Brze&#380;a&#324;ski"
  ]
  node [
    id 73
    label "obrona"
  ]
  node [
    id 74
    label "narodowy"
  ]
  node [
    id 75
    label "polskie"
  ]
  node [
    id 76
    label "ii"
  ]
  node [
    id 77
    label "RP"
  ]
  node [
    id 78
    label "lwowski"
  ]
  node [
    id 79
    label "Brze&#380;a&#324;skiego"
  ]
  node [
    id 80
    label "51"
  ]
  node [
    id 81
    label "piechota"
  ]
  node [
    id 82
    label "strzelec"
  ]
  node [
    id 83
    label "kresowy"
  ]
  node [
    id 84
    label "tarnopolski"
  ]
  node [
    id 85
    label "p&#243;&#322;brygada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 43
    target 75
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 78
  ]
  edge [
    source 70
    target 79
  ]
  edge [
    source 70
    target 84
  ]
  edge [
    source 70
    target 85
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 82
  ]
  edge [
    source 80
    target 83
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 84
    target 85
  ]
]
