graph [
  node [
    id 0
    label "stadion"
    origin "text"
  ]
  node [
    id 1
    label "punjab"
    origin "text"
  ]
  node [
    id 2
    label "trybuna"
  ]
  node [
    id 3
    label "court"
  ]
  node [
    id 4
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 5
    label "obiekt"
  ]
  node [
    id 6
    label "budowla"
  ]
  node [
    id 7
    label "zgromadzenie"
  ]
  node [
    id 8
    label "korona"
  ]
  node [
    id 9
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 10
    label "spotkanie"
  ]
  node [
    id 11
    label "spowodowanie"
  ]
  node [
    id 12
    label "pozyskanie"
  ]
  node [
    id 13
    label "kongregacja"
  ]
  node [
    id 14
    label "templum"
  ]
  node [
    id 15
    label "gromadzenie"
  ]
  node [
    id 16
    label "gathering"
  ]
  node [
    id 17
    label "konwentykiel"
  ]
  node [
    id 18
    label "klasztor"
  ]
  node [
    id 19
    label "caucus"
  ]
  node [
    id 20
    label "skupienie"
  ]
  node [
    id 21
    label "wsp&#243;lnota"
  ]
  node [
    id 22
    label "grupa"
  ]
  node [
    id 23
    label "organ"
  ]
  node [
    id 24
    label "concourse"
  ]
  node [
    id 25
    label "czynno&#347;&#263;"
  ]
  node [
    id 26
    label "poj&#281;cie"
  ]
  node [
    id 27
    label "rzecz"
  ]
  node [
    id 28
    label "thing"
  ]
  node [
    id 29
    label "co&#347;"
  ]
  node [
    id 30
    label "budynek"
  ]
  node [
    id 31
    label "program"
  ]
  node [
    id 32
    label "strona"
  ]
  node [
    id 33
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 34
    label "stan_surowy"
  ]
  node [
    id 35
    label "postanie"
  ]
  node [
    id 36
    label "zbudowa&#263;"
  ]
  node [
    id 37
    label "obudowywanie"
  ]
  node [
    id 38
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 39
    label "obudowywa&#263;"
  ]
  node [
    id 40
    label "konstrukcja"
  ]
  node [
    id 41
    label "Sukiennice"
  ]
  node [
    id 42
    label "kolumnada"
  ]
  node [
    id 43
    label "korpus"
  ]
  node [
    id 44
    label "zbudowanie"
  ]
  node [
    id 45
    label "fundament"
  ]
  node [
    id 46
    label "obudowa&#263;"
  ]
  node [
    id 47
    label "obudowanie"
  ]
  node [
    id 48
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 49
    label "kok"
  ]
  node [
    id 50
    label "przepaska"
  ]
  node [
    id 51
    label "maksimum"
  ]
  node [
    id 52
    label "zwie&#324;czenie"
  ]
  node [
    id 53
    label "g&#243;ra"
  ]
  node [
    id 54
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 55
    label "wieniec"
  ]
  node [
    id 56
    label "uk&#322;ad"
  ]
  node [
    id 57
    label "Crown"
  ]
  node [
    id 58
    label "element"
  ]
  node [
    id 59
    label "warkocz"
  ]
  node [
    id 60
    label "bryd&#380;"
  ]
  node [
    id 61
    label "jednostka_monetarna"
  ]
  node [
    id 62
    label "drzewo"
  ]
  node [
    id 63
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 64
    label "moneta"
  ]
  node [
    id 65
    label "corona"
  ]
  node [
    id 66
    label "geofit"
  ]
  node [
    id 67
    label "p&#322;atek"
  ]
  node [
    id 68
    label "pa&#324;stwo"
  ]
  node [
    id 69
    label "kres"
  ]
  node [
    id 70
    label "proteza_dentystyczna"
  ]
  node [
    id 71
    label "genitalia"
  ]
  node [
    id 72
    label "kwiat"
  ]
  node [
    id 73
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 74
    label "czub"
  ]
  node [
    id 75
    label "r&#243;g"
  ]
  node [
    id 76
    label "urz&#261;d"
  ]
  node [
    id 77
    label "znak_muzyczny"
  ]
  node [
    id 78
    label "liliowate"
  ]
  node [
    id 79
    label "regalia"
  ]
  node [
    id 80
    label "diadem"
  ]
  node [
    id 81
    label "z&#261;b"
  ]
  node [
    id 82
    label "motyl"
  ]
  node [
    id 83
    label "zesp&#243;&#322;"
  ]
  node [
    id 84
    label "podwy&#380;szenie"
  ]
  node [
    id 85
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 86
    label "cyrk"
  ]
  node [
    id 87
    label "widownia"
  ]
  node [
    id 88
    label "miejsce_stoj&#261;ce"
  ]
  node [
    id 89
    label "Punjab"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
]
