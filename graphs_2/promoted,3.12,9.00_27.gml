graph [
  node [
    id 0
    label "dawno"
    origin "text"
  ]
  node [
    id 1
    label "temu"
    origin "text"
  ]
  node [
    id 2
    label "kiedy"
    origin "text"
  ]
  node [
    id 3
    label "niewybredny"
    origin "text"
  ]
  node [
    id 4
    label "&#380;art"
    origin "text"
  ]
  node [
    id 5
    label "niepoprawny"
    origin "text"
  ]
  node [
    id 6
    label "politycznie"
    origin "text"
  ]
  node [
    id 7
    label "podtekst"
    origin "text"
  ]
  node [
    id 8
    label "doprowadza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "opinia"
    origin "text"
  ]
  node [
    id 10
    label "publiczny"
    origin "text"
  ]
  node [
    id 11
    label "szewski"
    origin "text"
  ]
  node [
    id 12
    label "pasja"
    origin "text"
  ]
  node [
    id 13
    label "lato"
    origin "text"
  ]
  node [
    id 14
    label "osiemdziesi&#261;ty"
    origin "text"
  ]
  node [
    id 15
    label "triumf"
    origin "text"
  ]
  node [
    id 16
    label "trio"
    origin "text"
  ]
  node [
    id 17
    label "znana"
    origin "text"
  ]
  node [
    id 18
    label "bran&#380;a"
    origin "text"
  ]
  node [
    id 19
    label "jako"
    origin "text"
  ]
  node [
    id 20
    label "zaz"
    origin "text"
  ]
  node [
    id 21
    label "dawny"
  ]
  node [
    id 22
    label "d&#322;ugotrwale"
  ]
  node [
    id 23
    label "wcze&#347;niej"
  ]
  node [
    id 24
    label "ongi&#347;"
  ]
  node [
    id 25
    label "dawnie"
  ]
  node [
    id 26
    label "drzewiej"
  ]
  node [
    id 27
    label "niegdysiejszy"
  ]
  node [
    id 28
    label "kiedy&#347;"
  ]
  node [
    id 29
    label "d&#322;ugotrwa&#322;y"
  ]
  node [
    id 30
    label "d&#322;ugo"
  ]
  node [
    id 31
    label "wcze&#347;niejszy"
  ]
  node [
    id 32
    label "przestarza&#322;y"
  ]
  node [
    id 33
    label "odleg&#322;y"
  ]
  node [
    id 34
    label "przesz&#322;y"
  ]
  node [
    id 35
    label "od_dawna"
  ]
  node [
    id 36
    label "poprzedni"
  ]
  node [
    id 37
    label "d&#322;ugoletni"
  ]
  node [
    id 38
    label "anachroniczny"
  ]
  node [
    id 39
    label "dawniej"
  ]
  node [
    id 40
    label "kombatant"
  ]
  node [
    id 41
    label "stary"
  ]
  node [
    id 42
    label "niewymy&#347;lny"
  ]
  node [
    id 43
    label "niewybrednie"
  ]
  node [
    id 44
    label "niewymagaj&#261;cy"
  ]
  node [
    id 45
    label "pospolity"
  ]
  node [
    id 46
    label "pospolicie"
  ]
  node [
    id 47
    label "zwyczajny"
  ]
  node [
    id 48
    label "wsp&#243;lny"
  ]
  node [
    id 49
    label "jak_ps&#243;w"
  ]
  node [
    id 50
    label "niewyszukany"
  ]
  node [
    id 51
    label "prosty"
  ]
  node [
    id 52
    label "niewymy&#347;lnie"
  ]
  node [
    id 53
    label "czyn"
  ]
  node [
    id 54
    label "szczeg&#243;&#322;"
  ]
  node [
    id 55
    label "humor"
  ]
  node [
    id 56
    label "cyrk"
  ]
  node [
    id 57
    label "dokazywanie"
  ]
  node [
    id 58
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 59
    label "szpas"
  ]
  node [
    id 60
    label "spalenie"
  ]
  node [
    id 61
    label "opowiadanie"
  ]
  node [
    id 62
    label "furda"
  ]
  node [
    id 63
    label "banalny"
  ]
  node [
    id 64
    label "koncept"
  ]
  node [
    id 65
    label "gryps"
  ]
  node [
    id 66
    label "anecdote"
  ]
  node [
    id 67
    label "turn"
  ]
  node [
    id 68
    label "sofcik"
  ]
  node [
    id 69
    label "pomys&#322;"
  ]
  node [
    id 70
    label "raptularz"
  ]
  node [
    id 71
    label "g&#243;wno"
  ]
  node [
    id 72
    label "palenie"
  ]
  node [
    id 73
    label "finfa"
  ]
  node [
    id 74
    label "wolty&#380;erka"
  ]
  node [
    id 75
    label "repryza"
  ]
  node [
    id 76
    label "ekwilibrystyka"
  ]
  node [
    id 77
    label "nied&#378;wiednik"
  ]
  node [
    id 78
    label "tresura"
  ]
  node [
    id 79
    label "skandal"
  ]
  node [
    id 80
    label "hipodrom"
  ]
  node [
    id 81
    label "instytucja"
  ]
  node [
    id 82
    label "przedstawienie"
  ]
  node [
    id 83
    label "namiot"
  ]
  node [
    id 84
    label "budynek"
  ]
  node [
    id 85
    label "circus"
  ]
  node [
    id 86
    label "heca"
  ]
  node [
    id 87
    label "arena"
  ]
  node [
    id 88
    label "akrobacja"
  ]
  node [
    id 89
    label "klownada"
  ]
  node [
    id 90
    label "grupa"
  ]
  node [
    id 91
    label "amfiteatr"
  ]
  node [
    id 92
    label "trybuna"
  ]
  node [
    id 93
    label "follow-up"
  ]
  node [
    id 94
    label "rozpowiadanie"
  ]
  node [
    id 95
    label "wypowied&#378;"
  ]
  node [
    id 96
    label "report"
  ]
  node [
    id 97
    label "podbarwianie"
  ]
  node [
    id 98
    label "przedstawianie"
  ]
  node [
    id 99
    label "story"
  ]
  node [
    id 100
    label "rozpowiedzenie"
  ]
  node [
    id 101
    label "proza"
  ]
  node [
    id 102
    label "prawienie"
  ]
  node [
    id 103
    label "utw&#243;r_epicki"
  ]
  node [
    id 104
    label "fabu&#322;a"
  ]
  node [
    id 105
    label "idea"
  ]
  node [
    id 106
    label "wytw&#243;r"
  ]
  node [
    id 107
    label "pocz&#261;tki"
  ]
  node [
    id 108
    label "ukradzenie"
  ]
  node [
    id 109
    label "ukra&#347;&#263;"
  ]
  node [
    id 110
    label "system"
  ]
  node [
    id 111
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "niuansowa&#263;"
  ]
  node [
    id 113
    label "element"
  ]
  node [
    id 114
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 115
    label "sk&#322;adnik"
  ]
  node [
    id 116
    label "zniuansowa&#263;"
  ]
  node [
    id 117
    label "funkcja"
  ]
  node [
    id 118
    label "act"
  ]
  node [
    id 119
    label "pami&#281;tnik"
  ]
  node [
    id 120
    label "ksi&#281;ga"
  ]
  node [
    id 121
    label "dowcip"
  ]
  node [
    id 122
    label "zapis"
  ]
  node [
    id 123
    label "bystro&#347;&#263;"
  ]
  node [
    id 124
    label "design"
  ]
  node [
    id 125
    label "grypsowa&#263;"
  ]
  node [
    id 126
    label "grypsowanie"
  ]
  node [
    id 127
    label "list"
  ]
  node [
    id 128
    label "ka&#322;"
  ]
  node [
    id 129
    label "tandeta"
  ]
  node [
    id 130
    label "zero"
  ]
  node [
    id 131
    label "drobiazg"
  ]
  node [
    id 132
    label "dym"
  ]
  node [
    id 133
    label "zepsucie"
  ]
  node [
    id 134
    label "zu&#380;ycie"
  ]
  node [
    id 135
    label "spalanie"
  ]
  node [
    id 136
    label "utlenienie"
  ]
  node [
    id 137
    label "zniszczenie"
  ]
  node [
    id 138
    label "podpalenie"
  ]
  node [
    id 139
    label "spieczenie_si&#281;"
  ]
  node [
    id 140
    label "przygrzanie"
  ]
  node [
    id 141
    label "burning"
  ]
  node [
    id 142
    label "napalenie"
  ]
  node [
    id 143
    label "paliwo"
  ]
  node [
    id 144
    label "combustion"
  ]
  node [
    id 145
    label "sp&#322;oni&#281;cie"
  ]
  node [
    id 146
    label "zmetabolizowanie"
  ]
  node [
    id 147
    label "deflagration"
  ]
  node [
    id 148
    label "zagranie"
  ]
  node [
    id 149
    label "chemikalia"
  ]
  node [
    id 150
    label "zabicie"
  ]
  node [
    id 151
    label "dopalenie"
  ]
  node [
    id 152
    label "powodowanie"
  ]
  node [
    id 153
    label "na&#322;&#243;g"
  ]
  node [
    id 154
    label "emergency"
  ]
  node [
    id 155
    label "burn"
  ]
  node [
    id 156
    label "dokuczanie"
  ]
  node [
    id 157
    label "cygaro"
  ]
  node [
    id 158
    label "robienie"
  ]
  node [
    id 159
    label "podpalanie"
  ]
  node [
    id 160
    label "rozpalanie"
  ]
  node [
    id 161
    label "pykni&#281;cie"
  ]
  node [
    id 162
    label "zu&#380;ywanie"
  ]
  node [
    id 163
    label "wypalanie"
  ]
  node [
    id 164
    label "fajka"
  ]
  node [
    id 165
    label "podtrzymywanie"
  ]
  node [
    id 166
    label "strzelanie"
  ]
  node [
    id 167
    label "papieros"
  ]
  node [
    id 168
    label "dra&#380;nienie"
  ]
  node [
    id 169
    label "kadzenie"
  ]
  node [
    id 170
    label "wypalenie"
  ]
  node [
    id 171
    label "czynno&#347;&#263;"
  ]
  node [
    id 172
    label "przygotowywanie"
  ]
  node [
    id 173
    label "ogie&#324;"
  ]
  node [
    id 174
    label "popalenie"
  ]
  node [
    id 175
    label "niszczenie"
  ]
  node [
    id 176
    label "grzanie"
  ]
  node [
    id 177
    label "bolenie"
  ]
  node [
    id 178
    label "palenie_si&#281;"
  ]
  node [
    id 179
    label "incineration"
  ]
  node [
    id 180
    label "psucie"
  ]
  node [
    id 181
    label "jaranie"
  ]
  node [
    id 182
    label "pornografia_mi&#281;kka"
  ]
  node [
    id 183
    label "poziom"
  ]
  node [
    id 184
    label "pornografia"
  ]
  node [
    id 185
    label "ocena"
  ]
  node [
    id 186
    label "play"
  ]
  node [
    id 187
    label "swawola"
  ]
  node [
    id 188
    label "zabawa"
  ]
  node [
    id 189
    label "bawienie_si&#281;"
  ]
  node [
    id 190
    label "temper"
  ]
  node [
    id 191
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 192
    label "stan"
  ]
  node [
    id 193
    label "samopoczucie"
  ]
  node [
    id 194
    label "mechanizm_obronny"
  ]
  node [
    id 195
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 196
    label "fondness"
  ]
  node [
    id 197
    label "nastr&#243;j"
  ]
  node [
    id 198
    label "state"
  ]
  node [
    id 199
    label "zbanalizowanie_si&#281;"
  ]
  node [
    id 200
    label "&#322;atwiutko"
  ]
  node [
    id 201
    label "b&#322;aho"
  ]
  node [
    id 202
    label "strywializowanie"
  ]
  node [
    id 203
    label "trywializowanie"
  ]
  node [
    id 204
    label "niepoczesny"
  ]
  node [
    id 205
    label "banalnie"
  ]
  node [
    id 206
    label "duperelny"
  ]
  node [
    id 207
    label "g&#322;upi"
  ]
  node [
    id 208
    label "banalnienie"
  ]
  node [
    id 209
    label "nies&#322;uszny"
  ]
  node [
    id 210
    label "nieprawid&#322;owo"
  ]
  node [
    id 211
    label "niestosowny"
  ]
  node [
    id 212
    label "niepoprawnie"
  ]
  node [
    id 213
    label "nieuleczalny"
  ]
  node [
    id 214
    label "z&#322;y"
  ]
  node [
    id 215
    label "nieuleczalnie"
  ]
  node [
    id 216
    label "&#347;miertelny"
  ]
  node [
    id 217
    label "niezwyk&#322;y"
  ]
  node [
    id 218
    label "wielki"
  ]
  node [
    id 219
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 220
    label "nies&#322;usznie"
  ]
  node [
    id 221
    label "nieprawdziwy"
  ]
  node [
    id 222
    label "bezsensowny"
  ]
  node [
    id 223
    label "pieski"
  ]
  node [
    id 224
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 225
    label "niekorzystny"
  ]
  node [
    id 226
    label "z&#322;oszczenie"
  ]
  node [
    id 227
    label "sierdzisty"
  ]
  node [
    id 228
    label "niegrzeczny"
  ]
  node [
    id 229
    label "zez&#322;oszczenie"
  ]
  node [
    id 230
    label "zdenerwowany"
  ]
  node [
    id 231
    label "negatywny"
  ]
  node [
    id 232
    label "rozgniewanie"
  ]
  node [
    id 233
    label "gniewanie"
  ]
  node [
    id 234
    label "niemoralny"
  ]
  node [
    id 235
    label "&#378;le"
  ]
  node [
    id 236
    label "niepomy&#347;lny"
  ]
  node [
    id 237
    label "syf"
  ]
  node [
    id 238
    label "nieodpowiednio"
  ]
  node [
    id 239
    label "nienale&#380;yty"
  ]
  node [
    id 240
    label "nieprawid&#322;owy"
  ]
  node [
    id 241
    label "improperly"
  ]
  node [
    id 242
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 243
    label "usilnie"
  ]
  node [
    id 244
    label "polityczny"
  ]
  node [
    id 245
    label "internowanie"
  ]
  node [
    id 246
    label "prorz&#261;dowy"
  ]
  node [
    id 247
    label "internowa&#263;"
  ]
  node [
    id 248
    label "wi&#281;zie&#324;"
  ]
  node [
    id 249
    label "ideologiczny"
  ]
  node [
    id 250
    label "znaczenie"
  ]
  node [
    id 251
    label "odk&#322;adanie"
  ]
  node [
    id 252
    label "condition"
  ]
  node [
    id 253
    label "liczenie"
  ]
  node [
    id 254
    label "stawianie"
  ]
  node [
    id 255
    label "bycie"
  ]
  node [
    id 256
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 257
    label "assay"
  ]
  node [
    id 258
    label "wskazywanie"
  ]
  node [
    id 259
    label "wyraz"
  ]
  node [
    id 260
    label "gravity"
  ]
  node [
    id 261
    label "weight"
  ]
  node [
    id 262
    label "command"
  ]
  node [
    id 263
    label "odgrywanie_roli"
  ]
  node [
    id 264
    label "istota"
  ]
  node [
    id 265
    label "informacja"
  ]
  node [
    id 266
    label "cecha"
  ]
  node [
    id 267
    label "okre&#347;lanie"
  ]
  node [
    id 268
    label "kto&#347;"
  ]
  node [
    id 269
    label "wyra&#380;enie"
  ]
  node [
    id 270
    label "rig"
  ]
  node [
    id 271
    label "message"
  ]
  node [
    id 272
    label "wykonywa&#263;"
  ]
  node [
    id 273
    label "prowadzi&#263;"
  ]
  node [
    id 274
    label "deliver"
  ]
  node [
    id 275
    label "powodowa&#263;"
  ]
  node [
    id 276
    label "wzbudza&#263;"
  ]
  node [
    id 277
    label "moderate"
  ]
  node [
    id 278
    label "wprowadza&#263;"
  ]
  node [
    id 279
    label "mie&#263;_miejsce"
  ]
  node [
    id 280
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 281
    label "motywowa&#263;"
  ]
  node [
    id 282
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 283
    label "go"
  ]
  node [
    id 284
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 285
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 286
    label "robi&#263;"
  ]
  node [
    id 287
    label "wytwarza&#263;"
  ]
  node [
    id 288
    label "muzyka"
  ]
  node [
    id 289
    label "work"
  ]
  node [
    id 290
    label "create"
  ]
  node [
    id 291
    label "praca"
  ]
  node [
    id 292
    label "rola"
  ]
  node [
    id 293
    label "&#380;y&#263;"
  ]
  node [
    id 294
    label "kierowa&#263;"
  ]
  node [
    id 295
    label "g&#243;rowa&#263;"
  ]
  node [
    id 296
    label "tworzy&#263;"
  ]
  node [
    id 297
    label "krzywa"
  ]
  node [
    id 298
    label "linia_melodyczna"
  ]
  node [
    id 299
    label "control"
  ]
  node [
    id 300
    label "string"
  ]
  node [
    id 301
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 302
    label "ukierunkowywa&#263;"
  ]
  node [
    id 303
    label "sterowa&#263;"
  ]
  node [
    id 304
    label "kre&#347;li&#263;"
  ]
  node [
    id 305
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 306
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 307
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 308
    label "eksponowa&#263;"
  ]
  node [
    id 309
    label "navigate"
  ]
  node [
    id 310
    label "manipulate"
  ]
  node [
    id 311
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 312
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 313
    label "przesuwa&#263;"
  ]
  node [
    id 314
    label "partner"
  ]
  node [
    id 315
    label "prowadzenie"
  ]
  node [
    id 316
    label "rynek"
  ]
  node [
    id 317
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 318
    label "wprawia&#263;"
  ]
  node [
    id 319
    label "zaczyna&#263;"
  ]
  node [
    id 320
    label "wpisywa&#263;"
  ]
  node [
    id 321
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 322
    label "wchodzi&#263;"
  ]
  node [
    id 323
    label "take"
  ]
  node [
    id 324
    label "zapoznawa&#263;"
  ]
  node [
    id 325
    label "inflict"
  ]
  node [
    id 326
    label "umieszcza&#263;"
  ]
  node [
    id 327
    label "schodzi&#263;"
  ]
  node [
    id 328
    label "induct"
  ]
  node [
    id 329
    label "begin"
  ]
  node [
    id 330
    label "reputacja"
  ]
  node [
    id 331
    label "pogl&#261;d"
  ]
  node [
    id 332
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 333
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 334
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 335
    label "wielko&#347;&#263;"
  ]
  node [
    id 336
    label "kryterium"
  ]
  node [
    id 337
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 338
    label "ekspertyza"
  ]
  node [
    id 339
    label "dokument"
  ]
  node [
    id 340
    label "appraisal"
  ]
  node [
    id 341
    label "charakterystyka"
  ]
  node [
    id 342
    label "m&#322;ot"
  ]
  node [
    id 343
    label "znak"
  ]
  node [
    id 344
    label "drzewo"
  ]
  node [
    id 345
    label "pr&#243;ba"
  ]
  node [
    id 346
    label "attribute"
  ]
  node [
    id 347
    label "marka"
  ]
  node [
    id 348
    label "&#347;wiadectwo"
  ]
  node [
    id 349
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 350
    label "parafa"
  ]
  node [
    id 351
    label "plik"
  ]
  node [
    id 352
    label "raport&#243;wka"
  ]
  node [
    id 353
    label "utw&#243;r"
  ]
  node [
    id 354
    label "record"
  ]
  node [
    id 355
    label "fascyku&#322;"
  ]
  node [
    id 356
    label "dokumentacja"
  ]
  node [
    id 357
    label "registratura"
  ]
  node [
    id 358
    label "artyku&#322;"
  ]
  node [
    id 359
    label "writing"
  ]
  node [
    id 360
    label "sygnatariusz"
  ]
  node [
    id 361
    label "badanie"
  ]
  node [
    id 362
    label "sketch"
  ]
  node [
    id 363
    label "survey"
  ]
  node [
    id 364
    label "s&#261;d"
  ]
  node [
    id 365
    label "teologicznie"
  ]
  node [
    id 366
    label "belief"
  ]
  node [
    id 367
    label "zderzenie_si&#281;"
  ]
  node [
    id 368
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 369
    label "teoria_Arrheniusa"
  ]
  node [
    id 370
    label "punkt"
  ]
  node [
    id 371
    label "publikacja"
  ]
  node [
    id 372
    label "wiedza"
  ]
  node [
    id 373
    label "doj&#347;cie"
  ]
  node [
    id 374
    label "obiega&#263;"
  ]
  node [
    id 375
    label "powzi&#281;cie"
  ]
  node [
    id 376
    label "dane"
  ]
  node [
    id 377
    label "obiegni&#281;cie"
  ]
  node [
    id 378
    label "sygna&#322;"
  ]
  node [
    id 379
    label "obieganie"
  ]
  node [
    id 380
    label "powzi&#261;&#263;"
  ]
  node [
    id 381
    label "obiec"
  ]
  node [
    id 382
    label "doj&#347;&#263;"
  ]
  node [
    id 383
    label "warunek_lokalowy"
  ]
  node [
    id 384
    label "rozmiar"
  ]
  node [
    id 385
    label "liczba"
  ]
  node [
    id 386
    label "rzadko&#347;&#263;"
  ]
  node [
    id 387
    label "zaleta"
  ]
  node [
    id 388
    label "ilo&#347;&#263;"
  ]
  node [
    id 389
    label "measure"
  ]
  node [
    id 390
    label "dymensja"
  ]
  node [
    id 391
    label "poj&#281;cie"
  ]
  node [
    id 392
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 393
    label "zdolno&#347;&#263;"
  ]
  node [
    id 394
    label "potencja"
  ]
  node [
    id 395
    label "property"
  ]
  node [
    id 396
    label "czynnik"
  ]
  node [
    id 397
    label "upublicznianie"
  ]
  node [
    id 398
    label "jawny"
  ]
  node [
    id 399
    label "upublicznienie"
  ]
  node [
    id 400
    label "publicznie"
  ]
  node [
    id 401
    label "jawnie"
  ]
  node [
    id 402
    label "udost&#281;pnianie"
  ]
  node [
    id 403
    label "udost&#281;pnienie"
  ]
  node [
    id 404
    label "ujawnienie_si&#281;"
  ]
  node [
    id 405
    label "ujawnianie_si&#281;"
  ]
  node [
    id 406
    label "zdecydowany"
  ]
  node [
    id 407
    label "znajomy"
  ]
  node [
    id 408
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 409
    label "ujawnienie"
  ]
  node [
    id 410
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 411
    label "ujawnianie"
  ]
  node [
    id 412
    label "ewidentny"
  ]
  node [
    id 413
    label "specjalny"
  ]
  node [
    id 414
    label "intencjonalny"
  ]
  node [
    id 415
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 416
    label "niedorozw&#243;j"
  ]
  node [
    id 417
    label "szczeg&#243;lny"
  ]
  node [
    id 418
    label "specjalnie"
  ]
  node [
    id 419
    label "nieetatowy"
  ]
  node [
    id 420
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 421
    label "nienormalny"
  ]
  node [
    id 422
    label "umy&#347;lnie"
  ]
  node [
    id 423
    label "odpowiedni"
  ]
  node [
    id 424
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 425
    label "zaj&#281;cie"
  ]
  node [
    id 426
    label "Jezus_Chrystus"
  ]
  node [
    id 427
    label "zajawka"
  ]
  node [
    id 428
    label "lunacy"
  ]
  node [
    id 429
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 430
    label "kurwica"
  ]
  node [
    id 431
    label "tendency"
  ]
  node [
    id 432
    label "feblik"
  ]
  node [
    id 433
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 434
    label "dzie&#322;o"
  ]
  node [
    id 435
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 436
    label "pie&#347;&#324;_pasyjna"
  ]
  node [
    id 437
    label "dedication"
  ]
  node [
    id 438
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 439
    label "nami&#281;tny"
  ]
  node [
    id 440
    label "z&#322;o&#347;&#263;"
  ]
  node [
    id 441
    label "poryw"
  ]
  node [
    id 442
    label "gniew"
  ]
  node [
    id 443
    label "wkurw"
  ]
  node [
    id 444
    label "fury"
  ]
  node [
    id 445
    label "sympatia"
  ]
  node [
    id 446
    label "podatno&#347;&#263;"
  ]
  node [
    id 447
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 448
    label "care"
  ]
  node [
    id 449
    label "zdarzenie_si&#281;"
  ]
  node [
    id 450
    label "benedykty&#324;ski"
  ]
  node [
    id 451
    label "career"
  ]
  node [
    id 452
    label "anektowanie"
  ]
  node [
    id 453
    label "dostarczenie"
  ]
  node [
    id 454
    label "u&#380;ycie"
  ]
  node [
    id 455
    label "spowodowanie"
  ]
  node [
    id 456
    label "klasyfikacja"
  ]
  node [
    id 457
    label "zadanie"
  ]
  node [
    id 458
    label "wzi&#281;cie"
  ]
  node [
    id 459
    label "wzbudzenie"
  ]
  node [
    id 460
    label "tynkarski"
  ]
  node [
    id 461
    label "wype&#322;nienie"
  ]
  node [
    id 462
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 463
    label "zapanowanie"
  ]
  node [
    id 464
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 465
    label "zmiana"
  ]
  node [
    id 466
    label "czynnik_produkcji"
  ]
  node [
    id 467
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 468
    label "pozajmowanie"
  ]
  node [
    id 469
    label "activity"
  ]
  node [
    id 470
    label "ulokowanie_si&#281;"
  ]
  node [
    id 471
    label "usytuowanie_si&#281;"
  ]
  node [
    id 472
    label "obj&#281;cie"
  ]
  node [
    id 473
    label "zabranie"
  ]
  node [
    id 474
    label "devotion"
  ]
  node [
    id 475
    label "powa&#380;anie"
  ]
  node [
    id 476
    label "dba&#322;o&#347;&#263;"
  ]
  node [
    id 477
    label "obrz&#281;d"
  ]
  node [
    id 478
    label "obrazowanie"
  ]
  node [
    id 479
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 480
    label "dorobek"
  ]
  node [
    id 481
    label "forma"
  ]
  node [
    id 482
    label "tre&#347;&#263;"
  ]
  node [
    id 483
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 484
    label "retrospektywa"
  ]
  node [
    id 485
    label "works"
  ]
  node [
    id 486
    label "creation"
  ]
  node [
    id 487
    label "tekst"
  ]
  node [
    id 488
    label "tetralogia"
  ]
  node [
    id 489
    label "komunikat"
  ]
  node [
    id 490
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 491
    label "organ"
  ]
  node [
    id 492
    label "part"
  ]
  node [
    id 493
    label "element_anatomiczny"
  ]
  node [
    id 494
    label "w&#347;ciek&#322;o&#347;&#263;"
  ]
  node [
    id 495
    label "streszczenie"
  ]
  node [
    id 496
    label "harbinger"
  ]
  node [
    id 497
    label "ch&#281;&#263;"
  ]
  node [
    id 498
    label "zapowied&#378;"
  ]
  node [
    id 499
    label "zami&#322;owanie"
  ]
  node [
    id 500
    label "czasopismo"
  ]
  node [
    id 501
    label "reklama"
  ]
  node [
    id 502
    label "gadka"
  ]
  node [
    id 503
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 504
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 505
    label "gor&#261;cy"
  ]
  node [
    id 506
    label "nami&#281;tnie"
  ]
  node [
    id 507
    label "g&#322;&#281;boki"
  ]
  node [
    id 508
    label "ciep&#322;y"
  ]
  node [
    id 509
    label "zmys&#322;owy"
  ]
  node [
    id 510
    label "&#380;ywy"
  ]
  node [
    id 511
    label "gorliwy"
  ]
  node [
    id 512
    label "czu&#322;y"
  ]
  node [
    id 513
    label "kusz&#261;cy"
  ]
  node [
    id 514
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 515
    label "wyci&#261;&#263;"
  ]
  node [
    id 516
    label "spa&#347;&#263;"
  ]
  node [
    id 517
    label "fall"
  ]
  node [
    id 518
    label "wybi&#263;"
  ]
  node [
    id 519
    label "uderzy&#263;"
  ]
  node [
    id 520
    label "zrobi&#263;"
  ]
  node [
    id 521
    label "slaughter"
  ]
  node [
    id 522
    label "overwhelm"
  ]
  node [
    id 523
    label "wyrze&#378;bi&#263;"
  ]
  node [
    id 524
    label "przewr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 525
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 526
    label "pora_roku"
  ]
  node [
    id 527
    label "conquest"
  ]
  node [
    id 528
    label "sukces"
  ]
  node [
    id 529
    label "puchar"
  ]
  node [
    id 530
    label "kobieta_sukcesu"
  ]
  node [
    id 531
    label "success"
  ]
  node [
    id 532
    label "rezultat"
  ]
  node [
    id 533
    label "passa"
  ]
  node [
    id 534
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 535
    label "naczynie"
  ]
  node [
    id 536
    label "nagroda"
  ]
  node [
    id 537
    label "zwyci&#281;stwo"
  ]
  node [
    id 538
    label "zawody"
  ]
  node [
    id 539
    label "zawarto&#347;&#263;"
  ]
  node [
    id 540
    label "zesp&#243;&#322;"
  ]
  node [
    id 541
    label "partia"
  ]
  node [
    id 542
    label "tr&#243;jka"
  ]
  node [
    id 543
    label "toto-lotek"
  ]
  node [
    id 544
    label "trafienie"
  ]
  node [
    id 545
    label "zbi&#243;r"
  ]
  node [
    id 546
    label "bilard"
  ]
  node [
    id 547
    label "kie&#322;"
  ]
  node [
    id 548
    label "hotel"
  ]
  node [
    id 549
    label "stopie&#324;"
  ]
  node [
    id 550
    label "cyfra"
  ]
  node [
    id 551
    label "pok&#243;j"
  ]
  node [
    id 552
    label "obiekt"
  ]
  node [
    id 553
    label "three"
  ]
  node [
    id 554
    label "blotka"
  ]
  node [
    id 555
    label "zaprz&#281;g"
  ]
  node [
    id 556
    label "krok_taneczny"
  ]
  node [
    id 557
    label "Mazowsze"
  ]
  node [
    id 558
    label "odm&#322;adzanie"
  ]
  node [
    id 559
    label "&#346;wietliki"
  ]
  node [
    id 560
    label "whole"
  ]
  node [
    id 561
    label "skupienie"
  ]
  node [
    id 562
    label "The_Beatles"
  ]
  node [
    id 563
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 564
    label "odm&#322;adza&#263;"
  ]
  node [
    id 565
    label "zabudowania"
  ]
  node [
    id 566
    label "group"
  ]
  node [
    id 567
    label "zespolik"
  ]
  node [
    id 568
    label "schorzenie"
  ]
  node [
    id 569
    label "ro&#347;lina"
  ]
  node [
    id 570
    label "Depeche_Mode"
  ]
  node [
    id 571
    label "batch"
  ]
  node [
    id 572
    label "odm&#322;odzenie"
  ]
  node [
    id 573
    label "Bund"
  ]
  node [
    id 574
    label "PPR"
  ]
  node [
    id 575
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 576
    label "wybranek"
  ]
  node [
    id 577
    label "Jakobici"
  ]
  node [
    id 578
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 579
    label "SLD"
  ]
  node [
    id 580
    label "Razem"
  ]
  node [
    id 581
    label "PiS"
  ]
  node [
    id 582
    label "package"
  ]
  node [
    id 583
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 584
    label "Kuomintang"
  ]
  node [
    id 585
    label "ZSL"
  ]
  node [
    id 586
    label "organizacja"
  ]
  node [
    id 587
    label "AWS"
  ]
  node [
    id 588
    label "gra"
  ]
  node [
    id 589
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 590
    label "game"
  ]
  node [
    id 591
    label "blok"
  ]
  node [
    id 592
    label "materia&#322;"
  ]
  node [
    id 593
    label "PO"
  ]
  node [
    id 594
    label "si&#322;a"
  ]
  node [
    id 595
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 596
    label "niedoczas"
  ]
  node [
    id 597
    label "Federali&#347;ci"
  ]
  node [
    id 598
    label "PSL"
  ]
  node [
    id 599
    label "Wigowie"
  ]
  node [
    id 600
    label "ZChN"
  ]
  node [
    id 601
    label "egzekutywa"
  ]
  node [
    id 602
    label "aktyw"
  ]
  node [
    id 603
    label "wybranka"
  ]
  node [
    id 604
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 605
    label "unit"
  ]
  node [
    id 606
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 607
    label "dziedzina"
  ]
  node [
    id 608
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 609
    label "sfera"
  ]
  node [
    id 610
    label "zakres"
  ]
  node [
    id 611
    label "bezdro&#380;e"
  ]
  node [
    id 612
    label "poddzia&#322;"
  ]
  node [
    id 613
    label "David"
  ]
  node [
    id 614
    label "Zucker"
  ]
  node [
    id 615
    label "Jim"
  ]
  node [
    id 616
    label "Abrahams"
  ]
  node [
    id 617
    label "Jerry"
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 196
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 528
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 536
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 538
  ]
  edge [
    source 15
    target 539
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 544
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 547
  ]
  edge [
    source 16
    target 548
  ]
  edge [
    source 16
    target 549
  ]
  edge [
    source 16
    target 550
  ]
  edge [
    source 16
    target 551
  ]
  edge [
    source 16
    target 552
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 554
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 557
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 560
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 565
  ]
  edge [
    source 16
    target 566
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 478
  ]
  edge [
    source 16
    target 479
  ]
  edge [
    source 16
    target 491
  ]
  edge [
    source 16
    target 482
  ]
  edge [
    source 16
    target 483
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 493
  ]
  edge [
    source 16
    target 487
  ]
  edge [
    source 16
    target 489
  ]
  edge [
    source 16
    target 490
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 613
    target 614
  ]
  edge [
    source 614
    target 617
  ]
  edge [
    source 615
    target 616
  ]
]
