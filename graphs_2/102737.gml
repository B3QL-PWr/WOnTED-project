graph [
  node [
    id 0
    label "prezydent"
    origin "text"
  ]
  node [
    id 1
    label "miasto"
    origin "text"
  ]
  node [
    id 2
    label "gdynia"
    origin "text"
  ]
  node [
    id 3
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "otwarty"
    origin "text"
  ]
  node [
    id 5
    label "konkurs"
    origin "text"
  ]
  node [
    id 6
    label "oferta"
    origin "text"
  ]
  node [
    id 7
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 8
    label "cela"
    origin "text"
  ]
  node [
    id 9
    label "przyznanie"
    origin "text"
  ]
  node [
    id 10
    label "dotacja"
    origin "text"
  ]
  node [
    id 11
    label "podmiot"
    origin "text"
  ]
  node [
    id 12
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 13
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 14
    label "po&#380;ytek"
    origin "text"
  ]
  node [
    id 15
    label "publiczny"
    origin "text"
  ]
  node [
    id 16
    label "wsparcie"
    origin "text"
  ]
  node [
    id 17
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 18
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 19
    label "hipoterapeutycznego"
    origin "text"
  ]
  node [
    id 20
    label "mie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 21
    label "si&#281;"
    origin "text"
  ]
  node [
    id 22
    label "przy"
    origin "text"
  ]
  node [
    id 23
    label "ulica"
    origin "text"
  ]
  node [
    id 24
    label "sp&#243;&#322;dzielczy"
    origin "text"
  ]
  node [
    id 25
    label "okres"
    origin "text"
  ]
  node [
    id 26
    label "lipiec"
    origin "text"
  ]
  node [
    id 27
    label "rok"
    origin "text"
  ]
  node [
    id 28
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 29
    label "gruba_ryba"
  ]
  node [
    id 30
    label "Gorbaczow"
  ]
  node [
    id 31
    label "zwierzchnik"
  ]
  node [
    id 32
    label "Putin"
  ]
  node [
    id 33
    label "Tito"
  ]
  node [
    id 34
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 35
    label "Naser"
  ]
  node [
    id 36
    label "de_Gaulle"
  ]
  node [
    id 37
    label "Nixon"
  ]
  node [
    id 38
    label "Kemal"
  ]
  node [
    id 39
    label "Clinton"
  ]
  node [
    id 40
    label "Bierut"
  ]
  node [
    id 41
    label "Roosevelt"
  ]
  node [
    id 42
    label "samorz&#261;dowiec"
  ]
  node [
    id 43
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 44
    label "Jelcyn"
  ]
  node [
    id 45
    label "dostojnik"
  ]
  node [
    id 46
    label "pryncypa&#322;"
  ]
  node [
    id 47
    label "kierowa&#263;"
  ]
  node [
    id 48
    label "kierownictwo"
  ]
  node [
    id 49
    label "cz&#322;owiek"
  ]
  node [
    id 50
    label "urz&#281;dnik"
  ]
  node [
    id 51
    label "notabl"
  ]
  node [
    id 52
    label "oficja&#322;"
  ]
  node [
    id 53
    label "samorz&#261;d"
  ]
  node [
    id 54
    label "polityk"
  ]
  node [
    id 55
    label "komuna"
  ]
  node [
    id 56
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 57
    label "Brunszwik"
  ]
  node [
    id 58
    label "Twer"
  ]
  node [
    id 59
    label "Marki"
  ]
  node [
    id 60
    label "Tarnopol"
  ]
  node [
    id 61
    label "Czerkiesk"
  ]
  node [
    id 62
    label "Johannesburg"
  ]
  node [
    id 63
    label "Nowogr&#243;d"
  ]
  node [
    id 64
    label "Heidelberg"
  ]
  node [
    id 65
    label "Korsze"
  ]
  node [
    id 66
    label "Chocim"
  ]
  node [
    id 67
    label "Lenzen"
  ]
  node [
    id 68
    label "Bie&#322;gorod"
  ]
  node [
    id 69
    label "Hebron"
  ]
  node [
    id 70
    label "Korynt"
  ]
  node [
    id 71
    label "Pemba"
  ]
  node [
    id 72
    label "Norfolk"
  ]
  node [
    id 73
    label "Tarragona"
  ]
  node [
    id 74
    label "Loreto"
  ]
  node [
    id 75
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 76
    label "Paczk&#243;w"
  ]
  node [
    id 77
    label "Krasnodar"
  ]
  node [
    id 78
    label "Hadziacz"
  ]
  node [
    id 79
    label "Cymlansk"
  ]
  node [
    id 80
    label "Efez"
  ]
  node [
    id 81
    label "Kandahar"
  ]
  node [
    id 82
    label "&#346;wiebodzice"
  ]
  node [
    id 83
    label "Antwerpia"
  ]
  node [
    id 84
    label "Baltimore"
  ]
  node [
    id 85
    label "Eger"
  ]
  node [
    id 86
    label "Cumana"
  ]
  node [
    id 87
    label "Kanton"
  ]
  node [
    id 88
    label "Sarat&#243;w"
  ]
  node [
    id 89
    label "Siena"
  ]
  node [
    id 90
    label "Dubno"
  ]
  node [
    id 91
    label "Tyl&#380;a"
  ]
  node [
    id 92
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 93
    label "Pi&#324;sk"
  ]
  node [
    id 94
    label "Toledo"
  ]
  node [
    id 95
    label "Piza"
  ]
  node [
    id 96
    label "Triest"
  ]
  node [
    id 97
    label "Struga"
  ]
  node [
    id 98
    label "Gettysburg"
  ]
  node [
    id 99
    label "Sierdobsk"
  ]
  node [
    id 100
    label "Xai-Xai"
  ]
  node [
    id 101
    label "Bristol"
  ]
  node [
    id 102
    label "Katania"
  ]
  node [
    id 103
    label "Parma"
  ]
  node [
    id 104
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 105
    label "Dniepropetrowsk"
  ]
  node [
    id 106
    label "Tours"
  ]
  node [
    id 107
    label "Mohylew"
  ]
  node [
    id 108
    label "Suzdal"
  ]
  node [
    id 109
    label "Samara"
  ]
  node [
    id 110
    label "Akerman"
  ]
  node [
    id 111
    label "Szk&#322;&#243;w"
  ]
  node [
    id 112
    label "Chimoio"
  ]
  node [
    id 113
    label "Perm"
  ]
  node [
    id 114
    label "Murma&#324;sk"
  ]
  node [
    id 115
    label "Z&#322;oczew"
  ]
  node [
    id 116
    label "Reda"
  ]
  node [
    id 117
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 118
    label "Aleksandria"
  ]
  node [
    id 119
    label "Kowel"
  ]
  node [
    id 120
    label "Hamburg"
  ]
  node [
    id 121
    label "Rudki"
  ]
  node [
    id 122
    label "O&#322;omuniec"
  ]
  node [
    id 123
    label "Kowno"
  ]
  node [
    id 124
    label "Luksor"
  ]
  node [
    id 125
    label "Cremona"
  ]
  node [
    id 126
    label "Suczawa"
  ]
  node [
    id 127
    label "M&#252;nster"
  ]
  node [
    id 128
    label "Peszawar"
  ]
  node [
    id 129
    label "Los_Angeles"
  ]
  node [
    id 130
    label "Szawle"
  ]
  node [
    id 131
    label "Winnica"
  ]
  node [
    id 132
    label "I&#322;awka"
  ]
  node [
    id 133
    label "Poniatowa"
  ]
  node [
    id 134
    label "Ko&#322;omyja"
  ]
  node [
    id 135
    label "Asy&#380;"
  ]
  node [
    id 136
    label "Tolkmicko"
  ]
  node [
    id 137
    label "Orlean"
  ]
  node [
    id 138
    label "Koper"
  ]
  node [
    id 139
    label "Le&#324;sk"
  ]
  node [
    id 140
    label "Rostock"
  ]
  node [
    id 141
    label "Mantua"
  ]
  node [
    id 142
    label "Barcelona"
  ]
  node [
    id 143
    label "Mo&#347;ciska"
  ]
  node [
    id 144
    label "Koluszki"
  ]
  node [
    id 145
    label "Stalingrad"
  ]
  node [
    id 146
    label "Fergana"
  ]
  node [
    id 147
    label "A&#322;czewsk"
  ]
  node [
    id 148
    label "Kaszyn"
  ]
  node [
    id 149
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 150
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 151
    label "D&#252;sseldorf"
  ]
  node [
    id 152
    label "Mozyrz"
  ]
  node [
    id 153
    label "Syrakuzy"
  ]
  node [
    id 154
    label "Peszt"
  ]
  node [
    id 155
    label "Lichinga"
  ]
  node [
    id 156
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 157
    label "Choroszcz"
  ]
  node [
    id 158
    label "Po&#322;ock"
  ]
  node [
    id 159
    label "Cherso&#324;"
  ]
  node [
    id 160
    label "Fryburg"
  ]
  node [
    id 161
    label "Izmir"
  ]
  node [
    id 162
    label "Jawor&#243;w"
  ]
  node [
    id 163
    label "Wenecja"
  ]
  node [
    id 164
    label "Kordoba"
  ]
  node [
    id 165
    label "Mrocza"
  ]
  node [
    id 166
    label "Solikamsk"
  ]
  node [
    id 167
    label "Be&#322;z"
  ]
  node [
    id 168
    label "Wo&#322;gograd"
  ]
  node [
    id 169
    label "&#379;ar&#243;w"
  ]
  node [
    id 170
    label "Brugia"
  ]
  node [
    id 171
    label "Radk&#243;w"
  ]
  node [
    id 172
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 173
    label "Harbin"
  ]
  node [
    id 174
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 175
    label "Zaporo&#380;e"
  ]
  node [
    id 176
    label "Smorgonie"
  ]
  node [
    id 177
    label "Nowa_D&#281;ba"
  ]
  node [
    id 178
    label "Aktobe"
  ]
  node [
    id 179
    label "Ussuryjsk"
  ]
  node [
    id 180
    label "Mo&#380;ajsk"
  ]
  node [
    id 181
    label "Tanger"
  ]
  node [
    id 182
    label "Nowogard"
  ]
  node [
    id 183
    label "Utrecht"
  ]
  node [
    id 184
    label "Czerniejewo"
  ]
  node [
    id 185
    label "Bazylea"
  ]
  node [
    id 186
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 187
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 188
    label "Tu&#322;a"
  ]
  node [
    id 189
    label "Al-Kufa"
  ]
  node [
    id 190
    label "Jutrosin"
  ]
  node [
    id 191
    label "Czelabi&#324;sk"
  ]
  node [
    id 192
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 193
    label "Split"
  ]
  node [
    id 194
    label "Czerniowce"
  ]
  node [
    id 195
    label "Majsur"
  ]
  node [
    id 196
    label "Poczdam"
  ]
  node [
    id 197
    label "Troick"
  ]
  node [
    id 198
    label "Minusi&#324;sk"
  ]
  node [
    id 199
    label "Kostroma"
  ]
  node [
    id 200
    label "Barwice"
  ]
  node [
    id 201
    label "U&#322;an_Ude"
  ]
  node [
    id 202
    label "Czeskie_Budziejowice"
  ]
  node [
    id 203
    label "Getynga"
  ]
  node [
    id 204
    label "Kercz"
  ]
  node [
    id 205
    label "B&#322;aszki"
  ]
  node [
    id 206
    label "Lipawa"
  ]
  node [
    id 207
    label "Bujnaksk"
  ]
  node [
    id 208
    label "Wittenberga"
  ]
  node [
    id 209
    label "Gorycja"
  ]
  node [
    id 210
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 211
    label "Swatowe"
  ]
  node [
    id 212
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 213
    label "Magadan"
  ]
  node [
    id 214
    label "Rzg&#243;w"
  ]
  node [
    id 215
    label "Bijsk"
  ]
  node [
    id 216
    label "Norylsk"
  ]
  node [
    id 217
    label "Mesyna"
  ]
  node [
    id 218
    label "Berezyna"
  ]
  node [
    id 219
    label "Stawropol"
  ]
  node [
    id 220
    label "Kircholm"
  ]
  node [
    id 221
    label "Hawana"
  ]
  node [
    id 222
    label "Pardubice"
  ]
  node [
    id 223
    label "Drezno"
  ]
  node [
    id 224
    label "Zaklik&#243;w"
  ]
  node [
    id 225
    label "Kozielsk"
  ]
  node [
    id 226
    label "Paw&#322;owo"
  ]
  node [
    id 227
    label "Kani&#243;w"
  ]
  node [
    id 228
    label "Adana"
  ]
  node [
    id 229
    label "Kleczew"
  ]
  node [
    id 230
    label "Rybi&#324;sk"
  ]
  node [
    id 231
    label "Dayton"
  ]
  node [
    id 232
    label "Nowy_Orlean"
  ]
  node [
    id 233
    label "Perejas&#322;aw"
  ]
  node [
    id 234
    label "Jenisejsk"
  ]
  node [
    id 235
    label "Bolonia"
  ]
  node [
    id 236
    label "Bir&#380;e"
  ]
  node [
    id 237
    label "Marsylia"
  ]
  node [
    id 238
    label "Workuta"
  ]
  node [
    id 239
    label "Sewilla"
  ]
  node [
    id 240
    label "Megara"
  ]
  node [
    id 241
    label "Gotha"
  ]
  node [
    id 242
    label "Kiejdany"
  ]
  node [
    id 243
    label "Zaleszczyki"
  ]
  node [
    id 244
    label "Ja&#322;ta"
  ]
  node [
    id 245
    label "Burgas"
  ]
  node [
    id 246
    label "Essen"
  ]
  node [
    id 247
    label "Czadca"
  ]
  node [
    id 248
    label "Manchester"
  ]
  node [
    id 249
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 250
    label "Schmalkalden"
  ]
  node [
    id 251
    label "Oleszyce"
  ]
  node [
    id 252
    label "Kie&#380;mark"
  ]
  node [
    id 253
    label "Kleck"
  ]
  node [
    id 254
    label "Suez"
  ]
  node [
    id 255
    label "Brack"
  ]
  node [
    id 256
    label "Symferopol"
  ]
  node [
    id 257
    label "Michalovce"
  ]
  node [
    id 258
    label "Tambow"
  ]
  node [
    id 259
    label "Turkmenbaszy"
  ]
  node [
    id 260
    label "Bogumin"
  ]
  node [
    id 261
    label "Sambor"
  ]
  node [
    id 262
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 263
    label "Milan&#243;wek"
  ]
  node [
    id 264
    label "Nachiczewan"
  ]
  node [
    id 265
    label "Cluny"
  ]
  node [
    id 266
    label "Stalinogorsk"
  ]
  node [
    id 267
    label "Lipsk"
  ]
  node [
    id 268
    label "Karlsbad"
  ]
  node [
    id 269
    label "Pietrozawodsk"
  ]
  node [
    id 270
    label "Bar"
  ]
  node [
    id 271
    label "Korfant&#243;w"
  ]
  node [
    id 272
    label "Nieftiegorsk"
  ]
  node [
    id 273
    label "Hanower"
  ]
  node [
    id 274
    label "Windawa"
  ]
  node [
    id 275
    label "&#346;niatyn"
  ]
  node [
    id 276
    label "Dalton"
  ]
  node [
    id 277
    label "tramwaj"
  ]
  node [
    id 278
    label "Kaszgar"
  ]
  node [
    id 279
    label "Berdia&#324;sk"
  ]
  node [
    id 280
    label "Koprzywnica"
  ]
  node [
    id 281
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 282
    label "Brno"
  ]
  node [
    id 283
    label "Wia&#378;ma"
  ]
  node [
    id 284
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 285
    label "Starobielsk"
  ]
  node [
    id 286
    label "Ostr&#243;g"
  ]
  node [
    id 287
    label "Oran"
  ]
  node [
    id 288
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 289
    label "Wyszehrad"
  ]
  node [
    id 290
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 291
    label "Trembowla"
  ]
  node [
    id 292
    label "Tobolsk"
  ]
  node [
    id 293
    label "Liberec"
  ]
  node [
    id 294
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 295
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 296
    label "G&#322;uszyca"
  ]
  node [
    id 297
    label "Akwileja"
  ]
  node [
    id 298
    label "Kar&#322;owice"
  ]
  node [
    id 299
    label "Borys&#243;w"
  ]
  node [
    id 300
    label "Stryj"
  ]
  node [
    id 301
    label "Czeski_Cieszyn"
  ]
  node [
    id 302
    label "Rydu&#322;towy"
  ]
  node [
    id 303
    label "Darmstadt"
  ]
  node [
    id 304
    label "Opawa"
  ]
  node [
    id 305
    label "Jerycho"
  ]
  node [
    id 306
    label "&#321;ohojsk"
  ]
  node [
    id 307
    label "Fatima"
  ]
  node [
    id 308
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 309
    label "Sara&#324;sk"
  ]
  node [
    id 310
    label "Lyon"
  ]
  node [
    id 311
    label "Wormacja"
  ]
  node [
    id 312
    label "Perwomajsk"
  ]
  node [
    id 313
    label "Lubeka"
  ]
  node [
    id 314
    label "Sura&#380;"
  ]
  node [
    id 315
    label "Karaganda"
  ]
  node [
    id 316
    label "Nazaret"
  ]
  node [
    id 317
    label "Poniewie&#380;"
  ]
  node [
    id 318
    label "Siewieromorsk"
  ]
  node [
    id 319
    label "Greifswald"
  ]
  node [
    id 320
    label "Trewir"
  ]
  node [
    id 321
    label "Nitra"
  ]
  node [
    id 322
    label "Karwina"
  ]
  node [
    id 323
    label "Houston"
  ]
  node [
    id 324
    label "Demmin"
  ]
  node [
    id 325
    label "Szamocin"
  ]
  node [
    id 326
    label "Kolkata"
  ]
  node [
    id 327
    label "Brasz&#243;w"
  ]
  node [
    id 328
    label "&#321;uck"
  ]
  node [
    id 329
    label "Peczora"
  ]
  node [
    id 330
    label "S&#322;onim"
  ]
  node [
    id 331
    label "Mekka"
  ]
  node [
    id 332
    label "Rzeczyca"
  ]
  node [
    id 333
    label "Konstancja"
  ]
  node [
    id 334
    label "Orenburg"
  ]
  node [
    id 335
    label "Pittsburgh"
  ]
  node [
    id 336
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 337
    label "Barabi&#324;sk"
  ]
  node [
    id 338
    label "Mory&#324;"
  ]
  node [
    id 339
    label "Hallstatt"
  ]
  node [
    id 340
    label "Mannheim"
  ]
  node [
    id 341
    label "Tarent"
  ]
  node [
    id 342
    label "Dortmund"
  ]
  node [
    id 343
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 344
    label "Dodona"
  ]
  node [
    id 345
    label "Trojan"
  ]
  node [
    id 346
    label "Nankin"
  ]
  node [
    id 347
    label "Weimar"
  ]
  node [
    id 348
    label "Brac&#322;aw"
  ]
  node [
    id 349
    label "Izbica_Kujawska"
  ]
  node [
    id 350
    label "Sankt_Florian"
  ]
  node [
    id 351
    label "Pilzno"
  ]
  node [
    id 352
    label "&#321;uga&#324;sk"
  ]
  node [
    id 353
    label "Sewastopol"
  ]
  node [
    id 354
    label "Poczaj&#243;w"
  ]
  node [
    id 355
    label "Pas&#322;&#281;k"
  ]
  node [
    id 356
    label "Sulech&#243;w"
  ]
  node [
    id 357
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 358
    label "Norak"
  ]
  node [
    id 359
    label "Filadelfia"
  ]
  node [
    id 360
    label "Maribor"
  ]
  node [
    id 361
    label "Detroit"
  ]
  node [
    id 362
    label "Bobolice"
  ]
  node [
    id 363
    label "K&#322;odawa"
  ]
  node [
    id 364
    label "Radziech&#243;w"
  ]
  node [
    id 365
    label "Eleusis"
  ]
  node [
    id 366
    label "W&#322;odzimierz"
  ]
  node [
    id 367
    label "Tartu"
  ]
  node [
    id 368
    label "Drohobycz"
  ]
  node [
    id 369
    label "Saloniki"
  ]
  node [
    id 370
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 371
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 372
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 373
    label "Buchara"
  ]
  node [
    id 374
    label "P&#322;owdiw"
  ]
  node [
    id 375
    label "Koszyce"
  ]
  node [
    id 376
    label "Brema"
  ]
  node [
    id 377
    label "Wagram"
  ]
  node [
    id 378
    label "Czarnobyl"
  ]
  node [
    id 379
    label "Brze&#347;&#263;"
  ]
  node [
    id 380
    label "S&#232;vres"
  ]
  node [
    id 381
    label "Dubrownik"
  ]
  node [
    id 382
    label "Grenada"
  ]
  node [
    id 383
    label "Jekaterynburg"
  ]
  node [
    id 384
    label "zabudowa"
  ]
  node [
    id 385
    label "Inhambane"
  ]
  node [
    id 386
    label "Konstantyn&#243;wka"
  ]
  node [
    id 387
    label "Krajowa"
  ]
  node [
    id 388
    label "Norymberga"
  ]
  node [
    id 389
    label "Tarnogr&#243;d"
  ]
  node [
    id 390
    label "Beresteczko"
  ]
  node [
    id 391
    label "Chabarowsk"
  ]
  node [
    id 392
    label "Boden"
  ]
  node [
    id 393
    label "Bamberg"
  ]
  node [
    id 394
    label "Podhajce"
  ]
  node [
    id 395
    label "Lhasa"
  ]
  node [
    id 396
    label "Oszmiana"
  ]
  node [
    id 397
    label "Narbona"
  ]
  node [
    id 398
    label "Carrara"
  ]
  node [
    id 399
    label "Soleczniki"
  ]
  node [
    id 400
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 401
    label "Malin"
  ]
  node [
    id 402
    label "Gandawa"
  ]
  node [
    id 403
    label "burmistrz"
  ]
  node [
    id 404
    label "Lancaster"
  ]
  node [
    id 405
    label "S&#322;uck"
  ]
  node [
    id 406
    label "Kronsztad"
  ]
  node [
    id 407
    label "Mosty"
  ]
  node [
    id 408
    label "Budionnowsk"
  ]
  node [
    id 409
    label "Oksford"
  ]
  node [
    id 410
    label "Awinion"
  ]
  node [
    id 411
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 412
    label "Edynburg"
  ]
  node [
    id 413
    label "Zagorsk"
  ]
  node [
    id 414
    label "Kaspijsk"
  ]
  node [
    id 415
    label "Konotop"
  ]
  node [
    id 416
    label "Nantes"
  ]
  node [
    id 417
    label "Sydney"
  ]
  node [
    id 418
    label "Orsza"
  ]
  node [
    id 419
    label "Krzanowice"
  ]
  node [
    id 420
    label "Tiume&#324;"
  ]
  node [
    id 421
    label "Wyborg"
  ]
  node [
    id 422
    label "Nerczy&#324;sk"
  ]
  node [
    id 423
    label "Rost&#243;w"
  ]
  node [
    id 424
    label "Halicz"
  ]
  node [
    id 425
    label "Sumy"
  ]
  node [
    id 426
    label "Locarno"
  ]
  node [
    id 427
    label "Luboml"
  ]
  node [
    id 428
    label "Mariupol"
  ]
  node [
    id 429
    label "Bras&#322;aw"
  ]
  node [
    id 430
    label "Witnica"
  ]
  node [
    id 431
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 432
    label "Orneta"
  ]
  node [
    id 433
    label "Gr&#243;dek"
  ]
  node [
    id 434
    label "Go&#347;cino"
  ]
  node [
    id 435
    label "Cannes"
  ]
  node [
    id 436
    label "Lw&#243;w"
  ]
  node [
    id 437
    label "Ulm"
  ]
  node [
    id 438
    label "Aczy&#324;sk"
  ]
  node [
    id 439
    label "Stuttgart"
  ]
  node [
    id 440
    label "weduta"
  ]
  node [
    id 441
    label "Borowsk"
  ]
  node [
    id 442
    label "Niko&#322;ajewsk"
  ]
  node [
    id 443
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 444
    label "Worone&#380;"
  ]
  node [
    id 445
    label "Delhi"
  ]
  node [
    id 446
    label "Adrianopol"
  ]
  node [
    id 447
    label "Byczyna"
  ]
  node [
    id 448
    label "Obuch&#243;w"
  ]
  node [
    id 449
    label "Tyraspol"
  ]
  node [
    id 450
    label "Modena"
  ]
  node [
    id 451
    label "Rajgr&#243;d"
  ]
  node [
    id 452
    label "Wo&#322;kowysk"
  ]
  node [
    id 453
    label "&#379;ylina"
  ]
  node [
    id 454
    label "Zurych"
  ]
  node [
    id 455
    label "Vukovar"
  ]
  node [
    id 456
    label "Narwa"
  ]
  node [
    id 457
    label "Neapol"
  ]
  node [
    id 458
    label "Frydek-Mistek"
  ]
  node [
    id 459
    label "W&#322;adywostok"
  ]
  node [
    id 460
    label "Calais"
  ]
  node [
    id 461
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 462
    label "Trydent"
  ]
  node [
    id 463
    label "Magnitogorsk"
  ]
  node [
    id 464
    label "Padwa"
  ]
  node [
    id 465
    label "Isfahan"
  ]
  node [
    id 466
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 467
    label "grupa"
  ]
  node [
    id 468
    label "Marburg"
  ]
  node [
    id 469
    label "Homel"
  ]
  node [
    id 470
    label "Boston"
  ]
  node [
    id 471
    label "W&#252;rzburg"
  ]
  node [
    id 472
    label "Antiochia"
  ]
  node [
    id 473
    label "Wotki&#324;sk"
  ]
  node [
    id 474
    label "A&#322;apajewsk"
  ]
  node [
    id 475
    label "Lejda"
  ]
  node [
    id 476
    label "Nieder_Selters"
  ]
  node [
    id 477
    label "Nicea"
  ]
  node [
    id 478
    label "Dmitrow"
  ]
  node [
    id 479
    label "Taganrog"
  ]
  node [
    id 480
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 481
    label "Nowomoskowsk"
  ]
  node [
    id 482
    label "Koby&#322;ka"
  ]
  node [
    id 483
    label "Iwano-Frankowsk"
  ]
  node [
    id 484
    label "Kis&#322;owodzk"
  ]
  node [
    id 485
    label "Tomsk"
  ]
  node [
    id 486
    label "Ferrara"
  ]
  node [
    id 487
    label "Edam"
  ]
  node [
    id 488
    label "Suworow"
  ]
  node [
    id 489
    label "Turka"
  ]
  node [
    id 490
    label "Aralsk"
  ]
  node [
    id 491
    label "Kobry&#324;"
  ]
  node [
    id 492
    label "Rotterdam"
  ]
  node [
    id 493
    label "Bordeaux"
  ]
  node [
    id 494
    label "L&#252;neburg"
  ]
  node [
    id 495
    label "Akwizgran"
  ]
  node [
    id 496
    label "Liverpool"
  ]
  node [
    id 497
    label "Asuan"
  ]
  node [
    id 498
    label "Bonn"
  ]
  node [
    id 499
    label "Teby"
  ]
  node [
    id 500
    label "Szumsk"
  ]
  node [
    id 501
    label "Ku&#378;nieck"
  ]
  node [
    id 502
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 503
    label "Tyberiada"
  ]
  node [
    id 504
    label "Turkiestan"
  ]
  node [
    id 505
    label "Nanning"
  ]
  node [
    id 506
    label "G&#322;uch&#243;w"
  ]
  node [
    id 507
    label "Bajonna"
  ]
  node [
    id 508
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 509
    label "Orze&#322;"
  ]
  node [
    id 510
    label "Opalenica"
  ]
  node [
    id 511
    label "Buczacz"
  ]
  node [
    id 512
    label "Armenia"
  ]
  node [
    id 513
    label "Nowoku&#378;nieck"
  ]
  node [
    id 514
    label "Wuppertal"
  ]
  node [
    id 515
    label "Wuhan"
  ]
  node [
    id 516
    label "Betlejem"
  ]
  node [
    id 517
    label "Wi&#322;komierz"
  ]
  node [
    id 518
    label "Podiebrady"
  ]
  node [
    id 519
    label "Rawenna"
  ]
  node [
    id 520
    label "Haarlem"
  ]
  node [
    id 521
    label "Woskriesiensk"
  ]
  node [
    id 522
    label "Pyskowice"
  ]
  node [
    id 523
    label "Kilonia"
  ]
  node [
    id 524
    label "Ruciane-Nida"
  ]
  node [
    id 525
    label "Kursk"
  ]
  node [
    id 526
    label "Wolgast"
  ]
  node [
    id 527
    label "Stralsund"
  ]
  node [
    id 528
    label "Sydon"
  ]
  node [
    id 529
    label "Natal"
  ]
  node [
    id 530
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 531
    label "Baranowicze"
  ]
  node [
    id 532
    label "Stara_Zagora"
  ]
  node [
    id 533
    label "Regensburg"
  ]
  node [
    id 534
    label "Kapsztad"
  ]
  node [
    id 535
    label "Kemerowo"
  ]
  node [
    id 536
    label "Mi&#347;nia"
  ]
  node [
    id 537
    label "Stary_Sambor"
  ]
  node [
    id 538
    label "Soligorsk"
  ]
  node [
    id 539
    label "Ostaszk&#243;w"
  ]
  node [
    id 540
    label "T&#322;uszcz"
  ]
  node [
    id 541
    label "Uljanowsk"
  ]
  node [
    id 542
    label "Tuluza"
  ]
  node [
    id 543
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 544
    label "Chicago"
  ]
  node [
    id 545
    label "Kamieniec_Podolski"
  ]
  node [
    id 546
    label "Dijon"
  ]
  node [
    id 547
    label "Siedliszcze"
  ]
  node [
    id 548
    label "Haga"
  ]
  node [
    id 549
    label "Bobrujsk"
  ]
  node [
    id 550
    label "Kokand"
  ]
  node [
    id 551
    label "Windsor"
  ]
  node [
    id 552
    label "Chmielnicki"
  ]
  node [
    id 553
    label "Winchester"
  ]
  node [
    id 554
    label "Bria&#324;sk"
  ]
  node [
    id 555
    label "Uppsala"
  ]
  node [
    id 556
    label "Paw&#322;odar"
  ]
  node [
    id 557
    label "Canterbury"
  ]
  node [
    id 558
    label "Omsk"
  ]
  node [
    id 559
    label "Tyr"
  ]
  node [
    id 560
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 561
    label "Kolonia"
  ]
  node [
    id 562
    label "Nowa_Ruda"
  ]
  node [
    id 563
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 564
    label "Czerkasy"
  ]
  node [
    id 565
    label "Budziszyn"
  ]
  node [
    id 566
    label "Rohatyn"
  ]
  node [
    id 567
    label "Nowogr&#243;dek"
  ]
  node [
    id 568
    label "Buda"
  ]
  node [
    id 569
    label "Zbara&#380;"
  ]
  node [
    id 570
    label "Korzec"
  ]
  node [
    id 571
    label "Medyna"
  ]
  node [
    id 572
    label "Piatigorsk"
  ]
  node [
    id 573
    label "Monako"
  ]
  node [
    id 574
    label "Chark&#243;w"
  ]
  node [
    id 575
    label "Zadar"
  ]
  node [
    id 576
    label "Brandenburg"
  ]
  node [
    id 577
    label "&#379;ytawa"
  ]
  node [
    id 578
    label "Konstantynopol"
  ]
  node [
    id 579
    label "Wismar"
  ]
  node [
    id 580
    label "Wielsk"
  ]
  node [
    id 581
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 582
    label "Genewa"
  ]
  node [
    id 583
    label "Merseburg"
  ]
  node [
    id 584
    label "Lozanna"
  ]
  node [
    id 585
    label "Azow"
  ]
  node [
    id 586
    label "K&#322;ajpeda"
  ]
  node [
    id 587
    label "Angarsk"
  ]
  node [
    id 588
    label "Ostrawa"
  ]
  node [
    id 589
    label "Jastarnia"
  ]
  node [
    id 590
    label "Moguncja"
  ]
  node [
    id 591
    label "Siewsk"
  ]
  node [
    id 592
    label "Pasawa"
  ]
  node [
    id 593
    label "Penza"
  ]
  node [
    id 594
    label "Borys&#322;aw"
  ]
  node [
    id 595
    label "Osaka"
  ]
  node [
    id 596
    label "Eupatoria"
  ]
  node [
    id 597
    label "Kalmar"
  ]
  node [
    id 598
    label "Troki"
  ]
  node [
    id 599
    label "Mosina"
  ]
  node [
    id 600
    label "Orany"
  ]
  node [
    id 601
    label "Zas&#322;aw"
  ]
  node [
    id 602
    label "Dobrodzie&#324;"
  ]
  node [
    id 603
    label "Kars"
  ]
  node [
    id 604
    label "Poprad"
  ]
  node [
    id 605
    label "Sajgon"
  ]
  node [
    id 606
    label "Tulon"
  ]
  node [
    id 607
    label "Kro&#347;niewice"
  ]
  node [
    id 608
    label "Krzywi&#324;"
  ]
  node [
    id 609
    label "Batumi"
  ]
  node [
    id 610
    label "Werona"
  ]
  node [
    id 611
    label "&#379;migr&#243;d"
  ]
  node [
    id 612
    label "Ka&#322;uga"
  ]
  node [
    id 613
    label "Rakoniewice"
  ]
  node [
    id 614
    label "Trabzon"
  ]
  node [
    id 615
    label "Debreczyn"
  ]
  node [
    id 616
    label "Jena"
  ]
  node [
    id 617
    label "Strzelno"
  ]
  node [
    id 618
    label "Gwardiejsk"
  ]
  node [
    id 619
    label "Wersal"
  ]
  node [
    id 620
    label "Bych&#243;w"
  ]
  node [
    id 621
    label "Ba&#322;tijsk"
  ]
  node [
    id 622
    label "Trenczyn"
  ]
  node [
    id 623
    label "Walencja"
  ]
  node [
    id 624
    label "Warna"
  ]
  node [
    id 625
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 626
    label "Huma&#324;"
  ]
  node [
    id 627
    label "Wilejka"
  ]
  node [
    id 628
    label "Ochryda"
  ]
  node [
    id 629
    label "Berdycz&#243;w"
  ]
  node [
    id 630
    label "Krasnogorsk"
  ]
  node [
    id 631
    label "Bogus&#322;aw"
  ]
  node [
    id 632
    label "Trzyniec"
  ]
  node [
    id 633
    label "urz&#261;d"
  ]
  node [
    id 634
    label "Mariampol"
  ]
  node [
    id 635
    label "Ko&#322;omna"
  ]
  node [
    id 636
    label "Chanty-Mansyjsk"
  ]
  node [
    id 637
    label "Piast&#243;w"
  ]
  node [
    id 638
    label "Jastrowie"
  ]
  node [
    id 639
    label "Nampula"
  ]
  node [
    id 640
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 641
    label "Bor"
  ]
  node [
    id 642
    label "Lengyel"
  ]
  node [
    id 643
    label "Lubecz"
  ]
  node [
    id 644
    label "Wierchoja&#324;sk"
  ]
  node [
    id 645
    label "Barczewo"
  ]
  node [
    id 646
    label "Madras"
  ]
  node [
    id 647
    label "stanowisko"
  ]
  node [
    id 648
    label "position"
  ]
  node [
    id 649
    label "instytucja"
  ]
  node [
    id 650
    label "siedziba"
  ]
  node [
    id 651
    label "organ"
  ]
  node [
    id 652
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 653
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 654
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 655
    label "mianowaniec"
  ]
  node [
    id 656
    label "dzia&#322;"
  ]
  node [
    id 657
    label "okienko"
  ]
  node [
    id 658
    label "w&#322;adza"
  ]
  node [
    id 659
    label "odm&#322;adzanie"
  ]
  node [
    id 660
    label "liga"
  ]
  node [
    id 661
    label "jednostka_systematyczna"
  ]
  node [
    id 662
    label "asymilowanie"
  ]
  node [
    id 663
    label "gromada"
  ]
  node [
    id 664
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 665
    label "asymilowa&#263;"
  ]
  node [
    id 666
    label "egzemplarz"
  ]
  node [
    id 667
    label "Entuzjastki"
  ]
  node [
    id 668
    label "zbi&#243;r"
  ]
  node [
    id 669
    label "kompozycja"
  ]
  node [
    id 670
    label "Terranie"
  ]
  node [
    id 671
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 672
    label "category"
  ]
  node [
    id 673
    label "pakiet_klimatyczny"
  ]
  node [
    id 674
    label "oddzia&#322;"
  ]
  node [
    id 675
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 676
    label "cz&#261;steczka"
  ]
  node [
    id 677
    label "stage_set"
  ]
  node [
    id 678
    label "type"
  ]
  node [
    id 679
    label "specgrupa"
  ]
  node [
    id 680
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 681
    label "&#346;wietliki"
  ]
  node [
    id 682
    label "odm&#322;odzenie"
  ]
  node [
    id 683
    label "Eurogrupa"
  ]
  node [
    id 684
    label "odm&#322;adza&#263;"
  ]
  node [
    id 685
    label "formacja_geologiczna"
  ]
  node [
    id 686
    label "harcerze_starsi"
  ]
  node [
    id 687
    label "Aurignac"
  ]
  node [
    id 688
    label "Sabaudia"
  ]
  node [
    id 689
    label "Cecora"
  ]
  node [
    id 690
    label "Saint-Acheul"
  ]
  node [
    id 691
    label "Boulogne"
  ]
  node [
    id 692
    label "Opat&#243;wek"
  ]
  node [
    id 693
    label "osiedle"
  ]
  node [
    id 694
    label "Levallois-Perret"
  ]
  node [
    id 695
    label "kompleks"
  ]
  node [
    id 696
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 697
    label "droga"
  ]
  node [
    id 698
    label "korona_drogi"
  ]
  node [
    id 699
    label "pas_rozdzielczy"
  ]
  node [
    id 700
    label "&#347;rodowisko"
  ]
  node [
    id 701
    label "streetball"
  ]
  node [
    id 702
    label "miasteczko"
  ]
  node [
    id 703
    label "pas_ruchu"
  ]
  node [
    id 704
    label "chodnik"
  ]
  node [
    id 705
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 706
    label "pierzeja"
  ]
  node [
    id 707
    label "wysepka"
  ]
  node [
    id 708
    label "arteria"
  ]
  node [
    id 709
    label "Broadway"
  ]
  node [
    id 710
    label "autostrada"
  ]
  node [
    id 711
    label "jezdnia"
  ]
  node [
    id 712
    label "Brenna"
  ]
  node [
    id 713
    label "Szwajcaria"
  ]
  node [
    id 714
    label "Rosja"
  ]
  node [
    id 715
    label "archidiecezja"
  ]
  node [
    id 716
    label "wirus"
  ]
  node [
    id 717
    label "gor&#261;czka_krwotoczna"
  ]
  node [
    id 718
    label "filowirusy"
  ]
  node [
    id 719
    label "Niemcy"
  ]
  node [
    id 720
    label "Swierd&#322;owsk"
  ]
  node [
    id 721
    label "Skierniewice"
  ]
  node [
    id 722
    label "Monaster"
  ]
  node [
    id 723
    label "edam"
  ]
  node [
    id 724
    label "mury_Jerycha"
  ]
  node [
    id 725
    label "Mozambik"
  ]
  node [
    id 726
    label "Francja"
  ]
  node [
    id 727
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 728
    label "dram"
  ]
  node [
    id 729
    label "Dunajec"
  ]
  node [
    id 730
    label "Tatry"
  ]
  node [
    id 731
    label "S&#261;decczyzna"
  ]
  node [
    id 732
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 733
    label "Stanis&#322;aw&#243;w"
  ]
  node [
    id 734
    label "Budapeszt"
  ]
  node [
    id 735
    label "Ukraina"
  ]
  node [
    id 736
    label "Dzikie_Pola"
  ]
  node [
    id 737
    label "Sicz"
  ]
  node [
    id 738
    label "Psie_Pole"
  ]
  node [
    id 739
    label "Frysztat"
  ]
  node [
    id 740
    label "Azerbejd&#380;an"
  ]
  node [
    id 741
    label "Prusy"
  ]
  node [
    id 742
    label "Budionowsk"
  ]
  node [
    id 743
    label "woda_kolo&#324;ska"
  ]
  node [
    id 744
    label "The_Beatles"
  ]
  node [
    id 745
    label "harcerstwo"
  ]
  node [
    id 746
    label "frank_monakijski"
  ]
  node [
    id 747
    label "euro"
  ]
  node [
    id 748
    label "&#321;otwa"
  ]
  node [
    id 749
    label "Litwa"
  ]
  node [
    id 750
    label "Hiszpania"
  ]
  node [
    id 751
    label "Stambu&#322;"
  ]
  node [
    id 752
    label "Bizancjum"
  ]
  node [
    id 753
    label "Kalinin"
  ]
  node [
    id 754
    label "&#321;yczak&#243;w"
  ]
  node [
    id 755
    label "obraz"
  ]
  node [
    id 756
    label "dzie&#322;o"
  ]
  node [
    id 757
    label "wagon"
  ]
  node [
    id 758
    label "bimba"
  ]
  node [
    id 759
    label "pojazd_szynowy"
  ]
  node [
    id 760
    label "odbierak"
  ]
  node [
    id 761
    label "&#347;rodek_transportu_publicznego"
  ]
  node [
    id 762
    label "ceklarz"
  ]
  node [
    id 763
    label "burmistrzyna"
  ]
  node [
    id 764
    label "podawa&#263;"
  ]
  node [
    id 765
    label "publikowa&#263;"
  ]
  node [
    id 766
    label "post"
  ]
  node [
    id 767
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 768
    label "announce"
  ]
  node [
    id 769
    label "tenis"
  ]
  node [
    id 770
    label "deal"
  ]
  node [
    id 771
    label "dawa&#263;"
  ]
  node [
    id 772
    label "stawia&#263;"
  ]
  node [
    id 773
    label "rozgrywa&#263;"
  ]
  node [
    id 774
    label "kelner"
  ]
  node [
    id 775
    label "siatk&#243;wka"
  ]
  node [
    id 776
    label "cover"
  ]
  node [
    id 777
    label "tender"
  ]
  node [
    id 778
    label "jedzenie"
  ]
  node [
    id 779
    label "faszerowa&#263;"
  ]
  node [
    id 780
    label "introduce"
  ]
  node [
    id 781
    label "informowa&#263;"
  ]
  node [
    id 782
    label "serwowa&#263;"
  ]
  node [
    id 783
    label "hail"
  ]
  node [
    id 784
    label "komunikowa&#263;"
  ]
  node [
    id 785
    label "okre&#347;la&#263;"
  ]
  node [
    id 786
    label "upublicznia&#263;"
  ]
  node [
    id 787
    label "give"
  ]
  node [
    id 788
    label "wydawnictwo"
  ]
  node [
    id 789
    label "wprowadza&#263;"
  ]
  node [
    id 790
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 791
    label "zachowanie"
  ]
  node [
    id 792
    label "zachowywanie"
  ]
  node [
    id 793
    label "rok_ko&#347;cielny"
  ]
  node [
    id 794
    label "tekst"
  ]
  node [
    id 795
    label "czas"
  ]
  node [
    id 796
    label "praktyka"
  ]
  node [
    id 797
    label "zachowa&#263;"
  ]
  node [
    id 798
    label "zachowywa&#263;"
  ]
  node [
    id 799
    label "otworzysty"
  ]
  node [
    id 800
    label "aktywny"
  ]
  node [
    id 801
    label "nieograniczony"
  ]
  node [
    id 802
    label "zdecydowany"
  ]
  node [
    id 803
    label "prostoduszny"
  ]
  node [
    id 804
    label "jawnie"
  ]
  node [
    id 805
    label "bezpo&#347;redni"
  ]
  node [
    id 806
    label "aktualny"
  ]
  node [
    id 807
    label "kontaktowy"
  ]
  node [
    id 808
    label "otwarcie"
  ]
  node [
    id 809
    label "ewidentny"
  ]
  node [
    id 810
    label "dost&#281;pny"
  ]
  node [
    id 811
    label "gotowy"
  ]
  node [
    id 812
    label "upublicznianie"
  ]
  node [
    id 813
    label "jawny"
  ]
  node [
    id 814
    label "upublicznienie"
  ]
  node [
    id 815
    label "publicznie"
  ]
  node [
    id 816
    label "oczywisty"
  ]
  node [
    id 817
    label "pewny"
  ]
  node [
    id 818
    label "wyra&#378;ny"
  ]
  node [
    id 819
    label "ewidentnie"
  ]
  node [
    id 820
    label "jednoznaczny"
  ]
  node [
    id 821
    label "zdecydowanie"
  ]
  node [
    id 822
    label "zauwa&#380;alny"
  ]
  node [
    id 823
    label "dowolny"
  ]
  node [
    id 824
    label "przestrze&#324;"
  ]
  node [
    id 825
    label "rozleg&#322;y"
  ]
  node [
    id 826
    label "nieograniczenie"
  ]
  node [
    id 827
    label "mo&#380;liwy"
  ]
  node [
    id 828
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 829
    label "odblokowanie_si&#281;"
  ]
  node [
    id 830
    label "zrozumia&#322;y"
  ]
  node [
    id 831
    label "dost&#281;pnie"
  ]
  node [
    id 832
    label "&#322;atwy"
  ]
  node [
    id 833
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 834
    label "przyst&#281;pnie"
  ]
  node [
    id 835
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 836
    label "bliski"
  ]
  node [
    id 837
    label "bezpo&#347;rednio"
  ]
  node [
    id 838
    label "szczery"
  ]
  node [
    id 839
    label "nietrze&#378;wy"
  ]
  node [
    id 840
    label "czekanie"
  ]
  node [
    id 841
    label "martwy"
  ]
  node [
    id 842
    label "m&#243;c"
  ]
  node [
    id 843
    label "gotowo"
  ]
  node [
    id 844
    label "przygotowanie"
  ]
  node [
    id 845
    label "przygotowywanie"
  ]
  node [
    id 846
    label "dyspozycyjny"
  ]
  node [
    id 847
    label "zalany"
  ]
  node [
    id 848
    label "nieuchronny"
  ]
  node [
    id 849
    label "doj&#347;cie"
  ]
  node [
    id 850
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 851
    label "aktualnie"
  ]
  node [
    id 852
    label "wa&#380;ny"
  ]
  node [
    id 853
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 854
    label "aktualizowanie"
  ]
  node [
    id 855
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 856
    label "uaktualnienie"
  ]
  node [
    id 857
    label "intensywny"
  ]
  node [
    id 858
    label "ciekawy"
  ]
  node [
    id 859
    label "realny"
  ]
  node [
    id 860
    label "dzia&#322;anie"
  ]
  node [
    id 861
    label "zdolny"
  ]
  node [
    id 862
    label "czynnie"
  ]
  node [
    id 863
    label "czynny"
  ]
  node [
    id 864
    label "uczynnianie"
  ]
  node [
    id 865
    label "aktywnie"
  ]
  node [
    id 866
    label "istotny"
  ]
  node [
    id 867
    label "faktyczny"
  ]
  node [
    id 868
    label "uczynnienie"
  ]
  node [
    id 869
    label "prostodusznie"
  ]
  node [
    id 870
    label "przyst&#281;pny"
  ]
  node [
    id 871
    label "kontaktowo"
  ]
  node [
    id 872
    label "jawno"
  ]
  node [
    id 873
    label "rozpocz&#281;cie"
  ]
  node [
    id 874
    label "udost&#281;pnienie"
  ]
  node [
    id 875
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 876
    label "opening"
  ]
  node [
    id 877
    label "gra&#263;"
  ]
  node [
    id 878
    label "czynno&#347;&#263;"
  ]
  node [
    id 879
    label "granie"
  ]
  node [
    id 880
    label "casting"
  ]
  node [
    id 881
    label "nab&#243;r"
  ]
  node [
    id 882
    label "Eurowizja"
  ]
  node [
    id 883
    label "eliminacje"
  ]
  node [
    id 884
    label "impreza"
  ]
  node [
    id 885
    label "emulation"
  ]
  node [
    id 886
    label "Interwizja"
  ]
  node [
    id 887
    label "impra"
  ]
  node [
    id 888
    label "rozrywka"
  ]
  node [
    id 889
    label "przyj&#281;cie"
  ]
  node [
    id 890
    label "okazja"
  ]
  node [
    id 891
    label "party"
  ]
  node [
    id 892
    label "recruitment"
  ]
  node [
    id 893
    label "wyb&#243;r"
  ]
  node [
    id 894
    label "faza"
  ]
  node [
    id 895
    label "runda"
  ]
  node [
    id 896
    label "turniej"
  ]
  node [
    id 897
    label "retirement"
  ]
  node [
    id 898
    label "przes&#322;uchanie"
  ]
  node [
    id 899
    label "w&#281;dkarstwo"
  ]
  node [
    id 900
    label "offer"
  ]
  node [
    id 901
    label "propozycja"
  ]
  node [
    id 902
    label "proposal"
  ]
  node [
    id 903
    label "pomys&#322;"
  ]
  node [
    id 904
    label "ozdabia&#263;"
  ]
  node [
    id 905
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 906
    label "trim"
  ]
  node [
    id 907
    label "klasztor"
  ]
  node [
    id 908
    label "pomieszczenie"
  ]
  node [
    id 909
    label "amfilada"
  ]
  node [
    id 910
    label "front"
  ]
  node [
    id 911
    label "apartment"
  ]
  node [
    id 912
    label "pod&#322;oga"
  ]
  node [
    id 913
    label "miejsce"
  ]
  node [
    id 914
    label "sklepienie"
  ]
  node [
    id 915
    label "sufit"
  ]
  node [
    id 916
    label "umieszczenie"
  ]
  node [
    id 917
    label "zakamarek"
  ]
  node [
    id 918
    label "wirydarz"
  ]
  node [
    id 919
    label "kustodia"
  ]
  node [
    id 920
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 921
    label "zakon"
  ]
  node [
    id 922
    label "refektarz"
  ]
  node [
    id 923
    label "kapitularz"
  ]
  node [
    id 924
    label "oratorium"
  ]
  node [
    id 925
    label "&#321;agiewniki"
  ]
  node [
    id 926
    label "danie"
  ]
  node [
    id 927
    label "confession"
  ]
  node [
    id 928
    label "stwierdzenie"
  ]
  node [
    id 929
    label "recognition"
  ]
  node [
    id 930
    label "oznajmienie"
  ]
  node [
    id 931
    label "obiecanie"
  ]
  node [
    id 932
    label "zap&#322;acenie"
  ]
  node [
    id 933
    label "cios"
  ]
  node [
    id 934
    label "rendition"
  ]
  node [
    id 935
    label "wymienienie_si&#281;"
  ]
  node [
    id 936
    label "eating"
  ]
  node [
    id 937
    label "coup"
  ]
  node [
    id 938
    label "hand"
  ]
  node [
    id 939
    label "uprawianie_seksu"
  ]
  node [
    id 940
    label "allow"
  ]
  node [
    id 941
    label "dostarczenie"
  ]
  node [
    id 942
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 943
    label "uderzenie"
  ]
  node [
    id 944
    label "zadanie"
  ]
  node [
    id 945
    label "powierzenie"
  ]
  node [
    id 946
    label "przeznaczenie"
  ]
  node [
    id 947
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 948
    label "przekazanie"
  ]
  node [
    id 949
    label "odst&#261;pienie"
  ]
  node [
    id 950
    label "dodanie"
  ]
  node [
    id 951
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 952
    label "wyposa&#380;enie"
  ]
  node [
    id 953
    label "dostanie"
  ]
  node [
    id 954
    label "karta"
  ]
  node [
    id 955
    label "potrawa"
  ]
  node [
    id 956
    label "pass"
  ]
  node [
    id 957
    label "menu"
  ]
  node [
    id 958
    label "uderzanie"
  ]
  node [
    id 959
    label "wyst&#261;pienie"
  ]
  node [
    id 960
    label "wyposa&#380;anie"
  ]
  node [
    id 961
    label "pobicie"
  ]
  node [
    id 962
    label "posi&#322;ek"
  ]
  node [
    id 963
    label "urz&#261;dzenie"
  ]
  node [
    id 964
    label "zrobienie"
  ]
  node [
    id 965
    label "wypowied&#378;"
  ]
  node [
    id 966
    label "ustalenie"
  ]
  node [
    id 967
    label "claim"
  ]
  node [
    id 968
    label "statement"
  ]
  node [
    id 969
    label "wypowiedzenie"
  ]
  node [
    id 970
    label "manifesto"
  ]
  node [
    id 971
    label "zwiastowanie"
  ]
  node [
    id 972
    label "announcement"
  ]
  node [
    id 973
    label "apel"
  ]
  node [
    id 974
    label "Manifest_lipcowy"
  ]
  node [
    id 975
    label "poinformowanie"
  ]
  node [
    id 976
    label "dop&#322;ata"
  ]
  node [
    id 977
    label "doch&#243;d"
  ]
  node [
    id 978
    label "dochodzenie"
  ]
  node [
    id 979
    label "doj&#347;&#263;"
  ]
  node [
    id 980
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 981
    label "byt"
  ]
  node [
    id 982
    label "osobowo&#347;&#263;"
  ]
  node [
    id 983
    label "organizacja"
  ]
  node [
    id 984
    label "prawo"
  ]
  node [
    id 985
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 986
    label "nauka_prawa"
  ]
  node [
    id 987
    label "ludzko&#347;&#263;"
  ]
  node [
    id 988
    label "wapniak"
  ]
  node [
    id 989
    label "os&#322;abia&#263;"
  ]
  node [
    id 990
    label "posta&#263;"
  ]
  node [
    id 991
    label "hominid"
  ]
  node [
    id 992
    label "podw&#322;adny"
  ]
  node [
    id 993
    label "os&#322;abianie"
  ]
  node [
    id 994
    label "g&#322;owa"
  ]
  node [
    id 995
    label "figura"
  ]
  node [
    id 996
    label "portrecista"
  ]
  node [
    id 997
    label "dwun&#243;g"
  ]
  node [
    id 998
    label "profanum"
  ]
  node [
    id 999
    label "mikrokosmos"
  ]
  node [
    id 1000
    label "nasada"
  ]
  node [
    id 1001
    label "duch"
  ]
  node [
    id 1002
    label "antropochoria"
  ]
  node [
    id 1003
    label "osoba"
  ]
  node [
    id 1004
    label "wz&#243;r"
  ]
  node [
    id 1005
    label "senior"
  ]
  node [
    id 1006
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1007
    label "Adam"
  ]
  node [
    id 1008
    label "homo_sapiens"
  ]
  node [
    id 1009
    label "polifag"
  ]
  node [
    id 1010
    label "utrzymywanie"
  ]
  node [
    id 1011
    label "bycie"
  ]
  node [
    id 1012
    label "entity"
  ]
  node [
    id 1013
    label "subsystencja"
  ]
  node [
    id 1014
    label "utrzyma&#263;"
  ]
  node [
    id 1015
    label "egzystencja"
  ]
  node [
    id 1016
    label "wy&#380;ywienie"
  ]
  node [
    id 1017
    label "ontologicznie"
  ]
  node [
    id 1018
    label "utrzymanie"
  ]
  node [
    id 1019
    label "potencja"
  ]
  node [
    id 1020
    label "utrzymywa&#263;"
  ]
  node [
    id 1021
    label "jednostka_organizacyjna"
  ]
  node [
    id 1022
    label "struktura"
  ]
  node [
    id 1023
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1024
    label "TOPR"
  ]
  node [
    id 1025
    label "endecki"
  ]
  node [
    id 1026
    label "zesp&#243;&#322;"
  ]
  node [
    id 1027
    label "od&#322;am"
  ]
  node [
    id 1028
    label "przedstawicielstwo"
  ]
  node [
    id 1029
    label "Cepelia"
  ]
  node [
    id 1030
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1031
    label "ZBoWiD"
  ]
  node [
    id 1032
    label "organization"
  ]
  node [
    id 1033
    label "centrala"
  ]
  node [
    id 1034
    label "GOPR"
  ]
  node [
    id 1035
    label "ZOMO"
  ]
  node [
    id 1036
    label "ZMP"
  ]
  node [
    id 1037
    label "komitet_koordynacyjny"
  ]
  node [
    id 1038
    label "przybud&#243;wka"
  ]
  node [
    id 1039
    label "boj&#243;wka"
  ]
  node [
    id 1040
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1041
    label "superego"
  ]
  node [
    id 1042
    label "wyj&#261;tkowy"
  ]
  node [
    id 1043
    label "psychika"
  ]
  node [
    id 1044
    label "charakter"
  ]
  node [
    id 1045
    label "wn&#281;trze"
  ]
  node [
    id 1046
    label "cecha"
  ]
  node [
    id 1047
    label "self"
  ]
  node [
    id 1048
    label "status"
  ]
  node [
    id 1049
    label "umocowa&#263;"
  ]
  node [
    id 1050
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1051
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1052
    label "procesualistyka"
  ]
  node [
    id 1053
    label "regu&#322;a_Allena"
  ]
  node [
    id 1054
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1055
    label "kryminalistyka"
  ]
  node [
    id 1056
    label "szko&#322;a"
  ]
  node [
    id 1057
    label "kierunek"
  ]
  node [
    id 1058
    label "zasada_d'Alemberta"
  ]
  node [
    id 1059
    label "obserwacja"
  ]
  node [
    id 1060
    label "normatywizm"
  ]
  node [
    id 1061
    label "jurisprudence"
  ]
  node [
    id 1062
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 1063
    label "kultura_duchowa"
  ]
  node [
    id 1064
    label "przepis"
  ]
  node [
    id 1065
    label "prawo_karne_procesowe"
  ]
  node [
    id 1066
    label "criterion"
  ]
  node [
    id 1067
    label "kazuistyka"
  ]
  node [
    id 1068
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1069
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 1070
    label "kryminologia"
  ]
  node [
    id 1071
    label "opis"
  ]
  node [
    id 1072
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1073
    label "prawo_Mendla"
  ]
  node [
    id 1074
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1075
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 1076
    label "prawo_karne"
  ]
  node [
    id 1077
    label "legislacyjnie"
  ]
  node [
    id 1078
    label "twierdzenie"
  ]
  node [
    id 1079
    label "cywilistyka"
  ]
  node [
    id 1080
    label "judykatura"
  ]
  node [
    id 1081
    label "kanonistyka"
  ]
  node [
    id 1082
    label "standard"
  ]
  node [
    id 1083
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1084
    label "law"
  ]
  node [
    id 1085
    label "qualification"
  ]
  node [
    id 1086
    label "dominion"
  ]
  node [
    id 1087
    label "wykonawczy"
  ]
  node [
    id 1088
    label "zasada"
  ]
  node [
    id 1089
    label "normalizacja"
  ]
  node [
    id 1090
    label "absolutorium"
  ]
  node [
    id 1091
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1092
    label "activity"
  ]
  node [
    id 1093
    label "infimum"
  ]
  node [
    id 1094
    label "powodowanie"
  ]
  node [
    id 1095
    label "liczenie"
  ]
  node [
    id 1096
    label "skutek"
  ]
  node [
    id 1097
    label "podzia&#322;anie"
  ]
  node [
    id 1098
    label "supremum"
  ]
  node [
    id 1099
    label "kampania"
  ]
  node [
    id 1100
    label "uruchamianie"
  ]
  node [
    id 1101
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1102
    label "operacja"
  ]
  node [
    id 1103
    label "jednostka"
  ]
  node [
    id 1104
    label "hipnotyzowanie"
  ]
  node [
    id 1105
    label "robienie"
  ]
  node [
    id 1106
    label "uruchomienie"
  ]
  node [
    id 1107
    label "nakr&#281;canie"
  ]
  node [
    id 1108
    label "matematyka"
  ]
  node [
    id 1109
    label "reakcja_chemiczna"
  ]
  node [
    id 1110
    label "tr&#243;jstronny"
  ]
  node [
    id 1111
    label "natural_process"
  ]
  node [
    id 1112
    label "nakr&#281;cenie"
  ]
  node [
    id 1113
    label "zatrzymanie"
  ]
  node [
    id 1114
    label "wp&#322;yw"
  ]
  node [
    id 1115
    label "rzut"
  ]
  node [
    id 1116
    label "podtrzymywanie"
  ]
  node [
    id 1117
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1118
    label "liczy&#263;"
  ]
  node [
    id 1119
    label "operation"
  ]
  node [
    id 1120
    label "rezultat"
  ]
  node [
    id 1121
    label "dzianie_si&#281;"
  ]
  node [
    id 1122
    label "zadzia&#322;anie"
  ]
  node [
    id 1123
    label "priorytet"
  ]
  node [
    id 1124
    label "kres"
  ]
  node [
    id 1125
    label "docieranie"
  ]
  node [
    id 1126
    label "funkcja"
  ]
  node [
    id 1127
    label "impact"
  ]
  node [
    id 1128
    label "zako&#324;czenie"
  ]
  node [
    id 1129
    label "act"
  ]
  node [
    id 1130
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1131
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1132
    label "graduation"
  ]
  node [
    id 1133
    label "uko&#324;czenie"
  ]
  node [
    id 1134
    label "kapita&#322;"
  ]
  node [
    id 1135
    label "ocena"
  ]
  node [
    id 1136
    label "surowiec"
  ]
  node [
    id 1137
    label "spad&#378;"
  ]
  node [
    id 1138
    label "zaleta"
  ]
  node [
    id 1139
    label "korzy&#347;&#263;"
  ]
  node [
    id 1140
    label "dobro"
  ]
  node [
    id 1141
    label "py&#322;ek"
  ]
  node [
    id 1142
    label "nektar"
  ]
  node [
    id 1143
    label "warto&#347;&#263;"
  ]
  node [
    id 1144
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 1145
    label "dobro&#263;"
  ]
  node [
    id 1146
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1147
    label "krzywa_Engla"
  ]
  node [
    id 1148
    label "cel"
  ]
  node [
    id 1149
    label "dobra"
  ]
  node [
    id 1150
    label "go&#322;&#261;bek"
  ]
  node [
    id 1151
    label "despond"
  ]
  node [
    id 1152
    label "litera"
  ]
  node [
    id 1153
    label "kalokagatia"
  ]
  node [
    id 1154
    label "rzecz"
  ]
  node [
    id 1155
    label "g&#322;agolica"
  ]
  node [
    id 1156
    label "sk&#322;adnik"
  ]
  node [
    id 1157
    label "tworzywo"
  ]
  node [
    id 1158
    label "zrewaluowa&#263;"
  ]
  node [
    id 1159
    label "rewaluowanie"
  ]
  node [
    id 1160
    label "zrewaluowanie"
  ]
  node [
    id 1161
    label "rewaluowa&#263;"
  ]
  node [
    id 1162
    label "wabik"
  ]
  node [
    id 1163
    label "strona"
  ]
  node [
    id 1164
    label "amrita"
  ]
  node [
    id 1165
    label "sok"
  ]
  node [
    id 1166
    label "mityczny"
  ]
  node [
    id 1167
    label "nap&#243;j"
  ]
  node [
    id 1168
    label "s&#322;odycz"
  ]
  node [
    id 1169
    label "ciecz"
  ]
  node [
    id 1170
    label "znami&#281;"
  ]
  node [
    id 1171
    label "pylnik"
  ]
  node [
    id 1172
    label "egzyna"
  ]
  node [
    id 1173
    label "py&#322;"
  ]
  node [
    id 1174
    label "drobina"
  ]
  node [
    id 1175
    label "paproch"
  ]
  node [
    id 1176
    label "spot"
  ]
  node [
    id 1177
    label "melitofag"
  ]
  node [
    id 1178
    label "udost&#281;pnianie"
  ]
  node [
    id 1179
    label "ujawnienie_si&#281;"
  ]
  node [
    id 1180
    label "ujawnianie_si&#281;"
  ]
  node [
    id 1181
    label "znajomy"
  ]
  node [
    id 1182
    label "ujawnienie"
  ]
  node [
    id 1183
    label "ujawnianie"
  ]
  node [
    id 1184
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 1185
    label "&#347;rodek"
  ]
  node [
    id 1186
    label "oparcie"
  ]
  node [
    id 1187
    label "darowizna"
  ]
  node [
    id 1188
    label "zapomoga"
  ]
  node [
    id 1189
    label "comfort"
  ]
  node [
    id 1190
    label "pocieszenie"
  ]
  node [
    id 1191
    label "telefon_zaufania"
  ]
  node [
    id 1192
    label "dar"
  ]
  node [
    id 1193
    label "support"
  ]
  node [
    id 1194
    label "u&#322;atwienie"
  ]
  node [
    id 1195
    label "pomoc"
  ]
  node [
    id 1196
    label "income"
  ]
  node [
    id 1197
    label "stopa_procentowa"
  ]
  node [
    id 1198
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1199
    label "dyspozycja"
  ]
  node [
    id 1200
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1201
    label "da&#324;"
  ]
  node [
    id 1202
    label "faculty"
  ]
  node [
    id 1203
    label "stygmat"
  ]
  node [
    id 1204
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1205
    label "przedmiot"
  ]
  node [
    id 1206
    label "pomocnik"
  ]
  node [
    id 1207
    label "zgodzi&#263;"
  ]
  node [
    id 1208
    label "property"
  ]
  node [
    id 1209
    label "ukojenie"
  ]
  node [
    id 1210
    label "pomo&#380;enie"
  ]
  node [
    id 1211
    label "facilitation"
  ]
  node [
    id 1212
    label "ulepszenie"
  ]
  node [
    id 1213
    label "punkt"
  ]
  node [
    id 1214
    label "spos&#243;b"
  ]
  node [
    id 1215
    label "abstrakcja"
  ]
  node [
    id 1216
    label "chemikalia"
  ]
  node [
    id 1217
    label "substancja"
  ]
  node [
    id 1218
    label "ustawienie"
  ]
  node [
    id 1219
    label "back"
  ]
  node [
    id 1220
    label "podpora"
  ]
  node [
    id 1221
    label "anchor"
  ]
  node [
    id 1222
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1223
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 1224
    label "podstawa"
  ]
  node [
    id 1225
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1226
    label "przeniesienie_praw"
  ]
  node [
    id 1227
    label "transakcja"
  ]
  node [
    id 1228
    label "wykonawca"
  ]
  node [
    id 1229
    label "interpretator"
  ]
  node [
    id 1230
    label "dysponowanie"
  ]
  node [
    id 1231
    label "sterowanie"
  ]
  node [
    id 1232
    label "management"
  ]
  node [
    id 1233
    label "kierowanie"
  ]
  node [
    id 1234
    label "ukierunkowywanie"
  ]
  node [
    id 1235
    label "przywodzenie"
  ]
  node [
    id 1236
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 1237
    label "doprowadzanie"
  ]
  node [
    id 1238
    label "kre&#347;lenie"
  ]
  node [
    id 1239
    label "lead"
  ]
  node [
    id 1240
    label "krzywa"
  ]
  node [
    id 1241
    label "eksponowanie"
  ]
  node [
    id 1242
    label "linia_melodyczna"
  ]
  node [
    id 1243
    label "prowadzi&#263;"
  ]
  node [
    id 1244
    label "prowadzanie"
  ]
  node [
    id 1245
    label "wprowadzanie"
  ]
  node [
    id 1246
    label "doprowadzenie"
  ]
  node [
    id 1247
    label "poprowadzenie"
  ]
  node [
    id 1248
    label "kszta&#322;towanie"
  ]
  node [
    id 1249
    label "aim"
  ]
  node [
    id 1250
    label "zwracanie"
  ]
  node [
    id 1251
    label "przecinanie"
  ]
  node [
    id 1252
    label "ta&#324;czenie"
  ]
  node [
    id 1253
    label "przewy&#380;szanie"
  ]
  node [
    id 1254
    label "g&#243;rowanie"
  ]
  node [
    id 1255
    label "zaprowadzanie"
  ]
  node [
    id 1256
    label "dawanie"
  ]
  node [
    id 1257
    label "trzymanie"
  ]
  node [
    id 1258
    label "oprowadzanie"
  ]
  node [
    id 1259
    label "wprowadzenie"
  ]
  node [
    id 1260
    label "drive"
  ]
  node [
    id 1261
    label "oprowadzenie"
  ]
  node [
    id 1262
    label "przeci&#281;cie"
  ]
  node [
    id 1263
    label "przeci&#261;ganie"
  ]
  node [
    id 1264
    label "pozarz&#261;dzanie"
  ]
  node [
    id 1265
    label "uniewa&#380;nianie"
  ]
  node [
    id 1266
    label "skre&#347;lanie"
  ]
  node [
    id 1267
    label "pokre&#347;lenie"
  ]
  node [
    id 1268
    label "anointing"
  ]
  node [
    id 1269
    label "usuwanie"
  ]
  node [
    id 1270
    label "sporz&#261;dzanie"
  ]
  node [
    id 1271
    label "opowiadanie"
  ]
  node [
    id 1272
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1273
    label "rozdysponowywanie"
  ]
  node [
    id 1274
    label "zarz&#261;dzanie"
  ]
  node [
    id 1275
    label "namaszczenie_chorych"
  ]
  node [
    id 1276
    label "disposal"
  ]
  node [
    id 1277
    label "skazany"
  ]
  node [
    id 1278
    label "rozporz&#261;dzanie"
  ]
  node [
    id 1279
    label "fabrication"
  ]
  node [
    id 1280
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1281
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1282
    label "creation"
  ]
  node [
    id 1283
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1284
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1285
    label "porobienie"
  ]
  node [
    id 1286
    label "tentegowanie"
  ]
  node [
    id 1287
    label "bezproblemowy"
  ]
  node [
    id 1288
    label "wydarzenie"
  ]
  node [
    id 1289
    label "radiation"
  ]
  node [
    id 1290
    label "podkre&#347;lanie"
  ]
  node [
    id 1291
    label "uwydatnianie_si&#281;"
  ]
  node [
    id 1292
    label "demonstrowanie"
  ]
  node [
    id 1293
    label "napromieniowywanie"
  ]
  node [
    id 1294
    label "przeorientowanie"
  ]
  node [
    id 1295
    label "orientation"
  ]
  node [
    id 1296
    label "oznaczanie"
  ]
  node [
    id 1297
    label "strike"
  ]
  node [
    id 1298
    label "dominance"
  ]
  node [
    id 1299
    label "lepszy"
  ]
  node [
    id 1300
    label "wygrywanie"
  ]
  node [
    id 1301
    label "control"
  ]
  node [
    id 1302
    label "obs&#322;ugiwanie"
  ]
  node [
    id 1303
    label "przesterowanie"
  ]
  node [
    id 1304
    label "steering"
  ]
  node [
    id 1305
    label "haftowanie"
  ]
  node [
    id 1306
    label "vomit"
  ]
  node [
    id 1307
    label "przeznaczanie"
  ]
  node [
    id 1308
    label "przekazywanie"
  ]
  node [
    id 1309
    label "powracanie"
  ]
  node [
    id 1310
    label "ustawianie"
  ]
  node [
    id 1311
    label "wydalanie"
  ]
  node [
    id 1312
    label "supply"
  ]
  node [
    id 1313
    label "znajdowanie_si&#281;"
  ]
  node [
    id 1314
    label "spe&#322;nianie"
  ]
  node [
    id 1315
    label "provision"
  ]
  node [
    id 1316
    label "wzbudzanie"
  ]
  node [
    id 1317
    label "montowanie"
  ]
  node [
    id 1318
    label "formation"
  ]
  node [
    id 1319
    label "rozwijanie"
  ]
  node [
    id 1320
    label "training"
  ]
  node [
    id 1321
    label "cause"
  ]
  node [
    id 1322
    label "causal_agent"
  ]
  node [
    id 1323
    label "wysy&#322;anie"
  ]
  node [
    id 1324
    label "wyre&#380;yserowanie"
  ]
  node [
    id 1325
    label "re&#380;yserowanie"
  ]
  node [
    id 1326
    label "nakierowanie_si&#281;"
  ]
  node [
    id 1327
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1328
    label "figura_geometryczna"
  ]
  node [
    id 1329
    label "linia"
  ]
  node [
    id 1330
    label "poprowadzi&#263;"
  ]
  node [
    id 1331
    label "curvature"
  ]
  node [
    id 1332
    label "curve"
  ]
  node [
    id 1333
    label "sterczenie"
  ]
  node [
    id 1334
    label "&#380;y&#263;"
  ]
  node [
    id 1335
    label "robi&#263;"
  ]
  node [
    id 1336
    label "g&#243;rowa&#263;"
  ]
  node [
    id 1337
    label "tworzy&#263;"
  ]
  node [
    id 1338
    label "string"
  ]
  node [
    id 1339
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1340
    label "ukierunkowywa&#263;"
  ]
  node [
    id 1341
    label "sterowa&#263;"
  ]
  node [
    id 1342
    label "kre&#347;li&#263;"
  ]
  node [
    id 1343
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 1344
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1345
    label "message"
  ]
  node [
    id 1346
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1347
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 1348
    label "eksponowa&#263;"
  ]
  node [
    id 1349
    label "navigate"
  ]
  node [
    id 1350
    label "manipulate"
  ]
  node [
    id 1351
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 1352
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1353
    label "przesuwa&#263;"
  ]
  node [
    id 1354
    label "partner"
  ]
  node [
    id 1355
    label "powodowa&#263;"
  ]
  node [
    id 1356
    label "artyku&#322;"
  ]
  node [
    id 1357
    label "akapit"
  ]
  node [
    id 1358
    label "p&#322;acenie"
  ]
  node [
    id 1359
    label "umo&#380;liwianie"
  ]
  node [
    id 1360
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 1361
    label "puszczanie_si&#281;"
  ]
  node [
    id 1362
    label "&#322;adowanie"
  ]
  node [
    id 1363
    label "zezwalanie"
  ]
  node [
    id 1364
    label "nalewanie"
  ]
  node [
    id 1365
    label "communication"
  ]
  node [
    id 1366
    label "urz&#261;dzanie"
  ]
  node [
    id 1367
    label "wyst&#281;powanie"
  ]
  node [
    id 1368
    label "dodawanie"
  ]
  node [
    id 1369
    label "giving"
  ]
  node [
    id 1370
    label "pra&#380;enie"
  ]
  node [
    id 1371
    label "powierzanie"
  ]
  node [
    id 1372
    label "emission"
  ]
  node [
    id 1373
    label "dostarczanie"
  ]
  node [
    id 1374
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 1375
    label "obiecywanie"
  ]
  node [
    id 1376
    label "odst&#281;powanie"
  ]
  node [
    id 1377
    label "administration"
  ]
  node [
    id 1378
    label "wymienianie_si&#281;"
  ]
  node [
    id 1379
    label "bycie_w_posiadaniu"
  ]
  node [
    id 1380
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1381
    label "pr&#243;bowanie"
  ]
  node [
    id 1382
    label "instrumentalizacja"
  ]
  node [
    id 1383
    label "wykonywanie"
  ]
  node [
    id 1384
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 1385
    label "pasowanie"
  ]
  node [
    id 1386
    label "staranie_si&#281;"
  ]
  node [
    id 1387
    label "wybijanie"
  ]
  node [
    id 1388
    label "odegranie_si&#281;"
  ]
  node [
    id 1389
    label "instrument_muzyczny"
  ]
  node [
    id 1390
    label "dogrywanie"
  ]
  node [
    id 1391
    label "rozgrywanie"
  ]
  node [
    id 1392
    label "grywanie"
  ]
  node [
    id 1393
    label "przygrywanie"
  ]
  node [
    id 1394
    label "lewa"
  ]
  node [
    id 1395
    label "zwalczenie"
  ]
  node [
    id 1396
    label "gra_w_karty"
  ]
  node [
    id 1397
    label "mienienie_si&#281;"
  ]
  node [
    id 1398
    label "wydawanie"
  ]
  node [
    id 1399
    label "pretense"
  ]
  node [
    id 1400
    label "prezentowanie"
  ]
  node [
    id 1401
    label "na&#347;ladowanie"
  ]
  node [
    id 1402
    label "dogranie"
  ]
  node [
    id 1403
    label "wybicie"
  ]
  node [
    id 1404
    label "playing"
  ]
  node [
    id 1405
    label "rozegranie_si&#281;"
  ]
  node [
    id 1406
    label "ust&#281;powanie"
  ]
  node [
    id 1407
    label "glitter"
  ]
  node [
    id 1408
    label "igranie"
  ]
  node [
    id 1409
    label "odgrywanie_si&#281;"
  ]
  node [
    id 1410
    label "pogranie"
  ]
  node [
    id 1411
    label "wyr&#243;wnywanie"
  ]
  node [
    id 1412
    label "szczekanie"
  ]
  node [
    id 1413
    label "brzmienie"
  ]
  node [
    id 1414
    label "przedstawianie"
  ]
  node [
    id 1415
    label "wyr&#243;wnanie"
  ]
  node [
    id 1416
    label "rola"
  ]
  node [
    id 1417
    label "nagranie_si&#281;"
  ]
  node [
    id 1418
    label "migotanie"
  ]
  node [
    id 1419
    label "&#347;ciganie"
  ]
  node [
    id 1420
    label "nakre&#347;lenie"
  ]
  node [
    id 1421
    label "guidance"
  ]
  node [
    id 1422
    label "proverb"
  ]
  node [
    id 1423
    label "wytyczenie"
  ]
  node [
    id 1424
    label "gibanie"
  ]
  node [
    id 1425
    label "przeta&#324;czenie"
  ]
  node [
    id 1426
    label "nata&#324;czenie_si&#281;"
  ]
  node [
    id 1427
    label "rozta&#324;czenie_si&#281;"
  ]
  node [
    id 1428
    label "pota&#324;czenie"
  ]
  node [
    id 1429
    label "poruszanie_si&#281;"
  ]
  node [
    id 1430
    label "chwianie_si&#281;"
  ]
  node [
    id 1431
    label "dancing"
  ]
  node [
    id 1432
    label "pokazywanie"
  ]
  node [
    id 1433
    label "spe&#322;nienie"
  ]
  node [
    id 1434
    label "wzbudzenie"
  ]
  node [
    id 1435
    label "spowodowanie"
  ]
  node [
    id 1436
    label "pos&#322;anie"
  ]
  node [
    id 1437
    label "znalezienie_si&#281;"
  ]
  node [
    id 1438
    label "introduction"
  ]
  node [
    id 1439
    label "sp&#281;dzenie"
  ]
  node [
    id 1440
    label "zainstalowanie"
  ]
  node [
    id 1441
    label "rynek"
  ]
  node [
    id 1442
    label "nuklearyzacja"
  ]
  node [
    id 1443
    label "deduction"
  ]
  node [
    id 1444
    label "entrance"
  ]
  node [
    id 1445
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 1446
    label "wst&#281;p"
  ]
  node [
    id 1447
    label "wej&#347;cie"
  ]
  node [
    id 1448
    label "issue"
  ]
  node [
    id 1449
    label "umo&#380;liwienie"
  ]
  node [
    id 1450
    label "wpisanie"
  ]
  node [
    id 1451
    label "podstawy"
  ]
  node [
    id 1452
    label "evocation"
  ]
  node [
    id 1453
    label "zapoznanie"
  ]
  node [
    id 1454
    label "zacz&#281;cie"
  ]
  node [
    id 1455
    label "przewietrzenie"
  ]
  node [
    id 1456
    label "umieszczanie"
  ]
  node [
    id 1457
    label "initiation"
  ]
  node [
    id 1458
    label "zak&#322;&#243;canie"
  ]
  node [
    id 1459
    label "zapoznawanie"
  ]
  node [
    id 1460
    label "zaczynanie"
  ]
  node [
    id 1461
    label "trigger"
  ]
  node [
    id 1462
    label "wpisywanie"
  ]
  node [
    id 1463
    label "mental_hospital"
  ]
  node [
    id 1464
    label "wchodzenie"
  ]
  node [
    id 1465
    label "retraction"
  ]
  node [
    id 1466
    label "przewietrzanie"
  ]
  node [
    id 1467
    label "pokazanie"
  ]
  node [
    id 1468
    label "przypominanie"
  ]
  node [
    id 1469
    label "kojarzenie_si&#281;"
  ]
  node [
    id 1470
    label "sk&#322;anianie"
  ]
  node [
    id 1471
    label "adduction"
  ]
  node [
    id 1472
    label "pull"
  ]
  node [
    id 1473
    label "przesuwanie"
  ]
  node [
    id 1474
    label "j&#261;kanie"
  ]
  node [
    id 1475
    label "przetykanie"
  ]
  node [
    id 1476
    label "zaci&#261;ganie"
  ]
  node [
    id 1477
    label "przymocowywanie"
  ]
  node [
    id 1478
    label "przemieszczanie"
  ]
  node [
    id 1479
    label "przed&#322;u&#380;anie"
  ]
  node [
    id 1480
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1481
    label "rozci&#261;ganie"
  ]
  node [
    id 1482
    label "wymawianie"
  ]
  node [
    id 1483
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1484
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1485
    label "time"
  ]
  node [
    id 1486
    label "dzielenie"
  ]
  node [
    id 1487
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1488
    label "kaleczenie"
  ]
  node [
    id 1489
    label "film_editing"
  ]
  node [
    id 1490
    label "intersection"
  ]
  node [
    id 1491
    label "przerywanie"
  ]
  node [
    id 1492
    label "poprzecinanie"
  ]
  node [
    id 1493
    label "przerwanie"
  ]
  node [
    id 1494
    label "carving"
  ]
  node [
    id 1495
    label "cut"
  ]
  node [
    id 1496
    label "w&#281;ze&#322;"
  ]
  node [
    id 1497
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1498
    label "zranienie"
  ]
  node [
    id 1499
    label "snub"
  ]
  node [
    id 1500
    label "podzielenie"
  ]
  node [
    id 1501
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1502
    label "wypuszczenie"
  ]
  node [
    id 1503
    label "niesienie"
  ]
  node [
    id 1504
    label "potrzymanie"
  ]
  node [
    id 1505
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 1506
    label "sprawowanie"
  ]
  node [
    id 1507
    label "wypuszczanie"
  ]
  node [
    id 1508
    label "poise"
  ]
  node [
    id 1509
    label "retention"
  ]
  node [
    id 1510
    label "dzier&#380;enie"
  ]
  node [
    id 1511
    label "noszenie"
  ]
  node [
    id 1512
    label "uniemo&#380;liwianie"
  ]
  node [
    id 1513
    label "clasp"
  ]
  node [
    id 1514
    label "przetrzymywanie"
  ]
  node [
    id 1515
    label "detention"
  ]
  node [
    id 1516
    label "przetrzymanie"
  ]
  node [
    id 1517
    label "zmuszanie"
  ]
  node [
    id 1518
    label "hodowanie"
  ]
  node [
    id 1519
    label "skupisko"
  ]
  node [
    id 1520
    label "zal&#261;&#380;ek"
  ]
  node [
    id 1521
    label "otoczenie"
  ]
  node [
    id 1522
    label "Hollywood"
  ]
  node [
    id 1523
    label "warunki"
  ]
  node [
    id 1524
    label "center"
  ]
  node [
    id 1525
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1526
    label "sytuacja"
  ]
  node [
    id 1527
    label "okrycie"
  ]
  node [
    id 1528
    label "class"
  ]
  node [
    id 1529
    label "background"
  ]
  node [
    id 1530
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1531
    label "crack"
  ]
  node [
    id 1532
    label "cortege"
  ]
  node [
    id 1533
    label "okolica"
  ]
  node [
    id 1534
    label "huczek"
  ]
  node [
    id 1535
    label "Wielki_Atraktor"
  ]
  node [
    id 1536
    label "warunek_lokalowy"
  ]
  node [
    id 1537
    label "plac"
  ]
  node [
    id 1538
    label "location"
  ]
  node [
    id 1539
    label "uwaga"
  ]
  node [
    id 1540
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1541
    label "chwila"
  ]
  node [
    id 1542
    label "cia&#322;o"
  ]
  node [
    id 1543
    label "praca"
  ]
  node [
    id 1544
    label "rz&#261;d"
  ]
  node [
    id 1545
    label "osoba_prawna"
  ]
  node [
    id 1546
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1547
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1548
    label "poj&#281;cie"
  ]
  node [
    id 1549
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1550
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1551
    label "biuro"
  ]
  node [
    id 1552
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1553
    label "Fundusze_Unijne"
  ]
  node [
    id 1554
    label "zamyka&#263;"
  ]
  node [
    id 1555
    label "establishment"
  ]
  node [
    id 1556
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1557
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1558
    label "afiliowa&#263;"
  ]
  node [
    id 1559
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1560
    label "zamykanie"
  ]
  node [
    id 1561
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1562
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1563
    label "zar&#243;d&#378;"
  ]
  node [
    id 1564
    label "pocz&#261;tek"
  ]
  node [
    id 1565
    label "integument"
  ]
  node [
    id 1566
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 1567
    label "zawiera&#263;"
  ]
  node [
    id 1568
    label "fold"
  ]
  node [
    id 1569
    label "mie&#263;"
  ]
  node [
    id 1570
    label "lock"
  ]
  node [
    id 1571
    label "hide"
  ]
  node [
    id 1572
    label "czu&#263;"
  ]
  node [
    id 1573
    label "need"
  ]
  node [
    id 1574
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1575
    label "poznawa&#263;"
  ]
  node [
    id 1576
    label "obejmowa&#263;"
  ]
  node [
    id 1577
    label "make"
  ]
  node [
    id 1578
    label "ustala&#263;"
  ]
  node [
    id 1579
    label "by&#263;"
  ]
  node [
    id 1580
    label "might"
  ]
  node [
    id 1581
    label "uprawi&#263;"
  ]
  node [
    id 1582
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1583
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1584
    label "Fremeni"
  ]
  node [
    id 1585
    label "obiekt_naturalny"
  ]
  node [
    id 1586
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1587
    label "environment"
  ]
  node [
    id 1588
    label "ekosystem"
  ]
  node [
    id 1589
    label "wszechstworzenie"
  ]
  node [
    id 1590
    label "woda"
  ]
  node [
    id 1591
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1592
    label "teren"
  ]
  node [
    id 1593
    label "stw&#243;r"
  ]
  node [
    id 1594
    label "Ziemia"
  ]
  node [
    id 1595
    label "fauna"
  ]
  node [
    id 1596
    label "biota"
  ]
  node [
    id 1597
    label "ekskursja"
  ]
  node [
    id 1598
    label "bezsilnikowy"
  ]
  node [
    id 1599
    label "budowla"
  ]
  node [
    id 1600
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 1601
    label "trasa"
  ]
  node [
    id 1602
    label "podbieg"
  ]
  node [
    id 1603
    label "turystyka"
  ]
  node [
    id 1604
    label "nawierzchnia"
  ]
  node [
    id 1605
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1606
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 1607
    label "rajza"
  ]
  node [
    id 1608
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1609
    label "passage"
  ]
  node [
    id 1610
    label "wylot"
  ]
  node [
    id 1611
    label "ekwipunek"
  ]
  node [
    id 1612
    label "zbior&#243;wka"
  ]
  node [
    id 1613
    label "marszrutyzacja"
  ]
  node [
    id 1614
    label "wyb&#243;j"
  ]
  node [
    id 1615
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1616
    label "drogowskaz"
  ]
  node [
    id 1617
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1618
    label "pobocze"
  ]
  node [
    id 1619
    label "journey"
  ]
  node [
    id 1620
    label "ruch"
  ]
  node [
    id 1621
    label "naczynie"
  ]
  node [
    id 1622
    label "ko&#322;o_t&#281;tnicze_m&#243;zgu"
  ]
  node [
    id 1623
    label "artery"
  ]
  node [
    id 1624
    label "Tuszyn"
  ]
  node [
    id 1625
    label "Nowy_Staw"
  ]
  node [
    id 1626
    label "Koronowo"
  ]
  node [
    id 1627
    label "Bia&#322;a_Piska"
  ]
  node [
    id 1628
    label "Wysoka"
  ]
  node [
    id 1629
    label "Ma&#322;ogoszcz"
  ]
  node [
    id 1630
    label "Niemodlin"
  ]
  node [
    id 1631
    label "Sulmierzyce"
  ]
  node [
    id 1632
    label "Parczew"
  ]
  node [
    id 1633
    label "Dyn&#243;w"
  ]
  node [
    id 1634
    label "Brwin&#243;w"
  ]
  node [
    id 1635
    label "Pogorzela"
  ]
  node [
    id 1636
    label "Mszczon&#243;w"
  ]
  node [
    id 1637
    label "Olsztynek"
  ]
  node [
    id 1638
    label "Soko&#322;&#243;w_Ma&#322;opolski"
  ]
  node [
    id 1639
    label "Resko"
  ]
  node [
    id 1640
    label "&#379;uromin"
  ]
  node [
    id 1641
    label "Dobrzany"
  ]
  node [
    id 1642
    label "Wilamowice"
  ]
  node [
    id 1643
    label "Kruszwica"
  ]
  node [
    id 1644
    label "Jedlina-Zdr&#243;j"
  ]
  node [
    id 1645
    label "Warta"
  ]
  node [
    id 1646
    label "&#321;och&#243;w"
  ]
  node [
    id 1647
    label "Milicz"
  ]
  node [
    id 1648
    label "Niepo&#322;omice"
  ]
  node [
    id 1649
    label "My&#347;lib&#243;rz"
  ]
  node [
    id 1650
    label "Prabuty"
  ]
  node [
    id 1651
    label "Sul&#281;cin"
  ]
  node [
    id 1652
    label "Kudowa-Zdr&#243;j"
  ]
  node [
    id 1653
    label "Pi&#324;cz&#243;w"
  ]
  node [
    id 1654
    label "Brzeziny"
  ]
  node [
    id 1655
    label "G&#322;ubczyce"
  ]
  node [
    id 1656
    label "Mogilno"
  ]
  node [
    id 1657
    label "Suchowola"
  ]
  node [
    id 1658
    label "Ch&#281;ciny"
  ]
  node [
    id 1659
    label "Pilawa"
  ]
  node [
    id 1660
    label "Oborniki_&#346;l&#261;skie"
  ]
  node [
    id 1661
    label "W&#322;adys&#322;aw&#243;w"
  ]
  node [
    id 1662
    label "St&#281;szew"
  ]
  node [
    id 1663
    label "Jasie&#324;"
  ]
  node [
    id 1664
    label "Sulej&#243;w"
  ]
  node [
    id 1665
    label "B&#322;a&#380;owa"
  ]
  node [
    id 1666
    label "D&#261;browa_Bia&#322;ostocka"
  ]
  node [
    id 1667
    label "Bychawa"
  ]
  node [
    id 1668
    label "Grab&#243;w_nad_Prosn&#261;"
  ]
  node [
    id 1669
    label "Dolsk"
  ]
  node [
    id 1670
    label "&#346;wierzawa"
  ]
  node [
    id 1671
    label "Brze&#347;&#263;_Kujawski"
  ]
  node [
    id 1672
    label "Zalewo"
  ]
  node [
    id 1673
    label "Olszyna"
  ]
  node [
    id 1674
    label "Czerwie&#324;sk"
  ]
  node [
    id 1675
    label "Biecz"
  ]
  node [
    id 1676
    label "S&#281;dzisz&#243;w"
  ]
  node [
    id 1677
    label "Gryf&#243;w_&#346;l&#261;ski"
  ]
  node [
    id 1678
    label "Drezdenko"
  ]
  node [
    id 1679
    label "Bia&#322;a"
  ]
  node [
    id 1680
    label "Lipsko"
  ]
  node [
    id 1681
    label "G&#243;rzno"
  ]
  node [
    id 1682
    label "&#346;migiel"
  ]
  node [
    id 1683
    label "&#346;wi&#261;tniki_G&#243;rne"
  ]
  node [
    id 1684
    label "Suchedni&#243;w"
  ]
  node [
    id 1685
    label "Lubacz&#243;w"
  ]
  node [
    id 1686
    label "Tuliszk&#243;w"
  ]
  node [
    id 1687
    label "Polanica-Zdr&#243;j"
  ]
  node [
    id 1688
    label "Mirsk"
  ]
  node [
    id 1689
    label "G&#243;ra"
  ]
  node [
    id 1690
    label "Rychwa&#322;"
  ]
  node [
    id 1691
    label "Jab&#322;onowo_Pomorskie"
  ]
  node [
    id 1692
    label "Olesno"
  ]
  node [
    id 1693
    label "Toszek"
  ]
  node [
    id 1694
    label "Prusice"
  ]
  node [
    id 1695
    label "Radzy&#324;_Che&#322;mi&#324;ski"
  ]
  node [
    id 1696
    label "Radzymin"
  ]
  node [
    id 1697
    label "Ryn"
  ]
  node [
    id 1698
    label "Orzysz"
  ]
  node [
    id 1699
    label "Radziej&#243;w"
  ]
  node [
    id 1700
    label "Supra&#347;l"
  ]
  node [
    id 1701
    label "Imielin"
  ]
  node [
    id 1702
    label "Karczew"
  ]
  node [
    id 1703
    label "Sucha_Beskidzka"
  ]
  node [
    id 1704
    label "&#346;wierad&#243;w-Zdr&#243;j"
  ]
  node [
    id 1705
    label "Szczucin"
  ]
  node [
    id 1706
    label "Kobylin"
  ]
  node [
    id 1707
    label "Niemcza"
  ]
  node [
    id 1708
    label "Tokaj"
  ]
  node [
    id 1709
    label "Pie&#324;sk"
  ]
  node [
    id 1710
    label "Kock"
  ]
  node [
    id 1711
    label "Mi&#281;dzylesie"
  ]
  node [
    id 1712
    label "Bodzentyn"
  ]
  node [
    id 1713
    label "Ska&#322;a"
  ]
  node [
    id 1714
    label "Przedb&#243;rz"
  ]
  node [
    id 1715
    label "Bielsk_Podlaski"
  ]
  node [
    id 1716
    label "Krzeszowice"
  ]
  node [
    id 1717
    label "Jeziorany"
  ]
  node [
    id 1718
    label "Czarnk&#243;w"
  ]
  node [
    id 1719
    label "Mi&#322;os&#322;aw"
  ]
  node [
    id 1720
    label "&#321;asin"
  ]
  node [
    id 1721
    label "Czch&#243;w"
  ]
  node [
    id 1722
    label "Drohiczyn"
  ]
  node [
    id 1723
    label "Kolno"
  ]
  node [
    id 1724
    label "Bie&#380;u&#324;"
  ]
  node [
    id 1725
    label "K&#322;ecko"
  ]
  node [
    id 1726
    label "Rabka-Zdr&#243;j"
  ]
  node [
    id 1727
    label "Golczewo"
  ]
  node [
    id 1728
    label "Pniewy"
  ]
  node [
    id 1729
    label "Jedlicze"
  ]
  node [
    id 1730
    label "Glinojeck"
  ]
  node [
    id 1731
    label "Wojnicz"
  ]
  node [
    id 1732
    label "Podd&#281;bice"
  ]
  node [
    id 1733
    label "Miastko"
  ]
  node [
    id 1734
    label "Kamie&#324;_Pomorski"
  ]
  node [
    id 1735
    label "Pako&#347;&#263;"
  ]
  node [
    id 1736
    label "Pi&#322;awa_G&#243;rna"
  ]
  node [
    id 1737
    label "I&#324;sko"
  ]
  node [
    id 1738
    label "Rudnik_nad_Sanem"
  ]
  node [
    id 1739
    label "Sejny"
  ]
  node [
    id 1740
    label "Skaryszew"
  ]
  node [
    id 1741
    label "Wojciesz&#243;w"
  ]
  node [
    id 1742
    label "Nieszawa"
  ]
  node [
    id 1743
    label "Gogolin"
  ]
  node [
    id 1744
    label "S&#322;awa"
  ]
  node [
    id 1745
    label "Bierut&#243;w"
  ]
  node [
    id 1746
    label "Knyszyn"
  ]
  node [
    id 1747
    label "Podkowa_Le&#347;na"
  ]
  node [
    id 1748
    label "I&#322;&#380;a"
  ]
  node [
    id 1749
    label "Grodk&#243;w"
  ]
  node [
    id 1750
    label "Krzepice"
  ]
  node [
    id 1751
    label "Janikowo"
  ]
  node [
    id 1752
    label "S&#261;dowa_Wisznia"
  ]
  node [
    id 1753
    label "&#321;osice"
  ]
  node [
    id 1754
    label "&#379;ukowo"
  ]
  node [
    id 1755
    label "Witkowo"
  ]
  node [
    id 1756
    label "Czempi&#324;"
  ]
  node [
    id 1757
    label "Wyszogr&#243;d"
  ]
  node [
    id 1758
    label "Dzia&#322;oszyn"
  ]
  node [
    id 1759
    label "Dzierzgo&#324;"
  ]
  node [
    id 1760
    label "S&#281;popol"
  ]
  node [
    id 1761
    label "Terespol"
  ]
  node [
    id 1762
    label "Brzoz&#243;w"
  ]
  node [
    id 1763
    label "Ko&#378;min_Wielkopolski"
  ]
  node [
    id 1764
    label "Bystrzyca_K&#322;odzka"
  ]
  node [
    id 1765
    label "Dobre_Miasto"
  ]
  node [
    id 1766
    label "Kcynia"
  ]
  node [
    id 1767
    label "&#262;miel&#243;w"
  ]
  node [
    id 1768
    label "Obrzycko"
  ]
  node [
    id 1769
    label "S&#281;p&#243;lno_Kraje&#324;skie"
  ]
  node [
    id 1770
    label "Iwonicz-Zdr&#243;j"
  ]
  node [
    id 1771
    label "S&#322;omniki"
  ]
  node [
    id 1772
    label "Barcin"
  ]
  node [
    id 1773
    label "Mak&#243;w_Mazowiecki"
  ]
  node [
    id 1774
    label "Gniewkowo"
  ]
  node [
    id 1775
    label "Paj&#281;czno"
  ]
  node [
    id 1776
    label "Jedwabne"
  ]
  node [
    id 1777
    label "Tyczyn"
  ]
  node [
    id 1778
    label "Osiek"
  ]
  node [
    id 1779
    label "Pu&#324;sk"
  ]
  node [
    id 1780
    label "Zakroczym"
  ]
  node [
    id 1781
    label "&#321;abiszyn"
  ]
  node [
    id 1782
    label "Skarszewy"
  ]
  node [
    id 1783
    label "Rapperswil"
  ]
  node [
    id 1784
    label "K&#261;ty_Wroc&#322;awskie"
  ]
  node [
    id 1785
    label "Rzepin"
  ]
  node [
    id 1786
    label "&#346;lesin"
  ]
  node [
    id 1787
    label "Ko&#380;uch&#243;w"
  ]
  node [
    id 1788
    label "Po&#322;aniec"
  ]
  node [
    id 1789
    label "Chodecz"
  ]
  node [
    id 1790
    label "W&#261;sosz"
  ]
  node [
    id 1791
    label "Kargowa"
  ]
  node [
    id 1792
    label "Krasnobr&#243;d"
  ]
  node [
    id 1793
    label "Zakliczyn"
  ]
  node [
    id 1794
    label "Bukowno"
  ]
  node [
    id 1795
    label "&#379;ychlin"
  ]
  node [
    id 1796
    label "&#321;askarzew"
  ]
  node [
    id 1797
    label "G&#322;og&#243;wek"
  ]
  node [
    id 1798
    label "Drawno"
  ]
  node [
    id 1799
    label "Kazimierza_Wielka"
  ]
  node [
    id 1800
    label "Kozieg&#322;owy"
  ]
  node [
    id 1801
    label "Kowal"
  ]
  node [
    id 1802
    label "Jordan&#243;w"
  ]
  node [
    id 1803
    label "S&#281;dzisz&#243;w_Ma&#322;opolski"
  ]
  node [
    id 1804
    label "Ustrzyki_Dolne"
  ]
  node [
    id 1805
    label "Strumie&#324;"
  ]
  node [
    id 1806
    label "Radymno"
  ]
  node [
    id 1807
    label "Otmuch&#243;w"
  ]
  node [
    id 1808
    label "K&#243;rnik"
  ]
  node [
    id 1809
    label "Wierusz&#243;w"
  ]
  node [
    id 1810
    label "Na&#322;&#281;cz&#243;w"
  ]
  node [
    id 1811
    label "Tychowo"
  ]
  node [
    id 1812
    label "Czersk"
  ]
  node [
    id 1813
    label "Mo&#324;ki"
  ]
  node [
    id 1814
    label "Trzci&#324;sko-Zdr&#243;j"
  ]
  node [
    id 1815
    label "Pelplin"
  ]
  node [
    id 1816
    label "Poniec"
  ]
  node [
    id 1817
    label "Piotrk&#243;w_Kujawski"
  ]
  node [
    id 1818
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1819
    label "G&#261;bin"
  ]
  node [
    id 1820
    label "Gniew"
  ]
  node [
    id 1821
    label "Cieszan&#243;w"
  ]
  node [
    id 1822
    label "Serock"
  ]
  node [
    id 1823
    label "Drzewica"
  ]
  node [
    id 1824
    label "Skwierzyna"
  ]
  node [
    id 1825
    label "Bra&#324;sk"
  ]
  node [
    id 1826
    label "Nowe_Brzesko"
  ]
  node [
    id 1827
    label "Dobrzy&#324;_nad_Wis&#322;&#261;"
  ]
  node [
    id 1828
    label "Nowe_Miasto_Lubawskie"
  ]
  node [
    id 1829
    label "Szadek"
  ]
  node [
    id 1830
    label "Kalety"
  ]
  node [
    id 1831
    label "Borek_Wielkopolski"
  ]
  node [
    id 1832
    label "Kalisz_Pomorski"
  ]
  node [
    id 1833
    label "Pyzdry"
  ]
  node [
    id 1834
    label "Ostr&#243;w_Lubelski"
  ]
  node [
    id 1835
    label "Bobowa"
  ]
  node [
    id 1836
    label "Cedynia"
  ]
  node [
    id 1837
    label "Bia&#322;a_Rawska"
  ]
  node [
    id 1838
    label "Sieniawa"
  ]
  node [
    id 1839
    label "Su&#322;kowice"
  ]
  node [
    id 1840
    label "Drobin"
  ]
  node [
    id 1841
    label "Zag&#243;rz"
  ]
  node [
    id 1842
    label "Brok"
  ]
  node [
    id 1843
    label "Nowe"
  ]
  node [
    id 1844
    label "Szczebrzeszyn"
  ]
  node [
    id 1845
    label "O&#380;ar&#243;w"
  ]
  node [
    id 1846
    label "Rydzyna"
  ]
  node [
    id 1847
    label "&#379;arki"
  ]
  node [
    id 1848
    label "Zwole&#324;"
  ]
  node [
    id 1849
    label "Nowy_Dw&#243;r_Gda&#324;ski"
  ]
  node [
    id 1850
    label "G&#322;og&#243;w_Ma&#322;opolski"
  ]
  node [
    id 1851
    label "Drawsko_Pomorskie"
  ]
  node [
    id 1852
    label "Torzym"
  ]
  node [
    id 1853
    label "Ryglice"
  ]
  node [
    id 1854
    label "Szepietowo"
  ]
  node [
    id 1855
    label "Biskupiec"
  ]
  node [
    id 1856
    label "&#379;abno"
  ]
  node [
    id 1857
    label "Opat&#243;w"
  ]
  node [
    id 1858
    label "Przysucha"
  ]
  node [
    id 1859
    label "Ryki"
  ]
  node [
    id 1860
    label "Reszel"
  ]
  node [
    id 1861
    label "Kolbuszowa"
  ]
  node [
    id 1862
    label "Margonin"
  ]
  node [
    id 1863
    label "Kamie&#324;_Kraje&#324;ski"
  ]
  node [
    id 1864
    label "Mi&#281;dzych&#243;d"
  ]
  node [
    id 1865
    label "Szubin"
  ]
  node [
    id 1866
    label "Sk&#281;pe"
  ]
  node [
    id 1867
    label "&#379;elech&#243;w"
  ]
  node [
    id 1868
    label "Proszowice"
  ]
  node [
    id 1869
    label "Polan&#243;w"
  ]
  node [
    id 1870
    label "Chorzele"
  ]
  node [
    id 1871
    label "Kostrzyn"
  ]
  node [
    id 1872
    label "Koniecpol"
  ]
  node [
    id 1873
    label "Ryman&#243;w"
  ]
  node [
    id 1874
    label "Dziwn&#243;w"
  ]
  node [
    id 1875
    label "Lesko"
  ]
  node [
    id 1876
    label "Lw&#243;wek"
  ]
  node [
    id 1877
    label "Brzeszcze"
  ]
  node [
    id 1878
    label "Strzy&#380;&#243;w"
  ]
  node [
    id 1879
    label "Sierak&#243;w"
  ]
  node [
    id 1880
    label "Bia&#322;obrzegi"
  ]
  node [
    id 1881
    label "Skalbmierz"
  ]
  node [
    id 1882
    label "Zawichost"
  ]
  node [
    id 1883
    label "Raszk&#243;w"
  ]
  node [
    id 1884
    label "Sian&#243;w"
  ]
  node [
    id 1885
    label "&#379;erk&#243;w"
  ]
  node [
    id 1886
    label "Pieszyce"
  ]
  node [
    id 1887
    label "I&#322;owa"
  ]
  node [
    id 1888
    label "Zel&#243;w"
  ]
  node [
    id 1889
    label "Uniej&#243;w"
  ]
  node [
    id 1890
    label "Przec&#322;aw"
  ]
  node [
    id 1891
    label "Mieszkowice"
  ]
  node [
    id 1892
    label "Wisztyniec"
  ]
  node [
    id 1893
    label "Petryk&#243;w"
  ]
  node [
    id 1894
    label "Wyrzysk"
  ]
  node [
    id 1895
    label "Myszyniec"
  ]
  node [
    id 1896
    label "Gorz&#243;w_&#346;l&#261;ski"
  ]
  node [
    id 1897
    label "W&#322;oszczowa"
  ]
  node [
    id 1898
    label "Goni&#261;dz"
  ]
  node [
    id 1899
    label "Dobrzyca"
  ]
  node [
    id 1900
    label "L&#261;dek-Zdr&#243;j"
  ]
  node [
    id 1901
    label "Dukla"
  ]
  node [
    id 1902
    label "Siewierz"
  ]
  node [
    id 1903
    label "Kun&#243;w"
  ]
  node [
    id 1904
    label "Lubie&#324;_Kujawski"
  ]
  node [
    id 1905
    label "Aleksandr&#243;w_Kujawski"
  ]
  node [
    id 1906
    label "O&#380;ar&#243;w_Mazowiecki"
  ]
  node [
    id 1907
    label "Piwniczna-Zdr&#243;j"
  ]
  node [
    id 1908
    label "Zator"
  ]
  node [
    id 1909
    label "Krosno_Odrza&#324;skie"
  ]
  node [
    id 1910
    label "Bolk&#243;w"
  ]
  node [
    id 1911
    label "Odolan&#243;w"
  ]
  node [
    id 1912
    label "Golina"
  ]
  node [
    id 1913
    label "Miech&#243;w"
  ]
  node [
    id 1914
    label "Mogielnica"
  ]
  node [
    id 1915
    label "Dobczyce"
  ]
  node [
    id 1916
    label "Muszyna"
  ]
  node [
    id 1917
    label "Radomy&#347;l_Wielki"
  ]
  node [
    id 1918
    label "R&#243;&#380;an"
  ]
  node [
    id 1919
    label "Zab&#322;ud&#243;w"
  ]
  node [
    id 1920
    label "Wysokie_Mazowieckie"
  ]
  node [
    id 1921
    label "Ulan&#243;w"
  ]
  node [
    id 1922
    label "Rogo&#378;no"
  ]
  node [
    id 1923
    label "Ciechanowiec"
  ]
  node [
    id 1924
    label "Lubomierz"
  ]
  node [
    id 1925
    label "Mierosz&#243;w"
  ]
  node [
    id 1926
    label "Lubawa"
  ]
  node [
    id 1927
    label "Ci&#281;&#380;kowice"
  ]
  node [
    id 1928
    label "Tykocin"
  ]
  node [
    id 1929
    label "Tarczyn"
  ]
  node [
    id 1930
    label "Rejowiec_Fabryczny"
  ]
  node [
    id 1931
    label "Alwernia"
  ]
  node [
    id 1932
    label "Karlino"
  ]
  node [
    id 1933
    label "Duszniki-Zdr&#243;j"
  ]
  node [
    id 1934
    label "Warka"
  ]
  node [
    id 1935
    label "Krynica_Morska"
  ]
  node [
    id 1936
    label "Lewin_Brzeski"
  ]
  node [
    id 1937
    label "Chyr&#243;w"
  ]
  node [
    id 1938
    label "Przemk&#243;w"
  ]
  node [
    id 1939
    label "Hel"
  ]
  node [
    id 1940
    label "Chocian&#243;w"
  ]
  node [
    id 1941
    label "Po&#322;czyn-Zdr&#243;j"
  ]
  node [
    id 1942
    label "Stawiszyn"
  ]
  node [
    id 1943
    label "Puszczykowo"
  ]
  node [
    id 1944
    label "Ciechocinek"
  ]
  node [
    id 1945
    label "Strzelce_Kraje&#324;skie"
  ]
  node [
    id 1946
    label "Mszana_Dolna"
  ]
  node [
    id 1947
    label "Rad&#322;&#243;w"
  ]
  node [
    id 1948
    label "Nasielsk"
  ]
  node [
    id 1949
    label "Szczyrk"
  ]
  node [
    id 1950
    label "Trzemeszno"
  ]
  node [
    id 1951
    label "Recz"
  ]
  node [
    id 1952
    label "Wo&#322;czyn"
  ]
  node [
    id 1953
    label "Pilica"
  ]
  node [
    id 1954
    label "Prochowice"
  ]
  node [
    id 1955
    label "Buk"
  ]
  node [
    id 1956
    label "Kowary"
  ]
  node [
    id 1957
    label "Szczawno-Zdr&#243;j"
  ]
  node [
    id 1958
    label "Bojanowo"
  ]
  node [
    id 1959
    label "Maszewo"
  ]
  node [
    id 1960
    label "Tyszowce"
  ]
  node [
    id 1961
    label "Ogrodzieniec"
  ]
  node [
    id 1962
    label "Tuch&#243;w"
  ]
  node [
    id 1963
    label "Chojna"
  ]
  node [
    id 1964
    label "Kamie&#324;sk"
  ]
  node [
    id 1965
    label "Gryb&#243;w"
  ]
  node [
    id 1966
    label "Wasilk&#243;w"
  ]
  node [
    id 1967
    label "Krzy&#380;_Wielkopolski"
  ]
  node [
    id 1968
    label "Janowiec_Wielkopolski"
  ]
  node [
    id 1969
    label "Zag&#243;r&#243;w"
  ]
  node [
    id 1970
    label "Che&#322;mek"
  ]
  node [
    id 1971
    label "Z&#322;oty_Stok"
  ]
  node [
    id 1972
    label "Stronie_&#346;l&#261;skie"
  ]
  node [
    id 1973
    label "Nowy_Wi&#347;nicz"
  ]
  node [
    id 1974
    label "Krynica-Zdr&#243;j"
  ]
  node [
    id 1975
    label "Wolbrom"
  ]
  node [
    id 1976
    label "Szczuczyn"
  ]
  node [
    id 1977
    label "S&#322;awk&#243;w"
  ]
  node [
    id 1978
    label "Kazimierz_Dolny"
  ]
  node [
    id 1979
    label "Wo&#378;niki"
  ]
  node [
    id 1980
    label "obwodnica_autostradowa"
  ]
  node [
    id 1981
    label "droga_publiczna"
  ]
  node [
    id 1982
    label "przej&#347;cie"
  ]
  node [
    id 1983
    label "kraw&#281;&#380;nik"
  ]
  node [
    id 1984
    label "chody"
  ]
  node [
    id 1985
    label "sztreka"
  ]
  node [
    id 1986
    label "kostka_brukowa"
  ]
  node [
    id 1987
    label "pieszy"
  ]
  node [
    id 1988
    label "drzewo"
  ]
  node [
    id 1989
    label "wyrobisko"
  ]
  node [
    id 1990
    label "kornik"
  ]
  node [
    id 1991
    label "dywanik"
  ]
  node [
    id 1992
    label "przodek"
  ]
  node [
    id 1993
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 1994
    label "koszyk&#243;wka"
  ]
  node [
    id 1995
    label "uspo&#322;ecznianie"
  ]
  node [
    id 1996
    label "uspo&#322;ecznienie"
  ]
  node [
    id 1997
    label "przyczynianie_si&#281;"
  ]
  node [
    id 1998
    label "przebudowywanie"
  ]
  node [
    id 1999
    label "zreorganizowanie"
  ]
  node [
    id 2000
    label "przyczynienie_si&#281;"
  ]
  node [
    id 2001
    label "okres_amazo&#324;ski"
  ]
  node [
    id 2002
    label "stater"
  ]
  node [
    id 2003
    label "flow"
  ]
  node [
    id 2004
    label "choroba_przyrodzona"
  ]
  node [
    id 2005
    label "postglacja&#322;"
  ]
  node [
    id 2006
    label "sylur"
  ]
  node [
    id 2007
    label "kreda"
  ]
  node [
    id 2008
    label "ordowik"
  ]
  node [
    id 2009
    label "okres_hesperyjski"
  ]
  node [
    id 2010
    label "paleogen"
  ]
  node [
    id 2011
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 2012
    label "okres_halsztacki"
  ]
  node [
    id 2013
    label "riak"
  ]
  node [
    id 2014
    label "czwartorz&#281;d"
  ]
  node [
    id 2015
    label "podokres"
  ]
  node [
    id 2016
    label "trzeciorz&#281;d"
  ]
  node [
    id 2017
    label "kalim"
  ]
  node [
    id 2018
    label "fala"
  ]
  node [
    id 2019
    label "perm"
  ]
  node [
    id 2020
    label "retoryka"
  ]
  node [
    id 2021
    label "prekambr"
  ]
  node [
    id 2022
    label "neogen"
  ]
  node [
    id 2023
    label "pulsacja"
  ]
  node [
    id 2024
    label "proces_fizjologiczny"
  ]
  node [
    id 2025
    label "kambr"
  ]
  node [
    id 2026
    label "dzieje"
  ]
  node [
    id 2027
    label "kriogen"
  ]
  node [
    id 2028
    label "jednostka_geologiczna"
  ]
  node [
    id 2029
    label "time_period"
  ]
  node [
    id 2030
    label "period"
  ]
  node [
    id 2031
    label "ton"
  ]
  node [
    id 2032
    label "orosir"
  ]
  node [
    id 2033
    label "okres_czasu"
  ]
  node [
    id 2034
    label "poprzednik"
  ]
  node [
    id 2035
    label "spell"
  ]
  node [
    id 2036
    label "interstadia&#322;"
  ]
  node [
    id 2037
    label "ektas"
  ]
  node [
    id 2038
    label "sider"
  ]
  node [
    id 2039
    label "epoka"
  ]
  node [
    id 2040
    label "rok_akademicki"
  ]
  node [
    id 2041
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 2042
    label "schy&#322;ek"
  ]
  node [
    id 2043
    label "cykl"
  ]
  node [
    id 2044
    label "ciota"
  ]
  node [
    id 2045
    label "pierwszorz&#281;d"
  ]
  node [
    id 2046
    label "okres_noachijski"
  ]
  node [
    id 2047
    label "ediakar"
  ]
  node [
    id 2048
    label "zdanie"
  ]
  node [
    id 2049
    label "nast&#281;pnik"
  ]
  node [
    id 2050
    label "condition"
  ]
  node [
    id 2051
    label "jura"
  ]
  node [
    id 2052
    label "glacja&#322;"
  ]
  node [
    id 2053
    label "sten"
  ]
  node [
    id 2054
    label "Zeitgeist"
  ]
  node [
    id 2055
    label "era"
  ]
  node [
    id 2056
    label "trias"
  ]
  node [
    id 2057
    label "p&#243;&#322;okres"
  ]
  node [
    id 2058
    label "rok_szkolny"
  ]
  node [
    id 2059
    label "dewon"
  ]
  node [
    id 2060
    label "karbon"
  ]
  node [
    id 2061
    label "izochronizm"
  ]
  node [
    id 2062
    label "preglacja&#322;"
  ]
  node [
    id 2063
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 2064
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 2065
    label "drugorz&#281;d"
  ]
  node [
    id 2066
    label "semester"
  ]
  node [
    id 2067
    label "zniewie&#347;cialec"
  ]
  node [
    id 2068
    label "miesi&#261;czka"
  ]
  node [
    id 2069
    label "oferma"
  ]
  node [
    id 2070
    label "gej"
  ]
  node [
    id 2071
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 2072
    label "pedalstwo"
  ]
  node [
    id 2073
    label "mazgaj"
  ]
  node [
    id 2074
    label "poprzedzanie"
  ]
  node [
    id 2075
    label "czasoprzestrze&#324;"
  ]
  node [
    id 2076
    label "laba"
  ]
  node [
    id 2077
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 2078
    label "chronometria"
  ]
  node [
    id 2079
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 2080
    label "rachuba_czasu"
  ]
  node [
    id 2081
    label "przep&#322;ywanie"
  ]
  node [
    id 2082
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 2083
    label "czasokres"
  ]
  node [
    id 2084
    label "odczyt"
  ]
  node [
    id 2085
    label "kategoria_gramatyczna"
  ]
  node [
    id 2086
    label "poprzedzenie"
  ]
  node [
    id 2087
    label "trawienie"
  ]
  node [
    id 2088
    label "pochodzi&#263;"
  ]
  node [
    id 2089
    label "poprzedza&#263;"
  ]
  node [
    id 2090
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 2091
    label "odwlekanie_si&#281;"
  ]
  node [
    id 2092
    label "zegar"
  ]
  node [
    id 2093
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 2094
    label "czwarty_wymiar"
  ]
  node [
    id 2095
    label "pochodzenie"
  ]
  node [
    id 2096
    label "koniugacja"
  ]
  node [
    id 2097
    label "trawi&#263;"
  ]
  node [
    id 2098
    label "pogoda"
  ]
  node [
    id 2099
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 2100
    label "poprzedzi&#263;"
  ]
  node [
    id 2101
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 2102
    label "fraza"
  ]
  node [
    id 2103
    label "prison_term"
  ]
  node [
    id 2104
    label "system"
  ]
  node [
    id 2105
    label "przedstawienie"
  ]
  node [
    id 2106
    label "wyra&#380;enie"
  ]
  node [
    id 2107
    label "zaliczenie"
  ]
  node [
    id 2108
    label "antylogizm"
  ]
  node [
    id 2109
    label "zmuszenie"
  ]
  node [
    id 2110
    label "konektyw"
  ]
  node [
    id 2111
    label "attitude"
  ]
  node [
    id 2112
    label "adjudication"
  ]
  node [
    id 2113
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 2114
    label "aalen"
  ]
  node [
    id 2115
    label "jura_wczesna"
  ]
  node [
    id 2116
    label "holocen"
  ]
  node [
    id 2117
    label "pliocen"
  ]
  node [
    id 2118
    label "plejstocen"
  ]
  node [
    id 2119
    label "paleocen"
  ]
  node [
    id 2120
    label "bajos"
  ]
  node [
    id 2121
    label "kelowej"
  ]
  node [
    id 2122
    label "eocen"
  ]
  node [
    id 2123
    label "miocen"
  ]
  node [
    id 2124
    label "&#347;rodkowy_trias"
  ]
  node [
    id 2125
    label "term"
  ]
  node [
    id 2126
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 2127
    label "wczesny_trias"
  ]
  node [
    id 2128
    label "jura_&#347;rodkowa"
  ]
  node [
    id 2129
    label "oligocen"
  ]
  node [
    id 2130
    label "implikacja"
  ]
  node [
    id 2131
    label "argument"
  ]
  node [
    id 2132
    label "kszta&#322;t"
  ]
  node [
    id 2133
    label "pasemko"
  ]
  node [
    id 2134
    label "znak_diakrytyczny"
  ]
  node [
    id 2135
    label "zjawisko"
  ]
  node [
    id 2136
    label "zafalowanie"
  ]
  node [
    id 2137
    label "kot"
  ]
  node [
    id 2138
    label "przemoc"
  ]
  node [
    id 2139
    label "reakcja"
  ]
  node [
    id 2140
    label "strumie&#324;"
  ]
  node [
    id 2141
    label "karb"
  ]
  node [
    id 2142
    label "mn&#243;stwo"
  ]
  node [
    id 2143
    label "fit"
  ]
  node [
    id 2144
    label "grzywa_fali"
  ]
  node [
    id 2145
    label "efekt_Dopplera"
  ]
  node [
    id 2146
    label "obcinka"
  ]
  node [
    id 2147
    label "t&#322;um"
  ]
  node [
    id 2148
    label "stream"
  ]
  node [
    id 2149
    label "zafalowa&#263;"
  ]
  node [
    id 2150
    label "rozbicie_si&#281;"
  ]
  node [
    id 2151
    label "wojsko"
  ]
  node [
    id 2152
    label "clutter"
  ]
  node [
    id 2153
    label "rozbijanie_si&#281;"
  ]
  node [
    id 2154
    label "czo&#322;o_fali"
  ]
  node [
    id 2155
    label "cykl_astronomiczny"
  ]
  node [
    id 2156
    label "coil"
  ]
  node [
    id 2157
    label "fotoelement"
  ]
  node [
    id 2158
    label "komutowanie"
  ]
  node [
    id 2159
    label "stan_skupienia"
  ]
  node [
    id 2160
    label "nastr&#243;j"
  ]
  node [
    id 2161
    label "przerywacz"
  ]
  node [
    id 2162
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 2163
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 2164
    label "kraw&#281;d&#378;"
  ]
  node [
    id 2165
    label "obsesja"
  ]
  node [
    id 2166
    label "dw&#243;jnik"
  ]
  node [
    id 2167
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 2168
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 2169
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 2170
    label "przew&#243;d"
  ]
  node [
    id 2171
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 2172
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 2173
    label "obw&#243;d"
  ]
  node [
    id 2174
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 2175
    label "degree"
  ]
  node [
    id 2176
    label "komutowa&#263;"
  ]
  node [
    id 2177
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 2178
    label "serce"
  ]
  node [
    id 2179
    label "ripple"
  ]
  node [
    id 2180
    label "pracowanie"
  ]
  node [
    id 2181
    label "zabicie"
  ]
  node [
    id 2182
    label "set"
  ]
  node [
    id 2183
    label "przebieg"
  ]
  node [
    id 2184
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 2185
    label "owulacja"
  ]
  node [
    id 2186
    label "sekwencja"
  ]
  node [
    id 2187
    label "edycja"
  ]
  node [
    id 2188
    label "cycle"
  ]
  node [
    id 2189
    label "nauka_humanistyczna"
  ]
  node [
    id 2190
    label "erystyka"
  ]
  node [
    id 2191
    label "chironomia"
  ]
  node [
    id 2192
    label "elokwencja"
  ]
  node [
    id 2193
    label "sztuka"
  ]
  node [
    id 2194
    label "elokucja"
  ]
  node [
    id 2195
    label "tropika"
  ]
  node [
    id 2196
    label "era_paleozoiczna"
  ]
  node [
    id 2197
    label "ludlow"
  ]
  node [
    id 2198
    label "moneta"
  ]
  node [
    id 2199
    label "paleoproterozoik"
  ]
  node [
    id 2200
    label "zlodowacenie"
  ]
  node [
    id 2201
    label "asteroksylon"
  ]
  node [
    id 2202
    label "pluwia&#322;"
  ]
  node [
    id 2203
    label "mezoproterozoik"
  ]
  node [
    id 2204
    label "era_kenozoiczna"
  ]
  node [
    id 2205
    label "p&#243;&#378;ny_trias"
  ]
  node [
    id 2206
    label "ret"
  ]
  node [
    id 2207
    label "era_mezozoiczna"
  ]
  node [
    id 2208
    label "konodont"
  ]
  node [
    id 2209
    label "kajper"
  ]
  node [
    id 2210
    label "neoproterozoik"
  ]
  node [
    id 2211
    label "chalk"
  ]
  node [
    id 2212
    label "narz&#281;dzie"
  ]
  node [
    id 2213
    label "santon"
  ]
  node [
    id 2214
    label "cenoman"
  ]
  node [
    id 2215
    label "neokom"
  ]
  node [
    id 2216
    label "apt"
  ]
  node [
    id 2217
    label "pobia&#322;ka"
  ]
  node [
    id 2218
    label "ska&#322;a_organogeniczna"
  ]
  node [
    id 2219
    label "alb"
  ]
  node [
    id 2220
    label "pastel"
  ]
  node [
    id 2221
    label "turon"
  ]
  node [
    id 2222
    label "pteranodon"
  ]
  node [
    id 2223
    label "wieloton"
  ]
  node [
    id 2224
    label "tu&#324;czyk"
  ]
  node [
    id 2225
    label "d&#378;wi&#281;k"
  ]
  node [
    id 2226
    label "zabarwienie"
  ]
  node [
    id 2227
    label "interwa&#322;"
  ]
  node [
    id 2228
    label "modalizm"
  ]
  node [
    id 2229
    label "ubarwienie"
  ]
  node [
    id 2230
    label "note"
  ]
  node [
    id 2231
    label "formality"
  ]
  node [
    id 2232
    label "glinka"
  ]
  node [
    id 2233
    label "sound"
  ]
  node [
    id 2234
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 2235
    label "zwyczaj"
  ]
  node [
    id 2236
    label "solmizacja"
  ]
  node [
    id 2237
    label "seria"
  ]
  node [
    id 2238
    label "tone"
  ]
  node [
    id 2239
    label "kolorystyka"
  ]
  node [
    id 2240
    label "r&#243;&#380;nica"
  ]
  node [
    id 2241
    label "akcent"
  ]
  node [
    id 2242
    label "repetycja"
  ]
  node [
    id 2243
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 2244
    label "heksachord"
  ]
  node [
    id 2245
    label "rejestr"
  ]
  node [
    id 2246
    label "pistolet_maszynowy"
  ]
  node [
    id 2247
    label "jednostka_si&#322;y"
  ]
  node [
    id 2248
    label "pozauk&#322;adowa_jednostka_miary"
  ]
  node [
    id 2249
    label "wapie&#324;_pi&#324;czowski"
  ]
  node [
    id 2250
    label "pensylwan"
  ]
  node [
    id 2251
    label "mezozaur"
  ]
  node [
    id 2252
    label "pikaia"
  ]
  node [
    id 2253
    label "era_eozoiczna"
  ]
  node [
    id 2254
    label "rand"
  ]
  node [
    id 2255
    label "era_archaiczna"
  ]
  node [
    id 2256
    label "huron"
  ]
  node [
    id 2257
    label "Permian"
  ]
  node [
    id 2258
    label "blokada"
  ]
  node [
    id 2259
    label "cechsztyn"
  ]
  node [
    id 2260
    label "dogger"
  ]
  node [
    id 2261
    label "plezjozaur"
  ]
  node [
    id 2262
    label "euoplocefal"
  ]
  node [
    id 2263
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 2264
    label "eon"
  ]
  node [
    id 2265
    label "miesi&#261;c"
  ]
  node [
    id 2266
    label "tydzie&#324;"
  ]
  node [
    id 2267
    label "miech"
  ]
  node [
    id 2268
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 2269
    label "kalendy"
  ]
  node [
    id 2270
    label "p&#243;&#322;rocze"
  ]
  node [
    id 2271
    label "martwy_sezon"
  ]
  node [
    id 2272
    label "kalendarz"
  ]
  node [
    id 2273
    label "lata"
  ]
  node [
    id 2274
    label "pora_roku"
  ]
  node [
    id 2275
    label "stulecie"
  ]
  node [
    id 2276
    label "kurs"
  ]
  node [
    id 2277
    label "jubileusz"
  ]
  node [
    id 2278
    label "kwarta&#322;"
  ]
  node [
    id 2279
    label "summer"
  ]
  node [
    id 2280
    label "anniwersarz"
  ]
  node [
    id 2281
    label "rocznica"
  ]
  node [
    id 2282
    label "obszar"
  ]
  node [
    id 2283
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 2284
    label "long_time"
  ]
  node [
    id 2285
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 2286
    label "almanac"
  ]
  node [
    id 2287
    label "rozk&#322;ad"
  ]
  node [
    id 2288
    label "Juliusz_Cezar"
  ]
  node [
    id 2289
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 2290
    label "zwy&#380;kowanie"
  ]
  node [
    id 2291
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 2292
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 2293
    label "zaj&#281;cia"
  ]
  node [
    id 2294
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 2295
    label "przeorientowywanie"
  ]
  node [
    id 2296
    label "przejazd"
  ]
  node [
    id 2297
    label "przeorientowywa&#263;"
  ]
  node [
    id 2298
    label "nauka"
  ]
  node [
    id 2299
    label "klasa"
  ]
  node [
    id 2300
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 2301
    label "przeorientowa&#263;"
  ]
  node [
    id 2302
    label "manner"
  ]
  node [
    id 2303
    label "course"
  ]
  node [
    id 2304
    label "zni&#380;kowanie"
  ]
  node [
    id 2305
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 2306
    label "stawka"
  ]
  node [
    id 2307
    label "way"
  ]
  node [
    id 2308
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 2309
    label "deprecjacja"
  ]
  node [
    id 2310
    label "cedu&#322;a"
  ]
  node [
    id 2311
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 2312
    label "bearing"
  ]
  node [
    id 2313
    label "Lira"
  ]
  node [
    id 2314
    label "Barb&#243;rka"
  ]
  node [
    id 2315
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 2316
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 2317
    label "Sylwester"
  ]
  node [
    id 2318
    label "g&#243;rnik"
  ]
  node [
    id 2319
    label "comber"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 465
  ]
  edge [
    source 1
    target 466
  ]
  edge [
    source 1
    target 467
  ]
  edge [
    source 1
    target 468
  ]
  edge [
    source 1
    target 469
  ]
  edge [
    source 1
    target 470
  ]
  edge [
    source 1
    target 471
  ]
  edge [
    source 1
    target 472
  ]
  edge [
    source 1
    target 473
  ]
  edge [
    source 1
    target 474
  ]
  edge [
    source 1
    target 475
  ]
  edge [
    source 1
    target 476
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 1
    target 478
  ]
  edge [
    source 1
    target 479
  ]
  edge [
    source 1
    target 480
  ]
  edge [
    source 1
    target 481
  ]
  edge [
    source 1
    target 482
  ]
  edge [
    source 1
    target 483
  ]
  edge [
    source 1
    target 484
  ]
  edge [
    source 1
    target 485
  ]
  edge [
    source 1
    target 486
  ]
  edge [
    source 1
    target 487
  ]
  edge [
    source 1
    target 488
  ]
  edge [
    source 1
    target 489
  ]
  edge [
    source 1
    target 490
  ]
  edge [
    source 1
    target 491
  ]
  edge [
    source 1
    target 492
  ]
  edge [
    source 1
    target 493
  ]
  edge [
    source 1
    target 494
  ]
  edge [
    source 1
    target 495
  ]
  edge [
    source 1
    target 496
  ]
  edge [
    source 1
    target 497
  ]
  edge [
    source 1
    target 498
  ]
  edge [
    source 1
    target 499
  ]
  edge [
    source 1
    target 500
  ]
  edge [
    source 1
    target 501
  ]
  edge [
    source 1
    target 502
  ]
  edge [
    source 1
    target 503
  ]
  edge [
    source 1
    target 504
  ]
  edge [
    source 1
    target 505
  ]
  edge [
    source 1
    target 506
  ]
  edge [
    source 1
    target 507
  ]
  edge [
    source 1
    target 508
  ]
  edge [
    source 1
    target 509
  ]
  edge [
    source 1
    target 510
  ]
  edge [
    source 1
    target 511
  ]
  edge [
    source 1
    target 512
  ]
  edge [
    source 1
    target 513
  ]
  edge [
    source 1
    target 514
  ]
  edge [
    source 1
    target 515
  ]
  edge [
    source 1
    target 516
  ]
  edge [
    source 1
    target 517
  ]
  edge [
    source 1
    target 518
  ]
  edge [
    source 1
    target 519
  ]
  edge [
    source 1
    target 520
  ]
  edge [
    source 1
    target 521
  ]
  edge [
    source 1
    target 522
  ]
  edge [
    source 1
    target 523
  ]
  edge [
    source 1
    target 524
  ]
  edge [
    source 1
    target 525
  ]
  edge [
    source 1
    target 526
  ]
  edge [
    source 1
    target 527
  ]
  edge [
    source 1
    target 528
  ]
  edge [
    source 1
    target 529
  ]
  edge [
    source 1
    target 530
  ]
  edge [
    source 1
    target 531
  ]
  edge [
    source 1
    target 532
  ]
  edge [
    source 1
    target 533
  ]
  edge [
    source 1
    target 534
  ]
  edge [
    source 1
    target 535
  ]
  edge [
    source 1
    target 536
  ]
  edge [
    source 1
    target 537
  ]
  edge [
    source 1
    target 538
  ]
  edge [
    source 1
    target 539
  ]
  edge [
    source 1
    target 540
  ]
  edge [
    source 1
    target 541
  ]
  edge [
    source 1
    target 542
  ]
  edge [
    source 1
    target 543
  ]
  edge [
    source 1
    target 544
  ]
  edge [
    source 1
    target 545
  ]
  edge [
    source 1
    target 546
  ]
  edge [
    source 1
    target 547
  ]
  edge [
    source 1
    target 548
  ]
  edge [
    source 1
    target 549
  ]
  edge [
    source 1
    target 550
  ]
  edge [
    source 1
    target 551
  ]
  edge [
    source 1
    target 552
  ]
  edge [
    source 1
    target 553
  ]
  edge [
    source 1
    target 554
  ]
  edge [
    source 1
    target 555
  ]
  edge [
    source 1
    target 556
  ]
  edge [
    source 1
    target 557
  ]
  edge [
    source 1
    target 558
  ]
  edge [
    source 1
    target 559
  ]
  edge [
    source 1
    target 560
  ]
  edge [
    source 1
    target 561
  ]
  edge [
    source 1
    target 562
  ]
  edge [
    source 1
    target 563
  ]
  edge [
    source 1
    target 564
  ]
  edge [
    source 1
    target 565
  ]
  edge [
    source 1
    target 566
  ]
  edge [
    source 1
    target 567
  ]
  edge [
    source 1
    target 568
  ]
  edge [
    source 1
    target 569
  ]
  edge [
    source 1
    target 570
  ]
  edge [
    source 1
    target 571
  ]
  edge [
    source 1
    target 572
  ]
  edge [
    source 1
    target 573
  ]
  edge [
    source 1
    target 574
  ]
  edge [
    source 1
    target 575
  ]
  edge [
    source 1
    target 576
  ]
  edge [
    source 1
    target 577
  ]
  edge [
    source 1
    target 578
  ]
  edge [
    source 1
    target 579
  ]
  edge [
    source 1
    target 580
  ]
  edge [
    source 1
    target 581
  ]
  edge [
    source 1
    target 582
  ]
  edge [
    source 1
    target 583
  ]
  edge [
    source 1
    target 584
  ]
  edge [
    source 1
    target 585
  ]
  edge [
    source 1
    target 586
  ]
  edge [
    source 1
    target 587
  ]
  edge [
    source 1
    target 588
  ]
  edge [
    source 1
    target 589
  ]
  edge [
    source 1
    target 590
  ]
  edge [
    source 1
    target 591
  ]
  edge [
    source 1
    target 592
  ]
  edge [
    source 1
    target 593
  ]
  edge [
    source 1
    target 594
  ]
  edge [
    source 1
    target 595
  ]
  edge [
    source 1
    target 596
  ]
  edge [
    source 1
    target 597
  ]
  edge [
    source 1
    target 598
  ]
  edge [
    source 1
    target 599
  ]
  edge [
    source 1
    target 600
  ]
  edge [
    source 1
    target 601
  ]
  edge [
    source 1
    target 602
  ]
  edge [
    source 1
    target 603
  ]
  edge [
    source 1
    target 604
  ]
  edge [
    source 1
    target 605
  ]
  edge [
    source 1
    target 606
  ]
  edge [
    source 1
    target 607
  ]
  edge [
    source 1
    target 608
  ]
  edge [
    source 1
    target 609
  ]
  edge [
    source 1
    target 610
  ]
  edge [
    source 1
    target 611
  ]
  edge [
    source 1
    target 612
  ]
  edge [
    source 1
    target 613
  ]
  edge [
    source 1
    target 614
  ]
  edge [
    source 1
    target 615
  ]
  edge [
    source 1
    target 616
  ]
  edge [
    source 1
    target 617
  ]
  edge [
    source 1
    target 618
  ]
  edge [
    source 1
    target 619
  ]
  edge [
    source 1
    target 620
  ]
  edge [
    source 1
    target 621
  ]
  edge [
    source 1
    target 622
  ]
  edge [
    source 1
    target 623
  ]
  edge [
    source 1
    target 624
  ]
  edge [
    source 1
    target 625
  ]
  edge [
    source 1
    target 626
  ]
  edge [
    source 1
    target 627
  ]
  edge [
    source 1
    target 628
  ]
  edge [
    source 1
    target 629
  ]
  edge [
    source 1
    target 630
  ]
  edge [
    source 1
    target 631
  ]
  edge [
    source 1
    target 632
  ]
  edge [
    source 1
    target 633
  ]
  edge [
    source 1
    target 634
  ]
  edge [
    source 1
    target 635
  ]
  edge [
    source 1
    target 636
  ]
  edge [
    source 1
    target 637
  ]
  edge [
    source 1
    target 638
  ]
  edge [
    source 1
    target 639
  ]
  edge [
    source 1
    target 640
  ]
  edge [
    source 1
    target 641
  ]
  edge [
    source 1
    target 642
  ]
  edge [
    source 1
    target 643
  ]
  edge [
    source 1
    target 644
  ]
  edge [
    source 1
    target 645
  ]
  edge [
    source 1
    target 646
  ]
  edge [
    source 1
    target 647
  ]
  edge [
    source 1
    target 648
  ]
  edge [
    source 1
    target 649
  ]
  edge [
    source 1
    target 650
  ]
  edge [
    source 1
    target 651
  ]
  edge [
    source 1
    target 652
  ]
  edge [
    source 1
    target 653
  ]
  edge [
    source 1
    target 654
  ]
  edge [
    source 1
    target 655
  ]
  edge [
    source 1
    target 656
  ]
  edge [
    source 1
    target 657
  ]
  edge [
    source 1
    target 658
  ]
  edge [
    source 1
    target 659
  ]
  edge [
    source 1
    target 660
  ]
  edge [
    source 1
    target 661
  ]
  edge [
    source 1
    target 662
  ]
  edge [
    source 1
    target 663
  ]
  edge [
    source 1
    target 664
  ]
  edge [
    source 1
    target 665
  ]
  edge [
    source 1
    target 666
  ]
  edge [
    source 1
    target 667
  ]
  edge [
    source 1
    target 668
  ]
  edge [
    source 1
    target 669
  ]
  edge [
    source 1
    target 670
  ]
  edge [
    source 1
    target 671
  ]
  edge [
    source 1
    target 672
  ]
  edge [
    source 1
    target 673
  ]
  edge [
    source 1
    target 674
  ]
  edge [
    source 1
    target 675
  ]
  edge [
    source 1
    target 676
  ]
  edge [
    source 1
    target 677
  ]
  edge [
    source 1
    target 678
  ]
  edge [
    source 1
    target 679
  ]
  edge [
    source 1
    target 680
  ]
  edge [
    source 1
    target 681
  ]
  edge [
    source 1
    target 682
  ]
  edge [
    source 1
    target 683
  ]
  edge [
    source 1
    target 684
  ]
  edge [
    source 1
    target 685
  ]
  edge [
    source 1
    target 686
  ]
  edge [
    source 1
    target 687
  ]
  edge [
    source 1
    target 688
  ]
  edge [
    source 1
    target 689
  ]
  edge [
    source 1
    target 690
  ]
  edge [
    source 1
    target 691
  ]
  edge [
    source 1
    target 692
  ]
  edge [
    source 1
    target 693
  ]
  edge [
    source 1
    target 694
  ]
  edge [
    source 1
    target 695
  ]
  edge [
    source 1
    target 696
  ]
  edge [
    source 1
    target 697
  ]
  edge [
    source 1
    target 698
  ]
  edge [
    source 1
    target 699
  ]
  edge [
    source 1
    target 700
  ]
  edge [
    source 1
    target 701
  ]
  edge [
    source 1
    target 702
  ]
  edge [
    source 1
    target 703
  ]
  edge [
    source 1
    target 704
  ]
  edge [
    source 1
    target 705
  ]
  edge [
    source 1
    target 706
  ]
  edge [
    source 1
    target 707
  ]
  edge [
    source 1
    target 708
  ]
  edge [
    source 1
    target 709
  ]
  edge [
    source 1
    target 710
  ]
  edge [
    source 1
    target 711
  ]
  edge [
    source 1
    target 712
  ]
  edge [
    source 1
    target 713
  ]
  edge [
    source 1
    target 714
  ]
  edge [
    source 1
    target 715
  ]
  edge [
    source 1
    target 716
  ]
  edge [
    source 1
    target 717
  ]
  edge [
    source 1
    target 718
  ]
  edge [
    source 1
    target 719
  ]
  edge [
    source 1
    target 720
  ]
  edge [
    source 1
    target 721
  ]
  edge [
    source 1
    target 722
  ]
  edge [
    source 1
    target 723
  ]
  edge [
    source 1
    target 724
  ]
  edge [
    source 1
    target 725
  ]
  edge [
    source 1
    target 726
  ]
  edge [
    source 1
    target 727
  ]
  edge [
    source 1
    target 728
  ]
  edge [
    source 1
    target 729
  ]
  edge [
    source 1
    target 730
  ]
  edge [
    source 1
    target 731
  ]
  edge [
    source 1
    target 732
  ]
  edge [
    source 1
    target 733
  ]
  edge [
    source 1
    target 734
  ]
  edge [
    source 1
    target 735
  ]
  edge [
    source 1
    target 736
  ]
  edge [
    source 1
    target 737
  ]
  edge [
    source 1
    target 738
  ]
  edge [
    source 1
    target 739
  ]
  edge [
    source 1
    target 740
  ]
  edge [
    source 1
    target 741
  ]
  edge [
    source 1
    target 742
  ]
  edge [
    source 1
    target 743
  ]
  edge [
    source 1
    target 744
  ]
  edge [
    source 1
    target 745
  ]
  edge [
    source 1
    target 746
  ]
  edge [
    source 1
    target 747
  ]
  edge [
    source 1
    target 748
  ]
  edge [
    source 1
    target 749
  ]
  edge [
    source 1
    target 750
  ]
  edge [
    source 1
    target 751
  ]
  edge [
    source 1
    target 752
  ]
  edge [
    source 1
    target 753
  ]
  edge [
    source 1
    target 754
  ]
  edge [
    source 1
    target 755
  ]
  edge [
    source 1
    target 756
  ]
  edge [
    source 1
    target 757
  ]
  edge [
    source 1
    target 758
  ]
  edge [
    source 1
    target 759
  ]
  edge [
    source 1
    target 760
  ]
  edge [
    source 1
    target 761
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 762
  ]
  edge [
    source 1
    target 763
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 764
  ]
  edge [
    source 3
    target 765
  ]
  edge [
    source 3
    target 766
  ]
  edge [
    source 3
    target 767
  ]
  edge [
    source 3
    target 768
  ]
  edge [
    source 3
    target 769
  ]
  edge [
    source 3
    target 770
  ]
  edge [
    source 3
    target 771
  ]
  edge [
    source 3
    target 772
  ]
  edge [
    source 3
    target 773
  ]
  edge [
    source 3
    target 774
  ]
  edge [
    source 3
    target 775
  ]
  edge [
    source 3
    target 776
  ]
  edge [
    source 3
    target 777
  ]
  edge [
    source 3
    target 778
  ]
  edge [
    source 3
    target 779
  ]
  edge [
    source 3
    target 780
  ]
  edge [
    source 3
    target 781
  ]
  edge [
    source 3
    target 782
  ]
  edge [
    source 3
    target 783
  ]
  edge [
    source 3
    target 784
  ]
  edge [
    source 3
    target 785
  ]
  edge [
    source 3
    target 786
  ]
  edge [
    source 3
    target 787
  ]
  edge [
    source 3
    target 788
  ]
  edge [
    source 3
    target 789
  ]
  edge [
    source 3
    target 790
  ]
  edge [
    source 3
    target 791
  ]
  edge [
    source 3
    target 792
  ]
  edge [
    source 3
    target 793
  ]
  edge [
    source 3
    target 794
  ]
  edge [
    source 3
    target 795
  ]
  edge [
    source 3
    target 796
  ]
  edge [
    source 3
    target 797
  ]
  edge [
    source 3
    target 798
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 799
  ]
  edge [
    source 4
    target 800
  ]
  edge [
    source 4
    target 801
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 802
  ]
  edge [
    source 4
    target 803
  ]
  edge [
    source 4
    target 804
  ]
  edge [
    source 4
    target 805
  ]
  edge [
    source 4
    target 806
  ]
  edge [
    source 4
    target 807
  ]
  edge [
    source 4
    target 808
  ]
  edge [
    source 4
    target 809
  ]
  edge [
    source 4
    target 810
  ]
  edge [
    source 4
    target 811
  ]
  edge [
    source 4
    target 812
  ]
  edge [
    source 4
    target 813
  ]
  edge [
    source 4
    target 814
  ]
  edge [
    source 4
    target 815
  ]
  edge [
    source 4
    target 816
  ]
  edge [
    source 4
    target 817
  ]
  edge [
    source 4
    target 818
  ]
  edge [
    source 4
    target 819
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 4
    target 824
  ]
  edge [
    source 4
    target 825
  ]
  edge [
    source 4
    target 826
  ]
  edge [
    source 4
    target 827
  ]
  edge [
    source 4
    target 828
  ]
  edge [
    source 4
    target 829
  ]
  edge [
    source 4
    target 830
  ]
  edge [
    source 4
    target 831
  ]
  edge [
    source 4
    target 832
  ]
  edge [
    source 4
    target 833
  ]
  edge [
    source 4
    target 834
  ]
  edge [
    source 4
    target 835
  ]
  edge [
    source 4
    target 836
  ]
  edge [
    source 4
    target 837
  ]
  edge [
    source 4
    target 838
  ]
  edge [
    source 4
    target 839
  ]
  edge [
    source 4
    target 840
  ]
  edge [
    source 4
    target 841
  ]
  edge [
    source 4
    target 842
  ]
  edge [
    source 4
    target 843
  ]
  edge [
    source 4
    target 844
  ]
  edge [
    source 4
    target 845
  ]
  edge [
    source 4
    target 846
  ]
  edge [
    source 4
    target 847
  ]
  edge [
    source 4
    target 848
  ]
  edge [
    source 4
    target 849
  ]
  edge [
    source 4
    target 850
  ]
  edge [
    source 4
    target 851
  ]
  edge [
    source 4
    target 852
  ]
  edge [
    source 4
    target 853
  ]
  edge [
    source 4
    target 854
  ]
  edge [
    source 4
    target 855
  ]
  edge [
    source 4
    target 856
  ]
  edge [
    source 4
    target 857
  ]
  edge [
    source 4
    target 858
  ]
  edge [
    source 4
    target 859
  ]
  edge [
    source 4
    target 860
  ]
  edge [
    source 4
    target 861
  ]
  edge [
    source 4
    target 862
  ]
  edge [
    source 4
    target 863
  ]
  edge [
    source 4
    target 864
  ]
  edge [
    source 4
    target 865
  ]
  edge [
    source 4
    target 866
  ]
  edge [
    source 4
    target 867
  ]
  edge [
    source 4
    target 868
  ]
  edge [
    source 4
    target 869
  ]
  edge [
    source 4
    target 870
  ]
  edge [
    source 4
    target 871
  ]
  edge [
    source 4
    target 872
  ]
  edge [
    source 4
    target 873
  ]
  edge [
    source 4
    target 874
  ]
  edge [
    source 4
    target 875
  ]
  edge [
    source 4
    target 876
  ]
  edge [
    source 4
    target 877
  ]
  edge [
    source 4
    target 878
  ]
  edge [
    source 4
    target 879
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 880
  ]
  edge [
    source 5
    target 881
  ]
  edge [
    source 5
    target 882
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 884
  ]
  edge [
    source 5
    target 885
  ]
  edge [
    source 5
    target 886
  ]
  edge [
    source 5
    target 887
  ]
  edge [
    source 5
    target 888
  ]
  edge [
    source 5
    target 889
  ]
  edge [
    source 5
    target 890
  ]
  edge [
    source 5
    target 891
  ]
  edge [
    source 5
    target 892
  ]
  edge [
    source 5
    target 893
  ]
  edge [
    source 5
    target 894
  ]
  edge [
    source 5
    target 895
  ]
  edge [
    source 5
    target 896
  ]
  edge [
    source 5
    target 897
  ]
  edge [
    source 5
    target 898
  ]
  edge [
    source 5
    target 899
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 907
  ]
  edge [
    source 8
    target 908
  ]
  edge [
    source 8
    target 909
  ]
  edge [
    source 8
    target 910
  ]
  edge [
    source 8
    target 911
  ]
  edge [
    source 8
    target 912
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 913
  ]
  edge [
    source 8
    target 914
  ]
  edge [
    source 8
    target 915
  ]
  edge [
    source 8
    target 916
  ]
  edge [
    source 8
    target 917
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 918
  ]
  edge [
    source 8
    target 919
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 921
  ]
  edge [
    source 8
    target 922
  ]
  edge [
    source 8
    target 923
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 925
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 942
  ]
  edge [
    source 9
    target 943
  ]
  edge [
    source 9
    target 944
  ]
  edge [
    source 9
    target 945
  ]
  edge [
    source 9
    target 946
  ]
  edge [
    source 9
    target 947
  ]
  edge [
    source 9
    target 948
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 9
    target 952
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 9
    target 962
  ]
  edge [
    source 9
    target 963
  ]
  edge [
    source 9
    target 964
  ]
  edge [
    source 9
    target 965
  ]
  edge [
    source 9
    target 966
  ]
  edge [
    source 9
    target 967
  ]
  edge [
    source 9
    target 968
  ]
  edge [
    source 9
    target 969
  ]
  edge [
    source 9
    target 970
  ]
  edge [
    source 9
    target 971
  ]
  edge [
    source 9
    target 972
  ]
  edge [
    source 9
    target 973
  ]
  edge [
    source 9
    target 974
  ]
  edge [
    source 9
    target 975
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 976
  ]
  edge [
    source 10
    target 977
  ]
  edge [
    source 10
    target 978
  ]
  edge [
    source 10
    target 979
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 980
  ]
  edge [
    source 11
    target 981
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 982
  ]
  edge [
    source 11
    target 983
  ]
  edge [
    source 11
    target 984
  ]
  edge [
    source 11
    target 985
  ]
  edge [
    source 11
    target 986
  ]
  edge [
    source 11
    target 987
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 988
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 989
  ]
  edge [
    source 11
    target 990
  ]
  edge [
    source 11
    target 991
  ]
  edge [
    source 11
    target 992
  ]
  edge [
    source 11
    target 993
  ]
  edge [
    source 11
    target 994
  ]
  edge [
    source 11
    target 995
  ]
  edge [
    source 11
    target 996
  ]
  edge [
    source 11
    target 997
  ]
  edge [
    source 11
    target 998
  ]
  edge [
    source 11
    target 999
  ]
  edge [
    source 11
    target 1000
  ]
  edge [
    source 11
    target 1001
  ]
  edge [
    source 11
    target 1002
  ]
  edge [
    source 11
    target 1003
  ]
  edge [
    source 11
    target 1004
  ]
  edge [
    source 11
    target 1005
  ]
  edge [
    source 11
    target 1006
  ]
  edge [
    source 11
    target 1007
  ]
  edge [
    source 11
    target 1008
  ]
  edge [
    source 11
    target 1009
  ]
  edge [
    source 11
    target 1010
  ]
  edge [
    source 11
    target 1011
  ]
  edge [
    source 11
    target 1012
  ]
  edge [
    source 11
    target 1013
  ]
  edge [
    source 11
    target 1014
  ]
  edge [
    source 11
    target 1015
  ]
  edge [
    source 11
    target 1016
  ]
  edge [
    source 11
    target 1017
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 1019
  ]
  edge [
    source 11
    target 1020
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1022
  ]
  edge [
    source 11
    target 1023
  ]
  edge [
    source 11
    target 1024
  ]
  edge [
    source 11
    target 1025
  ]
  edge [
    source 11
    target 1026
  ]
  edge [
    source 11
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 11
    target 1029
  ]
  edge [
    source 11
    target 1030
  ]
  edge [
    source 11
    target 1031
  ]
  edge [
    source 11
    target 1032
  ]
  edge [
    source 11
    target 1033
  ]
  edge [
    source 11
    target 1034
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 11
    target 1039
  ]
  edge [
    source 11
    target 1040
  ]
  edge [
    source 11
    target 1041
  ]
  edge [
    source 11
    target 1042
  ]
  edge [
    source 11
    target 1043
  ]
  edge [
    source 11
    target 1044
  ]
  edge [
    source 11
    target 1045
  ]
  edge [
    source 11
    target 1046
  ]
  edge [
    source 11
    target 1047
  ]
  edge [
    source 11
    target 1048
  ]
  edge [
    source 11
    target 1049
  ]
  edge [
    source 11
    target 1050
  ]
  edge [
    source 11
    target 1051
  ]
  edge [
    source 11
    target 1052
  ]
  edge [
    source 11
    target 1053
  ]
  edge [
    source 11
    target 1054
  ]
  edge [
    source 11
    target 1055
  ]
  edge [
    source 11
    target 1056
  ]
  edge [
    source 11
    target 1057
  ]
  edge [
    source 11
    target 1058
  ]
  edge [
    source 11
    target 1059
  ]
  edge [
    source 11
    target 1060
  ]
  edge [
    source 11
    target 1061
  ]
  edge [
    source 11
    target 1062
  ]
  edge [
    source 11
    target 1063
  ]
  edge [
    source 11
    target 1064
  ]
  edge [
    source 11
    target 1065
  ]
  edge [
    source 11
    target 1066
  ]
  edge [
    source 11
    target 1067
  ]
  edge [
    source 11
    target 1068
  ]
  edge [
    source 11
    target 1069
  ]
  edge [
    source 11
    target 1070
  ]
  edge [
    source 11
    target 1071
  ]
  edge [
    source 11
    target 1072
  ]
  edge [
    source 11
    target 1073
  ]
  edge [
    source 11
    target 1074
  ]
  edge [
    source 11
    target 1075
  ]
  edge [
    source 11
    target 1076
  ]
  edge [
    source 11
    target 1077
  ]
  edge [
    source 11
    target 1078
  ]
  edge [
    source 11
    target 1079
  ]
  edge [
    source 11
    target 1080
  ]
  edge [
    source 11
    target 1081
  ]
  edge [
    source 11
    target 1082
  ]
  edge [
    source 11
    target 1083
  ]
  edge [
    source 11
    target 1084
  ]
  edge [
    source 11
    target 1085
  ]
  edge [
    source 11
    target 1086
  ]
  edge [
    source 11
    target 1087
  ]
  edge [
    source 11
    target 1088
  ]
  edge [
    source 11
    target 1089
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1090
  ]
  edge [
    source 13
    target 1091
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 1092
  ]
  edge [
    source 13
    target 1093
  ]
  edge [
    source 13
    target 1094
  ]
  edge [
    source 13
    target 1095
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 1096
  ]
  edge [
    source 13
    target 1097
  ]
  edge [
    source 13
    target 1098
  ]
  edge [
    source 13
    target 1099
  ]
  edge [
    source 13
    target 1100
  ]
  edge [
    source 13
    target 1101
  ]
  edge [
    source 13
    target 1102
  ]
  edge [
    source 13
    target 1103
  ]
  edge [
    source 13
    target 1104
  ]
  edge [
    source 13
    target 1105
  ]
  edge [
    source 13
    target 1106
  ]
  edge [
    source 13
    target 1107
  ]
  edge [
    source 13
    target 1108
  ]
  edge [
    source 13
    target 1109
  ]
  edge [
    source 13
    target 1110
  ]
  edge [
    source 13
    target 1111
  ]
  edge [
    source 13
    target 1112
  ]
  edge [
    source 13
    target 1113
  ]
  edge [
    source 13
    target 1114
  ]
  edge [
    source 13
    target 1115
  ]
  edge [
    source 13
    target 1116
  ]
  edge [
    source 13
    target 1117
  ]
  edge [
    source 13
    target 1118
  ]
  edge [
    source 13
    target 1119
  ]
  edge [
    source 13
    target 1120
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 1121
  ]
  edge [
    source 13
    target 1122
  ]
  edge [
    source 13
    target 1123
  ]
  edge [
    source 13
    target 1011
  ]
  edge [
    source 13
    target 1124
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 1125
  ]
  edge [
    source 13
    target 1126
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 1127
  ]
  edge [
    source 13
    target 1128
  ]
  edge [
    source 13
    target 1129
  ]
  edge [
    source 13
    target 1130
  ]
  edge [
    source 13
    target 1131
  ]
  edge [
    source 13
    target 1132
  ]
  edge [
    source 13
    target 1133
  ]
  edge [
    source 13
    target 1134
  ]
  edge [
    source 13
    target 1135
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 1148
  ]
  edge [
    source 14
    target 1149
  ]
  edge [
    source 14
    target 1150
  ]
  edge [
    source 14
    target 1151
  ]
  edge [
    source 14
    target 1152
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 1178
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 1179
  ]
  edge [
    source 15
    target 1180
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 1181
  ]
  edge [
    source 15
    target 1182
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 1183
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1184
  ]
  edge [
    source 16
    target 1185
  ]
  edge [
    source 16
    target 1186
  ]
  edge [
    source 16
    target 1187
  ]
  edge [
    source 16
    target 1188
  ]
  edge [
    source 16
    target 1189
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 1190
  ]
  edge [
    source 16
    target 1191
  ]
  edge [
    source 16
    target 1192
  ]
  edge [
    source 16
    target 1193
  ]
  edge [
    source 16
    target 1194
  ]
  edge [
    source 16
    target 1195
  ]
  edge [
    source 16
    target 1196
  ]
  edge [
    source 16
    target 1197
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1198
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 1199
  ]
  edge [
    source 16
    target 1200
  ]
  edge [
    source 16
    target 1201
  ]
  edge [
    source 16
    target 1202
  ]
  edge [
    source 16
    target 1203
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1154
  ]
  edge [
    source 16
    target 1204
  ]
  edge [
    source 16
    target 1205
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 1206
  ]
  edge [
    source 16
    target 1207
  ]
  edge [
    source 16
    target 467
  ]
  edge [
    source 16
    target 1208
  ]
  edge [
    source 16
    target 1209
  ]
  edge [
    source 16
    target 1210
  ]
  edge [
    source 16
    target 1211
  ]
  edge [
    source 16
    target 1212
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 1213
  ]
  edge [
    source 16
    target 1214
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 1215
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 1216
  ]
  edge [
    source 16
    target 1217
  ]
  edge [
    source 16
    target 1218
  ]
  edge [
    source 16
    target 1219
  ]
  edge [
    source 16
    target 1220
  ]
  edge [
    source 16
    target 1221
  ]
  edge [
    source 16
    target 1222
  ]
  edge [
    source 16
    target 1223
  ]
  edge [
    source 16
    target 1224
  ]
  edge [
    source 16
    target 1225
  ]
  edge [
    source 16
    target 1226
  ]
  edge [
    source 16
    target 1227
  ]
  edge [
    source 16
    target 1228
  ]
  edge [
    source 16
    target 1229
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1233
  ]
  edge [
    source 17
    target 1234
  ]
  edge [
    source 17
    target 1235
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 17
    target 1237
  ]
  edge [
    source 17
    target 1238
  ]
  edge [
    source 17
    target 1239
  ]
  edge [
    source 17
    target 1240
  ]
  edge [
    source 17
    target 1241
  ]
  edge [
    source 17
    target 1242
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1243
  ]
  edge [
    source 17
    target 1244
  ]
  edge [
    source 17
    target 1245
  ]
  edge [
    source 17
    target 1246
  ]
  edge [
    source 17
    target 1247
  ]
  edge [
    source 17
    target 1248
  ]
  edge [
    source 17
    target 1249
  ]
  edge [
    source 17
    target 1250
  ]
  edge [
    source 17
    target 1251
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 1252
  ]
  edge [
    source 17
    target 1253
  ]
  edge [
    source 17
    target 1254
  ]
  edge [
    source 17
    target 1255
  ]
  edge [
    source 17
    target 1256
  ]
  edge [
    source 17
    target 1257
  ]
  edge [
    source 17
    target 1258
  ]
  edge [
    source 17
    target 1259
  ]
  edge [
    source 17
    target 1260
  ]
  edge [
    source 17
    target 1261
  ]
  edge [
    source 17
    target 1262
  ]
  edge [
    source 17
    target 1263
  ]
  edge [
    source 17
    target 1264
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 1265
  ]
  edge [
    source 17
    target 1266
  ]
  edge [
    source 17
    target 1267
  ]
  edge [
    source 17
    target 1268
  ]
  edge [
    source 17
    target 1269
  ]
  edge [
    source 17
    target 1270
  ]
  edge [
    source 17
    target 1271
  ]
  edge [
    source 17
    target 1272
  ]
  edge [
    source 17
    target 1273
  ]
  edge [
    source 17
    target 1274
  ]
  edge [
    source 17
    target 1275
  ]
  edge [
    source 17
    target 1276
  ]
  edge [
    source 17
    target 1277
  ]
  edge [
    source 17
    target 1278
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 1279
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1280
  ]
  edge [
    source 17
    target 1281
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 17
    target 1295
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 1298
  ]
  edge [
    source 17
    target 1299
  ]
  edge [
    source 17
    target 1300
  ]
  edge [
    source 17
    target 1301
  ]
  edge [
    source 17
    target 1302
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 1304
  ]
  edge [
    source 17
    target 1305
  ]
  edge [
    source 17
    target 1306
  ]
  edge [
    source 17
    target 1307
  ]
  edge [
    source 17
    target 1308
  ]
  edge [
    source 17
    target 1309
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 1310
  ]
  edge [
    source 17
    target 1311
  ]
  edge [
    source 17
    target 1312
  ]
  edge [
    source 17
    target 1313
  ]
  edge [
    source 17
    target 1314
  ]
  edge [
    source 17
    target 1315
  ]
  edge [
    source 17
    target 1316
  ]
  edge [
    source 17
    target 1317
  ]
  edge [
    source 17
    target 1318
  ]
  edge [
    source 17
    target 1319
  ]
  edge [
    source 17
    target 1320
  ]
  edge [
    source 17
    target 1321
  ]
  edge [
    source 17
    target 1322
  ]
  edge [
    source 17
    target 1323
  ]
  edge [
    source 17
    target 1324
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1325
  ]
  edge [
    source 17
    target 1326
  ]
  edge [
    source 17
    target 1327
  ]
  edge [
    source 17
    target 1328
  ]
  edge [
    source 17
    target 1329
  ]
  edge [
    source 17
    target 1330
  ]
  edge [
    source 17
    target 1331
  ]
  edge [
    source 17
    target 1332
  ]
  edge [
    source 17
    target 1333
  ]
  edge [
    source 17
    target 1334
  ]
  edge [
    source 17
    target 1335
  ]
  edge [
    source 17
    target 47
  ]
  edge [
    source 17
    target 1336
  ]
  edge [
    source 17
    target 1337
  ]
  edge [
    source 17
    target 1338
  ]
  edge [
    source 17
    target 1339
  ]
  edge [
    source 17
    target 1340
  ]
  edge [
    source 17
    target 1341
  ]
  edge [
    source 17
    target 1342
  ]
  edge [
    source 17
    target 1343
  ]
  edge [
    source 17
    target 1344
  ]
  edge [
    source 17
    target 1345
  ]
  edge [
    source 17
    target 1346
  ]
  edge [
    source 17
    target 1347
  ]
  edge [
    source 17
    target 1348
  ]
  edge [
    source 17
    target 1349
  ]
  edge [
    source 17
    target 1350
  ]
  edge [
    source 17
    target 1351
  ]
  edge [
    source 17
    target 1352
  ]
  edge [
    source 17
    target 1353
  ]
  edge [
    source 17
    target 1354
  ]
  edge [
    source 17
    target 1355
  ]
  edge [
    source 17
    target 1356
  ]
  edge [
    source 17
    target 1357
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1358
  ]
  edge [
    source 17
    target 1359
  ]
  edge [
    source 17
    target 1360
  ]
  edge [
    source 17
    target 1361
  ]
  edge [
    source 17
    target 1362
  ]
  edge [
    source 17
    target 1363
  ]
  edge [
    source 17
    target 1364
  ]
  edge [
    source 17
    target 1365
  ]
  edge [
    source 17
    target 1366
  ]
  edge [
    source 17
    target 1367
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1368
  ]
  edge [
    source 17
    target 1369
  ]
  edge [
    source 17
    target 1370
  ]
  edge [
    source 17
    target 1371
  ]
  edge [
    source 17
    target 1372
  ]
  edge [
    source 17
    target 1373
  ]
  edge [
    source 17
    target 1374
  ]
  edge [
    source 17
    target 1375
  ]
  edge [
    source 17
    target 1376
  ]
  edge [
    source 17
    target 1377
  ]
  edge [
    source 17
    target 1378
  ]
  edge [
    source 17
    target 1379
  ]
  edge [
    source 17
    target 1380
  ]
  edge [
    source 17
    target 1381
  ]
  edge [
    source 17
    target 1382
  ]
  edge [
    source 17
    target 1383
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 1385
  ]
  edge [
    source 17
    target 1386
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1388
  ]
  edge [
    source 17
    target 1389
  ]
  edge [
    source 17
    target 1390
  ]
  edge [
    source 17
    target 1391
  ]
  edge [
    source 17
    target 1392
  ]
  edge [
    source 17
    target 1393
  ]
  edge [
    source 17
    target 1394
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 1398
  ]
  edge [
    source 17
    target 1399
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 1407
  ]
  edge [
    source 17
    target 1408
  ]
  edge [
    source 17
    target 1409
  ]
  edge [
    source 17
    target 1410
  ]
  edge [
    source 17
    target 1411
  ]
  edge [
    source 17
    target 1412
  ]
  edge [
    source 17
    target 1413
  ]
  edge [
    source 17
    target 1414
  ]
  edge [
    source 17
    target 1415
  ]
  edge [
    source 17
    target 1416
  ]
  edge [
    source 17
    target 1417
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1419
  ]
  edge [
    source 17
    target 1420
  ]
  edge [
    source 17
    target 1421
  ]
  edge [
    source 17
    target 1422
  ]
  edge [
    source 17
    target 1423
  ]
  edge [
    source 17
    target 1424
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 1427
  ]
  edge [
    source 17
    target 1428
  ]
  edge [
    source 17
    target 1429
  ]
  edge [
    source 17
    target 1430
  ]
  edge [
    source 17
    target 1431
  ]
  edge [
    source 17
    target 1432
  ]
  edge [
    source 17
    target 1433
  ]
  edge [
    source 17
    target 1434
  ]
  edge [
    source 17
    target 1435
  ]
  edge [
    source 17
    target 1436
  ]
  edge [
    source 17
    target 1437
  ]
  edge [
    source 17
    target 1438
  ]
  edge [
    source 17
    target 1439
  ]
  edge [
    source 17
    target 1440
  ]
  edge [
    source 17
    target 1441
  ]
  edge [
    source 17
    target 1442
  ]
  edge [
    source 17
    target 1443
  ]
  edge [
    source 17
    target 1444
  ]
  edge [
    source 17
    target 1445
  ]
  edge [
    source 17
    target 1446
  ]
  edge [
    source 17
    target 1447
  ]
  edge [
    source 17
    target 1448
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 1449
  ]
  edge [
    source 17
    target 1450
  ]
  edge [
    source 17
    target 1451
  ]
  edge [
    source 17
    target 1452
  ]
  edge [
    source 17
    target 1453
  ]
  edge [
    source 17
    target 1131
  ]
  edge [
    source 17
    target 1454
  ]
  edge [
    source 17
    target 1455
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 1456
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1457
  ]
  edge [
    source 17
    target 1458
  ]
  edge [
    source 17
    target 1459
  ]
  edge [
    source 17
    target 1460
  ]
  edge [
    source 17
    target 1461
  ]
  edge [
    source 17
    target 1462
  ]
  edge [
    source 17
    target 1463
  ]
  edge [
    source 17
    target 1464
  ]
  edge [
    source 17
    target 1465
  ]
  edge [
    source 17
    target 1466
  ]
  edge [
    source 17
    target 1467
  ]
  edge [
    source 17
    target 1468
  ]
  edge [
    source 17
    target 1469
  ]
  edge [
    source 17
    target 1470
  ]
  edge [
    source 17
    target 1471
  ]
  edge [
    source 17
    target 1472
  ]
  edge [
    source 17
    target 1473
  ]
  edge [
    source 17
    target 1474
  ]
  edge [
    source 17
    target 1475
  ]
  edge [
    source 17
    target 1476
  ]
  edge [
    source 17
    target 1477
  ]
  edge [
    source 17
    target 1478
  ]
  edge [
    source 17
    target 1479
  ]
  edge [
    source 17
    target 1480
  ]
  edge [
    source 17
    target 1481
  ]
  edge [
    source 17
    target 1482
  ]
  edge [
    source 17
    target 1483
  ]
  edge [
    source 17
    target 1484
  ]
  edge [
    source 17
    target 1485
  ]
  edge [
    source 17
    target 1486
  ]
  edge [
    source 17
    target 1487
  ]
  edge [
    source 17
    target 1488
  ]
  edge [
    source 17
    target 1489
  ]
  edge [
    source 17
    target 1490
  ]
  edge [
    source 17
    target 1491
  ]
  edge [
    source 17
    target 1492
  ]
  edge [
    source 17
    target 1493
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 1494
  ]
  edge [
    source 17
    target 1495
  ]
  edge [
    source 17
    target 1496
  ]
  edge [
    source 17
    target 1497
  ]
  edge [
    source 17
    target 1498
  ]
  edge [
    source 17
    target 1499
  ]
  edge [
    source 17
    target 1500
  ]
  edge [
    source 17
    target 1501
  ]
  edge [
    source 17
    target 1502
  ]
  edge [
    source 17
    target 1503
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1504
  ]
  edge [
    source 17
    target 1505
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1506
  ]
  edge [
    source 17
    target 1507
  ]
  edge [
    source 17
    target 1508
  ]
  edge [
    source 17
    target 1509
  ]
  edge [
    source 17
    target 1510
  ]
  edge [
    source 17
    target 1511
  ]
  edge [
    source 17
    target 1512
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 1513
  ]
  edge [
    source 17
    target 1514
  ]
  edge [
    source 17
    target 1515
  ]
  edge [
    source 17
    target 1516
  ]
  edge [
    source 17
    target 1517
  ]
  edge [
    source 17
    target 1518
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1185
  ]
  edge [
    source 18
    target 1519
  ]
  edge [
    source 18
    target 1520
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 1521
  ]
  edge [
    source 18
    target 1522
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 1523
  ]
  edge [
    source 18
    target 1524
  ]
  edge [
    source 18
    target 1525
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1526
  ]
  edge [
    source 18
    target 1091
  ]
  edge [
    source 18
    target 1527
  ]
  edge [
    source 18
    target 1528
  ]
  edge [
    source 18
    target 1435
  ]
  edge [
    source 18
    target 1529
  ]
  edge [
    source 18
    target 1530
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 1531
  ]
  edge [
    source 18
    target 1532
  ]
  edge [
    source 18
    target 1533
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 1534
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 1535
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 1536
  ]
  edge [
    source 18
    target 1537
  ]
  edge [
    source 18
    target 1538
  ]
  edge [
    source 18
    target 1539
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 1540
  ]
  edge [
    source 18
    target 1541
  ]
  edge [
    source 18
    target 1542
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1225
  ]
  edge [
    source 18
    target 1543
  ]
  edge [
    source 18
    target 1544
  ]
  edge [
    source 18
    target 1213
  ]
  edge [
    source 18
    target 1214
  ]
  edge [
    source 18
    target 1215
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 1216
  ]
  edge [
    source 18
    target 1217
  ]
  edge [
    source 18
    target 1545
  ]
  edge [
    source 18
    target 1546
  ]
  edge [
    source 18
    target 1547
  ]
  edge [
    source 18
    target 1548
  ]
  edge [
    source 18
    target 1549
  ]
  edge [
    source 18
    target 1550
  ]
  edge [
    source 18
    target 1551
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 1552
  ]
  edge [
    source 18
    target 1553
  ]
  edge [
    source 18
    target 1554
  ]
  edge [
    source 18
    target 1555
  ]
  edge [
    source 18
    target 1556
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 1557
  ]
  edge [
    source 18
    target 1558
  ]
  edge [
    source 18
    target 1559
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1560
  ]
  edge [
    source 18
    target 1561
  ]
  edge [
    source 18
    target 1562
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 1563
  ]
  edge [
    source 18
    target 1564
  ]
  edge [
    source 18
    target 1565
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 1566
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1567
  ]
  edge [
    source 20
    target 1568
  ]
  edge [
    source 20
    target 842
  ]
  edge [
    source 20
    target 1569
  ]
  edge [
    source 20
    target 1570
  ]
  edge [
    source 20
    target 1571
  ]
  edge [
    source 20
    target 1572
  ]
  edge [
    source 20
    target 1193
  ]
  edge [
    source 20
    target 1573
  ]
  edge [
    source 20
    target 1574
  ]
  edge [
    source 20
    target 1575
  ]
  edge [
    source 20
    target 1576
  ]
  edge [
    source 20
    target 1577
  ]
  edge [
    source 20
    target 1578
  ]
  edge [
    source 20
    target 1554
  ]
  edge [
    source 20
    target 1579
  ]
  edge [
    source 20
    target 811
  ]
  edge [
    source 20
    target 1580
  ]
  edge [
    source 20
    target 1581
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 697
  ]
  edge [
    source 23
    target 698
  ]
  edge [
    source 23
    target 699
  ]
  edge [
    source 23
    target 700
  ]
  edge [
    source 23
    target 701
  ]
  edge [
    source 23
    target 702
  ]
  edge [
    source 23
    target 704
  ]
  edge [
    source 23
    target 703
  ]
  edge [
    source 23
    target 705
  ]
  edge [
    source 23
    target 706
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 710
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 467
  ]
  edge [
    source 23
    target 1582
  ]
  edge [
    source 23
    target 1583
  ]
  edge [
    source 23
    target 1584
  ]
  edge [
    source 23
    target 1528
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1585
  ]
  edge [
    source 23
    target 1521
  ]
  edge [
    source 23
    target 1586
  ]
  edge [
    source 23
    target 1587
  ]
  edge [
    source 23
    target 1154
  ]
  edge [
    source 23
    target 1534
  ]
  edge [
    source 23
    target 1588
  ]
  edge [
    source 23
    target 1589
  ]
  edge [
    source 23
    target 1590
  ]
  edge [
    source 23
    target 1591
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1592
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1593
  ]
  edge [
    source 23
    target 1523
  ]
  edge [
    source 23
    target 1594
  ]
  edge [
    source 23
    target 1595
  ]
  edge [
    source 23
    target 1596
  ]
  edge [
    source 23
    target 659
  ]
  edge [
    source 23
    target 660
  ]
  edge [
    source 23
    target 661
  ]
  edge [
    source 23
    target 662
  ]
  edge [
    source 23
    target 663
  ]
  edge [
    source 23
    target 664
  ]
  edge [
    source 23
    target 665
  ]
  edge [
    source 23
    target 666
  ]
  edge [
    source 23
    target 667
  ]
  edge [
    source 23
    target 668
  ]
  edge [
    source 23
    target 669
  ]
  edge [
    source 23
    target 670
  ]
  edge [
    source 23
    target 671
  ]
  edge [
    source 23
    target 672
  ]
  edge [
    source 23
    target 673
  ]
  edge [
    source 23
    target 674
  ]
  edge [
    source 23
    target 675
  ]
  edge [
    source 23
    target 676
  ]
  edge [
    source 23
    target 677
  ]
  edge [
    source 23
    target 678
  ]
  edge [
    source 23
    target 679
  ]
  edge [
    source 23
    target 680
  ]
  edge [
    source 23
    target 681
  ]
  edge [
    source 23
    target 682
  ]
  edge [
    source 23
    target 683
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 23
    target 685
  ]
  edge [
    source 23
    target 686
  ]
  edge [
    source 23
    target 1597
  ]
  edge [
    source 23
    target 1598
  ]
  edge [
    source 23
    target 1599
  ]
  edge [
    source 23
    target 1600
  ]
  edge [
    source 23
    target 1601
  ]
  edge [
    source 23
    target 1602
  ]
  edge [
    source 23
    target 1603
  ]
  edge [
    source 23
    target 1604
  ]
  edge [
    source 23
    target 1605
  ]
  edge [
    source 23
    target 1606
  ]
  edge [
    source 23
    target 1607
  ]
  edge [
    source 23
    target 1608
  ]
  edge [
    source 23
    target 1609
  ]
  edge [
    source 23
    target 1610
  ]
  edge [
    source 23
    target 1611
  ]
  edge [
    source 23
    target 1612
  ]
  edge [
    source 23
    target 1613
  ]
  edge [
    source 23
    target 1614
  ]
  edge [
    source 23
    target 1615
  ]
  edge [
    source 23
    target 1616
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 1617
  ]
  edge [
    source 23
    target 1618
  ]
  edge [
    source 23
    target 1619
  ]
  edge [
    source 23
    target 1620
  ]
  edge [
    source 23
    target 1621
  ]
  edge [
    source 23
    target 1622
  ]
  edge [
    source 23
    target 1623
  ]
  edge [
    source 23
    target 1624
  ]
  edge [
    source 23
    target 1625
  ]
  edge [
    source 23
    target 1626
  ]
  edge [
    source 23
    target 1627
  ]
  edge [
    source 23
    target 1628
  ]
  edge [
    source 23
    target 1629
  ]
  edge [
    source 23
    target 1630
  ]
  edge [
    source 23
    target 1631
  ]
  edge [
    source 23
    target 1632
  ]
  edge [
    source 23
    target 1633
  ]
  edge [
    source 23
    target 1634
  ]
  edge [
    source 23
    target 1635
  ]
  edge [
    source 23
    target 1636
  ]
  edge [
    source 23
    target 1637
  ]
  edge [
    source 23
    target 1638
  ]
  edge [
    source 23
    target 1639
  ]
  edge [
    source 23
    target 1640
  ]
  edge [
    source 23
    target 1641
  ]
  edge [
    source 23
    target 1642
  ]
  edge [
    source 23
    target 1643
  ]
  edge [
    source 23
    target 1644
  ]
  edge [
    source 23
    target 1645
  ]
  edge [
    source 23
    target 1646
  ]
  edge [
    source 23
    target 1647
  ]
  edge [
    source 23
    target 1648
  ]
  edge [
    source 23
    target 1649
  ]
  edge [
    source 23
    target 1650
  ]
  edge [
    source 23
    target 1651
  ]
  edge [
    source 23
    target 1652
  ]
  edge [
    source 23
    target 1653
  ]
  edge [
    source 23
    target 1654
  ]
  edge [
    source 23
    target 1655
  ]
  edge [
    source 23
    target 1656
  ]
  edge [
    source 23
    target 1657
  ]
  edge [
    source 23
    target 1658
  ]
  edge [
    source 23
    target 1659
  ]
  edge [
    source 23
    target 1660
  ]
  edge [
    source 23
    target 1661
  ]
  edge [
    source 23
    target 1662
  ]
  edge [
    source 23
    target 1663
  ]
  edge [
    source 23
    target 1664
  ]
  edge [
    source 23
    target 1665
  ]
  edge [
    source 23
    target 1666
  ]
  edge [
    source 23
    target 1667
  ]
  edge [
    source 23
    target 1668
  ]
  edge [
    source 23
    target 1669
  ]
  edge [
    source 23
    target 1670
  ]
  edge [
    source 23
    target 1671
  ]
  edge [
    source 23
    target 1672
  ]
  edge [
    source 23
    target 1673
  ]
  edge [
    source 23
    target 1674
  ]
  edge [
    source 23
    target 1675
  ]
  edge [
    source 23
    target 1676
  ]
  edge [
    source 23
    target 1677
  ]
  edge [
    source 23
    target 1678
  ]
  edge [
    source 23
    target 1679
  ]
  edge [
    source 23
    target 1680
  ]
  edge [
    source 23
    target 1681
  ]
  edge [
    source 23
    target 1682
  ]
  edge [
    source 23
    target 1683
  ]
  edge [
    source 23
    target 1684
  ]
  edge [
    source 23
    target 1685
  ]
  edge [
    source 23
    target 1686
  ]
  edge [
    source 23
    target 1687
  ]
  edge [
    source 23
    target 1688
  ]
  edge [
    source 23
    target 1689
  ]
  edge [
    source 23
    target 1690
  ]
  edge [
    source 23
    target 1691
  ]
  edge [
    source 23
    target 1692
  ]
  edge [
    source 23
    target 1693
  ]
  edge [
    source 23
    target 1694
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 1695
  ]
  edge [
    source 23
    target 1696
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 1697
  ]
  edge [
    source 23
    target 1698
  ]
  edge [
    source 23
    target 1699
  ]
  edge [
    source 23
    target 1700
  ]
  edge [
    source 23
    target 1701
  ]
  edge [
    source 23
    target 1702
  ]
  edge [
    source 23
    target 1703
  ]
  edge [
    source 23
    target 1704
  ]
  edge [
    source 23
    target 1705
  ]
  edge [
    source 23
    target 1706
  ]
  edge [
    source 23
    target 1707
  ]
  edge [
    source 23
    target 1708
  ]
  edge [
    source 23
    target 1709
  ]
  edge [
    source 23
    target 1710
  ]
  edge [
    source 23
    target 1711
  ]
  edge [
    source 23
    target 1712
  ]
  edge [
    source 23
    target 1713
  ]
  edge [
    source 23
    target 1714
  ]
  edge [
    source 23
    target 1715
  ]
  edge [
    source 23
    target 1716
  ]
  edge [
    source 23
    target 1717
  ]
  edge [
    source 23
    target 1718
  ]
  edge [
    source 23
    target 1719
  ]
  edge [
    source 23
    target 1720
  ]
  edge [
    source 23
    target 1721
  ]
  edge [
    source 23
    target 1722
  ]
  edge [
    source 23
    target 1723
  ]
  edge [
    source 23
    target 1724
  ]
  edge [
    source 23
    target 1725
  ]
  edge [
    source 23
    target 1726
  ]
  edge [
    source 23
    target 1727
  ]
  edge [
    source 23
    target 1728
  ]
  edge [
    source 23
    target 1729
  ]
  edge [
    source 23
    target 1730
  ]
  edge [
    source 23
    target 1731
  ]
  edge [
    source 23
    target 1732
  ]
  edge [
    source 23
    target 1733
  ]
  edge [
    source 23
    target 1734
  ]
  edge [
    source 23
    target 1735
  ]
  edge [
    source 23
    target 1736
  ]
  edge [
    source 23
    target 1737
  ]
  edge [
    source 23
    target 1738
  ]
  edge [
    source 23
    target 1739
  ]
  edge [
    source 23
    target 1740
  ]
  edge [
    source 23
    target 1741
  ]
  edge [
    source 23
    target 1742
  ]
  edge [
    source 23
    target 1743
  ]
  edge [
    source 23
    target 1744
  ]
  edge [
    source 23
    target 1745
  ]
  edge [
    source 23
    target 1746
  ]
  edge [
    source 23
    target 1747
  ]
  edge [
    source 23
    target 1748
  ]
  edge [
    source 23
    target 1749
  ]
  edge [
    source 23
    target 1750
  ]
  edge [
    source 23
    target 1751
  ]
  edge [
    source 23
    target 1752
  ]
  edge [
    source 23
    target 1753
  ]
  edge [
    source 23
    target 1754
  ]
  edge [
    source 23
    target 1755
  ]
  edge [
    source 23
    target 1756
  ]
  edge [
    source 23
    target 1757
  ]
  edge [
    source 23
    target 1758
  ]
  edge [
    source 23
    target 1759
  ]
  edge [
    source 23
    target 1760
  ]
  edge [
    source 23
    target 1761
  ]
  edge [
    source 23
    target 1762
  ]
  edge [
    source 23
    target 1763
  ]
  edge [
    source 23
    target 1764
  ]
  edge [
    source 23
    target 1765
  ]
  edge [
    source 23
    target 1766
  ]
  edge [
    source 23
    target 1767
  ]
  edge [
    source 23
    target 1768
  ]
  edge [
    source 23
    target 1769
  ]
  edge [
    source 23
    target 1770
  ]
  edge [
    source 23
    target 1771
  ]
  edge [
    source 23
    target 1772
  ]
  edge [
    source 23
    target 1773
  ]
  edge [
    source 23
    target 1774
  ]
  edge [
    source 23
    target 1775
  ]
  edge [
    source 23
    target 1776
  ]
  edge [
    source 23
    target 1777
  ]
  edge [
    source 23
    target 1778
  ]
  edge [
    source 23
    target 1779
  ]
  edge [
    source 23
    target 1780
  ]
  edge [
    source 23
    target 314
  ]
  edge [
    source 23
    target 1781
  ]
  edge [
    source 23
    target 1782
  ]
  edge [
    source 23
    target 1783
  ]
  edge [
    source 23
    target 1784
  ]
  edge [
    source 23
    target 1785
  ]
  edge [
    source 23
    target 1786
  ]
  edge [
    source 23
    target 1787
  ]
  edge [
    source 23
    target 1788
  ]
  edge [
    source 23
    target 1789
  ]
  edge [
    source 23
    target 1790
  ]
  edge [
    source 23
    target 1791
  ]
  edge [
    source 23
    target 1792
  ]
  edge [
    source 23
    target 1793
  ]
  edge [
    source 23
    target 1794
  ]
  edge [
    source 23
    target 1795
  ]
  edge [
    source 23
    target 1796
  ]
  edge [
    source 23
    target 1797
  ]
  edge [
    source 23
    target 1798
  ]
  edge [
    source 23
    target 1799
  ]
  edge [
    source 23
    target 1800
  ]
  edge [
    source 23
    target 1801
  ]
  edge [
    source 23
    target 351
  ]
  edge [
    source 23
    target 1802
  ]
  edge [
    source 23
    target 1803
  ]
  edge [
    source 23
    target 1804
  ]
  edge [
    source 23
    target 1805
  ]
  edge [
    source 23
    target 1806
  ]
  edge [
    source 23
    target 1807
  ]
  edge [
    source 23
    target 1808
  ]
  edge [
    source 23
    target 1809
  ]
  edge [
    source 23
    target 1810
  ]
  edge [
    source 23
    target 1811
  ]
  edge [
    source 23
    target 1812
  ]
  edge [
    source 23
    target 1813
  ]
  edge [
    source 23
    target 1814
  ]
  edge [
    source 23
    target 1815
  ]
  edge [
    source 23
    target 1816
  ]
  edge [
    source 23
    target 1817
  ]
  edge [
    source 23
    target 1818
  ]
  edge [
    source 23
    target 1819
  ]
  edge [
    source 23
    target 1820
  ]
  edge [
    source 23
    target 1821
  ]
  edge [
    source 23
    target 1822
  ]
  edge [
    source 23
    target 1823
  ]
  edge [
    source 23
    target 1824
  ]
  edge [
    source 23
    target 1825
  ]
  edge [
    source 23
    target 1826
  ]
  edge [
    source 23
    target 1827
  ]
  edge [
    source 23
    target 1828
  ]
  edge [
    source 23
    target 1829
  ]
  edge [
    source 23
    target 1830
  ]
  edge [
    source 23
    target 1831
  ]
  edge [
    source 23
    target 1832
  ]
  edge [
    source 23
    target 1833
  ]
  edge [
    source 23
    target 1834
  ]
  edge [
    source 23
    target 1835
  ]
  edge [
    source 23
    target 1836
  ]
  edge [
    source 23
    target 1837
  ]
  edge [
    source 23
    target 1838
  ]
  edge [
    source 23
    target 1839
  ]
  edge [
    source 23
    target 1840
  ]
  edge [
    source 23
    target 1841
  ]
  edge [
    source 23
    target 1842
  ]
  edge [
    source 23
    target 1843
  ]
  edge [
    source 23
    target 1844
  ]
  edge [
    source 23
    target 1845
  ]
  edge [
    source 23
    target 1846
  ]
  edge [
    source 23
    target 1847
  ]
  edge [
    source 23
    target 1848
  ]
  edge [
    source 23
    target 1849
  ]
  edge [
    source 23
    target 1850
  ]
  edge [
    source 23
    target 1851
  ]
  edge [
    source 23
    target 1852
  ]
  edge [
    source 23
    target 1853
  ]
  edge [
    source 23
    target 1854
  ]
  edge [
    source 23
    target 1855
  ]
  edge [
    source 23
    target 1856
  ]
  edge [
    source 23
    target 1857
  ]
  edge [
    source 23
    target 1858
  ]
  edge [
    source 23
    target 1859
  ]
  edge [
    source 23
    target 1860
  ]
  edge [
    source 23
    target 1861
  ]
  edge [
    source 23
    target 1862
  ]
  edge [
    source 23
    target 1863
  ]
  edge [
    source 23
    target 1864
  ]
  edge [
    source 23
    target 1865
  ]
  edge [
    source 23
    target 1866
  ]
  edge [
    source 23
    target 1867
  ]
  edge [
    source 23
    target 1868
  ]
  edge [
    source 23
    target 1869
  ]
  edge [
    source 23
    target 1870
  ]
  edge [
    source 23
    target 1871
  ]
  edge [
    source 23
    target 1872
  ]
  edge [
    source 23
    target 1873
  ]
  edge [
    source 23
    target 1874
  ]
  edge [
    source 23
    target 1875
  ]
  edge [
    source 23
    target 1876
  ]
  edge [
    source 23
    target 1877
  ]
  edge [
    source 23
    target 1878
  ]
  edge [
    source 23
    target 1879
  ]
  edge [
    source 23
    target 1880
  ]
  edge [
    source 23
    target 1881
  ]
  edge [
    source 23
    target 1882
  ]
  edge [
    source 23
    target 1883
  ]
  edge [
    source 23
    target 1884
  ]
  edge [
    source 23
    target 1885
  ]
  edge [
    source 23
    target 1886
  ]
  edge [
    source 23
    target 1887
  ]
  edge [
    source 23
    target 1888
  ]
  edge [
    source 23
    target 1889
  ]
  edge [
    source 23
    target 1890
  ]
  edge [
    source 23
    target 1891
  ]
  edge [
    source 23
    target 1892
  ]
  edge [
    source 23
    target 500
  ]
  edge [
    source 23
    target 1893
  ]
  edge [
    source 23
    target 1894
  ]
  edge [
    source 23
    target 1895
  ]
  edge [
    source 23
    target 1896
  ]
  edge [
    source 23
    target 1897
  ]
  edge [
    source 23
    target 1898
  ]
  edge [
    source 23
    target 1899
  ]
  edge [
    source 23
    target 1900
  ]
  edge [
    source 23
    target 1901
  ]
  edge [
    source 23
    target 1902
  ]
  edge [
    source 23
    target 1903
  ]
  edge [
    source 23
    target 1904
  ]
  edge [
    source 23
    target 1905
  ]
  edge [
    source 23
    target 1906
  ]
  edge [
    source 23
    target 1907
  ]
  edge [
    source 23
    target 1908
  ]
  edge [
    source 23
    target 1909
  ]
  edge [
    source 23
    target 1910
  ]
  edge [
    source 23
    target 1911
  ]
  edge [
    source 23
    target 1912
  ]
  edge [
    source 23
    target 1913
  ]
  edge [
    source 23
    target 1914
  ]
  edge [
    source 23
    target 1915
  ]
  edge [
    source 23
    target 1916
  ]
  edge [
    source 23
    target 1917
  ]
  edge [
    source 23
    target 1918
  ]
  edge [
    source 23
    target 1919
  ]
  edge [
    source 23
    target 1920
  ]
  edge [
    source 23
    target 1921
  ]
  edge [
    source 23
    target 1922
  ]
  edge [
    source 23
    target 1923
  ]
  edge [
    source 23
    target 1924
  ]
  edge [
    source 23
    target 1925
  ]
  edge [
    source 23
    target 1926
  ]
  edge [
    source 23
    target 1927
  ]
  edge [
    source 23
    target 1928
  ]
  edge [
    source 23
    target 1929
  ]
  edge [
    source 23
    target 1930
  ]
  edge [
    source 23
    target 1931
  ]
  edge [
    source 23
    target 1932
  ]
  edge [
    source 23
    target 1933
  ]
  edge [
    source 23
    target 1934
  ]
  edge [
    source 23
    target 1935
  ]
  edge [
    source 23
    target 1936
  ]
  edge [
    source 23
    target 1937
  ]
  edge [
    source 23
    target 1938
  ]
  edge [
    source 23
    target 1939
  ]
  edge [
    source 23
    target 1940
  ]
  edge [
    source 23
    target 1941
  ]
  edge [
    source 23
    target 1942
  ]
  edge [
    source 23
    target 1943
  ]
  edge [
    source 23
    target 1944
  ]
  edge [
    source 23
    target 1945
  ]
  edge [
    source 23
    target 1946
  ]
  edge [
    source 23
    target 1947
  ]
  edge [
    source 23
    target 1948
  ]
  edge [
    source 23
    target 1949
  ]
  edge [
    source 23
    target 1950
  ]
  edge [
    source 23
    target 1951
  ]
  edge [
    source 23
    target 1952
  ]
  edge [
    source 23
    target 1953
  ]
  edge [
    source 23
    target 1954
  ]
  edge [
    source 23
    target 1955
  ]
  edge [
    source 23
    target 1956
  ]
  edge [
    source 23
    target 1957
  ]
  edge [
    source 23
    target 1958
  ]
  edge [
    source 23
    target 1959
  ]
  edge [
    source 23
    target 1960
  ]
  edge [
    source 23
    target 1961
  ]
  edge [
    source 23
    target 1962
  ]
  edge [
    source 23
    target 1963
  ]
  edge [
    source 23
    target 1964
  ]
  edge [
    source 23
    target 1965
  ]
  edge [
    source 23
    target 1966
  ]
  edge [
    source 23
    target 1967
  ]
  edge [
    source 23
    target 1968
  ]
  edge [
    source 23
    target 1969
  ]
  edge [
    source 23
    target 1970
  ]
  edge [
    source 23
    target 1971
  ]
  edge [
    source 23
    target 1972
  ]
  edge [
    source 23
    target 1973
  ]
  edge [
    source 23
    target 1974
  ]
  edge [
    source 23
    target 1975
  ]
  edge [
    source 23
    target 1976
  ]
  edge [
    source 23
    target 1977
  ]
  edge [
    source 23
    target 1978
  ]
  edge [
    source 23
    target 1979
  ]
  edge [
    source 23
    target 1980
  ]
  edge [
    source 23
    target 1981
  ]
  edge [
    source 23
    target 1982
  ]
  edge [
    source 23
    target 1983
  ]
  edge [
    source 23
    target 1984
  ]
  edge [
    source 23
    target 1985
  ]
  edge [
    source 23
    target 1986
  ]
  edge [
    source 23
    target 1987
  ]
  edge [
    source 23
    target 1988
  ]
  edge [
    source 23
    target 1989
  ]
  edge [
    source 23
    target 1990
  ]
  edge [
    source 23
    target 1991
  ]
  edge [
    source 23
    target 1992
  ]
  edge [
    source 23
    target 1993
  ]
  edge [
    source 23
    target 1537
  ]
  edge [
    source 23
    target 1994
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1995
  ]
  edge [
    source 24
    target 1996
  ]
  edge [
    source 24
    target 1997
  ]
  edge [
    source 24
    target 1998
  ]
  edge [
    source 24
    target 1999
  ]
  edge [
    source 24
    target 2000
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 2001
  ]
  edge [
    source 25
    target 2002
  ]
  edge [
    source 25
    target 2003
  ]
  edge [
    source 25
    target 2004
  ]
  edge [
    source 25
    target 2005
  ]
  edge [
    source 25
    target 2006
  ]
  edge [
    source 25
    target 2007
  ]
  edge [
    source 25
    target 2008
  ]
  edge [
    source 25
    target 2009
  ]
  edge [
    source 25
    target 2010
  ]
  edge [
    source 25
    target 2011
  ]
  edge [
    source 25
    target 2012
  ]
  edge [
    source 25
    target 2013
  ]
  edge [
    source 25
    target 2014
  ]
  edge [
    source 25
    target 2015
  ]
  edge [
    source 25
    target 2016
  ]
  edge [
    source 25
    target 2017
  ]
  edge [
    source 25
    target 2018
  ]
  edge [
    source 25
    target 2019
  ]
  edge [
    source 25
    target 2020
  ]
  edge [
    source 25
    target 2021
  ]
  edge [
    source 25
    target 894
  ]
  edge [
    source 25
    target 2022
  ]
  edge [
    source 25
    target 2023
  ]
  edge [
    source 25
    target 2024
  ]
  edge [
    source 25
    target 2025
  ]
  edge [
    source 25
    target 2026
  ]
  edge [
    source 25
    target 2027
  ]
  edge [
    source 25
    target 2028
  ]
  edge [
    source 25
    target 2029
  ]
  edge [
    source 25
    target 2030
  ]
  edge [
    source 25
    target 2031
  ]
  edge [
    source 25
    target 2032
  ]
  edge [
    source 25
    target 2033
  ]
  edge [
    source 25
    target 2034
  ]
  edge [
    source 25
    target 2035
  ]
  edge [
    source 25
    target 2036
  ]
  edge [
    source 25
    target 2037
  ]
  edge [
    source 25
    target 2038
  ]
  edge [
    source 25
    target 2039
  ]
  edge [
    source 25
    target 2040
  ]
  edge [
    source 25
    target 2041
  ]
  edge [
    source 25
    target 2042
  ]
  edge [
    source 25
    target 2043
  ]
  edge [
    source 25
    target 2044
  ]
  edge [
    source 25
    target 2045
  ]
  edge [
    source 25
    target 2046
  ]
  edge [
    source 25
    target 795
  ]
  edge [
    source 25
    target 2047
  ]
  edge [
    source 25
    target 2048
  ]
  edge [
    source 25
    target 2049
  ]
  edge [
    source 25
    target 2050
  ]
  edge [
    source 25
    target 2051
  ]
  edge [
    source 25
    target 2052
  ]
  edge [
    source 25
    target 2053
  ]
  edge [
    source 25
    target 2054
  ]
  edge [
    source 25
    target 2055
  ]
  edge [
    source 25
    target 2056
  ]
  edge [
    source 25
    target 2057
  ]
  edge [
    source 25
    target 2058
  ]
  edge [
    source 25
    target 2059
  ]
  edge [
    source 25
    target 2060
  ]
  edge [
    source 25
    target 2061
  ]
  edge [
    source 25
    target 2062
  ]
  edge [
    source 25
    target 2063
  ]
  edge [
    source 25
    target 2064
  ]
  edge [
    source 25
    target 2065
  ]
  edge [
    source 25
    target 2066
  ]
  edge [
    source 25
    target 2067
  ]
  edge [
    source 25
    target 2068
  ]
  edge [
    source 25
    target 2069
  ]
  edge [
    source 25
    target 2070
  ]
  edge [
    source 25
    target 2071
  ]
  edge [
    source 25
    target 2072
  ]
  edge [
    source 25
    target 2073
  ]
  edge [
    source 25
    target 2074
  ]
  edge [
    source 25
    target 2075
  ]
  edge [
    source 25
    target 2076
  ]
  edge [
    source 25
    target 2077
  ]
  edge [
    source 25
    target 2078
  ]
  edge [
    source 25
    target 2079
  ]
  edge [
    source 25
    target 2080
  ]
  edge [
    source 25
    target 2081
  ]
  edge [
    source 25
    target 2082
  ]
  edge [
    source 25
    target 2083
  ]
  edge [
    source 25
    target 2084
  ]
  edge [
    source 25
    target 1541
  ]
  edge [
    source 25
    target 1480
  ]
  edge [
    source 25
    target 2085
  ]
  edge [
    source 25
    target 2086
  ]
  edge [
    source 25
    target 2087
  ]
  edge [
    source 25
    target 2088
  ]
  edge [
    source 25
    target 2089
  ]
  edge [
    source 25
    target 2090
  ]
  edge [
    source 25
    target 2091
  ]
  edge [
    source 25
    target 2092
  ]
  edge [
    source 25
    target 2093
  ]
  edge [
    source 25
    target 2094
  ]
  edge [
    source 25
    target 2095
  ]
  edge [
    source 25
    target 2096
  ]
  edge [
    source 25
    target 2097
  ]
  edge [
    source 25
    target 2098
  ]
  edge [
    source 25
    target 2099
  ]
  edge [
    source 25
    target 2100
  ]
  edge [
    source 25
    target 2101
  ]
  edge [
    source 25
    target 1056
  ]
  edge [
    source 25
    target 2102
  ]
  edge [
    source 25
    target 948
  ]
  edge [
    source 25
    target 647
  ]
  edge [
    source 25
    target 969
  ]
  edge [
    source 25
    target 2103
  ]
  edge [
    source 25
    target 2104
  ]
  edge [
    source 25
    target 2105
  ]
  edge [
    source 25
    target 2106
  ]
  edge [
    source 25
    target 2107
  ]
  edge [
    source 25
    target 2108
  ]
  edge [
    source 25
    target 2109
  ]
  edge [
    source 25
    target 2110
  ]
  edge [
    source 25
    target 2111
  ]
  edge [
    source 25
    target 945
  ]
  edge [
    source 25
    target 2112
  ]
  edge [
    source 25
    target 2113
  ]
  edge [
    source 25
    target 956
  ]
  edge [
    source 25
    target 1124
  ]
  edge [
    source 25
    target 2114
  ]
  edge [
    source 25
    target 2115
  ]
  edge [
    source 25
    target 2116
  ]
  edge [
    source 25
    target 2117
  ]
  edge [
    source 25
    target 2118
  ]
  edge [
    source 25
    target 2119
  ]
  edge [
    source 25
    target 2120
  ]
  edge [
    source 25
    target 2121
  ]
  edge [
    source 25
    target 2122
  ]
  edge [
    source 25
    target 2123
  ]
  edge [
    source 25
    target 2124
  ]
  edge [
    source 25
    target 2125
  ]
  edge [
    source 25
    target 2126
  ]
  edge [
    source 25
    target 2127
  ]
  edge [
    source 25
    target 2128
  ]
  edge [
    source 25
    target 2129
  ]
  edge [
    source 25
    target 2130
  ]
  edge [
    source 25
    target 1154
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 25
    target 2131
  ]
  edge [
    source 25
    target 2132
  ]
  edge [
    source 25
    target 2133
  ]
  edge [
    source 25
    target 2134
  ]
  edge [
    source 25
    target 2135
  ]
  edge [
    source 25
    target 2136
  ]
  edge [
    source 25
    target 2137
  ]
  edge [
    source 25
    target 2138
  ]
  edge [
    source 25
    target 2139
  ]
  edge [
    source 25
    target 2140
  ]
  edge [
    source 25
    target 2141
  ]
  edge [
    source 25
    target 2142
  ]
  edge [
    source 25
    target 2143
  ]
  edge [
    source 25
    target 2144
  ]
  edge [
    source 25
    target 1590
  ]
  edge [
    source 25
    target 2145
  ]
  edge [
    source 25
    target 2146
  ]
  edge [
    source 25
    target 2147
  ]
  edge [
    source 25
    target 2148
  ]
  edge [
    source 25
    target 2149
  ]
  edge [
    source 25
    target 2150
  ]
  edge [
    source 25
    target 2151
  ]
  edge [
    source 25
    target 2152
  ]
  edge [
    source 25
    target 2153
  ]
  edge [
    source 25
    target 2154
  ]
  edge [
    source 25
    target 2155
  ]
  edge [
    source 25
    target 2156
  ]
  edge [
    source 25
    target 2157
  ]
  edge [
    source 25
    target 2158
  ]
  edge [
    source 25
    target 2159
  ]
  edge [
    source 25
    target 2160
  ]
  edge [
    source 25
    target 2161
  ]
  edge [
    source 25
    target 2162
  ]
  edge [
    source 25
    target 2163
  ]
  edge [
    source 25
    target 2164
  ]
  edge [
    source 25
    target 2165
  ]
  edge [
    source 25
    target 2166
  ]
  edge [
    source 25
    target 2167
  ]
  edge [
    source 25
    target 2168
  ]
  edge [
    source 25
    target 2169
  ]
  edge [
    source 25
    target 2170
  ]
  edge [
    source 25
    target 2171
  ]
  edge [
    source 25
    target 2172
  ]
  edge [
    source 25
    target 2173
  ]
  edge [
    source 25
    target 2174
  ]
  edge [
    source 25
    target 2175
  ]
  edge [
    source 25
    target 2176
  ]
  edge [
    source 25
    target 2177
  ]
  edge [
    source 25
    target 1046
  ]
  edge [
    source 25
    target 2178
  ]
  edge [
    source 25
    target 2179
  ]
  edge [
    source 25
    target 2180
  ]
  edge [
    source 25
    target 2181
  ]
  edge [
    source 25
    target 2182
  ]
  edge [
    source 25
    target 2183
  ]
  edge [
    source 25
    target 2184
  ]
  edge [
    source 25
    target 2185
  ]
  edge [
    source 25
    target 2186
  ]
  edge [
    source 25
    target 664
  ]
  edge [
    source 25
    target 2187
  ]
  edge [
    source 25
    target 2188
  ]
  edge [
    source 25
    target 1044
  ]
  edge [
    source 25
    target 2189
  ]
  edge [
    source 25
    target 2190
  ]
  edge [
    source 25
    target 2191
  ]
  edge [
    source 25
    target 2192
  ]
  edge [
    source 25
    target 2193
  ]
  edge [
    source 25
    target 2194
  ]
  edge [
    source 25
    target 2195
  ]
  edge [
    source 25
    target 685
  ]
  edge [
    source 25
    target 2196
  ]
  edge [
    source 25
    target 2197
  ]
  edge [
    source 25
    target 2198
  ]
  edge [
    source 25
    target 2199
  ]
  edge [
    source 25
    target 2200
  ]
  edge [
    source 25
    target 2201
  ]
  edge [
    source 25
    target 2202
  ]
  edge [
    source 25
    target 2203
  ]
  edge [
    source 25
    target 2204
  ]
  edge [
    source 25
    target 2205
  ]
  edge [
    source 25
    target 2206
  ]
  edge [
    source 25
    target 2207
  ]
  edge [
    source 25
    target 2208
  ]
  edge [
    source 25
    target 2209
  ]
  edge [
    source 25
    target 2210
  ]
  edge [
    source 25
    target 2211
  ]
  edge [
    source 25
    target 2212
  ]
  edge [
    source 25
    target 2213
  ]
  edge [
    source 25
    target 2214
  ]
  edge [
    source 25
    target 2215
  ]
  edge [
    source 25
    target 2216
  ]
  edge [
    source 25
    target 2217
  ]
  edge [
    source 25
    target 2218
  ]
  edge [
    source 25
    target 2219
  ]
  edge [
    source 25
    target 2220
  ]
  edge [
    source 25
    target 2221
  ]
  edge [
    source 25
    target 2222
  ]
  edge [
    source 25
    target 2223
  ]
  edge [
    source 25
    target 2224
  ]
  edge [
    source 25
    target 2225
  ]
  edge [
    source 25
    target 2226
  ]
  edge [
    source 25
    target 2227
  ]
  edge [
    source 25
    target 2228
  ]
  edge [
    source 25
    target 2229
  ]
  edge [
    source 25
    target 2230
  ]
  edge [
    source 25
    target 2231
  ]
  edge [
    source 25
    target 2232
  ]
  edge [
    source 25
    target 1103
  ]
  edge [
    source 25
    target 2233
  ]
  edge [
    source 25
    target 2234
  ]
  edge [
    source 25
    target 2235
  ]
  edge [
    source 25
    target 2236
  ]
  edge [
    source 25
    target 2237
  ]
  edge [
    source 25
    target 2238
  ]
  edge [
    source 25
    target 2239
  ]
  edge [
    source 25
    target 2240
  ]
  edge [
    source 25
    target 2241
  ]
  edge [
    source 25
    target 2242
  ]
  edge [
    source 25
    target 2243
  ]
  edge [
    source 25
    target 2244
  ]
  edge [
    source 25
    target 2245
  ]
  edge [
    source 25
    target 2246
  ]
  edge [
    source 25
    target 2247
  ]
  edge [
    source 25
    target 2248
  ]
  edge [
    source 25
    target 2249
  ]
  edge [
    source 25
    target 2250
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 2251
  ]
  edge [
    source 25
    target 2252
  ]
  edge [
    source 25
    target 2253
  ]
  edge [
    source 25
    target 2254
  ]
  edge [
    source 25
    target 2255
  ]
  edge [
    source 25
    target 2256
  ]
  edge [
    source 25
    target 2257
  ]
  edge [
    source 25
    target 2258
  ]
  edge [
    source 25
    target 2259
  ]
  edge [
    source 25
    target 2260
  ]
  edge [
    source 25
    target 2261
  ]
  edge [
    source 25
    target 2262
  ]
  edge [
    source 25
    target 2263
  ]
  edge [
    source 25
    target 2264
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 2265
  ]
  edge [
    source 26
    target 2266
  ]
  edge [
    source 26
    target 2267
  ]
  edge [
    source 26
    target 2268
  ]
  edge [
    source 26
    target 795
  ]
  edge [
    source 26
    target 2269
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 2270
  ]
  edge [
    source 27
    target 2271
  ]
  edge [
    source 27
    target 2272
  ]
  edge [
    source 27
    target 2155
  ]
  edge [
    source 27
    target 2273
  ]
  edge [
    source 27
    target 2274
  ]
  edge [
    source 27
    target 2275
  ]
  edge [
    source 27
    target 2276
  ]
  edge [
    source 27
    target 795
  ]
  edge [
    source 27
    target 2277
  ]
  edge [
    source 27
    target 467
  ]
  edge [
    source 27
    target 2278
  ]
  edge [
    source 27
    target 2265
  ]
  edge [
    source 27
    target 2279
  ]
  edge [
    source 27
    target 659
  ]
  edge [
    source 27
    target 660
  ]
  edge [
    source 27
    target 661
  ]
  edge [
    source 27
    target 662
  ]
  edge [
    source 27
    target 663
  ]
  edge [
    source 27
    target 664
  ]
  edge [
    source 27
    target 665
  ]
  edge [
    source 27
    target 666
  ]
  edge [
    source 27
    target 667
  ]
  edge [
    source 27
    target 668
  ]
  edge [
    source 27
    target 669
  ]
  edge [
    source 27
    target 670
  ]
  edge [
    source 27
    target 671
  ]
  edge [
    source 27
    target 672
  ]
  edge [
    source 27
    target 673
  ]
  edge [
    source 27
    target 674
  ]
  edge [
    source 27
    target 675
  ]
  edge [
    source 27
    target 676
  ]
  edge [
    source 27
    target 677
  ]
  edge [
    source 27
    target 678
  ]
  edge [
    source 27
    target 679
  ]
  edge [
    source 27
    target 680
  ]
  edge [
    source 27
    target 681
  ]
  edge [
    source 27
    target 682
  ]
  edge [
    source 27
    target 683
  ]
  edge [
    source 27
    target 684
  ]
  edge [
    source 27
    target 685
  ]
  edge [
    source 27
    target 686
  ]
  edge [
    source 27
    target 2074
  ]
  edge [
    source 27
    target 2075
  ]
  edge [
    source 27
    target 2076
  ]
  edge [
    source 27
    target 2077
  ]
  edge [
    source 27
    target 2078
  ]
  edge [
    source 27
    target 2079
  ]
  edge [
    source 27
    target 2080
  ]
  edge [
    source 27
    target 2081
  ]
  edge [
    source 27
    target 2082
  ]
  edge [
    source 27
    target 2083
  ]
  edge [
    source 27
    target 2084
  ]
  edge [
    source 27
    target 1541
  ]
  edge [
    source 27
    target 1480
  ]
  edge [
    source 27
    target 2026
  ]
  edge [
    source 27
    target 2085
  ]
  edge [
    source 27
    target 2086
  ]
  edge [
    source 27
    target 2087
  ]
  edge [
    source 27
    target 2088
  ]
  edge [
    source 27
    target 2030
  ]
  edge [
    source 27
    target 2033
  ]
  edge [
    source 27
    target 2089
  ]
  edge [
    source 27
    target 2042
  ]
  edge [
    source 27
    target 2090
  ]
  edge [
    source 27
    target 2091
  ]
  edge [
    source 27
    target 2092
  ]
  edge [
    source 27
    target 2093
  ]
  edge [
    source 27
    target 2094
  ]
  edge [
    source 27
    target 2095
  ]
  edge [
    source 27
    target 2096
  ]
  edge [
    source 27
    target 2054
  ]
  edge [
    source 27
    target 2097
  ]
  edge [
    source 27
    target 2098
  ]
  edge [
    source 27
    target 2099
  ]
  edge [
    source 27
    target 2100
  ]
  edge [
    source 27
    target 2101
  ]
  edge [
    source 27
    target 2063
  ]
  edge [
    source 27
    target 2029
  ]
  edge [
    source 27
    target 2266
  ]
  edge [
    source 27
    target 2267
  ]
  edge [
    source 27
    target 2268
  ]
  edge [
    source 27
    target 2269
  ]
  edge [
    source 27
    target 2125
  ]
  edge [
    source 27
    target 2040
  ]
  edge [
    source 27
    target 2058
  ]
  edge [
    source 27
    target 2066
  ]
  edge [
    source 27
    target 2280
  ]
  edge [
    source 27
    target 2281
  ]
  edge [
    source 27
    target 2282
  ]
  edge [
    source 27
    target 2283
  ]
  edge [
    source 27
    target 2284
  ]
  edge [
    source 27
    target 2285
  ]
  edge [
    source 27
    target 2286
  ]
  edge [
    source 27
    target 2287
  ]
  edge [
    source 27
    target 788
  ]
  edge [
    source 27
    target 2288
  ]
  edge [
    source 27
    target 2289
  ]
  edge [
    source 27
    target 2290
  ]
  edge [
    source 27
    target 2291
  ]
  edge [
    source 27
    target 2292
  ]
  edge [
    source 27
    target 2293
  ]
  edge [
    source 27
    target 2294
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 2295
  ]
  edge [
    source 27
    target 2296
  ]
  edge [
    source 27
    target 1057
  ]
  edge [
    source 27
    target 2297
  ]
  edge [
    source 27
    target 2298
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 2299
  ]
  edge [
    source 27
    target 2300
  ]
  edge [
    source 27
    target 2301
  ]
  edge [
    source 27
    target 2302
  ]
  edge [
    source 27
    target 2303
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 2304
  ]
  edge [
    source 27
    target 2305
  ]
  edge [
    source 27
    target 2237
  ]
  edge [
    source 27
    target 2306
  ]
  edge [
    source 27
    target 2307
  ]
  edge [
    source 27
    target 2308
  ]
  edge [
    source 27
    target 1214
  ]
  edge [
    source 27
    target 2309
  ]
  edge [
    source 27
    target 2310
  ]
  edge [
    source 27
    target 2311
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 2312
  ]
  edge [
    source 27
    target 2313
  ]
  edge [
    source 28
    target 2314
  ]
  edge [
    source 28
    target 2265
  ]
  edge [
    source 28
    target 2315
  ]
  edge [
    source 28
    target 2316
  ]
  edge [
    source 28
    target 2317
  ]
  edge [
    source 28
    target 2266
  ]
  edge [
    source 28
    target 2267
  ]
  edge [
    source 28
    target 2268
  ]
  edge [
    source 28
    target 795
  ]
  edge [
    source 28
    target 2269
  ]
  edge [
    source 28
    target 2318
  ]
  edge [
    source 28
    target 2319
  ]
]
