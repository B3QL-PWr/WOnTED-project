graph [
  node [
    id 0
    label "synagoga"
    origin "text"
  ]
  node [
    id 1
    label "skwierzynie"
    origin "text"
  ]
  node [
    id 2
    label "judaizm"
  ]
  node [
    id 3
    label "synagogue"
  ]
  node [
    id 4
    label "siedziba"
  ]
  node [
    id 5
    label "budynek"
  ]
  node [
    id 6
    label "babiniec"
  ]
  node [
    id 7
    label "aron_ha-kodesz"
  ]
  node [
    id 8
    label "balkon"
  ]
  node [
    id 9
    label "budowla"
  ]
  node [
    id 10
    label "pod&#322;oga"
  ]
  node [
    id 11
    label "kondygnacja"
  ]
  node [
    id 12
    label "skrzyd&#322;o"
  ]
  node [
    id 13
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 14
    label "dach"
  ]
  node [
    id 15
    label "strop"
  ]
  node [
    id 16
    label "klatka_schodowa"
  ]
  node [
    id 17
    label "przedpro&#380;e"
  ]
  node [
    id 18
    label "Pentagon"
  ]
  node [
    id 19
    label "alkierz"
  ]
  node [
    id 20
    label "front"
  ]
  node [
    id 21
    label "&#321;ubianka"
  ]
  node [
    id 22
    label "miejsce_pracy"
  ]
  node [
    id 23
    label "dzia&#322;_personalny"
  ]
  node [
    id 24
    label "Kreml"
  ]
  node [
    id 25
    label "Bia&#322;y_Dom"
  ]
  node [
    id 26
    label "miejsce"
  ]
  node [
    id 27
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 28
    label "sadowisko"
  ]
  node [
    id 29
    label "cerkiew"
  ]
  node [
    id 30
    label "izba"
  ]
  node [
    id 31
    label "przedsionek"
  ]
  node [
    id 32
    label "pomieszczenie"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "kruchta"
  ]
  node [
    id 35
    label "kadisz"
  ]
  node [
    id 36
    label "hawdala"
  ]
  node [
    id 37
    label "kabalistyka"
  ]
  node [
    id 38
    label "rabin"
  ]
  node [
    id 39
    label "szabas"
  ]
  node [
    id 40
    label "konfirmacja"
  ]
  node [
    id 41
    label "chasydyzm"
  ]
  node [
    id 42
    label "talmudyzm"
  ]
  node [
    id 43
    label "halacha"
  ]
  node [
    id 44
    label "cherem"
  ]
  node [
    id 45
    label "majufes"
  ]
  node [
    id 46
    label "Szechina"
  ]
  node [
    id 47
    label "chupa"
  ]
  node [
    id 48
    label "szeol"
  ]
  node [
    id 49
    label "kaba&#322;a"
  ]
  node [
    id 50
    label "kidusz"
  ]
  node [
    id 51
    label "kabalista"
  ]
  node [
    id 52
    label "religia"
  ]
  node [
    id 53
    label "ta&#322;es"
  ]
  node [
    id 54
    label "nisan"
  ]
  node [
    id 55
    label "Chanuka"
  ]
  node [
    id 56
    label "faryzeizm"
  ]
  node [
    id 57
    label "talmudysta"
  ]
  node [
    id 58
    label "sidur"
  ]
  node [
    id 59
    label "on"
  ]
  node [
    id 60
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 61
    label "Jagie&#322;&#322;o"
  ]
  node [
    id 62
    label "ii"
  ]
  node [
    id 63
    label "wojna"
  ]
  node [
    id 64
    label "&#347;wiatowy"
  ]
  node [
    id 65
    label "noc"
  ]
  node [
    id 66
    label "kryszta&#322;owy"
  ]
  node [
    id 67
    label "powstaniec"
  ]
  node [
    id 68
    label "wielkopolski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
]
