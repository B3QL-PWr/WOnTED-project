graph [
  node [
    id 0
    label "raz"
    origin "text"
  ]
  node [
    id 1
    label "dobrze"
    origin "text"
  ]
  node [
    id 2
    label "time"
  ]
  node [
    id 3
    label "cios"
  ]
  node [
    id 4
    label "chwila"
  ]
  node [
    id 5
    label "uderzenie"
  ]
  node [
    id 6
    label "blok"
  ]
  node [
    id 7
    label "shot"
  ]
  node [
    id 8
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 9
    label "struktura_geologiczna"
  ]
  node [
    id 10
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 11
    label "pr&#243;ba"
  ]
  node [
    id 12
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 13
    label "coup"
  ]
  node [
    id 14
    label "siekacz"
  ]
  node [
    id 15
    label "instrumentalizacja"
  ]
  node [
    id 16
    label "trafienie"
  ]
  node [
    id 17
    label "walka"
  ]
  node [
    id 18
    label "zdarzenie_si&#281;"
  ]
  node [
    id 19
    label "wdarcie_si&#281;"
  ]
  node [
    id 20
    label "pogorszenie"
  ]
  node [
    id 21
    label "d&#378;wi&#281;k"
  ]
  node [
    id 22
    label "poczucie"
  ]
  node [
    id 23
    label "reakcja"
  ]
  node [
    id 24
    label "contact"
  ]
  node [
    id 25
    label "stukni&#281;cie"
  ]
  node [
    id 26
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 27
    label "bat"
  ]
  node [
    id 28
    label "spowodowanie"
  ]
  node [
    id 29
    label "rush"
  ]
  node [
    id 30
    label "odbicie"
  ]
  node [
    id 31
    label "dawka"
  ]
  node [
    id 32
    label "zadanie"
  ]
  node [
    id 33
    label "&#347;ci&#281;cie"
  ]
  node [
    id 34
    label "st&#322;uczenie"
  ]
  node [
    id 35
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 36
    label "odbicie_si&#281;"
  ]
  node [
    id 37
    label "dotkni&#281;cie"
  ]
  node [
    id 38
    label "charge"
  ]
  node [
    id 39
    label "dostanie"
  ]
  node [
    id 40
    label "skrytykowanie"
  ]
  node [
    id 41
    label "zagrywka"
  ]
  node [
    id 42
    label "manewr"
  ]
  node [
    id 43
    label "nast&#261;pienie"
  ]
  node [
    id 44
    label "uderzanie"
  ]
  node [
    id 45
    label "pogoda"
  ]
  node [
    id 46
    label "stroke"
  ]
  node [
    id 47
    label "pobicie"
  ]
  node [
    id 48
    label "ruch"
  ]
  node [
    id 49
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 50
    label "flap"
  ]
  node [
    id 51
    label "dotyk"
  ]
  node [
    id 52
    label "zrobienie"
  ]
  node [
    id 53
    label "czas"
  ]
  node [
    id 54
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 55
    label "odpowiednio"
  ]
  node [
    id 56
    label "dobroczynnie"
  ]
  node [
    id 57
    label "moralnie"
  ]
  node [
    id 58
    label "korzystnie"
  ]
  node [
    id 59
    label "pozytywnie"
  ]
  node [
    id 60
    label "lepiej"
  ]
  node [
    id 61
    label "wiele"
  ]
  node [
    id 62
    label "skutecznie"
  ]
  node [
    id 63
    label "pomy&#347;lnie"
  ]
  node [
    id 64
    label "dobry"
  ]
  node [
    id 65
    label "charakterystycznie"
  ]
  node [
    id 66
    label "nale&#380;nie"
  ]
  node [
    id 67
    label "stosowny"
  ]
  node [
    id 68
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 69
    label "nale&#380;ycie"
  ]
  node [
    id 70
    label "prawdziwie"
  ]
  node [
    id 71
    label "auspiciously"
  ]
  node [
    id 72
    label "pomy&#347;lny"
  ]
  node [
    id 73
    label "moralny"
  ]
  node [
    id 74
    label "etyczny"
  ]
  node [
    id 75
    label "skuteczny"
  ]
  node [
    id 76
    label "wiela"
  ]
  node [
    id 77
    label "du&#380;y"
  ]
  node [
    id 78
    label "utylitarnie"
  ]
  node [
    id 79
    label "korzystny"
  ]
  node [
    id 80
    label "beneficially"
  ]
  node [
    id 81
    label "przyjemnie"
  ]
  node [
    id 82
    label "pozytywny"
  ]
  node [
    id 83
    label "ontologicznie"
  ]
  node [
    id 84
    label "dodatni"
  ]
  node [
    id 85
    label "odpowiedni"
  ]
  node [
    id 86
    label "wiersz"
  ]
  node [
    id 87
    label "dobroczynny"
  ]
  node [
    id 88
    label "czw&#243;rka"
  ]
  node [
    id 89
    label "spokojny"
  ]
  node [
    id 90
    label "&#347;mieszny"
  ]
  node [
    id 91
    label "mi&#322;y"
  ]
  node [
    id 92
    label "grzeczny"
  ]
  node [
    id 93
    label "powitanie"
  ]
  node [
    id 94
    label "ca&#322;y"
  ]
  node [
    id 95
    label "zwrot"
  ]
  node [
    id 96
    label "drogi"
  ]
  node [
    id 97
    label "pos&#322;uszny"
  ]
  node [
    id 98
    label "philanthropically"
  ]
  node [
    id 99
    label "spo&#322;ecznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
]
