graph [
  node [
    id 0
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 1
    label "firma"
    origin "text"
  ]
  node [
    id 2
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 3
    label "warunek"
    origin "text"
  ]
  node [
    id 4
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 5
    label "biznes"
    origin "text"
  ]
  node [
    id 6
    label "rok"
    origin "text"
  ]
  node [
    id 7
    label "pogorszy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 10
    label "lato"
    origin "text"
  ]
  node [
    id 11
    label "poprzedni"
    origin "text"
  ]
  node [
    id 12
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 13
    label "badanie"
    origin "text"
  ]
  node [
    id 14
    label "nastr&#243;j"
    origin "text"
  ]
  node [
    id 15
    label "przedsi&#281;biorca"
    origin "text"
  ]
  node [
    id 16
    label "zrealizowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zlecenie"
    origin "text"
  ]
  node [
    id 18
    label "konfederacja"
    origin "text"
  ]
  node [
    id 19
    label "lewiatan"
    origin "text"
  ]
  node [
    id 20
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "medium"
  ]
  node [
    id 23
    label "chronometria"
  ]
  node [
    id 24
    label "odczyt"
  ]
  node [
    id 25
    label "laba"
  ]
  node [
    id 26
    label "czasoprzestrze&#324;"
  ]
  node [
    id 27
    label "time_period"
  ]
  node [
    id 28
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 29
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 30
    label "Zeitgeist"
  ]
  node [
    id 31
    label "pochodzenie"
  ]
  node [
    id 32
    label "przep&#322;ywanie"
  ]
  node [
    id 33
    label "schy&#322;ek"
  ]
  node [
    id 34
    label "czwarty_wymiar"
  ]
  node [
    id 35
    label "kategoria_gramatyczna"
  ]
  node [
    id 36
    label "poprzedzi&#263;"
  ]
  node [
    id 37
    label "pogoda"
  ]
  node [
    id 38
    label "czasokres"
  ]
  node [
    id 39
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 40
    label "poprzedzenie"
  ]
  node [
    id 41
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 42
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 43
    label "dzieje"
  ]
  node [
    id 44
    label "zegar"
  ]
  node [
    id 45
    label "koniugacja"
  ]
  node [
    id 46
    label "trawi&#263;"
  ]
  node [
    id 47
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 48
    label "poprzedza&#263;"
  ]
  node [
    id 49
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 50
    label "trawienie"
  ]
  node [
    id 51
    label "chwila"
  ]
  node [
    id 52
    label "rachuba_czasu"
  ]
  node [
    id 53
    label "poprzedzanie"
  ]
  node [
    id 54
    label "okres_czasu"
  ]
  node [
    id 55
    label "period"
  ]
  node [
    id 56
    label "odwlekanie_si&#281;"
  ]
  node [
    id 57
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 58
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 59
    label "pochodzi&#263;"
  ]
  node [
    id 60
    label "Rzym_Zachodni"
  ]
  node [
    id 61
    label "Rzym_Wschodni"
  ]
  node [
    id 62
    label "element"
  ]
  node [
    id 63
    label "ilo&#347;&#263;"
  ]
  node [
    id 64
    label "whole"
  ]
  node [
    id 65
    label "urz&#261;dzenie"
  ]
  node [
    id 66
    label "cz&#322;owiek"
  ]
  node [
    id 67
    label "hipnoza"
  ]
  node [
    id 68
    label "przekazior"
  ]
  node [
    id 69
    label "&#347;rodek"
  ]
  node [
    id 70
    label "warunki"
  ]
  node [
    id 71
    label "publikator"
  ]
  node [
    id 72
    label "spirytysta"
  ]
  node [
    id 73
    label "strona"
  ]
  node [
    id 74
    label "otoczenie"
  ]
  node [
    id 75
    label "jasnowidz"
  ]
  node [
    id 76
    label "MAN_SE"
  ]
  node [
    id 77
    label "Spo&#322;em"
  ]
  node [
    id 78
    label "Orbis"
  ]
  node [
    id 79
    label "Canon"
  ]
  node [
    id 80
    label "HP"
  ]
  node [
    id 81
    label "nazwa_w&#322;asna"
  ]
  node [
    id 82
    label "zasoby"
  ]
  node [
    id 83
    label "zasoby_ludzkie"
  ]
  node [
    id 84
    label "klasa"
  ]
  node [
    id 85
    label "Baltona"
  ]
  node [
    id 86
    label "zaufanie"
  ]
  node [
    id 87
    label "paczkarnia"
  ]
  node [
    id 88
    label "reengineering"
  ]
  node [
    id 89
    label "siedziba"
  ]
  node [
    id 90
    label "Orlen"
  ]
  node [
    id 91
    label "miejsce_pracy"
  ]
  node [
    id 92
    label "Pewex"
  ]
  node [
    id 93
    label "biurowiec"
  ]
  node [
    id 94
    label "Apeks"
  ]
  node [
    id 95
    label "MAC"
  ]
  node [
    id 96
    label "networking"
  ]
  node [
    id 97
    label "podmiot_gospodarczy"
  ]
  node [
    id 98
    label "Google"
  ]
  node [
    id 99
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 100
    label "Hortex"
  ]
  node [
    id 101
    label "interes"
  ]
  node [
    id 102
    label "zaleta"
  ]
  node [
    id 103
    label "rezerwa"
  ]
  node [
    id 104
    label "botanika"
  ]
  node [
    id 105
    label "jako&#347;&#263;"
  ]
  node [
    id 106
    label "type"
  ]
  node [
    id 107
    label "warstwa"
  ]
  node [
    id 108
    label "obiekt"
  ]
  node [
    id 109
    label "gromada"
  ]
  node [
    id 110
    label "atak"
  ]
  node [
    id 111
    label "mecz_mistrzowski"
  ]
  node [
    id 112
    label "class"
  ]
  node [
    id 113
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 114
    label "przedmiot"
  ]
  node [
    id 115
    label "zbi&#243;r"
  ]
  node [
    id 116
    label "Ekwici"
  ]
  node [
    id 117
    label "sala"
  ]
  node [
    id 118
    label "jednostka_systematyczna"
  ]
  node [
    id 119
    label "wagon"
  ]
  node [
    id 120
    label "przepisa&#263;"
  ]
  node [
    id 121
    label "promocja"
  ]
  node [
    id 122
    label "arrangement"
  ]
  node [
    id 123
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 124
    label "szko&#322;a"
  ]
  node [
    id 125
    label "programowanie_obiektowe"
  ]
  node [
    id 126
    label "&#347;rodowisko"
  ]
  node [
    id 127
    label "wykrzyknik"
  ]
  node [
    id 128
    label "obrona"
  ]
  node [
    id 129
    label "dziennik_lekcyjny"
  ]
  node [
    id 130
    label "typ"
  ]
  node [
    id 131
    label "znak_jako&#347;ci"
  ]
  node [
    id 132
    label "&#322;awka"
  ]
  node [
    id 133
    label "organizacja"
  ]
  node [
    id 134
    label "poziom"
  ]
  node [
    id 135
    label "grupa"
  ]
  node [
    id 136
    label "tablica"
  ]
  node [
    id 137
    label "przepisanie"
  ]
  node [
    id 138
    label "fakcja"
  ]
  node [
    id 139
    label "pomoc"
  ]
  node [
    id 140
    label "form"
  ]
  node [
    id 141
    label "kurs"
  ]
  node [
    id 142
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 143
    label "budynek"
  ]
  node [
    id 144
    label "&#321;ubianka"
  ]
  node [
    id 145
    label "Bia&#322;y_Dom"
  ]
  node [
    id 146
    label "miejsce"
  ]
  node [
    id 147
    label "dzia&#322;_personalny"
  ]
  node [
    id 148
    label "Kreml"
  ]
  node [
    id 149
    label "sadowisko"
  ]
  node [
    id 150
    label "asymilowa&#263;"
  ]
  node [
    id 151
    label "nasada"
  ]
  node [
    id 152
    label "profanum"
  ]
  node [
    id 153
    label "wz&#243;r"
  ]
  node [
    id 154
    label "senior"
  ]
  node [
    id 155
    label "asymilowanie"
  ]
  node [
    id 156
    label "os&#322;abia&#263;"
  ]
  node [
    id 157
    label "homo_sapiens"
  ]
  node [
    id 158
    label "osoba"
  ]
  node [
    id 159
    label "ludzko&#347;&#263;"
  ]
  node [
    id 160
    label "Adam"
  ]
  node [
    id 161
    label "hominid"
  ]
  node [
    id 162
    label "posta&#263;"
  ]
  node [
    id 163
    label "portrecista"
  ]
  node [
    id 164
    label "polifag"
  ]
  node [
    id 165
    label "podw&#322;adny"
  ]
  node [
    id 166
    label "dwun&#243;g"
  ]
  node [
    id 167
    label "wapniak"
  ]
  node [
    id 168
    label "duch"
  ]
  node [
    id 169
    label "os&#322;abianie"
  ]
  node [
    id 170
    label "antropochoria"
  ]
  node [
    id 171
    label "figura"
  ]
  node [
    id 172
    label "g&#322;owa"
  ]
  node [
    id 173
    label "mikrokosmos"
  ]
  node [
    id 174
    label "oddzia&#322;ywanie"
  ]
  node [
    id 175
    label "magazyn"
  ]
  node [
    id 176
    label "dzia&#322;"
  ]
  node [
    id 177
    label "zasoby_kopalin"
  ]
  node [
    id 178
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 179
    label "z&#322;o&#380;e"
  ]
  node [
    id 180
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 181
    label "driveway"
  ]
  node [
    id 182
    label "informatyka"
  ]
  node [
    id 183
    label "ropa_naftowa"
  ]
  node [
    id 184
    label "paliwo"
  ]
  node [
    id 185
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 186
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 187
    label "strategia"
  ]
  node [
    id 188
    label "oprogramowanie"
  ]
  node [
    id 189
    label "zmienia&#263;"
  ]
  node [
    id 190
    label "odmienienie"
  ]
  node [
    id 191
    label "przer&#243;bka"
  ]
  node [
    id 192
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 193
    label "dobro"
  ]
  node [
    id 194
    label "penis"
  ]
  node [
    id 195
    label "object"
  ]
  node [
    id 196
    label "sprawa"
  ]
  node [
    id 197
    label "postawa"
  ]
  node [
    id 198
    label "faith"
  ]
  node [
    id 199
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 200
    label "opoka"
  ]
  node [
    id 201
    label "credit"
  ]
  node [
    id 202
    label "zrobienie"
  ]
  node [
    id 203
    label "zacz&#281;cie"
  ]
  node [
    id 204
    label "robi&#263;"
  ]
  node [
    id 205
    label "obserwowa&#263;"
  ]
  node [
    id 206
    label "uznawa&#263;"
  ]
  node [
    id 207
    label "pilnowa&#263;"
  ]
  node [
    id 208
    label "consider"
  ]
  node [
    id 209
    label "continue"
  ]
  node [
    id 210
    label "deliver"
  ]
  node [
    id 211
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 212
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 213
    label "my&#347;le&#263;"
  ]
  node [
    id 214
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 215
    label "oszukiwa&#263;"
  ]
  node [
    id 216
    label "tentegowa&#263;"
  ]
  node [
    id 217
    label "urz&#261;dza&#263;"
  ]
  node [
    id 218
    label "praca"
  ]
  node [
    id 219
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 220
    label "czyni&#263;"
  ]
  node [
    id 221
    label "work"
  ]
  node [
    id 222
    label "przerabia&#263;"
  ]
  node [
    id 223
    label "act"
  ]
  node [
    id 224
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 225
    label "give"
  ]
  node [
    id 226
    label "post&#281;powa&#263;"
  ]
  node [
    id 227
    label "peddle"
  ]
  node [
    id 228
    label "organizowa&#263;"
  ]
  node [
    id 229
    label "falowa&#263;"
  ]
  node [
    id 230
    label "stylizowa&#263;"
  ]
  node [
    id 231
    label "wydala&#263;"
  ]
  node [
    id 232
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 233
    label "ukazywa&#263;"
  ]
  node [
    id 234
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 235
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 236
    label "rozpatrywa&#263;"
  ]
  node [
    id 237
    label "argue"
  ]
  node [
    id 238
    label "take_care"
  ]
  node [
    id 239
    label "zamierza&#263;"
  ]
  node [
    id 240
    label "os&#261;dza&#263;"
  ]
  node [
    id 241
    label "troska&#263;_si&#281;"
  ]
  node [
    id 242
    label "przyznawa&#263;"
  ]
  node [
    id 243
    label "stwierdza&#263;"
  ]
  node [
    id 244
    label "notice"
  ]
  node [
    id 245
    label "zachowywa&#263;"
  ]
  node [
    id 246
    label "patrze&#263;"
  ]
  node [
    id 247
    label "dostrzega&#263;"
  ]
  node [
    id 248
    label "look"
  ]
  node [
    id 249
    label "cover"
  ]
  node [
    id 250
    label "za&#322;o&#380;enie"
  ]
  node [
    id 251
    label "faktor"
  ]
  node [
    id 252
    label "umowa"
  ]
  node [
    id 253
    label "ekspozycja"
  ]
  node [
    id 254
    label "condition"
  ]
  node [
    id 255
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 256
    label "agent"
  ]
  node [
    id 257
    label "infliction"
  ]
  node [
    id 258
    label "spowodowanie"
  ]
  node [
    id 259
    label "proposition"
  ]
  node [
    id 260
    label "przygotowanie"
  ]
  node [
    id 261
    label "pozak&#322;adanie"
  ]
  node [
    id 262
    label "point"
  ]
  node [
    id 263
    label "program"
  ]
  node [
    id 264
    label "poubieranie"
  ]
  node [
    id 265
    label "rozebranie"
  ]
  node [
    id 266
    label "str&#243;j"
  ]
  node [
    id 267
    label "budowla"
  ]
  node [
    id 268
    label "przewidzenie"
  ]
  node [
    id 269
    label "zak&#322;adka"
  ]
  node [
    id 270
    label "czynno&#347;&#263;"
  ]
  node [
    id 271
    label "twierdzenie"
  ]
  node [
    id 272
    label "przygotowywanie"
  ]
  node [
    id 273
    label "podwini&#281;cie"
  ]
  node [
    id 274
    label "zap&#322;acenie"
  ]
  node [
    id 275
    label "wyko&#324;czenie"
  ]
  node [
    id 276
    label "struktura"
  ]
  node [
    id 277
    label "utworzenie"
  ]
  node [
    id 278
    label "przebranie"
  ]
  node [
    id 279
    label "obleczenie"
  ]
  node [
    id 280
    label "przymierzenie"
  ]
  node [
    id 281
    label "obleczenie_si&#281;"
  ]
  node [
    id 282
    label "przywdzianie"
  ]
  node [
    id 283
    label "umieszczenie"
  ]
  node [
    id 284
    label "przyodzianie"
  ]
  node [
    id 285
    label "pokrycie"
  ]
  node [
    id 286
    label "sytuacja"
  ]
  node [
    id 287
    label "sk&#322;adnik"
  ]
  node [
    id 288
    label "wydarzenie"
  ]
  node [
    id 289
    label "czyn"
  ]
  node [
    id 290
    label "zawarcie"
  ]
  node [
    id 291
    label "zawrze&#263;"
  ]
  node [
    id 292
    label "contract"
  ]
  node [
    id 293
    label "porozumienie"
  ]
  node [
    id 294
    label "gestia_transportowa"
  ]
  node [
    id 295
    label "klauzula"
  ]
  node [
    id 296
    label "programowanie_agentowe"
  ]
  node [
    id 297
    label "facet"
  ]
  node [
    id 298
    label "wojsko"
  ]
  node [
    id 299
    label "system_wieloagentowy"
  ]
  node [
    id 300
    label "kontrakt"
  ]
  node [
    id 301
    label "wywiad"
  ]
  node [
    id 302
    label "orygina&#322;"
  ]
  node [
    id 303
    label "dzier&#380;awca"
  ]
  node [
    id 304
    label "wytw&#243;r"
  ]
  node [
    id 305
    label "rep"
  ]
  node [
    id 306
    label "&#347;ledziciel"
  ]
  node [
    id 307
    label "informator"
  ]
  node [
    id 308
    label "przedstawiciel"
  ]
  node [
    id 309
    label "zi&#243;&#322;ko"
  ]
  node [
    id 310
    label "agentura"
  ]
  node [
    id 311
    label "funkcjonariusz"
  ]
  node [
    id 312
    label "detektyw"
  ]
  node [
    id 313
    label "po&#347;rednik"
  ]
  node [
    id 314
    label "czynnik"
  ]
  node [
    id 315
    label "filtr"
  ]
  node [
    id 316
    label "kustosz"
  ]
  node [
    id 317
    label "galeria"
  ]
  node [
    id 318
    label "fotografia"
  ]
  node [
    id 319
    label "spot"
  ]
  node [
    id 320
    label "wprowadzenie"
  ]
  node [
    id 321
    label "po&#322;o&#380;enie"
  ]
  node [
    id 322
    label "parametr"
  ]
  node [
    id 323
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 324
    label "scena"
  ]
  node [
    id 325
    label "muzeum"
  ]
  node [
    id 326
    label "impreza"
  ]
  node [
    id 327
    label "Arsena&#322;"
  ]
  node [
    id 328
    label "akcja"
  ]
  node [
    id 329
    label "wystawienie"
  ]
  node [
    id 330
    label "wst&#281;p"
  ]
  node [
    id 331
    label "kurator"
  ]
  node [
    id 332
    label "operacja"
  ]
  node [
    id 333
    label "strona_&#347;wiata"
  ]
  node [
    id 334
    label "kolekcja"
  ]
  node [
    id 335
    label "wernisa&#380;"
  ]
  node [
    id 336
    label "Agropromocja"
  ]
  node [
    id 337
    label "wystawa"
  ]
  node [
    id 338
    label "wspinaczka"
  ]
  node [
    id 339
    label "wprowadzanie"
  ]
  node [
    id 340
    label "g&#243;rowanie"
  ]
  node [
    id 341
    label "ukierunkowywanie"
  ]
  node [
    id 342
    label "poprowadzenie"
  ]
  node [
    id 343
    label "eksponowanie"
  ]
  node [
    id 344
    label "doprowadzenie"
  ]
  node [
    id 345
    label "przeci&#261;ganie"
  ]
  node [
    id 346
    label "sterowanie"
  ]
  node [
    id 347
    label "dysponowanie"
  ]
  node [
    id 348
    label "kszta&#322;towanie"
  ]
  node [
    id 349
    label "management"
  ]
  node [
    id 350
    label "powodowanie"
  ]
  node [
    id 351
    label "trzymanie"
  ]
  node [
    id 352
    label "dawanie"
  ]
  node [
    id 353
    label "linia_melodyczna"
  ]
  node [
    id 354
    label "prowadzi&#263;"
  ]
  node [
    id 355
    label "drive"
  ]
  node [
    id 356
    label "przywodzenie"
  ]
  node [
    id 357
    label "aim"
  ]
  node [
    id 358
    label "lead"
  ]
  node [
    id 359
    label "oprowadzenie"
  ]
  node [
    id 360
    label "prowadzanie"
  ]
  node [
    id 361
    label "oprowadzanie"
  ]
  node [
    id 362
    label "przewy&#380;szanie"
  ]
  node [
    id 363
    label "kierowanie"
  ]
  node [
    id 364
    label "zwracanie"
  ]
  node [
    id 365
    label "przeci&#281;cie"
  ]
  node [
    id 366
    label "granie"
  ]
  node [
    id 367
    label "ta&#324;czenie"
  ]
  node [
    id 368
    label "kre&#347;lenie"
  ]
  node [
    id 369
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 370
    label "zaprowadzanie"
  ]
  node [
    id 371
    label "pozarz&#261;dzanie"
  ]
  node [
    id 372
    label "robienie"
  ]
  node [
    id 373
    label "krzywa"
  ]
  node [
    id 374
    label "przecinanie"
  ]
  node [
    id 375
    label "doprowadzanie"
  ]
  node [
    id 376
    label "pokre&#347;lenie"
  ]
  node [
    id 377
    label "usuwanie"
  ]
  node [
    id 378
    label "opowiadanie"
  ]
  node [
    id 379
    label "anointing"
  ]
  node [
    id 380
    label "sporz&#261;dzanie"
  ]
  node [
    id 381
    label "skre&#347;lanie"
  ]
  node [
    id 382
    label "uniewa&#380;nianie"
  ]
  node [
    id 383
    label "skazany"
  ]
  node [
    id 384
    label "zarz&#261;dzanie"
  ]
  node [
    id 385
    label "rozporz&#261;dzanie"
  ]
  node [
    id 386
    label "namaszczenie_chorych"
  ]
  node [
    id 387
    label "dysponowanie_si&#281;"
  ]
  node [
    id 388
    label "disposal"
  ]
  node [
    id 389
    label "rozdysponowywanie"
  ]
  node [
    id 390
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 391
    label "bycie"
  ]
  node [
    id 392
    label "fabrication"
  ]
  node [
    id 393
    label "tentegowanie"
  ]
  node [
    id 394
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 395
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 396
    label "porobienie"
  ]
  node [
    id 397
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 398
    label "creation"
  ]
  node [
    id 399
    label "bezproblemowy"
  ]
  node [
    id 400
    label "activity"
  ]
  node [
    id 401
    label "podkre&#347;lanie"
  ]
  node [
    id 402
    label "radiation"
  ]
  node [
    id 403
    label "demonstrowanie"
  ]
  node [
    id 404
    label "napromieniowywanie"
  ]
  node [
    id 405
    label "uwydatnianie_si&#281;"
  ]
  node [
    id 406
    label "przeorientowanie"
  ]
  node [
    id 407
    label "oznaczanie"
  ]
  node [
    id 408
    label "orientation"
  ]
  node [
    id 409
    label "strike"
  ]
  node [
    id 410
    label "dominance"
  ]
  node [
    id 411
    label "wygrywanie"
  ]
  node [
    id 412
    label "lepszy"
  ]
  node [
    id 413
    label "przesterowanie"
  ]
  node [
    id 414
    label "steering"
  ]
  node [
    id 415
    label "obs&#322;ugiwanie"
  ]
  node [
    id 416
    label "control"
  ]
  node [
    id 417
    label "rendition"
  ]
  node [
    id 418
    label "powracanie"
  ]
  node [
    id 419
    label "wydalanie"
  ]
  node [
    id 420
    label "haftowanie"
  ]
  node [
    id 421
    label "przekazywanie"
  ]
  node [
    id 422
    label "vomit"
  ]
  node [
    id 423
    label "przeznaczanie"
  ]
  node [
    id 424
    label "ustawianie"
  ]
  node [
    id 425
    label "znajdowanie_si&#281;"
  ]
  node [
    id 426
    label "provision"
  ]
  node [
    id 427
    label "supply"
  ]
  node [
    id 428
    label "spe&#322;nianie"
  ]
  node [
    id 429
    label "montowanie"
  ]
  node [
    id 430
    label "wzbudzanie"
  ]
  node [
    id 431
    label "rozwijanie"
  ]
  node [
    id 432
    label "training"
  ]
  node [
    id 433
    label "formation"
  ]
  node [
    id 434
    label "cause"
  ]
  node [
    id 435
    label "causal_agent"
  ]
  node [
    id 436
    label "nakierowanie_si&#281;"
  ]
  node [
    id 437
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 438
    label "wysy&#322;anie"
  ]
  node [
    id 439
    label "wyre&#380;yserowanie"
  ]
  node [
    id 440
    label "re&#380;yserowanie"
  ]
  node [
    id 441
    label "linia"
  ]
  node [
    id 442
    label "curve"
  ]
  node [
    id 443
    label "curvature"
  ]
  node [
    id 444
    label "figura_geometryczna"
  ]
  node [
    id 445
    label "poprowadzi&#263;"
  ]
  node [
    id 446
    label "sterczenie"
  ]
  node [
    id 447
    label "eksponowa&#263;"
  ]
  node [
    id 448
    label "g&#243;rowa&#263;"
  ]
  node [
    id 449
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 450
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 451
    label "sterowa&#263;"
  ]
  node [
    id 452
    label "kierowa&#263;"
  ]
  node [
    id 453
    label "string"
  ]
  node [
    id 454
    label "kre&#347;li&#263;"
  ]
  node [
    id 455
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 456
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 457
    label "&#380;y&#263;"
  ]
  node [
    id 458
    label "partner"
  ]
  node [
    id 459
    label "ukierunkowywa&#263;"
  ]
  node [
    id 460
    label "przesuwa&#263;"
  ]
  node [
    id 461
    label "tworzy&#263;"
  ]
  node [
    id 462
    label "powodowa&#263;"
  ]
  node [
    id 463
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 464
    label "message"
  ]
  node [
    id 465
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 466
    label "navigate"
  ]
  node [
    id 467
    label "manipulate"
  ]
  node [
    id 468
    label "akapit"
  ]
  node [
    id 469
    label "artyku&#322;"
  ]
  node [
    id 470
    label "zesp&#243;&#322;"
  ]
  node [
    id 471
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 472
    label "udost&#281;pnianie"
  ]
  node [
    id 473
    label "pra&#380;enie"
  ]
  node [
    id 474
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 475
    label "urz&#261;dzanie"
  ]
  node [
    id 476
    label "p&#322;acenie"
  ]
  node [
    id 477
    label "puszczanie_si&#281;"
  ]
  node [
    id 478
    label "dodawanie"
  ]
  node [
    id 479
    label "wyst&#281;powanie"
  ]
  node [
    id 480
    label "bycie_w_posiadaniu"
  ]
  node [
    id 481
    label "communication"
  ]
  node [
    id 482
    label "&#322;adowanie"
  ]
  node [
    id 483
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 484
    label "nalewanie"
  ]
  node [
    id 485
    label "powierzanie"
  ]
  node [
    id 486
    label "zezwalanie"
  ]
  node [
    id 487
    label "giving"
  ]
  node [
    id 488
    label "obiecywanie"
  ]
  node [
    id 489
    label "odst&#281;powanie"
  ]
  node [
    id 490
    label "dostarczanie"
  ]
  node [
    id 491
    label "wymienianie_si&#281;"
  ]
  node [
    id 492
    label "emission"
  ]
  node [
    id 493
    label "administration"
  ]
  node [
    id 494
    label "umo&#380;liwianie"
  ]
  node [
    id 495
    label "pasowanie"
  ]
  node [
    id 496
    label "otwarcie"
  ]
  node [
    id 497
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 498
    label "playing"
  ]
  node [
    id 499
    label "dzianie_si&#281;"
  ]
  node [
    id 500
    label "rozgrywanie"
  ]
  node [
    id 501
    label "pretense"
  ]
  node [
    id 502
    label "dogranie"
  ]
  node [
    id 503
    label "uderzenie"
  ]
  node [
    id 504
    label "zwalczenie"
  ]
  node [
    id 505
    label "lewa"
  ]
  node [
    id 506
    label "instrumentalizacja"
  ]
  node [
    id 507
    label "migotanie"
  ]
  node [
    id 508
    label "wydawanie"
  ]
  node [
    id 509
    label "ust&#281;powanie"
  ]
  node [
    id 510
    label "odegranie_si&#281;"
  ]
  node [
    id 511
    label "grywanie"
  ]
  node [
    id 512
    label "dogrywanie"
  ]
  node [
    id 513
    label "wybijanie"
  ]
  node [
    id 514
    label "wykonywanie"
  ]
  node [
    id 515
    label "pogranie"
  ]
  node [
    id 516
    label "brzmienie"
  ]
  node [
    id 517
    label "na&#347;ladowanie"
  ]
  node [
    id 518
    label "przygrywanie"
  ]
  node [
    id 519
    label "nagranie_si&#281;"
  ]
  node [
    id 520
    label "wyr&#243;wnywanie"
  ]
  node [
    id 521
    label "rozegranie_si&#281;"
  ]
  node [
    id 522
    label "odgrywanie_si&#281;"
  ]
  node [
    id 523
    label "&#347;ciganie"
  ]
  node [
    id 524
    label "wyr&#243;wnanie"
  ]
  node [
    id 525
    label "pr&#243;bowanie"
  ]
  node [
    id 526
    label "szczekanie"
  ]
  node [
    id 527
    label "gra_w_karty"
  ]
  node [
    id 528
    label "przedstawianie"
  ]
  node [
    id 529
    label "instrument_muzyczny"
  ]
  node [
    id 530
    label "glitter"
  ]
  node [
    id 531
    label "igranie"
  ]
  node [
    id 532
    label "wybicie"
  ]
  node [
    id 533
    label "mienienie_si&#281;"
  ]
  node [
    id 534
    label "prezentowanie"
  ]
  node [
    id 535
    label "rola"
  ]
  node [
    id 536
    label "staranie_si&#281;"
  ]
  node [
    id 537
    label "wytyczenie"
  ]
  node [
    id 538
    label "nakre&#347;lenie"
  ]
  node [
    id 539
    label "proverb"
  ]
  node [
    id 540
    label "guidance"
  ]
  node [
    id 541
    label "nata&#324;czenie_si&#281;"
  ]
  node [
    id 542
    label "dancing"
  ]
  node [
    id 543
    label "poruszanie_si&#281;"
  ]
  node [
    id 544
    label "chwianie_si&#281;"
  ]
  node [
    id 545
    label "rozta&#324;czenie_si&#281;"
  ]
  node [
    id 546
    label "przeta&#324;czenie"
  ]
  node [
    id 547
    label "gibanie"
  ]
  node [
    id 548
    label "pota&#324;czenie"
  ]
  node [
    id 549
    label "pokazywanie"
  ]
  node [
    id 550
    label "zaczynanie"
  ]
  node [
    id 551
    label "w&#322;&#261;czanie"
  ]
  node [
    id 552
    label "initiation"
  ]
  node [
    id 553
    label "umieszczanie"
  ]
  node [
    id 554
    label "zak&#322;&#243;canie"
  ]
  node [
    id 555
    label "retraction"
  ]
  node [
    id 556
    label "zapoznawanie"
  ]
  node [
    id 557
    label "wpisywanie"
  ]
  node [
    id 558
    label "wchodzenie"
  ]
  node [
    id 559
    label "przewietrzanie"
  ]
  node [
    id 560
    label "trigger"
  ]
  node [
    id 561
    label "mental_hospital"
  ]
  node [
    id 562
    label "rynek"
  ]
  node [
    id 563
    label "sp&#281;dzenie"
  ]
  node [
    id 564
    label "wzbudzenie"
  ]
  node [
    id 565
    label "zainstalowanie"
  ]
  node [
    id 566
    label "spe&#322;nienie"
  ]
  node [
    id 567
    label "znalezienie_si&#281;"
  ]
  node [
    id 568
    label "introduction"
  ]
  node [
    id 569
    label "pos&#322;anie"
  ]
  node [
    id 570
    label "adduction"
  ]
  node [
    id 571
    label "sk&#322;anianie"
  ]
  node [
    id 572
    label "przypominanie"
  ]
  node [
    id 573
    label "kojarzenie_si&#281;"
  ]
  node [
    id 574
    label "pokazanie"
  ]
  node [
    id 575
    label "umo&#380;liwienie"
  ]
  node [
    id 576
    label "wej&#347;cie"
  ]
  node [
    id 577
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 578
    label "evocation"
  ]
  node [
    id 579
    label "nuklearyzacja"
  ]
  node [
    id 580
    label "w&#322;&#261;czenie"
  ]
  node [
    id 581
    label "zapoznanie"
  ]
  node [
    id 582
    label "podstawy"
  ]
  node [
    id 583
    label "entrance"
  ]
  node [
    id 584
    label "wpisanie"
  ]
  node [
    id 585
    label "deduction"
  ]
  node [
    id 586
    label "issue"
  ]
  node [
    id 587
    label "przewietrzenie"
  ]
  node [
    id 588
    label "przed&#322;u&#380;anie"
  ]
  node [
    id 589
    label "przemieszczanie"
  ]
  node [
    id 590
    label "rozci&#261;ganie"
  ]
  node [
    id 591
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 592
    label "wymawianie"
  ]
  node [
    id 593
    label "j&#261;kanie"
  ]
  node [
    id 594
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 595
    label "przymocowywanie"
  ]
  node [
    id 596
    label "pull"
  ]
  node [
    id 597
    label "przesuwanie"
  ]
  node [
    id 598
    label "zaci&#261;ganie"
  ]
  node [
    id 599
    label "przetykanie"
  ]
  node [
    id 600
    label "podzielenie"
  ]
  node [
    id 601
    label "poprzecinanie"
  ]
  node [
    id 602
    label "przerwanie"
  ]
  node [
    id 603
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 604
    label "w&#281;ze&#322;"
  ]
  node [
    id 605
    label "zranienie"
  ]
  node [
    id 606
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 607
    label "cut"
  ]
  node [
    id 608
    label "snub"
  ]
  node [
    id 609
    label "carving"
  ]
  node [
    id 610
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 611
    label "time"
  ]
  node [
    id 612
    label "kaleczenie"
  ]
  node [
    id 613
    label "przerywanie"
  ]
  node [
    id 614
    label "dzielenie"
  ]
  node [
    id 615
    label "intersection"
  ]
  node [
    id 616
    label "film_editing"
  ]
  node [
    id 617
    label "utrzymywanie"
  ]
  node [
    id 618
    label "wypuszczanie"
  ]
  node [
    id 619
    label "noszenie"
  ]
  node [
    id 620
    label "przetrzymanie"
  ]
  node [
    id 621
    label "przetrzymywanie"
  ]
  node [
    id 622
    label "hodowanie"
  ]
  node [
    id 623
    label "wypuszczenie"
  ]
  node [
    id 624
    label "niesienie"
  ]
  node [
    id 625
    label "utrzymanie"
  ]
  node [
    id 626
    label "zmuszanie"
  ]
  node [
    id 627
    label "podtrzymywanie"
  ]
  node [
    id 628
    label "sprawowanie"
  ]
  node [
    id 629
    label "poise"
  ]
  node [
    id 630
    label "uniemo&#380;liwianie"
  ]
  node [
    id 631
    label "dzier&#380;enie"
  ]
  node [
    id 632
    label "clasp"
  ]
  node [
    id 633
    label "zachowywanie"
  ]
  node [
    id 634
    label "detention"
  ]
  node [
    id 635
    label "potrzymanie"
  ]
  node [
    id 636
    label "retention"
  ]
  node [
    id 637
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 638
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 639
    label "korzy&#347;&#263;"
  ]
  node [
    id 640
    label "absolutorium"
  ]
  node [
    id 641
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 642
    label "dzia&#322;anie"
  ]
  node [
    id 643
    label "rzecz"
  ]
  node [
    id 644
    label "rozprawa"
  ]
  node [
    id 645
    label "kognicja"
  ]
  node [
    id 646
    label "idea"
  ]
  node [
    id 647
    label "przes&#322;anka"
  ]
  node [
    id 648
    label "szczeg&#243;&#322;"
  ]
  node [
    id 649
    label "temat"
  ]
  node [
    id 650
    label "plan"
  ]
  node [
    id 651
    label "consumption"
  ]
  node [
    id 652
    label "startup"
  ]
  node [
    id 653
    label "rynek_wt&#243;rny"
  ]
  node [
    id 654
    label "gospodarka"
  ]
  node [
    id 655
    label "pojawienie_si&#281;"
  ]
  node [
    id 656
    label "segment_rynku"
  ]
  node [
    id 657
    label "targowica"
  ]
  node [
    id 658
    label "obiekt_handlowy"
  ]
  node [
    id 659
    label "konsument"
  ]
  node [
    id 660
    label "rynek_podstawowy"
  ]
  node [
    id 661
    label "emitowa&#263;"
  ]
  node [
    id 662
    label "wprowadzi&#263;"
  ]
  node [
    id 663
    label "wytw&#243;rca"
  ]
  node [
    id 664
    label "emitowanie"
  ]
  node [
    id 665
    label "plac"
  ]
  node [
    id 666
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 667
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 668
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 669
    label "stoisko"
  ]
  node [
    id 670
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 671
    label "wprowadza&#263;"
  ]
  node [
    id 672
    label "kram"
  ]
  node [
    id 673
    label "pora_roku"
  ]
  node [
    id 674
    label "kwarta&#322;"
  ]
  node [
    id 675
    label "jubileusz"
  ]
  node [
    id 676
    label "miesi&#261;c"
  ]
  node [
    id 677
    label "martwy_sezon"
  ]
  node [
    id 678
    label "stulecie"
  ]
  node [
    id 679
    label "cykl_astronomiczny"
  ]
  node [
    id 680
    label "lata"
  ]
  node [
    id 681
    label "p&#243;&#322;rocze"
  ]
  node [
    id 682
    label "kalendarz"
  ]
  node [
    id 683
    label "summer"
  ]
  node [
    id 684
    label "kompozycja"
  ]
  node [
    id 685
    label "pakiet_klimatyczny"
  ]
  node [
    id 686
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 687
    label "cz&#261;steczka"
  ]
  node [
    id 688
    label "specgrupa"
  ]
  node [
    id 689
    label "egzemplarz"
  ]
  node [
    id 690
    label "stage_set"
  ]
  node [
    id 691
    label "odm&#322;odzenie"
  ]
  node [
    id 692
    label "odm&#322;adza&#263;"
  ]
  node [
    id 693
    label "harcerze_starsi"
  ]
  node [
    id 694
    label "oddzia&#322;"
  ]
  node [
    id 695
    label "category"
  ]
  node [
    id 696
    label "liga"
  ]
  node [
    id 697
    label "&#346;wietliki"
  ]
  node [
    id 698
    label "formacja_geologiczna"
  ]
  node [
    id 699
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 700
    label "Eurogrupa"
  ]
  node [
    id 701
    label "Terranie"
  ]
  node [
    id 702
    label "odm&#322;adzanie"
  ]
  node [
    id 703
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 704
    label "Entuzjastki"
  ]
  node [
    id 705
    label "tydzie&#324;"
  ]
  node [
    id 706
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 707
    label "miech"
  ]
  node [
    id 708
    label "kalendy"
  ]
  node [
    id 709
    label "rok_szkolny"
  ]
  node [
    id 710
    label "term"
  ]
  node [
    id 711
    label "rok_akademicki"
  ]
  node [
    id 712
    label "semester"
  ]
  node [
    id 713
    label "rocznica"
  ]
  node [
    id 714
    label "anniwersarz"
  ]
  node [
    id 715
    label "obszar"
  ]
  node [
    id 716
    label "long_time"
  ]
  node [
    id 717
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 718
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 719
    label "almanac"
  ]
  node [
    id 720
    label "wydawnictwo"
  ]
  node [
    id 721
    label "rozk&#322;ad"
  ]
  node [
    id 722
    label "Juliusz_Cezar"
  ]
  node [
    id 723
    label "zwy&#380;kowanie"
  ]
  node [
    id 724
    label "cedu&#322;a"
  ]
  node [
    id 725
    label "manner"
  ]
  node [
    id 726
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 727
    label "przejazd"
  ]
  node [
    id 728
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 729
    label "deprecjacja"
  ]
  node [
    id 730
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 731
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 732
    label "stawka"
  ]
  node [
    id 733
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 734
    label "przeorientowywanie"
  ]
  node [
    id 735
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 736
    label "nauka"
  ]
  node [
    id 737
    label "seria"
  ]
  node [
    id 738
    label "Lira"
  ]
  node [
    id 739
    label "course"
  ]
  node [
    id 740
    label "passage"
  ]
  node [
    id 741
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 742
    label "trasa"
  ]
  node [
    id 743
    label "przeorientowa&#263;"
  ]
  node [
    id 744
    label "bearing"
  ]
  node [
    id 745
    label "spos&#243;b"
  ]
  node [
    id 746
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 747
    label "way"
  ]
  node [
    id 748
    label "zni&#380;kowanie"
  ]
  node [
    id 749
    label "przeorientowywa&#263;"
  ]
  node [
    id 750
    label "kierunek"
  ]
  node [
    id 751
    label "zaj&#281;cia"
  ]
  node [
    id 752
    label "zmieni&#263;"
  ]
  node [
    id 753
    label "worsen"
  ]
  node [
    id 754
    label "zrobi&#263;"
  ]
  node [
    id 755
    label "come_up"
  ]
  node [
    id 756
    label "sprawi&#263;"
  ]
  node [
    id 757
    label "zyska&#263;"
  ]
  node [
    id 758
    label "change"
  ]
  node [
    id 759
    label "straci&#263;"
  ]
  node [
    id 760
    label "zast&#261;pi&#263;"
  ]
  node [
    id 761
    label "przej&#347;&#263;"
  ]
  node [
    id 762
    label "figura_stylistyczna"
  ]
  node [
    id 763
    label "simile"
  ]
  node [
    id 764
    label "zanalizowanie"
  ]
  node [
    id 765
    label "comparison"
  ]
  node [
    id 766
    label "zestawienie"
  ]
  node [
    id 767
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 768
    label "udowodnienie"
  ]
  node [
    id 769
    label "rozwa&#380;enie"
  ]
  node [
    id 770
    label "przebadanie"
  ]
  node [
    id 771
    label "catalog"
  ]
  node [
    id 772
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 773
    label "pozycja"
  ]
  node [
    id 774
    label "wyra&#380;enie"
  ]
  node [
    id 775
    label "tekst"
  ]
  node [
    id 776
    label "analiza"
  ]
  node [
    id 777
    label "composition"
  ]
  node [
    id 778
    label "ustawienie"
  ]
  node [
    id 779
    label "count"
  ]
  node [
    id 780
    label "figurowa&#263;"
  ]
  node [
    id 781
    label "przedstawienie"
  ]
  node [
    id 782
    label "strata"
  ]
  node [
    id 783
    label "z&#322;&#261;czenie"
  ]
  node [
    id 784
    label "informacja"
  ]
  node [
    id 785
    label "stock"
  ]
  node [
    id 786
    label "obrot&#243;wka"
  ]
  node [
    id 787
    label "book"
  ]
  node [
    id 788
    label "z&#322;amanie"
  ]
  node [
    id 789
    label "sprawozdanie_finansowe"
  ]
  node [
    id 790
    label "z&#322;o&#380;enie"
  ]
  node [
    id 791
    label "deficyt"
  ]
  node [
    id 792
    label "set"
  ]
  node [
    id 793
    label "wyliczanka"
  ]
  node [
    id 794
    label "sumariusz"
  ]
  node [
    id 795
    label "eklektyk"
  ]
  node [
    id 796
    label "differentiation"
  ]
  node [
    id 797
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 798
    label "bogactwo"
  ]
  node [
    id 799
    label "cecha"
  ]
  node [
    id 800
    label "discrimination"
  ]
  node [
    id 801
    label "rozdzielenie"
  ]
  node [
    id 802
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 803
    label "diverseness"
  ]
  node [
    id 804
    label "nadanie"
  ]
  node [
    id 805
    label "multikulturalizm"
  ]
  node [
    id 806
    label "rozproszenie_si&#281;"
  ]
  node [
    id 807
    label "przesz&#322;y"
  ]
  node [
    id 808
    label "wcze&#347;niejszy"
  ]
  node [
    id 809
    label "poprzednio"
  ]
  node [
    id 810
    label "ostatni"
  ]
  node [
    id 811
    label "miniony"
  ]
  node [
    id 812
    label "wcze&#347;niej"
  ]
  node [
    id 813
    label "rise"
  ]
  node [
    id 814
    label "appear"
  ]
  node [
    id 815
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 816
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 817
    label "krytykowanie"
  ]
  node [
    id 818
    label "sprawdzanie"
  ]
  node [
    id 819
    label "udowadnianie"
  ]
  node [
    id 820
    label "examination"
  ]
  node [
    id 821
    label "ustalenie"
  ]
  node [
    id 822
    label "macanie"
  ]
  node [
    id 823
    label "analysis"
  ]
  node [
    id 824
    label "rezultat"
  ]
  node [
    id 825
    label "investigation"
  ]
  node [
    id 826
    label "usi&#322;owanie"
  ]
  node [
    id 827
    label "dociekanie"
  ]
  node [
    id 828
    label "bia&#322;a_niedziela"
  ]
  node [
    id 829
    label "obserwowanie"
  ]
  node [
    id 830
    label "penetrowanie"
  ]
  node [
    id 831
    label "kontrola"
  ]
  node [
    id 832
    label "zrecenzowanie"
  ]
  node [
    id 833
    label "diagnostyka"
  ]
  node [
    id 834
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 835
    label "ustalanie"
  ]
  node [
    id 836
    label "omawianie"
  ]
  node [
    id 837
    label "wziernikowanie"
  ]
  node [
    id 838
    label "rozpatrywanie"
  ]
  node [
    id 839
    label "rektalny"
  ]
  node [
    id 840
    label "czepianie_si&#281;"
  ]
  node [
    id 841
    label "opiniowanie"
  ]
  node [
    id 842
    label "ocenianie"
  ]
  node [
    id 843
    label "dyskutowanie"
  ]
  node [
    id 844
    label "discussion"
  ]
  node [
    id 845
    label "zaopiniowanie"
  ]
  node [
    id 846
    label "decydowanie"
  ]
  node [
    id 847
    label "colony"
  ]
  node [
    id 848
    label "colonization"
  ]
  node [
    id 849
    label "liquidation"
  ]
  node [
    id 850
    label "umacnianie"
  ]
  node [
    id 851
    label "umocnienie"
  ]
  node [
    id 852
    label "zdecydowanie"
  ]
  node [
    id 853
    label "appointment"
  ]
  node [
    id 854
    label "decyzja"
  ]
  node [
    id 855
    label "localization"
  ]
  node [
    id 856
    label "penetration"
  ]
  node [
    id 857
    label "przeszukiwanie"
  ]
  node [
    id 858
    label "docieranie"
  ]
  node [
    id 859
    label "przemy&#347;liwanie"
  ]
  node [
    id 860
    label "przyczyna"
  ]
  node [
    id 861
    label "event"
  ]
  node [
    id 862
    label "perlustracja"
  ]
  node [
    id 863
    label "legalizacja_ponowna"
  ]
  node [
    id 864
    label "legalizacja_pierwotna"
  ]
  node [
    id 865
    label "w&#322;adza"
  ]
  node [
    id 866
    label "instytucja"
  ]
  node [
    id 867
    label "effort"
  ]
  node [
    id 868
    label "podejmowanie"
  ]
  node [
    id 869
    label "essay"
  ]
  node [
    id 870
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 871
    label "przymierzanie"
  ]
  node [
    id 872
    label "redagowanie"
  ]
  node [
    id 873
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 874
    label "zaw&#243;d"
  ]
  node [
    id 875
    label "zmiana"
  ]
  node [
    id 876
    label "pracowanie"
  ]
  node [
    id 877
    label "pracowa&#263;"
  ]
  node [
    id 878
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 879
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 880
    label "czynnik_produkcji"
  ]
  node [
    id 881
    label "stosunek_pracy"
  ]
  node [
    id 882
    label "kierownictwo"
  ]
  node [
    id 883
    label "najem"
  ]
  node [
    id 884
    label "zak&#322;ad"
  ]
  node [
    id 885
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 886
    label "tynkarski"
  ]
  node [
    id 887
    label "tyrka"
  ]
  node [
    id 888
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 889
    label "benedykty&#324;ski"
  ]
  node [
    id 890
    label "poda&#380;_pracy"
  ]
  node [
    id 891
    label "zobowi&#261;zanie"
  ]
  node [
    id 892
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 893
    label "presentation"
  ]
  node [
    id 894
    label "analizowanie"
  ]
  node [
    id 895
    label "uzasadnianie"
  ]
  node [
    id 896
    label "endoscopy"
  ]
  node [
    id 897
    label "quest"
  ]
  node [
    id 898
    label "dop&#322;ywanie"
  ]
  node [
    id 899
    label "rozmy&#347;lanie"
  ]
  node [
    id 900
    label "examen"
  ]
  node [
    id 901
    label "anamneza"
  ]
  node [
    id 902
    label "medycyna"
  ]
  node [
    id 903
    label "diagnosis"
  ]
  node [
    id 904
    label "namacanie"
  ]
  node [
    id 905
    label "pomacanie"
  ]
  node [
    id 906
    label "palpation"
  ]
  node [
    id 907
    label "dotykanie"
  ]
  node [
    id 908
    label "feel"
  ]
  node [
    id 909
    label "dr&#243;b"
  ]
  node [
    id 910
    label "doszukanie_si&#281;"
  ]
  node [
    id 911
    label "patrzenie"
  ]
  node [
    id 912
    label "observation"
  ]
  node [
    id 913
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 914
    label "dostrzeganie"
  ]
  node [
    id 915
    label "bocianie_gniazdo"
  ]
  node [
    id 916
    label "poobserwowanie"
  ]
  node [
    id 917
    label "charakter"
  ]
  node [
    id 918
    label "stan"
  ]
  node [
    id 919
    label "kwas"
  ]
  node [
    id 920
    label "klimat"
  ]
  node [
    id 921
    label "state"
  ]
  node [
    id 922
    label "samopoczucie"
  ]
  node [
    id 923
    label "fizjonomia"
  ]
  node [
    id 924
    label "kompleksja"
  ]
  node [
    id 925
    label "zjawisko"
  ]
  node [
    id 926
    label "entity"
  ]
  node [
    id 927
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 928
    label "psychika"
  ]
  node [
    id 929
    label "osobowo&#347;&#263;"
  ]
  node [
    id 930
    label "charakterystyka"
  ]
  node [
    id 931
    label "m&#322;ot"
  ]
  node [
    id 932
    label "marka"
  ]
  node [
    id 933
    label "pr&#243;ba"
  ]
  node [
    id 934
    label "attribute"
  ]
  node [
    id 935
    label "drzewo"
  ]
  node [
    id 936
    label "znak"
  ]
  node [
    id 937
    label "Arakan"
  ]
  node [
    id 938
    label "Teksas"
  ]
  node [
    id 939
    label "Georgia"
  ]
  node [
    id 940
    label "Maryland"
  ]
  node [
    id 941
    label "Michigan"
  ]
  node [
    id 942
    label "Massachusetts"
  ]
  node [
    id 943
    label "Luizjana"
  ]
  node [
    id 944
    label "by&#263;"
  ]
  node [
    id 945
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 946
    label "Floryda"
  ]
  node [
    id 947
    label "Ohio"
  ]
  node [
    id 948
    label "Alaska"
  ]
  node [
    id 949
    label "Nowy_Meksyk"
  ]
  node [
    id 950
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 951
    label "wci&#281;cie"
  ]
  node [
    id 952
    label "Kansas"
  ]
  node [
    id 953
    label "Alabama"
  ]
  node [
    id 954
    label "Kalifornia"
  ]
  node [
    id 955
    label "Wirginia"
  ]
  node [
    id 956
    label "punkt"
  ]
  node [
    id 957
    label "Nowy_York"
  ]
  node [
    id 958
    label "Waszyngton"
  ]
  node [
    id 959
    label "Pensylwania"
  ]
  node [
    id 960
    label "wektor"
  ]
  node [
    id 961
    label "Hawaje"
  ]
  node [
    id 962
    label "jednostka_administracyjna"
  ]
  node [
    id 963
    label "Illinois"
  ]
  node [
    id 964
    label "Oklahoma"
  ]
  node [
    id 965
    label "Oregon"
  ]
  node [
    id 966
    label "Arizona"
  ]
  node [
    id 967
    label "Jukatan"
  ]
  node [
    id 968
    label "shape"
  ]
  node [
    id 969
    label "Goa"
  ]
  node [
    id 970
    label "dyspozycja"
  ]
  node [
    id 971
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 972
    label "forma"
  ]
  node [
    id 973
    label "reszta_kwasowa"
  ]
  node [
    id 974
    label "narkotyk"
  ]
  node [
    id 975
    label "atmosfera"
  ]
  node [
    id 976
    label "ferment"
  ]
  node [
    id 977
    label "psychoaktywny"
  ]
  node [
    id 978
    label "nap&#243;j"
  ]
  node [
    id 979
    label "substancja"
  ]
  node [
    id 980
    label "kicha"
  ]
  node [
    id 981
    label "styl"
  ]
  node [
    id 982
    label "osoba_fizyczna"
  ]
  node [
    id 983
    label "klasa_&#347;rednia"
  ]
  node [
    id 984
    label "wydawca"
  ]
  node [
    id 985
    label "kapitalista"
  ]
  node [
    id 986
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 987
    label "wsp&#243;lnik"
  ]
  node [
    id 988
    label "podmiot"
  ]
  node [
    id 989
    label "wykupywanie"
  ]
  node [
    id 990
    label "wykupienie"
  ]
  node [
    id 991
    label "uczestnik"
  ]
  node [
    id 992
    label "sp&#243;lnik"
  ]
  node [
    id 993
    label "uczestniczenie"
  ]
  node [
    id 994
    label "issuer"
  ]
  node [
    id 995
    label "industrializm"
  ]
  node [
    id 996
    label "rentier"
  ]
  node [
    id 997
    label "finansjera"
  ]
  node [
    id 998
    label "bogacz"
  ]
  node [
    id 999
    label "realize"
  ]
  node [
    id 1000
    label "spieni&#281;&#380;y&#263;"
  ]
  node [
    id 1001
    label "wykorzysta&#263;"
  ]
  node [
    id 1002
    label "manufacture"
  ]
  node [
    id 1003
    label "actualize"
  ]
  node [
    id 1004
    label "spowodowa&#263;"
  ]
  node [
    id 1005
    label "stworzy&#263;"
  ]
  node [
    id 1006
    label "skorzysta&#263;"
  ]
  node [
    id 1007
    label "seize"
  ]
  node [
    id 1008
    label "u&#380;y&#263;"
  ]
  node [
    id 1009
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1010
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1011
    label "wizerunek"
  ]
  node [
    id 1012
    label "przygotowa&#263;"
  ]
  node [
    id 1013
    label "specjalista_od_public_relations"
  ]
  node [
    id 1014
    label "create"
  ]
  node [
    id 1015
    label "zhandlowa&#263;"
  ]
  node [
    id 1016
    label "capitalize"
  ]
  node [
    id 1017
    label "undertaking"
  ]
  node [
    id 1018
    label "decree"
  ]
  node [
    id 1019
    label "odebra&#263;"
  ]
  node [
    id 1020
    label "zbiegni&#281;cie"
  ]
  node [
    id 1021
    label "odbieranie"
  ]
  node [
    id 1022
    label "spadni&#281;cie"
  ]
  node [
    id 1023
    label "polecenie"
  ]
  node [
    id 1024
    label "odebranie"
  ]
  node [
    id 1025
    label "odbiera&#263;"
  ]
  node [
    id 1026
    label "consign"
  ]
  node [
    id 1027
    label "pognanie"
  ]
  node [
    id 1028
    label "recommendation"
  ]
  node [
    id 1029
    label "pobiegni&#281;cie"
  ]
  node [
    id 1030
    label "powierzenie"
  ]
  node [
    id 1031
    label "doradzenie"
  ]
  node [
    id 1032
    label "education"
  ]
  node [
    id 1033
    label "wypowied&#378;"
  ]
  node [
    id 1034
    label "zaordynowanie"
  ]
  node [
    id 1035
    label "przesadzenie"
  ]
  node [
    id 1036
    label "ukaz"
  ]
  node [
    id 1037
    label "rekomendacja"
  ]
  node [
    id 1038
    label "statement"
  ]
  node [
    id 1039
    label "zadanie"
  ]
  node [
    id 1040
    label "pouciekanie"
  ]
  node [
    id 1041
    label "wsi&#261;kni&#281;cie"
  ]
  node [
    id 1042
    label "spieprzenie"
  ]
  node [
    id 1043
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 1044
    label "wzi&#281;cie"
  ]
  node [
    id 1045
    label "zwianie"
  ]
  node [
    id 1046
    label "obni&#380;enie_si&#281;"
  ]
  node [
    id 1047
    label "sp&#322;oszenie"
  ]
  node [
    id 1048
    label "oddalenie_si&#281;"
  ]
  node [
    id 1049
    label "vent"
  ]
  node [
    id 1050
    label "przysypanie"
  ]
  node [
    id 1051
    label "spotkanie"
  ]
  node [
    id 1052
    label "wy&#347;lizgni&#281;cie_si&#281;"
  ]
  node [
    id 1053
    label "ogarni&#281;cie"
  ]
  node [
    id 1054
    label "napadni&#281;cie"
  ]
  node [
    id 1055
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1056
    label "obci&#261;&#380;enie"
  ]
  node [
    id 1057
    label "pogruchotanie_si&#281;"
  ]
  node [
    id 1058
    label "dopadanie"
  ]
  node [
    id 1059
    label "schrzanienie_si&#281;"
  ]
  node [
    id 1060
    label "pospadanie"
  ]
  node [
    id 1061
    label "opuszczenie_si&#281;"
  ]
  node [
    id 1062
    label "spierdolenie_si&#281;"
  ]
  node [
    id 1063
    label "zmniejszenie_si&#281;"
  ]
  node [
    id 1064
    label "schudni&#281;cie"
  ]
  node [
    id 1065
    label "sketch"
  ]
  node [
    id 1066
    label "odzyska&#263;"
  ]
  node [
    id 1067
    label "pozbawi&#263;"
  ]
  node [
    id 1068
    label "przyj&#261;&#263;"
  ]
  node [
    id 1069
    label "give_birth"
  ]
  node [
    id 1070
    label "zabra&#263;"
  ]
  node [
    id 1071
    label "dozna&#263;"
  ]
  node [
    id 1072
    label "chwyci&#263;"
  ]
  node [
    id 1073
    label "deprive"
  ]
  node [
    id 1074
    label "radio"
  ]
  node [
    id 1075
    label "zabiera&#263;"
  ]
  node [
    id 1076
    label "przyjmowa&#263;"
  ]
  node [
    id 1077
    label "doznawa&#263;"
  ]
  node [
    id 1078
    label "pozbawia&#263;"
  ]
  node [
    id 1079
    label "telewizor"
  ]
  node [
    id 1080
    label "fall"
  ]
  node [
    id 1081
    label "antena"
  ]
  node [
    id 1082
    label "bra&#263;"
  ]
  node [
    id 1083
    label "odzyskiwa&#263;"
  ]
  node [
    id 1084
    label "liszy&#263;"
  ]
  node [
    id 1085
    label "konfiskowa&#263;"
  ]
  node [
    id 1086
    label "accept"
  ]
  node [
    id 1087
    label "przyjmowanie"
  ]
  node [
    id 1088
    label "zabieranie"
  ]
  node [
    id 1089
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1090
    label "przyp&#322;ywanie"
  ]
  node [
    id 1091
    label "perception"
  ]
  node [
    id 1092
    label "zniewalanie"
  ]
  node [
    id 1093
    label "odzyskiwanie"
  ]
  node [
    id 1094
    label "konfiskowanie"
  ]
  node [
    id 1095
    label "dochodzenie"
  ]
  node [
    id 1096
    label "rozsi&#261;dni&#281;cie_si&#281;"
  ]
  node [
    id 1097
    label "wpadanie"
  ]
  node [
    id 1098
    label "solicitation"
  ]
  node [
    id 1099
    label "doj&#347;cie"
  ]
  node [
    id 1100
    label "collection"
  ]
  node [
    id 1101
    label "rozsiadanie_si&#281;"
  ]
  node [
    id 1102
    label "branie"
  ]
  node [
    id 1103
    label "odp&#322;ywanie"
  ]
  node [
    id 1104
    label "wpadni&#281;cie"
  ]
  node [
    id 1105
    label "withdrawal"
  ]
  node [
    id 1106
    label "pozabieranie"
  ]
  node [
    id 1107
    label "doznanie"
  ]
  node [
    id 1108
    label "chwycenie"
  ]
  node [
    id 1109
    label "przyj&#281;cie"
  ]
  node [
    id 1110
    label "removal"
  ]
  node [
    id 1111
    label "zareagowanie"
  ]
  node [
    id 1112
    label "odzyskanie"
  ]
  node [
    id 1113
    label "zabranie"
  ]
  node [
    id 1114
    label "zniewolenie"
  ]
  node [
    id 1115
    label "pa&#324;stwo_z&#322;o&#380;one"
  ]
  node [
    id 1116
    label "combination"
  ]
  node [
    id 1117
    label "konfederacja_barska"
  ]
  node [
    id 1118
    label "zwi&#261;zek"
  ]
  node [
    id 1119
    label "Unia_Europejska"
  ]
  node [
    id 1120
    label "alliance"
  ]
  node [
    id 1121
    label "Konfederacja"
  ]
  node [
    id 1122
    label "przybud&#243;wka"
  ]
  node [
    id 1123
    label "organization"
  ]
  node [
    id 1124
    label "od&#322;am"
  ]
  node [
    id 1125
    label "TOPR"
  ]
  node [
    id 1126
    label "komitet_koordynacyjny"
  ]
  node [
    id 1127
    label "przedstawicielstwo"
  ]
  node [
    id 1128
    label "ZMP"
  ]
  node [
    id 1129
    label "Cepelia"
  ]
  node [
    id 1130
    label "GOPR"
  ]
  node [
    id 1131
    label "endecki"
  ]
  node [
    id 1132
    label "ZBoWiD"
  ]
  node [
    id 1133
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1134
    label "boj&#243;wka"
  ]
  node [
    id 1135
    label "ZOMO"
  ]
  node [
    id 1136
    label "jednostka_organizacyjna"
  ]
  node [
    id 1137
    label "centrala"
  ]
  node [
    id 1138
    label "zwi&#261;zanie"
  ]
  node [
    id 1139
    label "odwadnianie"
  ]
  node [
    id 1140
    label "azeotrop"
  ]
  node [
    id 1141
    label "odwodni&#263;"
  ]
  node [
    id 1142
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1143
    label "lokant"
  ]
  node [
    id 1144
    label "marriage"
  ]
  node [
    id 1145
    label "bratnia_dusza"
  ]
  node [
    id 1146
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1147
    label "koligacja"
  ]
  node [
    id 1148
    label "odwodnienie"
  ]
  node [
    id 1149
    label "marketing_afiliacyjny"
  ]
  node [
    id 1150
    label "substancja_chemiczna"
  ]
  node [
    id 1151
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1152
    label "wi&#261;zanie"
  ]
  node [
    id 1153
    label "powi&#261;zanie"
  ]
  node [
    id 1154
    label "odwadnia&#263;"
  ]
  node [
    id 1155
    label "konstytucja"
  ]
  node [
    id 1156
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1157
    label "potw&#243;r"
  ]
  node [
    id 1158
    label "biblizm"
  ]
  node [
    id 1159
    label "monster"
  ]
  node [
    id 1160
    label "cudotw&#243;r"
  ]
  node [
    id 1161
    label "Godzilla"
  ]
  node [
    id 1162
    label "zwyrol"
  ]
  node [
    id 1163
    label "okrutnik"
  ]
  node [
    id 1164
    label "Tyfon"
  ]
  node [
    id 1165
    label "degenerat"
  ]
  node [
    id 1166
    label "Behemot"
  ]
  node [
    id 1167
    label "Cerber"
  ]
  node [
    id 1168
    label "szkarada"
  ]
  node [
    id 1169
    label "popapraniec"
  ]
  node [
    id 1170
    label "Hydra"
  ]
  node [
    id 1171
    label "stw&#243;r"
  ]
  node [
    id 1172
    label "Sodoma"
  ]
  node [
    id 1173
    label "ga&#322;&#261;zka_oliwna"
  ]
  node [
    id 1174
    label "mury_Jerycha"
  ]
  node [
    id 1175
    label "ucho_igielne"
  ]
  node [
    id 1176
    label "dziecko_Beliala"
  ]
  node [
    id 1177
    label "miska_soczewicy"
  ]
  node [
    id 1178
    label "je&#378;dziec_Apokalipsy"
  ]
  node [
    id 1179
    label "palec_bo&#380;y"
  ]
  node [
    id 1180
    label "judaszowe_srebrniki"
  ]
  node [
    id 1181
    label "niewierny_Tomasz"
  ]
  node [
    id 1182
    label "listek_figowy"
  ]
  node [
    id 1183
    label "odst&#281;pca"
  ]
  node [
    id 1184
    label "syn_marnotrawny"
  ]
  node [
    id 1185
    label "krzew_gorej&#261;cy"
  ]
  node [
    id 1186
    label "wiek_matuzalemowy"
  ]
  node [
    id 1187
    label "fa&#322;szywy_prorok"
  ]
  node [
    id 1188
    label "z&#322;oty_cielec"
  ]
  node [
    id 1189
    label "miecz_obosieczny"
  ]
  node [
    id 1190
    label "korona_cierniowa"
  ]
  node [
    id 1191
    label "samarytanka"
  ]
  node [
    id 1192
    label "alfa_i_omega"
  ]
  node [
    id 1193
    label "s&#243;l_ziemi"
  ]
  node [
    id 1194
    label "chleb_powszedni"
  ]
  node [
    id 1195
    label "arka_przymierza"
  ]
  node [
    id 1196
    label "Gomora"
  ]
  node [
    id 1197
    label "tr&#261;by_jerycho&#324;skie"
  ]
  node [
    id 1198
    label "&#380;ona_Putyfara"
  ]
  node [
    id 1199
    label "&#322;ono_Abrahama"
  ]
  node [
    id 1200
    label "list_Uriasza"
  ]
  node [
    id 1201
    label "miedziane_czo&#322;o"
  ]
  node [
    id 1202
    label "wie&#380;a_Babel"
  ]
  node [
    id 1203
    label "szatan"
  ]
  node [
    id 1204
    label "Herod"
  ]
  node [
    id 1205
    label "kainowe_pi&#281;tno"
  ]
  node [
    id 1206
    label "manna_z_nieba"
  ]
  node [
    id 1207
    label "&#380;ona_Lota"
  ]
  node [
    id 1208
    label "wdowi_grosz"
  ]
  node [
    id 1209
    label "o&#347;lica_Balaama"
  ]
  node [
    id 1210
    label "grdyka"
  ]
  node [
    id 1211
    label "winnica_Nabota"
  ]
  node [
    id 1212
    label "kamie&#324;_w&#281;gielny"
  ]
  node [
    id 1213
    label "droga_krzy&#380;owa"
  ]
  node [
    id 1214
    label "kozio&#322;_ofiarny"
  ]
  node [
    id 1215
    label "zb&#322;&#261;kana_owca"
  ]
  node [
    id 1216
    label "cnotliwa_Zuzanna"
  ]
  node [
    id 1217
    label "niebieski_ptak"
  ]
  node [
    id 1218
    label "niewola_egipska"
  ]
  node [
    id 1219
    label "rze&#378;_niewini&#261;tek"
  ]
  node [
    id 1220
    label "drabina_Jakubowa"
  ]
  node [
    id 1221
    label "gr&#243;b_pobielany"
  ]
  node [
    id 1222
    label "plaga_egipska"
  ]
  node [
    id 1223
    label "herod-baba"
  ]
  node [
    id 1224
    label "tr&#261;ba_jerycho&#324;ska"
  ]
  node [
    id 1225
    label "arka_Noego"
  ]
  node [
    id 1226
    label "kolos_na_glinianych_nogach"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 645
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 646
  ]
  edge [
    source 5
    target 647
  ]
  edge [
    source 5
    target 648
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 653
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 654
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 5
    target 658
  ]
  edge [
    source 5
    target 659
  ]
  edge [
    source 5
    target 660
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 661
  ]
  edge [
    source 5
    target 662
  ]
  edge [
    source 5
    target 663
  ]
  edge [
    source 5
    target 664
  ]
  edge [
    source 5
    target 665
  ]
  edge [
    source 5
    target 666
  ]
  edge [
    source 5
    target 667
  ]
  edge [
    source 5
    target 668
  ]
  edge [
    source 5
    target 669
  ]
  edge [
    source 5
    target 670
  ]
  edge [
    source 5
    target 671
  ]
  edge [
    source 5
    target 672
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 255
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 480
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1008
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 1011
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 1012
  ]
  edge [
    source 16
    target 1013
  ]
  edge [
    source 16
    target 1014
  ]
  edge [
    source 16
    target 1015
  ]
  edge [
    source 16
    target 1016
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 323
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 17
    target 89
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 304
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 210
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 350
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 372
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 276
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
]
