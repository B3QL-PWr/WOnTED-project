graph [
  node [
    id 0
    label "napi&#281;cie"
    origin "text"
  ]
  node [
    id 1
    label "niepok&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "przed"
    origin "text"
  ]
  node [
    id 3
    label "zapowiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sobota"
    origin "text"
  ]
  node [
    id 5
    label "kolejny"
    origin "text"
  ]
  node [
    id 6
    label "protest"
    origin "text"
  ]
  node [
    id 7
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 8
    label "kamizelka"
    origin "text"
  ]
  node [
    id 9
    label "francja"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;ga&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zenit"
    origin "text"
  ]
  node [
    id 12
    label "tension"
  ]
  node [
    id 13
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 14
    label "usztywnienie"
  ]
  node [
    id 15
    label "striving"
  ]
  node [
    id 16
    label "nastr&#243;j"
  ]
  node [
    id 17
    label "napr&#281;&#380;enie"
  ]
  node [
    id 18
    label "state"
  ]
  node [
    id 19
    label "klimat"
  ]
  node [
    id 20
    label "stan"
  ]
  node [
    id 21
    label "samopoczucie"
  ]
  node [
    id 22
    label "charakter"
  ]
  node [
    id 23
    label "cecha"
  ]
  node [
    id 24
    label "kwas"
  ]
  node [
    id 25
    label "web"
  ]
  node [
    id 26
    label "przedmiot"
  ]
  node [
    id 27
    label "twardy"
  ]
  node [
    id 28
    label "spowodowanie"
  ]
  node [
    id 29
    label "unieruchomienie"
  ]
  node [
    id 30
    label "stiffening"
  ]
  node [
    id 31
    label "czynno&#347;&#263;"
  ]
  node [
    id 32
    label "zjawisko"
  ]
  node [
    id 33
    label "w&#261;tpliwo&#347;&#263;"
  ]
  node [
    id 34
    label "nerwowo&#347;&#263;"
  ]
  node [
    id 35
    label "emocja"
  ]
  node [
    id 36
    label "niespokojno&#347;&#263;"
  ]
  node [
    id 37
    label "akatyzja"
  ]
  node [
    id 38
    label "motion"
  ]
  node [
    id 39
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 40
    label "wypowied&#378;"
  ]
  node [
    id 41
    label "w&#261;tpienie"
  ]
  node [
    id 42
    label "wytw&#243;r"
  ]
  node [
    id 43
    label "question"
  ]
  node [
    id 44
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 45
    label "ogrom"
  ]
  node [
    id 46
    label "iskrzy&#263;"
  ]
  node [
    id 47
    label "d&#322;awi&#263;"
  ]
  node [
    id 48
    label "ostygn&#261;&#263;"
  ]
  node [
    id 49
    label "stygn&#261;&#263;"
  ]
  node [
    id 50
    label "temperatura"
  ]
  node [
    id 51
    label "wpa&#347;&#263;"
  ]
  node [
    id 52
    label "afekt"
  ]
  node [
    id 53
    label "wpada&#263;"
  ]
  node [
    id 54
    label "zesp&#243;&#322;_chorobowy"
  ]
  node [
    id 55
    label "wyraz"
  ]
  node [
    id 56
    label "pobudliwo&#347;&#263;"
  ]
  node [
    id 57
    label "przesadno&#347;&#263;"
  ]
  node [
    id 58
    label "przestrzec"
  ]
  node [
    id 59
    label "poinformowa&#263;"
  ]
  node [
    id 60
    label "og&#322;osi&#263;"
  ]
  node [
    id 61
    label "spowodowa&#263;"
  ]
  node [
    id 62
    label "sign"
  ]
  node [
    id 63
    label "announce"
  ]
  node [
    id 64
    label "inform"
  ]
  node [
    id 65
    label "zakomunikowa&#263;"
  ]
  node [
    id 66
    label "publish"
  ]
  node [
    id 67
    label "poda&#263;"
  ]
  node [
    id 68
    label "opublikowa&#263;"
  ]
  node [
    id 69
    label "obwo&#322;a&#263;"
  ]
  node [
    id 70
    label "declare"
  ]
  node [
    id 71
    label "communicate"
  ]
  node [
    id 72
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 73
    label "act"
  ]
  node [
    id 74
    label "uprzedzi&#263;"
  ]
  node [
    id 75
    label "alarm"
  ]
  node [
    id 76
    label "weekend"
  ]
  node [
    id 77
    label "Wielka_Sobota"
  ]
  node [
    id 78
    label "dzie&#324;_powszedni"
  ]
  node [
    id 79
    label "niedziela"
  ]
  node [
    id 80
    label "tydzie&#324;"
  ]
  node [
    id 81
    label "nast&#281;pnie"
  ]
  node [
    id 82
    label "inny"
  ]
  node [
    id 83
    label "nastopny"
  ]
  node [
    id 84
    label "kolejno"
  ]
  node [
    id 85
    label "kt&#243;ry&#347;"
  ]
  node [
    id 86
    label "osobno"
  ]
  node [
    id 87
    label "r&#243;&#380;ny"
  ]
  node [
    id 88
    label "inszy"
  ]
  node [
    id 89
    label "inaczej"
  ]
  node [
    id 90
    label "czerwona_kartka"
  ]
  node [
    id 91
    label "protestacja"
  ]
  node [
    id 92
    label "reakcja"
  ]
  node [
    id 93
    label "react"
  ]
  node [
    id 94
    label "zachowanie"
  ]
  node [
    id 95
    label "reaction"
  ]
  node [
    id 96
    label "organizm"
  ]
  node [
    id 97
    label "rozmowa"
  ]
  node [
    id 98
    label "response"
  ]
  node [
    id 99
    label "rezultat"
  ]
  node [
    id 100
    label "respondent"
  ]
  node [
    id 101
    label "sprzeciw"
  ]
  node [
    id 102
    label "typ_mongoloidalny"
  ]
  node [
    id 103
    label "kolorowy"
  ]
  node [
    id 104
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 105
    label "ciep&#322;y"
  ]
  node [
    id 106
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 107
    label "jasny"
  ]
  node [
    id 108
    label "zabarwienie_si&#281;"
  ]
  node [
    id 109
    label "ciekawy"
  ]
  node [
    id 110
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 111
    label "cz&#322;owiek"
  ]
  node [
    id 112
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 113
    label "weso&#322;y"
  ]
  node [
    id 114
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 115
    label "barwienie"
  ]
  node [
    id 116
    label "kolorowo"
  ]
  node [
    id 117
    label "barwnie"
  ]
  node [
    id 118
    label "kolorowanie"
  ]
  node [
    id 119
    label "barwisty"
  ]
  node [
    id 120
    label "przyjemny"
  ]
  node [
    id 121
    label "barwienie_si&#281;"
  ]
  node [
    id 122
    label "pi&#281;kny"
  ]
  node [
    id 123
    label "ubarwienie"
  ]
  node [
    id 124
    label "mi&#322;y"
  ]
  node [
    id 125
    label "ocieplanie_si&#281;"
  ]
  node [
    id 126
    label "ocieplanie"
  ]
  node [
    id 127
    label "grzanie"
  ]
  node [
    id 128
    label "ocieplenie_si&#281;"
  ]
  node [
    id 129
    label "zagrzanie"
  ]
  node [
    id 130
    label "ocieplenie"
  ]
  node [
    id 131
    label "korzystny"
  ]
  node [
    id 132
    label "ciep&#322;o"
  ]
  node [
    id 133
    label "dobry"
  ]
  node [
    id 134
    label "o&#347;wietlenie"
  ]
  node [
    id 135
    label "szczery"
  ]
  node [
    id 136
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 137
    label "jasno"
  ]
  node [
    id 138
    label "o&#347;wietlanie"
  ]
  node [
    id 139
    label "przytomny"
  ]
  node [
    id 140
    label "zrozumia&#322;y"
  ]
  node [
    id 141
    label "niezm&#261;cony"
  ]
  node [
    id 142
    label "bia&#322;y"
  ]
  node [
    id 143
    label "klarowny"
  ]
  node [
    id 144
    label "jednoznaczny"
  ]
  node [
    id 145
    label "pogodny"
  ]
  node [
    id 146
    label "odcinanie_si&#281;"
  ]
  node [
    id 147
    label "g&#243;ra"
  ]
  node [
    id 148
    label "westa"
  ]
  node [
    id 149
    label "przelezienie"
  ]
  node [
    id 150
    label "&#347;piew"
  ]
  node [
    id 151
    label "Synaj"
  ]
  node [
    id 152
    label "Kreml"
  ]
  node [
    id 153
    label "d&#378;wi&#281;k"
  ]
  node [
    id 154
    label "kierunek"
  ]
  node [
    id 155
    label "wysoki"
  ]
  node [
    id 156
    label "element"
  ]
  node [
    id 157
    label "wzniesienie"
  ]
  node [
    id 158
    label "grupa"
  ]
  node [
    id 159
    label "pi&#281;tro"
  ]
  node [
    id 160
    label "Ropa"
  ]
  node [
    id 161
    label "kupa"
  ]
  node [
    id 162
    label "przele&#378;&#263;"
  ]
  node [
    id 163
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 164
    label "karczek"
  ]
  node [
    id 165
    label "rami&#261;czko"
  ]
  node [
    id 166
    label "Jaworze"
  ]
  node [
    id 167
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 168
    label "compass"
  ]
  node [
    id 169
    label "korzysta&#263;"
  ]
  node [
    id 170
    label "appreciation"
  ]
  node [
    id 171
    label "osi&#261;ga&#263;"
  ]
  node [
    id 172
    label "dociera&#263;"
  ]
  node [
    id 173
    label "get"
  ]
  node [
    id 174
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 175
    label "mierzy&#263;"
  ]
  node [
    id 176
    label "u&#380;ywa&#263;"
  ]
  node [
    id 177
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 178
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 179
    label "exsert"
  ]
  node [
    id 180
    label "distribute"
  ]
  node [
    id 181
    label "give"
  ]
  node [
    id 182
    label "bash"
  ]
  node [
    id 183
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 184
    label "doznawa&#263;"
  ]
  node [
    id 185
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 186
    label "take"
  ]
  node [
    id 187
    label "okre&#347;la&#263;"
  ]
  node [
    id 188
    label "widen"
  ]
  node [
    id 189
    label "develop"
  ]
  node [
    id 190
    label "perpetrate"
  ]
  node [
    id 191
    label "expand"
  ]
  node [
    id 192
    label "zdobywa&#263;_podst&#281;pem"
  ]
  node [
    id 193
    label "zmusza&#263;"
  ]
  node [
    id 194
    label "prostowa&#263;"
  ]
  node [
    id 195
    label "ocala&#263;"
  ]
  node [
    id 196
    label "wy&#322;udza&#263;"
  ]
  node [
    id 197
    label "przypomina&#263;"
  ]
  node [
    id 198
    label "&#347;piewa&#263;"
  ]
  node [
    id 199
    label "zabiera&#263;"
  ]
  node [
    id 200
    label "wydostawa&#263;"
  ]
  node [
    id 201
    label "dane"
  ]
  node [
    id 202
    label "przemieszcza&#263;"
  ]
  node [
    id 203
    label "wch&#322;ania&#263;"
  ]
  node [
    id 204
    label "rozci&#261;ga&#263;"
  ]
  node [
    id 205
    label "obrysowywa&#263;"
  ]
  node [
    id 206
    label "train"
  ]
  node [
    id 207
    label "zarabia&#263;"
  ]
  node [
    id 208
    label "nak&#322;ania&#263;"
  ]
  node [
    id 209
    label "pr&#281;dko&#347;&#263;"
  ]
  node [
    id 210
    label "use"
  ]
  node [
    id 211
    label "uzyskiwa&#263;"
  ]
  node [
    id 212
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 213
    label "silnik"
  ]
  node [
    id 214
    label "dopasowywa&#263;"
  ]
  node [
    id 215
    label "g&#322;adzi&#263;"
  ]
  node [
    id 216
    label "boost"
  ]
  node [
    id 217
    label "dorabia&#263;"
  ]
  node [
    id 218
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 219
    label "trze&#263;"
  ]
  node [
    id 220
    label "znajdowa&#263;"
  ]
  node [
    id 221
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 222
    label "mark"
  ]
  node [
    id 223
    label "poziom"
  ]
  node [
    id 224
    label "po&#322;o&#380;enie"
  ]
  node [
    id 225
    label "jako&#347;&#263;"
  ]
  node [
    id 226
    label "p&#322;aszczyzna"
  ]
  node [
    id 227
    label "punkt_widzenia"
  ]
  node [
    id 228
    label "wyk&#322;adnik"
  ]
  node [
    id 229
    label "faza"
  ]
  node [
    id 230
    label "szczebel"
  ]
  node [
    id 231
    label "budynek"
  ]
  node [
    id 232
    label "wysoko&#347;&#263;"
  ]
  node [
    id 233
    label "ranga"
  ]
  node [
    id 234
    label "Benjamin"
  ]
  node [
    id 235
    label "Cauchy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 234
    target 235
  ]
]
