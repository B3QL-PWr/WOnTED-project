graph [
  node [
    id 0
    label "geniusz"
    origin "text"
  ]
  node [
    id 1
    label "zbrodnia"
    origin "text"
  ]
  node [
    id 2
    label "zlodzieje"
    origin "text"
  ]
  node [
    id 3
    label "duch_opieku&#324;czy"
  ]
  node [
    id 4
    label "m&#243;zg"
  ]
  node [
    id 5
    label "talent"
  ]
  node [
    id 6
    label "nadcz&#322;owiek"
  ]
  node [
    id 7
    label "doskona&#322;o&#347;&#263;"
  ]
  node [
    id 8
    label "brylant"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "dyspozycja"
  ]
  node [
    id 11
    label "gigant"
  ]
  node [
    id 12
    label "faculty"
  ]
  node [
    id 13
    label "stygmat"
  ]
  node [
    id 14
    label "moneta"
  ]
  node [
    id 15
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 16
    label "znakomito&#347;&#263;"
  ]
  node [
    id 17
    label "wspania&#322;o&#347;&#263;"
  ]
  node [
    id 18
    label "wz&#243;r"
  ]
  node [
    id 19
    label "sko&#324;czono&#347;&#263;"
  ]
  node [
    id 20
    label "idea&#322;"
  ]
  node [
    id 21
    label "perfekcjonista"
  ]
  node [
    id 22
    label "substancja_szara"
  ]
  node [
    id 23
    label "wiedza"
  ]
  node [
    id 24
    label "encefalografia"
  ]
  node [
    id 25
    label "przedmurze"
  ]
  node [
    id 26
    label "bruzda"
  ]
  node [
    id 27
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 28
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 29
    label "most"
  ]
  node [
    id 30
    label "cecha"
  ]
  node [
    id 31
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 32
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 33
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 34
    label "podwzg&#243;rze"
  ]
  node [
    id 35
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 36
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 37
    label "wzg&#243;rze"
  ]
  node [
    id 38
    label "g&#322;owa"
  ]
  node [
    id 39
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 40
    label "noosfera"
  ]
  node [
    id 41
    label "elektroencefalogram"
  ]
  node [
    id 42
    label "przodom&#243;zgowie"
  ]
  node [
    id 43
    label "organ"
  ]
  node [
    id 44
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 45
    label "projektodawca"
  ]
  node [
    id 46
    label "przysadka"
  ]
  node [
    id 47
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 48
    label "zw&#243;j"
  ]
  node [
    id 49
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 50
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 51
    label "kora_m&#243;zgowa"
  ]
  node [
    id 52
    label "umys&#322;"
  ]
  node [
    id 53
    label "kresom&#243;zgowie"
  ]
  node [
    id 54
    label "poduszka"
  ]
  node [
    id 55
    label "nietzscheanizm"
  ]
  node [
    id 56
    label "dusza"
  ]
  node [
    id 57
    label "Supermen"
  ]
  node [
    id 58
    label "poj&#281;cie"
  ]
  node [
    id 59
    label "crime"
  ]
  node [
    id 60
    label "post&#281;pek"
  ]
  node [
    id 61
    label "przest&#281;pstwo"
  ]
  node [
    id 62
    label "brudny"
  ]
  node [
    id 63
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 64
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 65
    label "sprawstwo"
  ]
  node [
    id 66
    label "action"
  ]
  node [
    id 67
    label "czyn"
  ]
  node [
    id 68
    label "funkcja"
  ]
  node [
    id 69
    label "act"
  ]
  node [
    id 70
    label "rzecz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
]
