graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nie"
    origin "text"
  ]
  node [
    id 3
    label "p&#322;aka&#263;"
    origin "text"
  ]
  node [
    id 4
    label "powiada&#263;"
    origin "text"
  ]
  node [
    id 5
    label "judce"
    origin "text"
  ]
  node [
    id 6
    label "rycze&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ten"
    origin "text"
  ]
  node [
    id 8
    label "w&#243;&#322;"
    origin "text"
  ]
  node [
    id 9
    label "siostra"
    origin "text"
  ]
  node [
    id 10
    label "zbieg&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "hoffowie"
    origin "text"
  ]
  node [
    id 13
    label "czeladnik"
    origin "text"
  ]
  node [
    id 14
    label "terminator"
    origin "text"
  ]
  node [
    id 15
    label "daleko"
    origin "text"
  ]
  node [
    id 16
    label "kl&#261;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "skar&#380;y&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zabi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "znowu"
    origin "text"
  ]
  node [
    id 20
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wmawia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "sam"
    origin "text"
  ]
  node [
    id 23
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 24
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 25
    label "tema"
    origin "text"
  ]
  node [
    id 26
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 27
    label "zeszed&#322;"
    origin "text"
  ]
  node [
    id 28
    label "nieboszczka"
    origin "text"
  ]
  node [
    id 29
    label "pochowa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "nasz"
    origin "text"
  ]
  node [
    id 31
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 32
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 34
    label "belfer"
  ]
  node [
    id 35
    label "murza"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "ojciec"
  ]
  node [
    id 38
    label "samiec"
  ]
  node [
    id 39
    label "androlog"
  ]
  node [
    id 40
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 41
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 42
    label "efendi"
  ]
  node [
    id 43
    label "opiekun"
  ]
  node [
    id 44
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 45
    label "pa&#324;stwo"
  ]
  node [
    id 46
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 47
    label "bratek"
  ]
  node [
    id 48
    label "Mieszko_I"
  ]
  node [
    id 49
    label "Midas"
  ]
  node [
    id 50
    label "m&#261;&#380;"
  ]
  node [
    id 51
    label "bogaty"
  ]
  node [
    id 52
    label "popularyzator"
  ]
  node [
    id 53
    label "pracodawca"
  ]
  node [
    id 54
    label "kszta&#322;ciciel"
  ]
  node [
    id 55
    label "preceptor"
  ]
  node [
    id 56
    label "nabab"
  ]
  node [
    id 57
    label "pupil"
  ]
  node [
    id 58
    label "andropauza"
  ]
  node [
    id 59
    label "zwrot"
  ]
  node [
    id 60
    label "przyw&#243;dca"
  ]
  node [
    id 61
    label "doros&#322;y"
  ]
  node [
    id 62
    label "pedagog"
  ]
  node [
    id 63
    label "rz&#261;dzenie"
  ]
  node [
    id 64
    label "jegomo&#347;&#263;"
  ]
  node [
    id 65
    label "szkolnik"
  ]
  node [
    id 66
    label "ch&#322;opina"
  ]
  node [
    id 67
    label "w&#322;odarz"
  ]
  node [
    id 68
    label "profesor"
  ]
  node [
    id 69
    label "gra_w_karty"
  ]
  node [
    id 70
    label "w&#322;adza"
  ]
  node [
    id 71
    label "Fidel_Castro"
  ]
  node [
    id 72
    label "Anders"
  ]
  node [
    id 73
    label "Ko&#347;ciuszko"
  ]
  node [
    id 74
    label "Tito"
  ]
  node [
    id 75
    label "Miko&#322;ajczyk"
  ]
  node [
    id 76
    label "lider"
  ]
  node [
    id 77
    label "Mao"
  ]
  node [
    id 78
    label "Sabataj_Cwi"
  ]
  node [
    id 79
    label "p&#322;atnik"
  ]
  node [
    id 80
    label "zwierzchnik"
  ]
  node [
    id 81
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 82
    label "nadzorca"
  ]
  node [
    id 83
    label "funkcjonariusz"
  ]
  node [
    id 84
    label "podmiot"
  ]
  node [
    id 85
    label "wykupienie"
  ]
  node [
    id 86
    label "bycie_w_posiadaniu"
  ]
  node [
    id 87
    label "wykupywanie"
  ]
  node [
    id 88
    label "rozszerzyciel"
  ]
  node [
    id 89
    label "ludzko&#347;&#263;"
  ]
  node [
    id 90
    label "asymilowanie"
  ]
  node [
    id 91
    label "wapniak"
  ]
  node [
    id 92
    label "asymilowa&#263;"
  ]
  node [
    id 93
    label "os&#322;abia&#263;"
  ]
  node [
    id 94
    label "posta&#263;"
  ]
  node [
    id 95
    label "hominid"
  ]
  node [
    id 96
    label "podw&#322;adny"
  ]
  node [
    id 97
    label "os&#322;abianie"
  ]
  node [
    id 98
    label "g&#322;owa"
  ]
  node [
    id 99
    label "figura"
  ]
  node [
    id 100
    label "portrecista"
  ]
  node [
    id 101
    label "dwun&#243;g"
  ]
  node [
    id 102
    label "profanum"
  ]
  node [
    id 103
    label "mikrokosmos"
  ]
  node [
    id 104
    label "nasada"
  ]
  node [
    id 105
    label "duch"
  ]
  node [
    id 106
    label "antropochoria"
  ]
  node [
    id 107
    label "osoba"
  ]
  node [
    id 108
    label "wz&#243;r"
  ]
  node [
    id 109
    label "senior"
  ]
  node [
    id 110
    label "oddzia&#322;ywanie"
  ]
  node [
    id 111
    label "Adam"
  ]
  node [
    id 112
    label "homo_sapiens"
  ]
  node [
    id 113
    label "polifag"
  ]
  node [
    id 114
    label "wydoro&#347;lenie"
  ]
  node [
    id 115
    label "du&#380;y"
  ]
  node [
    id 116
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 117
    label "doro&#347;lenie"
  ]
  node [
    id 118
    label "&#378;ra&#322;y"
  ]
  node [
    id 119
    label "doro&#347;le"
  ]
  node [
    id 120
    label "dojrzale"
  ]
  node [
    id 121
    label "dojrza&#322;y"
  ]
  node [
    id 122
    label "m&#261;dry"
  ]
  node [
    id 123
    label "doletni"
  ]
  node [
    id 124
    label "punkt"
  ]
  node [
    id 125
    label "turn"
  ]
  node [
    id 126
    label "turning"
  ]
  node [
    id 127
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 128
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 129
    label "skr&#281;t"
  ]
  node [
    id 130
    label "obr&#243;t"
  ]
  node [
    id 131
    label "fraza_czasownikowa"
  ]
  node [
    id 132
    label "jednostka_leksykalna"
  ]
  node [
    id 133
    label "zmiana"
  ]
  node [
    id 134
    label "wyra&#380;enie"
  ]
  node [
    id 135
    label "starosta"
  ]
  node [
    id 136
    label "zarz&#261;dca"
  ]
  node [
    id 137
    label "w&#322;adca"
  ]
  node [
    id 138
    label "nauczyciel"
  ]
  node [
    id 139
    label "autor"
  ]
  node [
    id 140
    label "wyprawka"
  ]
  node [
    id 141
    label "mundurek"
  ]
  node [
    id 142
    label "szko&#322;a"
  ]
  node [
    id 143
    label "tarcza"
  ]
  node [
    id 144
    label "elew"
  ]
  node [
    id 145
    label "absolwent"
  ]
  node [
    id 146
    label "klasa"
  ]
  node [
    id 147
    label "stopie&#324;_naukowy"
  ]
  node [
    id 148
    label "nauczyciel_akademicki"
  ]
  node [
    id 149
    label "tytu&#322;"
  ]
  node [
    id 150
    label "profesura"
  ]
  node [
    id 151
    label "konsulent"
  ]
  node [
    id 152
    label "wirtuoz"
  ]
  node [
    id 153
    label "ekspert"
  ]
  node [
    id 154
    label "ochotnik"
  ]
  node [
    id 155
    label "pomocnik"
  ]
  node [
    id 156
    label "student"
  ]
  node [
    id 157
    label "nauczyciel_muzyki"
  ]
  node [
    id 158
    label "zakonnik"
  ]
  node [
    id 159
    label "urz&#281;dnik"
  ]
  node [
    id 160
    label "bogacz"
  ]
  node [
    id 161
    label "dostojnik"
  ]
  node [
    id 162
    label "mo&#347;&#263;"
  ]
  node [
    id 163
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 164
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 165
    label "kuwada"
  ]
  node [
    id 166
    label "tworzyciel"
  ]
  node [
    id 167
    label "rodzice"
  ]
  node [
    id 168
    label "&#347;w"
  ]
  node [
    id 169
    label "pomys&#322;odawca"
  ]
  node [
    id 170
    label "rodzic"
  ]
  node [
    id 171
    label "wykonawca"
  ]
  node [
    id 172
    label "ojczym"
  ]
  node [
    id 173
    label "przodek"
  ]
  node [
    id 174
    label "papa"
  ]
  node [
    id 175
    label "stary"
  ]
  node [
    id 176
    label "zwierz&#281;"
  ]
  node [
    id 177
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 178
    label "kochanek"
  ]
  node [
    id 179
    label "fio&#322;ek"
  ]
  node [
    id 180
    label "facet"
  ]
  node [
    id 181
    label "brat"
  ]
  node [
    id 182
    label "ma&#322;&#380;onek"
  ]
  node [
    id 183
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 184
    label "m&#243;j"
  ]
  node [
    id 185
    label "ch&#322;op"
  ]
  node [
    id 186
    label "pan_m&#322;ody"
  ]
  node [
    id 187
    label "&#347;lubny"
  ]
  node [
    id 188
    label "pan_domu"
  ]
  node [
    id 189
    label "pan_i_w&#322;adca"
  ]
  node [
    id 190
    label "Frygia"
  ]
  node [
    id 191
    label "sprawowanie"
  ]
  node [
    id 192
    label "dominion"
  ]
  node [
    id 193
    label "dominowanie"
  ]
  node [
    id 194
    label "reign"
  ]
  node [
    id 195
    label "rule"
  ]
  node [
    id 196
    label "zwierz&#281;_domowe"
  ]
  node [
    id 197
    label "J&#281;drzejewicz"
  ]
  node [
    id 198
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 199
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 200
    label "John_Dewey"
  ]
  node [
    id 201
    label "specjalista"
  ]
  node [
    id 202
    label "&#380;ycie"
  ]
  node [
    id 203
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 204
    label "Turek"
  ]
  node [
    id 205
    label "effendi"
  ]
  node [
    id 206
    label "obfituj&#261;cy"
  ]
  node [
    id 207
    label "r&#243;&#380;norodny"
  ]
  node [
    id 208
    label "spania&#322;y"
  ]
  node [
    id 209
    label "obficie"
  ]
  node [
    id 210
    label "sytuowany"
  ]
  node [
    id 211
    label "och&#281;do&#380;ny"
  ]
  node [
    id 212
    label "forsiasty"
  ]
  node [
    id 213
    label "zapa&#347;ny"
  ]
  node [
    id 214
    label "bogato"
  ]
  node [
    id 215
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 216
    label "Katar"
  ]
  node [
    id 217
    label "Libia"
  ]
  node [
    id 218
    label "Gwatemala"
  ]
  node [
    id 219
    label "Ekwador"
  ]
  node [
    id 220
    label "Afganistan"
  ]
  node [
    id 221
    label "Tad&#380;ykistan"
  ]
  node [
    id 222
    label "Bhutan"
  ]
  node [
    id 223
    label "Argentyna"
  ]
  node [
    id 224
    label "D&#380;ibuti"
  ]
  node [
    id 225
    label "Wenezuela"
  ]
  node [
    id 226
    label "Gabon"
  ]
  node [
    id 227
    label "Ukraina"
  ]
  node [
    id 228
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 229
    label "Rwanda"
  ]
  node [
    id 230
    label "Liechtenstein"
  ]
  node [
    id 231
    label "organizacja"
  ]
  node [
    id 232
    label "Sri_Lanka"
  ]
  node [
    id 233
    label "Madagaskar"
  ]
  node [
    id 234
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 235
    label "Kongo"
  ]
  node [
    id 236
    label "Tonga"
  ]
  node [
    id 237
    label "Bangladesz"
  ]
  node [
    id 238
    label "Kanada"
  ]
  node [
    id 239
    label "Wehrlen"
  ]
  node [
    id 240
    label "Algieria"
  ]
  node [
    id 241
    label "Uganda"
  ]
  node [
    id 242
    label "Surinam"
  ]
  node [
    id 243
    label "Sahara_Zachodnia"
  ]
  node [
    id 244
    label "Chile"
  ]
  node [
    id 245
    label "W&#281;gry"
  ]
  node [
    id 246
    label "Birma"
  ]
  node [
    id 247
    label "Kazachstan"
  ]
  node [
    id 248
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 249
    label "Armenia"
  ]
  node [
    id 250
    label "Tuwalu"
  ]
  node [
    id 251
    label "Timor_Wschodni"
  ]
  node [
    id 252
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 253
    label "Izrael"
  ]
  node [
    id 254
    label "Estonia"
  ]
  node [
    id 255
    label "Komory"
  ]
  node [
    id 256
    label "Kamerun"
  ]
  node [
    id 257
    label "Haiti"
  ]
  node [
    id 258
    label "Belize"
  ]
  node [
    id 259
    label "Sierra_Leone"
  ]
  node [
    id 260
    label "Luksemburg"
  ]
  node [
    id 261
    label "USA"
  ]
  node [
    id 262
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 263
    label "Barbados"
  ]
  node [
    id 264
    label "San_Marino"
  ]
  node [
    id 265
    label "Bu&#322;garia"
  ]
  node [
    id 266
    label "Indonezja"
  ]
  node [
    id 267
    label "Wietnam"
  ]
  node [
    id 268
    label "Malawi"
  ]
  node [
    id 269
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 270
    label "Francja"
  ]
  node [
    id 271
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 272
    label "partia"
  ]
  node [
    id 273
    label "Zambia"
  ]
  node [
    id 274
    label "Angola"
  ]
  node [
    id 275
    label "Grenada"
  ]
  node [
    id 276
    label "Nepal"
  ]
  node [
    id 277
    label "Panama"
  ]
  node [
    id 278
    label "Rumunia"
  ]
  node [
    id 279
    label "Czarnog&#243;ra"
  ]
  node [
    id 280
    label "Malediwy"
  ]
  node [
    id 281
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 282
    label "S&#322;owacja"
  ]
  node [
    id 283
    label "para"
  ]
  node [
    id 284
    label "Egipt"
  ]
  node [
    id 285
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 286
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 287
    label "Mozambik"
  ]
  node [
    id 288
    label "Kolumbia"
  ]
  node [
    id 289
    label "Laos"
  ]
  node [
    id 290
    label "Burundi"
  ]
  node [
    id 291
    label "Suazi"
  ]
  node [
    id 292
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 293
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 294
    label "Czechy"
  ]
  node [
    id 295
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 296
    label "Wyspy_Marshalla"
  ]
  node [
    id 297
    label "Dominika"
  ]
  node [
    id 298
    label "Trynidad_i_Tobago"
  ]
  node [
    id 299
    label "Syria"
  ]
  node [
    id 300
    label "Palau"
  ]
  node [
    id 301
    label "Gwinea_Bissau"
  ]
  node [
    id 302
    label "Liberia"
  ]
  node [
    id 303
    label "Jamajka"
  ]
  node [
    id 304
    label "Zimbabwe"
  ]
  node [
    id 305
    label "Polska"
  ]
  node [
    id 306
    label "Dominikana"
  ]
  node [
    id 307
    label "Senegal"
  ]
  node [
    id 308
    label "Togo"
  ]
  node [
    id 309
    label "Gujana"
  ]
  node [
    id 310
    label "Gruzja"
  ]
  node [
    id 311
    label "Albania"
  ]
  node [
    id 312
    label "Zair"
  ]
  node [
    id 313
    label "Meksyk"
  ]
  node [
    id 314
    label "Macedonia"
  ]
  node [
    id 315
    label "Chorwacja"
  ]
  node [
    id 316
    label "Kambod&#380;a"
  ]
  node [
    id 317
    label "Monako"
  ]
  node [
    id 318
    label "Mauritius"
  ]
  node [
    id 319
    label "Gwinea"
  ]
  node [
    id 320
    label "Mali"
  ]
  node [
    id 321
    label "Nigeria"
  ]
  node [
    id 322
    label "Kostaryka"
  ]
  node [
    id 323
    label "Hanower"
  ]
  node [
    id 324
    label "Paragwaj"
  ]
  node [
    id 325
    label "W&#322;ochy"
  ]
  node [
    id 326
    label "Seszele"
  ]
  node [
    id 327
    label "Wyspy_Salomona"
  ]
  node [
    id 328
    label "Hiszpania"
  ]
  node [
    id 329
    label "Boliwia"
  ]
  node [
    id 330
    label "Kirgistan"
  ]
  node [
    id 331
    label "Irlandia"
  ]
  node [
    id 332
    label "Czad"
  ]
  node [
    id 333
    label "Irak"
  ]
  node [
    id 334
    label "Lesoto"
  ]
  node [
    id 335
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 336
    label "Malta"
  ]
  node [
    id 337
    label "Andora"
  ]
  node [
    id 338
    label "Chiny"
  ]
  node [
    id 339
    label "Filipiny"
  ]
  node [
    id 340
    label "Antarktis"
  ]
  node [
    id 341
    label "Niemcy"
  ]
  node [
    id 342
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 343
    label "Pakistan"
  ]
  node [
    id 344
    label "terytorium"
  ]
  node [
    id 345
    label "Nikaragua"
  ]
  node [
    id 346
    label "Brazylia"
  ]
  node [
    id 347
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 348
    label "Maroko"
  ]
  node [
    id 349
    label "Portugalia"
  ]
  node [
    id 350
    label "Niger"
  ]
  node [
    id 351
    label "Kenia"
  ]
  node [
    id 352
    label "Botswana"
  ]
  node [
    id 353
    label "Fid&#380;i"
  ]
  node [
    id 354
    label "Tunezja"
  ]
  node [
    id 355
    label "Australia"
  ]
  node [
    id 356
    label "Tajlandia"
  ]
  node [
    id 357
    label "Burkina_Faso"
  ]
  node [
    id 358
    label "interior"
  ]
  node [
    id 359
    label "Tanzania"
  ]
  node [
    id 360
    label "Benin"
  ]
  node [
    id 361
    label "Indie"
  ]
  node [
    id 362
    label "&#321;otwa"
  ]
  node [
    id 363
    label "Kiribati"
  ]
  node [
    id 364
    label "Antigua_i_Barbuda"
  ]
  node [
    id 365
    label "Rodezja"
  ]
  node [
    id 366
    label "Cypr"
  ]
  node [
    id 367
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 368
    label "Peru"
  ]
  node [
    id 369
    label "Austria"
  ]
  node [
    id 370
    label "Urugwaj"
  ]
  node [
    id 371
    label "Jordania"
  ]
  node [
    id 372
    label "Grecja"
  ]
  node [
    id 373
    label "Azerbejd&#380;an"
  ]
  node [
    id 374
    label "Turcja"
  ]
  node [
    id 375
    label "Samoa"
  ]
  node [
    id 376
    label "Sudan"
  ]
  node [
    id 377
    label "Oman"
  ]
  node [
    id 378
    label "ziemia"
  ]
  node [
    id 379
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 380
    label "Uzbekistan"
  ]
  node [
    id 381
    label "Portoryko"
  ]
  node [
    id 382
    label "Honduras"
  ]
  node [
    id 383
    label "Mongolia"
  ]
  node [
    id 384
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 385
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 386
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 387
    label "Serbia"
  ]
  node [
    id 388
    label "Tajwan"
  ]
  node [
    id 389
    label "Wielka_Brytania"
  ]
  node [
    id 390
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 391
    label "Liban"
  ]
  node [
    id 392
    label "Japonia"
  ]
  node [
    id 393
    label "Ghana"
  ]
  node [
    id 394
    label "Belgia"
  ]
  node [
    id 395
    label "Bahrajn"
  ]
  node [
    id 396
    label "Mikronezja"
  ]
  node [
    id 397
    label "Etiopia"
  ]
  node [
    id 398
    label "Kuwejt"
  ]
  node [
    id 399
    label "grupa"
  ]
  node [
    id 400
    label "Bahamy"
  ]
  node [
    id 401
    label "Rosja"
  ]
  node [
    id 402
    label "Mo&#322;dawia"
  ]
  node [
    id 403
    label "Litwa"
  ]
  node [
    id 404
    label "S&#322;owenia"
  ]
  node [
    id 405
    label "Szwajcaria"
  ]
  node [
    id 406
    label "Erytrea"
  ]
  node [
    id 407
    label "Arabia_Saudyjska"
  ]
  node [
    id 408
    label "Kuba"
  ]
  node [
    id 409
    label "granica_pa&#324;stwa"
  ]
  node [
    id 410
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 411
    label "Malezja"
  ]
  node [
    id 412
    label "Korea"
  ]
  node [
    id 413
    label "Jemen"
  ]
  node [
    id 414
    label "Nowa_Zelandia"
  ]
  node [
    id 415
    label "Namibia"
  ]
  node [
    id 416
    label "Nauru"
  ]
  node [
    id 417
    label "holoarktyka"
  ]
  node [
    id 418
    label "Brunei"
  ]
  node [
    id 419
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 420
    label "Khitai"
  ]
  node [
    id 421
    label "Mauretania"
  ]
  node [
    id 422
    label "Iran"
  ]
  node [
    id 423
    label "Gambia"
  ]
  node [
    id 424
    label "Somalia"
  ]
  node [
    id 425
    label "Holandia"
  ]
  node [
    id 426
    label "Turkmenistan"
  ]
  node [
    id 427
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 428
    label "Salwador"
  ]
  node [
    id 429
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 430
    label "spoziera&#263;"
  ]
  node [
    id 431
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 432
    label "peek"
  ]
  node [
    id 433
    label "postrzec"
  ]
  node [
    id 434
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 435
    label "cognizance"
  ]
  node [
    id 436
    label "popatrze&#263;"
  ]
  node [
    id 437
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 438
    label "pojrze&#263;"
  ]
  node [
    id 439
    label "dostrzec"
  ]
  node [
    id 440
    label "spot"
  ]
  node [
    id 441
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 442
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 443
    label "go_steady"
  ]
  node [
    id 444
    label "zinterpretowa&#263;"
  ]
  node [
    id 445
    label "spotka&#263;"
  ]
  node [
    id 446
    label "obejrze&#263;"
  ]
  node [
    id 447
    label "znale&#378;&#263;"
  ]
  node [
    id 448
    label "see"
  ]
  node [
    id 449
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 450
    label "oceni&#263;"
  ]
  node [
    id 451
    label "zagra&#263;"
  ]
  node [
    id 452
    label "illustrate"
  ]
  node [
    id 453
    label "zanalizowa&#263;"
  ]
  node [
    id 454
    label "read"
  ]
  node [
    id 455
    label "think"
  ]
  node [
    id 456
    label "visualize"
  ]
  node [
    id 457
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 458
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 459
    label "sta&#263;_si&#281;"
  ]
  node [
    id 460
    label "zrobi&#263;"
  ]
  node [
    id 461
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 462
    label "notice"
  ]
  node [
    id 463
    label "respect"
  ]
  node [
    id 464
    label "insert"
  ]
  node [
    id 465
    label "pozna&#263;"
  ]
  node [
    id 466
    label "befall"
  ]
  node [
    id 467
    label "spowodowa&#263;"
  ]
  node [
    id 468
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 469
    label "pozyska&#263;"
  ]
  node [
    id 470
    label "devise"
  ]
  node [
    id 471
    label "dozna&#263;"
  ]
  node [
    id 472
    label "wykry&#263;"
  ]
  node [
    id 473
    label "odzyska&#263;"
  ]
  node [
    id 474
    label "znaj&#347;&#263;"
  ]
  node [
    id 475
    label "invent"
  ]
  node [
    id 476
    label "wymy&#347;li&#263;"
  ]
  node [
    id 477
    label "discover"
  ]
  node [
    id 478
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 479
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 480
    label "radio"
  ]
  node [
    id 481
    label "lampa"
  ]
  node [
    id 482
    label "film"
  ]
  node [
    id 483
    label "pomiar"
  ]
  node [
    id 484
    label "booklet"
  ]
  node [
    id 485
    label "transakcja"
  ]
  node [
    id 486
    label "ekspozycja"
  ]
  node [
    id 487
    label "reklama"
  ]
  node [
    id 488
    label "fotografia"
  ]
  node [
    id 489
    label "spojrze&#263;"
  ]
  node [
    id 490
    label "spogl&#261;da&#263;"
  ]
  node [
    id 491
    label "sprzeciw"
  ]
  node [
    id 492
    label "czerwona_kartka"
  ]
  node [
    id 493
    label "protestacja"
  ]
  node [
    id 494
    label "reakcja"
  ]
  node [
    id 495
    label "sting"
  ]
  node [
    id 496
    label "robi&#263;"
  ]
  node [
    id 497
    label "wy&#263;"
  ]
  node [
    id 498
    label "cudowa&#263;"
  ]
  node [
    id 499
    label "szkoda"
  ]
  node [
    id 500
    label "backfire"
  ]
  node [
    id 501
    label "wydziela&#263;"
  ]
  node [
    id 502
    label "sorrow"
  ]
  node [
    id 503
    label "narzeka&#263;"
  ]
  node [
    id 504
    label "snivel"
  ]
  node [
    id 505
    label "reagowa&#263;"
  ]
  node [
    id 506
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 507
    label "niezadowolenie"
  ]
  node [
    id 508
    label "m&#243;wi&#263;"
  ]
  node [
    id 509
    label "wyra&#380;a&#263;"
  ]
  node [
    id 510
    label "swarzy&#263;"
  ]
  node [
    id 511
    label "wyrzeka&#263;"
  ]
  node [
    id 512
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 513
    label "wilk"
  ]
  node [
    id 514
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 515
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 516
    label "pies"
  ]
  node [
    id 517
    label "yip"
  ]
  node [
    id 518
    label "pole"
  ]
  node [
    id 519
    label "zniszczenie"
  ]
  node [
    id 520
    label "ubytek"
  ]
  node [
    id 521
    label "&#380;erowisko"
  ]
  node [
    id 522
    label "szwank"
  ]
  node [
    id 523
    label "czu&#263;"
  ]
  node [
    id 524
    label "niepowodzenie"
  ]
  node [
    id 525
    label "commiseration"
  ]
  node [
    id 526
    label "organizowa&#263;"
  ]
  node [
    id 527
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 528
    label "czyni&#263;"
  ]
  node [
    id 529
    label "give"
  ]
  node [
    id 530
    label "stylizowa&#263;"
  ]
  node [
    id 531
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 532
    label "falowa&#263;"
  ]
  node [
    id 533
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 534
    label "peddle"
  ]
  node [
    id 535
    label "praca"
  ]
  node [
    id 536
    label "wydala&#263;"
  ]
  node [
    id 537
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 538
    label "tentegowa&#263;"
  ]
  node [
    id 539
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 540
    label "urz&#261;dza&#263;"
  ]
  node [
    id 541
    label "oszukiwa&#263;"
  ]
  node [
    id 542
    label "work"
  ]
  node [
    id 543
    label "ukazywa&#263;"
  ]
  node [
    id 544
    label "przerabia&#263;"
  ]
  node [
    id 545
    label "act"
  ]
  node [
    id 546
    label "post&#281;powa&#263;"
  ]
  node [
    id 547
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 548
    label "react"
  ]
  node [
    id 549
    label "answer"
  ]
  node [
    id 550
    label "odpowiada&#263;"
  ]
  node [
    id 551
    label "uczestniczy&#263;"
  ]
  node [
    id 552
    label "allocate"
  ]
  node [
    id 553
    label "wytwarza&#263;"
  ]
  node [
    id 554
    label "rozdziela&#263;"
  ]
  node [
    id 555
    label "exhaust"
  ]
  node [
    id 556
    label "przydziela&#263;"
  ]
  node [
    id 557
    label "wyznacza&#263;"
  ]
  node [
    id 558
    label "stagger"
  ]
  node [
    id 559
    label "oddziela&#263;"
  ]
  node [
    id 560
    label "wykrawa&#263;"
  ]
  node [
    id 561
    label "kombinowa&#263;"
  ]
  node [
    id 562
    label "ubolewa&#263;"
  ]
  node [
    id 563
    label "mawia&#263;"
  ]
  node [
    id 564
    label "informowa&#263;"
  ]
  node [
    id 565
    label "komunikowa&#263;"
  ]
  node [
    id 566
    label "inform"
  ]
  node [
    id 567
    label "gaworzy&#263;"
  ]
  node [
    id 568
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 569
    label "remark"
  ]
  node [
    id 570
    label "rozmawia&#263;"
  ]
  node [
    id 571
    label "umie&#263;"
  ]
  node [
    id 572
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 573
    label "dziama&#263;"
  ]
  node [
    id 574
    label "formu&#322;owa&#263;"
  ]
  node [
    id 575
    label "dysfonia"
  ]
  node [
    id 576
    label "express"
  ]
  node [
    id 577
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 578
    label "talk"
  ]
  node [
    id 579
    label "u&#380;ywa&#263;"
  ]
  node [
    id 580
    label "prawi&#263;"
  ]
  node [
    id 581
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 582
    label "tell"
  ]
  node [
    id 583
    label "chew_the_fat"
  ]
  node [
    id 584
    label "say"
  ]
  node [
    id 585
    label "j&#281;zyk"
  ]
  node [
    id 586
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 587
    label "wydobywa&#263;"
  ]
  node [
    id 588
    label "okre&#347;la&#263;"
  ]
  node [
    id 589
    label "zia&#263;"
  ]
  node [
    id 590
    label "hula&#263;"
  ]
  node [
    id 591
    label "wrzeszcze&#263;"
  ]
  node [
    id 592
    label "hucze&#263;"
  ]
  node [
    id 593
    label "mika&#263;"
  ]
  node [
    id 594
    label "bawl"
  ]
  node [
    id 595
    label "&#347;mia&#263;_si&#281;"
  ]
  node [
    id 596
    label "krzycze&#263;"
  ]
  node [
    id 597
    label "w&#322;&#243;czy&#263;_si&#281;"
  ]
  node [
    id 598
    label "chodzi&#263;"
  ]
  node [
    id 599
    label "biega&#263;"
  ]
  node [
    id 600
    label "funkcjonowa&#263;"
  ]
  node [
    id 601
    label "lampartowa&#263;_si&#281;"
  ]
  node [
    id 602
    label "bomblowa&#263;"
  ]
  node [
    id 603
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 604
    label "rant"
  ]
  node [
    id 605
    label "rozrabia&#263;"
  ]
  node [
    id 606
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 607
    label "wia&#263;"
  ]
  node [
    id 608
    label "carouse"
  ]
  node [
    id 609
    label "storm"
  ]
  node [
    id 610
    label "biec"
  ]
  node [
    id 611
    label "bawi&#263;_si&#281;"
  ]
  node [
    id 612
    label "brzmie&#263;"
  ]
  node [
    id 613
    label "panoszy&#263;_si&#281;"
  ]
  node [
    id 614
    label "folgowa&#263;"
  ]
  node [
    id 615
    label "czyha&#263;"
  ]
  node [
    id 616
    label "szale&#263;"
  ]
  node [
    id 617
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 618
    label "lumpowa&#263;"
  ]
  node [
    id 619
    label "lumpowa&#263;_si&#281;"
  ]
  node [
    id 620
    label "bucha&#263;"
  ]
  node [
    id 621
    label "breathe"
  ]
  node [
    id 622
    label "istnie&#263;"
  ]
  node [
    id 623
    label "gape"
  ]
  node [
    id 624
    label "oddycha&#263;"
  ]
  node [
    id 625
    label "blow"
  ]
  node [
    id 626
    label "emit"
  ]
  node [
    id 627
    label "panowa&#263;"
  ]
  node [
    id 628
    label "peal"
  ]
  node [
    id 629
    label "okre&#347;lony"
  ]
  node [
    id 630
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 631
    label "wiadomy"
  ]
  node [
    id 632
    label "pracownik"
  ]
  node [
    id 633
    label "czaban"
  ]
  node [
    id 634
    label "bydl&#281;"
  ]
  node [
    id 635
    label "kastrat"
  ]
  node [
    id 636
    label "bukranion"
  ]
  node [
    id 637
    label "salariat"
  ]
  node [
    id 638
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 639
    label "delegowanie"
  ]
  node [
    id 640
    label "pracu&#347;"
  ]
  node [
    id 641
    label "r&#281;ka"
  ]
  node [
    id 642
    label "delegowa&#263;"
  ]
  node [
    id 643
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 644
    label "byd&#322;o_domowe"
  ]
  node [
    id 645
    label "&#322;ajdak"
  ]
  node [
    id 646
    label "zwierz&#281;_gospodarskie"
  ]
  node [
    id 647
    label "prze&#380;uwacz"
  ]
  node [
    id 648
    label "rzezaniec"
  ]
  node [
    id 649
    label "kap&#322;on"
  ]
  node [
    id 650
    label "sopranista"
  ]
  node [
    id 651
    label "&#347;piewak"
  ]
  node [
    id 652
    label "g&#322;os_m&#281;ski"
  ]
  node [
    id 653
    label "baran"
  ]
  node [
    id 654
    label "krowiarz"
  ]
  node [
    id 655
    label "pasterz"
  ]
  node [
    id 656
    label "owczarz"
  ]
  node [
    id 657
    label "fryz"
  ]
  node [
    id 658
    label "relief"
  ]
  node [
    id 659
    label "czepek"
  ]
  node [
    id 660
    label "siostrzyca"
  ]
  node [
    id 661
    label "wyznawczyni"
  ]
  node [
    id 662
    label "rodze&#324;stwo"
  ]
  node [
    id 663
    label "krewna"
  ]
  node [
    id 664
    label "pingwin"
  ]
  node [
    id 665
    label "kornet"
  ]
  node [
    id 666
    label "anestetysta"
  ]
  node [
    id 667
    label "siora"
  ]
  node [
    id 668
    label "pigu&#322;a"
  ]
  node [
    id 669
    label "kula"
  ]
  node [
    id 670
    label "piel&#281;gniarka"
  ]
  node [
    id 671
    label "ptak_wodny"
  ]
  node [
    id 672
    label "nielot"
  ]
  node [
    id 673
    label "zakonnica"
  ]
  node [
    id 674
    label "pingwiny"
  ]
  node [
    id 675
    label "znieczulenie"
  ]
  node [
    id 676
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 677
    label "mszak"
  ]
  node [
    id 678
    label "k&#261;piel"
  ]
  node [
    id 679
    label "owodnia"
  ]
  node [
    id 680
    label "sporofit"
  ]
  node [
    id 681
    label "element_anatomiczny"
  ]
  node [
    id 682
    label "czapeczka"
  ]
  node [
    id 683
    label "sprzedawczyni"
  ]
  node [
    id 684
    label "kelnerka"
  ]
  node [
    id 685
    label "fonta&#378;"
  ]
  node [
    id 686
    label "czepiec"
  ]
  node [
    id 687
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 688
    label "cap"
  ]
  node [
    id 689
    label "kobieta"
  ]
  node [
    id 690
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 691
    label "krewni"
  ]
  node [
    id 692
    label "rzemie&#347;lnik"
  ]
  node [
    id 693
    label "ucze&#324;"
  ]
  node [
    id 694
    label "rzemie&#347;lniczek"
  ]
  node [
    id 695
    label "remiecha"
  ]
  node [
    id 696
    label "kontynuator"
  ]
  node [
    id 697
    label "zwolennik"
  ]
  node [
    id 698
    label "praktykant"
  ]
  node [
    id 699
    label "nisko"
  ]
  node [
    id 700
    label "znacznie"
  ]
  node [
    id 701
    label "het"
  ]
  node [
    id 702
    label "dawno"
  ]
  node [
    id 703
    label "daleki"
  ]
  node [
    id 704
    label "g&#322;&#281;boko"
  ]
  node [
    id 705
    label "nieobecnie"
  ]
  node [
    id 706
    label "wysoko"
  ]
  node [
    id 707
    label "du&#380;o"
  ]
  node [
    id 708
    label "dawny"
  ]
  node [
    id 709
    label "ogl&#281;dny"
  ]
  node [
    id 710
    label "d&#322;ugi"
  ]
  node [
    id 711
    label "odleg&#322;y"
  ]
  node [
    id 712
    label "zwi&#261;zany"
  ]
  node [
    id 713
    label "r&#243;&#380;ny"
  ]
  node [
    id 714
    label "s&#322;aby"
  ]
  node [
    id 715
    label "odlegle"
  ]
  node [
    id 716
    label "oddalony"
  ]
  node [
    id 717
    label "g&#322;&#281;boki"
  ]
  node [
    id 718
    label "obcy"
  ]
  node [
    id 719
    label "nieobecny"
  ]
  node [
    id 720
    label "przysz&#322;y"
  ]
  node [
    id 721
    label "niepo&#347;lednio"
  ]
  node [
    id 722
    label "wysoki"
  ]
  node [
    id 723
    label "g&#243;rno"
  ]
  node [
    id 724
    label "chwalebnie"
  ]
  node [
    id 725
    label "wznio&#347;le"
  ]
  node [
    id 726
    label "szczytny"
  ]
  node [
    id 727
    label "d&#322;ugotrwale"
  ]
  node [
    id 728
    label "wcze&#347;niej"
  ]
  node [
    id 729
    label "ongi&#347;"
  ]
  node [
    id 730
    label "dawnie"
  ]
  node [
    id 731
    label "zamy&#347;lony"
  ]
  node [
    id 732
    label "uni&#380;enie"
  ]
  node [
    id 733
    label "pospolicie"
  ]
  node [
    id 734
    label "blisko"
  ]
  node [
    id 735
    label "wstydliwie"
  ]
  node [
    id 736
    label "ma&#322;o"
  ]
  node [
    id 737
    label "vilely"
  ]
  node [
    id 738
    label "despicably"
  ]
  node [
    id 739
    label "niski"
  ]
  node [
    id 740
    label "po&#347;lednio"
  ]
  node [
    id 741
    label "ma&#322;y"
  ]
  node [
    id 742
    label "mocno"
  ]
  node [
    id 743
    label "gruntownie"
  ]
  node [
    id 744
    label "szczerze"
  ]
  node [
    id 745
    label "silnie"
  ]
  node [
    id 746
    label "intensywnie"
  ]
  node [
    id 747
    label "wiela"
  ]
  node [
    id 748
    label "bardzo"
  ]
  node [
    id 749
    label "cz&#281;sto"
  ]
  node [
    id 750
    label "zauwa&#380;alnie"
  ]
  node [
    id 751
    label "znaczny"
  ]
  node [
    id 752
    label "mistreat"
  ]
  node [
    id 753
    label "curse"
  ]
  node [
    id 754
    label "sobaczy&#263;"
  ]
  node [
    id 755
    label "blu&#378;ni&#263;"
  ]
  node [
    id 756
    label "oskar&#380;a&#263;"
  ]
  node [
    id 757
    label "prosecute"
  ]
  node [
    id 758
    label "twierdzi&#263;"
  ]
  node [
    id 759
    label "obra&#380;a&#263;"
  ]
  node [
    id 760
    label "spill_the_beans"
  ]
  node [
    id 761
    label "chatter"
  ]
  node [
    id 762
    label "donosi&#263;"
  ]
  node [
    id 763
    label "powodowa&#263;"
  ]
  node [
    id 764
    label "przeby&#263;"
  ]
  node [
    id 765
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 766
    label "zanosi&#263;"
  ]
  node [
    id 767
    label "zu&#380;y&#263;"
  ]
  node [
    id 768
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 769
    label "get"
  ]
  node [
    id 770
    label "introduce"
  ]
  node [
    id 771
    label "render"
  ]
  node [
    id 772
    label "ci&#261;&#380;a"
  ]
  node [
    id 773
    label "mie&#263;_miejsce"
  ]
  node [
    id 774
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 775
    label "motywowa&#263;"
  ]
  node [
    id 776
    label "zadzwoni&#263;"
  ]
  node [
    id 777
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 778
    label "skarci&#263;"
  ]
  node [
    id 779
    label "skrzywdzi&#263;"
  ]
  node [
    id 780
    label "os&#322;oni&#263;"
  ]
  node [
    id 781
    label "przybi&#263;"
  ]
  node [
    id 782
    label "rozbroi&#263;"
  ]
  node [
    id 783
    label "uderzy&#263;"
  ]
  node [
    id 784
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 785
    label "skrzywi&#263;"
  ]
  node [
    id 786
    label "dispatch"
  ]
  node [
    id 787
    label "zmordowa&#263;"
  ]
  node [
    id 788
    label "zakry&#263;"
  ]
  node [
    id 789
    label "zbi&#263;"
  ]
  node [
    id 790
    label "zapulsowa&#263;"
  ]
  node [
    id 791
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 792
    label "break"
  ]
  node [
    id 793
    label "zastrzeli&#263;"
  ]
  node [
    id 794
    label "u&#347;mierci&#263;"
  ]
  node [
    id 795
    label "zwalczy&#263;"
  ]
  node [
    id 796
    label "pomacha&#263;"
  ]
  node [
    id 797
    label "kill"
  ]
  node [
    id 798
    label "zako&#324;czy&#263;"
  ]
  node [
    id 799
    label "zniszczy&#263;"
  ]
  node [
    id 800
    label "ukara&#263;"
  ]
  node [
    id 801
    label "condemn"
  ]
  node [
    id 802
    label "upomnie&#263;"
  ]
  node [
    id 803
    label "wybuzowa&#263;"
  ]
  node [
    id 804
    label "urazi&#263;"
  ]
  node [
    id 805
    label "strike"
  ]
  node [
    id 806
    label "wystartowa&#263;"
  ]
  node [
    id 807
    label "przywali&#263;"
  ]
  node [
    id 808
    label "dupn&#261;&#263;"
  ]
  node [
    id 809
    label "skrytykowa&#263;"
  ]
  node [
    id 810
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 811
    label "nast&#261;pi&#263;"
  ]
  node [
    id 812
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 813
    label "sztachn&#261;&#263;"
  ]
  node [
    id 814
    label "rap"
  ]
  node [
    id 815
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 816
    label "crush"
  ]
  node [
    id 817
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 818
    label "postara&#263;_si&#281;"
  ]
  node [
    id 819
    label "fall"
  ]
  node [
    id 820
    label "hopn&#261;&#263;"
  ]
  node [
    id 821
    label "zada&#263;"
  ]
  node [
    id 822
    label "uda&#263;_si&#281;"
  ]
  node [
    id 823
    label "dotkn&#261;&#263;"
  ]
  node [
    id 824
    label "anoint"
  ]
  node [
    id 825
    label "transgress"
  ]
  node [
    id 826
    label "chop"
  ]
  node [
    id 827
    label "jebn&#261;&#263;"
  ]
  node [
    id 828
    label "lumber"
  ]
  node [
    id 829
    label "zdeformowa&#263;"
  ]
  node [
    id 830
    label "flex"
  ]
  node [
    id 831
    label "statek"
  ]
  node [
    id 832
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 833
    label "get_through"
  ]
  node [
    id 834
    label "zatwierdzi&#263;"
  ]
  node [
    id 835
    label "forge"
  ]
  node [
    id 836
    label "dobi&#263;"
  ]
  node [
    id 837
    label "ubi&#263;"
  ]
  node [
    id 838
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 839
    label "wbi&#263;"
  ]
  node [
    id 840
    label "ogarn&#261;&#263;"
  ]
  node [
    id 841
    label "przygnie&#347;&#263;"
  ]
  node [
    id 842
    label "przygn&#281;bi&#263;"
  ]
  node [
    id 843
    label "po&#347;wiadczy&#263;"
  ]
  node [
    id 844
    label "zastuka&#263;"
  ]
  node [
    id 845
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 846
    label "przyby&#263;"
  ]
  node [
    id 847
    label "uwi&#261;za&#263;"
  ]
  node [
    id 848
    label "zdecydowa&#263;"
  ]
  node [
    id 849
    label "przymocowa&#263;"
  ]
  node [
    id 850
    label "unieruchomi&#263;"
  ]
  node [
    id 851
    label "przystawi&#263;"
  ]
  node [
    id 852
    label "pod&#378;wiga&#263;"
  ]
  node [
    id 853
    label "ruszy&#263;"
  ]
  node [
    id 854
    label "wave"
  ]
  node [
    id 855
    label "znu&#380;y&#263;"
  ]
  node [
    id 856
    label "zamordowa&#263;"
  ]
  node [
    id 857
    label "os&#322;abi&#263;"
  ]
  node [
    id 858
    label "cause"
  ]
  node [
    id 859
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 860
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 861
    label "wytworzy&#263;"
  ]
  node [
    id 862
    label "dress"
  ]
  node [
    id 863
    label "zaora&#263;"
  ]
  node [
    id 864
    label "try"
  ]
  node [
    id 865
    label "zm&#281;czy&#263;"
  ]
  node [
    id 866
    label "torment"
  ]
  node [
    id 867
    label "zaszkodzi&#263;"
  ]
  node [
    id 868
    label "niesprawiedliwy"
  ]
  node [
    id 869
    label "ukrzywdzi&#263;"
  ]
  node [
    id 870
    label "hurt"
  ]
  node [
    id 871
    label "wrong"
  ]
  node [
    id 872
    label "poinformowa&#263;"
  ]
  node [
    id 873
    label "nieprawda"
  ]
  node [
    id 874
    label "pokona&#263;"
  ]
  node [
    id 875
    label "overwhelm"
  ]
  node [
    id 876
    label "wygra&#263;"
  ]
  node [
    id 877
    label "kondycja_fizyczna"
  ]
  node [
    id 878
    label "spoil"
  ]
  node [
    id 879
    label "zdrowie"
  ]
  node [
    id 880
    label "consume"
  ]
  node [
    id 881
    label "pamper"
  ]
  node [
    id 882
    label "call"
  ]
  node [
    id 883
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 884
    label "jingle"
  ]
  node [
    id 885
    label "dzwonek"
  ]
  node [
    id 886
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 887
    label "sound"
  ]
  node [
    id 888
    label "zadrynda&#263;"
  ]
  node [
    id 889
    label "zabrzmie&#263;"
  ]
  node [
    id 890
    label "telefon"
  ]
  node [
    id 891
    label "nacisn&#261;&#263;"
  ]
  node [
    id 892
    label "cover"
  ]
  node [
    id 893
    label "ukry&#263;"
  ]
  node [
    id 894
    label "report"
  ]
  node [
    id 895
    label "zas&#322;oni&#263;"
  ]
  node [
    id 896
    label "zamkn&#261;&#263;"
  ]
  node [
    id 897
    label "ochroni&#263;"
  ]
  node [
    id 898
    label "obroni&#263;"
  ]
  node [
    id 899
    label "odgrodzi&#263;"
  ]
  node [
    id 900
    label "brood"
  ]
  node [
    id 901
    label "zabezpieczy&#263;"
  ]
  node [
    id 902
    label "guard"
  ]
  node [
    id 903
    label "shroud"
  ]
  node [
    id 904
    label "dispose"
  ]
  node [
    id 905
    label "zrezygnowa&#263;"
  ]
  node [
    id 906
    label "communicate"
  ]
  node [
    id 907
    label "przesta&#263;"
  ]
  node [
    id 908
    label "shoot"
  ]
  node [
    id 909
    label "stukn&#261;&#263;"
  ]
  node [
    id 910
    label "rozwali&#263;"
  ]
  node [
    id 911
    label "zaskoczy&#263;"
  ]
  node [
    id 912
    label "zabra&#263;"
  ]
  node [
    id 913
    label "roz&#322;adowa&#263;"
  ]
  node [
    id 914
    label "rozebra&#263;"
  ]
  node [
    id 915
    label "discharge"
  ]
  node [
    id 916
    label "bro&#324;"
  ]
  node [
    id 917
    label "usun&#261;&#263;"
  ]
  node [
    id 918
    label "nabi&#263;"
  ]
  node [
    id 919
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 920
    label "wy&#322;oi&#263;"
  ]
  node [
    id 921
    label "wyrobi&#263;"
  ]
  node [
    id 922
    label "obni&#380;y&#263;"
  ]
  node [
    id 923
    label "uszkodzi&#263;"
  ]
  node [
    id 924
    label "&#347;cie&#347;ni&#263;"
  ]
  node [
    id 925
    label "str&#261;ci&#263;"
  ]
  node [
    id 926
    label "zebra&#263;"
  ]
  node [
    id 927
    label "obali&#263;"
  ]
  node [
    id 928
    label "zgromadzi&#263;"
  ]
  node [
    id 929
    label "beat"
  ]
  node [
    id 930
    label "pobi&#263;"
  ]
  node [
    id 931
    label "sku&#263;"
  ]
  node [
    id 932
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 933
    label "rozbi&#263;"
  ]
  node [
    id 934
    label "podci&#261;&#263;"
  ]
  node [
    id 935
    label "write_out"
  ]
  node [
    id 936
    label "hiphopowiec"
  ]
  node [
    id 937
    label "skejt"
  ]
  node [
    id 938
    label "taniec"
  ]
  node [
    id 939
    label "post&#261;pi&#263;"
  ]
  node [
    id 940
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 941
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 942
    label "odj&#261;&#263;"
  ]
  node [
    id 943
    label "begin"
  ]
  node [
    id 944
    label "do"
  ]
  node [
    id 945
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 946
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 947
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 948
    label "zorganizowa&#263;"
  ]
  node [
    id 949
    label "appoint"
  ]
  node [
    id 950
    label "wystylizowa&#263;"
  ]
  node [
    id 951
    label "przerobi&#263;"
  ]
  node [
    id 952
    label "nabra&#263;"
  ]
  node [
    id 953
    label "make"
  ]
  node [
    id 954
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 955
    label "wydali&#263;"
  ]
  node [
    id 956
    label "withdraw"
  ]
  node [
    id 957
    label "oddzieli&#263;"
  ]
  node [
    id 958
    label "policzy&#263;"
  ]
  node [
    id 959
    label "reduce"
  ]
  node [
    id 960
    label "oddali&#263;"
  ]
  node [
    id 961
    label "separate"
  ]
  node [
    id 962
    label "advance"
  ]
  node [
    id 963
    label "ut"
  ]
  node [
    id 964
    label "d&#378;wi&#281;k"
  ]
  node [
    id 965
    label "C"
  ]
  node [
    id 966
    label "his"
  ]
  node [
    id 967
    label "przekonywa&#263;"
  ]
  node [
    id 968
    label "nak&#322;ania&#263;"
  ]
  node [
    id 969
    label "argue"
  ]
  node [
    id 970
    label "sklep"
  ]
  node [
    id 971
    label "p&#243;&#322;ka"
  ]
  node [
    id 972
    label "firma"
  ]
  node [
    id 973
    label "stoisko"
  ]
  node [
    id 974
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 975
    label "sk&#322;ad"
  ]
  node [
    id 976
    label "obiekt_handlowy"
  ]
  node [
    id 977
    label "zaplecze"
  ]
  node [
    id 978
    label "witryna"
  ]
  node [
    id 979
    label "przekazywa&#263;"
  ]
  node [
    id 980
    label "zbiera&#263;"
  ]
  node [
    id 981
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 982
    label "przywraca&#263;"
  ]
  node [
    id 983
    label "dawa&#263;"
  ]
  node [
    id 984
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 985
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 986
    label "convey"
  ]
  node [
    id 987
    label "publicize"
  ]
  node [
    id 988
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 989
    label "uk&#322;ada&#263;"
  ]
  node [
    id 990
    label "opracowywa&#263;"
  ]
  node [
    id 991
    label "set"
  ]
  node [
    id 992
    label "oddawa&#263;"
  ]
  node [
    id 993
    label "train"
  ]
  node [
    id 994
    label "zmienia&#263;"
  ]
  node [
    id 995
    label "dzieli&#263;"
  ]
  node [
    id 996
    label "scala&#263;"
  ]
  node [
    id 997
    label "zestaw"
  ]
  node [
    id 998
    label "divide"
  ]
  node [
    id 999
    label "posiada&#263;"
  ]
  node [
    id 1000
    label "deal"
  ]
  node [
    id 1001
    label "liczy&#263;"
  ]
  node [
    id 1002
    label "assign"
  ]
  node [
    id 1003
    label "korzysta&#263;"
  ]
  node [
    id 1004
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 1005
    label "digest"
  ]
  node [
    id 1006
    label "share"
  ]
  node [
    id 1007
    label "iloraz"
  ]
  node [
    id 1008
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 1009
    label "rozdawa&#263;"
  ]
  node [
    id 1010
    label "sprawowa&#263;"
  ]
  node [
    id 1011
    label "dostarcza&#263;"
  ]
  node [
    id 1012
    label "sacrifice"
  ]
  node [
    id 1013
    label "odst&#281;powa&#263;"
  ]
  node [
    id 1014
    label "sprzedawa&#263;"
  ]
  node [
    id 1015
    label "reflect"
  ]
  node [
    id 1016
    label "surrender"
  ]
  node [
    id 1017
    label "deliver"
  ]
  node [
    id 1018
    label "umieszcza&#263;"
  ]
  node [
    id 1019
    label "blurt_out"
  ]
  node [
    id 1020
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 1021
    label "przedstawia&#263;"
  ]
  node [
    id 1022
    label "impart"
  ]
  node [
    id 1023
    label "przejmowa&#263;"
  ]
  node [
    id 1024
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 1025
    label "gromadzi&#263;"
  ]
  node [
    id 1026
    label "bra&#263;"
  ]
  node [
    id 1027
    label "pozyskiwa&#263;"
  ]
  node [
    id 1028
    label "poci&#261;ga&#263;"
  ]
  node [
    id 1029
    label "wzbiera&#263;"
  ]
  node [
    id 1030
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 1031
    label "meet"
  ]
  node [
    id 1032
    label "dostawa&#263;"
  ]
  node [
    id 1033
    label "consolidate"
  ]
  node [
    id 1034
    label "congregate"
  ]
  node [
    id 1035
    label "postpone"
  ]
  node [
    id 1036
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 1037
    label "znosi&#263;"
  ]
  node [
    id 1038
    label "chroni&#263;"
  ]
  node [
    id 1039
    label "darowywa&#263;"
  ]
  node [
    id 1040
    label "preserve"
  ]
  node [
    id 1041
    label "zachowywa&#263;"
  ]
  node [
    id 1042
    label "gospodarowa&#263;"
  ]
  node [
    id 1043
    label "uczy&#263;"
  ]
  node [
    id 1044
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 1045
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1046
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 1047
    label "przygotowywa&#263;"
  ]
  node [
    id 1048
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 1049
    label "tworzy&#263;"
  ]
  node [
    id 1050
    label "treser"
  ]
  node [
    id 1051
    label "raise"
  ]
  node [
    id 1052
    label "pozostawia&#263;"
  ]
  node [
    id 1053
    label "zaczyna&#263;"
  ]
  node [
    id 1054
    label "psu&#263;"
  ]
  node [
    id 1055
    label "wzbudza&#263;"
  ]
  node [
    id 1056
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1057
    label "go"
  ]
  node [
    id 1058
    label "inspirowa&#263;"
  ]
  node [
    id 1059
    label "wpaja&#263;"
  ]
  node [
    id 1060
    label "znak"
  ]
  node [
    id 1061
    label "seat"
  ]
  node [
    id 1062
    label "wygrywa&#263;"
  ]
  node [
    id 1063
    label "go&#347;ci&#263;"
  ]
  node [
    id 1064
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1065
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1066
    label "pour"
  ]
  node [
    id 1067
    label "elaborate"
  ]
  node [
    id 1068
    label "pokrywa&#263;"
  ]
  node [
    id 1069
    label "traci&#263;"
  ]
  node [
    id 1070
    label "alternate"
  ]
  node [
    id 1071
    label "change"
  ]
  node [
    id 1072
    label "reengineering"
  ]
  node [
    id 1073
    label "zast&#281;powa&#263;"
  ]
  node [
    id 1074
    label "sprawia&#263;"
  ]
  node [
    id 1075
    label "zyskiwa&#263;"
  ]
  node [
    id 1076
    label "przechodzi&#263;"
  ]
  node [
    id 1077
    label "consort"
  ]
  node [
    id 1078
    label "jednoczy&#263;"
  ]
  node [
    id 1079
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1080
    label "podawa&#263;"
  ]
  node [
    id 1081
    label "wp&#322;aca&#263;"
  ]
  node [
    id 1082
    label "sygna&#322;"
  ]
  node [
    id 1083
    label "doprowadza&#263;"
  ]
  node [
    id 1084
    label "&#322;adowa&#263;"
  ]
  node [
    id 1085
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1086
    label "przeznacza&#263;"
  ]
  node [
    id 1087
    label "traktowa&#263;"
  ]
  node [
    id 1088
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1089
    label "obiecywa&#263;"
  ]
  node [
    id 1090
    label "tender"
  ]
  node [
    id 1091
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 1092
    label "t&#322;uc"
  ]
  node [
    id 1093
    label "powierza&#263;"
  ]
  node [
    id 1094
    label "wpiernicza&#263;"
  ]
  node [
    id 1095
    label "exsert"
  ]
  node [
    id 1096
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 1097
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1098
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 1099
    label "p&#322;aci&#263;"
  ]
  node [
    id 1100
    label "hold_out"
  ]
  node [
    id 1101
    label "nalewa&#263;"
  ]
  node [
    id 1102
    label "zezwala&#263;"
  ]
  node [
    id 1103
    label "hold"
  ]
  node [
    id 1104
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1105
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 1106
    label "relate"
  ]
  node [
    id 1107
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 1108
    label "dopieprza&#263;"
  ]
  node [
    id 1109
    label "press"
  ]
  node [
    id 1110
    label "urge"
  ]
  node [
    id 1111
    label "zbli&#380;a&#263;"
  ]
  node [
    id 1112
    label "przykrochmala&#263;"
  ]
  node [
    id 1113
    label "uderza&#263;"
  ]
  node [
    id 1114
    label "gem"
  ]
  node [
    id 1115
    label "kompozycja"
  ]
  node [
    id 1116
    label "runda"
  ]
  node [
    id 1117
    label "muzyka"
  ]
  node [
    id 1118
    label "struktura"
  ]
  node [
    id 1119
    label "zbi&#243;r"
  ]
  node [
    id 1120
    label "stage_set"
  ]
  node [
    id 1121
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1122
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1123
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 1124
    label "end"
  ]
  node [
    id 1125
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1126
    label "coating"
  ]
  node [
    id 1127
    label "drop"
  ]
  node [
    id 1128
    label "leave_office"
  ]
  node [
    id 1129
    label "fail"
  ]
  node [
    id 1130
    label "Cesarstwo_Bizanty&#324;skie"
  ]
  node [
    id 1131
    label "jednostka_administracyjna"
  ]
  node [
    id 1132
    label "zesp&#243;&#322;"
  ]
  node [
    id 1133
    label "podejrzany"
  ]
  node [
    id 1134
    label "s&#261;downictwo"
  ]
  node [
    id 1135
    label "system"
  ]
  node [
    id 1136
    label "biuro"
  ]
  node [
    id 1137
    label "wytw&#243;r"
  ]
  node [
    id 1138
    label "court"
  ]
  node [
    id 1139
    label "forum"
  ]
  node [
    id 1140
    label "bronienie"
  ]
  node [
    id 1141
    label "urz&#261;d"
  ]
  node [
    id 1142
    label "wydarzenie"
  ]
  node [
    id 1143
    label "oskar&#380;yciel"
  ]
  node [
    id 1144
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1145
    label "skazany"
  ]
  node [
    id 1146
    label "post&#281;powanie"
  ]
  node [
    id 1147
    label "broni&#263;"
  ]
  node [
    id 1148
    label "my&#347;l"
  ]
  node [
    id 1149
    label "pods&#261;dny"
  ]
  node [
    id 1150
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1151
    label "obrona"
  ]
  node [
    id 1152
    label "wypowied&#378;"
  ]
  node [
    id 1153
    label "instytucja"
  ]
  node [
    id 1154
    label "antylogizm"
  ]
  node [
    id 1155
    label "konektyw"
  ]
  node [
    id 1156
    label "&#347;wiadek"
  ]
  node [
    id 1157
    label "procesowicz"
  ]
  node [
    id 1158
    label "strona"
  ]
  node [
    id 1159
    label "przedmiot"
  ]
  node [
    id 1160
    label "p&#322;&#243;d"
  ]
  node [
    id 1161
    label "rezultat"
  ]
  node [
    id 1162
    label "Mazowsze"
  ]
  node [
    id 1163
    label "odm&#322;adzanie"
  ]
  node [
    id 1164
    label "&#346;wietliki"
  ]
  node [
    id 1165
    label "whole"
  ]
  node [
    id 1166
    label "skupienie"
  ]
  node [
    id 1167
    label "The_Beatles"
  ]
  node [
    id 1168
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1169
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1170
    label "zabudowania"
  ]
  node [
    id 1171
    label "group"
  ]
  node [
    id 1172
    label "zespolik"
  ]
  node [
    id 1173
    label "schorzenie"
  ]
  node [
    id 1174
    label "ro&#347;lina"
  ]
  node [
    id 1175
    label "Depeche_Mode"
  ]
  node [
    id 1176
    label "batch"
  ]
  node [
    id 1177
    label "odm&#322;odzenie"
  ]
  node [
    id 1178
    label "stanowisko"
  ]
  node [
    id 1179
    label "position"
  ]
  node [
    id 1180
    label "siedziba"
  ]
  node [
    id 1181
    label "organ"
  ]
  node [
    id 1182
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1183
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1184
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1185
    label "mianowaniec"
  ]
  node [
    id 1186
    label "dzia&#322;"
  ]
  node [
    id 1187
    label "okienko"
  ]
  node [
    id 1188
    label "przebiec"
  ]
  node [
    id 1189
    label "charakter"
  ]
  node [
    id 1190
    label "czynno&#347;&#263;"
  ]
  node [
    id 1191
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1192
    label "motyw"
  ]
  node [
    id 1193
    label "przebiegni&#281;cie"
  ]
  node [
    id 1194
    label "fabu&#322;a"
  ]
  node [
    id 1195
    label "osoba_prawna"
  ]
  node [
    id 1196
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1197
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1198
    label "poj&#281;cie"
  ]
  node [
    id 1199
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1200
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1201
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1202
    label "Fundusze_Unijne"
  ]
  node [
    id 1203
    label "zamyka&#263;"
  ]
  node [
    id 1204
    label "establishment"
  ]
  node [
    id 1205
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1206
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1207
    label "afiliowa&#263;"
  ]
  node [
    id 1208
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1209
    label "standard"
  ]
  node [
    id 1210
    label "zamykanie"
  ]
  node [
    id 1211
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1212
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1213
    label "pos&#322;uchanie"
  ]
  node [
    id 1214
    label "sparafrazowanie"
  ]
  node [
    id 1215
    label "strawestowa&#263;"
  ]
  node [
    id 1216
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1217
    label "trawestowa&#263;"
  ]
  node [
    id 1218
    label "sparafrazowa&#263;"
  ]
  node [
    id 1219
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1220
    label "sformu&#322;owanie"
  ]
  node [
    id 1221
    label "parafrazowanie"
  ]
  node [
    id 1222
    label "ozdobnik"
  ]
  node [
    id 1223
    label "delimitacja"
  ]
  node [
    id 1224
    label "parafrazowa&#263;"
  ]
  node [
    id 1225
    label "stylizacja"
  ]
  node [
    id 1226
    label "komunikat"
  ]
  node [
    id 1227
    label "trawestowanie"
  ]
  node [
    id 1228
    label "strawestowanie"
  ]
  node [
    id 1229
    label "thinking"
  ]
  node [
    id 1230
    label "umys&#322;"
  ]
  node [
    id 1231
    label "political_orientation"
  ]
  node [
    id 1232
    label "istota"
  ]
  node [
    id 1233
    label "pomys&#322;"
  ]
  node [
    id 1234
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1235
    label "idea"
  ]
  node [
    id 1236
    label "fantomatyka"
  ]
  node [
    id 1237
    label "kognicja"
  ]
  node [
    id 1238
    label "campaign"
  ]
  node [
    id 1239
    label "rozprawa"
  ]
  node [
    id 1240
    label "zachowanie"
  ]
  node [
    id 1241
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1242
    label "fashion"
  ]
  node [
    id 1243
    label "robienie"
  ]
  node [
    id 1244
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1245
    label "zmierzanie"
  ]
  node [
    id 1246
    label "przes&#322;anka"
  ]
  node [
    id 1247
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1248
    label "kazanie"
  ]
  node [
    id 1249
    label "funktor"
  ]
  node [
    id 1250
    label "ob&#380;a&#322;owany"
  ]
  node [
    id 1251
    label "podejrzanie"
  ]
  node [
    id 1252
    label "pos&#261;dzanie"
  ]
  node [
    id 1253
    label "nieprzejrzysty"
  ]
  node [
    id 1254
    label "niepewny"
  ]
  node [
    id 1255
    label "z&#322;y"
  ]
  node [
    id 1256
    label "dysponowanie"
  ]
  node [
    id 1257
    label "dysponowa&#263;"
  ]
  node [
    id 1258
    label "skar&#380;yciel"
  ]
  node [
    id 1259
    label "egzamin"
  ]
  node [
    id 1260
    label "walka"
  ]
  node [
    id 1261
    label "liga"
  ]
  node [
    id 1262
    label "gracz"
  ]
  node [
    id 1263
    label "protection"
  ]
  node [
    id 1264
    label "poparcie"
  ]
  node [
    id 1265
    label "mecz"
  ]
  node [
    id 1266
    label "defense"
  ]
  node [
    id 1267
    label "auspices"
  ]
  node [
    id 1268
    label "gra"
  ]
  node [
    id 1269
    label "ochrona"
  ]
  node [
    id 1270
    label "sp&#243;r"
  ]
  node [
    id 1271
    label "wojsko"
  ]
  node [
    id 1272
    label "manewr"
  ]
  node [
    id 1273
    label "defensive_structure"
  ]
  node [
    id 1274
    label "guard_duty"
  ]
  node [
    id 1275
    label "uczestnik"
  ]
  node [
    id 1276
    label "dru&#380;ba"
  ]
  node [
    id 1277
    label "obserwator"
  ]
  node [
    id 1278
    label "osoba_fizyczna"
  ]
  node [
    id 1279
    label "fend"
  ]
  node [
    id 1280
    label "reprezentowa&#263;"
  ]
  node [
    id 1281
    label "zdawa&#263;"
  ]
  node [
    id 1282
    label "czuwa&#263;"
  ]
  node [
    id 1283
    label "preach"
  ]
  node [
    id 1284
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 1285
    label "walczy&#263;"
  ]
  node [
    id 1286
    label "resist"
  ]
  node [
    id 1287
    label "adwokatowa&#263;"
  ]
  node [
    id 1288
    label "rebuff"
  ]
  node [
    id 1289
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1290
    label "udowadnia&#263;"
  ]
  node [
    id 1291
    label "gra&#263;"
  ]
  node [
    id 1292
    label "refuse"
  ]
  node [
    id 1293
    label "kartka"
  ]
  node [
    id 1294
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1295
    label "logowanie"
  ]
  node [
    id 1296
    label "plik"
  ]
  node [
    id 1297
    label "adres_internetowy"
  ]
  node [
    id 1298
    label "linia"
  ]
  node [
    id 1299
    label "serwis_internetowy"
  ]
  node [
    id 1300
    label "bok"
  ]
  node [
    id 1301
    label "skr&#281;canie"
  ]
  node [
    id 1302
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1303
    label "orientowanie"
  ]
  node [
    id 1304
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1305
    label "uj&#281;cie"
  ]
  node [
    id 1306
    label "zorientowanie"
  ]
  node [
    id 1307
    label "ty&#322;"
  ]
  node [
    id 1308
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1309
    label "fragment"
  ]
  node [
    id 1310
    label "layout"
  ]
  node [
    id 1311
    label "obiekt"
  ]
  node [
    id 1312
    label "zorientowa&#263;"
  ]
  node [
    id 1313
    label "pagina"
  ]
  node [
    id 1314
    label "g&#243;ra"
  ]
  node [
    id 1315
    label "orientowa&#263;"
  ]
  node [
    id 1316
    label "voice"
  ]
  node [
    id 1317
    label "orientacja"
  ]
  node [
    id 1318
    label "prz&#243;d"
  ]
  node [
    id 1319
    label "internet"
  ]
  node [
    id 1320
    label "powierzchnia"
  ]
  node [
    id 1321
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1322
    label "forma"
  ]
  node [
    id 1323
    label "skr&#281;cenie"
  ]
  node [
    id 1324
    label "niedost&#281;pny"
  ]
  node [
    id 1325
    label "obstawanie"
  ]
  node [
    id 1326
    label "adwokatowanie"
  ]
  node [
    id 1327
    label "zdawanie"
  ]
  node [
    id 1328
    label "walczenie"
  ]
  node [
    id 1329
    label "zabezpieczenie"
  ]
  node [
    id 1330
    label "t&#322;umaczenie"
  ]
  node [
    id 1331
    label "parry"
  ]
  node [
    id 1332
    label "or&#281;dowanie"
  ]
  node [
    id 1333
    label "granie"
  ]
  node [
    id 1334
    label "biurko"
  ]
  node [
    id 1335
    label "boks"
  ]
  node [
    id 1336
    label "palestra"
  ]
  node [
    id 1337
    label "Biuro_Lustracyjne"
  ]
  node [
    id 1338
    label "agency"
  ]
  node [
    id 1339
    label "board"
  ]
  node [
    id 1340
    label "pomieszczenie"
  ]
  node [
    id 1341
    label "j&#261;dro"
  ]
  node [
    id 1342
    label "systemik"
  ]
  node [
    id 1343
    label "rozprz&#261;c"
  ]
  node [
    id 1344
    label "oprogramowanie"
  ]
  node [
    id 1345
    label "systemat"
  ]
  node [
    id 1346
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1347
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1348
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1349
    label "model"
  ]
  node [
    id 1350
    label "usenet"
  ]
  node [
    id 1351
    label "porz&#261;dek"
  ]
  node [
    id 1352
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1353
    label "przyn&#281;ta"
  ]
  node [
    id 1354
    label "net"
  ]
  node [
    id 1355
    label "w&#281;dkarstwo"
  ]
  node [
    id 1356
    label "eratem"
  ]
  node [
    id 1357
    label "oddzia&#322;"
  ]
  node [
    id 1358
    label "doktryna"
  ]
  node [
    id 1359
    label "pulpit"
  ]
  node [
    id 1360
    label "konstelacja"
  ]
  node [
    id 1361
    label "jednostka_geologiczna"
  ]
  node [
    id 1362
    label "o&#347;"
  ]
  node [
    id 1363
    label "podsystem"
  ]
  node [
    id 1364
    label "metoda"
  ]
  node [
    id 1365
    label "ryba"
  ]
  node [
    id 1366
    label "Leopard"
  ]
  node [
    id 1367
    label "spos&#243;b"
  ]
  node [
    id 1368
    label "Android"
  ]
  node [
    id 1369
    label "cybernetyk"
  ]
  node [
    id 1370
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1371
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1372
    label "method"
  ]
  node [
    id 1373
    label "podstawa"
  ]
  node [
    id 1374
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1375
    label "relacja_logiczna"
  ]
  node [
    id 1376
    label "judiciary"
  ]
  node [
    id 1377
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1378
    label "grupa_dyskusyjna"
  ]
  node [
    id 1379
    label "plac"
  ]
  node [
    id 1380
    label "bazylika"
  ]
  node [
    id 1381
    label "przestrze&#324;"
  ]
  node [
    id 1382
    label "miejsce"
  ]
  node [
    id 1383
    label "portal"
  ]
  node [
    id 1384
    label "konferencja"
  ]
  node [
    id 1385
    label "agora"
  ]
  node [
    id 1386
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1387
    label "poumieszcza&#263;"
  ]
  node [
    id 1388
    label "hide"
  ]
  node [
    id 1389
    label "znie&#347;&#263;"
  ]
  node [
    id 1390
    label "zw&#322;oki"
  ]
  node [
    id 1391
    label "straci&#263;"
  ]
  node [
    id 1392
    label "powk&#322;ada&#263;"
  ]
  node [
    id 1393
    label "bury"
  ]
  node [
    id 1394
    label "stracenie"
  ]
  node [
    id 1395
    label "forfeit"
  ]
  node [
    id 1396
    label "wytraci&#263;"
  ]
  node [
    id 1397
    label "waste"
  ]
  node [
    id 1398
    label "przegra&#263;"
  ]
  node [
    id 1399
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 1400
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 1401
    label "execute"
  ]
  node [
    id 1402
    label "omin&#261;&#263;"
  ]
  node [
    id 1403
    label "ubra&#263;"
  ]
  node [
    id 1404
    label "oblec_si&#281;"
  ]
  node [
    id 1405
    label "przekaza&#263;"
  ]
  node [
    id 1406
    label "str&#243;j"
  ]
  node [
    id 1407
    label "wpoi&#263;"
  ]
  node [
    id 1408
    label "przyodzia&#263;"
  ]
  node [
    id 1409
    label "natchn&#261;&#263;"
  ]
  node [
    id 1410
    label "load"
  ]
  node [
    id 1411
    label "wzbudzi&#263;"
  ]
  node [
    id 1412
    label "deposit"
  ]
  node [
    id 1413
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1414
    label "oblec"
  ]
  node [
    id 1415
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1416
    label "float"
  ]
  node [
    id 1417
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1418
    label "revoke"
  ]
  node [
    id 1419
    label "ranny"
  ]
  node [
    id 1420
    label "wytrzyma&#263;"
  ]
  node [
    id 1421
    label "lift"
  ]
  node [
    id 1422
    label "podda&#263;_si&#281;"
  ]
  node [
    id 1423
    label "przenie&#347;&#263;"
  ]
  node [
    id 1424
    label "&#347;cierpie&#263;"
  ]
  node [
    id 1425
    label "porwa&#263;"
  ]
  node [
    id 1426
    label "abolicjonista"
  ]
  node [
    id 1427
    label "ekshumowanie"
  ]
  node [
    id 1428
    label "pochowanie"
  ]
  node [
    id 1429
    label "zabalsamowanie"
  ]
  node [
    id 1430
    label "kremacja"
  ]
  node [
    id 1431
    label "pijany"
  ]
  node [
    id 1432
    label "zm&#281;czony"
  ]
  node [
    id 1433
    label "tanatoplastyka"
  ]
  node [
    id 1434
    label "balsamowa&#263;"
  ]
  node [
    id 1435
    label "nieumar&#322;y"
  ]
  node [
    id 1436
    label "balsamowanie"
  ]
  node [
    id 1437
    label "cia&#322;o"
  ]
  node [
    id 1438
    label "ekshumowa&#263;"
  ]
  node [
    id 1439
    label "sekcja"
  ]
  node [
    id 1440
    label "tanatoplastyk"
  ]
  node [
    id 1441
    label "zabalsamowa&#263;"
  ]
  node [
    id 1442
    label "pogrzeb"
  ]
  node [
    id 1443
    label "brunatny"
  ]
  node [
    id 1444
    label "przygn&#281;biaj&#261;cy"
  ]
  node [
    id 1445
    label "ciemnoszary"
  ]
  node [
    id 1446
    label "brudnoszary"
  ]
  node [
    id 1447
    label "buro"
  ]
  node [
    id 1448
    label "czyj&#347;"
  ]
  node [
    id 1449
    label "prywatny"
  ]
  node [
    id 1450
    label "g&#243;wniarz"
  ]
  node [
    id 1451
    label "synek"
  ]
  node [
    id 1452
    label "boyfriend"
  ]
  node [
    id 1453
    label "okrzos"
  ]
  node [
    id 1454
    label "dziecko"
  ]
  node [
    id 1455
    label "sympatia"
  ]
  node [
    id 1456
    label "usynowienie"
  ]
  node [
    id 1457
    label "kawaler"
  ]
  node [
    id 1458
    label "pederasta"
  ]
  node [
    id 1459
    label "m&#322;odzieniec"
  ]
  node [
    id 1460
    label "kajtek"
  ]
  node [
    id 1461
    label "&#347;l&#261;ski"
  ]
  node [
    id 1462
    label "usynawianie"
  ]
  node [
    id 1463
    label "utulenie"
  ]
  node [
    id 1464
    label "pediatra"
  ]
  node [
    id 1465
    label "dzieciak"
  ]
  node [
    id 1466
    label "utulanie"
  ]
  node [
    id 1467
    label "dzieciarnia"
  ]
  node [
    id 1468
    label "niepe&#322;noletni"
  ]
  node [
    id 1469
    label "organizm"
  ]
  node [
    id 1470
    label "utula&#263;"
  ]
  node [
    id 1471
    label "cz&#322;owieczek"
  ]
  node [
    id 1472
    label "fledgling"
  ]
  node [
    id 1473
    label "utuli&#263;"
  ]
  node [
    id 1474
    label "m&#322;odzik"
  ]
  node [
    id 1475
    label "pedofil"
  ]
  node [
    id 1476
    label "m&#322;odziak"
  ]
  node [
    id 1477
    label "potomek"
  ]
  node [
    id 1478
    label "entliczek-pentliczek"
  ]
  node [
    id 1479
    label "potomstwo"
  ]
  node [
    id 1480
    label "sraluch"
  ]
  node [
    id 1481
    label "emocja"
  ]
  node [
    id 1482
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1483
    label "partner"
  ]
  node [
    id 1484
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1485
    label "love"
  ]
  node [
    id 1486
    label "kredens"
  ]
  node [
    id 1487
    label "zawodnik"
  ]
  node [
    id 1488
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 1489
    label "bylina"
  ]
  node [
    id 1490
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 1491
    label "pomoc"
  ]
  node [
    id 1492
    label "wrzosowate"
  ]
  node [
    id 1493
    label "pomagacz"
  ]
  node [
    id 1494
    label "junior"
  ]
  node [
    id 1495
    label "junak"
  ]
  node [
    id 1496
    label "m&#322;odzie&#380;"
  ]
  node [
    id 1497
    label "mo&#322;ojec"
  ]
  node [
    id 1498
    label "m&#322;okos"
  ]
  node [
    id 1499
    label "smarkateria"
  ]
  node [
    id 1500
    label "ch&#322;opiec"
  ]
  node [
    id 1501
    label "kawa&#322;ek"
  ]
  node [
    id 1502
    label "gej"
  ]
  node [
    id 1503
    label "cug"
  ]
  node [
    id 1504
    label "krepel"
  ]
  node [
    id 1505
    label "mietlorz"
  ]
  node [
    id 1506
    label "francuz"
  ]
  node [
    id 1507
    label "etnolekt"
  ]
  node [
    id 1508
    label "sza&#322;ot"
  ]
  node [
    id 1509
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 1510
    label "polski"
  ]
  node [
    id 1511
    label "regionalny"
  ]
  node [
    id 1512
    label "halba"
  ]
  node [
    id 1513
    label "buchta"
  ]
  node [
    id 1514
    label "czarne_kluski"
  ]
  node [
    id 1515
    label "szpajza"
  ]
  node [
    id 1516
    label "szl&#261;ski"
  ]
  node [
    id 1517
    label "&#347;lonski"
  ]
  node [
    id 1518
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 1519
    label "waloszek"
  ]
  node [
    id 1520
    label "order"
  ]
  node [
    id 1521
    label "zalotnik"
  ]
  node [
    id 1522
    label "kawalerka"
  ]
  node [
    id 1523
    label "rycerz"
  ]
  node [
    id 1524
    label "odznaczenie"
  ]
  node [
    id 1525
    label "nie&#380;onaty"
  ]
  node [
    id 1526
    label "zakon_rycerski"
  ]
  node [
    id 1527
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 1528
    label "przysposobienie"
  ]
  node [
    id 1529
    label "syn"
  ]
  node [
    id 1530
    label "adoption"
  ]
  node [
    id 1531
    label "przysposabianie"
  ]
  node [
    id 1532
    label "sail"
  ]
  node [
    id 1533
    label "leave"
  ]
  node [
    id 1534
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 1535
    label "travel"
  ]
  node [
    id 1536
    label "proceed"
  ]
  node [
    id 1537
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1538
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 1539
    label "zmieni&#263;"
  ]
  node [
    id 1540
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 1541
    label "zosta&#263;"
  ]
  node [
    id 1542
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1543
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 1544
    label "przyj&#261;&#263;"
  ]
  node [
    id 1545
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 1546
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 1547
    label "play_along"
  ]
  node [
    id 1548
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 1549
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1550
    label "become"
  ]
  node [
    id 1551
    label "przybra&#263;"
  ]
  node [
    id 1552
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1553
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 1554
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 1555
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 1556
    label "receive"
  ]
  node [
    id 1557
    label "obra&#263;"
  ]
  node [
    id 1558
    label "uzna&#263;"
  ]
  node [
    id 1559
    label "draw"
  ]
  node [
    id 1560
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 1561
    label "przyj&#281;cie"
  ]
  node [
    id 1562
    label "swallow"
  ]
  node [
    id 1563
    label "odebra&#263;"
  ]
  node [
    id 1564
    label "dostarczy&#263;"
  ]
  node [
    id 1565
    label "wzi&#261;&#263;"
  ]
  node [
    id 1566
    label "absorb"
  ]
  node [
    id 1567
    label "undertake"
  ]
  node [
    id 1568
    label "sprawi&#263;"
  ]
  node [
    id 1569
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1570
    label "come_up"
  ]
  node [
    id 1571
    label "przej&#347;&#263;"
  ]
  node [
    id 1572
    label "zyska&#263;"
  ]
  node [
    id 1573
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1574
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1575
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1576
    label "pozosta&#263;"
  ]
  node [
    id 1577
    label "catch"
  ]
  node [
    id 1578
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1579
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1580
    label "pozostawi&#263;"
  ]
  node [
    id 1581
    label "zostawi&#263;"
  ]
  node [
    id 1582
    label "potani&#263;"
  ]
  node [
    id 1583
    label "evacuate"
  ]
  node [
    id 1584
    label "humiliate"
  ]
  node [
    id 1585
    label "tekst"
  ]
  node [
    id 1586
    label "authorize"
  ]
  node [
    id 1587
    label "loom"
  ]
  node [
    id 1588
    label "result"
  ]
  node [
    id 1589
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 1590
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 1591
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 1592
    label "appear"
  ]
  node [
    id 1593
    label "zgin&#261;&#263;"
  ]
  node [
    id 1594
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 1595
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 1596
    label "rise"
  ]
  node [
    id 1597
    label "Stary_&#346;wiat"
  ]
  node [
    id 1598
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1599
    label "p&#243;&#322;noc"
  ]
  node [
    id 1600
    label "Wsch&#243;d"
  ]
  node [
    id 1601
    label "class"
  ]
  node [
    id 1602
    label "geosfera"
  ]
  node [
    id 1603
    label "obiekt_naturalny"
  ]
  node [
    id 1604
    label "przejmowanie"
  ]
  node [
    id 1605
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1606
    label "przyroda"
  ]
  node [
    id 1607
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 1608
    label "po&#322;udnie"
  ]
  node [
    id 1609
    label "zjawisko"
  ]
  node [
    id 1610
    label "rzecz"
  ]
  node [
    id 1611
    label "makrokosmos"
  ]
  node [
    id 1612
    label "huczek"
  ]
  node [
    id 1613
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1614
    label "environment"
  ]
  node [
    id 1615
    label "morze"
  ]
  node [
    id 1616
    label "rze&#378;ba"
  ]
  node [
    id 1617
    label "hydrosfera"
  ]
  node [
    id 1618
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 1619
    label "ciemna_materia"
  ]
  node [
    id 1620
    label "ekosystem"
  ]
  node [
    id 1621
    label "biota"
  ]
  node [
    id 1622
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 1623
    label "ekosfera"
  ]
  node [
    id 1624
    label "geotermia"
  ]
  node [
    id 1625
    label "planeta"
  ]
  node [
    id 1626
    label "ozonosfera"
  ]
  node [
    id 1627
    label "wszechstworzenie"
  ]
  node [
    id 1628
    label "woda"
  ]
  node [
    id 1629
    label "kuchnia"
  ]
  node [
    id 1630
    label "biosfera"
  ]
  node [
    id 1631
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1632
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 1633
    label "populace"
  ]
  node [
    id 1634
    label "magnetosfera"
  ]
  node [
    id 1635
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1636
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1637
    label "universe"
  ]
  node [
    id 1638
    label "biegun"
  ]
  node [
    id 1639
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1640
    label "litosfera"
  ]
  node [
    id 1641
    label "teren"
  ]
  node [
    id 1642
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1643
    label "stw&#243;r"
  ]
  node [
    id 1644
    label "p&#243;&#322;kula"
  ]
  node [
    id 1645
    label "przej&#281;cie"
  ]
  node [
    id 1646
    label "barysfera"
  ]
  node [
    id 1647
    label "obszar"
  ]
  node [
    id 1648
    label "czarna_dziura"
  ]
  node [
    id 1649
    label "atmosfera"
  ]
  node [
    id 1650
    label "przej&#261;&#263;"
  ]
  node [
    id 1651
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1652
    label "Ziemia"
  ]
  node [
    id 1653
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1654
    label "geoida"
  ]
  node [
    id 1655
    label "zagranica"
  ]
  node [
    id 1656
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1657
    label "fauna"
  ]
  node [
    id 1658
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1659
    label "jednostka_systematyczna"
  ]
  node [
    id 1660
    label "gromada"
  ]
  node [
    id 1661
    label "egzemplarz"
  ]
  node [
    id 1662
    label "Entuzjastki"
  ]
  node [
    id 1663
    label "Terranie"
  ]
  node [
    id 1664
    label "category"
  ]
  node [
    id 1665
    label "pakiet_klimatyczny"
  ]
  node [
    id 1666
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1667
    label "cz&#261;steczka"
  ]
  node [
    id 1668
    label "type"
  ]
  node [
    id 1669
    label "specgrupa"
  ]
  node [
    id 1670
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1671
    label "Eurogrupa"
  ]
  node [
    id 1672
    label "formacja_geologiczna"
  ]
  node [
    id 1673
    label "harcerze_starsi"
  ]
  node [
    id 1674
    label "Kosowo"
  ]
  node [
    id 1675
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1676
    label "Zab&#322;ocie"
  ]
  node [
    id 1677
    label "zach&#243;d"
  ]
  node [
    id 1678
    label "Pow&#261;zki"
  ]
  node [
    id 1679
    label "Piotrowo"
  ]
  node [
    id 1680
    label "Olszanica"
  ]
  node [
    id 1681
    label "Ruda_Pabianicka"
  ]
  node [
    id 1682
    label "holarktyka"
  ]
  node [
    id 1683
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1684
    label "Ludwin&#243;w"
  ]
  node [
    id 1685
    label "Arktyka"
  ]
  node [
    id 1686
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1687
    label "Zabu&#380;e"
  ]
  node [
    id 1688
    label "antroposfera"
  ]
  node [
    id 1689
    label "Neogea"
  ]
  node [
    id 1690
    label "Syberia_Zachodnia"
  ]
  node [
    id 1691
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1692
    label "zakres"
  ]
  node [
    id 1693
    label "pas_planetoid"
  ]
  node [
    id 1694
    label "Syberia_Wschodnia"
  ]
  node [
    id 1695
    label "Antarktyka"
  ]
  node [
    id 1696
    label "Rakowice"
  ]
  node [
    id 1697
    label "akrecja"
  ]
  node [
    id 1698
    label "wymiar"
  ]
  node [
    id 1699
    label "&#321;&#281;g"
  ]
  node [
    id 1700
    label "Kresy_Zachodnie"
  ]
  node [
    id 1701
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1702
    label "wsch&#243;d"
  ]
  node [
    id 1703
    label "Notogea"
  ]
  node [
    id 1704
    label "integer"
  ]
  node [
    id 1705
    label "liczba"
  ]
  node [
    id 1706
    label "zlewanie_si&#281;"
  ]
  node [
    id 1707
    label "ilo&#347;&#263;"
  ]
  node [
    id 1708
    label "uk&#322;ad"
  ]
  node [
    id 1709
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1710
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1711
    label "pe&#322;ny"
  ]
  node [
    id 1712
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1713
    label "proces"
  ]
  node [
    id 1714
    label "boski"
  ]
  node [
    id 1715
    label "krajobraz"
  ]
  node [
    id 1716
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1717
    label "przywidzenie"
  ]
  node [
    id 1718
    label "presence"
  ]
  node [
    id 1719
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1720
    label "rozdzielanie"
  ]
  node [
    id 1721
    label "bezbrze&#380;e"
  ]
  node [
    id 1722
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1723
    label "niezmierzony"
  ]
  node [
    id 1724
    label "przedzielenie"
  ]
  node [
    id 1725
    label "nielito&#347;ciwy"
  ]
  node [
    id 1726
    label "oktant"
  ]
  node [
    id 1727
    label "przedzieli&#263;"
  ]
  node [
    id 1728
    label "przestw&#243;r"
  ]
  node [
    id 1729
    label "&#347;rodowisko"
  ]
  node [
    id 1730
    label "rura"
  ]
  node [
    id 1731
    label "grzebiuszka"
  ]
  node [
    id 1732
    label "odbicie"
  ]
  node [
    id 1733
    label "atom"
  ]
  node [
    id 1734
    label "kosmos"
  ]
  node [
    id 1735
    label "miniatura"
  ]
  node [
    id 1736
    label "smok_wawelski"
  ]
  node [
    id 1737
    label "niecz&#322;owiek"
  ]
  node [
    id 1738
    label "monster"
  ]
  node [
    id 1739
    label "istota_&#380;ywa"
  ]
  node [
    id 1740
    label "potw&#243;r"
  ]
  node [
    id 1741
    label "istota_fantastyczna"
  ]
  node [
    id 1742
    label "kultura"
  ]
  node [
    id 1743
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1744
    label "ciep&#322;o"
  ]
  node [
    id 1745
    label "energia_termiczna"
  ]
  node [
    id 1746
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 1747
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1748
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 1749
    label "aspekt"
  ]
  node [
    id 1750
    label "troposfera"
  ]
  node [
    id 1751
    label "klimat"
  ]
  node [
    id 1752
    label "metasfera"
  ]
  node [
    id 1753
    label "atmosferyki"
  ]
  node [
    id 1754
    label "homosfera"
  ]
  node [
    id 1755
    label "cecha"
  ]
  node [
    id 1756
    label "powietrznia"
  ]
  node [
    id 1757
    label "jonosfera"
  ]
  node [
    id 1758
    label "termosfera"
  ]
  node [
    id 1759
    label "egzosfera"
  ]
  node [
    id 1760
    label "heterosfera"
  ]
  node [
    id 1761
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1762
    label "tropopauza"
  ]
  node [
    id 1763
    label "kwas"
  ]
  node [
    id 1764
    label "powietrze"
  ]
  node [
    id 1765
    label "stratosfera"
  ]
  node [
    id 1766
    label "pow&#322;oka"
  ]
  node [
    id 1767
    label "mezosfera"
  ]
  node [
    id 1768
    label "mezopauza"
  ]
  node [
    id 1769
    label "atmosphere"
  ]
  node [
    id 1770
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 1771
    label "sferoida"
  ]
  node [
    id 1772
    label "object"
  ]
  node [
    id 1773
    label "temat"
  ]
  node [
    id 1774
    label "wpadni&#281;cie"
  ]
  node [
    id 1775
    label "mienie"
  ]
  node [
    id 1776
    label "wpa&#347;&#263;"
  ]
  node [
    id 1777
    label "wpadanie"
  ]
  node [
    id 1778
    label "wpada&#263;"
  ]
  node [
    id 1779
    label "treat"
  ]
  node [
    id 1780
    label "czerpa&#263;"
  ]
  node [
    id 1781
    label "handle"
  ]
  node [
    id 1782
    label "ogarnia&#263;"
  ]
  node [
    id 1783
    label "bang"
  ]
  node [
    id 1784
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1785
    label "stimulate"
  ]
  node [
    id 1786
    label "thrill"
  ]
  node [
    id 1787
    label "czerpanie"
  ]
  node [
    id 1788
    label "acquisition"
  ]
  node [
    id 1789
    label "branie"
  ]
  node [
    id 1790
    label "caparison"
  ]
  node [
    id 1791
    label "movement"
  ]
  node [
    id 1792
    label "wzbudzanie"
  ]
  node [
    id 1793
    label "ogarnianie"
  ]
  node [
    id 1794
    label "wra&#380;enie"
  ]
  node [
    id 1795
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1796
    label "interception"
  ]
  node [
    id 1797
    label "wzbudzenie"
  ]
  node [
    id 1798
    label "emotion"
  ]
  node [
    id 1799
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1800
    label "wzi&#281;cie"
  ]
  node [
    id 1801
    label "zboczenie"
  ]
  node [
    id 1802
    label "om&#243;wienie"
  ]
  node [
    id 1803
    label "sponiewieranie"
  ]
  node [
    id 1804
    label "discipline"
  ]
  node [
    id 1805
    label "omawia&#263;"
  ]
  node [
    id 1806
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1807
    label "tre&#347;&#263;"
  ]
  node [
    id 1808
    label "sponiewiera&#263;"
  ]
  node [
    id 1809
    label "element"
  ]
  node [
    id 1810
    label "entity"
  ]
  node [
    id 1811
    label "tematyka"
  ]
  node [
    id 1812
    label "w&#261;tek"
  ]
  node [
    id 1813
    label "zbaczanie"
  ]
  node [
    id 1814
    label "program_nauczania"
  ]
  node [
    id 1815
    label "om&#243;wi&#263;"
  ]
  node [
    id 1816
    label "omawianie"
  ]
  node [
    id 1817
    label "thing"
  ]
  node [
    id 1818
    label "zbacza&#263;"
  ]
  node [
    id 1819
    label "zboczy&#263;"
  ]
  node [
    id 1820
    label "performance"
  ]
  node [
    id 1821
    label "sztuka"
  ]
  node [
    id 1822
    label "Boreasz"
  ]
  node [
    id 1823
    label "noc"
  ]
  node [
    id 1824
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1825
    label "strona_&#347;wiata"
  ]
  node [
    id 1826
    label "godzina"
  ]
  node [
    id 1827
    label "warstwa"
  ]
  node [
    id 1828
    label "kriosfera"
  ]
  node [
    id 1829
    label "lej_polarny"
  ]
  node [
    id 1830
    label "sfera"
  ]
  node [
    id 1831
    label "brzeg"
  ]
  node [
    id 1832
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 1833
    label "p&#322;oza"
  ]
  node [
    id 1834
    label "zawiasy"
  ]
  node [
    id 1835
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1836
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 1837
    label "reda"
  ]
  node [
    id 1838
    label "zbiornik_wodny"
  ]
  node [
    id 1839
    label "przymorze"
  ]
  node [
    id 1840
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 1841
    label "bezmiar"
  ]
  node [
    id 1842
    label "pe&#322;ne_morze"
  ]
  node [
    id 1843
    label "latarnia_morska"
  ]
  node [
    id 1844
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1845
    label "nereida"
  ]
  node [
    id 1846
    label "okeanida"
  ]
  node [
    id 1847
    label "marina"
  ]
  node [
    id 1848
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 1849
    label "Morze_Czerwone"
  ]
  node [
    id 1850
    label "talasoterapia"
  ]
  node [
    id 1851
    label "Morze_Bia&#322;e"
  ]
  node [
    id 1852
    label "paliszcze"
  ]
  node [
    id 1853
    label "Neptun"
  ]
  node [
    id 1854
    label "Morze_Czarne"
  ]
  node [
    id 1855
    label "laguna"
  ]
  node [
    id 1856
    label "Morze_Egejskie"
  ]
  node [
    id 1857
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 1858
    label "Morze_Adriatyckie"
  ]
  node [
    id 1859
    label "rze&#378;biarstwo"
  ]
  node [
    id 1860
    label "planacja"
  ]
  node [
    id 1861
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1862
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1863
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 1864
    label "bozzetto"
  ]
  node [
    id 1865
    label "plastyka"
  ]
  node [
    id 1866
    label "&#347;rodek"
  ]
  node [
    id 1867
    label "dzie&#324;"
  ]
  node [
    id 1868
    label "dwunasta"
  ]
  node [
    id 1869
    label "pora"
  ]
  node [
    id 1870
    label "ozon"
  ]
  node [
    id 1871
    label "gleba"
  ]
  node [
    id 1872
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 1873
    label "sialma"
  ]
  node [
    id 1874
    label "skorupa_ziemska"
  ]
  node [
    id 1875
    label "warstwa_perydotytowa"
  ]
  node [
    id 1876
    label "warstwa_granitowa"
  ]
  node [
    id 1877
    label "kresom&#243;zgowie"
  ]
  node [
    id 1878
    label "przyra"
  ]
  node [
    id 1879
    label "biom"
  ]
  node [
    id 1880
    label "awifauna"
  ]
  node [
    id 1881
    label "ichtiofauna"
  ]
  node [
    id 1882
    label "geosystem"
  ]
  node [
    id 1883
    label "dotleni&#263;"
  ]
  node [
    id 1884
    label "spi&#281;trza&#263;"
  ]
  node [
    id 1885
    label "spi&#281;trzenie"
  ]
  node [
    id 1886
    label "utylizator"
  ]
  node [
    id 1887
    label "p&#322;ycizna"
  ]
  node [
    id 1888
    label "nabranie"
  ]
  node [
    id 1889
    label "Waruna"
  ]
  node [
    id 1890
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1891
    label "przybieranie"
  ]
  node [
    id 1892
    label "uci&#261;g"
  ]
  node [
    id 1893
    label "bombast"
  ]
  node [
    id 1894
    label "fala"
  ]
  node [
    id 1895
    label "kryptodepresja"
  ]
  node [
    id 1896
    label "water"
  ]
  node [
    id 1897
    label "wysi&#281;k"
  ]
  node [
    id 1898
    label "pustka"
  ]
  node [
    id 1899
    label "ciecz"
  ]
  node [
    id 1900
    label "przybrze&#380;e"
  ]
  node [
    id 1901
    label "nap&#243;j"
  ]
  node [
    id 1902
    label "spi&#281;trzanie"
  ]
  node [
    id 1903
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1904
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1905
    label "bicie"
  ]
  node [
    id 1906
    label "klarownik"
  ]
  node [
    id 1907
    label "chlastanie"
  ]
  node [
    id 1908
    label "woda_s&#322;odka"
  ]
  node [
    id 1909
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1910
    label "chlasta&#263;"
  ]
  node [
    id 1911
    label "uj&#281;cie_wody"
  ]
  node [
    id 1912
    label "zrzut"
  ]
  node [
    id 1913
    label "wodnik"
  ]
  node [
    id 1914
    label "pojazd"
  ]
  node [
    id 1915
    label "l&#243;d"
  ]
  node [
    id 1916
    label "wybrze&#380;e"
  ]
  node [
    id 1917
    label "deklamacja"
  ]
  node [
    id 1918
    label "tlenek"
  ]
  node [
    id 1919
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1920
    label "biotop"
  ]
  node [
    id 1921
    label "biocenoza"
  ]
  node [
    id 1922
    label "kontekst"
  ]
  node [
    id 1923
    label "miejsce_pracy"
  ]
  node [
    id 1924
    label "nation"
  ]
  node [
    id 1925
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1926
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1927
    label "szata_ro&#347;linna"
  ]
  node [
    id 1928
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1929
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1930
    label "zielono&#347;&#263;"
  ]
  node [
    id 1931
    label "pi&#281;tro"
  ]
  node [
    id 1932
    label "plant"
  ]
  node [
    id 1933
    label "iglak"
  ]
  node [
    id 1934
    label "cyprysowate"
  ]
  node [
    id 1935
    label "zaj&#281;cie"
  ]
  node [
    id 1936
    label "tajniki"
  ]
  node [
    id 1937
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 1938
    label "jedzenie"
  ]
  node [
    id 1939
    label "zlewozmywak"
  ]
  node [
    id 1940
    label "gotowa&#263;"
  ]
  node [
    id 1941
    label "strefa"
  ]
  node [
    id 1942
    label "Jowisz"
  ]
  node [
    id 1943
    label "syzygia"
  ]
  node [
    id 1944
    label "Saturn"
  ]
  node [
    id 1945
    label "Uran"
  ]
  node [
    id 1946
    label "message"
  ]
  node [
    id 1947
    label "dar"
  ]
  node [
    id 1948
    label "real"
  ]
  node [
    id 1949
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1950
    label "blok_wschodni"
  ]
  node [
    id 1951
    label "Europa_Wschodnia"
  ]
  node [
    id 1952
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 1953
    label "Daleki_Wsch&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 465
  ]
  edge [
    source 1
    target 466
  ]
  edge [
    source 1
    target 467
  ]
  edge [
    source 1
    target 468
  ]
  edge [
    source 1
    target 469
  ]
  edge [
    source 1
    target 470
  ]
  edge [
    source 1
    target 471
  ]
  edge [
    source 1
    target 472
  ]
  edge [
    source 1
    target 473
  ]
  edge [
    source 1
    target 474
  ]
  edge [
    source 1
    target 475
  ]
  edge [
    source 1
    target 476
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 1
    target 478
  ]
  edge [
    source 1
    target 479
  ]
  edge [
    source 1
    target 480
  ]
  edge [
    source 1
    target 481
  ]
  edge [
    source 1
    target 482
  ]
  edge [
    source 1
    target 483
  ]
  edge [
    source 1
    target 484
  ]
  edge [
    source 1
    target 485
  ]
  edge [
    source 1
    target 486
  ]
  edge [
    source 1
    target 487
  ]
  edge [
    source 1
    target 488
  ]
  edge [
    source 1
    target 489
  ]
  edge [
    source 1
    target 490
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 36
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 509
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 512
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 507
  ]
  edge [
    source 16
    target 510
  ]
  edge [
    source 16
    target 504
  ]
  edge [
    source 16
    target 511
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 496
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 566
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 581
  ]
  edge [
    source 17
    target 526
  ]
  edge [
    source 17
    target 527
  ]
  edge [
    source 17
    target 528
  ]
  edge [
    source 17
    target 530
  ]
  edge [
    source 17
    target 531
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 534
  ]
  edge [
    source 17
    target 535
  ]
  edge [
    source 17
    target 536
  ]
  edge [
    source 17
    target 537
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 29
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 939
  ]
  edge [
    source 20
    target 940
  ]
  edge [
    source 20
    target 941
  ]
  edge [
    source 20
    target 942
  ]
  edge [
    source 20
    target 460
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 943
  ]
  edge [
    source 20
    target 944
  ]
  edge [
    source 20
    target 945
  ]
  edge [
    source 20
    target 946
  ]
  edge [
    source 20
    target 947
  ]
  edge [
    source 20
    target 948
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 912
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 545
  ]
  edge [
    source 20
    target 448
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 581
  ]
  edge [
    source 22
    target 970
  ]
  edge [
    source 22
    target 971
  ]
  edge [
    source 22
    target 972
  ]
  edge [
    source 22
    target 973
  ]
  edge [
    source 22
    target 974
  ]
  edge [
    source 22
    target 975
  ]
  edge [
    source 22
    target 976
  ]
  edge [
    source 22
    target 977
  ]
  edge [
    source 22
    target 978
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 980
  ]
  edge [
    source 23
    target 981
  ]
  edge [
    source 23
    target 982
  ]
  edge [
    source 23
    target 983
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 771
  ]
  edge [
    source 23
    target 989
  ]
  edge [
    source 23
    target 990
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 992
  ]
  edge [
    source 23
    target 993
  ]
  edge [
    source 23
    target 994
  ]
  edge [
    source 23
    target 995
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 997
  ]
  edge [
    source 23
    target 998
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 496
  ]
  edge [
    source 23
    target 892
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1002
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 1004
  ]
  edge [
    source 23
    target 1005
  ]
  edge [
    source 23
    target 763
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 1009
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1011
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 529
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 550
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 773
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 904
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 581
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 542
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 814
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 460
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 798
  ]
  edge [
    source 24
    target 906
  ]
  edge [
    source 24
    target 907
  ]
  edge [
    source 24
    target 939
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 946
  ]
  edge [
    source 24
    target 947
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 949
  ]
  edge [
    source 24
    target 950
  ]
  edge [
    source 24
    target 858
  ]
  edge [
    source 24
    target 951
  ]
  edge [
    source 24
    target 952
  ]
  edge [
    source 24
    target 953
  ]
  edge [
    source 24
    target 954
  ]
  edge [
    source 24
    target 832
  ]
  edge [
    source 24
    target 955
  ]
  edge [
    source 24
    target 904
  ]
  edge [
    source 24
    target 905
  ]
  edge [
    source 24
    target 860
  ]
  edge [
    source 24
    target 861
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 32
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1130
  ]
  edge [
    source 25
    target 1131
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1132
  ]
  edge [
    source 26
    target 1133
  ]
  edge [
    source 26
    target 1134
  ]
  edge [
    source 26
    target 1135
  ]
  edge [
    source 26
    target 1136
  ]
  edge [
    source 26
    target 1137
  ]
  edge [
    source 26
    target 1138
  ]
  edge [
    source 26
    target 1139
  ]
  edge [
    source 26
    target 1140
  ]
  edge [
    source 26
    target 1141
  ]
  edge [
    source 26
    target 1142
  ]
  edge [
    source 26
    target 1143
  ]
  edge [
    source 26
    target 1144
  ]
  edge [
    source 26
    target 1145
  ]
  edge [
    source 26
    target 1146
  ]
  edge [
    source 26
    target 1147
  ]
  edge [
    source 26
    target 1148
  ]
  edge [
    source 26
    target 1149
  ]
  edge [
    source 26
    target 1150
  ]
  edge [
    source 26
    target 1151
  ]
  edge [
    source 26
    target 1152
  ]
  edge [
    source 26
    target 1153
  ]
  edge [
    source 26
    target 1154
  ]
  edge [
    source 26
    target 1155
  ]
  edge [
    source 26
    target 1156
  ]
  edge [
    source 26
    target 1157
  ]
  edge [
    source 26
    target 1158
  ]
  edge [
    source 26
    target 1159
  ]
  edge [
    source 26
    target 1160
  ]
  edge [
    source 26
    target 542
  ]
  edge [
    source 26
    target 1161
  ]
  edge [
    source 26
    target 1162
  ]
  edge [
    source 26
    target 1163
  ]
  edge [
    source 26
    target 1164
  ]
  edge [
    source 26
    target 1119
  ]
  edge [
    source 26
    target 1165
  ]
  edge [
    source 26
    target 1166
  ]
  edge [
    source 26
    target 1167
  ]
  edge [
    source 26
    target 1168
  ]
  edge [
    source 26
    target 1169
  ]
  edge [
    source 26
    target 1170
  ]
  edge [
    source 26
    target 1171
  ]
  edge [
    source 26
    target 1172
  ]
  edge [
    source 26
    target 1173
  ]
  edge [
    source 26
    target 1174
  ]
  edge [
    source 26
    target 399
  ]
  edge [
    source 26
    target 1175
  ]
  edge [
    source 26
    target 1176
  ]
  edge [
    source 26
    target 1177
  ]
  edge [
    source 26
    target 1178
  ]
  edge [
    source 26
    target 1179
  ]
  edge [
    source 26
    target 1180
  ]
  edge [
    source 26
    target 1181
  ]
  edge [
    source 26
    target 1182
  ]
  edge [
    source 26
    target 1183
  ]
  edge [
    source 26
    target 1184
  ]
  edge [
    source 26
    target 1185
  ]
  edge [
    source 26
    target 1186
  ]
  edge [
    source 26
    target 1187
  ]
  edge [
    source 26
    target 70
  ]
  edge [
    source 26
    target 1188
  ]
  edge [
    source 26
    target 1189
  ]
  edge [
    source 26
    target 1190
  ]
  edge [
    source 26
    target 1191
  ]
  edge [
    source 26
    target 1192
  ]
  edge [
    source 26
    target 1193
  ]
  edge [
    source 26
    target 1194
  ]
  edge [
    source 26
    target 1195
  ]
  edge [
    source 26
    target 1196
  ]
  edge [
    source 26
    target 1197
  ]
  edge [
    source 26
    target 1198
  ]
  edge [
    source 26
    target 1199
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 231
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 1204
  ]
  edge [
    source 26
    target 1205
  ]
  edge [
    source 26
    target 1206
  ]
  edge [
    source 26
    target 1207
  ]
  edge [
    source 26
    target 1208
  ]
  edge [
    source 26
    target 1209
  ]
  edge [
    source 26
    target 1210
  ]
  edge [
    source 26
    target 1211
  ]
  edge [
    source 26
    target 1212
  ]
  edge [
    source 26
    target 1213
  ]
  edge [
    source 26
    target 1214
  ]
  edge [
    source 26
    target 1215
  ]
  edge [
    source 26
    target 1216
  ]
  edge [
    source 26
    target 1217
  ]
  edge [
    source 26
    target 1218
  ]
  edge [
    source 26
    target 1219
  ]
  edge [
    source 26
    target 1220
  ]
  edge [
    source 26
    target 1221
  ]
  edge [
    source 26
    target 1222
  ]
  edge [
    source 26
    target 1223
  ]
  edge [
    source 26
    target 1224
  ]
  edge [
    source 26
    target 1225
  ]
  edge [
    source 26
    target 1226
  ]
  edge [
    source 26
    target 1227
  ]
  edge [
    source 26
    target 1228
  ]
  edge [
    source 26
    target 142
  ]
  edge [
    source 26
    target 1229
  ]
  edge [
    source 26
    target 1230
  ]
  edge [
    source 26
    target 1231
  ]
  edge [
    source 26
    target 1232
  ]
  edge [
    source 26
    target 1233
  ]
  edge [
    source 26
    target 1234
  ]
  edge [
    source 26
    target 1235
  ]
  edge [
    source 26
    target 1236
  ]
  edge [
    source 26
    target 1237
  ]
  edge [
    source 26
    target 1238
  ]
  edge [
    source 26
    target 1239
  ]
  edge [
    source 26
    target 1240
  ]
  edge [
    source 26
    target 1241
  ]
  edge [
    source 26
    target 1242
  ]
  edge [
    source 26
    target 1243
  ]
  edge [
    source 26
    target 1244
  ]
  edge [
    source 26
    target 1245
  ]
  edge [
    source 26
    target 1246
  ]
  edge [
    source 26
    target 1247
  ]
  edge [
    source 26
    target 1248
  ]
  edge [
    source 26
    target 1249
  ]
  edge [
    source 26
    target 1250
  ]
  edge [
    source 26
    target 1251
  ]
  edge [
    source 26
    target 84
  ]
  edge [
    source 26
    target 1252
  ]
  edge [
    source 26
    target 1253
  ]
  edge [
    source 26
    target 1254
  ]
  edge [
    source 26
    target 1255
  ]
  edge [
    source 26
    target 1256
  ]
  edge [
    source 26
    target 1257
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 26
    target 1258
  ]
  edge [
    source 26
    target 1259
  ]
  edge [
    source 26
    target 1260
  ]
  edge [
    source 26
    target 1261
  ]
  edge [
    source 26
    target 1262
  ]
  edge [
    source 26
    target 1263
  ]
  edge [
    source 26
    target 1264
  ]
  edge [
    source 26
    target 1265
  ]
  edge [
    source 26
    target 494
  ]
  edge [
    source 26
    target 1266
  ]
  edge [
    source 26
    target 1267
  ]
  edge [
    source 26
    target 1268
  ]
  edge [
    source 26
    target 1269
  ]
  edge [
    source 26
    target 1270
  ]
  edge [
    source 26
    target 1271
  ]
  edge [
    source 26
    target 1272
  ]
  edge [
    source 26
    target 1273
  ]
  edge [
    source 26
    target 1274
  ]
  edge [
    source 26
    target 1275
  ]
  edge [
    source 26
    target 1276
  ]
  edge [
    source 26
    target 1277
  ]
  edge [
    source 26
    target 1278
  ]
  edge [
    source 26
    target 1279
  ]
  edge [
    source 26
    target 1280
  ]
  edge [
    source 26
    target 496
  ]
  edge [
    source 26
    target 1281
  ]
  edge [
    source 26
    target 1282
  ]
  edge [
    source 26
    target 1283
  ]
  edge [
    source 26
    target 1038
  ]
  edge [
    source 26
    target 1284
  ]
  edge [
    source 26
    target 1285
  ]
  edge [
    source 26
    target 1286
  ]
  edge [
    source 26
    target 1287
  ]
  edge [
    source 26
    target 1288
  ]
  edge [
    source 26
    target 1289
  ]
  edge [
    source 26
    target 1290
  ]
  edge [
    source 26
    target 1291
  ]
  edge [
    source 26
    target 1010
  ]
  edge [
    source 26
    target 1292
  ]
  edge [
    source 26
    target 1293
  ]
  edge [
    source 26
    target 1294
  ]
  edge [
    source 26
    target 1295
  ]
  edge [
    source 26
    target 1296
  ]
  edge [
    source 26
    target 1297
  ]
  edge [
    source 26
    target 1298
  ]
  edge [
    source 26
    target 1299
  ]
  edge [
    source 26
    target 94
  ]
  edge [
    source 26
    target 1300
  ]
  edge [
    source 26
    target 1301
  ]
  edge [
    source 26
    target 1302
  ]
  edge [
    source 26
    target 1303
  ]
  edge [
    source 26
    target 1304
  ]
  edge [
    source 26
    target 1305
  ]
  edge [
    source 26
    target 1306
  ]
  edge [
    source 26
    target 1307
  ]
  edge [
    source 26
    target 1308
  ]
  edge [
    source 26
    target 1309
  ]
  edge [
    source 26
    target 1310
  ]
  edge [
    source 26
    target 1311
  ]
  edge [
    source 26
    target 1312
  ]
  edge [
    source 26
    target 1313
  ]
  edge [
    source 26
    target 1314
  ]
  edge [
    source 26
    target 1315
  ]
  edge [
    source 26
    target 1316
  ]
  edge [
    source 26
    target 1317
  ]
  edge [
    source 26
    target 1318
  ]
  edge [
    source 26
    target 1319
  ]
  edge [
    source 26
    target 1320
  ]
  edge [
    source 26
    target 1321
  ]
  edge [
    source 26
    target 1322
  ]
  edge [
    source 26
    target 1323
  ]
  edge [
    source 26
    target 1324
  ]
  edge [
    source 26
    target 1325
  ]
  edge [
    source 26
    target 1326
  ]
  edge [
    source 26
    target 1327
  ]
  edge [
    source 26
    target 1328
  ]
  edge [
    source 26
    target 1329
  ]
  edge [
    source 26
    target 1330
  ]
  edge [
    source 26
    target 1331
  ]
  edge [
    source 26
    target 1332
  ]
  edge [
    source 26
    target 1333
  ]
  edge [
    source 26
    target 1334
  ]
  edge [
    source 26
    target 1335
  ]
  edge [
    source 26
    target 1336
  ]
  edge [
    source 26
    target 1337
  ]
  edge [
    source 26
    target 1338
  ]
  edge [
    source 26
    target 1339
  ]
  edge [
    source 26
    target 1340
  ]
  edge [
    source 26
    target 1341
  ]
  edge [
    source 26
    target 1342
  ]
  edge [
    source 26
    target 1343
  ]
  edge [
    source 26
    target 1344
  ]
  edge [
    source 26
    target 1345
  ]
  edge [
    source 26
    target 1346
  ]
  edge [
    source 26
    target 1347
  ]
  edge [
    source 26
    target 1348
  ]
  edge [
    source 26
    target 1349
  ]
  edge [
    source 26
    target 1118
  ]
  edge [
    source 26
    target 1350
  ]
  edge [
    source 26
    target 1351
  ]
  edge [
    source 26
    target 1352
  ]
  edge [
    source 26
    target 1353
  ]
  edge [
    source 26
    target 1354
  ]
  edge [
    source 26
    target 1355
  ]
  edge [
    source 26
    target 1356
  ]
  edge [
    source 26
    target 1357
  ]
  edge [
    source 26
    target 1358
  ]
  edge [
    source 26
    target 1359
  ]
  edge [
    source 26
    target 1360
  ]
  edge [
    source 26
    target 1361
  ]
  edge [
    source 26
    target 1362
  ]
  edge [
    source 26
    target 1363
  ]
  edge [
    source 26
    target 1364
  ]
  edge [
    source 26
    target 1365
  ]
  edge [
    source 26
    target 1366
  ]
  edge [
    source 26
    target 1367
  ]
  edge [
    source 26
    target 1368
  ]
  edge [
    source 26
    target 1369
  ]
  edge [
    source 26
    target 1370
  ]
  edge [
    source 26
    target 1371
  ]
  edge [
    source 26
    target 1372
  ]
  edge [
    source 26
    target 975
  ]
  edge [
    source 26
    target 1373
  ]
  edge [
    source 26
    target 1374
  ]
  edge [
    source 26
    target 1375
  ]
  edge [
    source 26
    target 1376
  ]
  edge [
    source 26
    target 1377
  ]
  edge [
    source 26
    target 1378
  ]
  edge [
    source 26
    target 1379
  ]
  edge [
    source 26
    target 1380
  ]
  edge [
    source 26
    target 1381
  ]
  edge [
    source 26
    target 1382
  ]
  edge [
    source 26
    target 1383
  ]
  edge [
    source 26
    target 1384
  ]
  edge [
    source 26
    target 1385
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1386
  ]
  edge [
    source 29
    target 1387
  ]
  edge [
    source 29
    target 1388
  ]
  edge [
    source 29
    target 1389
  ]
  edge [
    source 29
    target 1390
  ]
  edge [
    source 29
    target 1391
  ]
  edge [
    source 29
    target 1392
  ]
  edge [
    source 29
    target 1393
  ]
  edge [
    source 29
    target 1394
  ]
  edge [
    source 29
    target 461
  ]
  edge [
    source 29
    target 1128
  ]
  edge [
    source 29
    target 1395
  ]
  edge [
    source 29
    target 1396
  ]
  edge [
    source 29
    target 1397
  ]
  edge [
    source 29
    target 467
  ]
  edge [
    source 29
    target 1398
  ]
  edge [
    source 29
    target 1399
  ]
  edge [
    source 29
    target 1400
  ]
  edge [
    source 29
    target 1401
  ]
  edge [
    source 29
    target 1402
  ]
  edge [
    source 29
    target 1403
  ]
  edge [
    source 29
    target 1404
  ]
  edge [
    source 29
    target 1405
  ]
  edge [
    source 29
    target 1406
  ]
  edge [
    source 29
    target 464
  ]
  edge [
    source 29
    target 1407
  ]
  edge [
    source 29
    target 1066
  ]
  edge [
    source 29
    target 1408
  ]
  edge [
    source 29
    target 1409
  ]
  edge [
    source 29
    target 1410
  ]
  edge [
    source 29
    target 1411
  ]
  edge [
    source 29
    target 1412
  ]
  edge [
    source 29
    target 1413
  ]
  edge [
    source 29
    target 1414
  ]
  edge [
    source 29
    target 928
  ]
  edge [
    source 29
    target 1415
  ]
  edge [
    source 29
    target 1416
  ]
  edge [
    source 29
    target 1417
  ]
  edge [
    source 29
    target 1418
  ]
  edge [
    source 29
    target 1419
  ]
  edge [
    source 29
    target 917
  ]
  edge [
    source 29
    target 926
  ]
  edge [
    source 29
    target 1420
  ]
  edge [
    source 29
    target 1005
  ]
  edge [
    source 29
    target 1421
  ]
  edge [
    source 29
    target 1422
  ]
  edge [
    source 29
    target 1423
  ]
  edge [
    source 29
    target 1121
  ]
  edge [
    source 29
    target 1424
  ]
  edge [
    source 29
    target 1425
  ]
  edge [
    source 29
    target 876
  ]
  edge [
    source 29
    target 1426
  ]
  edge [
    source 29
    target 1051
  ]
  edge [
    source 29
    target 799
  ]
  edge [
    source 29
    target 1427
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 29
    target 1428
  ]
  edge [
    source 29
    target 1429
  ]
  edge [
    source 29
    target 1430
  ]
  edge [
    source 29
    target 1431
  ]
  edge [
    source 29
    target 1432
  ]
  edge [
    source 29
    target 1433
  ]
  edge [
    source 29
    target 1434
  ]
  edge [
    source 29
    target 1435
  ]
  edge [
    source 29
    target 1436
  ]
  edge [
    source 29
    target 1437
  ]
  edge [
    source 29
    target 1438
  ]
  edge [
    source 29
    target 1439
  ]
  edge [
    source 29
    target 1440
  ]
  edge [
    source 29
    target 1441
  ]
  edge [
    source 29
    target 1442
  ]
  edge [
    source 29
    target 1443
  ]
  edge [
    source 29
    target 1444
  ]
  edge [
    source 29
    target 1445
  ]
  edge [
    source 29
    target 1446
  ]
  edge [
    source 29
    target 1447
  ]
  edge [
    source 29
    target 892
  ]
  edge [
    source 30
    target 1448
  ]
  edge [
    source 30
    target 1449
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1450
  ]
  edge [
    source 31
    target 1451
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 1452
  ]
  edge [
    source 31
    target 1453
  ]
  edge [
    source 31
    target 1454
  ]
  edge [
    source 31
    target 1455
  ]
  edge [
    source 31
    target 1456
  ]
  edge [
    source 31
    target 155
  ]
  edge [
    source 31
    target 1457
  ]
  edge [
    source 31
    target 1458
  ]
  edge [
    source 31
    target 1459
  ]
  edge [
    source 31
    target 1460
  ]
  edge [
    source 31
    target 1461
  ]
  edge [
    source 31
    target 1462
  ]
  edge [
    source 31
    target 1463
  ]
  edge [
    source 31
    target 1464
  ]
  edge [
    source 31
    target 1465
  ]
  edge [
    source 31
    target 1466
  ]
  edge [
    source 31
    target 1467
  ]
  edge [
    source 31
    target 1468
  ]
  edge [
    source 31
    target 1469
  ]
  edge [
    source 31
    target 1470
  ]
  edge [
    source 31
    target 1471
  ]
  edge [
    source 31
    target 1472
  ]
  edge [
    source 31
    target 176
  ]
  edge [
    source 31
    target 1473
  ]
  edge [
    source 31
    target 1474
  ]
  edge [
    source 31
    target 1475
  ]
  edge [
    source 31
    target 1476
  ]
  edge [
    source 31
    target 1477
  ]
  edge [
    source 31
    target 1478
  ]
  edge [
    source 31
    target 1479
  ]
  edge [
    source 31
    target 1480
  ]
  edge [
    source 31
    target 89
  ]
  edge [
    source 31
    target 90
  ]
  edge [
    source 31
    target 91
  ]
  edge [
    source 31
    target 92
  ]
  edge [
    source 31
    target 93
  ]
  edge [
    source 31
    target 94
  ]
  edge [
    source 31
    target 95
  ]
  edge [
    source 31
    target 96
  ]
  edge [
    source 31
    target 97
  ]
  edge [
    source 31
    target 98
  ]
  edge [
    source 31
    target 99
  ]
  edge [
    source 31
    target 100
  ]
  edge [
    source 31
    target 101
  ]
  edge [
    source 31
    target 102
  ]
  edge [
    source 31
    target 103
  ]
  edge [
    source 31
    target 104
  ]
  edge [
    source 31
    target 105
  ]
  edge [
    source 31
    target 106
  ]
  edge [
    source 31
    target 107
  ]
  edge [
    source 31
    target 108
  ]
  edge [
    source 31
    target 109
  ]
  edge [
    source 31
    target 110
  ]
  edge [
    source 31
    target 111
  ]
  edge [
    source 31
    target 112
  ]
  edge [
    source 31
    target 113
  ]
  edge [
    source 31
    target 1481
  ]
  edge [
    source 31
    target 1482
  ]
  edge [
    source 31
    target 1483
  ]
  edge [
    source 31
    target 1484
  ]
  edge [
    source 31
    target 1485
  ]
  edge [
    source 31
    target 1486
  ]
  edge [
    source 31
    target 1487
  ]
  edge [
    source 31
    target 1488
  ]
  edge [
    source 31
    target 1489
  ]
  edge [
    source 31
    target 1490
  ]
  edge [
    source 31
    target 1262
  ]
  edge [
    source 31
    target 641
  ]
  edge [
    source 31
    target 1491
  ]
  edge [
    source 31
    target 1492
  ]
  edge [
    source 31
    target 1493
  ]
  edge [
    source 31
    target 1494
  ]
  edge [
    source 31
    target 1495
  ]
  edge [
    source 31
    target 1496
  ]
  edge [
    source 31
    target 1497
  ]
  edge [
    source 31
    target 1498
  ]
  edge [
    source 31
    target 1499
  ]
  edge [
    source 31
    target 1500
  ]
  edge [
    source 31
    target 1501
  ]
  edge [
    source 31
    target 1502
  ]
  edge [
    source 31
    target 1503
  ]
  edge [
    source 31
    target 1504
  ]
  edge [
    source 31
    target 1505
  ]
  edge [
    source 31
    target 1506
  ]
  edge [
    source 31
    target 1507
  ]
  edge [
    source 31
    target 1508
  ]
  edge [
    source 31
    target 116
  ]
  edge [
    source 31
    target 1509
  ]
  edge [
    source 31
    target 1510
  ]
  edge [
    source 31
    target 1511
  ]
  edge [
    source 31
    target 1512
  ]
  edge [
    source 31
    target 1513
  ]
  edge [
    source 31
    target 1514
  ]
  edge [
    source 31
    target 1515
  ]
  edge [
    source 31
    target 1516
  ]
  edge [
    source 31
    target 1517
  ]
  edge [
    source 31
    target 1518
  ]
  edge [
    source 31
    target 1519
  ]
  edge [
    source 31
    target 149
  ]
  edge [
    source 31
    target 1520
  ]
  edge [
    source 31
    target 1521
  ]
  edge [
    source 31
    target 1522
  ]
  edge [
    source 31
    target 1523
  ]
  edge [
    source 31
    target 1524
  ]
  edge [
    source 31
    target 1525
  ]
  edge [
    source 31
    target 1526
  ]
  edge [
    source 31
    target 1527
  ]
  edge [
    source 31
    target 158
  ]
  edge [
    source 31
    target 1528
  ]
  edge [
    source 31
    target 1529
  ]
  edge [
    source 31
    target 1530
  ]
  edge [
    source 31
    target 1531
  ]
  edge [
    source 32
    target 1532
  ]
  edge [
    source 32
    target 1533
  ]
  edge [
    source 32
    target 1534
  ]
  edge [
    source 32
    target 1535
  ]
  edge [
    source 32
    target 1536
  ]
  edge [
    source 32
    target 1537
  ]
  edge [
    source 32
    target 1538
  ]
  edge [
    source 32
    target 460
  ]
  edge [
    source 32
    target 1539
  ]
  edge [
    source 32
    target 1540
  ]
  edge [
    source 32
    target 1541
  ]
  edge [
    source 32
    target 1542
  ]
  edge [
    source 32
    target 1543
  ]
  edge [
    source 32
    target 1544
  ]
  edge [
    source 32
    target 1545
  ]
  edge [
    source 32
    target 1546
  ]
  edge [
    source 32
    target 822
  ]
  edge [
    source 32
    target 461
  ]
  edge [
    source 32
    target 1547
  ]
  edge [
    source 32
    target 1548
  ]
  edge [
    source 32
    target 1549
  ]
  edge [
    source 32
    target 1550
  ]
  edge [
    source 32
    target 939
  ]
  edge [
    source 32
    target 940
  ]
  edge [
    source 32
    target 941
  ]
  edge [
    source 32
    target 942
  ]
  edge [
    source 32
    target 858
  ]
  edge [
    source 32
    target 770
  ]
  edge [
    source 32
    target 943
  ]
  edge [
    source 32
    target 944
  ]
  edge [
    source 32
    target 1551
  ]
  edge [
    source 32
    target 805
  ]
  edge [
    source 32
    target 1552
  ]
  edge [
    source 32
    target 1553
  ]
  edge [
    source 32
    target 1554
  ]
  edge [
    source 32
    target 1555
  ]
  edge [
    source 32
    target 1556
  ]
  edge [
    source 32
    target 1557
  ]
  edge [
    source 32
    target 1558
  ]
  edge [
    source 32
    target 1559
  ]
  edge [
    source 32
    target 457
  ]
  edge [
    source 32
    target 1560
  ]
  edge [
    source 32
    target 1561
  ]
  edge [
    source 32
    target 819
  ]
  edge [
    source 32
    target 1562
  ]
  edge [
    source 32
    target 1563
  ]
  edge [
    source 32
    target 1564
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1565
  ]
  edge [
    source 32
    target 1566
  ]
  edge [
    source 32
    target 1567
  ]
  edge [
    source 32
    target 1568
  ]
  edge [
    source 32
    target 1071
  ]
  edge [
    source 32
    target 1569
  ]
  edge [
    source 32
    target 1570
  ]
  edge [
    source 32
    target 1571
  ]
  edge [
    source 32
    target 1391
  ]
  edge [
    source 32
    target 1572
  ]
  edge [
    source 32
    target 1573
  ]
  edge [
    source 32
    target 1574
  ]
  edge [
    source 32
    target 1575
  ]
  edge [
    source 32
    target 1576
  ]
  edge [
    source 32
    target 1577
  ]
  edge [
    source 32
    target 1578
  ]
  edge [
    source 32
    target 945
  ]
  edge [
    source 32
    target 946
  ]
  edge [
    source 32
    target 947
  ]
  edge [
    source 32
    target 948
  ]
  edge [
    source 32
    target 949
  ]
  edge [
    source 32
    target 950
  ]
  edge [
    source 32
    target 951
  ]
  edge [
    source 32
    target 952
  ]
  edge [
    source 32
    target 953
  ]
  edge [
    source 32
    target 954
  ]
  edge [
    source 32
    target 832
  ]
  edge [
    source 32
    target 955
  ]
  edge [
    source 32
    target 1579
  ]
  edge [
    source 32
    target 1580
  ]
  edge [
    source 32
    target 922
  ]
  edge [
    source 32
    target 1581
  ]
  edge [
    source 32
    target 907
  ]
  edge [
    source 32
    target 1582
  ]
  edge [
    source 32
    target 1127
  ]
  edge [
    source 32
    target 1583
  ]
  edge [
    source 32
    target 1584
  ]
  edge [
    source 32
    target 1585
  ]
  edge [
    source 32
    target 1586
  ]
  edge [
    source 32
    target 1402
  ]
  edge [
    source 32
    target 1587
  ]
  edge [
    source 32
    target 1588
  ]
  edge [
    source 32
    target 1589
  ]
  edge [
    source 32
    target 1590
  ]
  edge [
    source 32
    target 1591
  ]
  edge [
    source 32
    target 1592
  ]
  edge [
    source 32
    target 1593
  ]
  edge [
    source 32
    target 1594
  ]
  edge [
    source 32
    target 1595
  ]
  edge [
    source 32
    target 1596
  ]
  edge [
    source 33
    target 1597
  ]
  edge [
    source 33
    target 1598
  ]
  edge [
    source 33
    target 1599
  ]
  edge [
    source 33
    target 1159
  ]
  edge [
    source 33
    target 1600
  ]
  edge [
    source 33
    target 1601
  ]
  edge [
    source 33
    target 1602
  ]
  edge [
    source 33
    target 1603
  ]
  edge [
    source 33
    target 1604
  ]
  edge [
    source 33
    target 1605
  ]
  edge [
    source 33
    target 1606
  ]
  edge [
    source 33
    target 1607
  ]
  edge [
    source 33
    target 1608
  ]
  edge [
    source 33
    target 1609
  ]
  edge [
    source 33
    target 1610
  ]
  edge [
    source 33
    target 1611
  ]
  edge [
    source 33
    target 1612
  ]
  edge [
    source 33
    target 1347
  ]
  edge [
    source 33
    target 1613
  ]
  edge [
    source 33
    target 1614
  ]
  edge [
    source 33
    target 1615
  ]
  edge [
    source 33
    target 1616
  ]
  edge [
    source 33
    target 271
  ]
  edge [
    source 33
    target 1023
  ]
  edge [
    source 33
    target 1617
  ]
  edge [
    source 33
    target 1618
  ]
  edge [
    source 33
    target 1619
  ]
  edge [
    source 33
    target 1620
  ]
  edge [
    source 33
    target 1621
  ]
  edge [
    source 33
    target 1622
  ]
  edge [
    source 33
    target 1623
  ]
  edge [
    source 33
    target 1624
  ]
  edge [
    source 33
    target 1625
  ]
  edge [
    source 33
    target 1626
  ]
  edge [
    source 33
    target 1627
  ]
  edge [
    source 33
    target 399
  ]
  edge [
    source 33
    target 1628
  ]
  edge [
    source 33
    target 1629
  ]
  edge [
    source 33
    target 1630
  ]
  edge [
    source 33
    target 1631
  ]
  edge [
    source 33
    target 1632
  ]
  edge [
    source 33
    target 1633
  ]
  edge [
    source 33
    target 1634
  ]
  edge [
    source 33
    target 1635
  ]
  edge [
    source 33
    target 1636
  ]
  edge [
    source 33
    target 1637
  ]
  edge [
    source 33
    target 1638
  ]
  edge [
    source 33
    target 1639
  ]
  edge [
    source 33
    target 1640
  ]
  edge [
    source 33
    target 1641
  ]
  edge [
    source 33
    target 103
  ]
  edge [
    source 33
    target 1642
  ]
  edge [
    source 33
    target 1381
  ]
  edge [
    source 33
    target 1643
  ]
  edge [
    source 33
    target 1644
  ]
  edge [
    source 33
    target 1645
  ]
  edge [
    source 33
    target 1646
  ]
  edge [
    source 33
    target 1647
  ]
  edge [
    source 33
    target 1648
  ]
  edge [
    source 33
    target 1649
  ]
  edge [
    source 33
    target 1650
  ]
  edge [
    source 33
    target 1651
  ]
  edge [
    source 33
    target 1652
  ]
  edge [
    source 33
    target 1653
  ]
  edge [
    source 33
    target 1654
  ]
  edge [
    source 33
    target 1655
  ]
  edge [
    source 33
    target 1656
  ]
  edge [
    source 33
    target 1657
  ]
  edge [
    source 33
    target 1658
  ]
  edge [
    source 33
    target 1163
  ]
  edge [
    source 33
    target 1261
  ]
  edge [
    source 33
    target 1659
  ]
  edge [
    source 33
    target 90
  ]
  edge [
    source 33
    target 1660
  ]
  edge [
    source 33
    target 92
  ]
  edge [
    source 33
    target 1661
  ]
  edge [
    source 33
    target 1662
  ]
  edge [
    source 33
    target 1119
  ]
  edge [
    source 33
    target 1115
  ]
  edge [
    source 33
    target 1663
  ]
  edge [
    source 33
    target 1169
  ]
  edge [
    source 33
    target 1664
  ]
  edge [
    source 33
    target 1665
  ]
  edge [
    source 33
    target 1357
  ]
  edge [
    source 33
    target 1666
  ]
  edge [
    source 33
    target 1667
  ]
  edge [
    source 33
    target 1120
  ]
  edge [
    source 33
    target 1668
  ]
  edge [
    source 33
    target 1669
  ]
  edge [
    source 33
    target 1670
  ]
  edge [
    source 33
    target 1164
  ]
  edge [
    source 33
    target 1177
  ]
  edge [
    source 33
    target 1671
  ]
  edge [
    source 33
    target 1168
  ]
  edge [
    source 33
    target 1672
  ]
  edge [
    source 33
    target 1673
  ]
  edge [
    source 33
    target 1674
  ]
  edge [
    source 33
    target 1675
  ]
  edge [
    source 33
    target 1676
  ]
  edge [
    source 33
    target 1677
  ]
  edge [
    source 33
    target 1678
  ]
  edge [
    source 33
    target 1679
  ]
  edge [
    source 33
    target 1680
  ]
  edge [
    source 33
    target 1681
  ]
  edge [
    source 33
    target 1682
  ]
  edge [
    source 33
    target 1683
  ]
  edge [
    source 33
    target 1684
  ]
  edge [
    source 33
    target 1685
  ]
  edge [
    source 33
    target 1686
  ]
  edge [
    source 33
    target 1687
  ]
  edge [
    source 33
    target 1382
  ]
  edge [
    source 33
    target 1688
  ]
  edge [
    source 33
    target 1689
  ]
  edge [
    source 33
    target 344
  ]
  edge [
    source 33
    target 1690
  ]
  edge [
    source 33
    target 1691
  ]
  edge [
    source 33
    target 1692
  ]
  edge [
    source 33
    target 1693
  ]
  edge [
    source 33
    target 1694
  ]
  edge [
    source 33
    target 1695
  ]
  edge [
    source 33
    target 1696
  ]
  edge [
    source 33
    target 1697
  ]
  edge [
    source 33
    target 1698
  ]
  edge [
    source 33
    target 1699
  ]
  edge [
    source 33
    target 1700
  ]
  edge [
    source 33
    target 1701
  ]
  edge [
    source 33
    target 1702
  ]
  edge [
    source 33
    target 1703
  ]
  edge [
    source 33
    target 1704
  ]
  edge [
    source 33
    target 1705
  ]
  edge [
    source 33
    target 1706
  ]
  edge [
    source 33
    target 1707
  ]
  edge [
    source 33
    target 1708
  ]
  edge [
    source 33
    target 1709
  ]
  edge [
    source 33
    target 1710
  ]
  edge [
    source 33
    target 1711
  ]
  edge [
    source 33
    target 1712
  ]
  edge [
    source 33
    target 1713
  ]
  edge [
    source 33
    target 1714
  ]
  edge [
    source 33
    target 1715
  ]
  edge [
    source 33
    target 1716
  ]
  edge [
    source 33
    target 1717
  ]
  edge [
    source 33
    target 1718
  ]
  edge [
    source 33
    target 1189
  ]
  edge [
    source 33
    target 1719
  ]
  edge [
    source 33
    target 1720
  ]
  edge [
    source 33
    target 1721
  ]
  edge [
    source 33
    target 124
  ]
  edge [
    source 33
    target 1722
  ]
  edge [
    source 33
    target 1723
  ]
  edge [
    source 33
    target 1724
  ]
  edge [
    source 33
    target 1725
  ]
  edge [
    source 33
    target 554
  ]
  edge [
    source 33
    target 1726
  ]
  edge [
    source 33
    target 1727
  ]
  edge [
    source 33
    target 1728
  ]
  edge [
    source 33
    target 1729
  ]
  edge [
    source 33
    target 1730
  ]
  edge [
    source 33
    target 1731
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 1732
  ]
  edge [
    source 33
    target 1733
  ]
  edge [
    source 33
    target 1734
  ]
  edge [
    source 33
    target 1735
  ]
  edge [
    source 33
    target 1736
  ]
  edge [
    source 33
    target 1737
  ]
  edge [
    source 33
    target 1738
  ]
  edge [
    source 33
    target 1739
  ]
  edge [
    source 33
    target 1740
  ]
  edge [
    source 33
    target 1741
  ]
  edge [
    source 33
    target 1742
  ]
  edge [
    source 33
    target 1743
  ]
  edge [
    source 33
    target 1744
  ]
  edge [
    source 33
    target 1745
  ]
  edge [
    source 33
    target 1746
  ]
  edge [
    source 33
    target 1747
  ]
  edge [
    source 33
    target 1748
  ]
  edge [
    source 33
    target 1749
  ]
  edge [
    source 33
    target 1750
  ]
  edge [
    source 33
    target 1751
  ]
  edge [
    source 33
    target 1752
  ]
  edge [
    source 33
    target 1753
  ]
  edge [
    source 33
    target 1754
  ]
  edge [
    source 33
    target 1755
  ]
  edge [
    source 33
    target 1756
  ]
  edge [
    source 33
    target 1757
  ]
  edge [
    source 33
    target 1758
  ]
  edge [
    source 33
    target 1759
  ]
  edge [
    source 33
    target 1760
  ]
  edge [
    source 33
    target 1761
  ]
  edge [
    source 33
    target 1762
  ]
  edge [
    source 33
    target 1763
  ]
  edge [
    source 33
    target 1764
  ]
  edge [
    source 33
    target 1765
  ]
  edge [
    source 33
    target 1766
  ]
  edge [
    source 33
    target 1767
  ]
  edge [
    source 33
    target 1768
  ]
  edge [
    source 33
    target 1769
  ]
  edge [
    source 33
    target 1770
  ]
  edge [
    source 33
    target 1771
  ]
  edge [
    source 33
    target 1772
  ]
  edge [
    source 33
    target 1773
  ]
  edge [
    source 33
    target 1774
  ]
  edge [
    source 33
    target 1775
  ]
  edge [
    source 33
    target 1232
  ]
  edge [
    source 33
    target 1311
  ]
  edge [
    source 33
    target 1776
  ]
  edge [
    source 33
    target 1777
  ]
  edge [
    source 33
    target 1778
  ]
  edge [
    source 33
    target 1779
  ]
  edge [
    source 33
    target 1780
  ]
  edge [
    source 33
    target 1026
  ]
  edge [
    source 33
    target 1057
  ]
  edge [
    source 33
    target 1781
  ]
  edge [
    source 33
    target 1055
  ]
  edge [
    source 33
    target 1782
  ]
  edge [
    source 33
    target 1783
  ]
  edge [
    source 33
    target 1565
  ]
  edge [
    source 33
    target 1784
  ]
  edge [
    source 33
    target 1785
  ]
  edge [
    source 33
    target 840
  ]
  edge [
    source 33
    target 1411
  ]
  edge [
    source 33
    target 1786
  ]
  edge [
    source 33
    target 1787
  ]
  edge [
    source 33
    target 1788
  ]
  edge [
    source 33
    target 1789
  ]
  edge [
    source 33
    target 1790
  ]
  edge [
    source 33
    target 1791
  ]
  edge [
    source 33
    target 1792
  ]
  edge [
    source 33
    target 1190
  ]
  edge [
    source 33
    target 1793
  ]
  edge [
    source 33
    target 1794
  ]
  edge [
    source 33
    target 1795
  ]
  edge [
    source 33
    target 1796
  ]
  edge [
    source 33
    target 1797
  ]
  edge [
    source 33
    target 1798
  ]
  edge [
    source 33
    target 1799
  ]
  edge [
    source 33
    target 1800
  ]
  edge [
    source 33
    target 1801
  ]
  edge [
    source 33
    target 1802
  ]
  edge [
    source 33
    target 1803
  ]
  edge [
    source 33
    target 1804
  ]
  edge [
    source 33
    target 1805
  ]
  edge [
    source 33
    target 1806
  ]
  edge [
    source 33
    target 1807
  ]
  edge [
    source 33
    target 1243
  ]
  edge [
    source 33
    target 1808
  ]
  edge [
    source 33
    target 1809
  ]
  edge [
    source 33
    target 1810
  ]
  edge [
    source 33
    target 606
  ]
  edge [
    source 33
    target 1811
  ]
  edge [
    source 33
    target 1812
  ]
  edge [
    source 33
    target 1813
  ]
  edge [
    source 33
    target 1814
  ]
  edge [
    source 33
    target 1815
  ]
  edge [
    source 33
    target 1816
  ]
  edge [
    source 33
    target 1817
  ]
  edge [
    source 33
    target 1818
  ]
  edge [
    source 33
    target 1819
  ]
  edge [
    source 33
    target 1820
  ]
  edge [
    source 33
    target 1821
  ]
  edge [
    source 33
    target 1822
  ]
  edge [
    source 33
    target 1823
  ]
  edge [
    source 33
    target 1824
  ]
  edge [
    source 33
    target 1825
  ]
  edge [
    source 33
    target 1826
  ]
  edge [
    source 33
    target 1321
  ]
  edge [
    source 33
    target 409
  ]
  edge [
    source 33
    target 1827
  ]
  edge [
    source 33
    target 1828
  ]
  edge [
    source 33
    target 1829
  ]
  edge [
    source 33
    target 1830
  ]
  edge [
    source 33
    target 1831
  ]
  edge [
    source 33
    target 1832
  ]
  edge [
    source 33
    target 1833
  ]
  edge [
    source 33
    target 1834
  ]
  edge [
    source 33
    target 1835
  ]
  edge [
    source 33
    target 1181
  ]
  edge [
    source 33
    target 681
  ]
  edge [
    source 33
    target 1836
  ]
  edge [
    source 33
    target 1837
  ]
  edge [
    source 33
    target 1838
  ]
  edge [
    source 33
    target 1839
  ]
  edge [
    source 33
    target 1840
  ]
  edge [
    source 33
    target 1841
  ]
  edge [
    source 33
    target 1842
  ]
  edge [
    source 33
    target 1843
  ]
  edge [
    source 33
    target 1844
  ]
  edge [
    source 33
    target 1845
  ]
  edge [
    source 33
    target 1846
  ]
  edge [
    source 33
    target 1847
  ]
  edge [
    source 33
    target 1848
  ]
  edge [
    source 33
    target 1849
  ]
  edge [
    source 33
    target 1850
  ]
  edge [
    source 33
    target 1851
  ]
  edge [
    source 33
    target 1852
  ]
  edge [
    source 33
    target 1853
  ]
  edge [
    source 33
    target 1854
  ]
  edge [
    source 33
    target 1855
  ]
  edge [
    source 33
    target 1856
  ]
  edge [
    source 33
    target 1857
  ]
  edge [
    source 33
    target 1858
  ]
  edge [
    source 33
    target 1859
  ]
  edge [
    source 33
    target 1860
  ]
  edge [
    source 33
    target 658
  ]
  edge [
    source 33
    target 1861
  ]
  edge [
    source 33
    target 1862
  ]
  edge [
    source 33
    target 1863
  ]
  edge [
    source 33
    target 1864
  ]
  edge [
    source 33
    target 1865
  ]
  edge [
    source 33
    target 1341
  ]
  edge [
    source 33
    target 1866
  ]
  edge [
    source 33
    target 1867
  ]
  edge [
    source 33
    target 1868
  ]
  edge [
    source 33
    target 1869
  ]
  edge [
    source 33
    target 1870
  ]
  edge [
    source 33
    target 1871
  ]
  edge [
    source 33
    target 1872
  ]
  edge [
    source 33
    target 1873
  ]
  edge [
    source 33
    target 1874
  ]
  edge [
    source 33
    target 1875
  ]
  edge [
    source 33
    target 1876
  ]
  edge [
    source 33
    target 669
  ]
  edge [
    source 33
    target 1877
  ]
  edge [
    source 33
    target 1878
  ]
  edge [
    source 33
    target 1879
  ]
  edge [
    source 33
    target 1880
  ]
  edge [
    source 33
    target 1881
  ]
  edge [
    source 33
    target 1882
  ]
  edge [
    source 33
    target 1883
  ]
  edge [
    source 33
    target 1884
  ]
  edge [
    source 33
    target 1885
  ]
  edge [
    source 33
    target 1886
  ]
  edge [
    source 33
    target 1887
  ]
  edge [
    source 33
    target 1888
  ]
  edge [
    source 33
    target 1889
  ]
  edge [
    source 33
    target 1890
  ]
  edge [
    source 33
    target 1891
  ]
  edge [
    source 33
    target 1892
  ]
  edge [
    source 33
    target 1893
  ]
  edge [
    source 33
    target 1894
  ]
  edge [
    source 33
    target 1895
  ]
  edge [
    source 33
    target 1896
  ]
  edge [
    source 33
    target 1897
  ]
  edge [
    source 33
    target 1898
  ]
  edge [
    source 33
    target 1899
  ]
  edge [
    source 33
    target 1900
  ]
  edge [
    source 33
    target 1901
  ]
  edge [
    source 33
    target 1902
  ]
  edge [
    source 33
    target 1903
  ]
  edge [
    source 33
    target 1904
  ]
  edge [
    source 33
    target 1905
  ]
  edge [
    source 33
    target 1906
  ]
  edge [
    source 33
    target 1907
  ]
  edge [
    source 33
    target 1908
  ]
  edge [
    source 33
    target 1909
  ]
  edge [
    source 33
    target 952
  ]
  edge [
    source 33
    target 1910
  ]
  edge [
    source 33
    target 1911
  ]
  edge [
    source 33
    target 1912
  ]
  edge [
    source 33
    target 1152
  ]
  edge [
    source 33
    target 1913
  ]
  edge [
    source 33
    target 1914
  ]
  edge [
    source 33
    target 1915
  ]
  edge [
    source 33
    target 1916
  ]
  edge [
    source 33
    target 1917
  ]
  edge [
    source 33
    target 1918
  ]
  edge [
    source 33
    target 1919
  ]
  edge [
    source 33
    target 1920
  ]
  edge [
    source 33
    target 1921
  ]
  edge [
    source 33
    target 1922
  ]
  edge [
    source 33
    target 1923
  ]
  edge [
    source 33
    target 1924
  ]
  edge [
    source 33
    target 1925
  ]
  edge [
    source 33
    target 1926
  ]
  edge [
    source 33
    target 70
  ]
  edge [
    source 33
    target 1927
  ]
  edge [
    source 33
    target 1928
  ]
  edge [
    source 33
    target 1929
  ]
  edge [
    source 33
    target 1930
  ]
  edge [
    source 33
    target 1931
  ]
  edge [
    source 33
    target 1932
  ]
  edge [
    source 33
    target 1174
  ]
  edge [
    source 33
    target 1933
  ]
  edge [
    source 33
    target 1934
  ]
  edge [
    source 33
    target 1935
  ]
  edge [
    source 33
    target 1153
  ]
  edge [
    source 33
    target 1936
  ]
  edge [
    source 33
    target 1937
  ]
  edge [
    source 33
    target 1938
  ]
  edge [
    source 33
    target 977
  ]
  edge [
    source 33
    target 1340
  ]
  edge [
    source 33
    target 1939
  ]
  edge [
    source 33
    target 1940
  ]
  edge [
    source 33
    target 1941
  ]
  edge [
    source 33
    target 1942
  ]
  edge [
    source 33
    target 1943
  ]
  edge [
    source 33
    target 1944
  ]
  edge [
    source 33
    target 1945
  ]
  edge [
    source 33
    target 1946
  ]
  edge [
    source 33
    target 1947
  ]
  edge [
    source 33
    target 1948
  ]
  edge [
    source 33
    target 227
  ]
  edge [
    source 33
    target 1949
  ]
  edge [
    source 33
    target 1950
  ]
  edge [
    source 33
    target 367
  ]
  edge [
    source 33
    target 1951
  ]
  edge [
    source 33
    target 1952
  ]
  edge [
    source 33
    target 1953
  ]
]
