graph [
  node [
    id 0
    label "rekord"
    origin "text"
  ]
  node [
    id 1
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 2
    label "ameryka"
    origin "text"
  ]
  node [
    id 3
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 4
    label "lekkoatletyka"
    origin "text"
  ]
  node [
    id 5
    label "dane"
  ]
  node [
    id 6
    label "szczyt"
  ]
  node [
    id 7
    label "record"
  ]
  node [
    id 8
    label "baza_danych"
  ]
  node [
    id 9
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 10
    label "edytowa&#263;"
  ]
  node [
    id 11
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 12
    label "spakowanie"
  ]
  node [
    id 13
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 14
    label "pakowa&#263;"
  ]
  node [
    id 15
    label "korelator"
  ]
  node [
    id 16
    label "wyci&#261;ganie"
  ]
  node [
    id 17
    label "pakowanie"
  ]
  node [
    id 18
    label "sekwencjonowa&#263;"
  ]
  node [
    id 19
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 20
    label "jednostka_informacji"
  ]
  node [
    id 21
    label "zbi&#243;r"
  ]
  node [
    id 22
    label "evidence"
  ]
  node [
    id 23
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 24
    label "rozpakowywanie"
  ]
  node [
    id 25
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 26
    label "rozpakowanie"
  ]
  node [
    id 27
    label "informacja"
  ]
  node [
    id 28
    label "rozpakowywa&#263;"
  ]
  node [
    id 29
    label "konwersja"
  ]
  node [
    id 30
    label "nap&#322;ywanie"
  ]
  node [
    id 31
    label "rozpakowa&#263;"
  ]
  node [
    id 32
    label "spakowa&#263;"
  ]
  node [
    id 33
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 34
    label "edytowanie"
  ]
  node [
    id 35
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 36
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 37
    label "sekwencjonowanie"
  ]
  node [
    id 38
    label "uzyskanie"
  ]
  node [
    id 39
    label "dochrapanie_si&#281;"
  ]
  node [
    id 40
    label "skill"
  ]
  node [
    id 41
    label "accomplishment"
  ]
  node [
    id 42
    label "zdarzenie_si&#281;"
  ]
  node [
    id 43
    label "sukces"
  ]
  node [
    id 44
    label "zaawansowanie"
  ]
  node [
    id 45
    label "dotarcie"
  ]
  node [
    id 46
    label "act"
  ]
  node [
    id 47
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 48
    label "zwie&#324;czenie"
  ]
  node [
    id 49
    label "Wielka_Racza"
  ]
  node [
    id 50
    label "koniec"
  ]
  node [
    id 51
    label "&#346;winica"
  ]
  node [
    id 52
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 53
    label "Che&#322;miec"
  ]
  node [
    id 54
    label "wierzcho&#322;"
  ]
  node [
    id 55
    label "wierzcho&#322;ek"
  ]
  node [
    id 56
    label "Radunia"
  ]
  node [
    id 57
    label "Barania_G&#243;ra"
  ]
  node [
    id 58
    label "Groniczki"
  ]
  node [
    id 59
    label "wierch"
  ]
  node [
    id 60
    label "konferencja"
  ]
  node [
    id 61
    label "Czupel"
  ]
  node [
    id 62
    label "&#347;ciana"
  ]
  node [
    id 63
    label "Jaworz"
  ]
  node [
    id 64
    label "Okr&#261;glica"
  ]
  node [
    id 65
    label "Walig&#243;ra"
  ]
  node [
    id 66
    label "bok"
  ]
  node [
    id 67
    label "Wielka_Sowa"
  ]
  node [
    id 68
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 69
    label "&#321;omnica"
  ]
  node [
    id 70
    label "wzniesienie"
  ]
  node [
    id 71
    label "Beskid"
  ]
  node [
    id 72
    label "fasada"
  ]
  node [
    id 73
    label "Wo&#322;ek"
  ]
  node [
    id 74
    label "summit"
  ]
  node [
    id 75
    label "Rysianka"
  ]
  node [
    id 76
    label "Mody&#324;"
  ]
  node [
    id 77
    label "poziom"
  ]
  node [
    id 78
    label "wzmo&#380;enie"
  ]
  node [
    id 79
    label "czas"
  ]
  node [
    id 80
    label "Obidowa"
  ]
  node [
    id 81
    label "Jaworzyna"
  ]
  node [
    id 82
    label "godzina_szczytu"
  ]
  node [
    id 83
    label "Turbacz"
  ]
  node [
    id 84
    label "Rudawiec"
  ]
  node [
    id 85
    label "g&#243;ra"
  ]
  node [
    id 86
    label "Ja&#322;owiec"
  ]
  node [
    id 87
    label "Wielki_Chocz"
  ]
  node [
    id 88
    label "Orlica"
  ]
  node [
    id 89
    label "Szrenica"
  ]
  node [
    id 90
    label "&#346;nie&#380;nik"
  ]
  node [
    id 91
    label "Cubryna"
  ]
  node [
    id 92
    label "Wielki_Bukowiec"
  ]
  node [
    id 93
    label "Magura"
  ]
  node [
    id 94
    label "korona"
  ]
  node [
    id 95
    label "Czarna_G&#243;ra"
  ]
  node [
    id 96
    label "Lubogoszcz"
  ]
  node [
    id 97
    label "zawody"
  ]
  node [
    id 98
    label "Formu&#322;a_1"
  ]
  node [
    id 99
    label "championship"
  ]
  node [
    id 100
    label "impreza"
  ]
  node [
    id 101
    label "contest"
  ]
  node [
    id 102
    label "walczy&#263;"
  ]
  node [
    id 103
    label "champion"
  ]
  node [
    id 104
    label "rywalizacja"
  ]
  node [
    id 105
    label "walczenie"
  ]
  node [
    id 106
    label "tysi&#281;cznik"
  ]
  node [
    id 107
    label "spadochroniarstwo"
  ]
  node [
    id 108
    label "kategoria_open"
  ]
  node [
    id 109
    label "gor&#261;cy"
  ]
  node [
    id 110
    label "s&#322;oneczny"
  ]
  node [
    id 111
    label "po&#322;udniowo"
  ]
  node [
    id 112
    label "charakterystycznie"
  ]
  node [
    id 113
    label "stresogenny"
  ]
  node [
    id 114
    label "szczery"
  ]
  node [
    id 115
    label "rozpalenie_si&#281;"
  ]
  node [
    id 116
    label "zdecydowany"
  ]
  node [
    id 117
    label "sensacyjny"
  ]
  node [
    id 118
    label "na_gor&#261;co"
  ]
  node [
    id 119
    label "rozpalanie_si&#281;"
  ]
  node [
    id 120
    label "&#380;arki"
  ]
  node [
    id 121
    label "serdeczny"
  ]
  node [
    id 122
    label "ciep&#322;y"
  ]
  node [
    id 123
    label "g&#322;&#281;boki"
  ]
  node [
    id 124
    label "gor&#261;co"
  ]
  node [
    id 125
    label "seksowny"
  ]
  node [
    id 126
    label "&#347;wie&#380;y"
  ]
  node [
    id 127
    label "s&#322;onecznie"
  ]
  node [
    id 128
    label "letni"
  ]
  node [
    id 129
    label "weso&#322;y"
  ]
  node [
    id 130
    label "bezdeszczowy"
  ]
  node [
    id 131
    label "bezchmurny"
  ]
  node [
    id 132
    label "pogodny"
  ]
  node [
    id 133
    label "fotowoltaiczny"
  ]
  node [
    id 134
    label "jasny"
  ]
  node [
    id 135
    label "rzut_oszczepem"
  ]
  node [
    id 136
    label "rzut_m&#322;otem"
  ]
  node [
    id 137
    label "bieg"
  ]
  node [
    id 138
    label "tr&#243;jskok"
  ]
  node [
    id 139
    label "skok_o_tyczce"
  ]
  node [
    id 140
    label "sport"
  ]
  node [
    id 141
    label "ch&#243;d"
  ]
  node [
    id 142
    label "skok_w_dal"
  ]
  node [
    id 143
    label "dziesi&#281;ciob&#243;j"
  ]
  node [
    id 144
    label "pchni&#281;cie_kul&#261;"
  ]
  node [
    id 145
    label "skok_wzwy&#380;"
  ]
  node [
    id 146
    label "kultura_fizyczna"
  ]
  node [
    id 147
    label "zgrupowanie"
  ]
  node [
    id 148
    label "usportowienie"
  ]
  node [
    id 149
    label "atakowa&#263;"
  ]
  node [
    id 150
    label "zaatakowanie"
  ]
  node [
    id 151
    label "atakowanie"
  ]
  node [
    id 152
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 153
    label "zaatakowa&#263;"
  ]
  node [
    id 154
    label "usportowi&#263;"
  ]
  node [
    id 155
    label "sokolstwo"
  ]
  node [
    id 156
    label "step"
  ]
  node [
    id 157
    label "konkurencja"
  ]
  node [
    id 158
    label "czerwona_kartka"
  ]
  node [
    id 159
    label "krok"
  ]
  node [
    id 160
    label "wy&#347;cig"
  ]
  node [
    id 161
    label "ruch"
  ]
  node [
    id 162
    label "bystrzyca"
  ]
  node [
    id 163
    label "cycle"
  ]
  node [
    id 164
    label "parametr"
  ]
  node [
    id 165
    label "roll"
  ]
  node [
    id 166
    label "linia"
  ]
  node [
    id 167
    label "procedura"
  ]
  node [
    id 168
    label "kierunek"
  ]
  node [
    id 169
    label "proces"
  ]
  node [
    id 170
    label "d&#261;&#380;enie"
  ]
  node [
    id 171
    label "przedbieg"
  ]
  node [
    id 172
    label "pr&#261;d"
  ]
  node [
    id 173
    label "ciek_wodny"
  ]
  node [
    id 174
    label "kurs"
  ]
  node [
    id 175
    label "tryb"
  ]
  node [
    id 176
    label "syfon"
  ]
  node [
    id 177
    label "pozycja"
  ]
  node [
    id 178
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 179
    label "czo&#322;&#243;wka"
  ]
  node [
    id 180
    label "skoki"
  ]
  node [
    id 181
    label "ameryk"
  ]
  node [
    id 182
    label "mistrzostwo"
  ]
  node [
    id 183
    label "wyspa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 181
    target 183
  ]
  edge [
    source 182
    target 183
  ]
]
