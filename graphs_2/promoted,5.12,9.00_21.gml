graph [
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "kasa"
    origin "text"
  ]
  node [
    id 2
    label "kafelek"
    origin "text"
  ]
  node [
    id 3
    label "czyj&#347;"
  ]
  node [
    id 4
    label "m&#261;&#380;"
  ]
  node [
    id 5
    label "prywatny"
  ]
  node [
    id 6
    label "ma&#322;&#380;onek"
  ]
  node [
    id 7
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 8
    label "ch&#322;op"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "pan_m&#322;ody"
  ]
  node [
    id 11
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 12
    label "&#347;lubny"
  ]
  node [
    id 13
    label "pan_domu"
  ]
  node [
    id 14
    label "pan_i_w&#322;adca"
  ]
  node [
    id 15
    label "stary"
  ]
  node [
    id 16
    label "skrzynia"
  ]
  node [
    id 17
    label "pieni&#261;dze"
  ]
  node [
    id 18
    label "instytucja"
  ]
  node [
    id 19
    label "szafa"
  ]
  node [
    id 20
    label "miejsce"
  ]
  node [
    id 21
    label "urz&#261;dzenie"
  ]
  node [
    id 22
    label "case"
  ]
  node [
    id 23
    label "pojemnik"
  ]
  node [
    id 24
    label "mebel"
  ]
  node [
    id 25
    label "tapczan"
  ]
  node [
    id 26
    label "kanapa"
  ]
  node [
    id 27
    label "odzie&#380;"
  ]
  node [
    id 28
    label "drzwi"
  ]
  node [
    id 29
    label "p&#243;&#322;ka"
  ]
  node [
    id 30
    label "pantograf"
  ]
  node [
    id 31
    label "meblo&#347;cianka"
  ]
  node [
    id 32
    label "almaria"
  ]
  node [
    id 33
    label "osoba_prawna"
  ]
  node [
    id 34
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 35
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 36
    label "poj&#281;cie"
  ]
  node [
    id 37
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 38
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 39
    label "biuro"
  ]
  node [
    id 40
    label "organizacja"
  ]
  node [
    id 41
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 42
    label "Fundusze_Unijne"
  ]
  node [
    id 43
    label "zamyka&#263;"
  ]
  node [
    id 44
    label "establishment"
  ]
  node [
    id 45
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 46
    label "urz&#261;d"
  ]
  node [
    id 47
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 48
    label "afiliowa&#263;"
  ]
  node [
    id 49
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 50
    label "standard"
  ]
  node [
    id 51
    label "zamykanie"
  ]
  node [
    id 52
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 53
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 54
    label "warunek_lokalowy"
  ]
  node [
    id 55
    label "plac"
  ]
  node [
    id 56
    label "location"
  ]
  node [
    id 57
    label "uwaga"
  ]
  node [
    id 58
    label "przestrze&#324;"
  ]
  node [
    id 59
    label "status"
  ]
  node [
    id 60
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 61
    label "chwila"
  ]
  node [
    id 62
    label "cia&#322;o"
  ]
  node [
    id 63
    label "cecha"
  ]
  node [
    id 64
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 65
    label "praca"
  ]
  node [
    id 66
    label "rz&#261;d"
  ]
  node [
    id 67
    label "przedmiot"
  ]
  node [
    id 68
    label "kom&#243;rka"
  ]
  node [
    id 69
    label "furnishing"
  ]
  node [
    id 70
    label "zabezpieczenie"
  ]
  node [
    id 71
    label "zrobienie"
  ]
  node [
    id 72
    label "wyrz&#261;dzenie"
  ]
  node [
    id 73
    label "zagospodarowanie"
  ]
  node [
    id 74
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 75
    label "ig&#322;a"
  ]
  node [
    id 76
    label "narz&#281;dzie"
  ]
  node [
    id 77
    label "wirnik"
  ]
  node [
    id 78
    label "aparatura"
  ]
  node [
    id 79
    label "system_energetyczny"
  ]
  node [
    id 80
    label "impulsator"
  ]
  node [
    id 81
    label "mechanizm"
  ]
  node [
    id 82
    label "sprz&#281;t"
  ]
  node [
    id 83
    label "czynno&#347;&#263;"
  ]
  node [
    id 84
    label "blokowanie"
  ]
  node [
    id 85
    label "set"
  ]
  node [
    id 86
    label "zablokowanie"
  ]
  node [
    id 87
    label "przygotowanie"
  ]
  node [
    id 88
    label "komora"
  ]
  node [
    id 89
    label "j&#281;zyk"
  ]
  node [
    id 90
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 91
    label "portfel"
  ]
  node [
    id 92
    label "kwota"
  ]
  node [
    id 93
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 94
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 95
    label "forsa"
  ]
  node [
    id 96
    label "kapa&#263;"
  ]
  node [
    id 97
    label "kapn&#261;&#263;"
  ]
  node [
    id 98
    label "kapanie"
  ]
  node [
    id 99
    label "kapita&#322;"
  ]
  node [
    id 100
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 101
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 102
    label "kapni&#281;cie"
  ]
  node [
    id 103
    label "wyda&#263;"
  ]
  node [
    id 104
    label "hajs"
  ]
  node [
    id 105
    label "dydki"
  ]
  node [
    id 106
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 107
    label "p&#322;ytka"
  ]
  node [
    id 108
    label "glazura"
  ]
  node [
    id 109
    label "blaszka"
  ]
  node [
    id 110
    label "plate"
  ]
  node [
    id 111
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 112
    label "p&#322;yta"
  ]
  node [
    id 113
    label "dysk_optyczny"
  ]
  node [
    id 114
    label "polewa"
  ]
  node [
    id 115
    label "ok&#322;adzina"
  ]
  node [
    id 116
    label "pow&#322;oka"
  ]
  node [
    id 117
    label "varnish"
  ]
  node [
    id 118
    label "pokrycie"
  ]
  node [
    id 119
    label "w&#322;osowina"
  ]
  node [
    id 120
    label "substancja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
]
