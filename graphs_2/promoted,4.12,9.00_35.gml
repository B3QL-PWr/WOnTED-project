graph [
  node [
    id 0
    label "spawalnictwo"
    origin "text"
  ]
  node [
    id 1
    label "budownictwo"
    origin "text"
  ]
  node [
    id 2
    label "welding"
  ]
  node [
    id 3
    label "stopiwo"
  ]
  node [
    id 4
    label "przemys&#322;"
  ]
  node [
    id 5
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 6
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 7
    label "uprzemys&#322;owienie"
  ]
  node [
    id 8
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 9
    label "przechowalnictwo"
  ]
  node [
    id 10
    label "uprzemys&#322;awianie"
  ]
  node [
    id 11
    label "gospodarka"
  ]
  node [
    id 12
    label "tworzywo"
  ]
  node [
    id 13
    label "metal"
  ]
  node [
    id 14
    label "spoiwo"
  ]
  node [
    id 15
    label "og&#243;lnobudowlany"
  ]
  node [
    id 16
    label "wymian"
  ]
  node [
    id 17
    label "iniekcyjny"
  ]
  node [
    id 18
    label "lateryt"
  ]
  node [
    id 19
    label "rozpierak"
  ]
  node [
    id 20
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 21
    label "zgarniacz"
  ]
  node [
    id 22
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 23
    label "architektura_rezydencjonalna"
  ]
  node [
    id 24
    label "nauka"
  ]
  node [
    id 25
    label "computer_architecture"
  ]
  node [
    id 26
    label "murarstwo"
  ]
  node [
    id 27
    label "styl_architektoniczny"
  ]
  node [
    id 28
    label "labirynt"
  ]
  node [
    id 29
    label "absolutorium"
  ]
  node [
    id 30
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 31
    label "dzia&#322;anie"
  ]
  node [
    id 32
    label "activity"
  ]
  node [
    id 33
    label "wiedza"
  ]
  node [
    id 34
    label "miasteczko_rowerowe"
  ]
  node [
    id 35
    label "porada"
  ]
  node [
    id 36
    label "fotowoltaika"
  ]
  node [
    id 37
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 38
    label "przem&#243;wienie"
  ]
  node [
    id 39
    label "nauki_o_poznaniu"
  ]
  node [
    id 40
    label "nomotetyczny"
  ]
  node [
    id 41
    label "systematyka"
  ]
  node [
    id 42
    label "proces"
  ]
  node [
    id 43
    label "typologia"
  ]
  node [
    id 44
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 45
    label "kultura_duchowa"
  ]
  node [
    id 46
    label "&#322;awa_szkolna"
  ]
  node [
    id 47
    label "nauki_penalne"
  ]
  node [
    id 48
    label "dziedzina"
  ]
  node [
    id 49
    label "imagineskopia"
  ]
  node [
    id 50
    label "teoria_naukowa"
  ]
  node [
    id 51
    label "inwentyka"
  ]
  node [
    id 52
    label "metodologia"
  ]
  node [
    id 53
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 54
    label "nauki_o_Ziemi"
  ]
  node [
    id 55
    label "belka"
  ]
  node [
    id 56
    label "figura_geometryczna"
  ]
  node [
    id 57
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 58
    label "architektura"
  ]
  node [
    id 59
    label "budowla"
  ]
  node [
    id 60
    label "&#322;amig&#322;&#243;wka"
  ]
  node [
    id 61
    label "wz&#243;r"
  ]
  node [
    id 62
    label "pl&#261;tanina"
  ]
  node [
    id 63
    label "maze"
  ]
  node [
    id 64
    label "odnoga"
  ]
  node [
    id 65
    label "tangle"
  ]
  node [
    id 66
    label "skrzela"
  ]
  node [
    id 67
    label "b&#322;&#281;dnik"
  ]
  node [
    id 68
    label "podci&#261;gnik"
  ]
  node [
    id 69
    label "g&#243;rnictwo"
  ]
  node [
    id 70
    label "urz&#261;dzenie"
  ]
  node [
    id 71
    label "alit"
  ]
  node [
    id 72
    label "lateryzacja"
  ]
  node [
    id 73
    label "hutnictwo"
  ]
  node [
    id 74
    label "laterite"
  ]
  node [
    id 75
    label "rolnictwo"
  ]
  node [
    id 76
    label "scraper"
  ]
  node [
    id 77
    label "masonry"
  ]
  node [
    id 78
    label "rzemios&#322;o"
  ]
  node [
    id 79
    label "Stefan"
  ]
  node [
    id 80
    label "bry&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 79
    target 80
  ]
]
