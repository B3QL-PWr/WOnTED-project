graph [
  node [
    id 0
    label "w&#281;giel"
    origin "text"
  ]
  node [
    id 1
    label "kamienny"
    origin "text"
  ]
  node [
    id 2
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "elektroenergetyka"
    origin "text"
  ]
  node [
    id 6
    label "ciep&#322;ownictwo"
    origin "text"
  ]
  node [
    id 7
    label "koksownictwo"
    origin "text"
  ]
  node [
    id 8
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 9
    label "sektor"
    origin "text"
  ]
  node [
    id 10
    label "komunalno"
    origin "text"
  ]
  node [
    id 11
    label "bytowy"
    origin "text"
  ]
  node [
    id 12
    label "coal"
  ]
  node [
    id 13
    label "fulleren"
  ]
  node [
    id 14
    label "niemetal"
  ]
  node [
    id 15
    label "rysunek"
  ]
  node [
    id 16
    label "ska&#322;a"
  ]
  node [
    id 17
    label "zsypnik"
  ]
  node [
    id 18
    label "surowiec_energetyczny"
  ]
  node [
    id 19
    label "zw&#281;glanie_si&#281;"
  ]
  node [
    id 20
    label "coil"
  ]
  node [
    id 21
    label "przybory_do_pisania"
  ]
  node [
    id 22
    label "w&#281;glarka"
  ]
  node [
    id 23
    label "bry&#322;a"
  ]
  node [
    id 24
    label "w&#281;glowodan"
  ]
  node [
    id 25
    label "makroelement"
  ]
  node [
    id 26
    label "zw&#281;glenie_si&#281;"
  ]
  node [
    id 27
    label "kopalina_podstawowa"
  ]
  node [
    id 28
    label "w&#281;glowiec"
  ]
  node [
    id 29
    label "carbon"
  ]
  node [
    id 30
    label "pierwiastek"
  ]
  node [
    id 31
    label "sk&#322;adnik_mineralny"
  ]
  node [
    id 32
    label "kszta&#322;t"
  ]
  node [
    id 33
    label "figura_geometryczna"
  ]
  node [
    id 34
    label "bezkszta&#322;tno&#347;&#263;"
  ]
  node [
    id 35
    label "kawa&#322;ek"
  ]
  node [
    id 36
    label "block"
  ]
  node [
    id 37
    label "solid"
  ]
  node [
    id 38
    label "kreska"
  ]
  node [
    id 39
    label "picture"
  ]
  node [
    id 40
    label "teka"
  ]
  node [
    id 41
    label "photograph"
  ]
  node [
    id 42
    label "ilustracja"
  ]
  node [
    id 43
    label "grafika"
  ]
  node [
    id 44
    label "plastyka"
  ]
  node [
    id 45
    label "shape"
  ]
  node [
    id 46
    label "uskoczenie"
  ]
  node [
    id 47
    label "mieszanina"
  ]
  node [
    id 48
    label "zmetamorfizowanie"
  ]
  node [
    id 49
    label "soczewa"
  ]
  node [
    id 50
    label "opoka"
  ]
  node [
    id 51
    label "uskakiwa&#263;"
  ]
  node [
    id 52
    label "sklerometr"
  ]
  node [
    id 53
    label "&#322;upkowato&#347;&#263;"
  ]
  node [
    id 54
    label "uskakiwanie"
  ]
  node [
    id 55
    label "uskoczy&#263;"
  ]
  node [
    id 56
    label "rock"
  ]
  node [
    id 57
    label "obiekt"
  ]
  node [
    id 58
    label "porwak"
  ]
  node [
    id 59
    label "bloczno&#347;&#263;"
  ]
  node [
    id 60
    label "zmetamorfizowa&#263;"
  ]
  node [
    id 61
    label "lepiszcze_skalne"
  ]
  node [
    id 62
    label "rygiel"
  ]
  node [
    id 63
    label "lamina"
  ]
  node [
    id 64
    label "wyg&#322;ad_lodowcowy"
  ]
  node [
    id 65
    label "cz&#261;steczka"
  ]
  node [
    id 66
    label "fullerene"
  ]
  node [
    id 67
    label "zbiornik"
  ]
  node [
    id 68
    label "tworzywo"
  ]
  node [
    id 69
    label "grupa_hydroksylowa"
  ]
  node [
    id 70
    label "carbohydrate"
  ]
  node [
    id 71
    label "sk&#322;adnik_pokarmowy"
  ]
  node [
    id 72
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 73
    label "w&#243;z"
  ]
  node [
    id 74
    label "wagon_towarowy"
  ]
  node [
    id 75
    label "naturalny"
  ]
  node [
    id 76
    label "kamiennie"
  ]
  node [
    id 77
    label "twardy"
  ]
  node [
    id 78
    label "mineralny"
  ]
  node [
    id 79
    label "niewzruszony"
  ]
  node [
    id 80
    label "g&#322;&#281;boki"
  ]
  node [
    id 81
    label "ghaty"
  ]
  node [
    id 82
    label "ch&#322;odny"
  ]
  node [
    id 83
    label "szczery"
  ]
  node [
    id 84
    label "prawy"
  ]
  node [
    id 85
    label "zrozumia&#322;y"
  ]
  node [
    id 86
    label "immanentny"
  ]
  node [
    id 87
    label "zwyczajny"
  ]
  node [
    id 88
    label "bezsporny"
  ]
  node [
    id 89
    label "organicznie"
  ]
  node [
    id 90
    label "pierwotny"
  ]
  node [
    id 91
    label "neutralny"
  ]
  node [
    id 92
    label "normalny"
  ]
  node [
    id 93
    label "rzeczywisty"
  ]
  node [
    id 94
    label "naturalnie"
  ]
  node [
    id 95
    label "zi&#281;bienie"
  ]
  node [
    id 96
    label "niesympatyczny"
  ]
  node [
    id 97
    label "och&#322;odzenie"
  ]
  node [
    id 98
    label "opanowany"
  ]
  node [
    id 99
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 100
    label "rozs&#261;dny"
  ]
  node [
    id 101
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 102
    label "sch&#322;adzanie"
  ]
  node [
    id 103
    label "ch&#322;odno"
  ]
  node [
    id 104
    label "spokojny"
  ]
  node [
    id 105
    label "niewzruszenie"
  ]
  node [
    id 106
    label "sta&#322;y"
  ]
  node [
    id 107
    label "nienaruszony"
  ]
  node [
    id 108
    label "nieporuszenie"
  ]
  node [
    id 109
    label "oboj&#281;tny"
  ]
  node [
    id 110
    label "rze&#347;ki"
  ]
  node [
    id 111
    label "usztywnianie"
  ]
  node [
    id 112
    label "mocny"
  ]
  node [
    id 113
    label "trudny"
  ]
  node [
    id 114
    label "sztywnienie"
  ]
  node [
    id 115
    label "silny"
  ]
  node [
    id 116
    label "usztywnienie"
  ]
  node [
    id 117
    label "nieugi&#281;ty"
  ]
  node [
    id 118
    label "zdeterminowany"
  ]
  node [
    id 119
    label "niewra&#380;liwy"
  ]
  node [
    id 120
    label "zesztywnienie"
  ]
  node [
    id 121
    label "konkretny"
  ]
  node [
    id 122
    label "wytrzyma&#322;y"
  ]
  node [
    id 123
    label "twardo"
  ]
  node [
    id 124
    label "intensywny"
  ]
  node [
    id 125
    label "gruntowny"
  ]
  node [
    id 126
    label "ukryty"
  ]
  node [
    id 127
    label "wyrazisty"
  ]
  node [
    id 128
    label "daleki"
  ]
  node [
    id 129
    label "dog&#322;&#281;bny"
  ]
  node [
    id 130
    label "g&#322;&#281;boko"
  ]
  node [
    id 131
    label "niezrozumia&#322;y"
  ]
  node [
    id 132
    label "niski"
  ]
  node [
    id 133
    label "m&#261;dry"
  ]
  node [
    id 134
    label "schody"
  ]
  node [
    id 135
    label "rzeka"
  ]
  node [
    id 136
    label "azjatycki"
  ]
  node [
    id 137
    label "korzysta&#263;"
  ]
  node [
    id 138
    label "liga&#263;"
  ]
  node [
    id 139
    label "give"
  ]
  node [
    id 140
    label "distribute"
  ]
  node [
    id 141
    label "u&#380;ywa&#263;"
  ]
  node [
    id 142
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 143
    label "use"
  ]
  node [
    id 144
    label "krzywdzi&#263;"
  ]
  node [
    id 145
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 146
    label "uzyskiwa&#263;"
  ]
  node [
    id 147
    label "bash"
  ]
  node [
    id 148
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 149
    label "doznawa&#263;"
  ]
  node [
    id 150
    label "robi&#263;"
  ]
  node [
    id 151
    label "ukrzywdza&#263;"
  ]
  node [
    id 152
    label "niesprawiedliwy"
  ]
  node [
    id 153
    label "szkodzi&#263;"
  ]
  node [
    id 154
    label "powodowa&#263;"
  ]
  node [
    id 155
    label "wrong"
  ]
  node [
    id 156
    label "copulate"
  ]
  node [
    id 157
    label "&#380;y&#263;"
  ]
  node [
    id 158
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 159
    label "wierzga&#263;"
  ]
  node [
    id 160
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 161
    label "mie&#263;_miejsce"
  ]
  node [
    id 162
    label "equal"
  ]
  node [
    id 163
    label "trwa&#263;"
  ]
  node [
    id 164
    label "chodzi&#263;"
  ]
  node [
    id 165
    label "si&#281;ga&#263;"
  ]
  node [
    id 166
    label "stan"
  ]
  node [
    id 167
    label "obecno&#347;&#263;"
  ]
  node [
    id 168
    label "stand"
  ]
  node [
    id 169
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 170
    label "uczestniczy&#263;"
  ]
  node [
    id 171
    label "participate"
  ]
  node [
    id 172
    label "istnie&#263;"
  ]
  node [
    id 173
    label "pozostawa&#263;"
  ]
  node [
    id 174
    label "zostawa&#263;"
  ]
  node [
    id 175
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 176
    label "adhere"
  ]
  node [
    id 177
    label "compass"
  ]
  node [
    id 178
    label "appreciation"
  ]
  node [
    id 179
    label "osi&#261;ga&#263;"
  ]
  node [
    id 180
    label "dociera&#263;"
  ]
  node [
    id 181
    label "get"
  ]
  node [
    id 182
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 183
    label "mierzy&#263;"
  ]
  node [
    id 184
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 185
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 186
    label "exsert"
  ]
  node [
    id 187
    label "being"
  ]
  node [
    id 188
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 189
    label "cecha"
  ]
  node [
    id 190
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 191
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 192
    label "p&#322;ywa&#263;"
  ]
  node [
    id 193
    label "run"
  ]
  node [
    id 194
    label "bangla&#263;"
  ]
  node [
    id 195
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 196
    label "przebiega&#263;"
  ]
  node [
    id 197
    label "wk&#322;ada&#263;"
  ]
  node [
    id 198
    label "proceed"
  ]
  node [
    id 199
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 200
    label "carry"
  ]
  node [
    id 201
    label "bywa&#263;"
  ]
  node [
    id 202
    label "dziama&#263;"
  ]
  node [
    id 203
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 204
    label "stara&#263;_si&#281;"
  ]
  node [
    id 205
    label "para"
  ]
  node [
    id 206
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 207
    label "str&#243;j"
  ]
  node [
    id 208
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 209
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 210
    label "krok"
  ]
  node [
    id 211
    label "tryb"
  ]
  node [
    id 212
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 213
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 214
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 215
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 216
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 217
    label "continue"
  ]
  node [
    id 218
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 219
    label "Ohio"
  ]
  node [
    id 220
    label "wci&#281;cie"
  ]
  node [
    id 221
    label "Nowy_York"
  ]
  node [
    id 222
    label "warstwa"
  ]
  node [
    id 223
    label "samopoczucie"
  ]
  node [
    id 224
    label "Illinois"
  ]
  node [
    id 225
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 226
    label "state"
  ]
  node [
    id 227
    label "Jukatan"
  ]
  node [
    id 228
    label "Kalifornia"
  ]
  node [
    id 229
    label "Wirginia"
  ]
  node [
    id 230
    label "wektor"
  ]
  node [
    id 231
    label "Goa"
  ]
  node [
    id 232
    label "Teksas"
  ]
  node [
    id 233
    label "Waszyngton"
  ]
  node [
    id 234
    label "miejsce"
  ]
  node [
    id 235
    label "Massachusetts"
  ]
  node [
    id 236
    label "Alaska"
  ]
  node [
    id 237
    label "Arakan"
  ]
  node [
    id 238
    label "Hawaje"
  ]
  node [
    id 239
    label "Maryland"
  ]
  node [
    id 240
    label "punkt"
  ]
  node [
    id 241
    label "Michigan"
  ]
  node [
    id 242
    label "Arizona"
  ]
  node [
    id 243
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 244
    label "Georgia"
  ]
  node [
    id 245
    label "poziom"
  ]
  node [
    id 246
    label "Pensylwania"
  ]
  node [
    id 247
    label "Luizjana"
  ]
  node [
    id 248
    label "Nowy_Meksyk"
  ]
  node [
    id 249
    label "Alabama"
  ]
  node [
    id 250
    label "ilo&#347;&#263;"
  ]
  node [
    id 251
    label "Kansas"
  ]
  node [
    id 252
    label "Oregon"
  ]
  node [
    id 253
    label "Oklahoma"
  ]
  node [
    id 254
    label "Floryda"
  ]
  node [
    id 255
    label "jednostka_administracyjna"
  ]
  node [
    id 256
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 257
    label "energetyka"
  ]
  node [
    id 258
    label "nauka"
  ]
  node [
    id 259
    label "heat"
  ]
  node [
    id 260
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 261
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 262
    label "uprzemys&#322;owienie"
  ]
  node [
    id 263
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 264
    label "przechowalnictwo"
  ]
  node [
    id 265
    label "uprzemys&#322;awianie"
  ]
  node [
    id 266
    label "gospodarka"
  ]
  node [
    id 267
    label "przemys&#322;_spo&#380;ywczy"
  ]
  node [
    id 268
    label "wiedza"
  ]
  node [
    id 269
    label "dobra_konsumpcyjne"
  ]
  node [
    id 270
    label "szko&#322;a"
  ]
  node [
    id 271
    label "produkt"
  ]
  node [
    id 272
    label "inwentarz"
  ]
  node [
    id 273
    label "rynek"
  ]
  node [
    id 274
    label "mieszkalnictwo"
  ]
  node [
    id 275
    label "agregat_ekonomiczny"
  ]
  node [
    id 276
    label "miejsce_pracy"
  ]
  node [
    id 277
    label "produkowanie"
  ]
  node [
    id 278
    label "farmaceutyka"
  ]
  node [
    id 279
    label "rolnictwo"
  ]
  node [
    id 280
    label "transport"
  ]
  node [
    id 281
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 282
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 283
    label "obronno&#347;&#263;"
  ]
  node [
    id 284
    label "sektor_prywatny"
  ]
  node [
    id 285
    label "sch&#322;adza&#263;"
  ]
  node [
    id 286
    label "czerwona_strefa"
  ]
  node [
    id 287
    label "struktura"
  ]
  node [
    id 288
    label "pole"
  ]
  node [
    id 289
    label "sektor_publiczny"
  ]
  node [
    id 290
    label "bankowo&#347;&#263;"
  ]
  node [
    id 291
    label "gospodarowanie"
  ]
  node [
    id 292
    label "obora"
  ]
  node [
    id 293
    label "gospodarka_wodna"
  ]
  node [
    id 294
    label "gospodarka_le&#347;na"
  ]
  node [
    id 295
    label "gospodarowa&#263;"
  ]
  node [
    id 296
    label "fabryka"
  ]
  node [
    id 297
    label "wytw&#243;rnia"
  ]
  node [
    id 298
    label "stodo&#322;a"
  ]
  node [
    id 299
    label "spichlerz"
  ]
  node [
    id 300
    label "administracja"
  ]
  node [
    id 301
    label "sch&#322;odzenie"
  ]
  node [
    id 302
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 303
    label "zasada"
  ]
  node [
    id 304
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 305
    label "regulacja_cen"
  ]
  node [
    id 306
    label "szkolnictwo"
  ]
  node [
    id 307
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 308
    label "rozwini&#281;cie"
  ]
  node [
    id 309
    label "proces_ekonomiczny"
  ]
  node [
    id 310
    label "modernizacja"
  ]
  node [
    id 311
    label "spowodowanie"
  ]
  node [
    id 312
    label "industrialization"
  ]
  node [
    id 313
    label "czynno&#347;&#263;"
  ]
  node [
    id 314
    label "powodowanie"
  ]
  node [
    id 315
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 316
    label "dziedzina"
  ]
  node [
    id 317
    label "klaster_dyskowy"
  ]
  node [
    id 318
    label "widownia"
  ]
  node [
    id 319
    label "teren"
  ]
  node [
    id 320
    label "balkon"
  ]
  node [
    id 321
    label "odbiorca"
  ]
  node [
    id 322
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 323
    label "proscenium"
  ]
  node [
    id 324
    label "audience"
  ]
  node [
    id 325
    label "publiczka"
  ]
  node [
    id 326
    label "grupa"
  ]
  node [
    id 327
    label "widzownia"
  ]
  node [
    id 328
    label "lo&#380;a"
  ]
  node [
    id 329
    label "Rzym_Zachodni"
  ]
  node [
    id 330
    label "whole"
  ]
  node [
    id 331
    label "element"
  ]
  node [
    id 332
    label "Rzym_Wschodni"
  ]
  node [
    id 333
    label "urz&#261;dzenie"
  ]
  node [
    id 334
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 335
    label "sfera"
  ]
  node [
    id 336
    label "zbi&#243;r"
  ]
  node [
    id 337
    label "zakres"
  ]
  node [
    id 338
    label "funkcja"
  ]
  node [
    id 339
    label "bezdro&#380;e"
  ]
  node [
    id 340
    label "poddzia&#322;"
  ]
  node [
    id 341
    label "zag&#322;&#281;bie"
  ]
  node [
    id 342
    label "g&#243;rno&#347;l&#261;ski"
  ]
  node [
    id 343
    label "Zag&#322;ebie"
  ]
  node [
    id 344
    label "lubelski"
  ]
  node [
    id 345
    label "ruda"
  ]
  node [
    id 346
    label "&#347;l&#261;ski"
  ]
  node [
    id 347
    label "elektrownia"
  ]
  node [
    id 348
    label "Be&#322;chat&#243;w"
  ]
  node [
    id 349
    label "Gorz&#243;w"
  ]
  node [
    id 350
    label "wielkopolski"
  ]
  node [
    id 351
    label "kamie&#324;"
  ]
  node [
    id 352
    label "pomorski"
  ]
  node [
    id 353
    label "morze"
  ]
  node [
    id 354
    label "ba&#322;tycki"
  ]
  node [
    id 355
    label "ostrowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 341
    target 342
  ]
  edge [
    source 343
    target 344
  ]
  edge [
    source 345
    target 346
  ]
  edge [
    source 347
    target 348
  ]
  edge [
    source 349
    target 350
  ]
  edge [
    source 350
    target 355
  ]
  edge [
    source 351
    target 352
  ]
  edge [
    source 353
    target 354
  ]
]
