graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pami&#261;tka"
    origin "text"
  ]
  node [
    id 3
    label "moi"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wi&#281;tej_pami&#281;ci"
    origin "text"
  ]
  node [
    id 5
    label "pradziadek"
    origin "text"
  ]
  node [
    id 6
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "lato"
    origin "text"
  ]
  node [
    id 10
    label "osiemdziesi&#261;ty"
    origin "text"
  ]
  node [
    id 11
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 12
    label "strona"
    origin "text"
  ]
  node [
    id 13
    label "przodkini"
  ]
  node [
    id 14
    label "matka_zast&#281;pcza"
  ]
  node [
    id 15
    label "matczysko"
  ]
  node [
    id 16
    label "rodzice"
  ]
  node [
    id 17
    label "stara"
  ]
  node [
    id 18
    label "macierz"
  ]
  node [
    id 19
    label "rodzic"
  ]
  node [
    id 20
    label "Matka_Boska"
  ]
  node [
    id 21
    label "macocha"
  ]
  node [
    id 22
    label "starzy"
  ]
  node [
    id 23
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 24
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 25
    label "pokolenie"
  ]
  node [
    id 26
    label "wapniaki"
  ]
  node [
    id 27
    label "opiekun"
  ]
  node [
    id 28
    label "wapniak"
  ]
  node [
    id 29
    label "rodzic_chrzestny"
  ]
  node [
    id 30
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 31
    label "krewna"
  ]
  node [
    id 32
    label "matka"
  ]
  node [
    id 33
    label "&#380;ona"
  ]
  node [
    id 34
    label "kobieta"
  ]
  node [
    id 35
    label "partnerka"
  ]
  node [
    id 36
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 37
    label "matuszka"
  ]
  node [
    id 38
    label "parametryzacja"
  ]
  node [
    id 39
    label "pa&#324;stwo"
  ]
  node [
    id 40
    label "poj&#281;cie"
  ]
  node [
    id 41
    label "mod"
  ]
  node [
    id 42
    label "patriota"
  ]
  node [
    id 43
    label "m&#281;&#380;atka"
  ]
  node [
    id 44
    label "testify"
  ]
  node [
    id 45
    label "point"
  ]
  node [
    id 46
    label "przedstawi&#263;"
  ]
  node [
    id 47
    label "poda&#263;"
  ]
  node [
    id 48
    label "poinformowa&#263;"
  ]
  node [
    id 49
    label "udowodni&#263;"
  ]
  node [
    id 50
    label "spowodowa&#263;"
  ]
  node [
    id 51
    label "wyrazi&#263;"
  ]
  node [
    id 52
    label "przeszkoli&#263;"
  ]
  node [
    id 53
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 54
    label "indicate"
  ]
  node [
    id 55
    label "pom&#243;c"
  ]
  node [
    id 56
    label "inform"
  ]
  node [
    id 57
    label "zakomunikowa&#263;"
  ]
  node [
    id 58
    label "uzasadni&#263;"
  ]
  node [
    id 59
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 60
    label "oznaczy&#263;"
  ]
  node [
    id 61
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 62
    label "vent"
  ]
  node [
    id 63
    label "act"
  ]
  node [
    id 64
    label "tenis"
  ]
  node [
    id 65
    label "supply"
  ]
  node [
    id 66
    label "da&#263;"
  ]
  node [
    id 67
    label "ustawi&#263;"
  ]
  node [
    id 68
    label "siatk&#243;wka"
  ]
  node [
    id 69
    label "give"
  ]
  node [
    id 70
    label "zagra&#263;"
  ]
  node [
    id 71
    label "jedzenie"
  ]
  node [
    id 72
    label "introduce"
  ]
  node [
    id 73
    label "nafaszerowa&#263;"
  ]
  node [
    id 74
    label "zaserwowa&#263;"
  ]
  node [
    id 75
    label "ukaza&#263;"
  ]
  node [
    id 76
    label "przedstawienie"
  ]
  node [
    id 77
    label "zapozna&#263;"
  ]
  node [
    id 78
    label "express"
  ]
  node [
    id 79
    label "represent"
  ]
  node [
    id 80
    label "zaproponowa&#263;"
  ]
  node [
    id 81
    label "zademonstrowa&#263;"
  ]
  node [
    id 82
    label "typify"
  ]
  node [
    id 83
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 84
    label "opisa&#263;"
  ]
  node [
    id 85
    label "przedmiot"
  ]
  node [
    id 86
    label "&#347;wiadectwo"
  ]
  node [
    id 87
    label "zboczenie"
  ]
  node [
    id 88
    label "om&#243;wienie"
  ]
  node [
    id 89
    label "sponiewieranie"
  ]
  node [
    id 90
    label "discipline"
  ]
  node [
    id 91
    label "rzecz"
  ]
  node [
    id 92
    label "omawia&#263;"
  ]
  node [
    id 93
    label "kr&#261;&#380;enie"
  ]
  node [
    id 94
    label "tre&#347;&#263;"
  ]
  node [
    id 95
    label "robienie"
  ]
  node [
    id 96
    label "sponiewiera&#263;"
  ]
  node [
    id 97
    label "element"
  ]
  node [
    id 98
    label "entity"
  ]
  node [
    id 99
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 100
    label "tematyka"
  ]
  node [
    id 101
    label "w&#261;tek"
  ]
  node [
    id 102
    label "charakter"
  ]
  node [
    id 103
    label "zbaczanie"
  ]
  node [
    id 104
    label "program_nauczania"
  ]
  node [
    id 105
    label "om&#243;wi&#263;"
  ]
  node [
    id 106
    label "omawianie"
  ]
  node [
    id 107
    label "thing"
  ]
  node [
    id 108
    label "kultura"
  ]
  node [
    id 109
    label "istota"
  ]
  node [
    id 110
    label "zbacza&#263;"
  ]
  node [
    id 111
    label "zboczy&#263;"
  ]
  node [
    id 112
    label "dow&#243;d"
  ]
  node [
    id 113
    label "o&#347;wiadczenie"
  ]
  node [
    id 114
    label "za&#347;wiadczenie"
  ]
  node [
    id 115
    label "certificate"
  ]
  node [
    id 116
    label "promocja"
  ]
  node [
    id 117
    label "dokument"
  ]
  node [
    id 118
    label "pradziad"
  ]
  node [
    id 119
    label "przodek"
  ]
  node [
    id 120
    label "pradziadkowie"
  ]
  node [
    id 121
    label "ojcowie"
  ]
  node [
    id 122
    label "linea&#380;"
  ]
  node [
    id 123
    label "krewny"
  ]
  node [
    id 124
    label "chodnik"
  ]
  node [
    id 125
    label "w&#243;z"
  ]
  node [
    id 126
    label "p&#322;ug"
  ]
  node [
    id 127
    label "wyrobisko"
  ]
  node [
    id 128
    label "dziad"
  ]
  node [
    id 129
    label "antecesor"
  ]
  node [
    id 130
    label "post&#281;p"
  ]
  node [
    id 131
    label "egzemplarz"
  ]
  node [
    id 132
    label "rozdzia&#322;"
  ]
  node [
    id 133
    label "wk&#322;ad"
  ]
  node [
    id 134
    label "tytu&#322;"
  ]
  node [
    id 135
    label "zak&#322;adka"
  ]
  node [
    id 136
    label "nomina&#322;"
  ]
  node [
    id 137
    label "ok&#322;adka"
  ]
  node [
    id 138
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 139
    label "wydawnictwo"
  ]
  node [
    id 140
    label "ekslibris"
  ]
  node [
    id 141
    label "tekst"
  ]
  node [
    id 142
    label "przek&#322;adacz"
  ]
  node [
    id 143
    label "bibliofilstwo"
  ]
  node [
    id 144
    label "falc"
  ]
  node [
    id 145
    label "pagina"
  ]
  node [
    id 146
    label "zw&#243;j"
  ]
  node [
    id 147
    label "ekscerpcja"
  ]
  node [
    id 148
    label "j&#281;zykowo"
  ]
  node [
    id 149
    label "wypowied&#378;"
  ]
  node [
    id 150
    label "redakcja"
  ]
  node [
    id 151
    label "wytw&#243;r"
  ]
  node [
    id 152
    label "pomini&#281;cie"
  ]
  node [
    id 153
    label "dzie&#322;o"
  ]
  node [
    id 154
    label "preparacja"
  ]
  node [
    id 155
    label "odmianka"
  ]
  node [
    id 156
    label "opu&#347;ci&#263;"
  ]
  node [
    id 157
    label "koniektura"
  ]
  node [
    id 158
    label "obelga"
  ]
  node [
    id 159
    label "czynnik_biotyczny"
  ]
  node [
    id 160
    label "wyewoluowanie"
  ]
  node [
    id 161
    label "reakcja"
  ]
  node [
    id 162
    label "individual"
  ]
  node [
    id 163
    label "przyswoi&#263;"
  ]
  node [
    id 164
    label "starzenie_si&#281;"
  ]
  node [
    id 165
    label "wyewoluowa&#263;"
  ]
  node [
    id 166
    label "okaz"
  ]
  node [
    id 167
    label "part"
  ]
  node [
    id 168
    label "ewoluowa&#263;"
  ]
  node [
    id 169
    label "przyswojenie"
  ]
  node [
    id 170
    label "ewoluowanie"
  ]
  node [
    id 171
    label "obiekt"
  ]
  node [
    id 172
    label "sztuka"
  ]
  node [
    id 173
    label "agent"
  ]
  node [
    id 174
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 175
    label "przyswaja&#263;"
  ]
  node [
    id 176
    label "nicpo&#324;"
  ]
  node [
    id 177
    label "przyswajanie"
  ]
  node [
    id 178
    label "debit"
  ]
  node [
    id 179
    label "redaktor"
  ]
  node [
    id 180
    label "druk"
  ]
  node [
    id 181
    label "publikacja"
  ]
  node [
    id 182
    label "szata_graficzna"
  ]
  node [
    id 183
    label "firma"
  ]
  node [
    id 184
    label "wydawa&#263;"
  ]
  node [
    id 185
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 186
    label "wyda&#263;"
  ]
  node [
    id 187
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 188
    label "poster"
  ]
  node [
    id 189
    label "wydarzenie"
  ]
  node [
    id 190
    label "faza"
  ]
  node [
    id 191
    label "interruption"
  ]
  node [
    id 192
    label "podzia&#322;"
  ]
  node [
    id 193
    label "podrozdzia&#322;"
  ]
  node [
    id 194
    label "fragment"
  ]
  node [
    id 195
    label "nadtytu&#322;"
  ]
  node [
    id 196
    label "tytulatura"
  ]
  node [
    id 197
    label "elevation"
  ]
  node [
    id 198
    label "mianowaniec"
  ]
  node [
    id 199
    label "nazwa"
  ]
  node [
    id 200
    label "podtytu&#322;"
  ]
  node [
    id 201
    label "blacha"
  ]
  node [
    id 202
    label "z&#322;&#261;czenie"
  ]
  node [
    id 203
    label "grzbiet"
  ]
  node [
    id 204
    label "kartka"
  ]
  node [
    id 205
    label "kwota"
  ]
  node [
    id 206
    label "uczestnictwo"
  ]
  node [
    id 207
    label "input"
  ]
  node [
    id 208
    label "czasopismo"
  ]
  node [
    id 209
    label "lokata"
  ]
  node [
    id 210
    label "zeszyt"
  ]
  node [
    id 211
    label "pagination"
  ]
  node [
    id 212
    label "numer"
  ]
  node [
    id 213
    label "blok"
  ]
  node [
    id 214
    label "oprawa"
  ]
  node [
    id 215
    label "boarding"
  ]
  node [
    id 216
    label "oprawianie"
  ]
  node [
    id 217
    label "os&#322;ona"
  ]
  node [
    id 218
    label "oprawia&#263;"
  ]
  node [
    id 219
    label "t&#322;umacz"
  ]
  node [
    id 220
    label "urz&#261;dzenie"
  ]
  node [
    id 221
    label "cz&#322;owiek"
  ]
  node [
    id 222
    label "kszta&#322;t"
  ]
  node [
    id 223
    label "wrench"
  ]
  node [
    id 224
    label "m&#243;zg"
  ]
  node [
    id 225
    label "kink"
  ]
  node [
    id 226
    label "plik"
  ]
  node [
    id 227
    label "manuskrypt"
  ]
  node [
    id 228
    label "rolka"
  ]
  node [
    id 229
    label "bookmark"
  ]
  node [
    id 230
    label "fa&#322;da"
  ]
  node [
    id 231
    label "znacznik"
  ]
  node [
    id 232
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 233
    label "widok"
  ]
  node [
    id 234
    label "program"
  ]
  node [
    id 235
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 236
    label "warto&#347;&#263;"
  ]
  node [
    id 237
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 238
    label "pieni&#261;dz"
  ]
  node [
    id 239
    label "par_value"
  ]
  node [
    id 240
    label "cena"
  ]
  node [
    id 241
    label "znaczek"
  ]
  node [
    id 242
    label "kolekcjonerstwo"
  ]
  node [
    id 243
    label "bibliomania"
  ]
  node [
    id 244
    label "oznaczenie"
  ]
  node [
    id 245
    label "formu&#322;owa&#263;"
  ]
  node [
    id 246
    label "ozdabia&#263;"
  ]
  node [
    id 247
    label "stawia&#263;"
  ]
  node [
    id 248
    label "spell"
  ]
  node [
    id 249
    label "styl"
  ]
  node [
    id 250
    label "skryba"
  ]
  node [
    id 251
    label "read"
  ]
  node [
    id 252
    label "donosi&#263;"
  ]
  node [
    id 253
    label "code"
  ]
  node [
    id 254
    label "dysgrafia"
  ]
  node [
    id 255
    label "dysortografia"
  ]
  node [
    id 256
    label "tworzy&#263;"
  ]
  node [
    id 257
    label "prasa"
  ]
  node [
    id 258
    label "robi&#263;"
  ]
  node [
    id 259
    label "pope&#322;nia&#263;"
  ]
  node [
    id 260
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 261
    label "wytwarza&#263;"
  ]
  node [
    id 262
    label "get"
  ]
  node [
    id 263
    label "consist"
  ]
  node [
    id 264
    label "stanowi&#263;"
  ]
  node [
    id 265
    label "raise"
  ]
  node [
    id 266
    label "spill_the_beans"
  ]
  node [
    id 267
    label "przeby&#263;"
  ]
  node [
    id 268
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 269
    label "zanosi&#263;"
  ]
  node [
    id 270
    label "zu&#380;y&#263;"
  ]
  node [
    id 271
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 272
    label "render"
  ]
  node [
    id 273
    label "ci&#261;&#380;a"
  ]
  node [
    id 274
    label "informowa&#263;"
  ]
  node [
    id 275
    label "komunikowa&#263;"
  ]
  node [
    id 276
    label "convey"
  ]
  node [
    id 277
    label "pozostawia&#263;"
  ]
  node [
    id 278
    label "czyni&#263;"
  ]
  node [
    id 279
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 280
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 281
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 282
    label "przewidywa&#263;"
  ]
  node [
    id 283
    label "przyznawa&#263;"
  ]
  node [
    id 284
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 285
    label "go"
  ]
  node [
    id 286
    label "obstawia&#263;"
  ]
  node [
    id 287
    label "umieszcza&#263;"
  ]
  node [
    id 288
    label "ocenia&#263;"
  ]
  node [
    id 289
    label "zastawia&#263;"
  ]
  node [
    id 290
    label "stanowisko"
  ]
  node [
    id 291
    label "znak"
  ]
  node [
    id 292
    label "wskazywa&#263;"
  ]
  node [
    id 293
    label "uruchamia&#263;"
  ]
  node [
    id 294
    label "fundowa&#263;"
  ]
  node [
    id 295
    label "zmienia&#263;"
  ]
  node [
    id 296
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 297
    label "deliver"
  ]
  node [
    id 298
    label "powodowa&#263;"
  ]
  node [
    id 299
    label "wyznacza&#263;"
  ]
  node [
    id 300
    label "przedstawia&#263;"
  ]
  node [
    id 301
    label "wydobywa&#263;"
  ]
  node [
    id 302
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 303
    label "trim"
  ]
  node [
    id 304
    label "gryzipi&#243;rek"
  ]
  node [
    id 305
    label "pisarz"
  ]
  node [
    id 306
    label "zesp&#243;&#322;"
  ]
  node [
    id 307
    label "t&#322;oczysko"
  ]
  node [
    id 308
    label "depesza"
  ]
  node [
    id 309
    label "maszyna"
  ]
  node [
    id 310
    label "media"
  ]
  node [
    id 311
    label "napisa&#263;"
  ]
  node [
    id 312
    label "dziennikarz_prasowy"
  ]
  node [
    id 313
    label "kiosk"
  ]
  node [
    id 314
    label "maszyna_rolnicza"
  ]
  node [
    id 315
    label "gazeta"
  ]
  node [
    id 316
    label "dysleksja"
  ]
  node [
    id 317
    label "pisanie"
  ]
  node [
    id 318
    label "trzonek"
  ]
  node [
    id 319
    label "narz&#281;dzie"
  ]
  node [
    id 320
    label "spos&#243;b"
  ]
  node [
    id 321
    label "zbi&#243;r"
  ]
  node [
    id 322
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 323
    label "zachowanie"
  ]
  node [
    id 324
    label "stylik"
  ]
  node [
    id 325
    label "dyscyplina_sportowa"
  ]
  node [
    id 326
    label "handle"
  ]
  node [
    id 327
    label "stroke"
  ]
  node [
    id 328
    label "line"
  ]
  node [
    id 329
    label "natural_language"
  ]
  node [
    id 330
    label "kanon"
  ]
  node [
    id 331
    label "behawior"
  ]
  node [
    id 332
    label "dysgraphia"
  ]
  node [
    id 333
    label "pora_roku"
  ]
  node [
    id 334
    label "punctiliously"
  ]
  node [
    id 335
    label "meticulously"
  ]
  node [
    id 336
    label "precyzyjnie"
  ]
  node [
    id 337
    label "dok&#322;adny"
  ]
  node [
    id 338
    label "rzetelnie"
  ]
  node [
    id 339
    label "sprecyzowanie"
  ]
  node [
    id 340
    label "precyzyjny"
  ]
  node [
    id 341
    label "miliamperomierz"
  ]
  node [
    id 342
    label "precyzowanie"
  ]
  node [
    id 343
    label "rzetelny"
  ]
  node [
    id 344
    label "przekonuj&#261;co"
  ]
  node [
    id 345
    label "porz&#261;dnie"
  ]
  node [
    id 346
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 347
    label "logowanie"
  ]
  node [
    id 348
    label "s&#261;d"
  ]
  node [
    id 349
    label "adres_internetowy"
  ]
  node [
    id 350
    label "linia"
  ]
  node [
    id 351
    label "serwis_internetowy"
  ]
  node [
    id 352
    label "posta&#263;"
  ]
  node [
    id 353
    label "bok"
  ]
  node [
    id 354
    label "skr&#281;canie"
  ]
  node [
    id 355
    label "skr&#281;ca&#263;"
  ]
  node [
    id 356
    label "orientowanie"
  ]
  node [
    id 357
    label "skr&#281;ci&#263;"
  ]
  node [
    id 358
    label "uj&#281;cie"
  ]
  node [
    id 359
    label "zorientowanie"
  ]
  node [
    id 360
    label "ty&#322;"
  ]
  node [
    id 361
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 362
    label "layout"
  ]
  node [
    id 363
    label "zorientowa&#263;"
  ]
  node [
    id 364
    label "podmiot"
  ]
  node [
    id 365
    label "g&#243;ra"
  ]
  node [
    id 366
    label "orientowa&#263;"
  ]
  node [
    id 367
    label "voice"
  ]
  node [
    id 368
    label "orientacja"
  ]
  node [
    id 369
    label "prz&#243;d"
  ]
  node [
    id 370
    label "internet"
  ]
  node [
    id 371
    label "powierzchnia"
  ]
  node [
    id 372
    label "forma"
  ]
  node [
    id 373
    label "skr&#281;cenie"
  ]
  node [
    id 374
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 375
    label "byt"
  ]
  node [
    id 376
    label "osobowo&#347;&#263;"
  ]
  node [
    id 377
    label "organizacja"
  ]
  node [
    id 378
    label "prawo"
  ]
  node [
    id 379
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 380
    label "nauka_prawa"
  ]
  node [
    id 381
    label "utw&#243;r"
  ]
  node [
    id 382
    label "charakterystyka"
  ]
  node [
    id 383
    label "zaistnie&#263;"
  ]
  node [
    id 384
    label "Osjan"
  ]
  node [
    id 385
    label "cecha"
  ]
  node [
    id 386
    label "kto&#347;"
  ]
  node [
    id 387
    label "wygl&#261;d"
  ]
  node [
    id 388
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 389
    label "poby&#263;"
  ]
  node [
    id 390
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 391
    label "Aspazja"
  ]
  node [
    id 392
    label "punkt_widzenia"
  ]
  node [
    id 393
    label "kompleksja"
  ]
  node [
    id 394
    label "wytrzyma&#263;"
  ]
  node [
    id 395
    label "budowa"
  ]
  node [
    id 396
    label "formacja"
  ]
  node [
    id 397
    label "pozosta&#263;"
  ]
  node [
    id 398
    label "go&#347;&#263;"
  ]
  node [
    id 399
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 400
    label "armia"
  ]
  node [
    id 401
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 402
    label "poprowadzi&#263;"
  ]
  node [
    id 403
    label "cord"
  ]
  node [
    id 404
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 405
    label "trasa"
  ]
  node [
    id 406
    label "po&#322;&#261;czenie"
  ]
  node [
    id 407
    label "tract"
  ]
  node [
    id 408
    label "materia&#322;_zecerski"
  ]
  node [
    id 409
    label "przeorientowywanie"
  ]
  node [
    id 410
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 411
    label "curve"
  ]
  node [
    id 412
    label "figura_geometryczna"
  ]
  node [
    id 413
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 414
    label "jard"
  ]
  node [
    id 415
    label "szczep"
  ]
  node [
    id 416
    label "phreaker"
  ]
  node [
    id 417
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 418
    label "grupa_organizm&#243;w"
  ]
  node [
    id 419
    label "prowadzi&#263;"
  ]
  node [
    id 420
    label "przeorientowywa&#263;"
  ]
  node [
    id 421
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 422
    label "access"
  ]
  node [
    id 423
    label "przeorientowanie"
  ]
  node [
    id 424
    label "przeorientowa&#263;"
  ]
  node [
    id 425
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 426
    label "billing"
  ]
  node [
    id 427
    label "granica"
  ]
  node [
    id 428
    label "szpaler"
  ]
  node [
    id 429
    label "sztrych"
  ]
  node [
    id 430
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 431
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 432
    label "drzewo_genealogiczne"
  ]
  node [
    id 433
    label "transporter"
  ]
  node [
    id 434
    label "przew&#243;d"
  ]
  node [
    id 435
    label "granice"
  ]
  node [
    id 436
    label "kontakt"
  ]
  node [
    id 437
    label "rz&#261;d"
  ]
  node [
    id 438
    label "przewo&#378;nik"
  ]
  node [
    id 439
    label "przystanek"
  ]
  node [
    id 440
    label "linijka"
  ]
  node [
    id 441
    label "uporz&#261;dkowanie"
  ]
  node [
    id 442
    label "coalescence"
  ]
  node [
    id 443
    label "Ural"
  ]
  node [
    id 444
    label "bearing"
  ]
  node [
    id 445
    label "prowadzenie"
  ]
  node [
    id 446
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 447
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 448
    label "koniec"
  ]
  node [
    id 449
    label "podkatalog"
  ]
  node [
    id 450
    label "nadpisa&#263;"
  ]
  node [
    id 451
    label "nadpisanie"
  ]
  node [
    id 452
    label "bundle"
  ]
  node [
    id 453
    label "folder"
  ]
  node [
    id 454
    label "nadpisywanie"
  ]
  node [
    id 455
    label "paczka"
  ]
  node [
    id 456
    label "nadpisywa&#263;"
  ]
  node [
    id 457
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 458
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 459
    label "Rzym_Zachodni"
  ]
  node [
    id 460
    label "whole"
  ]
  node [
    id 461
    label "ilo&#347;&#263;"
  ]
  node [
    id 462
    label "Rzym_Wschodni"
  ]
  node [
    id 463
    label "rozmiar"
  ]
  node [
    id 464
    label "obszar"
  ]
  node [
    id 465
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 466
    label "zwierciad&#322;o"
  ]
  node [
    id 467
    label "capacity"
  ]
  node [
    id 468
    label "plane"
  ]
  node [
    id 469
    label "temat"
  ]
  node [
    id 470
    label "jednostka_systematyczna"
  ]
  node [
    id 471
    label "poznanie"
  ]
  node [
    id 472
    label "leksem"
  ]
  node [
    id 473
    label "stan"
  ]
  node [
    id 474
    label "blaszka"
  ]
  node [
    id 475
    label "kantyzm"
  ]
  node [
    id 476
    label "zdolno&#347;&#263;"
  ]
  node [
    id 477
    label "do&#322;ek"
  ]
  node [
    id 478
    label "zawarto&#347;&#263;"
  ]
  node [
    id 479
    label "gwiazda"
  ]
  node [
    id 480
    label "formality"
  ]
  node [
    id 481
    label "struktura"
  ]
  node [
    id 482
    label "mode"
  ]
  node [
    id 483
    label "morfem"
  ]
  node [
    id 484
    label "rdze&#324;"
  ]
  node [
    id 485
    label "kielich"
  ]
  node [
    id 486
    label "ornamentyka"
  ]
  node [
    id 487
    label "pasmo"
  ]
  node [
    id 488
    label "zwyczaj"
  ]
  node [
    id 489
    label "g&#322;owa"
  ]
  node [
    id 490
    label "naczynie"
  ]
  node [
    id 491
    label "p&#322;at"
  ]
  node [
    id 492
    label "maszyna_drukarska"
  ]
  node [
    id 493
    label "style"
  ]
  node [
    id 494
    label "linearno&#347;&#263;"
  ]
  node [
    id 495
    label "wyra&#380;enie"
  ]
  node [
    id 496
    label "spirala"
  ]
  node [
    id 497
    label "dyspozycja"
  ]
  node [
    id 498
    label "odmiana"
  ]
  node [
    id 499
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 500
    label "wz&#243;r"
  ]
  node [
    id 501
    label "October"
  ]
  node [
    id 502
    label "creation"
  ]
  node [
    id 503
    label "p&#281;tla"
  ]
  node [
    id 504
    label "arystotelizm"
  ]
  node [
    id 505
    label "szablon"
  ]
  node [
    id 506
    label "miniatura"
  ]
  node [
    id 507
    label "podejrzany"
  ]
  node [
    id 508
    label "s&#261;downictwo"
  ]
  node [
    id 509
    label "system"
  ]
  node [
    id 510
    label "biuro"
  ]
  node [
    id 511
    label "court"
  ]
  node [
    id 512
    label "forum"
  ]
  node [
    id 513
    label "bronienie"
  ]
  node [
    id 514
    label "urz&#261;d"
  ]
  node [
    id 515
    label "oskar&#380;yciel"
  ]
  node [
    id 516
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 517
    label "skazany"
  ]
  node [
    id 518
    label "post&#281;powanie"
  ]
  node [
    id 519
    label "broni&#263;"
  ]
  node [
    id 520
    label "my&#347;l"
  ]
  node [
    id 521
    label "pods&#261;dny"
  ]
  node [
    id 522
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 523
    label "obrona"
  ]
  node [
    id 524
    label "instytucja"
  ]
  node [
    id 525
    label "antylogizm"
  ]
  node [
    id 526
    label "konektyw"
  ]
  node [
    id 527
    label "&#347;wiadek"
  ]
  node [
    id 528
    label "procesowicz"
  ]
  node [
    id 529
    label "pochwytanie"
  ]
  node [
    id 530
    label "wording"
  ]
  node [
    id 531
    label "wzbudzenie"
  ]
  node [
    id 532
    label "withdrawal"
  ]
  node [
    id 533
    label "capture"
  ]
  node [
    id 534
    label "podniesienie"
  ]
  node [
    id 535
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 536
    label "film"
  ]
  node [
    id 537
    label "scena"
  ]
  node [
    id 538
    label "zapisanie"
  ]
  node [
    id 539
    label "prezentacja"
  ]
  node [
    id 540
    label "rzucenie"
  ]
  node [
    id 541
    label "zamkni&#281;cie"
  ]
  node [
    id 542
    label "zabranie"
  ]
  node [
    id 543
    label "poinformowanie"
  ]
  node [
    id 544
    label "zaaresztowanie"
  ]
  node [
    id 545
    label "wzi&#281;cie"
  ]
  node [
    id 546
    label "eastern_hemisphere"
  ]
  node [
    id 547
    label "kierunek"
  ]
  node [
    id 548
    label "kierowa&#263;"
  ]
  node [
    id 549
    label "marshal"
  ]
  node [
    id 550
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 551
    label "pomaga&#263;"
  ]
  node [
    id 552
    label "tu&#322;&#243;w"
  ]
  node [
    id 553
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 554
    label "wielok&#261;t"
  ]
  node [
    id 555
    label "odcinek"
  ]
  node [
    id 556
    label "strzelba"
  ]
  node [
    id 557
    label "lufa"
  ]
  node [
    id 558
    label "&#347;ciana"
  ]
  node [
    id 559
    label "wyznaczenie"
  ]
  node [
    id 560
    label "przyczynienie_si&#281;"
  ]
  node [
    id 561
    label "zwr&#243;cenie"
  ]
  node [
    id 562
    label "zrozumienie"
  ]
  node [
    id 563
    label "po&#322;o&#380;enie"
  ]
  node [
    id 564
    label "seksualno&#347;&#263;"
  ]
  node [
    id 565
    label "wiedza"
  ]
  node [
    id 566
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 567
    label "zorientowanie_si&#281;"
  ]
  node [
    id 568
    label "pogubienie_si&#281;"
  ]
  node [
    id 569
    label "orientation"
  ]
  node [
    id 570
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 571
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 572
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 573
    label "gubienie_si&#281;"
  ]
  node [
    id 574
    label "turn"
  ]
  node [
    id 575
    label "nawini&#281;cie"
  ]
  node [
    id 576
    label "os&#322;abienie"
  ]
  node [
    id 577
    label "uszkodzenie"
  ]
  node [
    id 578
    label "odbicie"
  ]
  node [
    id 579
    label "poskr&#281;canie"
  ]
  node [
    id 580
    label "uraz"
  ]
  node [
    id 581
    label "odchylenie_si&#281;"
  ]
  node [
    id 582
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 583
    label "splecenie"
  ]
  node [
    id 584
    label "turning"
  ]
  node [
    id 585
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 586
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 587
    label "sple&#347;&#263;"
  ]
  node [
    id 588
    label "os&#322;abi&#263;"
  ]
  node [
    id 589
    label "nawin&#261;&#263;"
  ]
  node [
    id 590
    label "scali&#263;"
  ]
  node [
    id 591
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 592
    label "twist"
  ]
  node [
    id 593
    label "splay"
  ]
  node [
    id 594
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 595
    label "uszkodzi&#263;"
  ]
  node [
    id 596
    label "break"
  ]
  node [
    id 597
    label "flex"
  ]
  node [
    id 598
    label "przestrze&#324;"
  ]
  node [
    id 599
    label "zaty&#322;"
  ]
  node [
    id 600
    label "pupa"
  ]
  node [
    id 601
    label "cia&#322;o"
  ]
  node [
    id 602
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 603
    label "os&#322;abia&#263;"
  ]
  node [
    id 604
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 605
    label "splata&#263;"
  ]
  node [
    id 606
    label "throw"
  ]
  node [
    id 607
    label "screw"
  ]
  node [
    id 608
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 609
    label "scala&#263;"
  ]
  node [
    id 610
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 611
    label "przelezienie"
  ]
  node [
    id 612
    label "&#347;piew"
  ]
  node [
    id 613
    label "Synaj"
  ]
  node [
    id 614
    label "Kreml"
  ]
  node [
    id 615
    label "d&#378;wi&#281;k"
  ]
  node [
    id 616
    label "wysoki"
  ]
  node [
    id 617
    label "wzniesienie"
  ]
  node [
    id 618
    label "grupa"
  ]
  node [
    id 619
    label "pi&#281;tro"
  ]
  node [
    id 620
    label "Ropa"
  ]
  node [
    id 621
    label "kupa"
  ]
  node [
    id 622
    label "przele&#378;&#263;"
  ]
  node [
    id 623
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 624
    label "karczek"
  ]
  node [
    id 625
    label "rami&#261;czko"
  ]
  node [
    id 626
    label "Jaworze"
  ]
  node [
    id 627
    label "set"
  ]
  node [
    id 628
    label "orient"
  ]
  node [
    id 629
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 630
    label "aim"
  ]
  node [
    id 631
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 632
    label "wyznaczy&#263;"
  ]
  node [
    id 633
    label "pomaganie"
  ]
  node [
    id 634
    label "przyczynianie_si&#281;"
  ]
  node [
    id 635
    label "zwracanie"
  ]
  node [
    id 636
    label "rozeznawanie"
  ]
  node [
    id 637
    label "oznaczanie"
  ]
  node [
    id 638
    label "odchylanie_si&#281;"
  ]
  node [
    id 639
    label "kszta&#322;towanie"
  ]
  node [
    id 640
    label "os&#322;abianie"
  ]
  node [
    id 641
    label "uprz&#281;dzenie"
  ]
  node [
    id 642
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 643
    label "scalanie"
  ]
  node [
    id 644
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 645
    label "snucie"
  ]
  node [
    id 646
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 647
    label "tortuosity"
  ]
  node [
    id 648
    label "odbijanie"
  ]
  node [
    id 649
    label "contortion"
  ]
  node [
    id 650
    label "splatanie"
  ]
  node [
    id 651
    label "figura"
  ]
  node [
    id 652
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 653
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 654
    label "uwierzytelnienie"
  ]
  node [
    id 655
    label "liczba"
  ]
  node [
    id 656
    label "circumference"
  ]
  node [
    id 657
    label "cyrkumferencja"
  ]
  node [
    id 658
    label "miejsce"
  ]
  node [
    id 659
    label "provider"
  ]
  node [
    id 660
    label "hipertekst"
  ]
  node [
    id 661
    label "cyberprzestrze&#324;"
  ]
  node [
    id 662
    label "mem"
  ]
  node [
    id 663
    label "grooming"
  ]
  node [
    id 664
    label "gra_sieciowa"
  ]
  node [
    id 665
    label "biznes_elektroniczny"
  ]
  node [
    id 666
    label "sie&#263;_komputerowa"
  ]
  node [
    id 667
    label "punkt_dost&#281;pu"
  ]
  node [
    id 668
    label "us&#322;uga_internetowa"
  ]
  node [
    id 669
    label "netbook"
  ]
  node [
    id 670
    label "e-hazard"
  ]
  node [
    id 671
    label "podcast"
  ]
  node [
    id 672
    label "co&#347;"
  ]
  node [
    id 673
    label "budynek"
  ]
  node [
    id 674
    label "faul"
  ]
  node [
    id 675
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 676
    label "s&#281;dzia"
  ]
  node [
    id 677
    label "bon"
  ]
  node [
    id 678
    label "ticket"
  ]
  node [
    id 679
    label "arkusz"
  ]
  node [
    id 680
    label "kartonik"
  ]
  node [
    id 681
    label "kara"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 12
    target 204
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 221
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 223
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 212
  ]
]
