graph [
  node [
    id 0
    label "zapad&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "ostateczny"
    origin "text"
  ]
  node [
    id 2
    label "decyzja"
    origin "text"
  ]
  node [
    id 3
    label "inwestycyjny"
    origin "text"
  ]
  node [
    id 4
    label "powstanie"
    origin "text"
  ]
  node [
    id 5
    label "projekt"
    origin "text"
  ]
  node [
    id 6
    label "baltic"
    origin "text"
  ]
  node [
    id 7
    label "pipe"
    origin "text"
  ]
  node [
    id 8
    label "strona"
    origin "text"
  ]
  node [
    id 9
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 10
    label "polski"
    origin "text"
  ]
  node [
    id 11
    label "kr&#243;lestwo"
    origin "text"
  ]
  node [
    id 12
    label "dania"
    origin "text"
  ]
  node [
    id 13
    label "wkl&#281;s&#322;y"
  ]
  node [
    id 14
    label "daleki"
  ]
  node [
    id 15
    label "zniszczony"
  ]
  node [
    id 16
    label "dawny"
  ]
  node [
    id 17
    label "ogl&#281;dny"
  ]
  node [
    id 18
    label "d&#322;ugi"
  ]
  node [
    id 19
    label "du&#380;y"
  ]
  node [
    id 20
    label "daleko"
  ]
  node [
    id 21
    label "odleg&#322;y"
  ]
  node [
    id 22
    label "zwi&#261;zany"
  ]
  node [
    id 23
    label "r&#243;&#380;ny"
  ]
  node [
    id 24
    label "s&#322;aby"
  ]
  node [
    id 25
    label "odlegle"
  ]
  node [
    id 26
    label "oddalony"
  ]
  node [
    id 27
    label "g&#322;&#281;boki"
  ]
  node [
    id 28
    label "obcy"
  ]
  node [
    id 29
    label "nieobecny"
  ]
  node [
    id 30
    label "przysz&#322;y"
  ]
  node [
    id 31
    label "wkl&#281;s&#322;o"
  ]
  node [
    id 32
    label "niedobry"
  ]
  node [
    id 33
    label "konieczny"
  ]
  node [
    id 34
    label "ostatecznie"
  ]
  node [
    id 35
    label "zupe&#322;ny"
  ]
  node [
    id 36
    label "skrajny"
  ]
  node [
    id 37
    label "niezb&#281;dnie"
  ]
  node [
    id 38
    label "skrajnie"
  ]
  node [
    id 39
    label "okrajkowy"
  ]
  node [
    id 40
    label "og&#243;lnie"
  ]
  node [
    id 41
    label "w_pizdu"
  ]
  node [
    id 42
    label "ca&#322;y"
  ]
  node [
    id 43
    label "kompletnie"
  ]
  node [
    id 44
    label "&#322;&#261;czny"
  ]
  node [
    id 45
    label "zupe&#322;nie"
  ]
  node [
    id 46
    label "pe&#322;ny"
  ]
  node [
    id 47
    label "finalnie"
  ]
  node [
    id 48
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 49
    label "management"
  ]
  node [
    id 50
    label "resolution"
  ]
  node [
    id 51
    label "wytw&#243;r"
  ]
  node [
    id 52
    label "zdecydowanie"
  ]
  node [
    id 53
    label "dokument"
  ]
  node [
    id 54
    label "zapis"
  ]
  node [
    id 55
    label "&#347;wiadectwo"
  ]
  node [
    id 56
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 57
    label "parafa"
  ]
  node [
    id 58
    label "plik"
  ]
  node [
    id 59
    label "raport&#243;wka"
  ]
  node [
    id 60
    label "utw&#243;r"
  ]
  node [
    id 61
    label "record"
  ]
  node [
    id 62
    label "registratura"
  ]
  node [
    id 63
    label "dokumentacja"
  ]
  node [
    id 64
    label "fascyku&#322;"
  ]
  node [
    id 65
    label "artyku&#322;"
  ]
  node [
    id 66
    label "writing"
  ]
  node [
    id 67
    label "sygnatariusz"
  ]
  node [
    id 68
    label "przedmiot"
  ]
  node [
    id 69
    label "p&#322;&#243;d"
  ]
  node [
    id 70
    label "work"
  ]
  node [
    id 71
    label "rezultat"
  ]
  node [
    id 72
    label "zesp&#243;&#322;"
  ]
  node [
    id 73
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 74
    label "pewnie"
  ]
  node [
    id 75
    label "zdecydowany"
  ]
  node [
    id 76
    label "zauwa&#380;alnie"
  ]
  node [
    id 77
    label "oddzia&#322;anie"
  ]
  node [
    id 78
    label "podj&#281;cie"
  ]
  node [
    id 79
    label "cecha"
  ]
  node [
    id 80
    label "resoluteness"
  ]
  node [
    id 81
    label "judgment"
  ]
  node [
    id 82
    label "zrobienie"
  ]
  node [
    id 83
    label "inwestycyjnie"
  ]
  node [
    id 84
    label "stworzenie"
  ]
  node [
    id 85
    label "walka"
  ]
  node [
    id 86
    label "koliszczyzna"
  ]
  node [
    id 87
    label "zbuntowanie_si&#281;"
  ]
  node [
    id 88
    label "Ko&#347;ciuszko"
  ]
  node [
    id 89
    label "powstanie_tambowskie"
  ]
  node [
    id 90
    label "odbudowanie_si&#281;"
  ]
  node [
    id 91
    label "powstanie_listopadowe"
  ]
  node [
    id 92
    label "origin"
  ]
  node [
    id 93
    label "&#380;akieria"
  ]
  node [
    id 94
    label "zaistnienie"
  ]
  node [
    id 95
    label "potworzenie_si&#281;"
  ]
  node [
    id 96
    label "geneza"
  ]
  node [
    id 97
    label "orgy"
  ]
  node [
    id 98
    label "beginning"
  ]
  node [
    id 99
    label "utworzenie"
  ]
  node [
    id 100
    label "le&#380;enie"
  ]
  node [
    id 101
    label "kl&#281;czenie"
  ]
  node [
    id 102
    label "chmielnicczyzna"
  ]
  node [
    id 103
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 104
    label "pierwocina"
  ]
  node [
    id 105
    label "powstanie_warszawskie"
  ]
  node [
    id 106
    label "uniesienie_si&#281;"
  ]
  node [
    id 107
    label "siedzenie"
  ]
  node [
    id 108
    label "wydarzenie"
  ]
  node [
    id 109
    label "obrona"
  ]
  node [
    id 110
    label "zaatakowanie"
  ]
  node [
    id 111
    label "konfrontacyjny"
  ]
  node [
    id 112
    label "contest"
  ]
  node [
    id 113
    label "action"
  ]
  node [
    id 114
    label "sambo"
  ]
  node [
    id 115
    label "czyn"
  ]
  node [
    id 116
    label "rywalizacja"
  ]
  node [
    id 117
    label "trudno&#347;&#263;"
  ]
  node [
    id 118
    label "sp&#243;r"
  ]
  node [
    id 119
    label "wrestle"
  ]
  node [
    id 120
    label "military_action"
  ]
  node [
    id 121
    label "wojna_stuletnia"
  ]
  node [
    id 122
    label "trwanie"
  ]
  node [
    id 123
    label "wstanie"
  ]
  node [
    id 124
    label "po&#322;o&#380;enie"
  ]
  node [
    id 125
    label "bycie"
  ]
  node [
    id 126
    label "pobyczenie_si&#281;"
  ]
  node [
    id 127
    label "tarzanie_si&#281;"
  ]
  node [
    id 128
    label "zwierz&#281;"
  ]
  node [
    id 129
    label "przele&#380;enie"
  ]
  node [
    id 130
    label "odpowiedni"
  ]
  node [
    id 131
    label "zlegni&#281;cie"
  ]
  node [
    id 132
    label "fit"
  ]
  node [
    id 133
    label "spoczywanie"
  ]
  node [
    id 134
    label "pole&#380;enie"
  ]
  node [
    id 135
    label "odsiedzenie"
  ]
  node [
    id 136
    label "wysiadywanie"
  ]
  node [
    id 137
    label "odsiadywanie"
  ]
  node [
    id 138
    label "otoczenie_si&#281;"
  ]
  node [
    id 139
    label "posiedzenie"
  ]
  node [
    id 140
    label "wysiedzenie"
  ]
  node [
    id 141
    label "posadzenie"
  ]
  node [
    id 142
    label "wychodzenie"
  ]
  node [
    id 143
    label "zajmowanie_si&#281;"
  ]
  node [
    id 144
    label "tkwienie"
  ]
  node [
    id 145
    label "sadzanie"
  ]
  node [
    id 146
    label "trybuna"
  ]
  node [
    id 147
    label "ocieranie_si&#281;"
  ]
  node [
    id 148
    label "room"
  ]
  node [
    id 149
    label "jadalnia"
  ]
  node [
    id 150
    label "miejsce"
  ]
  node [
    id 151
    label "residency"
  ]
  node [
    id 152
    label "pupa"
  ]
  node [
    id 153
    label "samolot"
  ]
  node [
    id 154
    label "touch"
  ]
  node [
    id 155
    label "otarcie_si&#281;"
  ]
  node [
    id 156
    label "position"
  ]
  node [
    id 157
    label "otaczanie_si&#281;"
  ]
  node [
    id 158
    label "wyj&#347;cie"
  ]
  node [
    id 159
    label "przedzia&#322;"
  ]
  node [
    id 160
    label "umieszczenie"
  ]
  node [
    id 161
    label "mieszkanie"
  ]
  node [
    id 162
    label "przebywanie"
  ]
  node [
    id 163
    label "ujmowanie"
  ]
  node [
    id 164
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 165
    label "autobus"
  ]
  node [
    id 166
    label "foundation"
  ]
  node [
    id 167
    label "potworzenie"
  ]
  node [
    id 168
    label "erecting"
  ]
  node [
    id 169
    label "ukszta&#322;towanie"
  ]
  node [
    id 170
    label "stanie_si&#281;"
  ]
  node [
    id 171
    label "zorganizowanie"
  ]
  node [
    id 172
    label "obiekt_naturalny"
  ]
  node [
    id 173
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 174
    label "rzecz"
  ]
  node [
    id 175
    label "environment"
  ]
  node [
    id 176
    label "ekosystem"
  ]
  node [
    id 177
    label "organizm"
  ]
  node [
    id 178
    label "pope&#322;nienie"
  ]
  node [
    id 179
    label "wizerunek"
  ]
  node [
    id 180
    label "wszechstworzenie"
  ]
  node [
    id 181
    label "woda"
  ]
  node [
    id 182
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 183
    label "czynno&#347;&#263;"
  ]
  node [
    id 184
    label "teren"
  ]
  node [
    id 185
    label "mikrokosmos"
  ]
  node [
    id 186
    label "stw&#243;r"
  ]
  node [
    id 187
    label "Ziemia"
  ]
  node [
    id 188
    label "cia&#322;o"
  ]
  node [
    id 189
    label "istota"
  ]
  node [
    id 190
    label "fauna"
  ]
  node [
    id 191
    label "biota"
  ]
  node [
    id 192
    label "czynnik"
  ]
  node [
    id 193
    label "proces"
  ]
  node [
    id 194
    label "rodny"
  ]
  node [
    id 195
    label "monogeneza"
  ]
  node [
    id 196
    label "give"
  ]
  node [
    id 197
    label "pocz&#261;tek"
  ]
  node [
    id 198
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 199
    label "przyczyna"
  ]
  node [
    id 200
    label "apparition"
  ]
  node [
    id 201
    label "nap&#281;dzenie"
  ]
  node [
    id 202
    label "debiut"
  ]
  node [
    id 203
    label "intencja"
  ]
  node [
    id 204
    label "plan"
  ]
  node [
    id 205
    label "device"
  ]
  node [
    id 206
    label "program_u&#380;ytkowy"
  ]
  node [
    id 207
    label "pomys&#322;"
  ]
  node [
    id 208
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 209
    label "agreement"
  ]
  node [
    id 210
    label "thinking"
  ]
  node [
    id 211
    label "model"
  ]
  node [
    id 212
    label "punkt"
  ]
  node [
    id 213
    label "rysunek"
  ]
  node [
    id 214
    label "miejsce_pracy"
  ]
  node [
    id 215
    label "przestrze&#324;"
  ]
  node [
    id 216
    label "obraz"
  ]
  node [
    id 217
    label "reprezentacja"
  ]
  node [
    id 218
    label "dekoracja"
  ]
  node [
    id 219
    label "perspektywa"
  ]
  node [
    id 220
    label "ekscerpcja"
  ]
  node [
    id 221
    label "materia&#322;"
  ]
  node [
    id 222
    label "operat"
  ]
  node [
    id 223
    label "kosztorys"
  ]
  node [
    id 224
    label "pocz&#261;tki"
  ]
  node [
    id 225
    label "ukra&#347;&#263;"
  ]
  node [
    id 226
    label "ukradzenie"
  ]
  node [
    id 227
    label "idea"
  ]
  node [
    id 228
    label "system"
  ]
  node [
    id 229
    label "kartka"
  ]
  node [
    id 230
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 231
    label "logowanie"
  ]
  node [
    id 232
    label "s&#261;d"
  ]
  node [
    id 233
    label "adres_internetowy"
  ]
  node [
    id 234
    label "linia"
  ]
  node [
    id 235
    label "serwis_internetowy"
  ]
  node [
    id 236
    label "posta&#263;"
  ]
  node [
    id 237
    label "bok"
  ]
  node [
    id 238
    label "skr&#281;canie"
  ]
  node [
    id 239
    label "skr&#281;ca&#263;"
  ]
  node [
    id 240
    label "orientowanie"
  ]
  node [
    id 241
    label "skr&#281;ci&#263;"
  ]
  node [
    id 242
    label "uj&#281;cie"
  ]
  node [
    id 243
    label "zorientowanie"
  ]
  node [
    id 244
    label "ty&#322;"
  ]
  node [
    id 245
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 246
    label "fragment"
  ]
  node [
    id 247
    label "layout"
  ]
  node [
    id 248
    label "obiekt"
  ]
  node [
    id 249
    label "zorientowa&#263;"
  ]
  node [
    id 250
    label "pagina"
  ]
  node [
    id 251
    label "podmiot"
  ]
  node [
    id 252
    label "g&#243;ra"
  ]
  node [
    id 253
    label "orientowa&#263;"
  ]
  node [
    id 254
    label "voice"
  ]
  node [
    id 255
    label "orientacja"
  ]
  node [
    id 256
    label "prz&#243;d"
  ]
  node [
    id 257
    label "internet"
  ]
  node [
    id 258
    label "powierzchnia"
  ]
  node [
    id 259
    label "forma"
  ]
  node [
    id 260
    label "skr&#281;cenie"
  ]
  node [
    id 261
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 262
    label "byt"
  ]
  node [
    id 263
    label "cz&#322;owiek"
  ]
  node [
    id 264
    label "osobowo&#347;&#263;"
  ]
  node [
    id 265
    label "organizacja"
  ]
  node [
    id 266
    label "prawo"
  ]
  node [
    id 267
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 268
    label "nauka_prawa"
  ]
  node [
    id 269
    label "charakterystyka"
  ]
  node [
    id 270
    label "zaistnie&#263;"
  ]
  node [
    id 271
    label "Osjan"
  ]
  node [
    id 272
    label "kto&#347;"
  ]
  node [
    id 273
    label "wygl&#261;d"
  ]
  node [
    id 274
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 275
    label "trim"
  ]
  node [
    id 276
    label "poby&#263;"
  ]
  node [
    id 277
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 278
    label "Aspazja"
  ]
  node [
    id 279
    label "punkt_widzenia"
  ]
  node [
    id 280
    label "kompleksja"
  ]
  node [
    id 281
    label "wytrzyma&#263;"
  ]
  node [
    id 282
    label "budowa"
  ]
  node [
    id 283
    label "formacja"
  ]
  node [
    id 284
    label "pozosta&#263;"
  ]
  node [
    id 285
    label "point"
  ]
  node [
    id 286
    label "przedstawienie"
  ]
  node [
    id 287
    label "go&#347;&#263;"
  ]
  node [
    id 288
    label "kszta&#322;t"
  ]
  node [
    id 289
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 290
    label "armia"
  ]
  node [
    id 291
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 292
    label "poprowadzi&#263;"
  ]
  node [
    id 293
    label "cord"
  ]
  node [
    id 294
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 295
    label "trasa"
  ]
  node [
    id 296
    label "po&#322;&#261;czenie"
  ]
  node [
    id 297
    label "tract"
  ]
  node [
    id 298
    label "materia&#322;_zecerski"
  ]
  node [
    id 299
    label "przeorientowywanie"
  ]
  node [
    id 300
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 301
    label "curve"
  ]
  node [
    id 302
    label "figura_geometryczna"
  ]
  node [
    id 303
    label "zbi&#243;r"
  ]
  node [
    id 304
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 305
    label "jard"
  ]
  node [
    id 306
    label "szczep"
  ]
  node [
    id 307
    label "phreaker"
  ]
  node [
    id 308
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 309
    label "grupa_organizm&#243;w"
  ]
  node [
    id 310
    label "prowadzi&#263;"
  ]
  node [
    id 311
    label "przeorientowywa&#263;"
  ]
  node [
    id 312
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 313
    label "access"
  ]
  node [
    id 314
    label "przeorientowanie"
  ]
  node [
    id 315
    label "przeorientowa&#263;"
  ]
  node [
    id 316
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 317
    label "billing"
  ]
  node [
    id 318
    label "granica"
  ]
  node [
    id 319
    label "szpaler"
  ]
  node [
    id 320
    label "sztrych"
  ]
  node [
    id 321
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 322
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 323
    label "drzewo_genealogiczne"
  ]
  node [
    id 324
    label "transporter"
  ]
  node [
    id 325
    label "line"
  ]
  node [
    id 326
    label "przew&#243;d"
  ]
  node [
    id 327
    label "granice"
  ]
  node [
    id 328
    label "kontakt"
  ]
  node [
    id 329
    label "rz&#261;d"
  ]
  node [
    id 330
    label "przewo&#378;nik"
  ]
  node [
    id 331
    label "przystanek"
  ]
  node [
    id 332
    label "linijka"
  ]
  node [
    id 333
    label "spos&#243;b"
  ]
  node [
    id 334
    label "uporz&#261;dkowanie"
  ]
  node [
    id 335
    label "coalescence"
  ]
  node [
    id 336
    label "Ural"
  ]
  node [
    id 337
    label "bearing"
  ]
  node [
    id 338
    label "prowadzenie"
  ]
  node [
    id 339
    label "tekst"
  ]
  node [
    id 340
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 341
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 342
    label "koniec"
  ]
  node [
    id 343
    label "podkatalog"
  ]
  node [
    id 344
    label "nadpisa&#263;"
  ]
  node [
    id 345
    label "nadpisanie"
  ]
  node [
    id 346
    label "bundle"
  ]
  node [
    id 347
    label "folder"
  ]
  node [
    id 348
    label "nadpisywanie"
  ]
  node [
    id 349
    label "paczka"
  ]
  node [
    id 350
    label "nadpisywa&#263;"
  ]
  node [
    id 351
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 352
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 353
    label "Rzym_Zachodni"
  ]
  node [
    id 354
    label "whole"
  ]
  node [
    id 355
    label "ilo&#347;&#263;"
  ]
  node [
    id 356
    label "element"
  ]
  node [
    id 357
    label "Rzym_Wschodni"
  ]
  node [
    id 358
    label "urz&#261;dzenie"
  ]
  node [
    id 359
    label "rozmiar"
  ]
  node [
    id 360
    label "obszar"
  ]
  node [
    id 361
    label "poj&#281;cie"
  ]
  node [
    id 362
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 363
    label "zwierciad&#322;o"
  ]
  node [
    id 364
    label "capacity"
  ]
  node [
    id 365
    label "plane"
  ]
  node [
    id 366
    label "temat"
  ]
  node [
    id 367
    label "jednostka_systematyczna"
  ]
  node [
    id 368
    label "poznanie"
  ]
  node [
    id 369
    label "leksem"
  ]
  node [
    id 370
    label "dzie&#322;o"
  ]
  node [
    id 371
    label "stan"
  ]
  node [
    id 372
    label "blaszka"
  ]
  node [
    id 373
    label "kantyzm"
  ]
  node [
    id 374
    label "zdolno&#347;&#263;"
  ]
  node [
    id 375
    label "do&#322;ek"
  ]
  node [
    id 376
    label "zawarto&#347;&#263;"
  ]
  node [
    id 377
    label "gwiazda"
  ]
  node [
    id 378
    label "formality"
  ]
  node [
    id 379
    label "struktura"
  ]
  node [
    id 380
    label "mode"
  ]
  node [
    id 381
    label "morfem"
  ]
  node [
    id 382
    label "rdze&#324;"
  ]
  node [
    id 383
    label "kielich"
  ]
  node [
    id 384
    label "ornamentyka"
  ]
  node [
    id 385
    label "pasmo"
  ]
  node [
    id 386
    label "zwyczaj"
  ]
  node [
    id 387
    label "g&#322;owa"
  ]
  node [
    id 388
    label "naczynie"
  ]
  node [
    id 389
    label "p&#322;at"
  ]
  node [
    id 390
    label "maszyna_drukarska"
  ]
  node [
    id 391
    label "style"
  ]
  node [
    id 392
    label "linearno&#347;&#263;"
  ]
  node [
    id 393
    label "wyra&#380;enie"
  ]
  node [
    id 394
    label "spirala"
  ]
  node [
    id 395
    label "dyspozycja"
  ]
  node [
    id 396
    label "odmiana"
  ]
  node [
    id 397
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 398
    label "wz&#243;r"
  ]
  node [
    id 399
    label "October"
  ]
  node [
    id 400
    label "creation"
  ]
  node [
    id 401
    label "p&#281;tla"
  ]
  node [
    id 402
    label "arystotelizm"
  ]
  node [
    id 403
    label "szablon"
  ]
  node [
    id 404
    label "miniatura"
  ]
  node [
    id 405
    label "podejrzany"
  ]
  node [
    id 406
    label "s&#261;downictwo"
  ]
  node [
    id 407
    label "biuro"
  ]
  node [
    id 408
    label "court"
  ]
  node [
    id 409
    label "forum"
  ]
  node [
    id 410
    label "bronienie"
  ]
  node [
    id 411
    label "urz&#261;d"
  ]
  node [
    id 412
    label "oskar&#380;yciel"
  ]
  node [
    id 413
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 414
    label "skazany"
  ]
  node [
    id 415
    label "post&#281;powanie"
  ]
  node [
    id 416
    label "broni&#263;"
  ]
  node [
    id 417
    label "my&#347;l"
  ]
  node [
    id 418
    label "pods&#261;dny"
  ]
  node [
    id 419
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 420
    label "wypowied&#378;"
  ]
  node [
    id 421
    label "instytucja"
  ]
  node [
    id 422
    label "antylogizm"
  ]
  node [
    id 423
    label "konektyw"
  ]
  node [
    id 424
    label "&#347;wiadek"
  ]
  node [
    id 425
    label "procesowicz"
  ]
  node [
    id 426
    label "pochwytanie"
  ]
  node [
    id 427
    label "wording"
  ]
  node [
    id 428
    label "wzbudzenie"
  ]
  node [
    id 429
    label "withdrawal"
  ]
  node [
    id 430
    label "capture"
  ]
  node [
    id 431
    label "podniesienie"
  ]
  node [
    id 432
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 433
    label "film"
  ]
  node [
    id 434
    label "scena"
  ]
  node [
    id 435
    label "zapisanie"
  ]
  node [
    id 436
    label "prezentacja"
  ]
  node [
    id 437
    label "rzucenie"
  ]
  node [
    id 438
    label "zamkni&#281;cie"
  ]
  node [
    id 439
    label "zabranie"
  ]
  node [
    id 440
    label "poinformowanie"
  ]
  node [
    id 441
    label "zaaresztowanie"
  ]
  node [
    id 442
    label "wzi&#281;cie"
  ]
  node [
    id 443
    label "kierunek"
  ]
  node [
    id 444
    label "wyznaczenie"
  ]
  node [
    id 445
    label "przyczynienie_si&#281;"
  ]
  node [
    id 446
    label "zwr&#243;cenie"
  ]
  node [
    id 447
    label "zrozumienie"
  ]
  node [
    id 448
    label "tu&#322;&#243;w"
  ]
  node [
    id 449
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 450
    label "wielok&#261;t"
  ]
  node [
    id 451
    label "odcinek"
  ]
  node [
    id 452
    label "strzelba"
  ]
  node [
    id 453
    label "lufa"
  ]
  node [
    id 454
    label "&#347;ciana"
  ]
  node [
    id 455
    label "set"
  ]
  node [
    id 456
    label "orient"
  ]
  node [
    id 457
    label "eastern_hemisphere"
  ]
  node [
    id 458
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 459
    label "aim"
  ]
  node [
    id 460
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 461
    label "wyznaczy&#263;"
  ]
  node [
    id 462
    label "wrench"
  ]
  node [
    id 463
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 464
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 465
    label "sple&#347;&#263;"
  ]
  node [
    id 466
    label "os&#322;abi&#263;"
  ]
  node [
    id 467
    label "nawin&#261;&#263;"
  ]
  node [
    id 468
    label "scali&#263;"
  ]
  node [
    id 469
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 470
    label "twist"
  ]
  node [
    id 471
    label "splay"
  ]
  node [
    id 472
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 473
    label "uszkodzi&#263;"
  ]
  node [
    id 474
    label "break"
  ]
  node [
    id 475
    label "flex"
  ]
  node [
    id 476
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 477
    label "os&#322;abia&#263;"
  ]
  node [
    id 478
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 479
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 480
    label "splata&#263;"
  ]
  node [
    id 481
    label "throw"
  ]
  node [
    id 482
    label "screw"
  ]
  node [
    id 483
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 484
    label "scala&#263;"
  ]
  node [
    id 485
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 486
    label "przelezienie"
  ]
  node [
    id 487
    label "&#347;piew"
  ]
  node [
    id 488
    label "Synaj"
  ]
  node [
    id 489
    label "Kreml"
  ]
  node [
    id 490
    label "d&#378;wi&#281;k"
  ]
  node [
    id 491
    label "wysoki"
  ]
  node [
    id 492
    label "wzniesienie"
  ]
  node [
    id 493
    label "grupa"
  ]
  node [
    id 494
    label "pi&#281;tro"
  ]
  node [
    id 495
    label "Ropa"
  ]
  node [
    id 496
    label "kupa"
  ]
  node [
    id 497
    label "przele&#378;&#263;"
  ]
  node [
    id 498
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 499
    label "karczek"
  ]
  node [
    id 500
    label "rami&#261;czko"
  ]
  node [
    id 501
    label "Jaworze"
  ]
  node [
    id 502
    label "odchylanie_si&#281;"
  ]
  node [
    id 503
    label "kszta&#322;towanie"
  ]
  node [
    id 504
    label "os&#322;abianie"
  ]
  node [
    id 505
    label "uprz&#281;dzenie"
  ]
  node [
    id 506
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 507
    label "scalanie"
  ]
  node [
    id 508
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 509
    label "snucie"
  ]
  node [
    id 510
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 511
    label "tortuosity"
  ]
  node [
    id 512
    label "odbijanie"
  ]
  node [
    id 513
    label "contortion"
  ]
  node [
    id 514
    label "splatanie"
  ]
  node [
    id 515
    label "turn"
  ]
  node [
    id 516
    label "nawini&#281;cie"
  ]
  node [
    id 517
    label "os&#322;abienie"
  ]
  node [
    id 518
    label "uszkodzenie"
  ]
  node [
    id 519
    label "odbicie"
  ]
  node [
    id 520
    label "poskr&#281;canie"
  ]
  node [
    id 521
    label "uraz"
  ]
  node [
    id 522
    label "odchylenie_si&#281;"
  ]
  node [
    id 523
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 524
    label "z&#322;&#261;czenie"
  ]
  node [
    id 525
    label "splecenie"
  ]
  node [
    id 526
    label "turning"
  ]
  node [
    id 527
    label "kierowa&#263;"
  ]
  node [
    id 528
    label "inform"
  ]
  node [
    id 529
    label "marshal"
  ]
  node [
    id 530
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 531
    label "wyznacza&#263;"
  ]
  node [
    id 532
    label "pomaga&#263;"
  ]
  node [
    id 533
    label "pomaganie"
  ]
  node [
    id 534
    label "orientation"
  ]
  node [
    id 535
    label "przyczynianie_si&#281;"
  ]
  node [
    id 536
    label "zwracanie"
  ]
  node [
    id 537
    label "rozeznawanie"
  ]
  node [
    id 538
    label "oznaczanie"
  ]
  node [
    id 539
    label "seksualno&#347;&#263;"
  ]
  node [
    id 540
    label "wiedza"
  ]
  node [
    id 541
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 542
    label "zorientowanie_si&#281;"
  ]
  node [
    id 543
    label "pogubienie_si&#281;"
  ]
  node [
    id 544
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 545
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 546
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 547
    label "gubienie_si&#281;"
  ]
  node [
    id 548
    label "zaty&#322;"
  ]
  node [
    id 549
    label "figura"
  ]
  node [
    id 550
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 551
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 552
    label "uwierzytelnienie"
  ]
  node [
    id 553
    label "liczba"
  ]
  node [
    id 554
    label "circumference"
  ]
  node [
    id 555
    label "cyrkumferencja"
  ]
  node [
    id 556
    label "provider"
  ]
  node [
    id 557
    label "hipertekst"
  ]
  node [
    id 558
    label "cyberprzestrze&#324;"
  ]
  node [
    id 559
    label "mem"
  ]
  node [
    id 560
    label "gra_sieciowa"
  ]
  node [
    id 561
    label "grooming"
  ]
  node [
    id 562
    label "media"
  ]
  node [
    id 563
    label "biznes_elektroniczny"
  ]
  node [
    id 564
    label "sie&#263;_komputerowa"
  ]
  node [
    id 565
    label "punkt_dost&#281;pu"
  ]
  node [
    id 566
    label "us&#322;uga_internetowa"
  ]
  node [
    id 567
    label "netbook"
  ]
  node [
    id 568
    label "e-hazard"
  ]
  node [
    id 569
    label "podcast"
  ]
  node [
    id 570
    label "co&#347;"
  ]
  node [
    id 571
    label "budynek"
  ]
  node [
    id 572
    label "thing"
  ]
  node [
    id 573
    label "program"
  ]
  node [
    id 574
    label "faul"
  ]
  node [
    id 575
    label "wk&#322;ad"
  ]
  node [
    id 576
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 577
    label "s&#281;dzia"
  ]
  node [
    id 578
    label "bon"
  ]
  node [
    id 579
    label "ticket"
  ]
  node [
    id 580
    label "arkusz"
  ]
  node [
    id 581
    label "kartonik"
  ]
  node [
    id 582
    label "kara"
  ]
  node [
    id 583
    label "pagination"
  ]
  node [
    id 584
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 585
    label "numer"
  ]
  node [
    id 586
    label "Karelia"
  ]
  node [
    id 587
    label "Ka&#322;mucja"
  ]
  node [
    id 588
    label "Mari_El"
  ]
  node [
    id 589
    label "Inguszetia"
  ]
  node [
    id 590
    label "Udmurcja"
  ]
  node [
    id 591
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 592
    label "Singapur"
  ]
  node [
    id 593
    label "Ad&#380;aria"
  ]
  node [
    id 594
    label "Karaka&#322;pacja"
  ]
  node [
    id 595
    label "Czeczenia"
  ]
  node [
    id 596
    label "Abchazja"
  ]
  node [
    id 597
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 598
    label "Tatarstan"
  ]
  node [
    id 599
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 600
    label "pa&#324;stwo"
  ]
  node [
    id 601
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 602
    label "Baszkiria"
  ]
  node [
    id 603
    label "Jakucja"
  ]
  node [
    id 604
    label "Dagestan"
  ]
  node [
    id 605
    label "Buriacja"
  ]
  node [
    id 606
    label "Tuwa"
  ]
  node [
    id 607
    label "Komi"
  ]
  node [
    id 608
    label "Czuwaszja"
  ]
  node [
    id 609
    label "Chakasja"
  ]
  node [
    id 610
    label "Nachiczewan"
  ]
  node [
    id 611
    label "Mordowia"
  ]
  node [
    id 612
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 613
    label "Katar"
  ]
  node [
    id 614
    label "Libia"
  ]
  node [
    id 615
    label "Gwatemala"
  ]
  node [
    id 616
    label "Ekwador"
  ]
  node [
    id 617
    label "Afganistan"
  ]
  node [
    id 618
    label "Tad&#380;ykistan"
  ]
  node [
    id 619
    label "Bhutan"
  ]
  node [
    id 620
    label "Argentyna"
  ]
  node [
    id 621
    label "D&#380;ibuti"
  ]
  node [
    id 622
    label "Wenezuela"
  ]
  node [
    id 623
    label "Gabon"
  ]
  node [
    id 624
    label "Ukraina"
  ]
  node [
    id 625
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 626
    label "Rwanda"
  ]
  node [
    id 627
    label "Liechtenstein"
  ]
  node [
    id 628
    label "Sri_Lanka"
  ]
  node [
    id 629
    label "Madagaskar"
  ]
  node [
    id 630
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 631
    label "Kongo"
  ]
  node [
    id 632
    label "Tonga"
  ]
  node [
    id 633
    label "Bangladesz"
  ]
  node [
    id 634
    label "Kanada"
  ]
  node [
    id 635
    label "Wehrlen"
  ]
  node [
    id 636
    label "Algieria"
  ]
  node [
    id 637
    label "Uganda"
  ]
  node [
    id 638
    label "Surinam"
  ]
  node [
    id 639
    label "Sahara_Zachodnia"
  ]
  node [
    id 640
    label "Chile"
  ]
  node [
    id 641
    label "W&#281;gry"
  ]
  node [
    id 642
    label "Birma"
  ]
  node [
    id 643
    label "Kazachstan"
  ]
  node [
    id 644
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 645
    label "Armenia"
  ]
  node [
    id 646
    label "Tuwalu"
  ]
  node [
    id 647
    label "Timor_Wschodni"
  ]
  node [
    id 648
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 649
    label "Izrael"
  ]
  node [
    id 650
    label "Estonia"
  ]
  node [
    id 651
    label "Komory"
  ]
  node [
    id 652
    label "Kamerun"
  ]
  node [
    id 653
    label "Haiti"
  ]
  node [
    id 654
    label "Belize"
  ]
  node [
    id 655
    label "Sierra_Leone"
  ]
  node [
    id 656
    label "Luksemburg"
  ]
  node [
    id 657
    label "USA"
  ]
  node [
    id 658
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 659
    label "Barbados"
  ]
  node [
    id 660
    label "San_Marino"
  ]
  node [
    id 661
    label "Bu&#322;garia"
  ]
  node [
    id 662
    label "Indonezja"
  ]
  node [
    id 663
    label "Wietnam"
  ]
  node [
    id 664
    label "Malawi"
  ]
  node [
    id 665
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 666
    label "Francja"
  ]
  node [
    id 667
    label "partia"
  ]
  node [
    id 668
    label "Zambia"
  ]
  node [
    id 669
    label "Angola"
  ]
  node [
    id 670
    label "Grenada"
  ]
  node [
    id 671
    label "Nepal"
  ]
  node [
    id 672
    label "Panama"
  ]
  node [
    id 673
    label "Rumunia"
  ]
  node [
    id 674
    label "Czarnog&#243;ra"
  ]
  node [
    id 675
    label "Malediwy"
  ]
  node [
    id 676
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 677
    label "S&#322;owacja"
  ]
  node [
    id 678
    label "para"
  ]
  node [
    id 679
    label "Egipt"
  ]
  node [
    id 680
    label "zwrot"
  ]
  node [
    id 681
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 682
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 683
    label "Mozambik"
  ]
  node [
    id 684
    label "Kolumbia"
  ]
  node [
    id 685
    label "Laos"
  ]
  node [
    id 686
    label "Burundi"
  ]
  node [
    id 687
    label "Suazi"
  ]
  node [
    id 688
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 689
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 690
    label "Czechy"
  ]
  node [
    id 691
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 692
    label "Wyspy_Marshalla"
  ]
  node [
    id 693
    label "Dominika"
  ]
  node [
    id 694
    label "Trynidad_i_Tobago"
  ]
  node [
    id 695
    label "Syria"
  ]
  node [
    id 696
    label "Palau"
  ]
  node [
    id 697
    label "Gwinea_Bissau"
  ]
  node [
    id 698
    label "Liberia"
  ]
  node [
    id 699
    label "Jamajka"
  ]
  node [
    id 700
    label "Zimbabwe"
  ]
  node [
    id 701
    label "Polska"
  ]
  node [
    id 702
    label "Dominikana"
  ]
  node [
    id 703
    label "Senegal"
  ]
  node [
    id 704
    label "Togo"
  ]
  node [
    id 705
    label "Gujana"
  ]
  node [
    id 706
    label "Gruzja"
  ]
  node [
    id 707
    label "Albania"
  ]
  node [
    id 708
    label "Zair"
  ]
  node [
    id 709
    label "Meksyk"
  ]
  node [
    id 710
    label "Macedonia"
  ]
  node [
    id 711
    label "Chorwacja"
  ]
  node [
    id 712
    label "Kambod&#380;a"
  ]
  node [
    id 713
    label "Monako"
  ]
  node [
    id 714
    label "Mauritius"
  ]
  node [
    id 715
    label "Gwinea"
  ]
  node [
    id 716
    label "Mali"
  ]
  node [
    id 717
    label "Nigeria"
  ]
  node [
    id 718
    label "Kostaryka"
  ]
  node [
    id 719
    label "Hanower"
  ]
  node [
    id 720
    label "Paragwaj"
  ]
  node [
    id 721
    label "W&#322;ochy"
  ]
  node [
    id 722
    label "Seszele"
  ]
  node [
    id 723
    label "Wyspy_Salomona"
  ]
  node [
    id 724
    label "Hiszpania"
  ]
  node [
    id 725
    label "Boliwia"
  ]
  node [
    id 726
    label "Kirgistan"
  ]
  node [
    id 727
    label "Irlandia"
  ]
  node [
    id 728
    label "Czad"
  ]
  node [
    id 729
    label "Irak"
  ]
  node [
    id 730
    label "Lesoto"
  ]
  node [
    id 731
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 732
    label "Malta"
  ]
  node [
    id 733
    label "Andora"
  ]
  node [
    id 734
    label "Chiny"
  ]
  node [
    id 735
    label "Filipiny"
  ]
  node [
    id 736
    label "Antarktis"
  ]
  node [
    id 737
    label "Niemcy"
  ]
  node [
    id 738
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 739
    label "Pakistan"
  ]
  node [
    id 740
    label "terytorium"
  ]
  node [
    id 741
    label "Nikaragua"
  ]
  node [
    id 742
    label "Brazylia"
  ]
  node [
    id 743
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 744
    label "Maroko"
  ]
  node [
    id 745
    label "Portugalia"
  ]
  node [
    id 746
    label "Niger"
  ]
  node [
    id 747
    label "Kenia"
  ]
  node [
    id 748
    label "Botswana"
  ]
  node [
    id 749
    label "Fid&#380;i"
  ]
  node [
    id 750
    label "Tunezja"
  ]
  node [
    id 751
    label "Australia"
  ]
  node [
    id 752
    label "Tajlandia"
  ]
  node [
    id 753
    label "Burkina_Faso"
  ]
  node [
    id 754
    label "interior"
  ]
  node [
    id 755
    label "Tanzania"
  ]
  node [
    id 756
    label "Benin"
  ]
  node [
    id 757
    label "Indie"
  ]
  node [
    id 758
    label "&#321;otwa"
  ]
  node [
    id 759
    label "Kiribati"
  ]
  node [
    id 760
    label "Antigua_i_Barbuda"
  ]
  node [
    id 761
    label "Rodezja"
  ]
  node [
    id 762
    label "Cypr"
  ]
  node [
    id 763
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 764
    label "Peru"
  ]
  node [
    id 765
    label "Austria"
  ]
  node [
    id 766
    label "Urugwaj"
  ]
  node [
    id 767
    label "Jordania"
  ]
  node [
    id 768
    label "Grecja"
  ]
  node [
    id 769
    label "Azerbejd&#380;an"
  ]
  node [
    id 770
    label "Turcja"
  ]
  node [
    id 771
    label "Samoa"
  ]
  node [
    id 772
    label "Sudan"
  ]
  node [
    id 773
    label "Oman"
  ]
  node [
    id 774
    label "ziemia"
  ]
  node [
    id 775
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 776
    label "Uzbekistan"
  ]
  node [
    id 777
    label "Portoryko"
  ]
  node [
    id 778
    label "Honduras"
  ]
  node [
    id 779
    label "Mongolia"
  ]
  node [
    id 780
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 781
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 782
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 783
    label "Serbia"
  ]
  node [
    id 784
    label "Tajwan"
  ]
  node [
    id 785
    label "Wielka_Brytania"
  ]
  node [
    id 786
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 787
    label "Liban"
  ]
  node [
    id 788
    label "Japonia"
  ]
  node [
    id 789
    label "Ghana"
  ]
  node [
    id 790
    label "Belgia"
  ]
  node [
    id 791
    label "Bahrajn"
  ]
  node [
    id 792
    label "Mikronezja"
  ]
  node [
    id 793
    label "Etiopia"
  ]
  node [
    id 794
    label "Kuwejt"
  ]
  node [
    id 795
    label "Bahamy"
  ]
  node [
    id 796
    label "Rosja"
  ]
  node [
    id 797
    label "Mo&#322;dawia"
  ]
  node [
    id 798
    label "Litwa"
  ]
  node [
    id 799
    label "S&#322;owenia"
  ]
  node [
    id 800
    label "Szwajcaria"
  ]
  node [
    id 801
    label "Erytrea"
  ]
  node [
    id 802
    label "Arabia_Saudyjska"
  ]
  node [
    id 803
    label "Kuba"
  ]
  node [
    id 804
    label "granica_pa&#324;stwa"
  ]
  node [
    id 805
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 806
    label "Malezja"
  ]
  node [
    id 807
    label "Korea"
  ]
  node [
    id 808
    label "Jemen"
  ]
  node [
    id 809
    label "Nowa_Zelandia"
  ]
  node [
    id 810
    label "Namibia"
  ]
  node [
    id 811
    label "Nauru"
  ]
  node [
    id 812
    label "holoarktyka"
  ]
  node [
    id 813
    label "Brunei"
  ]
  node [
    id 814
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 815
    label "Khitai"
  ]
  node [
    id 816
    label "Mauretania"
  ]
  node [
    id 817
    label "Iran"
  ]
  node [
    id 818
    label "Gambia"
  ]
  node [
    id 819
    label "Somalia"
  ]
  node [
    id 820
    label "Holandia"
  ]
  node [
    id 821
    label "Turkmenistan"
  ]
  node [
    id 822
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 823
    label "Salwador"
  ]
  node [
    id 824
    label "Kaukaz"
  ]
  node [
    id 825
    label "Federacja_Rosyjska"
  ]
  node [
    id 826
    label "Zyrianka"
  ]
  node [
    id 827
    label "Syberia_Wschodnia"
  ]
  node [
    id 828
    label "Zabajkale"
  ]
  node [
    id 829
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 830
    label "dolar_singapurski"
  ]
  node [
    id 831
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 832
    label "Wepska_Gmina_Narodowa"
  ]
  node [
    id 833
    label "Polish"
  ]
  node [
    id 834
    label "goniony"
  ]
  node [
    id 835
    label "oberek"
  ]
  node [
    id 836
    label "ryba_po_grecku"
  ]
  node [
    id 837
    label "sztajer"
  ]
  node [
    id 838
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 839
    label "krakowiak"
  ]
  node [
    id 840
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 841
    label "pierogi_ruskie"
  ]
  node [
    id 842
    label "lacki"
  ]
  node [
    id 843
    label "polak"
  ]
  node [
    id 844
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 845
    label "chodzony"
  ]
  node [
    id 846
    label "po_polsku"
  ]
  node [
    id 847
    label "mazur"
  ]
  node [
    id 848
    label "polsko"
  ]
  node [
    id 849
    label "skoczny"
  ]
  node [
    id 850
    label "drabant"
  ]
  node [
    id 851
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 852
    label "j&#281;zyk"
  ]
  node [
    id 853
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 854
    label "artykulator"
  ]
  node [
    id 855
    label "kod"
  ]
  node [
    id 856
    label "kawa&#322;ek"
  ]
  node [
    id 857
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 858
    label "gramatyka"
  ]
  node [
    id 859
    label "stylik"
  ]
  node [
    id 860
    label "przet&#322;umaczenie"
  ]
  node [
    id 861
    label "formalizowanie"
  ]
  node [
    id 862
    label "ssanie"
  ]
  node [
    id 863
    label "ssa&#263;"
  ]
  node [
    id 864
    label "language"
  ]
  node [
    id 865
    label "liza&#263;"
  ]
  node [
    id 866
    label "napisa&#263;"
  ]
  node [
    id 867
    label "konsonantyzm"
  ]
  node [
    id 868
    label "wokalizm"
  ]
  node [
    id 869
    label "pisa&#263;"
  ]
  node [
    id 870
    label "fonetyka"
  ]
  node [
    id 871
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 872
    label "jeniec"
  ]
  node [
    id 873
    label "but"
  ]
  node [
    id 874
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 875
    label "po_koroniarsku"
  ]
  node [
    id 876
    label "kultura_duchowa"
  ]
  node [
    id 877
    label "t&#322;umaczenie"
  ]
  node [
    id 878
    label "m&#243;wienie"
  ]
  node [
    id 879
    label "pype&#263;"
  ]
  node [
    id 880
    label "lizanie"
  ]
  node [
    id 881
    label "pismo"
  ]
  node [
    id 882
    label "formalizowa&#263;"
  ]
  node [
    id 883
    label "rozumie&#263;"
  ]
  node [
    id 884
    label "organ"
  ]
  node [
    id 885
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 886
    label "rozumienie"
  ]
  node [
    id 887
    label "makroglosja"
  ]
  node [
    id 888
    label "m&#243;wi&#263;"
  ]
  node [
    id 889
    label "jama_ustna"
  ]
  node [
    id 890
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 891
    label "formacja_geologiczna"
  ]
  node [
    id 892
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 893
    label "natural_language"
  ]
  node [
    id 894
    label "s&#322;ownictwo"
  ]
  node [
    id 895
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 896
    label "wschodnioeuropejski"
  ]
  node [
    id 897
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 898
    label "poga&#324;ski"
  ]
  node [
    id 899
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 900
    label "topielec"
  ]
  node [
    id 901
    label "europejski"
  ]
  node [
    id 902
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 903
    label "langosz"
  ]
  node [
    id 904
    label "zboczenie"
  ]
  node [
    id 905
    label "om&#243;wienie"
  ]
  node [
    id 906
    label "sponiewieranie"
  ]
  node [
    id 907
    label "discipline"
  ]
  node [
    id 908
    label "omawia&#263;"
  ]
  node [
    id 909
    label "kr&#261;&#380;enie"
  ]
  node [
    id 910
    label "tre&#347;&#263;"
  ]
  node [
    id 911
    label "robienie"
  ]
  node [
    id 912
    label "sponiewiera&#263;"
  ]
  node [
    id 913
    label "entity"
  ]
  node [
    id 914
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 915
    label "tematyka"
  ]
  node [
    id 916
    label "w&#261;tek"
  ]
  node [
    id 917
    label "charakter"
  ]
  node [
    id 918
    label "zbaczanie"
  ]
  node [
    id 919
    label "program_nauczania"
  ]
  node [
    id 920
    label "om&#243;wi&#263;"
  ]
  node [
    id 921
    label "omawianie"
  ]
  node [
    id 922
    label "kultura"
  ]
  node [
    id 923
    label "zbacza&#263;"
  ]
  node [
    id 924
    label "zboczy&#263;"
  ]
  node [
    id 925
    label "gwardzista"
  ]
  node [
    id 926
    label "melodia"
  ]
  node [
    id 927
    label "taniec"
  ]
  node [
    id 928
    label "taniec_ludowy"
  ]
  node [
    id 929
    label "&#347;redniowieczny"
  ]
  node [
    id 930
    label "specjalny"
  ]
  node [
    id 931
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 932
    label "weso&#322;y"
  ]
  node [
    id 933
    label "sprawny"
  ]
  node [
    id 934
    label "rytmiczny"
  ]
  node [
    id 935
    label "skocznie"
  ]
  node [
    id 936
    label "energiczny"
  ]
  node [
    id 937
    label "lendler"
  ]
  node [
    id 938
    label "austriacki"
  ]
  node [
    id 939
    label "polka"
  ]
  node [
    id 940
    label "europejsko"
  ]
  node [
    id 941
    label "przytup"
  ]
  node [
    id 942
    label "ho&#322;ubiec"
  ]
  node [
    id 943
    label "wodzi&#263;"
  ]
  node [
    id 944
    label "ludowy"
  ]
  node [
    id 945
    label "pie&#347;&#324;"
  ]
  node [
    id 946
    label "mieszkaniec"
  ]
  node [
    id 947
    label "centu&#347;"
  ]
  node [
    id 948
    label "lalka"
  ]
  node [
    id 949
    label "Ma&#322;opolanin"
  ]
  node [
    id 950
    label "krakauer"
  ]
  node [
    id 951
    label "ro&#347;liny"
  ]
  node [
    id 952
    label "grzyby"
  ]
  node [
    id 953
    label "Arktogea"
  ]
  node [
    id 954
    label "prokarioty"
  ]
  node [
    id 955
    label "zwierz&#281;ta"
  ]
  node [
    id 956
    label "domena"
  ]
  node [
    id 957
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 958
    label "protisty"
  ]
  node [
    id 959
    label "kategoria_systematyczna"
  ]
  node [
    id 960
    label "Wile&#324;szczyzna"
  ]
  node [
    id 961
    label "jednostka_administracyjna"
  ]
  node [
    id 962
    label "Jukon"
  ]
  node [
    id 963
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 964
    label "sfera"
  ]
  node [
    id 965
    label "zakres"
  ]
  node [
    id 966
    label "bezdro&#380;e"
  ]
  node [
    id 967
    label "j&#261;drowce"
  ]
  node [
    id 968
    label "subdomena"
  ]
  node [
    id 969
    label "maj&#261;tek"
  ]
  node [
    id 970
    label "adres_elektroniczny"
  ]
  node [
    id 971
    label "poddzia&#322;"
  ]
  node [
    id 972
    label "feudalizm"
  ]
  node [
    id 973
    label "archeony"
  ]
  node [
    id 974
    label "posiad&#322;o&#347;&#263;"
  ]
  node [
    id 975
    label "prokaryote"
  ]
  node [
    id 976
    label "wyprze&#263;"
  ]
  node [
    id 977
    label "Baltic"
  ]
  node [
    id 978
    label "Pipe"
  ]
  node [
    id 979
    label "Dania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 881
  ]
  edge [
    source 10
    target 882
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 884
  ]
  edge [
    source 10
    target 885
  ]
  edge [
    source 10
    target 886
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 887
  ]
  edge [
    source 10
    target 888
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 10
    target 891
  ]
  edge [
    source 10
    target 892
  ]
  edge [
    source 10
    target 893
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 895
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 10
    target 904
  ]
  edge [
    source 10
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 930
  ]
  edge [
    source 10
    target 931
  ]
  edge [
    source 10
    target 932
  ]
  edge [
    source 10
    target 933
  ]
  edge [
    source 10
    target 934
  ]
  edge [
    source 10
    target 935
  ]
  edge [
    source 10
    target 936
  ]
  edge [
    source 10
    target 937
  ]
  edge [
    source 10
    target 938
  ]
  edge [
    source 10
    target 939
  ]
  edge [
    source 10
    target 940
  ]
  edge [
    source 10
    target 941
  ]
  edge [
    source 10
    target 942
  ]
  edge [
    source 10
    target 943
  ]
  edge [
    source 10
    target 944
  ]
  edge [
    source 10
    target 945
  ]
  edge [
    source 10
    target 946
  ]
  edge [
    source 10
    target 947
  ]
  edge [
    source 10
    target 948
  ]
  edge [
    source 10
    target 949
  ]
  edge [
    source 10
    target 950
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 951
  ]
  edge [
    source 11
    target 952
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 11
    target 962
  ]
  edge [
    source 11
    target 963
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 964
  ]
  edge [
    source 11
    target 965
  ]
  edge [
    source 11
    target 966
  ]
  edge [
    source 11
    target 967
  ]
  edge [
    source 11
    target 968
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 970
  ]
  edge [
    source 11
    target 971
  ]
  edge [
    source 11
    target 972
  ]
  edge [
    source 11
    target 973
  ]
  edge [
    source 11
    target 974
  ]
  edge [
    source 11
    target 975
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 976
  ]
  edge [
    source 11
    target 979
  ]
  edge [
    source 977
    target 978
  ]
]
