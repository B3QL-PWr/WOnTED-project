graph [
  node [
    id 0
    label "all"
    origin "text"
  ]
  node [
    id 1
    label "your"
    origin "text"
  ]
  node [
    id 2
    label "content"
    origin "text"
  ]
  node [
    id 3
    label "are"
    origin "text"
  ]
  node [
    id 4
    label "belong"
    origin "text"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
]
