graph [
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "juz"
    origin "text"
  ]
  node [
    id 3
    label "leca"
    origin "text"
  ]
  node [
    id 4
    label "marek"
    origin "text"
  ]
  node [
    id 5
    label "mowi"
    origin "text"
  ]
  node [
    id 6
    label "wygladam"
    origin "text"
  ]
  node [
    id 7
    label "lato"
    origin "text"
  ]
  node [
    id 8
    label "logikarozowychpaskow"
    origin "text"
  ]
  node [
    id 9
    label "atencyjnyrozowypasek"
    origin "text"
  ]
  node [
    id 10
    label "pytanie"
    origin "text"
  ]
  node [
    id 11
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 12
    label "jednostka_monetarna"
  ]
  node [
    id 13
    label "centym"
  ]
  node [
    id 14
    label "Wilko"
  ]
  node [
    id 15
    label "mienie"
  ]
  node [
    id 16
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 17
    label "frymark"
  ]
  node [
    id 18
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 19
    label "commodity"
  ]
  node [
    id 20
    label "integer"
  ]
  node [
    id 21
    label "liczba"
  ]
  node [
    id 22
    label "zlewanie_si&#281;"
  ]
  node [
    id 23
    label "ilo&#347;&#263;"
  ]
  node [
    id 24
    label "uk&#322;ad"
  ]
  node [
    id 25
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 26
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 27
    label "pe&#322;ny"
  ]
  node [
    id 28
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 29
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 31
    label "stan"
  ]
  node [
    id 32
    label "rzecz"
  ]
  node [
    id 33
    label "immoblizacja"
  ]
  node [
    id 34
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 35
    label "przej&#347;cie"
  ]
  node [
    id 36
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 37
    label "rodowo&#347;&#263;"
  ]
  node [
    id 38
    label "patent"
  ]
  node [
    id 39
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 40
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 41
    label "przej&#347;&#263;"
  ]
  node [
    id 42
    label "possession"
  ]
  node [
    id 43
    label "zamiana"
  ]
  node [
    id 44
    label "maj&#261;tek"
  ]
  node [
    id 45
    label "Iwaszkiewicz"
  ]
  node [
    id 46
    label "cognizance"
  ]
  node [
    id 47
    label "hygrofit"
  ]
  node [
    id 48
    label "bylina"
  ]
  node [
    id 49
    label "selerowate"
  ]
  node [
    id 50
    label "higrofil"
  ]
  node [
    id 51
    label "ro&#347;lina"
  ]
  node [
    id 52
    label "ludowy"
  ]
  node [
    id 53
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 54
    label "utw&#243;r_epicki"
  ]
  node [
    id 55
    label "pie&#347;&#324;"
  ]
  node [
    id 56
    label "selerowce"
  ]
  node [
    id 57
    label "pora_roku"
  ]
  node [
    id 58
    label "sprawa"
  ]
  node [
    id 59
    label "wypytanie"
  ]
  node [
    id 60
    label "egzaminowanie"
  ]
  node [
    id 61
    label "zwracanie_si&#281;"
  ]
  node [
    id 62
    label "wywo&#322;ywanie"
  ]
  node [
    id 63
    label "rozpytywanie"
  ]
  node [
    id 64
    label "wypowiedzenie"
  ]
  node [
    id 65
    label "wypowied&#378;"
  ]
  node [
    id 66
    label "problemat"
  ]
  node [
    id 67
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 68
    label "problematyka"
  ]
  node [
    id 69
    label "sprawdzian"
  ]
  node [
    id 70
    label "zadanie"
  ]
  node [
    id 71
    label "odpowiada&#263;"
  ]
  node [
    id 72
    label "przes&#322;uchiwanie"
  ]
  node [
    id 73
    label "question"
  ]
  node [
    id 74
    label "sprawdzanie"
  ]
  node [
    id 75
    label "odpowiadanie"
  ]
  node [
    id 76
    label "survey"
  ]
  node [
    id 77
    label "pos&#322;uchanie"
  ]
  node [
    id 78
    label "s&#261;d"
  ]
  node [
    id 79
    label "sparafrazowanie"
  ]
  node [
    id 80
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 81
    label "strawestowa&#263;"
  ]
  node [
    id 82
    label "sparafrazowa&#263;"
  ]
  node [
    id 83
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 84
    label "trawestowa&#263;"
  ]
  node [
    id 85
    label "sformu&#322;owanie"
  ]
  node [
    id 86
    label "parafrazowanie"
  ]
  node [
    id 87
    label "ozdobnik"
  ]
  node [
    id 88
    label "delimitacja"
  ]
  node [
    id 89
    label "parafrazowa&#263;"
  ]
  node [
    id 90
    label "stylizacja"
  ]
  node [
    id 91
    label "komunikat"
  ]
  node [
    id 92
    label "trawestowanie"
  ]
  node [
    id 93
    label "strawestowanie"
  ]
  node [
    id 94
    label "rezultat"
  ]
  node [
    id 95
    label "konwersja"
  ]
  node [
    id 96
    label "notice"
  ]
  node [
    id 97
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 98
    label "przepowiedzenie"
  ]
  node [
    id 99
    label "rozwi&#261;zanie"
  ]
  node [
    id 100
    label "generowa&#263;"
  ]
  node [
    id 101
    label "wydanie"
  ]
  node [
    id 102
    label "message"
  ]
  node [
    id 103
    label "generowanie"
  ]
  node [
    id 104
    label "wydobycie"
  ]
  node [
    id 105
    label "zwerbalizowanie"
  ]
  node [
    id 106
    label "szyk"
  ]
  node [
    id 107
    label "notification"
  ]
  node [
    id 108
    label "powiedzenie"
  ]
  node [
    id 109
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 110
    label "denunciation"
  ]
  node [
    id 111
    label "wyra&#380;enie"
  ]
  node [
    id 112
    label "zaj&#281;cie"
  ]
  node [
    id 113
    label "yield"
  ]
  node [
    id 114
    label "zbi&#243;r"
  ]
  node [
    id 115
    label "zaszkodzenie"
  ]
  node [
    id 116
    label "za&#322;o&#380;enie"
  ]
  node [
    id 117
    label "duty"
  ]
  node [
    id 118
    label "powierzanie"
  ]
  node [
    id 119
    label "work"
  ]
  node [
    id 120
    label "problem"
  ]
  node [
    id 121
    label "przepisanie"
  ]
  node [
    id 122
    label "nakarmienie"
  ]
  node [
    id 123
    label "przepisa&#263;"
  ]
  node [
    id 124
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 125
    label "czynno&#347;&#263;"
  ]
  node [
    id 126
    label "zobowi&#261;zanie"
  ]
  node [
    id 127
    label "kognicja"
  ]
  node [
    id 128
    label "object"
  ]
  node [
    id 129
    label "rozprawa"
  ]
  node [
    id 130
    label "temat"
  ]
  node [
    id 131
    label "wydarzenie"
  ]
  node [
    id 132
    label "szczeg&#243;&#322;"
  ]
  node [
    id 133
    label "proposition"
  ]
  node [
    id 134
    label "przes&#322;anka"
  ]
  node [
    id 135
    label "idea"
  ]
  node [
    id 136
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 137
    label "ustalenie"
  ]
  node [
    id 138
    label "redagowanie"
  ]
  node [
    id 139
    label "ustalanie"
  ]
  node [
    id 140
    label "dociekanie"
  ]
  node [
    id 141
    label "robienie"
  ]
  node [
    id 142
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 143
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 144
    label "investigation"
  ]
  node [
    id 145
    label "macanie"
  ]
  node [
    id 146
    label "usi&#322;owanie"
  ]
  node [
    id 147
    label "penetrowanie"
  ]
  node [
    id 148
    label "przymierzanie"
  ]
  node [
    id 149
    label "przymierzenie"
  ]
  node [
    id 150
    label "examination"
  ]
  node [
    id 151
    label "wypytywanie"
  ]
  node [
    id 152
    label "zbadanie"
  ]
  node [
    id 153
    label "react"
  ]
  node [
    id 154
    label "dawa&#263;"
  ]
  node [
    id 155
    label "by&#263;"
  ]
  node [
    id 156
    label "ponosi&#263;"
  ]
  node [
    id 157
    label "report"
  ]
  node [
    id 158
    label "equate"
  ]
  node [
    id 159
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 160
    label "answer"
  ]
  node [
    id 161
    label "powodowa&#263;"
  ]
  node [
    id 162
    label "tone"
  ]
  node [
    id 163
    label "contend"
  ]
  node [
    id 164
    label "reagowa&#263;"
  ]
  node [
    id 165
    label "impart"
  ]
  node [
    id 166
    label "reagowanie"
  ]
  node [
    id 167
    label "dawanie"
  ]
  node [
    id 168
    label "powodowanie"
  ]
  node [
    id 169
    label "bycie"
  ]
  node [
    id 170
    label "pokutowanie"
  ]
  node [
    id 171
    label "odpowiedzialny"
  ]
  node [
    id 172
    label "winny"
  ]
  node [
    id 173
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 174
    label "picie_piwa"
  ]
  node [
    id 175
    label "odpowiedni"
  ]
  node [
    id 176
    label "parry"
  ]
  node [
    id 177
    label "fit"
  ]
  node [
    id 178
    label "dzianie_si&#281;"
  ]
  node [
    id 179
    label "rendition"
  ]
  node [
    id 180
    label "ponoszenie"
  ]
  node [
    id 181
    label "rozmawianie"
  ]
  node [
    id 182
    label "faza"
  ]
  node [
    id 183
    label "podchodzi&#263;"
  ]
  node [
    id 184
    label "&#263;wiczenie"
  ]
  node [
    id 185
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 186
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 187
    label "praca_pisemna"
  ]
  node [
    id 188
    label "kontrola"
  ]
  node [
    id 189
    label "dydaktyka"
  ]
  node [
    id 190
    label "pr&#243;ba"
  ]
  node [
    id 191
    label "przepytywanie"
  ]
  node [
    id 192
    label "oznajmianie"
  ]
  node [
    id 193
    label "wzywanie"
  ]
  node [
    id 194
    label "development"
  ]
  node [
    id 195
    label "exploitation"
  ]
  node [
    id 196
    label "zdawanie"
  ]
  node [
    id 197
    label "w&#322;&#261;czanie"
  ]
  node [
    id 198
    label "s&#322;uchanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
]
