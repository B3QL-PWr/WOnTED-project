graph [
  node [
    id 0
    label "rozmowa"
    origin "text"
  ]
  node [
    id 1
    label "mec"
    origin "text"
  ]
  node [
    id 2
    label "monika"
    origin "text"
  ]
  node [
    id 3
    label "brzozowska"
    origin "text"
  ]
  node [
    id 4
    label "pasieka"
    origin "text"
  ]
  node [
    id 5
    label "pe&#322;nomocnik"
    origin "text"
  ]
  node [
    id 6
    label "strona"
    origin "text"
  ]
  node [
    id 7
    label "oskar&#380;aj&#261;cy"
    origin "text"
  ]
  node [
    id 8
    label "niemiec"
    origin "text"
  ]
  node [
    id 9
    label "obra&#380;a&#263;"
    origin "text"
  ]
  node [
    id 10
    label "polski"
    origin "text"
  ]
  node [
    id 11
    label "pracownik"
    origin "text"
  ]
  node [
    id 12
    label "cisza"
  ]
  node [
    id 13
    label "odpowied&#378;"
  ]
  node [
    id 14
    label "rozhowor"
  ]
  node [
    id 15
    label "discussion"
  ]
  node [
    id 16
    label "czynno&#347;&#263;"
  ]
  node [
    id 17
    label "activity"
  ]
  node [
    id 18
    label "bezproblemowy"
  ]
  node [
    id 19
    label "wydarzenie"
  ]
  node [
    id 20
    label "g&#322;adki"
  ]
  node [
    id 21
    label "cicha_praca"
  ]
  node [
    id 22
    label "przerwa"
  ]
  node [
    id 23
    label "cicha_msza"
  ]
  node [
    id 24
    label "pok&#243;j"
  ]
  node [
    id 25
    label "motionlessness"
  ]
  node [
    id 26
    label "spok&#243;j"
  ]
  node [
    id 27
    label "zjawisko"
  ]
  node [
    id 28
    label "cecha"
  ]
  node [
    id 29
    label "czas"
  ]
  node [
    id 30
    label "ci&#261;g"
  ]
  node [
    id 31
    label "tajemno&#347;&#263;"
  ]
  node [
    id 32
    label "peace"
  ]
  node [
    id 33
    label "cicha_modlitwa"
  ]
  node [
    id 34
    label "react"
  ]
  node [
    id 35
    label "replica"
  ]
  node [
    id 36
    label "wyj&#347;cie"
  ]
  node [
    id 37
    label "respondent"
  ]
  node [
    id 38
    label "dokument"
  ]
  node [
    id 39
    label "reakcja"
  ]
  node [
    id 40
    label "pasieczysko"
  ]
  node [
    id 41
    label "pasiecznik"
  ]
  node [
    id 42
    label "gospodarstwo"
  ]
  node [
    id 43
    label "inwentarz"
  ]
  node [
    id 44
    label "pole"
  ]
  node [
    id 45
    label "miejsce_pracy"
  ]
  node [
    id 46
    label "dom"
  ]
  node [
    id 47
    label "mienie"
  ]
  node [
    id 48
    label "stodo&#322;a"
  ]
  node [
    id 49
    label "gospodarowanie"
  ]
  node [
    id 50
    label "obora"
  ]
  node [
    id 51
    label "gospodarowa&#263;"
  ]
  node [
    id 52
    label "spichlerz"
  ]
  node [
    id 53
    label "grupa"
  ]
  node [
    id 54
    label "dom_rodzinny"
  ]
  node [
    id 55
    label "pszczelarz"
  ]
  node [
    id 56
    label "miejsce"
  ]
  node [
    id 57
    label "substytuowa&#263;"
  ]
  node [
    id 58
    label "substytuowanie"
  ]
  node [
    id 59
    label "zast&#281;pca"
  ]
  node [
    id 60
    label "cz&#322;owiek"
  ]
  node [
    id 61
    label "ludzko&#347;&#263;"
  ]
  node [
    id 62
    label "asymilowanie"
  ]
  node [
    id 63
    label "wapniak"
  ]
  node [
    id 64
    label "asymilowa&#263;"
  ]
  node [
    id 65
    label "os&#322;abia&#263;"
  ]
  node [
    id 66
    label "posta&#263;"
  ]
  node [
    id 67
    label "hominid"
  ]
  node [
    id 68
    label "podw&#322;adny"
  ]
  node [
    id 69
    label "os&#322;abianie"
  ]
  node [
    id 70
    label "g&#322;owa"
  ]
  node [
    id 71
    label "figura"
  ]
  node [
    id 72
    label "portrecista"
  ]
  node [
    id 73
    label "dwun&#243;g"
  ]
  node [
    id 74
    label "profanum"
  ]
  node [
    id 75
    label "mikrokosmos"
  ]
  node [
    id 76
    label "nasada"
  ]
  node [
    id 77
    label "duch"
  ]
  node [
    id 78
    label "antropochoria"
  ]
  node [
    id 79
    label "osoba"
  ]
  node [
    id 80
    label "wz&#243;r"
  ]
  node [
    id 81
    label "senior"
  ]
  node [
    id 82
    label "oddzia&#322;ywanie"
  ]
  node [
    id 83
    label "Adam"
  ]
  node [
    id 84
    label "homo_sapiens"
  ]
  node [
    id 85
    label "polifag"
  ]
  node [
    id 86
    label "wskaza&#263;"
  ]
  node [
    id 87
    label "podstawi&#263;"
  ]
  node [
    id 88
    label "wskazywa&#263;"
  ]
  node [
    id 89
    label "zast&#261;pi&#263;"
  ]
  node [
    id 90
    label "zast&#281;powa&#263;"
  ]
  node [
    id 91
    label "podstawia&#263;"
  ]
  node [
    id 92
    label "protezowa&#263;"
  ]
  node [
    id 93
    label "wskazywanie"
  ]
  node [
    id 94
    label "podstawienie"
  ]
  node [
    id 95
    label "wskazanie"
  ]
  node [
    id 96
    label "podstawianie"
  ]
  node [
    id 97
    label "kartka"
  ]
  node [
    id 98
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 99
    label "logowanie"
  ]
  node [
    id 100
    label "plik"
  ]
  node [
    id 101
    label "s&#261;d"
  ]
  node [
    id 102
    label "adres_internetowy"
  ]
  node [
    id 103
    label "linia"
  ]
  node [
    id 104
    label "serwis_internetowy"
  ]
  node [
    id 105
    label "bok"
  ]
  node [
    id 106
    label "skr&#281;canie"
  ]
  node [
    id 107
    label "skr&#281;ca&#263;"
  ]
  node [
    id 108
    label "orientowanie"
  ]
  node [
    id 109
    label "skr&#281;ci&#263;"
  ]
  node [
    id 110
    label "uj&#281;cie"
  ]
  node [
    id 111
    label "zorientowanie"
  ]
  node [
    id 112
    label "ty&#322;"
  ]
  node [
    id 113
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 114
    label "fragment"
  ]
  node [
    id 115
    label "layout"
  ]
  node [
    id 116
    label "obiekt"
  ]
  node [
    id 117
    label "zorientowa&#263;"
  ]
  node [
    id 118
    label "pagina"
  ]
  node [
    id 119
    label "podmiot"
  ]
  node [
    id 120
    label "g&#243;ra"
  ]
  node [
    id 121
    label "orientowa&#263;"
  ]
  node [
    id 122
    label "voice"
  ]
  node [
    id 123
    label "orientacja"
  ]
  node [
    id 124
    label "prz&#243;d"
  ]
  node [
    id 125
    label "internet"
  ]
  node [
    id 126
    label "powierzchnia"
  ]
  node [
    id 127
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 128
    label "forma"
  ]
  node [
    id 129
    label "skr&#281;cenie"
  ]
  node [
    id 130
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 131
    label "byt"
  ]
  node [
    id 132
    label "osobowo&#347;&#263;"
  ]
  node [
    id 133
    label "organizacja"
  ]
  node [
    id 134
    label "prawo"
  ]
  node [
    id 135
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 136
    label "nauka_prawa"
  ]
  node [
    id 137
    label "utw&#243;r"
  ]
  node [
    id 138
    label "charakterystyka"
  ]
  node [
    id 139
    label "zaistnie&#263;"
  ]
  node [
    id 140
    label "Osjan"
  ]
  node [
    id 141
    label "kto&#347;"
  ]
  node [
    id 142
    label "wygl&#261;d"
  ]
  node [
    id 143
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 144
    label "wytw&#243;r"
  ]
  node [
    id 145
    label "trim"
  ]
  node [
    id 146
    label "poby&#263;"
  ]
  node [
    id 147
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 148
    label "Aspazja"
  ]
  node [
    id 149
    label "punkt_widzenia"
  ]
  node [
    id 150
    label "kompleksja"
  ]
  node [
    id 151
    label "wytrzyma&#263;"
  ]
  node [
    id 152
    label "budowa"
  ]
  node [
    id 153
    label "formacja"
  ]
  node [
    id 154
    label "pozosta&#263;"
  ]
  node [
    id 155
    label "point"
  ]
  node [
    id 156
    label "przedstawienie"
  ]
  node [
    id 157
    label "go&#347;&#263;"
  ]
  node [
    id 158
    label "kszta&#322;t"
  ]
  node [
    id 159
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 160
    label "armia"
  ]
  node [
    id 161
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 162
    label "poprowadzi&#263;"
  ]
  node [
    id 163
    label "cord"
  ]
  node [
    id 164
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 165
    label "trasa"
  ]
  node [
    id 166
    label "po&#322;&#261;czenie"
  ]
  node [
    id 167
    label "tract"
  ]
  node [
    id 168
    label "materia&#322;_zecerski"
  ]
  node [
    id 169
    label "przeorientowywanie"
  ]
  node [
    id 170
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 171
    label "curve"
  ]
  node [
    id 172
    label "figura_geometryczna"
  ]
  node [
    id 173
    label "zbi&#243;r"
  ]
  node [
    id 174
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 175
    label "jard"
  ]
  node [
    id 176
    label "szczep"
  ]
  node [
    id 177
    label "phreaker"
  ]
  node [
    id 178
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 179
    label "grupa_organizm&#243;w"
  ]
  node [
    id 180
    label "prowadzi&#263;"
  ]
  node [
    id 181
    label "przeorientowywa&#263;"
  ]
  node [
    id 182
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 183
    label "access"
  ]
  node [
    id 184
    label "przeorientowanie"
  ]
  node [
    id 185
    label "przeorientowa&#263;"
  ]
  node [
    id 186
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 187
    label "billing"
  ]
  node [
    id 188
    label "granica"
  ]
  node [
    id 189
    label "szpaler"
  ]
  node [
    id 190
    label "sztrych"
  ]
  node [
    id 191
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 192
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 193
    label "drzewo_genealogiczne"
  ]
  node [
    id 194
    label "transporter"
  ]
  node [
    id 195
    label "line"
  ]
  node [
    id 196
    label "przew&#243;d"
  ]
  node [
    id 197
    label "granice"
  ]
  node [
    id 198
    label "kontakt"
  ]
  node [
    id 199
    label "rz&#261;d"
  ]
  node [
    id 200
    label "przewo&#378;nik"
  ]
  node [
    id 201
    label "przystanek"
  ]
  node [
    id 202
    label "linijka"
  ]
  node [
    id 203
    label "spos&#243;b"
  ]
  node [
    id 204
    label "uporz&#261;dkowanie"
  ]
  node [
    id 205
    label "coalescence"
  ]
  node [
    id 206
    label "Ural"
  ]
  node [
    id 207
    label "bearing"
  ]
  node [
    id 208
    label "prowadzenie"
  ]
  node [
    id 209
    label "tekst"
  ]
  node [
    id 210
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 211
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 212
    label "koniec"
  ]
  node [
    id 213
    label "podkatalog"
  ]
  node [
    id 214
    label "nadpisa&#263;"
  ]
  node [
    id 215
    label "nadpisanie"
  ]
  node [
    id 216
    label "bundle"
  ]
  node [
    id 217
    label "folder"
  ]
  node [
    id 218
    label "nadpisywanie"
  ]
  node [
    id 219
    label "paczka"
  ]
  node [
    id 220
    label "nadpisywa&#263;"
  ]
  node [
    id 221
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 222
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 223
    label "Rzym_Zachodni"
  ]
  node [
    id 224
    label "whole"
  ]
  node [
    id 225
    label "ilo&#347;&#263;"
  ]
  node [
    id 226
    label "element"
  ]
  node [
    id 227
    label "Rzym_Wschodni"
  ]
  node [
    id 228
    label "urz&#261;dzenie"
  ]
  node [
    id 229
    label "rozmiar"
  ]
  node [
    id 230
    label "obszar"
  ]
  node [
    id 231
    label "poj&#281;cie"
  ]
  node [
    id 232
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 233
    label "zwierciad&#322;o"
  ]
  node [
    id 234
    label "capacity"
  ]
  node [
    id 235
    label "plane"
  ]
  node [
    id 236
    label "temat"
  ]
  node [
    id 237
    label "jednostka_systematyczna"
  ]
  node [
    id 238
    label "poznanie"
  ]
  node [
    id 239
    label "leksem"
  ]
  node [
    id 240
    label "dzie&#322;o"
  ]
  node [
    id 241
    label "stan"
  ]
  node [
    id 242
    label "blaszka"
  ]
  node [
    id 243
    label "kantyzm"
  ]
  node [
    id 244
    label "zdolno&#347;&#263;"
  ]
  node [
    id 245
    label "do&#322;ek"
  ]
  node [
    id 246
    label "zawarto&#347;&#263;"
  ]
  node [
    id 247
    label "gwiazda"
  ]
  node [
    id 248
    label "formality"
  ]
  node [
    id 249
    label "struktura"
  ]
  node [
    id 250
    label "mode"
  ]
  node [
    id 251
    label "morfem"
  ]
  node [
    id 252
    label "rdze&#324;"
  ]
  node [
    id 253
    label "kielich"
  ]
  node [
    id 254
    label "ornamentyka"
  ]
  node [
    id 255
    label "pasmo"
  ]
  node [
    id 256
    label "zwyczaj"
  ]
  node [
    id 257
    label "naczynie"
  ]
  node [
    id 258
    label "p&#322;at"
  ]
  node [
    id 259
    label "maszyna_drukarska"
  ]
  node [
    id 260
    label "style"
  ]
  node [
    id 261
    label "linearno&#347;&#263;"
  ]
  node [
    id 262
    label "wyra&#380;enie"
  ]
  node [
    id 263
    label "spirala"
  ]
  node [
    id 264
    label "dyspozycja"
  ]
  node [
    id 265
    label "odmiana"
  ]
  node [
    id 266
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 267
    label "October"
  ]
  node [
    id 268
    label "creation"
  ]
  node [
    id 269
    label "p&#281;tla"
  ]
  node [
    id 270
    label "arystotelizm"
  ]
  node [
    id 271
    label "szablon"
  ]
  node [
    id 272
    label "miniatura"
  ]
  node [
    id 273
    label "zesp&#243;&#322;"
  ]
  node [
    id 274
    label "podejrzany"
  ]
  node [
    id 275
    label "s&#261;downictwo"
  ]
  node [
    id 276
    label "system"
  ]
  node [
    id 277
    label "biuro"
  ]
  node [
    id 278
    label "court"
  ]
  node [
    id 279
    label "forum"
  ]
  node [
    id 280
    label "bronienie"
  ]
  node [
    id 281
    label "urz&#261;d"
  ]
  node [
    id 282
    label "oskar&#380;yciel"
  ]
  node [
    id 283
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 284
    label "skazany"
  ]
  node [
    id 285
    label "post&#281;powanie"
  ]
  node [
    id 286
    label "broni&#263;"
  ]
  node [
    id 287
    label "my&#347;l"
  ]
  node [
    id 288
    label "pods&#261;dny"
  ]
  node [
    id 289
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 290
    label "obrona"
  ]
  node [
    id 291
    label "wypowied&#378;"
  ]
  node [
    id 292
    label "instytucja"
  ]
  node [
    id 293
    label "antylogizm"
  ]
  node [
    id 294
    label "konektyw"
  ]
  node [
    id 295
    label "&#347;wiadek"
  ]
  node [
    id 296
    label "procesowicz"
  ]
  node [
    id 297
    label "pochwytanie"
  ]
  node [
    id 298
    label "wording"
  ]
  node [
    id 299
    label "wzbudzenie"
  ]
  node [
    id 300
    label "withdrawal"
  ]
  node [
    id 301
    label "capture"
  ]
  node [
    id 302
    label "podniesienie"
  ]
  node [
    id 303
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 304
    label "film"
  ]
  node [
    id 305
    label "scena"
  ]
  node [
    id 306
    label "zapisanie"
  ]
  node [
    id 307
    label "prezentacja"
  ]
  node [
    id 308
    label "rzucenie"
  ]
  node [
    id 309
    label "zamkni&#281;cie"
  ]
  node [
    id 310
    label "zabranie"
  ]
  node [
    id 311
    label "poinformowanie"
  ]
  node [
    id 312
    label "zaaresztowanie"
  ]
  node [
    id 313
    label "wzi&#281;cie"
  ]
  node [
    id 314
    label "eastern_hemisphere"
  ]
  node [
    id 315
    label "kierunek"
  ]
  node [
    id 316
    label "kierowa&#263;"
  ]
  node [
    id 317
    label "inform"
  ]
  node [
    id 318
    label "marshal"
  ]
  node [
    id 319
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 320
    label "wyznacza&#263;"
  ]
  node [
    id 321
    label "pomaga&#263;"
  ]
  node [
    id 322
    label "tu&#322;&#243;w"
  ]
  node [
    id 323
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 324
    label "wielok&#261;t"
  ]
  node [
    id 325
    label "odcinek"
  ]
  node [
    id 326
    label "strzelba"
  ]
  node [
    id 327
    label "lufa"
  ]
  node [
    id 328
    label "&#347;ciana"
  ]
  node [
    id 329
    label "wyznaczenie"
  ]
  node [
    id 330
    label "przyczynienie_si&#281;"
  ]
  node [
    id 331
    label "zwr&#243;cenie"
  ]
  node [
    id 332
    label "zrozumienie"
  ]
  node [
    id 333
    label "po&#322;o&#380;enie"
  ]
  node [
    id 334
    label "seksualno&#347;&#263;"
  ]
  node [
    id 335
    label "wiedza"
  ]
  node [
    id 336
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 337
    label "zorientowanie_si&#281;"
  ]
  node [
    id 338
    label "pogubienie_si&#281;"
  ]
  node [
    id 339
    label "orientation"
  ]
  node [
    id 340
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 341
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 342
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 343
    label "gubienie_si&#281;"
  ]
  node [
    id 344
    label "turn"
  ]
  node [
    id 345
    label "wrench"
  ]
  node [
    id 346
    label "nawini&#281;cie"
  ]
  node [
    id 347
    label "os&#322;abienie"
  ]
  node [
    id 348
    label "uszkodzenie"
  ]
  node [
    id 349
    label "odbicie"
  ]
  node [
    id 350
    label "poskr&#281;canie"
  ]
  node [
    id 351
    label "uraz"
  ]
  node [
    id 352
    label "odchylenie_si&#281;"
  ]
  node [
    id 353
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 354
    label "z&#322;&#261;czenie"
  ]
  node [
    id 355
    label "splecenie"
  ]
  node [
    id 356
    label "turning"
  ]
  node [
    id 357
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 358
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 359
    label "sple&#347;&#263;"
  ]
  node [
    id 360
    label "os&#322;abi&#263;"
  ]
  node [
    id 361
    label "nawin&#261;&#263;"
  ]
  node [
    id 362
    label "scali&#263;"
  ]
  node [
    id 363
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 364
    label "twist"
  ]
  node [
    id 365
    label "splay"
  ]
  node [
    id 366
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 367
    label "uszkodzi&#263;"
  ]
  node [
    id 368
    label "break"
  ]
  node [
    id 369
    label "flex"
  ]
  node [
    id 370
    label "przestrze&#324;"
  ]
  node [
    id 371
    label "zaty&#322;"
  ]
  node [
    id 372
    label "pupa"
  ]
  node [
    id 373
    label "cia&#322;o"
  ]
  node [
    id 374
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 375
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 376
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 377
    label "splata&#263;"
  ]
  node [
    id 378
    label "throw"
  ]
  node [
    id 379
    label "screw"
  ]
  node [
    id 380
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 381
    label "scala&#263;"
  ]
  node [
    id 382
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 383
    label "przedmiot"
  ]
  node [
    id 384
    label "przelezienie"
  ]
  node [
    id 385
    label "&#347;piew"
  ]
  node [
    id 386
    label "Synaj"
  ]
  node [
    id 387
    label "Kreml"
  ]
  node [
    id 388
    label "d&#378;wi&#281;k"
  ]
  node [
    id 389
    label "wysoki"
  ]
  node [
    id 390
    label "wzniesienie"
  ]
  node [
    id 391
    label "pi&#281;tro"
  ]
  node [
    id 392
    label "Ropa"
  ]
  node [
    id 393
    label "kupa"
  ]
  node [
    id 394
    label "przele&#378;&#263;"
  ]
  node [
    id 395
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 396
    label "karczek"
  ]
  node [
    id 397
    label "rami&#261;czko"
  ]
  node [
    id 398
    label "Jaworze"
  ]
  node [
    id 399
    label "set"
  ]
  node [
    id 400
    label "orient"
  ]
  node [
    id 401
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 402
    label "aim"
  ]
  node [
    id 403
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 404
    label "wyznaczy&#263;"
  ]
  node [
    id 405
    label "pomaganie"
  ]
  node [
    id 406
    label "przyczynianie_si&#281;"
  ]
  node [
    id 407
    label "zwracanie"
  ]
  node [
    id 408
    label "rozeznawanie"
  ]
  node [
    id 409
    label "oznaczanie"
  ]
  node [
    id 410
    label "odchylanie_si&#281;"
  ]
  node [
    id 411
    label "kszta&#322;towanie"
  ]
  node [
    id 412
    label "uprz&#281;dzenie"
  ]
  node [
    id 413
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 414
    label "scalanie"
  ]
  node [
    id 415
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 416
    label "snucie"
  ]
  node [
    id 417
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 418
    label "tortuosity"
  ]
  node [
    id 419
    label "odbijanie"
  ]
  node [
    id 420
    label "contortion"
  ]
  node [
    id 421
    label "splatanie"
  ]
  node [
    id 422
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 423
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 424
    label "uwierzytelnienie"
  ]
  node [
    id 425
    label "liczba"
  ]
  node [
    id 426
    label "circumference"
  ]
  node [
    id 427
    label "cyrkumferencja"
  ]
  node [
    id 428
    label "provider"
  ]
  node [
    id 429
    label "hipertekst"
  ]
  node [
    id 430
    label "cyberprzestrze&#324;"
  ]
  node [
    id 431
    label "mem"
  ]
  node [
    id 432
    label "grooming"
  ]
  node [
    id 433
    label "gra_sieciowa"
  ]
  node [
    id 434
    label "media"
  ]
  node [
    id 435
    label "biznes_elektroniczny"
  ]
  node [
    id 436
    label "sie&#263;_komputerowa"
  ]
  node [
    id 437
    label "punkt_dost&#281;pu"
  ]
  node [
    id 438
    label "us&#322;uga_internetowa"
  ]
  node [
    id 439
    label "netbook"
  ]
  node [
    id 440
    label "e-hazard"
  ]
  node [
    id 441
    label "podcast"
  ]
  node [
    id 442
    label "co&#347;"
  ]
  node [
    id 443
    label "budynek"
  ]
  node [
    id 444
    label "thing"
  ]
  node [
    id 445
    label "program"
  ]
  node [
    id 446
    label "rzecz"
  ]
  node [
    id 447
    label "faul"
  ]
  node [
    id 448
    label "wk&#322;ad"
  ]
  node [
    id 449
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 450
    label "s&#281;dzia"
  ]
  node [
    id 451
    label "bon"
  ]
  node [
    id 452
    label "ticket"
  ]
  node [
    id 453
    label "arkusz"
  ]
  node [
    id 454
    label "kartonik"
  ]
  node [
    id 455
    label "kara"
  ]
  node [
    id 456
    label "pagination"
  ]
  node [
    id 457
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 458
    label "numer"
  ]
  node [
    id 459
    label "niemiecki"
  ]
  node [
    id 460
    label "po_niemiecku"
  ]
  node [
    id 461
    label "German"
  ]
  node [
    id 462
    label "niemiecko"
  ]
  node [
    id 463
    label "cenar"
  ]
  node [
    id 464
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 465
    label "europejski"
  ]
  node [
    id 466
    label "strudel"
  ]
  node [
    id 467
    label "pionier"
  ]
  node [
    id 468
    label "zachodnioeuropejski"
  ]
  node [
    id 469
    label "j&#281;zyk"
  ]
  node [
    id 470
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 471
    label "junkers"
  ]
  node [
    id 472
    label "szwabski"
  ]
  node [
    id 473
    label "mistreat"
  ]
  node [
    id 474
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 475
    label "narusza&#263;"
  ]
  node [
    id 476
    label "odejmowa&#263;"
  ]
  node [
    id 477
    label "robi&#263;"
  ]
  node [
    id 478
    label "zaczyna&#263;"
  ]
  node [
    id 479
    label "bankrupt"
  ]
  node [
    id 480
    label "psu&#263;"
  ]
  node [
    id 481
    label "transgress"
  ]
  node [
    id 482
    label "begin"
  ]
  node [
    id 483
    label "Polish"
  ]
  node [
    id 484
    label "goniony"
  ]
  node [
    id 485
    label "oberek"
  ]
  node [
    id 486
    label "ryba_po_grecku"
  ]
  node [
    id 487
    label "sztajer"
  ]
  node [
    id 488
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 489
    label "krakowiak"
  ]
  node [
    id 490
    label "pierogi_ruskie"
  ]
  node [
    id 491
    label "lacki"
  ]
  node [
    id 492
    label "polak"
  ]
  node [
    id 493
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 494
    label "chodzony"
  ]
  node [
    id 495
    label "po_polsku"
  ]
  node [
    id 496
    label "mazur"
  ]
  node [
    id 497
    label "polsko"
  ]
  node [
    id 498
    label "skoczny"
  ]
  node [
    id 499
    label "drabant"
  ]
  node [
    id 500
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 501
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 502
    label "artykulator"
  ]
  node [
    id 503
    label "kod"
  ]
  node [
    id 504
    label "kawa&#322;ek"
  ]
  node [
    id 505
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 506
    label "gramatyka"
  ]
  node [
    id 507
    label "stylik"
  ]
  node [
    id 508
    label "przet&#322;umaczenie"
  ]
  node [
    id 509
    label "formalizowanie"
  ]
  node [
    id 510
    label "ssanie"
  ]
  node [
    id 511
    label "ssa&#263;"
  ]
  node [
    id 512
    label "language"
  ]
  node [
    id 513
    label "liza&#263;"
  ]
  node [
    id 514
    label "napisa&#263;"
  ]
  node [
    id 515
    label "konsonantyzm"
  ]
  node [
    id 516
    label "wokalizm"
  ]
  node [
    id 517
    label "pisa&#263;"
  ]
  node [
    id 518
    label "fonetyka"
  ]
  node [
    id 519
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 520
    label "jeniec"
  ]
  node [
    id 521
    label "but"
  ]
  node [
    id 522
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 523
    label "po_koroniarsku"
  ]
  node [
    id 524
    label "kultura_duchowa"
  ]
  node [
    id 525
    label "t&#322;umaczenie"
  ]
  node [
    id 526
    label "m&#243;wienie"
  ]
  node [
    id 527
    label "pype&#263;"
  ]
  node [
    id 528
    label "lizanie"
  ]
  node [
    id 529
    label "pismo"
  ]
  node [
    id 530
    label "formalizowa&#263;"
  ]
  node [
    id 531
    label "rozumie&#263;"
  ]
  node [
    id 532
    label "organ"
  ]
  node [
    id 533
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 534
    label "rozumienie"
  ]
  node [
    id 535
    label "makroglosja"
  ]
  node [
    id 536
    label "m&#243;wi&#263;"
  ]
  node [
    id 537
    label "jama_ustna"
  ]
  node [
    id 538
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 539
    label "formacja_geologiczna"
  ]
  node [
    id 540
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 541
    label "natural_language"
  ]
  node [
    id 542
    label "s&#322;ownictwo"
  ]
  node [
    id 543
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 544
    label "wschodnioeuropejski"
  ]
  node [
    id 545
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 546
    label "poga&#324;ski"
  ]
  node [
    id 547
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 548
    label "topielec"
  ]
  node [
    id 549
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 550
    label "langosz"
  ]
  node [
    id 551
    label "zboczenie"
  ]
  node [
    id 552
    label "om&#243;wienie"
  ]
  node [
    id 553
    label "sponiewieranie"
  ]
  node [
    id 554
    label "discipline"
  ]
  node [
    id 555
    label "omawia&#263;"
  ]
  node [
    id 556
    label "kr&#261;&#380;enie"
  ]
  node [
    id 557
    label "tre&#347;&#263;"
  ]
  node [
    id 558
    label "robienie"
  ]
  node [
    id 559
    label "sponiewiera&#263;"
  ]
  node [
    id 560
    label "entity"
  ]
  node [
    id 561
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 562
    label "tematyka"
  ]
  node [
    id 563
    label "w&#261;tek"
  ]
  node [
    id 564
    label "charakter"
  ]
  node [
    id 565
    label "zbaczanie"
  ]
  node [
    id 566
    label "program_nauczania"
  ]
  node [
    id 567
    label "om&#243;wi&#263;"
  ]
  node [
    id 568
    label "omawianie"
  ]
  node [
    id 569
    label "kultura"
  ]
  node [
    id 570
    label "istota"
  ]
  node [
    id 571
    label "zbacza&#263;"
  ]
  node [
    id 572
    label "zboczy&#263;"
  ]
  node [
    id 573
    label "gwardzista"
  ]
  node [
    id 574
    label "melodia"
  ]
  node [
    id 575
    label "taniec"
  ]
  node [
    id 576
    label "taniec_ludowy"
  ]
  node [
    id 577
    label "&#347;redniowieczny"
  ]
  node [
    id 578
    label "europejsko"
  ]
  node [
    id 579
    label "specjalny"
  ]
  node [
    id 580
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 581
    label "weso&#322;y"
  ]
  node [
    id 582
    label "sprawny"
  ]
  node [
    id 583
    label "rytmiczny"
  ]
  node [
    id 584
    label "skocznie"
  ]
  node [
    id 585
    label "energiczny"
  ]
  node [
    id 586
    label "przytup"
  ]
  node [
    id 587
    label "ho&#322;ubiec"
  ]
  node [
    id 588
    label "wodzi&#263;"
  ]
  node [
    id 589
    label "lendler"
  ]
  node [
    id 590
    label "austriacki"
  ]
  node [
    id 591
    label "polka"
  ]
  node [
    id 592
    label "ludowy"
  ]
  node [
    id 593
    label "pie&#347;&#324;"
  ]
  node [
    id 594
    label "mieszkaniec"
  ]
  node [
    id 595
    label "centu&#347;"
  ]
  node [
    id 596
    label "lalka"
  ]
  node [
    id 597
    label "Ma&#322;opolanin"
  ]
  node [
    id 598
    label "krakauer"
  ]
  node [
    id 599
    label "salariat"
  ]
  node [
    id 600
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 601
    label "delegowanie"
  ]
  node [
    id 602
    label "pracu&#347;"
  ]
  node [
    id 603
    label "r&#281;ka"
  ]
  node [
    id 604
    label "delegowa&#263;"
  ]
  node [
    id 605
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 606
    label "warstwa"
  ]
  node [
    id 607
    label "p&#322;aca"
  ]
  node [
    id 608
    label "krzy&#380;"
  ]
  node [
    id 609
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 610
    label "handwriting"
  ]
  node [
    id 611
    label "d&#322;o&#324;"
  ]
  node [
    id 612
    label "gestykulowa&#263;"
  ]
  node [
    id 613
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 614
    label "palec"
  ]
  node [
    id 615
    label "przedrami&#281;"
  ]
  node [
    id 616
    label "hand"
  ]
  node [
    id 617
    label "&#322;okie&#263;"
  ]
  node [
    id 618
    label "hazena"
  ]
  node [
    id 619
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 620
    label "bramkarz"
  ]
  node [
    id 621
    label "nadgarstek"
  ]
  node [
    id 622
    label "graba"
  ]
  node [
    id 623
    label "r&#261;czyna"
  ]
  node [
    id 624
    label "k&#322;&#261;b"
  ]
  node [
    id 625
    label "pi&#322;ka"
  ]
  node [
    id 626
    label "chwyta&#263;"
  ]
  node [
    id 627
    label "cmoknonsens"
  ]
  node [
    id 628
    label "pomocnik"
  ]
  node [
    id 629
    label "gestykulowanie"
  ]
  node [
    id 630
    label "chwytanie"
  ]
  node [
    id 631
    label "obietnica"
  ]
  node [
    id 632
    label "zagrywka"
  ]
  node [
    id 633
    label "kroki"
  ]
  node [
    id 634
    label "hasta"
  ]
  node [
    id 635
    label "wykroczenie"
  ]
  node [
    id 636
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 637
    label "czerwona_kartka"
  ]
  node [
    id 638
    label "paw"
  ]
  node [
    id 639
    label "rami&#281;"
  ]
  node [
    id 640
    label "wysy&#322;a&#263;"
  ]
  node [
    id 641
    label "air"
  ]
  node [
    id 642
    label "wys&#322;a&#263;"
  ]
  node [
    id 643
    label "oddelegowa&#263;"
  ]
  node [
    id 644
    label "oddelegowywa&#263;"
  ]
  node [
    id 645
    label "zapaleniec"
  ]
  node [
    id 646
    label "wysy&#322;anie"
  ]
  node [
    id 647
    label "wys&#322;anie"
  ]
  node [
    id 648
    label "delegacy"
  ]
  node [
    id 649
    label "oddelegowywanie"
  ]
  node [
    id 650
    label "oddelegowanie"
  ]
  node [
    id 651
    label "Monika"
  ]
  node [
    id 652
    label "Brzozowska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 651
    target 652
  ]
]
