graph [
  node [
    id 0
    label "mireczka"
    origin "text"
  ]
  node [
    id 1
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "kozak"
    origin "text"
  ]
  node [
    id 4
    label "podchodzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 6
    label "pod"
    origin "text"
  ]
  node [
    id 7
    label "kategoria"
    origin "text"
  ]
  node [
    id 8
    label "pani"
    origin "text"
  ]
  node [
    id 9
    label "lekki"
    origin "text"
  ]
  node [
    id 10
    label "obyczaj"
    origin "text"
  ]
  node [
    id 11
    label "okre&#347;lony"
  ]
  node [
    id 12
    label "jaki&#347;"
  ]
  node [
    id 13
    label "przyzwoity"
  ]
  node [
    id 14
    label "ciekawy"
  ]
  node [
    id 15
    label "jako&#347;"
  ]
  node [
    id 16
    label "jako_tako"
  ]
  node [
    id 17
    label "niez&#322;y"
  ]
  node [
    id 18
    label "dziwny"
  ]
  node [
    id 19
    label "charakterystyczny"
  ]
  node [
    id 20
    label "wiadomy"
  ]
  node [
    id 21
    label "ludowy"
  ]
  node [
    id 22
    label "kawalerzysta"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "but"
  ]
  node [
    id 25
    label "sotnia"
  ]
  node [
    id 26
    label "postrzeleniec"
  ]
  node [
    id 27
    label "popisywa&#263;_si&#281;"
  ]
  node [
    id 28
    label "melodia"
  ]
  node [
    id 29
    label "taniec"
  ]
  node [
    id 30
    label "taniec_ludowy"
  ]
  node [
    id 31
    label "ko&#378;larz"
  ]
  node [
    id 32
    label "kure&#324;"
  ]
  node [
    id 33
    label "Kozak"
  ]
  node [
    id 34
    label "prysiudy"
  ]
  node [
    id 35
    label "ko&#378;larz_babka"
  ]
  node [
    id 36
    label "rzecz"
  ]
  node [
    id 37
    label "&#347;mia&#322;ek"
  ]
  node [
    id 38
    label "zapi&#281;tek"
  ]
  node [
    id 39
    label "sznurowad&#322;o"
  ]
  node [
    id 40
    label "rozbijarka"
  ]
  node [
    id 41
    label "podeszwa"
  ]
  node [
    id 42
    label "obcas"
  ]
  node [
    id 43
    label "wytw&#243;r"
  ]
  node [
    id 44
    label "wzu&#263;"
  ]
  node [
    id 45
    label "wzuwanie"
  ]
  node [
    id 46
    label "przyszwa"
  ]
  node [
    id 47
    label "raki"
  ]
  node [
    id 48
    label "cholewa"
  ]
  node [
    id 49
    label "cholewka"
  ]
  node [
    id 50
    label "zel&#243;wka"
  ]
  node [
    id 51
    label "obuwie"
  ]
  node [
    id 52
    label "j&#281;zyk"
  ]
  node [
    id 53
    label "napi&#281;tek"
  ]
  node [
    id 54
    label "wzucie"
  ]
  node [
    id 55
    label "object"
  ]
  node [
    id 56
    label "przedmiot"
  ]
  node [
    id 57
    label "temat"
  ]
  node [
    id 58
    label "wpadni&#281;cie"
  ]
  node [
    id 59
    label "mienie"
  ]
  node [
    id 60
    label "przyroda"
  ]
  node [
    id 61
    label "istota"
  ]
  node [
    id 62
    label "obiekt"
  ]
  node [
    id 63
    label "kultura"
  ]
  node [
    id 64
    label "wpa&#347;&#263;"
  ]
  node [
    id 65
    label "wpadanie"
  ]
  node [
    id 66
    label "wpada&#263;"
  ]
  node [
    id 67
    label "ludzko&#347;&#263;"
  ]
  node [
    id 68
    label "asymilowanie"
  ]
  node [
    id 69
    label "wapniak"
  ]
  node [
    id 70
    label "asymilowa&#263;"
  ]
  node [
    id 71
    label "os&#322;abia&#263;"
  ]
  node [
    id 72
    label "posta&#263;"
  ]
  node [
    id 73
    label "hominid"
  ]
  node [
    id 74
    label "podw&#322;adny"
  ]
  node [
    id 75
    label "os&#322;abianie"
  ]
  node [
    id 76
    label "g&#322;owa"
  ]
  node [
    id 77
    label "figura"
  ]
  node [
    id 78
    label "portrecista"
  ]
  node [
    id 79
    label "dwun&#243;g"
  ]
  node [
    id 80
    label "profanum"
  ]
  node [
    id 81
    label "mikrokosmos"
  ]
  node [
    id 82
    label "nasada"
  ]
  node [
    id 83
    label "duch"
  ]
  node [
    id 84
    label "antropochoria"
  ]
  node [
    id 85
    label "osoba"
  ]
  node [
    id 86
    label "wz&#243;r"
  ]
  node [
    id 87
    label "senior"
  ]
  node [
    id 88
    label "oddzia&#322;ywanie"
  ]
  node [
    id 89
    label "Adam"
  ]
  node [
    id 90
    label "homo_sapiens"
  ]
  node [
    id 91
    label "polifag"
  ]
  node [
    id 92
    label "ose&#322;edec"
  ]
  node [
    id 93
    label "Ukrainiec"
  ]
  node [
    id 94
    label "kozactwo"
  ]
  node [
    id 95
    label "ko&#324;"
  ]
  node [
    id 96
    label "&#380;o&#322;nierz"
  ]
  node [
    id 97
    label "jezdny"
  ]
  node [
    id 98
    label "jazda"
  ]
  node [
    id 99
    label "zbi&#243;r"
  ]
  node [
    id 100
    label "karnet"
  ]
  node [
    id 101
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 102
    label "utw&#243;r"
  ]
  node [
    id 103
    label "ruch"
  ]
  node [
    id 104
    label "parkiet"
  ]
  node [
    id 105
    label "choreologia"
  ]
  node [
    id 106
    label "czynno&#347;&#263;"
  ]
  node [
    id 107
    label "krok_taneczny"
  ]
  node [
    id 108
    label "zanucenie"
  ]
  node [
    id 109
    label "nuta"
  ]
  node [
    id 110
    label "zakosztowa&#263;"
  ]
  node [
    id 111
    label "zajawka"
  ]
  node [
    id 112
    label "zanuci&#263;"
  ]
  node [
    id 113
    label "emocja"
  ]
  node [
    id 114
    label "oskoma"
  ]
  node [
    id 115
    label "melika"
  ]
  node [
    id 116
    label "nucenie"
  ]
  node [
    id 117
    label "nuci&#263;"
  ]
  node [
    id 118
    label "brzmienie"
  ]
  node [
    id 119
    label "zjawisko"
  ]
  node [
    id 120
    label "taste"
  ]
  node [
    id 121
    label "muzyka"
  ]
  node [
    id 122
    label "inclination"
  ]
  node [
    id 123
    label "zuch"
  ]
  node [
    id 124
    label "rycerzyk"
  ]
  node [
    id 125
    label "odwa&#380;ny"
  ]
  node [
    id 126
    label "ryzykant"
  ]
  node [
    id 127
    label "morowiec"
  ]
  node [
    id 128
    label "trawa"
  ]
  node [
    id 129
    label "ro&#347;lina"
  ]
  node [
    id 130
    label "twardziel"
  ]
  node [
    id 131
    label "bohater"
  ]
  node [
    id 132
    label "owsowe"
  ]
  node [
    id 133
    label "szalona_g&#322;owa"
  ]
  node [
    id 134
    label "formacja"
  ]
  node [
    id 135
    label "ob&#243;z"
  ]
  node [
    id 136
    label "domostwo"
  ]
  node [
    id 137
    label "szwadron"
  ]
  node [
    id 138
    label "publiczny"
  ]
  node [
    id 139
    label "folk"
  ]
  node [
    id 140
    label "etniczny"
  ]
  node [
    id 141
    label "wiejski"
  ]
  node [
    id 142
    label "ludowo"
  ]
  node [
    id 143
    label "borowikowate"
  ]
  node [
    id 144
    label "pieczarniak"
  ]
  node [
    id 145
    label "bock"
  ]
  node [
    id 146
    label "ko&#378;lak"
  ]
  node [
    id 147
    label "grzyb_jadalny"
  ]
  node [
    id 148
    label "grzyb_mikoryzowy"
  ]
  node [
    id 149
    label "figura_taneczna"
  ]
  node [
    id 150
    label "przysiad"
  ]
  node [
    id 151
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 152
    label "oszukiwa&#263;"
  ]
  node [
    id 153
    label "ciecz"
  ]
  node [
    id 154
    label "set_about"
  ]
  node [
    id 155
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 156
    label "dociera&#263;"
  ]
  node [
    id 157
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 158
    label "approach"
  ]
  node [
    id 159
    label "dochodzi&#263;"
  ]
  node [
    id 160
    label "odpowiada&#263;"
  ]
  node [
    id 161
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 162
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 163
    label "traktowa&#263;"
  ]
  node [
    id 164
    label "sprawdzian"
  ]
  node [
    id 165
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 166
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 167
    label "wype&#322;nia&#263;"
  ]
  node [
    id 168
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 169
    label "silnik"
  ]
  node [
    id 170
    label "dopasowywa&#263;"
  ]
  node [
    id 171
    label "g&#322;adzi&#263;"
  ]
  node [
    id 172
    label "boost"
  ]
  node [
    id 173
    label "dorabia&#263;"
  ]
  node [
    id 174
    label "get"
  ]
  node [
    id 175
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 176
    label "trze&#263;"
  ]
  node [
    id 177
    label "znajdowa&#263;"
  ]
  node [
    id 178
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 179
    label "orzyna&#263;"
  ]
  node [
    id 180
    label "oszwabia&#263;"
  ]
  node [
    id 181
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 182
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 183
    label "cheat"
  ]
  node [
    id 184
    label "zaczyna&#263;"
  ]
  node [
    id 185
    label "wchodzi&#263;"
  ]
  node [
    id 186
    label "submit"
  ]
  node [
    id 187
    label "react"
  ]
  node [
    id 188
    label "dawa&#263;"
  ]
  node [
    id 189
    label "by&#263;"
  ]
  node [
    id 190
    label "ponosi&#263;"
  ]
  node [
    id 191
    label "report"
  ]
  node [
    id 192
    label "pytanie"
  ]
  node [
    id 193
    label "equate"
  ]
  node [
    id 194
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 195
    label "answer"
  ]
  node [
    id 196
    label "powodowa&#263;"
  ]
  node [
    id 197
    label "tone"
  ]
  node [
    id 198
    label "contend"
  ]
  node [
    id 199
    label "reagowa&#263;"
  ]
  node [
    id 200
    label "impart"
  ]
  node [
    id 201
    label "robi&#263;"
  ]
  node [
    id 202
    label "uzyskiwa&#263;"
  ]
  node [
    id 203
    label "claim"
  ]
  node [
    id 204
    label "osi&#261;ga&#263;"
  ]
  node [
    id 205
    label "ripen"
  ]
  node [
    id 206
    label "supervene"
  ]
  node [
    id 207
    label "doczeka&#263;"
  ]
  node [
    id 208
    label "przesy&#322;ka"
  ]
  node [
    id 209
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 210
    label "doznawa&#263;"
  ]
  node [
    id 211
    label "reach"
  ]
  node [
    id 212
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 213
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 214
    label "zachodzi&#263;"
  ]
  node [
    id 215
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 216
    label "postrzega&#263;"
  ]
  node [
    id 217
    label "orgazm"
  ]
  node [
    id 218
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 219
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 220
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 221
    label "dokoptowywa&#263;"
  ]
  node [
    id 222
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 223
    label "dolatywa&#263;"
  ]
  node [
    id 224
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 225
    label "poddawa&#263;"
  ]
  node [
    id 226
    label "dotyczy&#263;"
  ]
  node [
    id 227
    label "use"
  ]
  node [
    id 228
    label "zajmowa&#263;"
  ]
  node [
    id 229
    label "istnie&#263;"
  ]
  node [
    id 230
    label "perform"
  ]
  node [
    id 231
    label "close"
  ]
  node [
    id 232
    label "meet"
  ]
  node [
    id 233
    label "charge"
  ]
  node [
    id 234
    label "umieszcza&#263;"
  ]
  node [
    id 235
    label "do"
  ]
  node [
    id 236
    label "faza"
  ]
  node [
    id 237
    label "&#263;wiczenie"
  ]
  node [
    id 238
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 239
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 240
    label "praca_pisemna"
  ]
  node [
    id 241
    label "kontrola"
  ]
  node [
    id 242
    label "dydaktyka"
  ]
  node [
    id 243
    label "pr&#243;ba"
  ]
  node [
    id 244
    label "examination"
  ]
  node [
    id 245
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 246
    label "p&#322;ywa&#263;"
  ]
  node [
    id 247
    label "ciek&#322;y"
  ]
  node [
    id 248
    label "chlupa&#263;"
  ]
  node [
    id 249
    label "wytoczenie"
  ]
  node [
    id 250
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 251
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 252
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 253
    label "stan_skupienia"
  ]
  node [
    id 254
    label "nieprzejrzysty"
  ]
  node [
    id 255
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 256
    label "podbiega&#263;"
  ]
  node [
    id 257
    label "baniak"
  ]
  node [
    id 258
    label "zachlupa&#263;"
  ]
  node [
    id 259
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 260
    label "odp&#322;ywanie"
  ]
  node [
    id 261
    label "cia&#322;o"
  ]
  node [
    id 262
    label "podbiec"
  ]
  node [
    id 263
    label "substancja"
  ]
  node [
    id 264
    label "type"
  ]
  node [
    id 265
    label "poj&#281;cie"
  ]
  node [
    id 266
    label "teoria"
  ]
  node [
    id 267
    label "forma"
  ]
  node [
    id 268
    label "klasa"
  ]
  node [
    id 269
    label "s&#261;d"
  ]
  node [
    id 270
    label "teologicznie"
  ]
  node [
    id 271
    label "wiedza"
  ]
  node [
    id 272
    label "belief"
  ]
  node [
    id 273
    label "zderzenie_si&#281;"
  ]
  node [
    id 274
    label "twierdzenie"
  ]
  node [
    id 275
    label "teoria_Dowa"
  ]
  node [
    id 276
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 277
    label "przypuszczenie"
  ]
  node [
    id 278
    label "teoria_Fishera"
  ]
  node [
    id 279
    label "system"
  ]
  node [
    id 280
    label "teoria_Arrheniusa"
  ]
  node [
    id 281
    label "p&#322;&#243;d"
  ]
  node [
    id 282
    label "work"
  ]
  node [
    id 283
    label "rezultat"
  ]
  node [
    id 284
    label "egzemplarz"
  ]
  node [
    id 285
    label "series"
  ]
  node [
    id 286
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 287
    label "uprawianie"
  ]
  node [
    id 288
    label "praca_rolnicza"
  ]
  node [
    id 289
    label "collection"
  ]
  node [
    id 290
    label "dane"
  ]
  node [
    id 291
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 292
    label "pakiet_klimatyczny"
  ]
  node [
    id 293
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 294
    label "sum"
  ]
  node [
    id 295
    label "gathering"
  ]
  node [
    id 296
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 297
    label "album"
  ]
  node [
    id 298
    label "pos&#322;uchanie"
  ]
  node [
    id 299
    label "skumanie"
  ]
  node [
    id 300
    label "orientacja"
  ]
  node [
    id 301
    label "zorientowanie"
  ]
  node [
    id 302
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 303
    label "clasp"
  ]
  node [
    id 304
    label "przem&#243;wienie"
  ]
  node [
    id 305
    label "kszta&#322;t"
  ]
  node [
    id 306
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 307
    label "jednostka_systematyczna"
  ]
  node [
    id 308
    label "poznanie"
  ]
  node [
    id 309
    label "leksem"
  ]
  node [
    id 310
    label "dzie&#322;o"
  ]
  node [
    id 311
    label "stan"
  ]
  node [
    id 312
    label "blaszka"
  ]
  node [
    id 313
    label "kantyzm"
  ]
  node [
    id 314
    label "zdolno&#347;&#263;"
  ]
  node [
    id 315
    label "cecha"
  ]
  node [
    id 316
    label "do&#322;ek"
  ]
  node [
    id 317
    label "zawarto&#347;&#263;"
  ]
  node [
    id 318
    label "gwiazda"
  ]
  node [
    id 319
    label "formality"
  ]
  node [
    id 320
    label "struktura"
  ]
  node [
    id 321
    label "wygl&#261;d"
  ]
  node [
    id 322
    label "mode"
  ]
  node [
    id 323
    label "morfem"
  ]
  node [
    id 324
    label "rdze&#324;"
  ]
  node [
    id 325
    label "kielich"
  ]
  node [
    id 326
    label "ornamentyka"
  ]
  node [
    id 327
    label "pasmo"
  ]
  node [
    id 328
    label "zwyczaj"
  ]
  node [
    id 329
    label "punkt_widzenia"
  ]
  node [
    id 330
    label "naczynie"
  ]
  node [
    id 331
    label "p&#322;at"
  ]
  node [
    id 332
    label "maszyna_drukarska"
  ]
  node [
    id 333
    label "style"
  ]
  node [
    id 334
    label "linearno&#347;&#263;"
  ]
  node [
    id 335
    label "wyra&#380;enie"
  ]
  node [
    id 336
    label "spirala"
  ]
  node [
    id 337
    label "dyspozycja"
  ]
  node [
    id 338
    label "odmiana"
  ]
  node [
    id 339
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 340
    label "October"
  ]
  node [
    id 341
    label "creation"
  ]
  node [
    id 342
    label "p&#281;tla"
  ]
  node [
    id 343
    label "arystotelizm"
  ]
  node [
    id 344
    label "szablon"
  ]
  node [
    id 345
    label "miniatura"
  ]
  node [
    id 346
    label "wagon"
  ]
  node [
    id 347
    label "mecz_mistrzowski"
  ]
  node [
    id 348
    label "arrangement"
  ]
  node [
    id 349
    label "class"
  ]
  node [
    id 350
    label "&#322;awka"
  ]
  node [
    id 351
    label "wykrzyknik"
  ]
  node [
    id 352
    label "zaleta"
  ]
  node [
    id 353
    label "programowanie_obiektowe"
  ]
  node [
    id 354
    label "tablica"
  ]
  node [
    id 355
    label "warstwa"
  ]
  node [
    id 356
    label "rezerwa"
  ]
  node [
    id 357
    label "gromada"
  ]
  node [
    id 358
    label "Ekwici"
  ]
  node [
    id 359
    label "&#347;rodowisko"
  ]
  node [
    id 360
    label "szko&#322;a"
  ]
  node [
    id 361
    label "organizacja"
  ]
  node [
    id 362
    label "sala"
  ]
  node [
    id 363
    label "pomoc"
  ]
  node [
    id 364
    label "form"
  ]
  node [
    id 365
    label "grupa"
  ]
  node [
    id 366
    label "przepisa&#263;"
  ]
  node [
    id 367
    label "jako&#347;&#263;"
  ]
  node [
    id 368
    label "znak_jako&#347;ci"
  ]
  node [
    id 369
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 370
    label "poziom"
  ]
  node [
    id 371
    label "promocja"
  ]
  node [
    id 372
    label "przepisanie"
  ]
  node [
    id 373
    label "kurs"
  ]
  node [
    id 374
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 375
    label "dziennik_lekcyjny"
  ]
  node [
    id 376
    label "typ"
  ]
  node [
    id 377
    label "fakcja"
  ]
  node [
    id 378
    label "obrona"
  ]
  node [
    id 379
    label "atak"
  ]
  node [
    id 380
    label "botanika"
  ]
  node [
    id 381
    label "uleganie"
  ]
  node [
    id 382
    label "ulec"
  ]
  node [
    id 383
    label "m&#281;&#380;yna"
  ]
  node [
    id 384
    label "ulegni&#281;cie"
  ]
  node [
    id 385
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 386
    label "babka"
  ]
  node [
    id 387
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 388
    label "pa&#324;stwo"
  ]
  node [
    id 389
    label "kobita"
  ]
  node [
    id 390
    label "przekwitanie"
  ]
  node [
    id 391
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 392
    label "przyw&#243;dczyni"
  ]
  node [
    id 393
    label "samica"
  ]
  node [
    id 394
    label "kobieta"
  ]
  node [
    id 395
    label "zwrot"
  ]
  node [
    id 396
    label "menopauza"
  ]
  node [
    id 397
    label "ulega&#263;"
  ]
  node [
    id 398
    label "doros&#322;y"
  ]
  node [
    id 399
    label "partner"
  ]
  node [
    id 400
    label "&#322;ono"
  ]
  node [
    id 401
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 402
    label "Katar"
  ]
  node [
    id 403
    label "Libia"
  ]
  node [
    id 404
    label "Gwatemala"
  ]
  node [
    id 405
    label "Ekwador"
  ]
  node [
    id 406
    label "Afganistan"
  ]
  node [
    id 407
    label "Tad&#380;ykistan"
  ]
  node [
    id 408
    label "Bhutan"
  ]
  node [
    id 409
    label "Argentyna"
  ]
  node [
    id 410
    label "D&#380;ibuti"
  ]
  node [
    id 411
    label "Wenezuela"
  ]
  node [
    id 412
    label "Gabon"
  ]
  node [
    id 413
    label "Ukraina"
  ]
  node [
    id 414
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 415
    label "Rwanda"
  ]
  node [
    id 416
    label "Liechtenstein"
  ]
  node [
    id 417
    label "Sri_Lanka"
  ]
  node [
    id 418
    label "Madagaskar"
  ]
  node [
    id 419
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 420
    label "Kongo"
  ]
  node [
    id 421
    label "Tonga"
  ]
  node [
    id 422
    label "Bangladesz"
  ]
  node [
    id 423
    label "Kanada"
  ]
  node [
    id 424
    label "Wehrlen"
  ]
  node [
    id 425
    label "Algieria"
  ]
  node [
    id 426
    label "Uganda"
  ]
  node [
    id 427
    label "Surinam"
  ]
  node [
    id 428
    label "Sahara_Zachodnia"
  ]
  node [
    id 429
    label "Chile"
  ]
  node [
    id 430
    label "W&#281;gry"
  ]
  node [
    id 431
    label "Birma"
  ]
  node [
    id 432
    label "Kazachstan"
  ]
  node [
    id 433
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 434
    label "Armenia"
  ]
  node [
    id 435
    label "Tuwalu"
  ]
  node [
    id 436
    label "Timor_Wschodni"
  ]
  node [
    id 437
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 438
    label "Izrael"
  ]
  node [
    id 439
    label "Estonia"
  ]
  node [
    id 440
    label "Komory"
  ]
  node [
    id 441
    label "Kamerun"
  ]
  node [
    id 442
    label "Haiti"
  ]
  node [
    id 443
    label "Belize"
  ]
  node [
    id 444
    label "Sierra_Leone"
  ]
  node [
    id 445
    label "Luksemburg"
  ]
  node [
    id 446
    label "USA"
  ]
  node [
    id 447
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 448
    label "Barbados"
  ]
  node [
    id 449
    label "San_Marino"
  ]
  node [
    id 450
    label "Bu&#322;garia"
  ]
  node [
    id 451
    label "Indonezja"
  ]
  node [
    id 452
    label "Wietnam"
  ]
  node [
    id 453
    label "Malawi"
  ]
  node [
    id 454
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 455
    label "Francja"
  ]
  node [
    id 456
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 457
    label "partia"
  ]
  node [
    id 458
    label "Zambia"
  ]
  node [
    id 459
    label "Angola"
  ]
  node [
    id 460
    label "Grenada"
  ]
  node [
    id 461
    label "Nepal"
  ]
  node [
    id 462
    label "Panama"
  ]
  node [
    id 463
    label "Rumunia"
  ]
  node [
    id 464
    label "Czarnog&#243;ra"
  ]
  node [
    id 465
    label "Malediwy"
  ]
  node [
    id 466
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 467
    label "S&#322;owacja"
  ]
  node [
    id 468
    label "para"
  ]
  node [
    id 469
    label "Egipt"
  ]
  node [
    id 470
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 471
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 472
    label "Mozambik"
  ]
  node [
    id 473
    label "Kolumbia"
  ]
  node [
    id 474
    label "Laos"
  ]
  node [
    id 475
    label "Burundi"
  ]
  node [
    id 476
    label "Suazi"
  ]
  node [
    id 477
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 478
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 479
    label "Czechy"
  ]
  node [
    id 480
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 481
    label "Wyspy_Marshalla"
  ]
  node [
    id 482
    label "Dominika"
  ]
  node [
    id 483
    label "Trynidad_i_Tobago"
  ]
  node [
    id 484
    label "Syria"
  ]
  node [
    id 485
    label "Palau"
  ]
  node [
    id 486
    label "Gwinea_Bissau"
  ]
  node [
    id 487
    label "Liberia"
  ]
  node [
    id 488
    label "Jamajka"
  ]
  node [
    id 489
    label "Zimbabwe"
  ]
  node [
    id 490
    label "Polska"
  ]
  node [
    id 491
    label "Dominikana"
  ]
  node [
    id 492
    label "Senegal"
  ]
  node [
    id 493
    label "Togo"
  ]
  node [
    id 494
    label "Gujana"
  ]
  node [
    id 495
    label "Gruzja"
  ]
  node [
    id 496
    label "Albania"
  ]
  node [
    id 497
    label "Zair"
  ]
  node [
    id 498
    label "Meksyk"
  ]
  node [
    id 499
    label "Macedonia"
  ]
  node [
    id 500
    label "Chorwacja"
  ]
  node [
    id 501
    label "Kambod&#380;a"
  ]
  node [
    id 502
    label "Monako"
  ]
  node [
    id 503
    label "Mauritius"
  ]
  node [
    id 504
    label "Gwinea"
  ]
  node [
    id 505
    label "Mali"
  ]
  node [
    id 506
    label "Nigeria"
  ]
  node [
    id 507
    label "Kostaryka"
  ]
  node [
    id 508
    label "Hanower"
  ]
  node [
    id 509
    label "Paragwaj"
  ]
  node [
    id 510
    label "W&#322;ochy"
  ]
  node [
    id 511
    label "Seszele"
  ]
  node [
    id 512
    label "Wyspy_Salomona"
  ]
  node [
    id 513
    label "Hiszpania"
  ]
  node [
    id 514
    label "Boliwia"
  ]
  node [
    id 515
    label "Kirgistan"
  ]
  node [
    id 516
    label "Irlandia"
  ]
  node [
    id 517
    label "Czad"
  ]
  node [
    id 518
    label "Irak"
  ]
  node [
    id 519
    label "Lesoto"
  ]
  node [
    id 520
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 521
    label "Malta"
  ]
  node [
    id 522
    label "Andora"
  ]
  node [
    id 523
    label "Chiny"
  ]
  node [
    id 524
    label "Filipiny"
  ]
  node [
    id 525
    label "Antarktis"
  ]
  node [
    id 526
    label "Niemcy"
  ]
  node [
    id 527
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 528
    label "Pakistan"
  ]
  node [
    id 529
    label "terytorium"
  ]
  node [
    id 530
    label "Nikaragua"
  ]
  node [
    id 531
    label "Brazylia"
  ]
  node [
    id 532
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 533
    label "Maroko"
  ]
  node [
    id 534
    label "Portugalia"
  ]
  node [
    id 535
    label "Niger"
  ]
  node [
    id 536
    label "Kenia"
  ]
  node [
    id 537
    label "Botswana"
  ]
  node [
    id 538
    label "Fid&#380;i"
  ]
  node [
    id 539
    label "Tunezja"
  ]
  node [
    id 540
    label "Australia"
  ]
  node [
    id 541
    label "Tajlandia"
  ]
  node [
    id 542
    label "Burkina_Faso"
  ]
  node [
    id 543
    label "interior"
  ]
  node [
    id 544
    label "Tanzania"
  ]
  node [
    id 545
    label "Benin"
  ]
  node [
    id 546
    label "Indie"
  ]
  node [
    id 547
    label "&#321;otwa"
  ]
  node [
    id 548
    label "Kiribati"
  ]
  node [
    id 549
    label "Antigua_i_Barbuda"
  ]
  node [
    id 550
    label "Rodezja"
  ]
  node [
    id 551
    label "Cypr"
  ]
  node [
    id 552
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 553
    label "Peru"
  ]
  node [
    id 554
    label "Austria"
  ]
  node [
    id 555
    label "Urugwaj"
  ]
  node [
    id 556
    label "Jordania"
  ]
  node [
    id 557
    label "Grecja"
  ]
  node [
    id 558
    label "Azerbejd&#380;an"
  ]
  node [
    id 559
    label "Turcja"
  ]
  node [
    id 560
    label "Samoa"
  ]
  node [
    id 561
    label "Sudan"
  ]
  node [
    id 562
    label "Oman"
  ]
  node [
    id 563
    label "ziemia"
  ]
  node [
    id 564
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 565
    label "Uzbekistan"
  ]
  node [
    id 566
    label "Portoryko"
  ]
  node [
    id 567
    label "Honduras"
  ]
  node [
    id 568
    label "Mongolia"
  ]
  node [
    id 569
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 570
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 571
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 572
    label "Serbia"
  ]
  node [
    id 573
    label "Tajwan"
  ]
  node [
    id 574
    label "Wielka_Brytania"
  ]
  node [
    id 575
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 576
    label "Liban"
  ]
  node [
    id 577
    label "Japonia"
  ]
  node [
    id 578
    label "Ghana"
  ]
  node [
    id 579
    label "Belgia"
  ]
  node [
    id 580
    label "Bahrajn"
  ]
  node [
    id 581
    label "Mikronezja"
  ]
  node [
    id 582
    label "Etiopia"
  ]
  node [
    id 583
    label "Kuwejt"
  ]
  node [
    id 584
    label "Bahamy"
  ]
  node [
    id 585
    label "Rosja"
  ]
  node [
    id 586
    label "Mo&#322;dawia"
  ]
  node [
    id 587
    label "Litwa"
  ]
  node [
    id 588
    label "S&#322;owenia"
  ]
  node [
    id 589
    label "Szwajcaria"
  ]
  node [
    id 590
    label "Erytrea"
  ]
  node [
    id 591
    label "Arabia_Saudyjska"
  ]
  node [
    id 592
    label "Kuba"
  ]
  node [
    id 593
    label "granica_pa&#324;stwa"
  ]
  node [
    id 594
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 595
    label "Malezja"
  ]
  node [
    id 596
    label "Korea"
  ]
  node [
    id 597
    label "Jemen"
  ]
  node [
    id 598
    label "Nowa_Zelandia"
  ]
  node [
    id 599
    label "Namibia"
  ]
  node [
    id 600
    label "Nauru"
  ]
  node [
    id 601
    label "holoarktyka"
  ]
  node [
    id 602
    label "Brunei"
  ]
  node [
    id 603
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 604
    label "Khitai"
  ]
  node [
    id 605
    label "Mauretania"
  ]
  node [
    id 606
    label "Iran"
  ]
  node [
    id 607
    label "Gambia"
  ]
  node [
    id 608
    label "Somalia"
  ]
  node [
    id 609
    label "Holandia"
  ]
  node [
    id 610
    label "Turkmenistan"
  ]
  node [
    id 611
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 612
    label "Salwador"
  ]
  node [
    id 613
    label "wydoro&#347;lenie"
  ]
  node [
    id 614
    label "du&#380;y"
  ]
  node [
    id 615
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 616
    label "doro&#347;lenie"
  ]
  node [
    id 617
    label "&#378;ra&#322;y"
  ]
  node [
    id 618
    label "doro&#347;le"
  ]
  node [
    id 619
    label "dojrzale"
  ]
  node [
    id 620
    label "dojrza&#322;y"
  ]
  node [
    id 621
    label "m&#261;dry"
  ]
  node [
    id 622
    label "doletni"
  ]
  node [
    id 623
    label "pracownik"
  ]
  node [
    id 624
    label "przedsi&#281;biorca"
  ]
  node [
    id 625
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 626
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 627
    label "kolaborator"
  ]
  node [
    id 628
    label "prowadzi&#263;"
  ]
  node [
    id 629
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 630
    label "sp&#243;lnik"
  ]
  node [
    id 631
    label "aktor"
  ]
  node [
    id 632
    label "uczestniczenie"
  ]
  node [
    id 633
    label "&#380;ona"
  ]
  node [
    id 634
    label "partnerka"
  ]
  node [
    id 635
    label "w&#322;adza"
  ]
  node [
    id 636
    label "punkt"
  ]
  node [
    id 637
    label "turn"
  ]
  node [
    id 638
    label "turning"
  ]
  node [
    id 639
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 640
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 641
    label "skr&#281;t"
  ]
  node [
    id 642
    label "obr&#243;t"
  ]
  node [
    id 643
    label "fraza_czasownikowa"
  ]
  node [
    id 644
    label "jednostka_leksykalna"
  ]
  node [
    id 645
    label "zmiana"
  ]
  node [
    id 646
    label "klatka_piersiowa"
  ]
  node [
    id 647
    label "penis"
  ]
  node [
    id 648
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 649
    label "brzuch"
  ]
  node [
    id 650
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 651
    label "podbrzusze"
  ]
  node [
    id 652
    label "l&#281;d&#378;wie"
  ]
  node [
    id 653
    label "wn&#281;trze"
  ]
  node [
    id 654
    label "dziedzina"
  ]
  node [
    id 655
    label "powierzchnia"
  ]
  node [
    id 656
    label "macica"
  ]
  node [
    id 657
    label "pochwa"
  ]
  node [
    id 658
    label "przodkini"
  ]
  node [
    id 659
    label "baba"
  ]
  node [
    id 660
    label "babulinka"
  ]
  node [
    id 661
    label "ciasto"
  ]
  node [
    id 662
    label "ro&#347;lina_zielna"
  ]
  node [
    id 663
    label "babkowate"
  ]
  node [
    id 664
    label "po&#322;o&#380;na"
  ]
  node [
    id 665
    label "dziadkowie"
  ]
  node [
    id 666
    label "ryba"
  ]
  node [
    id 667
    label "moneta"
  ]
  node [
    id 668
    label "plantain"
  ]
  node [
    id 669
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 670
    label "samka"
  ]
  node [
    id 671
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 672
    label "drogi_rodne"
  ]
  node [
    id 673
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 674
    label "zwierz&#281;"
  ]
  node [
    id 675
    label "female"
  ]
  node [
    id 676
    label "zezwalanie"
  ]
  node [
    id 677
    label "return"
  ]
  node [
    id 678
    label "zaliczanie"
  ]
  node [
    id 679
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 680
    label "poddawanie"
  ]
  node [
    id 681
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 682
    label "burst"
  ]
  node [
    id 683
    label "przywo&#322;anie"
  ]
  node [
    id 684
    label "naginanie_si&#281;"
  ]
  node [
    id 685
    label "poddawanie_si&#281;"
  ]
  node [
    id 686
    label "stawanie_si&#281;"
  ]
  node [
    id 687
    label "przywo&#322;a&#263;"
  ]
  node [
    id 688
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 689
    label "postpone"
  ]
  node [
    id 690
    label "render"
  ]
  node [
    id 691
    label "zezwala&#263;"
  ]
  node [
    id 692
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 693
    label "subject"
  ]
  node [
    id 694
    label "poddanie_si&#281;"
  ]
  node [
    id 695
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 696
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 697
    label "poddanie"
  ]
  node [
    id 698
    label "nagi&#281;cie_si&#281;"
  ]
  node [
    id 699
    label "pozwolenie"
  ]
  node [
    id 700
    label "subjugation"
  ]
  node [
    id 701
    label "stanie_si&#281;"
  ]
  node [
    id 702
    label "kwitnienie"
  ]
  node [
    id 703
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 704
    label "przemijanie"
  ]
  node [
    id 705
    label "przestawanie"
  ]
  node [
    id 706
    label "starzenie_si&#281;"
  ]
  node [
    id 707
    label "menopause"
  ]
  node [
    id 708
    label "obumieranie"
  ]
  node [
    id 709
    label "dojrzewanie"
  ]
  node [
    id 710
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 711
    label "sta&#263;_si&#281;"
  ]
  node [
    id 712
    label "fall"
  ]
  node [
    id 713
    label "give"
  ]
  node [
    id 714
    label "pozwoli&#263;"
  ]
  node [
    id 715
    label "podda&#263;"
  ]
  node [
    id 716
    label "put_in"
  ]
  node [
    id 717
    label "podda&#263;_si&#281;"
  ]
  node [
    id 718
    label "&#380;ycie"
  ]
  node [
    id 719
    label "&#322;atwo"
  ]
  node [
    id 720
    label "delikatny"
  ]
  node [
    id 721
    label "przewiewny"
  ]
  node [
    id 722
    label "nieg&#322;&#281;boko"
  ]
  node [
    id 723
    label "subtelny"
  ]
  node [
    id 724
    label "bezpieczny"
  ]
  node [
    id 725
    label "lekko"
  ]
  node [
    id 726
    label "polotny"
  ]
  node [
    id 727
    label "przyswajalny"
  ]
  node [
    id 728
    label "beztrosko"
  ]
  node [
    id 729
    label "&#322;atwy"
  ]
  node [
    id 730
    label "piaszczysty"
  ]
  node [
    id 731
    label "suchy"
  ]
  node [
    id 732
    label "letki"
  ]
  node [
    id 733
    label "p&#322;ynny"
  ]
  node [
    id 734
    label "dietetyczny"
  ]
  node [
    id 735
    label "lekkozbrojny"
  ]
  node [
    id 736
    label "delikatnie"
  ]
  node [
    id 737
    label "s&#322;aby"
  ]
  node [
    id 738
    label "&#322;acny"
  ]
  node [
    id 739
    label "snadny"
  ]
  node [
    id 740
    label "nierozwa&#380;ny"
  ]
  node [
    id 741
    label "g&#322;adko"
  ]
  node [
    id 742
    label "zgrabny"
  ]
  node [
    id 743
    label "przyjemny"
  ]
  node [
    id 744
    label "ubogi"
  ]
  node [
    id 745
    label "zwinnie"
  ]
  node [
    id 746
    label "beztroskliwy"
  ]
  node [
    id 747
    label "zr&#281;czny"
  ]
  node [
    id 748
    label "prosty"
  ]
  node [
    id 749
    label "cienki"
  ]
  node [
    id 750
    label "nieznacznie"
  ]
  node [
    id 751
    label "&#322;agodny"
  ]
  node [
    id 752
    label "wydelikacanie"
  ]
  node [
    id 753
    label "delikatnienie"
  ]
  node [
    id 754
    label "nieszkodliwy"
  ]
  node [
    id 755
    label "k&#322;opotliwy"
  ]
  node [
    id 756
    label "zdelikatnienie"
  ]
  node [
    id 757
    label "dra&#380;liwy"
  ]
  node [
    id 758
    label "ostro&#380;ny"
  ]
  node [
    id 759
    label "wra&#380;liwy"
  ]
  node [
    id 760
    label "&#322;agodnie"
  ]
  node [
    id 761
    label "wydelikacenie"
  ]
  node [
    id 762
    label "taktowny"
  ]
  node [
    id 763
    label "rozlewny"
  ]
  node [
    id 764
    label "cytozol"
  ]
  node [
    id 765
    label "up&#322;ynnianie"
  ]
  node [
    id 766
    label "roztapianie_si&#281;"
  ]
  node [
    id 767
    label "roztopienie_si&#281;"
  ]
  node [
    id 768
    label "p&#322;ynnie"
  ]
  node [
    id 769
    label "bieg&#322;y"
  ]
  node [
    id 770
    label "nieokre&#347;lony"
  ]
  node [
    id 771
    label "up&#322;ynnienie"
  ]
  node [
    id 772
    label "skuteczny"
  ]
  node [
    id 773
    label "zwinny"
  ]
  node [
    id 774
    label "zgrabnie"
  ]
  node [
    id 775
    label "sprytny"
  ]
  node [
    id 776
    label "sprawny"
  ]
  node [
    id 777
    label "gracki"
  ]
  node [
    id 778
    label "kszta&#322;tny"
  ]
  node [
    id 779
    label "harmonijny"
  ]
  node [
    id 780
    label "filigranowo"
  ]
  node [
    id 781
    label "drobny"
  ]
  node [
    id 782
    label "elegancki"
  ]
  node [
    id 783
    label "subtelnie"
  ]
  node [
    id 784
    label "wnikliwy"
  ]
  node [
    id 785
    label "nie&#347;mieszny"
  ]
  node [
    id 786
    label "niesympatyczny"
  ]
  node [
    id 787
    label "twardy"
  ]
  node [
    id 788
    label "sucho"
  ]
  node [
    id 789
    label "wysuszanie"
  ]
  node [
    id 790
    label "czczy"
  ]
  node [
    id 791
    label "sam"
  ]
  node [
    id 792
    label "wysuszenie_si&#281;"
  ]
  node [
    id 793
    label "do_sucha"
  ]
  node [
    id 794
    label "suszenie"
  ]
  node [
    id 795
    label "matowy"
  ]
  node [
    id 796
    label "wysuszenie"
  ]
  node [
    id 797
    label "cichy"
  ]
  node [
    id 798
    label "chudy"
  ]
  node [
    id 799
    label "ch&#322;odno"
  ]
  node [
    id 800
    label "bankrutowanie"
  ]
  node [
    id 801
    label "ubo&#380;enie"
  ]
  node [
    id 802
    label "go&#322;odupiec"
  ]
  node [
    id 803
    label "biedny"
  ]
  node [
    id 804
    label "zubo&#380;enie"
  ]
  node [
    id 805
    label "raw_material"
  ]
  node [
    id 806
    label "zubo&#380;anie"
  ]
  node [
    id 807
    label "ho&#322;ysz"
  ]
  node [
    id 808
    label "zbiednienie"
  ]
  node [
    id 809
    label "proletariusz"
  ]
  node [
    id 810
    label "sytuowany"
  ]
  node [
    id 811
    label "biedota"
  ]
  node [
    id 812
    label "ubogo"
  ]
  node [
    id 813
    label "biednie"
  ]
  node [
    id 814
    label "piaszczysto"
  ]
  node [
    id 815
    label "kruchy"
  ]
  node [
    id 816
    label "dietetycznie"
  ]
  node [
    id 817
    label "zdrowy"
  ]
  node [
    id 818
    label "mo&#380;liwy"
  ]
  node [
    id 819
    label "nietrwa&#322;y"
  ]
  node [
    id 820
    label "mizerny"
  ]
  node [
    id 821
    label "marnie"
  ]
  node [
    id 822
    label "po&#347;ledni"
  ]
  node [
    id 823
    label "niezdrowy"
  ]
  node [
    id 824
    label "z&#322;y"
  ]
  node [
    id 825
    label "nieumiej&#281;tny"
  ]
  node [
    id 826
    label "s&#322;abo"
  ]
  node [
    id 827
    label "nieznaczny"
  ]
  node [
    id 828
    label "lura"
  ]
  node [
    id 829
    label "nieudany"
  ]
  node [
    id 830
    label "s&#322;abowity"
  ]
  node [
    id 831
    label "zawodny"
  ]
  node [
    id 832
    label "md&#322;y"
  ]
  node [
    id 833
    label "niedoskona&#322;y"
  ]
  node [
    id 834
    label "przemijaj&#261;cy"
  ]
  node [
    id 835
    label "niemocny"
  ]
  node [
    id 836
    label "niefajny"
  ]
  node [
    id 837
    label "kiepsko"
  ]
  node [
    id 838
    label "trudny"
  ]
  node [
    id 839
    label "wysoki"
  ]
  node [
    id 840
    label "kiepski"
  ]
  node [
    id 841
    label "ulotny"
  ]
  node [
    id 842
    label "nieuchwytny"
  ]
  node [
    id 843
    label "w&#261;ski"
  ]
  node [
    id 844
    label "w&#261;t&#322;y"
  ]
  node [
    id 845
    label "cienko"
  ]
  node [
    id 846
    label "do_cienka"
  ]
  node [
    id 847
    label "nieostry"
  ]
  node [
    id 848
    label "niem&#261;dry"
  ]
  node [
    id 849
    label "nierozwa&#380;nie"
  ]
  node [
    id 850
    label "przyjemnie"
  ]
  node [
    id 851
    label "dobry"
  ]
  node [
    id 852
    label "skromny"
  ]
  node [
    id 853
    label "po_prostu"
  ]
  node [
    id 854
    label "naturalny"
  ]
  node [
    id 855
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 856
    label "rozprostowanie"
  ]
  node [
    id 857
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 858
    label "prosto"
  ]
  node [
    id 859
    label "prostowanie_si&#281;"
  ]
  node [
    id 860
    label "niepozorny"
  ]
  node [
    id 861
    label "cios"
  ]
  node [
    id 862
    label "prostoduszny"
  ]
  node [
    id 863
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 864
    label "naiwny"
  ]
  node [
    id 865
    label "prostowanie"
  ]
  node [
    id 866
    label "zwyk&#322;y"
  ]
  node [
    id 867
    label "zbrojny"
  ]
  node [
    id 868
    label "wojownik"
  ]
  node [
    id 869
    label "schronienie"
  ]
  node [
    id 870
    label "bezpiecznie"
  ]
  node [
    id 871
    label "polotnie"
  ]
  node [
    id 872
    label "mi&#281;kko"
  ]
  node [
    id 873
    label "pewnie"
  ]
  node [
    id 874
    label "mo&#380;liwie"
  ]
  node [
    id 875
    label "g&#322;adki"
  ]
  node [
    id 876
    label "sprawnie"
  ]
  node [
    id 877
    label "snadnie"
  ]
  node [
    id 878
    label "&#322;atwie"
  ]
  node [
    id 879
    label "&#322;acno"
  ]
  node [
    id 880
    label "nieg&#322;&#281;boki"
  ]
  node [
    id 881
    label "mi&#281;ciuchno"
  ]
  node [
    id 882
    label "beztroski"
  ]
  node [
    id 883
    label "pogodnie"
  ]
  node [
    id 884
    label "p&#322;ytki"
  ]
  node [
    id 885
    label "shallowly"
  ]
  node [
    id 886
    label "przewiewnie"
  ]
  node [
    id 887
    label "przestronny"
  ]
  node [
    id 888
    label "przepuszczalny"
  ]
  node [
    id 889
    label "&#380;yzny"
  ]
  node [
    id 890
    label "nie&#347;cis&#322;y"
  ]
  node [
    id 891
    label "szybko"
  ]
  node [
    id 892
    label "nieistotnie"
  ]
  node [
    id 893
    label "ostro&#380;nie"
  ]
  node [
    id 894
    label "grzecznie"
  ]
  node [
    id 895
    label "jednobarwnie"
  ]
  node [
    id 896
    label "bezproblemowo"
  ]
  node [
    id 897
    label "nieruchomo"
  ]
  node [
    id 898
    label "elegancko"
  ]
  node [
    id 899
    label "okr&#261;g&#322;o"
  ]
  node [
    id 900
    label "r&#243;wno"
  ]
  node [
    id 901
    label "og&#243;lnikowo"
  ]
  node [
    id 902
    label "szybki"
  ]
  node [
    id 903
    label "lotny"
  ]
  node [
    id 904
    label "b&#322;yskotliwy"
  ]
  node [
    id 905
    label "r&#261;czy"
  ]
  node [
    id 906
    label "ceremony"
  ]
  node [
    id 907
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 908
    label "kultura_duchowa"
  ]
  node [
    id 909
    label "tradycja"
  ]
  node [
    id 910
    label "asymilowanie_si&#281;"
  ]
  node [
    id 911
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 912
    label "Wsch&#243;d"
  ]
  node [
    id 913
    label "przejmowanie"
  ]
  node [
    id 914
    label "makrokosmos"
  ]
  node [
    id 915
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 916
    label "konwencja"
  ]
  node [
    id 917
    label "propriety"
  ]
  node [
    id 918
    label "przejmowa&#263;"
  ]
  node [
    id 919
    label "brzoskwiniarnia"
  ]
  node [
    id 920
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 921
    label "sztuka"
  ]
  node [
    id 922
    label "kuchnia"
  ]
  node [
    id 923
    label "populace"
  ]
  node [
    id 924
    label "hodowla"
  ]
  node [
    id 925
    label "religia"
  ]
  node [
    id 926
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 927
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 928
    label "przej&#281;cie"
  ]
  node [
    id 929
    label "przej&#261;&#263;"
  ]
  node [
    id 930
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 931
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 932
    label "zasymilowanie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 9
    target 896
  ]
  edge [
    source 9
    target 897
  ]
  edge [
    source 9
    target 898
  ]
  edge [
    source 9
    target 899
  ]
  edge [
    source 9
    target 900
  ]
  edge [
    source 9
    target 901
  ]
  edge [
    source 9
    target 902
  ]
  edge [
    source 9
    target 903
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 930
  ]
  edge [
    source 10
    target 931
  ]
  edge [
    source 10
    target 932
  ]
]
