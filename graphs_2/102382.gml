graph [
  node [
    id 0
    label "bzura"
    origin "text"
  ]
  node [
    id 1
    label "ozorek"
    origin "text"
  ]
  node [
    id 2
    label "zremisowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mecz"
    origin "text"
  ]
  node [
    id 4
    label "puchar"
    origin "text"
  ]
  node [
    id 5
    label "polska"
    origin "text"
  ]
  node [
    id 6
    label "ekolog"
    origin "text"
  ]
  node [
    id 7
    label "zdu&#324;ski"
    origin "text"
  ]
  node [
    id 8
    label "wola"
    origin "text"
  ]
  node [
    id 9
    label "pierwsza"
    origin "text"
  ]
  node [
    id 10
    label "by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "dla"
    origin "text"
  ]
  node [
    id 12
    label "fortecki"
    origin "text"
  ]
  node [
    id 13
    label "ciszewski"
    origin "text"
  ]
  node [
    id 14
    label "suli&#324;ski"
    origin "text"
  ]
  node [
    id 15
    label "olejniczak"
    origin "text"
  ]
  node [
    id 16
    label "borkowski"
    origin "text"
  ]
  node [
    id 17
    label "karolak"
    origin "text"
  ]
  node [
    id 18
    label "szpiegowski"
    origin "text"
  ]
  node [
    id 19
    label "chmielecki"
    origin "text"
  ]
  node [
    id 20
    label "ziemniak"
    origin "text"
  ]
  node [
    id 21
    label "banasiak"
    origin "text"
  ]
  node [
    id 22
    label "koziak"
    origin "text"
  ]
  node [
    id 23
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 24
    label "podroby"
  ]
  node [
    id 25
    label "grzyb"
  ]
  node [
    id 26
    label "ozorkowate"
  ]
  node [
    id 27
    label "paso&#380;yt"
  ]
  node [
    id 28
    label "saprotrof"
  ]
  node [
    id 29
    label "pieczarkowiec"
  ]
  node [
    id 30
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 31
    label "towar"
  ]
  node [
    id 32
    label "jedzenie"
  ]
  node [
    id 33
    label "mi&#281;so"
  ]
  node [
    id 34
    label "kszta&#322;t"
  ]
  node [
    id 35
    label "starzec"
  ]
  node [
    id 36
    label "papierzak"
  ]
  node [
    id 37
    label "choroba_somatyczna"
  ]
  node [
    id 38
    label "fungus"
  ]
  node [
    id 39
    label "grzyby"
  ]
  node [
    id 40
    label "blanszownik"
  ]
  node [
    id 41
    label "zrz&#281;da"
  ]
  node [
    id 42
    label "tetryk"
  ]
  node [
    id 43
    label "ramolenie"
  ]
  node [
    id 44
    label "borowiec"
  ]
  node [
    id 45
    label "fungal_infection"
  ]
  node [
    id 46
    label "pierdo&#322;a"
  ]
  node [
    id 47
    label "ko&#378;larz"
  ]
  node [
    id 48
    label "zramolenie"
  ]
  node [
    id 49
    label "gametangium"
  ]
  node [
    id 50
    label "plechowiec"
  ]
  node [
    id 51
    label "borowikowate"
  ]
  node [
    id 52
    label "plemnia"
  ]
  node [
    id 53
    label "pieczarniak"
  ]
  node [
    id 54
    label "zarodnia"
  ]
  node [
    id 55
    label "saprofit"
  ]
  node [
    id 56
    label "agaric"
  ]
  node [
    id 57
    label "bed&#322;ka"
  ]
  node [
    id 58
    label "pieczarkowce"
  ]
  node [
    id 59
    label "odwszawianie"
  ]
  node [
    id 60
    label "cz&#322;owiek"
  ]
  node [
    id 61
    label "odrobacza&#263;"
  ]
  node [
    id 62
    label "konsument"
  ]
  node [
    id 63
    label "odrobaczanie"
  ]
  node [
    id 64
    label "istota_&#380;ywa"
  ]
  node [
    id 65
    label "tie"
  ]
  node [
    id 66
    label "sko&#324;czy&#263;"
  ]
  node [
    id 67
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 68
    label "zrobi&#263;"
  ]
  node [
    id 69
    label "end"
  ]
  node [
    id 70
    label "zako&#324;czy&#263;"
  ]
  node [
    id 71
    label "communicate"
  ]
  node [
    id 72
    label "przesta&#263;"
  ]
  node [
    id 73
    label "obrona"
  ]
  node [
    id 74
    label "gra"
  ]
  node [
    id 75
    label "game"
  ]
  node [
    id 76
    label "serw"
  ]
  node [
    id 77
    label "dwumecz"
  ]
  node [
    id 78
    label "zmienno&#347;&#263;"
  ]
  node [
    id 79
    label "play"
  ]
  node [
    id 80
    label "rozgrywka"
  ]
  node [
    id 81
    label "apparent_motion"
  ]
  node [
    id 82
    label "wydarzenie"
  ]
  node [
    id 83
    label "contest"
  ]
  node [
    id 84
    label "akcja"
  ]
  node [
    id 85
    label "komplet"
  ]
  node [
    id 86
    label "zabawa"
  ]
  node [
    id 87
    label "zasada"
  ]
  node [
    id 88
    label "rywalizacja"
  ]
  node [
    id 89
    label "zbijany"
  ]
  node [
    id 90
    label "post&#281;powanie"
  ]
  node [
    id 91
    label "odg&#322;os"
  ]
  node [
    id 92
    label "Pok&#233;mon"
  ]
  node [
    id 93
    label "czynno&#347;&#263;"
  ]
  node [
    id 94
    label "synteza"
  ]
  node [
    id 95
    label "odtworzenie"
  ]
  node [
    id 96
    label "rekwizyt_do_gry"
  ]
  node [
    id 97
    label "egzamin"
  ]
  node [
    id 98
    label "walka"
  ]
  node [
    id 99
    label "liga"
  ]
  node [
    id 100
    label "gracz"
  ]
  node [
    id 101
    label "poj&#281;cie"
  ]
  node [
    id 102
    label "protection"
  ]
  node [
    id 103
    label "poparcie"
  ]
  node [
    id 104
    label "reakcja"
  ]
  node [
    id 105
    label "defense"
  ]
  node [
    id 106
    label "s&#261;d"
  ]
  node [
    id 107
    label "auspices"
  ]
  node [
    id 108
    label "ochrona"
  ]
  node [
    id 109
    label "sp&#243;r"
  ]
  node [
    id 110
    label "wojsko"
  ]
  node [
    id 111
    label "manewr"
  ]
  node [
    id 112
    label "defensive_structure"
  ]
  node [
    id 113
    label "guard_duty"
  ]
  node [
    id 114
    label "strona"
  ]
  node [
    id 115
    label "uderzenie"
  ]
  node [
    id 116
    label "naczynie"
  ]
  node [
    id 117
    label "nagroda"
  ]
  node [
    id 118
    label "zwyci&#281;stwo"
  ]
  node [
    id 119
    label "zawody"
  ]
  node [
    id 120
    label "zawarto&#347;&#263;"
  ]
  node [
    id 121
    label "impreza"
  ]
  node [
    id 122
    label "walczy&#263;"
  ]
  node [
    id 123
    label "walczenie"
  ]
  node [
    id 124
    label "tysi&#281;cznik"
  ]
  node [
    id 125
    label "champion"
  ]
  node [
    id 126
    label "spadochroniarstwo"
  ]
  node [
    id 127
    label "kategoria_open"
  ]
  node [
    id 128
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 129
    label "vessel"
  ]
  node [
    id 130
    label "sprz&#281;t"
  ]
  node [
    id 131
    label "statki"
  ]
  node [
    id 132
    label "rewaskularyzacja"
  ]
  node [
    id 133
    label "ceramika"
  ]
  node [
    id 134
    label "drewno"
  ]
  node [
    id 135
    label "przew&#243;d"
  ]
  node [
    id 136
    label "unaczyni&#263;"
  ]
  node [
    id 137
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 138
    label "receptacle"
  ]
  node [
    id 139
    label "oskar"
  ]
  node [
    id 140
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 141
    label "return"
  ]
  node [
    id 142
    label "konsekwencja"
  ]
  node [
    id 143
    label "temat"
  ]
  node [
    id 144
    label "ilo&#347;&#263;"
  ]
  node [
    id 145
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 146
    label "wn&#281;trze"
  ]
  node [
    id 147
    label "informacja"
  ]
  node [
    id 148
    label "beat"
  ]
  node [
    id 149
    label "poradzenie_sobie"
  ]
  node [
    id 150
    label "sukces"
  ]
  node [
    id 151
    label "conquest"
  ]
  node [
    id 152
    label "biomedyk"
  ]
  node [
    id 153
    label "zwolennik"
  ]
  node [
    id 154
    label "zieloni"
  ]
  node [
    id 155
    label "dzia&#322;acz"
  ]
  node [
    id 156
    label "biolog"
  ]
  node [
    id 157
    label "Asnyk"
  ]
  node [
    id 158
    label "Michnik"
  ]
  node [
    id 159
    label "Owsiak"
  ]
  node [
    id 160
    label "cz&#322;onek"
  ]
  node [
    id 161
    label "nauczyciel"
  ]
  node [
    id 162
    label "Darwin"
  ]
  node [
    id 163
    label "naukowiec"
  ]
  node [
    id 164
    label "Lamarck"
  ]
  node [
    id 165
    label "student"
  ]
  node [
    id 166
    label "organizacja"
  ]
  node [
    id 167
    label "partia"
  ]
  node [
    id 168
    label "lewica"
  ]
  node [
    id 169
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 170
    label "zajawka"
  ]
  node [
    id 171
    label "emocja"
  ]
  node [
    id 172
    label "oskoma"
  ]
  node [
    id 173
    label "mniemanie"
  ]
  node [
    id 174
    label "inclination"
  ]
  node [
    id 175
    label "wish"
  ]
  node [
    id 176
    label "treatment"
  ]
  node [
    id 177
    label "pogl&#261;d"
  ]
  node [
    id 178
    label "my&#347;lenie"
  ]
  node [
    id 179
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 180
    label "ogrom"
  ]
  node [
    id 181
    label "iskrzy&#263;"
  ]
  node [
    id 182
    label "d&#322;awi&#263;"
  ]
  node [
    id 183
    label "ostygn&#261;&#263;"
  ]
  node [
    id 184
    label "stygn&#261;&#263;"
  ]
  node [
    id 185
    label "stan"
  ]
  node [
    id 186
    label "temperatura"
  ]
  node [
    id 187
    label "wpa&#347;&#263;"
  ]
  node [
    id 188
    label "afekt"
  ]
  node [
    id 189
    label "wpada&#263;"
  ]
  node [
    id 190
    label "ch&#281;&#263;"
  ]
  node [
    id 191
    label "smak"
  ]
  node [
    id 192
    label "streszczenie"
  ]
  node [
    id 193
    label "harbinger"
  ]
  node [
    id 194
    label "zapowied&#378;"
  ]
  node [
    id 195
    label "zami&#322;owanie"
  ]
  node [
    id 196
    label "czasopismo"
  ]
  node [
    id 197
    label "reklama"
  ]
  node [
    id 198
    label "gadka"
  ]
  node [
    id 199
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 200
    label "godzina"
  ]
  node [
    id 201
    label "time"
  ]
  node [
    id 202
    label "doba"
  ]
  node [
    id 203
    label "p&#243;&#322;godzina"
  ]
  node [
    id 204
    label "jednostka_czasu"
  ]
  node [
    id 205
    label "czas"
  ]
  node [
    id 206
    label "minuta"
  ]
  node [
    id 207
    label "kwadrans"
  ]
  node [
    id 208
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 209
    label "mie&#263;_miejsce"
  ]
  node [
    id 210
    label "equal"
  ]
  node [
    id 211
    label "trwa&#263;"
  ]
  node [
    id 212
    label "chodzi&#263;"
  ]
  node [
    id 213
    label "si&#281;ga&#263;"
  ]
  node [
    id 214
    label "obecno&#347;&#263;"
  ]
  node [
    id 215
    label "stand"
  ]
  node [
    id 216
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 217
    label "uczestniczy&#263;"
  ]
  node [
    id 218
    label "participate"
  ]
  node [
    id 219
    label "robi&#263;"
  ]
  node [
    id 220
    label "istnie&#263;"
  ]
  node [
    id 221
    label "pozostawa&#263;"
  ]
  node [
    id 222
    label "zostawa&#263;"
  ]
  node [
    id 223
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 224
    label "adhere"
  ]
  node [
    id 225
    label "compass"
  ]
  node [
    id 226
    label "korzysta&#263;"
  ]
  node [
    id 227
    label "appreciation"
  ]
  node [
    id 228
    label "osi&#261;ga&#263;"
  ]
  node [
    id 229
    label "dociera&#263;"
  ]
  node [
    id 230
    label "get"
  ]
  node [
    id 231
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 232
    label "mierzy&#263;"
  ]
  node [
    id 233
    label "u&#380;ywa&#263;"
  ]
  node [
    id 234
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 235
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 236
    label "exsert"
  ]
  node [
    id 237
    label "being"
  ]
  node [
    id 238
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 239
    label "cecha"
  ]
  node [
    id 240
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 241
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 242
    label "p&#322;ywa&#263;"
  ]
  node [
    id 243
    label "run"
  ]
  node [
    id 244
    label "bangla&#263;"
  ]
  node [
    id 245
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 246
    label "przebiega&#263;"
  ]
  node [
    id 247
    label "wk&#322;ada&#263;"
  ]
  node [
    id 248
    label "proceed"
  ]
  node [
    id 249
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 250
    label "carry"
  ]
  node [
    id 251
    label "bywa&#263;"
  ]
  node [
    id 252
    label "dziama&#263;"
  ]
  node [
    id 253
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 254
    label "stara&#263;_si&#281;"
  ]
  node [
    id 255
    label "para"
  ]
  node [
    id 256
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 257
    label "str&#243;j"
  ]
  node [
    id 258
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 259
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 260
    label "krok"
  ]
  node [
    id 261
    label "tryb"
  ]
  node [
    id 262
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 263
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 264
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 265
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 266
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 267
    label "continue"
  ]
  node [
    id 268
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 269
    label "Ohio"
  ]
  node [
    id 270
    label "wci&#281;cie"
  ]
  node [
    id 271
    label "Nowy_York"
  ]
  node [
    id 272
    label "warstwa"
  ]
  node [
    id 273
    label "samopoczucie"
  ]
  node [
    id 274
    label "Illinois"
  ]
  node [
    id 275
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 276
    label "state"
  ]
  node [
    id 277
    label "Jukatan"
  ]
  node [
    id 278
    label "Kalifornia"
  ]
  node [
    id 279
    label "Wirginia"
  ]
  node [
    id 280
    label "wektor"
  ]
  node [
    id 281
    label "Goa"
  ]
  node [
    id 282
    label "Teksas"
  ]
  node [
    id 283
    label "Waszyngton"
  ]
  node [
    id 284
    label "miejsce"
  ]
  node [
    id 285
    label "Massachusetts"
  ]
  node [
    id 286
    label "Alaska"
  ]
  node [
    id 287
    label "Arakan"
  ]
  node [
    id 288
    label "Hawaje"
  ]
  node [
    id 289
    label "Maryland"
  ]
  node [
    id 290
    label "punkt"
  ]
  node [
    id 291
    label "Michigan"
  ]
  node [
    id 292
    label "Arizona"
  ]
  node [
    id 293
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 294
    label "Georgia"
  ]
  node [
    id 295
    label "poziom"
  ]
  node [
    id 296
    label "Pensylwania"
  ]
  node [
    id 297
    label "shape"
  ]
  node [
    id 298
    label "Luizjana"
  ]
  node [
    id 299
    label "Nowy_Meksyk"
  ]
  node [
    id 300
    label "Alabama"
  ]
  node [
    id 301
    label "Kansas"
  ]
  node [
    id 302
    label "Oregon"
  ]
  node [
    id 303
    label "Oklahoma"
  ]
  node [
    id 304
    label "Floryda"
  ]
  node [
    id 305
    label "jednostka_administracyjna"
  ]
  node [
    id 306
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 307
    label "dyskretny"
  ]
  node [
    id 308
    label "szpiegowsko"
  ]
  node [
    id 309
    label "wewn&#281;trzny"
  ]
  node [
    id 310
    label "nieznaczny"
  ]
  node [
    id 311
    label "dyskretnie"
  ]
  node [
    id 312
    label "milcz&#261;cy"
  ]
  node [
    id 313
    label "niezr&#281;czny"
  ]
  node [
    id 314
    label "intymny"
  ]
  node [
    id 315
    label "taktowny"
  ]
  node [
    id 316
    label "cichy"
  ]
  node [
    id 317
    label "bulwa_p&#281;dowa"
  ]
  node [
    id 318
    label "p&#281;t&#243;wka"
  ]
  node [
    id 319
    label "grula"
  ]
  node [
    id 320
    label "psianka"
  ]
  node [
    id 321
    label "bylina"
  ]
  node [
    id 322
    label "ro&#347;lina_bulwiasta"
  ]
  node [
    id 323
    label "warzywo"
  ]
  node [
    id 324
    label "ba&#322;aban"
  ]
  node [
    id 325
    label "m&#261;ka_ziemniaczana"
  ]
  node [
    id 326
    label "potato"
  ]
  node [
    id 327
    label "ro&#347;lina"
  ]
  node [
    id 328
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 329
    label "produkt"
  ]
  node [
    id 330
    label "ogrodowizna"
  ]
  node [
    id 331
    label "zielenina"
  ]
  node [
    id 332
    label "obieralnia"
  ]
  node [
    id 333
    label "nieuleczalnie_chory"
  ]
  node [
    id 334
    label "psiankowate"
  ]
  node [
    id 335
    label "jagoda"
  ]
  node [
    id 336
    label "ro&#347;lina_zielna"
  ]
  node [
    id 337
    label "ludowy"
  ]
  node [
    id 338
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 339
    label "utw&#243;r_epicki"
  ]
  node [
    id 340
    label "pie&#347;&#324;"
  ]
  node [
    id 341
    label "zbiorowisko"
  ]
  node [
    id 342
    label "ro&#347;liny"
  ]
  node [
    id 343
    label "p&#281;d"
  ]
  node [
    id 344
    label "wegetowanie"
  ]
  node [
    id 345
    label "zadziorek"
  ]
  node [
    id 346
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 347
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 348
    label "do&#322;owa&#263;"
  ]
  node [
    id 349
    label "wegetacja"
  ]
  node [
    id 350
    label "owoc"
  ]
  node [
    id 351
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 352
    label "strzyc"
  ]
  node [
    id 353
    label "w&#322;&#243;kno"
  ]
  node [
    id 354
    label "g&#322;uszenie"
  ]
  node [
    id 355
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 356
    label "fitotron"
  ]
  node [
    id 357
    label "bulwka"
  ]
  node [
    id 358
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 359
    label "odn&#243;&#380;ka"
  ]
  node [
    id 360
    label "epiderma"
  ]
  node [
    id 361
    label "gumoza"
  ]
  node [
    id 362
    label "strzy&#380;enie"
  ]
  node [
    id 363
    label "wypotnik"
  ]
  node [
    id 364
    label "flawonoid"
  ]
  node [
    id 365
    label "wyro&#347;le"
  ]
  node [
    id 366
    label "do&#322;owanie"
  ]
  node [
    id 367
    label "g&#322;uszy&#263;"
  ]
  node [
    id 368
    label "pora&#380;a&#263;"
  ]
  node [
    id 369
    label "fitocenoza"
  ]
  node [
    id 370
    label "hodowla"
  ]
  node [
    id 371
    label "fotoautotrof"
  ]
  node [
    id 372
    label "wegetowa&#263;"
  ]
  node [
    id 373
    label "pochewka"
  ]
  node [
    id 374
    label "sok"
  ]
  node [
    id 375
    label "system_korzeniowy"
  ]
  node [
    id 376
    label "zawi&#261;zek"
  ]
  node [
    id 377
    label "ropucha"
  ]
  node [
    id 378
    label "ropuszkowate"
  ]
  node [
    id 379
    label "przedmiot"
  ]
  node [
    id 380
    label "zboczenie"
  ]
  node [
    id 381
    label "om&#243;wienie"
  ]
  node [
    id 382
    label "sponiewieranie"
  ]
  node [
    id 383
    label "discipline"
  ]
  node [
    id 384
    label "rzecz"
  ]
  node [
    id 385
    label "omawia&#263;"
  ]
  node [
    id 386
    label "kr&#261;&#380;enie"
  ]
  node [
    id 387
    label "tre&#347;&#263;"
  ]
  node [
    id 388
    label "robienie"
  ]
  node [
    id 389
    label "sponiewiera&#263;"
  ]
  node [
    id 390
    label "element"
  ]
  node [
    id 391
    label "entity"
  ]
  node [
    id 392
    label "tematyka"
  ]
  node [
    id 393
    label "w&#261;tek"
  ]
  node [
    id 394
    label "charakter"
  ]
  node [
    id 395
    label "zbaczanie"
  ]
  node [
    id 396
    label "program_nauczania"
  ]
  node [
    id 397
    label "om&#243;wi&#263;"
  ]
  node [
    id 398
    label "omawianie"
  ]
  node [
    id 399
    label "thing"
  ]
  node [
    id 400
    label "kultura"
  ]
  node [
    id 401
    label "istota"
  ]
  node [
    id 402
    label "zbacza&#263;"
  ]
  node [
    id 403
    label "zboczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 307
  ]
  edge [
    source 18
    target 308
  ]
  edge [
    source 18
    target 309
  ]
  edge [
    source 18
    target 310
  ]
  edge [
    source 18
    target 311
  ]
  edge [
    source 18
    target 312
  ]
  edge [
    source 18
    target 313
  ]
  edge [
    source 18
    target 314
  ]
  edge [
    source 18
    target 315
  ]
  edge [
    source 18
    target 316
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 317
  ]
  edge [
    source 20
    target 318
  ]
  edge [
    source 20
    target 319
  ]
  edge [
    source 20
    target 320
  ]
  edge [
    source 20
    target 321
  ]
  edge [
    source 20
    target 322
  ]
  edge [
    source 20
    target 323
  ]
  edge [
    source 20
    target 324
  ]
  edge [
    source 20
    target 325
  ]
  edge [
    source 20
    target 326
  ]
  edge [
    source 20
    target 327
  ]
  edge [
    source 20
    target 328
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 329
  ]
  edge [
    source 20
    target 330
  ]
  edge [
    source 20
    target 331
  ]
  edge [
    source 20
    target 332
  ]
  edge [
    source 20
    target 333
  ]
  edge [
    source 20
    target 334
  ]
  edge [
    source 20
    target 335
  ]
  edge [
    source 20
    target 336
  ]
  edge [
    source 20
    target 337
  ]
  edge [
    source 20
    target 338
  ]
  edge [
    source 20
    target 339
  ]
  edge [
    source 20
    target 340
  ]
  edge [
    source 20
    target 341
  ]
  edge [
    source 20
    target 342
  ]
  edge [
    source 20
    target 343
  ]
  edge [
    source 20
    target 344
  ]
  edge [
    source 20
    target 345
  ]
  edge [
    source 20
    target 346
  ]
  edge [
    source 20
    target 347
  ]
  edge [
    source 20
    target 348
  ]
  edge [
    source 20
    target 349
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 351
  ]
  edge [
    source 20
    target 352
  ]
  edge [
    source 20
    target 353
  ]
  edge [
    source 20
    target 354
  ]
  edge [
    source 20
    target 355
  ]
  edge [
    source 20
    target 356
  ]
  edge [
    source 20
    target 357
  ]
  edge [
    source 20
    target 358
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 360
  ]
  edge [
    source 20
    target 361
  ]
  edge [
    source 20
    target 362
  ]
  edge [
    source 20
    target 363
  ]
  edge [
    source 20
    target 364
  ]
  edge [
    source 20
    target 365
  ]
  edge [
    source 20
    target 366
  ]
  edge [
    source 20
    target 367
  ]
  edge [
    source 20
    target 368
  ]
  edge [
    source 20
    target 369
  ]
  edge [
    source 20
    target 370
  ]
  edge [
    source 20
    target 371
  ]
  edge [
    source 20
    target 372
  ]
  edge [
    source 20
    target 373
  ]
  edge [
    source 20
    target 374
  ]
  edge [
    source 20
    target 375
  ]
  edge [
    source 20
    target 376
  ]
  edge [
    source 20
    target 377
  ]
  edge [
    source 20
    target 378
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 379
  ]
  edge [
    source 23
    target 380
  ]
  edge [
    source 23
    target 381
  ]
  edge [
    source 23
    target 382
  ]
  edge [
    source 23
    target 383
  ]
  edge [
    source 23
    target 384
  ]
  edge [
    source 23
    target 385
  ]
  edge [
    source 23
    target 386
  ]
  edge [
    source 23
    target 387
  ]
  edge [
    source 23
    target 388
  ]
  edge [
    source 23
    target 389
  ]
  edge [
    source 23
    target 390
  ]
  edge [
    source 23
    target 391
  ]
  edge [
    source 23
    target 253
  ]
  edge [
    source 23
    target 392
  ]
  edge [
    source 23
    target 393
  ]
  edge [
    source 23
    target 394
  ]
  edge [
    source 23
    target 395
  ]
  edge [
    source 23
    target 396
  ]
  edge [
    source 23
    target 397
  ]
  edge [
    source 23
    target 398
  ]
  edge [
    source 23
    target 399
  ]
  edge [
    source 23
    target 400
  ]
  edge [
    source 23
    target 401
  ]
  edge [
    source 23
    target 402
  ]
  edge [
    source 23
    target 403
  ]
]
