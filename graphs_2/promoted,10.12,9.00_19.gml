graph [
  node [
    id 0
    label "obci&#261;&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "gospodarka"
    origin "text"
  ]
  node [
    id 3
    label "podatek"
    origin "text"
  ]
  node [
    id 4
    label "wzrosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "psucie_si&#281;"
  ]
  node [
    id 6
    label "oskar&#380;enie"
  ]
  node [
    id 7
    label "zaszkodzenie"
  ]
  node [
    id 8
    label "baga&#380;"
  ]
  node [
    id 9
    label "loading"
  ]
  node [
    id 10
    label "charge"
  ]
  node [
    id 11
    label "hindrance"
  ]
  node [
    id 12
    label "na&#322;o&#380;enie"
  ]
  node [
    id 13
    label "zawada"
  ]
  node [
    id 14
    label "encumbrance"
  ]
  node [
    id 15
    label "zobowi&#261;zanie"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "torba"
  ]
  node [
    id 18
    label "nadbaga&#380;"
  ]
  node [
    id 19
    label "pakunek"
  ]
  node [
    id 20
    label "zas&#243;b"
  ]
  node [
    id 21
    label "baga&#380;&#243;wka"
  ]
  node [
    id 22
    label "hazard"
  ]
  node [
    id 23
    label "przeszkoda"
  ]
  node [
    id 24
    label "stosunek_prawny"
  ]
  node [
    id 25
    label "oblig"
  ]
  node [
    id 26
    label "uregulowa&#263;"
  ]
  node [
    id 27
    label "oddzia&#322;anie"
  ]
  node [
    id 28
    label "occupation"
  ]
  node [
    id 29
    label "duty"
  ]
  node [
    id 30
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 31
    label "zapowied&#378;"
  ]
  node [
    id 32
    label "obowi&#261;zek"
  ]
  node [
    id 33
    label "statement"
  ]
  node [
    id 34
    label "zapewnienie"
  ]
  node [
    id 35
    label "po&#322;o&#380;enie"
  ]
  node [
    id 36
    label "obleczenie_si&#281;"
  ]
  node [
    id 37
    label "poubieranie"
  ]
  node [
    id 38
    label "przyodzianie"
  ]
  node [
    id 39
    label "measurement"
  ]
  node [
    id 40
    label "str&#243;j"
  ]
  node [
    id 41
    label "infliction"
  ]
  node [
    id 42
    label "spowodowanie"
  ]
  node [
    id 43
    label "obleczenie"
  ]
  node [
    id 44
    label "umieszczenie"
  ]
  node [
    id 45
    label "rozebranie"
  ]
  node [
    id 46
    label "przywdzianie"
  ]
  node [
    id 47
    label "przebranie"
  ]
  node [
    id 48
    label "czynno&#347;&#263;"
  ]
  node [
    id 49
    label "przymierzenie"
  ]
  node [
    id 50
    label "s&#261;d"
  ]
  node [
    id 51
    label "skar&#380;yciel"
  ]
  node [
    id 52
    label "wypowied&#378;"
  ]
  node [
    id 53
    label "suspicja"
  ]
  node [
    id 54
    label "poj&#281;cie"
  ]
  node [
    id 55
    label "post&#281;powanie"
  ]
  node [
    id 56
    label "ocenienie"
  ]
  node [
    id 57
    label "ocena"
  ]
  node [
    id 58
    label "strona"
  ]
  node [
    id 59
    label "damage"
  ]
  node [
    id 60
    label "zrobienie"
  ]
  node [
    id 61
    label "Polish"
  ]
  node [
    id 62
    label "goniony"
  ]
  node [
    id 63
    label "oberek"
  ]
  node [
    id 64
    label "ryba_po_grecku"
  ]
  node [
    id 65
    label "sztajer"
  ]
  node [
    id 66
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 67
    label "krakowiak"
  ]
  node [
    id 68
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 69
    label "pierogi_ruskie"
  ]
  node [
    id 70
    label "lacki"
  ]
  node [
    id 71
    label "polak"
  ]
  node [
    id 72
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 73
    label "chodzony"
  ]
  node [
    id 74
    label "po_polsku"
  ]
  node [
    id 75
    label "mazur"
  ]
  node [
    id 76
    label "polsko"
  ]
  node [
    id 77
    label "skoczny"
  ]
  node [
    id 78
    label "drabant"
  ]
  node [
    id 79
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 80
    label "j&#281;zyk"
  ]
  node [
    id 81
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 82
    label "artykulator"
  ]
  node [
    id 83
    label "kod"
  ]
  node [
    id 84
    label "kawa&#322;ek"
  ]
  node [
    id 85
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 86
    label "gramatyka"
  ]
  node [
    id 87
    label "stylik"
  ]
  node [
    id 88
    label "przet&#322;umaczenie"
  ]
  node [
    id 89
    label "formalizowanie"
  ]
  node [
    id 90
    label "ssanie"
  ]
  node [
    id 91
    label "ssa&#263;"
  ]
  node [
    id 92
    label "language"
  ]
  node [
    id 93
    label "liza&#263;"
  ]
  node [
    id 94
    label "napisa&#263;"
  ]
  node [
    id 95
    label "konsonantyzm"
  ]
  node [
    id 96
    label "wokalizm"
  ]
  node [
    id 97
    label "pisa&#263;"
  ]
  node [
    id 98
    label "fonetyka"
  ]
  node [
    id 99
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 100
    label "jeniec"
  ]
  node [
    id 101
    label "but"
  ]
  node [
    id 102
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 103
    label "po_koroniarsku"
  ]
  node [
    id 104
    label "kultura_duchowa"
  ]
  node [
    id 105
    label "t&#322;umaczenie"
  ]
  node [
    id 106
    label "m&#243;wienie"
  ]
  node [
    id 107
    label "pype&#263;"
  ]
  node [
    id 108
    label "lizanie"
  ]
  node [
    id 109
    label "pismo"
  ]
  node [
    id 110
    label "formalizowa&#263;"
  ]
  node [
    id 111
    label "rozumie&#263;"
  ]
  node [
    id 112
    label "organ"
  ]
  node [
    id 113
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 114
    label "rozumienie"
  ]
  node [
    id 115
    label "spos&#243;b"
  ]
  node [
    id 116
    label "makroglosja"
  ]
  node [
    id 117
    label "m&#243;wi&#263;"
  ]
  node [
    id 118
    label "jama_ustna"
  ]
  node [
    id 119
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 120
    label "formacja_geologiczna"
  ]
  node [
    id 121
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 122
    label "natural_language"
  ]
  node [
    id 123
    label "s&#322;ownictwo"
  ]
  node [
    id 124
    label "urz&#261;dzenie"
  ]
  node [
    id 125
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 126
    label "wschodnioeuropejski"
  ]
  node [
    id 127
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 128
    label "poga&#324;ski"
  ]
  node [
    id 129
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 130
    label "topielec"
  ]
  node [
    id 131
    label "europejski"
  ]
  node [
    id 132
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 133
    label "langosz"
  ]
  node [
    id 134
    label "zboczenie"
  ]
  node [
    id 135
    label "om&#243;wienie"
  ]
  node [
    id 136
    label "sponiewieranie"
  ]
  node [
    id 137
    label "discipline"
  ]
  node [
    id 138
    label "rzecz"
  ]
  node [
    id 139
    label "omawia&#263;"
  ]
  node [
    id 140
    label "kr&#261;&#380;enie"
  ]
  node [
    id 141
    label "tre&#347;&#263;"
  ]
  node [
    id 142
    label "robienie"
  ]
  node [
    id 143
    label "sponiewiera&#263;"
  ]
  node [
    id 144
    label "element"
  ]
  node [
    id 145
    label "entity"
  ]
  node [
    id 146
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 147
    label "tematyka"
  ]
  node [
    id 148
    label "w&#261;tek"
  ]
  node [
    id 149
    label "charakter"
  ]
  node [
    id 150
    label "zbaczanie"
  ]
  node [
    id 151
    label "program_nauczania"
  ]
  node [
    id 152
    label "om&#243;wi&#263;"
  ]
  node [
    id 153
    label "omawianie"
  ]
  node [
    id 154
    label "thing"
  ]
  node [
    id 155
    label "kultura"
  ]
  node [
    id 156
    label "istota"
  ]
  node [
    id 157
    label "zbacza&#263;"
  ]
  node [
    id 158
    label "zboczy&#263;"
  ]
  node [
    id 159
    label "gwardzista"
  ]
  node [
    id 160
    label "melodia"
  ]
  node [
    id 161
    label "taniec"
  ]
  node [
    id 162
    label "taniec_ludowy"
  ]
  node [
    id 163
    label "&#347;redniowieczny"
  ]
  node [
    id 164
    label "europejsko"
  ]
  node [
    id 165
    label "specjalny"
  ]
  node [
    id 166
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 167
    label "weso&#322;y"
  ]
  node [
    id 168
    label "sprawny"
  ]
  node [
    id 169
    label "rytmiczny"
  ]
  node [
    id 170
    label "skocznie"
  ]
  node [
    id 171
    label "energiczny"
  ]
  node [
    id 172
    label "przytup"
  ]
  node [
    id 173
    label "ho&#322;ubiec"
  ]
  node [
    id 174
    label "wodzi&#263;"
  ]
  node [
    id 175
    label "lendler"
  ]
  node [
    id 176
    label "austriacki"
  ]
  node [
    id 177
    label "polka"
  ]
  node [
    id 178
    label "ludowy"
  ]
  node [
    id 179
    label "pie&#347;&#324;"
  ]
  node [
    id 180
    label "mieszkaniec"
  ]
  node [
    id 181
    label "centu&#347;"
  ]
  node [
    id 182
    label "lalka"
  ]
  node [
    id 183
    label "Ma&#322;opolanin"
  ]
  node [
    id 184
    label "krakauer"
  ]
  node [
    id 185
    label "inwentarz"
  ]
  node [
    id 186
    label "rynek"
  ]
  node [
    id 187
    label "mieszkalnictwo"
  ]
  node [
    id 188
    label "agregat_ekonomiczny"
  ]
  node [
    id 189
    label "miejsce_pracy"
  ]
  node [
    id 190
    label "farmaceutyka"
  ]
  node [
    id 191
    label "produkowanie"
  ]
  node [
    id 192
    label "rolnictwo"
  ]
  node [
    id 193
    label "transport"
  ]
  node [
    id 194
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 195
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 196
    label "obronno&#347;&#263;"
  ]
  node [
    id 197
    label "sektor_prywatny"
  ]
  node [
    id 198
    label "sch&#322;adza&#263;"
  ]
  node [
    id 199
    label "czerwona_strefa"
  ]
  node [
    id 200
    label "struktura"
  ]
  node [
    id 201
    label "pole"
  ]
  node [
    id 202
    label "sektor_publiczny"
  ]
  node [
    id 203
    label "bankowo&#347;&#263;"
  ]
  node [
    id 204
    label "gospodarowanie"
  ]
  node [
    id 205
    label "obora"
  ]
  node [
    id 206
    label "gospodarka_wodna"
  ]
  node [
    id 207
    label "gospodarka_le&#347;na"
  ]
  node [
    id 208
    label "gospodarowa&#263;"
  ]
  node [
    id 209
    label "fabryka"
  ]
  node [
    id 210
    label "wytw&#243;rnia"
  ]
  node [
    id 211
    label "stodo&#322;a"
  ]
  node [
    id 212
    label "przemys&#322;"
  ]
  node [
    id 213
    label "spichlerz"
  ]
  node [
    id 214
    label "sch&#322;adzanie"
  ]
  node [
    id 215
    label "administracja"
  ]
  node [
    id 216
    label "sch&#322;odzenie"
  ]
  node [
    id 217
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 218
    label "zasada"
  ]
  node [
    id 219
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 220
    label "regulacja_cen"
  ]
  node [
    id 221
    label "szkolnictwo"
  ]
  node [
    id 222
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 223
    label "mechanika"
  ]
  node [
    id 224
    label "o&#347;"
  ]
  node [
    id 225
    label "usenet"
  ]
  node [
    id 226
    label "rozprz&#261;c"
  ]
  node [
    id 227
    label "zachowanie"
  ]
  node [
    id 228
    label "cybernetyk"
  ]
  node [
    id 229
    label "podsystem"
  ]
  node [
    id 230
    label "system"
  ]
  node [
    id 231
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 232
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 233
    label "sk&#322;ad"
  ]
  node [
    id 234
    label "systemat"
  ]
  node [
    id 235
    label "cecha"
  ]
  node [
    id 236
    label "konstrukcja"
  ]
  node [
    id 237
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 238
    label "konstelacja"
  ]
  node [
    id 239
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 240
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 241
    label "regu&#322;a_Allena"
  ]
  node [
    id 242
    label "base"
  ]
  node [
    id 243
    label "umowa"
  ]
  node [
    id 244
    label "obserwacja"
  ]
  node [
    id 245
    label "zasada_d'Alemberta"
  ]
  node [
    id 246
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 247
    label "normalizacja"
  ]
  node [
    id 248
    label "moralno&#347;&#263;"
  ]
  node [
    id 249
    label "criterion"
  ]
  node [
    id 250
    label "opis"
  ]
  node [
    id 251
    label "regu&#322;a_Glogera"
  ]
  node [
    id 252
    label "prawo_Mendla"
  ]
  node [
    id 253
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 254
    label "twierdzenie"
  ]
  node [
    id 255
    label "prawo"
  ]
  node [
    id 256
    label "standard"
  ]
  node [
    id 257
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 258
    label "qualification"
  ]
  node [
    id 259
    label "dominion"
  ]
  node [
    id 260
    label "podstawa"
  ]
  node [
    id 261
    label "substancja"
  ]
  node [
    id 262
    label "prawid&#322;o"
  ]
  node [
    id 263
    label "stable"
  ]
  node [
    id 264
    label "budynek_inwentarski"
  ]
  node [
    id 265
    label "krowiarnia"
  ]
  node [
    id 266
    label "gospodarstwo"
  ]
  node [
    id 267
    label "building"
  ]
  node [
    id 268
    label "wr&#243;tnia"
  ]
  node [
    id 269
    label "budynek_gospodarczy"
  ]
  node [
    id 270
    label "budynek"
  ]
  node [
    id 271
    label "s&#261;siek"
  ]
  node [
    id 272
    label "szafarnia"
  ]
  node [
    id 273
    label "&#347;pichrz"
  ]
  node [
    id 274
    label "region"
  ]
  node [
    id 275
    label "zbo&#380;e"
  ]
  node [
    id 276
    label "zbiornik"
  ]
  node [
    id 277
    label "magazyn"
  ]
  node [
    id 278
    label "&#347;pichlerz"
  ]
  node [
    id 279
    label "uprawienie"
  ]
  node [
    id 280
    label "u&#322;o&#380;enie"
  ]
  node [
    id 281
    label "p&#322;osa"
  ]
  node [
    id 282
    label "ziemia"
  ]
  node [
    id 283
    label "t&#322;o"
  ]
  node [
    id 284
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 285
    label "uprawi&#263;"
  ]
  node [
    id 286
    label "room"
  ]
  node [
    id 287
    label "dw&#243;r"
  ]
  node [
    id 288
    label "okazja"
  ]
  node [
    id 289
    label "rozmiar"
  ]
  node [
    id 290
    label "irygowanie"
  ]
  node [
    id 291
    label "compass"
  ]
  node [
    id 292
    label "square"
  ]
  node [
    id 293
    label "zmienna"
  ]
  node [
    id 294
    label "irygowa&#263;"
  ]
  node [
    id 295
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 296
    label "socjologia"
  ]
  node [
    id 297
    label "boisko"
  ]
  node [
    id 298
    label "dziedzina"
  ]
  node [
    id 299
    label "baza_danych"
  ]
  node [
    id 300
    label "przestrze&#324;"
  ]
  node [
    id 301
    label "zagon"
  ]
  node [
    id 302
    label "obszar"
  ]
  node [
    id 303
    label "powierzchnia"
  ]
  node [
    id 304
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 305
    label "plane"
  ]
  node [
    id 306
    label "radlina"
  ]
  node [
    id 307
    label "mienie"
  ]
  node [
    id 308
    label "spis"
  ]
  node [
    id 309
    label "stock"
  ]
  node [
    id 310
    label "Karta_Nauczyciela"
  ]
  node [
    id 311
    label "finanse"
  ]
  node [
    id 312
    label "bankowo&#347;&#263;_elektroniczna"
  ]
  node [
    id 313
    label "bankowo&#347;&#263;_detaliczna"
  ]
  node [
    id 314
    label "bankowo&#347;&#263;_inwestycyjna"
  ]
  node [
    id 315
    label "supernadz&#243;r"
  ]
  node [
    id 316
    label "bankowo&#347;&#263;_sp&#243;&#322;dzielcza"
  ]
  node [
    id 317
    label "ekonomia"
  ]
  node [
    id 318
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 319
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 320
    label "uprzemys&#322;owienie"
  ]
  node [
    id 321
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 322
    label "przechowalnictwo"
  ]
  node [
    id 323
    label "uprzemys&#322;awianie"
  ]
  node [
    id 324
    label "mieszkani&#243;wka"
  ]
  node [
    id 325
    label "roz&#322;adunek"
  ]
  node [
    id 326
    label "sprz&#281;t"
  ]
  node [
    id 327
    label "cedu&#322;a"
  ]
  node [
    id 328
    label "jednoszynowy"
  ]
  node [
    id 329
    label "unos"
  ]
  node [
    id 330
    label "traffic"
  ]
  node [
    id 331
    label "prze&#322;adunek"
  ]
  node [
    id 332
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 333
    label "us&#322;uga"
  ]
  node [
    id 334
    label "komunikacja"
  ]
  node [
    id 335
    label "tyfon"
  ]
  node [
    id 336
    label "zawarto&#347;&#263;"
  ]
  node [
    id 337
    label "grupa"
  ]
  node [
    id 338
    label "towar"
  ]
  node [
    id 339
    label "za&#322;adunek"
  ]
  node [
    id 340
    label "nasiennictwo"
  ]
  node [
    id 341
    label "agrotechnika"
  ]
  node [
    id 342
    label "agroekologia"
  ]
  node [
    id 343
    label "agrobiznes"
  ]
  node [
    id 344
    label "intensyfikacja"
  ]
  node [
    id 345
    label "uprawianie"
  ]
  node [
    id 346
    label "gleboznawstwo"
  ]
  node [
    id 347
    label "ogrodnictwo"
  ]
  node [
    id 348
    label "agronomia"
  ]
  node [
    id 349
    label "agrochemia"
  ]
  node [
    id 350
    label "farmerstwo"
  ]
  node [
    id 351
    label "zootechnika"
  ]
  node [
    id 352
    label "zgarniacz"
  ]
  node [
    id 353
    label "nauka"
  ]
  node [
    id 354
    label "hodowla"
  ]
  node [
    id 355
    label "sadownictwo"
  ]
  node [
    id 356
    label "&#322;&#261;karstwo"
  ]
  node [
    id 357
    label "&#322;owiectwo"
  ]
  node [
    id 358
    label "biuro"
  ]
  node [
    id 359
    label "siedziba"
  ]
  node [
    id 360
    label "zarz&#261;d"
  ]
  node [
    id 361
    label "petent"
  ]
  node [
    id 362
    label "dziekanat"
  ]
  node [
    id 363
    label "stoisko"
  ]
  node [
    id 364
    label "rynek_podstawowy"
  ]
  node [
    id 365
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 366
    label "konsument"
  ]
  node [
    id 367
    label "pojawienie_si&#281;"
  ]
  node [
    id 368
    label "obiekt_handlowy"
  ]
  node [
    id 369
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 370
    label "wytw&#243;rca"
  ]
  node [
    id 371
    label "rynek_wt&#243;rny"
  ]
  node [
    id 372
    label "wprowadzanie"
  ]
  node [
    id 373
    label "wprowadza&#263;"
  ]
  node [
    id 374
    label "kram"
  ]
  node [
    id 375
    label "plac"
  ]
  node [
    id 376
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 377
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 378
    label "emitowa&#263;"
  ]
  node [
    id 379
    label "wprowadzi&#263;"
  ]
  node [
    id 380
    label "emitowanie"
  ]
  node [
    id 381
    label "biznes"
  ]
  node [
    id 382
    label "segment_rynku"
  ]
  node [
    id 383
    label "wprowadzenie"
  ]
  node [
    id 384
    label "targowica"
  ]
  node [
    id 385
    label "establishment"
  ]
  node [
    id 386
    label "fabrication"
  ]
  node [
    id 387
    label "bycie"
  ]
  node [
    id 388
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 389
    label "pojawianie_si&#281;"
  ]
  node [
    id 390
    label "tworzenie"
  ]
  node [
    id 391
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 392
    label "manage"
  ]
  node [
    id 393
    label "control"
  ]
  node [
    id 394
    label "pracowa&#263;"
  ]
  node [
    id 395
    label "trzyma&#263;"
  ]
  node [
    id 396
    label "dysponowa&#263;"
  ]
  node [
    id 397
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 398
    label "cope"
  ]
  node [
    id 399
    label "dysponowanie"
  ]
  node [
    id 400
    label "dawanie"
  ]
  node [
    id 401
    label "trzymanie"
  ]
  node [
    id 402
    label "rozdysponowywanie"
  ]
  node [
    id 403
    label "zarz&#261;dzanie"
  ]
  node [
    id 404
    label "housework"
  ]
  node [
    id 405
    label "pozarz&#261;dzanie"
  ]
  node [
    id 406
    label "pracowanie"
  ]
  node [
    id 407
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 408
    label "obni&#380;enie"
  ]
  node [
    id 409
    label "wyhamowanie"
  ]
  node [
    id 410
    label "ch&#322;odny"
  ]
  node [
    id 411
    label "cooling"
  ]
  node [
    id 412
    label "obni&#380;a&#263;"
  ]
  node [
    id 413
    label "aplomb"
  ]
  node [
    id 414
    label "squelch"
  ]
  node [
    id 415
    label "hamowa&#263;"
  ]
  node [
    id 416
    label "obni&#380;y&#263;"
  ]
  node [
    id 417
    label "wyhamowa&#263;"
  ]
  node [
    id 418
    label "cool"
  ]
  node [
    id 419
    label "obni&#380;anie"
  ]
  node [
    id 420
    label "hamowanie"
  ]
  node [
    id 421
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 422
    label "refrigeration"
  ]
  node [
    id 423
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 424
    label "rurownia"
  ]
  node [
    id 425
    label "fryzernia"
  ]
  node [
    id 426
    label "wytrawialnia"
  ]
  node [
    id 427
    label "ucieralnia"
  ]
  node [
    id 428
    label "tkalnia"
  ]
  node [
    id 429
    label "farbiarnia"
  ]
  node [
    id 430
    label "szwalnia"
  ]
  node [
    id 431
    label "szlifiernia"
  ]
  node [
    id 432
    label "probiernia"
  ]
  node [
    id 433
    label "dziewiarnia"
  ]
  node [
    id 434
    label "celulozownia"
  ]
  node [
    id 435
    label "hala"
  ]
  node [
    id 436
    label "prz&#281;dzalnia"
  ]
  node [
    id 437
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 438
    label "obieralnia"
  ]
  node [
    id 439
    label "op&#322;ata"
  ]
  node [
    id 440
    label "danina"
  ]
  node [
    id 441
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 442
    label "trybut"
  ]
  node [
    id 443
    label "bilans_handlowy"
  ]
  node [
    id 444
    label "kwota"
  ]
  node [
    id 445
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 446
    label "&#347;wiadczenie"
  ]
  node [
    id 447
    label "sta&#263;_si&#281;"
  ]
  node [
    id 448
    label "urosn&#261;&#263;"
  ]
  node [
    id 449
    label "wzbi&#263;_si&#281;"
  ]
  node [
    id 450
    label "increase"
  ]
  node [
    id 451
    label "zrobi&#263;_si&#281;"
  ]
  node [
    id 452
    label "narosn&#261;&#263;"
  ]
  node [
    id 453
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 454
    label "rise"
  ]
  node [
    id 455
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 456
    label "wzm&#243;c_si&#281;"
  ]
  node [
    id 457
    label "accumulate"
  ]
  node [
    id 458
    label "zaistnie&#263;"
  ]
  node [
    id 459
    label "turn"
  ]
  node [
    id 460
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 461
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 462
    label "sprout"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
]
