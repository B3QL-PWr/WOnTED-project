graph [
  node [
    id 0
    label "premier"
    origin "text"
  ]
  node [
    id 1
    label "moja"
    origin "text"
  ]
  node [
    id 2
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 3
    label "forma"
    origin "text"
  ]
  node [
    id 4
    label "papierowy"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "op&#243;&#378;nia&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ale"
    origin "text"
  ]
  node [
    id 8
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 10
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wiki&#378;r&#243;d&#322;ach"
    origin "text"
  ]
  node [
    id 12
    label "liryk"
    origin "text"
  ]
  node [
    id 13
    label "polityk"
    origin "text"
  ]
  node [
    id 14
    label "mi&#322;a"
    origin "text"
  ]
  node [
    id 15
    label "lektura"
    origin "text"
  ]
  node [
    id 16
    label "rz&#261;d"
  ]
  node [
    id 17
    label "Bismarck"
  ]
  node [
    id 18
    label "zwierzchnik"
  ]
  node [
    id 19
    label "Sto&#322;ypin"
  ]
  node [
    id 20
    label "Miko&#322;ajczyk"
  ]
  node [
    id 21
    label "Chruszczow"
  ]
  node [
    id 22
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 23
    label "Jelcyn"
  ]
  node [
    id 24
    label "dostojnik"
  ]
  node [
    id 25
    label "pryncypa&#322;"
  ]
  node [
    id 26
    label "kierowa&#263;"
  ]
  node [
    id 27
    label "kierownictwo"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "w&#322;adza"
  ]
  node [
    id 30
    label "urz&#281;dnik"
  ]
  node [
    id 31
    label "notabl"
  ]
  node [
    id 32
    label "oficja&#322;"
  ]
  node [
    id 33
    label "przybli&#380;enie"
  ]
  node [
    id 34
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 35
    label "kategoria"
  ]
  node [
    id 36
    label "szpaler"
  ]
  node [
    id 37
    label "lon&#380;a"
  ]
  node [
    id 38
    label "uporz&#261;dkowanie"
  ]
  node [
    id 39
    label "egzekutywa"
  ]
  node [
    id 40
    label "jednostka_systematyczna"
  ]
  node [
    id 41
    label "instytucja"
  ]
  node [
    id 42
    label "Londyn"
  ]
  node [
    id 43
    label "gabinet_cieni"
  ]
  node [
    id 44
    label "gromada"
  ]
  node [
    id 45
    label "number"
  ]
  node [
    id 46
    label "Konsulat"
  ]
  node [
    id 47
    label "tract"
  ]
  node [
    id 48
    label "klasa"
  ]
  node [
    id 49
    label "egzemplarz"
  ]
  node [
    id 50
    label "rozdzia&#322;"
  ]
  node [
    id 51
    label "wk&#322;ad"
  ]
  node [
    id 52
    label "tytu&#322;"
  ]
  node [
    id 53
    label "zak&#322;adka"
  ]
  node [
    id 54
    label "nomina&#322;"
  ]
  node [
    id 55
    label "ok&#322;adka"
  ]
  node [
    id 56
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 57
    label "wydawnictwo"
  ]
  node [
    id 58
    label "ekslibris"
  ]
  node [
    id 59
    label "tekst"
  ]
  node [
    id 60
    label "przek&#322;adacz"
  ]
  node [
    id 61
    label "bibliofilstwo"
  ]
  node [
    id 62
    label "falc"
  ]
  node [
    id 63
    label "pagina"
  ]
  node [
    id 64
    label "zw&#243;j"
  ]
  node [
    id 65
    label "ekscerpcja"
  ]
  node [
    id 66
    label "j&#281;zykowo"
  ]
  node [
    id 67
    label "wypowied&#378;"
  ]
  node [
    id 68
    label "redakcja"
  ]
  node [
    id 69
    label "wytw&#243;r"
  ]
  node [
    id 70
    label "pomini&#281;cie"
  ]
  node [
    id 71
    label "dzie&#322;o"
  ]
  node [
    id 72
    label "preparacja"
  ]
  node [
    id 73
    label "odmianka"
  ]
  node [
    id 74
    label "opu&#347;ci&#263;"
  ]
  node [
    id 75
    label "koniektura"
  ]
  node [
    id 76
    label "pisa&#263;"
  ]
  node [
    id 77
    label "obelga"
  ]
  node [
    id 78
    label "czynnik_biotyczny"
  ]
  node [
    id 79
    label "wyewoluowanie"
  ]
  node [
    id 80
    label "reakcja"
  ]
  node [
    id 81
    label "individual"
  ]
  node [
    id 82
    label "przyswoi&#263;"
  ]
  node [
    id 83
    label "starzenie_si&#281;"
  ]
  node [
    id 84
    label "wyewoluowa&#263;"
  ]
  node [
    id 85
    label "okaz"
  ]
  node [
    id 86
    label "part"
  ]
  node [
    id 87
    label "ewoluowa&#263;"
  ]
  node [
    id 88
    label "przyswojenie"
  ]
  node [
    id 89
    label "ewoluowanie"
  ]
  node [
    id 90
    label "obiekt"
  ]
  node [
    id 91
    label "sztuka"
  ]
  node [
    id 92
    label "agent"
  ]
  node [
    id 93
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 94
    label "przyswaja&#263;"
  ]
  node [
    id 95
    label "nicpo&#324;"
  ]
  node [
    id 96
    label "przyswajanie"
  ]
  node [
    id 97
    label "debit"
  ]
  node [
    id 98
    label "redaktor"
  ]
  node [
    id 99
    label "druk"
  ]
  node [
    id 100
    label "publikacja"
  ]
  node [
    id 101
    label "szata_graficzna"
  ]
  node [
    id 102
    label "firma"
  ]
  node [
    id 103
    label "wydawa&#263;"
  ]
  node [
    id 104
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 105
    label "wyda&#263;"
  ]
  node [
    id 106
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 107
    label "poster"
  ]
  node [
    id 108
    label "nadtytu&#322;"
  ]
  node [
    id 109
    label "tytulatura"
  ]
  node [
    id 110
    label "elevation"
  ]
  node [
    id 111
    label "mianowaniec"
  ]
  node [
    id 112
    label "nazwa"
  ]
  node [
    id 113
    label "podtytu&#322;"
  ]
  node [
    id 114
    label "wydarzenie"
  ]
  node [
    id 115
    label "faza"
  ]
  node [
    id 116
    label "interruption"
  ]
  node [
    id 117
    label "podzia&#322;"
  ]
  node [
    id 118
    label "podrozdzia&#322;"
  ]
  node [
    id 119
    label "fragment"
  ]
  node [
    id 120
    label "pagination"
  ]
  node [
    id 121
    label "strona"
  ]
  node [
    id 122
    label "numer"
  ]
  node [
    id 123
    label "kartka"
  ]
  node [
    id 124
    label "kwota"
  ]
  node [
    id 125
    label "uczestnictwo"
  ]
  node [
    id 126
    label "element"
  ]
  node [
    id 127
    label "input"
  ]
  node [
    id 128
    label "czasopismo"
  ]
  node [
    id 129
    label "lokata"
  ]
  node [
    id 130
    label "zeszyt"
  ]
  node [
    id 131
    label "blok"
  ]
  node [
    id 132
    label "oprawa"
  ]
  node [
    id 133
    label "boarding"
  ]
  node [
    id 134
    label "oprawianie"
  ]
  node [
    id 135
    label "os&#322;ona"
  ]
  node [
    id 136
    label "oprawia&#263;"
  ]
  node [
    id 137
    label "blacha"
  ]
  node [
    id 138
    label "z&#322;&#261;czenie"
  ]
  node [
    id 139
    label "grzbiet"
  ]
  node [
    id 140
    label "kszta&#322;t"
  ]
  node [
    id 141
    label "wrench"
  ]
  node [
    id 142
    label "m&#243;zg"
  ]
  node [
    id 143
    label "kink"
  ]
  node [
    id 144
    label "plik"
  ]
  node [
    id 145
    label "manuskrypt"
  ]
  node [
    id 146
    label "rolka"
  ]
  node [
    id 147
    label "warto&#347;&#263;"
  ]
  node [
    id 148
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 149
    label "pieni&#261;dz"
  ]
  node [
    id 150
    label "par_value"
  ]
  node [
    id 151
    label "cena"
  ]
  node [
    id 152
    label "znaczek"
  ]
  node [
    id 153
    label "kolekcjonerstwo"
  ]
  node [
    id 154
    label "bibliomania"
  ]
  node [
    id 155
    label "t&#322;umacz"
  ]
  node [
    id 156
    label "urz&#261;dzenie"
  ]
  node [
    id 157
    label "bookmark"
  ]
  node [
    id 158
    label "fa&#322;da"
  ]
  node [
    id 159
    label "znacznik"
  ]
  node [
    id 160
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 161
    label "widok"
  ]
  node [
    id 162
    label "program"
  ]
  node [
    id 163
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 164
    label "oznaczenie"
  ]
  node [
    id 165
    label "temat"
  ]
  node [
    id 166
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 167
    label "poznanie"
  ]
  node [
    id 168
    label "leksem"
  ]
  node [
    id 169
    label "stan"
  ]
  node [
    id 170
    label "blaszka"
  ]
  node [
    id 171
    label "poj&#281;cie"
  ]
  node [
    id 172
    label "kantyzm"
  ]
  node [
    id 173
    label "zdolno&#347;&#263;"
  ]
  node [
    id 174
    label "cecha"
  ]
  node [
    id 175
    label "do&#322;ek"
  ]
  node [
    id 176
    label "zawarto&#347;&#263;"
  ]
  node [
    id 177
    label "gwiazda"
  ]
  node [
    id 178
    label "formality"
  ]
  node [
    id 179
    label "struktura"
  ]
  node [
    id 180
    label "wygl&#261;d"
  ]
  node [
    id 181
    label "mode"
  ]
  node [
    id 182
    label "morfem"
  ]
  node [
    id 183
    label "rdze&#324;"
  ]
  node [
    id 184
    label "posta&#263;"
  ]
  node [
    id 185
    label "kielich"
  ]
  node [
    id 186
    label "ornamentyka"
  ]
  node [
    id 187
    label "pasmo"
  ]
  node [
    id 188
    label "zwyczaj"
  ]
  node [
    id 189
    label "punkt_widzenia"
  ]
  node [
    id 190
    label "g&#322;owa"
  ]
  node [
    id 191
    label "naczynie"
  ]
  node [
    id 192
    label "p&#322;at"
  ]
  node [
    id 193
    label "maszyna_drukarska"
  ]
  node [
    id 194
    label "style"
  ]
  node [
    id 195
    label "linearno&#347;&#263;"
  ]
  node [
    id 196
    label "wyra&#380;enie"
  ]
  node [
    id 197
    label "formacja"
  ]
  node [
    id 198
    label "spirala"
  ]
  node [
    id 199
    label "dyspozycja"
  ]
  node [
    id 200
    label "odmiana"
  ]
  node [
    id 201
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 202
    label "wz&#243;r"
  ]
  node [
    id 203
    label "October"
  ]
  node [
    id 204
    label "creation"
  ]
  node [
    id 205
    label "p&#281;tla"
  ]
  node [
    id 206
    label "arystotelizm"
  ]
  node [
    id 207
    label "szablon"
  ]
  node [
    id 208
    label "miniatura"
  ]
  node [
    id 209
    label "acquaintance"
  ]
  node [
    id 210
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 211
    label "spotkanie"
  ]
  node [
    id 212
    label "nauczenie_si&#281;"
  ]
  node [
    id 213
    label "poczucie"
  ]
  node [
    id 214
    label "knowing"
  ]
  node [
    id 215
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 216
    label "zapoznanie_si&#281;"
  ]
  node [
    id 217
    label "wy&#347;wiadczenie"
  ]
  node [
    id 218
    label "inclusion"
  ]
  node [
    id 219
    label "zrozumienie"
  ]
  node [
    id 220
    label "zawarcie"
  ]
  node [
    id 221
    label "designation"
  ]
  node [
    id 222
    label "umo&#380;liwienie"
  ]
  node [
    id 223
    label "sensing"
  ]
  node [
    id 224
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 225
    label "gathering"
  ]
  node [
    id 226
    label "czynno&#347;&#263;"
  ]
  node [
    id 227
    label "zapoznanie"
  ]
  node [
    id 228
    label "znajomy"
  ]
  node [
    id 229
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 230
    label "zrobienie"
  ]
  node [
    id 231
    label "obrazowanie"
  ]
  node [
    id 232
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 233
    label "dorobek"
  ]
  node [
    id 234
    label "tre&#347;&#263;"
  ]
  node [
    id 235
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 236
    label "retrospektywa"
  ]
  node [
    id 237
    label "works"
  ]
  node [
    id 238
    label "tetralogia"
  ]
  node [
    id 239
    label "komunikat"
  ]
  node [
    id 240
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 241
    label "praca"
  ]
  node [
    id 242
    label "mutant"
  ]
  node [
    id 243
    label "rewizja"
  ]
  node [
    id 244
    label "gramatyka"
  ]
  node [
    id 245
    label "typ"
  ]
  node [
    id 246
    label "paradygmat"
  ]
  node [
    id 247
    label "change"
  ]
  node [
    id 248
    label "podgatunek"
  ]
  node [
    id 249
    label "ferment"
  ]
  node [
    id 250
    label "rasa"
  ]
  node [
    id 251
    label "zjawisko"
  ]
  node [
    id 252
    label "pos&#322;uchanie"
  ]
  node [
    id 253
    label "skumanie"
  ]
  node [
    id 254
    label "orientacja"
  ]
  node [
    id 255
    label "zorientowanie"
  ]
  node [
    id 256
    label "teoria"
  ]
  node [
    id 257
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 258
    label "clasp"
  ]
  node [
    id 259
    label "przem&#243;wienie"
  ]
  node [
    id 260
    label "idealizm"
  ]
  node [
    id 261
    label "szko&#322;a"
  ]
  node [
    id 262
    label "koncepcja"
  ]
  node [
    id 263
    label "imperatyw_kategoryczny"
  ]
  node [
    id 264
    label "signal"
  ]
  node [
    id 265
    label "przedmiot"
  ]
  node [
    id 266
    label "li&#347;&#263;"
  ]
  node [
    id 267
    label "tw&#243;r"
  ]
  node [
    id 268
    label "odznaczenie"
  ]
  node [
    id 269
    label "kapelusz"
  ]
  node [
    id 270
    label "Arktur"
  ]
  node [
    id 271
    label "Gwiazda_Polarna"
  ]
  node [
    id 272
    label "agregatka"
  ]
  node [
    id 273
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 274
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 275
    label "S&#322;o&#324;ce"
  ]
  node [
    id 276
    label "Nibiru"
  ]
  node [
    id 277
    label "konstelacja"
  ]
  node [
    id 278
    label "ornament"
  ]
  node [
    id 279
    label "delta_Scuti"
  ]
  node [
    id 280
    label "&#347;wiat&#322;o"
  ]
  node [
    id 281
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 282
    label "s&#322;awa"
  ]
  node [
    id 283
    label "promie&#324;"
  ]
  node [
    id 284
    label "star"
  ]
  node [
    id 285
    label "gwiazdosz"
  ]
  node [
    id 286
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 287
    label "asocjacja_gwiazd"
  ]
  node [
    id 288
    label "supergrupa"
  ]
  node [
    id 289
    label "sid&#322;a"
  ]
  node [
    id 290
    label "ko&#322;o"
  ]
  node [
    id 291
    label "p&#281;tlica"
  ]
  node [
    id 292
    label "hank"
  ]
  node [
    id 293
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 294
    label "akrobacja_lotnicza"
  ]
  node [
    id 295
    label "zawi&#261;zywanie"
  ]
  node [
    id 296
    label "zawi&#261;zanie"
  ]
  node [
    id 297
    label "arrest"
  ]
  node [
    id 298
    label "zawi&#261;za&#263;"
  ]
  node [
    id 299
    label "koniec"
  ]
  node [
    id 300
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 301
    label "roztruchan"
  ]
  node [
    id 302
    label "dzia&#322;ka"
  ]
  node [
    id 303
    label "kwiat"
  ]
  node [
    id 304
    label "puch_kielichowy"
  ]
  node [
    id 305
    label "Graal"
  ]
  node [
    id 306
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 307
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 308
    label "wiedza"
  ]
  node [
    id 309
    label "alkohol"
  ]
  node [
    id 310
    label "&#380;ycie"
  ]
  node [
    id 311
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 312
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 313
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 314
    label "dekiel"
  ]
  node [
    id 315
    label "ro&#347;lina"
  ]
  node [
    id 316
    label "&#347;ci&#281;cie"
  ]
  node [
    id 317
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 318
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 319
    label "&#347;ci&#281;gno"
  ]
  node [
    id 320
    label "noosfera"
  ]
  node [
    id 321
    label "byd&#322;o"
  ]
  node [
    id 322
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 323
    label "makrocefalia"
  ]
  node [
    id 324
    label "ucho"
  ]
  node [
    id 325
    label "g&#243;ra"
  ]
  node [
    id 326
    label "fryzura"
  ]
  node [
    id 327
    label "umys&#322;"
  ]
  node [
    id 328
    label "cia&#322;o"
  ]
  node [
    id 329
    label "cz&#322;onek"
  ]
  node [
    id 330
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 331
    label "czaszka"
  ]
  node [
    id 332
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 333
    label "whirl"
  ]
  node [
    id 334
    label "krzywa"
  ]
  node [
    id 335
    label "spiralny"
  ]
  node [
    id 336
    label "nagromadzenie"
  ]
  node [
    id 337
    label "wk&#322;adka"
  ]
  node [
    id 338
    label "spirograf"
  ]
  node [
    id 339
    label "spiral"
  ]
  node [
    id 340
    label "przebieg"
  ]
  node [
    id 341
    label "pas"
  ]
  node [
    id 342
    label "swath"
  ]
  node [
    id 343
    label "streak"
  ]
  node [
    id 344
    label "kana&#322;"
  ]
  node [
    id 345
    label "strip"
  ]
  node [
    id 346
    label "ulica"
  ]
  node [
    id 347
    label "postarzenie"
  ]
  node [
    id 348
    label "postarzanie"
  ]
  node [
    id 349
    label "brzydota"
  ]
  node [
    id 350
    label "portrecista"
  ]
  node [
    id 351
    label "postarza&#263;"
  ]
  node [
    id 352
    label "nadawanie"
  ]
  node [
    id 353
    label "postarzy&#263;"
  ]
  node [
    id 354
    label "prostota"
  ]
  node [
    id 355
    label "ubarwienie"
  ]
  node [
    id 356
    label "shape"
  ]
  node [
    id 357
    label "comeliness"
  ]
  node [
    id 358
    label "face"
  ]
  node [
    id 359
    label "charakter"
  ]
  node [
    id 360
    label "kopia"
  ]
  node [
    id 361
    label "utw&#243;r"
  ]
  node [
    id 362
    label "obraz"
  ]
  node [
    id 363
    label "ilustracja"
  ]
  node [
    id 364
    label "miniature"
  ]
  node [
    id 365
    label "kawa&#322;ek"
  ]
  node [
    id 366
    label "centrop&#322;at"
  ]
  node [
    id 367
    label "organ"
  ]
  node [
    id 368
    label "airfoil"
  ]
  node [
    id 369
    label "&#347;mig&#322;owiec"
  ]
  node [
    id 370
    label "samolot"
  ]
  node [
    id 371
    label "piece"
  ]
  node [
    id 372
    label "plaster"
  ]
  node [
    id 373
    label "organizacja"
  ]
  node [
    id 374
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 375
    label "sk&#322;ada&#263;"
  ]
  node [
    id 376
    label "odmawia&#263;"
  ]
  node [
    id 377
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 378
    label "wyra&#380;a&#263;"
  ]
  node [
    id 379
    label "thank"
  ]
  node [
    id 380
    label "etykieta"
  ]
  node [
    id 381
    label "areszt"
  ]
  node [
    id 382
    label "golf"
  ]
  node [
    id 383
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 384
    label "l&#261;d"
  ]
  node [
    id 385
    label "depressive_disorder"
  ]
  node [
    id 386
    label "bruzda"
  ]
  node [
    id 387
    label "obszar"
  ]
  node [
    id 388
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 389
    label "Pampa"
  ]
  node [
    id 390
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 391
    label "odlewnictwo"
  ]
  node [
    id 392
    label "za&#322;amanie"
  ]
  node [
    id 393
    label "tomizm"
  ]
  node [
    id 394
    label "akt"
  ]
  node [
    id 395
    label "kalokagatia"
  ]
  node [
    id 396
    label "potencja"
  ]
  node [
    id 397
    label "wordnet"
  ]
  node [
    id 398
    label "wypowiedzenie"
  ]
  node [
    id 399
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 400
    label "s&#322;ownictwo"
  ]
  node [
    id 401
    label "wykrzyknik"
  ]
  node [
    id 402
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 403
    label "pole_semantyczne"
  ]
  node [
    id 404
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 405
    label "pisanie_si&#281;"
  ]
  node [
    id 406
    label "nag&#322;os"
  ]
  node [
    id 407
    label "wyg&#322;os"
  ]
  node [
    id 408
    label "jednostka_leksykalna"
  ]
  node [
    id 409
    label "charakterystyka"
  ]
  node [
    id 410
    label "zaistnie&#263;"
  ]
  node [
    id 411
    label "Osjan"
  ]
  node [
    id 412
    label "kto&#347;"
  ]
  node [
    id 413
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 414
    label "osobowo&#347;&#263;"
  ]
  node [
    id 415
    label "trim"
  ]
  node [
    id 416
    label "poby&#263;"
  ]
  node [
    id 417
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 418
    label "Aspazja"
  ]
  node [
    id 419
    label "kompleksja"
  ]
  node [
    id 420
    label "wytrzyma&#263;"
  ]
  node [
    id 421
    label "budowa"
  ]
  node [
    id 422
    label "pozosta&#263;"
  ]
  node [
    id 423
    label "point"
  ]
  node [
    id 424
    label "przedstawienie"
  ]
  node [
    id 425
    label "go&#347;&#263;"
  ]
  node [
    id 426
    label "ilo&#347;&#263;"
  ]
  node [
    id 427
    label "wn&#281;trze"
  ]
  node [
    id 428
    label "informacja"
  ]
  node [
    id 429
    label "Ohio"
  ]
  node [
    id 430
    label "wci&#281;cie"
  ]
  node [
    id 431
    label "Nowy_York"
  ]
  node [
    id 432
    label "warstwa"
  ]
  node [
    id 433
    label "samopoczucie"
  ]
  node [
    id 434
    label "Illinois"
  ]
  node [
    id 435
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 436
    label "state"
  ]
  node [
    id 437
    label "Jukatan"
  ]
  node [
    id 438
    label "Kalifornia"
  ]
  node [
    id 439
    label "Wirginia"
  ]
  node [
    id 440
    label "wektor"
  ]
  node [
    id 441
    label "by&#263;"
  ]
  node [
    id 442
    label "Teksas"
  ]
  node [
    id 443
    label "Goa"
  ]
  node [
    id 444
    label "Waszyngton"
  ]
  node [
    id 445
    label "miejsce"
  ]
  node [
    id 446
    label "Massachusetts"
  ]
  node [
    id 447
    label "Alaska"
  ]
  node [
    id 448
    label "Arakan"
  ]
  node [
    id 449
    label "Hawaje"
  ]
  node [
    id 450
    label "Maryland"
  ]
  node [
    id 451
    label "punkt"
  ]
  node [
    id 452
    label "Michigan"
  ]
  node [
    id 453
    label "Arizona"
  ]
  node [
    id 454
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 455
    label "Georgia"
  ]
  node [
    id 456
    label "poziom"
  ]
  node [
    id 457
    label "Pensylwania"
  ]
  node [
    id 458
    label "Luizjana"
  ]
  node [
    id 459
    label "Nowy_Meksyk"
  ]
  node [
    id 460
    label "Alabama"
  ]
  node [
    id 461
    label "Kansas"
  ]
  node [
    id 462
    label "Oregon"
  ]
  node [
    id 463
    label "Floryda"
  ]
  node [
    id 464
    label "Oklahoma"
  ]
  node [
    id 465
    label "jednostka_administracyjna"
  ]
  node [
    id 466
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 467
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 468
    label "vessel"
  ]
  node [
    id 469
    label "sprz&#281;t"
  ]
  node [
    id 470
    label "statki"
  ]
  node [
    id 471
    label "rewaskularyzacja"
  ]
  node [
    id 472
    label "ceramika"
  ]
  node [
    id 473
    label "drewno"
  ]
  node [
    id 474
    label "przew&#243;d"
  ]
  node [
    id 475
    label "unaczyni&#263;"
  ]
  node [
    id 476
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 477
    label "receptacle"
  ]
  node [
    id 478
    label "co&#347;"
  ]
  node [
    id 479
    label "budynek"
  ]
  node [
    id 480
    label "thing"
  ]
  node [
    id 481
    label "rzecz"
  ]
  node [
    id 482
    label "posiada&#263;"
  ]
  node [
    id 483
    label "potencja&#322;"
  ]
  node [
    id 484
    label "zapomnienie"
  ]
  node [
    id 485
    label "zapomina&#263;"
  ]
  node [
    id 486
    label "zapominanie"
  ]
  node [
    id 487
    label "ability"
  ]
  node [
    id 488
    label "obliczeniowo"
  ]
  node [
    id 489
    label "zapomnie&#263;"
  ]
  node [
    id 490
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 491
    label "zachowanie"
  ]
  node [
    id 492
    label "kultura_duchowa"
  ]
  node [
    id 493
    label "kultura"
  ]
  node [
    id 494
    label "ceremony"
  ]
  node [
    id 495
    label "sformu&#322;owanie"
  ]
  node [
    id 496
    label "zdarzenie_si&#281;"
  ]
  node [
    id 497
    label "poinformowanie"
  ]
  node [
    id 498
    label "wording"
  ]
  node [
    id 499
    label "kompozycja"
  ]
  node [
    id 500
    label "znak_j&#281;zykowy"
  ]
  node [
    id 501
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 502
    label "ozdobnik"
  ]
  node [
    id 503
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 504
    label "grupa_imienna"
  ]
  node [
    id 505
    label "term"
  ]
  node [
    id 506
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 507
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 508
    label "ujawnienie"
  ]
  node [
    id 509
    label "affirmation"
  ]
  node [
    id 510
    label "zapisanie"
  ]
  node [
    id 511
    label "rzucenie"
  ]
  node [
    id 512
    label "zapis"
  ]
  node [
    id 513
    label "figure"
  ]
  node [
    id 514
    label "spos&#243;b"
  ]
  node [
    id 515
    label "mildew"
  ]
  node [
    id 516
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 517
    label "ideal"
  ]
  node [
    id 518
    label "rule"
  ]
  node [
    id 519
    label "ruch"
  ]
  node [
    id 520
    label "dekal"
  ]
  node [
    id 521
    label "motyw"
  ]
  node [
    id 522
    label "projekt"
  ]
  node [
    id 523
    label "m&#322;ot"
  ]
  node [
    id 524
    label "znak"
  ]
  node [
    id 525
    label "drzewo"
  ]
  node [
    id 526
    label "pr&#243;ba"
  ]
  node [
    id 527
    label "attribute"
  ]
  node [
    id 528
    label "marka"
  ]
  node [
    id 529
    label "mechanika"
  ]
  node [
    id 530
    label "o&#347;"
  ]
  node [
    id 531
    label "usenet"
  ]
  node [
    id 532
    label "rozprz&#261;c"
  ]
  node [
    id 533
    label "cybernetyk"
  ]
  node [
    id 534
    label "podsystem"
  ]
  node [
    id 535
    label "system"
  ]
  node [
    id 536
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 537
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 538
    label "sk&#322;ad"
  ]
  node [
    id 539
    label "systemat"
  ]
  node [
    id 540
    label "konstrukcja"
  ]
  node [
    id 541
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 542
    label "model"
  ]
  node [
    id 543
    label "jig"
  ]
  node [
    id 544
    label "drabina_analgetyczna"
  ]
  node [
    id 545
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 546
    label "C"
  ]
  node [
    id 547
    label "D"
  ]
  node [
    id 548
    label "exemplar"
  ]
  node [
    id 549
    label "sprawa"
  ]
  node [
    id 550
    label "wyraz_pochodny"
  ]
  node [
    id 551
    label "zboczenie"
  ]
  node [
    id 552
    label "om&#243;wienie"
  ]
  node [
    id 553
    label "omawia&#263;"
  ]
  node [
    id 554
    label "fraza"
  ]
  node [
    id 555
    label "entity"
  ]
  node [
    id 556
    label "forum"
  ]
  node [
    id 557
    label "topik"
  ]
  node [
    id 558
    label "tematyka"
  ]
  node [
    id 559
    label "w&#261;tek"
  ]
  node [
    id 560
    label "zbaczanie"
  ]
  node [
    id 561
    label "om&#243;wi&#263;"
  ]
  node [
    id 562
    label "omawianie"
  ]
  node [
    id 563
    label "melodia"
  ]
  node [
    id 564
    label "otoczka"
  ]
  node [
    id 565
    label "istota"
  ]
  node [
    id 566
    label "zbacza&#263;"
  ]
  node [
    id 567
    label "zboczy&#263;"
  ]
  node [
    id 568
    label "morpheme"
  ]
  node [
    id 569
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 570
    label "figura_stylistyczna"
  ]
  node [
    id 571
    label "decoration"
  ]
  node [
    id 572
    label "dekoracja"
  ]
  node [
    id 573
    label "reaktor_j&#261;drowy"
  ]
  node [
    id 574
    label "magnes"
  ]
  node [
    id 575
    label "spowalniacz"
  ]
  node [
    id 576
    label "transformator"
  ]
  node [
    id 577
    label "mi&#281;kisz"
  ]
  node [
    id 578
    label "marrow"
  ]
  node [
    id 579
    label "pocisk"
  ]
  node [
    id 580
    label "pr&#281;t_kontrolny"
  ]
  node [
    id 581
    label "procesor"
  ]
  node [
    id 582
    label "ch&#322;odziwo"
  ]
  node [
    id 583
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 584
    label "surowiak"
  ]
  node [
    id 585
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 586
    label "core"
  ]
  node [
    id 587
    label "plan"
  ]
  node [
    id 588
    label "kondycja"
  ]
  node [
    id 589
    label "polecenie"
  ]
  node [
    id 590
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 591
    label "capability"
  ]
  node [
    id 592
    label "prawo"
  ]
  node [
    id 593
    label "Bund"
  ]
  node [
    id 594
    label "Mazowsze"
  ]
  node [
    id 595
    label "PPR"
  ]
  node [
    id 596
    label "Jakobici"
  ]
  node [
    id 597
    label "zesp&#243;&#322;"
  ]
  node [
    id 598
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 599
    label "SLD"
  ]
  node [
    id 600
    label "zespolik"
  ]
  node [
    id 601
    label "Razem"
  ]
  node [
    id 602
    label "PiS"
  ]
  node [
    id 603
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 604
    label "partia"
  ]
  node [
    id 605
    label "Kuomintang"
  ]
  node [
    id 606
    label "ZSL"
  ]
  node [
    id 607
    label "jednostka"
  ]
  node [
    id 608
    label "proces"
  ]
  node [
    id 609
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 610
    label "rugby"
  ]
  node [
    id 611
    label "AWS"
  ]
  node [
    id 612
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 613
    label "PO"
  ]
  node [
    id 614
    label "si&#322;a"
  ]
  node [
    id 615
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 616
    label "Federali&#347;ci"
  ]
  node [
    id 617
    label "PSL"
  ]
  node [
    id 618
    label "wojsko"
  ]
  node [
    id 619
    label "Wigowie"
  ]
  node [
    id 620
    label "ZChN"
  ]
  node [
    id 621
    label "rocznik"
  ]
  node [
    id 622
    label "The_Beatles"
  ]
  node [
    id 623
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 624
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 625
    label "unit"
  ]
  node [
    id 626
    label "Depeche_Mode"
  ]
  node [
    id 627
    label "martwy"
  ]
  node [
    id 628
    label "nierealistyczny"
  ]
  node [
    id 629
    label "analogowy"
  ]
  node [
    id 630
    label "papierowo"
  ]
  node [
    id 631
    label "podobny"
  ]
  node [
    id 632
    label "w&#261;tpliwy"
  ]
  node [
    id 633
    label "specjalny"
  ]
  node [
    id 634
    label "przypominanie"
  ]
  node [
    id 635
    label "podobnie"
  ]
  node [
    id 636
    label "upodabnianie_si&#281;"
  ]
  node [
    id 637
    label "asymilowanie"
  ]
  node [
    id 638
    label "upodobnienie"
  ]
  node [
    id 639
    label "drugi"
  ]
  node [
    id 640
    label "taki"
  ]
  node [
    id 641
    label "charakterystyczny"
  ]
  node [
    id 642
    label "upodobnienie_si&#281;"
  ]
  node [
    id 643
    label "zasymilowanie"
  ]
  node [
    id 644
    label "intencjonalny"
  ]
  node [
    id 645
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 646
    label "niedorozw&#243;j"
  ]
  node [
    id 647
    label "szczeg&#243;lny"
  ]
  node [
    id 648
    label "specjalnie"
  ]
  node [
    id 649
    label "nieetatowy"
  ]
  node [
    id 650
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 651
    label "nienormalny"
  ]
  node [
    id 652
    label "umy&#347;lnie"
  ]
  node [
    id 653
    label "odpowiedni"
  ]
  node [
    id 654
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 655
    label "nierealny"
  ]
  node [
    id 656
    label "nierealistycznie"
  ]
  node [
    id 657
    label "tradycyjny"
  ]
  node [
    id 658
    label "analogowo"
  ]
  node [
    id 659
    label "martwo"
  ]
  node [
    id 660
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 661
    label "umarlak"
  ]
  node [
    id 662
    label "bezmy&#347;lny"
  ]
  node [
    id 663
    label "duch"
  ]
  node [
    id 664
    label "chowanie"
  ]
  node [
    id 665
    label "nieumar&#322;y"
  ]
  node [
    id 666
    label "umieranie"
  ]
  node [
    id 667
    label "nieaktualny"
  ]
  node [
    id 668
    label "wiszenie"
  ]
  node [
    id 669
    label "niesprawny"
  ]
  node [
    id 670
    label "umarcie"
  ]
  node [
    id 671
    label "zw&#322;oki"
  ]
  node [
    id 672
    label "obumarcie"
  ]
  node [
    id 673
    label "obumieranie"
  ]
  node [
    id 674
    label "trwa&#322;y"
  ]
  node [
    id 675
    label "w&#261;tpliwie"
  ]
  node [
    id 676
    label "pozorny"
  ]
  node [
    id 677
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 678
    label "delay"
  ]
  node [
    id 679
    label "sprawia&#263;"
  ]
  node [
    id 680
    label "kupywa&#263;"
  ]
  node [
    id 681
    label "bra&#263;"
  ]
  node [
    id 682
    label "bind"
  ]
  node [
    id 683
    label "get"
  ]
  node [
    id 684
    label "act"
  ]
  node [
    id 685
    label "powodowa&#263;"
  ]
  node [
    id 686
    label "przygotowywa&#263;"
  ]
  node [
    id 687
    label "piwo"
  ]
  node [
    id 688
    label "uwarzenie"
  ]
  node [
    id 689
    label "warzenie"
  ]
  node [
    id 690
    label "nap&#243;j"
  ]
  node [
    id 691
    label "bacik"
  ]
  node [
    id 692
    label "wyj&#347;cie"
  ]
  node [
    id 693
    label "uwarzy&#263;"
  ]
  node [
    id 694
    label "birofilia"
  ]
  node [
    id 695
    label "warzy&#263;"
  ]
  node [
    id 696
    label "nawarzy&#263;"
  ]
  node [
    id 697
    label "browarnia"
  ]
  node [
    id 698
    label "nawarzenie"
  ]
  node [
    id 699
    label "anta&#322;"
  ]
  node [
    id 700
    label "doba"
  ]
  node [
    id 701
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 702
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 703
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 704
    label "teraz"
  ]
  node [
    id 705
    label "czas"
  ]
  node [
    id 706
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 707
    label "jednocze&#347;nie"
  ]
  node [
    id 708
    label "tydzie&#324;"
  ]
  node [
    id 709
    label "noc"
  ]
  node [
    id 710
    label "dzie&#324;"
  ]
  node [
    id 711
    label "godzina"
  ]
  node [
    id 712
    label "long_time"
  ]
  node [
    id 713
    label "jednostka_geologiczna"
  ]
  node [
    id 714
    label "free"
  ]
  node [
    id 715
    label "dysleksja"
  ]
  node [
    id 716
    label "poznawa&#263;"
  ]
  node [
    id 717
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 718
    label "odczytywa&#263;"
  ]
  node [
    id 719
    label "umie&#263;"
  ]
  node [
    id 720
    label "obserwowa&#263;"
  ]
  node [
    id 721
    label "read"
  ]
  node [
    id 722
    label "przetwarza&#263;"
  ]
  node [
    id 723
    label "dostrzega&#263;"
  ]
  node [
    id 724
    label "patrze&#263;"
  ]
  node [
    id 725
    label "look"
  ]
  node [
    id 726
    label "sprawdza&#263;"
  ]
  node [
    id 727
    label "podawa&#263;"
  ]
  node [
    id 728
    label "interpretowa&#263;"
  ]
  node [
    id 729
    label "convert"
  ]
  node [
    id 730
    label "wyzyskiwa&#263;"
  ]
  node [
    id 731
    label "opracowywa&#263;"
  ]
  node [
    id 732
    label "analizowa&#263;"
  ]
  node [
    id 733
    label "tworzy&#263;"
  ]
  node [
    id 734
    label "przewidywa&#263;"
  ]
  node [
    id 735
    label "wnioskowa&#263;"
  ]
  node [
    id 736
    label "harbinger"
  ]
  node [
    id 737
    label "bode"
  ]
  node [
    id 738
    label "bespeak"
  ]
  node [
    id 739
    label "przepowiada&#263;"
  ]
  node [
    id 740
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 741
    label "zawiera&#263;"
  ]
  node [
    id 742
    label "cognizance"
  ]
  node [
    id 743
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 744
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 745
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 746
    label "go_steady"
  ]
  node [
    id 747
    label "detect"
  ]
  node [
    id 748
    label "make"
  ]
  node [
    id 749
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 750
    label "hurt"
  ]
  node [
    id 751
    label "styka&#263;_si&#281;"
  ]
  node [
    id 752
    label "wiedzie&#263;"
  ]
  node [
    id 753
    label "can"
  ]
  node [
    id 754
    label "m&#243;c"
  ]
  node [
    id 755
    label "dysfunkcja"
  ]
  node [
    id 756
    label "dyslexia"
  ]
  node [
    id 757
    label "czytanie"
  ]
  node [
    id 758
    label "pisarz"
  ]
  node [
    id 759
    label "Homer"
  ]
  node [
    id 760
    label "Puszkin"
  ]
  node [
    id 761
    label "Horacy"
  ]
  node [
    id 762
    label "Le&#347;mian"
  ]
  node [
    id 763
    label "Rej"
  ]
  node [
    id 764
    label "Schiller"
  ]
  node [
    id 765
    label "Norwid"
  ]
  node [
    id 766
    label "R&#243;&#380;ewicz"
  ]
  node [
    id 767
    label "Herbert"
  ]
  node [
    id 768
    label "Tuwim"
  ]
  node [
    id 769
    label "Zagajewski"
  ]
  node [
    id 770
    label "Bara&#324;czak"
  ]
  node [
    id 771
    label "Pindar"
  ]
  node [
    id 772
    label "Jesienin"
  ]
  node [
    id 773
    label "Jan_Czeczot"
  ]
  node [
    id 774
    label "Byron"
  ]
  node [
    id 775
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 776
    label "Asnyk"
  ]
  node [
    id 777
    label "Bia&#322;oszewski"
  ]
  node [
    id 778
    label "wierszopis"
  ]
  node [
    id 779
    label "Mi&#322;osz"
  ]
  node [
    id 780
    label "Dawid"
  ]
  node [
    id 781
    label "Dante"
  ]
  node [
    id 782
    label "Peiper"
  ]
  node [
    id 783
    label "lyric"
  ]
  node [
    id 784
    label "surogator"
  ]
  node [
    id 785
    label "Machiavelli"
  ]
  node [
    id 786
    label "Sienkiewicz"
  ]
  node [
    id 787
    label "Andersen"
  ]
  node [
    id 788
    label "Katon"
  ]
  node [
    id 789
    label "Gogol"
  ]
  node [
    id 790
    label "pisarczyk"
  ]
  node [
    id 791
    label "Iwaszkiewicz"
  ]
  node [
    id 792
    label "Gombrowicz"
  ]
  node [
    id 793
    label "Proust"
  ]
  node [
    id 794
    label "Boy-&#379;ele&#324;ski"
  ]
  node [
    id 795
    label "Thomas_Mann"
  ]
  node [
    id 796
    label "Balzak"
  ]
  node [
    id 797
    label "Reymont"
  ]
  node [
    id 798
    label "Walter_Scott"
  ]
  node [
    id 799
    label "Stendhal"
  ]
  node [
    id 800
    label "Juliusz_Cezar"
  ]
  node [
    id 801
    label "Flaubert"
  ]
  node [
    id 802
    label "Tolkien"
  ]
  node [
    id 803
    label "Brecht"
  ]
  node [
    id 804
    label "To&#322;stoj"
  ]
  node [
    id 805
    label "Lem"
  ]
  node [
    id 806
    label "Orwell"
  ]
  node [
    id 807
    label "Kafka"
  ]
  node [
    id 808
    label "Conrad"
  ]
  node [
    id 809
    label "Voltaire"
  ]
  node [
    id 810
    label "artysta"
  ]
  node [
    id 811
    label "Bergson"
  ]
  node [
    id 812
    label "grafoman"
  ]
  node [
    id 813
    label "poeta"
  ]
  node [
    id 814
    label "poecina"
  ]
  node [
    id 815
    label "ima&#380;ynizm"
  ]
  node [
    id 816
    label "poezja"
  ]
  node [
    id 817
    label "epos"
  ]
  node [
    id 818
    label "zwrotnicowy"
  ]
  node [
    id 819
    label "Gorbaczow"
  ]
  node [
    id 820
    label "Korwin"
  ]
  node [
    id 821
    label "McCarthy"
  ]
  node [
    id 822
    label "Goebbels"
  ]
  node [
    id 823
    label "Ziobro"
  ]
  node [
    id 824
    label "dzia&#322;acz"
  ]
  node [
    id 825
    label "Moczar"
  ]
  node [
    id 826
    label "Gierek"
  ]
  node [
    id 827
    label "Arafat"
  ]
  node [
    id 828
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 829
    label "Naser"
  ]
  node [
    id 830
    label "Bre&#380;niew"
  ]
  node [
    id 831
    label "Mao"
  ]
  node [
    id 832
    label "Nixon"
  ]
  node [
    id 833
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 834
    label "Perykles"
  ]
  node [
    id 835
    label "Metternich"
  ]
  node [
    id 836
    label "Kuro&#324;"
  ]
  node [
    id 837
    label "Borel"
  ]
  node [
    id 838
    label "Bierut"
  ]
  node [
    id 839
    label "bezpartyjny"
  ]
  node [
    id 840
    label "Leszek_Miller"
  ]
  node [
    id 841
    label "Falandysz"
  ]
  node [
    id 842
    label "Fidel_Castro"
  ]
  node [
    id 843
    label "Winston_Churchill"
  ]
  node [
    id 844
    label "Putin"
  ]
  node [
    id 845
    label "J&#281;drzejewicz"
  ]
  node [
    id 846
    label "de_Gaulle"
  ]
  node [
    id 847
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 848
    label "Gomu&#322;ka"
  ]
  node [
    id 849
    label "Michnik"
  ]
  node [
    id 850
    label "Owsiak"
  ]
  node [
    id 851
    label "komuna"
  ]
  node [
    id 852
    label "Polska_Rzeczpospolita_Ludowa"
  ]
  node [
    id 853
    label "Plan_Ko&#322;&#322;&#261;tajowski"
  ]
  node [
    id 854
    label "o&#347;wiecenie"
  ]
  node [
    id 855
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 856
    label "bezpartyjnie"
  ]
  node [
    id 857
    label "niezaanga&#380;owany"
  ]
  node [
    id 858
    label "niezale&#380;ny"
  ]
  node [
    id 859
    label "wybranka"
  ]
  node [
    id 860
    label "umi&#322;owana"
  ]
  node [
    id 861
    label "kochanie"
  ]
  node [
    id 862
    label "ptaszyna"
  ]
  node [
    id 863
    label "Dulcynea"
  ]
  node [
    id 864
    label "kochanka"
  ]
  node [
    id 865
    label "mi&#322;owanie"
  ]
  node [
    id 866
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 867
    label "love"
  ]
  node [
    id 868
    label "zwrot"
  ]
  node [
    id 869
    label "czucie"
  ]
  node [
    id 870
    label "patrzenie_"
  ]
  node [
    id 871
    label "mi&#322;o&#347;nica"
  ]
  node [
    id 872
    label "partnerka"
  ]
  node [
    id 873
    label "dupa"
  ]
  node [
    id 874
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 875
    label "kochanica"
  ]
  node [
    id 876
    label "Don_Kiszot"
  ]
  node [
    id 877
    label "jedyna"
  ]
  node [
    id 878
    label "tick"
  ]
  node [
    id 879
    label "ptasz&#281;"
  ]
  node [
    id 880
    label "recitation"
  ]
  node [
    id 881
    label "wczytywanie_si&#281;"
  ]
  node [
    id 882
    label "poczytanie"
  ]
  node [
    id 883
    label "doczytywanie"
  ]
  node [
    id 884
    label "zaczytanie_si&#281;"
  ]
  node [
    id 885
    label "czytywanie"
  ]
  node [
    id 886
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 887
    label "poznawanie"
  ]
  node [
    id 888
    label "wyczytywanie"
  ]
  node [
    id 889
    label "oczytywanie_si&#281;"
  ]
  node [
    id 890
    label "uczenie_si&#281;"
  ]
  node [
    id 891
    label "cognition"
  ]
  node [
    id 892
    label "umo&#380;liwianie"
  ]
  node [
    id 893
    label "rozr&#243;&#380;nianie"
  ]
  node [
    id 894
    label "zapoznawanie"
  ]
  node [
    id 895
    label "robienie"
  ]
  node [
    id 896
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 897
    label "recognition"
  ]
  node [
    id 898
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 899
    label "spotykanie"
  ]
  node [
    id 900
    label "merging"
  ]
  node [
    id 901
    label "przep&#322;ywanie"
  ]
  node [
    id 902
    label "zawieranie"
  ]
  node [
    id 903
    label "podawanie"
  ]
  node [
    id 904
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 905
    label "uznanie"
  ]
  node [
    id 906
    label "ko&#324;czenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 59
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 49
  ]
  edge [
    source 15
    target 50
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 52
  ]
  edge [
    source 15
    target 53
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 15
    target 55
  ]
  edge [
    source 15
    target 56
  ]
  edge [
    source 15
    target 57
  ]
  edge [
    source 15
    target 58
  ]
  edge [
    source 15
    target 60
  ]
  edge [
    source 15
    target 61
  ]
  edge [
    source 15
    target 62
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 251
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 67
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 69
  ]
  edge [
    source 15
    target 70
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 72
  ]
  edge [
    source 15
    target 73
  ]
  edge [
    source 15
    target 74
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 906
  ]
]
