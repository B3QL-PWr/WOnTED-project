graph [
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "temu"
    origin "text"
  ]
  node [
    id 2
    label "podczas"
    origin "text"
  ]
  node [
    id 3
    label "wykopaliska"
    origin "text"
  ]
  node [
    id 4
    label "okolica"
    origin "text"
  ]
  node [
    id 5
    label "betlejem"
    origin "text"
  ]
  node [
    id 6
    label "odkry&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wykona&#263;"
    origin "text"
  ]
  node [
    id 8
    label "br&#261;z"
    origin "text"
  ]
  node [
    id 9
    label "pier&#347;cie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "pora_roku"
  ]
  node [
    id 11
    label "metoda"
  ]
  node [
    id 12
    label "archeologia"
  ]
  node [
    id 13
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 14
    label "method"
  ]
  node [
    id 15
    label "spos&#243;b"
  ]
  node [
    id 16
    label "doktryna"
  ]
  node [
    id 17
    label "nauka_humanistyczna"
  ]
  node [
    id 18
    label "paleologia"
  ]
  node [
    id 19
    label "archeologia_&#347;r&#243;dziemnomorska"
  ]
  node [
    id 20
    label "zabytkoznawstwo"
  ]
  node [
    id 21
    label "archeologia_podwodna"
  ]
  node [
    id 22
    label "sampling"
  ]
  node [
    id 23
    label "astroarcheologia"
  ]
  node [
    id 24
    label "archeometria"
  ]
  node [
    id 25
    label "paleoetnografia"
  ]
  node [
    id 26
    label "paleopatologia"
  ]
  node [
    id 27
    label "dendrochronologia"
  ]
  node [
    id 28
    label "krajobraz"
  ]
  node [
    id 29
    label "organ"
  ]
  node [
    id 30
    label "obszar"
  ]
  node [
    id 31
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 32
    label "miejsce"
  ]
  node [
    id 33
    label "przyroda"
  ]
  node [
    id 34
    label "grupa"
  ]
  node [
    id 35
    label "po_s&#261;siedzku"
  ]
  node [
    id 36
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 37
    label "warunek_lokalowy"
  ]
  node [
    id 38
    label "plac"
  ]
  node [
    id 39
    label "location"
  ]
  node [
    id 40
    label "uwaga"
  ]
  node [
    id 41
    label "przestrze&#324;"
  ]
  node [
    id 42
    label "status"
  ]
  node [
    id 43
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 44
    label "chwila"
  ]
  node [
    id 45
    label "cia&#322;o"
  ]
  node [
    id 46
    label "cecha"
  ]
  node [
    id 47
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 48
    label "praca"
  ]
  node [
    id 49
    label "rz&#261;d"
  ]
  node [
    id 50
    label "odm&#322;adzanie"
  ]
  node [
    id 51
    label "liga"
  ]
  node [
    id 52
    label "jednostka_systematyczna"
  ]
  node [
    id 53
    label "asymilowanie"
  ]
  node [
    id 54
    label "gromada"
  ]
  node [
    id 55
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 56
    label "asymilowa&#263;"
  ]
  node [
    id 57
    label "egzemplarz"
  ]
  node [
    id 58
    label "Entuzjastki"
  ]
  node [
    id 59
    label "zbi&#243;r"
  ]
  node [
    id 60
    label "kompozycja"
  ]
  node [
    id 61
    label "Terranie"
  ]
  node [
    id 62
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 63
    label "category"
  ]
  node [
    id 64
    label "pakiet_klimatyczny"
  ]
  node [
    id 65
    label "oddzia&#322;"
  ]
  node [
    id 66
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 67
    label "cz&#261;steczka"
  ]
  node [
    id 68
    label "stage_set"
  ]
  node [
    id 69
    label "type"
  ]
  node [
    id 70
    label "specgrupa"
  ]
  node [
    id 71
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 72
    label "&#346;wietliki"
  ]
  node [
    id 73
    label "odm&#322;odzenie"
  ]
  node [
    id 74
    label "Eurogrupa"
  ]
  node [
    id 75
    label "odm&#322;adza&#263;"
  ]
  node [
    id 76
    label "formacja_geologiczna"
  ]
  node [
    id 77
    label "harcerze_starsi"
  ]
  node [
    id 78
    label "p&#243;&#322;noc"
  ]
  node [
    id 79
    label "Kosowo"
  ]
  node [
    id 80
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 81
    label "Zab&#322;ocie"
  ]
  node [
    id 82
    label "zach&#243;d"
  ]
  node [
    id 83
    label "po&#322;udnie"
  ]
  node [
    id 84
    label "Pow&#261;zki"
  ]
  node [
    id 85
    label "Piotrowo"
  ]
  node [
    id 86
    label "Olszanica"
  ]
  node [
    id 87
    label "holarktyka"
  ]
  node [
    id 88
    label "Ruda_Pabianicka"
  ]
  node [
    id 89
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 90
    label "Ludwin&#243;w"
  ]
  node [
    id 91
    label "Arktyka"
  ]
  node [
    id 92
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 93
    label "Zabu&#380;e"
  ]
  node [
    id 94
    label "antroposfera"
  ]
  node [
    id 95
    label "terytorium"
  ]
  node [
    id 96
    label "Neogea"
  ]
  node [
    id 97
    label "Syberia_Zachodnia"
  ]
  node [
    id 98
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 99
    label "zakres"
  ]
  node [
    id 100
    label "pas_planetoid"
  ]
  node [
    id 101
    label "Syberia_Wschodnia"
  ]
  node [
    id 102
    label "Antarktyka"
  ]
  node [
    id 103
    label "Rakowice"
  ]
  node [
    id 104
    label "akrecja"
  ]
  node [
    id 105
    label "wymiar"
  ]
  node [
    id 106
    label "&#321;&#281;g"
  ]
  node [
    id 107
    label "Kresy_Zachodnie"
  ]
  node [
    id 108
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 109
    label "wsch&#243;d"
  ]
  node [
    id 110
    label "Notogea"
  ]
  node [
    id 111
    label "tkanka"
  ]
  node [
    id 112
    label "jednostka_organizacyjna"
  ]
  node [
    id 113
    label "budowa"
  ]
  node [
    id 114
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 115
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 116
    label "tw&#243;r"
  ]
  node [
    id 117
    label "organogeneza"
  ]
  node [
    id 118
    label "zesp&#243;&#322;"
  ]
  node [
    id 119
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 120
    label "struktura_anatomiczna"
  ]
  node [
    id 121
    label "uk&#322;ad"
  ]
  node [
    id 122
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 123
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 124
    label "Izba_Konsyliarska"
  ]
  node [
    id 125
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 126
    label "stomia"
  ]
  node [
    id 127
    label "dekortykacja"
  ]
  node [
    id 128
    label "Komitet_Region&#243;w"
  ]
  node [
    id 129
    label "teren"
  ]
  node [
    id 130
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 131
    label "human_body"
  ]
  node [
    id 132
    label "dzie&#322;o"
  ]
  node [
    id 133
    label "obraz"
  ]
  node [
    id 134
    label "zjawisko"
  ]
  node [
    id 135
    label "widok"
  ]
  node [
    id 136
    label "zaj&#347;cie"
  ]
  node [
    id 137
    label "woda"
  ]
  node [
    id 138
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 139
    label "przedmiot"
  ]
  node [
    id 140
    label "mikrokosmos"
  ]
  node [
    id 141
    label "ekosystem"
  ]
  node [
    id 142
    label "rzecz"
  ]
  node [
    id 143
    label "stw&#243;r"
  ]
  node [
    id 144
    label "obiekt_naturalny"
  ]
  node [
    id 145
    label "environment"
  ]
  node [
    id 146
    label "Ziemia"
  ]
  node [
    id 147
    label "przyra"
  ]
  node [
    id 148
    label "wszechstworzenie"
  ]
  node [
    id 149
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 150
    label "fauna"
  ]
  node [
    id 151
    label "biota"
  ]
  node [
    id 152
    label "discover"
  ]
  node [
    id 153
    label "objawi&#263;"
  ]
  node [
    id 154
    label "ukaza&#263;"
  ]
  node [
    id 155
    label "&#322;owca_talent&#243;w"
  ]
  node [
    id 156
    label "pozna&#263;"
  ]
  node [
    id 157
    label "zsun&#261;&#263;"
  ]
  node [
    id 158
    label "unwrap"
  ]
  node [
    id 159
    label "poinformowa&#263;"
  ]
  node [
    id 160
    label "expose"
  ]
  node [
    id 161
    label "denounce"
  ]
  node [
    id 162
    label "podnie&#347;&#263;"
  ]
  node [
    id 163
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 164
    label "znale&#378;&#263;"
  ]
  node [
    id 165
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 166
    label "zrozumie&#263;"
  ]
  node [
    id 167
    label "feel"
  ]
  node [
    id 168
    label "topographic_point"
  ]
  node [
    id 169
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 170
    label "visualize"
  ]
  node [
    id 171
    label "przyswoi&#263;"
  ]
  node [
    id 172
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 173
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 174
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 175
    label "teach"
  ]
  node [
    id 176
    label "experience"
  ]
  node [
    id 177
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 178
    label "ascend"
  ]
  node [
    id 179
    label "allude"
  ]
  node [
    id 180
    label "raise"
  ]
  node [
    id 181
    label "pochwali&#263;"
  ]
  node [
    id 182
    label "os&#322;awi&#263;"
  ]
  node [
    id 183
    label "surface"
  ]
  node [
    id 184
    label "ulepszy&#263;"
  ]
  node [
    id 185
    label "policzy&#263;"
  ]
  node [
    id 186
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 187
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 188
    label "zmieni&#263;"
  ]
  node [
    id 189
    label "float"
  ]
  node [
    id 190
    label "odbudowa&#263;"
  ]
  node [
    id 191
    label "przybli&#380;y&#263;"
  ]
  node [
    id 192
    label "zacz&#261;&#263;"
  ]
  node [
    id 193
    label "za&#322;apa&#263;"
  ]
  node [
    id 194
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 195
    label "sorb"
  ]
  node [
    id 196
    label "better"
  ]
  node [
    id 197
    label "laud"
  ]
  node [
    id 198
    label "heft"
  ]
  node [
    id 199
    label "resume"
  ]
  node [
    id 200
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 201
    label "pom&#243;c"
  ]
  node [
    id 202
    label "z&#322;&#261;czy&#263;"
  ]
  node [
    id 203
    label "zdj&#261;&#263;"
  ]
  node [
    id 204
    label "opu&#347;ci&#263;"
  ]
  node [
    id 205
    label "inform"
  ]
  node [
    id 206
    label "zakomunikowa&#263;"
  ]
  node [
    id 207
    label "pozyska&#263;"
  ]
  node [
    id 208
    label "oceni&#263;"
  ]
  node [
    id 209
    label "devise"
  ]
  node [
    id 210
    label "dozna&#263;"
  ]
  node [
    id 211
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 212
    label "wykry&#263;"
  ]
  node [
    id 213
    label "odzyska&#263;"
  ]
  node [
    id 214
    label "znaj&#347;&#263;"
  ]
  node [
    id 215
    label "invent"
  ]
  node [
    id 216
    label "wymy&#347;li&#263;"
  ]
  node [
    id 217
    label "pokaza&#263;"
  ]
  node [
    id 218
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 219
    label "ujawni&#263;"
  ]
  node [
    id 220
    label "testify"
  ]
  node [
    id 221
    label "wytworzy&#263;"
  ]
  node [
    id 222
    label "picture"
  ]
  node [
    id 223
    label "manufacture"
  ]
  node [
    id 224
    label "zrobi&#263;"
  ]
  node [
    id 225
    label "cause"
  ]
  node [
    id 226
    label "post&#261;pi&#263;"
  ]
  node [
    id 227
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 228
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 229
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 230
    label "zorganizowa&#263;"
  ]
  node [
    id 231
    label "appoint"
  ]
  node [
    id 232
    label "wystylizowa&#263;"
  ]
  node [
    id 233
    label "przerobi&#263;"
  ]
  node [
    id 234
    label "nabra&#263;"
  ]
  node [
    id 235
    label "make"
  ]
  node [
    id 236
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 237
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 238
    label "wydali&#263;"
  ]
  node [
    id 239
    label "kolor"
  ]
  node [
    id 240
    label "stop"
  ]
  node [
    id 241
    label "medal"
  ]
  node [
    id 242
    label "metal_kolorowy"
  ]
  node [
    id 243
    label "tangent"
  ]
  node [
    id 244
    label "liczba_kwantowa"
  ]
  node [
    id 245
    label "&#347;wieci&#263;"
  ]
  node [
    id 246
    label "poker"
  ]
  node [
    id 247
    label "ubarwienie"
  ]
  node [
    id 248
    label "blakn&#261;&#263;"
  ]
  node [
    id 249
    label "struktura"
  ]
  node [
    id 250
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 251
    label "zblakni&#281;cie"
  ]
  node [
    id 252
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 253
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 254
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 255
    label "prze&#322;amanie"
  ]
  node [
    id 256
    label "prze&#322;amywanie"
  ]
  node [
    id 257
    label "&#347;wiecenie"
  ]
  node [
    id 258
    label "prze&#322;ama&#263;"
  ]
  node [
    id 259
    label "zblakn&#261;&#263;"
  ]
  node [
    id 260
    label "symbol"
  ]
  node [
    id 261
    label "blakni&#281;cie"
  ]
  node [
    id 262
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 263
    label "przesyca&#263;"
  ]
  node [
    id 264
    label "przesycenie"
  ]
  node [
    id 265
    label "przesycanie"
  ]
  node [
    id 266
    label "struktura_metalu"
  ]
  node [
    id 267
    label "mieszanina"
  ]
  node [
    id 268
    label "znak_nakazu"
  ]
  node [
    id 269
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 270
    label "reflektor"
  ]
  node [
    id 271
    label "alia&#380;"
  ]
  node [
    id 272
    label "przesyci&#263;"
  ]
  node [
    id 273
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 274
    label "numizmatyka"
  ]
  node [
    id 275
    label "awers"
  ]
  node [
    id 276
    label "legenda"
  ]
  node [
    id 277
    label "rewers"
  ]
  node [
    id 278
    label "numizmat"
  ]
  node [
    id 279
    label "decoration"
  ]
  node [
    id 280
    label "odznaka"
  ]
  node [
    id 281
    label "skleryt"
  ]
  node [
    id 282
    label "Pier&#347;cie&#324;_W&#322;adzy"
  ]
  node [
    id 283
    label "section"
  ]
  node [
    id 284
    label "pier&#347;cienica"
  ]
  node [
    id 285
    label "bi&#380;uteria"
  ]
  node [
    id 286
    label "ring"
  ]
  node [
    id 287
    label "band"
  ]
  node [
    id 288
    label "annulus"
  ]
  node [
    id 289
    label "zwi&#261;zek_cykliczny"
  ]
  node [
    id 290
    label "okr&#261;g"
  ]
  node [
    id 291
    label "ozdoba"
  ]
  node [
    id 292
    label "Rzym_Zachodni"
  ]
  node [
    id 293
    label "whole"
  ]
  node [
    id 294
    label "ilo&#347;&#263;"
  ]
  node [
    id 295
    label "element"
  ]
  node [
    id 296
    label "Rzym_Wschodni"
  ]
  node [
    id 297
    label "urz&#261;dzenie"
  ]
  node [
    id 298
    label "series"
  ]
  node [
    id 299
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 300
    label "uprawianie"
  ]
  node [
    id 301
    label "praca_rolnicza"
  ]
  node [
    id 302
    label "collection"
  ]
  node [
    id 303
    label "dane"
  ]
  node [
    id 304
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 305
    label "poj&#281;cie"
  ]
  node [
    id 306
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 307
    label "sum"
  ]
  node [
    id 308
    label "gathering"
  ]
  node [
    id 309
    label "album"
  ]
  node [
    id 310
    label "mechanika"
  ]
  node [
    id 311
    label "o&#347;"
  ]
  node [
    id 312
    label "usenet"
  ]
  node [
    id 313
    label "rozprz&#261;c"
  ]
  node [
    id 314
    label "zachowanie"
  ]
  node [
    id 315
    label "cybernetyk"
  ]
  node [
    id 316
    label "podsystem"
  ]
  node [
    id 317
    label "system"
  ]
  node [
    id 318
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 319
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 320
    label "sk&#322;ad"
  ]
  node [
    id 321
    label "systemat"
  ]
  node [
    id 322
    label "konstrukcja"
  ]
  node [
    id 323
    label "konstelacja"
  ]
  node [
    id 324
    label "dekor"
  ]
  node [
    id 325
    label "chluba"
  ]
  node [
    id 326
    label "dekoracja"
  ]
  node [
    id 327
    label "figura_p&#322;aska"
  ]
  node [
    id 328
    label "ko&#322;o"
  ]
  node [
    id 329
    label "figura_geometryczna"
  ]
  node [
    id 330
    label "circumference"
  ]
  node [
    id 331
    label "&#322;uk"
  ]
  node [
    id 332
    label "circle"
  ]
  node [
    id 333
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 334
    label "budowla"
  ]
  node [
    id 335
    label "pr&#281;t"
  ]
  node [
    id 336
    label "obiekt"
  ]
  node [
    id 337
    label "punkt_asekuracyjny"
  ]
  node [
    id 338
    label "metamer"
  ]
  node [
    id 339
    label "pier&#347;cienice"
  ]
  node [
    id 340
    label "bezkr&#281;gowiec"
  ]
  node [
    id 341
    label "macka"
  ]
  node [
    id 342
    label "g&#243;rka"
  ]
  node [
    id 343
    label "kosztowno&#347;ci"
  ]
  node [
    id 344
    label "sutasz"
  ]
  node [
    id 345
    label "Poncjusz"
  ]
  node [
    id 346
    label "pi&#322;at"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 345
    target 346
  ]
]
