graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 2
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rozedrze&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "dan"
    origin "text"
  ]
  node [
    id 6
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nikt"
    origin "text"
  ]
  node [
    id 8
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "ani"
    origin "text"
  ]
  node [
    id 13
    label "przodkini"
  ]
  node [
    id 14
    label "matka_zast&#281;pcza"
  ]
  node [
    id 15
    label "matczysko"
  ]
  node [
    id 16
    label "rodzice"
  ]
  node [
    id 17
    label "stara"
  ]
  node [
    id 18
    label "macierz"
  ]
  node [
    id 19
    label "rodzic"
  ]
  node [
    id 20
    label "Matka_Boska"
  ]
  node [
    id 21
    label "macocha"
  ]
  node [
    id 22
    label "starzy"
  ]
  node [
    id 23
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 24
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 25
    label "pokolenie"
  ]
  node [
    id 26
    label "wapniaki"
  ]
  node [
    id 27
    label "opiekun"
  ]
  node [
    id 28
    label "wapniak"
  ]
  node [
    id 29
    label "rodzic_chrzestny"
  ]
  node [
    id 30
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 31
    label "krewna"
  ]
  node [
    id 32
    label "matka"
  ]
  node [
    id 33
    label "&#380;ona"
  ]
  node [
    id 34
    label "kobieta"
  ]
  node [
    id 35
    label "partnerka"
  ]
  node [
    id 36
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 37
    label "matuszka"
  ]
  node [
    id 38
    label "parametryzacja"
  ]
  node [
    id 39
    label "pa&#324;stwo"
  ]
  node [
    id 40
    label "poj&#281;cie"
  ]
  node [
    id 41
    label "mod"
  ]
  node [
    id 42
    label "patriota"
  ]
  node [
    id 43
    label "m&#281;&#380;atka"
  ]
  node [
    id 44
    label "lacerate"
  ]
  node [
    id 45
    label "tear"
  ]
  node [
    id 46
    label "przeszy&#263;"
  ]
  node [
    id 47
    label "zm&#261;ci&#263;"
  ]
  node [
    id 48
    label "podrze&#263;"
  ]
  node [
    id 49
    label "rozerwa&#263;"
  ]
  node [
    id 50
    label "zedrze&#263;"
  ]
  node [
    id 51
    label "znosi&#263;"
  ]
  node [
    id 52
    label "overcharge"
  ]
  node [
    id 53
    label "sprawi&#263;"
  ]
  node [
    id 54
    label "popsu&#263;"
  ]
  node [
    id 55
    label "pomiesza&#263;"
  ]
  node [
    id 56
    label "wzburzy&#263;"
  ]
  node [
    id 57
    label "clutter"
  ]
  node [
    id 58
    label "naruszy&#263;"
  ]
  node [
    id 59
    label "zak&#322;ama&#263;"
  ]
  node [
    id 60
    label "interrupt"
  ]
  node [
    id 61
    label "namiesza&#263;"
  ]
  node [
    id 62
    label "zszy&#263;"
  ]
  node [
    id 63
    label "stick"
  ]
  node [
    id 64
    label "przej&#347;&#263;"
  ]
  node [
    id 65
    label "przerobi&#263;"
  ]
  node [
    id 66
    label "spike"
  ]
  node [
    id 67
    label "przelecie&#263;"
  ]
  node [
    id 68
    label "przebi&#263;"
  ]
  node [
    id 69
    label "fascinate"
  ]
  node [
    id 70
    label "przenikn&#261;&#263;"
  ]
  node [
    id 71
    label "doj&#261;&#263;"
  ]
  node [
    id 72
    label "paleocen"
  ]
  node [
    id 73
    label "wiek"
  ]
  node [
    id 74
    label "period"
  ]
  node [
    id 75
    label "choroba_wieku"
  ]
  node [
    id 76
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 77
    label "chron"
  ]
  node [
    id 78
    label "czas"
  ]
  node [
    id 79
    label "cecha"
  ]
  node [
    id 80
    label "rok"
  ]
  node [
    id 81
    label "long_time"
  ]
  node [
    id 82
    label "jednostka_geologiczna"
  ]
  node [
    id 83
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 84
    label "paleogen"
  ]
  node [
    id 85
    label "tanet"
  ]
  node [
    id 86
    label "formacja_geologiczna"
  ]
  node [
    id 87
    label "zeland"
  ]
  node [
    id 88
    label "wiedzie&#263;"
  ]
  node [
    id 89
    label "kuma&#263;"
  ]
  node [
    id 90
    label "czu&#263;"
  ]
  node [
    id 91
    label "give"
  ]
  node [
    id 92
    label "dziama&#263;"
  ]
  node [
    id 93
    label "match"
  ]
  node [
    id 94
    label "empatia"
  ]
  node [
    id 95
    label "j&#281;zyk"
  ]
  node [
    id 96
    label "odbiera&#263;"
  ]
  node [
    id 97
    label "see"
  ]
  node [
    id 98
    label "zna&#263;"
  ]
  node [
    id 99
    label "cognizance"
  ]
  node [
    id 100
    label "postrzega&#263;"
  ]
  node [
    id 101
    label "przewidywa&#263;"
  ]
  node [
    id 102
    label "smell"
  ]
  node [
    id 103
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 104
    label "uczuwa&#263;"
  ]
  node [
    id 105
    label "spirit"
  ]
  node [
    id 106
    label "doznawa&#263;"
  ]
  node [
    id 107
    label "anticipate"
  ]
  node [
    id 108
    label "zabiera&#263;"
  ]
  node [
    id 109
    label "zlecenie"
  ]
  node [
    id 110
    label "odzyskiwa&#263;"
  ]
  node [
    id 111
    label "radio"
  ]
  node [
    id 112
    label "przyjmowa&#263;"
  ]
  node [
    id 113
    label "bra&#263;"
  ]
  node [
    id 114
    label "antena"
  ]
  node [
    id 115
    label "fall"
  ]
  node [
    id 116
    label "liszy&#263;"
  ]
  node [
    id 117
    label "pozbawia&#263;"
  ]
  node [
    id 118
    label "telewizor"
  ]
  node [
    id 119
    label "konfiskowa&#263;"
  ]
  node [
    id 120
    label "deprive"
  ]
  node [
    id 121
    label "accept"
  ]
  node [
    id 122
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 123
    label "artykulator"
  ]
  node [
    id 124
    label "kod"
  ]
  node [
    id 125
    label "kawa&#322;ek"
  ]
  node [
    id 126
    label "przedmiot"
  ]
  node [
    id 127
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 128
    label "gramatyka"
  ]
  node [
    id 129
    label "stylik"
  ]
  node [
    id 130
    label "przet&#322;umaczenie"
  ]
  node [
    id 131
    label "formalizowanie"
  ]
  node [
    id 132
    label "ssa&#263;"
  ]
  node [
    id 133
    label "ssanie"
  ]
  node [
    id 134
    label "language"
  ]
  node [
    id 135
    label "liza&#263;"
  ]
  node [
    id 136
    label "napisa&#263;"
  ]
  node [
    id 137
    label "konsonantyzm"
  ]
  node [
    id 138
    label "wokalizm"
  ]
  node [
    id 139
    label "pisa&#263;"
  ]
  node [
    id 140
    label "fonetyka"
  ]
  node [
    id 141
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 142
    label "jeniec"
  ]
  node [
    id 143
    label "but"
  ]
  node [
    id 144
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 145
    label "po_koroniarsku"
  ]
  node [
    id 146
    label "kultura_duchowa"
  ]
  node [
    id 147
    label "t&#322;umaczenie"
  ]
  node [
    id 148
    label "m&#243;wienie"
  ]
  node [
    id 149
    label "pype&#263;"
  ]
  node [
    id 150
    label "lizanie"
  ]
  node [
    id 151
    label "pismo"
  ]
  node [
    id 152
    label "formalizowa&#263;"
  ]
  node [
    id 153
    label "organ"
  ]
  node [
    id 154
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 155
    label "rozumienie"
  ]
  node [
    id 156
    label "spos&#243;b"
  ]
  node [
    id 157
    label "makroglosja"
  ]
  node [
    id 158
    label "jama_ustna"
  ]
  node [
    id 159
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 160
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 161
    label "natural_language"
  ]
  node [
    id 162
    label "s&#322;ownictwo"
  ]
  node [
    id 163
    label "urz&#261;dzenie"
  ]
  node [
    id 164
    label "zrozumienie"
  ]
  node [
    id 165
    label "empathy"
  ]
  node [
    id 166
    label "lito&#347;&#263;"
  ]
  node [
    id 167
    label "wczuwa&#263;_si&#281;"
  ]
  node [
    id 168
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 169
    label "szczeka&#263;"
  ]
  node [
    id 170
    label "rozmawia&#263;"
  ]
  node [
    id 171
    label "funkcjonowa&#263;"
  ]
  node [
    id 172
    label "miernota"
  ]
  node [
    id 173
    label "ciura"
  ]
  node [
    id 174
    label "jako&#347;&#263;"
  ]
  node [
    id 175
    label "cz&#322;owiek"
  ]
  node [
    id 176
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 177
    label "tandetno&#347;&#263;"
  ]
  node [
    id 178
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 179
    label "chor&#261;&#380;y"
  ]
  node [
    id 180
    label "zero"
  ]
  node [
    id 181
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 182
    label "du&#380;y"
  ]
  node [
    id 183
    label "mocno"
  ]
  node [
    id 184
    label "wiela"
  ]
  node [
    id 185
    label "bardzo"
  ]
  node [
    id 186
    label "cz&#281;sto"
  ]
  node [
    id 187
    label "wiele"
  ]
  node [
    id 188
    label "doros&#322;y"
  ]
  node [
    id 189
    label "znaczny"
  ]
  node [
    id 190
    label "niema&#322;o"
  ]
  node [
    id 191
    label "rozwini&#281;ty"
  ]
  node [
    id 192
    label "dorodny"
  ]
  node [
    id 193
    label "wa&#380;ny"
  ]
  node [
    id 194
    label "prawdziwy"
  ]
  node [
    id 195
    label "intensywny"
  ]
  node [
    id 196
    label "mocny"
  ]
  node [
    id 197
    label "silny"
  ]
  node [
    id 198
    label "przekonuj&#261;co"
  ]
  node [
    id 199
    label "powerfully"
  ]
  node [
    id 200
    label "widocznie"
  ]
  node [
    id 201
    label "szczerze"
  ]
  node [
    id 202
    label "konkretnie"
  ]
  node [
    id 203
    label "niepodwa&#380;alnie"
  ]
  node [
    id 204
    label "stabilnie"
  ]
  node [
    id 205
    label "silnie"
  ]
  node [
    id 206
    label "zdecydowanie"
  ]
  node [
    id 207
    label "strongly"
  ]
  node [
    id 208
    label "w_chuj"
  ]
  node [
    id 209
    label "cz&#281;sty"
  ]
  node [
    id 210
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 211
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 212
    label "mie&#263;_miejsce"
  ]
  node [
    id 213
    label "equal"
  ]
  node [
    id 214
    label "trwa&#263;"
  ]
  node [
    id 215
    label "chodzi&#263;"
  ]
  node [
    id 216
    label "si&#281;ga&#263;"
  ]
  node [
    id 217
    label "stan"
  ]
  node [
    id 218
    label "obecno&#347;&#263;"
  ]
  node [
    id 219
    label "stand"
  ]
  node [
    id 220
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 221
    label "uczestniczy&#263;"
  ]
  node [
    id 222
    label "participate"
  ]
  node [
    id 223
    label "istnie&#263;"
  ]
  node [
    id 224
    label "pozostawa&#263;"
  ]
  node [
    id 225
    label "zostawa&#263;"
  ]
  node [
    id 226
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 227
    label "adhere"
  ]
  node [
    id 228
    label "compass"
  ]
  node [
    id 229
    label "korzysta&#263;"
  ]
  node [
    id 230
    label "appreciation"
  ]
  node [
    id 231
    label "osi&#261;ga&#263;"
  ]
  node [
    id 232
    label "dociera&#263;"
  ]
  node [
    id 233
    label "get"
  ]
  node [
    id 234
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 235
    label "mierzy&#263;"
  ]
  node [
    id 236
    label "u&#380;ywa&#263;"
  ]
  node [
    id 237
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 238
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 239
    label "exsert"
  ]
  node [
    id 240
    label "being"
  ]
  node [
    id 241
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 242
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 243
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 244
    label "p&#322;ywa&#263;"
  ]
  node [
    id 245
    label "run"
  ]
  node [
    id 246
    label "bangla&#263;"
  ]
  node [
    id 247
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 248
    label "przebiega&#263;"
  ]
  node [
    id 249
    label "wk&#322;ada&#263;"
  ]
  node [
    id 250
    label "proceed"
  ]
  node [
    id 251
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 252
    label "carry"
  ]
  node [
    id 253
    label "bywa&#263;"
  ]
  node [
    id 254
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 255
    label "stara&#263;_si&#281;"
  ]
  node [
    id 256
    label "para"
  ]
  node [
    id 257
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 258
    label "str&#243;j"
  ]
  node [
    id 259
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 260
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 261
    label "krok"
  ]
  node [
    id 262
    label "tryb"
  ]
  node [
    id 263
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 264
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 265
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 266
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 267
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 268
    label "continue"
  ]
  node [
    id 269
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 270
    label "Ohio"
  ]
  node [
    id 271
    label "wci&#281;cie"
  ]
  node [
    id 272
    label "Nowy_York"
  ]
  node [
    id 273
    label "warstwa"
  ]
  node [
    id 274
    label "samopoczucie"
  ]
  node [
    id 275
    label "Illinois"
  ]
  node [
    id 276
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 277
    label "state"
  ]
  node [
    id 278
    label "Jukatan"
  ]
  node [
    id 279
    label "Kalifornia"
  ]
  node [
    id 280
    label "Wirginia"
  ]
  node [
    id 281
    label "wektor"
  ]
  node [
    id 282
    label "Teksas"
  ]
  node [
    id 283
    label "Goa"
  ]
  node [
    id 284
    label "Waszyngton"
  ]
  node [
    id 285
    label "miejsce"
  ]
  node [
    id 286
    label "Massachusetts"
  ]
  node [
    id 287
    label "Alaska"
  ]
  node [
    id 288
    label "Arakan"
  ]
  node [
    id 289
    label "Hawaje"
  ]
  node [
    id 290
    label "Maryland"
  ]
  node [
    id 291
    label "punkt"
  ]
  node [
    id 292
    label "Michigan"
  ]
  node [
    id 293
    label "Arizona"
  ]
  node [
    id 294
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 295
    label "Georgia"
  ]
  node [
    id 296
    label "poziom"
  ]
  node [
    id 297
    label "Pensylwania"
  ]
  node [
    id 298
    label "shape"
  ]
  node [
    id 299
    label "Luizjana"
  ]
  node [
    id 300
    label "Nowy_Meksyk"
  ]
  node [
    id 301
    label "Alabama"
  ]
  node [
    id 302
    label "ilo&#347;&#263;"
  ]
  node [
    id 303
    label "Kansas"
  ]
  node [
    id 304
    label "Oregon"
  ]
  node [
    id 305
    label "Floryda"
  ]
  node [
    id 306
    label "Oklahoma"
  ]
  node [
    id 307
    label "jednostka_administracyjna"
  ]
  node [
    id 308
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 309
    label "gaworzy&#263;"
  ]
  node [
    id 310
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 311
    label "remark"
  ]
  node [
    id 312
    label "wyra&#380;a&#263;"
  ]
  node [
    id 313
    label "umie&#263;"
  ]
  node [
    id 314
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 315
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 316
    label "formu&#322;owa&#263;"
  ]
  node [
    id 317
    label "dysfonia"
  ]
  node [
    id 318
    label "express"
  ]
  node [
    id 319
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 320
    label "talk"
  ]
  node [
    id 321
    label "prawi&#263;"
  ]
  node [
    id 322
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 323
    label "powiada&#263;"
  ]
  node [
    id 324
    label "tell"
  ]
  node [
    id 325
    label "chew_the_fat"
  ]
  node [
    id 326
    label "say"
  ]
  node [
    id 327
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 328
    label "informowa&#263;"
  ]
  node [
    id 329
    label "wydobywa&#263;"
  ]
  node [
    id 330
    label "okre&#347;la&#263;"
  ]
  node [
    id 331
    label "distribute"
  ]
  node [
    id 332
    label "bash"
  ]
  node [
    id 333
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 334
    label "decydowa&#263;"
  ]
  node [
    id 335
    label "signify"
  ]
  node [
    id 336
    label "style"
  ]
  node [
    id 337
    label "powodowa&#263;"
  ]
  node [
    id 338
    label "komunikowa&#263;"
  ]
  node [
    id 339
    label "inform"
  ]
  node [
    id 340
    label "znaczy&#263;"
  ]
  node [
    id 341
    label "give_voice"
  ]
  node [
    id 342
    label "oznacza&#263;"
  ]
  node [
    id 343
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 344
    label "represent"
  ]
  node [
    id 345
    label "convey"
  ]
  node [
    id 346
    label "arouse"
  ]
  node [
    id 347
    label "determine"
  ]
  node [
    id 348
    label "work"
  ]
  node [
    id 349
    label "reakcja_chemiczna"
  ]
  node [
    id 350
    label "uwydatnia&#263;"
  ]
  node [
    id 351
    label "eksploatowa&#263;"
  ]
  node [
    id 352
    label "uzyskiwa&#263;"
  ]
  node [
    id 353
    label "wydostawa&#263;"
  ]
  node [
    id 354
    label "wyjmowa&#263;"
  ]
  node [
    id 355
    label "train"
  ]
  node [
    id 356
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 357
    label "wydawa&#263;"
  ]
  node [
    id 358
    label "dobywa&#263;"
  ]
  node [
    id 359
    label "ocala&#263;"
  ]
  node [
    id 360
    label "excavate"
  ]
  node [
    id 361
    label "g&#243;rnictwo"
  ]
  node [
    id 362
    label "raise"
  ]
  node [
    id 363
    label "can"
  ]
  node [
    id 364
    label "m&#243;c"
  ]
  node [
    id 365
    label "mawia&#263;"
  ]
  node [
    id 366
    label "opowiada&#263;"
  ]
  node [
    id 367
    label "chatter"
  ]
  node [
    id 368
    label "niemowl&#281;"
  ]
  node [
    id 369
    label "kosmetyk"
  ]
  node [
    id 370
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 371
    label "stanowisko_archeologiczne"
  ]
  node [
    id 372
    label "dysphonia"
  ]
  node [
    id 373
    label "dysleksja"
  ]
  node [
    id 374
    label "organizowa&#263;"
  ]
  node [
    id 375
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 376
    label "czyni&#263;"
  ]
  node [
    id 377
    label "stylizowa&#263;"
  ]
  node [
    id 378
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 379
    label "falowa&#263;"
  ]
  node [
    id 380
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 381
    label "peddle"
  ]
  node [
    id 382
    label "praca"
  ]
  node [
    id 383
    label "wydala&#263;"
  ]
  node [
    id 384
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 385
    label "tentegowa&#263;"
  ]
  node [
    id 386
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 387
    label "urz&#261;dza&#263;"
  ]
  node [
    id 388
    label "oszukiwa&#263;"
  ]
  node [
    id 389
    label "ukazywa&#263;"
  ]
  node [
    id 390
    label "przerabia&#263;"
  ]
  node [
    id 391
    label "act"
  ]
  node [
    id 392
    label "post&#281;powa&#263;"
  ]
  node [
    id 393
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 394
    label "billow"
  ]
  node [
    id 395
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 396
    label "beckon"
  ]
  node [
    id 397
    label "powiewa&#263;"
  ]
  node [
    id 398
    label "planowa&#263;"
  ]
  node [
    id 399
    label "dostosowywa&#263;"
  ]
  node [
    id 400
    label "treat"
  ]
  node [
    id 401
    label "pozyskiwa&#263;"
  ]
  node [
    id 402
    label "ensnare"
  ]
  node [
    id 403
    label "skupia&#263;"
  ]
  node [
    id 404
    label "create"
  ]
  node [
    id 405
    label "przygotowywa&#263;"
  ]
  node [
    id 406
    label "tworzy&#263;"
  ]
  node [
    id 407
    label "standard"
  ]
  node [
    id 408
    label "wprowadza&#263;"
  ]
  node [
    id 409
    label "kopiowa&#263;"
  ]
  node [
    id 410
    label "czerpa&#263;"
  ]
  node [
    id 411
    label "dally"
  ]
  node [
    id 412
    label "mock"
  ]
  node [
    id 413
    label "sprawia&#263;"
  ]
  node [
    id 414
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 415
    label "cast"
  ]
  node [
    id 416
    label "podbija&#263;"
  ]
  node [
    id 417
    label "przechodzi&#263;"
  ]
  node [
    id 418
    label "wytwarza&#263;"
  ]
  node [
    id 419
    label "amend"
  ]
  node [
    id 420
    label "zalicza&#263;"
  ]
  node [
    id 421
    label "overwork"
  ]
  node [
    id 422
    label "convert"
  ]
  node [
    id 423
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 424
    label "zamienia&#263;"
  ]
  node [
    id 425
    label "zmienia&#263;"
  ]
  node [
    id 426
    label "modyfikowa&#263;"
  ]
  node [
    id 427
    label "radzi&#263;_sobie"
  ]
  node [
    id 428
    label "pracowa&#263;"
  ]
  node [
    id 429
    label "przetwarza&#263;"
  ]
  node [
    id 430
    label "sp&#281;dza&#263;"
  ]
  node [
    id 431
    label "stylize"
  ]
  node [
    id 432
    label "upodabnia&#263;"
  ]
  node [
    id 433
    label "nadawa&#263;"
  ]
  node [
    id 434
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 435
    label "go"
  ]
  node [
    id 436
    label "przybiera&#263;"
  ]
  node [
    id 437
    label "i&#347;&#263;"
  ]
  node [
    id 438
    label "use"
  ]
  node [
    id 439
    label "blurt_out"
  ]
  node [
    id 440
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 441
    label "usuwa&#263;"
  ]
  node [
    id 442
    label "unwrap"
  ]
  node [
    id 443
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 444
    label "pokazywa&#263;"
  ]
  node [
    id 445
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 446
    label "orzyna&#263;"
  ]
  node [
    id 447
    label "oszwabia&#263;"
  ]
  node [
    id 448
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 449
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 450
    label "cheat"
  ]
  node [
    id 451
    label "dispose"
  ]
  node [
    id 452
    label "aran&#380;owa&#263;"
  ]
  node [
    id 453
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 454
    label "odpowiada&#263;"
  ]
  node [
    id 455
    label "zabezpiecza&#263;"
  ]
  node [
    id 456
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 457
    label "doprowadza&#263;"
  ]
  node [
    id 458
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 459
    label "najem"
  ]
  node [
    id 460
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 461
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 462
    label "zak&#322;ad"
  ]
  node [
    id 463
    label "stosunek_pracy"
  ]
  node [
    id 464
    label "benedykty&#324;ski"
  ]
  node [
    id 465
    label "poda&#380;_pracy"
  ]
  node [
    id 466
    label "pracowanie"
  ]
  node [
    id 467
    label "tyrka"
  ]
  node [
    id 468
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 469
    label "wytw&#243;r"
  ]
  node [
    id 470
    label "zaw&#243;d"
  ]
  node [
    id 471
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 472
    label "tynkarski"
  ]
  node [
    id 473
    label "czynno&#347;&#263;"
  ]
  node [
    id 474
    label "zmiana"
  ]
  node [
    id 475
    label "czynnik_produkcji"
  ]
  node [
    id 476
    label "zobowi&#261;zanie"
  ]
  node [
    id 477
    label "kierownictwo"
  ]
  node [
    id 478
    label "siedziba"
  ]
  node [
    id 479
    label "zmianowo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 12
    target 12
  ]
]
