graph [
  node [
    id 0
    label "sz&#243;stka"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "lubelskie"
    origin "text"
  ]
  node [
    id 3
    label "toto-lotek"
  ]
  node [
    id 4
    label "trafienie"
  ]
  node [
    id 5
    label "zbi&#243;r"
  ]
  node [
    id 6
    label "bilard"
  ]
  node [
    id 7
    label "cyfra"
  ]
  node [
    id 8
    label "stopie&#324;"
  ]
  node [
    id 9
    label "six"
  ]
  node [
    id 10
    label "obiekt"
  ]
  node [
    id 11
    label "blotka"
  ]
  node [
    id 12
    label "zaprz&#281;g"
  ]
  node [
    id 13
    label "przedtrzonowiec"
  ]
  node [
    id 14
    label "znak_pisarski"
  ]
  node [
    id 15
    label "inicja&#322;"
  ]
  node [
    id 16
    label "wz&#243;r"
  ]
  node [
    id 17
    label "zjawienie_si&#281;"
  ]
  node [
    id 18
    label "dolecenie"
  ]
  node [
    id 19
    label "punkt"
  ]
  node [
    id 20
    label "rozgrywka"
  ]
  node [
    id 21
    label "strike"
  ]
  node [
    id 22
    label "dostanie_si&#281;"
  ]
  node [
    id 23
    label "wpadni&#281;cie"
  ]
  node [
    id 24
    label "spowodowanie"
  ]
  node [
    id 25
    label "pocisk"
  ]
  node [
    id 26
    label "hit"
  ]
  node [
    id 27
    label "zdarzenie_si&#281;"
  ]
  node [
    id 28
    label "sukces"
  ]
  node [
    id 29
    label "znalezienie_si&#281;"
  ]
  node [
    id 30
    label "znalezienie"
  ]
  node [
    id 31
    label "dopasowanie_si&#281;"
  ]
  node [
    id 32
    label "dotarcie"
  ]
  node [
    id 33
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 34
    label "gather"
  ]
  node [
    id 35
    label "dostanie"
  ]
  node [
    id 36
    label "z&#261;b"
  ]
  node [
    id 37
    label "premolar"
  ]
  node [
    id 38
    label "u&#378;dzienica"
  ]
  node [
    id 39
    label "postronek"
  ]
  node [
    id 40
    label "tobogan"
  ]
  node [
    id 41
    label "uzda"
  ]
  node [
    id 42
    label "chom&#261;to"
  ]
  node [
    id 43
    label "pojazd_niemechaniczny"
  ]
  node [
    id 44
    label "naszelnik"
  ]
  node [
    id 45
    label "nakarcznik"
  ]
  node [
    id 46
    label "janczary"
  ]
  node [
    id 47
    label "moderunek"
  ]
  node [
    id 48
    label "podogonie"
  ]
  node [
    id 49
    label "co&#347;"
  ]
  node [
    id 50
    label "budynek"
  ]
  node [
    id 51
    label "thing"
  ]
  node [
    id 52
    label "poj&#281;cie"
  ]
  node [
    id 53
    label "program"
  ]
  node [
    id 54
    label "rzecz"
  ]
  node [
    id 55
    label "strona"
  ]
  node [
    id 56
    label "kszta&#322;t"
  ]
  node [
    id 57
    label "podstopie&#324;"
  ]
  node [
    id 58
    label "wielko&#347;&#263;"
  ]
  node [
    id 59
    label "rank"
  ]
  node [
    id 60
    label "minuta"
  ]
  node [
    id 61
    label "d&#378;wi&#281;k"
  ]
  node [
    id 62
    label "wschodek"
  ]
  node [
    id 63
    label "przymiotnik"
  ]
  node [
    id 64
    label "gama"
  ]
  node [
    id 65
    label "jednostka"
  ]
  node [
    id 66
    label "podzia&#322;"
  ]
  node [
    id 67
    label "miejsce"
  ]
  node [
    id 68
    label "element"
  ]
  node [
    id 69
    label "schody"
  ]
  node [
    id 70
    label "kategoria_gramatyczna"
  ]
  node [
    id 71
    label "poziom"
  ]
  node [
    id 72
    label "przys&#322;&#243;wek"
  ]
  node [
    id 73
    label "ocena"
  ]
  node [
    id 74
    label "degree"
  ]
  node [
    id 75
    label "szczebel"
  ]
  node [
    id 76
    label "znaczenie"
  ]
  node [
    id 77
    label "podn&#243;&#380;ek"
  ]
  node [
    id 78
    label "forma"
  ]
  node [
    id 79
    label "egzemplarz"
  ]
  node [
    id 80
    label "series"
  ]
  node [
    id 81
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 82
    label "uprawianie"
  ]
  node [
    id 83
    label "praca_rolnicza"
  ]
  node [
    id 84
    label "collection"
  ]
  node [
    id 85
    label "dane"
  ]
  node [
    id 86
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 87
    label "pakiet_klimatyczny"
  ]
  node [
    id 88
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 89
    label "sum"
  ]
  node [
    id 90
    label "gathering"
  ]
  node [
    id 91
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 92
    label "album"
  ]
  node [
    id 93
    label "plichta"
  ]
  node [
    id 94
    label "karta"
  ]
  node [
    id 95
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 96
    label "st&#243;&#322;"
  ]
  node [
    id 97
    label "skiksowanie"
  ]
  node [
    id 98
    label "kiksowanie"
  ]
  node [
    id 99
    label "skiksowa&#263;"
  ]
  node [
    id 100
    label "sztos"
  ]
  node [
    id 101
    label "sport"
  ]
  node [
    id 102
    label "&#322;uza"
  ]
  node [
    id 103
    label "kiksowa&#263;"
  ]
  node [
    id 104
    label "banda"
  ]
  node [
    id 105
    label "rest"
  ]
  node [
    id 106
    label "czw&#243;rka"
  ]
  node [
    id 107
    label "pi&#261;tka"
  ]
  node [
    id 108
    label "totalizator"
  ]
  node [
    id 109
    label "tr&#243;jka"
  ]
  node [
    id 110
    label "powiat"
  ]
  node [
    id 111
    label "mikroregion"
  ]
  node [
    id 112
    label "makroregion"
  ]
  node [
    id 113
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 114
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 115
    label "pa&#324;stwo"
  ]
  node [
    id 116
    label "jednostka_administracyjna"
  ]
  node [
    id 117
    label "region"
  ]
  node [
    id 118
    label "gmina"
  ]
  node [
    id 119
    label "mezoregion"
  ]
  node [
    id 120
    label "Jura"
  ]
  node [
    id 121
    label "Beskidy_Zachodnie"
  ]
  node [
    id 122
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 123
    label "Katar"
  ]
  node [
    id 124
    label "Libia"
  ]
  node [
    id 125
    label "Gwatemala"
  ]
  node [
    id 126
    label "Ekwador"
  ]
  node [
    id 127
    label "Afganistan"
  ]
  node [
    id 128
    label "Tad&#380;ykistan"
  ]
  node [
    id 129
    label "Bhutan"
  ]
  node [
    id 130
    label "Argentyna"
  ]
  node [
    id 131
    label "D&#380;ibuti"
  ]
  node [
    id 132
    label "Wenezuela"
  ]
  node [
    id 133
    label "Gabon"
  ]
  node [
    id 134
    label "Ukraina"
  ]
  node [
    id 135
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 136
    label "Rwanda"
  ]
  node [
    id 137
    label "Liechtenstein"
  ]
  node [
    id 138
    label "organizacja"
  ]
  node [
    id 139
    label "Sri_Lanka"
  ]
  node [
    id 140
    label "Madagaskar"
  ]
  node [
    id 141
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 142
    label "Kongo"
  ]
  node [
    id 143
    label "Tonga"
  ]
  node [
    id 144
    label "Bangladesz"
  ]
  node [
    id 145
    label "Kanada"
  ]
  node [
    id 146
    label "Wehrlen"
  ]
  node [
    id 147
    label "Algieria"
  ]
  node [
    id 148
    label "Uganda"
  ]
  node [
    id 149
    label "Surinam"
  ]
  node [
    id 150
    label "Sahara_Zachodnia"
  ]
  node [
    id 151
    label "Chile"
  ]
  node [
    id 152
    label "W&#281;gry"
  ]
  node [
    id 153
    label "Birma"
  ]
  node [
    id 154
    label "Kazachstan"
  ]
  node [
    id 155
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 156
    label "Armenia"
  ]
  node [
    id 157
    label "Tuwalu"
  ]
  node [
    id 158
    label "Timor_Wschodni"
  ]
  node [
    id 159
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 160
    label "Izrael"
  ]
  node [
    id 161
    label "Estonia"
  ]
  node [
    id 162
    label "Komory"
  ]
  node [
    id 163
    label "Kamerun"
  ]
  node [
    id 164
    label "Haiti"
  ]
  node [
    id 165
    label "Belize"
  ]
  node [
    id 166
    label "Sierra_Leone"
  ]
  node [
    id 167
    label "Luksemburg"
  ]
  node [
    id 168
    label "USA"
  ]
  node [
    id 169
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 170
    label "Barbados"
  ]
  node [
    id 171
    label "San_Marino"
  ]
  node [
    id 172
    label "Bu&#322;garia"
  ]
  node [
    id 173
    label "Indonezja"
  ]
  node [
    id 174
    label "Wietnam"
  ]
  node [
    id 175
    label "Malawi"
  ]
  node [
    id 176
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 177
    label "Francja"
  ]
  node [
    id 178
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 179
    label "partia"
  ]
  node [
    id 180
    label "Zambia"
  ]
  node [
    id 181
    label "Angola"
  ]
  node [
    id 182
    label "Grenada"
  ]
  node [
    id 183
    label "Nepal"
  ]
  node [
    id 184
    label "Panama"
  ]
  node [
    id 185
    label "Rumunia"
  ]
  node [
    id 186
    label "Czarnog&#243;ra"
  ]
  node [
    id 187
    label "Malediwy"
  ]
  node [
    id 188
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 189
    label "S&#322;owacja"
  ]
  node [
    id 190
    label "para"
  ]
  node [
    id 191
    label "Egipt"
  ]
  node [
    id 192
    label "zwrot"
  ]
  node [
    id 193
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 194
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 195
    label "Mozambik"
  ]
  node [
    id 196
    label "Kolumbia"
  ]
  node [
    id 197
    label "Laos"
  ]
  node [
    id 198
    label "Burundi"
  ]
  node [
    id 199
    label "Suazi"
  ]
  node [
    id 200
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 201
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 202
    label "Czechy"
  ]
  node [
    id 203
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 204
    label "Wyspy_Marshalla"
  ]
  node [
    id 205
    label "Dominika"
  ]
  node [
    id 206
    label "Trynidad_i_Tobago"
  ]
  node [
    id 207
    label "Syria"
  ]
  node [
    id 208
    label "Palau"
  ]
  node [
    id 209
    label "Gwinea_Bissau"
  ]
  node [
    id 210
    label "Liberia"
  ]
  node [
    id 211
    label "Jamajka"
  ]
  node [
    id 212
    label "Zimbabwe"
  ]
  node [
    id 213
    label "Polska"
  ]
  node [
    id 214
    label "Dominikana"
  ]
  node [
    id 215
    label "Senegal"
  ]
  node [
    id 216
    label "Togo"
  ]
  node [
    id 217
    label "Gujana"
  ]
  node [
    id 218
    label "Gruzja"
  ]
  node [
    id 219
    label "Albania"
  ]
  node [
    id 220
    label "Zair"
  ]
  node [
    id 221
    label "Meksyk"
  ]
  node [
    id 222
    label "Macedonia"
  ]
  node [
    id 223
    label "Chorwacja"
  ]
  node [
    id 224
    label "Kambod&#380;a"
  ]
  node [
    id 225
    label "Monako"
  ]
  node [
    id 226
    label "Mauritius"
  ]
  node [
    id 227
    label "Gwinea"
  ]
  node [
    id 228
    label "Mali"
  ]
  node [
    id 229
    label "Nigeria"
  ]
  node [
    id 230
    label "Kostaryka"
  ]
  node [
    id 231
    label "Hanower"
  ]
  node [
    id 232
    label "Paragwaj"
  ]
  node [
    id 233
    label "W&#322;ochy"
  ]
  node [
    id 234
    label "Seszele"
  ]
  node [
    id 235
    label "Wyspy_Salomona"
  ]
  node [
    id 236
    label "Hiszpania"
  ]
  node [
    id 237
    label "Boliwia"
  ]
  node [
    id 238
    label "Kirgistan"
  ]
  node [
    id 239
    label "Irlandia"
  ]
  node [
    id 240
    label "Czad"
  ]
  node [
    id 241
    label "Irak"
  ]
  node [
    id 242
    label "Lesoto"
  ]
  node [
    id 243
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 244
    label "Malta"
  ]
  node [
    id 245
    label "Andora"
  ]
  node [
    id 246
    label "Chiny"
  ]
  node [
    id 247
    label "Filipiny"
  ]
  node [
    id 248
    label "Antarktis"
  ]
  node [
    id 249
    label "Niemcy"
  ]
  node [
    id 250
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 251
    label "Pakistan"
  ]
  node [
    id 252
    label "terytorium"
  ]
  node [
    id 253
    label "Nikaragua"
  ]
  node [
    id 254
    label "Brazylia"
  ]
  node [
    id 255
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 256
    label "Maroko"
  ]
  node [
    id 257
    label "Portugalia"
  ]
  node [
    id 258
    label "Niger"
  ]
  node [
    id 259
    label "Kenia"
  ]
  node [
    id 260
    label "Botswana"
  ]
  node [
    id 261
    label "Fid&#380;i"
  ]
  node [
    id 262
    label "Tunezja"
  ]
  node [
    id 263
    label "Australia"
  ]
  node [
    id 264
    label "Tajlandia"
  ]
  node [
    id 265
    label "Burkina_Faso"
  ]
  node [
    id 266
    label "interior"
  ]
  node [
    id 267
    label "Tanzania"
  ]
  node [
    id 268
    label "Benin"
  ]
  node [
    id 269
    label "Indie"
  ]
  node [
    id 270
    label "&#321;otwa"
  ]
  node [
    id 271
    label "Kiribati"
  ]
  node [
    id 272
    label "Antigua_i_Barbuda"
  ]
  node [
    id 273
    label "Rodezja"
  ]
  node [
    id 274
    label "Cypr"
  ]
  node [
    id 275
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 276
    label "Peru"
  ]
  node [
    id 277
    label "Austria"
  ]
  node [
    id 278
    label "Urugwaj"
  ]
  node [
    id 279
    label "Jordania"
  ]
  node [
    id 280
    label "Grecja"
  ]
  node [
    id 281
    label "Azerbejd&#380;an"
  ]
  node [
    id 282
    label "Turcja"
  ]
  node [
    id 283
    label "Samoa"
  ]
  node [
    id 284
    label "Sudan"
  ]
  node [
    id 285
    label "Oman"
  ]
  node [
    id 286
    label "ziemia"
  ]
  node [
    id 287
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 288
    label "Uzbekistan"
  ]
  node [
    id 289
    label "Portoryko"
  ]
  node [
    id 290
    label "Honduras"
  ]
  node [
    id 291
    label "Mongolia"
  ]
  node [
    id 292
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 293
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 294
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 295
    label "Serbia"
  ]
  node [
    id 296
    label "Tajwan"
  ]
  node [
    id 297
    label "Wielka_Brytania"
  ]
  node [
    id 298
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 299
    label "Liban"
  ]
  node [
    id 300
    label "Japonia"
  ]
  node [
    id 301
    label "Ghana"
  ]
  node [
    id 302
    label "Belgia"
  ]
  node [
    id 303
    label "Bahrajn"
  ]
  node [
    id 304
    label "Mikronezja"
  ]
  node [
    id 305
    label "Etiopia"
  ]
  node [
    id 306
    label "Kuwejt"
  ]
  node [
    id 307
    label "grupa"
  ]
  node [
    id 308
    label "Bahamy"
  ]
  node [
    id 309
    label "Rosja"
  ]
  node [
    id 310
    label "Mo&#322;dawia"
  ]
  node [
    id 311
    label "Litwa"
  ]
  node [
    id 312
    label "S&#322;owenia"
  ]
  node [
    id 313
    label "Szwajcaria"
  ]
  node [
    id 314
    label "Erytrea"
  ]
  node [
    id 315
    label "Arabia_Saudyjska"
  ]
  node [
    id 316
    label "Kuba"
  ]
  node [
    id 317
    label "granica_pa&#324;stwa"
  ]
  node [
    id 318
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 319
    label "Malezja"
  ]
  node [
    id 320
    label "Korea"
  ]
  node [
    id 321
    label "Jemen"
  ]
  node [
    id 322
    label "Nowa_Zelandia"
  ]
  node [
    id 323
    label "Namibia"
  ]
  node [
    id 324
    label "Nauru"
  ]
  node [
    id 325
    label "holoarktyka"
  ]
  node [
    id 326
    label "Brunei"
  ]
  node [
    id 327
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 328
    label "Khitai"
  ]
  node [
    id 329
    label "Mauretania"
  ]
  node [
    id 330
    label "Iran"
  ]
  node [
    id 331
    label "Gambia"
  ]
  node [
    id 332
    label "Somalia"
  ]
  node [
    id 333
    label "Holandia"
  ]
  node [
    id 334
    label "Turkmenistan"
  ]
  node [
    id 335
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 336
    label "Salwador"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
]
