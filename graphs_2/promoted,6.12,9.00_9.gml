graph [
  node [
    id 0
    label "mecz"
    origin "text"
  ]
  node [
    id 1
    label "liga"
    origin "text"
  ]
  node [
    id 2
    label "argenty&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "obrona"
  ]
  node [
    id 4
    label "gra"
  ]
  node [
    id 5
    label "game"
  ]
  node [
    id 6
    label "serw"
  ]
  node [
    id 7
    label "dwumecz"
  ]
  node [
    id 8
    label "zmienno&#347;&#263;"
  ]
  node [
    id 9
    label "play"
  ]
  node [
    id 10
    label "rozgrywka"
  ]
  node [
    id 11
    label "apparent_motion"
  ]
  node [
    id 12
    label "wydarzenie"
  ]
  node [
    id 13
    label "contest"
  ]
  node [
    id 14
    label "akcja"
  ]
  node [
    id 15
    label "komplet"
  ]
  node [
    id 16
    label "zabawa"
  ]
  node [
    id 17
    label "zasada"
  ]
  node [
    id 18
    label "rywalizacja"
  ]
  node [
    id 19
    label "zbijany"
  ]
  node [
    id 20
    label "post&#281;powanie"
  ]
  node [
    id 21
    label "odg&#322;os"
  ]
  node [
    id 22
    label "Pok&#233;mon"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "synteza"
  ]
  node [
    id 25
    label "odtworzenie"
  ]
  node [
    id 26
    label "rekwizyt_do_gry"
  ]
  node [
    id 27
    label "egzamin"
  ]
  node [
    id 28
    label "walka"
  ]
  node [
    id 29
    label "gracz"
  ]
  node [
    id 30
    label "poj&#281;cie"
  ]
  node [
    id 31
    label "protection"
  ]
  node [
    id 32
    label "poparcie"
  ]
  node [
    id 33
    label "reakcja"
  ]
  node [
    id 34
    label "defense"
  ]
  node [
    id 35
    label "s&#261;d"
  ]
  node [
    id 36
    label "auspices"
  ]
  node [
    id 37
    label "ochrona"
  ]
  node [
    id 38
    label "sp&#243;r"
  ]
  node [
    id 39
    label "wojsko"
  ]
  node [
    id 40
    label "manewr"
  ]
  node [
    id 41
    label "defensive_structure"
  ]
  node [
    id 42
    label "guard_duty"
  ]
  node [
    id 43
    label "strona"
  ]
  node [
    id 44
    label "uderzenie"
  ]
  node [
    id 45
    label "mecz_mistrzowski"
  ]
  node [
    id 46
    label "&#347;rodowisko"
  ]
  node [
    id 47
    label "arrangement"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "pomoc"
  ]
  node [
    id 50
    label "organizacja"
  ]
  node [
    id 51
    label "poziom"
  ]
  node [
    id 52
    label "rezerwa"
  ]
  node [
    id 53
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 54
    label "pr&#243;ba"
  ]
  node [
    id 55
    label "atak"
  ]
  node [
    id 56
    label "moneta"
  ]
  node [
    id 57
    label "union"
  ]
  node [
    id 58
    label "grupa"
  ]
  node [
    id 59
    label "po&#322;o&#380;enie"
  ]
  node [
    id 60
    label "jako&#347;&#263;"
  ]
  node [
    id 61
    label "p&#322;aszczyzna"
  ]
  node [
    id 62
    label "punkt_widzenia"
  ]
  node [
    id 63
    label "kierunek"
  ]
  node [
    id 64
    label "wyk&#322;adnik"
  ]
  node [
    id 65
    label "faza"
  ]
  node [
    id 66
    label "szczebel"
  ]
  node [
    id 67
    label "budynek"
  ]
  node [
    id 68
    label "wysoko&#347;&#263;"
  ]
  node [
    id 69
    label "ranga"
  ]
  node [
    id 70
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 71
    label "class"
  ]
  node [
    id 72
    label "zesp&#243;&#322;"
  ]
  node [
    id 73
    label "obiekt_naturalny"
  ]
  node [
    id 74
    label "otoczenie"
  ]
  node [
    id 75
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 76
    label "environment"
  ]
  node [
    id 77
    label "rzecz"
  ]
  node [
    id 78
    label "huczek"
  ]
  node [
    id 79
    label "ekosystem"
  ]
  node [
    id 80
    label "wszechstworzenie"
  ]
  node [
    id 81
    label "woda"
  ]
  node [
    id 82
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 83
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 84
    label "teren"
  ]
  node [
    id 85
    label "mikrokosmos"
  ]
  node [
    id 86
    label "stw&#243;r"
  ]
  node [
    id 87
    label "warunki"
  ]
  node [
    id 88
    label "Ziemia"
  ]
  node [
    id 89
    label "fauna"
  ]
  node [
    id 90
    label "biota"
  ]
  node [
    id 91
    label "do&#347;wiadczenie"
  ]
  node [
    id 92
    label "spotkanie"
  ]
  node [
    id 93
    label "pobiera&#263;"
  ]
  node [
    id 94
    label "metal_szlachetny"
  ]
  node [
    id 95
    label "pobranie"
  ]
  node [
    id 96
    label "usi&#322;owanie"
  ]
  node [
    id 97
    label "pobra&#263;"
  ]
  node [
    id 98
    label "pobieranie"
  ]
  node [
    id 99
    label "znak"
  ]
  node [
    id 100
    label "rezultat"
  ]
  node [
    id 101
    label "effort"
  ]
  node [
    id 102
    label "analiza_chemiczna"
  ]
  node [
    id 103
    label "item"
  ]
  node [
    id 104
    label "sytuacja"
  ]
  node [
    id 105
    label "probiernictwo"
  ]
  node [
    id 106
    label "ilo&#347;&#263;"
  ]
  node [
    id 107
    label "test"
  ]
  node [
    id 108
    label "podmiot"
  ]
  node [
    id 109
    label "jednostka_organizacyjna"
  ]
  node [
    id 110
    label "struktura"
  ]
  node [
    id 111
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 112
    label "TOPR"
  ]
  node [
    id 113
    label "endecki"
  ]
  node [
    id 114
    label "przedstawicielstwo"
  ]
  node [
    id 115
    label "od&#322;am"
  ]
  node [
    id 116
    label "Cepelia"
  ]
  node [
    id 117
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 118
    label "ZBoWiD"
  ]
  node [
    id 119
    label "organization"
  ]
  node [
    id 120
    label "centrala"
  ]
  node [
    id 121
    label "GOPR"
  ]
  node [
    id 122
    label "ZOMO"
  ]
  node [
    id 123
    label "ZMP"
  ]
  node [
    id 124
    label "komitet_koordynacyjny"
  ]
  node [
    id 125
    label "przybud&#243;wka"
  ]
  node [
    id 126
    label "boj&#243;wka"
  ]
  node [
    id 127
    label "egzemplarz"
  ]
  node [
    id 128
    label "series"
  ]
  node [
    id 129
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 130
    label "uprawianie"
  ]
  node [
    id 131
    label "praca_rolnicza"
  ]
  node [
    id 132
    label "collection"
  ]
  node [
    id 133
    label "dane"
  ]
  node [
    id 134
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 135
    label "pakiet_klimatyczny"
  ]
  node [
    id 136
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 137
    label "sum"
  ]
  node [
    id 138
    label "gathering"
  ]
  node [
    id 139
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 140
    label "album"
  ]
  node [
    id 141
    label "awers"
  ]
  node [
    id 142
    label "legenda"
  ]
  node [
    id 143
    label "rewers"
  ]
  node [
    id 144
    label "egzerga"
  ]
  node [
    id 145
    label "pieni&#261;dz"
  ]
  node [
    id 146
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 147
    label "otok"
  ]
  node [
    id 148
    label "balansjerka"
  ]
  node [
    id 149
    label "odm&#322;adzanie"
  ]
  node [
    id 150
    label "jednostka_systematyczna"
  ]
  node [
    id 151
    label "asymilowanie"
  ]
  node [
    id 152
    label "gromada"
  ]
  node [
    id 153
    label "asymilowa&#263;"
  ]
  node [
    id 154
    label "Entuzjastki"
  ]
  node [
    id 155
    label "kompozycja"
  ]
  node [
    id 156
    label "Terranie"
  ]
  node [
    id 157
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 158
    label "category"
  ]
  node [
    id 159
    label "oddzia&#322;"
  ]
  node [
    id 160
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 161
    label "cz&#261;steczka"
  ]
  node [
    id 162
    label "stage_set"
  ]
  node [
    id 163
    label "type"
  ]
  node [
    id 164
    label "specgrupa"
  ]
  node [
    id 165
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 166
    label "&#346;wietliki"
  ]
  node [
    id 167
    label "odm&#322;odzenie"
  ]
  node [
    id 168
    label "Eurogrupa"
  ]
  node [
    id 169
    label "odm&#322;adza&#263;"
  ]
  node [
    id 170
    label "formacja_geologiczna"
  ]
  node [
    id 171
    label "harcerze_starsi"
  ]
  node [
    id 172
    label "&#347;rodek"
  ]
  node [
    id 173
    label "darowizna"
  ]
  node [
    id 174
    label "przedmiot"
  ]
  node [
    id 175
    label "doch&#243;d"
  ]
  node [
    id 176
    label "telefon_zaufania"
  ]
  node [
    id 177
    label "pomocnik"
  ]
  node [
    id 178
    label "zgodzi&#263;"
  ]
  node [
    id 179
    label "property"
  ]
  node [
    id 180
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 181
    label "zas&#243;b"
  ]
  node [
    id 182
    label "nieufno&#347;&#263;"
  ]
  node [
    id 183
    label "zapasy"
  ]
  node [
    id 184
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 185
    label "resource"
  ]
  node [
    id 186
    label "oznaka"
  ]
  node [
    id 187
    label "pogorszenie"
  ]
  node [
    id 188
    label "przemoc"
  ]
  node [
    id 189
    label "krytyka"
  ]
  node [
    id 190
    label "bat"
  ]
  node [
    id 191
    label "kaszel"
  ]
  node [
    id 192
    label "fit"
  ]
  node [
    id 193
    label "rzuci&#263;"
  ]
  node [
    id 194
    label "spasm"
  ]
  node [
    id 195
    label "zagrywka"
  ]
  node [
    id 196
    label "wypowied&#378;"
  ]
  node [
    id 197
    label "&#380;&#261;danie"
  ]
  node [
    id 198
    label "przyp&#322;yw"
  ]
  node [
    id 199
    label "ofensywa"
  ]
  node [
    id 200
    label "pogoda"
  ]
  node [
    id 201
    label "stroke"
  ]
  node [
    id 202
    label "pozycja"
  ]
  node [
    id 203
    label "rzucenie"
  ]
  node [
    id 204
    label "knock"
  ]
  node [
    id 205
    label "po_argenty&#324;sku"
  ]
  node [
    id 206
    label "po&#322;udniowoameryka&#324;ski"
  ]
  node [
    id 207
    label "tango"
  ]
  node [
    id 208
    label "ameryka&#324;ski"
  ]
  node [
    id 209
    label "latynoameryka&#324;ski"
  ]
  node [
    id 210
    label "po_po&#322;udniowoameryka&#324;sku"
  ]
  node [
    id 211
    label "melodia"
  ]
  node [
    id 212
    label "taniec"
  ]
  node [
    id 213
    label "taniec_towarzyski"
  ]
  node [
    id 214
    label "kwadrat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
]
