graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "jako&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "powietrze"
    origin "text"
  ]
  node [
    id 4
    label "unia"
    origin "text"
  ]
  node [
    id 5
    label "europejski"
    origin "text"
  ]
  node [
    id 6
    label "przodkini"
  ]
  node [
    id 7
    label "matka_zast&#281;pcza"
  ]
  node [
    id 8
    label "matczysko"
  ]
  node [
    id 9
    label "rodzice"
  ]
  node [
    id 10
    label "stara"
  ]
  node [
    id 11
    label "macierz"
  ]
  node [
    id 12
    label "rodzic"
  ]
  node [
    id 13
    label "Matka_Boska"
  ]
  node [
    id 14
    label "macocha"
  ]
  node [
    id 15
    label "starzy"
  ]
  node [
    id 16
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 17
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 18
    label "pokolenie"
  ]
  node [
    id 19
    label "wapniaki"
  ]
  node [
    id 20
    label "opiekun"
  ]
  node [
    id 21
    label "wapniak"
  ]
  node [
    id 22
    label "rodzic_chrzestny"
  ]
  node [
    id 23
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 24
    label "krewna"
  ]
  node [
    id 25
    label "matka"
  ]
  node [
    id 26
    label "&#380;ona"
  ]
  node [
    id 27
    label "kobieta"
  ]
  node [
    id 28
    label "partnerka"
  ]
  node [
    id 29
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 30
    label "matuszka"
  ]
  node [
    id 31
    label "parametryzacja"
  ]
  node [
    id 32
    label "pa&#324;stwo"
  ]
  node [
    id 33
    label "poj&#281;cie"
  ]
  node [
    id 34
    label "mod"
  ]
  node [
    id 35
    label "patriota"
  ]
  node [
    id 36
    label "m&#281;&#380;atka"
  ]
  node [
    id 37
    label "pieski"
  ]
  node [
    id 38
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 39
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 40
    label "niekorzystny"
  ]
  node [
    id 41
    label "z&#322;oszczenie"
  ]
  node [
    id 42
    label "sierdzisty"
  ]
  node [
    id 43
    label "niegrzeczny"
  ]
  node [
    id 44
    label "zez&#322;oszczenie"
  ]
  node [
    id 45
    label "zdenerwowany"
  ]
  node [
    id 46
    label "negatywny"
  ]
  node [
    id 47
    label "rozgniewanie"
  ]
  node [
    id 48
    label "gniewanie"
  ]
  node [
    id 49
    label "niemoralny"
  ]
  node [
    id 50
    label "&#378;le"
  ]
  node [
    id 51
    label "niepomy&#347;lny"
  ]
  node [
    id 52
    label "syf"
  ]
  node [
    id 53
    label "niespokojny"
  ]
  node [
    id 54
    label "niekorzystnie"
  ]
  node [
    id 55
    label "ujemny"
  ]
  node [
    id 56
    label "nagannie"
  ]
  node [
    id 57
    label "niemoralnie"
  ]
  node [
    id 58
    label "nieprzyzwoity"
  ]
  node [
    id 59
    label "niepomy&#347;lnie"
  ]
  node [
    id 60
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 61
    label "nieodpowiednio"
  ]
  node [
    id 62
    label "r&#243;&#380;ny"
  ]
  node [
    id 63
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 64
    label "swoisty"
  ]
  node [
    id 65
    label "nienale&#380;yty"
  ]
  node [
    id 66
    label "dziwny"
  ]
  node [
    id 67
    label "niezno&#347;ny"
  ]
  node [
    id 68
    label "niegrzecznie"
  ]
  node [
    id 69
    label "trudny"
  ]
  node [
    id 70
    label "niestosowny"
  ]
  node [
    id 71
    label "brzydal"
  ]
  node [
    id 72
    label "niepos&#322;uszny"
  ]
  node [
    id 73
    label "negatywnie"
  ]
  node [
    id 74
    label "nieprzyjemny"
  ]
  node [
    id 75
    label "ujemnie"
  ]
  node [
    id 76
    label "gniewny"
  ]
  node [
    id 77
    label "serdeczny"
  ]
  node [
    id 78
    label "srogi"
  ]
  node [
    id 79
    label "sierdzi&#347;cie"
  ]
  node [
    id 80
    label "piesko"
  ]
  node [
    id 81
    label "rozgniewanie_si&#281;"
  ]
  node [
    id 82
    label "zagniewanie"
  ]
  node [
    id 83
    label "wzbudzenie"
  ]
  node [
    id 84
    label "wzbudzanie"
  ]
  node [
    id 85
    label "z&#322;oszczenie_si&#281;"
  ]
  node [
    id 86
    label "gniewanie_si&#281;"
  ]
  node [
    id 87
    label "niezgodnie"
  ]
  node [
    id 88
    label "gorzej"
  ]
  node [
    id 89
    label "syphilis"
  ]
  node [
    id 90
    label "tragedia"
  ]
  node [
    id 91
    label "nieporz&#261;dek"
  ]
  node [
    id 92
    label "kr&#281;tek_blady"
  ]
  node [
    id 93
    label "krosta"
  ]
  node [
    id 94
    label "choroba_dworska"
  ]
  node [
    id 95
    label "choroba_bakteryjna"
  ]
  node [
    id 96
    label "zabrudzenie"
  ]
  node [
    id 97
    label "substancja"
  ]
  node [
    id 98
    label "sk&#322;ad"
  ]
  node [
    id 99
    label "choroba_weneryczna"
  ]
  node [
    id 100
    label "ki&#322;a_wrodzona"
  ]
  node [
    id 101
    label "szankier_twardy"
  ]
  node [
    id 102
    label "spot"
  ]
  node [
    id 103
    label "zanieczyszczenie"
  ]
  node [
    id 104
    label "warto&#347;&#263;"
  ]
  node [
    id 105
    label "quality"
  ]
  node [
    id 106
    label "co&#347;"
  ]
  node [
    id 107
    label "state"
  ]
  node [
    id 108
    label "rozmiar"
  ]
  node [
    id 109
    label "rewaluowa&#263;"
  ]
  node [
    id 110
    label "zrewaluowa&#263;"
  ]
  node [
    id 111
    label "zmienna"
  ]
  node [
    id 112
    label "wskazywanie"
  ]
  node [
    id 113
    label "rewaluowanie"
  ]
  node [
    id 114
    label "cel"
  ]
  node [
    id 115
    label "wskazywa&#263;"
  ]
  node [
    id 116
    label "korzy&#347;&#263;"
  ]
  node [
    id 117
    label "worth"
  ]
  node [
    id 118
    label "cecha"
  ]
  node [
    id 119
    label "zrewaluowanie"
  ]
  node [
    id 120
    label "wabik"
  ]
  node [
    id 121
    label "strona"
  ]
  node [
    id 122
    label "thing"
  ]
  node [
    id 123
    label "cosik"
  ]
  node [
    id 124
    label "dmuchni&#281;cie"
  ]
  node [
    id 125
    label "eter"
  ]
  node [
    id 126
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 127
    label "breeze"
  ]
  node [
    id 128
    label "mieszanina"
  ]
  node [
    id 129
    label "front"
  ]
  node [
    id 130
    label "napowietrzy&#263;"
  ]
  node [
    id 131
    label "pneumatyczny"
  ]
  node [
    id 132
    label "przewietrza&#263;"
  ]
  node [
    id 133
    label "tlen"
  ]
  node [
    id 134
    label "wydychanie"
  ]
  node [
    id 135
    label "dmuchanie"
  ]
  node [
    id 136
    label "wdychanie"
  ]
  node [
    id 137
    label "przewietrzy&#263;"
  ]
  node [
    id 138
    label "luft"
  ]
  node [
    id 139
    label "dmucha&#263;"
  ]
  node [
    id 140
    label "podgrzew"
  ]
  node [
    id 141
    label "wydycha&#263;"
  ]
  node [
    id 142
    label "wdycha&#263;"
  ]
  node [
    id 143
    label "przewietrzanie"
  ]
  node [
    id 144
    label "geosystem"
  ]
  node [
    id 145
    label "pojazd"
  ]
  node [
    id 146
    label "&#380;ywio&#322;"
  ]
  node [
    id 147
    label "przewietrzenie"
  ]
  node [
    id 148
    label "frakcja"
  ]
  node [
    id 149
    label "synteza"
  ]
  node [
    id 150
    label "zbi&#243;r"
  ]
  node [
    id 151
    label "energia"
  ]
  node [
    id 152
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 153
    label "materia"
  ]
  node [
    id 154
    label "zajawka"
  ]
  node [
    id 155
    label "class"
  ]
  node [
    id 156
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 157
    label "feblik"
  ]
  node [
    id 158
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 159
    label "wdarcie_si&#281;"
  ]
  node [
    id 160
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 161
    label "wdzieranie_si&#281;"
  ]
  node [
    id 162
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 163
    label "tendency"
  ]
  node [
    id 164
    label "grupa"
  ]
  node [
    id 165
    label "huczek"
  ]
  node [
    id 166
    label "tlenowiec"
  ]
  node [
    id 167
    label "niemetal"
  ]
  node [
    id 168
    label "paramagnetyk"
  ]
  node [
    id 169
    label "przyducha"
  ]
  node [
    id 170
    label "makroelement"
  ]
  node [
    id 171
    label "hipoksja"
  ]
  node [
    id 172
    label "anoksja"
  ]
  node [
    id 173
    label "gaz"
  ]
  node [
    id 174
    label "niedotlenienie"
  ]
  node [
    id 175
    label "piec"
  ]
  node [
    id 176
    label "przew&#243;d"
  ]
  node [
    id 177
    label "komin"
  ]
  node [
    id 178
    label "etyl"
  ]
  node [
    id 179
    label "przestrze&#324;"
  ]
  node [
    id 180
    label "ciecz"
  ]
  node [
    id 181
    label "rozpuszczalnik"
  ]
  node [
    id 182
    label "ether"
  ]
  node [
    id 183
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 184
    label "gleba"
  ]
  node [
    id 185
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 186
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 187
    label "fauna"
  ]
  node [
    id 188
    label "wydala&#263;"
  ]
  node [
    id 189
    label "rokada"
  ]
  node [
    id 190
    label "linia"
  ]
  node [
    id 191
    label "pole_bitwy"
  ]
  node [
    id 192
    label "sfera"
  ]
  node [
    id 193
    label "zaleganie"
  ]
  node [
    id 194
    label "zjednoczenie"
  ]
  node [
    id 195
    label "przedpole"
  ]
  node [
    id 196
    label "prz&#243;d"
  ]
  node [
    id 197
    label "budynek"
  ]
  node [
    id 198
    label "szczyt"
  ]
  node [
    id 199
    label "zalega&#263;"
  ]
  node [
    id 200
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 201
    label "zjawisko"
  ]
  node [
    id 202
    label "pomieszczenie"
  ]
  node [
    id 203
    label "elewacja"
  ]
  node [
    id 204
    label "stowarzyszenie"
  ]
  node [
    id 205
    label "refresher_course"
  ]
  node [
    id 206
    label "oczyszczenie"
  ]
  node [
    id 207
    label "wymienienie"
  ]
  node [
    id 208
    label "vaporization"
  ]
  node [
    id 209
    label "potraktowanie"
  ]
  node [
    id 210
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 211
    label "ventilation"
  ]
  node [
    id 212
    label "zast&#281;powanie"
  ]
  node [
    id 213
    label "przewietrzanie_si&#281;"
  ]
  node [
    id 214
    label "aeration"
  ]
  node [
    id 215
    label "traktowanie"
  ]
  node [
    id 216
    label "czyszczenie"
  ]
  node [
    id 217
    label "oddychanie"
  ]
  node [
    id 218
    label "wydalanie"
  ]
  node [
    id 219
    label "pneumatycznie"
  ]
  node [
    id 220
    label "pneumatic"
  ]
  node [
    id 221
    label "publicize"
  ]
  node [
    id 222
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 223
    label "podda&#263;"
  ]
  node [
    id 224
    label "zmieni&#263;"
  ]
  node [
    id 225
    label "vent"
  ]
  node [
    id 226
    label "czy&#347;ci&#263;"
  ]
  node [
    id 227
    label "air"
  ]
  node [
    id 228
    label "zmienia&#263;"
  ]
  node [
    id 229
    label "poddawa&#263;"
  ]
  node [
    id 230
    label "update"
  ]
  node [
    id 231
    label "wia&#263;"
  ]
  node [
    id 232
    label "bra&#263;"
  ]
  node [
    id 233
    label "blow"
  ]
  node [
    id 234
    label "rozdyma&#263;"
  ]
  node [
    id 235
    label "rycze&#263;"
  ]
  node [
    id 236
    label "wypuszcza&#263;"
  ]
  node [
    id 237
    label "inflate"
  ]
  node [
    id 238
    label "pump"
  ]
  node [
    id 239
    label "gra&#263;"
  ]
  node [
    id 240
    label "wype&#322;nia&#263;"
  ]
  node [
    id 241
    label "odholowa&#263;"
  ]
  node [
    id 242
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 243
    label "tabor"
  ]
  node [
    id 244
    label "przyholowywanie"
  ]
  node [
    id 245
    label "przyholowa&#263;"
  ]
  node [
    id 246
    label "przyholowanie"
  ]
  node [
    id 247
    label "fukni&#281;cie"
  ]
  node [
    id 248
    label "l&#261;d"
  ]
  node [
    id 249
    label "zielona_karta"
  ]
  node [
    id 250
    label "fukanie"
  ]
  node [
    id 251
    label "przyholowywa&#263;"
  ]
  node [
    id 252
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 253
    label "woda"
  ]
  node [
    id 254
    label "przeszklenie"
  ]
  node [
    id 255
    label "test_zderzeniowy"
  ]
  node [
    id 256
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 257
    label "odzywka"
  ]
  node [
    id 258
    label "nadwozie"
  ]
  node [
    id 259
    label "odholowanie"
  ]
  node [
    id 260
    label "prowadzenie_si&#281;"
  ]
  node [
    id 261
    label "odholowywa&#263;"
  ]
  node [
    id 262
    label "pod&#322;oga"
  ]
  node [
    id 263
    label "odholowywanie"
  ]
  node [
    id 264
    label "hamulec"
  ]
  node [
    id 265
    label "podwozie"
  ]
  node [
    id 266
    label "podmucha&#263;"
  ]
  node [
    id 267
    label "zaliczy&#263;"
  ]
  node [
    id 268
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 269
    label "uciec"
  ]
  node [
    id 270
    label "powia&#263;"
  ]
  node [
    id 271
    label "ukra&#347;&#263;"
  ]
  node [
    id 272
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 273
    label "wdmuchanie"
  ]
  node [
    id 274
    label "wianie"
  ]
  node [
    id 275
    label "wypuszczanie"
  ]
  node [
    id 276
    label "uleganie"
  ]
  node [
    id 277
    label "instrument_d&#281;ty"
  ]
  node [
    id 278
    label "branie"
  ]
  node [
    id 279
    label "wydmuchiwanie"
  ]
  node [
    id 280
    label "wype&#322;nianie"
  ]
  node [
    id 281
    label "wdmuchiwanie"
  ]
  node [
    id 282
    label "blowing"
  ]
  node [
    id 283
    label "granie"
  ]
  node [
    id 284
    label "nawdychanie_si&#281;"
  ]
  node [
    id 285
    label "aspiration"
  ]
  node [
    id 286
    label "wci&#261;ganie"
  ]
  node [
    id 287
    label "nasyci&#263;"
  ]
  node [
    id 288
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "cheat"
  ]
  node [
    id 290
    label "wci&#261;ga&#263;"
  ]
  node [
    id 291
    label "styka&#263;_si&#281;"
  ]
  node [
    id 292
    label "wypuszczenie"
  ]
  node [
    id 293
    label "powianie"
  ]
  node [
    id 294
    label "breath"
  ]
  node [
    id 295
    label "zaliczanie"
  ]
  node [
    id 296
    label "whiff"
  ]
  node [
    id 297
    label "ukradzenie"
  ]
  node [
    id 298
    label "uciekni&#281;cie"
  ]
  node [
    id 299
    label "wzi&#281;cie"
  ]
  node [
    id 300
    label "organizacja"
  ]
  node [
    id 301
    label "uk&#322;ad"
  ]
  node [
    id 302
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 303
    label "Unia"
  ]
  node [
    id 304
    label "combination"
  ]
  node [
    id 305
    label "Unia_Europejska"
  ]
  node [
    id 306
    label "union"
  ]
  node [
    id 307
    label "partia"
  ]
  node [
    id 308
    label "podmiot"
  ]
  node [
    id 309
    label "jednostka_organizacyjna"
  ]
  node [
    id 310
    label "struktura"
  ]
  node [
    id 311
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 312
    label "TOPR"
  ]
  node [
    id 313
    label "endecki"
  ]
  node [
    id 314
    label "zesp&#243;&#322;"
  ]
  node [
    id 315
    label "przedstawicielstwo"
  ]
  node [
    id 316
    label "od&#322;am"
  ]
  node [
    id 317
    label "Cepelia"
  ]
  node [
    id 318
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 319
    label "ZBoWiD"
  ]
  node [
    id 320
    label "organization"
  ]
  node [
    id 321
    label "centrala"
  ]
  node [
    id 322
    label "GOPR"
  ]
  node [
    id 323
    label "ZOMO"
  ]
  node [
    id 324
    label "ZMP"
  ]
  node [
    id 325
    label "komitet_koordynacyjny"
  ]
  node [
    id 326
    label "przybud&#243;wka"
  ]
  node [
    id 327
    label "boj&#243;wka"
  ]
  node [
    id 328
    label "Bund"
  ]
  node [
    id 329
    label "PPR"
  ]
  node [
    id 330
    label "wybranek"
  ]
  node [
    id 331
    label "Jakobici"
  ]
  node [
    id 332
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 333
    label "SLD"
  ]
  node [
    id 334
    label "Razem"
  ]
  node [
    id 335
    label "PiS"
  ]
  node [
    id 336
    label "package"
  ]
  node [
    id 337
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 338
    label "Kuomintang"
  ]
  node [
    id 339
    label "ZSL"
  ]
  node [
    id 340
    label "AWS"
  ]
  node [
    id 341
    label "gra"
  ]
  node [
    id 342
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 343
    label "game"
  ]
  node [
    id 344
    label "blok"
  ]
  node [
    id 345
    label "materia&#322;"
  ]
  node [
    id 346
    label "PO"
  ]
  node [
    id 347
    label "si&#322;a"
  ]
  node [
    id 348
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 349
    label "niedoczas"
  ]
  node [
    id 350
    label "Federali&#347;ci"
  ]
  node [
    id 351
    label "PSL"
  ]
  node [
    id 352
    label "Wigowie"
  ]
  node [
    id 353
    label "ZChN"
  ]
  node [
    id 354
    label "egzekutywa"
  ]
  node [
    id 355
    label "aktyw"
  ]
  node [
    id 356
    label "wybranka"
  ]
  node [
    id 357
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 358
    label "unit"
  ]
  node [
    id 359
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 360
    label "rozprz&#261;c"
  ]
  node [
    id 361
    label "treaty"
  ]
  node [
    id 362
    label "systemat"
  ]
  node [
    id 363
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 364
    label "system"
  ]
  node [
    id 365
    label "umowa"
  ]
  node [
    id 366
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 367
    label "usenet"
  ]
  node [
    id 368
    label "przestawi&#263;"
  ]
  node [
    id 369
    label "alliance"
  ]
  node [
    id 370
    label "ONZ"
  ]
  node [
    id 371
    label "NATO"
  ]
  node [
    id 372
    label "konstelacja"
  ]
  node [
    id 373
    label "o&#347;"
  ]
  node [
    id 374
    label "podsystem"
  ]
  node [
    id 375
    label "zawarcie"
  ]
  node [
    id 376
    label "zawrze&#263;"
  ]
  node [
    id 377
    label "organ"
  ]
  node [
    id 378
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 379
    label "wi&#281;&#378;"
  ]
  node [
    id 380
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 381
    label "zachowanie"
  ]
  node [
    id 382
    label "cybernetyk"
  ]
  node [
    id 383
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 384
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 385
    label "traktat_wersalski"
  ]
  node [
    id 386
    label "cia&#322;o"
  ]
  node [
    id 387
    label "eurosceptycyzm"
  ]
  node [
    id 388
    label "euroentuzjasta"
  ]
  node [
    id 389
    label "euroentuzjazm"
  ]
  node [
    id 390
    label "euroko&#322;choz"
  ]
  node [
    id 391
    label "strefa_euro"
  ]
  node [
    id 392
    label "eurorealizm"
  ]
  node [
    id 393
    label "Bruksela"
  ]
  node [
    id 394
    label "Eurogrupa"
  ]
  node [
    id 395
    label "eurorealista"
  ]
  node [
    id 396
    label "eurosceptyczny"
  ]
  node [
    id 397
    label "eurosceptyk"
  ]
  node [
    id 398
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 399
    label "prawo_unijne"
  ]
  node [
    id 400
    label "Fundusze_Unijne"
  ]
  node [
    id 401
    label "p&#322;atnik_netto"
  ]
  node [
    id 402
    label "po_europejsku"
  ]
  node [
    id 403
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 404
    label "European"
  ]
  node [
    id 405
    label "typowy"
  ]
  node [
    id 406
    label "charakterystyczny"
  ]
  node [
    id 407
    label "europejsko"
  ]
  node [
    id 408
    label "charakterystycznie"
  ]
  node [
    id 409
    label "szczeg&#243;lny"
  ]
  node [
    id 410
    label "wyj&#261;tkowy"
  ]
  node [
    id 411
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 412
    label "podobny"
  ]
  node [
    id 413
    label "zwyczajny"
  ]
  node [
    id 414
    label "typowo"
  ]
  node [
    id 415
    label "cz&#281;sty"
  ]
  node [
    id 416
    label "zwyk&#322;y"
  ]
  node [
    id 417
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 418
    label "nale&#380;ny"
  ]
  node [
    id 419
    label "nale&#380;yty"
  ]
  node [
    id 420
    label "uprawniony"
  ]
  node [
    id 421
    label "zasadniczy"
  ]
  node [
    id 422
    label "stosownie"
  ]
  node [
    id 423
    label "taki"
  ]
  node [
    id 424
    label "prawdziwy"
  ]
  node [
    id 425
    label "ten"
  ]
  node [
    id 426
    label "dobry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
]
