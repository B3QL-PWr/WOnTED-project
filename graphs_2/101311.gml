graph [
  node [
    id 0
    label "catalogue"
    origin "text"
  ]
  node [
    id 1
    label "star"
    origin "text"
  ]
  node [
    id 2
    label "clusters"
    origin "text"
  ]
  node [
    id 3
    label "anda"
    origin "text"
  ]
  node [
    id 4
    label "associations"
    origin "text"
  ]
  node [
    id 5
    label "Catalogue"
  ]
  node [
    id 6
    label "of"
  ]
  node [
    id 7
    label "Clusters"
  ]
  node [
    id 8
    label "Anda"
  ]
  node [
    id 9
    label "Associations"
  ]
  node [
    id 10
    label "katalog"
  ]
  node [
    id 11
    label "gromada"
  ]
  node [
    id 12
    label "gwiazda"
  ]
  node [
    id 13
    label "oraz"
  ]
  node [
    id 14
    label "asocjacja"
  ]
  node [
    id 15
    label "Georga"
  ]
  node [
    id 16
    label "Altera"
  ]
  node [
    id 17
    label "verte"
  ]
  node [
    id 18
    label "Van&#253;ska"
  ]
  node [
    id 19
    label "J"
  ]
  node [
    id 20
    label "Ruprecht"
  ]
  node [
    id 21
    label "bela"
  ]
  node [
    id 22
    label "Balazs"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
]
