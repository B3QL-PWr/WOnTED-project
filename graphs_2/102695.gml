graph [
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "dobry"
    origin "text"
  ]
  node [
    id 3
    label "xxx"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 5
    label "drzewo_owocowe"
  ]
  node [
    id 6
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 7
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 8
    label "skuteczny"
  ]
  node [
    id 9
    label "ca&#322;y"
  ]
  node [
    id 10
    label "czw&#243;rka"
  ]
  node [
    id 11
    label "spokojny"
  ]
  node [
    id 12
    label "pos&#322;uszny"
  ]
  node [
    id 13
    label "korzystny"
  ]
  node [
    id 14
    label "drogi"
  ]
  node [
    id 15
    label "pozytywny"
  ]
  node [
    id 16
    label "moralny"
  ]
  node [
    id 17
    label "pomy&#347;lny"
  ]
  node [
    id 18
    label "powitanie"
  ]
  node [
    id 19
    label "grzeczny"
  ]
  node [
    id 20
    label "&#347;mieszny"
  ]
  node [
    id 21
    label "odpowiedni"
  ]
  node [
    id 22
    label "zwrot"
  ]
  node [
    id 23
    label "dobrze"
  ]
  node [
    id 24
    label "dobroczynny"
  ]
  node [
    id 25
    label "mi&#322;y"
  ]
  node [
    id 26
    label "etycznie"
  ]
  node [
    id 27
    label "moralnie"
  ]
  node [
    id 28
    label "warto&#347;ciowy"
  ]
  node [
    id 29
    label "taki"
  ]
  node [
    id 30
    label "stosownie"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 32
    label "prawdziwy"
  ]
  node [
    id 33
    label "typowy"
  ]
  node [
    id 34
    label "zasadniczy"
  ]
  node [
    id 35
    label "charakterystyczny"
  ]
  node [
    id 36
    label "uprawniony"
  ]
  node [
    id 37
    label "nale&#380;yty"
  ]
  node [
    id 38
    label "ten"
  ]
  node [
    id 39
    label "nale&#380;ny"
  ]
  node [
    id 40
    label "pozytywnie"
  ]
  node [
    id 41
    label "fajny"
  ]
  node [
    id 42
    label "przyjemny"
  ]
  node [
    id 43
    label "po&#380;&#261;dany"
  ]
  node [
    id 44
    label "dodatnio"
  ]
  node [
    id 45
    label "o&#347;mieszenie"
  ]
  node [
    id 46
    label "o&#347;mieszanie"
  ]
  node [
    id 47
    label "&#347;miesznie"
  ]
  node [
    id 48
    label "nieadekwatny"
  ]
  node [
    id 49
    label "bawny"
  ]
  node [
    id 50
    label "niepowa&#380;ny"
  ]
  node [
    id 51
    label "dziwny"
  ]
  node [
    id 52
    label "uspokojenie_si&#281;"
  ]
  node [
    id 53
    label "wolny"
  ]
  node [
    id 54
    label "bezproblemowy"
  ]
  node [
    id 55
    label "uspokajanie_si&#281;"
  ]
  node [
    id 56
    label "spokojnie"
  ]
  node [
    id 57
    label "uspokojenie"
  ]
  node [
    id 58
    label "nietrudny"
  ]
  node [
    id 59
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 60
    label "cicho"
  ]
  node [
    id 61
    label "uspokajanie"
  ]
  node [
    id 62
    label "pos&#322;usznie"
  ]
  node [
    id 63
    label "zale&#380;ny"
  ]
  node [
    id 64
    label "uleg&#322;y"
  ]
  node [
    id 65
    label "konserwatywny"
  ]
  node [
    id 66
    label "stosowny"
  ]
  node [
    id 67
    label "grzecznie"
  ]
  node [
    id 68
    label "nijaki"
  ]
  node [
    id 69
    label "niewinny"
  ]
  node [
    id 70
    label "korzystnie"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "przyjaciel"
  ]
  node [
    id 73
    label "bliski"
  ]
  node [
    id 74
    label "drogo"
  ]
  node [
    id 75
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 76
    label "kompletny"
  ]
  node [
    id 77
    label "zdr&#243;w"
  ]
  node [
    id 78
    label "ca&#322;o"
  ]
  node [
    id 79
    label "du&#380;y"
  ]
  node [
    id 80
    label "calu&#347;ko"
  ]
  node [
    id 81
    label "podobny"
  ]
  node [
    id 82
    label "&#380;ywy"
  ]
  node [
    id 83
    label "pe&#322;ny"
  ]
  node [
    id 84
    label "jedyny"
  ]
  node [
    id 85
    label "sprawny"
  ]
  node [
    id 86
    label "skutkowanie"
  ]
  node [
    id 87
    label "poskutkowanie"
  ]
  node [
    id 88
    label "skutecznie"
  ]
  node [
    id 89
    label "pomy&#347;lnie"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "przedtrzonowiec"
  ]
  node [
    id 92
    label "trafienie"
  ]
  node [
    id 93
    label "osada"
  ]
  node [
    id 94
    label "blotka"
  ]
  node [
    id 95
    label "p&#322;yta_winylowa"
  ]
  node [
    id 96
    label "cyfra"
  ]
  node [
    id 97
    label "pok&#243;j"
  ]
  node [
    id 98
    label "obiekt"
  ]
  node [
    id 99
    label "stopie&#324;"
  ]
  node [
    id 100
    label "arkusz_drukarski"
  ]
  node [
    id 101
    label "zaprz&#281;g"
  ]
  node [
    id 102
    label "toto-lotek"
  ]
  node [
    id 103
    label "&#263;wiartka"
  ]
  node [
    id 104
    label "&#322;&#243;dka"
  ]
  node [
    id 105
    label "four"
  ]
  node [
    id 106
    label "minialbum"
  ]
  node [
    id 107
    label "hotel"
  ]
  node [
    id 108
    label "punkt"
  ]
  node [
    id 109
    label "zmiana"
  ]
  node [
    id 110
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 111
    label "turn"
  ]
  node [
    id 112
    label "wyra&#380;enie"
  ]
  node [
    id 113
    label "fraza_czasownikowa"
  ]
  node [
    id 114
    label "turning"
  ]
  node [
    id 115
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 116
    label "skr&#281;t"
  ]
  node [
    id 117
    label "jednostka_leksykalna"
  ]
  node [
    id 118
    label "obr&#243;t"
  ]
  node [
    id 119
    label "spotkanie"
  ]
  node [
    id 120
    label "pozdrowienie"
  ]
  node [
    id 121
    label "welcome"
  ]
  node [
    id 122
    label "zwyczaj"
  ]
  node [
    id 123
    label "greeting"
  ]
  node [
    id 124
    label "zdarzony"
  ]
  node [
    id 125
    label "odpowiednio"
  ]
  node [
    id 126
    label "specjalny"
  ]
  node [
    id 127
    label "odpowiadanie"
  ]
  node [
    id 128
    label "wybranek"
  ]
  node [
    id 129
    label "sk&#322;onny"
  ]
  node [
    id 130
    label "kochanek"
  ]
  node [
    id 131
    label "mi&#322;o"
  ]
  node [
    id 132
    label "dyplomata"
  ]
  node [
    id 133
    label "umi&#322;owany"
  ]
  node [
    id 134
    label "kochanie"
  ]
  node [
    id 135
    label "przyjemnie"
  ]
  node [
    id 136
    label "wiele"
  ]
  node [
    id 137
    label "lepiej"
  ]
  node [
    id 138
    label "dobroczynnie"
  ]
  node [
    id 139
    label "spo&#322;eczny"
  ]
  node [
    id 140
    label "wybiera&#263;"
  ]
  node [
    id 141
    label "lubi&#263;"
  ]
  node [
    id 142
    label "continue"
  ]
  node [
    id 143
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 144
    label "odtwarza&#263;"
  ]
  node [
    id 145
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 146
    label "odbiera&#263;"
  ]
  node [
    id 147
    label "radio"
  ]
  node [
    id 148
    label "zlecenie"
  ]
  node [
    id 149
    label "zabiera&#263;"
  ]
  node [
    id 150
    label "przyjmowa&#263;"
  ]
  node [
    id 151
    label "doznawa&#263;"
  ]
  node [
    id 152
    label "pozbawia&#263;"
  ]
  node [
    id 153
    label "telewizor"
  ]
  node [
    id 154
    label "fall"
  ]
  node [
    id 155
    label "antena"
  ]
  node [
    id 156
    label "bra&#263;"
  ]
  node [
    id 157
    label "odzyskiwa&#263;"
  ]
  node [
    id 158
    label "deprive"
  ]
  node [
    id 159
    label "liszy&#263;"
  ]
  node [
    id 160
    label "konfiskowa&#263;"
  ]
  node [
    id 161
    label "accept"
  ]
  node [
    id 162
    label "love"
  ]
  node [
    id 163
    label "Facebook"
  ]
  node [
    id 164
    label "chowa&#263;"
  ]
  node [
    id 165
    label "aprobowa&#263;"
  ]
  node [
    id 166
    label "mie&#263;_do_siebie"
  ]
  node [
    id 167
    label "corroborate"
  ]
  node [
    id 168
    label "czu&#263;"
  ]
  node [
    id 169
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 170
    label "wyjmowa&#263;"
  ]
  node [
    id 171
    label "take"
  ]
  node [
    id 172
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 173
    label "sie&#263;_rybacka"
  ]
  node [
    id 174
    label "ustala&#263;"
  ]
  node [
    id 175
    label "kotwica"
  ]
  node [
    id 176
    label "wzmacnia&#263;"
  ]
  node [
    id 177
    label "exert"
  ]
  node [
    id 178
    label "odbudowywa&#263;"
  ]
  node [
    id 179
    label "robi&#263;"
  ]
  node [
    id 180
    label "puszcza&#263;"
  ]
  node [
    id 181
    label "okre&#347;la&#263;"
  ]
  node [
    id 182
    label "kopiowa&#263;"
  ]
  node [
    id 183
    label "czerpa&#263;"
  ]
  node [
    id 184
    label "post&#281;powa&#263;"
  ]
  node [
    id 185
    label "przedstawia&#263;"
  ]
  node [
    id 186
    label "mock"
  ]
  node [
    id 187
    label "dally"
  ]
  node [
    id 188
    label "impart"
  ]
  node [
    id 189
    label "play"
  ]
  node [
    id 190
    label "przywraca&#263;"
  ]
  node [
    id 191
    label "cover"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
]
