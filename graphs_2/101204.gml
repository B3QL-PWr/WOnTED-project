graph [
  node [
    id 0
    label "puchar"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 2
    label "wio&#347;larstwo"
    origin "text"
  ]
  node [
    id 3
    label "naczynie"
  ]
  node [
    id 4
    label "nagroda"
  ]
  node [
    id 5
    label "zwyci&#281;stwo"
  ]
  node [
    id 6
    label "zawody"
  ]
  node [
    id 7
    label "zawarto&#347;&#263;"
  ]
  node [
    id 8
    label "impreza"
  ]
  node [
    id 9
    label "contest"
  ]
  node [
    id 10
    label "walczy&#263;"
  ]
  node [
    id 11
    label "rywalizacja"
  ]
  node [
    id 12
    label "walczenie"
  ]
  node [
    id 13
    label "tysi&#281;cznik"
  ]
  node [
    id 14
    label "champion"
  ]
  node [
    id 15
    label "spadochroniarstwo"
  ]
  node [
    id 16
    label "kategoria_open"
  ]
  node [
    id 17
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 18
    label "vessel"
  ]
  node [
    id 19
    label "sprz&#281;t"
  ]
  node [
    id 20
    label "statki"
  ]
  node [
    id 21
    label "rewaskularyzacja"
  ]
  node [
    id 22
    label "ceramika"
  ]
  node [
    id 23
    label "drewno"
  ]
  node [
    id 24
    label "przew&#243;d"
  ]
  node [
    id 25
    label "unaczyni&#263;"
  ]
  node [
    id 26
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 27
    label "receptacle"
  ]
  node [
    id 28
    label "oskar"
  ]
  node [
    id 29
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 30
    label "return"
  ]
  node [
    id 31
    label "konsekwencja"
  ]
  node [
    id 32
    label "temat"
  ]
  node [
    id 33
    label "ilo&#347;&#263;"
  ]
  node [
    id 34
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 35
    label "wn&#281;trze"
  ]
  node [
    id 36
    label "informacja"
  ]
  node [
    id 37
    label "beat"
  ]
  node [
    id 38
    label "poradzenie_sobie"
  ]
  node [
    id 39
    label "sukces"
  ]
  node [
    id 40
    label "conquest"
  ]
  node [
    id 41
    label "Stary_&#346;wiat"
  ]
  node [
    id 42
    label "asymilowanie_si&#281;"
  ]
  node [
    id 43
    label "p&#243;&#322;noc"
  ]
  node [
    id 44
    label "przedmiot"
  ]
  node [
    id 45
    label "Wsch&#243;d"
  ]
  node [
    id 46
    label "class"
  ]
  node [
    id 47
    label "geosfera"
  ]
  node [
    id 48
    label "obiekt_naturalny"
  ]
  node [
    id 49
    label "przejmowanie"
  ]
  node [
    id 50
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 51
    label "przyroda"
  ]
  node [
    id 52
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 53
    label "po&#322;udnie"
  ]
  node [
    id 54
    label "zjawisko"
  ]
  node [
    id 55
    label "rzecz"
  ]
  node [
    id 56
    label "makrokosmos"
  ]
  node [
    id 57
    label "huczek"
  ]
  node [
    id 58
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 59
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 60
    label "environment"
  ]
  node [
    id 61
    label "morze"
  ]
  node [
    id 62
    label "rze&#378;ba"
  ]
  node [
    id 63
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 64
    label "przejmowa&#263;"
  ]
  node [
    id 65
    label "hydrosfera"
  ]
  node [
    id 66
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 67
    label "ciemna_materia"
  ]
  node [
    id 68
    label "ekosystem"
  ]
  node [
    id 69
    label "biota"
  ]
  node [
    id 70
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 71
    label "planeta"
  ]
  node [
    id 72
    label "geotermia"
  ]
  node [
    id 73
    label "ekosfera"
  ]
  node [
    id 74
    label "ozonosfera"
  ]
  node [
    id 75
    label "wszechstworzenie"
  ]
  node [
    id 76
    label "grupa"
  ]
  node [
    id 77
    label "woda"
  ]
  node [
    id 78
    label "kuchnia"
  ]
  node [
    id 79
    label "biosfera"
  ]
  node [
    id 80
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 81
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 82
    label "populace"
  ]
  node [
    id 83
    label "magnetosfera"
  ]
  node [
    id 84
    label "Nowy_&#346;wiat"
  ]
  node [
    id 85
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 86
    label "universe"
  ]
  node [
    id 87
    label "biegun"
  ]
  node [
    id 88
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 89
    label "litosfera"
  ]
  node [
    id 90
    label "teren"
  ]
  node [
    id 91
    label "mikrokosmos"
  ]
  node [
    id 92
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 93
    label "przestrze&#324;"
  ]
  node [
    id 94
    label "stw&#243;r"
  ]
  node [
    id 95
    label "p&#243;&#322;kula"
  ]
  node [
    id 96
    label "przej&#281;cie"
  ]
  node [
    id 97
    label "barysfera"
  ]
  node [
    id 98
    label "obszar"
  ]
  node [
    id 99
    label "czarna_dziura"
  ]
  node [
    id 100
    label "atmosfera"
  ]
  node [
    id 101
    label "przej&#261;&#263;"
  ]
  node [
    id 102
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 103
    label "Ziemia"
  ]
  node [
    id 104
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 105
    label "geoida"
  ]
  node [
    id 106
    label "zagranica"
  ]
  node [
    id 107
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 108
    label "fauna"
  ]
  node [
    id 109
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 110
    label "odm&#322;adzanie"
  ]
  node [
    id 111
    label "liga"
  ]
  node [
    id 112
    label "jednostka_systematyczna"
  ]
  node [
    id 113
    label "asymilowanie"
  ]
  node [
    id 114
    label "gromada"
  ]
  node [
    id 115
    label "asymilowa&#263;"
  ]
  node [
    id 116
    label "egzemplarz"
  ]
  node [
    id 117
    label "Entuzjastki"
  ]
  node [
    id 118
    label "zbi&#243;r"
  ]
  node [
    id 119
    label "kompozycja"
  ]
  node [
    id 120
    label "Terranie"
  ]
  node [
    id 121
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 122
    label "category"
  ]
  node [
    id 123
    label "pakiet_klimatyczny"
  ]
  node [
    id 124
    label "oddzia&#322;"
  ]
  node [
    id 125
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 126
    label "cz&#261;steczka"
  ]
  node [
    id 127
    label "stage_set"
  ]
  node [
    id 128
    label "type"
  ]
  node [
    id 129
    label "specgrupa"
  ]
  node [
    id 130
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 131
    label "&#346;wietliki"
  ]
  node [
    id 132
    label "odm&#322;odzenie"
  ]
  node [
    id 133
    label "Eurogrupa"
  ]
  node [
    id 134
    label "odm&#322;adza&#263;"
  ]
  node [
    id 135
    label "formacja_geologiczna"
  ]
  node [
    id 136
    label "harcerze_starsi"
  ]
  node [
    id 137
    label "Kosowo"
  ]
  node [
    id 138
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 139
    label "Zab&#322;ocie"
  ]
  node [
    id 140
    label "zach&#243;d"
  ]
  node [
    id 141
    label "Pow&#261;zki"
  ]
  node [
    id 142
    label "Piotrowo"
  ]
  node [
    id 143
    label "Olszanica"
  ]
  node [
    id 144
    label "holarktyka"
  ]
  node [
    id 145
    label "Ruda_Pabianicka"
  ]
  node [
    id 146
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 147
    label "Ludwin&#243;w"
  ]
  node [
    id 148
    label "Arktyka"
  ]
  node [
    id 149
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 150
    label "Zabu&#380;e"
  ]
  node [
    id 151
    label "miejsce"
  ]
  node [
    id 152
    label "antroposfera"
  ]
  node [
    id 153
    label "terytorium"
  ]
  node [
    id 154
    label "Neogea"
  ]
  node [
    id 155
    label "Syberia_Zachodnia"
  ]
  node [
    id 156
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 157
    label "zakres"
  ]
  node [
    id 158
    label "pas_planetoid"
  ]
  node [
    id 159
    label "Syberia_Wschodnia"
  ]
  node [
    id 160
    label "Antarktyka"
  ]
  node [
    id 161
    label "Rakowice"
  ]
  node [
    id 162
    label "akrecja"
  ]
  node [
    id 163
    label "wymiar"
  ]
  node [
    id 164
    label "&#321;&#281;g"
  ]
  node [
    id 165
    label "Kresy_Zachodnie"
  ]
  node [
    id 166
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 167
    label "wsch&#243;d"
  ]
  node [
    id 168
    label "Notogea"
  ]
  node [
    id 169
    label "integer"
  ]
  node [
    id 170
    label "liczba"
  ]
  node [
    id 171
    label "zlewanie_si&#281;"
  ]
  node [
    id 172
    label "uk&#322;ad"
  ]
  node [
    id 173
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 174
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 175
    label "pe&#322;ny"
  ]
  node [
    id 176
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 177
    label "proces"
  ]
  node [
    id 178
    label "boski"
  ]
  node [
    id 179
    label "krajobraz"
  ]
  node [
    id 180
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 181
    label "przywidzenie"
  ]
  node [
    id 182
    label "presence"
  ]
  node [
    id 183
    label "charakter"
  ]
  node [
    id 184
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 185
    label "rozdzielanie"
  ]
  node [
    id 186
    label "bezbrze&#380;e"
  ]
  node [
    id 187
    label "punkt"
  ]
  node [
    id 188
    label "czasoprzestrze&#324;"
  ]
  node [
    id 189
    label "niezmierzony"
  ]
  node [
    id 190
    label "przedzielenie"
  ]
  node [
    id 191
    label "nielito&#347;ciwy"
  ]
  node [
    id 192
    label "rozdziela&#263;"
  ]
  node [
    id 193
    label "oktant"
  ]
  node [
    id 194
    label "przedzieli&#263;"
  ]
  node [
    id 195
    label "przestw&#243;r"
  ]
  node [
    id 196
    label "&#347;rodowisko"
  ]
  node [
    id 197
    label "rura"
  ]
  node [
    id 198
    label "grzebiuszka"
  ]
  node [
    id 199
    label "cz&#322;owiek"
  ]
  node [
    id 200
    label "odbicie"
  ]
  node [
    id 201
    label "atom"
  ]
  node [
    id 202
    label "kosmos"
  ]
  node [
    id 203
    label "miniatura"
  ]
  node [
    id 204
    label "smok_wawelski"
  ]
  node [
    id 205
    label "niecz&#322;owiek"
  ]
  node [
    id 206
    label "monster"
  ]
  node [
    id 207
    label "istota_&#380;ywa"
  ]
  node [
    id 208
    label "potw&#243;r"
  ]
  node [
    id 209
    label "istota_fantastyczna"
  ]
  node [
    id 210
    label "kultura"
  ]
  node [
    id 211
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 212
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 213
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 214
    label "aspekt"
  ]
  node [
    id 215
    label "troposfera"
  ]
  node [
    id 216
    label "klimat"
  ]
  node [
    id 217
    label "metasfera"
  ]
  node [
    id 218
    label "atmosferyki"
  ]
  node [
    id 219
    label "homosfera"
  ]
  node [
    id 220
    label "cecha"
  ]
  node [
    id 221
    label "powietrznia"
  ]
  node [
    id 222
    label "jonosfera"
  ]
  node [
    id 223
    label "termosfera"
  ]
  node [
    id 224
    label "egzosfera"
  ]
  node [
    id 225
    label "heterosfera"
  ]
  node [
    id 226
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 227
    label "tropopauza"
  ]
  node [
    id 228
    label "kwas"
  ]
  node [
    id 229
    label "powietrze"
  ]
  node [
    id 230
    label "stratosfera"
  ]
  node [
    id 231
    label "pow&#322;oka"
  ]
  node [
    id 232
    label "mezosfera"
  ]
  node [
    id 233
    label "mezopauza"
  ]
  node [
    id 234
    label "atmosphere"
  ]
  node [
    id 235
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 236
    label "ciep&#322;o"
  ]
  node [
    id 237
    label "energia_termiczna"
  ]
  node [
    id 238
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 239
    label "sferoida"
  ]
  node [
    id 240
    label "object"
  ]
  node [
    id 241
    label "wpadni&#281;cie"
  ]
  node [
    id 242
    label "mienie"
  ]
  node [
    id 243
    label "istota"
  ]
  node [
    id 244
    label "obiekt"
  ]
  node [
    id 245
    label "wpa&#347;&#263;"
  ]
  node [
    id 246
    label "wpadanie"
  ]
  node [
    id 247
    label "wpada&#263;"
  ]
  node [
    id 248
    label "wra&#380;enie"
  ]
  node [
    id 249
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 250
    label "interception"
  ]
  node [
    id 251
    label "wzbudzenie"
  ]
  node [
    id 252
    label "emotion"
  ]
  node [
    id 253
    label "movement"
  ]
  node [
    id 254
    label "zaczerpni&#281;cie"
  ]
  node [
    id 255
    label "wzi&#281;cie"
  ]
  node [
    id 256
    label "bang"
  ]
  node [
    id 257
    label "wzi&#261;&#263;"
  ]
  node [
    id 258
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 259
    label "stimulate"
  ]
  node [
    id 260
    label "ogarn&#261;&#263;"
  ]
  node [
    id 261
    label "wzbudzi&#263;"
  ]
  node [
    id 262
    label "thrill"
  ]
  node [
    id 263
    label "treat"
  ]
  node [
    id 264
    label "czerpa&#263;"
  ]
  node [
    id 265
    label "bra&#263;"
  ]
  node [
    id 266
    label "go"
  ]
  node [
    id 267
    label "handle"
  ]
  node [
    id 268
    label "wzbudza&#263;"
  ]
  node [
    id 269
    label "ogarnia&#263;"
  ]
  node [
    id 270
    label "czerpanie"
  ]
  node [
    id 271
    label "acquisition"
  ]
  node [
    id 272
    label "branie"
  ]
  node [
    id 273
    label "caparison"
  ]
  node [
    id 274
    label "wzbudzanie"
  ]
  node [
    id 275
    label "czynno&#347;&#263;"
  ]
  node [
    id 276
    label "ogarnianie"
  ]
  node [
    id 277
    label "zboczenie"
  ]
  node [
    id 278
    label "om&#243;wienie"
  ]
  node [
    id 279
    label "sponiewieranie"
  ]
  node [
    id 280
    label "discipline"
  ]
  node [
    id 281
    label "omawia&#263;"
  ]
  node [
    id 282
    label "kr&#261;&#380;enie"
  ]
  node [
    id 283
    label "tre&#347;&#263;"
  ]
  node [
    id 284
    label "robienie"
  ]
  node [
    id 285
    label "sponiewiera&#263;"
  ]
  node [
    id 286
    label "element"
  ]
  node [
    id 287
    label "entity"
  ]
  node [
    id 288
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 289
    label "tematyka"
  ]
  node [
    id 290
    label "w&#261;tek"
  ]
  node [
    id 291
    label "zbaczanie"
  ]
  node [
    id 292
    label "program_nauczania"
  ]
  node [
    id 293
    label "om&#243;wi&#263;"
  ]
  node [
    id 294
    label "omawianie"
  ]
  node [
    id 295
    label "thing"
  ]
  node [
    id 296
    label "zbacza&#263;"
  ]
  node [
    id 297
    label "zboczy&#263;"
  ]
  node [
    id 298
    label "performance"
  ]
  node [
    id 299
    label "sztuka"
  ]
  node [
    id 300
    label "granica_pa&#324;stwa"
  ]
  node [
    id 301
    label "Boreasz"
  ]
  node [
    id 302
    label "noc"
  ]
  node [
    id 303
    label "p&#243;&#322;nocek"
  ]
  node [
    id 304
    label "strona_&#347;wiata"
  ]
  node [
    id 305
    label "godzina"
  ]
  node [
    id 306
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 307
    label "&#347;rodek"
  ]
  node [
    id 308
    label "dzie&#324;"
  ]
  node [
    id 309
    label "dwunasta"
  ]
  node [
    id 310
    label "pora"
  ]
  node [
    id 311
    label "brzeg"
  ]
  node [
    id 312
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 313
    label "p&#322;oza"
  ]
  node [
    id 314
    label "zawiasy"
  ]
  node [
    id 315
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 316
    label "organ"
  ]
  node [
    id 317
    label "element_anatomiczny"
  ]
  node [
    id 318
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 319
    label "reda"
  ]
  node [
    id 320
    label "zbiornik_wodny"
  ]
  node [
    id 321
    label "przymorze"
  ]
  node [
    id 322
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 323
    label "bezmiar"
  ]
  node [
    id 324
    label "pe&#322;ne_morze"
  ]
  node [
    id 325
    label "latarnia_morska"
  ]
  node [
    id 326
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 327
    label "nereida"
  ]
  node [
    id 328
    label "okeanida"
  ]
  node [
    id 329
    label "marina"
  ]
  node [
    id 330
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 331
    label "Morze_Czerwone"
  ]
  node [
    id 332
    label "talasoterapia"
  ]
  node [
    id 333
    label "Morze_Bia&#322;e"
  ]
  node [
    id 334
    label "paliszcze"
  ]
  node [
    id 335
    label "Neptun"
  ]
  node [
    id 336
    label "Morze_Czarne"
  ]
  node [
    id 337
    label "laguna"
  ]
  node [
    id 338
    label "Morze_Egejskie"
  ]
  node [
    id 339
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 340
    label "Morze_Adriatyckie"
  ]
  node [
    id 341
    label "rze&#378;biarstwo"
  ]
  node [
    id 342
    label "planacja"
  ]
  node [
    id 343
    label "relief"
  ]
  node [
    id 344
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 345
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 346
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 347
    label "bozzetto"
  ]
  node [
    id 348
    label "plastyka"
  ]
  node [
    id 349
    label "sfera"
  ]
  node [
    id 350
    label "gleba"
  ]
  node [
    id 351
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 352
    label "warstwa"
  ]
  node [
    id 353
    label "sialma"
  ]
  node [
    id 354
    label "skorupa_ziemska"
  ]
  node [
    id 355
    label "warstwa_perydotytowa"
  ]
  node [
    id 356
    label "warstwa_granitowa"
  ]
  node [
    id 357
    label "kriosfera"
  ]
  node [
    id 358
    label "j&#261;dro"
  ]
  node [
    id 359
    label "lej_polarny"
  ]
  node [
    id 360
    label "kula"
  ]
  node [
    id 361
    label "kresom&#243;zgowie"
  ]
  node [
    id 362
    label "ozon"
  ]
  node [
    id 363
    label "przyra"
  ]
  node [
    id 364
    label "kontekst"
  ]
  node [
    id 365
    label "miejsce_pracy"
  ]
  node [
    id 366
    label "nation"
  ]
  node [
    id 367
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 368
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 369
    label "w&#322;adza"
  ]
  node [
    id 370
    label "iglak"
  ]
  node [
    id 371
    label "cyprysowate"
  ]
  node [
    id 372
    label "biom"
  ]
  node [
    id 373
    label "szata_ro&#347;linna"
  ]
  node [
    id 374
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 375
    label "formacja_ro&#347;linna"
  ]
  node [
    id 376
    label "zielono&#347;&#263;"
  ]
  node [
    id 377
    label "pi&#281;tro"
  ]
  node [
    id 378
    label "plant"
  ]
  node [
    id 379
    label "ro&#347;lina"
  ]
  node [
    id 380
    label "geosystem"
  ]
  node [
    id 381
    label "dotleni&#263;"
  ]
  node [
    id 382
    label "spi&#281;trza&#263;"
  ]
  node [
    id 383
    label "spi&#281;trzenie"
  ]
  node [
    id 384
    label "utylizator"
  ]
  node [
    id 385
    label "p&#322;ycizna"
  ]
  node [
    id 386
    label "nabranie"
  ]
  node [
    id 387
    label "Waruna"
  ]
  node [
    id 388
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 389
    label "przybieranie"
  ]
  node [
    id 390
    label "uci&#261;g"
  ]
  node [
    id 391
    label "bombast"
  ]
  node [
    id 392
    label "fala"
  ]
  node [
    id 393
    label "kryptodepresja"
  ]
  node [
    id 394
    label "water"
  ]
  node [
    id 395
    label "wysi&#281;k"
  ]
  node [
    id 396
    label "pustka"
  ]
  node [
    id 397
    label "ciecz"
  ]
  node [
    id 398
    label "przybrze&#380;e"
  ]
  node [
    id 399
    label "nap&#243;j"
  ]
  node [
    id 400
    label "spi&#281;trzanie"
  ]
  node [
    id 401
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 402
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 403
    label "bicie"
  ]
  node [
    id 404
    label "klarownik"
  ]
  node [
    id 405
    label "chlastanie"
  ]
  node [
    id 406
    label "woda_s&#322;odka"
  ]
  node [
    id 407
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 408
    label "nabra&#263;"
  ]
  node [
    id 409
    label "chlasta&#263;"
  ]
  node [
    id 410
    label "uj&#281;cie_wody"
  ]
  node [
    id 411
    label "zrzut"
  ]
  node [
    id 412
    label "wypowied&#378;"
  ]
  node [
    id 413
    label "wodnik"
  ]
  node [
    id 414
    label "pojazd"
  ]
  node [
    id 415
    label "l&#243;d"
  ]
  node [
    id 416
    label "wybrze&#380;e"
  ]
  node [
    id 417
    label "deklamacja"
  ]
  node [
    id 418
    label "tlenek"
  ]
  node [
    id 419
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 420
    label "biotop"
  ]
  node [
    id 421
    label "biocenoza"
  ]
  node [
    id 422
    label "awifauna"
  ]
  node [
    id 423
    label "ichtiofauna"
  ]
  node [
    id 424
    label "zaj&#281;cie"
  ]
  node [
    id 425
    label "instytucja"
  ]
  node [
    id 426
    label "tajniki"
  ]
  node [
    id 427
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 428
    label "jedzenie"
  ]
  node [
    id 429
    label "zaplecze"
  ]
  node [
    id 430
    label "pomieszczenie"
  ]
  node [
    id 431
    label "zlewozmywak"
  ]
  node [
    id 432
    label "gotowa&#263;"
  ]
  node [
    id 433
    label "Jowisz"
  ]
  node [
    id 434
    label "syzygia"
  ]
  node [
    id 435
    label "Saturn"
  ]
  node [
    id 436
    label "Uran"
  ]
  node [
    id 437
    label "strefa"
  ]
  node [
    id 438
    label "message"
  ]
  node [
    id 439
    label "dar"
  ]
  node [
    id 440
    label "real"
  ]
  node [
    id 441
    label "Ukraina"
  ]
  node [
    id 442
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 443
    label "blok_wschodni"
  ]
  node [
    id 444
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 445
    label "Europa_Wschodnia"
  ]
  node [
    id 446
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 447
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 448
    label "sport_wodny"
  ]
  node [
    id 449
    label "wyspa"
  ]
  node [
    id 450
    label "igrzysko"
  ]
  node [
    id 451
    label "olimpijski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 449
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 450
    target 451
  ]
]
