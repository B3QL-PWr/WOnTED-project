graph [
  node [
    id 0
    label "mie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;os"
    origin "text"
  ]
  node [
    id 3
    label "daleki"
  ]
  node [
    id 4
    label "ruch"
  ]
  node [
    id 5
    label "d&#322;ugo"
  ]
  node [
    id 6
    label "mechanika"
  ]
  node [
    id 7
    label "utrzymywanie"
  ]
  node [
    id 8
    label "move"
  ]
  node [
    id 9
    label "poruszenie"
  ]
  node [
    id 10
    label "movement"
  ]
  node [
    id 11
    label "myk"
  ]
  node [
    id 12
    label "utrzyma&#263;"
  ]
  node [
    id 13
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 14
    label "zjawisko"
  ]
  node [
    id 15
    label "utrzymanie"
  ]
  node [
    id 16
    label "travel"
  ]
  node [
    id 17
    label "kanciasty"
  ]
  node [
    id 18
    label "commercial_enterprise"
  ]
  node [
    id 19
    label "model"
  ]
  node [
    id 20
    label "strumie&#324;"
  ]
  node [
    id 21
    label "proces"
  ]
  node [
    id 22
    label "aktywno&#347;&#263;"
  ]
  node [
    id 23
    label "kr&#243;tki"
  ]
  node [
    id 24
    label "taktyka"
  ]
  node [
    id 25
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 26
    label "apraksja"
  ]
  node [
    id 27
    label "natural_process"
  ]
  node [
    id 28
    label "utrzymywa&#263;"
  ]
  node [
    id 29
    label "wydarzenie"
  ]
  node [
    id 30
    label "dyssypacja_energii"
  ]
  node [
    id 31
    label "tumult"
  ]
  node [
    id 32
    label "stopek"
  ]
  node [
    id 33
    label "czynno&#347;&#263;"
  ]
  node [
    id 34
    label "zmiana"
  ]
  node [
    id 35
    label "manewr"
  ]
  node [
    id 36
    label "lokomocja"
  ]
  node [
    id 37
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 38
    label "komunikacja"
  ]
  node [
    id 39
    label "drift"
  ]
  node [
    id 40
    label "dawny"
  ]
  node [
    id 41
    label "ogl&#281;dny"
  ]
  node [
    id 42
    label "du&#380;y"
  ]
  node [
    id 43
    label "daleko"
  ]
  node [
    id 44
    label "odleg&#322;y"
  ]
  node [
    id 45
    label "zwi&#261;zany"
  ]
  node [
    id 46
    label "r&#243;&#380;ny"
  ]
  node [
    id 47
    label "s&#322;aby"
  ]
  node [
    id 48
    label "odlegle"
  ]
  node [
    id 49
    label "oddalony"
  ]
  node [
    id 50
    label "g&#322;&#281;boki"
  ]
  node [
    id 51
    label "obcy"
  ]
  node [
    id 52
    label "nieobecny"
  ]
  node [
    id 53
    label "przysz&#322;y"
  ]
  node [
    id 54
    label "wytw&#243;r"
  ]
  node [
    id 55
    label "tkanina"
  ]
  node [
    id 56
    label "powierzchnia"
  ]
  node [
    id 57
    label "mieszek_w&#322;osowy"
  ]
  node [
    id 58
    label "cebulka"
  ]
  node [
    id 59
    label "przedmiot"
  ]
  node [
    id 60
    label "p&#322;&#243;d"
  ]
  node [
    id 61
    label "work"
  ]
  node [
    id 62
    label "rezultat"
  ]
  node [
    id 63
    label "rozmiar"
  ]
  node [
    id 64
    label "obszar"
  ]
  node [
    id 65
    label "poj&#281;cie"
  ]
  node [
    id 66
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 67
    label "zwierciad&#322;o"
  ]
  node [
    id 68
    label "capacity"
  ]
  node [
    id 69
    label "plane"
  ]
  node [
    id 70
    label "geofit_cebulowy"
  ]
  node [
    id 71
    label "element_anatomiczny"
  ]
  node [
    id 72
    label "korze&#324;"
  ]
  node [
    id 73
    label "onion"
  ]
  node [
    id 74
    label "maglownia"
  ]
  node [
    id 75
    label "materia&#322;"
  ]
  node [
    id 76
    label "pru&#263;_si&#281;"
  ]
  node [
    id 77
    label "opalarnia"
  ]
  node [
    id 78
    label "prucie_si&#281;"
  ]
  node [
    id 79
    label "apretura"
  ]
  node [
    id 80
    label "splot"
  ]
  node [
    id 81
    label "karbonizowa&#263;"
  ]
  node [
    id 82
    label "karbonizacja"
  ]
  node [
    id 83
    label "rozprucie_si&#281;"
  ]
  node [
    id 84
    label "towar"
  ]
  node [
    id 85
    label "rozpru&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
]
