graph [
  node [
    id 0
    label "kr&#243;tki"
    origin "text"
  ]
  node [
    id 1
    label "spotkanie"
    origin "text"
  ]
  node [
    id 2
    label "pi&#261;tkowy"
    origin "text"
  ]
  node [
    id 3
    label "popo&#322;udnie"
    origin "text"
  ]
  node [
    id 4
    label "zdominowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "model"
    origin "text"
  ]
  node [
    id 6
    label "elektryczny"
    origin "text"
  ]
  node [
    id 7
    label "g&#322;&#243;wnie"
    origin "text"
  ]
  node [
    id 8
    label "sprawa"
    origin "text"
  ]
  node [
    id 9
    label "maciolusa"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "specjalizowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "nowy"
    origin "text"
  ]
  node [
    id 14
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 15
    label "zakres"
    origin "text"
  ]
  node [
    id 16
    label "nap&#281;d"
    origin "text"
  ]
  node [
    id 17
    label "oryginalny"
    origin "text"
  ]
  node [
    id 18
    label "jak"
    origin "text"
  ]
  node [
    id 19
    label "zawsze"
    origin "text"
  ]
  node [
    id 20
    label "wzbudzi&#263;"
    origin "text"
  ]
  node [
    id 21
    label "duha"
    origin "text"
  ]
  node [
    id 22
    label "zainteresowanie"
    origin "text"
  ]
  node [
    id 23
    label "tym"
    origin "text"
  ]
  node [
    id 24
    label "bardzo"
    origin "text"
  ]
  node [
    id 25
    label "konstruktor"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 27
    label "znany"
    origin "text"
  ]
  node [
    id 28
    label "by&#263;"
    origin "text"
  ]
  node [
    id 29
    label "wyj&#261;tkowo"
    origin "text"
  ]
  node [
    id 30
    label "bezkompromisowy"
    origin "text"
  ]
  node [
    id 31
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 32
    label "powiedziec"
    origin "text"
  ]
  node [
    id 33
    label "brutalny"
    origin "text"
  ]
  node [
    id 34
    label "traktowanie"
    origin "text"
  ]
  node [
    id 35
    label "swoje"
    origin "text"
  ]
  node [
    id 36
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 37
    label "razem"
    origin "text"
  ]
  node [
    id 38
    label "mocno"
    origin "text"
  ]
  node [
    id 39
    label "pokiereszowa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "mocowa&#263;"
    origin "text"
  ]
  node [
    id 41
    label "dyferencja&#322;"
    origin "text"
  ]
  node [
    id 42
    label "bezszczotkowym"
    origin "text"
  ]
  node [
    id 43
    label "lightningu"
    origin "text"
  ]
  node [
    id 44
    label "stadium"
    origin "text"
  ]
  node [
    id 45
    label "szybki"
  ]
  node [
    id 46
    label "jednowyrazowy"
  ]
  node [
    id 47
    label "bliski"
  ]
  node [
    id 48
    label "s&#322;aby"
  ]
  node [
    id 49
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 50
    label "kr&#243;tko"
  ]
  node [
    id 51
    label "drobny"
  ]
  node [
    id 52
    label "ruch"
  ]
  node [
    id 53
    label "brak"
  ]
  node [
    id 54
    label "z&#322;y"
  ]
  node [
    id 55
    label "pieski"
  ]
  node [
    id 56
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 57
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 58
    label "niekorzystny"
  ]
  node [
    id 59
    label "z&#322;oszczenie"
  ]
  node [
    id 60
    label "sierdzisty"
  ]
  node [
    id 61
    label "niegrzeczny"
  ]
  node [
    id 62
    label "zez&#322;oszczenie"
  ]
  node [
    id 63
    label "zdenerwowany"
  ]
  node [
    id 64
    label "negatywny"
  ]
  node [
    id 65
    label "rozgniewanie"
  ]
  node [
    id 66
    label "gniewanie"
  ]
  node [
    id 67
    label "niemoralny"
  ]
  node [
    id 68
    label "&#378;le"
  ]
  node [
    id 69
    label "niepomy&#347;lny"
  ]
  node [
    id 70
    label "syf"
  ]
  node [
    id 71
    label "sprawny"
  ]
  node [
    id 72
    label "efektywny"
  ]
  node [
    id 73
    label "zwi&#281;&#378;le"
  ]
  node [
    id 74
    label "oszcz&#281;dny"
  ]
  node [
    id 75
    label "nietrwa&#322;y"
  ]
  node [
    id 76
    label "mizerny"
  ]
  node [
    id 77
    label "marnie"
  ]
  node [
    id 78
    label "delikatny"
  ]
  node [
    id 79
    label "po&#347;ledni"
  ]
  node [
    id 80
    label "niezdrowy"
  ]
  node [
    id 81
    label "nieumiej&#281;tny"
  ]
  node [
    id 82
    label "s&#322;abo"
  ]
  node [
    id 83
    label "nieznaczny"
  ]
  node [
    id 84
    label "lura"
  ]
  node [
    id 85
    label "nieudany"
  ]
  node [
    id 86
    label "s&#322;abowity"
  ]
  node [
    id 87
    label "zawodny"
  ]
  node [
    id 88
    label "&#322;agodny"
  ]
  node [
    id 89
    label "md&#322;y"
  ]
  node [
    id 90
    label "niedoskona&#322;y"
  ]
  node [
    id 91
    label "przemijaj&#261;cy"
  ]
  node [
    id 92
    label "niemocny"
  ]
  node [
    id 93
    label "niefajny"
  ]
  node [
    id 94
    label "kiepsko"
  ]
  node [
    id 95
    label "blisko"
  ]
  node [
    id 96
    label "cz&#322;owiek"
  ]
  node [
    id 97
    label "znajomy"
  ]
  node [
    id 98
    label "zwi&#261;zany"
  ]
  node [
    id 99
    label "przesz&#322;y"
  ]
  node [
    id 100
    label "silny"
  ]
  node [
    id 101
    label "zbli&#380;enie"
  ]
  node [
    id 102
    label "oddalony"
  ]
  node [
    id 103
    label "dok&#322;adny"
  ]
  node [
    id 104
    label "nieodleg&#322;y"
  ]
  node [
    id 105
    label "przysz&#322;y"
  ]
  node [
    id 106
    label "gotowy"
  ]
  node [
    id 107
    label "ma&#322;y"
  ]
  node [
    id 108
    label "intensywny"
  ]
  node [
    id 109
    label "prosty"
  ]
  node [
    id 110
    label "temperamentny"
  ]
  node [
    id 111
    label "bystrolotny"
  ]
  node [
    id 112
    label "dynamiczny"
  ]
  node [
    id 113
    label "szybko"
  ]
  node [
    id 114
    label "bezpo&#347;redni"
  ]
  node [
    id 115
    label "energiczny"
  ]
  node [
    id 116
    label "skromny"
  ]
  node [
    id 117
    label "niesamodzielny"
  ]
  node [
    id 118
    label "niewa&#380;ny"
  ]
  node [
    id 119
    label "podhala&#324;ski"
  ]
  node [
    id 120
    label "taniec_ludowy"
  ]
  node [
    id 121
    label "szczup&#322;y"
  ]
  node [
    id 122
    label "drobno"
  ]
  node [
    id 123
    label "ma&#322;oletni"
  ]
  node [
    id 124
    label "nieistnienie"
  ]
  node [
    id 125
    label "odej&#347;cie"
  ]
  node [
    id 126
    label "defect"
  ]
  node [
    id 127
    label "gap"
  ]
  node [
    id 128
    label "odej&#347;&#263;"
  ]
  node [
    id 129
    label "wada"
  ]
  node [
    id 130
    label "odchodzi&#263;"
  ]
  node [
    id 131
    label "wyr&#243;b"
  ]
  node [
    id 132
    label "odchodzenie"
  ]
  node [
    id 133
    label "prywatywny"
  ]
  node [
    id 134
    label "jednocz&#322;onowy"
  ]
  node [
    id 135
    label "mechanika"
  ]
  node [
    id 136
    label "utrzymywanie"
  ]
  node [
    id 137
    label "move"
  ]
  node [
    id 138
    label "poruszenie"
  ]
  node [
    id 139
    label "movement"
  ]
  node [
    id 140
    label "myk"
  ]
  node [
    id 141
    label "utrzyma&#263;"
  ]
  node [
    id 142
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 143
    label "zjawisko"
  ]
  node [
    id 144
    label "utrzymanie"
  ]
  node [
    id 145
    label "travel"
  ]
  node [
    id 146
    label "kanciasty"
  ]
  node [
    id 147
    label "commercial_enterprise"
  ]
  node [
    id 148
    label "strumie&#324;"
  ]
  node [
    id 149
    label "proces"
  ]
  node [
    id 150
    label "aktywno&#347;&#263;"
  ]
  node [
    id 151
    label "taktyka"
  ]
  node [
    id 152
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 153
    label "apraksja"
  ]
  node [
    id 154
    label "natural_process"
  ]
  node [
    id 155
    label "utrzymywa&#263;"
  ]
  node [
    id 156
    label "d&#322;ugi"
  ]
  node [
    id 157
    label "wydarzenie"
  ]
  node [
    id 158
    label "dyssypacja_energii"
  ]
  node [
    id 159
    label "tumult"
  ]
  node [
    id 160
    label "stopek"
  ]
  node [
    id 161
    label "czynno&#347;&#263;"
  ]
  node [
    id 162
    label "zmiana"
  ]
  node [
    id 163
    label "manewr"
  ]
  node [
    id 164
    label "lokomocja"
  ]
  node [
    id 165
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 166
    label "komunikacja"
  ]
  node [
    id 167
    label "drift"
  ]
  node [
    id 168
    label "doznanie"
  ]
  node [
    id 169
    label "gathering"
  ]
  node [
    id 170
    label "zawarcie"
  ]
  node [
    id 171
    label "powitanie"
  ]
  node [
    id 172
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 173
    label "spowodowanie"
  ]
  node [
    id 174
    label "zdarzenie_si&#281;"
  ]
  node [
    id 175
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 176
    label "znalezienie"
  ]
  node [
    id 177
    label "match"
  ]
  node [
    id 178
    label "employment"
  ]
  node [
    id 179
    label "po&#380;egnanie"
  ]
  node [
    id 180
    label "gather"
  ]
  node [
    id 181
    label "spotykanie"
  ]
  node [
    id 182
    label "spotkanie_si&#281;"
  ]
  node [
    id 183
    label "dzianie_si&#281;"
  ]
  node [
    id 184
    label "zaznawanie"
  ]
  node [
    id 185
    label "znajdowanie"
  ]
  node [
    id 186
    label "zdarzanie_si&#281;"
  ]
  node [
    id 187
    label "merging"
  ]
  node [
    id 188
    label "meeting"
  ]
  node [
    id 189
    label "zawieranie"
  ]
  node [
    id 190
    label "campaign"
  ]
  node [
    id 191
    label "causing"
  ]
  node [
    id 192
    label "przebiec"
  ]
  node [
    id 193
    label "charakter"
  ]
  node [
    id 194
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 195
    label "motyw"
  ]
  node [
    id 196
    label "przebiegni&#281;cie"
  ]
  node [
    id 197
    label "fabu&#322;a"
  ]
  node [
    id 198
    label "postaranie_si&#281;"
  ]
  node [
    id 199
    label "discovery"
  ]
  node [
    id 200
    label "wymy&#347;lenie"
  ]
  node [
    id 201
    label "determination"
  ]
  node [
    id 202
    label "dorwanie"
  ]
  node [
    id 203
    label "znalezienie_si&#281;"
  ]
  node [
    id 204
    label "wykrycie"
  ]
  node [
    id 205
    label "poszukanie"
  ]
  node [
    id 206
    label "invention"
  ]
  node [
    id 207
    label "pozyskanie"
  ]
  node [
    id 208
    label "zmieszczenie"
  ]
  node [
    id 209
    label "umawianie_si&#281;"
  ]
  node [
    id 210
    label "zapoznanie"
  ]
  node [
    id 211
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 212
    label "zapoznanie_si&#281;"
  ]
  node [
    id 213
    label "ustalenie"
  ]
  node [
    id 214
    label "dissolution"
  ]
  node [
    id 215
    label "przyskrzynienie"
  ]
  node [
    id 216
    label "uk&#322;ad"
  ]
  node [
    id 217
    label "pozamykanie"
  ]
  node [
    id 218
    label "inclusion"
  ]
  node [
    id 219
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 220
    label "uchwalenie"
  ]
  node [
    id 221
    label "umowa"
  ]
  node [
    id 222
    label "zrobienie"
  ]
  node [
    id 223
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 224
    label "wy&#347;wiadczenie"
  ]
  node [
    id 225
    label "zmys&#322;"
  ]
  node [
    id 226
    label "czucie"
  ]
  node [
    id 227
    label "przeczulica"
  ]
  node [
    id 228
    label "poczucie"
  ]
  node [
    id 229
    label "sw&#243;j"
  ]
  node [
    id 230
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 231
    label "znajomek"
  ]
  node [
    id 232
    label "zapoznawanie"
  ]
  node [
    id 233
    label "przyj&#281;ty"
  ]
  node [
    id 234
    label "pewien"
  ]
  node [
    id 235
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 236
    label "znajomo"
  ]
  node [
    id 237
    label "za_pan_brat"
  ]
  node [
    id 238
    label "rozstanie_si&#281;"
  ]
  node [
    id 239
    label "adieu"
  ]
  node [
    id 240
    label "pozdrowienie"
  ]
  node [
    id 241
    label "zwyczaj"
  ]
  node [
    id 242
    label "farewell"
  ]
  node [
    id 243
    label "welcome"
  ]
  node [
    id 244
    label "greeting"
  ]
  node [
    id 245
    label "wyra&#380;enie"
  ]
  node [
    id 246
    label "dzie&#324;"
  ]
  node [
    id 247
    label "odwieczerz"
  ]
  node [
    id 248
    label "pora"
  ]
  node [
    id 249
    label "run"
  ]
  node [
    id 250
    label "okres_czasu"
  ]
  node [
    id 251
    label "czas"
  ]
  node [
    id 252
    label "kolacja"
  ]
  node [
    id 253
    label "ranek"
  ]
  node [
    id 254
    label "doba"
  ]
  node [
    id 255
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 256
    label "noc"
  ]
  node [
    id 257
    label "podwiecz&#243;r"
  ]
  node [
    id 258
    label "po&#322;udnie"
  ]
  node [
    id 259
    label "godzina"
  ]
  node [
    id 260
    label "przedpo&#322;udnie"
  ]
  node [
    id 261
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 262
    label "long_time"
  ]
  node [
    id 263
    label "wiecz&#243;r"
  ]
  node [
    id 264
    label "t&#322;usty_czwartek"
  ]
  node [
    id 265
    label "walentynki"
  ]
  node [
    id 266
    label "czynienie_si&#281;"
  ]
  node [
    id 267
    label "s&#322;o&#324;ce"
  ]
  node [
    id 268
    label "rano"
  ]
  node [
    id 269
    label "tydzie&#324;"
  ]
  node [
    id 270
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 271
    label "wzej&#347;cie"
  ]
  node [
    id 272
    label "wsta&#263;"
  ]
  node [
    id 273
    label "day"
  ]
  node [
    id 274
    label "termin"
  ]
  node [
    id 275
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 276
    label "wstanie"
  ]
  node [
    id 277
    label "przedwiecz&#243;r"
  ]
  node [
    id 278
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 279
    label "Sylwester"
  ]
  node [
    id 280
    label "rule"
  ]
  node [
    id 281
    label "zdecydowa&#263;"
  ]
  node [
    id 282
    label "sta&#263;_si&#281;"
  ]
  node [
    id 283
    label "podj&#261;&#263;"
  ]
  node [
    id 284
    label "decide"
  ]
  node [
    id 285
    label "determine"
  ]
  node [
    id 286
    label "zrobi&#263;"
  ]
  node [
    id 287
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 288
    label "spos&#243;b"
  ]
  node [
    id 289
    label "prezenter"
  ]
  node [
    id 290
    label "typ"
  ]
  node [
    id 291
    label "mildew"
  ]
  node [
    id 292
    label "zi&#243;&#322;ko"
  ]
  node [
    id 293
    label "motif"
  ]
  node [
    id 294
    label "pozowanie"
  ]
  node [
    id 295
    label "ideal"
  ]
  node [
    id 296
    label "wz&#243;r"
  ]
  node [
    id 297
    label "matryca"
  ]
  node [
    id 298
    label "adaptation"
  ]
  node [
    id 299
    label "pozowa&#263;"
  ]
  node [
    id 300
    label "imitacja"
  ]
  node [
    id 301
    label "orygina&#322;"
  ]
  node [
    id 302
    label "facet"
  ]
  node [
    id 303
    label "miniatura"
  ]
  node [
    id 304
    label "narz&#281;dzie"
  ]
  node [
    id 305
    label "gablotka"
  ]
  node [
    id 306
    label "pokaz"
  ]
  node [
    id 307
    label "szkatu&#322;ka"
  ]
  node [
    id 308
    label "pude&#322;ko"
  ]
  node [
    id 309
    label "bran&#380;owiec"
  ]
  node [
    id 310
    label "prowadz&#261;cy"
  ]
  node [
    id 311
    label "ludzko&#347;&#263;"
  ]
  node [
    id 312
    label "asymilowanie"
  ]
  node [
    id 313
    label "wapniak"
  ]
  node [
    id 314
    label "asymilowa&#263;"
  ]
  node [
    id 315
    label "os&#322;abia&#263;"
  ]
  node [
    id 316
    label "posta&#263;"
  ]
  node [
    id 317
    label "hominid"
  ]
  node [
    id 318
    label "podw&#322;adny"
  ]
  node [
    id 319
    label "os&#322;abianie"
  ]
  node [
    id 320
    label "g&#322;owa"
  ]
  node [
    id 321
    label "figura"
  ]
  node [
    id 322
    label "portrecista"
  ]
  node [
    id 323
    label "dwun&#243;g"
  ]
  node [
    id 324
    label "profanum"
  ]
  node [
    id 325
    label "mikrokosmos"
  ]
  node [
    id 326
    label "nasada"
  ]
  node [
    id 327
    label "duch"
  ]
  node [
    id 328
    label "antropochoria"
  ]
  node [
    id 329
    label "osoba"
  ]
  node [
    id 330
    label "senior"
  ]
  node [
    id 331
    label "oddzia&#322;ywanie"
  ]
  node [
    id 332
    label "Adam"
  ]
  node [
    id 333
    label "homo_sapiens"
  ]
  node [
    id 334
    label "polifag"
  ]
  node [
    id 335
    label "kszta&#322;t"
  ]
  node [
    id 336
    label "przedmiot"
  ]
  node [
    id 337
    label "kopia"
  ]
  node [
    id 338
    label "utw&#243;r"
  ]
  node [
    id 339
    label "obraz"
  ]
  node [
    id 340
    label "obiekt"
  ]
  node [
    id 341
    label "ilustracja"
  ]
  node [
    id 342
    label "miniature"
  ]
  node [
    id 343
    label "zapis"
  ]
  node [
    id 344
    label "figure"
  ]
  node [
    id 345
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 346
    label "dekal"
  ]
  node [
    id 347
    label "projekt"
  ]
  node [
    id 348
    label "technika"
  ]
  node [
    id 349
    label "praktyka"
  ]
  node [
    id 350
    label "na&#347;ladownictwo"
  ]
  node [
    id 351
    label "zbi&#243;r"
  ]
  node [
    id 352
    label "tryb"
  ]
  node [
    id 353
    label "nature"
  ]
  node [
    id 354
    label "bratek"
  ]
  node [
    id 355
    label "kod_genetyczny"
  ]
  node [
    id 356
    label "t&#322;ocznik"
  ]
  node [
    id 357
    label "aparat_cyfrowy"
  ]
  node [
    id 358
    label "detector"
  ]
  node [
    id 359
    label "forma"
  ]
  node [
    id 360
    label "jednostka_systematyczna"
  ]
  node [
    id 361
    label "kr&#243;lestwo"
  ]
  node [
    id 362
    label "autorament"
  ]
  node [
    id 363
    label "variety"
  ]
  node [
    id 364
    label "antycypacja"
  ]
  node [
    id 365
    label "przypuszczenie"
  ]
  node [
    id 366
    label "cynk"
  ]
  node [
    id 367
    label "obstawia&#263;"
  ]
  node [
    id 368
    label "gromada"
  ]
  node [
    id 369
    label "sztuka"
  ]
  node [
    id 370
    label "rezultat"
  ]
  node [
    id 371
    label "design"
  ]
  node [
    id 372
    label "sfotografowanie_si&#281;"
  ]
  node [
    id 373
    label "na&#347;ladowanie"
  ]
  node [
    id 374
    label "robienie"
  ]
  node [
    id 375
    label "fotografowanie_si&#281;"
  ]
  node [
    id 376
    label "pretense"
  ]
  node [
    id 377
    label "sit"
  ]
  node [
    id 378
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 379
    label "robi&#263;"
  ]
  node [
    id 380
    label "dally"
  ]
  node [
    id 381
    label "nicpo&#324;"
  ]
  node [
    id 382
    label "agent"
  ]
  node [
    id 383
    label "elektrycznie"
  ]
  node [
    id 384
    label "g&#322;&#243;wny"
  ]
  node [
    id 385
    label "najwa&#380;niejszy"
  ]
  node [
    id 386
    label "kognicja"
  ]
  node [
    id 387
    label "object"
  ]
  node [
    id 388
    label "rozprawa"
  ]
  node [
    id 389
    label "temat"
  ]
  node [
    id 390
    label "szczeg&#243;&#322;"
  ]
  node [
    id 391
    label "proposition"
  ]
  node [
    id 392
    label "przes&#322;anka"
  ]
  node [
    id 393
    label "rzecz"
  ]
  node [
    id 394
    label "idea"
  ]
  node [
    id 395
    label "ideologia"
  ]
  node [
    id 396
    label "byt"
  ]
  node [
    id 397
    label "intelekt"
  ]
  node [
    id 398
    label "Kant"
  ]
  node [
    id 399
    label "p&#322;&#243;d"
  ]
  node [
    id 400
    label "cel"
  ]
  node [
    id 401
    label "poj&#281;cie"
  ]
  node [
    id 402
    label "istota"
  ]
  node [
    id 403
    label "pomys&#322;"
  ]
  node [
    id 404
    label "ideacja"
  ]
  node [
    id 405
    label "wpadni&#281;cie"
  ]
  node [
    id 406
    label "mienie"
  ]
  node [
    id 407
    label "przyroda"
  ]
  node [
    id 408
    label "kultura"
  ]
  node [
    id 409
    label "wpa&#347;&#263;"
  ]
  node [
    id 410
    label "wpadanie"
  ]
  node [
    id 411
    label "wpada&#263;"
  ]
  node [
    id 412
    label "s&#261;d"
  ]
  node [
    id 413
    label "rozumowanie"
  ]
  node [
    id 414
    label "opracowanie"
  ]
  node [
    id 415
    label "obrady"
  ]
  node [
    id 416
    label "cytat"
  ]
  node [
    id 417
    label "tekst"
  ]
  node [
    id 418
    label "obja&#347;nienie"
  ]
  node [
    id 419
    label "s&#261;dzenie"
  ]
  node [
    id 420
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 421
    label "niuansowa&#263;"
  ]
  node [
    id 422
    label "element"
  ]
  node [
    id 423
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 424
    label "sk&#322;adnik"
  ]
  node [
    id 425
    label "zniuansowa&#263;"
  ]
  node [
    id 426
    label "fakt"
  ]
  node [
    id 427
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 428
    label "przyczyna"
  ]
  node [
    id 429
    label "wnioskowanie"
  ]
  node [
    id 430
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 431
    label "wyraz_pochodny"
  ]
  node [
    id 432
    label "zboczenie"
  ]
  node [
    id 433
    label "om&#243;wienie"
  ]
  node [
    id 434
    label "cecha"
  ]
  node [
    id 435
    label "omawia&#263;"
  ]
  node [
    id 436
    label "fraza"
  ]
  node [
    id 437
    label "tre&#347;&#263;"
  ]
  node [
    id 438
    label "entity"
  ]
  node [
    id 439
    label "forum"
  ]
  node [
    id 440
    label "topik"
  ]
  node [
    id 441
    label "tematyka"
  ]
  node [
    id 442
    label "w&#261;tek"
  ]
  node [
    id 443
    label "zbaczanie"
  ]
  node [
    id 444
    label "om&#243;wi&#263;"
  ]
  node [
    id 445
    label "omawianie"
  ]
  node [
    id 446
    label "melodia"
  ]
  node [
    id 447
    label "otoczka"
  ]
  node [
    id 448
    label "zbacza&#263;"
  ]
  node [
    id 449
    label "zboczy&#263;"
  ]
  node [
    id 450
    label "szkoli&#263;"
  ]
  node [
    id 451
    label "specify"
  ]
  node [
    id 452
    label "ukierunkowywa&#263;"
  ]
  node [
    id 453
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 454
    label "marshal"
  ]
  node [
    id 455
    label "wyznacza&#263;"
  ]
  node [
    id 456
    label "train"
  ]
  node [
    id 457
    label "prowadzi&#263;"
  ]
  node [
    id 458
    label "doskonali&#263;"
  ]
  node [
    id 459
    label "o&#347;wieca&#263;"
  ]
  node [
    id 460
    label "pomaga&#263;"
  ]
  node [
    id 461
    label "stagger"
  ]
  node [
    id 462
    label "oddziela&#263;"
  ]
  node [
    id 463
    label "wykrawa&#263;"
  ]
  node [
    id 464
    label "kolejny"
  ]
  node [
    id 465
    label "nowo"
  ]
  node [
    id 466
    label "bie&#380;&#261;cy"
  ]
  node [
    id 467
    label "drugi"
  ]
  node [
    id 468
    label "narybek"
  ]
  node [
    id 469
    label "obcy"
  ]
  node [
    id 470
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 471
    label "nowotny"
  ]
  node [
    id 472
    label "nadprzyrodzony"
  ]
  node [
    id 473
    label "nieznany"
  ]
  node [
    id 474
    label "pozaludzki"
  ]
  node [
    id 475
    label "obco"
  ]
  node [
    id 476
    label "tameczny"
  ]
  node [
    id 477
    label "nieznajomo"
  ]
  node [
    id 478
    label "inny"
  ]
  node [
    id 479
    label "cudzy"
  ]
  node [
    id 480
    label "istota_&#380;ywa"
  ]
  node [
    id 481
    label "zaziemsko"
  ]
  node [
    id 482
    label "jednoczesny"
  ]
  node [
    id 483
    label "unowocze&#347;nianie"
  ]
  node [
    id 484
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 485
    label "tera&#378;niejszy"
  ]
  node [
    id 486
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 487
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 488
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 489
    label "nast&#281;pnie"
  ]
  node [
    id 490
    label "nastopny"
  ]
  node [
    id 491
    label "kolejno"
  ]
  node [
    id 492
    label "kt&#243;ry&#347;"
  ]
  node [
    id 493
    label "przeciwny"
  ]
  node [
    id 494
    label "wt&#243;ry"
  ]
  node [
    id 495
    label "odwrotnie"
  ]
  node [
    id 496
    label "podobny"
  ]
  node [
    id 497
    label "bie&#380;&#261;co"
  ]
  node [
    id 498
    label "ci&#261;g&#322;y"
  ]
  node [
    id 499
    label "aktualny"
  ]
  node [
    id 500
    label "dopiero_co"
  ]
  node [
    id 501
    label "formacja"
  ]
  node [
    id 502
    label "potomstwo"
  ]
  node [
    id 503
    label "po&#322;&#243;g"
  ]
  node [
    id 504
    label "spe&#322;nienie"
  ]
  node [
    id 505
    label "dula"
  ]
  node [
    id 506
    label "usuni&#281;cie"
  ]
  node [
    id 507
    label "po&#322;o&#380;na"
  ]
  node [
    id 508
    label "wyj&#347;cie"
  ]
  node [
    id 509
    label "uniewa&#380;nienie"
  ]
  node [
    id 510
    label "proces_fizjologiczny"
  ]
  node [
    id 511
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 512
    label "szok_poporodowy"
  ]
  node [
    id 513
    label "event"
  ]
  node [
    id 514
    label "marc&#243;wka"
  ]
  node [
    id 515
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 516
    label "birth"
  ]
  node [
    id 517
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 518
    label "wynik"
  ]
  node [
    id 519
    label "przestanie"
  ]
  node [
    id 520
    label "wyniesienie"
  ]
  node [
    id 521
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 522
    label "pozabieranie"
  ]
  node [
    id 523
    label "pozbycie_si&#281;"
  ]
  node [
    id 524
    label "pousuwanie"
  ]
  node [
    id 525
    label "przesuni&#281;cie"
  ]
  node [
    id 526
    label "przeniesienie"
  ]
  node [
    id 527
    label "znikni&#281;cie"
  ]
  node [
    id 528
    label "coitus_interruptus"
  ]
  node [
    id 529
    label "abstraction"
  ]
  node [
    id 530
    label "removal"
  ]
  node [
    id 531
    label "wyrugowanie"
  ]
  node [
    id 532
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 533
    label "urzeczywistnienie"
  ]
  node [
    id 534
    label "emocja"
  ]
  node [
    id 535
    label "completion"
  ]
  node [
    id 536
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 537
    label "ziszczenie_si&#281;"
  ]
  node [
    id 538
    label "realization"
  ]
  node [
    id 539
    label "pe&#322;ny"
  ]
  node [
    id 540
    label "realizowanie_si&#281;"
  ]
  node [
    id 541
    label "enjoyment"
  ]
  node [
    id 542
    label "gratyfikacja"
  ]
  node [
    id 543
    label "wytw&#243;r"
  ]
  node [
    id 544
    label "pocz&#261;tki"
  ]
  node [
    id 545
    label "ukradzenie"
  ]
  node [
    id 546
    label "ukra&#347;&#263;"
  ]
  node [
    id 547
    label "system"
  ]
  node [
    id 548
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 549
    label "oduczenie"
  ]
  node [
    id 550
    label "disavowal"
  ]
  node [
    id 551
    label "zako&#324;czenie"
  ]
  node [
    id 552
    label "cessation"
  ]
  node [
    id 553
    label "przeczekanie"
  ]
  node [
    id 554
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 555
    label "okazanie_si&#281;"
  ]
  node [
    id 556
    label "ograniczenie"
  ]
  node [
    id 557
    label "uzyskanie"
  ]
  node [
    id 558
    label "ruszenie"
  ]
  node [
    id 559
    label "podzianie_si&#281;"
  ]
  node [
    id 560
    label "powychodzenie"
  ]
  node [
    id 561
    label "opuszczenie"
  ]
  node [
    id 562
    label "postrze&#380;enie"
  ]
  node [
    id 563
    label "transgression"
  ]
  node [
    id 564
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 565
    label "wychodzenie"
  ]
  node [
    id 566
    label "uko&#324;czenie"
  ]
  node [
    id 567
    label "miejsce"
  ]
  node [
    id 568
    label "powiedzenie_si&#281;"
  ]
  node [
    id 569
    label "policzenie"
  ]
  node [
    id 570
    label "podziewanie_si&#281;"
  ]
  node [
    id 571
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 572
    label "exit"
  ]
  node [
    id 573
    label "vent"
  ]
  node [
    id 574
    label "uwolnienie_si&#281;"
  ]
  node [
    id 575
    label "deviation"
  ]
  node [
    id 576
    label "release"
  ]
  node [
    id 577
    label "wych&#243;d"
  ]
  node [
    id 578
    label "withdrawal"
  ]
  node [
    id 579
    label "wypadni&#281;cie"
  ]
  node [
    id 580
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 581
    label "kres"
  ]
  node [
    id 582
    label "odch&#243;d"
  ]
  node [
    id 583
    label "przebywanie"
  ]
  node [
    id 584
    label "przedstawienie"
  ]
  node [
    id 585
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 586
    label "zagranie"
  ]
  node [
    id 587
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 588
    label "emergence"
  ]
  node [
    id 589
    label "zaokr&#261;glenie"
  ]
  node [
    id 590
    label "dzia&#322;anie"
  ]
  node [
    id 591
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 592
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 593
    label "retraction"
  ]
  node [
    id 594
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 595
    label "zerwanie"
  ]
  node [
    id 596
    label "konsekwencja"
  ]
  node [
    id 597
    label "przyjmowa&#263;_por&#243;d"
  ]
  node [
    id 598
    label "przyj&#261;&#263;_por&#243;d"
  ]
  node [
    id 599
    label "babka"
  ]
  node [
    id 600
    label "piel&#281;gniarka"
  ]
  node [
    id 601
    label "zabory"
  ]
  node [
    id 602
    label "ci&#281;&#380;arna"
  ]
  node [
    id 603
    label "asystentka"
  ]
  node [
    id 604
    label "pomoc"
  ]
  node [
    id 605
    label "&#380;ycie"
  ]
  node [
    id 606
    label "zlec"
  ]
  node [
    id 607
    label "zlegni&#281;cie"
  ]
  node [
    id 608
    label "granica"
  ]
  node [
    id 609
    label "sfera"
  ]
  node [
    id 610
    label "wielko&#347;&#263;"
  ]
  node [
    id 611
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 612
    label "podzakres"
  ]
  node [
    id 613
    label "dziedzina"
  ]
  node [
    id 614
    label "desygnat"
  ]
  node [
    id 615
    label "circle"
  ]
  node [
    id 616
    label "rozmiar"
  ]
  node [
    id 617
    label "zasi&#261;g"
  ]
  node [
    id 618
    label "izochronizm"
  ]
  node [
    id 619
    label "bridge"
  ]
  node [
    id 620
    label "distribution"
  ]
  node [
    id 621
    label "warunek_lokalowy"
  ]
  node [
    id 622
    label "liczba"
  ]
  node [
    id 623
    label "rzadko&#347;&#263;"
  ]
  node [
    id 624
    label "zaleta"
  ]
  node [
    id 625
    label "ilo&#347;&#263;"
  ]
  node [
    id 626
    label "measure"
  ]
  node [
    id 627
    label "znaczenie"
  ]
  node [
    id 628
    label "opinia"
  ]
  node [
    id 629
    label "dymensja"
  ]
  node [
    id 630
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 631
    label "zdolno&#347;&#263;"
  ]
  node [
    id 632
    label "potencja"
  ]
  node [
    id 633
    label "property"
  ]
  node [
    id 634
    label "egzemplarz"
  ]
  node [
    id 635
    label "series"
  ]
  node [
    id 636
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 637
    label "uprawianie"
  ]
  node [
    id 638
    label "praca_rolnicza"
  ]
  node [
    id 639
    label "collection"
  ]
  node [
    id 640
    label "dane"
  ]
  node [
    id 641
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 642
    label "pakiet_klimatyczny"
  ]
  node [
    id 643
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 644
    label "sum"
  ]
  node [
    id 645
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 646
    label "album"
  ]
  node [
    id 647
    label "przej&#347;cie"
  ]
  node [
    id 648
    label "granica_pa&#324;stwa"
  ]
  node [
    id 649
    label "Ural"
  ]
  node [
    id 650
    label "miara"
  ]
  node [
    id 651
    label "end"
  ]
  node [
    id 652
    label "pu&#322;ap"
  ]
  node [
    id 653
    label "koniec"
  ]
  node [
    id 654
    label "granice"
  ]
  node [
    id 655
    label "frontier"
  ]
  node [
    id 656
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 657
    label "funkcja"
  ]
  node [
    id 658
    label "bezdro&#380;e"
  ]
  node [
    id 659
    label "poddzia&#322;"
  ]
  node [
    id 660
    label "wymiar"
  ]
  node [
    id 661
    label "strefa"
  ]
  node [
    id 662
    label "kula"
  ]
  node [
    id 663
    label "class"
  ]
  node [
    id 664
    label "sector"
  ]
  node [
    id 665
    label "przestrze&#324;"
  ]
  node [
    id 666
    label "p&#243;&#322;kula"
  ]
  node [
    id 667
    label "huczek"
  ]
  node [
    id 668
    label "p&#243;&#322;sfera"
  ]
  node [
    id 669
    label "powierzchnia"
  ]
  node [
    id 670
    label "kolur"
  ]
  node [
    id 671
    label "grupa"
  ]
  node [
    id 672
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 673
    label "odpowiednik"
  ]
  node [
    id 674
    label "designatum"
  ]
  node [
    id 675
    label "nazwa_rzetelna"
  ]
  node [
    id 676
    label "nazwa_pozorna"
  ]
  node [
    id 677
    label "denotacja"
  ]
  node [
    id 678
    label "energia"
  ]
  node [
    id 679
    label "most"
  ]
  node [
    id 680
    label "urz&#261;dzenie"
  ]
  node [
    id 681
    label "propulsion"
  ]
  node [
    id 682
    label "kom&#243;rka"
  ]
  node [
    id 683
    label "furnishing"
  ]
  node [
    id 684
    label "zabezpieczenie"
  ]
  node [
    id 685
    label "wyrz&#261;dzenie"
  ]
  node [
    id 686
    label "zagospodarowanie"
  ]
  node [
    id 687
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 688
    label "ig&#322;a"
  ]
  node [
    id 689
    label "wirnik"
  ]
  node [
    id 690
    label "aparatura"
  ]
  node [
    id 691
    label "system_energetyczny"
  ]
  node [
    id 692
    label "impulsator"
  ]
  node [
    id 693
    label "mechanizm"
  ]
  node [
    id 694
    label "sprz&#281;t"
  ]
  node [
    id 695
    label "blokowanie"
  ]
  node [
    id 696
    label "set"
  ]
  node [
    id 697
    label "zablokowanie"
  ]
  node [
    id 698
    label "przygotowanie"
  ]
  node [
    id 699
    label "komora"
  ]
  node [
    id 700
    label "j&#281;zyk"
  ]
  node [
    id 701
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 702
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 703
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 704
    label "egzergia"
  ]
  node [
    id 705
    label "emitowa&#263;"
  ]
  node [
    id 706
    label "kwant_energii"
  ]
  node [
    id 707
    label "szwung"
  ]
  node [
    id 708
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 709
    label "power"
  ]
  node [
    id 710
    label "emitowanie"
  ]
  node [
    id 711
    label "energy"
  ]
  node [
    id 712
    label "rzuci&#263;"
  ]
  node [
    id 713
    label "prz&#281;s&#322;o"
  ]
  node [
    id 714
    label "m&#243;zg"
  ]
  node [
    id 715
    label "trasa"
  ]
  node [
    id 716
    label "jarzmo_mostowe"
  ]
  node [
    id 717
    label "pylon"
  ]
  node [
    id 718
    label "zam&#243;zgowie"
  ]
  node [
    id 719
    label "obiekt_mostowy"
  ]
  node [
    id 720
    label "szczelina_dylatacyjna"
  ]
  node [
    id 721
    label "rzucenie"
  ]
  node [
    id 722
    label "rzuca&#263;"
  ]
  node [
    id 723
    label "suwnica"
  ]
  node [
    id 724
    label "porozumienie"
  ]
  node [
    id 725
    label "rzucanie"
  ]
  node [
    id 726
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 727
    label "niespotykany"
  ]
  node [
    id 728
    label "o&#380;ywczy"
  ]
  node [
    id 729
    label "ekscentryczny"
  ]
  node [
    id 730
    label "oryginalnie"
  ]
  node [
    id 731
    label "pierwotny"
  ]
  node [
    id 732
    label "prawdziwy"
  ]
  node [
    id 733
    label "warto&#347;ciowy"
  ]
  node [
    id 734
    label "naturalny"
  ]
  node [
    id 735
    label "pocz&#261;tkowy"
  ]
  node [
    id 736
    label "dziewiczy"
  ]
  node [
    id 737
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 738
    label "prymarnie"
  ]
  node [
    id 739
    label "pradawny"
  ]
  node [
    id 740
    label "pierwotnie"
  ]
  node [
    id 741
    label "podstawowy"
  ]
  node [
    id 742
    label "dziki"
  ]
  node [
    id 743
    label "ekscentrycznie"
  ]
  node [
    id 744
    label "dziwny"
  ]
  node [
    id 745
    label "niekonwencjonalny"
  ]
  node [
    id 746
    label "rewaluowanie"
  ]
  node [
    id 747
    label "warto&#347;ciowo"
  ]
  node [
    id 748
    label "drogi"
  ]
  node [
    id 749
    label "u&#380;yteczny"
  ]
  node [
    id 750
    label "zrewaluowanie"
  ]
  node [
    id 751
    label "dobry"
  ]
  node [
    id 752
    label "&#380;ywny"
  ]
  node [
    id 753
    label "szczery"
  ]
  node [
    id 754
    label "naprawd&#281;"
  ]
  node [
    id 755
    label "realnie"
  ]
  node [
    id 756
    label "zgodny"
  ]
  node [
    id 757
    label "m&#261;dry"
  ]
  node [
    id 758
    label "prawdziwie"
  ]
  node [
    id 759
    label "niezwyk&#322;y"
  ]
  node [
    id 760
    label "osobno"
  ]
  node [
    id 761
    label "r&#243;&#380;ny"
  ]
  node [
    id 762
    label "inszy"
  ]
  node [
    id 763
    label "inaczej"
  ]
  node [
    id 764
    label "pobudzaj&#261;cy"
  ]
  node [
    id 765
    label "zbawienny"
  ]
  node [
    id 766
    label "stymuluj&#261;cy"
  ]
  node [
    id 767
    label "o&#380;ywczo"
  ]
  node [
    id 768
    label "odmiennie"
  ]
  node [
    id 769
    label "niestandardowo"
  ]
  node [
    id 770
    label "niezwykle"
  ]
  node [
    id 771
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 772
    label "zobo"
  ]
  node [
    id 773
    label "yakalo"
  ]
  node [
    id 774
    label "byd&#322;o"
  ]
  node [
    id 775
    label "dzo"
  ]
  node [
    id 776
    label "kr&#281;torogie"
  ]
  node [
    id 777
    label "czochrad&#322;o"
  ]
  node [
    id 778
    label "posp&#243;lstwo"
  ]
  node [
    id 779
    label "kraal"
  ]
  node [
    id 780
    label "livestock"
  ]
  node [
    id 781
    label "prze&#380;uwacz"
  ]
  node [
    id 782
    label "bizon"
  ]
  node [
    id 783
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 784
    label "zebu"
  ]
  node [
    id 785
    label "byd&#322;o_domowe"
  ]
  node [
    id 786
    label "cz&#281;sto"
  ]
  node [
    id 787
    label "ci&#261;gle"
  ]
  node [
    id 788
    label "zaw&#380;dy"
  ]
  node [
    id 789
    label "na_zawsze"
  ]
  node [
    id 790
    label "cz&#281;sty"
  ]
  node [
    id 791
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 792
    label "stale"
  ]
  node [
    id 793
    label "nieprzerwanie"
  ]
  node [
    id 794
    label "wywo&#322;a&#263;"
  ]
  node [
    id 795
    label "arouse"
  ]
  node [
    id 796
    label "poleci&#263;"
  ]
  node [
    id 797
    label "wezwa&#263;"
  ]
  node [
    id 798
    label "trip"
  ]
  node [
    id 799
    label "spowodowa&#263;"
  ]
  node [
    id 800
    label "oznajmi&#263;"
  ]
  node [
    id 801
    label "revolutionize"
  ]
  node [
    id 802
    label "przetworzy&#263;"
  ]
  node [
    id 803
    label "wydali&#263;"
  ]
  node [
    id 804
    label "wzbudzenie"
  ]
  node [
    id 805
    label "care"
  ]
  node [
    id 806
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 807
    label "love"
  ]
  node [
    id 808
    label "ciekawo&#347;&#263;"
  ]
  node [
    id 809
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 810
    label "nastawienie"
  ]
  node [
    id 811
    label "foreignness"
  ]
  node [
    id 812
    label "arousal"
  ]
  node [
    id 813
    label "wywo&#322;anie"
  ]
  node [
    id 814
    label "rozbudzenie"
  ]
  node [
    id 815
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 816
    label "ogrom"
  ]
  node [
    id 817
    label "iskrzy&#263;"
  ]
  node [
    id 818
    label "d&#322;awi&#263;"
  ]
  node [
    id 819
    label "ostygn&#261;&#263;"
  ]
  node [
    id 820
    label "stygn&#261;&#263;"
  ]
  node [
    id 821
    label "stan"
  ]
  node [
    id 822
    label "temperatura"
  ]
  node [
    id 823
    label "afekt"
  ]
  node [
    id 824
    label "sympatia"
  ]
  node [
    id 825
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 826
    label "podatno&#347;&#263;"
  ]
  node [
    id 827
    label "w_chuj"
  ]
  node [
    id 828
    label "projektant"
  ]
  node [
    id 829
    label "architekt"
  ]
  node [
    id 830
    label "autor"
  ]
  node [
    id 831
    label "Witkiewicz"
  ]
  node [
    id 832
    label "Rafael"
  ]
  node [
    id 833
    label "architekci"
  ]
  node [
    id 834
    label "Corazzi"
  ]
  node [
    id 835
    label "Palladio"
  ]
  node [
    id 836
    label "Churriguera"
  ]
  node [
    id 837
    label "in&#380;ynier"
  ]
  node [
    id 838
    label "podmiot"
  ]
  node [
    id 839
    label "wykupienie"
  ]
  node [
    id 840
    label "bycie_w_posiadaniu"
  ]
  node [
    id 841
    label "wykupywanie"
  ]
  node [
    id 842
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 843
    label "osobowo&#347;&#263;"
  ]
  node [
    id 844
    label "organizacja"
  ]
  node [
    id 845
    label "prawo"
  ]
  node [
    id 846
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 847
    label "nauka_prawa"
  ]
  node [
    id 848
    label "wyczerpanie"
  ]
  node [
    id 849
    label "odkupienie"
  ]
  node [
    id 850
    label "wyswobodzenie"
  ]
  node [
    id 851
    label "kupienie"
  ]
  node [
    id 852
    label "ransom"
  ]
  node [
    id 853
    label "kupowanie"
  ]
  node [
    id 854
    label "odkupywanie"
  ]
  node [
    id 855
    label "wyczerpywanie"
  ]
  node [
    id 856
    label "wyswobadzanie"
  ]
  node [
    id 857
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 858
    label "wielki"
  ]
  node [
    id 859
    label "rozpowszechnianie"
  ]
  node [
    id 860
    label "znaczny"
  ]
  node [
    id 861
    label "wyj&#261;tkowy"
  ]
  node [
    id 862
    label "nieprzeci&#281;tny"
  ]
  node [
    id 863
    label "wysoce"
  ]
  node [
    id 864
    label "wa&#380;ny"
  ]
  node [
    id 865
    label "wybitny"
  ]
  node [
    id 866
    label "dupny"
  ]
  node [
    id 867
    label "ujawnienie_si&#281;"
  ]
  node [
    id 868
    label "powstanie"
  ]
  node [
    id 869
    label "wydostanie_si&#281;"
  ]
  node [
    id 870
    label "ukazanie_si&#281;"
  ]
  node [
    id 871
    label "wyr&#243;&#380;nienie_si&#281;"
  ]
  node [
    id 872
    label "zgini&#281;cie"
  ]
  node [
    id 873
    label "dochodzenie"
  ]
  node [
    id 874
    label "powodowanie"
  ]
  node [
    id 875
    label "deployment"
  ]
  node [
    id 876
    label "nuklearyzacja"
  ]
  node [
    id 877
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 878
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 879
    label "mie&#263;_miejsce"
  ]
  node [
    id 880
    label "equal"
  ]
  node [
    id 881
    label "trwa&#263;"
  ]
  node [
    id 882
    label "chodzi&#263;"
  ]
  node [
    id 883
    label "si&#281;ga&#263;"
  ]
  node [
    id 884
    label "obecno&#347;&#263;"
  ]
  node [
    id 885
    label "stand"
  ]
  node [
    id 886
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 887
    label "uczestniczy&#263;"
  ]
  node [
    id 888
    label "participate"
  ]
  node [
    id 889
    label "istnie&#263;"
  ]
  node [
    id 890
    label "pozostawa&#263;"
  ]
  node [
    id 891
    label "zostawa&#263;"
  ]
  node [
    id 892
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 893
    label "adhere"
  ]
  node [
    id 894
    label "compass"
  ]
  node [
    id 895
    label "korzysta&#263;"
  ]
  node [
    id 896
    label "appreciation"
  ]
  node [
    id 897
    label "osi&#261;ga&#263;"
  ]
  node [
    id 898
    label "dociera&#263;"
  ]
  node [
    id 899
    label "get"
  ]
  node [
    id 900
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 901
    label "mierzy&#263;"
  ]
  node [
    id 902
    label "u&#380;ywa&#263;"
  ]
  node [
    id 903
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 904
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 905
    label "exsert"
  ]
  node [
    id 906
    label "being"
  ]
  node [
    id 907
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 908
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 909
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 910
    label "p&#322;ywa&#263;"
  ]
  node [
    id 911
    label "bangla&#263;"
  ]
  node [
    id 912
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 913
    label "przebiega&#263;"
  ]
  node [
    id 914
    label "wk&#322;ada&#263;"
  ]
  node [
    id 915
    label "proceed"
  ]
  node [
    id 916
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 917
    label "carry"
  ]
  node [
    id 918
    label "bywa&#263;"
  ]
  node [
    id 919
    label "dziama&#263;"
  ]
  node [
    id 920
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 921
    label "stara&#263;_si&#281;"
  ]
  node [
    id 922
    label "para"
  ]
  node [
    id 923
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 924
    label "str&#243;j"
  ]
  node [
    id 925
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 926
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 927
    label "krok"
  ]
  node [
    id 928
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 929
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 930
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 931
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 932
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 933
    label "continue"
  ]
  node [
    id 934
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 935
    label "Ohio"
  ]
  node [
    id 936
    label "wci&#281;cie"
  ]
  node [
    id 937
    label "Nowy_York"
  ]
  node [
    id 938
    label "warstwa"
  ]
  node [
    id 939
    label "samopoczucie"
  ]
  node [
    id 940
    label "Illinois"
  ]
  node [
    id 941
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 942
    label "state"
  ]
  node [
    id 943
    label "Jukatan"
  ]
  node [
    id 944
    label "Kalifornia"
  ]
  node [
    id 945
    label "Wirginia"
  ]
  node [
    id 946
    label "wektor"
  ]
  node [
    id 947
    label "Goa"
  ]
  node [
    id 948
    label "Teksas"
  ]
  node [
    id 949
    label "Waszyngton"
  ]
  node [
    id 950
    label "Massachusetts"
  ]
  node [
    id 951
    label "Alaska"
  ]
  node [
    id 952
    label "Arakan"
  ]
  node [
    id 953
    label "Hawaje"
  ]
  node [
    id 954
    label "Maryland"
  ]
  node [
    id 955
    label "punkt"
  ]
  node [
    id 956
    label "Michigan"
  ]
  node [
    id 957
    label "Arizona"
  ]
  node [
    id 958
    label "Georgia"
  ]
  node [
    id 959
    label "poziom"
  ]
  node [
    id 960
    label "Pensylwania"
  ]
  node [
    id 961
    label "shape"
  ]
  node [
    id 962
    label "Luizjana"
  ]
  node [
    id 963
    label "Nowy_Meksyk"
  ]
  node [
    id 964
    label "Alabama"
  ]
  node [
    id 965
    label "Kansas"
  ]
  node [
    id 966
    label "Oregon"
  ]
  node [
    id 967
    label "Oklahoma"
  ]
  node [
    id 968
    label "Floryda"
  ]
  node [
    id 969
    label "jednostka_administracyjna"
  ]
  node [
    id 970
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 971
    label "niestandardowy"
  ]
  node [
    id 972
    label "niekonwencjonalnie"
  ]
  node [
    id 973
    label "nietypowo"
  ]
  node [
    id 974
    label "zdecydowany"
  ]
  node [
    id 975
    label "twardy"
  ]
  node [
    id 976
    label "radykalizowanie"
  ]
  node [
    id 977
    label "zasadniczy"
  ]
  node [
    id 978
    label "niepob&#322;a&#380;liwy"
  ]
  node [
    id 979
    label "zradykalizowanie"
  ]
  node [
    id 980
    label "bezkompromisowo"
  ]
  node [
    id 981
    label "og&#243;lny"
  ]
  node [
    id 982
    label "zasadniczo"
  ]
  node [
    id 983
    label "surowy"
  ]
  node [
    id 984
    label "zdecydowanie"
  ]
  node [
    id 985
    label "pewny"
  ]
  node [
    id 986
    label "zauwa&#380;alny"
  ]
  node [
    id 987
    label "usztywnianie"
  ]
  node [
    id 988
    label "mocny"
  ]
  node [
    id 989
    label "trudny"
  ]
  node [
    id 990
    label "sztywnienie"
  ]
  node [
    id 991
    label "sta&#322;y"
  ]
  node [
    id 992
    label "usztywnienie"
  ]
  node [
    id 993
    label "nieugi&#281;ty"
  ]
  node [
    id 994
    label "zdeterminowany"
  ]
  node [
    id 995
    label "niewra&#380;liwy"
  ]
  node [
    id 996
    label "zesztywnienie"
  ]
  node [
    id 997
    label "konkretny"
  ]
  node [
    id 998
    label "wytrzyma&#322;y"
  ]
  node [
    id 999
    label "twardo"
  ]
  node [
    id 1000
    label "nak&#322;anianie_si&#281;"
  ]
  node [
    id 1001
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1002
    label "przemoc"
  ]
  node [
    id 1003
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1004
    label "bezlitosny"
  ]
  node [
    id 1005
    label "okrutny"
  ]
  node [
    id 1006
    label "brutalnie"
  ]
  node [
    id 1007
    label "niedelikatny"
  ]
  node [
    id 1008
    label "drastycznie"
  ]
  node [
    id 1009
    label "straszny"
  ]
  node [
    id 1010
    label "przykry"
  ]
  node [
    id 1011
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1012
    label "bezlito&#347;ny"
  ]
  node [
    id 1013
    label "bezlito&#347;nie"
  ]
  node [
    id 1014
    label "nie&#322;askawy"
  ]
  node [
    id 1015
    label "gro&#378;ny"
  ]
  node [
    id 1016
    label "okrutnie"
  ]
  node [
    id 1017
    label "nieludzki"
  ]
  node [
    id 1018
    label "pod&#322;y"
  ]
  node [
    id 1019
    label "nieprzyjemny"
  ]
  node [
    id 1020
    label "nieoboj&#281;tny"
  ]
  node [
    id 1021
    label "niedelikatnie"
  ]
  node [
    id 1022
    label "szczodry"
  ]
  node [
    id 1023
    label "s&#322;uszny"
  ]
  node [
    id 1024
    label "uczciwy"
  ]
  node [
    id 1025
    label "przekonuj&#261;cy"
  ]
  node [
    id 1026
    label "prostoduszny"
  ]
  node [
    id 1027
    label "szczyry"
  ]
  node [
    id 1028
    label "szczerze"
  ]
  node [
    id 1029
    label "czysty"
  ]
  node [
    id 1030
    label "krzepienie"
  ]
  node [
    id 1031
    label "&#380;ywotny"
  ]
  node [
    id 1032
    label "pokrzepienie"
  ]
  node [
    id 1033
    label "niepodwa&#380;alny"
  ]
  node [
    id 1034
    label "du&#380;y"
  ]
  node [
    id 1035
    label "zdrowy"
  ]
  node [
    id 1036
    label "silnie"
  ]
  node [
    id 1037
    label "meflochina"
  ]
  node [
    id 1038
    label "zajebisty"
  ]
  node [
    id 1039
    label "stabilny"
  ]
  node [
    id 1040
    label "krzepki"
  ]
  node [
    id 1041
    label "wyrazisty"
  ]
  node [
    id 1042
    label "widoczny"
  ]
  node [
    id 1043
    label "wzmocni&#263;"
  ]
  node [
    id 1044
    label "wzmacnia&#263;"
  ]
  node [
    id 1045
    label "intensywnie"
  ]
  node [
    id 1046
    label "viciously"
  ]
  node [
    id 1047
    label "wyrzyna&#263;"
  ]
  node [
    id 1048
    label "cruelly"
  ]
  node [
    id 1049
    label "bezpardonowo"
  ]
  node [
    id 1050
    label "barbarously"
  ]
  node [
    id 1051
    label "patologia"
  ]
  node [
    id 1052
    label "agresja"
  ]
  node [
    id 1053
    label "przewaga"
  ]
  node [
    id 1054
    label "drastyczny"
  ]
  node [
    id 1055
    label "radykalnie"
  ]
  node [
    id 1056
    label "do&#347;wiadczanie"
  ]
  node [
    id 1057
    label "odnoszenie_si&#281;"
  ]
  node [
    id 1058
    label "treatment"
  ]
  node [
    id 1059
    label "cz&#281;stowanie"
  ]
  node [
    id 1060
    label "use"
  ]
  node [
    id 1061
    label "oferowanie"
  ]
  node [
    id 1062
    label "goszczenie"
  ]
  node [
    id 1063
    label "krzywdzenie"
  ]
  node [
    id 1064
    label "opowiadanie"
  ]
  node [
    id 1065
    label "fabrication"
  ]
  node [
    id 1066
    label "porobienie"
  ]
  node [
    id 1067
    label "bycie"
  ]
  node [
    id 1068
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1069
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1070
    label "creation"
  ]
  node [
    id 1071
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1072
    label "act"
  ]
  node [
    id 1073
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1074
    label "tentegowanie"
  ]
  node [
    id 1075
    label "activity"
  ]
  node [
    id 1076
    label "bezproblemowy"
  ]
  node [
    id 1077
    label "hipnotyzowanie"
  ]
  node [
    id 1078
    label "&#347;lad"
  ]
  node [
    id 1079
    label "docieranie"
  ]
  node [
    id 1080
    label "reakcja_chemiczna"
  ]
  node [
    id 1081
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1082
    label "lobbysta"
  ]
  node [
    id 1083
    label "badanie"
  ]
  node [
    id 1084
    label "recognition"
  ]
  node [
    id 1085
    label "przep&#322;ywanie"
  ]
  node [
    id 1086
    label "pojazd_drogowy"
  ]
  node [
    id 1087
    label "spryskiwacz"
  ]
  node [
    id 1088
    label "baga&#380;nik"
  ]
  node [
    id 1089
    label "silnik"
  ]
  node [
    id 1090
    label "dachowanie"
  ]
  node [
    id 1091
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 1092
    label "pompa_wodna"
  ]
  node [
    id 1093
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 1094
    label "poduszka_powietrzna"
  ]
  node [
    id 1095
    label "tempomat"
  ]
  node [
    id 1096
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 1097
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 1098
    label "deska_rozdzielcza"
  ]
  node [
    id 1099
    label "immobilizer"
  ]
  node [
    id 1100
    label "t&#322;umik"
  ]
  node [
    id 1101
    label "kierownica"
  ]
  node [
    id 1102
    label "ABS"
  ]
  node [
    id 1103
    label "bak"
  ]
  node [
    id 1104
    label "dwu&#347;lad"
  ]
  node [
    id 1105
    label "poci&#261;g_drogowy"
  ]
  node [
    id 1106
    label "wycieraczka"
  ]
  node [
    id 1107
    label "pojazd"
  ]
  node [
    id 1108
    label "sprinkler"
  ]
  node [
    id 1109
    label "przyrz&#261;d"
  ]
  node [
    id 1110
    label "motor"
  ]
  node [
    id 1111
    label "rower"
  ]
  node [
    id 1112
    label "stolik_topograficzny"
  ]
  node [
    id 1113
    label "kontroler_gier"
  ]
  node [
    id 1114
    label "bakenbardy"
  ]
  node [
    id 1115
    label "tank"
  ]
  node [
    id 1116
    label "fordek"
  ]
  node [
    id 1117
    label "zbiornik"
  ]
  node [
    id 1118
    label "beard"
  ]
  node [
    id 1119
    label "zarost"
  ]
  node [
    id 1120
    label "ochrona"
  ]
  node [
    id 1121
    label "mata"
  ]
  node [
    id 1122
    label "biblioteka"
  ]
  node [
    id 1123
    label "radiator"
  ]
  node [
    id 1124
    label "wyci&#261;garka"
  ]
  node [
    id 1125
    label "gondola_silnikowa"
  ]
  node [
    id 1126
    label "aerosanie"
  ]
  node [
    id 1127
    label "podgrzewacz"
  ]
  node [
    id 1128
    label "motogodzina"
  ]
  node [
    id 1129
    label "motoszybowiec"
  ]
  node [
    id 1130
    label "program"
  ]
  node [
    id 1131
    label "gniazdo_zaworowe"
  ]
  node [
    id 1132
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 1133
    label "dotarcie"
  ]
  node [
    id 1134
    label "motor&#243;wka"
  ]
  node [
    id 1135
    label "rz&#281;zi&#263;"
  ]
  node [
    id 1136
    label "perpetuum_mobile"
  ]
  node [
    id 1137
    label "bombowiec"
  ]
  node [
    id 1138
    label "dotrze&#263;"
  ]
  node [
    id 1139
    label "rz&#281;&#380;enie"
  ]
  node [
    id 1140
    label "rekwizyt_muzyczny"
  ]
  node [
    id 1141
    label "attenuator"
  ]
  node [
    id 1142
    label "regulator"
  ]
  node [
    id 1143
    label "bro&#324;_palna"
  ]
  node [
    id 1144
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 1145
    label "cycek"
  ]
  node [
    id 1146
    label "biust"
  ]
  node [
    id 1147
    label "hamowanie"
  ]
  node [
    id 1148
    label "acrylonitrile-butadiene-styrene"
  ]
  node [
    id 1149
    label "sze&#347;ciopak"
  ]
  node [
    id 1150
    label "kulturysta"
  ]
  node [
    id 1151
    label "mu&#322;y"
  ]
  node [
    id 1152
    label "przewracanie_si&#281;"
  ]
  node [
    id 1153
    label "przewr&#243;cenie_si&#281;"
  ]
  node [
    id 1154
    label "jechanie"
  ]
  node [
    id 1155
    label "&#322;&#261;cznie"
  ]
  node [
    id 1156
    label "&#322;&#261;czny"
  ]
  node [
    id 1157
    label "zbiorczo"
  ]
  node [
    id 1158
    label "przekonuj&#261;co"
  ]
  node [
    id 1159
    label "niema&#322;o"
  ]
  node [
    id 1160
    label "powerfully"
  ]
  node [
    id 1161
    label "widocznie"
  ]
  node [
    id 1162
    label "konkretnie"
  ]
  node [
    id 1163
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1164
    label "stabilnie"
  ]
  node [
    id 1165
    label "strongly"
  ]
  node [
    id 1166
    label "znacz&#261;cy"
  ]
  node [
    id 1167
    label "zwarty"
  ]
  node [
    id 1168
    label "ogrodnictwo"
  ]
  node [
    id 1169
    label "nieproporcjonalny"
  ]
  node [
    id 1170
    label "specjalny"
  ]
  node [
    id 1171
    label "zajebi&#347;cie"
  ]
  node [
    id 1172
    label "dusznie"
  ]
  node [
    id 1173
    label "s&#322;usznie"
  ]
  node [
    id 1174
    label "szczero"
  ]
  node [
    id 1175
    label "hojnie"
  ]
  node [
    id 1176
    label "honestly"
  ]
  node [
    id 1177
    label "outspokenly"
  ]
  node [
    id 1178
    label "artlessly"
  ]
  node [
    id 1179
    label "uczciwie"
  ]
  node [
    id 1180
    label "bluffly"
  ]
  node [
    id 1181
    label "visibly"
  ]
  node [
    id 1182
    label "poznawalnie"
  ]
  node [
    id 1183
    label "widno"
  ]
  node [
    id 1184
    label "wyra&#378;nie"
  ]
  node [
    id 1185
    label "widzialnie"
  ]
  node [
    id 1186
    label "dostrzegalnie"
  ]
  node [
    id 1187
    label "widomie"
  ]
  node [
    id 1188
    label "jasno"
  ]
  node [
    id 1189
    label "posilnie"
  ]
  node [
    id 1190
    label "dok&#322;adnie"
  ]
  node [
    id 1191
    label "tre&#347;ciwie"
  ]
  node [
    id 1192
    label "po&#380;ywnie"
  ]
  node [
    id 1193
    label "solidny"
  ]
  node [
    id 1194
    label "&#322;adnie"
  ]
  node [
    id 1195
    label "nie&#378;le"
  ]
  node [
    id 1196
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 1197
    label "decyzja"
  ]
  node [
    id 1198
    label "pewnie"
  ]
  node [
    id 1199
    label "zauwa&#380;alnie"
  ]
  node [
    id 1200
    label "oddzia&#322;anie"
  ]
  node [
    id 1201
    label "podj&#281;cie"
  ]
  node [
    id 1202
    label "resoluteness"
  ]
  node [
    id 1203
    label "judgment"
  ]
  node [
    id 1204
    label "skutecznie"
  ]
  node [
    id 1205
    label "convincingly"
  ]
  node [
    id 1206
    label "trwale"
  ]
  node [
    id 1207
    label "porz&#261;dnie"
  ]
  node [
    id 1208
    label "injury"
  ]
  node [
    id 1209
    label "uszkodzi&#263;"
  ]
  node [
    id 1210
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1211
    label "naruszy&#263;"
  ]
  node [
    id 1212
    label "shatter"
  ]
  node [
    id 1213
    label "pamper"
  ]
  node [
    id 1214
    label "pin"
  ]
  node [
    id 1215
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 1216
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1217
    label "bind"
  ]
  node [
    id 1218
    label "dodawa&#263;"
  ]
  node [
    id 1219
    label "dokoptowywa&#263;"
  ]
  node [
    id 1220
    label "submit"
  ]
  node [
    id 1221
    label "przypinka"
  ]
  node [
    id 1222
    label "peg"
  ]
  node [
    id 1223
    label "przek&#322;adnia_z&#281;bata"
  ]
  node [
    id 1224
    label "differential_gear"
  ]
  node [
    id 1225
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1226
    label "maszyneria"
  ]
  node [
    id 1227
    label "maszyna"
  ]
  node [
    id 1228
    label "podstawa"
  ]
  node [
    id 1229
    label "jednostka_miary_u&#380;ywana_w_&#380;egludze"
  ]
  node [
    id 1230
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1231
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1232
    label "poprzedzanie"
  ]
  node [
    id 1233
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1234
    label "laba"
  ]
  node [
    id 1235
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1236
    label "chronometria"
  ]
  node [
    id 1237
    label "rachuba_czasu"
  ]
  node [
    id 1238
    label "czasokres"
  ]
  node [
    id 1239
    label "odczyt"
  ]
  node [
    id 1240
    label "chwila"
  ]
  node [
    id 1241
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1242
    label "dzieje"
  ]
  node [
    id 1243
    label "kategoria_gramatyczna"
  ]
  node [
    id 1244
    label "poprzedzenie"
  ]
  node [
    id 1245
    label "trawienie"
  ]
  node [
    id 1246
    label "pochodzi&#263;"
  ]
  node [
    id 1247
    label "period"
  ]
  node [
    id 1248
    label "poprzedza&#263;"
  ]
  node [
    id 1249
    label "schy&#322;ek"
  ]
  node [
    id 1250
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1251
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1252
    label "zegar"
  ]
  node [
    id 1253
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1254
    label "czwarty_wymiar"
  ]
  node [
    id 1255
    label "pochodzenie"
  ]
  node [
    id 1256
    label "koniugacja"
  ]
  node [
    id 1257
    label "Zeitgeist"
  ]
  node [
    id 1258
    label "trawi&#263;"
  ]
  node [
    id 1259
    label "pogoda"
  ]
  node [
    id 1260
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1261
    label "poprzedzi&#263;"
  ]
  node [
    id 1262
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1263
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1264
    label "time_period"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 403
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 222
  ]
  edge [
    source 14
    target 394
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 304
  ]
  edge [
    source 14
    target 351
  ]
  edge [
    source 14
    target 352
  ]
  edge [
    source 14
    target 353
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 370
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 351
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 434
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 222
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 304
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 619
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 384
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 114
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 496
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 96
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 351
  ]
  edge [
    source 18
    target 320
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 498
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 796
  ]
  edge [
    source 20
    target 456
  ]
  edge [
    source 20
    target 797
  ]
  edge [
    source 20
    target 798
  ]
  edge [
    source 20
    target 799
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 803
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 804
  ]
  edge [
    source 22
    target 534
  ]
  edge [
    source 22
    target 805
  ]
  edge [
    source 22
    target 806
  ]
  edge [
    source 22
    target 807
  ]
  edge [
    source 22
    target 808
  ]
  edge [
    source 22
    target 809
  ]
  edge [
    source 22
    target 810
  ]
  edge [
    source 22
    target 811
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 22
    target 813
  ]
  edge [
    source 22
    target 814
  ]
  edge [
    source 22
    target 815
  ]
  edge [
    source 22
    target 816
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 818
  ]
  edge [
    source 22
    target 819
  ]
  edge [
    source 22
    target 820
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 409
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 411
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 825
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 37
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 827
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 828
  ]
  edge [
    source 25
    target 829
  ]
  edge [
    source 25
    target 830
  ]
  edge [
    source 25
    target 831
  ]
  edge [
    source 25
    target 832
  ]
  edge [
    source 25
    target 833
  ]
  edge [
    source 25
    target 834
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 836
  ]
  edge [
    source 25
    target 837
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 838
  ]
  edge [
    source 26
    target 839
  ]
  edge [
    source 26
    target 840
  ]
  edge [
    source 26
    target 841
  ]
  edge [
    source 26
    target 842
  ]
  edge [
    source 26
    target 396
  ]
  edge [
    source 26
    target 96
  ]
  edge [
    source 26
    target 843
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 845
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 26
    target 847
  ]
  edge [
    source 26
    target 848
  ]
  edge [
    source 26
    target 849
  ]
  edge [
    source 26
    target 850
  ]
  edge [
    source 26
    target 851
  ]
  edge [
    source 26
    target 852
  ]
  edge [
    source 26
    target 853
  ]
  edge [
    source 26
    target 854
  ]
  edge [
    source 26
    target 855
  ]
  edge [
    source 26
    target 856
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 857
  ]
  edge [
    source 27
    target 858
  ]
  edge [
    source 27
    target 859
  ]
  edge [
    source 27
    target 860
  ]
  edge [
    source 27
    target 861
  ]
  edge [
    source 27
    target 862
  ]
  edge [
    source 27
    target 863
  ]
  edge [
    source 27
    target 864
  ]
  edge [
    source 27
    target 732
  ]
  edge [
    source 27
    target 865
  ]
  edge [
    source 27
    target 866
  ]
  edge [
    source 27
    target 867
  ]
  edge [
    source 27
    target 868
  ]
  edge [
    source 27
    target 869
  ]
  edge [
    source 27
    target 561
  ]
  edge [
    source 27
    target 870
  ]
  edge [
    source 27
    target 588
  ]
  edge [
    source 27
    target 871
  ]
  edge [
    source 27
    target 872
  ]
  edge [
    source 27
    target 873
  ]
  edge [
    source 27
    target 874
  ]
  edge [
    source 27
    target 875
  ]
  edge [
    source 27
    target 374
  ]
  edge [
    source 27
    target 876
  ]
  edge [
    source 27
    target 877
  ]
  edge [
    source 27
    target 161
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 878
  ]
  edge [
    source 28
    target 879
  ]
  edge [
    source 28
    target 880
  ]
  edge [
    source 28
    target 881
  ]
  edge [
    source 28
    target 882
  ]
  edge [
    source 28
    target 883
  ]
  edge [
    source 28
    target 821
  ]
  edge [
    source 28
    target 884
  ]
  edge [
    source 28
    target 885
  ]
  edge [
    source 28
    target 886
  ]
  edge [
    source 28
    target 887
  ]
  edge [
    source 28
    target 888
  ]
  edge [
    source 28
    target 379
  ]
  edge [
    source 28
    target 889
  ]
  edge [
    source 28
    target 890
  ]
  edge [
    source 28
    target 891
  ]
  edge [
    source 28
    target 892
  ]
  edge [
    source 28
    target 893
  ]
  edge [
    source 28
    target 894
  ]
  edge [
    source 28
    target 895
  ]
  edge [
    source 28
    target 896
  ]
  edge [
    source 28
    target 897
  ]
  edge [
    source 28
    target 898
  ]
  edge [
    source 28
    target 899
  ]
  edge [
    source 28
    target 900
  ]
  edge [
    source 28
    target 901
  ]
  edge [
    source 28
    target 902
  ]
  edge [
    source 28
    target 903
  ]
  edge [
    source 28
    target 904
  ]
  edge [
    source 28
    target 905
  ]
  edge [
    source 28
    target 906
  ]
  edge [
    source 28
    target 907
  ]
  edge [
    source 28
    target 434
  ]
  edge [
    source 28
    target 908
  ]
  edge [
    source 28
    target 909
  ]
  edge [
    source 28
    target 910
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 911
  ]
  edge [
    source 28
    target 912
  ]
  edge [
    source 28
    target 913
  ]
  edge [
    source 28
    target 914
  ]
  edge [
    source 28
    target 915
  ]
  edge [
    source 28
    target 916
  ]
  edge [
    source 28
    target 917
  ]
  edge [
    source 28
    target 918
  ]
  edge [
    source 28
    target 919
  ]
  edge [
    source 28
    target 920
  ]
  edge [
    source 28
    target 921
  ]
  edge [
    source 28
    target 922
  ]
  edge [
    source 28
    target 923
  ]
  edge [
    source 28
    target 924
  ]
  edge [
    source 28
    target 925
  ]
  edge [
    source 28
    target 926
  ]
  edge [
    source 28
    target 927
  ]
  edge [
    source 28
    target 352
  ]
  edge [
    source 28
    target 928
  ]
  edge [
    source 28
    target 929
  ]
  edge [
    source 28
    target 930
  ]
  edge [
    source 28
    target 931
  ]
  edge [
    source 28
    target 932
  ]
  edge [
    source 28
    target 933
  ]
  edge [
    source 28
    target 934
  ]
  edge [
    source 28
    target 935
  ]
  edge [
    source 28
    target 936
  ]
  edge [
    source 28
    target 937
  ]
  edge [
    source 28
    target 938
  ]
  edge [
    source 28
    target 939
  ]
  edge [
    source 28
    target 940
  ]
  edge [
    source 28
    target 941
  ]
  edge [
    source 28
    target 942
  ]
  edge [
    source 28
    target 943
  ]
  edge [
    source 28
    target 944
  ]
  edge [
    source 28
    target 945
  ]
  edge [
    source 28
    target 946
  ]
  edge [
    source 28
    target 947
  ]
  edge [
    source 28
    target 948
  ]
  edge [
    source 28
    target 949
  ]
  edge [
    source 28
    target 567
  ]
  edge [
    source 28
    target 950
  ]
  edge [
    source 28
    target 951
  ]
  edge [
    source 28
    target 952
  ]
  edge [
    source 28
    target 953
  ]
  edge [
    source 28
    target 954
  ]
  edge [
    source 28
    target 955
  ]
  edge [
    source 28
    target 956
  ]
  edge [
    source 28
    target 957
  ]
  edge [
    source 28
    target 427
  ]
  edge [
    source 28
    target 958
  ]
  edge [
    source 28
    target 959
  ]
  edge [
    source 28
    target 960
  ]
  edge [
    source 28
    target 961
  ]
  edge [
    source 28
    target 962
  ]
  edge [
    source 28
    target 963
  ]
  edge [
    source 28
    target 964
  ]
  edge [
    source 28
    target 625
  ]
  edge [
    source 28
    target 965
  ]
  edge [
    source 28
    target 966
  ]
  edge [
    source 28
    target 967
  ]
  edge [
    source 28
    target 968
  ]
  edge [
    source 28
    target 969
  ]
  edge [
    source 28
    target 970
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 769
  ]
  edge [
    source 29
    target 861
  ]
  edge [
    source 29
    target 770
  ]
  edge [
    source 29
    target 971
  ]
  edge [
    source 29
    target 972
  ]
  edge [
    source 29
    target 973
  ]
  edge [
    source 29
    target 759
  ]
  edge [
    source 29
    target 478
  ]
  edge [
    source 29
    target 43
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 974
  ]
  edge [
    source 30
    target 975
  ]
  edge [
    source 30
    target 976
  ]
  edge [
    source 30
    target 977
  ]
  edge [
    source 30
    target 978
  ]
  edge [
    source 30
    target 979
  ]
  edge [
    source 30
    target 980
  ]
  edge [
    source 30
    target 384
  ]
  edge [
    source 30
    target 981
  ]
  edge [
    source 30
    target 982
  ]
  edge [
    source 30
    target 983
  ]
  edge [
    source 30
    target 984
  ]
  edge [
    source 30
    target 985
  ]
  edge [
    source 30
    target 986
  ]
  edge [
    source 30
    target 106
  ]
  edge [
    source 30
    target 987
  ]
  edge [
    source 30
    target 988
  ]
  edge [
    source 30
    target 989
  ]
  edge [
    source 30
    target 990
  ]
  edge [
    source 30
    target 991
  ]
  edge [
    source 30
    target 100
  ]
  edge [
    source 30
    target 992
  ]
  edge [
    source 30
    target 993
  ]
  edge [
    source 30
    target 994
  ]
  edge [
    source 30
    target 995
  ]
  edge [
    source 30
    target 996
  ]
  edge [
    source 30
    target 997
  ]
  edge [
    source 30
    target 998
  ]
  edge [
    source 30
    target 999
  ]
  edge [
    source 30
    target 1000
  ]
  edge [
    source 30
    target 874
  ]
  edge [
    source 30
    target 161
  ]
  edge [
    source 30
    target 173
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1001
  ]
  edge [
    source 33
    target 753
  ]
  edge [
    source 33
    target 988
  ]
  edge [
    source 33
    target 1002
  ]
  edge [
    source 33
    target 1003
  ]
  edge [
    source 33
    target 1004
  ]
  edge [
    source 33
    target 100
  ]
  edge [
    source 33
    target 1005
  ]
  edge [
    source 33
    target 1006
  ]
  edge [
    source 33
    target 1007
  ]
  edge [
    source 33
    target 1008
  ]
  edge [
    source 33
    target 1009
  ]
  edge [
    source 33
    target 1010
  ]
  edge [
    source 33
    target 1011
  ]
  edge [
    source 33
    target 989
  ]
  edge [
    source 33
    target 1012
  ]
  edge [
    source 33
    target 665
  ]
  edge [
    source 33
    target 1013
  ]
  edge [
    source 33
    target 732
  ]
  edge [
    source 33
    target 978
  ]
  edge [
    source 33
    target 1014
  ]
  edge [
    source 33
    target 1015
  ]
  edge [
    source 33
    target 1016
  ]
  edge [
    source 33
    target 54
  ]
  edge [
    source 33
    target 1017
  ]
  edge [
    source 33
    target 1018
  ]
  edge [
    source 33
    target 1019
  ]
  edge [
    source 33
    target 61
  ]
  edge [
    source 33
    target 1020
  ]
  edge [
    source 33
    target 995
  ]
  edge [
    source 33
    target 998
  ]
  edge [
    source 33
    target 1021
  ]
  edge [
    source 33
    target 1022
  ]
  edge [
    source 33
    target 1023
  ]
  edge [
    source 33
    target 1024
  ]
  edge [
    source 33
    target 1025
  ]
  edge [
    source 33
    target 1026
  ]
  edge [
    source 33
    target 1027
  ]
  edge [
    source 33
    target 1028
  ]
  edge [
    source 33
    target 1029
  ]
  edge [
    source 33
    target 108
  ]
  edge [
    source 33
    target 1030
  ]
  edge [
    source 33
    target 1031
  ]
  edge [
    source 33
    target 1032
  ]
  edge [
    source 33
    target 974
  ]
  edge [
    source 33
    target 1033
  ]
  edge [
    source 33
    target 1034
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 997
  ]
  edge [
    source 33
    target 1035
  ]
  edge [
    source 33
    target 1036
  ]
  edge [
    source 33
    target 1037
  ]
  edge [
    source 33
    target 1038
  ]
  edge [
    source 33
    target 1039
  ]
  edge [
    source 33
    target 1040
  ]
  edge [
    source 33
    target 1041
  ]
  edge [
    source 33
    target 1042
  ]
  edge [
    source 33
    target 1043
  ]
  edge [
    source 33
    target 1044
  ]
  edge [
    source 33
    target 1045
  ]
  edge [
    source 33
    target 751
  ]
  edge [
    source 33
    target 1046
  ]
  edge [
    source 33
    target 1047
  ]
  edge [
    source 33
    target 1048
  ]
  edge [
    source 33
    target 1049
  ]
  edge [
    source 33
    target 1050
  ]
  edge [
    source 33
    target 1051
  ]
  edge [
    source 33
    target 1052
  ]
  edge [
    source 33
    target 1053
  ]
  edge [
    source 33
    target 1054
  ]
  edge [
    source 33
    target 1055
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1056
  ]
  edge [
    source 34
    target 1057
  ]
  edge [
    source 34
    target 1058
  ]
  edge [
    source 34
    target 374
  ]
  edge [
    source 34
    target 331
  ]
  edge [
    source 34
    target 1059
  ]
  edge [
    source 34
    target 161
  ]
  edge [
    source 34
    target 1060
  ]
  edge [
    source 34
    target 1061
  ]
  edge [
    source 34
    target 1062
  ]
  edge [
    source 34
    target 1063
  ]
  edge [
    source 34
    target 1064
  ]
  edge [
    source 34
    target 1065
  ]
  edge [
    source 34
    target 1066
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 34
    target 1067
  ]
  edge [
    source 34
    target 1068
  ]
  edge [
    source 34
    target 1069
  ]
  edge [
    source 34
    target 1070
  ]
  edge [
    source 34
    target 1071
  ]
  edge [
    source 34
    target 1072
  ]
  edge [
    source 34
    target 1073
  ]
  edge [
    source 34
    target 1074
  ]
  edge [
    source 34
    target 1075
  ]
  edge [
    source 34
    target 1076
  ]
  edge [
    source 34
    target 157
  ]
  edge [
    source 34
    target 874
  ]
  edge [
    source 34
    target 1077
  ]
  edge [
    source 34
    target 96
  ]
  edge [
    source 34
    target 1078
  ]
  edge [
    source 34
    target 1079
  ]
  edge [
    source 34
    target 154
  ]
  edge [
    source 34
    target 1080
  ]
  edge [
    source 34
    target 1081
  ]
  edge [
    source 34
    target 143
  ]
  edge [
    source 34
    target 370
  ]
  edge [
    source 34
    target 1082
  ]
  edge [
    source 34
    target 1083
  ]
  edge [
    source 34
    target 1084
  ]
  edge [
    source 34
    target 226
  ]
  edge [
    source 34
    target 181
  ]
  edge [
    source 34
    target 1085
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 35
    target 42
  ]
  edge [
    source 36
    target 1086
  ]
  edge [
    source 36
    target 1087
  ]
  edge [
    source 36
    target 679
  ]
  edge [
    source 36
    target 1088
  ]
  edge [
    source 36
    target 1089
  ]
  edge [
    source 36
    target 1090
  ]
  edge [
    source 36
    target 1091
  ]
  edge [
    source 36
    target 1092
  ]
  edge [
    source 36
    target 1093
  ]
  edge [
    source 36
    target 1094
  ]
  edge [
    source 36
    target 1095
  ]
  edge [
    source 36
    target 1096
  ]
  edge [
    source 36
    target 1097
  ]
  edge [
    source 36
    target 1098
  ]
  edge [
    source 36
    target 1099
  ]
  edge [
    source 36
    target 1100
  ]
  edge [
    source 36
    target 1101
  ]
  edge [
    source 36
    target 1102
  ]
  edge [
    source 36
    target 1103
  ]
  edge [
    source 36
    target 1104
  ]
  edge [
    source 36
    target 1105
  ]
  edge [
    source 36
    target 1106
  ]
  edge [
    source 36
    target 1107
  ]
  edge [
    source 36
    target 1108
  ]
  edge [
    source 36
    target 680
  ]
  edge [
    source 36
    target 1109
  ]
  edge [
    source 36
    target 712
  ]
  edge [
    source 36
    target 713
  ]
  edge [
    source 36
    target 714
  ]
  edge [
    source 36
    target 715
  ]
  edge [
    source 36
    target 716
  ]
  edge [
    source 36
    target 717
  ]
  edge [
    source 36
    target 718
  ]
  edge [
    source 36
    target 719
  ]
  edge [
    source 36
    target 720
  ]
  edge [
    source 36
    target 721
  ]
  edge [
    source 36
    target 619
  ]
  edge [
    source 36
    target 722
  ]
  edge [
    source 36
    target 723
  ]
  edge [
    source 36
    target 724
  ]
  edge [
    source 36
    target 725
  ]
  edge [
    source 36
    target 726
  ]
  edge [
    source 36
    target 1110
  ]
  edge [
    source 36
    target 1111
  ]
  edge [
    source 36
    target 1112
  ]
  edge [
    source 36
    target 1113
  ]
  edge [
    source 36
    target 1114
  ]
  edge [
    source 36
    target 1115
  ]
  edge [
    source 36
    target 1116
  ]
  edge [
    source 36
    target 1117
  ]
  edge [
    source 36
    target 1118
  ]
  edge [
    source 36
    target 1119
  ]
  edge [
    source 36
    target 1120
  ]
  edge [
    source 36
    target 1121
  ]
  edge [
    source 36
    target 1122
  ]
  edge [
    source 36
    target 1123
  ]
  edge [
    source 36
    target 1124
  ]
  edge [
    source 36
    target 1125
  ]
  edge [
    source 36
    target 1126
  ]
  edge [
    source 36
    target 1127
  ]
  edge [
    source 36
    target 1128
  ]
  edge [
    source 36
    target 1129
  ]
  edge [
    source 36
    target 1130
  ]
  edge [
    source 36
    target 1131
  ]
  edge [
    source 36
    target 693
  ]
  edge [
    source 36
    target 1132
  ]
  edge [
    source 36
    target 898
  ]
  edge [
    source 36
    target 1133
  ]
  edge [
    source 36
    target 1134
  ]
  edge [
    source 36
    target 1135
  ]
  edge [
    source 36
    target 1136
  ]
  edge [
    source 36
    target 1079
  ]
  edge [
    source 36
    target 1137
  ]
  edge [
    source 36
    target 1138
  ]
  edge [
    source 36
    target 1139
  ]
  edge [
    source 36
    target 1140
  ]
  edge [
    source 36
    target 1141
  ]
  edge [
    source 36
    target 1142
  ]
  edge [
    source 36
    target 1143
  ]
  edge [
    source 36
    target 1144
  ]
  edge [
    source 36
    target 1145
  ]
  edge [
    source 36
    target 1146
  ]
  edge [
    source 36
    target 96
  ]
  edge [
    source 36
    target 1147
  ]
  edge [
    source 36
    target 216
  ]
  edge [
    source 36
    target 1148
  ]
  edge [
    source 36
    target 1149
  ]
  edge [
    source 36
    target 1150
  ]
  edge [
    source 36
    target 1151
  ]
  edge [
    source 36
    target 1152
  ]
  edge [
    source 36
    target 1153
  ]
  edge [
    source 36
    target 1154
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1155
  ]
  edge [
    source 37
    target 1156
  ]
  edge [
    source 37
    target 1157
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 108
  ]
  edge [
    source 38
    target 988
  ]
  edge [
    source 38
    target 100
  ]
  edge [
    source 38
    target 1158
  ]
  edge [
    source 38
    target 1159
  ]
  edge [
    source 38
    target 1160
  ]
  edge [
    source 38
    target 1161
  ]
  edge [
    source 38
    target 1028
  ]
  edge [
    source 38
    target 1162
  ]
  edge [
    source 38
    target 1163
  ]
  edge [
    source 38
    target 1164
  ]
  edge [
    source 38
    target 1036
  ]
  edge [
    source 38
    target 984
  ]
  edge [
    source 38
    target 1165
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 1166
  ]
  edge [
    source 38
    target 1167
  ]
  edge [
    source 38
    target 72
  ]
  edge [
    source 38
    target 1168
  ]
  edge [
    source 38
    target 112
  ]
  edge [
    source 38
    target 539
  ]
  edge [
    source 38
    target 1045
  ]
  edge [
    source 38
    target 1169
  ]
  edge [
    source 38
    target 1170
  ]
  edge [
    source 38
    target 753
  ]
  edge [
    source 38
    target 1033
  ]
  edge [
    source 38
    target 974
  ]
  edge [
    source 38
    target 1039
  ]
  edge [
    source 38
    target 989
  ]
  edge [
    source 38
    target 1040
  ]
  edge [
    source 38
    target 1034
  ]
  edge [
    source 38
    target 1041
  ]
  edge [
    source 38
    target 1025
  ]
  edge [
    source 38
    target 1042
  ]
  edge [
    source 38
    target 1043
  ]
  edge [
    source 38
    target 1044
  ]
  edge [
    source 38
    target 997
  ]
  edge [
    source 38
    target 998
  ]
  edge [
    source 38
    target 1037
  ]
  edge [
    source 38
    target 751
  ]
  edge [
    source 38
    target 1030
  ]
  edge [
    source 38
    target 1031
  ]
  edge [
    source 38
    target 1032
  ]
  edge [
    source 38
    target 1035
  ]
  edge [
    source 38
    target 1038
  ]
  edge [
    source 38
    target 1171
  ]
  edge [
    source 38
    target 1172
  ]
  edge [
    source 38
    target 1173
  ]
  edge [
    source 38
    target 1174
  ]
  edge [
    source 38
    target 1175
  ]
  edge [
    source 38
    target 1176
  ]
  edge [
    source 38
    target 1177
  ]
  edge [
    source 38
    target 1178
  ]
  edge [
    source 38
    target 1179
  ]
  edge [
    source 38
    target 1180
  ]
  edge [
    source 38
    target 1181
  ]
  edge [
    source 38
    target 1182
  ]
  edge [
    source 38
    target 1183
  ]
  edge [
    source 38
    target 1184
  ]
  edge [
    source 38
    target 1185
  ]
  edge [
    source 38
    target 1186
  ]
  edge [
    source 38
    target 1187
  ]
  edge [
    source 38
    target 1188
  ]
  edge [
    source 38
    target 1189
  ]
  edge [
    source 38
    target 1190
  ]
  edge [
    source 38
    target 1191
  ]
  edge [
    source 38
    target 1192
  ]
  edge [
    source 38
    target 1193
  ]
  edge [
    source 38
    target 1194
  ]
  edge [
    source 38
    target 1195
  ]
  edge [
    source 38
    target 1196
  ]
  edge [
    source 38
    target 1197
  ]
  edge [
    source 38
    target 1198
  ]
  edge [
    source 38
    target 1199
  ]
  edge [
    source 38
    target 1200
  ]
  edge [
    source 38
    target 1201
  ]
  edge [
    source 38
    target 434
  ]
  edge [
    source 38
    target 1202
  ]
  edge [
    source 38
    target 1203
  ]
  edge [
    source 38
    target 222
  ]
  edge [
    source 38
    target 1204
  ]
  edge [
    source 38
    target 1205
  ]
  edge [
    source 38
    target 1206
  ]
  edge [
    source 38
    target 792
  ]
  edge [
    source 38
    target 1207
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1208
  ]
  edge [
    source 39
    target 1209
  ]
  edge [
    source 39
    target 1210
  ]
  edge [
    source 39
    target 1211
  ]
  edge [
    source 39
    target 799
  ]
  edge [
    source 39
    target 1212
  ]
  edge [
    source 39
    target 1213
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1214
  ]
  edge [
    source 40
    target 1215
  ]
  edge [
    source 40
    target 1216
  ]
  edge [
    source 40
    target 1217
  ]
  edge [
    source 40
    target 1218
  ]
  edge [
    source 40
    target 1219
  ]
  edge [
    source 40
    target 1220
  ]
  edge [
    source 40
    target 1221
  ]
  edge [
    source 40
    target 1222
  ]
  edge [
    source 41
    target 1223
  ]
  edge [
    source 41
    target 1224
  ]
  edge [
    source 41
    target 693
  ]
  edge [
    source 41
    target 288
  ]
  edge [
    source 41
    target 1225
  ]
  edge [
    source 41
    target 1226
  ]
  edge [
    source 41
    target 1227
  ]
  edge [
    source 41
    target 1228
  ]
  edge [
    source 41
    target 680
  ]
  edge [
    source 41
    target 726
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 1229
  ]
  edge [
    source 44
    target 1230
  ]
  edge [
    source 44
    target 1231
  ]
  edge [
    source 44
    target 251
  ]
  edge [
    source 44
    target 1232
  ]
  edge [
    source 44
    target 1233
  ]
  edge [
    source 44
    target 1234
  ]
  edge [
    source 44
    target 1235
  ]
  edge [
    source 44
    target 1236
  ]
  edge [
    source 44
    target 903
  ]
  edge [
    source 44
    target 1237
  ]
  edge [
    source 44
    target 1085
  ]
  edge [
    source 44
    target 223
  ]
  edge [
    source 44
    target 1238
  ]
  edge [
    source 44
    target 1239
  ]
  edge [
    source 44
    target 1240
  ]
  edge [
    source 44
    target 1241
  ]
  edge [
    source 44
    target 1242
  ]
  edge [
    source 44
    target 1243
  ]
  edge [
    source 44
    target 1244
  ]
  edge [
    source 44
    target 1245
  ]
  edge [
    source 44
    target 1246
  ]
  edge [
    source 44
    target 1247
  ]
  edge [
    source 44
    target 250
  ]
  edge [
    source 44
    target 1248
  ]
  edge [
    source 44
    target 1249
  ]
  edge [
    source 44
    target 1250
  ]
  edge [
    source 44
    target 1251
  ]
  edge [
    source 44
    target 1252
  ]
  edge [
    source 44
    target 1253
  ]
  edge [
    source 44
    target 1254
  ]
  edge [
    source 44
    target 1255
  ]
  edge [
    source 44
    target 1256
  ]
  edge [
    source 44
    target 1257
  ]
  edge [
    source 44
    target 1258
  ]
  edge [
    source 44
    target 1259
  ]
  edge [
    source 44
    target 1260
  ]
  edge [
    source 44
    target 1261
  ]
  edge [
    source 44
    target 1262
  ]
  edge [
    source 44
    target 1263
  ]
  edge [
    source 44
    target 1264
  ]
]
