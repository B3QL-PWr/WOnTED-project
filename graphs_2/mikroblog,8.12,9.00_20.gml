graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rodzina"
    origin "text"
  ]
  node [
    id 4
    label "przy"
    origin "text"
  ]
  node [
    id 5
    label "stola"
    origin "text"
  ]
  node [
    id 6
    label "wigilijny"
    origin "text"
  ]
  node [
    id 7
    label "dok&#322;adnie"
  ]
  node [
    id 8
    label "meticulously"
  ]
  node [
    id 9
    label "punctiliously"
  ]
  node [
    id 10
    label "precyzyjnie"
  ]
  node [
    id 11
    label "dok&#322;adny"
  ]
  node [
    id 12
    label "rzetelnie"
  ]
  node [
    id 13
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 14
    label "mie&#263;_miejsce"
  ]
  node [
    id 15
    label "equal"
  ]
  node [
    id 16
    label "trwa&#263;"
  ]
  node [
    id 17
    label "chodzi&#263;"
  ]
  node [
    id 18
    label "si&#281;ga&#263;"
  ]
  node [
    id 19
    label "stan"
  ]
  node [
    id 20
    label "obecno&#347;&#263;"
  ]
  node [
    id 21
    label "stand"
  ]
  node [
    id 22
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "uczestniczy&#263;"
  ]
  node [
    id 24
    label "participate"
  ]
  node [
    id 25
    label "robi&#263;"
  ]
  node [
    id 26
    label "istnie&#263;"
  ]
  node [
    id 27
    label "pozostawa&#263;"
  ]
  node [
    id 28
    label "zostawa&#263;"
  ]
  node [
    id 29
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 30
    label "adhere"
  ]
  node [
    id 31
    label "compass"
  ]
  node [
    id 32
    label "korzysta&#263;"
  ]
  node [
    id 33
    label "appreciation"
  ]
  node [
    id 34
    label "osi&#261;ga&#263;"
  ]
  node [
    id 35
    label "dociera&#263;"
  ]
  node [
    id 36
    label "get"
  ]
  node [
    id 37
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 38
    label "mierzy&#263;"
  ]
  node [
    id 39
    label "u&#380;ywa&#263;"
  ]
  node [
    id 40
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 41
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 42
    label "exsert"
  ]
  node [
    id 43
    label "being"
  ]
  node [
    id 44
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "cecha"
  ]
  node [
    id 46
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 47
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 48
    label "p&#322;ywa&#263;"
  ]
  node [
    id 49
    label "run"
  ]
  node [
    id 50
    label "bangla&#263;"
  ]
  node [
    id 51
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 52
    label "przebiega&#263;"
  ]
  node [
    id 53
    label "wk&#322;ada&#263;"
  ]
  node [
    id 54
    label "proceed"
  ]
  node [
    id 55
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 56
    label "carry"
  ]
  node [
    id 57
    label "bywa&#263;"
  ]
  node [
    id 58
    label "dziama&#263;"
  ]
  node [
    id 59
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 60
    label "stara&#263;_si&#281;"
  ]
  node [
    id 61
    label "para"
  ]
  node [
    id 62
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 63
    label "str&#243;j"
  ]
  node [
    id 64
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 65
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 66
    label "krok"
  ]
  node [
    id 67
    label "tryb"
  ]
  node [
    id 68
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 69
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 70
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 71
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 72
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 73
    label "continue"
  ]
  node [
    id 74
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 75
    label "Ohio"
  ]
  node [
    id 76
    label "wci&#281;cie"
  ]
  node [
    id 77
    label "Nowy_York"
  ]
  node [
    id 78
    label "warstwa"
  ]
  node [
    id 79
    label "samopoczucie"
  ]
  node [
    id 80
    label "Illinois"
  ]
  node [
    id 81
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 82
    label "state"
  ]
  node [
    id 83
    label "Jukatan"
  ]
  node [
    id 84
    label "Kalifornia"
  ]
  node [
    id 85
    label "Wirginia"
  ]
  node [
    id 86
    label "wektor"
  ]
  node [
    id 87
    label "Teksas"
  ]
  node [
    id 88
    label "Goa"
  ]
  node [
    id 89
    label "Waszyngton"
  ]
  node [
    id 90
    label "miejsce"
  ]
  node [
    id 91
    label "Massachusetts"
  ]
  node [
    id 92
    label "Alaska"
  ]
  node [
    id 93
    label "Arakan"
  ]
  node [
    id 94
    label "Hawaje"
  ]
  node [
    id 95
    label "Maryland"
  ]
  node [
    id 96
    label "punkt"
  ]
  node [
    id 97
    label "Michigan"
  ]
  node [
    id 98
    label "Arizona"
  ]
  node [
    id 99
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 100
    label "Georgia"
  ]
  node [
    id 101
    label "poziom"
  ]
  node [
    id 102
    label "Pensylwania"
  ]
  node [
    id 103
    label "shape"
  ]
  node [
    id 104
    label "Luizjana"
  ]
  node [
    id 105
    label "Nowy_Meksyk"
  ]
  node [
    id 106
    label "Alabama"
  ]
  node [
    id 107
    label "ilo&#347;&#263;"
  ]
  node [
    id 108
    label "Kansas"
  ]
  node [
    id 109
    label "Oregon"
  ]
  node [
    id 110
    label "Floryda"
  ]
  node [
    id 111
    label "Oklahoma"
  ]
  node [
    id 112
    label "jednostka_administracyjna"
  ]
  node [
    id 113
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 114
    label "powinowaci"
  ]
  node [
    id 115
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 116
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 117
    label "rodze&#324;stwo"
  ]
  node [
    id 118
    label "jednostka_systematyczna"
  ]
  node [
    id 119
    label "krewni"
  ]
  node [
    id 120
    label "Ossoli&#324;scy"
  ]
  node [
    id 121
    label "potomstwo"
  ]
  node [
    id 122
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 123
    label "theater"
  ]
  node [
    id 124
    label "zbi&#243;r"
  ]
  node [
    id 125
    label "Soplicowie"
  ]
  node [
    id 126
    label "kin"
  ]
  node [
    id 127
    label "family"
  ]
  node [
    id 128
    label "rodzice"
  ]
  node [
    id 129
    label "ordynacja"
  ]
  node [
    id 130
    label "grupa"
  ]
  node [
    id 131
    label "dom_rodzinny"
  ]
  node [
    id 132
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 133
    label "Ostrogscy"
  ]
  node [
    id 134
    label "bliscy"
  ]
  node [
    id 135
    label "przyjaciel_domu"
  ]
  node [
    id 136
    label "dom"
  ]
  node [
    id 137
    label "rz&#261;d"
  ]
  node [
    id 138
    label "Firlejowie"
  ]
  node [
    id 139
    label "Kossakowie"
  ]
  node [
    id 140
    label "Czartoryscy"
  ]
  node [
    id 141
    label "Sapiehowie"
  ]
  node [
    id 142
    label "odm&#322;adzanie"
  ]
  node [
    id 143
    label "liga"
  ]
  node [
    id 144
    label "asymilowanie"
  ]
  node [
    id 145
    label "gromada"
  ]
  node [
    id 146
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 147
    label "asymilowa&#263;"
  ]
  node [
    id 148
    label "egzemplarz"
  ]
  node [
    id 149
    label "Entuzjastki"
  ]
  node [
    id 150
    label "kompozycja"
  ]
  node [
    id 151
    label "Terranie"
  ]
  node [
    id 152
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 153
    label "category"
  ]
  node [
    id 154
    label "pakiet_klimatyczny"
  ]
  node [
    id 155
    label "oddzia&#322;"
  ]
  node [
    id 156
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 157
    label "cz&#261;steczka"
  ]
  node [
    id 158
    label "stage_set"
  ]
  node [
    id 159
    label "type"
  ]
  node [
    id 160
    label "specgrupa"
  ]
  node [
    id 161
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 162
    label "&#346;wietliki"
  ]
  node [
    id 163
    label "odm&#322;odzenie"
  ]
  node [
    id 164
    label "Eurogrupa"
  ]
  node [
    id 165
    label "odm&#322;adza&#263;"
  ]
  node [
    id 166
    label "formacja_geologiczna"
  ]
  node [
    id 167
    label "harcerze_starsi"
  ]
  node [
    id 168
    label "series"
  ]
  node [
    id 169
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 170
    label "uprawianie"
  ]
  node [
    id 171
    label "praca_rolnicza"
  ]
  node [
    id 172
    label "collection"
  ]
  node [
    id 173
    label "dane"
  ]
  node [
    id 174
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 175
    label "poj&#281;cie"
  ]
  node [
    id 176
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 177
    label "sum"
  ]
  node [
    id 178
    label "gathering"
  ]
  node [
    id 179
    label "album"
  ]
  node [
    id 180
    label "grono"
  ]
  node [
    id 181
    label "kuzynostwo"
  ]
  node [
    id 182
    label "stan_cywilny"
  ]
  node [
    id 183
    label "matrymonialny"
  ]
  node [
    id 184
    label "lewirat"
  ]
  node [
    id 185
    label "sakrament"
  ]
  node [
    id 186
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 187
    label "zwi&#261;zek"
  ]
  node [
    id 188
    label "partia"
  ]
  node [
    id 189
    label "czeladka"
  ]
  node [
    id 190
    label "dzietno&#347;&#263;"
  ]
  node [
    id 191
    label "bawienie_si&#281;"
  ]
  node [
    id 192
    label "pomiot"
  ]
  node [
    id 193
    label "starzy"
  ]
  node [
    id 194
    label "pokolenie"
  ]
  node [
    id 195
    label "wapniaki"
  ]
  node [
    id 196
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 197
    label "substancja_mieszkaniowa"
  ]
  node [
    id 198
    label "instytucja"
  ]
  node [
    id 199
    label "siedziba"
  ]
  node [
    id 200
    label "budynek"
  ]
  node [
    id 201
    label "stead"
  ]
  node [
    id 202
    label "garderoba"
  ]
  node [
    id 203
    label "wiecha"
  ]
  node [
    id 204
    label "fratria"
  ]
  node [
    id 205
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 206
    label "obrz&#281;d"
  ]
  node [
    id 207
    label "przybli&#380;enie"
  ]
  node [
    id 208
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 209
    label "kategoria"
  ]
  node [
    id 210
    label "szpaler"
  ]
  node [
    id 211
    label "lon&#380;a"
  ]
  node [
    id 212
    label "uporz&#261;dkowanie"
  ]
  node [
    id 213
    label "egzekutywa"
  ]
  node [
    id 214
    label "premier"
  ]
  node [
    id 215
    label "Londyn"
  ]
  node [
    id 216
    label "gabinet_cieni"
  ]
  node [
    id 217
    label "number"
  ]
  node [
    id 218
    label "Konsulat"
  ]
  node [
    id 219
    label "tract"
  ]
  node [
    id 220
    label "klasa"
  ]
  node [
    id 221
    label "w&#322;adza"
  ]
  node [
    id 222
    label "folk_music"
  ]
  node [
    id 223
    label "sztolnia"
  ]
  node [
    id 224
    label "wyrobisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
]
