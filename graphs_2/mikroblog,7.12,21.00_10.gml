graph [
  node [
    id 0
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 1
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 2
    label "raz"
    origin "text"
  ]
  node [
    id 3
    label "ogl&#261;da&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "viv&#281;"
    origin "text"
  ]
  node [
    id 6
    label "ten"
    origin "text"
  ]
  node [
    id 7
    label "wpis"
    origin "text"
  ]
  node [
    id 8
    label "time"
  ]
  node [
    id 9
    label "cios"
  ]
  node [
    id 10
    label "chwila"
  ]
  node [
    id 11
    label "uderzenie"
  ]
  node [
    id 12
    label "blok"
  ]
  node [
    id 13
    label "shot"
  ]
  node [
    id 14
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 15
    label "struktura_geologiczna"
  ]
  node [
    id 16
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 17
    label "pr&#243;ba"
  ]
  node [
    id 18
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 19
    label "coup"
  ]
  node [
    id 20
    label "siekacz"
  ]
  node [
    id 21
    label "instrumentalizacja"
  ]
  node [
    id 22
    label "trafienie"
  ]
  node [
    id 23
    label "walka"
  ]
  node [
    id 24
    label "zdarzenie_si&#281;"
  ]
  node [
    id 25
    label "wdarcie_si&#281;"
  ]
  node [
    id 26
    label "pogorszenie"
  ]
  node [
    id 27
    label "d&#378;wi&#281;k"
  ]
  node [
    id 28
    label "poczucie"
  ]
  node [
    id 29
    label "reakcja"
  ]
  node [
    id 30
    label "contact"
  ]
  node [
    id 31
    label "stukni&#281;cie"
  ]
  node [
    id 32
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 33
    label "bat"
  ]
  node [
    id 34
    label "spowodowanie"
  ]
  node [
    id 35
    label "rush"
  ]
  node [
    id 36
    label "odbicie"
  ]
  node [
    id 37
    label "dawka"
  ]
  node [
    id 38
    label "zadanie"
  ]
  node [
    id 39
    label "&#347;ci&#281;cie"
  ]
  node [
    id 40
    label "st&#322;uczenie"
  ]
  node [
    id 41
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 42
    label "odbicie_si&#281;"
  ]
  node [
    id 43
    label "dotkni&#281;cie"
  ]
  node [
    id 44
    label "charge"
  ]
  node [
    id 45
    label "dostanie"
  ]
  node [
    id 46
    label "skrytykowanie"
  ]
  node [
    id 47
    label "zagrywka"
  ]
  node [
    id 48
    label "manewr"
  ]
  node [
    id 49
    label "nast&#261;pienie"
  ]
  node [
    id 50
    label "uderzanie"
  ]
  node [
    id 51
    label "pogoda"
  ]
  node [
    id 52
    label "stroke"
  ]
  node [
    id 53
    label "pobicie"
  ]
  node [
    id 54
    label "ruch"
  ]
  node [
    id 55
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 56
    label "flap"
  ]
  node [
    id 57
    label "dotyk"
  ]
  node [
    id 58
    label "zrobienie"
  ]
  node [
    id 59
    label "czas"
  ]
  node [
    id 60
    label "po_niemiecku"
  ]
  node [
    id 61
    label "German"
  ]
  node [
    id 62
    label "niemiecko"
  ]
  node [
    id 63
    label "cenar"
  ]
  node [
    id 64
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 65
    label "europejski"
  ]
  node [
    id 66
    label "strudel"
  ]
  node [
    id 67
    label "niemiec"
  ]
  node [
    id 68
    label "pionier"
  ]
  node [
    id 69
    label "zachodnioeuropejski"
  ]
  node [
    id 70
    label "j&#281;zyk"
  ]
  node [
    id 71
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 72
    label "junkers"
  ]
  node [
    id 73
    label "szwabski"
  ]
  node [
    id 74
    label "szwabsko"
  ]
  node [
    id 75
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 76
    label "po_szwabsku"
  ]
  node [
    id 77
    label "platt"
  ]
  node [
    id 78
    label "europejsko"
  ]
  node [
    id 79
    label "&#380;o&#322;nierz"
  ]
  node [
    id 80
    label "saper"
  ]
  node [
    id 81
    label "prekursor"
  ]
  node [
    id 82
    label "osadnik"
  ]
  node [
    id 83
    label "skaut"
  ]
  node [
    id 84
    label "g&#322;osiciel"
  ]
  node [
    id 85
    label "ciasto"
  ]
  node [
    id 86
    label "taniec_ludowy"
  ]
  node [
    id 87
    label "melodia"
  ]
  node [
    id 88
    label "taniec"
  ]
  node [
    id 89
    label "podgrzewacz"
  ]
  node [
    id 90
    label "samolot_wojskowy"
  ]
  node [
    id 91
    label "moreska"
  ]
  node [
    id 92
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 93
    label "zachodni"
  ]
  node [
    id 94
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 95
    label "langosz"
  ]
  node [
    id 96
    label "po_europejsku"
  ]
  node [
    id 97
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 98
    label "European"
  ]
  node [
    id 99
    label "typowy"
  ]
  node [
    id 100
    label "charakterystyczny"
  ]
  node [
    id 101
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 102
    label "artykulator"
  ]
  node [
    id 103
    label "kod"
  ]
  node [
    id 104
    label "kawa&#322;ek"
  ]
  node [
    id 105
    label "przedmiot"
  ]
  node [
    id 106
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 107
    label "gramatyka"
  ]
  node [
    id 108
    label "stylik"
  ]
  node [
    id 109
    label "przet&#322;umaczenie"
  ]
  node [
    id 110
    label "formalizowanie"
  ]
  node [
    id 111
    label "ssanie"
  ]
  node [
    id 112
    label "ssa&#263;"
  ]
  node [
    id 113
    label "language"
  ]
  node [
    id 114
    label "liza&#263;"
  ]
  node [
    id 115
    label "napisa&#263;"
  ]
  node [
    id 116
    label "konsonantyzm"
  ]
  node [
    id 117
    label "wokalizm"
  ]
  node [
    id 118
    label "pisa&#263;"
  ]
  node [
    id 119
    label "fonetyka"
  ]
  node [
    id 120
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 121
    label "jeniec"
  ]
  node [
    id 122
    label "but"
  ]
  node [
    id 123
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 124
    label "po_koroniarsku"
  ]
  node [
    id 125
    label "kultura_duchowa"
  ]
  node [
    id 126
    label "t&#322;umaczenie"
  ]
  node [
    id 127
    label "m&#243;wienie"
  ]
  node [
    id 128
    label "pype&#263;"
  ]
  node [
    id 129
    label "lizanie"
  ]
  node [
    id 130
    label "pismo"
  ]
  node [
    id 131
    label "formalizowa&#263;"
  ]
  node [
    id 132
    label "rozumie&#263;"
  ]
  node [
    id 133
    label "organ"
  ]
  node [
    id 134
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 135
    label "rozumienie"
  ]
  node [
    id 136
    label "spos&#243;b"
  ]
  node [
    id 137
    label "makroglosja"
  ]
  node [
    id 138
    label "m&#243;wi&#263;"
  ]
  node [
    id 139
    label "jama_ustna"
  ]
  node [
    id 140
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 141
    label "formacja_geologiczna"
  ]
  node [
    id 142
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 143
    label "natural_language"
  ]
  node [
    id 144
    label "s&#322;ownictwo"
  ]
  node [
    id 145
    label "urz&#261;dzenie"
  ]
  node [
    id 146
    label "okre&#347;lony"
  ]
  node [
    id 147
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 148
    label "wiadomy"
  ]
  node [
    id 149
    label "inscription"
  ]
  node [
    id 150
    label "op&#322;ata"
  ]
  node [
    id 151
    label "akt"
  ]
  node [
    id 152
    label "tekst"
  ]
  node [
    id 153
    label "czynno&#347;&#263;"
  ]
  node [
    id 154
    label "entrance"
  ]
  node [
    id 155
    label "ekscerpcja"
  ]
  node [
    id 156
    label "j&#281;zykowo"
  ]
  node [
    id 157
    label "wypowied&#378;"
  ]
  node [
    id 158
    label "redakcja"
  ]
  node [
    id 159
    label "wytw&#243;r"
  ]
  node [
    id 160
    label "pomini&#281;cie"
  ]
  node [
    id 161
    label "dzie&#322;o"
  ]
  node [
    id 162
    label "preparacja"
  ]
  node [
    id 163
    label "odmianka"
  ]
  node [
    id 164
    label "opu&#347;ci&#263;"
  ]
  node [
    id 165
    label "koniektura"
  ]
  node [
    id 166
    label "obelga"
  ]
  node [
    id 167
    label "kwota"
  ]
  node [
    id 168
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 169
    label "activity"
  ]
  node [
    id 170
    label "bezproblemowy"
  ]
  node [
    id 171
    label "wydarzenie"
  ]
  node [
    id 172
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 173
    label "podnieci&#263;"
  ]
  node [
    id 174
    label "scena"
  ]
  node [
    id 175
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 176
    label "numer"
  ]
  node [
    id 177
    label "po&#380;ycie"
  ]
  node [
    id 178
    label "poj&#281;cie"
  ]
  node [
    id 179
    label "podniecenie"
  ]
  node [
    id 180
    label "nago&#347;&#263;"
  ]
  node [
    id 181
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 182
    label "fascyku&#322;"
  ]
  node [
    id 183
    label "seks"
  ]
  node [
    id 184
    label "podniecanie"
  ]
  node [
    id 185
    label "imisja"
  ]
  node [
    id 186
    label "zwyczaj"
  ]
  node [
    id 187
    label "rozmna&#380;anie"
  ]
  node [
    id 188
    label "ruch_frykcyjny"
  ]
  node [
    id 189
    label "ontologia"
  ]
  node [
    id 190
    label "na_pieska"
  ]
  node [
    id 191
    label "pozycja_misjonarska"
  ]
  node [
    id 192
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 193
    label "fragment"
  ]
  node [
    id 194
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 195
    label "z&#322;&#261;czenie"
  ]
  node [
    id 196
    label "gra_wst&#281;pna"
  ]
  node [
    id 197
    label "erotyka"
  ]
  node [
    id 198
    label "urzeczywistnienie"
  ]
  node [
    id 199
    label "baraszki"
  ]
  node [
    id 200
    label "certificate"
  ]
  node [
    id 201
    label "po&#380;&#261;danie"
  ]
  node [
    id 202
    label "wzw&#243;d"
  ]
  node [
    id 203
    label "funkcja"
  ]
  node [
    id 204
    label "act"
  ]
  node [
    id 205
    label "dokument"
  ]
  node [
    id 206
    label "arystotelizm"
  ]
  node [
    id 207
    label "podnieca&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
]
