graph [
  node [
    id 0
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 1
    label "odkrycie"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "podwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 5
    label "informacja"
    origin "text"
  ]
  node [
    id 6
    label "przekazywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dawny"
    origin "text"
  ]
  node [
    id 8
    label "temat"
    origin "text"
  ]
  node [
    id 9
    label "elektrownia"
    origin "text"
  ]
  node [
    id 10
    label "cause"
  ]
  node [
    id 11
    label "przesta&#263;"
  ]
  node [
    id 12
    label "communicate"
  ]
  node [
    id 13
    label "zrobi&#263;"
  ]
  node [
    id 14
    label "post&#261;pi&#263;"
  ]
  node [
    id 15
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 16
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 17
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 18
    label "zorganizowa&#263;"
  ]
  node [
    id 19
    label "appoint"
  ]
  node [
    id 20
    label "wystylizowa&#263;"
  ]
  node [
    id 21
    label "przerobi&#263;"
  ]
  node [
    id 22
    label "nabra&#263;"
  ]
  node [
    id 23
    label "make"
  ]
  node [
    id 24
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 25
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 26
    label "wydali&#263;"
  ]
  node [
    id 27
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 28
    label "coating"
  ]
  node [
    id 29
    label "drop"
  ]
  node [
    id 30
    label "sko&#324;czy&#263;"
  ]
  node [
    id 31
    label "leave_office"
  ]
  node [
    id 32
    label "fail"
  ]
  node [
    id 33
    label "ukazanie"
  ]
  node [
    id 34
    label "detection"
  ]
  node [
    id 35
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 36
    label "podniesienie"
  ]
  node [
    id 37
    label "discovery"
  ]
  node [
    id 38
    label "poznanie"
  ]
  node [
    id 39
    label "novum"
  ]
  node [
    id 40
    label "disclosure"
  ]
  node [
    id 41
    label "zsuni&#281;cie"
  ]
  node [
    id 42
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 43
    label "znalezienie"
  ]
  node [
    id 44
    label "jawny"
  ]
  node [
    id 45
    label "niespodzianka"
  ]
  node [
    id 46
    label "objawienie"
  ]
  node [
    id 47
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 48
    label "poinformowanie"
  ]
  node [
    id 49
    label "acquaintance"
  ]
  node [
    id 50
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 51
    label "spotkanie"
  ]
  node [
    id 52
    label "nauczenie_si&#281;"
  ]
  node [
    id 53
    label "poczucie"
  ]
  node [
    id 54
    label "knowing"
  ]
  node [
    id 55
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 56
    label "zapoznanie_si&#281;"
  ]
  node [
    id 57
    label "wy&#347;wiadczenie"
  ]
  node [
    id 58
    label "inclusion"
  ]
  node [
    id 59
    label "zrozumienie"
  ]
  node [
    id 60
    label "zawarcie"
  ]
  node [
    id 61
    label "designation"
  ]
  node [
    id 62
    label "umo&#380;liwienie"
  ]
  node [
    id 63
    label "sensing"
  ]
  node [
    id 64
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 65
    label "gathering"
  ]
  node [
    id 66
    label "czynno&#347;&#263;"
  ]
  node [
    id 67
    label "zapoznanie"
  ]
  node [
    id 68
    label "znajomy"
  ]
  node [
    id 69
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 70
    label "forma"
  ]
  node [
    id 71
    label "zrobienie"
  ]
  node [
    id 72
    label "z&#322;&#261;czenie"
  ]
  node [
    id 73
    label "zdj&#281;cie"
  ]
  node [
    id 74
    label "opuszczenie"
  ]
  node [
    id 75
    label "stoczenie"
  ]
  node [
    id 76
    label "powi&#281;kszenie"
  ]
  node [
    id 77
    label "movement"
  ]
  node [
    id 78
    label "raise"
  ]
  node [
    id 79
    label "obrz&#281;d"
  ]
  node [
    id 80
    label "przewr&#243;cenie"
  ]
  node [
    id 81
    label "pomo&#380;enie"
  ]
  node [
    id 82
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 83
    label "erection"
  ]
  node [
    id 84
    label "za&#322;apanie"
  ]
  node [
    id 85
    label "spowodowanie"
  ]
  node [
    id 86
    label "ulepszenie"
  ]
  node [
    id 87
    label "policzenie"
  ]
  node [
    id 88
    label "przybli&#380;enie"
  ]
  node [
    id 89
    label "erecting"
  ]
  node [
    id 90
    label "msza"
  ]
  node [
    id 91
    label "przemieszczenie"
  ]
  node [
    id 92
    label "pochwalenie"
  ]
  node [
    id 93
    label "wywy&#380;szenie"
  ]
  node [
    id 94
    label "wy&#380;szy"
  ]
  node [
    id 95
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 96
    label "zacz&#281;cie"
  ]
  node [
    id 97
    label "zmienienie"
  ]
  node [
    id 98
    label "odbudowanie"
  ]
  node [
    id 99
    label "telling"
  ]
  node [
    id 100
    label "knickknack"
  ]
  node [
    id 101
    label "przedmiot"
  ]
  node [
    id 102
    label "nowo&#347;&#263;"
  ]
  node [
    id 103
    label "zbi&#243;r"
  ]
  node [
    id 104
    label "dorobek"
  ]
  node [
    id 105
    label "tworzenie"
  ]
  node [
    id 106
    label "kreacja"
  ]
  node [
    id 107
    label "creation"
  ]
  node [
    id 108
    label "kultura"
  ]
  node [
    id 109
    label "surprise"
  ]
  node [
    id 110
    label "prezent"
  ]
  node [
    id 111
    label "siurpryza"
  ]
  node [
    id 112
    label "wydarzenie"
  ]
  node [
    id 113
    label "pokazanie"
  ]
  node [
    id 114
    label "przedstawienie"
  ]
  node [
    id 115
    label "postaranie_si&#281;"
  ]
  node [
    id 116
    label "doznanie"
  ]
  node [
    id 117
    label "wymy&#347;lenie"
  ]
  node [
    id 118
    label "determination"
  ]
  node [
    id 119
    label "dorwanie"
  ]
  node [
    id 120
    label "zdarzenie_si&#281;"
  ]
  node [
    id 121
    label "znalezienie_si&#281;"
  ]
  node [
    id 122
    label "wykrycie"
  ]
  node [
    id 123
    label "poszukanie"
  ]
  node [
    id 124
    label "invention"
  ]
  node [
    id 125
    label "pozyskanie"
  ]
  node [
    id 126
    label "katolicyzm"
  ]
  node [
    id 127
    label "term"
  ]
  node [
    id 128
    label "tradycja"
  ]
  node [
    id 129
    label "sformu&#322;owanie"
  ]
  node [
    id 130
    label "ujawnienie"
  ]
  node [
    id 131
    label "light"
  ]
  node [
    id 132
    label "przes&#322;anie"
  ]
  node [
    id 133
    label "zjawisko"
  ]
  node [
    id 134
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 135
    label "ujawnienie_si&#281;"
  ]
  node [
    id 136
    label "ujawnianie_si&#281;"
  ]
  node [
    id 137
    label "zdecydowany"
  ]
  node [
    id 138
    label "jawnie"
  ]
  node [
    id 139
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 140
    label "ujawnianie"
  ]
  node [
    id 141
    label "ewidentny"
  ]
  node [
    id 142
    label "podnosi&#263;"
  ]
  node [
    id 143
    label "sabotage"
  ]
  node [
    id 144
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 145
    label "zaczyna&#263;"
  ]
  node [
    id 146
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 147
    label "escalate"
  ]
  node [
    id 148
    label "pia&#263;"
  ]
  node [
    id 149
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 150
    label "przybli&#380;a&#263;"
  ]
  node [
    id 151
    label "ulepsza&#263;"
  ]
  node [
    id 152
    label "tire"
  ]
  node [
    id 153
    label "pomaga&#263;"
  ]
  node [
    id 154
    label "liczy&#263;"
  ]
  node [
    id 155
    label "express"
  ]
  node [
    id 156
    label "przemieszcza&#263;"
  ]
  node [
    id 157
    label "chwali&#263;"
  ]
  node [
    id 158
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 159
    label "rise"
  ]
  node [
    id 160
    label "os&#322;awia&#263;"
  ]
  node [
    id 161
    label "odbudowywa&#263;"
  ]
  node [
    id 162
    label "drive"
  ]
  node [
    id 163
    label "zmienia&#263;"
  ]
  node [
    id 164
    label "enhance"
  ]
  node [
    id 165
    label "za&#322;apywa&#263;"
  ]
  node [
    id 166
    label "lift"
  ]
  node [
    id 167
    label "wynios&#322;y"
  ]
  node [
    id 168
    label "dono&#347;ny"
  ]
  node [
    id 169
    label "silny"
  ]
  node [
    id 170
    label "wa&#380;nie"
  ]
  node [
    id 171
    label "istotnie"
  ]
  node [
    id 172
    label "znaczny"
  ]
  node [
    id 173
    label "eksponowany"
  ]
  node [
    id 174
    label "dobry"
  ]
  node [
    id 175
    label "dobroczynny"
  ]
  node [
    id 176
    label "czw&#243;rka"
  ]
  node [
    id 177
    label "spokojny"
  ]
  node [
    id 178
    label "skuteczny"
  ]
  node [
    id 179
    label "&#347;mieszny"
  ]
  node [
    id 180
    label "mi&#322;y"
  ]
  node [
    id 181
    label "grzeczny"
  ]
  node [
    id 182
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 183
    label "powitanie"
  ]
  node [
    id 184
    label "dobrze"
  ]
  node [
    id 185
    label "ca&#322;y"
  ]
  node [
    id 186
    label "zwrot"
  ]
  node [
    id 187
    label "pomy&#347;lny"
  ]
  node [
    id 188
    label "moralny"
  ]
  node [
    id 189
    label "drogi"
  ]
  node [
    id 190
    label "pozytywny"
  ]
  node [
    id 191
    label "odpowiedni"
  ]
  node [
    id 192
    label "korzystny"
  ]
  node [
    id 193
    label "pos&#322;uszny"
  ]
  node [
    id 194
    label "niedost&#281;pny"
  ]
  node [
    id 195
    label "pot&#281;&#380;ny"
  ]
  node [
    id 196
    label "wysoki"
  ]
  node [
    id 197
    label "wynio&#347;le"
  ]
  node [
    id 198
    label "dumny"
  ]
  node [
    id 199
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 200
    label "znacznie"
  ]
  node [
    id 201
    label "zauwa&#380;alny"
  ]
  node [
    id 202
    label "intensywny"
  ]
  node [
    id 203
    label "krzepienie"
  ]
  node [
    id 204
    label "&#380;ywotny"
  ]
  node [
    id 205
    label "mocny"
  ]
  node [
    id 206
    label "pokrzepienie"
  ]
  node [
    id 207
    label "niepodwa&#380;alny"
  ]
  node [
    id 208
    label "du&#380;y"
  ]
  node [
    id 209
    label "mocno"
  ]
  node [
    id 210
    label "przekonuj&#261;cy"
  ]
  node [
    id 211
    label "wytrzyma&#322;y"
  ]
  node [
    id 212
    label "konkretny"
  ]
  node [
    id 213
    label "zdrowy"
  ]
  node [
    id 214
    label "silnie"
  ]
  node [
    id 215
    label "meflochina"
  ]
  node [
    id 216
    label "zajebisty"
  ]
  node [
    id 217
    label "istotny"
  ]
  node [
    id 218
    label "realnie"
  ]
  node [
    id 219
    label "importantly"
  ]
  node [
    id 220
    label "gromowy"
  ]
  node [
    id 221
    label "dono&#347;nie"
  ]
  node [
    id 222
    label "g&#322;o&#347;ny"
  ]
  node [
    id 223
    label "punkt"
  ]
  node [
    id 224
    label "publikacja"
  ]
  node [
    id 225
    label "wiedza"
  ]
  node [
    id 226
    label "doj&#347;cie"
  ]
  node [
    id 227
    label "obiega&#263;"
  ]
  node [
    id 228
    label "powzi&#281;cie"
  ]
  node [
    id 229
    label "dane"
  ]
  node [
    id 230
    label "obiegni&#281;cie"
  ]
  node [
    id 231
    label "sygna&#322;"
  ]
  node [
    id 232
    label "obieganie"
  ]
  node [
    id 233
    label "powzi&#261;&#263;"
  ]
  node [
    id 234
    label "obiec"
  ]
  node [
    id 235
    label "doj&#347;&#263;"
  ]
  node [
    id 236
    label "po&#322;o&#380;enie"
  ]
  node [
    id 237
    label "sprawa"
  ]
  node [
    id 238
    label "ust&#281;p"
  ]
  node [
    id 239
    label "plan"
  ]
  node [
    id 240
    label "obiekt_matematyczny"
  ]
  node [
    id 241
    label "problemat"
  ]
  node [
    id 242
    label "plamka"
  ]
  node [
    id 243
    label "stopie&#324;_pisma"
  ]
  node [
    id 244
    label "jednostka"
  ]
  node [
    id 245
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 246
    label "miejsce"
  ]
  node [
    id 247
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 248
    label "mark"
  ]
  node [
    id 249
    label "chwila"
  ]
  node [
    id 250
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 251
    label "prosta"
  ]
  node [
    id 252
    label "problematyka"
  ]
  node [
    id 253
    label "obiekt"
  ]
  node [
    id 254
    label "zapunktowa&#263;"
  ]
  node [
    id 255
    label "podpunkt"
  ]
  node [
    id 256
    label "wojsko"
  ]
  node [
    id 257
    label "kres"
  ]
  node [
    id 258
    label "przestrze&#324;"
  ]
  node [
    id 259
    label "point"
  ]
  node [
    id 260
    label "pozycja"
  ]
  node [
    id 261
    label "cognition"
  ]
  node [
    id 262
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 263
    label "intelekt"
  ]
  node [
    id 264
    label "pozwolenie"
  ]
  node [
    id 265
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 266
    label "zaawansowanie"
  ]
  node [
    id 267
    label "wykszta&#322;cenie"
  ]
  node [
    id 268
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 269
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 270
    label "pulsation"
  ]
  node [
    id 271
    label "przekazywanie"
  ]
  node [
    id 272
    label "przewodzenie"
  ]
  node [
    id 273
    label "d&#378;wi&#281;k"
  ]
  node [
    id 274
    label "po&#322;&#261;czenie"
  ]
  node [
    id 275
    label "fala"
  ]
  node [
    id 276
    label "przekazanie"
  ]
  node [
    id 277
    label "przewodzi&#263;"
  ]
  node [
    id 278
    label "znak"
  ]
  node [
    id 279
    label "zapowied&#378;"
  ]
  node [
    id 280
    label "medium_transmisyjne"
  ]
  node [
    id 281
    label "demodulacja"
  ]
  node [
    id 282
    label "przekaza&#263;"
  ]
  node [
    id 283
    label "czynnik"
  ]
  node [
    id 284
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 285
    label "aliasing"
  ]
  node [
    id 286
    label "wizja"
  ]
  node [
    id 287
    label "modulacja"
  ]
  node [
    id 288
    label "drift"
  ]
  node [
    id 289
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 290
    label "tekst"
  ]
  node [
    id 291
    label "druk"
  ]
  node [
    id 292
    label "produkcja"
  ]
  node [
    id 293
    label "notification"
  ]
  node [
    id 294
    label "edytowa&#263;"
  ]
  node [
    id 295
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 296
    label "spakowanie"
  ]
  node [
    id 297
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 298
    label "pakowa&#263;"
  ]
  node [
    id 299
    label "rekord"
  ]
  node [
    id 300
    label "korelator"
  ]
  node [
    id 301
    label "wyci&#261;ganie"
  ]
  node [
    id 302
    label "pakowanie"
  ]
  node [
    id 303
    label "sekwencjonowa&#263;"
  ]
  node [
    id 304
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 305
    label "jednostka_informacji"
  ]
  node [
    id 306
    label "evidence"
  ]
  node [
    id 307
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 308
    label "rozpakowywanie"
  ]
  node [
    id 309
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 310
    label "rozpakowanie"
  ]
  node [
    id 311
    label "rozpakowywa&#263;"
  ]
  node [
    id 312
    label "konwersja"
  ]
  node [
    id 313
    label "nap&#322;ywanie"
  ]
  node [
    id 314
    label "rozpakowa&#263;"
  ]
  node [
    id 315
    label "spakowa&#263;"
  ]
  node [
    id 316
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 317
    label "edytowanie"
  ]
  node [
    id 318
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 319
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 320
    label "sekwencjonowanie"
  ]
  node [
    id 321
    label "flow"
  ]
  node [
    id 322
    label "odwiedza&#263;"
  ]
  node [
    id 323
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 324
    label "rotate"
  ]
  node [
    id 325
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 326
    label "authorize"
  ]
  node [
    id 327
    label "podj&#261;&#263;"
  ]
  node [
    id 328
    label "zacz&#261;&#263;"
  ]
  node [
    id 329
    label "otrzyma&#263;"
  ]
  node [
    id 330
    label "sta&#263;_si&#281;"
  ]
  node [
    id 331
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 332
    label "supervene"
  ]
  node [
    id 333
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 334
    label "zaj&#347;&#263;"
  ]
  node [
    id 335
    label "catch"
  ]
  node [
    id 336
    label "get"
  ]
  node [
    id 337
    label "bodziec"
  ]
  node [
    id 338
    label "przesy&#322;ka"
  ]
  node [
    id 339
    label "dodatek"
  ]
  node [
    id 340
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 341
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 342
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 343
    label "heed"
  ]
  node [
    id 344
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 345
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 346
    label "spowodowa&#263;"
  ]
  node [
    id 347
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 348
    label "dozna&#263;"
  ]
  node [
    id 349
    label "dokoptowa&#263;"
  ]
  node [
    id 350
    label "postrzega&#263;"
  ]
  node [
    id 351
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 352
    label "orgazm"
  ]
  node [
    id 353
    label "dolecie&#263;"
  ]
  node [
    id 354
    label "dotrze&#263;"
  ]
  node [
    id 355
    label "uzyska&#263;"
  ]
  node [
    id 356
    label "dop&#322;ata"
  ]
  node [
    id 357
    label "become"
  ]
  node [
    id 358
    label "odwiedzi&#263;"
  ]
  node [
    id 359
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 360
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 361
    label "orb"
  ]
  node [
    id 362
    label "podj&#281;cie"
  ]
  node [
    id 363
    label "otrzymanie"
  ]
  node [
    id 364
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 365
    label "dochodzenie"
  ]
  node [
    id 366
    label "uzyskanie"
  ]
  node [
    id 367
    label "skill"
  ]
  node [
    id 368
    label "dochrapanie_si&#281;"
  ]
  node [
    id 369
    label "znajomo&#347;ci"
  ]
  node [
    id 370
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 371
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 372
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 373
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 374
    label "powi&#261;zanie"
  ]
  node [
    id 375
    label "entrance"
  ]
  node [
    id 376
    label "affiliation"
  ]
  node [
    id 377
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 378
    label "dor&#281;czenie"
  ]
  node [
    id 379
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 380
    label "dost&#281;p"
  ]
  node [
    id 381
    label "gotowy"
  ]
  node [
    id 382
    label "avenue"
  ]
  node [
    id 383
    label "postrzeganie"
  ]
  node [
    id 384
    label "dojrza&#322;y"
  ]
  node [
    id 385
    label "dojechanie"
  ]
  node [
    id 386
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 387
    label "ingress"
  ]
  node [
    id 388
    label "strzelenie"
  ]
  node [
    id 389
    label "orzekni&#281;cie"
  ]
  node [
    id 390
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 391
    label "dolecenie"
  ]
  node [
    id 392
    label "rozpowszechnienie"
  ]
  node [
    id 393
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 394
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 395
    label "stanie_si&#281;"
  ]
  node [
    id 396
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 397
    label "odwiedzanie"
  ]
  node [
    id 398
    label "biegni&#281;cie"
  ]
  node [
    id 399
    label "zakre&#347;lanie"
  ]
  node [
    id 400
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 401
    label "okr&#261;&#380;anie"
  ]
  node [
    id 402
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 403
    label "zakre&#347;lenie"
  ]
  node [
    id 404
    label "odwiedzenie"
  ]
  node [
    id 405
    label "okr&#261;&#380;enie"
  ]
  node [
    id 406
    label "wysy&#322;a&#263;"
  ]
  node [
    id 407
    label "podawa&#263;"
  ]
  node [
    id 408
    label "give"
  ]
  node [
    id 409
    label "wp&#322;aca&#263;"
  ]
  node [
    id 410
    label "powodowa&#263;"
  ]
  node [
    id 411
    label "impart"
  ]
  node [
    id 412
    label "mie&#263;_miejsce"
  ]
  node [
    id 413
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 414
    label "motywowa&#263;"
  ]
  node [
    id 415
    label "act"
  ]
  node [
    id 416
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 417
    label "p&#322;aci&#263;"
  ]
  node [
    id 418
    label "dispatch"
  ]
  node [
    id 419
    label "wytwarza&#263;"
  ]
  node [
    id 420
    label "nakazywa&#263;"
  ]
  node [
    id 421
    label "order"
  ]
  node [
    id 422
    label "grant"
  ]
  node [
    id 423
    label "tenis"
  ]
  node [
    id 424
    label "deal"
  ]
  node [
    id 425
    label "dawa&#263;"
  ]
  node [
    id 426
    label "stawia&#263;"
  ]
  node [
    id 427
    label "rozgrywa&#263;"
  ]
  node [
    id 428
    label "kelner"
  ]
  node [
    id 429
    label "siatk&#243;wka"
  ]
  node [
    id 430
    label "cover"
  ]
  node [
    id 431
    label "tender"
  ]
  node [
    id 432
    label "jedzenie"
  ]
  node [
    id 433
    label "faszerowa&#263;"
  ]
  node [
    id 434
    label "introduce"
  ]
  node [
    id 435
    label "informowa&#263;"
  ]
  node [
    id 436
    label "serwowa&#263;"
  ]
  node [
    id 437
    label "przestarza&#322;y"
  ]
  node [
    id 438
    label "odleg&#322;y"
  ]
  node [
    id 439
    label "przesz&#322;y"
  ]
  node [
    id 440
    label "od_dawna"
  ]
  node [
    id 441
    label "poprzedni"
  ]
  node [
    id 442
    label "dawno"
  ]
  node [
    id 443
    label "d&#322;ugoletni"
  ]
  node [
    id 444
    label "anachroniczny"
  ]
  node [
    id 445
    label "dawniej"
  ]
  node [
    id 446
    label "niegdysiejszy"
  ]
  node [
    id 447
    label "wcze&#347;niejszy"
  ]
  node [
    id 448
    label "kombatant"
  ]
  node [
    id 449
    label "stary"
  ]
  node [
    id 450
    label "poprzednio"
  ]
  node [
    id 451
    label "anachronicznie"
  ]
  node [
    id 452
    label "niezgodny"
  ]
  node [
    id 453
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 454
    label "zestarzenie_si&#281;"
  ]
  node [
    id 455
    label "starzenie_si&#281;"
  ]
  node [
    id 456
    label "archaicznie"
  ]
  node [
    id 457
    label "zgrzybienie"
  ]
  node [
    id 458
    label "niedzisiejszy"
  ]
  node [
    id 459
    label "staro&#347;wiecki"
  ]
  node [
    id 460
    label "przestarzale"
  ]
  node [
    id 461
    label "ongi&#347;"
  ]
  node [
    id 462
    label "odlegle"
  ]
  node [
    id 463
    label "delikatny"
  ]
  node [
    id 464
    label "r&#243;&#380;ny"
  ]
  node [
    id 465
    label "daleko"
  ]
  node [
    id 466
    label "s&#322;aby"
  ]
  node [
    id 467
    label "daleki"
  ]
  node [
    id 468
    label "oddalony"
  ]
  node [
    id 469
    label "obcy"
  ]
  node [
    id 470
    label "nieobecny"
  ]
  node [
    id 471
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 472
    label "ojciec"
  ]
  node [
    id 473
    label "nienowoczesny"
  ]
  node [
    id 474
    label "gruba_ryba"
  ]
  node [
    id 475
    label "staro"
  ]
  node [
    id 476
    label "m&#261;&#380;"
  ]
  node [
    id 477
    label "starzy"
  ]
  node [
    id 478
    label "dotychczasowy"
  ]
  node [
    id 479
    label "p&#243;&#378;ny"
  ]
  node [
    id 480
    label "charakterystyczny"
  ]
  node [
    id 481
    label "brat"
  ]
  node [
    id 482
    label "po_staro&#347;wiecku"
  ]
  node [
    id 483
    label "zwierzchnik"
  ]
  node [
    id 484
    label "starczo"
  ]
  node [
    id 485
    label "wcze&#347;niej"
  ]
  node [
    id 486
    label "miniony"
  ]
  node [
    id 487
    label "ostatni"
  ]
  node [
    id 488
    label "d&#322;ugi"
  ]
  node [
    id 489
    label "wieloletni"
  ]
  node [
    id 490
    label "kiedy&#347;"
  ]
  node [
    id 491
    label "d&#322;ugotrwale"
  ]
  node [
    id 492
    label "dawnie"
  ]
  node [
    id 493
    label "wyjadacz"
  ]
  node [
    id 494
    label "weteran"
  ]
  node [
    id 495
    label "wyraz_pochodny"
  ]
  node [
    id 496
    label "zboczenie"
  ]
  node [
    id 497
    label "om&#243;wienie"
  ]
  node [
    id 498
    label "cecha"
  ]
  node [
    id 499
    label "rzecz"
  ]
  node [
    id 500
    label "omawia&#263;"
  ]
  node [
    id 501
    label "fraza"
  ]
  node [
    id 502
    label "tre&#347;&#263;"
  ]
  node [
    id 503
    label "entity"
  ]
  node [
    id 504
    label "forum"
  ]
  node [
    id 505
    label "topik"
  ]
  node [
    id 506
    label "tematyka"
  ]
  node [
    id 507
    label "w&#261;tek"
  ]
  node [
    id 508
    label "zbaczanie"
  ]
  node [
    id 509
    label "om&#243;wi&#263;"
  ]
  node [
    id 510
    label "omawianie"
  ]
  node [
    id 511
    label "melodia"
  ]
  node [
    id 512
    label "otoczka"
  ]
  node [
    id 513
    label "istota"
  ]
  node [
    id 514
    label "zbacza&#263;"
  ]
  node [
    id 515
    label "zboczy&#263;"
  ]
  node [
    id 516
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 517
    label "wypowiedzenie"
  ]
  node [
    id 518
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 519
    label "zdanie"
  ]
  node [
    id 520
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 521
    label "motyw"
  ]
  node [
    id 522
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 523
    label "zawarto&#347;&#263;"
  ]
  node [
    id 524
    label "kszta&#322;t"
  ]
  node [
    id 525
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 526
    label "jednostka_systematyczna"
  ]
  node [
    id 527
    label "leksem"
  ]
  node [
    id 528
    label "dzie&#322;o"
  ]
  node [
    id 529
    label "stan"
  ]
  node [
    id 530
    label "blaszka"
  ]
  node [
    id 531
    label "poj&#281;cie"
  ]
  node [
    id 532
    label "kantyzm"
  ]
  node [
    id 533
    label "zdolno&#347;&#263;"
  ]
  node [
    id 534
    label "do&#322;ek"
  ]
  node [
    id 535
    label "gwiazda"
  ]
  node [
    id 536
    label "formality"
  ]
  node [
    id 537
    label "struktura"
  ]
  node [
    id 538
    label "wygl&#261;d"
  ]
  node [
    id 539
    label "mode"
  ]
  node [
    id 540
    label "morfem"
  ]
  node [
    id 541
    label "rdze&#324;"
  ]
  node [
    id 542
    label "posta&#263;"
  ]
  node [
    id 543
    label "kielich"
  ]
  node [
    id 544
    label "ornamentyka"
  ]
  node [
    id 545
    label "pasmo"
  ]
  node [
    id 546
    label "zwyczaj"
  ]
  node [
    id 547
    label "punkt_widzenia"
  ]
  node [
    id 548
    label "g&#322;owa"
  ]
  node [
    id 549
    label "naczynie"
  ]
  node [
    id 550
    label "p&#322;at"
  ]
  node [
    id 551
    label "maszyna_drukarska"
  ]
  node [
    id 552
    label "style"
  ]
  node [
    id 553
    label "linearno&#347;&#263;"
  ]
  node [
    id 554
    label "wyra&#380;enie"
  ]
  node [
    id 555
    label "formacja"
  ]
  node [
    id 556
    label "spirala"
  ]
  node [
    id 557
    label "dyspozycja"
  ]
  node [
    id 558
    label "odmiana"
  ]
  node [
    id 559
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 560
    label "wz&#243;r"
  ]
  node [
    id 561
    label "October"
  ]
  node [
    id 562
    label "p&#281;tla"
  ]
  node [
    id 563
    label "arystotelizm"
  ]
  node [
    id 564
    label "szablon"
  ]
  node [
    id 565
    label "miniatura"
  ]
  node [
    id 566
    label "zanucenie"
  ]
  node [
    id 567
    label "nuta"
  ]
  node [
    id 568
    label "zakosztowa&#263;"
  ]
  node [
    id 569
    label "zajawka"
  ]
  node [
    id 570
    label "zanuci&#263;"
  ]
  node [
    id 571
    label "emocja"
  ]
  node [
    id 572
    label "oskoma"
  ]
  node [
    id 573
    label "melika"
  ]
  node [
    id 574
    label "nucenie"
  ]
  node [
    id 575
    label "nuci&#263;"
  ]
  node [
    id 576
    label "brzmienie"
  ]
  node [
    id 577
    label "taste"
  ]
  node [
    id 578
    label "muzyka"
  ]
  node [
    id 579
    label "inclination"
  ]
  node [
    id 580
    label "charakterystyka"
  ]
  node [
    id 581
    label "m&#322;ot"
  ]
  node [
    id 582
    label "drzewo"
  ]
  node [
    id 583
    label "pr&#243;ba"
  ]
  node [
    id 584
    label "attribute"
  ]
  node [
    id 585
    label "marka"
  ]
  node [
    id 586
    label "mentalno&#347;&#263;"
  ]
  node [
    id 587
    label "superego"
  ]
  node [
    id 588
    label "psychika"
  ]
  node [
    id 589
    label "znaczenie"
  ]
  node [
    id 590
    label "wn&#281;trze"
  ]
  node [
    id 591
    label "charakter"
  ]
  node [
    id 592
    label "matter"
  ]
  node [
    id 593
    label "splot"
  ]
  node [
    id 594
    label "wytw&#243;r"
  ]
  node [
    id 595
    label "ceg&#322;a"
  ]
  node [
    id 596
    label "socket"
  ]
  node [
    id 597
    label "rozmieszczenie"
  ]
  node [
    id 598
    label "fabu&#322;a"
  ]
  node [
    id 599
    label "okrywa"
  ]
  node [
    id 600
    label "kontekst"
  ]
  node [
    id 601
    label "object"
  ]
  node [
    id 602
    label "wpadni&#281;cie"
  ]
  node [
    id 603
    label "mienie"
  ]
  node [
    id 604
    label "przyroda"
  ]
  node [
    id 605
    label "wpa&#347;&#263;"
  ]
  node [
    id 606
    label "wpadanie"
  ]
  node [
    id 607
    label "wpada&#263;"
  ]
  node [
    id 608
    label "discussion"
  ]
  node [
    id 609
    label "rozpatrywanie"
  ]
  node [
    id 610
    label "dyskutowanie"
  ]
  node [
    id 611
    label "omowny"
  ]
  node [
    id 612
    label "figura_stylistyczna"
  ]
  node [
    id 613
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 614
    label "odchodzenie"
  ]
  node [
    id 615
    label "aberrance"
  ]
  node [
    id 616
    label "swerve"
  ]
  node [
    id 617
    label "kierunek"
  ]
  node [
    id 618
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 619
    label "distract"
  ]
  node [
    id 620
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 621
    label "odej&#347;&#263;"
  ]
  node [
    id 622
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 623
    label "twist"
  ]
  node [
    id 624
    label "zmieni&#263;"
  ]
  node [
    id 625
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 626
    label "przedyskutowa&#263;"
  ]
  node [
    id 627
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 628
    label "publicize"
  ]
  node [
    id 629
    label "digress"
  ]
  node [
    id 630
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 631
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 632
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 633
    label "odchodzi&#263;"
  ]
  node [
    id 634
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 635
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 636
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 637
    label "perversion"
  ]
  node [
    id 638
    label "death"
  ]
  node [
    id 639
    label "odej&#347;cie"
  ]
  node [
    id 640
    label "turn"
  ]
  node [
    id 641
    label "k&#261;t"
  ]
  node [
    id 642
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 643
    label "odchylenie_si&#281;"
  ]
  node [
    id 644
    label "deviation"
  ]
  node [
    id 645
    label "patologia"
  ]
  node [
    id 646
    label "dyskutowa&#263;"
  ]
  node [
    id 647
    label "formu&#322;owa&#263;"
  ]
  node [
    id 648
    label "discourse"
  ]
  node [
    id 649
    label "kognicja"
  ]
  node [
    id 650
    label "rozprawa"
  ]
  node [
    id 651
    label "szczeg&#243;&#322;"
  ]
  node [
    id 652
    label "proposition"
  ]
  node [
    id 653
    label "przes&#322;anka"
  ]
  node [
    id 654
    label "idea"
  ]
  node [
    id 655
    label "paj&#261;k"
  ]
  node [
    id 656
    label "przewodnik"
  ]
  node [
    id 657
    label "odcinek"
  ]
  node [
    id 658
    label "topikowate"
  ]
  node [
    id 659
    label "grupa_dyskusyjna"
  ]
  node [
    id 660
    label "s&#261;d"
  ]
  node [
    id 661
    label "plac"
  ]
  node [
    id 662
    label "bazylika"
  ]
  node [
    id 663
    label "portal"
  ]
  node [
    id 664
    label "konferencja"
  ]
  node [
    id 665
    label "agora"
  ]
  node [
    id 666
    label "grupa"
  ]
  node [
    id 667
    label "strona"
  ]
  node [
    id 668
    label "zak&#322;ad_komunalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 9
    target 668
  ]
]
