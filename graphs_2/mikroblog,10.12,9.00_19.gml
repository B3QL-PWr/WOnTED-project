graph [
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "greet"
  ]
  node [
    id 2
    label "welcome"
  ]
  node [
    id 3
    label "pozdrawia&#263;"
  ]
  node [
    id 4
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 5
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 6
    label "robi&#263;"
  ]
  node [
    id 7
    label "obchodzi&#263;"
  ]
  node [
    id 8
    label "bless"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
