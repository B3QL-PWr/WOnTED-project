graph [
  node [
    id 0
    label "humor"
    origin "text"
  ]
  node [
    id 1
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 2
    label "motoryzacja"
    origin "text"
  ]
  node [
    id 3
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 4
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 5
    label "temper"
  ]
  node [
    id 6
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 7
    label "stan"
  ]
  node [
    id 8
    label "samopoczucie"
  ]
  node [
    id 9
    label "mechanizm_obronny"
  ]
  node [
    id 10
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 11
    label "fondness"
  ]
  node [
    id 12
    label "nastr&#243;j"
  ]
  node [
    id 13
    label "state"
  ]
  node [
    id 14
    label "wstyd"
  ]
  node [
    id 15
    label "upokorzenie"
  ]
  node [
    id 16
    label "cecha"
  ]
  node [
    id 17
    label "klimat"
  ]
  node [
    id 18
    label "charakter"
  ]
  node [
    id 19
    label "kwas"
  ]
  node [
    id 20
    label "Ohio"
  ]
  node [
    id 21
    label "wci&#281;cie"
  ]
  node [
    id 22
    label "Nowy_York"
  ]
  node [
    id 23
    label "warstwa"
  ]
  node [
    id 24
    label "Illinois"
  ]
  node [
    id 25
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 26
    label "Jukatan"
  ]
  node [
    id 27
    label "Kalifornia"
  ]
  node [
    id 28
    label "Wirginia"
  ]
  node [
    id 29
    label "wektor"
  ]
  node [
    id 30
    label "by&#263;"
  ]
  node [
    id 31
    label "Teksas"
  ]
  node [
    id 32
    label "Goa"
  ]
  node [
    id 33
    label "Waszyngton"
  ]
  node [
    id 34
    label "miejsce"
  ]
  node [
    id 35
    label "Massachusetts"
  ]
  node [
    id 36
    label "Alaska"
  ]
  node [
    id 37
    label "Arakan"
  ]
  node [
    id 38
    label "Hawaje"
  ]
  node [
    id 39
    label "Maryland"
  ]
  node [
    id 40
    label "punkt"
  ]
  node [
    id 41
    label "Michigan"
  ]
  node [
    id 42
    label "Arizona"
  ]
  node [
    id 43
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 44
    label "Georgia"
  ]
  node [
    id 45
    label "poziom"
  ]
  node [
    id 46
    label "Pensylwania"
  ]
  node [
    id 47
    label "shape"
  ]
  node [
    id 48
    label "Luizjana"
  ]
  node [
    id 49
    label "Nowy_Meksyk"
  ]
  node [
    id 50
    label "Alabama"
  ]
  node [
    id 51
    label "ilo&#347;&#263;"
  ]
  node [
    id 52
    label "Kansas"
  ]
  node [
    id 53
    label "Oregon"
  ]
  node [
    id 54
    label "Floryda"
  ]
  node [
    id 55
    label "Oklahoma"
  ]
  node [
    id 56
    label "jednostka_administracyjna"
  ]
  node [
    id 57
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 58
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 59
    label "dyspozycja"
  ]
  node [
    id 60
    label "forma"
  ]
  node [
    id 61
    label "modernizacja"
  ]
  node [
    id 62
    label "transport"
  ]
  node [
    id 63
    label "roz&#322;adunek"
  ]
  node [
    id 64
    label "sprz&#281;t"
  ]
  node [
    id 65
    label "cedu&#322;a"
  ]
  node [
    id 66
    label "jednoszynowy"
  ]
  node [
    id 67
    label "unos"
  ]
  node [
    id 68
    label "traffic"
  ]
  node [
    id 69
    label "prze&#322;adunek"
  ]
  node [
    id 70
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 71
    label "us&#322;uga"
  ]
  node [
    id 72
    label "komunikacja"
  ]
  node [
    id 73
    label "tyfon"
  ]
  node [
    id 74
    label "zawarto&#347;&#263;"
  ]
  node [
    id 75
    label "grupa"
  ]
  node [
    id 76
    label "towar"
  ]
  node [
    id 77
    label "gospodarka"
  ]
  node [
    id 78
    label "za&#322;adunek"
  ]
  node [
    id 79
    label "rozw&#243;j"
  ]
  node [
    id 80
    label "modernization"
  ]
  node [
    id 81
    label "ulepszenie"
  ]
  node [
    id 82
    label "pojazd_drogowy"
  ]
  node [
    id 83
    label "spryskiwacz"
  ]
  node [
    id 84
    label "most"
  ]
  node [
    id 85
    label "baga&#380;nik"
  ]
  node [
    id 86
    label "silnik"
  ]
  node [
    id 87
    label "dachowanie"
  ]
  node [
    id 88
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 89
    label "pompa_wodna"
  ]
  node [
    id 90
    label "poduszka_powietrzna"
  ]
  node [
    id 91
    label "tempomat"
  ]
  node [
    id 92
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 93
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 94
    label "deska_rozdzielcza"
  ]
  node [
    id 95
    label "immobilizer"
  ]
  node [
    id 96
    label "t&#322;umik"
  ]
  node [
    id 97
    label "ABS"
  ]
  node [
    id 98
    label "kierownica"
  ]
  node [
    id 99
    label "bak"
  ]
  node [
    id 100
    label "dwu&#347;lad"
  ]
  node [
    id 101
    label "poci&#261;g_drogowy"
  ]
  node [
    id 102
    label "wycieraczka"
  ]
  node [
    id 103
    label "pojazd"
  ]
  node [
    id 104
    label "rekwizyt_muzyczny"
  ]
  node [
    id 105
    label "attenuator"
  ]
  node [
    id 106
    label "regulator"
  ]
  node [
    id 107
    label "bro&#324;_palna"
  ]
  node [
    id 108
    label "urz&#261;dzenie"
  ]
  node [
    id 109
    label "mata"
  ]
  node [
    id 110
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 111
    label "cycek"
  ]
  node [
    id 112
    label "biust"
  ]
  node [
    id 113
    label "cz&#322;owiek"
  ]
  node [
    id 114
    label "hamowanie"
  ]
  node [
    id 115
    label "uk&#322;ad"
  ]
  node [
    id 116
    label "acrylonitrile-butadiene-styrene"
  ]
  node [
    id 117
    label "sze&#347;ciopak"
  ]
  node [
    id 118
    label "kulturysta"
  ]
  node [
    id 119
    label "mu&#322;y"
  ]
  node [
    id 120
    label "motor"
  ]
  node [
    id 121
    label "rower"
  ]
  node [
    id 122
    label "stolik_topograficzny"
  ]
  node [
    id 123
    label "przyrz&#261;d"
  ]
  node [
    id 124
    label "kontroler_gier"
  ]
  node [
    id 125
    label "biblioteka"
  ]
  node [
    id 126
    label "radiator"
  ]
  node [
    id 127
    label "wyci&#261;garka"
  ]
  node [
    id 128
    label "gondola_silnikowa"
  ]
  node [
    id 129
    label "aerosanie"
  ]
  node [
    id 130
    label "podgrzewacz"
  ]
  node [
    id 131
    label "motogodzina"
  ]
  node [
    id 132
    label "motoszybowiec"
  ]
  node [
    id 133
    label "program"
  ]
  node [
    id 134
    label "gniazdo_zaworowe"
  ]
  node [
    id 135
    label "mechanizm"
  ]
  node [
    id 136
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 137
    label "dociera&#263;"
  ]
  node [
    id 138
    label "dotarcie"
  ]
  node [
    id 139
    label "nap&#281;d"
  ]
  node [
    id 140
    label "motor&#243;wka"
  ]
  node [
    id 141
    label "rz&#281;zi&#263;"
  ]
  node [
    id 142
    label "perpetuum_mobile"
  ]
  node [
    id 143
    label "docieranie"
  ]
  node [
    id 144
    label "bombowiec"
  ]
  node [
    id 145
    label "dotrze&#263;"
  ]
  node [
    id 146
    label "rz&#281;&#380;enie"
  ]
  node [
    id 147
    label "ochrona"
  ]
  node [
    id 148
    label "rzuci&#263;"
  ]
  node [
    id 149
    label "prz&#281;s&#322;o"
  ]
  node [
    id 150
    label "m&#243;zg"
  ]
  node [
    id 151
    label "trasa"
  ]
  node [
    id 152
    label "jarzmo_mostowe"
  ]
  node [
    id 153
    label "pylon"
  ]
  node [
    id 154
    label "zam&#243;zgowie"
  ]
  node [
    id 155
    label "obiekt_mostowy"
  ]
  node [
    id 156
    label "szczelina_dylatacyjna"
  ]
  node [
    id 157
    label "rzucenie"
  ]
  node [
    id 158
    label "bridge"
  ]
  node [
    id 159
    label "rzuca&#263;"
  ]
  node [
    id 160
    label "suwnica"
  ]
  node [
    id 161
    label "porozumienie"
  ]
  node [
    id 162
    label "rzucanie"
  ]
  node [
    id 163
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 164
    label "sprinkler"
  ]
  node [
    id 165
    label "bakenbardy"
  ]
  node [
    id 166
    label "tank"
  ]
  node [
    id 167
    label "fordek"
  ]
  node [
    id 168
    label "zbiornik"
  ]
  node [
    id 169
    label "beard"
  ]
  node [
    id 170
    label "zarost"
  ]
  node [
    id 171
    label "przewracanie_si&#281;"
  ]
  node [
    id 172
    label "przewr&#243;cenie_si&#281;"
  ]
  node [
    id 173
    label "jechanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
]
