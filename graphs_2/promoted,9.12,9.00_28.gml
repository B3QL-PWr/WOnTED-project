graph [
  node [
    id 0
    label "drogi"
    origin "text"
  ]
  node [
    id 1
    label "wykopowicze"
    origin "text"
  ]
  node [
    id 2
    label "drogo"
  ]
  node [
    id 3
    label "mi&#322;y"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "bliski"
  ]
  node [
    id 6
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 7
    label "przyjaciel"
  ]
  node [
    id 8
    label "warto&#347;ciowy"
  ]
  node [
    id 9
    label "blisko"
  ]
  node [
    id 10
    label "znajomy"
  ]
  node [
    id 11
    label "zwi&#261;zany"
  ]
  node [
    id 12
    label "przesz&#322;y"
  ]
  node [
    id 13
    label "silny"
  ]
  node [
    id 14
    label "zbli&#380;enie"
  ]
  node [
    id 15
    label "kr&#243;tki"
  ]
  node [
    id 16
    label "oddalony"
  ]
  node [
    id 17
    label "dok&#322;adny"
  ]
  node [
    id 18
    label "nieodleg&#322;y"
  ]
  node [
    id 19
    label "przysz&#322;y"
  ]
  node [
    id 20
    label "gotowy"
  ]
  node [
    id 21
    label "ma&#322;y"
  ]
  node [
    id 22
    label "ludzko&#347;&#263;"
  ]
  node [
    id 23
    label "asymilowanie"
  ]
  node [
    id 24
    label "wapniak"
  ]
  node [
    id 25
    label "asymilowa&#263;"
  ]
  node [
    id 26
    label "os&#322;abia&#263;"
  ]
  node [
    id 27
    label "posta&#263;"
  ]
  node [
    id 28
    label "hominid"
  ]
  node [
    id 29
    label "podw&#322;adny"
  ]
  node [
    id 30
    label "os&#322;abianie"
  ]
  node [
    id 31
    label "g&#322;owa"
  ]
  node [
    id 32
    label "figura"
  ]
  node [
    id 33
    label "portrecista"
  ]
  node [
    id 34
    label "dwun&#243;g"
  ]
  node [
    id 35
    label "profanum"
  ]
  node [
    id 36
    label "mikrokosmos"
  ]
  node [
    id 37
    label "nasada"
  ]
  node [
    id 38
    label "duch"
  ]
  node [
    id 39
    label "antropochoria"
  ]
  node [
    id 40
    label "osoba"
  ]
  node [
    id 41
    label "wz&#243;r"
  ]
  node [
    id 42
    label "senior"
  ]
  node [
    id 43
    label "oddzia&#322;ywanie"
  ]
  node [
    id 44
    label "Adam"
  ]
  node [
    id 45
    label "homo_sapiens"
  ]
  node [
    id 46
    label "polifag"
  ]
  node [
    id 47
    label "droga"
  ]
  node [
    id 48
    label "ukochanie"
  ]
  node [
    id 49
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 50
    label "feblik"
  ]
  node [
    id 51
    label "podnieci&#263;"
  ]
  node [
    id 52
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 53
    label "numer"
  ]
  node [
    id 54
    label "po&#380;ycie"
  ]
  node [
    id 55
    label "tendency"
  ]
  node [
    id 56
    label "podniecenie"
  ]
  node [
    id 57
    label "afekt"
  ]
  node [
    id 58
    label "zakochanie"
  ]
  node [
    id 59
    label "zajawka"
  ]
  node [
    id 60
    label "seks"
  ]
  node [
    id 61
    label "podniecanie"
  ]
  node [
    id 62
    label "imisja"
  ]
  node [
    id 63
    label "love"
  ]
  node [
    id 64
    label "rozmna&#380;anie"
  ]
  node [
    id 65
    label "ruch_frykcyjny"
  ]
  node [
    id 66
    label "na_pieska"
  ]
  node [
    id 67
    label "serce"
  ]
  node [
    id 68
    label "pozycja_misjonarska"
  ]
  node [
    id 69
    label "wi&#281;&#378;"
  ]
  node [
    id 70
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 71
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 72
    label "z&#322;&#261;czenie"
  ]
  node [
    id 73
    label "czynno&#347;&#263;"
  ]
  node [
    id 74
    label "gra_wst&#281;pna"
  ]
  node [
    id 75
    label "erotyka"
  ]
  node [
    id 76
    label "emocja"
  ]
  node [
    id 77
    label "baraszki"
  ]
  node [
    id 78
    label "po&#380;&#261;danie"
  ]
  node [
    id 79
    label "wzw&#243;d"
  ]
  node [
    id 80
    label "podnieca&#263;"
  ]
  node [
    id 81
    label "kochanek"
  ]
  node [
    id 82
    label "kum"
  ]
  node [
    id 83
    label "amikus"
  ]
  node [
    id 84
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 85
    label "pobratymiec"
  ]
  node [
    id 86
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 87
    label "sympatyk"
  ]
  node [
    id 88
    label "bratnia_dusza"
  ]
  node [
    id 89
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 90
    label "sk&#322;onny"
  ]
  node [
    id 91
    label "wybranek"
  ]
  node [
    id 92
    label "umi&#322;owany"
  ]
  node [
    id 93
    label "przyjemnie"
  ]
  node [
    id 94
    label "mi&#322;o"
  ]
  node [
    id 95
    label "kochanie"
  ]
  node [
    id 96
    label "dyplomata"
  ]
  node [
    id 97
    label "dobry"
  ]
  node [
    id 98
    label "dro&#380;ej"
  ]
  node [
    id 99
    label "p&#322;atnie"
  ]
  node [
    id 100
    label "rewaluowanie"
  ]
  node [
    id 101
    label "warto&#347;ciowo"
  ]
  node [
    id 102
    label "u&#380;yteczny"
  ]
  node [
    id 103
    label "zrewaluowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
]
