graph [
  node [
    id 0
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 1
    label "zem&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "ojciec"
    origin "text"
  ]
  node [
    id 4
    label "kszta&#322;ciciel"
  ]
  node [
    id 5
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 6
    label "kuwada"
  ]
  node [
    id 7
    label "tworzyciel"
  ]
  node [
    id 8
    label "rodzice"
  ]
  node [
    id 9
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 10
    label "&#347;w"
  ]
  node [
    id 11
    label "pomys&#322;odawca"
  ]
  node [
    id 12
    label "rodzic"
  ]
  node [
    id 13
    label "wykonawca"
  ]
  node [
    id 14
    label "ojczym"
  ]
  node [
    id 15
    label "samiec"
  ]
  node [
    id 16
    label "przodek"
  ]
  node [
    id 17
    label "papa"
  ]
  node [
    id 18
    label "zakonnik"
  ]
  node [
    id 19
    label "stary"
  ]
  node [
    id 20
    label "br"
  ]
  node [
    id 21
    label "mnich"
  ]
  node [
    id 22
    label "zakon"
  ]
  node [
    id 23
    label "wyznawca"
  ]
  node [
    id 24
    label "zwierz&#281;"
  ]
  node [
    id 25
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 26
    label "opiekun"
  ]
  node [
    id 27
    label "wapniak"
  ]
  node [
    id 28
    label "rodzic_chrzestny"
  ]
  node [
    id 29
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 30
    label "ojcowie"
  ]
  node [
    id 31
    label "linea&#380;"
  ]
  node [
    id 32
    label "krewny"
  ]
  node [
    id 33
    label "chodnik"
  ]
  node [
    id 34
    label "w&#243;z"
  ]
  node [
    id 35
    label "p&#322;ug"
  ]
  node [
    id 36
    label "wyrobisko"
  ]
  node [
    id 37
    label "dziad"
  ]
  node [
    id 38
    label "antecesor"
  ]
  node [
    id 39
    label "post&#281;p"
  ]
  node [
    id 40
    label "inicjator"
  ]
  node [
    id 41
    label "podmiot_gospodarczy"
  ]
  node [
    id 42
    label "artysta"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "muzyk"
  ]
  node [
    id 45
    label "materia&#322;_budowlany"
  ]
  node [
    id 46
    label "twarz"
  ]
  node [
    id 47
    label "gun_muzzle"
  ]
  node [
    id 48
    label "izolacja"
  ]
  node [
    id 49
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 50
    label "nienowoczesny"
  ]
  node [
    id 51
    label "gruba_ryba"
  ]
  node [
    id 52
    label "zestarzenie_si&#281;"
  ]
  node [
    id 53
    label "poprzedni"
  ]
  node [
    id 54
    label "dawno"
  ]
  node [
    id 55
    label "staro"
  ]
  node [
    id 56
    label "m&#261;&#380;"
  ]
  node [
    id 57
    label "starzy"
  ]
  node [
    id 58
    label "dotychczasowy"
  ]
  node [
    id 59
    label "p&#243;&#378;ny"
  ]
  node [
    id 60
    label "d&#322;ugoletni"
  ]
  node [
    id 61
    label "charakterystyczny"
  ]
  node [
    id 62
    label "brat"
  ]
  node [
    id 63
    label "po_staro&#347;wiecku"
  ]
  node [
    id 64
    label "zwierzchnik"
  ]
  node [
    id 65
    label "znajomy"
  ]
  node [
    id 66
    label "odleg&#322;y"
  ]
  node [
    id 67
    label "starzenie_si&#281;"
  ]
  node [
    id 68
    label "starczo"
  ]
  node [
    id 69
    label "dawniej"
  ]
  node [
    id 70
    label "niegdysiejszy"
  ]
  node [
    id 71
    label "dojrza&#322;y"
  ]
  node [
    id 72
    label "nauczyciel"
  ]
  node [
    id 73
    label "autor"
  ]
  node [
    id 74
    label "doros&#322;y"
  ]
  node [
    id 75
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 76
    label "jegomo&#347;&#263;"
  ]
  node [
    id 77
    label "andropauza"
  ]
  node [
    id 78
    label "pa&#324;stwo"
  ]
  node [
    id 79
    label "bratek"
  ]
  node [
    id 80
    label "ch&#322;opina"
  ]
  node [
    id 81
    label "twardziel"
  ]
  node [
    id 82
    label "androlog"
  ]
  node [
    id 83
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 84
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 85
    label "pokolenie"
  ]
  node [
    id 86
    label "wapniaki"
  ]
  node [
    id 87
    label "syndrom_kuwady"
  ]
  node [
    id 88
    label "na&#347;ladownictwo"
  ]
  node [
    id 89
    label "zwyczaj"
  ]
  node [
    id 90
    label "ci&#261;&#380;a"
  ]
  node [
    id 91
    label "&#380;onaty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
]
