graph [
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 2
    label "zamyka&#263;"
    origin "text"
  ]
  node [
    id 3
    label "usta"
    origin "text"
  ]
  node [
    id 4
    label "nast&#281;pnie"
  ]
  node [
    id 5
    label "inny"
  ]
  node [
    id 6
    label "nastopny"
  ]
  node [
    id 7
    label "kolejno"
  ]
  node [
    id 8
    label "kt&#243;ry&#347;"
  ]
  node [
    id 9
    label "osobno"
  ]
  node [
    id 10
    label "r&#243;&#380;ny"
  ]
  node [
    id 11
    label "inszy"
  ]
  node [
    id 12
    label "inaczej"
  ]
  node [
    id 13
    label "do&#347;wiadczenie"
  ]
  node [
    id 14
    label "spotkanie"
  ]
  node [
    id 15
    label "pobiera&#263;"
  ]
  node [
    id 16
    label "metal_szlachetny"
  ]
  node [
    id 17
    label "pobranie"
  ]
  node [
    id 18
    label "zbi&#243;r"
  ]
  node [
    id 19
    label "usi&#322;owanie"
  ]
  node [
    id 20
    label "pobra&#263;"
  ]
  node [
    id 21
    label "pobieranie"
  ]
  node [
    id 22
    label "znak"
  ]
  node [
    id 23
    label "rezultat"
  ]
  node [
    id 24
    label "effort"
  ]
  node [
    id 25
    label "analiza_chemiczna"
  ]
  node [
    id 26
    label "item"
  ]
  node [
    id 27
    label "czynno&#347;&#263;"
  ]
  node [
    id 28
    label "sytuacja"
  ]
  node [
    id 29
    label "probiernictwo"
  ]
  node [
    id 30
    label "ilo&#347;&#263;"
  ]
  node [
    id 31
    label "test"
  ]
  node [
    id 32
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 33
    label "dow&#243;d"
  ]
  node [
    id 34
    label "oznakowanie"
  ]
  node [
    id 35
    label "fakt"
  ]
  node [
    id 36
    label "stawia&#263;"
  ]
  node [
    id 37
    label "wytw&#243;r"
  ]
  node [
    id 38
    label "point"
  ]
  node [
    id 39
    label "kodzik"
  ]
  node [
    id 40
    label "postawi&#263;"
  ]
  node [
    id 41
    label "mark"
  ]
  node [
    id 42
    label "herb"
  ]
  node [
    id 43
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 44
    label "attribute"
  ]
  node [
    id 45
    label "implikowa&#263;"
  ]
  node [
    id 46
    label "badanie"
  ]
  node [
    id 47
    label "narz&#281;dzie"
  ]
  node [
    id 48
    label "przechodzenie"
  ]
  node [
    id 49
    label "quiz"
  ]
  node [
    id 50
    label "sprawdzian"
  ]
  node [
    id 51
    label "arkusz"
  ]
  node [
    id 52
    label "przechodzi&#263;"
  ]
  node [
    id 53
    label "activity"
  ]
  node [
    id 54
    label "bezproblemowy"
  ]
  node [
    id 55
    label "wydarzenie"
  ]
  node [
    id 56
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "rozmiar"
  ]
  node [
    id 58
    label "part"
  ]
  node [
    id 59
    label "Rzym_Zachodni"
  ]
  node [
    id 60
    label "whole"
  ]
  node [
    id 61
    label "element"
  ]
  node [
    id 62
    label "Rzym_Wschodni"
  ]
  node [
    id 63
    label "urz&#261;dzenie"
  ]
  node [
    id 64
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 65
    label "szko&#322;a"
  ]
  node [
    id 66
    label "obserwowanie"
  ]
  node [
    id 67
    label "wiedza"
  ]
  node [
    id 68
    label "wy&#347;wiadczenie"
  ]
  node [
    id 69
    label "assay"
  ]
  node [
    id 70
    label "znawstwo"
  ]
  node [
    id 71
    label "skill"
  ]
  node [
    id 72
    label "checkup"
  ]
  node [
    id 73
    label "do&#347;wiadczanie"
  ]
  node [
    id 74
    label "zbadanie"
  ]
  node [
    id 75
    label "potraktowanie"
  ]
  node [
    id 76
    label "eksperiencja"
  ]
  node [
    id 77
    label "poczucie"
  ]
  node [
    id 78
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 79
    label "warunki"
  ]
  node [
    id 80
    label "szczeg&#243;&#322;"
  ]
  node [
    id 81
    label "state"
  ]
  node [
    id 82
    label "motyw"
  ]
  node [
    id 83
    label "realia"
  ]
  node [
    id 84
    label "doznanie"
  ]
  node [
    id 85
    label "gathering"
  ]
  node [
    id 86
    label "zawarcie"
  ]
  node [
    id 87
    label "znajomy"
  ]
  node [
    id 88
    label "powitanie"
  ]
  node [
    id 89
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 90
    label "spowodowanie"
  ]
  node [
    id 91
    label "zdarzenie_si&#281;"
  ]
  node [
    id 92
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 93
    label "znalezienie"
  ]
  node [
    id 94
    label "match"
  ]
  node [
    id 95
    label "employment"
  ]
  node [
    id 96
    label "po&#380;egnanie"
  ]
  node [
    id 97
    label "gather"
  ]
  node [
    id 98
    label "spotykanie"
  ]
  node [
    id 99
    label "spotkanie_si&#281;"
  ]
  node [
    id 100
    label "egzemplarz"
  ]
  node [
    id 101
    label "series"
  ]
  node [
    id 102
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 103
    label "uprawianie"
  ]
  node [
    id 104
    label "praca_rolnicza"
  ]
  node [
    id 105
    label "collection"
  ]
  node [
    id 106
    label "dane"
  ]
  node [
    id 107
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 108
    label "pakiet_klimatyczny"
  ]
  node [
    id 109
    label "poj&#281;cie"
  ]
  node [
    id 110
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 111
    label "sum"
  ]
  node [
    id 112
    label "album"
  ]
  node [
    id 113
    label "dzia&#322;anie"
  ]
  node [
    id 114
    label "typ"
  ]
  node [
    id 115
    label "event"
  ]
  node [
    id 116
    label "przyczyna"
  ]
  node [
    id 117
    label "podejmowanie"
  ]
  node [
    id 118
    label "staranie_si&#281;"
  ]
  node [
    id 119
    label "essay"
  ]
  node [
    id 120
    label "kontrola"
  ]
  node [
    id 121
    label "branie"
  ]
  node [
    id 122
    label "wycinanie"
  ]
  node [
    id 123
    label "bite"
  ]
  node [
    id 124
    label "otrzymywanie"
  ]
  node [
    id 125
    label "pr&#243;bka"
  ]
  node [
    id 126
    label "wch&#322;anianie"
  ]
  node [
    id 127
    label "wymienianie_si&#281;"
  ]
  node [
    id 128
    label "przeszczepianie"
  ]
  node [
    id 129
    label "levy"
  ]
  node [
    id 130
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 131
    label "uzyskanie"
  ]
  node [
    id 132
    label "otrzymanie"
  ]
  node [
    id 133
    label "capture"
  ]
  node [
    id 134
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 135
    label "wyci&#281;cie"
  ]
  node [
    id 136
    label "przeszczepienie"
  ]
  node [
    id 137
    label "wymienienie_si&#281;"
  ]
  node [
    id 138
    label "wzi&#281;cie"
  ]
  node [
    id 139
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 140
    label "wyci&#261;&#263;"
  ]
  node [
    id 141
    label "wzi&#261;&#263;"
  ]
  node [
    id 142
    label "catch"
  ]
  node [
    id 143
    label "otrzyma&#263;"
  ]
  node [
    id 144
    label "skopiowa&#263;"
  ]
  node [
    id 145
    label "get"
  ]
  node [
    id 146
    label "uzyska&#263;"
  ]
  node [
    id 147
    label "arise"
  ]
  node [
    id 148
    label "wycina&#263;"
  ]
  node [
    id 149
    label "kopiowa&#263;"
  ]
  node [
    id 150
    label "bra&#263;"
  ]
  node [
    id 151
    label "open"
  ]
  node [
    id 152
    label "wch&#322;ania&#263;"
  ]
  node [
    id 153
    label "otrzymywa&#263;"
  ]
  node [
    id 154
    label "raise"
  ]
  node [
    id 155
    label "zawiera&#263;"
  ]
  node [
    id 156
    label "suspend"
  ]
  node [
    id 157
    label "ujmowa&#263;"
  ]
  node [
    id 158
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 159
    label "instytucja"
  ]
  node [
    id 160
    label "unieruchamia&#263;"
  ]
  node [
    id 161
    label "sk&#322;ada&#263;"
  ]
  node [
    id 162
    label "ko&#324;czy&#263;"
  ]
  node [
    id 163
    label "blokowa&#263;"
  ]
  node [
    id 164
    label "lock"
  ]
  node [
    id 165
    label "umieszcza&#263;"
  ]
  node [
    id 166
    label "ukrywa&#263;"
  ]
  node [
    id 167
    label "exsert"
  ]
  node [
    id 168
    label "poznawa&#263;"
  ]
  node [
    id 169
    label "fold"
  ]
  node [
    id 170
    label "obejmowa&#263;"
  ]
  node [
    id 171
    label "mie&#263;"
  ]
  node [
    id 172
    label "make"
  ]
  node [
    id 173
    label "ustala&#263;"
  ]
  node [
    id 174
    label "zabiera&#263;"
  ]
  node [
    id 175
    label "zgarnia&#263;"
  ]
  node [
    id 176
    label "chwyta&#263;"
  ]
  node [
    id 177
    label "&#322;apa&#263;"
  ]
  node [
    id 178
    label "reduce"
  ]
  node [
    id 179
    label "allude"
  ]
  node [
    id 180
    label "wzbudza&#263;"
  ]
  node [
    id 181
    label "komunikowa&#263;"
  ]
  node [
    id 182
    label "convey"
  ]
  node [
    id 183
    label "throng"
  ]
  node [
    id 184
    label "powodowa&#263;"
  ]
  node [
    id 185
    label "wstrzymywa&#263;"
  ]
  node [
    id 186
    label "przeszkadza&#263;"
  ]
  node [
    id 187
    label "zajmowa&#263;"
  ]
  node [
    id 188
    label "kiblowa&#263;"
  ]
  node [
    id 189
    label "walczy&#263;"
  ]
  node [
    id 190
    label "zablokowywa&#263;"
  ]
  node [
    id 191
    label "zatrzymywa&#263;"
  ]
  node [
    id 192
    label "interlock"
  ]
  node [
    id 193
    label "parry"
  ]
  node [
    id 194
    label "suppress"
  ]
  node [
    id 195
    label "report"
  ]
  node [
    id 196
    label "meliniarz"
  ]
  node [
    id 197
    label "cache"
  ]
  node [
    id 198
    label "zachowywa&#263;"
  ]
  node [
    id 199
    label "plasowa&#263;"
  ]
  node [
    id 200
    label "umie&#347;ci&#263;"
  ]
  node [
    id 201
    label "robi&#263;"
  ]
  node [
    id 202
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 203
    label "pomieszcza&#263;"
  ]
  node [
    id 204
    label "accommodate"
  ]
  node [
    id 205
    label "zmienia&#263;"
  ]
  node [
    id 206
    label "venture"
  ]
  node [
    id 207
    label "wpiernicza&#263;"
  ]
  node [
    id 208
    label "okre&#347;la&#263;"
  ]
  node [
    id 209
    label "przekazywa&#263;"
  ]
  node [
    id 210
    label "zbiera&#263;"
  ]
  node [
    id 211
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 212
    label "przywraca&#263;"
  ]
  node [
    id 213
    label "dawa&#263;"
  ]
  node [
    id 214
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 215
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 216
    label "publicize"
  ]
  node [
    id 217
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 218
    label "render"
  ]
  node [
    id 219
    label "uk&#322;ada&#263;"
  ]
  node [
    id 220
    label "opracowywa&#263;"
  ]
  node [
    id 221
    label "set"
  ]
  node [
    id 222
    label "oddawa&#263;"
  ]
  node [
    id 223
    label "train"
  ]
  node [
    id 224
    label "dzieli&#263;"
  ]
  node [
    id 225
    label "scala&#263;"
  ]
  node [
    id 226
    label "zestaw"
  ]
  node [
    id 227
    label "usuwa&#263;"
  ]
  node [
    id 228
    label "odkrywa&#263;"
  ]
  node [
    id 229
    label "urzeczywistnia&#263;"
  ]
  node [
    id 230
    label "undo"
  ]
  node [
    id 231
    label "przestawa&#263;"
  ]
  node [
    id 232
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 233
    label "cope"
  ]
  node [
    id 234
    label "satisfy"
  ]
  node [
    id 235
    label "close"
  ]
  node [
    id 236
    label "determine"
  ]
  node [
    id 237
    label "zako&#324;cza&#263;"
  ]
  node [
    id 238
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 239
    label "stanowi&#263;"
  ]
  node [
    id 240
    label "osoba_prawna"
  ]
  node [
    id 241
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 242
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 243
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 244
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 245
    label "biuro"
  ]
  node [
    id 246
    label "organizacja"
  ]
  node [
    id 247
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 248
    label "Fundusze_Unijne"
  ]
  node [
    id 249
    label "establishment"
  ]
  node [
    id 250
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 251
    label "urz&#261;d"
  ]
  node [
    id 252
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 253
    label "afiliowa&#263;"
  ]
  node [
    id 254
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 255
    label "standard"
  ]
  node [
    id 256
    label "zamykanie"
  ]
  node [
    id 257
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 258
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 259
    label "dzi&#243;b"
  ]
  node [
    id 260
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 261
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 262
    label "zacinanie"
  ]
  node [
    id 263
    label "ssa&#263;"
  ]
  node [
    id 264
    label "organ"
  ]
  node [
    id 265
    label "zacina&#263;"
  ]
  node [
    id 266
    label "zaci&#261;&#263;"
  ]
  node [
    id 267
    label "ssanie"
  ]
  node [
    id 268
    label "jama_ustna"
  ]
  node [
    id 269
    label "jadaczka"
  ]
  node [
    id 270
    label "zaci&#281;cie"
  ]
  node [
    id 271
    label "warga_dolna"
  ]
  node [
    id 272
    label "twarz"
  ]
  node [
    id 273
    label "warga_g&#243;rna"
  ]
  node [
    id 274
    label "ryjek"
  ]
  node [
    id 275
    label "tkanka"
  ]
  node [
    id 276
    label "jednostka_organizacyjna"
  ]
  node [
    id 277
    label "budowa"
  ]
  node [
    id 278
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 279
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 280
    label "tw&#243;r"
  ]
  node [
    id 281
    label "organogeneza"
  ]
  node [
    id 282
    label "zesp&#243;&#322;"
  ]
  node [
    id 283
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 284
    label "struktura_anatomiczna"
  ]
  node [
    id 285
    label "uk&#322;ad"
  ]
  node [
    id 286
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 287
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 288
    label "Izba_Konsyliarska"
  ]
  node [
    id 289
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 290
    label "stomia"
  ]
  node [
    id 291
    label "dekortykacja"
  ]
  node [
    id 292
    label "okolica"
  ]
  node [
    id 293
    label "Komitet_Region&#243;w"
  ]
  node [
    id 294
    label "ptak"
  ]
  node [
    id 295
    label "grzebie&#324;"
  ]
  node [
    id 296
    label "bow"
  ]
  node [
    id 297
    label "statek"
  ]
  node [
    id 298
    label "ustnik"
  ]
  node [
    id 299
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 300
    label "samolot"
  ]
  node [
    id 301
    label "zako&#324;czenie"
  ]
  node [
    id 302
    label "ostry"
  ]
  node [
    id 303
    label "blizna"
  ]
  node [
    id 304
    label "dziob&#243;wka"
  ]
  node [
    id 305
    label "cz&#322;owiek"
  ]
  node [
    id 306
    label "cera"
  ]
  node [
    id 307
    label "wielko&#347;&#263;"
  ]
  node [
    id 308
    label "rys"
  ]
  node [
    id 309
    label "przedstawiciel"
  ]
  node [
    id 310
    label "profil"
  ]
  node [
    id 311
    label "p&#322;e&#263;"
  ]
  node [
    id 312
    label "posta&#263;"
  ]
  node [
    id 313
    label "zas&#322;ona"
  ]
  node [
    id 314
    label "p&#243;&#322;profil"
  ]
  node [
    id 315
    label "policzek"
  ]
  node [
    id 316
    label "brew"
  ]
  node [
    id 317
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 318
    label "uj&#281;cie"
  ]
  node [
    id 319
    label "micha"
  ]
  node [
    id 320
    label "reputacja"
  ]
  node [
    id 321
    label "wyraz_twarzy"
  ]
  node [
    id 322
    label "powieka"
  ]
  node [
    id 323
    label "czo&#322;o"
  ]
  node [
    id 324
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 325
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 326
    label "twarzyczka"
  ]
  node [
    id 327
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 328
    label "ucho"
  ]
  node [
    id 329
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 330
    label "prz&#243;d"
  ]
  node [
    id 331
    label "oko"
  ]
  node [
    id 332
    label "nos"
  ]
  node [
    id 333
    label "podbr&#243;dek"
  ]
  node [
    id 334
    label "liczko"
  ]
  node [
    id 335
    label "pysk"
  ]
  node [
    id 336
    label "maskowato&#347;&#263;"
  ]
  node [
    id 337
    label "&#347;cina&#263;"
  ]
  node [
    id 338
    label "zaciera&#263;"
  ]
  node [
    id 339
    label "przerywa&#263;"
  ]
  node [
    id 340
    label "psu&#263;"
  ]
  node [
    id 341
    label "zaciska&#263;"
  ]
  node [
    id 342
    label "odbiera&#263;"
  ]
  node [
    id 343
    label "zakrawa&#263;"
  ]
  node [
    id 344
    label "wprawia&#263;"
  ]
  node [
    id 345
    label "bat"
  ]
  node [
    id 346
    label "cut"
  ]
  node [
    id 347
    label "w&#281;dka"
  ]
  node [
    id 348
    label "lejce"
  ]
  node [
    id 349
    label "podrywa&#263;"
  ]
  node [
    id 350
    label "hack"
  ]
  node [
    id 351
    label "nacina&#263;"
  ]
  node [
    id 352
    label "pocina&#263;"
  ]
  node [
    id 353
    label "uderza&#263;"
  ]
  node [
    id 354
    label "mina"
  ]
  node [
    id 355
    label "ch&#322;osta&#263;"
  ]
  node [
    id 356
    label "kaleczy&#263;"
  ]
  node [
    id 357
    label "struga&#263;"
  ]
  node [
    id 358
    label "write_out"
  ]
  node [
    id 359
    label "ch&#322;o&#347;ni&#281;cie"
  ]
  node [
    id 360
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 361
    label "kszta&#322;t"
  ]
  node [
    id 362
    label "naci&#281;cie"
  ]
  node [
    id 363
    label "ko&#324;"
  ]
  node [
    id 364
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 365
    label "zaci&#281;ty"
  ]
  node [
    id 366
    label "poderwanie"
  ]
  node [
    id 367
    label "talent"
  ]
  node [
    id 368
    label "go"
  ]
  node [
    id 369
    label "miejsce"
  ]
  node [
    id 370
    label "capability"
  ]
  node [
    id 371
    label "stanowczo"
  ]
  node [
    id 372
    label "w&#281;dkowanie"
  ]
  node [
    id 373
    label "ostruganie"
  ]
  node [
    id 374
    label "formacja_skalna"
  ]
  node [
    id 375
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 376
    label "potkni&#281;cie"
  ]
  node [
    id 377
    label "zranienie"
  ]
  node [
    id 378
    label "turn"
  ]
  node [
    id 379
    label "dash"
  ]
  node [
    id 380
    label "uwa&#380;nie"
  ]
  node [
    id 381
    label "nieust&#281;pliwie"
  ]
  node [
    id 382
    label "powo&#380;enie"
  ]
  node [
    id 383
    label "poderwa&#263;"
  ]
  node [
    id 384
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 385
    label "ostruga&#263;"
  ]
  node [
    id 386
    label "nadci&#261;&#263;"
  ]
  node [
    id 387
    label "uderzy&#263;"
  ]
  node [
    id 388
    label "zrani&#263;"
  ]
  node [
    id 389
    label "wprawi&#263;"
  ]
  node [
    id 390
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 391
    label "przerwa&#263;"
  ]
  node [
    id 392
    label "ch&#322;osn&#261;&#263;"
  ]
  node [
    id 393
    label "zepsu&#263;"
  ]
  node [
    id 394
    label "naci&#261;&#263;"
  ]
  node [
    id 395
    label "odebra&#263;"
  ]
  node [
    id 396
    label "zablokowa&#263;"
  ]
  node [
    id 397
    label "padanie"
  ]
  node [
    id 398
    label "kaleczenie"
  ]
  node [
    id 399
    label "nacinanie"
  ]
  node [
    id 400
    label "podrywanie"
  ]
  node [
    id 401
    label "zaciskanie"
  ]
  node [
    id 402
    label "struganie"
  ]
  node [
    id 403
    label "ch&#322;ostanie"
  ]
  node [
    id 404
    label "narz&#261;d_g&#281;bowy"
  ]
  node [
    id 405
    label "picie"
  ]
  node [
    id 406
    label "ruszanie"
  ]
  node [
    id 407
    label "&#347;lina"
  ]
  node [
    id 408
    label "consumption"
  ]
  node [
    id 409
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 410
    label "rozpuszczanie"
  ]
  node [
    id 411
    label "aspiration"
  ]
  node [
    id 412
    label "wci&#261;ganie"
  ]
  node [
    id 413
    label "odci&#261;ganie"
  ]
  node [
    id 414
    label "j&#281;zyk"
  ]
  node [
    id 415
    label "wessanie"
  ]
  node [
    id 416
    label "ga&#378;nik"
  ]
  node [
    id 417
    label "mechanizm"
  ]
  node [
    id 418
    label "wysysanie"
  ]
  node [
    id 419
    label "wyssanie"
  ]
  node [
    id 420
    label "pi&#263;"
  ]
  node [
    id 421
    label "sponge"
  ]
  node [
    id 422
    label "mleko"
  ]
  node [
    id 423
    label "rozpuszcza&#263;"
  ]
  node [
    id 424
    label "wci&#261;ga&#263;"
  ]
  node [
    id 425
    label "rusza&#263;"
  ]
  node [
    id 426
    label "sucking"
  ]
  node [
    id 427
    label "smoczek"
  ]
  node [
    id 428
    label "unia"
  ]
  node [
    id 429
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 428
    target 429
  ]
]
