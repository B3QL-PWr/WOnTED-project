graph [
  node [
    id 0
    label "centrum"
    origin "text"
  ]
  node [
    id 1
    label "kszta&#322;cenie"
    origin "text"
  ]
  node [
    id 2
    label "in&#380;ynier"
    origin "text"
  ]
  node [
    id 3
    label "punkt"
  ]
  node [
    id 4
    label "blok"
  ]
  node [
    id 5
    label "sejm"
  ]
  node [
    id 6
    label "centroprawica"
  ]
  node [
    id 7
    label "core"
  ]
  node [
    id 8
    label "o&#347;rodek"
  ]
  node [
    id 9
    label "Hollywood"
  ]
  node [
    id 10
    label "miejsce"
  ]
  node [
    id 11
    label "centrolew"
  ]
  node [
    id 12
    label "&#347;rodek"
  ]
  node [
    id 13
    label "warunki"
  ]
  node [
    id 14
    label "zal&#261;&#380;ek"
  ]
  node [
    id 15
    label "skupisko"
  ]
  node [
    id 16
    label "center"
  ]
  node [
    id 17
    label "instytucja"
  ]
  node [
    id 18
    label "otoczenie"
  ]
  node [
    id 19
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 20
    label "dzia&#322;"
  ]
  node [
    id 21
    label "skorupa_ziemska"
  ]
  node [
    id 22
    label "budynek"
  ]
  node [
    id 23
    label "przeszkoda"
  ]
  node [
    id 24
    label "bry&#322;a"
  ]
  node [
    id 25
    label "j&#261;kanie"
  ]
  node [
    id 26
    label "program"
  ]
  node [
    id 27
    label "square"
  ]
  node [
    id 28
    label "bloking"
  ]
  node [
    id 29
    label "kontynent"
  ]
  node [
    id 30
    label "ok&#322;adka"
  ]
  node [
    id 31
    label "zbi&#243;r"
  ]
  node [
    id 32
    label "kr&#261;g"
  ]
  node [
    id 33
    label "start"
  ]
  node [
    id 34
    label "blockage"
  ]
  node [
    id 35
    label "blokowisko"
  ]
  node [
    id 36
    label "artyku&#322;"
  ]
  node [
    id 37
    label "blokada"
  ]
  node [
    id 38
    label "whole"
  ]
  node [
    id 39
    label "stok_kontynentalny"
  ]
  node [
    id 40
    label "bajt"
  ]
  node [
    id 41
    label "barak"
  ]
  node [
    id 42
    label "zamek"
  ]
  node [
    id 43
    label "referat"
  ]
  node [
    id 44
    label "nastawnia"
  ]
  node [
    id 45
    label "obrona"
  ]
  node [
    id 46
    label "organizacja"
  ]
  node [
    id 47
    label "grupa"
  ]
  node [
    id 48
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 49
    label "dom_wielorodzinny"
  ]
  node [
    id 50
    label "zeszyt"
  ]
  node [
    id 51
    label "ram&#243;wka"
  ]
  node [
    id 52
    label "siatk&#243;wka"
  ]
  node [
    id 53
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 54
    label "block"
  ]
  node [
    id 55
    label "bie&#380;nia"
  ]
  node [
    id 56
    label "zesp&#243;&#322;"
  ]
  node [
    id 57
    label "obiekt_matematyczny"
  ]
  node [
    id 58
    label "stopie&#324;_pisma"
  ]
  node [
    id 59
    label "pozycja"
  ]
  node [
    id 60
    label "problemat"
  ]
  node [
    id 61
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 62
    label "obiekt"
  ]
  node [
    id 63
    label "point"
  ]
  node [
    id 64
    label "plamka"
  ]
  node [
    id 65
    label "przestrze&#324;"
  ]
  node [
    id 66
    label "mark"
  ]
  node [
    id 67
    label "ust&#281;p"
  ]
  node [
    id 68
    label "po&#322;o&#380;enie"
  ]
  node [
    id 69
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 70
    label "kres"
  ]
  node [
    id 71
    label "plan"
  ]
  node [
    id 72
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 73
    label "chwila"
  ]
  node [
    id 74
    label "podpunkt"
  ]
  node [
    id 75
    label "jednostka"
  ]
  node [
    id 76
    label "sprawa"
  ]
  node [
    id 77
    label "problematyka"
  ]
  node [
    id 78
    label "prosta"
  ]
  node [
    id 79
    label "wojsko"
  ]
  node [
    id 80
    label "zapunktowa&#263;"
  ]
  node [
    id 81
    label "rz&#261;d"
  ]
  node [
    id 82
    label "uwaga"
  ]
  node [
    id 83
    label "cecha"
  ]
  node [
    id 84
    label "praca"
  ]
  node [
    id 85
    label "plac"
  ]
  node [
    id 86
    label "location"
  ]
  node [
    id 87
    label "warunek_lokalowy"
  ]
  node [
    id 88
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 89
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 90
    label "cia&#322;o"
  ]
  node [
    id 91
    label "status"
  ]
  node [
    id 92
    label "koalicja"
  ]
  node [
    id 93
    label "parlament"
  ]
  node [
    id 94
    label "siedziba"
  ]
  node [
    id 95
    label "lewica"
  ]
  node [
    id 96
    label "prawica"
  ]
  node [
    id 97
    label "izba_ni&#380;sza"
  ]
  node [
    id 98
    label "zgromadzenie"
  ]
  node [
    id 99
    label "obrady"
  ]
  node [
    id 100
    label "parliament"
  ]
  node [
    id 101
    label "Los_Angeles"
  ]
  node [
    id 102
    label "rozwijanie"
  ]
  node [
    id 103
    label "training"
  ]
  node [
    id 104
    label "wysy&#322;anie"
  ]
  node [
    id 105
    label "o&#347;wiecanie"
  ]
  node [
    id 106
    label "kliker"
  ]
  node [
    id 107
    label "pomaganie"
  ]
  node [
    id 108
    label "zapoznawanie"
  ]
  node [
    id 109
    label "pouczenie"
  ]
  node [
    id 110
    label "heureza"
  ]
  node [
    id 111
    label "formation"
  ]
  node [
    id 112
    label "nauka"
  ]
  node [
    id 113
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 114
    label "typologia"
  ]
  node [
    id 115
    label "nomotetyczny"
  ]
  node [
    id 116
    label "wiedza"
  ]
  node [
    id 117
    label "dziedzina"
  ]
  node [
    id 118
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 119
    label "&#322;awa_szkolna"
  ]
  node [
    id 120
    label "nauki_o_poznaniu"
  ]
  node [
    id 121
    label "kultura_duchowa"
  ]
  node [
    id 122
    label "teoria_naukowa"
  ]
  node [
    id 123
    label "nauki_penalne"
  ]
  node [
    id 124
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 125
    label "nauki_o_Ziemi"
  ]
  node [
    id 126
    label "imagineskopia"
  ]
  node [
    id 127
    label "metodologia"
  ]
  node [
    id 128
    label "fotowoltaika"
  ]
  node [
    id 129
    label "proces"
  ]
  node [
    id 130
    label "inwentyka"
  ]
  node [
    id 131
    label "systematyka"
  ]
  node [
    id 132
    label "porada"
  ]
  node [
    id 133
    label "miasteczko_rowerowe"
  ]
  node [
    id 134
    label "przem&#243;wienie"
  ]
  node [
    id 135
    label "cover"
  ]
  node [
    id 136
    label "wytwarzanie"
  ]
  node [
    id 137
    label "przekazywanie"
  ]
  node [
    id 138
    label "nakazywanie"
  ]
  node [
    id 139
    label "transmission"
  ]
  node [
    id 140
    label "exploitation"
  ]
  node [
    id 141
    label "dodawanie"
  ]
  node [
    id 142
    label "rozpakowywanie"
  ]
  node [
    id 143
    label "rozwijanie_si&#281;"
  ]
  node [
    id 144
    label "opowiadanie"
  ]
  node [
    id 145
    label "rozk&#322;adanie"
  ]
  node [
    id 146
    label "oddzia&#322;ywanie"
  ]
  node [
    id 147
    label "rozstawianie"
  ]
  node [
    id 148
    label "zwi&#281;kszanie"
  ]
  node [
    id 149
    label "puszczanie"
  ]
  node [
    id 150
    label "development"
  ]
  node [
    id 151
    label "stawianie"
  ]
  node [
    id 152
    label "obznajamianie"
  ]
  node [
    id 153
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 154
    label "zawieranie"
  ]
  node [
    id 155
    label "merging"
  ]
  node [
    id 156
    label "informowanie"
  ]
  node [
    id 157
    label "recognition"
  ]
  node [
    id 158
    label "znajomy"
  ]
  node [
    id 159
    label "umo&#380;liwianie"
  ]
  node [
    id 160
    label "helping"
  ]
  node [
    id 161
    label "wyci&#261;ganie_pomocnej_d&#322;oni"
  ]
  node [
    id 162
    label "sprowadzanie"
  ]
  node [
    id 163
    label "u&#322;atwianie"
  ]
  node [
    id 164
    label "care"
  ]
  node [
    id 165
    label "aid"
  ]
  node [
    id 166
    label "skutkowanie"
  ]
  node [
    id 167
    label "dzianie_si&#281;"
  ]
  node [
    id 168
    label "robienie"
  ]
  node [
    id 169
    label "czynno&#347;&#263;"
  ]
  node [
    id 170
    label "metoda"
  ]
  node [
    id 171
    label "nauczanie"
  ]
  node [
    id 172
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 173
    label "poinformowanie"
  ]
  node [
    id 174
    label "information"
  ]
  node [
    id 175
    label "upomnienie"
  ]
  node [
    id 176
    label "wskaz&#243;wka"
  ]
  node [
    id 177
    label "o&#347;wietlanie"
  ]
  node [
    id 178
    label "light"
  ]
  node [
    id 179
    label "tresura"
  ]
  node [
    id 180
    label "narz&#281;dzie"
  ]
  node [
    id 181
    label "tytu&#322;"
  ]
  node [
    id 182
    label "inteligent"
  ]
  node [
    id 183
    label "Pierre-&#201;mile_Martin"
  ]
  node [
    id 184
    label "Tesla"
  ]
  node [
    id 185
    label "fachowiec"
  ]
  node [
    id 186
    label "druk"
  ]
  node [
    id 187
    label "mianowaniec"
  ]
  node [
    id 188
    label "podtytu&#322;"
  ]
  node [
    id 189
    label "poster"
  ]
  node [
    id 190
    label "publikacja"
  ]
  node [
    id 191
    label "nadtytu&#322;"
  ]
  node [
    id 192
    label "redaktor"
  ]
  node [
    id 193
    label "nazwa"
  ]
  node [
    id 194
    label "szata_graficzna"
  ]
  node [
    id 195
    label "debit"
  ]
  node [
    id 196
    label "tytulatura"
  ]
  node [
    id 197
    label "wyda&#263;"
  ]
  node [
    id 198
    label "elevation"
  ]
  node [
    id 199
    label "wydawa&#263;"
  ]
  node [
    id 200
    label "przedstawiciel"
  ]
  node [
    id 201
    label "cz&#322;owiek"
  ]
  node [
    id 202
    label "inteligencja"
  ]
  node [
    id 203
    label "robotnik"
  ]
  node [
    id 204
    label "macher"
  ]
  node [
    id 205
    label "specjalista"
  ]
  node [
    id 206
    label "us&#322;ugowiec"
  ]
  node [
    id 207
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 208
    label "politechnika"
  ]
  node [
    id 209
    label "&#347;l&#261;ski"
  ]
  node [
    id 210
    label "g&#322;&#243;wny"
  ]
  node [
    id 211
    label "tom"
  ]
  node [
    id 212
    label "Ko&#347;ciuszko"
  ]
  node [
    id 213
    label "laboratorium"
  ]
  node [
    id 214
    label "nowocze&#347;ni"
  ]
  node [
    id 215
    label "technologia"
  ]
  node [
    id 216
    label "przemys&#322;owy"
  ]
  node [
    id 217
    label "informatyczny"
  ]
  node [
    id 218
    label "studencki"
  ]
  node [
    id 219
    label "ko&#322;o"
  ]
  node [
    id 220
    label "naukowy"
  ]
  node [
    id 221
    label "energetyka"
  ]
  node [
    id 222
    label "komunalny"
  ]
  node [
    id 223
    label "Linuksa"
  ]
  node [
    id 224
    label "i"
  ]
  node [
    id 225
    label "wolny"
  ]
  node [
    id 226
    label "oprogramowa&#263;"
  ]
  node [
    id 227
    label "trwa&#322;o&#347;&#263;"
  ]
  node [
    id 228
    label "audytor"
  ]
  node [
    id 229
    label "energetyczny"
  ]
  node [
    id 230
    label "wyspa"
  ]
  node [
    id 231
    label "rybnik"
  ]
  node [
    id 232
    label "skaner"
  ]
  node [
    id 233
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 234
    label "algorytm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 208
    target 209
  ]
  edge [
    source 211
    target 212
  ]
  edge [
    source 213
    target 214
  ]
  edge [
    source 213
    target 215
  ]
  edge [
    source 213
    target 216
  ]
  edge [
    source 213
    target 217
  ]
  edge [
    source 214
    target 215
  ]
  edge [
    source 214
    target 216
  ]
  edge [
    source 215
    target 216
  ]
  edge [
    source 218
    target 219
  ]
  edge [
    source 218
    target 220
  ]
  edge [
    source 218
    target 221
  ]
  edge [
    source 218
    target 222
  ]
  edge [
    source 218
    target 223
  ]
  edge [
    source 218
    target 224
  ]
  edge [
    source 218
    target 225
  ]
  edge [
    source 218
    target 226
  ]
  edge [
    source 218
    target 227
  ]
  edge [
    source 218
    target 228
  ]
  edge [
    source 218
    target 229
  ]
  edge [
    source 218
    target 230
  ]
  edge [
    source 218
    target 231
  ]
  edge [
    source 218
    target 232
  ]
  edge [
    source 218
    target 233
  ]
  edge [
    source 218
    target 234
  ]
  edge [
    source 219
    target 220
  ]
  edge [
    source 219
    target 221
  ]
  edge [
    source 219
    target 222
  ]
  edge [
    source 219
    target 223
  ]
  edge [
    source 219
    target 224
  ]
  edge [
    source 219
    target 225
  ]
  edge [
    source 219
    target 226
  ]
  edge [
    source 219
    target 227
  ]
  edge [
    source 219
    target 228
  ]
  edge [
    source 219
    target 229
  ]
  edge [
    source 219
    target 230
  ]
  edge [
    source 219
    target 231
  ]
  edge [
    source 219
    target 232
  ]
  edge [
    source 219
    target 233
  ]
  edge [
    source 219
    target 234
  ]
  edge [
    source 220
    target 221
  ]
  edge [
    source 220
    target 222
  ]
  edge [
    source 220
    target 223
  ]
  edge [
    source 220
    target 224
  ]
  edge [
    source 220
    target 225
  ]
  edge [
    source 220
    target 226
  ]
  edge [
    source 220
    target 227
  ]
  edge [
    source 220
    target 233
  ]
  edge [
    source 220
    target 234
  ]
  edge [
    source 221
    target 222
  ]
  edge [
    source 223
    target 224
  ]
  edge [
    source 223
    target 225
  ]
  edge [
    source 223
    target 226
  ]
  edge [
    source 224
    target 225
  ]
  edge [
    source 224
    target 226
  ]
  edge [
    source 225
    target 226
  ]
  edge [
    source 228
    target 229
  ]
  edge [
    source 228
    target 230
  ]
  edge [
    source 228
    target 231
  ]
  edge [
    source 228
    target 232
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 229
    target 231
  ]
  edge [
    source 229
    target 232
  ]
  edge [
    source 230
    target 231
  ]
  edge [
    source 230
    target 232
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 233
    target 234
  ]
]
