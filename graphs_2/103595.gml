graph [
  node [
    id 0
    label "gdyby"
    origin "text"
  ]
  node [
    id 1
    label "droga"
    origin "text"
  ]
  node [
    id 2
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "prosta"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nudno"
    origin "text"
  ]
  node [
    id 6
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "tomasz"
    origin "text"
  ]
  node [
    id 8
    label "ekwipunek"
  ]
  node [
    id 9
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 10
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 11
    label "podbieg"
  ]
  node [
    id 12
    label "wyb&#243;j"
  ]
  node [
    id 13
    label "journey"
  ]
  node [
    id 14
    label "pobocze"
  ]
  node [
    id 15
    label "ekskursja"
  ]
  node [
    id 16
    label "drogowskaz"
  ]
  node [
    id 17
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 18
    label "budowla"
  ]
  node [
    id 19
    label "rajza"
  ]
  node [
    id 20
    label "passage"
  ]
  node [
    id 21
    label "marszrutyzacja"
  ]
  node [
    id 22
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 23
    label "trasa"
  ]
  node [
    id 24
    label "zbior&#243;wka"
  ]
  node [
    id 25
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 26
    label "spos&#243;b"
  ]
  node [
    id 27
    label "turystyka"
  ]
  node [
    id 28
    label "wylot"
  ]
  node [
    id 29
    label "ruch"
  ]
  node [
    id 30
    label "bezsilnikowy"
  ]
  node [
    id 31
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 32
    label "nawierzchnia"
  ]
  node [
    id 33
    label "korona_drogi"
  ]
  node [
    id 34
    label "przebieg"
  ]
  node [
    id 35
    label "infrastruktura"
  ]
  node [
    id 36
    label "w&#281;ze&#322;"
  ]
  node [
    id 37
    label "rzecz"
  ]
  node [
    id 38
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 39
    label "stan_surowy"
  ]
  node [
    id 40
    label "postanie"
  ]
  node [
    id 41
    label "zbudowa&#263;"
  ]
  node [
    id 42
    label "obudowywa&#263;"
  ]
  node [
    id 43
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 44
    label "obudowywanie"
  ]
  node [
    id 45
    label "konstrukcja"
  ]
  node [
    id 46
    label "Sukiennice"
  ]
  node [
    id 47
    label "kolumnada"
  ]
  node [
    id 48
    label "korpus"
  ]
  node [
    id 49
    label "zbudowanie"
  ]
  node [
    id 50
    label "fundament"
  ]
  node [
    id 51
    label "obudowa&#263;"
  ]
  node [
    id 52
    label "obudowanie"
  ]
  node [
    id 53
    label "zbi&#243;r"
  ]
  node [
    id 54
    label "model"
  ]
  node [
    id 55
    label "narz&#281;dzie"
  ]
  node [
    id 56
    label "nature"
  ]
  node [
    id 57
    label "tryb"
  ]
  node [
    id 58
    label "odcinek"
  ]
  node [
    id 59
    label "ton"
  ]
  node [
    id 60
    label "ambitus"
  ]
  node [
    id 61
    label "rozmiar"
  ]
  node [
    id 62
    label "skala"
  ]
  node [
    id 63
    label "czas"
  ]
  node [
    id 64
    label "move"
  ]
  node [
    id 65
    label "zmiana"
  ]
  node [
    id 66
    label "aktywno&#347;&#263;"
  ]
  node [
    id 67
    label "utrzymywanie"
  ]
  node [
    id 68
    label "utrzymywa&#263;"
  ]
  node [
    id 69
    label "taktyka"
  ]
  node [
    id 70
    label "d&#322;ugi"
  ]
  node [
    id 71
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 72
    label "natural_process"
  ]
  node [
    id 73
    label "kanciasty"
  ]
  node [
    id 74
    label "utrzyma&#263;"
  ]
  node [
    id 75
    label "myk"
  ]
  node [
    id 76
    label "manewr"
  ]
  node [
    id 77
    label "utrzymanie"
  ]
  node [
    id 78
    label "wydarzenie"
  ]
  node [
    id 79
    label "tumult"
  ]
  node [
    id 80
    label "stopek"
  ]
  node [
    id 81
    label "movement"
  ]
  node [
    id 82
    label "strumie&#324;"
  ]
  node [
    id 83
    label "czynno&#347;&#263;"
  ]
  node [
    id 84
    label "komunikacja"
  ]
  node [
    id 85
    label "lokomocja"
  ]
  node [
    id 86
    label "drift"
  ]
  node [
    id 87
    label "commercial_enterprise"
  ]
  node [
    id 88
    label "zjawisko"
  ]
  node [
    id 89
    label "apraksja"
  ]
  node [
    id 90
    label "proces"
  ]
  node [
    id 91
    label "poruszenie"
  ]
  node [
    id 92
    label "mechanika"
  ]
  node [
    id 93
    label "travel"
  ]
  node [
    id 94
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 95
    label "dyssypacja_energii"
  ]
  node [
    id 96
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 97
    label "kr&#243;tki"
  ]
  node [
    id 98
    label "r&#281;kaw"
  ]
  node [
    id 99
    label "koniec"
  ]
  node [
    id 100
    label "kontusz"
  ]
  node [
    id 101
    label "otw&#243;r"
  ]
  node [
    id 102
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 103
    label "warstwa"
  ]
  node [
    id 104
    label "pokrycie"
  ]
  node [
    id 105
    label "tablica"
  ]
  node [
    id 106
    label "fingerpost"
  ]
  node [
    id 107
    label "przydro&#380;e"
  ]
  node [
    id 108
    label "autostrada"
  ]
  node [
    id 109
    label "bieg"
  ]
  node [
    id 110
    label "operacja"
  ]
  node [
    id 111
    label "podr&#243;&#380;"
  ]
  node [
    id 112
    label "mieszanie_si&#281;"
  ]
  node [
    id 113
    label "chodzenie"
  ]
  node [
    id 114
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 115
    label "stray"
  ]
  node [
    id 116
    label "pozostawa&#263;"
  ]
  node [
    id 117
    label "s&#261;dzi&#263;"
  ]
  node [
    id 118
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 119
    label "digress"
  ]
  node [
    id 120
    label "chodzi&#263;"
  ]
  node [
    id 121
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 122
    label "wyposa&#380;enie"
  ]
  node [
    id 123
    label "nie&#347;miertelnik"
  ]
  node [
    id 124
    label "moderunek"
  ]
  node [
    id 125
    label "kocher"
  ]
  node [
    id 126
    label "dormitorium"
  ]
  node [
    id 127
    label "fotografia"
  ]
  node [
    id 128
    label "spis"
  ]
  node [
    id 129
    label "sk&#322;adanka"
  ]
  node [
    id 130
    label "polowanie"
  ]
  node [
    id 131
    label "wyprawa"
  ]
  node [
    id 132
    label "pomieszczenie"
  ]
  node [
    id 133
    label "beznap&#281;dowy"
  ]
  node [
    id 134
    label "erotyka"
  ]
  node [
    id 135
    label "zajawka"
  ]
  node [
    id 136
    label "love"
  ]
  node [
    id 137
    label "podniecanie"
  ]
  node [
    id 138
    label "po&#380;ycie"
  ]
  node [
    id 139
    label "ukochanie"
  ]
  node [
    id 140
    label "baraszki"
  ]
  node [
    id 141
    label "numer"
  ]
  node [
    id 142
    label "ruch_frykcyjny"
  ]
  node [
    id 143
    label "tendency"
  ]
  node [
    id 144
    label "wzw&#243;d"
  ]
  node [
    id 145
    label "serce"
  ]
  node [
    id 146
    label "wi&#281;&#378;"
  ]
  node [
    id 147
    label "cz&#322;owiek"
  ]
  node [
    id 148
    label "seks"
  ]
  node [
    id 149
    label "pozycja_misjonarska"
  ]
  node [
    id 150
    label "rozmna&#380;anie"
  ]
  node [
    id 151
    label "feblik"
  ]
  node [
    id 152
    label "z&#322;&#261;czenie"
  ]
  node [
    id 153
    label "imisja"
  ]
  node [
    id 154
    label "podniecenie"
  ]
  node [
    id 155
    label "podnieca&#263;"
  ]
  node [
    id 156
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 157
    label "zakochanie"
  ]
  node [
    id 158
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 159
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 160
    label "gra_wst&#281;pna"
  ]
  node [
    id 161
    label "drogi"
  ]
  node [
    id 162
    label "po&#380;&#261;danie"
  ]
  node [
    id 163
    label "podnieci&#263;"
  ]
  node [
    id 164
    label "emocja"
  ]
  node [
    id 165
    label "afekt"
  ]
  node [
    id 166
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 167
    label "na_pieska"
  ]
  node [
    id 168
    label "kochanka"
  ]
  node [
    id 169
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 170
    label "kultura_fizyczna"
  ]
  node [
    id 171
    label "turyzm"
  ]
  node [
    id 172
    label "partnerka"
  ]
  node [
    id 173
    label "aktorka"
  ]
  node [
    id 174
    label "partner"
  ]
  node [
    id 175
    label "kobieta"
  ]
  node [
    id 176
    label "kobita"
  ]
  node [
    id 177
    label "proste_sko&#347;ne"
  ]
  node [
    id 178
    label "punkt"
  ]
  node [
    id 179
    label "krzywa"
  ]
  node [
    id 180
    label "straight_line"
  ]
  node [
    id 181
    label "linia"
  ]
  node [
    id 182
    label "curve"
  ]
  node [
    id 183
    label "prowadzi&#263;"
  ]
  node [
    id 184
    label "curvature"
  ]
  node [
    id 185
    label "prowadzenie"
  ]
  node [
    id 186
    label "figura_geometryczna"
  ]
  node [
    id 187
    label "poprowadzi&#263;"
  ]
  node [
    id 188
    label "chronometria"
  ]
  node [
    id 189
    label "odczyt"
  ]
  node [
    id 190
    label "laba"
  ]
  node [
    id 191
    label "czasoprzestrze&#324;"
  ]
  node [
    id 192
    label "time_period"
  ]
  node [
    id 193
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 194
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 195
    label "Zeitgeist"
  ]
  node [
    id 196
    label "pochodzenie"
  ]
  node [
    id 197
    label "przep&#322;ywanie"
  ]
  node [
    id 198
    label "schy&#322;ek"
  ]
  node [
    id 199
    label "czwarty_wymiar"
  ]
  node [
    id 200
    label "kategoria_gramatyczna"
  ]
  node [
    id 201
    label "poprzedzi&#263;"
  ]
  node [
    id 202
    label "pogoda"
  ]
  node [
    id 203
    label "czasokres"
  ]
  node [
    id 204
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 205
    label "poprzedzenie"
  ]
  node [
    id 206
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 207
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 208
    label "dzieje"
  ]
  node [
    id 209
    label "zegar"
  ]
  node [
    id 210
    label "koniugacja"
  ]
  node [
    id 211
    label "trawi&#263;"
  ]
  node [
    id 212
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 213
    label "poprzedza&#263;"
  ]
  node [
    id 214
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 215
    label "trawienie"
  ]
  node [
    id 216
    label "chwila"
  ]
  node [
    id 217
    label "rachuba_czasu"
  ]
  node [
    id 218
    label "poprzedzanie"
  ]
  node [
    id 219
    label "okres_czasu"
  ]
  node [
    id 220
    label "period"
  ]
  node [
    id 221
    label "odwlekanie_si&#281;"
  ]
  node [
    id 222
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 223
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 224
    label "pochodzi&#263;"
  ]
  node [
    id 225
    label "pole"
  ]
  node [
    id 226
    label "part"
  ]
  node [
    id 227
    label "line"
  ]
  node [
    id 228
    label "fragment"
  ]
  node [
    id 229
    label "kawa&#322;ek"
  ]
  node [
    id 230
    label "teren"
  ]
  node [
    id 231
    label "coupon"
  ]
  node [
    id 232
    label "epizod"
  ]
  node [
    id 233
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 234
    label "moneta"
  ]
  node [
    id 235
    label "pokwitowanie"
  ]
  node [
    id 236
    label "obiekt_matematyczny"
  ]
  node [
    id 237
    label "stopie&#324;_pisma"
  ]
  node [
    id 238
    label "pozycja"
  ]
  node [
    id 239
    label "problemat"
  ]
  node [
    id 240
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 241
    label "obiekt"
  ]
  node [
    id 242
    label "point"
  ]
  node [
    id 243
    label "plamka"
  ]
  node [
    id 244
    label "przestrze&#324;"
  ]
  node [
    id 245
    label "mark"
  ]
  node [
    id 246
    label "ust&#281;p"
  ]
  node [
    id 247
    label "po&#322;o&#380;enie"
  ]
  node [
    id 248
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 249
    label "miejsce"
  ]
  node [
    id 250
    label "kres"
  ]
  node [
    id 251
    label "plan"
  ]
  node [
    id 252
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 253
    label "podpunkt"
  ]
  node [
    id 254
    label "jednostka"
  ]
  node [
    id 255
    label "sprawa"
  ]
  node [
    id 256
    label "problematyka"
  ]
  node [
    id 257
    label "wojsko"
  ]
  node [
    id 258
    label "zapunktowa&#263;"
  ]
  node [
    id 259
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 260
    label "stan"
  ]
  node [
    id 261
    label "stand"
  ]
  node [
    id 262
    label "trwa&#263;"
  ]
  node [
    id 263
    label "equal"
  ]
  node [
    id 264
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 265
    label "uczestniczy&#263;"
  ]
  node [
    id 266
    label "obecno&#347;&#263;"
  ]
  node [
    id 267
    label "si&#281;ga&#263;"
  ]
  node [
    id 268
    label "mie&#263;_miejsce"
  ]
  node [
    id 269
    label "robi&#263;"
  ]
  node [
    id 270
    label "participate"
  ]
  node [
    id 271
    label "adhere"
  ]
  node [
    id 272
    label "zostawa&#263;"
  ]
  node [
    id 273
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 274
    label "istnie&#263;"
  ]
  node [
    id 275
    label "compass"
  ]
  node [
    id 276
    label "exsert"
  ]
  node [
    id 277
    label "get"
  ]
  node [
    id 278
    label "u&#380;ywa&#263;"
  ]
  node [
    id 279
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 280
    label "osi&#261;ga&#263;"
  ]
  node [
    id 281
    label "korzysta&#263;"
  ]
  node [
    id 282
    label "appreciation"
  ]
  node [
    id 283
    label "dociera&#263;"
  ]
  node [
    id 284
    label "mierzy&#263;"
  ]
  node [
    id 285
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 286
    label "being"
  ]
  node [
    id 287
    label "cecha"
  ]
  node [
    id 288
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "proceed"
  ]
  node [
    id 290
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 291
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 292
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 293
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 294
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 295
    label "str&#243;j"
  ]
  node [
    id 296
    label "para"
  ]
  node [
    id 297
    label "krok"
  ]
  node [
    id 298
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 299
    label "przebiega&#263;"
  ]
  node [
    id 300
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 301
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 302
    label "continue"
  ]
  node [
    id 303
    label "carry"
  ]
  node [
    id 304
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 305
    label "wk&#322;ada&#263;"
  ]
  node [
    id 306
    label "p&#322;ywa&#263;"
  ]
  node [
    id 307
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 308
    label "bangla&#263;"
  ]
  node [
    id 309
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 310
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 311
    label "bywa&#263;"
  ]
  node [
    id 312
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 313
    label "dziama&#263;"
  ]
  node [
    id 314
    label "run"
  ]
  node [
    id 315
    label "stara&#263;_si&#281;"
  ]
  node [
    id 316
    label "Arakan"
  ]
  node [
    id 317
    label "Teksas"
  ]
  node [
    id 318
    label "Georgia"
  ]
  node [
    id 319
    label "Maryland"
  ]
  node [
    id 320
    label "Michigan"
  ]
  node [
    id 321
    label "Massachusetts"
  ]
  node [
    id 322
    label "Luizjana"
  ]
  node [
    id 323
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 324
    label "samopoczucie"
  ]
  node [
    id 325
    label "Floryda"
  ]
  node [
    id 326
    label "Ohio"
  ]
  node [
    id 327
    label "Alaska"
  ]
  node [
    id 328
    label "Nowy_Meksyk"
  ]
  node [
    id 329
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 330
    label "wci&#281;cie"
  ]
  node [
    id 331
    label "Kansas"
  ]
  node [
    id 332
    label "Alabama"
  ]
  node [
    id 333
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 334
    label "Kalifornia"
  ]
  node [
    id 335
    label "Wirginia"
  ]
  node [
    id 336
    label "Nowy_York"
  ]
  node [
    id 337
    label "Waszyngton"
  ]
  node [
    id 338
    label "Pensylwania"
  ]
  node [
    id 339
    label "wektor"
  ]
  node [
    id 340
    label "Hawaje"
  ]
  node [
    id 341
    label "state"
  ]
  node [
    id 342
    label "poziom"
  ]
  node [
    id 343
    label "jednostka_administracyjna"
  ]
  node [
    id 344
    label "Illinois"
  ]
  node [
    id 345
    label "Oklahoma"
  ]
  node [
    id 346
    label "Oregon"
  ]
  node [
    id 347
    label "Arizona"
  ]
  node [
    id 348
    label "ilo&#347;&#263;"
  ]
  node [
    id 349
    label "Jukatan"
  ]
  node [
    id 350
    label "shape"
  ]
  node [
    id 351
    label "Goa"
  ]
  node [
    id 352
    label "nudny"
  ]
  node [
    id 353
    label "blandly"
  ]
  node [
    id 354
    label "zwyczajnie"
  ]
  node [
    id 355
    label "nieefektownie"
  ]
  node [
    id 356
    label "nieciekawie"
  ]
  node [
    id 357
    label "nijaki"
  ]
  node [
    id 358
    label "boringly"
  ]
  node [
    id 359
    label "poprostu"
  ]
  node [
    id 360
    label "zwyczajny"
  ]
  node [
    id 361
    label "cz&#281;sto"
  ]
  node [
    id 362
    label "zwyk&#322;y"
  ]
  node [
    id 363
    label "&#378;le"
  ]
  node [
    id 364
    label "nieciekawy"
  ]
  node [
    id 365
    label "niepokoj&#261;co"
  ]
  node [
    id 366
    label "nieatrakcyjnie"
  ]
  node [
    id 367
    label "nieefektowny"
  ]
  node [
    id 368
    label "bezbarwnie"
  ]
  node [
    id 369
    label "oboj&#281;tny"
  ]
  node [
    id 370
    label "szarzenie"
  ]
  node [
    id 371
    label "neutralny"
  ]
  node [
    id 372
    label "&#380;aden"
  ]
  node [
    id 373
    label "poszarzenie"
  ]
  node [
    id 374
    label "niezabawny"
  ]
  node [
    id 375
    label "nijak"
  ]
  node [
    id 376
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 377
    label "okre&#347;li&#263;"
  ]
  node [
    id 378
    label "express"
  ]
  node [
    id 379
    label "wydoby&#263;"
  ]
  node [
    id 380
    label "wyrazi&#263;"
  ]
  node [
    id 381
    label "poda&#263;"
  ]
  node [
    id 382
    label "unwrap"
  ]
  node [
    id 383
    label "rzekn&#261;&#263;"
  ]
  node [
    id 384
    label "discover"
  ]
  node [
    id 385
    label "convey"
  ]
  node [
    id 386
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 387
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 388
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 389
    label "siatk&#243;wka"
  ]
  node [
    id 390
    label "nafaszerowa&#263;"
  ]
  node [
    id 391
    label "supply"
  ]
  node [
    id 392
    label "tenis"
  ]
  node [
    id 393
    label "jedzenie"
  ]
  node [
    id 394
    label "ustawi&#263;"
  ]
  node [
    id 395
    label "poinformowa&#263;"
  ]
  node [
    id 396
    label "give"
  ]
  node [
    id 397
    label "zagra&#263;"
  ]
  node [
    id 398
    label "da&#263;"
  ]
  node [
    id 399
    label "zaserwowa&#263;"
  ]
  node [
    id 400
    label "introduce"
  ]
  node [
    id 401
    label "wydosta&#263;"
  ]
  node [
    id 402
    label "wyj&#261;&#263;"
  ]
  node [
    id 403
    label "ocali&#263;"
  ]
  node [
    id 404
    label "g&#243;rnictwo"
  ]
  node [
    id 405
    label "distill"
  ]
  node [
    id 406
    label "extract"
  ]
  node [
    id 407
    label "obtain"
  ]
  node [
    id 408
    label "uwydatni&#263;"
  ]
  node [
    id 409
    label "draw"
  ]
  node [
    id 410
    label "raise"
  ]
  node [
    id 411
    label "wyeksploatowa&#263;"
  ]
  node [
    id 412
    label "wyda&#263;"
  ]
  node [
    id 413
    label "uzyska&#263;"
  ]
  node [
    id 414
    label "doby&#263;"
  ]
  node [
    id 415
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 416
    label "testify"
  ]
  node [
    id 417
    label "oznaczy&#263;"
  ]
  node [
    id 418
    label "zakomunikowa&#263;"
  ]
  node [
    id 419
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 420
    label "vent"
  ]
  node [
    id 421
    label "situate"
  ]
  node [
    id 422
    label "zdecydowa&#263;"
  ]
  node [
    id 423
    label "zrobi&#263;"
  ]
  node [
    id 424
    label "nominate"
  ]
  node [
    id 425
    label "spowodowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
]
