graph [
  node [
    id 0
    label "szary"
    origin "text"
  ]
  node [
    id 1
    label "myszka"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "anonka"
    origin "text"
  ]
  node [
    id 4
    label "chmurnienie"
  ]
  node [
    id 5
    label "p&#322;owy"
  ]
  node [
    id 6
    label "niezabawny"
  ]
  node [
    id 7
    label "srebrny"
  ]
  node [
    id 8
    label "brzydki"
  ]
  node [
    id 9
    label "szarzenie"
  ]
  node [
    id 10
    label "blady"
  ]
  node [
    id 11
    label "zwyczajny"
  ]
  node [
    id 12
    label "pochmurno"
  ]
  node [
    id 13
    label "zielono"
  ]
  node [
    id 14
    label "oboj&#281;tny"
  ]
  node [
    id 15
    label "poszarzenie"
  ]
  node [
    id 16
    label "szaro"
  ]
  node [
    id 17
    label "ch&#322;odny"
  ]
  node [
    id 18
    label "spochmurnienie"
  ]
  node [
    id 19
    label "zwyk&#322;y"
  ]
  node [
    id 20
    label "bezbarwnie"
  ]
  node [
    id 21
    label "nieciekawy"
  ]
  node [
    id 22
    label "zbrzydni&#281;cie"
  ]
  node [
    id 23
    label "skandaliczny"
  ]
  node [
    id 24
    label "nieprzyjemny"
  ]
  node [
    id 25
    label "oszpecanie"
  ]
  node [
    id 26
    label "brzydni&#281;cie"
  ]
  node [
    id 27
    label "nie&#322;adny"
  ]
  node [
    id 28
    label "brzydko"
  ]
  node [
    id 29
    label "nieprzyzwoity"
  ]
  node [
    id 30
    label "oszpecenie"
  ]
  node [
    id 31
    label "przeci&#281;tny"
  ]
  node [
    id 32
    label "zwyczajnie"
  ]
  node [
    id 33
    label "zwykle"
  ]
  node [
    id 34
    label "cz&#281;sty"
  ]
  node [
    id 35
    label "okre&#347;lony"
  ]
  node [
    id 36
    label "nieatrakcyjny"
  ]
  node [
    id 37
    label "blado"
  ]
  node [
    id 38
    label "mizerny"
  ]
  node [
    id 39
    label "nienasycony"
  ]
  node [
    id 40
    label "s&#322;aby"
  ]
  node [
    id 41
    label "niewa&#380;ny"
  ]
  node [
    id 42
    label "jasny"
  ]
  node [
    id 43
    label "zi&#281;bienie"
  ]
  node [
    id 44
    label "niesympatyczny"
  ]
  node [
    id 45
    label "och&#322;odzenie"
  ]
  node [
    id 46
    label "opanowany"
  ]
  node [
    id 47
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 48
    label "rozs&#261;dny"
  ]
  node [
    id 49
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 50
    label "sch&#322;adzanie"
  ]
  node [
    id 51
    label "ch&#322;odno"
  ]
  node [
    id 52
    label "oswojony"
  ]
  node [
    id 53
    label "na&#322;o&#380;ny"
  ]
  node [
    id 54
    label "nieciekawie"
  ]
  node [
    id 55
    label "z&#322;y"
  ]
  node [
    id 56
    label "zoboj&#281;tnienie"
  ]
  node [
    id 57
    label "nieszkodliwy"
  ]
  node [
    id 58
    label "&#347;ni&#281;ty"
  ]
  node [
    id 59
    label "oboj&#281;tnie"
  ]
  node [
    id 60
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 61
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 62
    label "neutralizowanie"
  ]
  node [
    id 63
    label "bierny"
  ]
  node [
    id 64
    label "zneutralizowanie"
  ]
  node [
    id 65
    label "neutralny"
  ]
  node [
    id 66
    label "zasnuwanie_si&#281;"
  ]
  node [
    id 67
    label "powa&#380;nienie"
  ]
  node [
    id 68
    label "pochmurny"
  ]
  node [
    id 69
    label "zasnucie_si&#281;"
  ]
  node [
    id 70
    label "zielony"
  ]
  node [
    id 71
    label "&#380;ywo"
  ]
  node [
    id 72
    label "&#347;wie&#380;o"
  ]
  node [
    id 73
    label "blandly"
  ]
  node [
    id 74
    label "nieefektownie"
  ]
  node [
    id 75
    label "nijaki"
  ]
  node [
    id 76
    label "zabarwienie_si&#281;"
  ]
  node [
    id 77
    label "rozwidnienie_si&#281;"
  ]
  node [
    id 78
    label "zmierzchni&#281;cie_si&#281;"
  ]
  node [
    id 79
    label "stanie_si&#281;"
  ]
  node [
    id 80
    label "rozwidnianie_si&#281;"
  ]
  node [
    id 81
    label "odcinanie_si&#281;"
  ]
  node [
    id 82
    label "zmierzchanie_si&#281;"
  ]
  node [
    id 83
    label "barwienie_si&#281;"
  ]
  node [
    id 84
    label "stawanie_si&#281;"
  ]
  node [
    id 85
    label "srebrzenie"
  ]
  node [
    id 86
    label "metaliczny"
  ]
  node [
    id 87
    label "srebrzy&#347;cie"
  ]
  node [
    id 88
    label "posrebrzenie"
  ]
  node [
    id 89
    label "srebrzenie_si&#281;"
  ]
  node [
    id 90
    label "utytu&#322;owany"
  ]
  node [
    id 91
    label "srebrno"
  ]
  node [
    id 92
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 93
    label "p&#322;owo"
  ]
  node [
    id 94
    label "&#380;&#243;&#322;tawy"
  ]
  node [
    id 95
    label "niedobrze"
  ]
  node [
    id 96
    label "wyblak&#322;y"
  ]
  node [
    id 97
    label "bezbarwny"
  ]
  node [
    id 98
    label "znami&#281;"
  ]
  node [
    id 99
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 100
    label "klawisz_myszki"
  ]
  node [
    id 101
    label "bouquet"
  ]
  node [
    id 102
    label "srom"
  ]
  node [
    id 103
    label "kobieta"
  ]
  node [
    id 104
    label "komputer"
  ]
  node [
    id 105
    label "kuciapa"
  ]
  node [
    id 106
    label "warga_sromowa"
  ]
  node [
    id 107
    label "wilk"
  ]
  node [
    id 108
    label "wstyd"
  ]
  node [
    id 109
    label "cunnilingus"
  ]
  node [
    id 110
    label "&#322;echtaczka"
  ]
  node [
    id 111
    label "b&#322;ona_dziewicza"
  ]
  node [
    id 112
    label "cipa"
  ]
  node [
    id 113
    label "uj&#347;cie_pochwy"
  ]
  node [
    id 114
    label "przedsionek_pochwy"
  ]
  node [
    id 115
    label "doros&#322;y"
  ]
  node [
    id 116
    label "&#380;ona"
  ]
  node [
    id 117
    label "cz&#322;owiek"
  ]
  node [
    id 118
    label "samica"
  ]
  node [
    id 119
    label "uleganie"
  ]
  node [
    id 120
    label "ulec"
  ]
  node [
    id 121
    label "m&#281;&#380;yna"
  ]
  node [
    id 122
    label "partnerka"
  ]
  node [
    id 123
    label "ulegni&#281;cie"
  ]
  node [
    id 124
    label "pa&#324;stwo"
  ]
  node [
    id 125
    label "&#322;ono"
  ]
  node [
    id 126
    label "menopauza"
  ]
  node [
    id 127
    label "przekwitanie"
  ]
  node [
    id 128
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 129
    label "babka"
  ]
  node [
    id 130
    label "ulega&#263;"
  ]
  node [
    id 131
    label "stamp"
  ]
  node [
    id 132
    label "wygl&#261;d"
  ]
  node [
    id 133
    label "s&#322;upek"
  ]
  node [
    id 134
    label "okrytonasienne"
  ]
  node [
    id 135
    label "cecha"
  ]
  node [
    id 136
    label "py&#322;ek"
  ]
  node [
    id 137
    label "zmiana"
  ]
  node [
    id 138
    label "stacja_dysk&#243;w"
  ]
  node [
    id 139
    label "instalowa&#263;"
  ]
  node [
    id 140
    label "moc_obliczeniowa"
  ]
  node [
    id 141
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 142
    label "pad"
  ]
  node [
    id 143
    label "modem"
  ]
  node [
    id 144
    label "pami&#281;&#263;"
  ]
  node [
    id 145
    label "monitor"
  ]
  node [
    id 146
    label "zainstalowanie"
  ]
  node [
    id 147
    label "emulacja"
  ]
  node [
    id 148
    label "zainstalowa&#263;"
  ]
  node [
    id 149
    label "procesor"
  ]
  node [
    id 150
    label "maszyna_Turinga"
  ]
  node [
    id 151
    label "karta"
  ]
  node [
    id 152
    label "twardy_dysk"
  ]
  node [
    id 153
    label "klawiatura"
  ]
  node [
    id 154
    label "botnet"
  ]
  node [
    id 155
    label "mysz"
  ]
  node [
    id 156
    label "instalowanie"
  ]
  node [
    id 157
    label "radiator"
  ]
  node [
    id 158
    label "urz&#261;dzenie"
  ]
  node [
    id 159
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 2
    target 3
  ]
]
