graph [
  node [
    id 0
    label "serial"
    origin "text"
  ]
  node [
    id 1
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 2
    label "znikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "serwis"
    origin "text"
  ]
  node [
    id 4
    label "netflix"
    origin "text"
  ]
  node [
    id 5
    label "program_telewizyjny"
  ]
  node [
    id 6
    label "film"
  ]
  node [
    id 7
    label "seria"
  ]
  node [
    id 8
    label "Klan"
  ]
  node [
    id 9
    label "Ranczo"
  ]
  node [
    id 10
    label "set"
  ]
  node [
    id 11
    label "przebieg"
  ]
  node [
    id 12
    label "zbi&#243;r"
  ]
  node [
    id 13
    label "jednostka"
  ]
  node [
    id 14
    label "jednostka_systematyczna"
  ]
  node [
    id 15
    label "stage_set"
  ]
  node [
    id 16
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 17
    label "d&#378;wi&#281;k"
  ]
  node [
    id 18
    label "komplet"
  ]
  node [
    id 19
    label "line"
  ]
  node [
    id 20
    label "sekwencja"
  ]
  node [
    id 21
    label "zestawienie"
  ]
  node [
    id 22
    label "partia"
  ]
  node [
    id 23
    label "produkcja"
  ]
  node [
    id 24
    label "animatronika"
  ]
  node [
    id 25
    label "odczulenie"
  ]
  node [
    id 26
    label "odczula&#263;"
  ]
  node [
    id 27
    label "blik"
  ]
  node [
    id 28
    label "odczuli&#263;"
  ]
  node [
    id 29
    label "scena"
  ]
  node [
    id 30
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 31
    label "muza"
  ]
  node [
    id 32
    label "postprodukcja"
  ]
  node [
    id 33
    label "block"
  ]
  node [
    id 34
    label "trawiarnia"
  ]
  node [
    id 35
    label "sklejarka"
  ]
  node [
    id 36
    label "sztuka"
  ]
  node [
    id 37
    label "uj&#281;cie"
  ]
  node [
    id 38
    label "filmoteka"
  ]
  node [
    id 39
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 40
    label "klatka"
  ]
  node [
    id 41
    label "rozbieg&#243;wka"
  ]
  node [
    id 42
    label "napisy"
  ]
  node [
    id 43
    label "ta&#347;ma"
  ]
  node [
    id 44
    label "odczulanie"
  ]
  node [
    id 45
    label "anamorfoza"
  ]
  node [
    id 46
    label "dorobek"
  ]
  node [
    id 47
    label "ty&#322;&#243;wka"
  ]
  node [
    id 48
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 49
    label "b&#322;ona"
  ]
  node [
    id 50
    label "emulsja_fotograficzna"
  ]
  node [
    id 51
    label "photograph"
  ]
  node [
    id 52
    label "czo&#322;&#243;wka"
  ]
  node [
    id 53
    label "rola"
  ]
  node [
    id 54
    label "kochanek"
  ]
  node [
    id 55
    label "kum"
  ]
  node [
    id 56
    label "amikus"
  ]
  node [
    id 57
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 58
    label "pobratymiec"
  ]
  node [
    id 59
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 60
    label "drogi"
  ]
  node [
    id 61
    label "sympatyk"
  ]
  node [
    id 62
    label "bratnia_dusza"
  ]
  node [
    id 63
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 64
    label "mi&#322;y"
  ]
  node [
    id 65
    label "kocha&#347;"
  ]
  node [
    id 66
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 67
    label "zwrot"
  ]
  node [
    id 68
    label "partner"
  ]
  node [
    id 69
    label "bratek"
  ]
  node [
    id 70
    label "fagas"
  ]
  node [
    id 71
    label "ojczyc"
  ]
  node [
    id 72
    label "stronnik"
  ]
  node [
    id 73
    label "pobratymca"
  ]
  node [
    id 74
    label "plemiennik"
  ]
  node [
    id 75
    label "wsp&#243;&#322;obywatel"
  ]
  node [
    id 76
    label "wsp&#243;&#322;ziomek"
  ]
  node [
    id 77
    label "brat"
  ]
  node [
    id 78
    label "chrzest"
  ]
  node [
    id 79
    label "kumostwo"
  ]
  node [
    id 80
    label "cz&#322;owiek"
  ]
  node [
    id 81
    label "zwolennik"
  ]
  node [
    id 82
    label "drogo"
  ]
  node [
    id 83
    label "bliski"
  ]
  node [
    id 84
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 85
    label "warto&#347;ciowy"
  ]
  node [
    id 86
    label "sta&#263;_si&#281;"
  ]
  node [
    id 87
    label "vanish"
  ]
  node [
    id 88
    label "wyj&#347;&#263;"
  ]
  node [
    id 89
    label "die"
  ]
  node [
    id 90
    label "dissolve"
  ]
  node [
    id 91
    label "zgin&#261;&#263;"
  ]
  node [
    id 92
    label "przepa&#347;&#263;"
  ]
  node [
    id 93
    label "r&#243;&#380;nica"
  ]
  node [
    id 94
    label "podzia&#263;_si&#281;"
  ]
  node [
    id 95
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 96
    label "gulf"
  ]
  node [
    id 97
    label "zmarnowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "pa&#347;&#263;"
  ]
  node [
    id 99
    label "przegra&#263;"
  ]
  node [
    id 100
    label "dziura"
  ]
  node [
    id 101
    label "collapse"
  ]
  node [
    id 102
    label "po&#380;egna&#263;_si&#281;_z_&#380;yciem"
  ]
  node [
    id 103
    label "sko&#324;czy&#263;"
  ]
  node [
    id 104
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 105
    label "fail"
  ]
  node [
    id 106
    label "przesta&#263;"
  ]
  node [
    id 107
    label "ograniczenie"
  ]
  node [
    id 108
    label "drop"
  ]
  node [
    id 109
    label "ruszy&#263;"
  ]
  node [
    id 110
    label "zademonstrowa&#263;"
  ]
  node [
    id 111
    label "leave"
  ]
  node [
    id 112
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 113
    label "uko&#324;czy&#263;"
  ]
  node [
    id 114
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 115
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 116
    label "mount"
  ]
  node [
    id 117
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 118
    label "get"
  ]
  node [
    id 119
    label "moderate"
  ]
  node [
    id 120
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 121
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 122
    label "zej&#347;&#263;"
  ]
  node [
    id 123
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 124
    label "wystarczy&#263;"
  ]
  node [
    id 125
    label "perform"
  ]
  node [
    id 126
    label "open"
  ]
  node [
    id 127
    label "drive"
  ]
  node [
    id 128
    label "zagra&#263;"
  ]
  node [
    id 129
    label "uzyska&#263;"
  ]
  node [
    id 130
    label "opu&#347;ci&#263;"
  ]
  node [
    id 131
    label "wypa&#347;&#263;"
  ]
  node [
    id 132
    label "punkt"
  ]
  node [
    id 133
    label "YouTube"
  ]
  node [
    id 134
    label "wytw&#243;r"
  ]
  node [
    id 135
    label "zak&#322;ad"
  ]
  node [
    id 136
    label "uderzenie"
  ]
  node [
    id 137
    label "service"
  ]
  node [
    id 138
    label "us&#322;uga"
  ]
  node [
    id 139
    label "porcja"
  ]
  node [
    id 140
    label "zastawa"
  ]
  node [
    id 141
    label "mecz"
  ]
  node [
    id 142
    label "strona"
  ]
  node [
    id 143
    label "doniesienie"
  ]
  node [
    id 144
    label "instrumentalizacja"
  ]
  node [
    id 145
    label "trafienie"
  ]
  node [
    id 146
    label "walka"
  ]
  node [
    id 147
    label "cios"
  ]
  node [
    id 148
    label "zdarzenie_si&#281;"
  ]
  node [
    id 149
    label "wdarcie_si&#281;"
  ]
  node [
    id 150
    label "pogorszenie"
  ]
  node [
    id 151
    label "poczucie"
  ]
  node [
    id 152
    label "coup"
  ]
  node [
    id 153
    label "reakcja"
  ]
  node [
    id 154
    label "contact"
  ]
  node [
    id 155
    label "stukni&#281;cie"
  ]
  node [
    id 156
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 157
    label "bat"
  ]
  node [
    id 158
    label "spowodowanie"
  ]
  node [
    id 159
    label "rush"
  ]
  node [
    id 160
    label "odbicie"
  ]
  node [
    id 161
    label "dawka"
  ]
  node [
    id 162
    label "zadanie"
  ]
  node [
    id 163
    label "&#347;ci&#281;cie"
  ]
  node [
    id 164
    label "st&#322;uczenie"
  ]
  node [
    id 165
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 166
    label "time"
  ]
  node [
    id 167
    label "odbicie_si&#281;"
  ]
  node [
    id 168
    label "dotkni&#281;cie"
  ]
  node [
    id 169
    label "charge"
  ]
  node [
    id 170
    label "dostanie"
  ]
  node [
    id 171
    label "skrytykowanie"
  ]
  node [
    id 172
    label "zagrywka"
  ]
  node [
    id 173
    label "manewr"
  ]
  node [
    id 174
    label "nast&#261;pienie"
  ]
  node [
    id 175
    label "uderzanie"
  ]
  node [
    id 176
    label "pogoda"
  ]
  node [
    id 177
    label "stroke"
  ]
  node [
    id 178
    label "pobicie"
  ]
  node [
    id 179
    label "ruch"
  ]
  node [
    id 180
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 181
    label "flap"
  ]
  node [
    id 182
    label "dotyk"
  ]
  node [
    id 183
    label "zrobienie"
  ]
  node [
    id 184
    label "produkt_gotowy"
  ]
  node [
    id 185
    label "asortyment"
  ]
  node [
    id 186
    label "czynno&#347;&#263;"
  ]
  node [
    id 187
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 188
    label "&#347;wiadczenie"
  ]
  node [
    id 189
    label "element_wyposa&#380;enia"
  ]
  node [
    id 190
    label "sto&#322;owizna"
  ]
  node [
    id 191
    label "przedmiot"
  ]
  node [
    id 192
    label "p&#322;&#243;d"
  ]
  node [
    id 193
    label "work"
  ]
  node [
    id 194
    label "rezultat"
  ]
  node [
    id 195
    label "zas&#243;b"
  ]
  node [
    id 196
    label "ilo&#347;&#263;"
  ]
  node [
    id 197
    label "&#380;o&#322;d"
  ]
  node [
    id 198
    label "zak&#322;adka"
  ]
  node [
    id 199
    label "jednostka_organizacyjna"
  ]
  node [
    id 200
    label "miejsce_pracy"
  ]
  node [
    id 201
    label "instytucja"
  ]
  node [
    id 202
    label "wyko&#324;czenie"
  ]
  node [
    id 203
    label "firma"
  ]
  node [
    id 204
    label "czyn"
  ]
  node [
    id 205
    label "company"
  ]
  node [
    id 206
    label "instytut"
  ]
  node [
    id 207
    label "umowa"
  ]
  node [
    id 208
    label "po&#322;o&#380;enie"
  ]
  node [
    id 209
    label "sprawa"
  ]
  node [
    id 210
    label "ust&#281;p"
  ]
  node [
    id 211
    label "plan"
  ]
  node [
    id 212
    label "obiekt_matematyczny"
  ]
  node [
    id 213
    label "problemat"
  ]
  node [
    id 214
    label "plamka"
  ]
  node [
    id 215
    label "stopie&#324;_pisma"
  ]
  node [
    id 216
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 217
    label "miejsce"
  ]
  node [
    id 218
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 219
    label "mark"
  ]
  node [
    id 220
    label "chwila"
  ]
  node [
    id 221
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 222
    label "prosta"
  ]
  node [
    id 223
    label "problematyka"
  ]
  node [
    id 224
    label "obiekt"
  ]
  node [
    id 225
    label "zapunktowa&#263;"
  ]
  node [
    id 226
    label "podpunkt"
  ]
  node [
    id 227
    label "wojsko"
  ]
  node [
    id 228
    label "kres"
  ]
  node [
    id 229
    label "przestrze&#324;"
  ]
  node [
    id 230
    label "point"
  ]
  node [
    id 231
    label "pozycja"
  ]
  node [
    id 232
    label "obrona"
  ]
  node [
    id 233
    label "gra"
  ]
  node [
    id 234
    label "game"
  ]
  node [
    id 235
    label "serw"
  ]
  node [
    id 236
    label "dwumecz"
  ]
  node [
    id 237
    label "kartka"
  ]
  node [
    id 238
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 239
    label "logowanie"
  ]
  node [
    id 240
    label "plik"
  ]
  node [
    id 241
    label "s&#261;d"
  ]
  node [
    id 242
    label "adres_internetowy"
  ]
  node [
    id 243
    label "linia"
  ]
  node [
    id 244
    label "serwis_internetowy"
  ]
  node [
    id 245
    label "posta&#263;"
  ]
  node [
    id 246
    label "bok"
  ]
  node [
    id 247
    label "skr&#281;canie"
  ]
  node [
    id 248
    label "skr&#281;ca&#263;"
  ]
  node [
    id 249
    label "orientowanie"
  ]
  node [
    id 250
    label "skr&#281;ci&#263;"
  ]
  node [
    id 251
    label "zorientowanie"
  ]
  node [
    id 252
    label "ty&#322;"
  ]
  node [
    id 253
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 254
    label "fragment"
  ]
  node [
    id 255
    label "layout"
  ]
  node [
    id 256
    label "zorientowa&#263;"
  ]
  node [
    id 257
    label "pagina"
  ]
  node [
    id 258
    label "podmiot"
  ]
  node [
    id 259
    label "g&#243;ra"
  ]
  node [
    id 260
    label "orientowa&#263;"
  ]
  node [
    id 261
    label "voice"
  ]
  node [
    id 262
    label "orientacja"
  ]
  node [
    id 263
    label "prz&#243;d"
  ]
  node [
    id 264
    label "internet"
  ]
  node [
    id 265
    label "powierzchnia"
  ]
  node [
    id 266
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 267
    label "forma"
  ]
  node [
    id 268
    label "skr&#281;cenie"
  ]
  node [
    id 269
    label "do&#322;&#261;czenie"
  ]
  node [
    id 270
    label "message"
  ]
  node [
    id 271
    label "naznoszenie"
  ]
  node [
    id 272
    label "zawiadomienie"
  ]
  node [
    id 273
    label "zniesienie"
  ]
  node [
    id 274
    label "zaniesienie"
  ]
  node [
    id 275
    label "announcement"
  ]
  node [
    id 276
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 277
    label "fetch"
  ]
  node [
    id 278
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 279
    label "poinformowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
]
