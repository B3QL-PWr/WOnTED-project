graph [
  node [
    id 0
    label "nagranie"
    origin "text"
  ]
  node [
    id 1
    label "telefon"
    origin "text"
  ]
  node [
    id 2
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kierowca"
    origin "text"
  ]
  node [
    id 4
    label "bmw"
    origin "text"
  ]
  node [
    id 5
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 6
    label "brak"
    origin "text"
  ]
  node [
    id 7
    label "m&#243;zg"
    origin "text"
  ]
  node [
    id 8
    label "wytw&#243;r"
  ]
  node [
    id 9
    label "wys&#322;uchanie"
  ]
  node [
    id 10
    label "utrwalenie"
  ]
  node [
    id 11
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 12
    label "recording"
  ]
  node [
    id 13
    label "ustalenie"
  ]
  node [
    id 14
    label "trwalszy"
  ]
  node [
    id 15
    label "confirmation"
  ]
  node [
    id 16
    label "zachowanie"
  ]
  node [
    id 17
    label "przedmiot"
  ]
  node [
    id 18
    label "p&#322;&#243;d"
  ]
  node [
    id 19
    label "work"
  ]
  node [
    id 20
    label "rezultat"
  ]
  node [
    id 21
    label "pos&#322;uchanie"
  ]
  node [
    id 22
    label "spe&#322;nienie"
  ]
  node [
    id 23
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 24
    label "hearing"
  ]
  node [
    id 25
    label "muzyka"
  ]
  node [
    id 26
    label "spe&#322;ni&#263;"
  ]
  node [
    id 27
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 28
    label "zadzwoni&#263;"
  ]
  node [
    id 29
    label "provider"
  ]
  node [
    id 30
    label "infrastruktura"
  ]
  node [
    id 31
    label "numer"
  ]
  node [
    id 32
    label "po&#322;&#261;czenie"
  ]
  node [
    id 33
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 34
    label "phreaker"
  ]
  node [
    id 35
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 36
    label "mikrotelefon"
  ]
  node [
    id 37
    label "billing"
  ]
  node [
    id 38
    label "dzwoni&#263;"
  ]
  node [
    id 39
    label "instalacja"
  ]
  node [
    id 40
    label "kontakt"
  ]
  node [
    id 41
    label "coalescence"
  ]
  node [
    id 42
    label "wy&#347;wietlacz"
  ]
  node [
    id 43
    label "dzwonienie"
  ]
  node [
    id 44
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 45
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 46
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 47
    label "urz&#261;dzenie"
  ]
  node [
    id 48
    label "punkt"
  ]
  node [
    id 49
    label "turn"
  ]
  node [
    id 50
    label "liczba"
  ]
  node [
    id 51
    label "&#380;art"
  ]
  node [
    id 52
    label "zi&#243;&#322;ko"
  ]
  node [
    id 53
    label "publikacja"
  ]
  node [
    id 54
    label "manewr"
  ]
  node [
    id 55
    label "impression"
  ]
  node [
    id 56
    label "wyst&#281;p"
  ]
  node [
    id 57
    label "sztos"
  ]
  node [
    id 58
    label "oznaczenie"
  ]
  node [
    id 59
    label "hotel"
  ]
  node [
    id 60
    label "pok&#243;j"
  ]
  node [
    id 61
    label "czasopismo"
  ]
  node [
    id 62
    label "akt_p&#322;ciowy"
  ]
  node [
    id 63
    label "orygina&#322;"
  ]
  node [
    id 64
    label "facet"
  ]
  node [
    id 65
    label "kom&#243;rka"
  ]
  node [
    id 66
    label "furnishing"
  ]
  node [
    id 67
    label "zabezpieczenie"
  ]
  node [
    id 68
    label "zrobienie"
  ]
  node [
    id 69
    label "wyrz&#261;dzenie"
  ]
  node [
    id 70
    label "zagospodarowanie"
  ]
  node [
    id 71
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 72
    label "ig&#322;a"
  ]
  node [
    id 73
    label "narz&#281;dzie"
  ]
  node [
    id 74
    label "wirnik"
  ]
  node [
    id 75
    label "aparatura"
  ]
  node [
    id 76
    label "system_energetyczny"
  ]
  node [
    id 77
    label "impulsator"
  ]
  node [
    id 78
    label "mechanizm"
  ]
  node [
    id 79
    label "sprz&#281;t"
  ]
  node [
    id 80
    label "czynno&#347;&#263;"
  ]
  node [
    id 81
    label "blokowanie"
  ]
  node [
    id 82
    label "set"
  ]
  node [
    id 83
    label "zablokowanie"
  ]
  node [
    id 84
    label "przygotowanie"
  ]
  node [
    id 85
    label "komora"
  ]
  node [
    id 86
    label "j&#281;zyk"
  ]
  node [
    id 87
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 88
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 89
    label "communication"
  ]
  node [
    id 90
    label "styk"
  ]
  node [
    id 91
    label "wydarzenie"
  ]
  node [
    id 92
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 93
    label "association"
  ]
  node [
    id 94
    label "&#322;&#261;cznik"
  ]
  node [
    id 95
    label "katalizator"
  ]
  node [
    id 96
    label "socket"
  ]
  node [
    id 97
    label "instalacja_elektryczna"
  ]
  node [
    id 98
    label "soczewka"
  ]
  node [
    id 99
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 100
    label "formacja_geologiczna"
  ]
  node [
    id 101
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 102
    label "linkage"
  ]
  node [
    id 103
    label "regulator"
  ]
  node [
    id 104
    label "z&#322;&#261;czenie"
  ]
  node [
    id 105
    label "zwi&#261;zek"
  ]
  node [
    id 106
    label "contact"
  ]
  node [
    id 107
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 108
    label "proces"
  ]
  node [
    id 109
    label "kompozycja"
  ]
  node [
    id 110
    label "uzbrajanie"
  ]
  node [
    id 111
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 112
    label "ekran"
  ]
  node [
    id 113
    label "handset"
  ]
  node [
    id 114
    label "dodzwanianie_si&#281;"
  ]
  node [
    id 115
    label "jingle"
  ]
  node [
    id 116
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 117
    label "wydzwanianie"
  ]
  node [
    id 118
    label "dzwonek"
  ]
  node [
    id 119
    label "naciskanie"
  ]
  node [
    id 120
    label "sound"
  ]
  node [
    id 121
    label "brzmienie"
  ]
  node [
    id 122
    label "wybijanie"
  ]
  node [
    id 123
    label "dryndanie"
  ]
  node [
    id 124
    label "dodzwonienie_si&#281;"
  ]
  node [
    id 125
    label "wydzwonienie"
  ]
  node [
    id 126
    label "call"
  ]
  node [
    id 127
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 128
    label "zabrz&#281;cze&#263;"
  ]
  node [
    id 129
    label "zabi&#263;"
  ]
  node [
    id 130
    label "zadrynda&#263;"
  ]
  node [
    id 131
    label "zabrzmie&#263;"
  ]
  node [
    id 132
    label "nacisn&#261;&#263;"
  ]
  node [
    id 133
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 134
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 135
    label "bi&#263;"
  ]
  node [
    id 136
    label "brzmie&#263;"
  ]
  node [
    id 137
    label "drynda&#263;"
  ]
  node [
    id 138
    label "brz&#281;cze&#263;"
  ]
  node [
    id 139
    label "zjednoczy&#263;"
  ]
  node [
    id 140
    label "stworzy&#263;"
  ]
  node [
    id 141
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 142
    label "incorporate"
  ]
  node [
    id 143
    label "zrobi&#263;"
  ]
  node [
    id 144
    label "connect"
  ]
  node [
    id 145
    label "spowodowa&#263;"
  ]
  node [
    id 146
    label "relate"
  ]
  node [
    id 147
    label "paj&#281;czarz"
  ]
  node [
    id 148
    label "z&#322;odziej"
  ]
  node [
    id 149
    label "severance"
  ]
  node [
    id 150
    label "przerwanie"
  ]
  node [
    id 151
    label "od&#322;&#261;czenie"
  ]
  node [
    id 152
    label "oddzielenie"
  ]
  node [
    id 153
    label "prze&#322;&#261;czenie"
  ]
  node [
    id 154
    label "rozdzielenie"
  ]
  node [
    id 155
    label "dissociation"
  ]
  node [
    id 156
    label "spis"
  ]
  node [
    id 157
    label "biling"
  ]
  node [
    id 158
    label "rozdzieli&#263;"
  ]
  node [
    id 159
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 160
    label "detach"
  ]
  node [
    id 161
    label "oddzieli&#263;"
  ]
  node [
    id 162
    label "abstract"
  ]
  node [
    id 163
    label "amputate"
  ]
  node [
    id 164
    label "przerwa&#263;"
  ]
  node [
    id 165
    label "rozdzielanie"
  ]
  node [
    id 166
    label "separation"
  ]
  node [
    id 167
    label "oddzielanie"
  ]
  node [
    id 168
    label "rozsuwanie"
  ]
  node [
    id 169
    label "od&#322;&#261;czanie"
  ]
  node [
    id 170
    label "przerywanie"
  ]
  node [
    id 171
    label "prze&#322;&#261;czanie"
  ]
  node [
    id 172
    label "stworzenie"
  ]
  node [
    id 173
    label "zespolenie"
  ]
  node [
    id 174
    label "dressing"
  ]
  node [
    id 175
    label "pomy&#347;lenie"
  ]
  node [
    id 176
    label "zjednoczenie"
  ]
  node [
    id 177
    label "spowodowanie"
  ]
  node [
    id 178
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 179
    label "element"
  ]
  node [
    id 180
    label "alliance"
  ]
  node [
    id 181
    label "joining"
  ]
  node [
    id 182
    label "umo&#380;liwienie"
  ]
  node [
    id 183
    label "mention"
  ]
  node [
    id 184
    label "zwi&#261;zany"
  ]
  node [
    id 185
    label "port"
  ]
  node [
    id 186
    label "komunikacja"
  ]
  node [
    id 187
    label "rzucenie"
  ]
  node [
    id 188
    label "zgrzeina"
  ]
  node [
    id 189
    label "zestawienie"
  ]
  node [
    id 190
    label "cover"
  ]
  node [
    id 191
    label "gulf"
  ]
  node [
    id 192
    label "part"
  ]
  node [
    id 193
    label "rozdziela&#263;"
  ]
  node [
    id 194
    label "przerywa&#263;"
  ]
  node [
    id 195
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 196
    label "oddziela&#263;"
  ]
  node [
    id 197
    label "internet"
  ]
  node [
    id 198
    label "dostawca"
  ]
  node [
    id 199
    label "telefonia"
  ]
  node [
    id 200
    label "zaplecze"
  ]
  node [
    id 201
    label "radiofonia"
  ]
  node [
    id 202
    label "trasa"
  ]
  node [
    id 203
    label "transportowiec"
  ]
  node [
    id 204
    label "cz&#322;owiek"
  ]
  node [
    id 205
    label "ludzko&#347;&#263;"
  ]
  node [
    id 206
    label "asymilowanie"
  ]
  node [
    id 207
    label "wapniak"
  ]
  node [
    id 208
    label "asymilowa&#263;"
  ]
  node [
    id 209
    label "os&#322;abia&#263;"
  ]
  node [
    id 210
    label "posta&#263;"
  ]
  node [
    id 211
    label "hominid"
  ]
  node [
    id 212
    label "podw&#322;adny"
  ]
  node [
    id 213
    label "os&#322;abianie"
  ]
  node [
    id 214
    label "g&#322;owa"
  ]
  node [
    id 215
    label "figura"
  ]
  node [
    id 216
    label "portrecista"
  ]
  node [
    id 217
    label "dwun&#243;g"
  ]
  node [
    id 218
    label "profanum"
  ]
  node [
    id 219
    label "mikrokosmos"
  ]
  node [
    id 220
    label "nasada"
  ]
  node [
    id 221
    label "duch"
  ]
  node [
    id 222
    label "antropochoria"
  ]
  node [
    id 223
    label "osoba"
  ]
  node [
    id 224
    label "wz&#243;r"
  ]
  node [
    id 225
    label "senior"
  ]
  node [
    id 226
    label "oddzia&#322;ywanie"
  ]
  node [
    id 227
    label "Adam"
  ]
  node [
    id 228
    label "homo_sapiens"
  ]
  node [
    id 229
    label "polifag"
  ]
  node [
    id 230
    label "pracownik"
  ]
  node [
    id 231
    label "statek_handlowy"
  ]
  node [
    id 232
    label "okr&#281;t_nawodny"
  ]
  node [
    id 233
    label "bran&#380;owiec"
  ]
  node [
    id 234
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 235
    label "samoch&#243;d"
  ]
  node [
    id 236
    label "BMW"
  ]
  node [
    id 237
    label "pojazd_drogowy"
  ]
  node [
    id 238
    label "spryskiwacz"
  ]
  node [
    id 239
    label "most"
  ]
  node [
    id 240
    label "baga&#380;nik"
  ]
  node [
    id 241
    label "silnik"
  ]
  node [
    id 242
    label "dachowanie"
  ]
  node [
    id 243
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 244
    label "pompa_wodna"
  ]
  node [
    id 245
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 246
    label "poduszka_powietrzna"
  ]
  node [
    id 247
    label "tempomat"
  ]
  node [
    id 248
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 249
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 250
    label "deska_rozdzielcza"
  ]
  node [
    id 251
    label "immobilizer"
  ]
  node [
    id 252
    label "t&#322;umik"
  ]
  node [
    id 253
    label "kierownica"
  ]
  node [
    id 254
    label "ABS"
  ]
  node [
    id 255
    label "bak"
  ]
  node [
    id 256
    label "dwu&#347;lad"
  ]
  node [
    id 257
    label "poci&#261;g_drogowy"
  ]
  node [
    id 258
    label "wycieraczka"
  ]
  node [
    id 259
    label "nieistnienie"
  ]
  node [
    id 260
    label "odej&#347;cie"
  ]
  node [
    id 261
    label "defect"
  ]
  node [
    id 262
    label "gap"
  ]
  node [
    id 263
    label "odej&#347;&#263;"
  ]
  node [
    id 264
    label "kr&#243;tki"
  ]
  node [
    id 265
    label "wada"
  ]
  node [
    id 266
    label "odchodzi&#263;"
  ]
  node [
    id 267
    label "wyr&#243;b"
  ]
  node [
    id 268
    label "odchodzenie"
  ]
  node [
    id 269
    label "prywatywny"
  ]
  node [
    id 270
    label "stan"
  ]
  node [
    id 271
    label "niebyt"
  ]
  node [
    id 272
    label "nonexistence"
  ]
  node [
    id 273
    label "cecha"
  ]
  node [
    id 274
    label "faintness"
  ]
  node [
    id 275
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 276
    label "schorzenie"
  ]
  node [
    id 277
    label "strona"
  ]
  node [
    id 278
    label "imperfection"
  ]
  node [
    id 279
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 280
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 281
    label "produkt"
  ]
  node [
    id 282
    label "creation"
  ]
  node [
    id 283
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 284
    label "p&#322;uczkarnia"
  ]
  node [
    id 285
    label "znakowarka"
  ]
  node [
    id 286
    label "produkcja"
  ]
  node [
    id 287
    label "szybki"
  ]
  node [
    id 288
    label "jednowyrazowy"
  ]
  node [
    id 289
    label "bliski"
  ]
  node [
    id 290
    label "s&#322;aby"
  ]
  node [
    id 291
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 292
    label "kr&#243;tko"
  ]
  node [
    id 293
    label "drobny"
  ]
  node [
    id 294
    label "ruch"
  ]
  node [
    id 295
    label "z&#322;y"
  ]
  node [
    id 296
    label "odrzut"
  ]
  node [
    id 297
    label "drop"
  ]
  node [
    id 298
    label "proceed"
  ]
  node [
    id 299
    label "zrezygnowa&#263;"
  ]
  node [
    id 300
    label "ruszy&#263;"
  ]
  node [
    id 301
    label "min&#261;&#263;"
  ]
  node [
    id 302
    label "leave_office"
  ]
  node [
    id 303
    label "die"
  ]
  node [
    id 304
    label "retract"
  ]
  node [
    id 305
    label "opu&#347;ci&#263;"
  ]
  node [
    id 306
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 307
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 308
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 309
    label "przesta&#263;"
  ]
  node [
    id 310
    label "korkowanie"
  ]
  node [
    id 311
    label "death"
  ]
  node [
    id 312
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 313
    label "przestawanie"
  ]
  node [
    id 314
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 315
    label "zb&#281;dny"
  ]
  node [
    id 316
    label "zdychanie"
  ]
  node [
    id 317
    label "spisywanie_"
  ]
  node [
    id 318
    label "usuwanie"
  ]
  node [
    id 319
    label "tracenie"
  ]
  node [
    id 320
    label "ko&#324;czenie"
  ]
  node [
    id 321
    label "zwalnianie_si&#281;"
  ]
  node [
    id 322
    label "&#380;ycie"
  ]
  node [
    id 323
    label "robienie"
  ]
  node [
    id 324
    label "opuszczanie"
  ]
  node [
    id 325
    label "wydalanie"
  ]
  node [
    id 326
    label "odrzucanie"
  ]
  node [
    id 327
    label "odstawianie"
  ]
  node [
    id 328
    label "martwy"
  ]
  node [
    id 329
    label "ust&#281;powanie"
  ]
  node [
    id 330
    label "egress"
  ]
  node [
    id 331
    label "zrzekanie_si&#281;"
  ]
  node [
    id 332
    label "dzianie_si&#281;"
  ]
  node [
    id 333
    label "oddzielanie_si&#281;"
  ]
  node [
    id 334
    label "bycie"
  ]
  node [
    id 335
    label "wyruszanie"
  ]
  node [
    id 336
    label "odumieranie"
  ]
  node [
    id 337
    label "odstawanie"
  ]
  node [
    id 338
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 339
    label "mijanie"
  ]
  node [
    id 340
    label "wracanie"
  ]
  node [
    id 341
    label "oddalanie_si&#281;"
  ]
  node [
    id 342
    label "kursowanie"
  ]
  node [
    id 343
    label "blend"
  ]
  node [
    id 344
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 345
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 346
    label "opuszcza&#263;"
  ]
  node [
    id 347
    label "impart"
  ]
  node [
    id 348
    label "wyrusza&#263;"
  ]
  node [
    id 349
    label "go"
  ]
  node [
    id 350
    label "seclude"
  ]
  node [
    id 351
    label "gasn&#261;&#263;"
  ]
  node [
    id 352
    label "przestawa&#263;"
  ]
  node [
    id 353
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 354
    label "odstawa&#263;"
  ]
  node [
    id 355
    label "rezygnowa&#263;"
  ]
  node [
    id 356
    label "i&#347;&#263;"
  ]
  node [
    id 357
    label "mija&#263;"
  ]
  node [
    id 358
    label "mini&#281;cie"
  ]
  node [
    id 359
    label "odumarcie"
  ]
  node [
    id 360
    label "dysponowanie_si&#281;"
  ]
  node [
    id 361
    label "ruszenie"
  ]
  node [
    id 362
    label "ust&#261;pienie"
  ]
  node [
    id 363
    label "mogi&#322;a"
  ]
  node [
    id 364
    label "pomarcie"
  ]
  node [
    id 365
    label "opuszczenie"
  ]
  node [
    id 366
    label "spisanie_"
  ]
  node [
    id 367
    label "oddalenie_si&#281;"
  ]
  node [
    id 368
    label "defenestracja"
  ]
  node [
    id 369
    label "danie_sobie_spokoju"
  ]
  node [
    id 370
    label "kres_&#380;ycia"
  ]
  node [
    id 371
    label "zwolnienie_si&#281;"
  ]
  node [
    id 372
    label "zdechni&#281;cie"
  ]
  node [
    id 373
    label "exit"
  ]
  node [
    id 374
    label "stracenie"
  ]
  node [
    id 375
    label "przestanie"
  ]
  node [
    id 376
    label "wr&#243;cenie"
  ]
  node [
    id 377
    label "szeol"
  ]
  node [
    id 378
    label "oddzielenie_si&#281;"
  ]
  node [
    id 379
    label "deviation"
  ]
  node [
    id 380
    label "wydalenie"
  ]
  node [
    id 381
    label "&#380;a&#322;oba"
  ]
  node [
    id 382
    label "pogrzebanie"
  ]
  node [
    id 383
    label "sko&#324;czenie"
  ]
  node [
    id 384
    label "withdrawal"
  ]
  node [
    id 385
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 386
    label "zabicie"
  ]
  node [
    id 387
    label "agonia"
  ]
  node [
    id 388
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 389
    label "kres"
  ]
  node [
    id 390
    label "usuni&#281;cie"
  ]
  node [
    id 391
    label "relinquishment"
  ]
  node [
    id 392
    label "p&#243;j&#347;cie"
  ]
  node [
    id 393
    label "poniechanie"
  ]
  node [
    id 394
    label "zako&#324;czenie"
  ]
  node [
    id 395
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 396
    label "wypisanie_si&#281;"
  ]
  node [
    id 397
    label "ciekawski"
  ]
  node [
    id 398
    label "statysta"
  ]
  node [
    id 399
    label "substancja_szara"
  ]
  node [
    id 400
    label "wiedza"
  ]
  node [
    id 401
    label "encefalografia"
  ]
  node [
    id 402
    label "przedmurze"
  ]
  node [
    id 403
    label "bruzda"
  ]
  node [
    id 404
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 405
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 406
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 407
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 408
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 409
    label "podwzg&#243;rze"
  ]
  node [
    id 410
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 411
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 412
    label "wzg&#243;rze"
  ]
  node [
    id 413
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 414
    label "noosfera"
  ]
  node [
    id 415
    label "elektroencefalogram"
  ]
  node [
    id 416
    label "przodom&#243;zgowie"
  ]
  node [
    id 417
    label "organ"
  ]
  node [
    id 418
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 419
    label "projektodawca"
  ]
  node [
    id 420
    label "przysadka"
  ]
  node [
    id 421
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 422
    label "zw&#243;j"
  ]
  node [
    id 423
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 424
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 425
    label "kora_m&#243;zgowa"
  ]
  node [
    id 426
    label "umys&#322;"
  ]
  node [
    id 427
    label "kresom&#243;zgowie"
  ]
  node [
    id 428
    label "poduszka"
  ]
  node [
    id 429
    label "tkanka"
  ]
  node [
    id 430
    label "jednostka_organizacyjna"
  ]
  node [
    id 431
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 432
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 433
    label "tw&#243;r"
  ]
  node [
    id 434
    label "organogeneza"
  ]
  node [
    id 435
    label "zesp&#243;&#322;"
  ]
  node [
    id 436
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 437
    label "struktura_anatomiczna"
  ]
  node [
    id 438
    label "uk&#322;ad"
  ]
  node [
    id 439
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 440
    label "dekortykacja"
  ]
  node [
    id 441
    label "Izba_Konsyliarska"
  ]
  node [
    id 442
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 443
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 444
    label "stomia"
  ]
  node [
    id 445
    label "budowa"
  ]
  node [
    id 446
    label "okolica"
  ]
  node [
    id 447
    label "Komitet_Region&#243;w"
  ]
  node [
    id 448
    label "inicjator"
  ]
  node [
    id 449
    label "pryncypa&#322;"
  ]
  node [
    id 450
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 451
    label "kszta&#322;t"
  ]
  node [
    id 452
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 453
    label "kierowa&#263;"
  ]
  node [
    id 454
    label "alkohol"
  ]
  node [
    id 455
    label "zdolno&#347;&#263;"
  ]
  node [
    id 456
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 457
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 458
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 459
    label "sztuka"
  ]
  node [
    id 460
    label "dekiel"
  ]
  node [
    id 461
    label "ro&#347;lina"
  ]
  node [
    id 462
    label "&#347;ci&#281;cie"
  ]
  node [
    id 463
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 464
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 465
    label "&#347;ci&#281;gno"
  ]
  node [
    id 466
    label "byd&#322;o"
  ]
  node [
    id 467
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 468
    label "makrocefalia"
  ]
  node [
    id 469
    label "obiekt"
  ]
  node [
    id 470
    label "ucho"
  ]
  node [
    id 471
    label "g&#243;ra"
  ]
  node [
    id 472
    label "kierownictwo"
  ]
  node [
    id 473
    label "fryzura"
  ]
  node [
    id 474
    label "cia&#322;o"
  ]
  node [
    id 475
    label "cz&#322;onek"
  ]
  node [
    id 476
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 477
    label "czaszka"
  ]
  node [
    id 478
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 479
    label "charakterystyka"
  ]
  node [
    id 480
    label "m&#322;ot"
  ]
  node [
    id 481
    label "znak"
  ]
  node [
    id 482
    label "drzewo"
  ]
  node [
    id 483
    label "pr&#243;ba"
  ]
  node [
    id 484
    label "attribute"
  ]
  node [
    id 485
    label "marka"
  ]
  node [
    id 486
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 487
    label "zmarszczka"
  ]
  node [
    id 488
    label "line"
  ]
  node [
    id 489
    label "rowkowa&#263;"
  ]
  node [
    id 490
    label "fa&#322;da"
  ]
  node [
    id 491
    label "szczelina"
  ]
  node [
    id 492
    label "teren"
  ]
  node [
    id 493
    label "ostoja"
  ]
  node [
    id 494
    label "mur"
  ]
  node [
    id 495
    label "warstwa"
  ]
  node [
    id 496
    label "cia&#322;o_modzelowate"
  ]
  node [
    id 497
    label "wyspa"
  ]
  node [
    id 498
    label "j&#261;dro_podstawne"
  ]
  node [
    id 499
    label "bruzda_przed&#347;rodkowa"
  ]
  node [
    id 500
    label "p&#243;&#322;kula_m&#243;zgu"
  ]
  node [
    id 501
    label "p&#322;at_skroniowy"
  ]
  node [
    id 502
    label "w&#281;chom&#243;zgowie"
  ]
  node [
    id 503
    label "cerebrum"
  ]
  node [
    id 504
    label "p&#322;at_czo&#322;owy"
  ]
  node [
    id 505
    label "p&#322;at_ciemieniowy"
  ]
  node [
    id 506
    label "p&#322;at_potyliczny"
  ]
  node [
    id 507
    label "ga&#322;ka_blada"
  ]
  node [
    id 508
    label "kresom&#243;zgowie_&#347;rodkowe"
  ]
  node [
    id 509
    label "piecz&#261;tka"
  ]
  node [
    id 510
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 511
    label "po&#347;ciel"
  ]
  node [
    id 512
    label "podpora"
  ]
  node [
    id 513
    label "d&#322;o&#324;"
  ]
  node [
    id 514
    label "wyko&#324;czenie"
  ]
  node [
    id 515
    label "wype&#322;niacz"
  ]
  node [
    id 516
    label "fotel"
  ]
  node [
    id 517
    label "palec"
  ]
  node [
    id 518
    label "&#322;apa"
  ]
  node [
    id 519
    label "kanapa"
  ]
  node [
    id 520
    label "hindbrain"
  ]
  node [
    id 521
    label "zam&#243;zgowie"
  ]
  node [
    id 522
    label "hipoplazja_cia&#322;a_modzelowatego"
  ]
  node [
    id 523
    label "agenezja_cia&#322;a_modzelowatego"
  ]
  node [
    id 524
    label "forebrain"
  ]
  node [
    id 525
    label "holoprozencefalia"
  ]
  node [
    id 526
    label "li&#347;&#263;"
  ]
  node [
    id 527
    label "melanotropina"
  ]
  node [
    id 528
    label "gruczo&#322;_dokrewny"
  ]
  node [
    id 529
    label "podroby"
  ]
  node [
    id 530
    label "robak"
  ]
  node [
    id 531
    label "intelekt"
  ]
  node [
    id 532
    label "cerebellum"
  ]
  node [
    id 533
    label "j&#261;dro_z&#281;bate"
  ]
  node [
    id 534
    label "kom&#243;rka_Purkyniego"
  ]
  node [
    id 535
    label "Eskwilin"
  ]
  node [
    id 536
    label "Palatyn"
  ]
  node [
    id 537
    label "Kapitol"
  ]
  node [
    id 538
    label "Syjon"
  ]
  node [
    id 539
    label "wzniesienie"
  ]
  node [
    id 540
    label "Kwiryna&#322;"
  ]
  node [
    id 541
    label "Awentyn"
  ]
  node [
    id 542
    label "Wawel"
  ]
  node [
    id 543
    label "rzuci&#263;"
  ]
  node [
    id 544
    label "prz&#281;s&#322;o"
  ]
  node [
    id 545
    label "jarzmo_mostowe"
  ]
  node [
    id 546
    label "pylon"
  ]
  node [
    id 547
    label "obiekt_mostowy"
  ]
  node [
    id 548
    label "szczelina_dylatacyjna"
  ]
  node [
    id 549
    label "bridge"
  ]
  node [
    id 550
    label "rzuca&#263;"
  ]
  node [
    id 551
    label "suwnica"
  ]
  node [
    id 552
    label "porozumienie"
  ]
  node [
    id 553
    label "nap&#281;d"
  ]
  node [
    id 554
    label "rzucanie"
  ]
  node [
    id 555
    label "zawzg&#243;rze"
  ]
  node [
    id 556
    label "niskowzg&#243;rze"
  ]
  node [
    id 557
    label "diencephalon"
  ]
  node [
    id 558
    label "lejek"
  ]
  node [
    id 559
    label "cia&#322;o_suteczkowate"
  ]
  node [
    id 560
    label "egzemplarz"
  ]
  node [
    id 561
    label "wrench"
  ]
  node [
    id 562
    label "kink"
  ]
  node [
    id 563
    label "plik"
  ]
  node [
    id 564
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 565
    label "manuskrypt"
  ]
  node [
    id 566
    label "rolka"
  ]
  node [
    id 567
    label "pokrywa"
  ]
  node [
    id 568
    label "istota_czarna"
  ]
  node [
    id 569
    label "pami&#281;&#263;"
  ]
  node [
    id 570
    label "pomieszanie_si&#281;"
  ]
  node [
    id 571
    label "wn&#281;trze"
  ]
  node [
    id 572
    label "wyobra&#378;nia"
  ]
  node [
    id 573
    label "badanie"
  ]
  node [
    id 574
    label "encephalography"
  ]
  node [
    id 575
    label "electroencephalogram"
  ]
  node [
    id 576
    label "wynik_badania"
  ]
  node [
    id 577
    label "elektroencefalografia"
  ]
  node [
    id 578
    label "wada_wrodzona"
  ]
  node [
    id 579
    label "cognition"
  ]
  node [
    id 580
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 581
    label "pozwolenie"
  ]
  node [
    id 582
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 583
    label "zaawansowanie"
  ]
  node [
    id 584
    label "wykszta&#322;cenie"
  ]
  node [
    id 585
    label "ca&#322;o&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
]
