graph [
  node [
    id 0
    label "janusz"
    origin "text"
  ]
  node [
    id 1
    label "niemcewicz"
    origin "text"
  ]
  node [
    id 2
    label "sprawozdawca"
    origin "text"
  ]
  node [
    id 3
    label "protokolant"
    origin "text"
  ]
  node [
    id 4
    label "krzysztof"
    origin "text"
  ]
  node [
    id 5
    label "zalecki"
    origin "text"
  ]
  node [
    id 6
    label "tatusiek"
  ]
  node [
    id 7
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 8
    label "cwaniaczek"
  ]
  node [
    id 9
    label "pata&#322;ach"
  ]
  node [
    id 10
    label "gra&#380;yna"
  ]
  node [
    id 11
    label "kibic"
  ]
  node [
    id 12
    label "przeci&#281;tniak"
  ]
  node [
    id 13
    label "facet"
  ]
  node [
    id 14
    label "Polak"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "Mro&#380;ek"
  ]
  node [
    id 17
    label "Ko&#347;ciuszko"
  ]
  node [
    id 18
    label "Saba&#322;a"
  ]
  node [
    id 19
    label "Europejczyk"
  ]
  node [
    id 20
    label "Lach"
  ]
  node [
    id 21
    label "Anders"
  ]
  node [
    id 22
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 23
    label "mieszkaniec"
  ]
  node [
    id 24
    label "Jakub_Wujek"
  ]
  node [
    id 25
    label "Polaczek"
  ]
  node [
    id 26
    label "Kie&#347;lowski"
  ]
  node [
    id 27
    label "Daniel_Dubicki"
  ]
  node [
    id 28
    label "&#346;ledzi&#324;ski"
  ]
  node [
    id 29
    label "Pola&#324;ski"
  ]
  node [
    id 30
    label "Towia&#324;ski"
  ]
  node [
    id 31
    label "Zanussi"
  ]
  node [
    id 32
    label "Wajda"
  ]
  node [
    id 33
    label "Pi&#322;sudski"
  ]
  node [
    id 34
    label "S&#322;owianin"
  ]
  node [
    id 35
    label "Owsiak"
  ]
  node [
    id 36
    label "Asnyk"
  ]
  node [
    id 37
    label "Daniel_Olbrychski"
  ]
  node [
    id 38
    label "Conrad"
  ]
  node [
    id 39
    label "Ma&#322;ysz"
  ]
  node [
    id 40
    label "Wojciech_Mann"
  ]
  node [
    id 41
    label "doros&#322;y"
  ]
  node [
    id 42
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 43
    label "ojciec"
  ]
  node [
    id 44
    label "jegomo&#347;&#263;"
  ]
  node [
    id 45
    label "andropauza"
  ]
  node [
    id 46
    label "pa&#324;stwo"
  ]
  node [
    id 47
    label "bratek"
  ]
  node [
    id 48
    label "samiec"
  ]
  node [
    id 49
    label "ch&#322;opina"
  ]
  node [
    id 50
    label "twardziel"
  ]
  node [
    id 51
    label "androlog"
  ]
  node [
    id 52
    label "m&#261;&#380;"
  ]
  node [
    id 53
    label "partacz"
  ]
  node [
    id 54
    label "nieudacznik"
  ]
  node [
    id 55
    label "zach&#281;ta"
  ]
  node [
    id 56
    label "fan"
  ]
  node [
    id 57
    label "widz"
  ]
  node [
    id 58
    label "&#380;yleta"
  ]
  node [
    id 59
    label "Polka"
  ]
  node [
    id 60
    label "kura_domowa"
  ]
  node [
    id 61
    label "&#347;wiadek"
  ]
  node [
    id 62
    label "przekaziciel"
  ]
  node [
    id 63
    label "dziennikarz"
  ]
  node [
    id 64
    label "s&#261;d"
  ]
  node [
    id 65
    label "uczestnik"
  ]
  node [
    id 66
    label "dru&#380;ba"
  ]
  node [
    id 67
    label "obserwator"
  ]
  node [
    id 68
    label "osoba_fizyczna"
  ]
  node [
    id 69
    label "po&#347;rednik"
  ]
  node [
    id 70
    label "informator"
  ]
  node [
    id 71
    label "wys&#322;annik"
  ]
  node [
    id 72
    label "publicysta"
  ]
  node [
    id 73
    label "nowiniarz"
  ]
  node [
    id 74
    label "bran&#380;owiec"
  ]
  node [
    id 75
    label "akredytowanie"
  ]
  node [
    id 76
    label "akredytowa&#263;"
  ]
  node [
    id 77
    label "urz&#281;dnik"
  ]
  node [
    id 78
    label "sekretarz"
  ]
  node [
    id 79
    label "pracownik"
  ]
  node [
    id 80
    label "korpus_urz&#281;dniczy"
  ]
  node [
    id 81
    label "pragmatyka"
  ]
  node [
    id 82
    label "tytu&#322;"
  ]
  node [
    id 83
    label "zwierzchnik"
  ]
  node [
    id 84
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 85
    label "Gierek"
  ]
  node [
    id 86
    label "ptak_egzotyczny"
  ]
  node [
    id 87
    label "Jan_Czeczot"
  ]
  node [
    id 88
    label "Bre&#380;niew"
  ]
  node [
    id 89
    label "odznaczenie"
  ]
  node [
    id 90
    label "pracownik_biurowy"
  ]
  node [
    id 91
    label "Gomu&#322;ka"
  ]
  node [
    id 92
    label "kancelaria"
  ]
  node [
    id 93
    label "sekretarze"
  ]
  node [
    id 94
    label "asystent"
  ]
  node [
    id 95
    label "administracja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 4
    target 5
  ]
]
