graph [
  node [
    id 0
    label "fajny"
    origin "text"
  ]
  node [
    id 1
    label "obrazek"
    origin "text"
  ]
  node [
    id 2
    label "imgur"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "poprawi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "humor"
    origin "text"
  ]
  node [
    id 6
    label "byczy"
  ]
  node [
    id 7
    label "fajnie"
  ]
  node [
    id 8
    label "klawy"
  ]
  node [
    id 9
    label "dobry"
  ]
  node [
    id 10
    label "dobroczynny"
  ]
  node [
    id 11
    label "czw&#243;rka"
  ]
  node [
    id 12
    label "spokojny"
  ]
  node [
    id 13
    label "skuteczny"
  ]
  node [
    id 14
    label "&#347;mieszny"
  ]
  node [
    id 15
    label "mi&#322;y"
  ]
  node [
    id 16
    label "grzeczny"
  ]
  node [
    id 17
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 18
    label "powitanie"
  ]
  node [
    id 19
    label "dobrze"
  ]
  node [
    id 20
    label "ca&#322;y"
  ]
  node [
    id 21
    label "zwrot"
  ]
  node [
    id 22
    label "pomy&#347;lny"
  ]
  node [
    id 23
    label "moralny"
  ]
  node [
    id 24
    label "drogi"
  ]
  node [
    id 25
    label "pozytywny"
  ]
  node [
    id 26
    label "odpowiedni"
  ]
  node [
    id 27
    label "korzystny"
  ]
  node [
    id 28
    label "pos&#322;uszny"
  ]
  node [
    id 29
    label "na_schwa&#322;"
  ]
  node [
    id 30
    label "klawo"
  ]
  node [
    id 31
    label "byczo"
  ]
  node [
    id 32
    label "podobny"
  ]
  node [
    id 33
    label "du&#380;y"
  ]
  node [
    id 34
    label "druk_ulotny"
  ]
  node [
    id 35
    label "rysunek"
  ]
  node [
    id 36
    label "opowiadanie"
  ]
  node [
    id 37
    label "picture"
  ]
  node [
    id 38
    label "kreska"
  ]
  node [
    id 39
    label "kszta&#322;t"
  ]
  node [
    id 40
    label "teka"
  ]
  node [
    id 41
    label "photograph"
  ]
  node [
    id 42
    label "ilustracja"
  ]
  node [
    id 43
    label "grafika"
  ]
  node [
    id 44
    label "plastyka"
  ]
  node [
    id 45
    label "shape"
  ]
  node [
    id 46
    label "follow-up"
  ]
  node [
    id 47
    label "rozpowiadanie"
  ]
  node [
    id 48
    label "wypowied&#378;"
  ]
  node [
    id 49
    label "report"
  ]
  node [
    id 50
    label "spalenie"
  ]
  node [
    id 51
    label "podbarwianie"
  ]
  node [
    id 52
    label "przedstawianie"
  ]
  node [
    id 53
    label "story"
  ]
  node [
    id 54
    label "rozpowiedzenie"
  ]
  node [
    id 55
    label "proza"
  ]
  node [
    id 56
    label "prawienie"
  ]
  node [
    id 57
    label "utw&#243;r_epicki"
  ]
  node [
    id 58
    label "fabu&#322;a"
  ]
  node [
    id 59
    label "upomnie&#263;"
  ]
  node [
    id 60
    label "correct"
  ]
  node [
    id 61
    label "sprawdzi&#263;"
  ]
  node [
    id 62
    label "amend"
  ]
  node [
    id 63
    label "ulepszy&#263;"
  ]
  node [
    id 64
    label "rectify"
  ]
  node [
    id 65
    label "level"
  ]
  node [
    id 66
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 67
    label "caution"
  ]
  node [
    id 68
    label "zakomunikowa&#263;"
  ]
  node [
    id 69
    label "examine"
  ]
  node [
    id 70
    label "zrobi&#263;"
  ]
  node [
    id 71
    label "modify"
  ]
  node [
    id 72
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 73
    label "zmieni&#263;"
  ]
  node [
    id 74
    label "manipulate"
  ]
  node [
    id 75
    label "spowodowa&#263;"
  ]
  node [
    id 76
    label "faza"
  ]
  node [
    id 77
    label "ranga"
  ]
  node [
    id 78
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 79
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 80
    label "temper"
  ]
  node [
    id 81
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 82
    label "stan"
  ]
  node [
    id 83
    label "samopoczucie"
  ]
  node [
    id 84
    label "mechanizm_obronny"
  ]
  node [
    id 85
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 86
    label "fondness"
  ]
  node [
    id 87
    label "nastr&#243;j"
  ]
  node [
    id 88
    label "state"
  ]
  node [
    id 89
    label "wstyd"
  ]
  node [
    id 90
    label "upokorzenie"
  ]
  node [
    id 91
    label "cecha"
  ]
  node [
    id 92
    label "klimat"
  ]
  node [
    id 93
    label "charakter"
  ]
  node [
    id 94
    label "kwas"
  ]
  node [
    id 95
    label "Ohio"
  ]
  node [
    id 96
    label "wci&#281;cie"
  ]
  node [
    id 97
    label "Nowy_York"
  ]
  node [
    id 98
    label "warstwa"
  ]
  node [
    id 99
    label "Illinois"
  ]
  node [
    id 100
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 101
    label "Jukatan"
  ]
  node [
    id 102
    label "Kalifornia"
  ]
  node [
    id 103
    label "Wirginia"
  ]
  node [
    id 104
    label "wektor"
  ]
  node [
    id 105
    label "by&#263;"
  ]
  node [
    id 106
    label "Teksas"
  ]
  node [
    id 107
    label "Goa"
  ]
  node [
    id 108
    label "Waszyngton"
  ]
  node [
    id 109
    label "miejsce"
  ]
  node [
    id 110
    label "Massachusetts"
  ]
  node [
    id 111
    label "Alaska"
  ]
  node [
    id 112
    label "Arakan"
  ]
  node [
    id 113
    label "Hawaje"
  ]
  node [
    id 114
    label "Maryland"
  ]
  node [
    id 115
    label "punkt"
  ]
  node [
    id 116
    label "Michigan"
  ]
  node [
    id 117
    label "Arizona"
  ]
  node [
    id 118
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 119
    label "Georgia"
  ]
  node [
    id 120
    label "poziom"
  ]
  node [
    id 121
    label "Pensylwania"
  ]
  node [
    id 122
    label "Luizjana"
  ]
  node [
    id 123
    label "Nowy_Meksyk"
  ]
  node [
    id 124
    label "Alabama"
  ]
  node [
    id 125
    label "ilo&#347;&#263;"
  ]
  node [
    id 126
    label "Kansas"
  ]
  node [
    id 127
    label "Oregon"
  ]
  node [
    id 128
    label "Floryda"
  ]
  node [
    id 129
    label "Oklahoma"
  ]
  node [
    id 130
    label "jednostka_administracyjna"
  ]
  node [
    id 131
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 132
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 133
    label "dyspozycja"
  ]
  node [
    id 134
    label "forma"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
]
