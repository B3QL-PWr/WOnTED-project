graph [
  node [
    id 0
    label "odpowied&#378;"
    origin "text"
  ]
  node [
    id 1
    label "pytanie"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 4
    label "m&#281;&#380;yd&#322;o"
    origin "text"
  ]
  node [
    id 5
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 6
    label "cz&#281;&#347;ciowo"
    origin "text"
  ]
  node [
    id 7
    label "tak"
    origin "text"
  ]
  node [
    id 8
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 9
    label "uregulowanie"
    origin "text"
  ]
  node [
    id 10
    label "prokonsumenckie"
    origin "text"
  ]
  node [
    id 11
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "zar&#243;wno"
    origin "text"
  ]
  node [
    id 14
    label "pierwsza"
    origin "text"
  ]
  node [
    id 15
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "europejski"
    origin "text"
  ]
  node [
    id 17
    label "jak"
    origin "text"
  ]
  node [
    id 18
    label "znajda"
    origin "text"
  ]
  node [
    id 19
    label "druga"
    origin "text"
  ]
  node [
    id 20
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 21
    label "przewidywa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "typ"
    origin "text"
  ]
  node [
    id 23
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 24
    label "prawny"
    origin "text"
  ]
  node [
    id 25
    label "nowelizacja"
    origin "text"
  ]
  node [
    id 26
    label "prawa"
    origin "text"
  ]
  node [
    id 27
    label "telekomunikacyjny"
    origin "text"
  ]
  node [
    id 28
    label "jeden"
    origin "text"
  ]
  node [
    id 29
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 30
    label "projekt"
    origin "text"
  ]
  node [
    id 31
    label "poselski"
    origin "text"
  ]
  node [
    id 32
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 33
    label "metr"
    origin "text"
  ]
  node [
    id 34
    label "mediacja"
    origin "text"
  ]
  node [
    id 35
    label "trudno"
    origin "text"
  ]
  node [
    id 36
    label "by&#263;"
    origin "text"
  ]
  node [
    id 37
    label "zgodzi&#263;"
    origin "text"
  ]
  node [
    id 38
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 39
    label "dobrze"
    origin "text"
  ]
  node [
    id 40
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 41
    label "system"
    origin "text"
  ]
  node [
    id 42
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 43
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 44
    label "miejsce"
    origin "text"
  ]
  node [
    id 45
    label "funkcjonowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "sprawnie"
    origin "text"
  ]
  node [
    id 47
    label "wystarczy&#263;"
    origin "text"
  ]
  node [
    id 48
    label "chocia&#380;by"
    origin "text"
  ]
  node [
    id 49
    label "zerkn&#261;&#263;"
    origin "text"
  ]
  node [
    id 50
    label "przepis"
    origin "text"
  ]
  node [
    id 51
    label "kodeks"
    origin "text"
  ]
  node [
    id 52
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 53
    label "administracyjny"
    origin "text"
  ]
  node [
    id 54
    label "te&#380;"
    origin "text"
  ]
  node [
    id 55
    label "cywilny"
    origin "text"
  ]
  node [
    id 56
    label "tworzenie"
    origin "text"
  ]
  node [
    id 57
    label "kolejny"
    origin "text"
  ]
  node [
    id 58
    label "odnosi&#263;"
    origin "text"
  ]
  node [
    id 59
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 60
    label "zb&#281;dny"
    origin "text"
  ]
  node [
    id 61
    label "react"
  ]
  node [
    id 62
    label "replica"
  ]
  node [
    id 63
    label "rozmowa"
  ]
  node [
    id 64
    label "wyj&#347;cie"
  ]
  node [
    id 65
    label "respondent"
  ]
  node [
    id 66
    label "dokument"
  ]
  node [
    id 67
    label "reakcja"
  ]
  node [
    id 68
    label "zachowanie"
  ]
  node [
    id 69
    label "reaction"
  ]
  node [
    id 70
    label "organizm"
  ]
  node [
    id 71
    label "response"
  ]
  node [
    id 72
    label "rezultat"
  ]
  node [
    id 73
    label "zapis"
  ]
  node [
    id 74
    label "&#347;wiadectwo"
  ]
  node [
    id 75
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 76
    label "wytw&#243;r"
  ]
  node [
    id 77
    label "parafa"
  ]
  node [
    id 78
    label "plik"
  ]
  node [
    id 79
    label "raport&#243;wka"
  ]
  node [
    id 80
    label "utw&#243;r"
  ]
  node [
    id 81
    label "record"
  ]
  node [
    id 82
    label "registratura"
  ]
  node [
    id 83
    label "dokumentacja"
  ]
  node [
    id 84
    label "fascyku&#322;"
  ]
  node [
    id 85
    label "artyku&#322;"
  ]
  node [
    id 86
    label "writing"
  ]
  node [
    id 87
    label "sygnatariusz"
  ]
  node [
    id 88
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 89
    label "okazanie_si&#281;"
  ]
  node [
    id 90
    label "ograniczenie"
  ]
  node [
    id 91
    label "uzyskanie"
  ]
  node [
    id 92
    label "ruszenie"
  ]
  node [
    id 93
    label "podzianie_si&#281;"
  ]
  node [
    id 94
    label "spotkanie"
  ]
  node [
    id 95
    label "powychodzenie"
  ]
  node [
    id 96
    label "opuszczenie"
  ]
  node [
    id 97
    label "postrze&#380;enie"
  ]
  node [
    id 98
    label "transgression"
  ]
  node [
    id 99
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 100
    label "wychodzenie"
  ]
  node [
    id 101
    label "uko&#324;czenie"
  ]
  node [
    id 102
    label "powiedzenie_si&#281;"
  ]
  node [
    id 103
    label "policzenie"
  ]
  node [
    id 104
    label "podziewanie_si&#281;"
  ]
  node [
    id 105
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 106
    label "exit"
  ]
  node [
    id 107
    label "vent"
  ]
  node [
    id 108
    label "uwolnienie_si&#281;"
  ]
  node [
    id 109
    label "deviation"
  ]
  node [
    id 110
    label "release"
  ]
  node [
    id 111
    label "wych&#243;d"
  ]
  node [
    id 112
    label "withdrawal"
  ]
  node [
    id 113
    label "wypadni&#281;cie"
  ]
  node [
    id 114
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 115
    label "kres"
  ]
  node [
    id 116
    label "odch&#243;d"
  ]
  node [
    id 117
    label "przebywanie"
  ]
  node [
    id 118
    label "przedstawienie"
  ]
  node [
    id 119
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 120
    label "zagranie"
  ]
  node [
    id 121
    label "zako&#324;czenie"
  ]
  node [
    id 122
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 123
    label "emergence"
  ]
  node [
    id 124
    label "cisza"
  ]
  node [
    id 125
    label "rozhowor"
  ]
  node [
    id 126
    label "discussion"
  ]
  node [
    id 127
    label "czynno&#347;&#263;"
  ]
  node [
    id 128
    label "badany"
  ]
  node [
    id 129
    label "sprawa"
  ]
  node [
    id 130
    label "wypytanie"
  ]
  node [
    id 131
    label "egzaminowanie"
  ]
  node [
    id 132
    label "zwracanie_si&#281;"
  ]
  node [
    id 133
    label "wywo&#322;ywanie"
  ]
  node [
    id 134
    label "rozpytywanie"
  ]
  node [
    id 135
    label "wypowiedzenie"
  ]
  node [
    id 136
    label "wypowied&#378;"
  ]
  node [
    id 137
    label "problemat"
  ]
  node [
    id 138
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 139
    label "problematyka"
  ]
  node [
    id 140
    label "sprawdzian"
  ]
  node [
    id 141
    label "zadanie"
  ]
  node [
    id 142
    label "odpowiada&#263;"
  ]
  node [
    id 143
    label "przes&#322;uchiwanie"
  ]
  node [
    id 144
    label "question"
  ]
  node [
    id 145
    label "sprawdzanie"
  ]
  node [
    id 146
    label "odpowiadanie"
  ]
  node [
    id 147
    label "survey"
  ]
  node [
    id 148
    label "pos&#322;uchanie"
  ]
  node [
    id 149
    label "s&#261;d"
  ]
  node [
    id 150
    label "sparafrazowanie"
  ]
  node [
    id 151
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 152
    label "strawestowa&#263;"
  ]
  node [
    id 153
    label "sparafrazowa&#263;"
  ]
  node [
    id 154
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 155
    label "trawestowa&#263;"
  ]
  node [
    id 156
    label "sformu&#322;owanie"
  ]
  node [
    id 157
    label "parafrazowanie"
  ]
  node [
    id 158
    label "ozdobnik"
  ]
  node [
    id 159
    label "delimitacja"
  ]
  node [
    id 160
    label "parafrazowa&#263;"
  ]
  node [
    id 161
    label "stylizacja"
  ]
  node [
    id 162
    label "komunikat"
  ]
  node [
    id 163
    label "trawestowanie"
  ]
  node [
    id 164
    label "strawestowanie"
  ]
  node [
    id 165
    label "konwersja"
  ]
  node [
    id 166
    label "notice"
  ]
  node [
    id 167
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 168
    label "przepowiedzenie"
  ]
  node [
    id 169
    label "generowa&#263;"
  ]
  node [
    id 170
    label "wydanie"
  ]
  node [
    id 171
    label "message"
  ]
  node [
    id 172
    label "generowanie"
  ]
  node [
    id 173
    label "wydobycie"
  ]
  node [
    id 174
    label "zwerbalizowanie"
  ]
  node [
    id 175
    label "szyk"
  ]
  node [
    id 176
    label "notification"
  ]
  node [
    id 177
    label "powiedzenie"
  ]
  node [
    id 178
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 179
    label "denunciation"
  ]
  node [
    id 180
    label "wyra&#380;enie"
  ]
  node [
    id 181
    label "zaj&#281;cie"
  ]
  node [
    id 182
    label "yield"
  ]
  node [
    id 183
    label "zbi&#243;r"
  ]
  node [
    id 184
    label "zaszkodzenie"
  ]
  node [
    id 185
    label "za&#322;o&#380;enie"
  ]
  node [
    id 186
    label "duty"
  ]
  node [
    id 187
    label "powierzanie"
  ]
  node [
    id 188
    label "work"
  ]
  node [
    id 189
    label "problem"
  ]
  node [
    id 190
    label "przepisanie"
  ]
  node [
    id 191
    label "nakarmienie"
  ]
  node [
    id 192
    label "przepisa&#263;"
  ]
  node [
    id 193
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 194
    label "zobowi&#261;zanie"
  ]
  node [
    id 195
    label "kognicja"
  ]
  node [
    id 196
    label "object"
  ]
  node [
    id 197
    label "rozprawa"
  ]
  node [
    id 198
    label "temat"
  ]
  node [
    id 199
    label "wydarzenie"
  ]
  node [
    id 200
    label "szczeg&#243;&#322;"
  ]
  node [
    id 201
    label "proposition"
  ]
  node [
    id 202
    label "przes&#322;anka"
  ]
  node [
    id 203
    label "rzecz"
  ]
  node [
    id 204
    label "idea"
  ]
  node [
    id 205
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 206
    label "ustalenie"
  ]
  node [
    id 207
    label "redagowanie"
  ]
  node [
    id 208
    label "ustalanie"
  ]
  node [
    id 209
    label "dociekanie"
  ]
  node [
    id 210
    label "robienie"
  ]
  node [
    id 211
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 212
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 213
    label "investigation"
  ]
  node [
    id 214
    label "macanie"
  ]
  node [
    id 215
    label "usi&#322;owanie"
  ]
  node [
    id 216
    label "penetrowanie"
  ]
  node [
    id 217
    label "przymierzanie"
  ]
  node [
    id 218
    label "przymierzenie"
  ]
  node [
    id 219
    label "examination"
  ]
  node [
    id 220
    label "wypytywanie"
  ]
  node [
    id 221
    label "zbadanie"
  ]
  node [
    id 222
    label "dawa&#263;"
  ]
  node [
    id 223
    label "ponosi&#263;"
  ]
  node [
    id 224
    label "report"
  ]
  node [
    id 225
    label "equate"
  ]
  node [
    id 226
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 227
    label "answer"
  ]
  node [
    id 228
    label "powodowa&#263;"
  ]
  node [
    id 229
    label "tone"
  ]
  node [
    id 230
    label "contend"
  ]
  node [
    id 231
    label "reagowa&#263;"
  ]
  node [
    id 232
    label "impart"
  ]
  node [
    id 233
    label "reagowanie"
  ]
  node [
    id 234
    label "dawanie"
  ]
  node [
    id 235
    label "powodowanie"
  ]
  node [
    id 236
    label "bycie"
  ]
  node [
    id 237
    label "pokutowanie"
  ]
  node [
    id 238
    label "odpowiedzialny"
  ]
  node [
    id 239
    label "winny"
  ]
  node [
    id 240
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 241
    label "picie_piwa"
  ]
  node [
    id 242
    label "odpowiedni"
  ]
  node [
    id 243
    label "parry"
  ]
  node [
    id 244
    label "fit"
  ]
  node [
    id 245
    label "dzianie_si&#281;"
  ]
  node [
    id 246
    label "rendition"
  ]
  node [
    id 247
    label "ponoszenie"
  ]
  node [
    id 248
    label "rozmawianie"
  ]
  node [
    id 249
    label "faza"
  ]
  node [
    id 250
    label "podchodzi&#263;"
  ]
  node [
    id 251
    label "&#263;wiczenie"
  ]
  node [
    id 252
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 253
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 254
    label "praca_pisemna"
  ]
  node [
    id 255
    label "kontrola"
  ]
  node [
    id 256
    label "dydaktyka"
  ]
  node [
    id 257
    label "pr&#243;ba"
  ]
  node [
    id 258
    label "przepytywanie"
  ]
  node [
    id 259
    label "oznajmianie"
  ]
  node [
    id 260
    label "wzywanie"
  ]
  node [
    id 261
    label "development"
  ]
  node [
    id 262
    label "exploitation"
  ]
  node [
    id 263
    label "zdawanie"
  ]
  node [
    id 264
    label "w&#322;&#261;czanie"
  ]
  node [
    id 265
    label "s&#322;uchanie"
  ]
  node [
    id 266
    label "belfer"
  ]
  node [
    id 267
    label "murza"
  ]
  node [
    id 268
    label "cz&#322;owiek"
  ]
  node [
    id 269
    label "ojciec"
  ]
  node [
    id 270
    label "samiec"
  ]
  node [
    id 271
    label "androlog"
  ]
  node [
    id 272
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 273
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 274
    label "efendi"
  ]
  node [
    id 275
    label "opiekun"
  ]
  node [
    id 276
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 277
    label "pa&#324;stwo"
  ]
  node [
    id 278
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 279
    label "bratek"
  ]
  node [
    id 280
    label "Mieszko_I"
  ]
  node [
    id 281
    label "Midas"
  ]
  node [
    id 282
    label "m&#261;&#380;"
  ]
  node [
    id 283
    label "bogaty"
  ]
  node [
    id 284
    label "popularyzator"
  ]
  node [
    id 285
    label "pracodawca"
  ]
  node [
    id 286
    label "kszta&#322;ciciel"
  ]
  node [
    id 287
    label "preceptor"
  ]
  node [
    id 288
    label "nabab"
  ]
  node [
    id 289
    label "pupil"
  ]
  node [
    id 290
    label "andropauza"
  ]
  node [
    id 291
    label "zwrot"
  ]
  node [
    id 292
    label "przyw&#243;dca"
  ]
  node [
    id 293
    label "doros&#322;y"
  ]
  node [
    id 294
    label "pedagog"
  ]
  node [
    id 295
    label "rz&#261;dzenie"
  ]
  node [
    id 296
    label "jegomo&#347;&#263;"
  ]
  node [
    id 297
    label "szkolnik"
  ]
  node [
    id 298
    label "ch&#322;opina"
  ]
  node [
    id 299
    label "w&#322;odarz"
  ]
  node [
    id 300
    label "profesor"
  ]
  node [
    id 301
    label "gra_w_karty"
  ]
  node [
    id 302
    label "w&#322;adza"
  ]
  node [
    id 303
    label "Fidel_Castro"
  ]
  node [
    id 304
    label "Anders"
  ]
  node [
    id 305
    label "Ko&#347;ciuszko"
  ]
  node [
    id 306
    label "Tito"
  ]
  node [
    id 307
    label "Miko&#322;ajczyk"
  ]
  node [
    id 308
    label "lider"
  ]
  node [
    id 309
    label "Mao"
  ]
  node [
    id 310
    label "Sabataj_Cwi"
  ]
  node [
    id 311
    label "p&#322;atnik"
  ]
  node [
    id 312
    label "zwierzchnik"
  ]
  node [
    id 313
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 314
    label "nadzorca"
  ]
  node [
    id 315
    label "funkcjonariusz"
  ]
  node [
    id 316
    label "podmiot"
  ]
  node [
    id 317
    label "wykupienie"
  ]
  node [
    id 318
    label "bycie_w_posiadaniu"
  ]
  node [
    id 319
    label "wykupywanie"
  ]
  node [
    id 320
    label "rozszerzyciel"
  ]
  node [
    id 321
    label "ludzko&#347;&#263;"
  ]
  node [
    id 322
    label "asymilowanie"
  ]
  node [
    id 323
    label "wapniak"
  ]
  node [
    id 324
    label "asymilowa&#263;"
  ]
  node [
    id 325
    label "os&#322;abia&#263;"
  ]
  node [
    id 326
    label "posta&#263;"
  ]
  node [
    id 327
    label "hominid"
  ]
  node [
    id 328
    label "podw&#322;adny"
  ]
  node [
    id 329
    label "os&#322;abianie"
  ]
  node [
    id 330
    label "g&#322;owa"
  ]
  node [
    id 331
    label "figura"
  ]
  node [
    id 332
    label "portrecista"
  ]
  node [
    id 333
    label "dwun&#243;g"
  ]
  node [
    id 334
    label "profanum"
  ]
  node [
    id 335
    label "mikrokosmos"
  ]
  node [
    id 336
    label "nasada"
  ]
  node [
    id 337
    label "duch"
  ]
  node [
    id 338
    label "antropochoria"
  ]
  node [
    id 339
    label "osoba"
  ]
  node [
    id 340
    label "wz&#243;r"
  ]
  node [
    id 341
    label "senior"
  ]
  node [
    id 342
    label "oddzia&#322;ywanie"
  ]
  node [
    id 343
    label "Adam"
  ]
  node [
    id 344
    label "homo_sapiens"
  ]
  node [
    id 345
    label "polifag"
  ]
  node [
    id 346
    label "wydoro&#347;lenie"
  ]
  node [
    id 347
    label "du&#380;y"
  ]
  node [
    id 348
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 349
    label "doro&#347;lenie"
  ]
  node [
    id 350
    label "&#378;ra&#322;y"
  ]
  node [
    id 351
    label "doro&#347;le"
  ]
  node [
    id 352
    label "dojrzale"
  ]
  node [
    id 353
    label "dojrza&#322;y"
  ]
  node [
    id 354
    label "m&#261;dry"
  ]
  node [
    id 355
    label "doletni"
  ]
  node [
    id 356
    label "punkt"
  ]
  node [
    id 357
    label "turn"
  ]
  node [
    id 358
    label "turning"
  ]
  node [
    id 359
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 360
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 361
    label "skr&#281;t"
  ]
  node [
    id 362
    label "obr&#243;t"
  ]
  node [
    id 363
    label "fraza_czasownikowa"
  ]
  node [
    id 364
    label "jednostka_leksykalna"
  ]
  node [
    id 365
    label "zmiana"
  ]
  node [
    id 366
    label "starosta"
  ]
  node [
    id 367
    label "zarz&#261;dca"
  ]
  node [
    id 368
    label "w&#322;adca"
  ]
  node [
    id 369
    label "nauczyciel"
  ]
  node [
    id 370
    label "stopie&#324;_naukowy"
  ]
  node [
    id 371
    label "nauczyciel_akademicki"
  ]
  node [
    id 372
    label "tytu&#322;"
  ]
  node [
    id 373
    label "profesura"
  ]
  node [
    id 374
    label "konsulent"
  ]
  node [
    id 375
    label "wirtuoz"
  ]
  node [
    id 376
    label "autor"
  ]
  node [
    id 377
    label "wyprawka"
  ]
  node [
    id 378
    label "mundurek"
  ]
  node [
    id 379
    label "szko&#322;a"
  ]
  node [
    id 380
    label "tarcza"
  ]
  node [
    id 381
    label "elew"
  ]
  node [
    id 382
    label "absolwent"
  ]
  node [
    id 383
    label "klasa"
  ]
  node [
    id 384
    label "ekspert"
  ]
  node [
    id 385
    label "ochotnik"
  ]
  node [
    id 386
    label "pomocnik"
  ]
  node [
    id 387
    label "student"
  ]
  node [
    id 388
    label "nauczyciel_muzyki"
  ]
  node [
    id 389
    label "zakonnik"
  ]
  node [
    id 390
    label "urz&#281;dnik"
  ]
  node [
    id 391
    label "bogacz"
  ]
  node [
    id 392
    label "dostojnik"
  ]
  node [
    id 393
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 394
    label "kuwada"
  ]
  node [
    id 395
    label "tworzyciel"
  ]
  node [
    id 396
    label "rodzice"
  ]
  node [
    id 397
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 398
    label "&#347;w"
  ]
  node [
    id 399
    label "pomys&#322;odawca"
  ]
  node [
    id 400
    label "rodzic"
  ]
  node [
    id 401
    label "wykonawca"
  ]
  node [
    id 402
    label "ojczym"
  ]
  node [
    id 403
    label "przodek"
  ]
  node [
    id 404
    label "papa"
  ]
  node [
    id 405
    label "stary"
  ]
  node [
    id 406
    label "kochanek"
  ]
  node [
    id 407
    label "fio&#322;ek"
  ]
  node [
    id 408
    label "facet"
  ]
  node [
    id 409
    label "brat"
  ]
  node [
    id 410
    label "zwierz&#281;"
  ]
  node [
    id 411
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 412
    label "ma&#322;&#380;onek"
  ]
  node [
    id 413
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 414
    label "ch&#322;op"
  ]
  node [
    id 415
    label "pan_m&#322;ody"
  ]
  node [
    id 416
    label "&#347;lubny"
  ]
  node [
    id 417
    label "pan_domu"
  ]
  node [
    id 418
    label "pan_i_w&#322;adca"
  ]
  node [
    id 419
    label "mo&#347;&#263;"
  ]
  node [
    id 420
    label "Frygia"
  ]
  node [
    id 421
    label "sprawowanie"
  ]
  node [
    id 422
    label "dominion"
  ]
  node [
    id 423
    label "dominowanie"
  ]
  node [
    id 424
    label "reign"
  ]
  node [
    id 425
    label "rule"
  ]
  node [
    id 426
    label "zwierz&#281;_domowe"
  ]
  node [
    id 427
    label "J&#281;drzejewicz"
  ]
  node [
    id 428
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 429
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 430
    label "John_Dewey"
  ]
  node [
    id 431
    label "specjalista"
  ]
  node [
    id 432
    label "&#380;ycie"
  ]
  node [
    id 433
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 434
    label "Turek"
  ]
  node [
    id 435
    label "effendi"
  ]
  node [
    id 436
    label "obfituj&#261;cy"
  ]
  node [
    id 437
    label "r&#243;&#380;norodny"
  ]
  node [
    id 438
    label "spania&#322;y"
  ]
  node [
    id 439
    label "obficie"
  ]
  node [
    id 440
    label "sytuowany"
  ]
  node [
    id 441
    label "och&#281;do&#380;ny"
  ]
  node [
    id 442
    label "forsiasty"
  ]
  node [
    id 443
    label "zapa&#347;ny"
  ]
  node [
    id 444
    label "bogato"
  ]
  node [
    id 445
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 446
    label "Katar"
  ]
  node [
    id 447
    label "Libia"
  ]
  node [
    id 448
    label "Gwatemala"
  ]
  node [
    id 449
    label "Ekwador"
  ]
  node [
    id 450
    label "Afganistan"
  ]
  node [
    id 451
    label "Tad&#380;ykistan"
  ]
  node [
    id 452
    label "Bhutan"
  ]
  node [
    id 453
    label "Argentyna"
  ]
  node [
    id 454
    label "D&#380;ibuti"
  ]
  node [
    id 455
    label "Wenezuela"
  ]
  node [
    id 456
    label "Gabon"
  ]
  node [
    id 457
    label "Ukraina"
  ]
  node [
    id 458
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 459
    label "Rwanda"
  ]
  node [
    id 460
    label "Liechtenstein"
  ]
  node [
    id 461
    label "organizacja"
  ]
  node [
    id 462
    label "Sri_Lanka"
  ]
  node [
    id 463
    label "Madagaskar"
  ]
  node [
    id 464
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 465
    label "Kongo"
  ]
  node [
    id 466
    label "Tonga"
  ]
  node [
    id 467
    label "Bangladesz"
  ]
  node [
    id 468
    label "Kanada"
  ]
  node [
    id 469
    label "Wehrlen"
  ]
  node [
    id 470
    label "Algieria"
  ]
  node [
    id 471
    label "Uganda"
  ]
  node [
    id 472
    label "Surinam"
  ]
  node [
    id 473
    label "Sahara_Zachodnia"
  ]
  node [
    id 474
    label "Chile"
  ]
  node [
    id 475
    label "W&#281;gry"
  ]
  node [
    id 476
    label "Birma"
  ]
  node [
    id 477
    label "Kazachstan"
  ]
  node [
    id 478
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 479
    label "Armenia"
  ]
  node [
    id 480
    label "Tuwalu"
  ]
  node [
    id 481
    label "Timor_Wschodni"
  ]
  node [
    id 482
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 483
    label "Izrael"
  ]
  node [
    id 484
    label "Estonia"
  ]
  node [
    id 485
    label "Komory"
  ]
  node [
    id 486
    label "Kamerun"
  ]
  node [
    id 487
    label "Haiti"
  ]
  node [
    id 488
    label "Belize"
  ]
  node [
    id 489
    label "Sierra_Leone"
  ]
  node [
    id 490
    label "Luksemburg"
  ]
  node [
    id 491
    label "USA"
  ]
  node [
    id 492
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 493
    label "Barbados"
  ]
  node [
    id 494
    label "San_Marino"
  ]
  node [
    id 495
    label "Bu&#322;garia"
  ]
  node [
    id 496
    label "Indonezja"
  ]
  node [
    id 497
    label "Wietnam"
  ]
  node [
    id 498
    label "Malawi"
  ]
  node [
    id 499
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 500
    label "Francja"
  ]
  node [
    id 501
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 502
    label "partia"
  ]
  node [
    id 503
    label "Zambia"
  ]
  node [
    id 504
    label "Angola"
  ]
  node [
    id 505
    label "Grenada"
  ]
  node [
    id 506
    label "Nepal"
  ]
  node [
    id 507
    label "Panama"
  ]
  node [
    id 508
    label "Rumunia"
  ]
  node [
    id 509
    label "Czarnog&#243;ra"
  ]
  node [
    id 510
    label "Malediwy"
  ]
  node [
    id 511
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 512
    label "S&#322;owacja"
  ]
  node [
    id 513
    label "para"
  ]
  node [
    id 514
    label "Egipt"
  ]
  node [
    id 515
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 516
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 517
    label "Mozambik"
  ]
  node [
    id 518
    label "Kolumbia"
  ]
  node [
    id 519
    label "Laos"
  ]
  node [
    id 520
    label "Burundi"
  ]
  node [
    id 521
    label "Suazi"
  ]
  node [
    id 522
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 523
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 524
    label "Czechy"
  ]
  node [
    id 525
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 526
    label "Wyspy_Marshalla"
  ]
  node [
    id 527
    label "Dominika"
  ]
  node [
    id 528
    label "Trynidad_i_Tobago"
  ]
  node [
    id 529
    label "Syria"
  ]
  node [
    id 530
    label "Palau"
  ]
  node [
    id 531
    label "Gwinea_Bissau"
  ]
  node [
    id 532
    label "Liberia"
  ]
  node [
    id 533
    label "Jamajka"
  ]
  node [
    id 534
    label "Zimbabwe"
  ]
  node [
    id 535
    label "Polska"
  ]
  node [
    id 536
    label "Dominikana"
  ]
  node [
    id 537
    label "Senegal"
  ]
  node [
    id 538
    label "Togo"
  ]
  node [
    id 539
    label "Gujana"
  ]
  node [
    id 540
    label "Gruzja"
  ]
  node [
    id 541
    label "Albania"
  ]
  node [
    id 542
    label "Zair"
  ]
  node [
    id 543
    label "Meksyk"
  ]
  node [
    id 544
    label "Macedonia"
  ]
  node [
    id 545
    label "Chorwacja"
  ]
  node [
    id 546
    label "Kambod&#380;a"
  ]
  node [
    id 547
    label "Monako"
  ]
  node [
    id 548
    label "Mauritius"
  ]
  node [
    id 549
    label "Gwinea"
  ]
  node [
    id 550
    label "Mali"
  ]
  node [
    id 551
    label "Nigeria"
  ]
  node [
    id 552
    label "Kostaryka"
  ]
  node [
    id 553
    label "Hanower"
  ]
  node [
    id 554
    label "Paragwaj"
  ]
  node [
    id 555
    label "W&#322;ochy"
  ]
  node [
    id 556
    label "Seszele"
  ]
  node [
    id 557
    label "Wyspy_Salomona"
  ]
  node [
    id 558
    label "Hiszpania"
  ]
  node [
    id 559
    label "Boliwia"
  ]
  node [
    id 560
    label "Kirgistan"
  ]
  node [
    id 561
    label "Irlandia"
  ]
  node [
    id 562
    label "Czad"
  ]
  node [
    id 563
    label "Irak"
  ]
  node [
    id 564
    label "Lesoto"
  ]
  node [
    id 565
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 566
    label "Malta"
  ]
  node [
    id 567
    label "Andora"
  ]
  node [
    id 568
    label "Chiny"
  ]
  node [
    id 569
    label "Filipiny"
  ]
  node [
    id 570
    label "Antarktis"
  ]
  node [
    id 571
    label "Niemcy"
  ]
  node [
    id 572
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 573
    label "Pakistan"
  ]
  node [
    id 574
    label "terytorium"
  ]
  node [
    id 575
    label "Nikaragua"
  ]
  node [
    id 576
    label "Brazylia"
  ]
  node [
    id 577
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 578
    label "Maroko"
  ]
  node [
    id 579
    label "Portugalia"
  ]
  node [
    id 580
    label "Niger"
  ]
  node [
    id 581
    label "Kenia"
  ]
  node [
    id 582
    label "Botswana"
  ]
  node [
    id 583
    label "Fid&#380;i"
  ]
  node [
    id 584
    label "Tunezja"
  ]
  node [
    id 585
    label "Australia"
  ]
  node [
    id 586
    label "Tajlandia"
  ]
  node [
    id 587
    label "Burkina_Faso"
  ]
  node [
    id 588
    label "interior"
  ]
  node [
    id 589
    label "Tanzania"
  ]
  node [
    id 590
    label "Benin"
  ]
  node [
    id 591
    label "Indie"
  ]
  node [
    id 592
    label "&#321;otwa"
  ]
  node [
    id 593
    label "Kiribati"
  ]
  node [
    id 594
    label "Antigua_i_Barbuda"
  ]
  node [
    id 595
    label "Rodezja"
  ]
  node [
    id 596
    label "Cypr"
  ]
  node [
    id 597
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 598
    label "Peru"
  ]
  node [
    id 599
    label "Austria"
  ]
  node [
    id 600
    label "Urugwaj"
  ]
  node [
    id 601
    label "Jordania"
  ]
  node [
    id 602
    label "Grecja"
  ]
  node [
    id 603
    label "Azerbejd&#380;an"
  ]
  node [
    id 604
    label "Turcja"
  ]
  node [
    id 605
    label "Samoa"
  ]
  node [
    id 606
    label "Sudan"
  ]
  node [
    id 607
    label "Oman"
  ]
  node [
    id 608
    label "ziemia"
  ]
  node [
    id 609
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 610
    label "Uzbekistan"
  ]
  node [
    id 611
    label "Portoryko"
  ]
  node [
    id 612
    label "Honduras"
  ]
  node [
    id 613
    label "Mongolia"
  ]
  node [
    id 614
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 615
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 616
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 617
    label "Serbia"
  ]
  node [
    id 618
    label "Tajwan"
  ]
  node [
    id 619
    label "Wielka_Brytania"
  ]
  node [
    id 620
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 621
    label "Liban"
  ]
  node [
    id 622
    label "Japonia"
  ]
  node [
    id 623
    label "Ghana"
  ]
  node [
    id 624
    label "Belgia"
  ]
  node [
    id 625
    label "Bahrajn"
  ]
  node [
    id 626
    label "Mikronezja"
  ]
  node [
    id 627
    label "Etiopia"
  ]
  node [
    id 628
    label "Kuwejt"
  ]
  node [
    id 629
    label "grupa"
  ]
  node [
    id 630
    label "Bahamy"
  ]
  node [
    id 631
    label "Rosja"
  ]
  node [
    id 632
    label "Mo&#322;dawia"
  ]
  node [
    id 633
    label "Litwa"
  ]
  node [
    id 634
    label "S&#322;owenia"
  ]
  node [
    id 635
    label "Szwajcaria"
  ]
  node [
    id 636
    label "Erytrea"
  ]
  node [
    id 637
    label "Arabia_Saudyjska"
  ]
  node [
    id 638
    label "Kuba"
  ]
  node [
    id 639
    label "granica_pa&#324;stwa"
  ]
  node [
    id 640
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 641
    label "Malezja"
  ]
  node [
    id 642
    label "Korea"
  ]
  node [
    id 643
    label "Jemen"
  ]
  node [
    id 644
    label "Nowa_Zelandia"
  ]
  node [
    id 645
    label "Namibia"
  ]
  node [
    id 646
    label "Nauru"
  ]
  node [
    id 647
    label "holoarktyka"
  ]
  node [
    id 648
    label "Brunei"
  ]
  node [
    id 649
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 650
    label "Khitai"
  ]
  node [
    id 651
    label "Mauretania"
  ]
  node [
    id 652
    label "Iran"
  ]
  node [
    id 653
    label "Gambia"
  ]
  node [
    id 654
    label "Somalia"
  ]
  node [
    id 655
    label "Holandia"
  ]
  node [
    id 656
    label "Turkmenistan"
  ]
  node [
    id 657
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 658
    label "Salwador"
  ]
  node [
    id 659
    label "ablegat"
  ]
  node [
    id 660
    label "izba_ni&#380;sza"
  ]
  node [
    id 661
    label "Korwin"
  ]
  node [
    id 662
    label "dyscyplina_partyjna"
  ]
  node [
    id 663
    label "kurier_dyplomatyczny"
  ]
  node [
    id 664
    label "wys&#322;annik"
  ]
  node [
    id 665
    label "poselstwo"
  ]
  node [
    id 666
    label "parlamentarzysta"
  ]
  node [
    id 667
    label "przedstawiciel"
  ]
  node [
    id 668
    label "dyplomata"
  ]
  node [
    id 669
    label "klubista"
  ]
  node [
    id 670
    label "reprezentacja"
  ]
  node [
    id 671
    label "klub"
  ]
  node [
    id 672
    label "cz&#322;onek"
  ]
  node [
    id 673
    label "mandatariusz"
  ]
  node [
    id 674
    label "grupa_bilateralna"
  ]
  node [
    id 675
    label "polityk"
  ]
  node [
    id 676
    label "parlament"
  ]
  node [
    id 677
    label "mi&#322;y"
  ]
  node [
    id 678
    label "korpus_dyplomatyczny"
  ]
  node [
    id 679
    label "dyplomatyczny"
  ]
  node [
    id 680
    label "takt"
  ]
  node [
    id 681
    label "Metternich"
  ]
  node [
    id 682
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 683
    label "przyk&#322;ad"
  ]
  node [
    id 684
    label "substytuowa&#263;"
  ]
  node [
    id 685
    label "substytuowanie"
  ]
  node [
    id 686
    label "zast&#281;pca"
  ]
  node [
    id 687
    label "discover"
  ]
  node [
    id 688
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 689
    label "wydoby&#263;"
  ]
  node [
    id 690
    label "okre&#347;li&#263;"
  ]
  node [
    id 691
    label "poda&#263;"
  ]
  node [
    id 692
    label "express"
  ]
  node [
    id 693
    label "wyrazi&#263;"
  ]
  node [
    id 694
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 695
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 696
    label "rzekn&#261;&#263;"
  ]
  node [
    id 697
    label "unwrap"
  ]
  node [
    id 698
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 699
    label "convey"
  ]
  node [
    id 700
    label "tenis"
  ]
  node [
    id 701
    label "supply"
  ]
  node [
    id 702
    label "da&#263;"
  ]
  node [
    id 703
    label "ustawi&#263;"
  ]
  node [
    id 704
    label "siatk&#243;wka"
  ]
  node [
    id 705
    label "give"
  ]
  node [
    id 706
    label "zagra&#263;"
  ]
  node [
    id 707
    label "jedzenie"
  ]
  node [
    id 708
    label "poinformowa&#263;"
  ]
  node [
    id 709
    label "introduce"
  ]
  node [
    id 710
    label "nafaszerowa&#263;"
  ]
  node [
    id 711
    label "zaserwowa&#263;"
  ]
  node [
    id 712
    label "draw"
  ]
  node [
    id 713
    label "doby&#263;"
  ]
  node [
    id 714
    label "g&#243;rnictwo"
  ]
  node [
    id 715
    label "wyeksploatowa&#263;"
  ]
  node [
    id 716
    label "extract"
  ]
  node [
    id 717
    label "obtain"
  ]
  node [
    id 718
    label "wyj&#261;&#263;"
  ]
  node [
    id 719
    label "ocali&#263;"
  ]
  node [
    id 720
    label "uzyska&#263;"
  ]
  node [
    id 721
    label "wyda&#263;"
  ]
  node [
    id 722
    label "wydosta&#263;"
  ]
  node [
    id 723
    label "uwydatni&#263;"
  ]
  node [
    id 724
    label "distill"
  ]
  node [
    id 725
    label "raise"
  ]
  node [
    id 726
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 727
    label "testify"
  ]
  node [
    id 728
    label "zakomunikowa&#263;"
  ]
  node [
    id 729
    label "oznaczy&#263;"
  ]
  node [
    id 730
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 731
    label "zdecydowa&#263;"
  ]
  node [
    id 732
    label "zrobi&#263;"
  ]
  node [
    id 733
    label "spowodowa&#263;"
  ]
  node [
    id 734
    label "situate"
  ]
  node [
    id 735
    label "nominate"
  ]
  node [
    id 736
    label "nieca&#322;y"
  ]
  node [
    id 737
    label "dekompletowanie_si&#281;"
  ]
  node [
    id 738
    label "zdekompletowanie_si&#281;"
  ]
  node [
    id 739
    label "nieca&#322;kowicie"
  ]
  node [
    id 740
    label "calibration"
  ]
  node [
    id 741
    label "dominance"
  ]
  node [
    id 742
    label "control"
  ]
  node [
    id 743
    label "zasada"
  ]
  node [
    id 744
    label "sfinansowanie"
  ]
  node [
    id 745
    label "ulepszenie"
  ]
  node [
    id 746
    label "rewizja"
  ]
  node [
    id 747
    label "passage"
  ]
  node [
    id 748
    label "oznaka"
  ]
  node [
    id 749
    label "change"
  ]
  node [
    id 750
    label "ferment"
  ]
  node [
    id 751
    label "komplet"
  ]
  node [
    id 752
    label "anatomopatolog"
  ]
  node [
    id 753
    label "zmianka"
  ]
  node [
    id 754
    label "czas"
  ]
  node [
    id 755
    label "zjawisko"
  ]
  node [
    id 756
    label "amendment"
  ]
  node [
    id 757
    label "praca"
  ]
  node [
    id 758
    label "odmienianie"
  ]
  node [
    id 759
    label "tura"
  ]
  node [
    id 760
    label "payment"
  ]
  node [
    id 761
    label "modyfikacja"
  ]
  node [
    id 762
    label "lepszy"
  ]
  node [
    id 763
    label "poprawa"
  ]
  node [
    id 764
    label "zmienienie"
  ]
  node [
    id 765
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 766
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 767
    label "regu&#322;a_Allena"
  ]
  node [
    id 768
    label "base"
  ]
  node [
    id 769
    label "umowa"
  ]
  node [
    id 770
    label "obserwacja"
  ]
  node [
    id 771
    label "zasada_d'Alemberta"
  ]
  node [
    id 772
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 773
    label "normalizacja"
  ]
  node [
    id 774
    label "moralno&#347;&#263;"
  ]
  node [
    id 775
    label "criterion"
  ]
  node [
    id 776
    label "opis"
  ]
  node [
    id 777
    label "regu&#322;a_Glogera"
  ]
  node [
    id 778
    label "prawo_Mendla"
  ]
  node [
    id 779
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 780
    label "twierdzenie"
  ]
  node [
    id 781
    label "prawo"
  ]
  node [
    id 782
    label "standard"
  ]
  node [
    id 783
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 784
    label "spos&#243;b"
  ]
  node [
    id 785
    label "qualification"
  ]
  node [
    id 786
    label "occupation"
  ]
  node [
    id 787
    label "podstawa"
  ]
  node [
    id 788
    label "substancja"
  ]
  node [
    id 789
    label "prawid&#322;o"
  ]
  node [
    id 790
    label "klawisz"
  ]
  node [
    id 791
    label "odzyskiwa&#263;"
  ]
  node [
    id 792
    label "znachodzi&#263;"
  ]
  node [
    id 793
    label "pozyskiwa&#263;"
  ]
  node [
    id 794
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 795
    label "detect"
  ]
  node [
    id 796
    label "wykrywa&#263;"
  ]
  node [
    id 797
    label "os&#261;dza&#263;"
  ]
  node [
    id 798
    label "doznawa&#263;"
  ]
  node [
    id 799
    label "wymy&#347;la&#263;"
  ]
  node [
    id 800
    label "mistreat"
  ]
  node [
    id 801
    label "obra&#380;a&#263;"
  ]
  node [
    id 802
    label "odkrywa&#263;"
  ]
  node [
    id 803
    label "debunk"
  ]
  node [
    id 804
    label "dostrzega&#263;"
  ]
  node [
    id 805
    label "okre&#347;la&#263;"
  ]
  node [
    id 806
    label "mie&#263;_miejsce"
  ]
  node [
    id 807
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 808
    label "motywowa&#263;"
  ]
  node [
    id 809
    label "act"
  ]
  node [
    id 810
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 811
    label "uzyskiwa&#263;"
  ]
  node [
    id 812
    label "wytwarza&#263;"
  ]
  node [
    id 813
    label "tease"
  ]
  node [
    id 814
    label "take"
  ]
  node [
    id 815
    label "hurt"
  ]
  node [
    id 816
    label "recur"
  ]
  node [
    id 817
    label "przychodzi&#263;"
  ]
  node [
    id 818
    label "sum_up"
  ]
  node [
    id 819
    label "strike"
  ]
  node [
    id 820
    label "robi&#263;"
  ]
  node [
    id 821
    label "s&#261;dzi&#263;"
  ]
  node [
    id 822
    label "hold"
  ]
  node [
    id 823
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 824
    label "godzina"
  ]
  node [
    id 825
    label "time"
  ]
  node [
    id 826
    label "doba"
  ]
  node [
    id 827
    label "p&#243;&#322;godzina"
  ]
  node [
    id 828
    label "jednostka_czasu"
  ]
  node [
    id 829
    label "minuta"
  ]
  node [
    id 830
    label "kwadrans"
  ]
  node [
    id 831
    label "Rzym_Zachodni"
  ]
  node [
    id 832
    label "whole"
  ]
  node [
    id 833
    label "ilo&#347;&#263;"
  ]
  node [
    id 834
    label "element"
  ]
  node [
    id 835
    label "Rzym_Wschodni"
  ]
  node [
    id 836
    label "urz&#261;dzenie"
  ]
  node [
    id 837
    label "r&#243;&#380;niczka"
  ]
  node [
    id 838
    label "&#347;rodowisko"
  ]
  node [
    id 839
    label "przedmiot"
  ]
  node [
    id 840
    label "materia"
  ]
  node [
    id 841
    label "szambo"
  ]
  node [
    id 842
    label "aspo&#322;eczny"
  ]
  node [
    id 843
    label "component"
  ]
  node [
    id 844
    label "szkodnik"
  ]
  node [
    id 845
    label "gangsterski"
  ]
  node [
    id 846
    label "poj&#281;cie"
  ]
  node [
    id 847
    label "underworld"
  ]
  node [
    id 848
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 849
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 850
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 851
    label "rozmiar"
  ]
  node [
    id 852
    label "part"
  ]
  node [
    id 853
    label "kom&#243;rka"
  ]
  node [
    id 854
    label "furnishing"
  ]
  node [
    id 855
    label "zabezpieczenie"
  ]
  node [
    id 856
    label "zrobienie"
  ]
  node [
    id 857
    label "wyrz&#261;dzenie"
  ]
  node [
    id 858
    label "zagospodarowanie"
  ]
  node [
    id 859
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 860
    label "ig&#322;a"
  ]
  node [
    id 861
    label "narz&#281;dzie"
  ]
  node [
    id 862
    label "wirnik"
  ]
  node [
    id 863
    label "aparatura"
  ]
  node [
    id 864
    label "system_energetyczny"
  ]
  node [
    id 865
    label "impulsator"
  ]
  node [
    id 866
    label "mechanizm"
  ]
  node [
    id 867
    label "sprz&#281;t"
  ]
  node [
    id 868
    label "blokowanie"
  ]
  node [
    id 869
    label "set"
  ]
  node [
    id 870
    label "zablokowanie"
  ]
  node [
    id 871
    label "przygotowanie"
  ]
  node [
    id 872
    label "komora"
  ]
  node [
    id 873
    label "j&#281;zyk"
  ]
  node [
    id 874
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 875
    label "po_europejsku"
  ]
  node [
    id 876
    label "European"
  ]
  node [
    id 877
    label "typowy"
  ]
  node [
    id 878
    label "charakterystyczny"
  ]
  node [
    id 879
    label "europejsko"
  ]
  node [
    id 880
    label "zwyczajny"
  ]
  node [
    id 881
    label "typowo"
  ]
  node [
    id 882
    label "cz&#281;sty"
  ]
  node [
    id 883
    label "zwyk&#322;y"
  ]
  node [
    id 884
    label "charakterystycznie"
  ]
  node [
    id 885
    label "szczeg&#243;lny"
  ]
  node [
    id 886
    label "wyj&#261;tkowy"
  ]
  node [
    id 887
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 888
    label "podobny"
  ]
  node [
    id 889
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 890
    label "nale&#380;ny"
  ]
  node [
    id 891
    label "nale&#380;yty"
  ]
  node [
    id 892
    label "uprawniony"
  ]
  node [
    id 893
    label "zasadniczy"
  ]
  node [
    id 894
    label "stosownie"
  ]
  node [
    id 895
    label "taki"
  ]
  node [
    id 896
    label "prawdziwy"
  ]
  node [
    id 897
    label "ten"
  ]
  node [
    id 898
    label "dobry"
  ]
  node [
    id 899
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 900
    label "zobo"
  ]
  node [
    id 901
    label "yakalo"
  ]
  node [
    id 902
    label "byd&#322;o"
  ]
  node [
    id 903
    label "dzo"
  ]
  node [
    id 904
    label "kr&#281;torogie"
  ]
  node [
    id 905
    label "czochrad&#322;o"
  ]
  node [
    id 906
    label "posp&#243;lstwo"
  ]
  node [
    id 907
    label "kraal"
  ]
  node [
    id 908
    label "livestock"
  ]
  node [
    id 909
    label "prze&#380;uwacz"
  ]
  node [
    id 910
    label "bizon"
  ]
  node [
    id 911
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 912
    label "zebu"
  ]
  node [
    id 913
    label "byd&#322;o_domowe"
  ]
  node [
    id 914
    label "Logan"
  ]
  node [
    id 915
    label "dziecko"
  ]
  node [
    id 916
    label "podciep"
  ]
  node [
    id 917
    label "degenerat"
  ]
  node [
    id 918
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 919
    label "zwyrol"
  ]
  node [
    id 920
    label "czerniak"
  ]
  node [
    id 921
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 922
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 923
    label "paszcza"
  ]
  node [
    id 924
    label "popapraniec"
  ]
  node [
    id 925
    label "skuba&#263;"
  ]
  node [
    id 926
    label "skubanie"
  ]
  node [
    id 927
    label "agresja"
  ]
  node [
    id 928
    label "skubni&#281;cie"
  ]
  node [
    id 929
    label "zwierz&#281;ta"
  ]
  node [
    id 930
    label "fukni&#281;cie"
  ]
  node [
    id 931
    label "farba"
  ]
  node [
    id 932
    label "fukanie"
  ]
  node [
    id 933
    label "istota_&#380;ywa"
  ]
  node [
    id 934
    label "gad"
  ]
  node [
    id 935
    label "tresowa&#263;"
  ]
  node [
    id 936
    label "siedzie&#263;"
  ]
  node [
    id 937
    label "oswaja&#263;"
  ]
  node [
    id 938
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 939
    label "poligamia"
  ]
  node [
    id 940
    label "oz&#243;r"
  ]
  node [
    id 941
    label "skubn&#261;&#263;"
  ]
  node [
    id 942
    label "wios&#322;owa&#263;"
  ]
  node [
    id 943
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 944
    label "le&#380;enie"
  ]
  node [
    id 945
    label "niecz&#322;owiek"
  ]
  node [
    id 946
    label "wios&#322;owanie"
  ]
  node [
    id 947
    label "napasienie_si&#281;"
  ]
  node [
    id 948
    label "wiwarium"
  ]
  node [
    id 949
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 950
    label "animalista"
  ]
  node [
    id 951
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 952
    label "budowa"
  ]
  node [
    id 953
    label "hodowla"
  ]
  node [
    id 954
    label "pasienie_si&#281;"
  ]
  node [
    id 955
    label "sodomita"
  ]
  node [
    id 956
    label "monogamia"
  ]
  node [
    id 957
    label "przyssawka"
  ]
  node [
    id 958
    label "budowa_cia&#322;a"
  ]
  node [
    id 959
    label "okrutnik"
  ]
  node [
    id 960
    label "grzbiet"
  ]
  node [
    id 961
    label "weterynarz"
  ]
  node [
    id 962
    label "&#322;eb"
  ]
  node [
    id 963
    label "wylinka"
  ]
  node [
    id 964
    label "bestia"
  ]
  node [
    id 965
    label "poskramia&#263;"
  ]
  node [
    id 966
    label "fauna"
  ]
  node [
    id 967
    label "treser"
  ]
  node [
    id 968
    label "siedzenie"
  ]
  node [
    id 969
    label "le&#380;e&#263;"
  ]
  node [
    id 970
    label "utulenie"
  ]
  node [
    id 971
    label "pediatra"
  ]
  node [
    id 972
    label "dzieciak"
  ]
  node [
    id 973
    label "utulanie"
  ]
  node [
    id 974
    label "dzieciarnia"
  ]
  node [
    id 975
    label "niepe&#322;noletni"
  ]
  node [
    id 976
    label "utula&#263;"
  ]
  node [
    id 977
    label "cz&#322;owieczek"
  ]
  node [
    id 978
    label "fledgling"
  ]
  node [
    id 979
    label "utuli&#263;"
  ]
  node [
    id 980
    label "m&#322;odzik"
  ]
  node [
    id 981
    label "pedofil"
  ]
  node [
    id 982
    label "m&#322;odziak"
  ]
  node [
    id 983
    label "potomek"
  ]
  node [
    id 984
    label "entliczek-pentliczek"
  ]
  node [
    id 985
    label "potomstwo"
  ]
  node [
    id 986
    label "sraluch"
  ]
  node [
    id 987
    label "Dacia"
  ]
  node [
    id 988
    label "logan"
  ]
  node [
    id 989
    label "Bachus"
  ]
  node [
    id 990
    label "podrzutek"
  ]
  node [
    id 991
    label "dziwo&#380;ona"
  ]
  node [
    id 992
    label "mamuna"
  ]
  node [
    id 993
    label "przybli&#380;enie"
  ]
  node [
    id 994
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 995
    label "kategoria"
  ]
  node [
    id 996
    label "szpaler"
  ]
  node [
    id 997
    label "lon&#380;a"
  ]
  node [
    id 998
    label "uporz&#261;dkowanie"
  ]
  node [
    id 999
    label "instytucja"
  ]
  node [
    id 1000
    label "jednostka_systematyczna"
  ]
  node [
    id 1001
    label "egzekutywa"
  ]
  node [
    id 1002
    label "premier"
  ]
  node [
    id 1003
    label "Londyn"
  ]
  node [
    id 1004
    label "gabinet_cieni"
  ]
  node [
    id 1005
    label "gromada"
  ]
  node [
    id 1006
    label "number"
  ]
  node [
    id 1007
    label "Konsulat"
  ]
  node [
    id 1008
    label "tract"
  ]
  node [
    id 1009
    label "struktura"
  ]
  node [
    id 1010
    label "spowodowanie"
  ]
  node [
    id 1011
    label "structure"
  ]
  node [
    id 1012
    label "sequence"
  ]
  node [
    id 1013
    label "succession"
  ]
  node [
    id 1014
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1015
    label "zapoznanie"
  ]
  node [
    id 1016
    label "podanie"
  ]
  node [
    id 1017
    label "bliski"
  ]
  node [
    id 1018
    label "wyja&#347;nienie"
  ]
  node [
    id 1019
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1020
    label "przemieszczenie"
  ]
  node [
    id 1021
    label "approach"
  ]
  node [
    id 1022
    label "pickup"
  ]
  node [
    id 1023
    label "estimate"
  ]
  node [
    id 1024
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1025
    label "ocena"
  ]
  node [
    id 1026
    label "type"
  ]
  node [
    id 1027
    label "teoria"
  ]
  node [
    id 1028
    label "forma"
  ]
  node [
    id 1029
    label "osoba_prawna"
  ]
  node [
    id 1030
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1031
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1032
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1033
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1034
    label "biuro"
  ]
  node [
    id 1035
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1036
    label "Fundusze_Unijne"
  ]
  node [
    id 1037
    label "zamyka&#263;"
  ]
  node [
    id 1038
    label "establishment"
  ]
  node [
    id 1039
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1040
    label "urz&#261;d"
  ]
  node [
    id 1041
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1042
    label "afiliowa&#263;"
  ]
  node [
    id 1043
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1044
    label "zamykanie"
  ]
  node [
    id 1045
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1046
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1047
    label "organ"
  ]
  node [
    id 1048
    label "obrady"
  ]
  node [
    id 1049
    label "executive"
  ]
  node [
    id 1050
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1051
    label "federacja"
  ]
  node [
    id 1052
    label "przej&#347;cie"
  ]
  node [
    id 1053
    label "espalier"
  ]
  node [
    id 1054
    label "aleja"
  ]
  node [
    id 1055
    label "jednostka_administracyjna"
  ]
  node [
    id 1056
    label "zoologia"
  ]
  node [
    id 1057
    label "skupienie"
  ]
  node [
    id 1058
    label "kr&#243;lestwo"
  ]
  node [
    id 1059
    label "stage_set"
  ]
  node [
    id 1060
    label "tribe"
  ]
  node [
    id 1061
    label "hurma"
  ]
  node [
    id 1062
    label "botanika"
  ]
  node [
    id 1063
    label "wagon"
  ]
  node [
    id 1064
    label "mecz_mistrzowski"
  ]
  node [
    id 1065
    label "arrangement"
  ]
  node [
    id 1066
    label "class"
  ]
  node [
    id 1067
    label "&#322;awka"
  ]
  node [
    id 1068
    label "wykrzyknik"
  ]
  node [
    id 1069
    label "zaleta"
  ]
  node [
    id 1070
    label "programowanie_obiektowe"
  ]
  node [
    id 1071
    label "tablica"
  ]
  node [
    id 1072
    label "warstwa"
  ]
  node [
    id 1073
    label "rezerwa"
  ]
  node [
    id 1074
    label "Ekwici"
  ]
  node [
    id 1075
    label "sala"
  ]
  node [
    id 1076
    label "pomoc"
  ]
  node [
    id 1077
    label "form"
  ]
  node [
    id 1078
    label "jako&#347;&#263;"
  ]
  node [
    id 1079
    label "znak_jako&#347;ci"
  ]
  node [
    id 1080
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1081
    label "poziom"
  ]
  node [
    id 1082
    label "promocja"
  ]
  node [
    id 1083
    label "kurs"
  ]
  node [
    id 1084
    label "obiekt"
  ]
  node [
    id 1085
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1086
    label "dziennik_lekcyjny"
  ]
  node [
    id 1087
    label "fakcja"
  ]
  node [
    id 1088
    label "obrona"
  ]
  node [
    id 1089
    label "atak"
  ]
  node [
    id 1090
    label "lina"
  ]
  node [
    id 1091
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 1092
    label "Bismarck"
  ]
  node [
    id 1093
    label "Sto&#322;ypin"
  ]
  node [
    id 1094
    label "Chruszczow"
  ]
  node [
    id 1095
    label "Jelcyn"
  ]
  node [
    id 1096
    label "panowanie"
  ]
  node [
    id 1097
    label "Kreml"
  ]
  node [
    id 1098
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 1099
    label "wydolno&#347;&#263;"
  ]
  node [
    id 1100
    label "Wimbledon"
  ]
  node [
    id 1101
    label "Westminster"
  ]
  node [
    id 1102
    label "Londek"
  ]
  node [
    id 1103
    label "zamierza&#263;"
  ]
  node [
    id 1104
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1105
    label "anticipate"
  ]
  node [
    id 1106
    label "volunteer"
  ]
  node [
    id 1107
    label "autorament"
  ]
  node [
    id 1108
    label "variety"
  ]
  node [
    id 1109
    label "antycypacja"
  ]
  node [
    id 1110
    label "przypuszczenie"
  ]
  node [
    id 1111
    label "cynk"
  ]
  node [
    id 1112
    label "obstawia&#263;"
  ]
  node [
    id 1113
    label "sztuka"
  ]
  node [
    id 1114
    label "design"
  ]
  node [
    id 1115
    label "pob&#243;r"
  ]
  node [
    id 1116
    label "wojsko"
  ]
  node [
    id 1117
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1118
    label "wygl&#261;d"
  ]
  node [
    id 1119
    label "pogl&#261;d"
  ]
  node [
    id 1120
    label "proces"
  ]
  node [
    id 1121
    label "zapowied&#378;"
  ]
  node [
    id 1122
    label "upodobnienie"
  ]
  node [
    id 1123
    label "narracja"
  ]
  node [
    id 1124
    label "prediction"
  ]
  node [
    id 1125
    label "pr&#243;bowanie"
  ]
  node [
    id 1126
    label "rola"
  ]
  node [
    id 1127
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1128
    label "realizacja"
  ]
  node [
    id 1129
    label "scena"
  ]
  node [
    id 1130
    label "didaskalia"
  ]
  node [
    id 1131
    label "czyn"
  ]
  node [
    id 1132
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1133
    label "environment"
  ]
  node [
    id 1134
    label "head"
  ]
  node [
    id 1135
    label "scenariusz"
  ]
  node [
    id 1136
    label "egzemplarz"
  ]
  node [
    id 1137
    label "jednostka"
  ]
  node [
    id 1138
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1139
    label "kultura_duchowa"
  ]
  node [
    id 1140
    label "fortel"
  ]
  node [
    id 1141
    label "theatrical_performance"
  ]
  node [
    id 1142
    label "ambala&#380;"
  ]
  node [
    id 1143
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1144
    label "kobieta"
  ]
  node [
    id 1145
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1146
    label "Faust"
  ]
  node [
    id 1147
    label "scenografia"
  ]
  node [
    id 1148
    label "ods&#322;ona"
  ]
  node [
    id 1149
    label "pokaz"
  ]
  node [
    id 1150
    label "przedstawi&#263;"
  ]
  node [
    id 1151
    label "Apollo"
  ]
  node [
    id 1152
    label "kultura"
  ]
  node [
    id 1153
    label "przedstawianie"
  ]
  node [
    id 1154
    label "przedstawia&#263;"
  ]
  node [
    id 1155
    label "towar"
  ]
  node [
    id 1156
    label "datum"
  ]
  node [
    id 1157
    label "poszlaka"
  ]
  node [
    id 1158
    label "dopuszczenie"
  ]
  node [
    id 1159
    label "conjecture"
  ]
  node [
    id 1160
    label "koniektura"
  ]
  node [
    id 1161
    label "tip-off"
  ]
  node [
    id 1162
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 1163
    label "tip"
  ]
  node [
    id 1164
    label "sygna&#322;"
  ]
  node [
    id 1165
    label "metal_kolorowy"
  ]
  node [
    id 1166
    label "mikroelement"
  ]
  node [
    id 1167
    label "cynkowiec"
  ]
  node [
    id 1168
    label "ubezpiecza&#263;"
  ]
  node [
    id 1169
    label "venture"
  ]
  node [
    id 1170
    label "zapewnia&#263;"
  ]
  node [
    id 1171
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 1172
    label "typowa&#263;"
  ]
  node [
    id 1173
    label "ochrona"
  ]
  node [
    id 1174
    label "zastawia&#263;"
  ]
  node [
    id 1175
    label "budowa&#263;"
  ]
  node [
    id 1176
    label "zajmowa&#263;"
  ]
  node [
    id 1177
    label "obejmowa&#263;"
  ]
  node [
    id 1178
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 1179
    label "os&#322;ania&#263;"
  ]
  node [
    id 1180
    label "otacza&#263;"
  ]
  node [
    id 1181
    label "broni&#263;"
  ]
  node [
    id 1182
    label "powierza&#263;"
  ]
  node [
    id 1183
    label "bramka"
  ]
  node [
    id 1184
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 1185
    label "frame"
  ]
  node [
    id 1186
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1187
    label "dzia&#322;anie"
  ]
  node [
    id 1188
    label "event"
  ]
  node [
    id 1189
    label "przyczyna"
  ]
  node [
    id 1190
    label "ro&#347;liny"
  ]
  node [
    id 1191
    label "grzyby"
  ]
  node [
    id 1192
    label "Arktogea"
  ]
  node [
    id 1193
    label "prokarioty"
  ]
  node [
    id 1194
    label "domena"
  ]
  node [
    id 1195
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 1196
    label "protisty"
  ]
  node [
    id 1197
    label "kategoria_systematyczna"
  ]
  node [
    id 1198
    label "po&#322;&#243;g"
  ]
  node [
    id 1199
    label "spe&#322;nienie"
  ]
  node [
    id 1200
    label "dula"
  ]
  node [
    id 1201
    label "usuni&#281;cie"
  ]
  node [
    id 1202
    label "wymy&#347;lenie"
  ]
  node [
    id 1203
    label "po&#322;o&#380;na"
  ]
  node [
    id 1204
    label "uniewa&#380;nienie"
  ]
  node [
    id 1205
    label "proces_fizjologiczny"
  ]
  node [
    id 1206
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1207
    label "pomys&#322;"
  ]
  node [
    id 1208
    label "szok_poporodowy"
  ]
  node [
    id 1209
    label "marc&#243;wka"
  ]
  node [
    id 1210
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 1211
    label "birth"
  ]
  node [
    id 1212
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1213
    label "wynik"
  ]
  node [
    id 1214
    label "przestanie"
  ]
  node [
    id 1215
    label "wyniesienie"
  ]
  node [
    id 1216
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1217
    label "odej&#347;cie"
  ]
  node [
    id 1218
    label "pozabieranie"
  ]
  node [
    id 1219
    label "pozbycie_si&#281;"
  ]
  node [
    id 1220
    label "pousuwanie"
  ]
  node [
    id 1221
    label "przesuni&#281;cie"
  ]
  node [
    id 1222
    label "przeniesienie"
  ]
  node [
    id 1223
    label "znikni&#281;cie"
  ]
  node [
    id 1224
    label "coitus_interruptus"
  ]
  node [
    id 1225
    label "abstraction"
  ]
  node [
    id 1226
    label "removal"
  ]
  node [
    id 1227
    label "wyrugowanie"
  ]
  node [
    id 1228
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 1229
    label "urzeczywistnienie"
  ]
  node [
    id 1230
    label "emocja"
  ]
  node [
    id 1231
    label "completion"
  ]
  node [
    id 1232
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1233
    label "ziszczenie_si&#281;"
  ]
  node [
    id 1234
    label "realization"
  ]
  node [
    id 1235
    label "pe&#322;ny"
  ]
  node [
    id 1236
    label "realizowanie_si&#281;"
  ]
  node [
    id 1237
    label "enjoyment"
  ]
  node [
    id 1238
    label "gratyfikacja"
  ]
  node [
    id 1239
    label "pocz&#261;tki"
  ]
  node [
    id 1240
    label "ukra&#347;&#263;"
  ]
  node [
    id 1241
    label "ukradzenie"
  ]
  node [
    id 1242
    label "model"
  ]
  node [
    id 1243
    label "tryb"
  ]
  node [
    id 1244
    label "nature"
  ]
  node [
    id 1245
    label "invention"
  ]
  node [
    id 1246
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1247
    label "oduczenie"
  ]
  node [
    id 1248
    label "disavowal"
  ]
  node [
    id 1249
    label "cessation"
  ]
  node [
    id 1250
    label "przeczekanie"
  ]
  node [
    id 1251
    label "zaokr&#261;glenie"
  ]
  node [
    id 1252
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 1253
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1254
    label "retraction"
  ]
  node [
    id 1255
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 1256
    label "zerwanie"
  ]
  node [
    id 1257
    label "konsekwencja"
  ]
  node [
    id 1258
    label "przyjmowa&#263;_por&#243;d"
  ]
  node [
    id 1259
    label "przyj&#261;&#263;_por&#243;d"
  ]
  node [
    id 1260
    label "babka"
  ]
  node [
    id 1261
    label "piel&#281;gniarka"
  ]
  node [
    id 1262
    label "zabory"
  ]
  node [
    id 1263
    label "ci&#281;&#380;arna"
  ]
  node [
    id 1264
    label "asystentka"
  ]
  node [
    id 1265
    label "zlec"
  ]
  node [
    id 1266
    label "zlegni&#281;cie"
  ]
  node [
    id 1267
    label "konstytucyjnoprawny"
  ]
  node [
    id 1268
    label "prawniczo"
  ]
  node [
    id 1269
    label "prawnie"
  ]
  node [
    id 1270
    label "legalny"
  ]
  node [
    id 1271
    label "jurydyczny"
  ]
  node [
    id 1272
    label "urz&#281;dowo"
  ]
  node [
    id 1273
    label "prawniczy"
  ]
  node [
    id 1274
    label "legalnie"
  ]
  node [
    id 1275
    label "gajny"
  ]
  node [
    id 1276
    label "ustawa"
  ]
  node [
    id 1277
    label "story"
  ]
  node [
    id 1278
    label "modification"
  ]
  node [
    id 1279
    label "wyraz_pochodny"
  ]
  node [
    id 1280
    label "przer&#243;bka"
  ]
  node [
    id 1281
    label "przystosowanie"
  ]
  node [
    id 1282
    label "Karta_Nauczyciela"
  ]
  node [
    id 1283
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 1284
    label "akt"
  ]
  node [
    id 1285
    label "przej&#347;&#263;"
  ]
  node [
    id 1286
    label "charter"
  ]
  node [
    id 1287
    label "shot"
  ]
  node [
    id 1288
    label "jednakowy"
  ]
  node [
    id 1289
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 1290
    label "ujednolicenie"
  ]
  node [
    id 1291
    label "jaki&#347;"
  ]
  node [
    id 1292
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 1293
    label "jednolicie"
  ]
  node [
    id 1294
    label "kieliszek"
  ]
  node [
    id 1295
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 1296
    label "w&#243;dka"
  ]
  node [
    id 1297
    label "szk&#322;o"
  ]
  node [
    id 1298
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1299
    label "naczynie"
  ]
  node [
    id 1300
    label "alkohol"
  ]
  node [
    id 1301
    label "sznaps"
  ]
  node [
    id 1302
    label "nap&#243;j"
  ]
  node [
    id 1303
    label "gorza&#322;ka"
  ]
  node [
    id 1304
    label "mohorycz"
  ]
  node [
    id 1305
    label "okre&#347;lony"
  ]
  node [
    id 1306
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1307
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 1308
    label "mundurowanie"
  ]
  node [
    id 1309
    label "zr&#243;wnanie"
  ]
  node [
    id 1310
    label "taki&#380;"
  ]
  node [
    id 1311
    label "mundurowa&#263;"
  ]
  node [
    id 1312
    label "jednakowo"
  ]
  node [
    id 1313
    label "zr&#243;wnywanie"
  ]
  node [
    id 1314
    label "identyczny"
  ]
  node [
    id 1315
    label "z&#322;o&#380;ony"
  ]
  node [
    id 1316
    label "przyzwoity"
  ]
  node [
    id 1317
    label "ciekawy"
  ]
  node [
    id 1318
    label "jako&#347;"
  ]
  node [
    id 1319
    label "jako_tako"
  ]
  node [
    id 1320
    label "niez&#322;y"
  ]
  node [
    id 1321
    label "dziwny"
  ]
  node [
    id 1322
    label "g&#322;&#281;bszy"
  ]
  node [
    id 1323
    label "drink"
  ]
  node [
    id 1324
    label "jednolity"
  ]
  node [
    id 1325
    label "intencja"
  ]
  node [
    id 1326
    label "plan"
  ]
  node [
    id 1327
    label "device"
  ]
  node [
    id 1328
    label "program_u&#380;ytkowy"
  ]
  node [
    id 1329
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 1330
    label "agreement"
  ]
  node [
    id 1331
    label "thinking"
  ]
  node [
    id 1332
    label "rysunek"
  ]
  node [
    id 1333
    label "miejsce_pracy"
  ]
  node [
    id 1334
    label "przestrze&#324;"
  ]
  node [
    id 1335
    label "obraz"
  ]
  node [
    id 1336
    label "dekoracja"
  ]
  node [
    id 1337
    label "perspektywa"
  ]
  node [
    id 1338
    label "ekscerpcja"
  ]
  node [
    id 1339
    label "materia&#322;"
  ]
  node [
    id 1340
    label "operat"
  ]
  node [
    id 1341
    label "kosztorys"
  ]
  node [
    id 1342
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1343
    label "consist"
  ]
  node [
    id 1344
    label "ufa&#263;"
  ]
  node [
    id 1345
    label "trust"
  ]
  node [
    id 1346
    label "wierza&#263;"
  ]
  node [
    id 1347
    label "powierzy&#263;"
  ]
  node [
    id 1348
    label "czu&#263;"
  ]
  node [
    id 1349
    label "chowa&#263;"
  ]
  node [
    id 1350
    label "monopol"
  ]
  node [
    id 1351
    label "kilometr_kwadratowy"
  ]
  node [
    id 1352
    label "centymetr_kwadratowy"
  ]
  node [
    id 1353
    label "dekametr"
  ]
  node [
    id 1354
    label "gigametr"
  ]
  node [
    id 1355
    label "plon"
  ]
  node [
    id 1356
    label "meter"
  ]
  node [
    id 1357
    label "miara"
  ]
  node [
    id 1358
    label "uk&#322;ad_SI"
  ]
  node [
    id 1359
    label "wiersz"
  ]
  node [
    id 1360
    label "jednostka_metryczna"
  ]
  node [
    id 1361
    label "metrum"
  ]
  node [
    id 1362
    label "decymetr"
  ]
  node [
    id 1363
    label "megabyte"
  ]
  node [
    id 1364
    label "literaturoznawstwo"
  ]
  node [
    id 1365
    label "jednostka_powierzchni"
  ]
  node [
    id 1366
    label "jednostka_masy"
  ]
  node [
    id 1367
    label "proportion"
  ]
  node [
    id 1368
    label "wielko&#347;&#263;"
  ]
  node [
    id 1369
    label "continence"
  ]
  node [
    id 1370
    label "supremum"
  ]
  node [
    id 1371
    label "cecha"
  ]
  node [
    id 1372
    label "skala"
  ]
  node [
    id 1373
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1374
    label "przeliczy&#263;"
  ]
  node [
    id 1375
    label "matematyka"
  ]
  node [
    id 1376
    label "rzut"
  ]
  node [
    id 1377
    label "odwiedziny"
  ]
  node [
    id 1378
    label "liczba"
  ]
  node [
    id 1379
    label "granica"
  ]
  node [
    id 1380
    label "zakres"
  ]
  node [
    id 1381
    label "warunek_lokalowy"
  ]
  node [
    id 1382
    label "przeliczanie"
  ]
  node [
    id 1383
    label "dymensja"
  ]
  node [
    id 1384
    label "funkcja"
  ]
  node [
    id 1385
    label "przelicza&#263;"
  ]
  node [
    id 1386
    label "infimum"
  ]
  node [
    id 1387
    label "przeliczenie"
  ]
  node [
    id 1388
    label "rytm"
  ]
  node [
    id 1389
    label "rytmika"
  ]
  node [
    id 1390
    label "centymetr"
  ]
  node [
    id 1391
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1392
    label "hektometr"
  ]
  node [
    id 1393
    label "return"
  ]
  node [
    id 1394
    label "produkcja"
  ]
  node [
    id 1395
    label "naturalia"
  ]
  node [
    id 1396
    label "strofoida"
  ]
  node [
    id 1397
    label "figura_stylistyczna"
  ]
  node [
    id 1398
    label "podmiot_liryczny"
  ]
  node [
    id 1399
    label "cezura"
  ]
  node [
    id 1400
    label "zwrotka"
  ]
  node [
    id 1401
    label "fragment"
  ]
  node [
    id 1402
    label "refren"
  ]
  node [
    id 1403
    label "tekst"
  ]
  node [
    id 1404
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 1405
    label "nauka_humanistyczna"
  ]
  node [
    id 1406
    label "teoria_literatury"
  ]
  node [
    id 1407
    label "historia_literatury"
  ]
  node [
    id 1408
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 1409
    label "komparatystyka"
  ]
  node [
    id 1410
    label "literature"
  ]
  node [
    id 1411
    label "stylistyka"
  ]
  node [
    id 1412
    label "krytyka_literacka"
  ]
  node [
    id 1413
    label "pojednawstwo"
  ]
  node [
    id 1414
    label "komunikacja"
  ]
  node [
    id 1415
    label "trudny"
  ]
  node [
    id 1416
    label "hard"
  ]
  node [
    id 1417
    label "k&#322;opotliwy"
  ]
  node [
    id 1418
    label "skomplikowany"
  ]
  node [
    id 1419
    label "ci&#281;&#380;ko"
  ]
  node [
    id 1420
    label "wymagaj&#261;cy"
  ]
  node [
    id 1421
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1422
    label "equal"
  ]
  node [
    id 1423
    label "trwa&#263;"
  ]
  node [
    id 1424
    label "chodzi&#263;"
  ]
  node [
    id 1425
    label "si&#281;ga&#263;"
  ]
  node [
    id 1426
    label "stan"
  ]
  node [
    id 1427
    label "obecno&#347;&#263;"
  ]
  node [
    id 1428
    label "stand"
  ]
  node [
    id 1429
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1430
    label "uczestniczy&#263;"
  ]
  node [
    id 1431
    label "participate"
  ]
  node [
    id 1432
    label "istnie&#263;"
  ]
  node [
    id 1433
    label "pozostawa&#263;"
  ]
  node [
    id 1434
    label "zostawa&#263;"
  ]
  node [
    id 1435
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1436
    label "adhere"
  ]
  node [
    id 1437
    label "compass"
  ]
  node [
    id 1438
    label "korzysta&#263;"
  ]
  node [
    id 1439
    label "appreciation"
  ]
  node [
    id 1440
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1441
    label "dociera&#263;"
  ]
  node [
    id 1442
    label "get"
  ]
  node [
    id 1443
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1444
    label "mierzy&#263;"
  ]
  node [
    id 1445
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1446
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1447
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1448
    label "exsert"
  ]
  node [
    id 1449
    label "being"
  ]
  node [
    id 1450
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1451
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1452
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1453
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1454
    label "run"
  ]
  node [
    id 1455
    label "bangla&#263;"
  ]
  node [
    id 1456
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1457
    label "przebiega&#263;"
  ]
  node [
    id 1458
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1459
    label "proceed"
  ]
  node [
    id 1460
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1461
    label "carry"
  ]
  node [
    id 1462
    label "bywa&#263;"
  ]
  node [
    id 1463
    label "dziama&#263;"
  ]
  node [
    id 1464
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1465
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1466
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1467
    label "str&#243;j"
  ]
  node [
    id 1468
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1469
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1470
    label "krok"
  ]
  node [
    id 1471
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1472
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1473
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1474
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1475
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1476
    label "continue"
  ]
  node [
    id 1477
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1478
    label "Ohio"
  ]
  node [
    id 1479
    label "wci&#281;cie"
  ]
  node [
    id 1480
    label "Nowy_York"
  ]
  node [
    id 1481
    label "samopoczucie"
  ]
  node [
    id 1482
    label "Illinois"
  ]
  node [
    id 1483
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1484
    label "state"
  ]
  node [
    id 1485
    label "Jukatan"
  ]
  node [
    id 1486
    label "Kalifornia"
  ]
  node [
    id 1487
    label "Wirginia"
  ]
  node [
    id 1488
    label "wektor"
  ]
  node [
    id 1489
    label "Goa"
  ]
  node [
    id 1490
    label "Teksas"
  ]
  node [
    id 1491
    label "Waszyngton"
  ]
  node [
    id 1492
    label "Massachusetts"
  ]
  node [
    id 1493
    label "Alaska"
  ]
  node [
    id 1494
    label "Arakan"
  ]
  node [
    id 1495
    label "Hawaje"
  ]
  node [
    id 1496
    label "Maryland"
  ]
  node [
    id 1497
    label "Michigan"
  ]
  node [
    id 1498
    label "Arizona"
  ]
  node [
    id 1499
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1500
    label "Georgia"
  ]
  node [
    id 1501
    label "Pensylwania"
  ]
  node [
    id 1502
    label "shape"
  ]
  node [
    id 1503
    label "Luizjana"
  ]
  node [
    id 1504
    label "Nowy_Meksyk"
  ]
  node [
    id 1505
    label "Alabama"
  ]
  node [
    id 1506
    label "Kansas"
  ]
  node [
    id 1507
    label "Oregon"
  ]
  node [
    id 1508
    label "Oklahoma"
  ]
  node [
    id 1509
    label "Floryda"
  ]
  node [
    id 1510
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1511
    label "zatrudni&#263;"
  ]
  node [
    id 1512
    label "zgodzenie"
  ]
  node [
    id 1513
    label "zgadza&#263;"
  ]
  node [
    id 1514
    label "wzi&#261;&#263;"
  ]
  node [
    id 1515
    label "zatrudnia&#263;"
  ]
  node [
    id 1516
    label "zatrudnienie"
  ]
  node [
    id 1517
    label "zgadzanie"
  ]
  node [
    id 1518
    label "assent"
  ]
  node [
    id 1519
    label "&#347;rodek"
  ]
  node [
    id 1520
    label "darowizna"
  ]
  node [
    id 1521
    label "liga"
  ]
  node [
    id 1522
    label "doch&#243;d"
  ]
  node [
    id 1523
    label "telefon_zaufania"
  ]
  node [
    id 1524
    label "property"
  ]
  node [
    id 1525
    label "uwaga"
  ]
  node [
    id 1526
    label "punkt_widzenia"
  ]
  node [
    id 1527
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1528
    label "subject"
  ]
  node [
    id 1529
    label "czynnik"
  ]
  node [
    id 1530
    label "matuszka"
  ]
  node [
    id 1531
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1532
    label "geneza"
  ]
  node [
    id 1533
    label "poci&#261;ganie"
  ]
  node [
    id 1534
    label "sk&#322;adnik"
  ]
  node [
    id 1535
    label "warunki"
  ]
  node [
    id 1536
    label "sytuacja"
  ]
  node [
    id 1537
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1538
    label "nagana"
  ]
  node [
    id 1539
    label "upomnienie"
  ]
  node [
    id 1540
    label "dzienniczek"
  ]
  node [
    id 1541
    label "gossip"
  ]
  node [
    id 1542
    label "odpowiednio"
  ]
  node [
    id 1543
    label "dobroczynnie"
  ]
  node [
    id 1544
    label "moralnie"
  ]
  node [
    id 1545
    label "korzystnie"
  ]
  node [
    id 1546
    label "pozytywnie"
  ]
  node [
    id 1547
    label "lepiej"
  ]
  node [
    id 1548
    label "wiele"
  ]
  node [
    id 1549
    label "skutecznie"
  ]
  node [
    id 1550
    label "pomy&#347;lnie"
  ]
  node [
    id 1551
    label "nale&#380;nie"
  ]
  node [
    id 1552
    label "stosowny"
  ]
  node [
    id 1553
    label "nale&#380;ycie"
  ]
  node [
    id 1554
    label "prawdziwie"
  ]
  node [
    id 1555
    label "auspiciously"
  ]
  node [
    id 1556
    label "pomy&#347;lny"
  ]
  node [
    id 1557
    label "moralny"
  ]
  node [
    id 1558
    label "etyczny"
  ]
  node [
    id 1559
    label "skuteczny"
  ]
  node [
    id 1560
    label "wiela"
  ]
  node [
    id 1561
    label "utylitarnie"
  ]
  node [
    id 1562
    label "korzystny"
  ]
  node [
    id 1563
    label "beneficially"
  ]
  node [
    id 1564
    label "przyjemnie"
  ]
  node [
    id 1565
    label "pozytywny"
  ]
  node [
    id 1566
    label "ontologicznie"
  ]
  node [
    id 1567
    label "dodatni"
  ]
  node [
    id 1568
    label "dobroczynny"
  ]
  node [
    id 1569
    label "czw&#243;rka"
  ]
  node [
    id 1570
    label "spokojny"
  ]
  node [
    id 1571
    label "&#347;mieszny"
  ]
  node [
    id 1572
    label "grzeczny"
  ]
  node [
    id 1573
    label "powitanie"
  ]
  node [
    id 1574
    label "ca&#322;y"
  ]
  node [
    id 1575
    label "drogi"
  ]
  node [
    id 1576
    label "pos&#322;uszny"
  ]
  node [
    id 1577
    label "philanthropically"
  ]
  node [
    id 1578
    label "spo&#322;ecznie"
  ]
  node [
    id 1579
    label "cognizance"
  ]
  node [
    id 1580
    label "j&#261;dro"
  ]
  node [
    id 1581
    label "systemik"
  ]
  node [
    id 1582
    label "rozprz&#261;c"
  ]
  node [
    id 1583
    label "oprogramowanie"
  ]
  node [
    id 1584
    label "systemat"
  ]
  node [
    id 1585
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1586
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1587
    label "usenet"
  ]
  node [
    id 1588
    label "porz&#261;dek"
  ]
  node [
    id 1589
    label "przyn&#281;ta"
  ]
  node [
    id 1590
    label "p&#322;&#243;d"
  ]
  node [
    id 1591
    label "net"
  ]
  node [
    id 1592
    label "w&#281;dkarstwo"
  ]
  node [
    id 1593
    label "eratem"
  ]
  node [
    id 1594
    label "oddzia&#322;"
  ]
  node [
    id 1595
    label "doktryna"
  ]
  node [
    id 1596
    label "pulpit"
  ]
  node [
    id 1597
    label "konstelacja"
  ]
  node [
    id 1598
    label "jednostka_geologiczna"
  ]
  node [
    id 1599
    label "o&#347;"
  ]
  node [
    id 1600
    label "podsystem"
  ]
  node [
    id 1601
    label "metoda"
  ]
  node [
    id 1602
    label "ryba"
  ]
  node [
    id 1603
    label "Leopard"
  ]
  node [
    id 1604
    label "Android"
  ]
  node [
    id 1605
    label "cybernetyk"
  ]
  node [
    id 1606
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1607
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1608
    label "method"
  ]
  node [
    id 1609
    label "sk&#322;ad"
  ]
  node [
    id 1610
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1611
    label "pot&#281;ga"
  ]
  node [
    id 1612
    label "documentation"
  ]
  node [
    id 1613
    label "column"
  ]
  node [
    id 1614
    label "zasadzenie"
  ]
  node [
    id 1615
    label "punkt_odniesienia"
  ]
  node [
    id 1616
    label "zasadzi&#263;"
  ]
  node [
    id 1617
    label "bok"
  ]
  node [
    id 1618
    label "d&#243;&#322;"
  ]
  node [
    id 1619
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1620
    label "background"
  ]
  node [
    id 1621
    label "podstawowy"
  ]
  node [
    id 1622
    label "strategia"
  ]
  node [
    id 1623
    label "&#347;ciana"
  ]
  node [
    id 1624
    label "relacja"
  ]
  node [
    id 1625
    label "uk&#322;ad"
  ]
  node [
    id 1626
    label "styl_architektoniczny"
  ]
  node [
    id 1627
    label "skumanie"
  ]
  node [
    id 1628
    label "orientacja"
  ]
  node [
    id 1629
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1630
    label "clasp"
  ]
  node [
    id 1631
    label "przem&#243;wienie"
  ]
  node [
    id 1632
    label "zorientowanie"
  ]
  node [
    id 1633
    label "series"
  ]
  node [
    id 1634
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1635
    label "uprawianie"
  ]
  node [
    id 1636
    label "praca_rolnicza"
  ]
  node [
    id 1637
    label "collection"
  ]
  node [
    id 1638
    label "dane"
  ]
  node [
    id 1639
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1640
    label "pakiet_klimatyczny"
  ]
  node [
    id 1641
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1642
    label "sum"
  ]
  node [
    id 1643
    label "gathering"
  ]
  node [
    id 1644
    label "album"
  ]
  node [
    id 1645
    label "system_komputerowy"
  ]
  node [
    id 1646
    label "mechanika"
  ]
  node [
    id 1647
    label "konstrukcja"
  ]
  node [
    id 1648
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1649
    label "moczownik"
  ]
  node [
    id 1650
    label "embryo"
  ]
  node [
    id 1651
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1652
    label "zarodek"
  ]
  node [
    id 1653
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1654
    label "latawiec"
  ]
  node [
    id 1655
    label "reengineering"
  ]
  node [
    id 1656
    label "program"
  ]
  node [
    id 1657
    label "integer"
  ]
  node [
    id 1658
    label "zlewanie_si&#281;"
  ]
  node [
    id 1659
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1660
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1661
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1662
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 1663
    label "grupa_dyskusyjna"
  ]
  node [
    id 1664
    label "doctrine"
  ]
  node [
    id 1665
    label "urozmaicenie"
  ]
  node [
    id 1666
    label "pu&#322;apka"
  ]
  node [
    id 1667
    label "pon&#281;ta"
  ]
  node [
    id 1668
    label "wabik"
  ]
  node [
    id 1669
    label "sport"
  ]
  node [
    id 1670
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 1671
    label "kr&#281;gowiec"
  ]
  node [
    id 1672
    label "doniczkowiec"
  ]
  node [
    id 1673
    label "mi&#281;so"
  ]
  node [
    id 1674
    label "patroszy&#263;"
  ]
  node [
    id 1675
    label "rakowato&#347;&#263;"
  ]
  node [
    id 1676
    label "ryby"
  ]
  node [
    id 1677
    label "fish"
  ]
  node [
    id 1678
    label "linia_boczna"
  ]
  node [
    id 1679
    label "tar&#322;o"
  ]
  node [
    id 1680
    label "wyrostek_filtracyjny"
  ]
  node [
    id 1681
    label "m&#281;tnooki"
  ]
  node [
    id 1682
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 1683
    label "pokrywa_skrzelowa"
  ]
  node [
    id 1684
    label "ikra"
  ]
  node [
    id 1685
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 1686
    label "szczelina_skrzelowa"
  ]
  node [
    id 1687
    label "blat"
  ]
  node [
    id 1688
    label "interfejs"
  ]
  node [
    id 1689
    label "okno"
  ]
  node [
    id 1690
    label "obszar"
  ]
  node [
    id 1691
    label "ikona"
  ]
  node [
    id 1692
    label "system_operacyjny"
  ]
  node [
    id 1693
    label "mebel"
  ]
  node [
    id 1694
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1695
    label "relaxation"
  ]
  node [
    id 1696
    label "os&#322;abienie"
  ]
  node [
    id 1697
    label "oswobodzenie"
  ]
  node [
    id 1698
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1699
    label "zdezorganizowanie"
  ]
  node [
    id 1700
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1701
    label "tajemnica"
  ]
  node [
    id 1702
    label "pochowanie"
  ]
  node [
    id 1703
    label "zdyscyplinowanie"
  ]
  node [
    id 1704
    label "post&#261;pienie"
  ]
  node [
    id 1705
    label "post"
  ]
  node [
    id 1706
    label "bearing"
  ]
  node [
    id 1707
    label "behawior"
  ]
  node [
    id 1708
    label "observation"
  ]
  node [
    id 1709
    label "dieta"
  ]
  node [
    id 1710
    label "podtrzymanie"
  ]
  node [
    id 1711
    label "etolog"
  ]
  node [
    id 1712
    label "przechowanie"
  ]
  node [
    id 1713
    label "oswobodzi&#263;"
  ]
  node [
    id 1714
    label "os&#322;abi&#263;"
  ]
  node [
    id 1715
    label "disengage"
  ]
  node [
    id 1716
    label "zdezorganizowa&#263;"
  ]
  node [
    id 1717
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1718
    label "naukowiec"
  ]
  node [
    id 1719
    label "provider"
  ]
  node [
    id 1720
    label "b&#322;&#261;d"
  ]
  node [
    id 1721
    label "hipertekst"
  ]
  node [
    id 1722
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1723
    label "mem"
  ]
  node [
    id 1724
    label "gra_sieciowa"
  ]
  node [
    id 1725
    label "grooming"
  ]
  node [
    id 1726
    label "media"
  ]
  node [
    id 1727
    label "biznes_elektroniczny"
  ]
  node [
    id 1728
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1729
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1730
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1731
    label "netbook"
  ]
  node [
    id 1732
    label "e-hazard"
  ]
  node [
    id 1733
    label "podcast"
  ]
  node [
    id 1734
    label "strona"
  ]
  node [
    id 1735
    label "prezenter"
  ]
  node [
    id 1736
    label "mildew"
  ]
  node [
    id 1737
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1738
    label "motif"
  ]
  node [
    id 1739
    label "pozowanie"
  ]
  node [
    id 1740
    label "ideal"
  ]
  node [
    id 1741
    label "matryca"
  ]
  node [
    id 1742
    label "adaptation"
  ]
  node [
    id 1743
    label "ruch"
  ]
  node [
    id 1744
    label "pozowa&#263;"
  ]
  node [
    id 1745
    label "imitacja"
  ]
  node [
    id 1746
    label "orygina&#322;"
  ]
  node [
    id 1747
    label "miniatura"
  ]
  node [
    id 1748
    label "zesp&#243;&#322;"
  ]
  node [
    id 1749
    label "podejrzany"
  ]
  node [
    id 1750
    label "s&#261;downictwo"
  ]
  node [
    id 1751
    label "court"
  ]
  node [
    id 1752
    label "forum"
  ]
  node [
    id 1753
    label "bronienie"
  ]
  node [
    id 1754
    label "oskar&#380;yciel"
  ]
  node [
    id 1755
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1756
    label "skazany"
  ]
  node [
    id 1757
    label "my&#347;l"
  ]
  node [
    id 1758
    label "pods&#261;dny"
  ]
  node [
    id 1759
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1760
    label "antylogizm"
  ]
  node [
    id 1761
    label "konektyw"
  ]
  node [
    id 1762
    label "&#347;wiadek"
  ]
  node [
    id 1763
    label "procesowicz"
  ]
  node [
    id 1764
    label "lias"
  ]
  node [
    id 1765
    label "dzia&#322;"
  ]
  node [
    id 1766
    label "pi&#281;tro"
  ]
  node [
    id 1767
    label "filia"
  ]
  node [
    id 1768
    label "malm"
  ]
  node [
    id 1769
    label "dogger"
  ]
  node [
    id 1770
    label "bank"
  ]
  node [
    id 1771
    label "formacja"
  ]
  node [
    id 1772
    label "ajencja"
  ]
  node [
    id 1773
    label "siedziba"
  ]
  node [
    id 1774
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1775
    label "agencja"
  ]
  node [
    id 1776
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1777
    label "szpital"
  ]
  node [
    id 1778
    label "algebra_liniowa"
  ]
  node [
    id 1779
    label "macierz_j&#261;drowa"
  ]
  node [
    id 1780
    label "atom"
  ]
  node [
    id 1781
    label "nukleon"
  ]
  node [
    id 1782
    label "kariokineza"
  ]
  node [
    id 1783
    label "core"
  ]
  node [
    id 1784
    label "chemia_j&#261;drowa"
  ]
  node [
    id 1785
    label "anorchizm"
  ]
  node [
    id 1786
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 1787
    label "nasieniak"
  ]
  node [
    id 1788
    label "wn&#281;trostwo"
  ]
  node [
    id 1789
    label "ziarno"
  ]
  node [
    id 1790
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 1791
    label "j&#261;derko"
  ]
  node [
    id 1792
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 1793
    label "jajo"
  ]
  node [
    id 1794
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1795
    label "chromosom"
  ]
  node [
    id 1796
    label "organellum"
  ]
  node [
    id 1797
    label "moszna"
  ]
  node [
    id 1798
    label "przeciwobraz"
  ]
  node [
    id 1799
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 1800
    label "protoplazma"
  ]
  node [
    id 1801
    label "znaczenie"
  ]
  node [
    id 1802
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 1803
    label "nukleosynteza"
  ]
  node [
    id 1804
    label "subsystem"
  ]
  node [
    id 1805
    label "ko&#322;o"
  ]
  node [
    id 1806
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 1807
    label "suport"
  ]
  node [
    id 1808
    label "prosta"
  ]
  node [
    id 1809
    label "o&#347;rodek"
  ]
  node [
    id 1810
    label "eonotem"
  ]
  node [
    id 1811
    label "constellation"
  ]
  node [
    id 1812
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 1813
    label "Ptak_Rajski"
  ]
  node [
    id 1814
    label "W&#281;&#380;ownik"
  ]
  node [
    id 1815
    label "Panna"
  ]
  node [
    id 1816
    label "W&#261;&#380;"
  ]
  node [
    id 1817
    label "blokada"
  ]
  node [
    id 1818
    label "hurtownia"
  ]
  node [
    id 1819
    label "pomieszczenie"
  ]
  node [
    id 1820
    label "pole"
  ]
  node [
    id 1821
    label "pas"
  ]
  node [
    id 1822
    label "basic"
  ]
  node [
    id 1823
    label "sklep"
  ]
  node [
    id 1824
    label "obr&#243;bka"
  ]
  node [
    id 1825
    label "constitution"
  ]
  node [
    id 1826
    label "fabryka"
  ]
  node [
    id 1827
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1828
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1829
    label "syf"
  ]
  node [
    id 1830
    label "rank_and_file"
  ]
  node [
    id 1831
    label "tabulacja"
  ]
  node [
    id 1832
    label "czyj&#347;"
  ]
  node [
    id 1833
    label "prywatny"
  ]
  node [
    id 1834
    label "plac"
  ]
  node [
    id 1835
    label "location"
  ]
  node [
    id 1836
    label "status"
  ]
  node [
    id 1837
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1838
    label "chwila"
  ]
  node [
    id 1839
    label "cia&#322;o"
  ]
  node [
    id 1840
    label "charakterystyka"
  ]
  node [
    id 1841
    label "m&#322;ot"
  ]
  node [
    id 1842
    label "znak"
  ]
  node [
    id 1843
    label "drzewo"
  ]
  node [
    id 1844
    label "attribute"
  ]
  node [
    id 1845
    label "marka"
  ]
  node [
    id 1846
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1847
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1848
    label "najem"
  ]
  node [
    id 1849
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1850
    label "zak&#322;ad"
  ]
  node [
    id 1851
    label "stosunek_pracy"
  ]
  node [
    id 1852
    label "benedykty&#324;ski"
  ]
  node [
    id 1853
    label "poda&#380;_pracy"
  ]
  node [
    id 1854
    label "pracowanie"
  ]
  node [
    id 1855
    label "tyrka"
  ]
  node [
    id 1856
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1857
    label "zaw&#243;d"
  ]
  node [
    id 1858
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1859
    label "tynkarski"
  ]
  node [
    id 1860
    label "pracowa&#263;"
  ]
  node [
    id 1861
    label "czynnik_produkcji"
  ]
  node [
    id 1862
    label "kierownictwo"
  ]
  node [
    id 1863
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1864
    label "rozdzielanie"
  ]
  node [
    id 1865
    label "bezbrze&#380;e"
  ]
  node [
    id 1866
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1867
    label "niezmierzony"
  ]
  node [
    id 1868
    label "przedzielenie"
  ]
  node [
    id 1869
    label "nielito&#347;ciwy"
  ]
  node [
    id 1870
    label "rozdziela&#263;"
  ]
  node [
    id 1871
    label "oktant"
  ]
  node [
    id 1872
    label "przedzieli&#263;"
  ]
  node [
    id 1873
    label "przestw&#243;r"
  ]
  node [
    id 1874
    label "condition"
  ]
  node [
    id 1875
    label "awansowa&#263;"
  ]
  node [
    id 1876
    label "awans"
  ]
  node [
    id 1877
    label "podmiotowo"
  ]
  node [
    id 1878
    label "awansowanie"
  ]
  node [
    id 1879
    label "circumference"
  ]
  node [
    id 1880
    label "leksem"
  ]
  node [
    id 1881
    label "cyrkumferencja"
  ]
  node [
    id 1882
    label "ekshumowanie"
  ]
  node [
    id 1883
    label "jednostka_organizacyjna"
  ]
  node [
    id 1884
    label "p&#322;aszczyzna"
  ]
  node [
    id 1885
    label "odwadnia&#263;"
  ]
  node [
    id 1886
    label "zabalsamowanie"
  ]
  node [
    id 1887
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1888
    label "odwodni&#263;"
  ]
  node [
    id 1889
    label "sk&#243;ra"
  ]
  node [
    id 1890
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1891
    label "staw"
  ]
  node [
    id 1892
    label "ow&#322;osienie"
  ]
  node [
    id 1893
    label "zabalsamowa&#263;"
  ]
  node [
    id 1894
    label "Izba_Konsyliarska"
  ]
  node [
    id 1895
    label "unerwienie"
  ]
  node [
    id 1896
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1897
    label "kremacja"
  ]
  node [
    id 1898
    label "biorytm"
  ]
  node [
    id 1899
    label "sekcja"
  ]
  node [
    id 1900
    label "otworzy&#263;"
  ]
  node [
    id 1901
    label "otwiera&#263;"
  ]
  node [
    id 1902
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1903
    label "otworzenie"
  ]
  node [
    id 1904
    label "otwieranie"
  ]
  node [
    id 1905
    label "szkielet"
  ]
  node [
    id 1906
    label "ty&#322;"
  ]
  node [
    id 1907
    label "tanatoplastyk"
  ]
  node [
    id 1908
    label "odwadnianie"
  ]
  node [
    id 1909
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1910
    label "odwodnienie"
  ]
  node [
    id 1911
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1912
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1913
    label "pochowa&#263;"
  ]
  node [
    id 1914
    label "tanatoplastyka"
  ]
  node [
    id 1915
    label "balsamowa&#263;"
  ]
  node [
    id 1916
    label "nieumar&#322;y"
  ]
  node [
    id 1917
    label "temperatura"
  ]
  node [
    id 1918
    label "balsamowanie"
  ]
  node [
    id 1919
    label "ekshumowa&#263;"
  ]
  node [
    id 1920
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1921
    label "prz&#243;d"
  ]
  node [
    id 1922
    label "pogrzeb"
  ]
  node [
    id 1923
    label "&#321;ubianka"
  ]
  node [
    id 1924
    label "area"
  ]
  node [
    id 1925
    label "Majdan"
  ]
  node [
    id 1926
    label "pole_bitwy"
  ]
  node [
    id 1927
    label "stoisko"
  ]
  node [
    id 1928
    label "pierzeja"
  ]
  node [
    id 1929
    label "obiekt_handlowy"
  ]
  node [
    id 1930
    label "zgromadzenie"
  ]
  node [
    id 1931
    label "miasto"
  ]
  node [
    id 1932
    label "targowica"
  ]
  node [
    id 1933
    label "kram"
  ]
  node [
    id 1934
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1935
    label "rozumie&#263;"
  ]
  node [
    id 1936
    label "szczeka&#263;"
  ]
  node [
    id 1937
    label "rozmawia&#263;"
  ]
  node [
    id 1938
    label "m&#243;wi&#263;"
  ]
  node [
    id 1939
    label "modalno&#347;&#263;"
  ]
  node [
    id 1940
    label "z&#261;b"
  ]
  node [
    id 1941
    label "kategoria_gramatyczna"
  ]
  node [
    id 1942
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1943
    label "koniugacja"
  ]
  node [
    id 1944
    label "umiej&#281;tnie"
  ]
  node [
    id 1945
    label "kompetentnie"
  ]
  node [
    id 1946
    label "funkcjonalnie"
  ]
  node [
    id 1947
    label "szybko"
  ]
  node [
    id 1948
    label "sprawny"
  ]
  node [
    id 1949
    label "udanie"
  ]
  node [
    id 1950
    label "zdrowo"
  ]
  node [
    id 1951
    label "fachowo"
  ]
  node [
    id 1952
    label "kompetentny"
  ]
  node [
    id 1953
    label "solidnie"
  ]
  node [
    id 1954
    label "normalnie"
  ]
  node [
    id 1955
    label "rozs&#261;dnie"
  ]
  node [
    id 1956
    label "zdrowy"
  ]
  node [
    id 1957
    label "zachowanie_si&#281;"
  ]
  node [
    id 1958
    label "udany"
  ]
  node [
    id 1959
    label "maneuver"
  ]
  node [
    id 1960
    label "funkcjonalny"
  ]
  node [
    id 1961
    label "praktycznie"
  ]
  node [
    id 1962
    label "umiej&#281;tny"
  ]
  node [
    id 1963
    label "szybki"
  ]
  node [
    id 1964
    label "letki"
  ]
  node [
    id 1965
    label "dzia&#322;alny"
  ]
  node [
    id 1966
    label "quickest"
  ]
  node [
    id 1967
    label "szybciochem"
  ]
  node [
    id 1968
    label "prosto"
  ]
  node [
    id 1969
    label "quicker"
  ]
  node [
    id 1970
    label "szybciej"
  ]
  node [
    id 1971
    label "promptly"
  ]
  node [
    id 1972
    label "bezpo&#347;rednio"
  ]
  node [
    id 1973
    label "dynamicznie"
  ]
  node [
    id 1974
    label "suffice"
  ]
  node [
    id 1975
    label "stan&#261;&#263;"
  ]
  node [
    id 1976
    label "zaspokoi&#263;"
  ]
  node [
    id 1977
    label "dosta&#263;"
  ]
  node [
    id 1978
    label "satisfy"
  ]
  node [
    id 1979
    label "p&#243;j&#347;&#263;_do_&#322;&#243;&#380;ka"
  ]
  node [
    id 1980
    label "napoi&#263;_si&#281;"
  ]
  node [
    id 1981
    label "zadowoli&#263;"
  ]
  node [
    id 1982
    label "serve"
  ]
  node [
    id 1983
    label "zapanowa&#263;"
  ]
  node [
    id 1984
    label "develop"
  ]
  node [
    id 1985
    label "schorzenie"
  ]
  node [
    id 1986
    label "nabawienie_si&#281;"
  ]
  node [
    id 1987
    label "obskoczy&#263;"
  ]
  node [
    id 1988
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 1989
    label "catch"
  ]
  node [
    id 1990
    label "zwiastun"
  ]
  node [
    id 1991
    label "doczeka&#263;"
  ]
  node [
    id 1992
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1993
    label "kupi&#263;"
  ]
  node [
    id 1994
    label "wysta&#263;"
  ]
  node [
    id 1995
    label "naby&#263;"
  ]
  node [
    id 1996
    label "nabawianie_si&#281;"
  ]
  node [
    id 1997
    label "range"
  ]
  node [
    id 1998
    label "reserve"
  ]
  node [
    id 1999
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 2000
    label "originate"
  ]
  node [
    id 2001
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 2002
    label "zatrzyma&#263;_si&#281;"
  ]
  node [
    id 2003
    label "przyby&#263;"
  ]
  node [
    id 2004
    label "obj&#261;&#263;"
  ]
  node [
    id 2005
    label "unie&#347;&#263;_si&#281;"
  ]
  node [
    id 2006
    label "zmieni&#263;"
  ]
  node [
    id 2007
    label "przyj&#261;&#263;"
  ]
  node [
    id 2008
    label "zosta&#263;"
  ]
  node [
    id 2009
    label "przesta&#263;"
  ]
  node [
    id 2010
    label "peek"
  ]
  node [
    id 2011
    label "spojrze&#263;"
  ]
  node [
    id 2012
    label "zinterpretowa&#263;"
  ]
  node [
    id 2013
    label "spoziera&#263;"
  ]
  node [
    id 2014
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 2015
    label "popatrze&#263;"
  ]
  node [
    id 2016
    label "pojrze&#263;"
  ]
  node [
    id 2017
    label "see"
  ]
  node [
    id 2018
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 2019
    label "norma_prawna"
  ]
  node [
    id 2020
    label "przedawnienie_si&#281;"
  ]
  node [
    id 2021
    label "przedawnianie_si&#281;"
  ]
  node [
    id 2022
    label "porada"
  ]
  node [
    id 2023
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 2024
    label "regulation"
  ]
  node [
    id 2025
    label "recepta"
  ]
  node [
    id 2026
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 2027
    label "wskaz&#243;wka"
  ]
  node [
    id 2028
    label "zlecenie"
  ]
  node [
    id 2029
    label "receipt"
  ]
  node [
    id 2030
    label "receptariusz"
  ]
  node [
    id 2031
    label "obwiniony"
  ]
  node [
    id 2032
    label "r&#281;kopis"
  ]
  node [
    id 2033
    label "kodeks_pracy"
  ]
  node [
    id 2034
    label "kodeks_morski"
  ]
  node [
    id 2035
    label "Justynian"
  ]
  node [
    id 2036
    label "code"
  ]
  node [
    id 2037
    label "kodeks_drogowy"
  ]
  node [
    id 2038
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 2039
    label "kodeks_rodzinny"
  ]
  node [
    id 2040
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 2041
    label "kodeks_cywilny"
  ]
  node [
    id 2042
    label "kodeks_karny"
  ]
  node [
    id 2043
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 2044
    label "umocowa&#263;"
  ]
  node [
    id 2045
    label "procesualistyka"
  ]
  node [
    id 2046
    label "kryminalistyka"
  ]
  node [
    id 2047
    label "kierunek"
  ]
  node [
    id 2048
    label "normatywizm"
  ]
  node [
    id 2049
    label "jurisprudence"
  ]
  node [
    id 2050
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 2051
    label "prawo_karne_procesowe"
  ]
  node [
    id 2052
    label "kazuistyka"
  ]
  node [
    id 2053
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 2054
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 2055
    label "kryminologia"
  ]
  node [
    id 2056
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 2057
    label "prawo_karne"
  ]
  node [
    id 2058
    label "legislacyjnie"
  ]
  node [
    id 2059
    label "cywilistyka"
  ]
  node [
    id 2060
    label "judykatura"
  ]
  node [
    id 2061
    label "kanonistyka"
  ]
  node [
    id 2062
    label "nauka_prawa"
  ]
  node [
    id 2063
    label "law"
  ]
  node [
    id 2064
    label "wykonawczy"
  ]
  node [
    id 2065
    label "cymelium"
  ]
  node [
    id 2066
    label "campaign"
  ]
  node [
    id 2067
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 2068
    label "fashion"
  ]
  node [
    id 2069
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 2070
    label "zmierzanie"
  ]
  node [
    id 2071
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 2072
    label "kazanie"
  ]
  node [
    id 2073
    label "przebiec"
  ]
  node [
    id 2074
    label "charakter"
  ]
  node [
    id 2075
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 2076
    label "motyw"
  ]
  node [
    id 2077
    label "przebiegni&#281;cie"
  ]
  node [
    id 2078
    label "fabu&#322;a"
  ]
  node [
    id 2079
    label "dochodzenie"
  ]
  node [
    id 2080
    label "przychodzenie"
  ]
  node [
    id 2081
    label "wyprzedzenie"
  ]
  node [
    id 2082
    label "podchodzenie"
  ]
  node [
    id 2083
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 2084
    label "przyj&#347;cie"
  ]
  node [
    id 2085
    label "przemierzanie"
  ]
  node [
    id 2086
    label "udawanie_si&#281;"
  ]
  node [
    id 2087
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 2088
    label "odchodzenie"
  ]
  node [
    id 2089
    label "wyprzedzanie"
  ]
  node [
    id 2090
    label "spotykanie"
  ]
  node [
    id 2091
    label "podej&#347;cie"
  ]
  node [
    id 2092
    label "przemierzenie"
  ]
  node [
    id 2093
    label "wodzenie"
  ]
  node [
    id 2094
    label "fabrication"
  ]
  node [
    id 2095
    label "porobienie"
  ]
  node [
    id 2096
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 2097
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 2098
    label "creation"
  ]
  node [
    id 2099
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 2100
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 2101
    label "tentegowanie"
  ]
  node [
    id 2102
    label "activity"
  ]
  node [
    id 2103
    label "bezproblemowy"
  ]
  node [
    id 2104
    label "rozumowanie"
  ]
  node [
    id 2105
    label "opracowanie"
  ]
  node [
    id 2106
    label "cytat"
  ]
  node [
    id 2107
    label "obja&#347;nienie"
  ]
  node [
    id 2108
    label "s&#261;dzenie"
  ]
  node [
    id 2109
    label "fakt"
  ]
  node [
    id 2110
    label "wnioskowanie"
  ]
  node [
    id 2111
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 2112
    label "nakazywanie"
  ]
  node [
    id 2113
    label "krytyka"
  ]
  node [
    id 2114
    label "wyg&#322;oszenie"
  ]
  node [
    id 2115
    label "zmuszanie"
  ]
  node [
    id 2116
    label "wyg&#322;aszanie"
  ]
  node [
    id 2117
    label "nakazanie"
  ]
  node [
    id 2118
    label "wymaganie"
  ]
  node [
    id 2119
    label "msza"
  ]
  node [
    id 2120
    label "zmuszenie"
  ]
  node [
    id 2121
    label "nauka"
  ]
  node [
    id 2122
    label "sermon"
  ]
  node [
    id 2123
    label "command"
  ]
  node [
    id 2124
    label "order"
  ]
  node [
    id 2125
    label "administracyjnie"
  ]
  node [
    id 2126
    label "administrative"
  ]
  node [
    id 2127
    label "urz&#281;dowy"
  ]
  node [
    id 2128
    label "oficjalny"
  ]
  node [
    id 2129
    label "formalny"
  ]
  node [
    id 2130
    label "nieoficjalny"
  ]
  node [
    id 2131
    label "cywilnie"
  ]
  node [
    id 2132
    label "nieoficjalnie"
  ]
  node [
    id 2133
    label "nieformalny"
  ]
  node [
    id 2134
    label "pope&#322;nianie"
  ]
  node [
    id 2135
    label "stanowienie"
  ]
  node [
    id 2136
    label "zatrzymywanie"
  ]
  node [
    id 2137
    label "krycie"
  ]
  node [
    id 2138
    label "pies_my&#347;liwski"
  ]
  node [
    id 2139
    label "rozstrzyganie_si&#281;"
  ]
  node [
    id 2140
    label "decydowanie"
  ]
  node [
    id 2141
    label "polowanie"
  ]
  node [
    id 2142
    label "&#322;&#261;czenie"
  ]
  node [
    id 2143
    label "liquidation"
  ]
  node [
    id 2144
    label "committee"
  ]
  node [
    id 2145
    label "dorobek"
  ]
  node [
    id 2146
    label "kreacja"
  ]
  node [
    id 2147
    label "nast&#281;pnie"
  ]
  node [
    id 2148
    label "inny"
  ]
  node [
    id 2149
    label "nastopny"
  ]
  node [
    id 2150
    label "kolejno"
  ]
  node [
    id 2151
    label "kt&#243;ry&#347;"
  ]
  node [
    id 2152
    label "osobno"
  ]
  node [
    id 2153
    label "r&#243;&#380;ny"
  ]
  node [
    id 2154
    label "inszy"
  ]
  node [
    id 2155
    label "inaczej"
  ]
  node [
    id 2156
    label "dostarcza&#263;"
  ]
  node [
    id 2157
    label "oddawa&#263;"
  ]
  node [
    id 2158
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 2159
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 2160
    label "mark"
  ]
  node [
    id 2161
    label "przekazywa&#263;"
  ]
  node [
    id 2162
    label "sacrifice"
  ]
  node [
    id 2163
    label "odst&#281;powa&#263;"
  ]
  node [
    id 2164
    label "sprzedawa&#263;"
  ]
  node [
    id 2165
    label "reflect"
  ]
  node [
    id 2166
    label "surrender"
  ]
  node [
    id 2167
    label "deliver"
  ]
  node [
    id 2168
    label "umieszcza&#263;"
  ]
  node [
    id 2169
    label "render"
  ]
  node [
    id 2170
    label "blurt_out"
  ]
  node [
    id 2171
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 2172
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 2173
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 2174
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 2175
    label "relate"
  ]
  node [
    id 2176
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 2177
    label "kojarzy&#263;"
  ]
  node [
    id 2178
    label "d&#378;wi&#281;k"
  ]
  node [
    id 2179
    label "reszta"
  ]
  node [
    id 2180
    label "zapach"
  ]
  node [
    id 2181
    label "wydawnictwo"
  ]
  node [
    id 2182
    label "wiano"
  ]
  node [
    id 2183
    label "wprowadza&#263;"
  ]
  node [
    id 2184
    label "podawa&#263;"
  ]
  node [
    id 2185
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 2186
    label "ujawnia&#263;"
  ]
  node [
    id 2187
    label "placard"
  ]
  node [
    id 2188
    label "denuncjowa&#263;"
  ]
  node [
    id 2189
    label "panna_na_wydaniu"
  ]
  node [
    id 2190
    label "train"
  ]
  node [
    id 2191
    label "&#322;adowa&#263;"
  ]
  node [
    id 2192
    label "przeznacza&#263;"
  ]
  node [
    id 2193
    label "traktowa&#263;"
  ]
  node [
    id 2194
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 2195
    label "obiecywa&#263;"
  ]
  node [
    id 2196
    label "tender"
  ]
  node [
    id 2197
    label "rap"
  ]
  node [
    id 2198
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 2199
    label "t&#322;uc"
  ]
  node [
    id 2200
    label "wpiernicza&#263;"
  ]
  node [
    id 2201
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 2202
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 2203
    label "p&#322;aci&#263;"
  ]
  node [
    id 2204
    label "hold_out"
  ]
  node [
    id 2205
    label "nalewa&#263;"
  ]
  node [
    id 2206
    label "zezwala&#263;"
  ]
  node [
    id 2207
    label "organizowa&#263;"
  ]
  node [
    id 2208
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 2209
    label "czyni&#263;"
  ]
  node [
    id 2210
    label "stylizowa&#263;"
  ]
  node [
    id 2211
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 2212
    label "falowa&#263;"
  ]
  node [
    id 2213
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 2214
    label "peddle"
  ]
  node [
    id 2215
    label "wydala&#263;"
  ]
  node [
    id 2216
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 2217
    label "tentegowa&#263;"
  ]
  node [
    id 2218
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 2219
    label "urz&#261;dza&#263;"
  ]
  node [
    id 2220
    label "oszukiwa&#263;"
  ]
  node [
    id 2221
    label "ukazywa&#263;"
  ]
  node [
    id 2222
    label "przerabia&#263;"
  ]
  node [
    id 2223
    label "post&#281;powa&#263;"
  ]
  node [
    id 2224
    label "rynek"
  ]
  node [
    id 2225
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 2226
    label "wprawia&#263;"
  ]
  node [
    id 2227
    label "zaczyna&#263;"
  ]
  node [
    id 2228
    label "wpisywa&#263;"
  ]
  node [
    id 2229
    label "wchodzi&#263;"
  ]
  node [
    id 2230
    label "zapoznawa&#263;"
  ]
  node [
    id 2231
    label "inflict"
  ]
  node [
    id 2232
    label "schodzi&#263;"
  ]
  node [
    id 2233
    label "induct"
  ]
  node [
    id 2234
    label "begin"
  ]
  node [
    id 2235
    label "doprowadza&#263;"
  ]
  node [
    id 2236
    label "create"
  ]
  node [
    id 2237
    label "demaskator"
  ]
  node [
    id 2238
    label "objawia&#263;"
  ]
  node [
    id 2239
    label "informowa&#263;"
  ]
  node [
    id 2240
    label "indicate"
  ]
  node [
    id 2241
    label "donosi&#263;"
  ]
  node [
    id 2242
    label "inform"
  ]
  node [
    id 2243
    label "zaskakiwa&#263;"
  ]
  node [
    id 2244
    label "cover"
  ]
  node [
    id 2245
    label "swat"
  ]
  node [
    id 2246
    label "wyznawa&#263;"
  ]
  node [
    id 2247
    label "confide"
  ]
  node [
    id 2248
    label "zleca&#263;"
  ]
  node [
    id 2249
    label "grant"
  ]
  node [
    id 2250
    label "deal"
  ]
  node [
    id 2251
    label "stawia&#263;"
  ]
  node [
    id 2252
    label "rozgrywa&#263;"
  ]
  node [
    id 2253
    label "kelner"
  ]
  node [
    id 2254
    label "faszerowa&#263;"
  ]
  node [
    id 2255
    label "serwowa&#263;"
  ]
  node [
    id 2256
    label "kwota"
  ]
  node [
    id 2257
    label "remainder"
  ]
  node [
    id 2258
    label "pozosta&#322;y"
  ]
  node [
    id 2259
    label "impreza"
  ]
  node [
    id 2260
    label "tingel-tangel"
  ]
  node [
    id 2261
    label "numer"
  ]
  node [
    id 2262
    label "monta&#380;"
  ]
  node [
    id 2263
    label "postprodukcja"
  ]
  node [
    id 2264
    label "performance"
  ]
  node [
    id 2265
    label "product"
  ]
  node [
    id 2266
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 2267
    label "uzysk"
  ]
  node [
    id 2268
    label "rozw&#243;j"
  ]
  node [
    id 2269
    label "odtworzenie"
  ]
  node [
    id 2270
    label "trema"
  ]
  node [
    id 2271
    label "kooperowa&#263;"
  ]
  node [
    id 2272
    label "wypaplanie"
  ]
  node [
    id 2273
    label "enigmat"
  ]
  node [
    id 2274
    label "wiedza"
  ]
  node [
    id 2275
    label "zachowywanie"
  ]
  node [
    id 2276
    label "secret"
  ]
  node [
    id 2277
    label "obowi&#261;zek"
  ]
  node [
    id 2278
    label "dyskrecja"
  ]
  node [
    id 2279
    label "informacja"
  ]
  node [
    id 2280
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 2281
    label "taj&#324;"
  ]
  node [
    id 2282
    label "zachowa&#263;"
  ]
  node [
    id 2283
    label "zachowywa&#263;"
  ]
  node [
    id 2284
    label "posa&#380;ek"
  ]
  node [
    id 2285
    label "mienie"
  ]
  node [
    id 2286
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 2287
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 2288
    label "debit"
  ]
  node [
    id 2289
    label "redaktor"
  ]
  node [
    id 2290
    label "druk"
  ]
  node [
    id 2291
    label "publikacja"
  ]
  node [
    id 2292
    label "redakcja"
  ]
  node [
    id 2293
    label "szata_graficzna"
  ]
  node [
    id 2294
    label "firma"
  ]
  node [
    id 2295
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 2296
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 2297
    label "poster"
  ]
  node [
    id 2298
    label "phone"
  ]
  node [
    id 2299
    label "wpadni&#281;cie"
  ]
  node [
    id 2300
    label "intonacja"
  ]
  node [
    id 2301
    label "wpa&#347;&#263;"
  ]
  node [
    id 2302
    label "note"
  ]
  node [
    id 2303
    label "onomatopeja"
  ]
  node [
    id 2304
    label "modalizm"
  ]
  node [
    id 2305
    label "nadlecenie"
  ]
  node [
    id 2306
    label "sound"
  ]
  node [
    id 2307
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 2308
    label "wpada&#263;"
  ]
  node [
    id 2309
    label "solmizacja"
  ]
  node [
    id 2310
    label "seria"
  ]
  node [
    id 2311
    label "dobiec"
  ]
  node [
    id 2312
    label "transmiter"
  ]
  node [
    id 2313
    label "heksachord"
  ]
  node [
    id 2314
    label "akcent"
  ]
  node [
    id 2315
    label "repetycja"
  ]
  node [
    id 2316
    label "brzmienie"
  ]
  node [
    id 2317
    label "wpadanie"
  ]
  node [
    id 2318
    label "liczba_kwantowa"
  ]
  node [
    id 2319
    label "kosmetyk"
  ]
  node [
    id 2320
    label "ciasto"
  ]
  node [
    id 2321
    label "aromat"
  ]
  node [
    id 2322
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 2323
    label "puff"
  ]
  node [
    id 2324
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 2325
    label "przyprawa"
  ]
  node [
    id 2326
    label "upojno&#347;&#263;"
  ]
  node [
    id 2327
    label "owiewanie"
  ]
  node [
    id 2328
    label "smak"
  ]
  node [
    id 2329
    label "nadmiarowy"
  ]
  node [
    id 2330
    label "zb&#281;dnie"
  ]
  node [
    id 2331
    label "superfluously"
  ]
  node [
    id 2332
    label "uselessly"
  ]
  node [
    id 2333
    label "nadmiarowo"
  ]
  node [
    id 2334
    label "korkowanie"
  ]
  node [
    id 2335
    label "death"
  ]
  node [
    id 2336
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 2337
    label "przestawanie"
  ]
  node [
    id 2338
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 2339
    label "zdychanie"
  ]
  node [
    id 2340
    label "spisywanie_"
  ]
  node [
    id 2341
    label "usuwanie"
  ]
  node [
    id 2342
    label "tracenie"
  ]
  node [
    id 2343
    label "ko&#324;czenie"
  ]
  node [
    id 2344
    label "odrzut"
  ]
  node [
    id 2345
    label "zwalnianie_si&#281;"
  ]
  node [
    id 2346
    label "opuszczanie"
  ]
  node [
    id 2347
    label "wydalanie"
  ]
  node [
    id 2348
    label "odrzucanie"
  ]
  node [
    id 2349
    label "odstawianie"
  ]
  node [
    id 2350
    label "martwy"
  ]
  node [
    id 2351
    label "ust&#281;powanie"
  ]
  node [
    id 2352
    label "egress"
  ]
  node [
    id 2353
    label "zrzekanie_si&#281;"
  ]
  node [
    id 2354
    label "oddzielanie_si&#281;"
  ]
  node [
    id 2355
    label "wyruszanie"
  ]
  node [
    id 2356
    label "odumieranie"
  ]
  node [
    id 2357
    label "odstawanie"
  ]
  node [
    id 2358
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 2359
    label "mijanie"
  ]
  node [
    id 2360
    label "wracanie"
  ]
  node [
    id 2361
    label "oddalanie_si&#281;"
  ]
  node [
    id 2362
    label "kursowanie"
  ]
  node [
    id 2363
    label "mini&#281;cie"
  ]
  node [
    id 2364
    label "odumarcie"
  ]
  node [
    id 2365
    label "dysponowanie_si&#281;"
  ]
  node [
    id 2366
    label "ust&#261;pienie"
  ]
  node [
    id 2367
    label "mogi&#322;a"
  ]
  node [
    id 2368
    label "pomarcie"
  ]
  node [
    id 2369
    label "spisanie_"
  ]
  node [
    id 2370
    label "oddalenie_si&#281;"
  ]
  node [
    id 2371
    label "defenestracja"
  ]
  node [
    id 2372
    label "danie_sobie_spokoju"
  ]
  node [
    id 2373
    label "kres_&#380;ycia"
  ]
  node [
    id 2374
    label "zwolnienie_si&#281;"
  ]
  node [
    id 2375
    label "zdechni&#281;cie"
  ]
  node [
    id 2376
    label "stracenie"
  ]
  node [
    id 2377
    label "wr&#243;cenie"
  ]
  node [
    id 2378
    label "szeol"
  ]
  node [
    id 2379
    label "die"
  ]
  node [
    id 2380
    label "oddzielenie_si&#281;"
  ]
  node [
    id 2381
    label "wydalenie"
  ]
  node [
    id 2382
    label "pogrzebanie"
  ]
  node [
    id 2383
    label "&#380;a&#322;oba"
  ]
  node [
    id 2384
    label "sko&#324;czenie"
  ]
  node [
    id 2385
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 2386
    label "zabicie"
  ]
  node [
    id 2387
    label "agonia"
  ]
  node [
    id 2388
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 2389
    label "relinquishment"
  ]
  node [
    id 2390
    label "p&#243;j&#347;cie"
  ]
  node [
    id 2391
    label "poniechanie"
  ]
  node [
    id 2392
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 2393
    label "wypisanie_si&#281;"
  ]
  node [
    id 2394
    label "prawy"
  ]
  node [
    id 2395
    label "trybuna&#322;"
  ]
  node [
    id 2396
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 2397
    label "telekomunikacja"
  ]
  node [
    id 2398
    label "polski"
  ]
  node [
    id 2399
    label "Stanis&#322;awa"
  ]
  node [
    id 2400
    label "pi&#261;tka"
  ]
  node [
    id 2401
    label "Antoni"
  ]
  node [
    id 2402
    label "M&#281;&#380;yd&#322;o"
  ]
  node [
    id 2403
    label "centralny"
  ]
  node [
    id 2404
    label "Antykorupcyjne"
  ]
  node [
    id 2405
    label "rada"
  ]
  node [
    id 2406
    label "Stefan"
  ]
  node [
    id 2407
    label "Niesio&#322;owski"
  ]
  node [
    id 2408
    label "Jacek"
  ]
  node [
    id 2409
    label "Tomczak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 2
    target 583
  ]
  edge [
    source 2
    target 584
  ]
  edge [
    source 2
    target 585
  ]
  edge [
    source 2
    target 586
  ]
  edge [
    source 2
    target 587
  ]
  edge [
    source 2
    target 588
  ]
  edge [
    source 2
    target 589
  ]
  edge [
    source 2
    target 590
  ]
  edge [
    source 2
    target 591
  ]
  edge [
    source 2
    target 592
  ]
  edge [
    source 2
    target 593
  ]
  edge [
    source 2
    target 594
  ]
  edge [
    source 2
    target 595
  ]
  edge [
    source 2
    target 596
  ]
  edge [
    source 2
    target 597
  ]
  edge [
    source 2
    target 598
  ]
  edge [
    source 2
    target 599
  ]
  edge [
    source 2
    target 600
  ]
  edge [
    source 2
    target 601
  ]
  edge [
    source 2
    target 602
  ]
  edge [
    source 2
    target 603
  ]
  edge [
    source 2
    target 604
  ]
  edge [
    source 2
    target 605
  ]
  edge [
    source 2
    target 606
  ]
  edge [
    source 2
    target 607
  ]
  edge [
    source 2
    target 608
  ]
  edge [
    source 2
    target 609
  ]
  edge [
    source 2
    target 610
  ]
  edge [
    source 2
    target 611
  ]
  edge [
    source 2
    target 612
  ]
  edge [
    source 2
    target 613
  ]
  edge [
    source 2
    target 614
  ]
  edge [
    source 2
    target 615
  ]
  edge [
    source 2
    target 616
  ]
  edge [
    source 2
    target 617
  ]
  edge [
    source 2
    target 618
  ]
  edge [
    source 2
    target 619
  ]
  edge [
    source 2
    target 620
  ]
  edge [
    source 2
    target 621
  ]
  edge [
    source 2
    target 622
  ]
  edge [
    source 2
    target 623
  ]
  edge [
    source 2
    target 624
  ]
  edge [
    source 2
    target 625
  ]
  edge [
    source 2
    target 626
  ]
  edge [
    source 2
    target 627
  ]
  edge [
    source 2
    target 628
  ]
  edge [
    source 2
    target 629
  ]
  edge [
    source 2
    target 630
  ]
  edge [
    source 2
    target 631
  ]
  edge [
    source 2
    target 632
  ]
  edge [
    source 2
    target 633
  ]
  edge [
    source 2
    target 634
  ]
  edge [
    source 2
    target 635
  ]
  edge [
    source 2
    target 636
  ]
  edge [
    source 2
    target 637
  ]
  edge [
    source 2
    target 638
  ]
  edge [
    source 2
    target 639
  ]
  edge [
    source 2
    target 640
  ]
  edge [
    source 2
    target 641
  ]
  edge [
    source 2
    target 642
  ]
  edge [
    source 2
    target 643
  ]
  edge [
    source 2
    target 644
  ]
  edge [
    source 2
    target 645
  ]
  edge [
    source 2
    target 646
  ]
  edge [
    source 2
    target 647
  ]
  edge [
    source 2
    target 648
  ]
  edge [
    source 2
    target 649
  ]
  edge [
    source 2
    target 650
  ]
  edge [
    source 2
    target 651
  ]
  edge [
    source 2
    target 652
  ]
  edge [
    source 2
    target 653
  ]
  edge [
    source 2
    target 654
  ]
  edge [
    source 2
    target 655
  ]
  edge [
    source 2
    target 656
  ]
  edge [
    source 2
    target 657
  ]
  edge [
    source 2
    target 658
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 659
  ]
  edge [
    source 3
    target 660
  ]
  edge [
    source 3
    target 661
  ]
  edge [
    source 3
    target 662
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 663
  ]
  edge [
    source 3
    target 664
  ]
  edge [
    source 3
    target 665
  ]
  edge [
    source 3
    target 666
  ]
  edge [
    source 3
    target 667
  ]
  edge [
    source 3
    target 668
  ]
  edge [
    source 3
    target 669
  ]
  edge [
    source 3
    target 670
  ]
  edge [
    source 3
    target 671
  ]
  edge [
    source 3
    target 672
  ]
  edge [
    source 3
    target 673
  ]
  edge [
    source 3
    target 674
  ]
  edge [
    source 3
    target 675
  ]
  edge [
    source 3
    target 676
  ]
  edge [
    source 3
    target 677
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 678
  ]
  edge [
    source 3
    target 679
  ]
  edge [
    source 3
    target 680
  ]
  edge [
    source 3
    target 681
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 682
  ]
  edge [
    source 3
    target 683
  ]
  edge [
    source 3
    target 684
  ]
  edge [
    source 3
    target 685
  ]
  edge [
    source 3
    target 686
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 687
  ]
  edge [
    source 5
    target 688
  ]
  edge [
    source 5
    target 689
  ]
  edge [
    source 5
    target 690
  ]
  edge [
    source 5
    target 691
  ]
  edge [
    source 5
    target 692
  ]
  edge [
    source 5
    target 693
  ]
  edge [
    source 5
    target 694
  ]
  edge [
    source 5
    target 695
  ]
  edge [
    source 5
    target 696
  ]
  edge [
    source 5
    target 697
  ]
  edge [
    source 5
    target 698
  ]
  edge [
    source 5
    target 699
  ]
  edge [
    source 5
    target 700
  ]
  edge [
    source 5
    target 701
  ]
  edge [
    source 5
    target 702
  ]
  edge [
    source 5
    target 703
  ]
  edge [
    source 5
    target 704
  ]
  edge [
    source 5
    target 705
  ]
  edge [
    source 5
    target 706
  ]
  edge [
    source 5
    target 707
  ]
  edge [
    source 5
    target 708
  ]
  edge [
    source 5
    target 709
  ]
  edge [
    source 5
    target 710
  ]
  edge [
    source 5
    target 711
  ]
  edge [
    source 5
    target 712
  ]
  edge [
    source 5
    target 713
  ]
  edge [
    source 5
    target 714
  ]
  edge [
    source 5
    target 715
  ]
  edge [
    source 5
    target 716
  ]
  edge [
    source 5
    target 717
  ]
  edge [
    source 5
    target 718
  ]
  edge [
    source 5
    target 719
  ]
  edge [
    source 5
    target 720
  ]
  edge [
    source 5
    target 721
  ]
  edge [
    source 5
    target 722
  ]
  edge [
    source 5
    target 723
  ]
  edge [
    source 5
    target 724
  ]
  edge [
    source 5
    target 725
  ]
  edge [
    source 5
    target 726
  ]
  edge [
    source 5
    target 727
  ]
  edge [
    source 5
    target 728
  ]
  edge [
    source 5
    target 729
  ]
  edge [
    source 5
    target 730
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 731
  ]
  edge [
    source 5
    target 732
  ]
  edge [
    source 5
    target 733
  ]
  edge [
    source 5
    target 734
  ]
  edge [
    source 5
    target 735
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 37
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 824
  ]
  edge [
    source 14
    target 825
  ]
  edge [
    source 14
    target 826
  ]
  edge [
    source 14
    target 827
  ]
  edge [
    source 14
    target 828
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 829
  ]
  edge [
    source 14
    target 830
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 15
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 15
    target 849
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 852
  ]
  edge [
    source 15
    target 853
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 856
  ]
  edge [
    source 15
    target 857
  ]
  edge [
    source 15
    target 858
  ]
  edge [
    source 15
    target 859
  ]
  edge [
    source 15
    target 860
  ]
  edge [
    source 15
    target 861
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 863
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 15
    target 59
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 47
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 2405
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 330
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 268
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 68
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 754
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 37
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 993
  ]
  edge [
    source 20
    target 994
  ]
  edge [
    source 20
    target 995
  ]
  edge [
    source 20
    target 996
  ]
  edge [
    source 20
    target 997
  ]
  edge [
    source 20
    target 998
  ]
  edge [
    source 20
    target 999
  ]
  edge [
    source 20
    target 1000
  ]
  edge [
    source 20
    target 1001
  ]
  edge [
    source 20
    target 1002
  ]
  edge [
    source 20
    target 1003
  ]
  edge [
    source 20
    target 1004
  ]
  edge [
    source 20
    target 1005
  ]
  edge [
    source 20
    target 1006
  ]
  edge [
    source 20
    target 1007
  ]
  edge [
    source 20
    target 1008
  ]
  edge [
    source 20
    target 383
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 1009
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 1010
  ]
  edge [
    source 20
    target 1011
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 1012
  ]
  edge [
    source 20
    target 1013
  ]
  edge [
    source 20
    target 1014
  ]
  edge [
    source 20
    target 1015
  ]
  edge [
    source 20
    target 1016
  ]
  edge [
    source 20
    target 1017
  ]
  edge [
    source 20
    target 1018
  ]
  edge [
    source 20
    target 1019
  ]
  edge [
    source 20
    target 1020
  ]
  edge [
    source 20
    target 1021
  ]
  edge [
    source 20
    target 1022
  ]
  edge [
    source 20
    target 1023
  ]
  edge [
    source 20
    target 1024
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 76
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 20
    target 846
  ]
  edge [
    source 20
    target 1027
  ]
  edge [
    source 20
    target 1028
  ]
  edge [
    source 20
    target 1029
  ]
  edge [
    source 20
    target 1030
  ]
  edge [
    source 20
    target 1031
  ]
  edge [
    source 20
    target 1032
  ]
  edge [
    source 20
    target 1033
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 461
  ]
  edge [
    source 20
    target 1035
  ]
  edge [
    source 20
    target 1036
  ]
  edge [
    source 20
    target 1037
  ]
  edge [
    source 20
    target 1038
  ]
  edge [
    source 20
    target 1039
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 1041
  ]
  edge [
    source 20
    target 1042
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 20
    target 1045
  ]
  edge [
    source 20
    target 1046
  ]
  edge [
    source 20
    target 1047
  ]
  edge [
    source 20
    target 1048
  ]
  edge [
    source 20
    target 1049
  ]
  edge [
    source 20
    target 1050
  ]
  edge [
    source 20
    target 502
  ]
  edge [
    source 20
    target 1051
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 20
    target 1064
  ]
  edge [
    source 20
    target 839
  ]
  edge [
    source 20
    target 1065
  ]
  edge [
    source 20
    target 1066
  ]
  edge [
    source 20
    target 1067
  ]
  edge [
    source 20
    target 1068
  ]
  edge [
    source 20
    target 1069
  ]
  edge [
    source 20
    target 1070
  ]
  edge [
    source 20
    target 1071
  ]
  edge [
    source 20
    target 1072
  ]
  edge [
    source 20
    target 1073
  ]
  edge [
    source 20
    target 1074
  ]
  edge [
    source 20
    target 838
  ]
  edge [
    source 20
    target 379
  ]
  edge [
    source 20
    target 1075
  ]
  edge [
    source 20
    target 1076
  ]
  edge [
    source 20
    target 1077
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 1078
  ]
  edge [
    source 20
    target 1079
  ]
  edge [
    source 20
    target 1080
  ]
  edge [
    source 20
    target 1081
  ]
  edge [
    source 20
    target 1082
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 1083
  ]
  edge [
    source 20
    target 1084
  ]
  edge [
    source 20
    target 1085
  ]
  edge [
    source 20
    target 1086
  ]
  edge [
    source 20
    target 1087
  ]
  edge [
    source 20
    target 1088
  ]
  edge [
    source 20
    target 1089
  ]
  edge [
    source 20
    target 1090
  ]
  edge [
    source 20
    target 1091
  ]
  edge [
    source 20
    target 1092
  ]
  edge [
    source 20
    target 1093
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 1094
  ]
  edge [
    source 20
    target 1095
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 392
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 295
  ]
  edge [
    source 20
    target 1096
  ]
  edge [
    source 20
    target 1097
  ]
  edge [
    source 20
    target 1098
  ]
  edge [
    source 20
    target 1099
  ]
  edge [
    source 20
    target 1100
  ]
  edge [
    source 20
    target 1101
  ]
  edge [
    source 20
    target 1102
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 268
  ]
  edge [
    source 22
    target 408
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1058
  ]
  edge [
    source 22
    target 1107
  ]
  edge [
    source 22
    target 1108
  ]
  edge [
    source 22
    target 1109
  ]
  edge [
    source 22
    target 1110
  ]
  edge [
    source 22
    target 1111
  ]
  edge [
    source 22
    target 1112
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 1113
  ]
  edge [
    source 22
    target 72
  ]
  edge [
    source 22
    target 1114
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 1116
  ]
  edge [
    source 22
    target 1026
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 76
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 22
    target 1122
  ]
  edge [
    source 22
    target 755
  ]
  edge [
    source 22
    target 1123
  ]
  edge [
    source 22
    target 1124
  ]
  edge [
    source 22
    target 1125
  ]
  edge [
    source 22
    target 1126
  ]
  edge [
    source 22
    target 839
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 1128
  ]
  edge [
    source 22
    target 1129
  ]
  edge [
    source 22
    target 1130
  ]
  edge [
    source 22
    target 1131
  ]
  edge [
    source 22
    target 1132
  ]
  edge [
    source 22
    target 1133
  ]
  edge [
    source 22
    target 1134
  ]
  edge [
    source 22
    target 1135
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 1137
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 80
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 357
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 118
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1027
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 321
  ]
  edge [
    source 22
    target 322
  ]
  edge [
    source 22
    target 323
  ]
  edge [
    source 22
    target 324
  ]
  edge [
    source 22
    target 325
  ]
  edge [
    source 22
    target 326
  ]
  edge [
    source 22
    target 327
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 333
  ]
  edge [
    source 22
    target 334
  ]
  edge [
    source 22
    target 335
  ]
  edge [
    source 22
    target 336
  ]
  edge [
    source 22
    target 337
  ]
  edge [
    source 22
    target 338
  ]
  edge [
    source 22
    target 339
  ]
  edge [
    source 22
    target 340
  ]
  edge [
    source 22
    target 341
  ]
  edge [
    source 22
    target 342
  ]
  edge [
    source 22
    target 343
  ]
  edge [
    source 22
    target 344
  ]
  edge [
    source 22
    target 345
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1165
  ]
  edge [
    source 22
    target 1166
  ]
  edge [
    source 22
    target 1167
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1057
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1061
  ]
  edge [
    source 22
    target 629
  ]
  edge [
    source 22
    target 1062
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 929
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 277
  ]
  edge [
    source 22
    target 574
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 1198
  ]
  edge [
    source 23
    target 1199
  ]
  edge [
    source 23
    target 1200
  ]
  edge [
    source 23
    target 784
  ]
  edge [
    source 23
    target 1201
  ]
  edge [
    source 23
    target 1202
  ]
  edge [
    source 23
    target 1203
  ]
  edge [
    source 23
    target 64
  ]
  edge [
    source 23
    target 1204
  ]
  edge [
    source 23
    target 1205
  ]
  edge [
    source 23
    target 1206
  ]
  edge [
    source 23
    target 1207
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 1209
  ]
  edge [
    source 23
    target 1210
  ]
  edge [
    source 23
    target 1211
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 23
    target 1217
  ]
  edge [
    source 23
    target 1218
  ]
  edge [
    source 23
    target 1219
  ]
  edge [
    source 23
    target 1220
  ]
  edge [
    source 23
    target 1221
  ]
  edge [
    source 23
    target 1222
  ]
  edge [
    source 23
    target 1223
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1224
  ]
  edge [
    source 23
    target 1225
  ]
  edge [
    source 23
    target 1226
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 1227
  ]
  edge [
    source 23
    target 1228
  ]
  edge [
    source 23
    target 1229
  ]
  edge [
    source 23
    target 1230
  ]
  edge [
    source 23
    target 1231
  ]
  edge [
    source 23
    target 1232
  ]
  edge [
    source 23
    target 1233
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 1235
  ]
  edge [
    source 23
    target 1236
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1238
  ]
  edge [
    source 23
    target 856
  ]
  edge [
    source 23
    target 76
  ]
  edge [
    source 23
    target 1239
  ]
  edge [
    source 23
    target 1240
  ]
  edge [
    source 23
    target 1241
  ]
  edge [
    source 23
    target 204
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 861
  ]
  edge [
    source 23
    target 183
  ]
  edge [
    source 23
    target 1243
  ]
  edge [
    source 23
    target 1244
  ]
  edge [
    source 23
    target 1245
  ]
  edge [
    source 23
    target 1246
  ]
  edge [
    source 23
    target 1247
  ]
  edge [
    source 23
    target 1248
  ]
  edge [
    source 23
    target 121
  ]
  edge [
    source 23
    target 1249
  ]
  edge [
    source 23
    target 1250
  ]
  edge [
    source 23
    target 88
  ]
  edge [
    source 23
    target 89
  ]
  edge [
    source 23
    target 90
  ]
  edge [
    source 23
    target 91
  ]
  edge [
    source 23
    target 92
  ]
  edge [
    source 23
    target 93
  ]
  edge [
    source 23
    target 94
  ]
  edge [
    source 23
    target 95
  ]
  edge [
    source 23
    target 96
  ]
  edge [
    source 23
    target 97
  ]
  edge [
    source 23
    target 98
  ]
  edge [
    source 23
    target 99
  ]
  edge [
    source 23
    target 100
  ]
  edge [
    source 23
    target 101
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 102
  ]
  edge [
    source 23
    target 103
  ]
  edge [
    source 23
    target 104
  ]
  edge [
    source 23
    target 105
  ]
  edge [
    source 23
    target 106
  ]
  edge [
    source 23
    target 107
  ]
  edge [
    source 23
    target 108
  ]
  edge [
    source 23
    target 109
  ]
  edge [
    source 23
    target 110
  ]
  edge [
    source 23
    target 111
  ]
  edge [
    source 23
    target 112
  ]
  edge [
    source 23
    target 113
  ]
  edge [
    source 23
    target 114
  ]
  edge [
    source 23
    target 115
  ]
  edge [
    source 23
    target 116
  ]
  edge [
    source 23
    target 117
  ]
  edge [
    source 23
    target 118
  ]
  edge [
    source 23
    target 119
  ]
  edge [
    source 23
    target 120
  ]
  edge [
    source 23
    target 122
  ]
  edge [
    source 23
    target 123
  ]
  edge [
    source 23
    target 1251
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 1252
  ]
  edge [
    source 23
    target 1253
  ]
  edge [
    source 23
    target 72
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1254
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 1256
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1262
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 1264
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 432
  ]
  edge [
    source 23
    target 1265
  ]
  edge [
    source 23
    target 754
  ]
  edge [
    source 23
    target 1266
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 1267
  ]
  edge [
    source 24
    target 1268
  ]
  edge [
    source 24
    target 1269
  ]
  edge [
    source 24
    target 1270
  ]
  edge [
    source 24
    target 1271
  ]
  edge [
    source 24
    target 889
  ]
  edge [
    source 24
    target 1272
  ]
  edge [
    source 24
    target 1273
  ]
  edge [
    source 24
    target 1274
  ]
  edge [
    source 24
    target 1275
  ]
  edge [
    source 24
    target 53
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 761
  ]
  edge [
    source 25
    target 1276
  ]
  edge [
    source 25
    target 1277
  ]
  edge [
    source 25
    target 756
  ]
  edge [
    source 25
    target 1278
  ]
  edge [
    source 25
    target 1279
  ]
  edge [
    source 25
    target 1280
  ]
  edge [
    source 25
    target 1281
  ]
  edge [
    source 25
    target 365
  ]
  edge [
    source 25
    target 1282
  ]
  edge [
    source 25
    target 1052
  ]
  edge [
    source 25
    target 1283
  ]
  edge [
    source 25
    target 1284
  ]
  edge [
    source 25
    target 1285
  ]
  edge [
    source 25
    target 1286
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 27
    target 2394
  ]
  edge [
    source 28
    target 1287
  ]
  edge [
    source 28
    target 1288
  ]
  edge [
    source 28
    target 1289
  ]
  edge [
    source 28
    target 1290
  ]
  edge [
    source 28
    target 1291
  ]
  edge [
    source 28
    target 1292
  ]
  edge [
    source 28
    target 1293
  ]
  edge [
    source 28
    target 1294
  ]
  edge [
    source 28
    target 1295
  ]
  edge [
    source 28
    target 1296
  ]
  edge [
    source 28
    target 897
  ]
  edge [
    source 28
    target 1297
  ]
  edge [
    source 28
    target 1298
  ]
  edge [
    source 28
    target 1299
  ]
  edge [
    source 28
    target 1300
  ]
  edge [
    source 28
    target 1301
  ]
  edge [
    source 28
    target 1302
  ]
  edge [
    source 28
    target 1303
  ]
  edge [
    source 28
    target 1304
  ]
  edge [
    source 28
    target 1305
  ]
  edge [
    source 28
    target 1306
  ]
  edge [
    source 28
    target 1307
  ]
  edge [
    source 28
    target 1308
  ]
  edge [
    source 28
    target 1309
  ]
  edge [
    source 28
    target 1310
  ]
  edge [
    source 28
    target 1311
  ]
  edge [
    source 28
    target 1312
  ]
  edge [
    source 28
    target 1313
  ]
  edge [
    source 28
    target 1314
  ]
  edge [
    source 28
    target 1315
  ]
  edge [
    source 28
    target 1316
  ]
  edge [
    source 28
    target 1317
  ]
  edge [
    source 28
    target 1318
  ]
  edge [
    source 28
    target 1319
  ]
  edge [
    source 28
    target 1320
  ]
  edge [
    source 28
    target 1321
  ]
  edge [
    source 28
    target 878
  ]
  edge [
    source 28
    target 1322
  ]
  edge [
    source 28
    target 1323
  ]
  edge [
    source 28
    target 1122
  ]
  edge [
    source 28
    target 1324
  ]
  edge [
    source 28
    target 740
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1325
  ]
  edge [
    source 30
    target 1326
  ]
  edge [
    source 30
    target 1327
  ]
  edge [
    source 30
    target 1328
  ]
  edge [
    source 30
    target 1207
  ]
  edge [
    source 30
    target 83
  ]
  edge [
    source 30
    target 1329
  ]
  edge [
    source 30
    target 1330
  ]
  edge [
    source 30
    target 66
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 1331
  ]
  edge [
    source 30
    target 73
  ]
  edge [
    source 30
    target 74
  ]
  edge [
    source 30
    target 75
  ]
  edge [
    source 30
    target 77
  ]
  edge [
    source 30
    target 78
  ]
  edge [
    source 30
    target 79
  ]
  edge [
    source 30
    target 80
  ]
  edge [
    source 30
    target 81
  ]
  edge [
    source 30
    target 82
  ]
  edge [
    source 30
    target 84
  ]
  edge [
    source 30
    target 85
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 30
    target 87
  ]
  edge [
    source 30
    target 1242
  ]
  edge [
    source 30
    target 356
  ]
  edge [
    source 30
    target 1332
  ]
  edge [
    source 30
    target 1333
  ]
  edge [
    source 30
    target 1334
  ]
  edge [
    source 30
    target 1335
  ]
  edge [
    source 30
    target 670
  ]
  edge [
    source 30
    target 1336
  ]
  edge [
    source 30
    target 1337
  ]
  edge [
    source 30
    target 1338
  ]
  edge [
    source 30
    target 1339
  ]
  edge [
    source 30
    target 1340
  ]
  edge [
    source 30
    target 1341
  ]
  edge [
    source 30
    target 204
  ]
  edge [
    source 30
    target 1239
  ]
  edge [
    source 30
    target 1241
  ]
  edge [
    source 30
    target 1240
  ]
  edge [
    source 30
    target 41
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1342
  ]
  edge [
    source 32
    target 1343
  ]
  edge [
    source 32
    target 1344
  ]
  edge [
    source 32
    target 1345
  ]
  edge [
    source 32
    target 1346
  ]
  edge [
    source 32
    target 1347
  ]
  edge [
    source 32
    target 1348
  ]
  edge [
    source 32
    target 1349
  ]
  edge [
    source 32
    target 1182
  ]
  edge [
    source 32
    target 1350
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 369
  ]
  edge [
    source 33
    target 1351
  ]
  edge [
    source 33
    target 1352
  ]
  edge [
    source 33
    target 1353
  ]
  edge [
    source 33
    target 1354
  ]
  edge [
    source 33
    target 1355
  ]
  edge [
    source 33
    target 1356
  ]
  edge [
    source 33
    target 1357
  ]
  edge [
    source 33
    target 1358
  ]
  edge [
    source 33
    target 1359
  ]
  edge [
    source 33
    target 1360
  ]
  edge [
    source 33
    target 1361
  ]
  edge [
    source 33
    target 1362
  ]
  edge [
    source 33
    target 1363
  ]
  edge [
    source 33
    target 1364
  ]
  edge [
    source 33
    target 1365
  ]
  edge [
    source 33
    target 1366
  ]
  edge [
    source 33
    target 1367
  ]
  edge [
    source 33
    target 252
  ]
  edge [
    source 33
    target 1368
  ]
  edge [
    source 33
    target 846
  ]
  edge [
    source 33
    target 1369
  ]
  edge [
    source 33
    target 1370
  ]
  edge [
    source 33
    target 1371
  ]
  edge [
    source 33
    target 1372
  ]
  edge [
    source 33
    target 1014
  ]
  edge [
    source 33
    target 1373
  ]
  edge [
    source 33
    target 1137
  ]
  edge [
    source 33
    target 1374
  ]
  edge [
    source 33
    target 1375
  ]
  edge [
    source 33
    target 1376
  ]
  edge [
    source 33
    target 1377
  ]
  edge [
    source 33
    target 1378
  ]
  edge [
    source 33
    target 1379
  ]
  edge [
    source 33
    target 1380
  ]
  edge [
    source 33
    target 1381
  ]
  edge [
    source 33
    target 833
  ]
  edge [
    source 33
    target 1382
  ]
  edge [
    source 33
    target 1383
  ]
  edge [
    source 33
    target 1384
  ]
  edge [
    source 33
    target 1385
  ]
  edge [
    source 33
    target 1386
  ]
  edge [
    source 33
    target 1387
  ]
  edge [
    source 33
    target 266
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 33
    target 287
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 297
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 1009
  ]
  edge [
    source 33
    target 782
  ]
  edge [
    source 33
    target 1388
  ]
  edge [
    source 33
    target 1389
  ]
  edge [
    source 33
    target 1390
  ]
  edge [
    source 33
    target 1391
  ]
  edge [
    source 33
    target 1392
  ]
  edge [
    source 33
    target 1393
  ]
  edge [
    source 33
    target 59
  ]
  edge [
    source 33
    target 721
  ]
  edge [
    source 33
    target 72
  ]
  edge [
    source 33
    target 1394
  ]
  edge [
    source 33
    target 1395
  ]
  edge [
    source 33
    target 1396
  ]
  edge [
    source 33
    target 1397
  ]
  edge [
    source 33
    target 136
  ]
  edge [
    source 33
    target 1398
  ]
  edge [
    source 33
    target 1399
  ]
  edge [
    source 33
    target 1400
  ]
  edge [
    source 33
    target 1401
  ]
  edge [
    source 33
    target 1402
  ]
  edge [
    source 33
    target 1403
  ]
  edge [
    source 33
    target 1404
  ]
  edge [
    source 33
    target 1405
  ]
  edge [
    source 33
    target 1406
  ]
  edge [
    source 33
    target 1407
  ]
  edge [
    source 33
    target 1408
  ]
  edge [
    source 33
    target 1409
  ]
  edge [
    source 33
    target 1410
  ]
  edge [
    source 33
    target 1411
  ]
  edge [
    source 33
    target 1412
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 42
  ]
  edge [
    source 34
    target 59
  ]
  edge [
    source 34
    target 1413
  ]
  edge [
    source 34
    target 1414
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1415
  ]
  edge [
    source 35
    target 1416
  ]
  edge [
    source 35
    target 1417
  ]
  edge [
    source 35
    target 1418
  ]
  edge [
    source 35
    target 1419
  ]
  edge [
    source 35
    target 1420
  ]
  edge [
    source 36
    target 1421
  ]
  edge [
    source 36
    target 806
  ]
  edge [
    source 36
    target 1422
  ]
  edge [
    source 36
    target 1423
  ]
  edge [
    source 36
    target 1424
  ]
  edge [
    source 36
    target 1425
  ]
  edge [
    source 36
    target 1426
  ]
  edge [
    source 36
    target 1427
  ]
  edge [
    source 36
    target 1428
  ]
  edge [
    source 36
    target 1429
  ]
  edge [
    source 36
    target 1430
  ]
  edge [
    source 36
    target 1431
  ]
  edge [
    source 36
    target 820
  ]
  edge [
    source 36
    target 1432
  ]
  edge [
    source 36
    target 1433
  ]
  edge [
    source 36
    target 1434
  ]
  edge [
    source 36
    target 1435
  ]
  edge [
    source 36
    target 1436
  ]
  edge [
    source 36
    target 1437
  ]
  edge [
    source 36
    target 1438
  ]
  edge [
    source 36
    target 1439
  ]
  edge [
    source 36
    target 1440
  ]
  edge [
    source 36
    target 1441
  ]
  edge [
    source 36
    target 1442
  ]
  edge [
    source 36
    target 1443
  ]
  edge [
    source 36
    target 1444
  ]
  edge [
    source 36
    target 1445
  ]
  edge [
    source 36
    target 1446
  ]
  edge [
    source 36
    target 1447
  ]
  edge [
    source 36
    target 1448
  ]
  edge [
    source 36
    target 1449
  ]
  edge [
    source 36
    target 1450
  ]
  edge [
    source 36
    target 1371
  ]
  edge [
    source 36
    target 1451
  ]
  edge [
    source 36
    target 1452
  ]
  edge [
    source 36
    target 1453
  ]
  edge [
    source 36
    target 1454
  ]
  edge [
    source 36
    target 1455
  ]
  edge [
    source 36
    target 1456
  ]
  edge [
    source 36
    target 1457
  ]
  edge [
    source 36
    target 1458
  ]
  edge [
    source 36
    target 1459
  ]
  edge [
    source 36
    target 1460
  ]
  edge [
    source 36
    target 1461
  ]
  edge [
    source 36
    target 1462
  ]
  edge [
    source 36
    target 1463
  ]
  edge [
    source 36
    target 1464
  ]
  edge [
    source 36
    target 1465
  ]
  edge [
    source 36
    target 513
  ]
  edge [
    source 36
    target 1466
  ]
  edge [
    source 36
    target 1467
  ]
  edge [
    source 36
    target 1468
  ]
  edge [
    source 36
    target 1469
  ]
  edge [
    source 36
    target 1470
  ]
  edge [
    source 36
    target 1243
  ]
  edge [
    source 36
    target 1471
  ]
  edge [
    source 36
    target 1472
  ]
  edge [
    source 36
    target 1473
  ]
  edge [
    source 36
    target 1474
  ]
  edge [
    source 36
    target 1475
  ]
  edge [
    source 36
    target 1476
  ]
  edge [
    source 36
    target 1477
  ]
  edge [
    source 36
    target 1478
  ]
  edge [
    source 36
    target 1479
  ]
  edge [
    source 36
    target 1480
  ]
  edge [
    source 36
    target 1072
  ]
  edge [
    source 36
    target 1481
  ]
  edge [
    source 36
    target 1482
  ]
  edge [
    source 36
    target 1483
  ]
  edge [
    source 36
    target 1484
  ]
  edge [
    source 36
    target 1485
  ]
  edge [
    source 36
    target 1486
  ]
  edge [
    source 36
    target 1487
  ]
  edge [
    source 36
    target 1488
  ]
  edge [
    source 36
    target 1489
  ]
  edge [
    source 36
    target 1490
  ]
  edge [
    source 36
    target 1491
  ]
  edge [
    source 36
    target 44
  ]
  edge [
    source 36
    target 1492
  ]
  edge [
    source 36
    target 1493
  ]
  edge [
    source 36
    target 1494
  ]
  edge [
    source 36
    target 1495
  ]
  edge [
    source 36
    target 1496
  ]
  edge [
    source 36
    target 356
  ]
  edge [
    source 36
    target 1497
  ]
  edge [
    source 36
    target 1498
  ]
  edge [
    source 36
    target 1499
  ]
  edge [
    source 36
    target 1500
  ]
  edge [
    source 36
    target 1081
  ]
  edge [
    source 36
    target 1501
  ]
  edge [
    source 36
    target 1502
  ]
  edge [
    source 36
    target 1503
  ]
  edge [
    source 36
    target 1504
  ]
  edge [
    source 36
    target 1505
  ]
  edge [
    source 36
    target 833
  ]
  edge [
    source 36
    target 1506
  ]
  edge [
    source 36
    target 1507
  ]
  edge [
    source 36
    target 1508
  ]
  edge [
    source 36
    target 1509
  ]
  edge [
    source 36
    target 1055
  ]
  edge [
    source 36
    target 1510
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1076
  ]
  edge [
    source 37
    target 1511
  ]
  edge [
    source 37
    target 1512
  ]
  edge [
    source 37
    target 1513
  ]
  edge [
    source 37
    target 1514
  ]
  edge [
    source 37
    target 1515
  ]
  edge [
    source 37
    target 1516
  ]
  edge [
    source 37
    target 1517
  ]
  edge [
    source 37
    target 1518
  ]
  edge [
    source 37
    target 1519
  ]
  edge [
    source 37
    target 1520
  ]
  edge [
    source 37
    target 839
  ]
  edge [
    source 37
    target 1521
  ]
  edge [
    source 37
    target 1522
  ]
  edge [
    source 37
    target 1523
  ]
  edge [
    source 37
    target 386
  ]
  edge [
    source 37
    target 629
  ]
  edge [
    source 37
    target 1524
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1525
  ]
  edge [
    source 38
    target 1526
  ]
  edge [
    source 38
    target 1189
  ]
  edge [
    source 38
    target 1499
  ]
  edge [
    source 38
    target 1527
  ]
  edge [
    source 38
    target 1528
  ]
  edge [
    source 38
    target 1529
  ]
  edge [
    source 38
    target 1530
  ]
  edge [
    source 38
    target 72
  ]
  edge [
    source 38
    target 1531
  ]
  edge [
    source 38
    target 1532
  ]
  edge [
    source 38
    target 1533
  ]
  edge [
    source 38
    target 1534
  ]
  edge [
    source 38
    target 1535
  ]
  edge [
    source 38
    target 1536
  ]
  edge [
    source 38
    target 199
  ]
  edge [
    source 38
    target 136
  ]
  edge [
    source 38
    target 1537
  ]
  edge [
    source 38
    target 1426
  ]
  edge [
    source 38
    target 1538
  ]
  edge [
    source 38
    target 1403
  ]
  edge [
    source 38
    target 1539
  ]
  edge [
    source 38
    target 1540
  ]
  edge [
    source 38
    target 1541
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 889
  ]
  edge [
    source 39
    target 1542
  ]
  edge [
    source 39
    target 1543
  ]
  edge [
    source 39
    target 1544
  ]
  edge [
    source 39
    target 1545
  ]
  edge [
    source 39
    target 1546
  ]
  edge [
    source 39
    target 1547
  ]
  edge [
    source 39
    target 1548
  ]
  edge [
    source 39
    target 1549
  ]
  edge [
    source 39
    target 1550
  ]
  edge [
    source 39
    target 898
  ]
  edge [
    source 39
    target 884
  ]
  edge [
    source 39
    target 1551
  ]
  edge [
    source 39
    target 1552
  ]
  edge [
    source 39
    target 348
  ]
  edge [
    source 39
    target 1553
  ]
  edge [
    source 39
    target 1554
  ]
  edge [
    source 39
    target 1555
  ]
  edge [
    source 39
    target 1556
  ]
  edge [
    source 39
    target 1557
  ]
  edge [
    source 39
    target 1558
  ]
  edge [
    source 39
    target 1559
  ]
  edge [
    source 39
    target 1560
  ]
  edge [
    source 39
    target 347
  ]
  edge [
    source 39
    target 1561
  ]
  edge [
    source 39
    target 1562
  ]
  edge [
    source 39
    target 1563
  ]
  edge [
    source 39
    target 1564
  ]
  edge [
    source 39
    target 1565
  ]
  edge [
    source 39
    target 1566
  ]
  edge [
    source 39
    target 1567
  ]
  edge [
    source 39
    target 242
  ]
  edge [
    source 39
    target 1359
  ]
  edge [
    source 39
    target 1568
  ]
  edge [
    source 39
    target 1569
  ]
  edge [
    source 39
    target 1570
  ]
  edge [
    source 39
    target 1571
  ]
  edge [
    source 39
    target 677
  ]
  edge [
    source 39
    target 1572
  ]
  edge [
    source 39
    target 1573
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 291
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 39
    target 1576
  ]
  edge [
    source 39
    target 1577
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1579
  ]
  edge [
    source 41
    target 1580
  ]
  edge [
    source 41
    target 1581
  ]
  edge [
    source 41
    target 1582
  ]
  edge [
    source 41
    target 1583
  ]
  edge [
    source 41
    target 846
  ]
  edge [
    source 41
    target 1584
  ]
  edge [
    source 41
    target 1585
  ]
  edge [
    source 41
    target 850
  ]
  edge [
    source 41
    target 1586
  ]
  edge [
    source 41
    target 1242
  ]
  edge [
    source 41
    target 1009
  ]
  edge [
    source 41
    target 1587
  ]
  edge [
    source 41
    target 149
  ]
  edge [
    source 41
    target 183
  ]
  edge [
    source 41
    target 1588
  ]
  edge [
    source 41
    target 772
  ]
  edge [
    source 41
    target 1589
  ]
  edge [
    source 41
    target 1590
  ]
  edge [
    source 41
    target 1591
  ]
  edge [
    source 41
    target 1592
  ]
  edge [
    source 41
    target 1593
  ]
  edge [
    source 41
    target 1594
  ]
  edge [
    source 41
    target 1595
  ]
  edge [
    source 41
    target 1596
  ]
  edge [
    source 41
    target 1597
  ]
  edge [
    source 41
    target 1598
  ]
  edge [
    source 41
    target 1599
  ]
  edge [
    source 41
    target 1600
  ]
  edge [
    source 41
    target 1601
  ]
  edge [
    source 41
    target 1602
  ]
  edge [
    source 41
    target 1603
  ]
  edge [
    source 41
    target 784
  ]
  edge [
    source 41
    target 1604
  ]
  edge [
    source 41
    target 68
  ]
  edge [
    source 41
    target 1605
  ]
  edge [
    source 41
    target 1606
  ]
  edge [
    source 41
    target 1607
  ]
  edge [
    source 41
    target 1608
  ]
  edge [
    source 41
    target 1609
  ]
  edge [
    source 41
    target 787
  ]
  edge [
    source 41
    target 1610
  ]
  edge [
    source 41
    target 861
  ]
  edge [
    source 41
    target 1243
  ]
  edge [
    source 41
    target 1244
  ]
  edge [
    source 41
    target 1611
  ]
  edge [
    source 41
    target 1612
  ]
  edge [
    source 41
    target 839
  ]
  edge [
    source 41
    target 1613
  ]
  edge [
    source 41
    target 1614
  ]
  edge [
    source 41
    target 185
  ]
  edge [
    source 41
    target 1615
  ]
  edge [
    source 41
    target 1616
  ]
  edge [
    source 41
    target 1617
  ]
  edge [
    source 41
    target 1618
  ]
  edge [
    source 41
    target 1619
  ]
  edge [
    source 41
    target 1620
  ]
  edge [
    source 41
    target 1621
  ]
  edge [
    source 41
    target 1253
  ]
  edge [
    source 41
    target 1622
  ]
  edge [
    source 41
    target 1207
  ]
  edge [
    source 41
    target 1623
  ]
  edge [
    source 41
    target 1624
  ]
  edge [
    source 41
    target 1625
  ]
  edge [
    source 41
    target 1426
  ]
  edge [
    source 41
    target 743
  ]
  edge [
    source 41
    target 1371
  ]
  edge [
    source 41
    target 1626
  ]
  edge [
    source 41
    target 773
  ]
  edge [
    source 41
    target 148
  ]
  edge [
    source 41
    target 1627
  ]
  edge [
    source 41
    target 1628
  ]
  edge [
    source 41
    target 76
  ]
  edge [
    source 41
    target 1027
  ]
  edge [
    source 41
    target 1629
  ]
  edge [
    source 41
    target 1630
  ]
  edge [
    source 41
    target 1631
  ]
  edge [
    source 41
    target 1028
  ]
  edge [
    source 41
    target 1632
  ]
  edge [
    source 41
    target 1136
  ]
  edge [
    source 41
    target 1633
  ]
  edge [
    source 41
    target 1634
  ]
  edge [
    source 41
    target 1635
  ]
  edge [
    source 41
    target 1636
  ]
  edge [
    source 41
    target 1637
  ]
  edge [
    source 41
    target 1638
  ]
  edge [
    source 41
    target 1639
  ]
  edge [
    source 41
    target 1640
  ]
  edge [
    source 41
    target 1641
  ]
  edge [
    source 41
    target 1642
  ]
  edge [
    source 41
    target 1643
  ]
  edge [
    source 41
    target 1644
  ]
  edge [
    source 41
    target 1645
  ]
  edge [
    source 41
    target 867
  ]
  edge [
    source 41
    target 1646
  ]
  edge [
    source 41
    target 1647
  ]
  edge [
    source 41
    target 1648
  ]
  edge [
    source 41
    target 1649
  ]
  edge [
    source 41
    target 1650
  ]
  edge [
    source 41
    target 1651
  ]
  edge [
    source 41
    target 1652
  ]
  edge [
    source 41
    target 1653
  ]
  edge [
    source 41
    target 1654
  ]
  edge [
    source 41
    target 1655
  ]
  edge [
    source 41
    target 1656
  ]
  edge [
    source 41
    target 1657
  ]
  edge [
    source 41
    target 1378
  ]
  edge [
    source 41
    target 1658
  ]
  edge [
    source 41
    target 833
  ]
  edge [
    source 41
    target 1659
  ]
  edge [
    source 41
    target 1660
  ]
  edge [
    source 41
    target 1235
  ]
  edge [
    source 41
    target 1661
  ]
  edge [
    source 41
    target 1662
  ]
  edge [
    source 41
    target 1663
  ]
  edge [
    source 41
    target 1664
  ]
  edge [
    source 41
    target 1665
  ]
  edge [
    source 41
    target 1666
  ]
  edge [
    source 41
    target 1667
  ]
  edge [
    source 41
    target 1668
  ]
  edge [
    source 41
    target 1669
  ]
  edge [
    source 41
    target 1670
  ]
  edge [
    source 41
    target 1671
  ]
  edge [
    source 41
    target 268
  ]
  edge [
    source 41
    target 1672
  ]
  edge [
    source 41
    target 1673
  ]
  edge [
    source 41
    target 1674
  ]
  edge [
    source 41
    target 1675
  ]
  edge [
    source 41
    target 1676
  ]
  edge [
    source 41
    target 1677
  ]
  edge [
    source 41
    target 1678
  ]
  edge [
    source 41
    target 1679
  ]
  edge [
    source 41
    target 1680
  ]
  edge [
    source 41
    target 1681
  ]
  edge [
    source 41
    target 1682
  ]
  edge [
    source 41
    target 1683
  ]
  edge [
    source 41
    target 1684
  ]
  edge [
    source 41
    target 1685
  ]
  edge [
    source 41
    target 1686
  ]
  edge [
    source 41
    target 1687
  ]
  edge [
    source 41
    target 1688
  ]
  edge [
    source 41
    target 1689
  ]
  edge [
    source 41
    target 1690
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 67
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 199
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 410
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 856
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 41
    target 1730
  ]
  edge [
    source 41
    target 1731
  ]
  edge [
    source 41
    target 1732
  ]
  edge [
    source 41
    target 1733
  ]
  edge [
    source 41
    target 1734
  ]
  edge [
    source 41
    target 1735
  ]
  edge [
    source 41
    target 1736
  ]
  edge [
    source 41
    target 1737
  ]
  edge [
    source 41
    target 1738
  ]
  edge [
    source 41
    target 1739
  ]
  edge [
    source 41
    target 1740
  ]
  edge [
    source 41
    target 340
  ]
  edge [
    source 41
    target 1741
  ]
  edge [
    source 41
    target 1742
  ]
  edge [
    source 41
    target 1743
  ]
  edge [
    source 41
    target 1744
  ]
  edge [
    source 41
    target 1745
  ]
  edge [
    source 41
    target 1746
  ]
  edge [
    source 41
    target 408
  ]
  edge [
    source 41
    target 1747
  ]
  edge [
    source 41
    target 1748
  ]
  edge [
    source 41
    target 1749
  ]
  edge [
    source 41
    target 1750
  ]
  edge [
    source 41
    target 1034
  ]
  edge [
    source 41
    target 1751
  ]
  edge [
    source 41
    target 1752
  ]
  edge [
    source 41
    target 1753
  ]
  edge [
    source 41
    target 1040
  ]
  edge [
    source 41
    target 1754
  ]
  edge [
    source 41
    target 1755
  ]
  edge [
    source 41
    target 1756
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 1181
  ]
  edge [
    source 41
    target 1757
  ]
  edge [
    source 41
    target 1758
  ]
  edge [
    source 41
    target 1759
  ]
  edge [
    source 41
    target 1088
  ]
  edge [
    source 41
    target 136
  ]
  edge [
    source 41
    target 999
  ]
  edge [
    source 41
    target 1760
  ]
  edge [
    source 41
    target 1761
  ]
  edge [
    source 41
    target 1762
  ]
  edge [
    source 41
    target 1763
  ]
  edge [
    source 41
    target 1764
  ]
  edge [
    source 41
    target 1765
  ]
  edge [
    source 41
    target 1137
  ]
  edge [
    source 41
    target 1766
  ]
  edge [
    source 41
    target 383
  ]
  edge [
    source 41
    target 1767
  ]
  edge [
    source 41
    target 1768
  ]
  edge [
    source 41
    target 832
  ]
  edge [
    source 41
    target 1769
  ]
  edge [
    source 41
    target 1081
  ]
  edge [
    source 41
    target 1082
  ]
  edge [
    source 41
    target 1083
  ]
  edge [
    source 41
    target 1770
  ]
  edge [
    source 41
    target 1771
  ]
  edge [
    source 41
    target 1772
  ]
  edge [
    source 41
    target 1116
  ]
  edge [
    source 41
    target 1773
  ]
  edge [
    source 41
    target 1774
  ]
  edge [
    source 41
    target 1775
  ]
  edge [
    source 41
    target 1776
  ]
  edge [
    source 41
    target 1777
  ]
  edge [
    source 41
    target 1778
  ]
  edge [
    source 41
    target 1779
  ]
  edge [
    source 41
    target 1780
  ]
  edge [
    source 41
    target 1781
  ]
  edge [
    source 41
    target 1782
  ]
  edge [
    source 41
    target 1783
  ]
  edge [
    source 41
    target 1784
  ]
  edge [
    source 41
    target 1785
  ]
  edge [
    source 41
    target 1786
  ]
  edge [
    source 41
    target 1787
  ]
  edge [
    source 41
    target 1788
  ]
  edge [
    source 41
    target 1789
  ]
  edge [
    source 41
    target 1790
  ]
  edge [
    source 41
    target 1791
  ]
  edge [
    source 41
    target 1792
  ]
  edge [
    source 41
    target 1793
  ]
  edge [
    source 41
    target 1794
  ]
  edge [
    source 41
    target 1795
  ]
  edge [
    source 41
    target 1796
  ]
  edge [
    source 41
    target 1797
  ]
  edge [
    source 41
    target 1798
  ]
  edge [
    source 41
    target 1799
  ]
  edge [
    source 41
    target 1519
  ]
  edge [
    source 41
    target 1800
  ]
  edge [
    source 41
    target 1801
  ]
  edge [
    source 41
    target 1802
  ]
  edge [
    source 41
    target 1803
  ]
  edge [
    source 41
    target 1804
  ]
  edge [
    source 41
    target 1805
  ]
  edge [
    source 41
    target 1379
  ]
  edge [
    source 41
    target 1806
  ]
  edge [
    source 41
    target 1807
  ]
  edge [
    source 41
    target 1808
  ]
  edge [
    source 41
    target 1809
  ]
  edge [
    source 41
    target 1810
  ]
  edge [
    source 41
    target 1811
  ]
  edge [
    source 41
    target 1812
  ]
  edge [
    source 41
    target 1813
  ]
  edge [
    source 41
    target 1814
  ]
  edge [
    source 41
    target 1815
  ]
  edge [
    source 41
    target 1816
  ]
  edge [
    source 41
    target 1817
  ]
  edge [
    source 41
    target 1818
  ]
  edge [
    source 41
    target 1819
  ]
  edge [
    source 41
    target 1820
  ]
  edge [
    source 41
    target 1821
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 1822
  ]
  edge [
    source 41
    target 1534
  ]
  edge [
    source 41
    target 1823
  ]
  edge [
    source 41
    target 1824
  ]
  edge [
    source 41
    target 1825
  ]
  edge [
    source 41
    target 1826
  ]
  edge [
    source 41
    target 1827
  ]
  edge [
    source 41
    target 1828
  ]
  edge [
    source 41
    target 1829
  ]
  edge [
    source 41
    target 1830
  ]
  edge [
    source 41
    target 869
  ]
  edge [
    source 41
    target 1831
  ]
  edge [
    source 41
    target 1403
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1832
  ]
  edge [
    source 43
    target 282
  ]
  edge [
    source 43
    target 1833
  ]
  edge [
    source 43
    target 412
  ]
  edge [
    source 43
    target 413
  ]
  edge [
    source 43
    target 414
  ]
  edge [
    source 43
    target 268
  ]
  edge [
    source 43
    target 415
  ]
  edge [
    source 43
    target 397
  ]
  edge [
    source 43
    target 416
  ]
  edge [
    source 43
    target 417
  ]
  edge [
    source 43
    target 418
  ]
  edge [
    source 43
    target 405
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1381
  ]
  edge [
    source 44
    target 1834
  ]
  edge [
    source 44
    target 1835
  ]
  edge [
    source 44
    target 1525
  ]
  edge [
    source 44
    target 1334
  ]
  edge [
    source 44
    target 1836
  ]
  edge [
    source 44
    target 1837
  ]
  edge [
    source 44
    target 1838
  ]
  edge [
    source 44
    target 1839
  ]
  edge [
    source 44
    target 1371
  ]
  edge [
    source 44
    target 757
  ]
  edge [
    source 44
    target 1840
  ]
  edge [
    source 44
    target 1841
  ]
  edge [
    source 44
    target 1842
  ]
  edge [
    source 44
    target 1843
  ]
  edge [
    source 44
    target 257
  ]
  edge [
    source 44
    target 1844
  ]
  edge [
    source 44
    target 1845
  ]
  edge [
    source 44
    target 831
  ]
  edge [
    source 44
    target 832
  ]
  edge [
    source 44
    target 833
  ]
  edge [
    source 44
    target 834
  ]
  edge [
    source 44
    target 835
  ]
  edge [
    source 44
    target 836
  ]
  edge [
    source 44
    target 136
  ]
  edge [
    source 44
    target 1537
  ]
  edge [
    source 44
    target 1426
  ]
  edge [
    source 44
    target 1538
  ]
  edge [
    source 44
    target 1403
  ]
  edge [
    source 44
    target 1539
  ]
  edge [
    source 44
    target 1540
  ]
  edge [
    source 44
    target 1541
  ]
  edge [
    source 44
    target 1846
  ]
  edge [
    source 44
    target 1847
  ]
  edge [
    source 44
    target 1848
  ]
  edge [
    source 44
    target 1849
  ]
  edge [
    source 44
    target 1850
  ]
  edge [
    source 44
    target 1851
  ]
  edge [
    source 44
    target 1852
  ]
  edge [
    source 44
    target 1853
  ]
  edge [
    source 44
    target 1854
  ]
  edge [
    source 44
    target 1855
  ]
  edge [
    source 44
    target 1856
  ]
  edge [
    source 44
    target 76
  ]
  edge [
    source 44
    target 1857
  ]
  edge [
    source 44
    target 1858
  ]
  edge [
    source 44
    target 1859
  ]
  edge [
    source 44
    target 1860
  ]
  edge [
    source 44
    target 127
  ]
  edge [
    source 44
    target 365
  ]
  edge [
    source 44
    target 1861
  ]
  edge [
    source 44
    target 194
  ]
  edge [
    source 44
    target 1862
  ]
  edge [
    source 44
    target 1773
  ]
  edge [
    source 44
    target 1863
  ]
  edge [
    source 44
    target 1864
  ]
  edge [
    source 44
    target 1865
  ]
  edge [
    source 44
    target 356
  ]
  edge [
    source 44
    target 1866
  ]
  edge [
    source 44
    target 183
  ]
  edge [
    source 44
    target 1867
  ]
  edge [
    source 44
    target 1868
  ]
  edge [
    source 44
    target 1869
  ]
  edge [
    source 44
    target 1870
  ]
  edge [
    source 44
    target 1871
  ]
  edge [
    source 44
    target 1872
  ]
  edge [
    source 44
    target 1873
  ]
  edge [
    source 44
    target 1874
  ]
  edge [
    source 44
    target 1875
  ]
  edge [
    source 44
    target 1080
  ]
  edge [
    source 44
    target 1801
  ]
  edge [
    source 44
    target 1876
  ]
  edge [
    source 44
    target 1877
  ]
  edge [
    source 44
    target 1878
  ]
  edge [
    source 44
    target 1536
  ]
  edge [
    source 44
    target 825
  ]
  edge [
    source 44
    target 754
  ]
  edge [
    source 44
    target 851
  ]
  edge [
    source 44
    target 1378
  ]
  edge [
    source 44
    target 1879
  ]
  edge [
    source 44
    target 1880
  ]
  edge [
    source 44
    target 1881
  ]
  edge [
    source 44
    target 1734
  ]
  edge [
    source 44
    target 1882
  ]
  edge [
    source 44
    target 1625
  ]
  edge [
    source 44
    target 1883
  ]
  edge [
    source 44
    target 1884
  ]
  edge [
    source 44
    target 1885
  ]
  edge [
    source 44
    target 1886
  ]
  edge [
    source 44
    target 1748
  ]
  edge [
    source 44
    target 1887
  ]
  edge [
    source 44
    target 1888
  ]
  edge [
    source 44
    target 1889
  ]
  edge [
    source 44
    target 1890
  ]
  edge [
    source 44
    target 1891
  ]
  edge [
    source 44
    target 1892
  ]
  edge [
    source 44
    target 1673
  ]
  edge [
    source 44
    target 1893
  ]
  edge [
    source 44
    target 1894
  ]
  edge [
    source 44
    target 1895
  ]
  edge [
    source 44
    target 1896
  ]
  edge [
    source 44
    target 1897
  ]
  edge [
    source 44
    target 1898
  ]
  edge [
    source 44
    target 1899
  ]
  edge [
    source 44
    target 933
  ]
  edge [
    source 44
    target 1900
  ]
  edge [
    source 44
    target 1901
  ]
  edge [
    source 44
    target 1902
  ]
  edge [
    source 44
    target 1903
  ]
  edge [
    source 44
    target 840
  ]
  edge [
    source 44
    target 1702
  ]
  edge [
    source 44
    target 1904
  ]
  edge [
    source 44
    target 1905
  ]
  edge [
    source 44
    target 1906
  ]
  edge [
    source 44
    target 1907
  ]
  edge [
    source 44
    target 1908
  ]
  edge [
    source 44
    target 1909
  ]
  edge [
    source 44
    target 1910
  ]
  edge [
    source 44
    target 1911
  ]
  edge [
    source 44
    target 1912
  ]
  edge [
    source 44
    target 1913
  ]
  edge [
    source 44
    target 1914
  ]
  edge [
    source 44
    target 1915
  ]
  edge [
    source 44
    target 1916
  ]
  edge [
    source 44
    target 1917
  ]
  edge [
    source 44
    target 1918
  ]
  edge [
    source 44
    target 1919
  ]
  edge [
    source 44
    target 1920
  ]
  edge [
    source 44
    target 1921
  ]
  edge [
    source 44
    target 672
  ]
  edge [
    source 44
    target 1922
  ]
  edge [
    source 44
    target 1923
  ]
  edge [
    source 44
    target 1924
  ]
  edge [
    source 44
    target 1925
  ]
  edge [
    source 44
    target 1926
  ]
  edge [
    source 44
    target 1927
  ]
  edge [
    source 44
    target 1690
  ]
  edge [
    source 44
    target 1928
  ]
  edge [
    source 44
    target 1929
  ]
  edge [
    source 44
    target 1930
  ]
  edge [
    source 44
    target 1931
  ]
  edge [
    source 44
    target 1932
  ]
  edge [
    source 44
    target 1933
  ]
  edge [
    source 44
    target 993
  ]
  edge [
    source 44
    target 994
  ]
  edge [
    source 44
    target 995
  ]
  edge [
    source 44
    target 996
  ]
  edge [
    source 44
    target 997
  ]
  edge [
    source 44
    target 998
  ]
  edge [
    source 44
    target 999
  ]
  edge [
    source 44
    target 1000
  ]
  edge [
    source 44
    target 1001
  ]
  edge [
    source 44
    target 1002
  ]
  edge [
    source 44
    target 1003
  ]
  edge [
    source 44
    target 1004
  ]
  edge [
    source 44
    target 1005
  ]
  edge [
    source 44
    target 1006
  ]
  edge [
    source 44
    target 1007
  ]
  edge [
    source 44
    target 1008
  ]
  edge [
    source 44
    target 383
  ]
  edge [
    source 44
    target 302
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1463
  ]
  edge [
    source 45
    target 806
  ]
  edge [
    source 45
    target 1455
  ]
  edge [
    source 45
    target 1243
  ]
  edge [
    source 45
    target 1934
  ]
  edge [
    source 45
    target 1935
  ]
  edge [
    source 45
    target 1936
  ]
  edge [
    source 45
    target 1937
  ]
  edge [
    source 45
    target 1938
  ]
  edge [
    source 45
    target 1805
  ]
  edge [
    source 45
    target 784
  ]
  edge [
    source 45
    target 1939
  ]
  edge [
    source 45
    target 1940
  ]
  edge [
    source 45
    target 1371
  ]
  edge [
    source 45
    target 1941
  ]
  edge [
    source 45
    target 1372
  ]
  edge [
    source 45
    target 1942
  ]
  edge [
    source 45
    target 1943
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1944
  ]
  edge [
    source 46
    target 1945
  ]
  edge [
    source 46
    target 1946
  ]
  edge [
    source 46
    target 1947
  ]
  edge [
    source 46
    target 1948
  ]
  edge [
    source 46
    target 1949
  ]
  edge [
    source 46
    target 1549
  ]
  edge [
    source 46
    target 1950
  ]
  edge [
    source 46
    target 1559
  ]
  edge [
    source 46
    target 1951
  ]
  edge [
    source 46
    target 1952
  ]
  edge [
    source 46
    target 1542
  ]
  edge [
    source 46
    target 1953
  ]
  edge [
    source 46
    target 1545
  ]
  edge [
    source 46
    target 1954
  ]
  edge [
    source 46
    target 1955
  ]
  edge [
    source 46
    target 1956
  ]
  edge [
    source 46
    target 1957
  ]
  edge [
    source 46
    target 1564
  ]
  edge [
    source 46
    target 1958
  ]
  edge [
    source 46
    target 1959
  ]
  edge [
    source 46
    target 1960
  ]
  edge [
    source 46
    target 1961
  ]
  edge [
    source 46
    target 889
  ]
  edge [
    source 46
    target 1543
  ]
  edge [
    source 46
    target 1544
  ]
  edge [
    source 46
    target 1546
  ]
  edge [
    source 46
    target 1547
  ]
  edge [
    source 46
    target 1548
  ]
  edge [
    source 46
    target 1550
  ]
  edge [
    source 46
    target 898
  ]
  edge [
    source 46
    target 1962
  ]
  edge [
    source 46
    target 1963
  ]
  edge [
    source 46
    target 1964
  ]
  edge [
    source 46
    target 1965
  ]
  edge [
    source 46
    target 1966
  ]
  edge [
    source 46
    target 1967
  ]
  edge [
    source 46
    target 1968
  ]
  edge [
    source 46
    target 1969
  ]
  edge [
    source 46
    target 1970
  ]
  edge [
    source 46
    target 1971
  ]
  edge [
    source 46
    target 1972
  ]
  edge [
    source 46
    target 1973
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1974
  ]
  edge [
    source 47
    target 733
  ]
  edge [
    source 47
    target 1975
  ]
  edge [
    source 47
    target 1976
  ]
  edge [
    source 47
    target 1977
  ]
  edge [
    source 47
    target 726
  ]
  edge [
    source 47
    target 809
  ]
  edge [
    source 47
    target 1978
  ]
  edge [
    source 47
    target 1979
  ]
  edge [
    source 47
    target 1980
  ]
  edge [
    source 47
    target 1981
  ]
  edge [
    source 47
    target 1982
  ]
  edge [
    source 47
    target 1983
  ]
  edge [
    source 47
    target 1984
  ]
  edge [
    source 47
    target 1985
  ]
  edge [
    source 47
    target 1986
  ]
  edge [
    source 47
    target 1987
  ]
  edge [
    source 47
    target 1988
  ]
  edge [
    source 47
    target 1989
  ]
  edge [
    source 47
    target 732
  ]
  edge [
    source 47
    target 1442
  ]
  edge [
    source 47
    target 1990
  ]
  edge [
    source 47
    target 1991
  ]
  edge [
    source 47
    target 1992
  ]
  edge [
    source 47
    target 1993
  ]
  edge [
    source 47
    target 1994
  ]
  edge [
    source 47
    target 1514
  ]
  edge [
    source 47
    target 1995
  ]
  edge [
    source 47
    target 1996
  ]
  edge [
    source 47
    target 1997
  ]
  edge [
    source 47
    target 720
  ]
  edge [
    source 47
    target 1998
  ]
  edge [
    source 47
    target 1999
  ]
  edge [
    source 47
    target 2000
  ]
  edge [
    source 47
    target 2001
  ]
  edge [
    source 47
    target 2002
  ]
  edge [
    source 47
    target 2003
  ]
  edge [
    source 47
    target 2004
  ]
  edge [
    source 47
    target 2005
  ]
  edge [
    source 47
    target 2006
  ]
  edge [
    source 47
    target 2007
  ]
  edge [
    source 47
    target 2008
  ]
  edge [
    source 47
    target 2009
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 2010
  ]
  edge [
    source 49
    target 2011
  ]
  edge [
    source 49
    target 2012
  ]
  edge [
    source 49
    target 2013
  ]
  edge [
    source 49
    target 2014
  ]
  edge [
    source 49
    target 2015
  ]
  edge [
    source 49
    target 2016
  ]
  edge [
    source 49
    target 2017
  ]
  edge [
    source 49
    target 2018
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 54
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 50
    target 2019
  ]
  edge [
    source 50
    target 2020
  ]
  edge [
    source 50
    target 784
  ]
  edge [
    source 50
    target 2021
  ]
  edge [
    source 50
    target 2022
  ]
  edge [
    source 50
    target 2023
  ]
  edge [
    source 50
    target 2024
  ]
  edge [
    source 50
    target 2025
  ]
  edge [
    source 50
    target 2026
  ]
  edge [
    source 50
    target 781
  ]
  edge [
    source 50
    target 2027
  ]
  edge [
    source 50
    target 1242
  ]
  edge [
    source 50
    target 861
  ]
  edge [
    source 50
    target 183
  ]
  edge [
    source 50
    target 1243
  ]
  edge [
    source 50
    target 1244
  ]
  edge [
    source 50
    target 2028
  ]
  edge [
    source 50
    target 2029
  ]
  edge [
    source 50
    target 2030
  ]
  edge [
    source 50
    target 2031
  ]
  edge [
    source 50
    target 2032
  ]
  edge [
    source 50
    target 2033
  ]
  edge [
    source 50
    target 2034
  ]
  edge [
    source 50
    target 2035
  ]
  edge [
    source 50
    target 2036
  ]
  edge [
    source 50
    target 2037
  ]
  edge [
    source 50
    target 743
  ]
  edge [
    source 50
    target 2038
  ]
  edge [
    source 50
    target 2039
  ]
  edge [
    source 50
    target 2040
  ]
  edge [
    source 50
    target 2041
  ]
  edge [
    source 50
    target 2042
  ]
  edge [
    source 50
    target 2043
  ]
  edge [
    source 50
    target 2044
  ]
  edge [
    source 50
    target 765
  ]
  edge [
    source 50
    target 766
  ]
  edge [
    source 50
    target 2045
  ]
  edge [
    source 50
    target 767
  ]
  edge [
    source 50
    target 1050
  ]
  edge [
    source 50
    target 2046
  ]
  edge [
    source 50
    target 1009
  ]
  edge [
    source 50
    target 379
  ]
  edge [
    source 50
    target 2047
  ]
  edge [
    source 50
    target 771
  ]
  edge [
    source 50
    target 770
  ]
  edge [
    source 50
    target 2048
  ]
  edge [
    source 50
    target 2049
  ]
  edge [
    source 50
    target 2050
  ]
  edge [
    source 50
    target 1139
  ]
  edge [
    source 50
    target 2051
  ]
  edge [
    source 50
    target 775
  ]
  edge [
    source 50
    target 2052
  ]
  edge [
    source 50
    target 2053
  ]
  edge [
    source 50
    target 2054
  ]
  edge [
    source 50
    target 2055
  ]
  edge [
    source 50
    target 776
  ]
  edge [
    source 50
    target 777
  ]
  edge [
    source 50
    target 778
  ]
  edge [
    source 50
    target 779
  ]
  edge [
    source 50
    target 2056
  ]
  edge [
    source 50
    target 2057
  ]
  edge [
    source 50
    target 2058
  ]
  edge [
    source 50
    target 780
  ]
  edge [
    source 50
    target 2059
  ]
  edge [
    source 50
    target 2060
  ]
  edge [
    source 50
    target 2061
  ]
  edge [
    source 50
    target 782
  ]
  edge [
    source 50
    target 2062
  ]
  edge [
    source 50
    target 783
  ]
  edge [
    source 50
    target 316
  ]
  edge [
    source 50
    target 2063
  ]
  edge [
    source 50
    target 785
  ]
  edge [
    source 50
    target 422
  ]
  edge [
    source 50
    target 2064
  ]
  edge [
    source 50
    target 773
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 2031
  ]
  edge [
    source 51
    target 2032
  ]
  edge [
    source 51
    target 2033
  ]
  edge [
    source 51
    target 183
  ]
  edge [
    source 51
    target 2034
  ]
  edge [
    source 51
    target 2035
  ]
  edge [
    source 51
    target 2036
  ]
  edge [
    source 51
    target 2037
  ]
  edge [
    source 51
    target 743
  ]
  edge [
    source 51
    target 2038
  ]
  edge [
    source 51
    target 2039
  ]
  edge [
    source 51
    target 2040
  ]
  edge [
    source 51
    target 2041
  ]
  edge [
    source 51
    target 2042
  ]
  edge [
    source 51
    target 1136
  ]
  edge [
    source 51
    target 1633
  ]
  edge [
    source 51
    target 1634
  ]
  edge [
    source 51
    target 1635
  ]
  edge [
    source 51
    target 1636
  ]
  edge [
    source 51
    target 1637
  ]
  edge [
    source 51
    target 1638
  ]
  edge [
    source 51
    target 1639
  ]
  edge [
    source 51
    target 1640
  ]
  edge [
    source 51
    target 846
  ]
  edge [
    source 51
    target 1641
  ]
  edge [
    source 51
    target 1642
  ]
  edge [
    source 51
    target 1643
  ]
  edge [
    source 51
    target 850
  ]
  edge [
    source 51
    target 1644
  ]
  edge [
    source 51
    target 2065
  ]
  edge [
    source 51
    target 73
  ]
  edge [
    source 51
    target 765
  ]
  edge [
    source 51
    target 766
  ]
  edge [
    source 51
    target 767
  ]
  edge [
    source 51
    target 768
  ]
  edge [
    source 51
    target 769
  ]
  edge [
    source 51
    target 770
  ]
  edge [
    source 51
    target 771
  ]
  edge [
    source 51
    target 772
  ]
  edge [
    source 51
    target 773
  ]
  edge [
    source 51
    target 774
  ]
  edge [
    source 51
    target 775
  ]
  edge [
    source 51
    target 776
  ]
  edge [
    source 51
    target 777
  ]
  edge [
    source 51
    target 778
  ]
  edge [
    source 51
    target 779
  ]
  edge [
    source 51
    target 780
  ]
  edge [
    source 51
    target 781
  ]
  edge [
    source 51
    target 782
  ]
  edge [
    source 51
    target 783
  ]
  edge [
    source 51
    target 784
  ]
  edge [
    source 51
    target 422
  ]
  edge [
    source 51
    target 785
  ]
  edge [
    source 51
    target 786
  ]
  edge [
    source 51
    target 787
  ]
  edge [
    source 51
    target 788
  ]
  edge [
    source 51
    target 789
  ]
  edge [
    source 51
    target 2019
  ]
  edge [
    source 51
    target 2020
  ]
  edge [
    source 51
    target 2021
  ]
  edge [
    source 51
    target 2022
  ]
  edge [
    source 51
    target 2023
  ]
  edge [
    source 51
    target 2024
  ]
  edge [
    source 51
    target 2025
  ]
  edge [
    source 51
    target 2026
  ]
  edge [
    source 51
    target 268
  ]
  edge [
    source 51
    target 2223
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 149
  ]
  edge [
    source 52
    target 195
  ]
  edge [
    source 52
    target 2066
  ]
  edge [
    source 52
    target 197
  ]
  edge [
    source 52
    target 68
  ]
  edge [
    source 52
    target 199
  ]
  edge [
    source 52
    target 2067
  ]
  edge [
    source 52
    target 2068
  ]
  edge [
    source 52
    target 210
  ]
  edge [
    source 52
    target 2069
  ]
  edge [
    source 52
    target 2070
  ]
  edge [
    source 52
    target 202
  ]
  edge [
    source 52
    target 2071
  ]
  edge [
    source 52
    target 2072
  ]
  edge [
    source 52
    target 127
  ]
  edge [
    source 52
    target 2073
  ]
  edge [
    source 52
    target 2074
  ]
  edge [
    source 52
    target 2075
  ]
  edge [
    source 52
    target 2076
  ]
  edge [
    source 52
    target 2077
  ]
  edge [
    source 52
    target 2078
  ]
  edge [
    source 52
    target 2079
  ]
  edge [
    source 52
    target 2080
  ]
  edge [
    source 52
    target 1052
  ]
  edge [
    source 52
    target 2081
  ]
  edge [
    source 52
    target 2082
  ]
  edge [
    source 52
    target 2083
  ]
  edge [
    source 52
    target 2084
  ]
  edge [
    source 52
    target 2085
  ]
  edge [
    source 52
    target 2086
  ]
  edge [
    source 52
    target 2087
  ]
  edge [
    source 52
    target 2088
  ]
  edge [
    source 52
    target 2089
  ]
  edge [
    source 52
    target 2090
  ]
  edge [
    source 52
    target 2091
  ]
  edge [
    source 52
    target 2092
  ]
  edge [
    source 52
    target 2093
  ]
  edge [
    source 52
    target 2094
  ]
  edge [
    source 52
    target 2095
  ]
  edge [
    source 52
    target 839
  ]
  edge [
    source 52
    target 236
  ]
  edge [
    source 52
    target 2096
  ]
  edge [
    source 52
    target 2097
  ]
  edge [
    source 52
    target 2098
  ]
  edge [
    source 52
    target 2099
  ]
  edge [
    source 52
    target 809
  ]
  edge [
    source 52
    target 2100
  ]
  edge [
    source 52
    target 2101
  ]
  edge [
    source 52
    target 2102
  ]
  edge [
    source 52
    target 2103
  ]
  edge [
    source 52
    target 67
  ]
  edge [
    source 52
    target 1700
  ]
  edge [
    source 52
    target 1701
  ]
  edge [
    source 52
    target 1009
  ]
  edge [
    source 52
    target 784
  ]
  edge [
    source 52
    target 1702
  ]
  edge [
    source 52
    target 1703
  ]
  edge [
    source 52
    target 1704
  ]
  edge [
    source 52
    target 1705
  ]
  edge [
    source 52
    target 1706
  ]
  edge [
    source 52
    target 410
  ]
  edge [
    source 52
    target 1707
  ]
  edge [
    source 52
    target 1708
  ]
  edge [
    source 52
    target 1709
  ]
  edge [
    source 52
    target 1710
  ]
  edge [
    source 52
    target 1711
  ]
  edge [
    source 52
    target 1712
  ]
  edge [
    source 52
    target 856
  ]
  edge [
    source 52
    target 2104
  ]
  edge [
    source 52
    target 2105
  ]
  edge [
    source 52
    target 1120
  ]
  edge [
    source 52
    target 1048
  ]
  edge [
    source 52
    target 2106
  ]
  edge [
    source 52
    target 1403
  ]
  edge [
    source 52
    target 2107
  ]
  edge [
    source 52
    target 2108
  ]
  edge [
    source 52
    target 2109
  ]
  edge [
    source 52
    target 1499
  ]
  edge [
    source 52
    target 1189
  ]
  edge [
    source 52
    target 2110
  ]
  edge [
    source 52
    target 2111
  ]
  edge [
    source 52
    target 1748
  ]
  edge [
    source 52
    target 1749
  ]
  edge [
    source 52
    target 1750
  ]
  edge [
    source 52
    target 1034
  ]
  edge [
    source 52
    target 76
  ]
  edge [
    source 52
    target 1751
  ]
  edge [
    source 52
    target 1752
  ]
  edge [
    source 52
    target 1753
  ]
  edge [
    source 52
    target 1040
  ]
  edge [
    source 52
    target 1754
  ]
  edge [
    source 52
    target 1755
  ]
  edge [
    source 52
    target 1756
  ]
  edge [
    source 52
    target 1181
  ]
  edge [
    source 52
    target 1757
  ]
  edge [
    source 52
    target 1758
  ]
  edge [
    source 52
    target 1759
  ]
  edge [
    source 52
    target 1088
  ]
  edge [
    source 52
    target 136
  ]
  edge [
    source 52
    target 999
  ]
  edge [
    source 52
    target 1760
  ]
  edge [
    source 52
    target 1761
  ]
  edge [
    source 52
    target 1762
  ]
  edge [
    source 52
    target 1763
  ]
  edge [
    source 52
    target 1734
  ]
  edge [
    source 52
    target 2112
  ]
  edge [
    source 52
    target 2113
  ]
  edge [
    source 52
    target 2114
  ]
  edge [
    source 52
    target 2115
  ]
  edge [
    source 52
    target 2116
  ]
  edge [
    source 52
    target 2117
  ]
  edge [
    source 52
    target 2118
  ]
  edge [
    source 52
    target 2119
  ]
  edge [
    source 52
    target 2120
  ]
  edge [
    source 52
    target 2121
  ]
  edge [
    source 52
    target 2122
  ]
  edge [
    source 52
    target 2123
  ]
  edge [
    source 52
    target 2124
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 2125
  ]
  edge [
    source 53
    target 2126
  ]
  edge [
    source 53
    target 2127
  ]
  edge [
    source 53
    target 2128
  ]
  edge [
    source 53
    target 1272
  ]
  edge [
    source 53
    target 2129
  ]
  edge [
    source 53
    target 1267
  ]
  edge [
    source 53
    target 1268
  ]
  edge [
    source 53
    target 1269
  ]
  edge [
    source 53
    target 1270
  ]
  edge [
    source 53
    target 1271
  ]
  edge [
    source 53
    target 2223
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 2130
  ]
  edge [
    source 55
    target 2131
  ]
  edge [
    source 55
    target 2132
  ]
  edge [
    source 55
    target 2133
  ]
  edge [
    source 55
    target 2223
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 2134
  ]
  edge [
    source 56
    target 1246
  ]
  edge [
    source 56
    target 1117
  ]
  edge [
    source 56
    target 2135
  ]
  edge [
    source 56
    target 210
  ]
  edge [
    source 56
    target 1011
  ]
  edge [
    source 56
    target 261
  ]
  edge [
    source 56
    target 262
  ]
  edge [
    source 56
    target 2094
  ]
  edge [
    source 56
    target 2095
  ]
  edge [
    source 56
    target 839
  ]
  edge [
    source 56
    target 236
  ]
  edge [
    source 56
    target 2096
  ]
  edge [
    source 56
    target 2097
  ]
  edge [
    source 56
    target 2098
  ]
  edge [
    source 56
    target 2099
  ]
  edge [
    source 56
    target 809
  ]
  edge [
    source 56
    target 2100
  ]
  edge [
    source 56
    target 127
  ]
  edge [
    source 56
    target 2101
  ]
  edge [
    source 56
    target 2136
  ]
  edge [
    source 56
    target 295
  ]
  edge [
    source 56
    target 2137
  ]
  edge [
    source 56
    target 342
  ]
  edge [
    source 56
    target 2138
  ]
  edge [
    source 56
    target 2139
  ]
  edge [
    source 56
    target 2140
  ]
  edge [
    source 56
    target 2141
  ]
  edge [
    source 56
    target 2142
  ]
  edge [
    source 56
    target 2143
  ]
  edge [
    source 56
    target 235
  ]
  edge [
    source 56
    target 2144
  ]
  edge [
    source 56
    target 856
  ]
  edge [
    source 56
    target 183
  ]
  edge [
    source 56
    target 2145
  ]
  edge [
    source 56
    target 2146
  ]
  edge [
    source 56
    target 1152
  ]
  edge [
    source 57
    target 2147
  ]
  edge [
    source 57
    target 2148
  ]
  edge [
    source 57
    target 2149
  ]
  edge [
    source 57
    target 2150
  ]
  edge [
    source 57
    target 2151
  ]
  edge [
    source 57
    target 2152
  ]
  edge [
    source 57
    target 2153
  ]
  edge [
    source 57
    target 2154
  ]
  edge [
    source 57
    target 2155
  ]
  edge [
    source 58
    target 2156
  ]
  edge [
    source 58
    target 2157
  ]
  edge [
    source 58
    target 1989
  ]
  edge [
    source 58
    target 1442
  ]
  edge [
    source 58
    target 2158
  ]
  edge [
    source 58
    target 1440
  ]
  edge [
    source 58
    target 798
  ]
  edge [
    source 58
    target 228
  ]
  edge [
    source 58
    target 820
  ]
  edge [
    source 58
    target 812
  ]
  edge [
    source 58
    target 2159
  ]
  edge [
    source 58
    target 811
  ]
  edge [
    source 58
    target 1441
  ]
  edge [
    source 58
    target 2160
  ]
  edge [
    source 58
    target 2161
  ]
  edge [
    source 58
    target 2162
  ]
  edge [
    source 58
    target 222
  ]
  edge [
    source 58
    target 2163
  ]
  edge [
    source 58
    target 2164
  ]
  edge [
    source 58
    target 705
  ]
  edge [
    source 58
    target 2165
  ]
  edge [
    source 58
    target 2166
  ]
  edge [
    source 58
    target 2167
  ]
  edge [
    source 58
    target 142
  ]
  edge [
    source 58
    target 2168
  ]
  edge [
    source 58
    target 2169
  ]
  edge [
    source 58
    target 2170
  ]
  edge [
    source 58
    target 2171
  ]
  edge [
    source 58
    target 1154
  ]
  edge [
    source 58
    target 232
  ]
  edge [
    source 58
    target 815
  ]
  edge [
    source 58
    target 2172
  ]
  edge [
    source 58
    target 2173
  ]
  edge [
    source 58
    target 2174
  ]
  edge [
    source 58
    target 2175
  ]
  edge [
    source 58
    target 2176
  ]
  edge [
    source 59
    target 820
  ]
  edge [
    source 59
    target 806
  ]
  edge [
    source 59
    target 1355
  ]
  edge [
    source 59
    target 705
  ]
  edge [
    source 59
    target 2166
  ]
  edge [
    source 59
    target 2177
  ]
  edge [
    source 59
    target 2178
  ]
  edge [
    source 59
    target 232
  ]
  edge [
    source 59
    target 222
  ]
  edge [
    source 59
    target 2179
  ]
  edge [
    source 59
    target 2180
  ]
  edge [
    source 59
    target 2181
  ]
  edge [
    source 59
    target 2182
  ]
  edge [
    source 59
    target 1394
  ]
  edge [
    source 59
    target 2183
  ]
  edge [
    source 59
    target 2184
  ]
  edge [
    source 59
    target 2185
  ]
  edge [
    source 59
    target 2186
  ]
  edge [
    source 59
    target 2187
  ]
  edge [
    source 59
    target 1182
  ]
  edge [
    source 59
    target 2188
  ]
  edge [
    source 59
    target 1701
  ]
  edge [
    source 59
    target 2189
  ]
  edge [
    source 59
    target 812
  ]
  edge [
    source 59
    target 2190
  ]
  edge [
    source 59
    target 2161
  ]
  edge [
    source 59
    target 2156
  ]
  edge [
    source 59
    target 2191
  ]
  edge [
    source 59
    target 2173
  ]
  edge [
    source 59
    target 2192
  ]
  edge [
    source 59
    target 2193
  ]
  edge [
    source 59
    target 2194
  ]
  edge [
    source 59
    target 2195
  ]
  edge [
    source 59
    target 2163
  ]
  edge [
    source 59
    target 2196
  ]
  edge [
    source 59
    target 2197
  ]
  edge [
    source 59
    target 2168
  ]
  edge [
    source 59
    target 2198
  ]
  edge [
    source 59
    target 2199
  ]
  edge [
    source 59
    target 2169
  ]
  edge [
    source 59
    target 2200
  ]
  edge [
    source 59
    target 1448
  ]
  edge [
    source 59
    target 2201
  ]
  edge [
    source 59
    target 1474
  ]
  edge [
    source 59
    target 2202
  ]
  edge [
    source 59
    target 2203
  ]
  edge [
    source 59
    target 2204
  ]
  edge [
    source 59
    target 2205
  ]
  edge [
    source 59
    target 2206
  ]
  edge [
    source 59
    target 822
  ]
  edge [
    source 59
    target 2207
  ]
  edge [
    source 59
    target 2208
  ]
  edge [
    source 59
    target 2209
  ]
  edge [
    source 59
    target 2210
  ]
  edge [
    source 59
    target 2211
  ]
  edge [
    source 59
    target 2212
  ]
  edge [
    source 59
    target 2213
  ]
  edge [
    source 59
    target 2214
  ]
  edge [
    source 59
    target 757
  ]
  edge [
    source 59
    target 2215
  ]
  edge [
    source 59
    target 2216
  ]
  edge [
    source 59
    target 2217
  ]
  edge [
    source 59
    target 2218
  ]
  edge [
    source 59
    target 2219
  ]
  edge [
    source 59
    target 2220
  ]
  edge [
    source 59
    target 188
  ]
  edge [
    source 59
    target 2221
  ]
  edge [
    source 59
    target 2222
  ]
  edge [
    source 59
    target 809
  ]
  edge [
    source 59
    target 2223
  ]
  edge [
    source 59
    target 2224
  ]
  edge [
    source 59
    target 2225
  ]
  edge [
    source 59
    target 2226
  ]
  edge [
    source 59
    target 2227
  ]
  edge [
    source 59
    target 2228
  ]
  edge [
    source 59
    target 2229
  ]
  edge [
    source 59
    target 814
  ]
  edge [
    source 59
    target 2230
  ]
  edge [
    source 59
    target 228
  ]
  edge [
    source 59
    target 2231
  ]
  edge [
    source 59
    target 2232
  ]
  edge [
    source 59
    target 2233
  ]
  edge [
    source 59
    target 2234
  ]
  edge [
    source 59
    target 2235
  ]
  edge [
    source 59
    target 2236
  ]
  edge [
    source 59
    target 2237
  ]
  edge [
    source 59
    target 804
  ]
  edge [
    source 59
    target 2238
  ]
  edge [
    source 59
    target 697
  ]
  edge [
    source 59
    target 2239
  ]
  edge [
    source 59
    target 2240
  ]
  edge [
    source 59
    target 2241
  ]
  edge [
    source 59
    target 2242
  ]
  edge [
    source 59
    target 2243
  ]
  edge [
    source 59
    target 2244
  ]
  edge [
    source 59
    target 1935
  ]
  edge [
    source 59
    target 2245
  ]
  edge [
    source 59
    target 2158
  ]
  edge [
    source 59
    target 2175
  ]
  edge [
    source 59
    target 2246
  ]
  edge [
    source 59
    target 2157
  ]
  edge [
    source 59
    target 2247
  ]
  edge [
    source 59
    target 2248
  ]
  edge [
    source 59
    target 1344
  ]
  edge [
    source 59
    target 2123
  ]
  edge [
    source 59
    target 2249
  ]
  edge [
    source 59
    target 700
  ]
  edge [
    source 59
    target 2250
  ]
  edge [
    source 59
    target 2251
  ]
  edge [
    source 59
    target 2252
  ]
  edge [
    source 59
    target 2253
  ]
  edge [
    source 59
    target 704
  ]
  edge [
    source 59
    target 707
  ]
  edge [
    source 59
    target 2254
  ]
  edge [
    source 59
    target 709
  ]
  edge [
    source 59
    target 2255
  ]
  edge [
    source 59
    target 2256
  ]
  edge [
    source 59
    target 170
  ]
  edge [
    source 59
    target 2257
  ]
  edge [
    source 59
    target 2258
  ]
  edge [
    source 59
    target 721
  ]
  edge [
    source 59
    target 2259
  ]
  edge [
    source 59
    target 1128
  ]
  edge [
    source 59
    target 2260
  ]
  edge [
    source 59
    target 2261
  ]
  edge [
    source 59
    target 2262
  ]
  edge [
    source 59
    target 2263
  ]
  edge [
    source 59
    target 2264
  ]
  edge [
    source 59
    target 2094
  ]
  edge [
    source 59
    target 183
  ]
  edge [
    source 59
    target 2265
  ]
  edge [
    source 59
    target 2266
  ]
  edge [
    source 59
    target 2267
  ]
  edge [
    source 59
    target 2268
  ]
  edge [
    source 59
    target 2269
  ]
  edge [
    source 59
    target 2145
  ]
  edge [
    source 59
    target 2146
  ]
  edge [
    source 59
    target 2270
  ]
  edge [
    source 59
    target 2098
  ]
  edge [
    source 59
    target 2271
  ]
  edge [
    source 59
    target 1393
  ]
  edge [
    source 59
    target 72
  ]
  edge [
    source 59
    target 1395
  ]
  edge [
    source 59
    target 2272
  ]
  edge [
    source 59
    target 2273
  ]
  edge [
    source 59
    target 784
  ]
  edge [
    source 59
    target 2274
  ]
  edge [
    source 59
    target 68
  ]
  edge [
    source 59
    target 2275
  ]
  edge [
    source 59
    target 2276
  ]
  edge [
    source 59
    target 2277
  ]
  edge [
    source 59
    target 2278
  ]
  edge [
    source 59
    target 2279
  ]
  edge [
    source 59
    target 203
  ]
  edge [
    source 59
    target 2280
  ]
  edge [
    source 59
    target 2281
  ]
  edge [
    source 59
    target 2282
  ]
  edge [
    source 59
    target 2283
  ]
  edge [
    source 59
    target 2284
  ]
  edge [
    source 59
    target 2285
  ]
  edge [
    source 59
    target 2286
  ]
  edge [
    source 59
    target 2287
  ]
  edge [
    source 59
    target 2288
  ]
  edge [
    source 59
    target 2289
  ]
  edge [
    source 59
    target 2290
  ]
  edge [
    source 59
    target 2291
  ]
  edge [
    source 59
    target 2292
  ]
  edge [
    source 59
    target 2293
  ]
  edge [
    source 59
    target 2294
  ]
  edge [
    source 59
    target 2295
  ]
  edge [
    source 59
    target 2296
  ]
  edge [
    source 59
    target 2297
  ]
  edge [
    source 59
    target 2298
  ]
  edge [
    source 59
    target 2299
  ]
  edge [
    source 59
    target 755
  ]
  edge [
    source 59
    target 2300
  ]
  edge [
    source 59
    target 2301
  ]
  edge [
    source 59
    target 2302
  ]
  edge [
    source 59
    target 2303
  ]
  edge [
    source 59
    target 2304
  ]
  edge [
    source 59
    target 2305
  ]
  edge [
    source 59
    target 2306
  ]
  edge [
    source 59
    target 2307
  ]
  edge [
    source 59
    target 2308
  ]
  edge [
    source 59
    target 2309
  ]
  edge [
    source 59
    target 2310
  ]
  edge [
    source 59
    target 2311
  ]
  edge [
    source 59
    target 2312
  ]
  edge [
    source 59
    target 2313
  ]
  edge [
    source 59
    target 2314
  ]
  edge [
    source 59
    target 2315
  ]
  edge [
    source 59
    target 2316
  ]
  edge [
    source 59
    target 2317
  ]
  edge [
    source 59
    target 2318
  ]
  edge [
    source 59
    target 2319
  ]
  edge [
    source 59
    target 2320
  ]
  edge [
    source 59
    target 2321
  ]
  edge [
    source 59
    target 2322
  ]
  edge [
    source 59
    target 2323
  ]
  edge [
    source 59
    target 2324
  ]
  edge [
    source 59
    target 2325
  ]
  edge [
    source 59
    target 2326
  ]
  edge [
    source 59
    target 2327
  ]
  edge [
    source 59
    target 2328
  ]
  edge [
    source 60
    target 2329
  ]
  edge [
    source 60
    target 2330
  ]
  edge [
    source 60
    target 1217
  ]
  edge [
    source 60
    target 2088
  ]
  edge [
    source 60
    target 2331
  ]
  edge [
    source 60
    target 2332
  ]
  edge [
    source 60
    target 2333
  ]
  edge [
    source 60
    target 2334
  ]
  edge [
    source 60
    target 2335
  ]
  edge [
    source 60
    target 2336
  ]
  edge [
    source 60
    target 2337
  ]
  edge [
    source 60
    target 2338
  ]
  edge [
    source 60
    target 2339
  ]
  edge [
    source 60
    target 2340
  ]
  edge [
    source 60
    target 2341
  ]
  edge [
    source 60
    target 2342
  ]
  edge [
    source 60
    target 2343
  ]
  edge [
    source 60
    target 2344
  ]
  edge [
    source 60
    target 2345
  ]
  edge [
    source 60
    target 432
  ]
  edge [
    source 60
    target 210
  ]
  edge [
    source 60
    target 2346
  ]
  edge [
    source 60
    target 2347
  ]
  edge [
    source 60
    target 2348
  ]
  edge [
    source 60
    target 2349
  ]
  edge [
    source 60
    target 2350
  ]
  edge [
    source 60
    target 2351
  ]
  edge [
    source 60
    target 2352
  ]
  edge [
    source 60
    target 2353
  ]
  edge [
    source 60
    target 245
  ]
  edge [
    source 60
    target 2354
  ]
  edge [
    source 60
    target 236
  ]
  edge [
    source 60
    target 2355
  ]
  edge [
    source 60
    target 2356
  ]
  edge [
    source 60
    target 2357
  ]
  edge [
    source 60
    target 2358
  ]
  edge [
    source 60
    target 2359
  ]
  edge [
    source 60
    target 2360
  ]
  edge [
    source 60
    target 2361
  ]
  edge [
    source 60
    target 2362
  ]
  edge [
    source 60
    target 2363
  ]
  edge [
    source 60
    target 2364
  ]
  edge [
    source 60
    target 2365
  ]
  edge [
    source 60
    target 92
  ]
  edge [
    source 60
    target 2366
  ]
  edge [
    source 60
    target 2367
  ]
  edge [
    source 60
    target 2368
  ]
  edge [
    source 60
    target 96
  ]
  edge [
    source 60
    target 2369
  ]
  edge [
    source 60
    target 2370
  ]
  edge [
    source 60
    target 2371
  ]
  edge [
    source 60
    target 2372
  ]
  edge [
    source 60
    target 2373
  ]
  edge [
    source 60
    target 2374
  ]
  edge [
    source 60
    target 2375
  ]
  edge [
    source 60
    target 106
  ]
  edge [
    source 60
    target 2376
  ]
  edge [
    source 60
    target 1214
  ]
  edge [
    source 60
    target 2377
  ]
  edge [
    source 60
    target 2378
  ]
  edge [
    source 60
    target 2379
  ]
  edge [
    source 60
    target 2380
  ]
  edge [
    source 60
    target 109
  ]
  edge [
    source 60
    target 2381
  ]
  edge [
    source 60
    target 2382
  ]
  edge [
    source 60
    target 2383
  ]
  edge [
    source 60
    target 2384
  ]
  edge [
    source 60
    target 112
  ]
  edge [
    source 60
    target 2385
  ]
  edge [
    source 60
    target 2386
  ]
  edge [
    source 60
    target 2387
  ]
  edge [
    source 60
    target 2388
  ]
  edge [
    source 60
    target 115
  ]
  edge [
    source 60
    target 1201
  ]
  edge [
    source 60
    target 2389
  ]
  edge [
    source 60
    target 2390
  ]
  edge [
    source 60
    target 2391
  ]
  edge [
    source 60
    target 121
  ]
  edge [
    source 60
    target 2392
  ]
  edge [
    source 60
    target 2393
  ]
  edge [
    source 60
    target 856
  ]
  edge [
    source 1034
    target 2403
  ]
  edge [
    source 1034
    target 2404
  ]
  edge [
    source 2395
    target 2396
  ]
  edge [
    source 2397
    target 2398
  ]
  edge [
    source 2399
    target 2400
  ]
  edge [
    source 2401
    target 2402
  ]
  edge [
    source 2403
    target 2404
  ]
  edge [
    source 2406
    target 2407
  ]
  edge [
    source 2408
    target 2409
  ]
]
