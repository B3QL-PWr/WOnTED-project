graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "zdrowie"
    origin "text"
  ]
  node [
    id 4
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 5
    label "poz"
    origin "text"
  ]
  node [
    id 6
    label "program_informacyjny"
  ]
  node [
    id 7
    label "journal"
  ]
  node [
    id 8
    label "diariusz"
  ]
  node [
    id 9
    label "spis"
  ]
  node [
    id 10
    label "ksi&#281;ga"
  ]
  node [
    id 11
    label "sheet"
  ]
  node [
    id 12
    label "pami&#281;tnik"
  ]
  node [
    id 13
    label "gazeta"
  ]
  node [
    id 14
    label "tytu&#322;"
  ]
  node [
    id 15
    label "redakcja"
  ]
  node [
    id 16
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 17
    label "czasopismo"
  ]
  node [
    id 18
    label "prasa"
  ]
  node [
    id 19
    label "rozdzia&#322;"
  ]
  node [
    id 20
    label "pismo"
  ]
  node [
    id 21
    label "Ewangelia"
  ]
  node [
    id 22
    label "book"
  ]
  node [
    id 23
    label "dokument"
  ]
  node [
    id 24
    label "tome"
  ]
  node [
    id 25
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 26
    label "pami&#261;tka"
  ]
  node [
    id 27
    label "notes"
  ]
  node [
    id 28
    label "zapiski"
  ]
  node [
    id 29
    label "raptularz"
  ]
  node [
    id 30
    label "album"
  ]
  node [
    id 31
    label "utw&#243;r_epicki"
  ]
  node [
    id 32
    label "zbi&#243;r"
  ]
  node [
    id 33
    label "catalog"
  ]
  node [
    id 34
    label "pozycja"
  ]
  node [
    id 35
    label "akt"
  ]
  node [
    id 36
    label "tekst"
  ]
  node [
    id 37
    label "sumariusz"
  ]
  node [
    id 38
    label "stock"
  ]
  node [
    id 39
    label "figurowa&#263;"
  ]
  node [
    id 40
    label "czynno&#347;&#263;"
  ]
  node [
    id 41
    label "wyliczanka"
  ]
  node [
    id 42
    label "oficjalny"
  ]
  node [
    id 43
    label "urz&#281;dowo"
  ]
  node [
    id 44
    label "formalny"
  ]
  node [
    id 45
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 46
    label "formalizowanie"
  ]
  node [
    id 47
    label "formalnie"
  ]
  node [
    id 48
    label "oficjalnie"
  ]
  node [
    id 49
    label "jawny"
  ]
  node [
    id 50
    label "legalny"
  ]
  node [
    id 51
    label "sformalizowanie"
  ]
  node [
    id 52
    label "pozorny"
  ]
  node [
    id 53
    label "kompletny"
  ]
  node [
    id 54
    label "prawdziwy"
  ]
  node [
    id 55
    label "prawomocny"
  ]
  node [
    id 56
    label "dostojnik"
  ]
  node [
    id 57
    label "Goebbels"
  ]
  node [
    id 58
    label "Sto&#322;ypin"
  ]
  node [
    id 59
    label "rz&#261;d"
  ]
  node [
    id 60
    label "przybli&#380;enie"
  ]
  node [
    id 61
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 62
    label "kategoria"
  ]
  node [
    id 63
    label "szpaler"
  ]
  node [
    id 64
    label "lon&#380;a"
  ]
  node [
    id 65
    label "uporz&#261;dkowanie"
  ]
  node [
    id 66
    label "egzekutywa"
  ]
  node [
    id 67
    label "jednostka_systematyczna"
  ]
  node [
    id 68
    label "instytucja"
  ]
  node [
    id 69
    label "premier"
  ]
  node [
    id 70
    label "Londyn"
  ]
  node [
    id 71
    label "gabinet_cieni"
  ]
  node [
    id 72
    label "gromada"
  ]
  node [
    id 73
    label "number"
  ]
  node [
    id 74
    label "Konsulat"
  ]
  node [
    id 75
    label "tract"
  ]
  node [
    id 76
    label "klasa"
  ]
  node [
    id 77
    label "w&#322;adza"
  ]
  node [
    id 78
    label "urz&#281;dnik"
  ]
  node [
    id 79
    label "notabl"
  ]
  node [
    id 80
    label "oficja&#322;"
  ]
  node [
    id 81
    label "os&#322;abianie"
  ]
  node [
    id 82
    label "kondycja"
  ]
  node [
    id 83
    label "os&#322;abienie"
  ]
  node [
    id 84
    label "zniszczenie"
  ]
  node [
    id 85
    label "os&#322;abia&#263;"
  ]
  node [
    id 86
    label "zedrze&#263;"
  ]
  node [
    id 87
    label "niszczenie"
  ]
  node [
    id 88
    label "os&#322;abi&#263;"
  ]
  node [
    id 89
    label "soundness"
  ]
  node [
    id 90
    label "stan"
  ]
  node [
    id 91
    label "niszczy&#263;"
  ]
  node [
    id 92
    label "zniszczy&#263;"
  ]
  node [
    id 93
    label "zdarcie"
  ]
  node [
    id 94
    label "firmness"
  ]
  node [
    id 95
    label "cecha"
  ]
  node [
    id 96
    label "rozsypanie_si&#281;"
  ]
  node [
    id 97
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 98
    label "charakterystyka"
  ]
  node [
    id 99
    label "m&#322;ot"
  ]
  node [
    id 100
    label "znak"
  ]
  node [
    id 101
    label "drzewo"
  ]
  node [
    id 102
    label "pr&#243;ba"
  ]
  node [
    id 103
    label "attribute"
  ]
  node [
    id 104
    label "marka"
  ]
  node [
    id 105
    label "Ohio"
  ]
  node [
    id 106
    label "wci&#281;cie"
  ]
  node [
    id 107
    label "Nowy_York"
  ]
  node [
    id 108
    label "warstwa"
  ]
  node [
    id 109
    label "samopoczucie"
  ]
  node [
    id 110
    label "Illinois"
  ]
  node [
    id 111
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 112
    label "state"
  ]
  node [
    id 113
    label "Jukatan"
  ]
  node [
    id 114
    label "Kalifornia"
  ]
  node [
    id 115
    label "Wirginia"
  ]
  node [
    id 116
    label "wektor"
  ]
  node [
    id 117
    label "by&#263;"
  ]
  node [
    id 118
    label "Goa"
  ]
  node [
    id 119
    label "Teksas"
  ]
  node [
    id 120
    label "Waszyngton"
  ]
  node [
    id 121
    label "miejsce"
  ]
  node [
    id 122
    label "Massachusetts"
  ]
  node [
    id 123
    label "Alaska"
  ]
  node [
    id 124
    label "Arakan"
  ]
  node [
    id 125
    label "Hawaje"
  ]
  node [
    id 126
    label "Maryland"
  ]
  node [
    id 127
    label "punkt"
  ]
  node [
    id 128
    label "Michigan"
  ]
  node [
    id 129
    label "Arizona"
  ]
  node [
    id 130
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 131
    label "Georgia"
  ]
  node [
    id 132
    label "poziom"
  ]
  node [
    id 133
    label "Pensylwania"
  ]
  node [
    id 134
    label "shape"
  ]
  node [
    id 135
    label "Luizjana"
  ]
  node [
    id 136
    label "Nowy_Meksyk"
  ]
  node [
    id 137
    label "Alabama"
  ]
  node [
    id 138
    label "ilo&#347;&#263;"
  ]
  node [
    id 139
    label "Kansas"
  ]
  node [
    id 140
    label "Oregon"
  ]
  node [
    id 141
    label "Oklahoma"
  ]
  node [
    id 142
    label "Floryda"
  ]
  node [
    id 143
    label "jednostka_administracyjna"
  ]
  node [
    id 144
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 145
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 146
    label "dyspozycja"
  ]
  node [
    id 147
    label "situation"
  ]
  node [
    id 148
    label "rank"
  ]
  node [
    id 149
    label "zdolno&#347;&#263;"
  ]
  node [
    id 150
    label "sytuacja"
  ]
  node [
    id 151
    label "doznanie"
  ]
  node [
    id 152
    label "fatigue_duty"
  ]
  node [
    id 153
    label "zmniejszenie"
  ]
  node [
    id 154
    label "kondycja_fizyczna"
  ]
  node [
    id 155
    label "spowodowanie"
  ]
  node [
    id 156
    label "infirmity"
  ]
  node [
    id 157
    label "s&#322;abszy"
  ]
  node [
    id 158
    label "pogorszenie"
  ]
  node [
    id 159
    label "suppress"
  ]
  node [
    id 160
    label "robi&#263;"
  ]
  node [
    id 161
    label "cz&#322;owiek"
  ]
  node [
    id 162
    label "powodowa&#263;"
  ]
  node [
    id 163
    label "zmniejsza&#263;"
  ]
  node [
    id 164
    label "bate"
  ]
  node [
    id 165
    label "de-escalation"
  ]
  node [
    id 166
    label "powodowanie"
  ]
  node [
    id 167
    label "debilitation"
  ]
  node [
    id 168
    label "zmniejszanie"
  ]
  node [
    id 169
    label "pogarszanie"
  ]
  node [
    id 170
    label "wear"
  ]
  node [
    id 171
    label "destruction"
  ]
  node [
    id 172
    label "zu&#380;ycie"
  ]
  node [
    id 173
    label "attrition"
  ]
  node [
    id 174
    label "zaszkodzenie"
  ]
  node [
    id 175
    label "podpalenie"
  ]
  node [
    id 176
    label "strata"
  ]
  node [
    id 177
    label "spl&#261;drowanie"
  ]
  node [
    id 178
    label "poniszczenie"
  ]
  node [
    id 179
    label "ruin"
  ]
  node [
    id 180
    label "stanie_si&#281;"
  ]
  node [
    id 181
    label "rezultat"
  ]
  node [
    id 182
    label "poniszczenie_si&#281;"
  ]
  node [
    id 183
    label "reduce"
  ]
  node [
    id 184
    label "spowodowa&#263;"
  ]
  node [
    id 185
    label "zmniejszy&#263;"
  ]
  node [
    id 186
    label "cushion"
  ]
  node [
    id 187
    label "health"
  ]
  node [
    id 188
    label "wp&#322;yw"
  ]
  node [
    id 189
    label "destroy"
  ]
  node [
    id 190
    label "uszkadza&#263;"
  ]
  node [
    id 191
    label "szkodzi&#263;"
  ]
  node [
    id 192
    label "mar"
  ]
  node [
    id 193
    label "wygrywa&#263;"
  ]
  node [
    id 194
    label "pamper"
  ]
  node [
    id 195
    label "zaszkodzi&#263;"
  ]
  node [
    id 196
    label "zu&#380;y&#263;"
  ]
  node [
    id 197
    label "spoil"
  ]
  node [
    id 198
    label "consume"
  ]
  node [
    id 199
    label "wygra&#263;"
  ]
  node [
    id 200
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 201
    label "nadwyr&#281;&#380;enie"
  ]
  node [
    id 202
    label "gard&#322;o"
  ]
  node [
    id 203
    label "zranienie"
  ]
  node [
    id 204
    label "s&#322;ony"
  ]
  node [
    id 205
    label "wzi&#281;cie"
  ]
  node [
    id 206
    label "pl&#261;drowanie"
  ]
  node [
    id 207
    label "ravaging"
  ]
  node [
    id 208
    label "gnojenie"
  ]
  node [
    id 209
    label "szkodzenie"
  ]
  node [
    id 210
    label "pustoszenie"
  ]
  node [
    id 211
    label "decay"
  ]
  node [
    id 212
    label "poniewieranie_si&#281;"
  ]
  node [
    id 213
    label "devastation"
  ]
  node [
    id 214
    label "zu&#380;ywanie"
  ]
  node [
    id 215
    label "stawanie_si&#281;"
  ]
  node [
    id 216
    label "zarobi&#263;"
  ]
  node [
    id 217
    label "nadwyr&#281;&#380;y&#263;"
  ]
  node [
    id 218
    label "wzi&#261;&#263;"
  ]
  node [
    id 219
    label "zrani&#263;"
  ]
  node [
    id 220
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 221
    label "oktober"
  ]
  node [
    id 222
    label "miesi&#261;c"
  ]
  node [
    id 223
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 224
    label "tydzie&#324;"
  ]
  node [
    id 225
    label "miech"
  ]
  node [
    id 226
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 227
    label "czas"
  ]
  node [
    id 228
    label "rok"
  ]
  node [
    id 229
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
]
