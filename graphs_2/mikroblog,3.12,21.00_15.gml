graph [
  node [
    id 0
    label "nudzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "wczoraj"
    origin "text"
  ]
  node [
    id 3
    label "za&#322;o&#380;y&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "fake"
    origin "text"
  ]
  node [
    id 5
    label "konto"
    origin "text"
  ]
  node [
    id 6
    label "r&#243;&#380;owy"
    origin "text"
  ]
  node [
    id 7
    label "tinder"
    origin "text"
  ]
  node [
    id 8
    label "pare"
    origin "text"
  ]
  node [
    id 9
    label "minuta"
    origin "text"
  ]
  node [
    id 10
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 11
    label "typ"
    origin "text"
  ]
  node [
    id 12
    label "podawa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "adres"
    origin "text"
  ]
  node [
    id 14
    label "siebie"
    origin "text"
  ]
  node [
    id 15
    label "pyta"
    origin "text"
  ]
  node [
    id 16
    label "jeszcze"
    origin "text"
  ]
  node [
    id 17
    label "luba"
    origin "text"
  ]
  node [
    id 18
    label "&#347;niadanie"
    origin "text"
  ]
  node [
    id 19
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 20
    label "co&#347;"
    origin "text"
  ]
  node [
    id 21
    label "by&#263;"
    origin "text"
  ]
  node [
    id 22
    label "harass"
  ]
  node [
    id 23
    label "naprzykrza&#263;_si&#281;"
  ]
  node [
    id 24
    label "gada&#263;"
  ]
  node [
    id 25
    label "nalega&#263;"
  ]
  node [
    id 26
    label "rant"
  ]
  node [
    id 27
    label "buttonhole"
  ]
  node [
    id 28
    label "wzbudza&#263;"
  ]
  node [
    id 29
    label "narzeka&#263;"
  ]
  node [
    id 30
    label "chat_up"
  ]
  node [
    id 31
    label "niezadowolenie"
  ]
  node [
    id 32
    label "m&#243;wi&#263;"
  ]
  node [
    id 33
    label "wyra&#380;a&#263;"
  ]
  node [
    id 34
    label "swarzy&#263;"
  ]
  node [
    id 35
    label "snivel"
  ]
  node [
    id 36
    label "wyrzeka&#263;"
  ]
  node [
    id 37
    label "rush"
  ]
  node [
    id 38
    label "przekonywa&#263;"
  ]
  node [
    id 39
    label "go"
  ]
  node [
    id 40
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 41
    label "brzuch"
  ]
  node [
    id 42
    label "rozmawia&#263;"
  ]
  node [
    id 43
    label "chew_the_fat"
  ]
  node [
    id 44
    label "talk"
  ]
  node [
    id 45
    label "napierdziela&#263;"
  ]
  node [
    id 46
    label "growl"
  ]
  node [
    id 47
    label "brzmie&#263;"
  ]
  node [
    id 48
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 49
    label "kraw&#281;d&#378;"
  ]
  node [
    id 50
    label "dawno"
  ]
  node [
    id 51
    label "doba"
  ]
  node [
    id 52
    label "niedawno"
  ]
  node [
    id 53
    label "aktualnie"
  ]
  node [
    id 54
    label "ostatni"
  ]
  node [
    id 55
    label "dawny"
  ]
  node [
    id 56
    label "d&#322;ugotrwale"
  ]
  node [
    id 57
    label "wcze&#347;niej"
  ]
  node [
    id 58
    label "ongi&#347;"
  ]
  node [
    id 59
    label "dawnie"
  ]
  node [
    id 60
    label "tydzie&#324;"
  ]
  node [
    id 61
    label "noc"
  ]
  node [
    id 62
    label "dzie&#324;"
  ]
  node [
    id 63
    label "czas"
  ]
  node [
    id 64
    label "godzina"
  ]
  node [
    id 65
    label "long_time"
  ]
  node [
    id 66
    label "jednostka_geologiczna"
  ]
  node [
    id 67
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "dorobek"
  ]
  node [
    id 69
    label "mienie"
  ]
  node [
    id 70
    label "subkonto"
  ]
  node [
    id 71
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 72
    label "debet"
  ]
  node [
    id 73
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 74
    label "kariera"
  ]
  node [
    id 75
    label "reprezentacja"
  ]
  node [
    id 76
    label "bank"
  ]
  node [
    id 77
    label "dost&#281;p"
  ]
  node [
    id 78
    label "rachunek"
  ]
  node [
    id 79
    label "kredyt"
  ]
  node [
    id 80
    label "przej&#347;cie"
  ]
  node [
    id 81
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 82
    label "rodowo&#347;&#263;"
  ]
  node [
    id 83
    label "patent"
  ]
  node [
    id 84
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 85
    label "dobra"
  ]
  node [
    id 86
    label "stan"
  ]
  node [
    id 87
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 88
    label "przej&#347;&#263;"
  ]
  node [
    id 89
    label "possession"
  ]
  node [
    id 90
    label "wytw&#243;r"
  ]
  node [
    id 91
    label "spis"
  ]
  node [
    id 92
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 93
    label "check"
  ]
  node [
    id 94
    label "count"
  ]
  node [
    id 95
    label "zesp&#243;&#322;"
  ]
  node [
    id 96
    label "dru&#380;yna"
  ]
  node [
    id 97
    label "emblemat"
  ]
  node [
    id 98
    label "deputation"
  ]
  node [
    id 99
    label "przebieg"
  ]
  node [
    id 100
    label "awansowa&#263;"
  ]
  node [
    id 101
    label "pozycja_spo&#322;eczna"
  ]
  node [
    id 102
    label "rozw&#243;j"
  ]
  node [
    id 103
    label "awans"
  ]
  node [
    id 104
    label "awansowanie"
  ]
  node [
    id 105
    label "degradacja"
  ]
  node [
    id 106
    label "d&#322;ug"
  ]
  node [
    id 107
    label "borg"
  ]
  node [
    id 108
    label "pasywa"
  ]
  node [
    id 109
    label "odsetki"
  ]
  node [
    id 110
    label "konsolidacja"
  ]
  node [
    id 111
    label "rata"
  ]
  node [
    id 112
    label "aktywa"
  ]
  node [
    id 113
    label "zapa&#347;&#263;_kredytowa"
  ]
  node [
    id 114
    label "arrozacja"
  ]
  node [
    id 115
    label "sp&#322;ata"
  ]
  node [
    id 116
    label "linia_kredytowa"
  ]
  node [
    id 117
    label "zobowi&#261;zanie"
  ]
  node [
    id 118
    label "storno"
  ]
  node [
    id 119
    label "bilans&#243;wka"
  ]
  node [
    id 120
    label "buchalteria"
  ]
  node [
    id 121
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 122
    label "dzia&#322;"
  ]
  node [
    id 123
    label "rachunkowo&#347;&#263;"
  ]
  node [
    id 124
    label "informatyka"
  ]
  node [
    id 125
    label "operacja"
  ]
  node [
    id 126
    label "miejsce"
  ]
  node [
    id 127
    label "has&#322;o"
  ]
  node [
    id 128
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 129
    label "agent_rozliczeniowy"
  ]
  node [
    id 130
    label "kwota"
  ]
  node [
    id 131
    label "zbi&#243;r"
  ]
  node [
    id 132
    label "instytucja"
  ]
  node [
    id 133
    label "siedziba"
  ]
  node [
    id 134
    label "wk&#322;adca"
  ]
  node [
    id 135
    label "agencja"
  ]
  node [
    id 136
    label "eurorynek"
  ]
  node [
    id 137
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 138
    label "wypracowa&#263;"
  ]
  node [
    id 139
    label "zar&#243;&#380;owienie"
  ]
  node [
    id 140
    label "czerwonawy"
  ]
  node [
    id 141
    label "r&#243;&#380;owo"
  ]
  node [
    id 142
    label "r&#243;&#380;owienie"
  ]
  node [
    id 143
    label "weso&#322;y"
  ]
  node [
    id 144
    label "optymistyczny"
  ]
  node [
    id 145
    label "zar&#243;&#380;owienie_si&#281;"
  ]
  node [
    id 146
    label "czerwonawo"
  ]
  node [
    id 147
    label "ciep&#322;y"
  ]
  node [
    id 148
    label "pijany"
  ]
  node [
    id 149
    label "weso&#322;o"
  ]
  node [
    id 150
    label "pozytywny"
  ]
  node [
    id 151
    label "beztroski"
  ]
  node [
    id 152
    label "dobry"
  ]
  node [
    id 153
    label "optymistycznie"
  ]
  node [
    id 154
    label "pomy&#347;lnie"
  ]
  node [
    id 155
    label "odcinanie_si&#281;"
  ]
  node [
    id 156
    label "barwienie"
  ]
  node [
    id 157
    label "barwienie_si&#281;"
  ]
  node [
    id 158
    label "zabarwienie_si&#281;"
  ]
  node [
    id 159
    label "zabarwienie"
  ]
  node [
    id 160
    label "time"
  ]
  node [
    id 161
    label "zapis"
  ]
  node [
    id 162
    label "sekunda"
  ]
  node [
    id 163
    label "jednostka"
  ]
  node [
    id 164
    label "stopie&#324;"
  ]
  node [
    id 165
    label "design"
  ]
  node [
    id 166
    label "kwadrans"
  ]
  node [
    id 167
    label "przyswoi&#263;"
  ]
  node [
    id 168
    label "ludzko&#347;&#263;"
  ]
  node [
    id 169
    label "one"
  ]
  node [
    id 170
    label "poj&#281;cie"
  ]
  node [
    id 171
    label "ewoluowanie"
  ]
  node [
    id 172
    label "supremum"
  ]
  node [
    id 173
    label "skala"
  ]
  node [
    id 174
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 175
    label "przyswajanie"
  ]
  node [
    id 176
    label "wyewoluowanie"
  ]
  node [
    id 177
    label "reakcja"
  ]
  node [
    id 178
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 179
    label "przeliczy&#263;"
  ]
  node [
    id 180
    label "wyewoluowa&#263;"
  ]
  node [
    id 181
    label "ewoluowa&#263;"
  ]
  node [
    id 182
    label "matematyka"
  ]
  node [
    id 183
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 184
    label "rzut"
  ]
  node [
    id 185
    label "liczba_naturalna"
  ]
  node [
    id 186
    label "czynnik_biotyczny"
  ]
  node [
    id 187
    label "g&#322;owa"
  ]
  node [
    id 188
    label "figura"
  ]
  node [
    id 189
    label "individual"
  ]
  node [
    id 190
    label "portrecista"
  ]
  node [
    id 191
    label "obiekt"
  ]
  node [
    id 192
    label "przyswaja&#263;"
  ]
  node [
    id 193
    label "przyswojenie"
  ]
  node [
    id 194
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 195
    label "profanum"
  ]
  node [
    id 196
    label "mikrokosmos"
  ]
  node [
    id 197
    label "starzenie_si&#281;"
  ]
  node [
    id 198
    label "duch"
  ]
  node [
    id 199
    label "przeliczanie"
  ]
  node [
    id 200
    label "osoba"
  ]
  node [
    id 201
    label "oddzia&#322;ywanie"
  ]
  node [
    id 202
    label "antropochoria"
  ]
  node [
    id 203
    label "funkcja"
  ]
  node [
    id 204
    label "homo_sapiens"
  ]
  node [
    id 205
    label "przelicza&#263;"
  ]
  node [
    id 206
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 207
    label "infimum"
  ]
  node [
    id 208
    label "przeliczenie"
  ]
  node [
    id 209
    label "spos&#243;b"
  ]
  node [
    id 210
    label "entrance"
  ]
  node [
    id 211
    label "czynno&#347;&#263;"
  ]
  node [
    id 212
    label "wpis"
  ]
  node [
    id 213
    label "normalizacja"
  ]
  node [
    id 214
    label "mikrosekunda"
  ]
  node [
    id 215
    label "milisekunda"
  ]
  node [
    id 216
    label "tercja"
  ]
  node [
    id 217
    label "nanosekunda"
  ]
  node [
    id 218
    label "uk&#322;ad_SI"
  ]
  node [
    id 219
    label "jednostka_czasu"
  ]
  node [
    id 220
    label "p&#243;&#322;godzina"
  ]
  node [
    id 221
    label "kszta&#322;t"
  ]
  node [
    id 222
    label "podstopie&#324;"
  ]
  node [
    id 223
    label "wielko&#347;&#263;"
  ]
  node [
    id 224
    label "rank"
  ]
  node [
    id 225
    label "d&#378;wi&#281;k"
  ]
  node [
    id 226
    label "wschodek"
  ]
  node [
    id 227
    label "przymiotnik"
  ]
  node [
    id 228
    label "gama"
  ]
  node [
    id 229
    label "podzia&#322;"
  ]
  node [
    id 230
    label "element"
  ]
  node [
    id 231
    label "schody"
  ]
  node [
    id 232
    label "kategoria_gramatyczna"
  ]
  node [
    id 233
    label "poziom"
  ]
  node [
    id 234
    label "przys&#322;&#243;wek"
  ]
  node [
    id 235
    label "ocena"
  ]
  node [
    id 236
    label "degree"
  ]
  node [
    id 237
    label "szczebel"
  ]
  node [
    id 238
    label "znaczenie"
  ]
  node [
    id 239
    label "podn&#243;&#380;ek"
  ]
  node [
    id 240
    label "forma"
  ]
  node [
    id 241
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 242
    label "wygl&#261;d"
  ]
  node [
    id 243
    label "przyzwoity"
  ]
  node [
    id 244
    label "ciekawy"
  ]
  node [
    id 245
    label "jako&#347;"
  ]
  node [
    id 246
    label "jako_tako"
  ]
  node [
    id 247
    label "niez&#322;y"
  ]
  node [
    id 248
    label "dziwny"
  ]
  node [
    id 249
    label "charakterystyczny"
  ]
  node [
    id 250
    label "intensywny"
  ]
  node [
    id 251
    label "udolny"
  ]
  node [
    id 252
    label "skuteczny"
  ]
  node [
    id 253
    label "&#347;mieszny"
  ]
  node [
    id 254
    label "niczegowaty"
  ]
  node [
    id 255
    label "dobrze"
  ]
  node [
    id 256
    label "nieszpetny"
  ]
  node [
    id 257
    label "spory"
  ]
  node [
    id 258
    label "korzystny"
  ]
  node [
    id 259
    label "nie&#378;le"
  ]
  node [
    id 260
    label "skromny"
  ]
  node [
    id 261
    label "kulturalny"
  ]
  node [
    id 262
    label "grzeczny"
  ]
  node [
    id 263
    label "stosowny"
  ]
  node [
    id 264
    label "przystojny"
  ]
  node [
    id 265
    label "nale&#380;yty"
  ]
  node [
    id 266
    label "moralny"
  ]
  node [
    id 267
    label "przyzwoicie"
  ]
  node [
    id 268
    label "wystarczaj&#261;cy"
  ]
  node [
    id 269
    label "nietuzinkowy"
  ]
  node [
    id 270
    label "cz&#322;owiek"
  ]
  node [
    id 271
    label "intryguj&#261;cy"
  ]
  node [
    id 272
    label "ch&#281;tny"
  ]
  node [
    id 273
    label "swoisty"
  ]
  node [
    id 274
    label "interesowanie"
  ]
  node [
    id 275
    label "interesuj&#261;cy"
  ]
  node [
    id 276
    label "ciekawie"
  ]
  node [
    id 277
    label "indagator"
  ]
  node [
    id 278
    label "charakterystycznie"
  ]
  node [
    id 279
    label "szczeg&#243;lny"
  ]
  node [
    id 280
    label "wyj&#261;tkowy"
  ]
  node [
    id 281
    label "typowy"
  ]
  node [
    id 282
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 283
    label "podobny"
  ]
  node [
    id 284
    label "dziwnie"
  ]
  node [
    id 285
    label "dziwy"
  ]
  node [
    id 286
    label "inny"
  ]
  node [
    id 287
    label "w_miar&#281;"
  ]
  node [
    id 288
    label "jako_taki"
  ]
  node [
    id 289
    label "facet"
  ]
  node [
    id 290
    label "jednostka_systematyczna"
  ]
  node [
    id 291
    label "kr&#243;lestwo"
  ]
  node [
    id 292
    label "autorament"
  ]
  node [
    id 293
    label "variety"
  ]
  node [
    id 294
    label "antycypacja"
  ]
  node [
    id 295
    label "przypuszczenie"
  ]
  node [
    id 296
    label "cynk"
  ]
  node [
    id 297
    label "obstawia&#263;"
  ]
  node [
    id 298
    label "gromada"
  ]
  node [
    id 299
    label "sztuka"
  ]
  node [
    id 300
    label "rezultat"
  ]
  node [
    id 301
    label "pob&#243;r"
  ]
  node [
    id 302
    label "wojsko"
  ]
  node [
    id 303
    label "type"
  ]
  node [
    id 304
    label "pogl&#261;d"
  ]
  node [
    id 305
    label "proces"
  ]
  node [
    id 306
    label "zapowied&#378;"
  ]
  node [
    id 307
    label "upodobnienie"
  ]
  node [
    id 308
    label "zjawisko"
  ]
  node [
    id 309
    label "narracja"
  ]
  node [
    id 310
    label "prediction"
  ]
  node [
    id 311
    label "pr&#243;bowanie"
  ]
  node [
    id 312
    label "rola"
  ]
  node [
    id 313
    label "przedmiot"
  ]
  node [
    id 314
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 315
    label "realizacja"
  ]
  node [
    id 316
    label "scena"
  ]
  node [
    id 317
    label "didaskalia"
  ]
  node [
    id 318
    label "czyn"
  ]
  node [
    id 319
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 320
    label "environment"
  ]
  node [
    id 321
    label "head"
  ]
  node [
    id 322
    label "scenariusz"
  ]
  node [
    id 323
    label "egzemplarz"
  ]
  node [
    id 324
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 325
    label "utw&#243;r"
  ]
  node [
    id 326
    label "kultura_duchowa"
  ]
  node [
    id 327
    label "fortel"
  ]
  node [
    id 328
    label "theatrical_performance"
  ]
  node [
    id 329
    label "ambala&#380;"
  ]
  node [
    id 330
    label "sprawno&#347;&#263;"
  ]
  node [
    id 331
    label "kobieta"
  ]
  node [
    id 332
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 333
    label "Faust"
  ]
  node [
    id 334
    label "scenografia"
  ]
  node [
    id 335
    label "ods&#322;ona"
  ]
  node [
    id 336
    label "turn"
  ]
  node [
    id 337
    label "pokaz"
  ]
  node [
    id 338
    label "ilo&#347;&#263;"
  ]
  node [
    id 339
    label "przedstawienie"
  ]
  node [
    id 340
    label "przedstawi&#263;"
  ]
  node [
    id 341
    label "Apollo"
  ]
  node [
    id 342
    label "kultura"
  ]
  node [
    id 343
    label "przedstawianie"
  ]
  node [
    id 344
    label "przedstawia&#263;"
  ]
  node [
    id 345
    label "towar"
  ]
  node [
    id 346
    label "bratek"
  ]
  node [
    id 347
    label "datum"
  ]
  node [
    id 348
    label "poszlaka"
  ]
  node [
    id 349
    label "dopuszczenie"
  ]
  node [
    id 350
    label "teoria"
  ]
  node [
    id 351
    label "conjecture"
  ]
  node [
    id 352
    label "koniektura"
  ]
  node [
    id 353
    label "asymilowanie"
  ]
  node [
    id 354
    label "wapniak"
  ]
  node [
    id 355
    label "asymilowa&#263;"
  ]
  node [
    id 356
    label "os&#322;abia&#263;"
  ]
  node [
    id 357
    label "posta&#263;"
  ]
  node [
    id 358
    label "hominid"
  ]
  node [
    id 359
    label "podw&#322;adny"
  ]
  node [
    id 360
    label "os&#322;abianie"
  ]
  node [
    id 361
    label "dwun&#243;g"
  ]
  node [
    id 362
    label "nasada"
  ]
  node [
    id 363
    label "wz&#243;r"
  ]
  node [
    id 364
    label "senior"
  ]
  node [
    id 365
    label "Adam"
  ]
  node [
    id 366
    label "polifag"
  ]
  node [
    id 367
    label "tip-off"
  ]
  node [
    id 368
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 369
    label "tip"
  ]
  node [
    id 370
    label "sygna&#322;"
  ]
  node [
    id 371
    label "metal_kolorowy"
  ]
  node [
    id 372
    label "mikroelement"
  ]
  node [
    id 373
    label "cynkowiec"
  ]
  node [
    id 374
    label "ubezpiecza&#263;"
  ]
  node [
    id 375
    label "venture"
  ]
  node [
    id 376
    label "przewidywa&#263;"
  ]
  node [
    id 377
    label "zapewnia&#263;"
  ]
  node [
    id 378
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 379
    label "typowa&#263;"
  ]
  node [
    id 380
    label "ochrona"
  ]
  node [
    id 381
    label "zastawia&#263;"
  ]
  node [
    id 382
    label "budowa&#263;"
  ]
  node [
    id 383
    label "zajmowa&#263;"
  ]
  node [
    id 384
    label "obejmowa&#263;"
  ]
  node [
    id 385
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 386
    label "os&#322;ania&#263;"
  ]
  node [
    id 387
    label "otacza&#263;"
  ]
  node [
    id 388
    label "broni&#263;"
  ]
  node [
    id 389
    label "powierza&#263;"
  ]
  node [
    id 390
    label "bramka"
  ]
  node [
    id 391
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 392
    label "frame"
  ]
  node [
    id 393
    label "wysy&#322;a&#263;"
  ]
  node [
    id 394
    label "dzia&#322;anie"
  ]
  node [
    id 395
    label "event"
  ]
  node [
    id 396
    label "przyczyna"
  ]
  node [
    id 397
    label "jednostka_administracyjna"
  ]
  node [
    id 398
    label "zoologia"
  ]
  node [
    id 399
    label "skupienie"
  ]
  node [
    id 400
    label "stage_set"
  ]
  node [
    id 401
    label "tribe"
  ]
  node [
    id 402
    label "hurma"
  ]
  node [
    id 403
    label "grupa"
  ]
  node [
    id 404
    label "botanika"
  ]
  node [
    id 405
    label "ro&#347;liny"
  ]
  node [
    id 406
    label "grzyby"
  ]
  node [
    id 407
    label "Arktogea"
  ]
  node [
    id 408
    label "prokarioty"
  ]
  node [
    id 409
    label "zwierz&#281;ta"
  ]
  node [
    id 410
    label "domena"
  ]
  node [
    id 411
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 412
    label "protisty"
  ]
  node [
    id 413
    label "pa&#324;stwo"
  ]
  node [
    id 414
    label "terytorium"
  ]
  node [
    id 415
    label "kategoria_systematyczna"
  ]
  node [
    id 416
    label "tenis"
  ]
  node [
    id 417
    label "deal"
  ]
  node [
    id 418
    label "dawa&#263;"
  ]
  node [
    id 419
    label "stawia&#263;"
  ]
  node [
    id 420
    label "rozgrywa&#263;"
  ]
  node [
    id 421
    label "kelner"
  ]
  node [
    id 422
    label "siatk&#243;wka"
  ]
  node [
    id 423
    label "cover"
  ]
  node [
    id 424
    label "tender"
  ]
  node [
    id 425
    label "jedzenie"
  ]
  node [
    id 426
    label "faszerowa&#263;"
  ]
  node [
    id 427
    label "introduce"
  ]
  node [
    id 428
    label "informowa&#263;"
  ]
  node [
    id 429
    label "serwowa&#263;"
  ]
  node [
    id 430
    label "powiada&#263;"
  ]
  node [
    id 431
    label "komunikowa&#263;"
  ]
  node [
    id 432
    label "inform"
  ]
  node [
    id 433
    label "zaczyna&#263;"
  ]
  node [
    id 434
    label "pi&#322;ka"
  ]
  node [
    id 435
    label "give"
  ]
  node [
    id 436
    label "przeprowadza&#263;"
  ]
  node [
    id 437
    label "gra&#263;"
  ]
  node [
    id 438
    label "play"
  ]
  node [
    id 439
    label "robi&#263;"
  ]
  node [
    id 440
    label "przekazywa&#263;"
  ]
  node [
    id 441
    label "dostarcza&#263;"
  ]
  node [
    id 442
    label "mie&#263;_miejsce"
  ]
  node [
    id 443
    label "&#322;adowa&#263;"
  ]
  node [
    id 444
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 445
    label "przeznacza&#263;"
  ]
  node [
    id 446
    label "surrender"
  ]
  node [
    id 447
    label "traktowa&#263;"
  ]
  node [
    id 448
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 449
    label "obiecywa&#263;"
  ]
  node [
    id 450
    label "odst&#281;powa&#263;"
  ]
  node [
    id 451
    label "rap"
  ]
  node [
    id 452
    label "umieszcza&#263;"
  ]
  node [
    id 453
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 454
    label "t&#322;uc"
  ]
  node [
    id 455
    label "render"
  ]
  node [
    id 456
    label "wpiernicza&#263;"
  ]
  node [
    id 457
    label "exsert"
  ]
  node [
    id 458
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 459
    label "train"
  ]
  node [
    id 460
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 461
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 462
    label "p&#322;aci&#263;"
  ]
  node [
    id 463
    label "hold_out"
  ]
  node [
    id 464
    label "nalewa&#263;"
  ]
  node [
    id 465
    label "zezwala&#263;"
  ]
  node [
    id 466
    label "hold"
  ]
  node [
    id 467
    label "pozostawia&#263;"
  ]
  node [
    id 468
    label "czyni&#263;"
  ]
  node [
    id 469
    label "wydawa&#263;"
  ]
  node [
    id 470
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 471
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 472
    label "raise"
  ]
  node [
    id 473
    label "przyznawa&#263;"
  ]
  node [
    id 474
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 475
    label "ocenia&#263;"
  ]
  node [
    id 476
    label "stanowisko"
  ]
  node [
    id 477
    label "znak"
  ]
  node [
    id 478
    label "wskazywa&#263;"
  ]
  node [
    id 479
    label "uruchamia&#263;"
  ]
  node [
    id 480
    label "wytwarza&#263;"
  ]
  node [
    id 481
    label "fundowa&#263;"
  ]
  node [
    id 482
    label "zmienia&#263;"
  ]
  node [
    id 483
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 484
    label "deliver"
  ]
  node [
    id 485
    label "powodowa&#263;"
  ]
  node [
    id 486
    label "wyznacza&#263;"
  ]
  node [
    id 487
    label "wydobywa&#263;"
  ]
  node [
    id 488
    label "charge"
  ]
  node [
    id 489
    label "nadziewa&#263;"
  ]
  node [
    id 490
    label "przesadza&#263;"
  ]
  node [
    id 491
    label "blok"
  ]
  node [
    id 492
    label "lobowanie"
  ]
  node [
    id 493
    label "&#347;cina&#263;"
  ]
  node [
    id 494
    label "retinopatia"
  ]
  node [
    id 495
    label "podanie"
  ]
  node [
    id 496
    label "plamka_&#380;&#243;&#322;ta"
  ]
  node [
    id 497
    label "cia&#322;o_szkliste"
  ]
  node [
    id 498
    label "&#347;cinanie"
  ]
  node [
    id 499
    label "zeaksantyna"
  ]
  node [
    id 500
    label "przelobowa&#263;"
  ]
  node [
    id 501
    label "lobowa&#263;"
  ]
  node [
    id 502
    label "dno_oka"
  ]
  node [
    id 503
    label "przelobowanie"
  ]
  node [
    id 504
    label "poda&#263;"
  ]
  node [
    id 505
    label "barwnik_&#347;wiat&#322;oczu&#322;y"
  ]
  node [
    id 506
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 507
    label "&#347;ci&#281;cie"
  ]
  node [
    id 508
    label "podawanie"
  ]
  node [
    id 509
    label "pelota"
  ]
  node [
    id 510
    label "sport_rakietowy"
  ]
  node [
    id 511
    label "wolej"
  ]
  node [
    id 512
    label "supervisor"
  ]
  node [
    id 513
    label "ubrani&#243;wka"
  ]
  node [
    id 514
    label "singlista"
  ]
  node [
    id 515
    label "bekhend"
  ]
  node [
    id 516
    label "forhend"
  ]
  node [
    id 517
    label "p&#243;&#322;wolej"
  ]
  node [
    id 518
    label "pr&#261;&#380;kowany"
  ]
  node [
    id 519
    label "singlowy"
  ]
  node [
    id 520
    label "tkanina_we&#322;niana"
  ]
  node [
    id 521
    label "deblowy"
  ]
  node [
    id 522
    label "tkanina"
  ]
  node [
    id 523
    label "mikst"
  ]
  node [
    id 524
    label "slajs"
  ]
  node [
    id 525
    label "deblista"
  ]
  node [
    id 526
    label "miksista"
  ]
  node [
    id 527
    label "Wimbledon"
  ]
  node [
    id 528
    label "zatruwanie_si&#281;"
  ]
  node [
    id 529
    label "przejadanie_si&#281;"
  ]
  node [
    id 530
    label "szama"
  ]
  node [
    id 531
    label "koryto"
  ]
  node [
    id 532
    label "rzecz"
  ]
  node [
    id 533
    label "odpasanie_si&#281;"
  ]
  node [
    id 534
    label "eating"
  ]
  node [
    id 535
    label "jadanie"
  ]
  node [
    id 536
    label "posilenie"
  ]
  node [
    id 537
    label "wpieprzanie"
  ]
  node [
    id 538
    label "wmuszanie"
  ]
  node [
    id 539
    label "robienie"
  ]
  node [
    id 540
    label "wiwenda"
  ]
  node [
    id 541
    label "polowanie"
  ]
  node [
    id 542
    label "ufetowanie_si&#281;"
  ]
  node [
    id 543
    label "wyjadanie"
  ]
  node [
    id 544
    label "smakowanie"
  ]
  node [
    id 545
    label "przejedzenie"
  ]
  node [
    id 546
    label "jad&#322;o"
  ]
  node [
    id 547
    label "mlaskanie"
  ]
  node [
    id 548
    label "papusianie"
  ]
  node [
    id 549
    label "posilanie"
  ]
  node [
    id 550
    label "przejedzenie_si&#281;"
  ]
  node [
    id 551
    label "&#380;arcie"
  ]
  node [
    id 552
    label "odpasienie_si&#281;"
  ]
  node [
    id 553
    label "wyjedzenie"
  ]
  node [
    id 554
    label "przejadanie"
  ]
  node [
    id 555
    label "objadanie"
  ]
  node [
    id 556
    label "pracownik"
  ]
  node [
    id 557
    label "ober"
  ]
  node [
    id 558
    label "kawa&#322;ek"
  ]
  node [
    id 559
    label "aran&#380;acja"
  ]
  node [
    id 560
    label "wagon"
  ]
  node [
    id 561
    label "pojazd_kolejowy"
  ]
  node [
    id 562
    label "poci&#261;g"
  ]
  node [
    id 563
    label "statek"
  ]
  node [
    id 564
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 565
    label "okr&#281;t"
  ]
  node [
    id 566
    label "po&#322;o&#380;enie"
  ]
  node [
    id 567
    label "pismo"
  ]
  node [
    id 568
    label "personalia"
  ]
  node [
    id 569
    label "dane"
  ]
  node [
    id 570
    label "kod_pocztowy"
  ]
  node [
    id 571
    label "adres_elektroniczny"
  ]
  node [
    id 572
    label "dziedzina"
  ]
  node [
    id 573
    label "przesy&#322;ka"
  ]
  node [
    id 574
    label "strona"
  ]
  node [
    id 575
    label "&#321;ubianka"
  ]
  node [
    id 576
    label "miejsce_pracy"
  ]
  node [
    id 577
    label "dzia&#322;_personalny"
  ]
  node [
    id 578
    label "Kreml"
  ]
  node [
    id 579
    label "Bia&#322;y_Dom"
  ]
  node [
    id 580
    label "budynek"
  ]
  node [
    id 581
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 582
    label "sadowisko"
  ]
  node [
    id 583
    label "edytowa&#263;"
  ]
  node [
    id 584
    label "spakowanie"
  ]
  node [
    id 585
    label "pakowa&#263;"
  ]
  node [
    id 586
    label "rekord"
  ]
  node [
    id 587
    label "korelator"
  ]
  node [
    id 588
    label "wyci&#261;ganie"
  ]
  node [
    id 589
    label "pakowanie"
  ]
  node [
    id 590
    label "sekwencjonowa&#263;"
  ]
  node [
    id 591
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 592
    label "jednostka_informacji"
  ]
  node [
    id 593
    label "evidence"
  ]
  node [
    id 594
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 595
    label "rozpakowywanie"
  ]
  node [
    id 596
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 597
    label "rozpakowanie"
  ]
  node [
    id 598
    label "informacja"
  ]
  node [
    id 599
    label "rozpakowywa&#263;"
  ]
  node [
    id 600
    label "konwersja"
  ]
  node [
    id 601
    label "nap&#322;ywanie"
  ]
  node [
    id 602
    label "rozpakowa&#263;"
  ]
  node [
    id 603
    label "spakowa&#263;"
  ]
  node [
    id 604
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 605
    label "edytowanie"
  ]
  node [
    id 606
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 607
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 608
    label "sekwencjonowanie"
  ]
  node [
    id 609
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 610
    label "sfera"
  ]
  node [
    id 611
    label "zakres"
  ]
  node [
    id 612
    label "bezdro&#380;e"
  ]
  node [
    id 613
    label "poddzia&#322;"
  ]
  node [
    id 614
    label "psychotest"
  ]
  node [
    id 615
    label "wk&#322;ad"
  ]
  node [
    id 616
    label "handwriting"
  ]
  node [
    id 617
    label "przekaz"
  ]
  node [
    id 618
    label "dzie&#322;o"
  ]
  node [
    id 619
    label "paleograf"
  ]
  node [
    id 620
    label "interpunkcja"
  ]
  node [
    id 621
    label "cecha"
  ]
  node [
    id 622
    label "grafia"
  ]
  node [
    id 623
    label "communication"
  ]
  node [
    id 624
    label "script"
  ]
  node [
    id 625
    label "zajawka"
  ]
  node [
    id 626
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 627
    label "list"
  ]
  node [
    id 628
    label "Zwrotnica"
  ]
  node [
    id 629
    label "czasopismo"
  ]
  node [
    id 630
    label "ok&#322;adka"
  ]
  node [
    id 631
    label "ortografia"
  ]
  node [
    id 632
    label "letter"
  ]
  node [
    id 633
    label "komunikacja"
  ]
  node [
    id 634
    label "paleografia"
  ]
  node [
    id 635
    label "j&#281;zyk"
  ]
  node [
    id 636
    label "dokument"
  ]
  node [
    id 637
    label "prasa"
  ]
  node [
    id 638
    label "adres_internetowy"
  ]
  node [
    id 639
    label "j&#261;drowce"
  ]
  node [
    id 640
    label "subdomena"
  ]
  node [
    id 641
    label "maj&#261;tek"
  ]
  node [
    id 642
    label "feudalizm"
  ]
  node [
    id 643
    label "archeony"
  ]
  node [
    id 644
    label "posiad&#322;o&#347;&#263;"
  ]
  node [
    id 645
    label "NN"
  ]
  node [
    id 646
    label "nazwisko"
  ]
  node [
    id 647
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 648
    label "imi&#281;"
  ]
  node [
    id 649
    label "pesel"
  ]
  node [
    id 650
    label "przenocowanie"
  ]
  node [
    id 651
    label "pora&#380;ka"
  ]
  node [
    id 652
    label "nak&#322;adzenie"
  ]
  node [
    id 653
    label "pouk&#322;adanie"
  ]
  node [
    id 654
    label "pokrycie"
  ]
  node [
    id 655
    label "zepsucie"
  ]
  node [
    id 656
    label "ustawienie"
  ]
  node [
    id 657
    label "spowodowanie"
  ]
  node [
    id 658
    label "trim"
  ]
  node [
    id 659
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 660
    label "ugoszczenie"
  ]
  node [
    id 661
    label "le&#380;enie"
  ]
  node [
    id 662
    label "zbudowanie"
  ]
  node [
    id 663
    label "umieszczenie"
  ]
  node [
    id 664
    label "reading"
  ]
  node [
    id 665
    label "sytuacja"
  ]
  node [
    id 666
    label "zabicie"
  ]
  node [
    id 667
    label "wygranie"
  ]
  node [
    id 668
    label "presentation"
  ]
  node [
    id 669
    label "le&#380;e&#263;"
  ]
  node [
    id 670
    label "dochodzenie"
  ]
  node [
    id 671
    label "posy&#322;ka"
  ]
  node [
    id 672
    label "nadawca"
  ]
  node [
    id 673
    label "dochodzi&#263;"
  ]
  node [
    id 674
    label "doj&#347;cie"
  ]
  node [
    id 675
    label "doj&#347;&#263;"
  ]
  node [
    id 676
    label "kartka"
  ]
  node [
    id 677
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 678
    label "logowanie"
  ]
  node [
    id 679
    label "plik"
  ]
  node [
    id 680
    label "s&#261;d"
  ]
  node [
    id 681
    label "linia"
  ]
  node [
    id 682
    label "serwis_internetowy"
  ]
  node [
    id 683
    label "bok"
  ]
  node [
    id 684
    label "skr&#281;canie"
  ]
  node [
    id 685
    label "skr&#281;ca&#263;"
  ]
  node [
    id 686
    label "orientowanie"
  ]
  node [
    id 687
    label "skr&#281;ci&#263;"
  ]
  node [
    id 688
    label "uj&#281;cie"
  ]
  node [
    id 689
    label "zorientowanie"
  ]
  node [
    id 690
    label "ty&#322;"
  ]
  node [
    id 691
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 692
    label "fragment"
  ]
  node [
    id 693
    label "layout"
  ]
  node [
    id 694
    label "zorientowa&#263;"
  ]
  node [
    id 695
    label "pagina"
  ]
  node [
    id 696
    label "podmiot"
  ]
  node [
    id 697
    label "g&#243;ra"
  ]
  node [
    id 698
    label "orientowa&#263;"
  ]
  node [
    id 699
    label "voice"
  ]
  node [
    id 700
    label "orientacja"
  ]
  node [
    id 701
    label "prz&#243;d"
  ]
  node [
    id 702
    label "internet"
  ]
  node [
    id 703
    label "powierzchnia"
  ]
  node [
    id 704
    label "skr&#281;cenie"
  ]
  node [
    id 705
    label "penis"
  ]
  node [
    id 706
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 707
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 708
    label "ptaszek"
  ]
  node [
    id 709
    label "przyrodzenie"
  ]
  node [
    id 710
    label "shaft"
  ]
  node [
    id 711
    label "fiut"
  ]
  node [
    id 712
    label "ci&#261;gle"
  ]
  node [
    id 713
    label "stale"
  ]
  node [
    id 714
    label "ci&#261;g&#322;y"
  ]
  node [
    id 715
    label "nieprzerwanie"
  ]
  node [
    id 716
    label "wybranka"
  ]
  node [
    id 717
    label "umi&#322;owana"
  ]
  node [
    id 718
    label "kochanie"
  ]
  node [
    id 719
    label "ptaszyna"
  ]
  node [
    id 720
    label "Dulcynea"
  ]
  node [
    id 721
    label "kochanka"
  ]
  node [
    id 722
    label "mi&#322;owanie"
  ]
  node [
    id 723
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 724
    label "love"
  ]
  node [
    id 725
    label "zwrot"
  ]
  node [
    id 726
    label "chowanie"
  ]
  node [
    id 727
    label "czucie"
  ]
  node [
    id 728
    label "patrzenie_"
  ]
  node [
    id 729
    label "mi&#322;a"
  ]
  node [
    id 730
    label "mi&#322;o&#347;nica"
  ]
  node [
    id 731
    label "partnerka"
  ]
  node [
    id 732
    label "dupa"
  ]
  node [
    id 733
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 734
    label "kochanica"
  ]
  node [
    id 735
    label "Don_Kiszot"
  ]
  node [
    id 736
    label "tick"
  ]
  node [
    id 737
    label "ptasz&#281;"
  ]
  node [
    id 738
    label "partia"
  ]
  node [
    id 739
    label "jedyna"
  ]
  node [
    id 740
    label "posi&#322;ek"
  ]
  node [
    id 741
    label "danie"
  ]
  node [
    id 742
    label "szko&#322;a"
  ]
  node [
    id 743
    label "p&#322;&#243;d"
  ]
  node [
    id 744
    label "thinking"
  ]
  node [
    id 745
    label "umys&#322;"
  ]
  node [
    id 746
    label "political_orientation"
  ]
  node [
    id 747
    label "istota"
  ]
  node [
    id 748
    label "pomys&#322;"
  ]
  node [
    id 749
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 750
    label "idea"
  ]
  node [
    id 751
    label "system"
  ]
  node [
    id 752
    label "fantomatyka"
  ]
  node [
    id 753
    label "pami&#281;&#263;"
  ]
  node [
    id 754
    label "intelekt"
  ]
  node [
    id 755
    label "pomieszanie_si&#281;"
  ]
  node [
    id 756
    label "wn&#281;trze"
  ]
  node [
    id 757
    label "wyobra&#378;nia"
  ]
  node [
    id 758
    label "work"
  ]
  node [
    id 759
    label "mentalno&#347;&#263;"
  ]
  node [
    id 760
    label "superego"
  ]
  node [
    id 761
    label "psychika"
  ]
  node [
    id 762
    label "charakter"
  ]
  node [
    id 763
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 764
    label "moczownik"
  ]
  node [
    id 765
    label "embryo"
  ]
  node [
    id 766
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 767
    label "zarodek"
  ]
  node [
    id 768
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 769
    label "latawiec"
  ]
  node [
    id 770
    label "j&#261;dro"
  ]
  node [
    id 771
    label "systemik"
  ]
  node [
    id 772
    label "rozprz&#261;c"
  ]
  node [
    id 773
    label "oprogramowanie"
  ]
  node [
    id 774
    label "systemat"
  ]
  node [
    id 775
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 776
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 777
    label "model"
  ]
  node [
    id 778
    label "struktura"
  ]
  node [
    id 779
    label "usenet"
  ]
  node [
    id 780
    label "porz&#261;dek"
  ]
  node [
    id 781
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 782
    label "przyn&#281;ta"
  ]
  node [
    id 783
    label "net"
  ]
  node [
    id 784
    label "w&#281;dkarstwo"
  ]
  node [
    id 785
    label "eratem"
  ]
  node [
    id 786
    label "oddzia&#322;"
  ]
  node [
    id 787
    label "doktryna"
  ]
  node [
    id 788
    label "pulpit"
  ]
  node [
    id 789
    label "konstelacja"
  ]
  node [
    id 790
    label "o&#347;"
  ]
  node [
    id 791
    label "podsystem"
  ]
  node [
    id 792
    label "metoda"
  ]
  node [
    id 793
    label "ryba"
  ]
  node [
    id 794
    label "Leopard"
  ]
  node [
    id 795
    label "Android"
  ]
  node [
    id 796
    label "zachowanie"
  ]
  node [
    id 797
    label "cybernetyk"
  ]
  node [
    id 798
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 799
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 800
    label "method"
  ]
  node [
    id 801
    label "sk&#322;ad"
  ]
  node [
    id 802
    label "podstawa"
  ]
  node [
    id 803
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 804
    label "podejrzany"
  ]
  node [
    id 805
    label "s&#261;downictwo"
  ]
  node [
    id 806
    label "biuro"
  ]
  node [
    id 807
    label "court"
  ]
  node [
    id 808
    label "forum"
  ]
  node [
    id 809
    label "bronienie"
  ]
  node [
    id 810
    label "urz&#261;d"
  ]
  node [
    id 811
    label "wydarzenie"
  ]
  node [
    id 812
    label "oskar&#380;yciel"
  ]
  node [
    id 813
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 814
    label "skazany"
  ]
  node [
    id 815
    label "post&#281;powanie"
  ]
  node [
    id 816
    label "pods&#261;dny"
  ]
  node [
    id 817
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 818
    label "obrona"
  ]
  node [
    id 819
    label "wypowied&#378;"
  ]
  node [
    id 820
    label "antylogizm"
  ]
  node [
    id 821
    label "konektyw"
  ]
  node [
    id 822
    label "&#347;wiadek"
  ]
  node [
    id 823
    label "procesowicz"
  ]
  node [
    id 824
    label "technika"
  ]
  node [
    id 825
    label "pocz&#261;tki"
  ]
  node [
    id 826
    label "ukradzenie"
  ]
  node [
    id 827
    label "ukra&#347;&#263;"
  ]
  node [
    id 828
    label "do&#347;wiadczenie"
  ]
  node [
    id 829
    label "teren_szko&#322;y"
  ]
  node [
    id 830
    label "wiedza"
  ]
  node [
    id 831
    label "Mickiewicz"
  ]
  node [
    id 832
    label "kwalifikacje"
  ]
  node [
    id 833
    label "podr&#281;cznik"
  ]
  node [
    id 834
    label "absolwent"
  ]
  node [
    id 835
    label "praktyka"
  ]
  node [
    id 836
    label "school"
  ]
  node [
    id 837
    label "zda&#263;"
  ]
  node [
    id 838
    label "gabinet"
  ]
  node [
    id 839
    label "urszulanki"
  ]
  node [
    id 840
    label "sztuba"
  ]
  node [
    id 841
    label "&#322;awa_szkolna"
  ]
  node [
    id 842
    label "nauka"
  ]
  node [
    id 843
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 844
    label "przepisa&#263;"
  ]
  node [
    id 845
    label "muzyka"
  ]
  node [
    id 846
    label "form"
  ]
  node [
    id 847
    label "klasa"
  ]
  node [
    id 848
    label "lekcja"
  ]
  node [
    id 849
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 850
    label "przepisanie"
  ]
  node [
    id 851
    label "skolaryzacja"
  ]
  node [
    id 852
    label "zdanie"
  ]
  node [
    id 853
    label "stopek"
  ]
  node [
    id 854
    label "sekretariat"
  ]
  node [
    id 855
    label "ideologia"
  ]
  node [
    id 856
    label "lesson"
  ]
  node [
    id 857
    label "niepokalanki"
  ]
  node [
    id 858
    label "szkolenie"
  ]
  node [
    id 859
    label "kara"
  ]
  node [
    id 860
    label "tablica"
  ]
  node [
    id 861
    label "byt"
  ]
  node [
    id 862
    label "Kant"
  ]
  node [
    id 863
    label "cel"
  ]
  node [
    id 864
    label "ideacja"
  ]
  node [
    id 865
    label "thing"
  ]
  node [
    id 866
    label "cosik"
  ]
  node [
    id 867
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 868
    label "equal"
  ]
  node [
    id 869
    label "trwa&#263;"
  ]
  node [
    id 870
    label "chodzi&#263;"
  ]
  node [
    id 871
    label "si&#281;ga&#263;"
  ]
  node [
    id 872
    label "obecno&#347;&#263;"
  ]
  node [
    id 873
    label "stand"
  ]
  node [
    id 874
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 875
    label "uczestniczy&#263;"
  ]
  node [
    id 876
    label "participate"
  ]
  node [
    id 877
    label "istnie&#263;"
  ]
  node [
    id 878
    label "pozostawa&#263;"
  ]
  node [
    id 879
    label "zostawa&#263;"
  ]
  node [
    id 880
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 881
    label "adhere"
  ]
  node [
    id 882
    label "compass"
  ]
  node [
    id 883
    label "korzysta&#263;"
  ]
  node [
    id 884
    label "appreciation"
  ]
  node [
    id 885
    label "osi&#261;ga&#263;"
  ]
  node [
    id 886
    label "dociera&#263;"
  ]
  node [
    id 887
    label "get"
  ]
  node [
    id 888
    label "mierzy&#263;"
  ]
  node [
    id 889
    label "u&#380;ywa&#263;"
  ]
  node [
    id 890
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 891
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 892
    label "being"
  ]
  node [
    id 893
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 894
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 895
    label "p&#322;ywa&#263;"
  ]
  node [
    id 896
    label "run"
  ]
  node [
    id 897
    label "bangla&#263;"
  ]
  node [
    id 898
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 899
    label "przebiega&#263;"
  ]
  node [
    id 900
    label "wk&#322;ada&#263;"
  ]
  node [
    id 901
    label "proceed"
  ]
  node [
    id 902
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 903
    label "carry"
  ]
  node [
    id 904
    label "bywa&#263;"
  ]
  node [
    id 905
    label "dziama&#263;"
  ]
  node [
    id 906
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 907
    label "stara&#263;_si&#281;"
  ]
  node [
    id 908
    label "para"
  ]
  node [
    id 909
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 910
    label "str&#243;j"
  ]
  node [
    id 911
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 912
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 913
    label "krok"
  ]
  node [
    id 914
    label "tryb"
  ]
  node [
    id 915
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 916
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 917
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 918
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 919
    label "continue"
  ]
  node [
    id 920
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 921
    label "Ohio"
  ]
  node [
    id 922
    label "wci&#281;cie"
  ]
  node [
    id 923
    label "Nowy_York"
  ]
  node [
    id 924
    label "warstwa"
  ]
  node [
    id 925
    label "samopoczucie"
  ]
  node [
    id 926
    label "Illinois"
  ]
  node [
    id 927
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 928
    label "state"
  ]
  node [
    id 929
    label "Jukatan"
  ]
  node [
    id 930
    label "Kalifornia"
  ]
  node [
    id 931
    label "Wirginia"
  ]
  node [
    id 932
    label "wektor"
  ]
  node [
    id 933
    label "Teksas"
  ]
  node [
    id 934
    label "Goa"
  ]
  node [
    id 935
    label "Waszyngton"
  ]
  node [
    id 936
    label "Massachusetts"
  ]
  node [
    id 937
    label "Alaska"
  ]
  node [
    id 938
    label "Arakan"
  ]
  node [
    id 939
    label "Hawaje"
  ]
  node [
    id 940
    label "Maryland"
  ]
  node [
    id 941
    label "punkt"
  ]
  node [
    id 942
    label "Michigan"
  ]
  node [
    id 943
    label "Arizona"
  ]
  node [
    id 944
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 945
    label "Georgia"
  ]
  node [
    id 946
    label "Pensylwania"
  ]
  node [
    id 947
    label "shape"
  ]
  node [
    id 948
    label "Luizjana"
  ]
  node [
    id 949
    label "Nowy_Meksyk"
  ]
  node [
    id 950
    label "Alabama"
  ]
  node [
    id 951
    label "Kansas"
  ]
  node [
    id 952
    label "Oregon"
  ]
  node [
    id 953
    label "Floryda"
  ]
  node [
    id 954
    label "Oklahoma"
  ]
  node [
    id 955
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 240
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 680
  ]
  edge [
    source 19
    target 742
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 743
  ]
  edge [
    source 19
    target 744
  ]
  edge [
    source 19
    target 745
  ]
  edge [
    source 19
    target 746
  ]
  edge [
    source 19
    target 747
  ]
  edge [
    source 19
    target 748
  ]
  edge [
    source 19
    target 749
  ]
  edge [
    source 19
    target 750
  ]
  edge [
    source 19
    target 751
  ]
  edge [
    source 19
    target 752
  ]
  edge [
    source 19
    target 753
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 754
  ]
  edge [
    source 19
    target 755
  ]
  edge [
    source 19
    target 756
  ]
  edge [
    source 19
    target 757
  ]
  edge [
    source 19
    target 313
  ]
  edge [
    source 19
    target 758
  ]
  edge [
    source 19
    target 300
  ]
  edge [
    source 19
    target 759
  ]
  edge [
    source 19
    target 760
  ]
  edge [
    source 19
    target 761
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 762
  ]
  edge [
    source 19
    target 621
  ]
  edge [
    source 19
    target 763
  ]
  edge [
    source 19
    target 764
  ]
  edge [
    source 19
    target 765
  ]
  edge [
    source 19
    target 766
  ]
  edge [
    source 19
    target 767
  ]
  edge [
    source 19
    target 768
  ]
  edge [
    source 19
    target 769
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 771
  ]
  edge [
    source 19
    target 772
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 775
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 777
  ]
  edge [
    source 19
    target 778
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 66
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 388
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 574
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 832
  ]
  edge [
    source 19
    target 833
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 19
    target 841
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 403
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 63
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 865
  ]
  edge [
    source 20
    target 866
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 442
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 871
  ]
  edge [
    source 21
    target 86
  ]
  edge [
    source 21
    target 872
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 875
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 439
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 596
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 457
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 621
  ]
  edge [
    source 21
    target 677
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 126
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 233
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 338
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 397
  ]
  edge [
    source 21
    target 955
  ]
]
