graph [
  node [
    id 0
    label "wie&#380;a"
    origin "text"
  ]
  node [
    id 1
    label "pancerny"
    origin "text"
  ]
  node [
    id 2
    label "budynek"
  ]
  node [
    id 3
    label "tuner"
  ]
  node [
    id 4
    label "wzmacniacz"
  ]
  node [
    id 5
    label "strzelec"
  ]
  node [
    id 6
    label "odtwarzacz"
  ]
  node [
    id 7
    label "hejnalica"
  ]
  node [
    id 8
    label "sprz&#281;t_audio"
  ]
  node [
    id 9
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 10
    label "korektor"
  ]
  node [
    id 11
    label "zestaw"
  ]
  node [
    id 12
    label "struktura"
  ]
  node [
    id 13
    label "zbi&#243;r"
  ]
  node [
    id 14
    label "stage_set"
  ]
  node [
    id 15
    label "sk&#322;ada&#263;"
  ]
  node [
    id 16
    label "sygna&#322;"
  ]
  node [
    id 17
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 18
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 19
    label "&#380;o&#322;nierz"
  ]
  node [
    id 20
    label "figura"
  ]
  node [
    id 21
    label "cz&#322;owiek"
  ]
  node [
    id 22
    label "futbolista"
  ]
  node [
    id 23
    label "sportowiec"
  ]
  node [
    id 24
    label "Renata_Mauer"
  ]
  node [
    id 25
    label "balkon"
  ]
  node [
    id 26
    label "budowla"
  ]
  node [
    id 27
    label "pod&#322;oga"
  ]
  node [
    id 28
    label "kondygnacja"
  ]
  node [
    id 29
    label "skrzyd&#322;o"
  ]
  node [
    id 30
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 31
    label "dach"
  ]
  node [
    id 32
    label "strop"
  ]
  node [
    id 33
    label "klatka_schodowa"
  ]
  node [
    id 34
    label "przedpro&#380;e"
  ]
  node [
    id 35
    label "Pentagon"
  ]
  node [
    id 36
    label "alkierz"
  ]
  node [
    id 37
    label "front"
  ]
  node [
    id 38
    label "amplifikator"
  ]
  node [
    id 39
    label "radiator"
  ]
  node [
    id 40
    label "urz&#261;dzenie"
  ]
  node [
    id 41
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 42
    label "redaktor"
  ]
  node [
    id 43
    label "przybory_do_pisania"
  ]
  node [
    id 44
    label "counterweight"
  ]
  node [
    id 45
    label "kosmetyk_kolorowy"
  ]
  node [
    id 46
    label "artyku&#322;"
  ]
  node [
    id 47
    label "ci&#281;&#380;kozbrojny"
  ]
  node [
    id 48
    label "ochronny"
  ]
  node [
    id 49
    label "mocny"
  ]
  node [
    id 50
    label "odporny"
  ]
  node [
    id 51
    label "uodparnianie_si&#281;"
  ]
  node [
    id 52
    label "silny"
  ]
  node [
    id 53
    label "uodpornianie"
  ]
  node [
    id 54
    label "uodpornienie_si&#281;"
  ]
  node [
    id 55
    label "uodparnianie"
  ]
  node [
    id 56
    label "hartowny"
  ]
  node [
    id 57
    label "uodpornienie"
  ]
  node [
    id 58
    label "ochronnie"
  ]
  node [
    id 59
    label "bojowy"
  ]
  node [
    id 60
    label "zbrojny"
  ]
  node [
    id 61
    label "szczery"
  ]
  node [
    id 62
    label "niepodwa&#380;alny"
  ]
  node [
    id 63
    label "zdecydowany"
  ]
  node [
    id 64
    label "stabilny"
  ]
  node [
    id 65
    label "trudny"
  ]
  node [
    id 66
    label "krzepki"
  ]
  node [
    id 67
    label "du&#380;y"
  ]
  node [
    id 68
    label "wyrazisty"
  ]
  node [
    id 69
    label "przekonuj&#261;cy"
  ]
  node [
    id 70
    label "widoczny"
  ]
  node [
    id 71
    label "mocno"
  ]
  node [
    id 72
    label "wzmocni&#263;"
  ]
  node [
    id 73
    label "wzmacnia&#263;"
  ]
  node [
    id 74
    label "konkretny"
  ]
  node [
    id 75
    label "wytrzyma&#322;y"
  ]
  node [
    id 76
    label "silnie"
  ]
  node [
    id 77
    label "intensywnie"
  ]
  node [
    id 78
    label "meflochina"
  ]
  node [
    id 79
    label "dobry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
]
