graph [
  node [
    id 0
    label "pi&#281;&#263;set"
    origin "text"
  ]
  node [
    id 1
    label "dwadzie&#347;cia"
    origin "text"
  ]
  node [
    id 2
    label "cztery"
    origin "text"
  ]
  node [
    id 3
    label "tak"
    origin "text"
  ]
  node [
    id 4
    label "troszk&#281;"
    origin "text"
  ]
  node [
    id 5
    label "okr&#281;&#380;nie"
    origin "text"
  ]
  node [
    id 6
    label "jakby"
    origin "text"
  ]
  node [
    id 7
    label "zawraca&#263;"
    origin "text"
  ]
  node [
    id 8
    label "lewo"
    origin "text"
  ]
  node [
    id 9
    label "jak"
    origin "text"
  ]
  node [
    id 10
    label "patrzy&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "plan"
    origin "text"
  ]
  node [
    id 12
    label "okr&#281;&#380;ny"
  ]
  node [
    id 13
    label "po&#347;rednio"
  ]
  node [
    id 14
    label "zawile"
  ]
  node [
    id 15
    label "po&#347;redni"
  ]
  node [
    id 16
    label "zagmatwany"
  ]
  node [
    id 17
    label "zawi&#322;y"
  ]
  node [
    id 18
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 19
    label "round"
  ]
  node [
    id 20
    label "ok&#243;lny"
  ]
  node [
    id 21
    label "return"
  ]
  node [
    id 22
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 23
    label "szko&#322;a"
  ]
  node [
    id 24
    label "nielegalnie"
  ]
  node [
    id 25
    label "lewy"
  ]
  node [
    id 26
    label "podejrzanie"
  ]
  node [
    id 27
    label "kierunek"
  ]
  node [
    id 28
    label "kiepsko"
  ]
  node [
    id 29
    label "linia"
  ]
  node [
    id 30
    label "przebieg"
  ]
  node [
    id 31
    label "orientowa&#263;"
  ]
  node [
    id 32
    label "zorientowa&#263;"
  ]
  node [
    id 33
    label "praktyka"
  ]
  node [
    id 34
    label "skr&#281;cenie"
  ]
  node [
    id 35
    label "skr&#281;ci&#263;"
  ]
  node [
    id 36
    label "przeorientowanie"
  ]
  node [
    id 37
    label "g&#243;ra"
  ]
  node [
    id 38
    label "orientowanie"
  ]
  node [
    id 39
    label "zorientowanie"
  ]
  node [
    id 40
    label "ty&#322;"
  ]
  node [
    id 41
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 43
    label "przeorientowywanie"
  ]
  node [
    id 44
    label "bok"
  ]
  node [
    id 45
    label "ideologia"
  ]
  node [
    id 46
    label "skr&#281;canie"
  ]
  node [
    id 47
    label "orientacja"
  ]
  node [
    id 48
    label "metoda"
  ]
  node [
    id 49
    label "studia"
  ]
  node [
    id 50
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 51
    label "przeorientowa&#263;"
  ]
  node [
    id 52
    label "bearing"
  ]
  node [
    id 53
    label "spos&#243;b"
  ]
  node [
    id 54
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 55
    label "prz&#243;d"
  ]
  node [
    id 56
    label "skr&#281;ca&#263;"
  ]
  node [
    id 57
    label "system"
  ]
  node [
    id 58
    label "przeorientowywa&#263;"
  ]
  node [
    id 59
    label "marnie"
  ]
  node [
    id 60
    label "marny"
  ]
  node [
    id 61
    label "&#378;le"
  ]
  node [
    id 62
    label "kiepski"
  ]
  node [
    id 63
    label "choro"
  ]
  node [
    id 64
    label "unlawfully"
  ]
  node [
    id 65
    label "na_nielegalu"
  ]
  node [
    id 66
    label "nielegalny"
  ]
  node [
    id 67
    label "nieoficjalnie"
  ]
  node [
    id 68
    label "ciemny"
  ]
  node [
    id 69
    label "niepewnie"
  ]
  node [
    id 70
    label "zdanie"
  ]
  node [
    id 71
    label "lekcja"
  ]
  node [
    id 72
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 73
    label "skolaryzacja"
  ]
  node [
    id 74
    label "wiedza"
  ]
  node [
    id 75
    label "lesson"
  ]
  node [
    id 76
    label "niepokalanki"
  ]
  node [
    id 77
    label "kwalifikacje"
  ]
  node [
    id 78
    label "Mickiewicz"
  ]
  node [
    id 79
    label "muzyka"
  ]
  node [
    id 80
    label "klasa"
  ]
  node [
    id 81
    label "stopek"
  ]
  node [
    id 82
    label "school"
  ]
  node [
    id 83
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 84
    label "&#322;awa_szkolna"
  ]
  node [
    id 85
    label "przepisa&#263;"
  ]
  node [
    id 86
    label "instytucja"
  ]
  node [
    id 87
    label "nauka"
  ]
  node [
    id 88
    label "siedziba"
  ]
  node [
    id 89
    label "gabinet"
  ]
  node [
    id 90
    label "sekretariat"
  ]
  node [
    id 91
    label "szkolenie"
  ]
  node [
    id 92
    label "sztuba"
  ]
  node [
    id 93
    label "grupa"
  ]
  node [
    id 94
    label "do&#347;wiadczenie"
  ]
  node [
    id 95
    label "podr&#281;cznik"
  ]
  node [
    id 96
    label "zda&#263;"
  ]
  node [
    id 97
    label "tablica"
  ]
  node [
    id 98
    label "przepisanie"
  ]
  node [
    id 99
    label "kara"
  ]
  node [
    id 100
    label "teren_szko&#322;y"
  ]
  node [
    id 101
    label "form"
  ]
  node [
    id 102
    label "czas"
  ]
  node [
    id 103
    label "urszulanki"
  ]
  node [
    id 104
    label "absolwent"
  ]
  node [
    id 105
    label "w_lewo"
  ]
  node [
    id 106
    label "z_lewa"
  ]
  node [
    id 107
    label "na_lewo"
  ]
  node [
    id 108
    label "szemrany"
  ]
  node [
    id 109
    label "lewicowy"
  ]
  node [
    id 110
    label "wewn&#281;trzny"
  ]
  node [
    id 111
    label "na_czarno"
  ]
  node [
    id 112
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 113
    label "zobo"
  ]
  node [
    id 114
    label "byd&#322;o"
  ]
  node [
    id 115
    label "dzo"
  ]
  node [
    id 116
    label "yakalo"
  ]
  node [
    id 117
    label "zbi&#243;r"
  ]
  node [
    id 118
    label "kr&#281;torogie"
  ]
  node [
    id 119
    label "g&#322;owa"
  ]
  node [
    id 120
    label "livestock"
  ]
  node [
    id 121
    label "posp&#243;lstwo"
  ]
  node [
    id 122
    label "kraal"
  ]
  node [
    id 123
    label "czochrad&#322;o"
  ]
  node [
    id 124
    label "prze&#380;uwacz"
  ]
  node [
    id 125
    label "zebu"
  ]
  node [
    id 126
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 127
    label "bizon"
  ]
  node [
    id 128
    label "byd&#322;o_domowe"
  ]
  node [
    id 129
    label "reprezentacja"
  ]
  node [
    id 130
    label "przestrze&#324;"
  ]
  node [
    id 131
    label "intencja"
  ]
  node [
    id 132
    label "punkt"
  ]
  node [
    id 133
    label "perspektywa"
  ]
  node [
    id 134
    label "model"
  ]
  node [
    id 135
    label "miejsce_pracy"
  ]
  node [
    id 136
    label "device"
  ]
  node [
    id 137
    label "obraz"
  ]
  node [
    id 138
    label "rysunek"
  ]
  node [
    id 139
    label "agreement"
  ]
  node [
    id 140
    label "dekoracja"
  ]
  node [
    id 141
    label "wytw&#243;r"
  ]
  node [
    id 142
    label "pomys&#322;"
  ]
  node [
    id 143
    label "dru&#380;yna"
  ]
  node [
    id 144
    label "zesp&#243;&#322;"
  ]
  node [
    id 145
    label "deputation"
  ]
  node [
    id 146
    label "emblemat"
  ]
  node [
    id 147
    label "grafika"
  ]
  node [
    id 148
    label "ilustracja"
  ]
  node [
    id 149
    label "plastyka"
  ]
  node [
    id 150
    label "kreska"
  ]
  node [
    id 151
    label "shape"
  ]
  node [
    id 152
    label "teka"
  ]
  node [
    id 153
    label "picture"
  ]
  node [
    id 154
    label "kszta&#322;t"
  ]
  node [
    id 155
    label "photograph"
  ]
  node [
    id 156
    label "cz&#322;owiek"
  ]
  node [
    id 157
    label "matryca"
  ]
  node [
    id 158
    label "facet"
  ]
  node [
    id 159
    label "zi&#243;&#322;ko"
  ]
  node [
    id 160
    label "mildew"
  ]
  node [
    id 161
    label "miniatura"
  ]
  node [
    id 162
    label "ideal"
  ]
  node [
    id 163
    label "adaptation"
  ]
  node [
    id 164
    label "typ"
  ]
  node [
    id 165
    label "ruch"
  ]
  node [
    id 166
    label "imitacja"
  ]
  node [
    id 167
    label "pozowa&#263;"
  ]
  node [
    id 168
    label "orygina&#322;"
  ]
  node [
    id 169
    label "wz&#243;r"
  ]
  node [
    id 170
    label "motif"
  ]
  node [
    id 171
    label "prezenter"
  ]
  node [
    id 172
    label "pozowanie"
  ]
  node [
    id 173
    label "oktant"
  ]
  node [
    id 174
    label "przedzielenie"
  ]
  node [
    id 175
    label "przedzieli&#263;"
  ]
  node [
    id 176
    label "przestw&#243;r"
  ]
  node [
    id 177
    label "rozdziela&#263;"
  ]
  node [
    id 178
    label "nielito&#347;ciwy"
  ]
  node [
    id 179
    label "czasoprzestrze&#324;"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "niezmierzony"
  ]
  node [
    id 182
    label "bezbrze&#380;e"
  ]
  node [
    id 183
    label "rozdzielanie"
  ]
  node [
    id 184
    label "projekcja"
  ]
  node [
    id 185
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 186
    label "wypunktowa&#263;"
  ]
  node [
    id 187
    label "opinion"
  ]
  node [
    id 188
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 189
    label "inning"
  ]
  node [
    id 190
    label "zaj&#347;cie"
  ]
  node [
    id 191
    label "representation"
  ]
  node [
    id 192
    label "filmoteka"
  ]
  node [
    id 193
    label "widok"
  ]
  node [
    id 194
    label "podobrazie"
  ]
  node [
    id 195
    label "przeplot"
  ]
  node [
    id 196
    label "napisy"
  ]
  node [
    id 197
    label "pogl&#261;d"
  ]
  node [
    id 198
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 199
    label "oprawia&#263;"
  ]
  node [
    id 200
    label "ziarno"
  ]
  node [
    id 201
    label "human_body"
  ]
  node [
    id 202
    label "ostro&#347;&#263;"
  ]
  node [
    id 203
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 204
    label "pulment"
  ]
  node [
    id 205
    label "sztafa&#380;"
  ]
  node [
    id 206
    label "punktowa&#263;"
  ]
  node [
    id 207
    label "scena"
  ]
  node [
    id 208
    label "przedstawienie"
  ]
  node [
    id 209
    label "persona"
  ]
  node [
    id 210
    label "zjawisko"
  ]
  node [
    id 211
    label "oprawianie"
  ]
  node [
    id 212
    label "malarz"
  ]
  node [
    id 213
    label "uj&#281;cie"
  ]
  node [
    id 214
    label "parkiet"
  ]
  node [
    id 215
    label "t&#322;o"
  ]
  node [
    id 216
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 217
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 218
    label "effigy"
  ]
  node [
    id 219
    label "anamorfoza"
  ]
  node [
    id 220
    label "czo&#322;&#243;wka"
  ]
  node [
    id 221
    label "plama_barwna"
  ]
  node [
    id 222
    label "postprodukcja"
  ]
  node [
    id 223
    label "rola"
  ]
  node [
    id 224
    label "ty&#322;&#243;wka"
  ]
  node [
    id 225
    label "thinking"
  ]
  node [
    id 226
    label "ukradzenie"
  ]
  node [
    id 227
    label "pocz&#261;tki"
  ]
  node [
    id 228
    label "idea"
  ]
  node [
    id 229
    label "ukra&#347;&#263;"
  ]
  node [
    id 230
    label "przedmiot"
  ]
  node [
    id 231
    label "rezultat"
  ]
  node [
    id 232
    label "p&#322;&#243;d"
  ]
  node [
    id 233
    label "work"
  ]
  node [
    id 234
    label "patrze&#263;"
  ]
  node [
    id 235
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 236
    label "anticipation"
  ]
  node [
    id 237
    label "dystans"
  ]
  node [
    id 238
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 239
    label "posta&#263;"
  ]
  node [
    id 240
    label "patrzenie"
  ]
  node [
    id 241
    label "decentracja"
  ]
  node [
    id 242
    label "scene"
  ]
  node [
    id 243
    label "prognoza"
  ]
  node [
    id 244
    label "figura_geometryczna"
  ]
  node [
    id 245
    label "pojmowanie"
  ]
  node [
    id 246
    label "widzie&#263;"
  ]
  node [
    id 247
    label "krajobraz"
  ]
  node [
    id 248
    label "tryb"
  ]
  node [
    id 249
    label "expectation"
  ]
  node [
    id 250
    label "sznurownia"
  ]
  node [
    id 251
    label "upi&#281;kszanie"
  ]
  node [
    id 252
    label "adornment"
  ]
  node [
    id 253
    label "ozdoba"
  ]
  node [
    id 254
    label "pi&#281;kniejszy"
  ]
  node [
    id 255
    label "ferm"
  ]
  node [
    id 256
    label "scenografia"
  ]
  node [
    id 257
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 258
    label "wystr&#243;j"
  ]
  node [
    id 259
    label "obiekt_matematyczny"
  ]
  node [
    id 260
    label "stopie&#324;_pisma"
  ]
  node [
    id 261
    label "pozycja"
  ]
  node [
    id 262
    label "problemat"
  ]
  node [
    id 263
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 264
    label "obiekt"
  ]
  node [
    id 265
    label "point"
  ]
  node [
    id 266
    label "plamka"
  ]
  node [
    id 267
    label "mark"
  ]
  node [
    id 268
    label "ust&#281;p"
  ]
  node [
    id 269
    label "po&#322;o&#380;enie"
  ]
  node [
    id 270
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 271
    label "kres"
  ]
  node [
    id 272
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 273
    label "chwila"
  ]
  node [
    id 274
    label "podpunkt"
  ]
  node [
    id 275
    label "jednostka"
  ]
  node [
    id 276
    label "sprawa"
  ]
  node [
    id 277
    label "problematyka"
  ]
  node [
    id 278
    label "prosta"
  ]
  node [
    id 279
    label "wojsko"
  ]
  node [
    id 280
    label "zapunktowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 155
  ]
  edge [
    source 11
    target 156
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 159
  ]
  edge [
    source 11
    target 160
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 163
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 192
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 195
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 200
  ]
  edge [
    source 11
    target 201
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 203
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 208
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
]
