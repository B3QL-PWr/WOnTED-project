graph [
  node [
    id 0
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 1
    label "oficjalnie"
    origin "text"
  ]
  node [
    id 2
    label "gra"
    origin "text"
  ]
  node [
    id 3
    label "sztab"
    origin "text"
  ]
  node [
    id 4
    label "xiv"
    origin "text"
  ]
  node [
    id 5
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 6
    label "wo&#347;p"
    origin "text"
  ]
  node [
    id 7
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 8
    label "lista"
    origin "text"
  ]
  node [
    id 9
    label "serdecznie"
    origin "text"
  ]
  node [
    id 10
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 12
    label "wszystek"
    origin "text"
  ]
  node [
    id 13
    label "osoba"
    origin "text"
  ]
  node [
    id 14
    label "wielki"
    origin "text"
  ]
  node [
    id 15
    label "serce"
    origin "text"
  ]
  node [
    id 16
    label "doba"
  ]
  node [
    id 17
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 18
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 19
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 20
    label "dzi&#347;"
  ]
  node [
    id 21
    label "teraz"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 24
    label "jednocze&#347;nie"
  ]
  node [
    id 25
    label "tydzie&#324;"
  ]
  node [
    id 26
    label "noc"
  ]
  node [
    id 27
    label "dzie&#324;"
  ]
  node [
    id 28
    label "godzina"
  ]
  node [
    id 29
    label "long_time"
  ]
  node [
    id 30
    label "jednostka_geologiczna"
  ]
  node [
    id 31
    label "legalnie"
  ]
  node [
    id 32
    label "pow&#347;ci&#261;gliwie"
  ]
  node [
    id 33
    label "formalny"
  ]
  node [
    id 34
    label "oficjalny"
  ]
  node [
    id 35
    label "regularly"
  ]
  node [
    id 36
    label "formalizowanie"
  ]
  node [
    id 37
    label "formalnie"
  ]
  node [
    id 38
    label "pozorny"
  ]
  node [
    id 39
    label "kompletny"
  ]
  node [
    id 40
    label "prawdziwy"
  ]
  node [
    id 41
    label "sformalizowanie"
  ]
  node [
    id 42
    label "prawomocny"
  ]
  node [
    id 43
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 44
    label "jawny"
  ]
  node [
    id 45
    label "legalny"
  ]
  node [
    id 46
    label "legally"
  ]
  node [
    id 47
    label "zmienno&#347;&#263;"
  ]
  node [
    id 48
    label "play"
  ]
  node [
    id 49
    label "rozgrywka"
  ]
  node [
    id 50
    label "apparent_motion"
  ]
  node [
    id 51
    label "wydarzenie"
  ]
  node [
    id 52
    label "contest"
  ]
  node [
    id 53
    label "akcja"
  ]
  node [
    id 54
    label "komplet"
  ]
  node [
    id 55
    label "zabawa"
  ]
  node [
    id 56
    label "zasada"
  ]
  node [
    id 57
    label "rywalizacja"
  ]
  node [
    id 58
    label "zbijany"
  ]
  node [
    id 59
    label "post&#281;powanie"
  ]
  node [
    id 60
    label "game"
  ]
  node [
    id 61
    label "odg&#322;os"
  ]
  node [
    id 62
    label "Pok&#233;mon"
  ]
  node [
    id 63
    label "czynno&#347;&#263;"
  ]
  node [
    id 64
    label "synteza"
  ]
  node [
    id 65
    label "odtworzenie"
  ]
  node [
    id 66
    label "rekwizyt_do_gry"
  ]
  node [
    id 67
    label "resonance"
  ]
  node [
    id 68
    label "wydanie"
  ]
  node [
    id 69
    label "wpadni&#281;cie"
  ]
  node [
    id 70
    label "d&#378;wi&#281;k"
  ]
  node [
    id 71
    label "wpadanie"
  ]
  node [
    id 72
    label "wydawa&#263;"
  ]
  node [
    id 73
    label "sound"
  ]
  node [
    id 74
    label "brzmienie"
  ]
  node [
    id 75
    label "zjawisko"
  ]
  node [
    id 76
    label "wyda&#263;"
  ]
  node [
    id 77
    label "wpa&#347;&#263;"
  ]
  node [
    id 78
    label "note"
  ]
  node [
    id 79
    label "onomatopeja"
  ]
  node [
    id 80
    label "wpada&#263;"
  ]
  node [
    id 81
    label "cecha"
  ]
  node [
    id 82
    label "s&#261;d"
  ]
  node [
    id 83
    label "kognicja"
  ]
  node [
    id 84
    label "campaign"
  ]
  node [
    id 85
    label "rozprawa"
  ]
  node [
    id 86
    label "zachowanie"
  ]
  node [
    id 87
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 88
    label "fashion"
  ]
  node [
    id 89
    label "robienie"
  ]
  node [
    id 90
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 91
    label "zmierzanie"
  ]
  node [
    id 92
    label "przes&#322;anka"
  ]
  node [
    id 93
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 94
    label "kazanie"
  ]
  node [
    id 95
    label "trafienie"
  ]
  node [
    id 96
    label "rewan&#380;owy"
  ]
  node [
    id 97
    label "zagrywka"
  ]
  node [
    id 98
    label "faza"
  ]
  node [
    id 99
    label "euroliga"
  ]
  node [
    id 100
    label "interliga"
  ]
  node [
    id 101
    label "runda"
  ]
  node [
    id 102
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 103
    label "rozrywka"
  ]
  node [
    id 104
    label "impreza"
  ]
  node [
    id 105
    label "igraszka"
  ]
  node [
    id 106
    label "taniec"
  ]
  node [
    id 107
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 108
    label "gambling"
  ]
  node [
    id 109
    label "chwyt"
  ]
  node [
    id 110
    label "igra"
  ]
  node [
    id 111
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 112
    label "nabawienie_si&#281;"
  ]
  node [
    id 113
    label "ubaw"
  ]
  node [
    id 114
    label "wodzirej"
  ]
  node [
    id 115
    label "activity"
  ]
  node [
    id 116
    label "bezproblemowy"
  ]
  node [
    id 117
    label "przebiec"
  ]
  node [
    id 118
    label "charakter"
  ]
  node [
    id 119
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 120
    label "motyw"
  ]
  node [
    id 121
    label "przebiegni&#281;cie"
  ]
  node [
    id 122
    label "fabu&#322;a"
  ]
  node [
    id 123
    label "proces_technologiczny"
  ]
  node [
    id 124
    label "mieszanina"
  ]
  node [
    id 125
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 126
    label "fusion"
  ]
  node [
    id 127
    label "poj&#281;cie"
  ]
  node [
    id 128
    label "reakcja_chemiczna"
  ]
  node [
    id 129
    label "zestawienie"
  ]
  node [
    id 130
    label "uog&#243;lnienie"
  ]
  node [
    id 131
    label "puszczenie"
  ]
  node [
    id 132
    label "ustalenie"
  ]
  node [
    id 133
    label "wyst&#281;p"
  ]
  node [
    id 134
    label "reproduction"
  ]
  node [
    id 135
    label "przedstawienie"
  ]
  node [
    id 136
    label "przywr&#243;cenie"
  ]
  node [
    id 137
    label "w&#322;&#261;czenie"
  ]
  node [
    id 138
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 139
    label "restoration"
  ]
  node [
    id 140
    label "odbudowanie"
  ]
  node [
    id 141
    label "lekcja"
  ]
  node [
    id 142
    label "ensemble"
  ]
  node [
    id 143
    label "grupa"
  ]
  node [
    id 144
    label "klasa"
  ]
  node [
    id 145
    label "zestaw"
  ]
  node [
    id 146
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 147
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 148
    label "regu&#322;a_Allena"
  ]
  node [
    id 149
    label "base"
  ]
  node [
    id 150
    label "umowa"
  ]
  node [
    id 151
    label "obserwacja"
  ]
  node [
    id 152
    label "zasada_d'Alemberta"
  ]
  node [
    id 153
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 154
    label "normalizacja"
  ]
  node [
    id 155
    label "moralno&#347;&#263;"
  ]
  node [
    id 156
    label "criterion"
  ]
  node [
    id 157
    label "opis"
  ]
  node [
    id 158
    label "regu&#322;a_Glogera"
  ]
  node [
    id 159
    label "prawo_Mendla"
  ]
  node [
    id 160
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 161
    label "twierdzenie"
  ]
  node [
    id 162
    label "prawo"
  ]
  node [
    id 163
    label "standard"
  ]
  node [
    id 164
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 165
    label "spos&#243;b"
  ]
  node [
    id 166
    label "qualification"
  ]
  node [
    id 167
    label "dominion"
  ]
  node [
    id 168
    label "occupation"
  ]
  node [
    id 169
    label "podstawa"
  ]
  node [
    id 170
    label "substancja"
  ]
  node [
    id 171
    label "prawid&#322;o"
  ]
  node [
    id 172
    label "dywidenda"
  ]
  node [
    id 173
    label "przebieg"
  ]
  node [
    id 174
    label "operacja"
  ]
  node [
    id 175
    label "udzia&#322;"
  ]
  node [
    id 176
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 177
    label "commotion"
  ]
  node [
    id 178
    label "jazda"
  ]
  node [
    id 179
    label "czyn"
  ]
  node [
    id 180
    label "stock"
  ]
  node [
    id 181
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 182
    label "w&#281;ze&#322;"
  ]
  node [
    id 183
    label "wysoko&#347;&#263;"
  ]
  node [
    id 184
    label "instrument_strunowy"
  ]
  node [
    id 185
    label "pi&#322;ka"
  ]
  node [
    id 186
    label "zast&#281;p"
  ]
  node [
    id 187
    label "centrala"
  ]
  node [
    id 188
    label "siedziba"
  ]
  node [
    id 189
    label "dow&#243;dztwo"
  ]
  node [
    id 190
    label "b&#281;ben_wielki"
  ]
  node [
    id 191
    label "organizacja"
  ]
  node [
    id 192
    label "Bruksela"
  ]
  node [
    id 193
    label "administration"
  ]
  node [
    id 194
    label "miejsce"
  ]
  node [
    id 195
    label "zarz&#261;d"
  ]
  node [
    id 196
    label "stopa"
  ]
  node [
    id 197
    label "o&#347;rodek"
  ]
  node [
    id 198
    label "urz&#261;dzenie"
  ]
  node [
    id 199
    label "w&#322;adza"
  ]
  node [
    id 200
    label "&#321;ubianka"
  ]
  node [
    id 201
    label "miejsce_pracy"
  ]
  node [
    id 202
    label "dzia&#322;_personalny"
  ]
  node [
    id 203
    label "Kreml"
  ]
  node [
    id 204
    label "Bia&#322;y_Dom"
  ]
  node [
    id 205
    label "budynek"
  ]
  node [
    id 206
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 207
    label "sadowisko"
  ]
  node [
    id 208
    label "zesp&#243;&#322;"
  ]
  node [
    id 209
    label "dru&#380;yna_harcerska"
  ]
  node [
    id 210
    label "t&#322;um"
  ]
  node [
    id 211
    label "organ"
  ]
  node [
    id 212
    label "dow&#243;dca"
  ]
  node [
    id 213
    label "command"
  ]
  node [
    id 214
    label "przyw&#243;dztwo"
  ]
  node [
    id 215
    label "coating"
  ]
  node [
    id 216
    label "conclusion"
  ]
  node [
    id 217
    label "koniec"
  ]
  node [
    id 218
    label "seria"
  ]
  node [
    id 219
    label "rhythm"
  ]
  node [
    id 220
    label "turniej"
  ]
  node [
    id 221
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 222
    label "okr&#261;&#380;enie"
  ]
  node [
    id 223
    label "ostatnie_podrygi"
  ]
  node [
    id 224
    label "visitation"
  ]
  node [
    id 225
    label "agonia"
  ]
  node [
    id 226
    label "defenestracja"
  ]
  node [
    id 227
    label "punkt"
  ]
  node [
    id 228
    label "dzia&#322;anie"
  ]
  node [
    id 229
    label "kres"
  ]
  node [
    id 230
    label "mogi&#322;a"
  ]
  node [
    id 231
    label "kres_&#380;ycia"
  ]
  node [
    id 232
    label "szereg"
  ]
  node [
    id 233
    label "szeol"
  ]
  node [
    id 234
    label "pogrzebanie"
  ]
  node [
    id 235
    label "chwila"
  ]
  node [
    id 236
    label "&#380;a&#322;oba"
  ]
  node [
    id 237
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 238
    label "zabicie"
  ]
  node [
    id 239
    label "zbi&#243;r"
  ]
  node [
    id 240
    label "catalog"
  ]
  node [
    id 241
    label "pozycja"
  ]
  node [
    id 242
    label "tekst"
  ]
  node [
    id 243
    label "sumariusz"
  ]
  node [
    id 244
    label "book"
  ]
  node [
    id 245
    label "figurowa&#263;"
  ]
  node [
    id 246
    label "wyliczanka"
  ]
  node [
    id 247
    label "ekscerpcja"
  ]
  node [
    id 248
    label "j&#281;zykowo"
  ]
  node [
    id 249
    label "wypowied&#378;"
  ]
  node [
    id 250
    label "redakcja"
  ]
  node [
    id 251
    label "wytw&#243;r"
  ]
  node [
    id 252
    label "pomini&#281;cie"
  ]
  node [
    id 253
    label "dzie&#322;o"
  ]
  node [
    id 254
    label "preparacja"
  ]
  node [
    id 255
    label "odmianka"
  ]
  node [
    id 256
    label "opu&#347;ci&#263;"
  ]
  node [
    id 257
    label "koniektura"
  ]
  node [
    id 258
    label "pisa&#263;"
  ]
  node [
    id 259
    label "obelga"
  ]
  node [
    id 260
    label "egzemplarz"
  ]
  node [
    id 261
    label "series"
  ]
  node [
    id 262
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 263
    label "uprawianie"
  ]
  node [
    id 264
    label "praca_rolnicza"
  ]
  node [
    id 265
    label "collection"
  ]
  node [
    id 266
    label "dane"
  ]
  node [
    id 267
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 268
    label "pakiet_klimatyczny"
  ]
  node [
    id 269
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 270
    label "sum"
  ]
  node [
    id 271
    label "gathering"
  ]
  node [
    id 272
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 273
    label "album"
  ]
  node [
    id 274
    label "po&#322;o&#380;enie"
  ]
  node [
    id 275
    label "debit"
  ]
  node [
    id 276
    label "druk"
  ]
  node [
    id 277
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 278
    label "szata_graficzna"
  ]
  node [
    id 279
    label "szermierka"
  ]
  node [
    id 280
    label "spis"
  ]
  node [
    id 281
    label "ustawienie"
  ]
  node [
    id 282
    label "publikacja"
  ]
  node [
    id 283
    label "status"
  ]
  node [
    id 284
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 285
    label "adres"
  ]
  node [
    id 286
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 287
    label "rozmieszczenie"
  ]
  node [
    id 288
    label "sytuacja"
  ]
  node [
    id 289
    label "rz&#261;d"
  ]
  node [
    id 290
    label "redaktor"
  ]
  node [
    id 291
    label "awansowa&#263;"
  ]
  node [
    id 292
    label "wojsko"
  ]
  node [
    id 293
    label "bearing"
  ]
  node [
    id 294
    label "znaczenie"
  ]
  node [
    id 295
    label "awans"
  ]
  node [
    id 296
    label "awansowanie"
  ]
  node [
    id 297
    label "poster"
  ]
  node [
    id 298
    label "le&#380;e&#263;"
  ]
  node [
    id 299
    label "entliczek"
  ]
  node [
    id 300
    label "wiersz"
  ]
  node [
    id 301
    label "pentliczek"
  ]
  node [
    id 302
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 303
    label "szczerze"
  ]
  node [
    id 304
    label "serdeczny"
  ]
  node [
    id 305
    label "mi&#322;o"
  ]
  node [
    id 306
    label "siarczy&#347;cie"
  ]
  node [
    id 307
    label "przyja&#378;nie"
  ]
  node [
    id 308
    label "przychylnie"
  ]
  node [
    id 309
    label "mi&#322;y"
  ]
  node [
    id 310
    label "dobrze"
  ]
  node [
    id 311
    label "pleasantly"
  ]
  node [
    id 312
    label "deliciously"
  ]
  node [
    id 313
    label "przyjemny"
  ]
  node [
    id 314
    label "gratifyingly"
  ]
  node [
    id 315
    label "szczery"
  ]
  node [
    id 316
    label "s&#322;usznie"
  ]
  node [
    id 317
    label "szczero"
  ]
  node [
    id 318
    label "hojnie"
  ]
  node [
    id 319
    label "honestly"
  ]
  node [
    id 320
    label "przekonuj&#261;co"
  ]
  node [
    id 321
    label "outspokenly"
  ]
  node [
    id 322
    label "artlessly"
  ]
  node [
    id 323
    label "uczciwie"
  ]
  node [
    id 324
    label "bluffly"
  ]
  node [
    id 325
    label "dosadnie"
  ]
  node [
    id 326
    label "silnie"
  ]
  node [
    id 327
    label "siarczysty"
  ]
  node [
    id 328
    label "drogi"
  ]
  node [
    id 329
    label "ciep&#322;y"
  ]
  node [
    id 330
    label "&#380;yczliwy"
  ]
  node [
    id 331
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 332
    label "invite"
  ]
  node [
    id 333
    label "ask"
  ]
  node [
    id 334
    label "oferowa&#263;"
  ]
  node [
    id 335
    label "zach&#281;ca&#263;"
  ]
  node [
    id 336
    label "volunteer"
  ]
  node [
    id 337
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 338
    label "ca&#322;y"
  ]
  node [
    id 339
    label "jedyny"
  ]
  node [
    id 340
    label "du&#380;y"
  ]
  node [
    id 341
    label "zdr&#243;w"
  ]
  node [
    id 342
    label "calu&#347;ko"
  ]
  node [
    id 343
    label "&#380;ywy"
  ]
  node [
    id 344
    label "pe&#322;ny"
  ]
  node [
    id 345
    label "podobny"
  ]
  node [
    id 346
    label "ca&#322;o"
  ]
  node [
    id 347
    label "Chocho&#322;"
  ]
  node [
    id 348
    label "Herkules_Poirot"
  ]
  node [
    id 349
    label "Edyp"
  ]
  node [
    id 350
    label "ludzko&#347;&#263;"
  ]
  node [
    id 351
    label "parali&#380;owa&#263;"
  ]
  node [
    id 352
    label "Harry_Potter"
  ]
  node [
    id 353
    label "Casanova"
  ]
  node [
    id 354
    label "Gargantua"
  ]
  node [
    id 355
    label "Zgredek"
  ]
  node [
    id 356
    label "Winnetou"
  ]
  node [
    id 357
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 358
    label "posta&#263;"
  ]
  node [
    id 359
    label "Dulcynea"
  ]
  node [
    id 360
    label "kategoria_gramatyczna"
  ]
  node [
    id 361
    label "g&#322;owa"
  ]
  node [
    id 362
    label "figura"
  ]
  node [
    id 363
    label "portrecista"
  ]
  node [
    id 364
    label "person"
  ]
  node [
    id 365
    label "Sherlock_Holmes"
  ]
  node [
    id 366
    label "Quasimodo"
  ]
  node [
    id 367
    label "Plastu&#347;"
  ]
  node [
    id 368
    label "Faust"
  ]
  node [
    id 369
    label "Wallenrod"
  ]
  node [
    id 370
    label "Dwukwiat"
  ]
  node [
    id 371
    label "koniugacja"
  ]
  node [
    id 372
    label "profanum"
  ]
  node [
    id 373
    label "Don_Juan"
  ]
  node [
    id 374
    label "Don_Kiszot"
  ]
  node [
    id 375
    label "mikrokosmos"
  ]
  node [
    id 376
    label "duch"
  ]
  node [
    id 377
    label "antropochoria"
  ]
  node [
    id 378
    label "oddzia&#322;ywanie"
  ]
  node [
    id 379
    label "Hamlet"
  ]
  node [
    id 380
    label "Werter"
  ]
  node [
    id 381
    label "istota"
  ]
  node [
    id 382
    label "Szwejk"
  ]
  node [
    id 383
    label "homo_sapiens"
  ]
  node [
    id 384
    label "mentalno&#347;&#263;"
  ]
  node [
    id 385
    label "superego"
  ]
  node [
    id 386
    label "psychika"
  ]
  node [
    id 387
    label "wn&#281;trze"
  ]
  node [
    id 388
    label "charakterystyka"
  ]
  node [
    id 389
    label "cz&#322;owiek"
  ]
  node [
    id 390
    label "zaistnie&#263;"
  ]
  node [
    id 391
    label "Osjan"
  ]
  node [
    id 392
    label "kto&#347;"
  ]
  node [
    id 393
    label "wygl&#261;d"
  ]
  node [
    id 394
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 395
    label "osobowo&#347;&#263;"
  ]
  node [
    id 396
    label "trim"
  ]
  node [
    id 397
    label "poby&#263;"
  ]
  node [
    id 398
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 399
    label "Aspazja"
  ]
  node [
    id 400
    label "punkt_widzenia"
  ]
  node [
    id 401
    label "kompleksja"
  ]
  node [
    id 402
    label "wytrzyma&#263;"
  ]
  node [
    id 403
    label "budowa"
  ]
  node [
    id 404
    label "formacja"
  ]
  node [
    id 405
    label "pozosta&#263;"
  ]
  node [
    id 406
    label "point"
  ]
  node [
    id 407
    label "go&#347;&#263;"
  ]
  node [
    id 408
    label "hamper"
  ]
  node [
    id 409
    label "spasm"
  ]
  node [
    id 410
    label "mrozi&#263;"
  ]
  node [
    id 411
    label "pora&#380;a&#263;"
  ]
  node [
    id 412
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 413
    label "fleksja"
  ]
  node [
    id 414
    label "liczba"
  ]
  node [
    id 415
    label "coupling"
  ]
  node [
    id 416
    label "tryb"
  ]
  node [
    id 417
    label "czasownik"
  ]
  node [
    id 418
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 419
    label "orz&#281;sek"
  ]
  node [
    id 420
    label "fotograf"
  ]
  node [
    id 421
    label "malarz"
  ]
  node [
    id 422
    label "artysta"
  ]
  node [
    id 423
    label "powodowanie"
  ]
  node [
    id 424
    label "hipnotyzowanie"
  ]
  node [
    id 425
    label "&#347;lad"
  ]
  node [
    id 426
    label "docieranie"
  ]
  node [
    id 427
    label "natural_process"
  ]
  node [
    id 428
    label "wdzieranie_si&#281;"
  ]
  node [
    id 429
    label "act"
  ]
  node [
    id 430
    label "rezultat"
  ]
  node [
    id 431
    label "lobbysta"
  ]
  node [
    id 432
    label "pryncypa&#322;"
  ]
  node [
    id 433
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 434
    label "kszta&#322;t"
  ]
  node [
    id 435
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 436
    label "wiedza"
  ]
  node [
    id 437
    label "kierowa&#263;"
  ]
  node [
    id 438
    label "alkohol"
  ]
  node [
    id 439
    label "zdolno&#347;&#263;"
  ]
  node [
    id 440
    label "&#380;ycie"
  ]
  node [
    id 441
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 442
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 443
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 444
    label "sztuka"
  ]
  node [
    id 445
    label "dekiel"
  ]
  node [
    id 446
    label "ro&#347;lina"
  ]
  node [
    id 447
    label "&#347;ci&#281;cie"
  ]
  node [
    id 448
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 449
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 450
    label "&#347;ci&#281;gno"
  ]
  node [
    id 451
    label "noosfera"
  ]
  node [
    id 452
    label "byd&#322;o"
  ]
  node [
    id 453
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 454
    label "makrocefalia"
  ]
  node [
    id 455
    label "obiekt"
  ]
  node [
    id 456
    label "ucho"
  ]
  node [
    id 457
    label "g&#243;ra"
  ]
  node [
    id 458
    label "m&#243;zg"
  ]
  node [
    id 459
    label "kierownictwo"
  ]
  node [
    id 460
    label "fryzura"
  ]
  node [
    id 461
    label "umys&#322;"
  ]
  node [
    id 462
    label "cia&#322;o"
  ]
  node [
    id 463
    label "cz&#322;onek"
  ]
  node [
    id 464
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 465
    label "czaszka"
  ]
  node [
    id 466
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 467
    label "allochoria"
  ]
  node [
    id 468
    label "p&#322;aszczyzna"
  ]
  node [
    id 469
    label "przedmiot"
  ]
  node [
    id 470
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 471
    label "bierka_szachowa"
  ]
  node [
    id 472
    label "obiekt_matematyczny"
  ]
  node [
    id 473
    label "gestaltyzm"
  ]
  node [
    id 474
    label "styl"
  ]
  node [
    id 475
    label "obraz"
  ]
  node [
    id 476
    label "rzecz"
  ]
  node [
    id 477
    label "character"
  ]
  node [
    id 478
    label "rze&#378;ba"
  ]
  node [
    id 479
    label "stylistyka"
  ]
  node [
    id 480
    label "figure"
  ]
  node [
    id 481
    label "antycypacja"
  ]
  node [
    id 482
    label "ornamentyka"
  ]
  node [
    id 483
    label "informacja"
  ]
  node [
    id 484
    label "facet"
  ]
  node [
    id 485
    label "popis"
  ]
  node [
    id 486
    label "symetria"
  ]
  node [
    id 487
    label "lingwistyka_kognitywna"
  ]
  node [
    id 488
    label "karta"
  ]
  node [
    id 489
    label "shape"
  ]
  node [
    id 490
    label "podzbi&#243;r"
  ]
  node [
    id 491
    label "perspektywa"
  ]
  node [
    id 492
    label "dziedzina"
  ]
  node [
    id 493
    label "Szekspir"
  ]
  node [
    id 494
    label "Mickiewicz"
  ]
  node [
    id 495
    label "cierpienie"
  ]
  node [
    id 496
    label "piek&#322;o"
  ]
  node [
    id 497
    label "human_body"
  ]
  node [
    id 498
    label "ofiarowywanie"
  ]
  node [
    id 499
    label "sfera_afektywna"
  ]
  node [
    id 500
    label "nekromancja"
  ]
  node [
    id 501
    label "Po&#347;wist"
  ]
  node [
    id 502
    label "podekscytowanie"
  ]
  node [
    id 503
    label "deformowanie"
  ]
  node [
    id 504
    label "sumienie"
  ]
  node [
    id 505
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 506
    label "deformowa&#263;"
  ]
  node [
    id 507
    label "zjawa"
  ]
  node [
    id 508
    label "zmar&#322;y"
  ]
  node [
    id 509
    label "istota_nadprzyrodzona"
  ]
  node [
    id 510
    label "power"
  ]
  node [
    id 511
    label "entity"
  ]
  node [
    id 512
    label "ofiarowywa&#263;"
  ]
  node [
    id 513
    label "oddech"
  ]
  node [
    id 514
    label "seksualno&#347;&#263;"
  ]
  node [
    id 515
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 516
    label "byt"
  ]
  node [
    id 517
    label "si&#322;a"
  ]
  node [
    id 518
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 519
    label "ego"
  ]
  node [
    id 520
    label "ofiarowanie"
  ]
  node [
    id 521
    label "fizjonomia"
  ]
  node [
    id 522
    label "kompleks"
  ]
  node [
    id 523
    label "zapalno&#347;&#263;"
  ]
  node [
    id 524
    label "T&#281;sknica"
  ]
  node [
    id 525
    label "ofiarowa&#263;"
  ]
  node [
    id 526
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 527
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 528
    label "passion"
  ]
  node [
    id 529
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 530
    label "atom"
  ]
  node [
    id 531
    label "odbicie"
  ]
  node [
    id 532
    label "przyroda"
  ]
  node [
    id 533
    label "Ziemia"
  ]
  node [
    id 534
    label "kosmos"
  ]
  node [
    id 535
    label "miniatura"
  ]
  node [
    id 536
    label "znaczny"
  ]
  node [
    id 537
    label "wyj&#261;tkowy"
  ]
  node [
    id 538
    label "nieprzeci&#281;tny"
  ]
  node [
    id 539
    label "wysoce"
  ]
  node [
    id 540
    label "wa&#380;ny"
  ]
  node [
    id 541
    label "wybitny"
  ]
  node [
    id 542
    label "dupny"
  ]
  node [
    id 543
    label "wysoki"
  ]
  node [
    id 544
    label "intensywnie"
  ]
  node [
    id 545
    label "nieprzeci&#281;tnie"
  ]
  node [
    id 546
    label "niespotykany"
  ]
  node [
    id 547
    label "wydatny"
  ]
  node [
    id 548
    label "wspania&#322;y"
  ]
  node [
    id 549
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 550
    label "&#347;wietny"
  ]
  node [
    id 551
    label "imponuj&#261;cy"
  ]
  node [
    id 552
    label "wybitnie"
  ]
  node [
    id 553
    label "celny"
  ]
  node [
    id 554
    label "&#380;ywny"
  ]
  node [
    id 555
    label "naturalny"
  ]
  node [
    id 556
    label "naprawd&#281;"
  ]
  node [
    id 557
    label "realnie"
  ]
  node [
    id 558
    label "zgodny"
  ]
  node [
    id 559
    label "m&#261;dry"
  ]
  node [
    id 560
    label "prawdziwie"
  ]
  node [
    id 561
    label "wyj&#261;tkowo"
  ]
  node [
    id 562
    label "inny"
  ]
  node [
    id 563
    label "znacznie"
  ]
  node [
    id 564
    label "zauwa&#380;alny"
  ]
  node [
    id 565
    label "wynios&#322;y"
  ]
  node [
    id 566
    label "dono&#347;ny"
  ]
  node [
    id 567
    label "silny"
  ]
  node [
    id 568
    label "wa&#380;nie"
  ]
  node [
    id 569
    label "istotnie"
  ]
  node [
    id 570
    label "eksponowany"
  ]
  node [
    id 571
    label "dobry"
  ]
  node [
    id 572
    label "do_dupy"
  ]
  node [
    id 573
    label "z&#322;y"
  ]
  node [
    id 574
    label "dobro&#263;"
  ]
  node [
    id 575
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 576
    label "pulsowa&#263;"
  ]
  node [
    id 577
    label "koniuszek_serca"
  ]
  node [
    id 578
    label "pulsowanie"
  ]
  node [
    id 579
    label "nastawienie"
  ]
  node [
    id 580
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 581
    label "wola"
  ]
  node [
    id 582
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 583
    label "courage"
  ]
  node [
    id 584
    label "przedsionek"
  ]
  node [
    id 585
    label "systol"
  ]
  node [
    id 586
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 587
    label "heart"
  ]
  node [
    id 588
    label "dzwon"
  ]
  node [
    id 589
    label "strunowiec"
  ]
  node [
    id 590
    label "kier"
  ]
  node [
    id 591
    label "elektrokardiografia"
  ]
  node [
    id 592
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 593
    label "podroby"
  ]
  node [
    id 594
    label "dusza"
  ]
  node [
    id 595
    label "mi&#281;sie&#324;"
  ]
  node [
    id 596
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 597
    label "wsierdzie"
  ]
  node [
    id 598
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 599
    label "favor"
  ]
  node [
    id 600
    label "pikawa"
  ]
  node [
    id 601
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 602
    label "zastawka"
  ]
  node [
    id 603
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 604
    label "komora"
  ]
  node [
    id 605
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 606
    label "kardiografia"
  ]
  node [
    id 607
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 608
    label "z&#322;amanie"
  ]
  node [
    id 609
    label "set"
  ]
  node [
    id 610
    label "gotowanie_si&#281;"
  ]
  node [
    id 611
    label "oddzia&#322;anie"
  ]
  node [
    id 612
    label "ponastawianie"
  ]
  node [
    id 613
    label "powaga"
  ]
  node [
    id 614
    label "z&#322;o&#380;enie"
  ]
  node [
    id 615
    label "podej&#347;cie"
  ]
  node [
    id 616
    label "umieszczenie"
  ]
  node [
    id 617
    label "ukierunkowanie"
  ]
  node [
    id 618
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 619
    label "go&#322;&#261;bek"
  ]
  node [
    id 620
    label "dobro"
  ]
  node [
    id 621
    label "zajawka"
  ]
  node [
    id 622
    label "emocja"
  ]
  node [
    id 623
    label "oskoma"
  ]
  node [
    id 624
    label "mniemanie"
  ]
  node [
    id 625
    label "inclination"
  ]
  node [
    id 626
    label "wish"
  ]
  node [
    id 627
    label "spirala"
  ]
  node [
    id 628
    label "p&#322;at"
  ]
  node [
    id 629
    label "comeliness"
  ]
  node [
    id 630
    label "kielich"
  ]
  node [
    id 631
    label "face"
  ]
  node [
    id 632
    label "blaszka"
  ]
  node [
    id 633
    label "p&#281;tla"
  ]
  node [
    id 634
    label "pasmo"
  ]
  node [
    id 635
    label "linearno&#347;&#263;"
  ]
  node [
    id 636
    label "gwiazda"
  ]
  node [
    id 637
    label "tkanka"
  ]
  node [
    id 638
    label "jednostka_organizacyjna"
  ]
  node [
    id 639
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 640
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 641
    label "tw&#243;r"
  ]
  node [
    id 642
    label "organogeneza"
  ]
  node [
    id 643
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 644
    label "struktura_anatomiczna"
  ]
  node [
    id 645
    label "uk&#322;ad"
  ]
  node [
    id 646
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 647
    label "dekortykacja"
  ]
  node [
    id 648
    label "Izba_Konsyliarska"
  ]
  node [
    id 649
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 650
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 651
    label "stomia"
  ]
  node [
    id 652
    label "okolica"
  ]
  node [
    id 653
    label "Komitet_Region&#243;w"
  ]
  node [
    id 654
    label "dogrza&#263;"
  ]
  node [
    id 655
    label "niedow&#322;ad_po&#322;owiczy"
  ]
  node [
    id 656
    label "fosfagen"
  ]
  node [
    id 657
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 658
    label "dogrzewa&#263;"
  ]
  node [
    id 659
    label "dogrzanie"
  ]
  node [
    id 660
    label "dogrzewanie"
  ]
  node [
    id 661
    label "hemiplegia"
  ]
  node [
    id 662
    label "elektromiografia"
  ]
  node [
    id 663
    label "brzusiec"
  ]
  node [
    id 664
    label "masa_mi&#281;&#347;niowa"
  ]
  node [
    id 665
    label "przyczep"
  ]
  node [
    id 666
    label "m&#322;ot"
  ]
  node [
    id 667
    label "znak"
  ]
  node [
    id 668
    label "drzewo"
  ]
  node [
    id 669
    label "pr&#243;ba"
  ]
  node [
    id 670
    label "attribute"
  ]
  node [
    id 671
    label "marka"
  ]
  node [
    id 672
    label "kartka"
  ]
  node [
    id 673
    label "danie"
  ]
  node [
    id 674
    label "menu"
  ]
  node [
    id 675
    label "zezwolenie"
  ]
  node [
    id 676
    label "restauracja"
  ]
  node [
    id 677
    label "chart"
  ]
  node [
    id 678
    label "p&#322;ytka"
  ]
  node [
    id 679
    label "formularz"
  ]
  node [
    id 680
    label "ticket"
  ]
  node [
    id 681
    label "cennik"
  ]
  node [
    id 682
    label "oferta"
  ]
  node [
    id 683
    label "komputer"
  ]
  node [
    id 684
    label "charter"
  ]
  node [
    id 685
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 686
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 687
    label "kartonik"
  ]
  node [
    id 688
    label "circuit_board"
  ]
  node [
    id 689
    label "agitation"
  ]
  node [
    id 690
    label "podniecenie_si&#281;"
  ]
  node [
    id 691
    label "poruszenie"
  ]
  node [
    id 692
    label "nastr&#243;j"
  ]
  node [
    id 693
    label "excitation"
  ]
  node [
    id 694
    label "asymilowanie"
  ]
  node [
    id 695
    label "wapniak"
  ]
  node [
    id 696
    label "asymilowa&#263;"
  ]
  node [
    id 697
    label "os&#322;abia&#263;"
  ]
  node [
    id 698
    label "hominid"
  ]
  node [
    id 699
    label "podw&#322;adny"
  ]
  node [
    id 700
    label "os&#322;abianie"
  ]
  node [
    id 701
    label "dwun&#243;g"
  ]
  node [
    id 702
    label "nasada"
  ]
  node [
    id 703
    label "wz&#243;r"
  ]
  node [
    id 704
    label "senior"
  ]
  node [
    id 705
    label "Adam"
  ]
  node [
    id 706
    label "polifag"
  ]
  node [
    id 707
    label "sprawa"
  ]
  node [
    id 708
    label "ust&#281;p"
  ]
  node [
    id 709
    label "plan"
  ]
  node [
    id 710
    label "problemat"
  ]
  node [
    id 711
    label "plamka"
  ]
  node [
    id 712
    label "stopie&#324;_pisma"
  ]
  node [
    id 713
    label "jednostka"
  ]
  node [
    id 714
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 715
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 716
    label "mark"
  ]
  node [
    id 717
    label "prosta"
  ]
  node [
    id 718
    label "problematyka"
  ]
  node [
    id 719
    label "zapunktowa&#263;"
  ]
  node [
    id 720
    label "podpunkt"
  ]
  node [
    id 721
    label "przestrze&#324;"
  ]
  node [
    id 722
    label "warto&#347;&#263;"
  ]
  node [
    id 723
    label "jako&#347;&#263;"
  ]
  node [
    id 724
    label "nieoboj&#281;tno&#347;&#263;"
  ]
  node [
    id 725
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 726
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 727
    label "zmienia&#263;"
  ]
  node [
    id 728
    label "corrupt"
  ]
  node [
    id 729
    label "zmienianie"
  ]
  node [
    id 730
    label "distortion"
  ]
  node [
    id 731
    label "contortion"
  ]
  node [
    id 732
    label "struktura"
  ]
  node [
    id 733
    label "group"
  ]
  node [
    id 734
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 735
    label "ligand"
  ]
  node [
    id 736
    label "band"
  ]
  node [
    id 737
    label "wi&#261;zanie_koordynacyjne"
  ]
  node [
    id 738
    label "riot"
  ]
  node [
    id 739
    label "k&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 740
    label "wzbiera&#263;"
  ]
  node [
    id 741
    label "pracowa&#263;"
  ]
  node [
    id 742
    label "proceed"
  ]
  node [
    id 743
    label "bycie"
  ]
  node [
    id 744
    label "throb"
  ]
  node [
    id 745
    label "ripple"
  ]
  node [
    id 746
    label "pracowanie"
  ]
  node [
    id 747
    label "badanie"
  ]
  node [
    id 748
    label "cardiography"
  ]
  node [
    id 749
    label "spoczynkowy"
  ]
  node [
    id 750
    label "kolor"
  ]
  node [
    id 751
    label "core"
  ]
  node [
    id 752
    label "droga"
  ]
  node [
    id 753
    label "ukochanie"
  ]
  node [
    id 754
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 755
    label "feblik"
  ]
  node [
    id 756
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 757
    label "podnieci&#263;"
  ]
  node [
    id 758
    label "numer"
  ]
  node [
    id 759
    label "po&#380;ycie"
  ]
  node [
    id 760
    label "tendency"
  ]
  node [
    id 761
    label "podniecenie"
  ]
  node [
    id 762
    label "afekt"
  ]
  node [
    id 763
    label "zakochanie"
  ]
  node [
    id 764
    label "seks"
  ]
  node [
    id 765
    label "podniecanie"
  ]
  node [
    id 766
    label "imisja"
  ]
  node [
    id 767
    label "love"
  ]
  node [
    id 768
    label "rozmna&#380;anie"
  ]
  node [
    id 769
    label "ruch_frykcyjny"
  ]
  node [
    id 770
    label "na_pieska"
  ]
  node [
    id 771
    label "pozycja_misjonarska"
  ]
  node [
    id 772
    label "wi&#281;&#378;"
  ]
  node [
    id 773
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 774
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 775
    label "z&#322;&#261;czenie"
  ]
  node [
    id 776
    label "gra_wst&#281;pna"
  ]
  node [
    id 777
    label "erotyka"
  ]
  node [
    id 778
    label "baraszki"
  ]
  node [
    id 779
    label "po&#380;&#261;danie"
  ]
  node [
    id 780
    label "wzw&#243;d"
  ]
  node [
    id 781
    label "podnieca&#263;"
  ]
  node [
    id 782
    label "&#380;elazko"
  ]
  node [
    id 783
    label "pi&#243;ro"
  ]
  node [
    id 784
    label "odwaga"
  ]
  node [
    id 785
    label "mind"
  ]
  node [
    id 786
    label "sztabka"
  ]
  node [
    id 787
    label "rdze&#324;"
  ]
  node [
    id 788
    label "schody"
  ]
  node [
    id 789
    label "pupa"
  ]
  node [
    id 790
    label "klocek"
  ]
  node [
    id 791
    label "instrument_smyczkowy"
  ]
  node [
    id 792
    label "lina"
  ]
  node [
    id 793
    label "motor"
  ]
  node [
    id 794
    label "mi&#281;kisz"
  ]
  node [
    id 795
    label "marrow"
  ]
  node [
    id 796
    label "facjata"
  ]
  node [
    id 797
    label "twarz"
  ]
  node [
    id 798
    label "energia"
  ]
  node [
    id 799
    label "zapa&#322;"
  ]
  node [
    id 800
    label "carillon"
  ]
  node [
    id 801
    label "dzwonnica"
  ]
  node [
    id 802
    label "dzwonek_r&#281;czny"
  ]
  node [
    id 803
    label "sygnalizator"
  ]
  node [
    id 804
    label "ludwisarnia"
  ]
  node [
    id 805
    label "podmiot"
  ]
  node [
    id 806
    label "self"
  ]
  node [
    id 807
    label "oczko_Hessego"
  ]
  node [
    id 808
    label "uk&#322;ad_pokarmowy"
  ]
  node [
    id 809
    label "cewa_nerwowa"
  ]
  node [
    id 810
    label "chorda"
  ]
  node [
    id 811
    label "zwierz&#281;"
  ]
  node [
    id 812
    label "strunowce"
  ]
  node [
    id 813
    label "ogon"
  ]
  node [
    id 814
    label "gardziel"
  ]
  node [
    id 815
    label "_id"
  ]
  node [
    id 816
    label "ignorantness"
  ]
  node [
    id 817
    label "niewiedza"
  ]
  node [
    id 818
    label "unconsciousness"
  ]
  node [
    id 819
    label "stan"
  ]
  node [
    id 820
    label "psychoanaliza"
  ]
  node [
    id 821
    label "Freud"
  ]
  node [
    id 822
    label "poczucie_w&#322;asnej_warto&#347;ci"
  ]
  node [
    id 823
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 824
    label "zamek"
  ]
  node [
    id 825
    label "tama"
  ]
  node [
    id 826
    label "mechanizm"
  ]
  node [
    id 827
    label "b&#322;ona"
  ]
  node [
    id 828
    label "endocardium"
  ]
  node [
    id 829
    label "b&#322;&#281;dnik_kostny"
  ]
  node [
    id 830
    label "zapowied&#378;"
  ]
  node [
    id 831
    label "pomieszczenie"
  ]
  node [
    id 832
    label "preview"
  ]
  node [
    id 833
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 834
    label "izba"
  ]
  node [
    id 835
    label "zal&#261;&#380;nia"
  ]
  node [
    id 836
    label "jaskinia"
  ]
  node [
    id 837
    label "nora"
  ]
  node [
    id 838
    label "wyrobisko"
  ]
  node [
    id 839
    label "spi&#380;arnia"
  ]
  node [
    id 840
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 841
    label "towar"
  ]
  node [
    id 842
    label "jedzenie"
  ]
  node [
    id 843
    label "mi&#281;so"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 315
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 15
    target 434
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 389
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 499
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 502
  ]
  edge [
    source 15
    target 503
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 504
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 505
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 510
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 511
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 514
  ]
  edge [
    source 15
    target 515
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 518
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 519
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 521
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 522
  ]
  edge [
    source 15
    target 488
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 523
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 526
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 528
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 281
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 293
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 361
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 455
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 450
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 350
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 358
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 362
  ]
  edge [
    source 15
    target 363
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 372
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 376
  ]
  edge [
    source 15
    target 377
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 274
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 472
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 292
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 241
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 469
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 51
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 270
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 328
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 496
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 444
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 516
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 489
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
]
