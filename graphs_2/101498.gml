graph [
  node [
    id 0
    label "barnet"
    origin "text"
  ]
  node [
    id 1
    label "femininum"
    origin "text"
  ]
  node [
    id 2
    label "cent"
    origin "text"
  ]
  node [
    id 3
    label "moneta"
  ]
  node [
    id 4
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 5
    label "awers"
  ]
  node [
    id 6
    label "legenda"
  ]
  node [
    id 7
    label "liga"
  ]
  node [
    id 8
    label "rewers"
  ]
  node [
    id 9
    label "egzerga"
  ]
  node [
    id 10
    label "pieni&#261;dz"
  ]
  node [
    id 11
    label "otok"
  ]
  node [
    id 12
    label "balansjerka"
  ]
  node [
    id 13
    label "Barnet"
  ]
  node [
    id 14
    label "c&#243;rka"
  ]
  node [
    id 15
    label "football"
  ]
  node [
    id 16
    label "Club"
  ]
  node [
    id 17
    label "League"
  ]
  node [
    id 18
    label "Two"
  ]
  node [
    id 19
    label "Arsenalu"
  ]
  node [
    id 20
    label "Londyn"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
]
