graph [
  node [
    id 0
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszystek"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;liwy"
    origin "text"
  ]
  node [
    id 3
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 4
    label "zareagowa&#263;"
  ]
  node [
    id 5
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 6
    label "draw"
  ]
  node [
    id 7
    label "zrobi&#263;"
  ]
  node [
    id 8
    label "allude"
  ]
  node [
    id 9
    label "zmieni&#263;"
  ]
  node [
    id 10
    label "zacz&#261;&#263;"
  ]
  node [
    id 11
    label "raise"
  ]
  node [
    id 12
    label "odpowiedzie&#263;"
  ]
  node [
    id 13
    label "react"
  ]
  node [
    id 14
    label "sta&#263;_si&#281;"
  ]
  node [
    id 15
    label "post&#261;pi&#263;"
  ]
  node [
    id 16
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 17
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 18
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 19
    label "zorganizowa&#263;"
  ]
  node [
    id 20
    label "appoint"
  ]
  node [
    id 21
    label "wystylizowa&#263;"
  ]
  node [
    id 22
    label "cause"
  ]
  node [
    id 23
    label "przerobi&#263;"
  ]
  node [
    id 24
    label "nabra&#263;"
  ]
  node [
    id 25
    label "make"
  ]
  node [
    id 26
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 27
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 28
    label "wydali&#263;"
  ]
  node [
    id 29
    label "sprawi&#263;"
  ]
  node [
    id 30
    label "change"
  ]
  node [
    id 31
    label "zast&#261;pi&#263;"
  ]
  node [
    id 32
    label "come_up"
  ]
  node [
    id 33
    label "przej&#347;&#263;"
  ]
  node [
    id 34
    label "straci&#263;"
  ]
  node [
    id 35
    label "zyska&#263;"
  ]
  node [
    id 36
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 37
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 38
    label "odj&#261;&#263;"
  ]
  node [
    id 39
    label "introduce"
  ]
  node [
    id 40
    label "begin"
  ]
  node [
    id 41
    label "do"
  ]
  node [
    id 42
    label "ca&#322;y"
  ]
  node [
    id 43
    label "jedyny"
  ]
  node [
    id 44
    label "du&#380;y"
  ]
  node [
    id 45
    label "zdr&#243;w"
  ]
  node [
    id 46
    label "calu&#347;ko"
  ]
  node [
    id 47
    label "kompletny"
  ]
  node [
    id 48
    label "&#380;ywy"
  ]
  node [
    id 49
    label "pe&#322;ny"
  ]
  node [
    id 50
    label "podobny"
  ]
  node [
    id 51
    label "ca&#322;o"
  ]
  node [
    id 52
    label "urealnianie"
  ]
  node [
    id 53
    label "mo&#380;ebny"
  ]
  node [
    id 54
    label "umo&#380;liwianie"
  ]
  node [
    id 55
    label "zno&#347;ny"
  ]
  node [
    id 56
    label "umo&#380;liwienie"
  ]
  node [
    id 57
    label "mo&#380;liwie"
  ]
  node [
    id 58
    label "urealnienie"
  ]
  node [
    id 59
    label "dost&#281;pny"
  ]
  node [
    id 60
    label "zno&#347;nie"
  ]
  node [
    id 61
    label "niez&#322;y"
  ]
  node [
    id 62
    label "niedokuczliwy"
  ]
  node [
    id 63
    label "wzgl&#281;dny"
  ]
  node [
    id 64
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 65
    label "odblokowanie_si&#281;"
  ]
  node [
    id 66
    label "zrozumia&#322;y"
  ]
  node [
    id 67
    label "dost&#281;pnie"
  ]
  node [
    id 68
    label "&#322;atwy"
  ]
  node [
    id 69
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 70
    label "przyst&#281;pnie"
  ]
  node [
    id 71
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 72
    label "powodowanie"
  ]
  node [
    id 73
    label "robienie"
  ]
  node [
    id 74
    label "upowa&#380;nianie"
  ]
  node [
    id 75
    label "czynno&#347;&#263;"
  ]
  node [
    id 76
    label "spowodowanie"
  ]
  node [
    id 77
    label "upowa&#380;nienie"
  ]
  node [
    id 78
    label "zrobienie"
  ]
  node [
    id 79
    label "akceptowalny"
  ]
  node [
    id 80
    label "punkt"
  ]
  node [
    id 81
    label "spos&#243;b"
  ]
  node [
    id 82
    label "miejsce"
  ]
  node [
    id 83
    label "abstrakcja"
  ]
  node [
    id 84
    label "czas"
  ]
  node [
    id 85
    label "chemikalia"
  ]
  node [
    id 86
    label "substancja"
  ]
  node [
    id 87
    label "poprzedzanie"
  ]
  node [
    id 88
    label "czasoprzestrze&#324;"
  ]
  node [
    id 89
    label "laba"
  ]
  node [
    id 90
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 91
    label "chronometria"
  ]
  node [
    id 92
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 93
    label "rachuba_czasu"
  ]
  node [
    id 94
    label "przep&#322;ywanie"
  ]
  node [
    id 95
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 96
    label "czasokres"
  ]
  node [
    id 97
    label "odczyt"
  ]
  node [
    id 98
    label "chwila"
  ]
  node [
    id 99
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 100
    label "dzieje"
  ]
  node [
    id 101
    label "kategoria_gramatyczna"
  ]
  node [
    id 102
    label "poprzedzenie"
  ]
  node [
    id 103
    label "trawienie"
  ]
  node [
    id 104
    label "pochodzi&#263;"
  ]
  node [
    id 105
    label "period"
  ]
  node [
    id 106
    label "okres_czasu"
  ]
  node [
    id 107
    label "poprzedza&#263;"
  ]
  node [
    id 108
    label "schy&#322;ek"
  ]
  node [
    id 109
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 110
    label "odwlekanie_si&#281;"
  ]
  node [
    id 111
    label "zegar"
  ]
  node [
    id 112
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 113
    label "czwarty_wymiar"
  ]
  node [
    id 114
    label "pochodzenie"
  ]
  node [
    id 115
    label "koniugacja"
  ]
  node [
    id 116
    label "Zeitgeist"
  ]
  node [
    id 117
    label "trawi&#263;"
  ]
  node [
    id 118
    label "pogoda"
  ]
  node [
    id 119
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 120
    label "poprzedzi&#263;"
  ]
  node [
    id 121
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 122
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 123
    label "time_period"
  ]
  node [
    id 124
    label "model"
  ]
  node [
    id 125
    label "narz&#281;dzie"
  ]
  node [
    id 126
    label "zbi&#243;r"
  ]
  node [
    id 127
    label "tryb"
  ]
  node [
    id 128
    label "nature"
  ]
  node [
    id 129
    label "po&#322;o&#380;enie"
  ]
  node [
    id 130
    label "sprawa"
  ]
  node [
    id 131
    label "ust&#281;p"
  ]
  node [
    id 132
    label "plan"
  ]
  node [
    id 133
    label "obiekt_matematyczny"
  ]
  node [
    id 134
    label "problemat"
  ]
  node [
    id 135
    label "plamka"
  ]
  node [
    id 136
    label "stopie&#324;_pisma"
  ]
  node [
    id 137
    label "jednostka"
  ]
  node [
    id 138
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 139
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 140
    label "mark"
  ]
  node [
    id 141
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 142
    label "prosta"
  ]
  node [
    id 143
    label "problematyka"
  ]
  node [
    id 144
    label "obiekt"
  ]
  node [
    id 145
    label "zapunktowa&#263;"
  ]
  node [
    id 146
    label "podpunkt"
  ]
  node [
    id 147
    label "wojsko"
  ]
  node [
    id 148
    label "kres"
  ]
  node [
    id 149
    label "przestrze&#324;"
  ]
  node [
    id 150
    label "point"
  ]
  node [
    id 151
    label "pozycja"
  ]
  node [
    id 152
    label "warunek_lokalowy"
  ]
  node [
    id 153
    label "plac"
  ]
  node [
    id 154
    label "location"
  ]
  node [
    id 155
    label "uwaga"
  ]
  node [
    id 156
    label "status"
  ]
  node [
    id 157
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 158
    label "cia&#322;o"
  ]
  node [
    id 159
    label "cecha"
  ]
  node [
    id 160
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 161
    label "praca"
  ]
  node [
    id 162
    label "rz&#261;d"
  ]
  node [
    id 163
    label "przenikanie"
  ]
  node [
    id 164
    label "byt"
  ]
  node [
    id 165
    label "materia"
  ]
  node [
    id 166
    label "cz&#261;steczka"
  ]
  node [
    id 167
    label "temperatura_krytyczna"
  ]
  node [
    id 168
    label "przenika&#263;"
  ]
  node [
    id 169
    label "smolisty"
  ]
  node [
    id 170
    label "proces_my&#347;lowy"
  ]
  node [
    id 171
    label "abstractedness"
  ]
  node [
    id 172
    label "abstraction"
  ]
  node [
    id 173
    label "poj&#281;cie"
  ]
  node [
    id 174
    label "obraz"
  ]
  node [
    id 175
    label "sytuacja"
  ]
  node [
    id 176
    label "spalenie"
  ]
  node [
    id 177
    label "spalanie"
  ]
  node [
    id 178
    label "Huawei"
  ]
  node [
    id 179
    label "Ltd"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 178
    target 179
  ]
]
