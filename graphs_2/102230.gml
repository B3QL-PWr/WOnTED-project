graph [
  node [
    id 0
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 1
    label "konsultacja"
    origin "text"
  ]
  node [
    id 2
    label "przed"
    origin "text"
  ]
  node [
    id 3
    label "wydanie"
    origin "text"
  ]
  node [
    id 4
    label "decyzja"
    origin "text"
  ]
  node [
    id 5
    label "zgodnie"
    origin "text"
  ]
  node [
    id 6
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 7
    label "wprowadzanie"
  ]
  node [
    id 8
    label "g&#243;rowanie"
  ]
  node [
    id 9
    label "ukierunkowywanie"
  ]
  node [
    id 10
    label "poprowadzenie"
  ]
  node [
    id 11
    label "eksponowanie"
  ]
  node [
    id 12
    label "doprowadzenie"
  ]
  node [
    id 13
    label "przeci&#261;ganie"
  ]
  node [
    id 14
    label "sterowanie"
  ]
  node [
    id 15
    label "dysponowanie"
  ]
  node [
    id 16
    label "kszta&#322;towanie"
  ]
  node [
    id 17
    label "management"
  ]
  node [
    id 18
    label "powodowanie"
  ]
  node [
    id 19
    label "trzymanie"
  ]
  node [
    id 20
    label "dawanie"
  ]
  node [
    id 21
    label "linia_melodyczna"
  ]
  node [
    id 22
    label "prowadzi&#263;"
  ]
  node [
    id 23
    label "drive"
  ]
  node [
    id 24
    label "przywodzenie"
  ]
  node [
    id 25
    label "wprowadzenie"
  ]
  node [
    id 26
    label "aim"
  ]
  node [
    id 27
    label "czynno&#347;&#263;"
  ]
  node [
    id 28
    label "lead"
  ]
  node [
    id 29
    label "oprowadzenie"
  ]
  node [
    id 30
    label "prowadzanie"
  ]
  node [
    id 31
    label "oprowadzanie"
  ]
  node [
    id 32
    label "przewy&#380;szanie"
  ]
  node [
    id 33
    label "kierowanie"
  ]
  node [
    id 34
    label "zwracanie"
  ]
  node [
    id 35
    label "przeci&#281;cie"
  ]
  node [
    id 36
    label "granie"
  ]
  node [
    id 37
    label "ta&#324;czenie"
  ]
  node [
    id 38
    label "kre&#347;lenie"
  ]
  node [
    id 39
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 40
    label "zaprowadzanie"
  ]
  node [
    id 41
    label "pozarz&#261;dzanie"
  ]
  node [
    id 42
    label "robienie"
  ]
  node [
    id 43
    label "krzywa"
  ]
  node [
    id 44
    label "przecinanie"
  ]
  node [
    id 45
    label "doprowadzanie"
  ]
  node [
    id 46
    label "pokre&#347;lenie"
  ]
  node [
    id 47
    label "usuwanie"
  ]
  node [
    id 48
    label "opowiadanie"
  ]
  node [
    id 49
    label "anointing"
  ]
  node [
    id 50
    label "sporz&#261;dzanie"
  ]
  node [
    id 51
    label "skre&#347;lanie"
  ]
  node [
    id 52
    label "uniewa&#380;nianie"
  ]
  node [
    id 53
    label "przygotowywanie"
  ]
  node [
    id 54
    label "skazany"
  ]
  node [
    id 55
    label "zarz&#261;dzanie"
  ]
  node [
    id 56
    label "rozporz&#261;dzanie"
  ]
  node [
    id 57
    label "namaszczenie_chorych"
  ]
  node [
    id 58
    label "dysponowanie_si&#281;"
  ]
  node [
    id 59
    label "disposal"
  ]
  node [
    id 60
    label "rozdysponowywanie"
  ]
  node [
    id 61
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 62
    label "bycie"
  ]
  node [
    id 63
    label "przedmiot"
  ]
  node [
    id 64
    label "act"
  ]
  node [
    id 65
    label "fabrication"
  ]
  node [
    id 66
    label "tentegowanie"
  ]
  node [
    id 67
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 68
    label "porobienie"
  ]
  node [
    id 69
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 70
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 71
    label "creation"
  ]
  node [
    id 72
    label "bezproblemowy"
  ]
  node [
    id 73
    label "wydarzenie"
  ]
  node [
    id 74
    label "activity"
  ]
  node [
    id 75
    label "podkre&#347;lanie"
  ]
  node [
    id 76
    label "radiation"
  ]
  node [
    id 77
    label "demonstrowanie"
  ]
  node [
    id 78
    label "napromieniowywanie"
  ]
  node [
    id 79
    label "uwydatnianie_si&#281;"
  ]
  node [
    id 80
    label "przeorientowanie"
  ]
  node [
    id 81
    label "oznaczanie"
  ]
  node [
    id 82
    label "orientation"
  ]
  node [
    id 83
    label "strike"
  ]
  node [
    id 84
    label "dominance"
  ]
  node [
    id 85
    label "wygrywanie"
  ]
  node [
    id 86
    label "lepszy"
  ]
  node [
    id 87
    label "przesterowanie"
  ]
  node [
    id 88
    label "steering"
  ]
  node [
    id 89
    label "obs&#322;ugiwanie"
  ]
  node [
    id 90
    label "control"
  ]
  node [
    id 91
    label "rendition"
  ]
  node [
    id 92
    label "powracanie"
  ]
  node [
    id 93
    label "wydalanie"
  ]
  node [
    id 94
    label "haftowanie"
  ]
  node [
    id 95
    label "przekazywanie"
  ]
  node [
    id 96
    label "vomit"
  ]
  node [
    id 97
    label "przeznaczanie"
  ]
  node [
    id 98
    label "ustawianie"
  ]
  node [
    id 99
    label "znajdowanie_si&#281;"
  ]
  node [
    id 100
    label "provision"
  ]
  node [
    id 101
    label "supply"
  ]
  node [
    id 102
    label "spe&#322;nianie"
  ]
  node [
    id 103
    label "wzbudzanie"
  ]
  node [
    id 104
    label "montowanie"
  ]
  node [
    id 105
    label "rozwijanie"
  ]
  node [
    id 106
    label "training"
  ]
  node [
    id 107
    label "formation"
  ]
  node [
    id 108
    label "cause"
  ]
  node [
    id 109
    label "causal_agent"
  ]
  node [
    id 110
    label "nakierowanie_si&#281;"
  ]
  node [
    id 111
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 112
    label "wysy&#322;anie"
  ]
  node [
    id 113
    label "wyre&#380;yserowanie"
  ]
  node [
    id 114
    label "oddzia&#322;ywanie"
  ]
  node [
    id 115
    label "re&#380;yserowanie"
  ]
  node [
    id 116
    label "linia"
  ]
  node [
    id 117
    label "curve"
  ]
  node [
    id 118
    label "curvature"
  ]
  node [
    id 119
    label "figura_geometryczna"
  ]
  node [
    id 120
    label "poprowadzi&#263;"
  ]
  node [
    id 121
    label "sterczenie"
  ]
  node [
    id 122
    label "eksponowa&#263;"
  ]
  node [
    id 123
    label "g&#243;rowa&#263;"
  ]
  node [
    id 124
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 125
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 126
    label "sterowa&#263;"
  ]
  node [
    id 127
    label "kierowa&#263;"
  ]
  node [
    id 128
    label "string"
  ]
  node [
    id 129
    label "kre&#347;li&#263;"
  ]
  node [
    id 130
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 131
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 132
    label "&#380;y&#263;"
  ]
  node [
    id 133
    label "partner"
  ]
  node [
    id 134
    label "ukierunkowywa&#263;"
  ]
  node [
    id 135
    label "przesuwa&#263;"
  ]
  node [
    id 136
    label "tworzy&#263;"
  ]
  node [
    id 137
    label "powodowa&#263;"
  ]
  node [
    id 138
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 139
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 140
    label "message"
  ]
  node [
    id 141
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 142
    label "robi&#263;"
  ]
  node [
    id 143
    label "navigate"
  ]
  node [
    id 144
    label "manipulate"
  ]
  node [
    id 145
    label "akapit"
  ]
  node [
    id 146
    label "zesp&#243;&#322;"
  ]
  node [
    id 147
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 148
    label "udost&#281;pnianie"
  ]
  node [
    id 149
    label "pra&#380;enie"
  ]
  node [
    id 150
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 151
    label "urz&#261;dzanie"
  ]
  node [
    id 152
    label "p&#322;acenie"
  ]
  node [
    id 153
    label "puszczanie_si&#281;"
  ]
  node [
    id 154
    label "dodawanie"
  ]
  node [
    id 155
    label "wyst&#281;powanie"
  ]
  node [
    id 156
    label "bycie_w_posiadaniu"
  ]
  node [
    id 157
    label "communication"
  ]
  node [
    id 158
    label "&#322;adowanie"
  ]
  node [
    id 159
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 160
    label "nalewanie"
  ]
  node [
    id 161
    label "powierzanie"
  ]
  node [
    id 162
    label "zezwalanie"
  ]
  node [
    id 163
    label "giving"
  ]
  node [
    id 164
    label "obiecywanie"
  ]
  node [
    id 165
    label "odst&#281;powanie"
  ]
  node [
    id 166
    label "dostarczanie"
  ]
  node [
    id 167
    label "wymienianie_si&#281;"
  ]
  node [
    id 168
    label "emission"
  ]
  node [
    id 169
    label "administration"
  ]
  node [
    id 170
    label "umo&#380;liwianie"
  ]
  node [
    id 171
    label "pasowanie"
  ]
  node [
    id 172
    label "otwarcie"
  ]
  node [
    id 173
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 174
    label "playing"
  ]
  node [
    id 175
    label "dzianie_si&#281;"
  ]
  node [
    id 176
    label "rozgrywanie"
  ]
  node [
    id 177
    label "pretense"
  ]
  node [
    id 178
    label "dogranie"
  ]
  node [
    id 179
    label "uderzenie"
  ]
  node [
    id 180
    label "zwalczenie"
  ]
  node [
    id 181
    label "lewa"
  ]
  node [
    id 182
    label "instrumentalizacja"
  ]
  node [
    id 183
    label "migotanie"
  ]
  node [
    id 184
    label "wydawanie"
  ]
  node [
    id 185
    label "ust&#281;powanie"
  ]
  node [
    id 186
    label "odegranie_si&#281;"
  ]
  node [
    id 187
    label "grywanie"
  ]
  node [
    id 188
    label "dogrywanie"
  ]
  node [
    id 189
    label "wybijanie"
  ]
  node [
    id 190
    label "wykonywanie"
  ]
  node [
    id 191
    label "pogranie"
  ]
  node [
    id 192
    label "brzmienie"
  ]
  node [
    id 193
    label "na&#347;ladowanie"
  ]
  node [
    id 194
    label "przygrywanie"
  ]
  node [
    id 195
    label "nagranie_si&#281;"
  ]
  node [
    id 196
    label "wyr&#243;wnywanie"
  ]
  node [
    id 197
    label "rozegranie_si&#281;"
  ]
  node [
    id 198
    label "odgrywanie_si&#281;"
  ]
  node [
    id 199
    label "&#347;ciganie"
  ]
  node [
    id 200
    label "wyr&#243;wnanie"
  ]
  node [
    id 201
    label "pr&#243;bowanie"
  ]
  node [
    id 202
    label "szczekanie"
  ]
  node [
    id 203
    label "gra_w_karty"
  ]
  node [
    id 204
    label "przedstawianie"
  ]
  node [
    id 205
    label "instrument_muzyczny"
  ]
  node [
    id 206
    label "glitter"
  ]
  node [
    id 207
    label "igranie"
  ]
  node [
    id 208
    label "wybicie"
  ]
  node [
    id 209
    label "mienienie_si&#281;"
  ]
  node [
    id 210
    label "prezentowanie"
  ]
  node [
    id 211
    label "rola"
  ]
  node [
    id 212
    label "staranie_si&#281;"
  ]
  node [
    id 213
    label "wytyczenie"
  ]
  node [
    id 214
    label "nakre&#347;lenie"
  ]
  node [
    id 215
    label "proverb"
  ]
  node [
    id 216
    label "guidance"
  ]
  node [
    id 217
    label "nata&#324;czenie_si&#281;"
  ]
  node [
    id 218
    label "dancing"
  ]
  node [
    id 219
    label "poruszanie_si&#281;"
  ]
  node [
    id 220
    label "chwianie_si&#281;"
  ]
  node [
    id 221
    label "rozta&#324;czenie_si&#281;"
  ]
  node [
    id 222
    label "przeta&#324;czenie"
  ]
  node [
    id 223
    label "gibanie"
  ]
  node [
    id 224
    label "pota&#324;czenie"
  ]
  node [
    id 225
    label "pokazywanie"
  ]
  node [
    id 226
    label "zaczynanie"
  ]
  node [
    id 227
    label "w&#322;&#261;czanie"
  ]
  node [
    id 228
    label "initiation"
  ]
  node [
    id 229
    label "umieszczanie"
  ]
  node [
    id 230
    label "zak&#322;&#243;canie"
  ]
  node [
    id 231
    label "retraction"
  ]
  node [
    id 232
    label "zapoznawanie"
  ]
  node [
    id 233
    label "wpisywanie"
  ]
  node [
    id 234
    label "wchodzenie"
  ]
  node [
    id 235
    label "przewietrzanie"
  ]
  node [
    id 236
    label "trigger"
  ]
  node [
    id 237
    label "mental_hospital"
  ]
  node [
    id 238
    label "rynek"
  ]
  node [
    id 239
    label "sp&#281;dzenie"
  ]
  node [
    id 240
    label "spowodowanie"
  ]
  node [
    id 241
    label "wzbudzenie"
  ]
  node [
    id 242
    label "zainstalowanie"
  ]
  node [
    id 243
    label "spe&#322;nienie"
  ]
  node [
    id 244
    label "znalezienie_si&#281;"
  ]
  node [
    id 245
    label "introduction"
  ]
  node [
    id 246
    label "pos&#322;anie"
  ]
  node [
    id 247
    label "adduction"
  ]
  node [
    id 248
    label "sk&#322;anianie"
  ]
  node [
    id 249
    label "przypominanie"
  ]
  node [
    id 250
    label "kojarzenie_si&#281;"
  ]
  node [
    id 251
    label "pokazanie"
  ]
  node [
    id 252
    label "umo&#380;liwienie"
  ]
  node [
    id 253
    label "wej&#347;cie"
  ]
  node [
    id 254
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 255
    label "evocation"
  ]
  node [
    id 256
    label "nuklearyzacja"
  ]
  node [
    id 257
    label "w&#322;&#261;czenie"
  ]
  node [
    id 258
    label "zacz&#281;cie"
  ]
  node [
    id 259
    label "zapoznanie"
  ]
  node [
    id 260
    label "podstawy"
  ]
  node [
    id 261
    label "wst&#281;p"
  ]
  node [
    id 262
    label "entrance"
  ]
  node [
    id 263
    label "wpisanie"
  ]
  node [
    id 264
    label "deduction"
  ]
  node [
    id 265
    label "umieszczenie"
  ]
  node [
    id 266
    label "issue"
  ]
  node [
    id 267
    label "zrobienie"
  ]
  node [
    id 268
    label "przewietrzenie"
  ]
  node [
    id 269
    label "przed&#322;u&#380;anie"
  ]
  node [
    id 270
    label "przemieszczanie"
  ]
  node [
    id 271
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 272
    label "rozci&#261;ganie"
  ]
  node [
    id 273
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 274
    label "wymawianie"
  ]
  node [
    id 275
    label "j&#261;kanie"
  ]
  node [
    id 276
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 277
    label "przymocowywanie"
  ]
  node [
    id 278
    label "pull"
  ]
  node [
    id 279
    label "przesuwanie"
  ]
  node [
    id 280
    label "zaci&#261;ganie"
  ]
  node [
    id 281
    label "przetykanie"
  ]
  node [
    id 282
    label "podzielenie"
  ]
  node [
    id 283
    label "poprzecinanie"
  ]
  node [
    id 284
    label "przerwanie"
  ]
  node [
    id 285
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 286
    label "w&#281;ze&#322;"
  ]
  node [
    id 287
    label "zranienie"
  ]
  node [
    id 288
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 289
    label "cut"
  ]
  node [
    id 290
    label "miejsce"
  ]
  node [
    id 291
    label "snub"
  ]
  node [
    id 292
    label "carving"
  ]
  node [
    id 293
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 294
    label "time"
  ]
  node [
    id 295
    label "kaleczenie"
  ]
  node [
    id 296
    label "przerywanie"
  ]
  node [
    id 297
    label "dzielenie"
  ]
  node [
    id 298
    label "intersection"
  ]
  node [
    id 299
    label "film_editing"
  ]
  node [
    id 300
    label "utrzymywanie"
  ]
  node [
    id 301
    label "wypuszczanie"
  ]
  node [
    id 302
    label "noszenie"
  ]
  node [
    id 303
    label "przetrzymanie"
  ]
  node [
    id 304
    label "przetrzymywanie"
  ]
  node [
    id 305
    label "hodowanie"
  ]
  node [
    id 306
    label "wypuszczenie"
  ]
  node [
    id 307
    label "niesienie"
  ]
  node [
    id 308
    label "utrzymanie"
  ]
  node [
    id 309
    label "zmuszanie"
  ]
  node [
    id 310
    label "podtrzymywanie"
  ]
  node [
    id 311
    label "sprawowanie"
  ]
  node [
    id 312
    label "poise"
  ]
  node [
    id 313
    label "uniemo&#380;liwianie"
  ]
  node [
    id 314
    label "dzier&#380;enie"
  ]
  node [
    id 315
    label "clasp"
  ]
  node [
    id 316
    label "zachowywanie"
  ]
  node [
    id 317
    label "detention"
  ]
  node [
    id 318
    label "potrzymanie"
  ]
  node [
    id 319
    label "retention"
  ]
  node [
    id 320
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 321
    label "ocena"
  ]
  node [
    id 322
    label "porada"
  ]
  node [
    id 323
    label "narada"
  ]
  node [
    id 324
    label "kryterium"
  ]
  node [
    id 325
    label "sofcik"
  ]
  node [
    id 326
    label "informacja"
  ]
  node [
    id 327
    label "pogl&#261;d"
  ]
  node [
    id 328
    label "appraisal"
  ]
  node [
    id 329
    label "wskaz&#243;wka"
  ]
  node [
    id 330
    label "konsylium"
  ]
  node [
    id 331
    label "zgromadzenie"
  ]
  node [
    id 332
    label "dyskusja"
  ]
  node [
    id 333
    label "conference"
  ]
  node [
    id 334
    label "podanie"
  ]
  node [
    id 335
    label "d&#378;wi&#281;k"
  ]
  node [
    id 336
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 337
    label "reszta"
  ]
  node [
    id 338
    label "egzemplarz"
  ]
  node [
    id 339
    label "zapach"
  ]
  node [
    id 340
    label "danie"
  ]
  node [
    id 341
    label "urz&#261;dzenie"
  ]
  node [
    id 342
    label "odmiana"
  ]
  node [
    id 343
    label "wytworzenie"
  ]
  node [
    id 344
    label "publikacja"
  ]
  node [
    id 345
    label "delivery"
  ]
  node [
    id 346
    label "impression"
  ]
  node [
    id 347
    label "zadenuncjowanie"
  ]
  node [
    id 348
    label "czasopismo"
  ]
  node [
    id 349
    label "ujawnienie"
  ]
  node [
    id 350
    label "zdarzenie_si&#281;"
  ]
  node [
    id 351
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 352
    label "gramatyka"
  ]
  node [
    id 353
    label "podgatunek"
  ]
  node [
    id 354
    label "rewizja"
  ]
  node [
    id 355
    label "zjawisko"
  ]
  node [
    id 356
    label "typ"
  ]
  node [
    id 357
    label "change"
  ]
  node [
    id 358
    label "mutant"
  ]
  node [
    id 359
    label "jednostka_systematyczna"
  ]
  node [
    id 360
    label "paradygmat"
  ]
  node [
    id 361
    label "ferment"
  ]
  node [
    id 362
    label "rasa"
  ]
  node [
    id 363
    label "posta&#263;"
  ]
  node [
    id 364
    label "komora"
  ]
  node [
    id 365
    label "wyrz&#261;dzenie"
  ]
  node [
    id 366
    label "kom&#243;rka"
  ]
  node [
    id 367
    label "impulsator"
  ]
  node [
    id 368
    label "przygotowanie"
  ]
  node [
    id 369
    label "furnishing"
  ]
  node [
    id 370
    label "zabezpieczenie"
  ]
  node [
    id 371
    label "sprz&#281;t"
  ]
  node [
    id 372
    label "aparatura"
  ]
  node [
    id 373
    label "ig&#322;a"
  ]
  node [
    id 374
    label "wirnik"
  ]
  node [
    id 375
    label "zablokowanie"
  ]
  node [
    id 376
    label "blokowanie"
  ]
  node [
    id 377
    label "j&#281;zyk"
  ]
  node [
    id 378
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 379
    label "system_energetyczny"
  ]
  node [
    id 380
    label "narz&#281;dzie"
  ]
  node [
    id 381
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 382
    label "set"
  ]
  node [
    id 383
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 384
    label "zagospodarowanie"
  ]
  node [
    id 385
    label "mechanizm"
  ]
  node [
    id 386
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 387
    label "narobienie"
  ]
  node [
    id 388
    label "coevals"
  ]
  node [
    id 389
    label "druk"
  ]
  node [
    id 390
    label "produkcja"
  ]
  node [
    id 391
    label "tekst"
  ]
  node [
    id 392
    label "notification"
  ]
  node [
    id 393
    label "denunciation"
  ]
  node [
    id 394
    label "doniesienie"
  ]
  node [
    id 395
    label "objawienie"
  ]
  node [
    id 396
    label "detection"
  ]
  node [
    id 397
    label "poinformowanie"
  ]
  node [
    id 398
    label "disclosure"
  ]
  node [
    id 399
    label "dostrze&#380;enie"
  ]
  node [
    id 400
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 401
    label "jawny"
  ]
  node [
    id 402
    label "siatk&#243;wka"
  ]
  node [
    id 403
    label "opowie&#347;&#263;"
  ]
  node [
    id 404
    label "jedzenie"
  ]
  node [
    id 405
    label "tenis"
  ]
  node [
    id 406
    label "zagranie"
  ]
  node [
    id 407
    label "narrative"
  ]
  node [
    id 408
    label "pass"
  ]
  node [
    id 409
    label "pismo"
  ]
  node [
    id 410
    label "pi&#322;ka"
  ]
  node [
    id 411
    label "give"
  ]
  node [
    id 412
    label "prayer"
  ]
  node [
    id 413
    label "ustawienie"
  ]
  node [
    id 414
    label "service"
  ]
  node [
    id 415
    label "nafaszerowanie"
  ]
  node [
    id 416
    label "myth"
  ]
  node [
    id 417
    label "zaserwowanie"
  ]
  node [
    id 418
    label "wyposa&#380;enie"
  ]
  node [
    id 419
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 420
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 421
    label "posi&#322;ek"
  ]
  node [
    id 422
    label "wyst&#261;pienie"
  ]
  node [
    id 423
    label "zadanie"
  ]
  node [
    id 424
    label "pobicie"
  ]
  node [
    id 425
    label "dodanie"
  ]
  node [
    id 426
    label "potrawa"
  ]
  node [
    id 427
    label "przeznaczenie"
  ]
  node [
    id 428
    label "uderzanie"
  ]
  node [
    id 429
    label "obiecanie"
  ]
  node [
    id 430
    label "uprawianie_seksu"
  ]
  node [
    id 431
    label "wyposa&#380;anie"
  ]
  node [
    id 432
    label "allow"
  ]
  node [
    id 433
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 434
    label "powierzenie"
  ]
  node [
    id 435
    label "dostanie"
  ]
  node [
    id 436
    label "udost&#281;pnienie"
  ]
  node [
    id 437
    label "cios"
  ]
  node [
    id 438
    label "zap&#322;acenie"
  ]
  node [
    id 439
    label "eating"
  ]
  node [
    id 440
    label "menu"
  ]
  node [
    id 441
    label "przekazanie"
  ]
  node [
    id 442
    label "hand"
  ]
  node [
    id 443
    label "wymienienie_si&#281;"
  ]
  node [
    id 444
    label "odst&#261;pienie"
  ]
  node [
    id 445
    label "coup"
  ]
  node [
    id 446
    label "dostarczenie"
  ]
  node [
    id 447
    label "karta"
  ]
  node [
    id 448
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 449
    label "nicpo&#324;"
  ]
  node [
    id 450
    label "wyewoluowanie"
  ]
  node [
    id 451
    label "wyewoluowa&#263;"
  ]
  node [
    id 452
    label "part"
  ]
  node [
    id 453
    label "przyswojenie"
  ]
  node [
    id 454
    label "przyswoi&#263;"
  ]
  node [
    id 455
    label "starzenie_si&#281;"
  ]
  node [
    id 456
    label "przyswaja&#263;"
  ]
  node [
    id 457
    label "okaz"
  ]
  node [
    id 458
    label "obiekt"
  ]
  node [
    id 459
    label "sztuka"
  ]
  node [
    id 460
    label "reakcja"
  ]
  node [
    id 461
    label "wytw&#243;r"
  ]
  node [
    id 462
    label "individual"
  ]
  node [
    id 463
    label "ewoluowanie"
  ]
  node [
    id 464
    label "ewoluowa&#263;"
  ]
  node [
    id 465
    label "czynnik_biotyczny"
  ]
  node [
    id 466
    label "agent"
  ]
  node [
    id 467
    label "przyswajanie"
  ]
  node [
    id 468
    label "solmizacja"
  ]
  node [
    id 469
    label "transmiter"
  ]
  node [
    id 470
    label "repetycja"
  ]
  node [
    id 471
    label "wpa&#347;&#263;"
  ]
  node [
    id 472
    label "akcent"
  ]
  node [
    id 473
    label "nadlecenie"
  ]
  node [
    id 474
    label "note"
  ]
  node [
    id 475
    label "heksachord"
  ]
  node [
    id 476
    label "wpadanie"
  ]
  node [
    id 477
    label "phone"
  ]
  node [
    id 478
    label "wydawa&#263;"
  ]
  node [
    id 479
    label "seria"
  ]
  node [
    id 480
    label "onomatopeja"
  ]
  node [
    id 481
    label "wpada&#263;"
  ]
  node [
    id 482
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 483
    label "dobiec"
  ]
  node [
    id 484
    label "intonacja"
  ]
  node [
    id 485
    label "wpadni&#281;cie"
  ]
  node [
    id 486
    label "modalizm"
  ]
  node [
    id 487
    label "wyda&#263;"
  ]
  node [
    id 488
    label "sound"
  ]
  node [
    id 489
    label "kosmetyk"
  ]
  node [
    id 490
    label "smak"
  ]
  node [
    id 491
    label "przyprawa"
  ]
  node [
    id 492
    label "upojno&#347;&#263;"
  ]
  node [
    id 493
    label "ciasto"
  ]
  node [
    id 494
    label "owiewanie"
  ]
  node [
    id 495
    label "aromat"
  ]
  node [
    id 496
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 497
    label "puff"
  ]
  node [
    id 498
    label "liczba_kwantowa"
  ]
  node [
    id 499
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 500
    label "pozosta&#322;y"
  ]
  node [
    id 501
    label "kwota"
  ]
  node [
    id 502
    label "remainder"
  ]
  node [
    id 503
    label "ok&#322;adka"
  ]
  node [
    id 504
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 505
    label "prasa"
  ]
  node [
    id 506
    label "dzia&#322;"
  ]
  node [
    id 507
    label "zajawka"
  ]
  node [
    id 508
    label "psychotest"
  ]
  node [
    id 509
    label "wk&#322;ad"
  ]
  node [
    id 510
    label "Zwrotnica"
  ]
  node [
    id 511
    label "zdecydowanie"
  ]
  node [
    id 512
    label "resolution"
  ]
  node [
    id 513
    label "dokument"
  ]
  node [
    id 514
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 515
    label "sygnatariusz"
  ]
  node [
    id 516
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 517
    label "dokumentacja"
  ]
  node [
    id 518
    label "writing"
  ]
  node [
    id 519
    label "&#347;wiadectwo"
  ]
  node [
    id 520
    label "zapis"
  ]
  node [
    id 521
    label "utw&#243;r"
  ]
  node [
    id 522
    label "record"
  ]
  node [
    id 523
    label "raport&#243;wka"
  ]
  node [
    id 524
    label "registratura"
  ]
  node [
    id 525
    label "fascyku&#322;"
  ]
  node [
    id 526
    label "parafa"
  ]
  node [
    id 527
    label "plik"
  ]
  node [
    id 528
    label "rezultat"
  ]
  node [
    id 529
    label "p&#322;&#243;d"
  ]
  node [
    id 530
    label "work"
  ]
  node [
    id 531
    label "zauwa&#380;alnie"
  ]
  node [
    id 532
    label "judgment"
  ]
  node [
    id 533
    label "cecha"
  ]
  node [
    id 534
    label "podj&#281;cie"
  ]
  node [
    id 535
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 536
    label "oddzia&#322;anie"
  ]
  node [
    id 537
    label "resoluteness"
  ]
  node [
    id 538
    label "pewnie"
  ]
  node [
    id 539
    label "zdecydowany"
  ]
  node [
    id 540
    label "zgodny"
  ]
  node [
    id 541
    label "spokojnie"
  ]
  node [
    id 542
    label "zbie&#380;nie"
  ]
  node [
    id 543
    label "jednakowo"
  ]
  node [
    id 544
    label "dobrze"
  ]
  node [
    id 545
    label "zbie&#380;ny"
  ]
  node [
    id 546
    label "podobnie"
  ]
  node [
    id 547
    label "identically"
  ]
  node [
    id 548
    label "jednakowy"
  ]
  node [
    id 549
    label "pozytywnie"
  ]
  node [
    id 550
    label "korzystnie"
  ]
  node [
    id 551
    label "wiele"
  ]
  node [
    id 552
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 553
    label "pomy&#347;lnie"
  ]
  node [
    id 554
    label "lepiej"
  ]
  node [
    id 555
    label "moralnie"
  ]
  node [
    id 556
    label "odpowiednio"
  ]
  node [
    id 557
    label "dobry"
  ]
  node [
    id 558
    label "skutecznie"
  ]
  node [
    id 559
    label "dobroczynnie"
  ]
  node [
    id 560
    label "spokojny"
  ]
  node [
    id 561
    label "cichy"
  ]
  node [
    id 562
    label "przyjemnie"
  ]
  node [
    id 563
    label "bezproblemowo"
  ]
  node [
    id 564
    label "wolno"
  ]
  node [
    id 565
    label "prawda"
  ]
  node [
    id 566
    label "wyr&#243;b"
  ]
  node [
    id 567
    label "blok"
  ]
  node [
    id 568
    label "nag&#322;&#243;wek"
  ]
  node [
    id 569
    label "szkic"
  ]
  node [
    id 570
    label "line"
  ]
  node [
    id 571
    label "rodzajnik"
  ]
  node [
    id 572
    label "fragment"
  ]
  node [
    id 573
    label "towar"
  ]
  node [
    id 574
    label "paragraf"
  ]
  node [
    id 575
    label "znak_j&#281;zykowy"
  ]
  node [
    id 576
    label "pisa&#263;"
  ]
  node [
    id 577
    label "j&#281;zykowo"
  ]
  node [
    id 578
    label "redakcja"
  ]
  node [
    id 579
    label "preparacja"
  ]
  node [
    id 580
    label "dzie&#322;o"
  ]
  node [
    id 581
    label "wypowied&#378;"
  ]
  node [
    id 582
    label "obelga"
  ]
  node [
    id 583
    label "odmianka"
  ]
  node [
    id 584
    label "opu&#347;ci&#263;"
  ]
  node [
    id 585
    label "pomini&#281;cie"
  ]
  node [
    id 586
    label "koniektura"
  ]
  node [
    id 587
    label "ekscerpcja"
  ]
  node [
    id 588
    label "za&#322;o&#380;enie"
  ]
  node [
    id 589
    label "prawdziwy"
  ]
  node [
    id 590
    label "truth"
  ]
  node [
    id 591
    label "nieprawdziwy"
  ]
  node [
    id 592
    label "s&#261;d"
  ]
  node [
    id 593
    label "realia"
  ]
  node [
    id 594
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 595
    label "produkt"
  ]
  node [
    id 596
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 597
    label "p&#322;uczkarnia"
  ]
  node [
    id 598
    label "znakowarka"
  ]
  node [
    id 599
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 600
    label "head"
  ]
  node [
    id 601
    label "tytu&#322;"
  ]
  node [
    id 602
    label "znak_pisarski"
  ]
  node [
    id 603
    label "przepis"
  ]
  node [
    id 604
    label "skorupa_ziemska"
  ]
  node [
    id 605
    label "budynek"
  ]
  node [
    id 606
    label "przeszkoda"
  ]
  node [
    id 607
    label "bry&#322;a"
  ]
  node [
    id 608
    label "program"
  ]
  node [
    id 609
    label "square"
  ]
  node [
    id 610
    label "bloking"
  ]
  node [
    id 611
    label "kontynent"
  ]
  node [
    id 612
    label "zbi&#243;r"
  ]
  node [
    id 613
    label "kr&#261;g"
  ]
  node [
    id 614
    label "start"
  ]
  node [
    id 615
    label "blockage"
  ]
  node [
    id 616
    label "blokowisko"
  ]
  node [
    id 617
    label "blokada"
  ]
  node [
    id 618
    label "whole"
  ]
  node [
    id 619
    label "stok_kontynentalny"
  ]
  node [
    id 620
    label "bajt"
  ]
  node [
    id 621
    label "barak"
  ]
  node [
    id 622
    label "zamek"
  ]
  node [
    id 623
    label "referat"
  ]
  node [
    id 624
    label "nastawnia"
  ]
  node [
    id 625
    label "obrona"
  ]
  node [
    id 626
    label "organizacja"
  ]
  node [
    id 627
    label "grupa"
  ]
  node [
    id 628
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 629
    label "dom_wielorodzinny"
  ]
  node [
    id 630
    label "zeszyt"
  ]
  node [
    id 631
    label "ram&#243;wka"
  ]
  node [
    id 632
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 633
    label "block"
  ]
  node [
    id 634
    label "bie&#380;nia"
  ]
  node [
    id 635
    label "sketch"
  ]
  node [
    id 636
    label "szkicownik"
  ]
  node [
    id 637
    label "rysunek"
  ]
  node [
    id 638
    label "opracowanie"
  ]
  node [
    id 639
    label "pomys&#322;"
  ]
  node [
    id 640
    label "plot"
  ]
  node [
    id 641
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 642
    label "rzuca&#263;"
  ]
  node [
    id 643
    label "rzuci&#263;"
  ]
  node [
    id 644
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 645
    label "szprycowa&#263;"
  ]
  node [
    id 646
    label "rzucanie"
  ]
  node [
    id 647
    label "&#322;&#243;dzki"
  ]
  node [
    id 648
    label "za&#322;adownia"
  ]
  node [
    id 649
    label "cz&#322;owiek"
  ]
  node [
    id 650
    label "naszprycowanie"
  ]
  node [
    id 651
    label "rzucenie"
  ]
  node [
    id 652
    label "tkanina"
  ]
  node [
    id 653
    label "szprycowanie"
  ]
  node [
    id 654
    label "obr&#243;t_handlowy"
  ]
  node [
    id 655
    label "narkobiznes"
  ]
  node [
    id 656
    label "metka"
  ]
  node [
    id 657
    label "tandeta"
  ]
  node [
    id 658
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 659
    label "naszprycowa&#263;"
  ]
  node [
    id 660
    label "asortyment"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
]
