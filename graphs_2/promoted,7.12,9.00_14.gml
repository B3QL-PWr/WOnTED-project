graph [
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "gra"
    origin "text"
  ]
  node [
    id 2
    label "zr&#281;czno&#347;ciowy"
    origin "text"
  ]
  node [
    id 3
    label "zagadka"
    origin "text"
  ]
  node [
    id 4
    label "logiczny"
    origin "text"
  ]
  node [
    id 5
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 6
    label "g&#322;&#243;wny"
    origin "text"
  ]
  node [
    id 7
    label "bohater"
    origin "text"
  ]
  node [
    id 8
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "akcja"
    origin "text"
  ]
  node [
    id 10
    label "dzieje"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 13
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 14
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 15
    label "pod&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 17
    label "mie&#263;_miejsce"
  ]
  node [
    id 18
    label "equal"
  ]
  node [
    id 19
    label "trwa&#263;"
  ]
  node [
    id 20
    label "chodzi&#263;"
  ]
  node [
    id 21
    label "si&#281;ga&#263;"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "obecno&#347;&#263;"
  ]
  node [
    id 24
    label "stand"
  ]
  node [
    id 25
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "uczestniczy&#263;"
  ]
  node [
    id 27
    label "participate"
  ]
  node [
    id 28
    label "robi&#263;"
  ]
  node [
    id 29
    label "istnie&#263;"
  ]
  node [
    id 30
    label "pozostawa&#263;"
  ]
  node [
    id 31
    label "zostawa&#263;"
  ]
  node [
    id 32
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 33
    label "adhere"
  ]
  node [
    id 34
    label "compass"
  ]
  node [
    id 35
    label "korzysta&#263;"
  ]
  node [
    id 36
    label "appreciation"
  ]
  node [
    id 37
    label "osi&#261;ga&#263;"
  ]
  node [
    id 38
    label "dociera&#263;"
  ]
  node [
    id 39
    label "get"
  ]
  node [
    id 40
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 41
    label "mierzy&#263;"
  ]
  node [
    id 42
    label "u&#380;ywa&#263;"
  ]
  node [
    id 43
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 44
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 45
    label "exsert"
  ]
  node [
    id 46
    label "being"
  ]
  node [
    id 47
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 48
    label "cecha"
  ]
  node [
    id 49
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 50
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 51
    label "p&#322;ywa&#263;"
  ]
  node [
    id 52
    label "run"
  ]
  node [
    id 53
    label "bangla&#263;"
  ]
  node [
    id 54
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 55
    label "przebiega&#263;"
  ]
  node [
    id 56
    label "wk&#322;ada&#263;"
  ]
  node [
    id 57
    label "proceed"
  ]
  node [
    id 58
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 59
    label "carry"
  ]
  node [
    id 60
    label "bywa&#263;"
  ]
  node [
    id 61
    label "dziama&#263;"
  ]
  node [
    id 62
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 63
    label "stara&#263;_si&#281;"
  ]
  node [
    id 64
    label "para"
  ]
  node [
    id 65
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 66
    label "str&#243;j"
  ]
  node [
    id 67
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 68
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 69
    label "krok"
  ]
  node [
    id 70
    label "tryb"
  ]
  node [
    id 71
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 72
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 73
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 74
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 75
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 76
    label "continue"
  ]
  node [
    id 77
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 78
    label "Ohio"
  ]
  node [
    id 79
    label "wci&#281;cie"
  ]
  node [
    id 80
    label "Nowy_York"
  ]
  node [
    id 81
    label "warstwa"
  ]
  node [
    id 82
    label "samopoczucie"
  ]
  node [
    id 83
    label "Illinois"
  ]
  node [
    id 84
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 85
    label "state"
  ]
  node [
    id 86
    label "Jukatan"
  ]
  node [
    id 87
    label "Kalifornia"
  ]
  node [
    id 88
    label "Wirginia"
  ]
  node [
    id 89
    label "wektor"
  ]
  node [
    id 90
    label "Teksas"
  ]
  node [
    id 91
    label "Goa"
  ]
  node [
    id 92
    label "Waszyngton"
  ]
  node [
    id 93
    label "miejsce"
  ]
  node [
    id 94
    label "Massachusetts"
  ]
  node [
    id 95
    label "Alaska"
  ]
  node [
    id 96
    label "Arakan"
  ]
  node [
    id 97
    label "Hawaje"
  ]
  node [
    id 98
    label "Maryland"
  ]
  node [
    id 99
    label "punkt"
  ]
  node [
    id 100
    label "Michigan"
  ]
  node [
    id 101
    label "Arizona"
  ]
  node [
    id 102
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 103
    label "Georgia"
  ]
  node [
    id 104
    label "poziom"
  ]
  node [
    id 105
    label "Pensylwania"
  ]
  node [
    id 106
    label "shape"
  ]
  node [
    id 107
    label "Luizjana"
  ]
  node [
    id 108
    label "Nowy_Meksyk"
  ]
  node [
    id 109
    label "Alabama"
  ]
  node [
    id 110
    label "ilo&#347;&#263;"
  ]
  node [
    id 111
    label "Kansas"
  ]
  node [
    id 112
    label "Oregon"
  ]
  node [
    id 113
    label "Floryda"
  ]
  node [
    id 114
    label "Oklahoma"
  ]
  node [
    id 115
    label "jednostka_administracyjna"
  ]
  node [
    id 116
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 117
    label "zmienno&#347;&#263;"
  ]
  node [
    id 118
    label "play"
  ]
  node [
    id 119
    label "rozgrywka"
  ]
  node [
    id 120
    label "apparent_motion"
  ]
  node [
    id 121
    label "wydarzenie"
  ]
  node [
    id 122
    label "contest"
  ]
  node [
    id 123
    label "komplet"
  ]
  node [
    id 124
    label "zabawa"
  ]
  node [
    id 125
    label "zasada"
  ]
  node [
    id 126
    label "rywalizacja"
  ]
  node [
    id 127
    label "zbijany"
  ]
  node [
    id 128
    label "post&#281;powanie"
  ]
  node [
    id 129
    label "game"
  ]
  node [
    id 130
    label "odg&#322;os"
  ]
  node [
    id 131
    label "Pok&#233;mon"
  ]
  node [
    id 132
    label "czynno&#347;&#263;"
  ]
  node [
    id 133
    label "synteza"
  ]
  node [
    id 134
    label "odtworzenie"
  ]
  node [
    id 135
    label "rekwizyt_do_gry"
  ]
  node [
    id 136
    label "resonance"
  ]
  node [
    id 137
    label "wydanie"
  ]
  node [
    id 138
    label "wpadni&#281;cie"
  ]
  node [
    id 139
    label "d&#378;wi&#281;k"
  ]
  node [
    id 140
    label "wpadanie"
  ]
  node [
    id 141
    label "wydawa&#263;"
  ]
  node [
    id 142
    label "sound"
  ]
  node [
    id 143
    label "brzmienie"
  ]
  node [
    id 144
    label "zjawisko"
  ]
  node [
    id 145
    label "wyda&#263;"
  ]
  node [
    id 146
    label "wpa&#347;&#263;"
  ]
  node [
    id 147
    label "note"
  ]
  node [
    id 148
    label "onomatopeja"
  ]
  node [
    id 149
    label "wpada&#263;"
  ]
  node [
    id 150
    label "s&#261;d"
  ]
  node [
    id 151
    label "kognicja"
  ]
  node [
    id 152
    label "campaign"
  ]
  node [
    id 153
    label "rozprawa"
  ]
  node [
    id 154
    label "zachowanie"
  ]
  node [
    id 155
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 156
    label "fashion"
  ]
  node [
    id 157
    label "robienie"
  ]
  node [
    id 158
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 159
    label "zmierzanie"
  ]
  node [
    id 160
    label "przes&#322;anka"
  ]
  node [
    id 161
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 162
    label "kazanie"
  ]
  node [
    id 163
    label "trafienie"
  ]
  node [
    id 164
    label "rewan&#380;owy"
  ]
  node [
    id 165
    label "zagrywka"
  ]
  node [
    id 166
    label "faza"
  ]
  node [
    id 167
    label "euroliga"
  ]
  node [
    id 168
    label "interliga"
  ]
  node [
    id 169
    label "runda"
  ]
  node [
    id 170
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 171
    label "rozrywka"
  ]
  node [
    id 172
    label "impreza"
  ]
  node [
    id 173
    label "igraszka"
  ]
  node [
    id 174
    label "taniec"
  ]
  node [
    id 175
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 176
    label "gambling"
  ]
  node [
    id 177
    label "chwyt"
  ]
  node [
    id 178
    label "igra"
  ]
  node [
    id 179
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 180
    label "nabawienie_si&#281;"
  ]
  node [
    id 181
    label "ubaw"
  ]
  node [
    id 182
    label "wodzirej"
  ]
  node [
    id 183
    label "activity"
  ]
  node [
    id 184
    label "bezproblemowy"
  ]
  node [
    id 185
    label "przebiec"
  ]
  node [
    id 186
    label "charakter"
  ]
  node [
    id 187
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 188
    label "motyw"
  ]
  node [
    id 189
    label "przebiegni&#281;cie"
  ]
  node [
    id 190
    label "fabu&#322;a"
  ]
  node [
    id 191
    label "proces_technologiczny"
  ]
  node [
    id 192
    label "mieszanina"
  ]
  node [
    id 193
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 194
    label "fusion"
  ]
  node [
    id 195
    label "poj&#281;cie"
  ]
  node [
    id 196
    label "reakcja_chemiczna"
  ]
  node [
    id 197
    label "zestawienie"
  ]
  node [
    id 198
    label "uog&#243;lnienie"
  ]
  node [
    id 199
    label "puszczenie"
  ]
  node [
    id 200
    label "ustalenie"
  ]
  node [
    id 201
    label "wyst&#281;p"
  ]
  node [
    id 202
    label "reproduction"
  ]
  node [
    id 203
    label "przedstawienie"
  ]
  node [
    id 204
    label "przywr&#243;cenie"
  ]
  node [
    id 205
    label "w&#322;&#261;czenie"
  ]
  node [
    id 206
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 207
    label "restoration"
  ]
  node [
    id 208
    label "odbudowanie"
  ]
  node [
    id 209
    label "lekcja"
  ]
  node [
    id 210
    label "ensemble"
  ]
  node [
    id 211
    label "grupa"
  ]
  node [
    id 212
    label "klasa"
  ]
  node [
    id 213
    label "zestaw"
  ]
  node [
    id 214
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 215
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 216
    label "regu&#322;a_Allena"
  ]
  node [
    id 217
    label "base"
  ]
  node [
    id 218
    label "umowa"
  ]
  node [
    id 219
    label "obserwacja"
  ]
  node [
    id 220
    label "zasada_d'Alemberta"
  ]
  node [
    id 221
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 222
    label "normalizacja"
  ]
  node [
    id 223
    label "moralno&#347;&#263;"
  ]
  node [
    id 224
    label "criterion"
  ]
  node [
    id 225
    label "opis"
  ]
  node [
    id 226
    label "regu&#322;a_Glogera"
  ]
  node [
    id 227
    label "prawo_Mendla"
  ]
  node [
    id 228
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 229
    label "twierdzenie"
  ]
  node [
    id 230
    label "prawo"
  ]
  node [
    id 231
    label "standard"
  ]
  node [
    id 232
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 233
    label "spos&#243;b"
  ]
  node [
    id 234
    label "qualification"
  ]
  node [
    id 235
    label "dominion"
  ]
  node [
    id 236
    label "occupation"
  ]
  node [
    id 237
    label "podstawa"
  ]
  node [
    id 238
    label "substancja"
  ]
  node [
    id 239
    label "prawid&#322;o"
  ]
  node [
    id 240
    label "dywidenda"
  ]
  node [
    id 241
    label "przebieg"
  ]
  node [
    id 242
    label "operacja"
  ]
  node [
    id 243
    label "udzia&#322;"
  ]
  node [
    id 244
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 245
    label "commotion"
  ]
  node [
    id 246
    label "jazda"
  ]
  node [
    id 247
    label "czyn"
  ]
  node [
    id 248
    label "stock"
  ]
  node [
    id 249
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 250
    label "w&#281;ze&#322;"
  ]
  node [
    id 251
    label "wysoko&#347;&#263;"
  ]
  node [
    id 252
    label "instrument_strunowy"
  ]
  node [
    id 253
    label "pi&#322;ka"
  ]
  node [
    id 254
    label "enigmat"
  ]
  node [
    id 255
    label "mystery"
  ]
  node [
    id 256
    label "&#322;amig&#322;&#243;wka"
  ]
  node [
    id 257
    label "po&#322;udnica"
  ]
  node [
    id 258
    label "rzecz"
  ]
  node [
    id 259
    label "taj&#324;"
  ]
  node [
    id 260
    label "object"
  ]
  node [
    id 261
    label "przedmiot"
  ]
  node [
    id 262
    label "temat"
  ]
  node [
    id 263
    label "mienie"
  ]
  node [
    id 264
    label "przyroda"
  ]
  node [
    id 265
    label "istota"
  ]
  node [
    id 266
    label "obiekt"
  ]
  node [
    id 267
    label "kultura"
  ]
  node [
    id 268
    label "zadanie"
  ]
  node [
    id 269
    label "riddle"
  ]
  node [
    id 270
    label "tajemnica"
  ]
  node [
    id 271
    label "motyl_dzienny"
  ]
  node [
    id 272
    label "duch"
  ]
  node [
    id 273
    label "polewik"
  ]
  node [
    id 274
    label "istota_fantastyczna"
  ]
  node [
    id 275
    label "rozumowy"
  ]
  node [
    id 276
    label "uporz&#261;dkowany"
  ]
  node [
    id 277
    label "logicznie"
  ]
  node [
    id 278
    label "rozs&#261;dny"
  ]
  node [
    id 279
    label "sensowny"
  ]
  node [
    id 280
    label "umotywowany"
  ]
  node [
    id 281
    label "skuteczny"
  ]
  node [
    id 282
    label "uzasadniony"
  ]
  node [
    id 283
    label "zrozumia&#322;y"
  ]
  node [
    id 284
    label "sensownie"
  ]
  node [
    id 285
    label "przyjemny"
  ]
  node [
    id 286
    label "rozumowany"
  ]
  node [
    id 287
    label "racjonalizowanie"
  ]
  node [
    id 288
    label "rozumowo"
  ]
  node [
    id 289
    label "umys&#322;owy"
  ]
  node [
    id 290
    label "zracjonalizowanie"
  ]
  node [
    id 291
    label "przemy&#347;lany"
  ]
  node [
    id 292
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 293
    label "rozs&#261;dnie"
  ]
  node [
    id 294
    label "rozumny"
  ]
  node [
    id 295
    label "m&#261;dry"
  ]
  node [
    id 296
    label "naukowo"
  ]
  node [
    id 297
    label "logically"
  ]
  node [
    id 298
    label "defenestracja"
  ]
  node [
    id 299
    label "agonia"
  ]
  node [
    id 300
    label "kres"
  ]
  node [
    id 301
    label "mogi&#322;a"
  ]
  node [
    id 302
    label "&#380;ycie"
  ]
  node [
    id 303
    label "kres_&#380;ycia"
  ]
  node [
    id 304
    label "upadek"
  ]
  node [
    id 305
    label "szeol"
  ]
  node [
    id 306
    label "pogrzebanie"
  ]
  node [
    id 307
    label "istota_nadprzyrodzona"
  ]
  node [
    id 308
    label "&#380;a&#322;oba"
  ]
  node [
    id 309
    label "pogrzeb"
  ]
  node [
    id 310
    label "zabicie"
  ]
  node [
    id 311
    label "ostatnie_podrygi"
  ]
  node [
    id 312
    label "dzia&#322;anie"
  ]
  node [
    id 313
    label "chwila"
  ]
  node [
    id 314
    label "koniec"
  ]
  node [
    id 315
    label "gleba"
  ]
  node [
    id 316
    label "kondycja"
  ]
  node [
    id 317
    label "ruch"
  ]
  node [
    id 318
    label "pogorszenie"
  ]
  node [
    id 319
    label "inclination"
  ]
  node [
    id 320
    label "death"
  ]
  node [
    id 321
    label "zmierzch"
  ]
  node [
    id 322
    label "nieuleczalnie_chory"
  ]
  node [
    id 323
    label "spocz&#261;&#263;"
  ]
  node [
    id 324
    label "spocz&#281;cie"
  ]
  node [
    id 325
    label "pochowanie"
  ]
  node [
    id 326
    label "spoczywa&#263;"
  ]
  node [
    id 327
    label "chowanie"
  ]
  node [
    id 328
    label "park_sztywnych"
  ]
  node [
    id 329
    label "pomnik"
  ]
  node [
    id 330
    label "nagrobek"
  ]
  node [
    id 331
    label "prochowisko"
  ]
  node [
    id 332
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 333
    label "spoczywanie"
  ]
  node [
    id 334
    label "za&#347;wiaty"
  ]
  node [
    id 335
    label "piek&#322;o"
  ]
  node [
    id 336
    label "judaizm"
  ]
  node [
    id 337
    label "destruction"
  ]
  node [
    id 338
    label "zabrzmienie"
  ]
  node [
    id 339
    label "skrzywdzenie"
  ]
  node [
    id 340
    label "pozabijanie"
  ]
  node [
    id 341
    label "zniszczenie"
  ]
  node [
    id 342
    label "zaszkodzenie"
  ]
  node [
    id 343
    label "usuni&#281;cie"
  ]
  node [
    id 344
    label "spowodowanie"
  ]
  node [
    id 345
    label "killing"
  ]
  node [
    id 346
    label "zdarzenie_si&#281;"
  ]
  node [
    id 347
    label "umarcie"
  ]
  node [
    id 348
    label "granie"
  ]
  node [
    id 349
    label "zamkni&#281;cie"
  ]
  node [
    id 350
    label "compaction"
  ]
  node [
    id 351
    label "&#380;al"
  ]
  node [
    id 352
    label "paznokie&#263;"
  ]
  node [
    id 353
    label "symbol"
  ]
  node [
    id 354
    label "czas"
  ]
  node [
    id 355
    label "kir"
  ]
  node [
    id 356
    label "brud"
  ]
  node [
    id 357
    label "wyrzucenie"
  ]
  node [
    id 358
    label "defenestration"
  ]
  node [
    id 359
    label "zaj&#347;cie"
  ]
  node [
    id 360
    label "burying"
  ]
  node [
    id 361
    label "zasypanie"
  ]
  node [
    id 362
    label "zw&#322;oki"
  ]
  node [
    id 363
    label "burial"
  ]
  node [
    id 364
    label "w&#322;o&#380;enie"
  ]
  node [
    id 365
    label "porobienie"
  ]
  node [
    id 366
    label "gr&#243;b"
  ]
  node [
    id 367
    label "uniemo&#380;liwienie"
  ]
  node [
    id 368
    label "niepowodzenie"
  ]
  node [
    id 369
    label "stypa"
  ]
  node [
    id 370
    label "pusta_noc"
  ]
  node [
    id 371
    label "grabarz"
  ]
  node [
    id 372
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 373
    label "obrz&#281;d"
  ]
  node [
    id 374
    label "raj_utracony"
  ]
  node [
    id 375
    label "umieranie"
  ]
  node [
    id 376
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 377
    label "prze&#380;ywanie"
  ]
  node [
    id 378
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 379
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 380
    label "po&#322;&#243;g"
  ]
  node [
    id 381
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 382
    label "subsistence"
  ]
  node [
    id 383
    label "power"
  ]
  node [
    id 384
    label "okres_noworodkowy"
  ]
  node [
    id 385
    label "prze&#380;ycie"
  ]
  node [
    id 386
    label "wiek_matuzalemowy"
  ]
  node [
    id 387
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 388
    label "entity"
  ]
  node [
    id 389
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 390
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 391
    label "do&#380;ywanie"
  ]
  node [
    id 392
    label "byt"
  ]
  node [
    id 393
    label "dzieci&#324;stwo"
  ]
  node [
    id 394
    label "andropauza"
  ]
  node [
    id 395
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 396
    label "rozw&#243;j"
  ]
  node [
    id 397
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 398
    label "menopauza"
  ]
  node [
    id 399
    label "koleje_losu"
  ]
  node [
    id 400
    label "bycie"
  ]
  node [
    id 401
    label "zegar_biologiczny"
  ]
  node [
    id 402
    label "szwung"
  ]
  node [
    id 403
    label "przebywanie"
  ]
  node [
    id 404
    label "warunki"
  ]
  node [
    id 405
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 406
    label "niemowl&#281;ctwo"
  ]
  node [
    id 407
    label "&#380;ywy"
  ]
  node [
    id 408
    label "life"
  ]
  node [
    id 409
    label "staro&#347;&#263;"
  ]
  node [
    id 410
    label "energy"
  ]
  node [
    id 411
    label "najwa&#380;niejszy"
  ]
  node [
    id 412
    label "g&#322;&#243;wnie"
  ]
  node [
    id 413
    label "Messi"
  ]
  node [
    id 414
    label "Herkules_Poirot"
  ]
  node [
    id 415
    label "cz&#322;owiek"
  ]
  node [
    id 416
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 417
    label "Achilles"
  ]
  node [
    id 418
    label "Harry_Potter"
  ]
  node [
    id 419
    label "Casanova"
  ]
  node [
    id 420
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 421
    label "Zgredek"
  ]
  node [
    id 422
    label "Asterix"
  ]
  node [
    id 423
    label "Winnetou"
  ]
  node [
    id 424
    label "uczestnik"
  ]
  node [
    id 425
    label "posta&#263;"
  ]
  node [
    id 426
    label "&#347;mia&#322;ek"
  ]
  node [
    id 427
    label "Mario"
  ]
  node [
    id 428
    label "Borewicz"
  ]
  node [
    id 429
    label "Quasimodo"
  ]
  node [
    id 430
    label "Sherlock_Holmes"
  ]
  node [
    id 431
    label "Wallenrod"
  ]
  node [
    id 432
    label "Herkules"
  ]
  node [
    id 433
    label "bohaterski"
  ]
  node [
    id 434
    label "Don_Juan"
  ]
  node [
    id 435
    label "podmiot"
  ]
  node [
    id 436
    label "Don_Kiszot"
  ]
  node [
    id 437
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 438
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 439
    label "Hamlet"
  ]
  node [
    id 440
    label "Werter"
  ]
  node [
    id 441
    label "Szwejk"
  ]
  node [
    id 442
    label "charakterystyka"
  ]
  node [
    id 443
    label "zaistnie&#263;"
  ]
  node [
    id 444
    label "Osjan"
  ]
  node [
    id 445
    label "kto&#347;"
  ]
  node [
    id 446
    label "wygl&#261;d"
  ]
  node [
    id 447
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 448
    label "osobowo&#347;&#263;"
  ]
  node [
    id 449
    label "wytw&#243;r"
  ]
  node [
    id 450
    label "trim"
  ]
  node [
    id 451
    label "poby&#263;"
  ]
  node [
    id 452
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 453
    label "Aspazja"
  ]
  node [
    id 454
    label "punkt_widzenia"
  ]
  node [
    id 455
    label "kompleksja"
  ]
  node [
    id 456
    label "wytrzyma&#263;"
  ]
  node [
    id 457
    label "budowa"
  ]
  node [
    id 458
    label "formacja"
  ]
  node [
    id 459
    label "pozosta&#263;"
  ]
  node [
    id 460
    label "point"
  ]
  node [
    id 461
    label "go&#347;&#263;"
  ]
  node [
    id 462
    label "zuch"
  ]
  node [
    id 463
    label "rycerzyk"
  ]
  node [
    id 464
    label "odwa&#380;ny"
  ]
  node [
    id 465
    label "ryzykant"
  ]
  node [
    id 466
    label "morowiec"
  ]
  node [
    id 467
    label "trawa"
  ]
  node [
    id 468
    label "ro&#347;lina"
  ]
  node [
    id 469
    label "twardziel"
  ]
  node [
    id 470
    label "owsowe"
  ]
  node [
    id 471
    label "ludzko&#347;&#263;"
  ]
  node [
    id 472
    label "asymilowanie"
  ]
  node [
    id 473
    label "wapniak"
  ]
  node [
    id 474
    label "asymilowa&#263;"
  ]
  node [
    id 475
    label "os&#322;abia&#263;"
  ]
  node [
    id 476
    label "hominid"
  ]
  node [
    id 477
    label "podw&#322;adny"
  ]
  node [
    id 478
    label "os&#322;abianie"
  ]
  node [
    id 479
    label "figura"
  ]
  node [
    id 480
    label "portrecista"
  ]
  node [
    id 481
    label "dwun&#243;g"
  ]
  node [
    id 482
    label "profanum"
  ]
  node [
    id 483
    label "mikrokosmos"
  ]
  node [
    id 484
    label "nasada"
  ]
  node [
    id 485
    label "antropochoria"
  ]
  node [
    id 486
    label "osoba"
  ]
  node [
    id 487
    label "wz&#243;r"
  ]
  node [
    id 488
    label "senior"
  ]
  node [
    id 489
    label "oddzia&#322;ywanie"
  ]
  node [
    id 490
    label "Adam"
  ]
  node [
    id 491
    label "homo_sapiens"
  ]
  node [
    id 492
    label "polifag"
  ]
  node [
    id 493
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 494
    label "organizacja"
  ]
  node [
    id 495
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 496
    label "nauka_prawa"
  ]
  node [
    id 497
    label "Szekspir"
  ]
  node [
    id 498
    label "Mickiewicz"
  ]
  node [
    id 499
    label "cierpienie"
  ]
  node [
    id 500
    label "Atomowa_Pch&#322;a"
  ]
  node [
    id 501
    label "bohatersko"
  ]
  node [
    id 502
    label "dzielny"
  ]
  node [
    id 503
    label "silny"
  ]
  node [
    id 504
    label "waleczny"
  ]
  node [
    id 505
    label "jedyny"
  ]
  node [
    id 506
    label "du&#380;y"
  ]
  node [
    id 507
    label "zdr&#243;w"
  ]
  node [
    id 508
    label "calu&#347;ko"
  ]
  node [
    id 509
    label "kompletny"
  ]
  node [
    id 510
    label "pe&#322;ny"
  ]
  node [
    id 511
    label "podobny"
  ]
  node [
    id 512
    label "ca&#322;o"
  ]
  node [
    id 513
    label "kompletnie"
  ]
  node [
    id 514
    label "zupe&#322;ny"
  ]
  node [
    id 515
    label "w_pizdu"
  ]
  node [
    id 516
    label "przypominanie"
  ]
  node [
    id 517
    label "podobnie"
  ]
  node [
    id 518
    label "upodabnianie_si&#281;"
  ]
  node [
    id 519
    label "upodobnienie"
  ]
  node [
    id 520
    label "drugi"
  ]
  node [
    id 521
    label "taki"
  ]
  node [
    id 522
    label "charakterystyczny"
  ]
  node [
    id 523
    label "upodobnienie_si&#281;"
  ]
  node [
    id 524
    label "zasymilowanie"
  ]
  node [
    id 525
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 526
    label "ukochany"
  ]
  node [
    id 527
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 528
    label "najlepszy"
  ]
  node [
    id 529
    label "optymalnie"
  ]
  node [
    id 530
    label "doros&#322;y"
  ]
  node [
    id 531
    label "znaczny"
  ]
  node [
    id 532
    label "niema&#322;o"
  ]
  node [
    id 533
    label "wiele"
  ]
  node [
    id 534
    label "rozwini&#281;ty"
  ]
  node [
    id 535
    label "dorodny"
  ]
  node [
    id 536
    label "wa&#380;ny"
  ]
  node [
    id 537
    label "prawdziwy"
  ]
  node [
    id 538
    label "du&#380;o"
  ]
  node [
    id 539
    label "zdrowy"
  ]
  node [
    id 540
    label "ciekawy"
  ]
  node [
    id 541
    label "szybki"
  ]
  node [
    id 542
    label "&#380;ywotny"
  ]
  node [
    id 543
    label "naturalny"
  ]
  node [
    id 544
    label "&#380;ywo"
  ]
  node [
    id 545
    label "o&#380;ywianie"
  ]
  node [
    id 546
    label "g&#322;&#281;boki"
  ]
  node [
    id 547
    label "wyra&#378;ny"
  ]
  node [
    id 548
    label "czynny"
  ]
  node [
    id 549
    label "aktualny"
  ]
  node [
    id 550
    label "zgrabny"
  ]
  node [
    id 551
    label "realistyczny"
  ]
  node [
    id 552
    label "energiczny"
  ]
  node [
    id 553
    label "nieograniczony"
  ]
  node [
    id 554
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 555
    label "satysfakcja"
  ]
  node [
    id 556
    label "bezwzgl&#281;dny"
  ]
  node [
    id 557
    label "otwarty"
  ]
  node [
    id 558
    label "wype&#322;nienie"
  ]
  node [
    id 559
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 560
    label "pe&#322;no"
  ]
  node [
    id 561
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 562
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 563
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 564
    label "r&#243;wny"
  ]
  node [
    id 565
    label "nieuszkodzony"
  ]
  node [
    id 566
    label "odpowiednio"
  ]
  node [
    id 567
    label "funkcja"
  ]
  node [
    id 568
    label "act"
  ]
  node [
    id 569
    label "tallness"
  ]
  node [
    id 570
    label "altitude"
  ]
  node [
    id 571
    label "rozmiar"
  ]
  node [
    id 572
    label "degree"
  ]
  node [
    id 573
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 574
    label "odcinek"
  ]
  node [
    id 575
    label "k&#261;t"
  ]
  node [
    id 576
    label "wielko&#347;&#263;"
  ]
  node [
    id 577
    label "sum"
  ]
  node [
    id 578
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 579
    label "gambit"
  ]
  node [
    id 580
    label "move"
  ]
  node [
    id 581
    label "manewr"
  ]
  node [
    id 582
    label "uderzenie"
  ]
  node [
    id 583
    label "posuni&#281;cie"
  ]
  node [
    id 584
    label "myk"
  ]
  node [
    id 585
    label "gra_w_karty"
  ]
  node [
    id 586
    label "mecz"
  ]
  node [
    id 587
    label "travel"
  ]
  node [
    id 588
    label "proces_my&#347;lowy"
  ]
  node [
    id 589
    label "liczenie"
  ]
  node [
    id 590
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 591
    label "supremum"
  ]
  node [
    id 592
    label "laparotomia"
  ]
  node [
    id 593
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 594
    label "jednostka"
  ]
  node [
    id 595
    label "matematyka"
  ]
  node [
    id 596
    label "rzut"
  ]
  node [
    id 597
    label "liczy&#263;"
  ]
  node [
    id 598
    label "strategia"
  ]
  node [
    id 599
    label "torakotomia"
  ]
  node [
    id 600
    label "chirurg"
  ]
  node [
    id 601
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 602
    label "zabieg"
  ]
  node [
    id 603
    label "szew"
  ]
  node [
    id 604
    label "mathematical_process"
  ]
  node [
    id 605
    label "infimum"
  ]
  node [
    id 606
    label "linia"
  ]
  node [
    id 607
    label "procedura"
  ]
  node [
    id 608
    label "zbi&#243;r"
  ]
  node [
    id 609
    label "proces"
  ]
  node [
    id 610
    label "room"
  ]
  node [
    id 611
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 612
    label "sequence"
  ]
  node [
    id 613
    label "praca"
  ]
  node [
    id 614
    label "cycle"
  ]
  node [
    id 615
    label "kwota"
  ]
  node [
    id 616
    label "szwadron"
  ]
  node [
    id 617
    label "wykrzyknik"
  ]
  node [
    id 618
    label "awantura"
  ]
  node [
    id 619
    label "journey"
  ]
  node [
    id 620
    label "sport"
  ]
  node [
    id 621
    label "heca"
  ]
  node [
    id 622
    label "cavalry"
  ]
  node [
    id 623
    label "szale&#324;stwo"
  ]
  node [
    id 624
    label "chor&#261;giew"
  ]
  node [
    id 625
    label "doch&#243;d"
  ]
  node [
    id 626
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 627
    label "wi&#261;zanie"
  ]
  node [
    id 628
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 629
    label "bratnia_dusza"
  ]
  node [
    id 630
    label "trasa"
  ]
  node [
    id 631
    label "uczesanie"
  ]
  node [
    id 632
    label "orbita"
  ]
  node [
    id 633
    label "kryszta&#322;"
  ]
  node [
    id 634
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 635
    label "zwi&#261;zanie"
  ]
  node [
    id 636
    label "graf"
  ]
  node [
    id 637
    label "hitch"
  ]
  node [
    id 638
    label "struktura_anatomiczna"
  ]
  node [
    id 639
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 640
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 641
    label "o&#347;rodek"
  ]
  node [
    id 642
    label "marriage"
  ]
  node [
    id 643
    label "ekliptyka"
  ]
  node [
    id 644
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 645
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 646
    label "problem"
  ]
  node [
    id 647
    label "zawi&#261;za&#263;"
  ]
  node [
    id 648
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 649
    label "fala_stoj&#261;ca"
  ]
  node [
    id 650
    label "tying"
  ]
  node [
    id 651
    label "argument"
  ]
  node [
    id 652
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 653
    label "zwi&#261;za&#263;"
  ]
  node [
    id 654
    label "mila_morska"
  ]
  node [
    id 655
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 656
    label "skupienie"
  ]
  node [
    id 657
    label "zgrubienie"
  ]
  node [
    id 658
    label "pismo_klinowe"
  ]
  node [
    id 659
    label "przeci&#281;cie"
  ]
  node [
    id 660
    label "band"
  ]
  node [
    id 661
    label "zwi&#261;zek"
  ]
  node [
    id 662
    label "epoka"
  ]
  node [
    id 663
    label "aalen"
  ]
  node [
    id 664
    label "jura_wczesna"
  ]
  node [
    id 665
    label "holocen"
  ]
  node [
    id 666
    label "pliocen"
  ]
  node [
    id 667
    label "plejstocen"
  ]
  node [
    id 668
    label "paleocen"
  ]
  node [
    id 669
    label "bajos"
  ]
  node [
    id 670
    label "kelowej"
  ]
  node [
    id 671
    label "eocen"
  ]
  node [
    id 672
    label "jednostka_geologiczna"
  ]
  node [
    id 673
    label "okres"
  ]
  node [
    id 674
    label "schy&#322;ek"
  ]
  node [
    id 675
    label "miocen"
  ]
  node [
    id 676
    label "&#347;rodkowy_trias"
  ]
  node [
    id 677
    label "term"
  ]
  node [
    id 678
    label "Zeitgeist"
  ]
  node [
    id 679
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 680
    label "wczesny_trias"
  ]
  node [
    id 681
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 682
    label "jura_&#347;rodkowa"
  ]
  node [
    id 683
    label "oligocen"
  ]
  node [
    id 684
    label "pryncypa&#322;"
  ]
  node [
    id 685
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 686
    label "kszta&#322;t"
  ]
  node [
    id 687
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 688
    label "wiedza"
  ]
  node [
    id 689
    label "kierowa&#263;"
  ]
  node [
    id 690
    label "alkohol"
  ]
  node [
    id 691
    label "zdolno&#347;&#263;"
  ]
  node [
    id 692
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 693
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 694
    label "sztuka"
  ]
  node [
    id 695
    label "dekiel"
  ]
  node [
    id 696
    label "&#347;ci&#281;cie"
  ]
  node [
    id 697
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 698
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 699
    label "&#347;ci&#281;gno"
  ]
  node [
    id 700
    label "noosfera"
  ]
  node [
    id 701
    label "byd&#322;o"
  ]
  node [
    id 702
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 703
    label "makrocefalia"
  ]
  node [
    id 704
    label "ucho"
  ]
  node [
    id 705
    label "g&#243;ra"
  ]
  node [
    id 706
    label "m&#243;zg"
  ]
  node [
    id 707
    label "kierownictwo"
  ]
  node [
    id 708
    label "fryzura"
  ]
  node [
    id 709
    label "umys&#322;"
  ]
  node [
    id 710
    label "cia&#322;o"
  ]
  node [
    id 711
    label "cz&#322;onek"
  ]
  node [
    id 712
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 713
    label "czaszka"
  ]
  node [
    id 714
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 715
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 716
    label "organ"
  ]
  node [
    id 717
    label "ptaszek"
  ]
  node [
    id 718
    label "element_anatomiczny"
  ]
  node [
    id 719
    label "przyrodzenie"
  ]
  node [
    id 720
    label "fiut"
  ]
  node [
    id 721
    label "shaft"
  ]
  node [
    id 722
    label "wchodzenie"
  ]
  node [
    id 723
    label "przedstawiciel"
  ]
  node [
    id 724
    label "wej&#347;cie"
  ]
  node [
    id 725
    label "co&#347;"
  ]
  node [
    id 726
    label "budynek"
  ]
  node [
    id 727
    label "thing"
  ]
  node [
    id 728
    label "program"
  ]
  node [
    id 729
    label "strona"
  ]
  node [
    id 730
    label "przelezienie"
  ]
  node [
    id 731
    label "&#347;piew"
  ]
  node [
    id 732
    label "Synaj"
  ]
  node [
    id 733
    label "Kreml"
  ]
  node [
    id 734
    label "kierunek"
  ]
  node [
    id 735
    label "wysoki"
  ]
  node [
    id 736
    label "element"
  ]
  node [
    id 737
    label "wzniesienie"
  ]
  node [
    id 738
    label "pi&#281;tro"
  ]
  node [
    id 739
    label "Ropa"
  ]
  node [
    id 740
    label "kupa"
  ]
  node [
    id 741
    label "przele&#378;&#263;"
  ]
  node [
    id 742
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 743
    label "karczek"
  ]
  node [
    id 744
    label "rami&#261;czko"
  ]
  node [
    id 745
    label "Jaworze"
  ]
  node [
    id 746
    label "przedzia&#322;ek"
  ]
  node [
    id 747
    label "pasemko"
  ]
  node [
    id 748
    label "fryz"
  ]
  node [
    id 749
    label "w&#322;osy"
  ]
  node [
    id 750
    label "grzywka"
  ]
  node [
    id 751
    label "egreta"
  ]
  node [
    id 752
    label "falownica"
  ]
  node [
    id 753
    label "fonta&#378;"
  ]
  node [
    id 754
    label "fryzura_intymna"
  ]
  node [
    id 755
    label "ozdoba"
  ]
  node [
    id 756
    label "pr&#243;bowanie"
  ]
  node [
    id 757
    label "rola"
  ]
  node [
    id 758
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 759
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 760
    label "realizacja"
  ]
  node [
    id 761
    label "scena"
  ]
  node [
    id 762
    label "didaskalia"
  ]
  node [
    id 763
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 764
    label "environment"
  ]
  node [
    id 765
    label "head"
  ]
  node [
    id 766
    label "scenariusz"
  ]
  node [
    id 767
    label "egzemplarz"
  ]
  node [
    id 768
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 769
    label "utw&#243;r"
  ]
  node [
    id 770
    label "kultura_duchowa"
  ]
  node [
    id 771
    label "fortel"
  ]
  node [
    id 772
    label "theatrical_performance"
  ]
  node [
    id 773
    label "ambala&#380;"
  ]
  node [
    id 774
    label "sprawno&#347;&#263;"
  ]
  node [
    id 775
    label "kobieta"
  ]
  node [
    id 776
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 777
    label "Faust"
  ]
  node [
    id 778
    label "scenografia"
  ]
  node [
    id 779
    label "ods&#322;ona"
  ]
  node [
    id 780
    label "turn"
  ]
  node [
    id 781
    label "pokaz"
  ]
  node [
    id 782
    label "przedstawi&#263;"
  ]
  node [
    id 783
    label "Apollo"
  ]
  node [
    id 784
    label "przedstawianie"
  ]
  node [
    id 785
    label "przedstawia&#263;"
  ]
  node [
    id 786
    label "towar"
  ]
  node [
    id 787
    label "posiada&#263;"
  ]
  node [
    id 788
    label "potencja&#322;"
  ]
  node [
    id 789
    label "zapomnienie"
  ]
  node [
    id 790
    label "zapomina&#263;"
  ]
  node [
    id 791
    label "zapominanie"
  ]
  node [
    id 792
    label "ability"
  ]
  node [
    id 793
    label "obliczeniowo"
  ]
  node [
    id 794
    label "zapomnie&#263;"
  ]
  node [
    id 795
    label "mi&#281;sie&#324;"
  ]
  node [
    id 796
    label "m&#322;ot"
  ]
  node [
    id 797
    label "znak"
  ]
  node [
    id 798
    label "drzewo"
  ]
  node [
    id 799
    label "pr&#243;ba"
  ]
  node [
    id 800
    label "attribute"
  ]
  node [
    id 801
    label "marka"
  ]
  node [
    id 802
    label "napinacz"
  ]
  node [
    id 803
    label "czapka"
  ]
  node [
    id 804
    label "ucho_zewn&#281;trzne"
  ]
  node [
    id 805
    label "elektronystagmografia"
  ]
  node [
    id 806
    label "handle"
  ]
  node [
    id 807
    label "ochraniacz"
  ]
  node [
    id 808
    label "ma&#322;&#380;owina"
  ]
  node [
    id 809
    label "twarz"
  ]
  node [
    id 810
    label "ucho_wewn&#281;trzne"
  ]
  node [
    id 811
    label "uchwyt"
  ]
  node [
    id 812
    label "ucho_&#347;rodkowe"
  ]
  node [
    id 813
    label "przew&#243;d_s&#322;uchowy"
  ]
  node [
    id 814
    label "otw&#243;r"
  ]
  node [
    id 815
    label "szew_kostny"
  ]
  node [
    id 816
    label "trzewioczaszka"
  ]
  node [
    id 817
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 818
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 819
    label "m&#243;zgoczaszka"
  ]
  node [
    id 820
    label "ciemi&#281;"
  ]
  node [
    id 821
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 822
    label "dynia"
  ]
  node [
    id 823
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 824
    label "rozszczep_czaszki"
  ]
  node [
    id 825
    label "szew_strza&#322;kowy"
  ]
  node [
    id 826
    label "puszka_m&#243;zgowa"
  ]
  node [
    id 827
    label "mak&#243;wka"
  ]
  node [
    id 828
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 829
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 830
    label "szkielet"
  ]
  node [
    id 831
    label "zatoka"
  ]
  node [
    id 832
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 833
    label "oczod&#243;&#322;"
  ]
  node [
    id 834
    label "potylica"
  ]
  node [
    id 835
    label "lemiesz"
  ]
  node [
    id 836
    label "&#380;uchwa"
  ]
  node [
    id 837
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 838
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 839
    label "diafanoskopia"
  ]
  node [
    id 840
    label "&#322;eb"
  ]
  node [
    id 841
    label "substancja_szara"
  ]
  node [
    id 842
    label "encefalografia"
  ]
  node [
    id 843
    label "przedmurze"
  ]
  node [
    id 844
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 845
    label "bruzda"
  ]
  node [
    id 846
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 847
    label "most"
  ]
  node [
    id 848
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 849
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 850
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 851
    label "podwzg&#243;rze"
  ]
  node [
    id 852
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 853
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 854
    label "wzg&#243;rze"
  ]
  node [
    id 855
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 856
    label "elektroencefalogram"
  ]
  node [
    id 857
    label "przodom&#243;zgowie"
  ]
  node [
    id 858
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 859
    label "projektodawca"
  ]
  node [
    id 860
    label "przysadka"
  ]
  node [
    id 861
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 862
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 863
    label "zw&#243;j"
  ]
  node [
    id 864
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 865
    label "kora_m&#243;zgowa"
  ]
  node [
    id 866
    label "kresom&#243;zgowie"
  ]
  node [
    id 867
    label "poduszka"
  ]
  node [
    id 868
    label "intelekt"
  ]
  node [
    id 869
    label "lid"
  ]
  node [
    id 870
    label "ko&#322;o"
  ]
  node [
    id 871
    label "pokrywa"
  ]
  node [
    id 872
    label "dekielek"
  ]
  node [
    id 873
    label "os&#322;ona"
  ]
  node [
    id 874
    label "g&#322;upek"
  ]
  node [
    id 875
    label "g&#322;os"
  ]
  node [
    id 876
    label "zwierzchnik"
  ]
  node [
    id 877
    label "ekshumowanie"
  ]
  node [
    id 878
    label "jednostka_organizacyjna"
  ]
  node [
    id 879
    label "p&#322;aszczyzna"
  ]
  node [
    id 880
    label "odwadnia&#263;"
  ]
  node [
    id 881
    label "zabalsamowanie"
  ]
  node [
    id 882
    label "zesp&#243;&#322;"
  ]
  node [
    id 883
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 884
    label "odwodni&#263;"
  ]
  node [
    id 885
    label "sk&#243;ra"
  ]
  node [
    id 886
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 887
    label "staw"
  ]
  node [
    id 888
    label "ow&#322;osienie"
  ]
  node [
    id 889
    label "mi&#281;so"
  ]
  node [
    id 890
    label "zabalsamowa&#263;"
  ]
  node [
    id 891
    label "Izba_Konsyliarska"
  ]
  node [
    id 892
    label "unerwienie"
  ]
  node [
    id 893
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 894
    label "kremacja"
  ]
  node [
    id 895
    label "biorytm"
  ]
  node [
    id 896
    label "sekcja"
  ]
  node [
    id 897
    label "istota_&#380;ywa"
  ]
  node [
    id 898
    label "otworzy&#263;"
  ]
  node [
    id 899
    label "otwiera&#263;"
  ]
  node [
    id 900
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 901
    label "otworzenie"
  ]
  node [
    id 902
    label "materia"
  ]
  node [
    id 903
    label "otwieranie"
  ]
  node [
    id 904
    label "ty&#322;"
  ]
  node [
    id 905
    label "tanatoplastyk"
  ]
  node [
    id 906
    label "odwadnianie"
  ]
  node [
    id 907
    label "Komitet_Region&#243;w"
  ]
  node [
    id 908
    label "odwodnienie"
  ]
  node [
    id 909
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 910
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 911
    label "nieumar&#322;y"
  ]
  node [
    id 912
    label "pochowa&#263;"
  ]
  node [
    id 913
    label "balsamowa&#263;"
  ]
  node [
    id 914
    label "tanatoplastyka"
  ]
  node [
    id 915
    label "temperatura"
  ]
  node [
    id 916
    label "ekshumowa&#263;"
  ]
  node [
    id 917
    label "balsamowanie"
  ]
  node [
    id 918
    label "uk&#322;ad"
  ]
  node [
    id 919
    label "prz&#243;d"
  ]
  node [
    id 920
    label "l&#281;d&#378;wie"
  ]
  node [
    id 921
    label "zbiorowisko"
  ]
  node [
    id 922
    label "ro&#347;liny"
  ]
  node [
    id 923
    label "p&#281;d"
  ]
  node [
    id 924
    label "wegetowanie"
  ]
  node [
    id 925
    label "zadziorek"
  ]
  node [
    id 926
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 927
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 928
    label "do&#322;owa&#263;"
  ]
  node [
    id 929
    label "wegetacja"
  ]
  node [
    id 930
    label "owoc"
  ]
  node [
    id 931
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 932
    label "strzyc"
  ]
  node [
    id 933
    label "w&#322;&#243;kno"
  ]
  node [
    id 934
    label "g&#322;uszenie"
  ]
  node [
    id 935
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 936
    label "fitotron"
  ]
  node [
    id 937
    label "bulwka"
  ]
  node [
    id 938
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 939
    label "odn&#243;&#380;ka"
  ]
  node [
    id 940
    label "epiderma"
  ]
  node [
    id 941
    label "gumoza"
  ]
  node [
    id 942
    label "strzy&#380;enie"
  ]
  node [
    id 943
    label "wypotnik"
  ]
  node [
    id 944
    label "flawonoid"
  ]
  node [
    id 945
    label "wyro&#347;le"
  ]
  node [
    id 946
    label "do&#322;owanie"
  ]
  node [
    id 947
    label "g&#322;uszy&#263;"
  ]
  node [
    id 948
    label "pora&#380;a&#263;"
  ]
  node [
    id 949
    label "fitocenoza"
  ]
  node [
    id 950
    label "hodowla"
  ]
  node [
    id 951
    label "fotoautotrof"
  ]
  node [
    id 952
    label "wegetowa&#263;"
  ]
  node [
    id 953
    label "pochewka"
  ]
  node [
    id 954
    label "sok"
  ]
  node [
    id 955
    label "system_korzeniowy"
  ]
  node [
    id 956
    label "zawi&#261;zek"
  ]
  node [
    id 957
    label "pami&#281;&#263;"
  ]
  node [
    id 958
    label "pomieszanie_si&#281;"
  ]
  node [
    id 959
    label "wn&#281;trze"
  ]
  node [
    id 960
    label "wyobra&#378;nia"
  ]
  node [
    id 961
    label "obci&#281;cie"
  ]
  node [
    id 962
    label "decapitation"
  ]
  node [
    id 963
    label "opitolenie"
  ]
  node [
    id 964
    label "poobcinanie"
  ]
  node [
    id 965
    label "zmro&#380;enie"
  ]
  node [
    id 966
    label "snub"
  ]
  node [
    id 967
    label "kr&#243;j"
  ]
  node [
    id 968
    label "oblanie"
  ]
  node [
    id 969
    label "przeegzaminowanie"
  ]
  node [
    id 970
    label "odbicie"
  ]
  node [
    id 971
    label "ping-pong"
  ]
  node [
    id 972
    label "cut"
  ]
  node [
    id 973
    label "gilotyna"
  ]
  node [
    id 974
    label "szafot"
  ]
  node [
    id 975
    label "skr&#243;cenie"
  ]
  node [
    id 976
    label "kara_&#347;mierci"
  ]
  node [
    id 977
    label "siatk&#243;wka"
  ]
  node [
    id 978
    label "k&#322;&#243;tnia"
  ]
  node [
    id 979
    label "ukszta&#322;towanie"
  ]
  node [
    id 980
    label "splay"
  ]
  node [
    id 981
    label "tenis"
  ]
  node [
    id 982
    label "odci&#281;cie"
  ]
  node [
    id 983
    label "st&#281;&#380;enie"
  ]
  node [
    id 984
    label "chop"
  ]
  node [
    id 985
    label "wada_wrodzona"
  ]
  node [
    id 986
    label "decapitate"
  ]
  node [
    id 987
    label "usun&#261;&#263;"
  ]
  node [
    id 988
    label "obci&#261;&#263;"
  ]
  node [
    id 989
    label "naruszy&#263;"
  ]
  node [
    id 990
    label "obni&#380;y&#263;"
  ]
  node [
    id 991
    label "okroi&#263;"
  ]
  node [
    id 992
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 993
    label "zaci&#261;&#263;"
  ]
  node [
    id 994
    label "uderzy&#263;"
  ]
  node [
    id 995
    label "obla&#263;"
  ]
  node [
    id 996
    label "odbi&#263;"
  ]
  node [
    id 997
    label "skr&#243;ci&#263;"
  ]
  node [
    id 998
    label "pozbawi&#263;"
  ]
  node [
    id 999
    label "opitoli&#263;"
  ]
  node [
    id 1000
    label "zabi&#263;"
  ]
  node [
    id 1001
    label "spowodowa&#263;"
  ]
  node [
    id 1002
    label "wywo&#322;a&#263;"
  ]
  node [
    id 1003
    label "unieruchomi&#263;"
  ]
  node [
    id 1004
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 1005
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1006
    label "odci&#261;&#263;"
  ]
  node [
    id 1007
    label "write_out"
  ]
  node [
    id 1008
    label "spirala"
  ]
  node [
    id 1009
    label "p&#322;at"
  ]
  node [
    id 1010
    label "comeliness"
  ]
  node [
    id 1011
    label "kielich"
  ]
  node [
    id 1012
    label "face"
  ]
  node [
    id 1013
    label "blaszka"
  ]
  node [
    id 1014
    label "p&#281;tla"
  ]
  node [
    id 1015
    label "pasmo"
  ]
  node [
    id 1016
    label "linearno&#347;&#263;"
  ]
  node [
    id 1017
    label "gwiazda"
  ]
  node [
    id 1018
    label "miniatura"
  ]
  node [
    id 1019
    label "kr&#281;torogie"
  ]
  node [
    id 1020
    label "czochrad&#322;o"
  ]
  node [
    id 1021
    label "posp&#243;lstwo"
  ]
  node [
    id 1022
    label "kraal"
  ]
  node [
    id 1023
    label "livestock"
  ]
  node [
    id 1024
    label "u&#380;ywka"
  ]
  node [
    id 1025
    label "najebka"
  ]
  node [
    id 1026
    label "upajanie"
  ]
  node [
    id 1027
    label "szk&#322;o"
  ]
  node [
    id 1028
    label "wypicie"
  ]
  node [
    id 1029
    label "rozgrzewacz"
  ]
  node [
    id 1030
    label "nap&#243;j"
  ]
  node [
    id 1031
    label "alko"
  ]
  node [
    id 1032
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1033
    label "picie"
  ]
  node [
    id 1034
    label "upojenie"
  ]
  node [
    id 1035
    label "upija&#263;"
  ]
  node [
    id 1036
    label "likwor"
  ]
  node [
    id 1037
    label "poniewierca"
  ]
  node [
    id 1038
    label "grupa_hydroksylowa"
  ]
  node [
    id 1039
    label "spirytualia"
  ]
  node [
    id 1040
    label "le&#380;akownia"
  ]
  node [
    id 1041
    label "upi&#263;"
  ]
  node [
    id 1042
    label "piwniczka"
  ]
  node [
    id 1043
    label "gorzelnia_rolnicza"
  ]
  node [
    id 1044
    label "sterowa&#263;"
  ]
  node [
    id 1045
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1046
    label "manipulate"
  ]
  node [
    id 1047
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1048
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1049
    label "ustawia&#263;"
  ]
  node [
    id 1050
    label "give"
  ]
  node [
    id 1051
    label "przeznacza&#263;"
  ]
  node [
    id 1052
    label "control"
  ]
  node [
    id 1053
    label "match"
  ]
  node [
    id 1054
    label "motywowa&#263;"
  ]
  node [
    id 1055
    label "administrowa&#263;"
  ]
  node [
    id 1056
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1057
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1058
    label "order"
  ]
  node [
    id 1059
    label "indicate"
  ]
  node [
    id 1060
    label "cognition"
  ]
  node [
    id 1061
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1062
    label "pozwolenie"
  ]
  node [
    id 1063
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1064
    label "zaawansowanie"
  ]
  node [
    id 1065
    label "wykszta&#322;cenie"
  ]
  node [
    id 1066
    label "biuro"
  ]
  node [
    id 1067
    label "lead"
  ]
  node [
    id 1068
    label "siedziba"
  ]
  node [
    id 1069
    label "w&#322;adza"
  ]
  node [
    id 1070
    label "talk"
  ]
  node [
    id 1071
    label "gaworzy&#263;"
  ]
  node [
    id 1072
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1073
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1074
    label "chatter"
  ]
  node [
    id 1075
    label "m&#243;wi&#263;"
  ]
  node [
    id 1076
    label "niemowl&#281;"
  ]
  node [
    id 1077
    label "kosmetyk"
  ]
  node [
    id 1078
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 1079
    label "samodzielny"
  ]
  node [
    id 1080
    label "swojak"
  ]
  node [
    id 1081
    label "odpowiedni"
  ]
  node [
    id 1082
    label "bli&#378;ni"
  ]
  node [
    id 1083
    label "odr&#281;bny"
  ]
  node [
    id 1084
    label "sobieradzki"
  ]
  node [
    id 1085
    label "niepodleg&#322;y"
  ]
  node [
    id 1086
    label "czyj&#347;"
  ]
  node [
    id 1087
    label "autonomicznie"
  ]
  node [
    id 1088
    label "indywidualny"
  ]
  node [
    id 1089
    label "samodzielnie"
  ]
  node [
    id 1090
    label "w&#322;asny"
  ]
  node [
    id 1091
    label "osobny"
  ]
  node [
    id 1092
    label "zdarzony"
  ]
  node [
    id 1093
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1094
    label "nale&#380;ny"
  ]
  node [
    id 1095
    label "nale&#380;yty"
  ]
  node [
    id 1096
    label "stosownie"
  ]
  node [
    id 1097
    label "odpowiadanie"
  ]
  node [
    id 1098
    label "specjalny"
  ]
  node [
    id 1099
    label "_id"
  ]
  node [
    id 1100
    label "psychika"
  ]
  node [
    id 1101
    label "Freud"
  ]
  node [
    id 1102
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1103
    label "psychoanaliza"
  ]
  node [
    id 1104
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1105
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1106
    label "deformowa&#263;"
  ]
  node [
    id 1107
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1108
    label "ego"
  ]
  node [
    id 1109
    label "sfera_afektywna"
  ]
  node [
    id 1110
    label "deformowanie"
  ]
  node [
    id 1111
    label "kompleks"
  ]
  node [
    id 1112
    label "sumienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 211
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 203
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1070
  ]
  edge [
    source 13
    target 1071
  ]
  edge [
    source 13
    target 1072
  ]
  edge [
    source 13
    target 1073
  ]
  edge [
    source 13
    target 1074
  ]
  edge [
    source 13
    target 1075
  ]
  edge [
    source 13
    target 1076
  ]
  edge [
    source 13
    target 1077
  ]
  edge [
    source 13
    target 1078
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 425
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 15
    target 1099
  ]
  edge [
    source 15
    target 1100
  ]
  edge [
    source 15
    target 1101
  ]
  edge [
    source 15
    target 1102
  ]
  edge [
    source 15
    target 1103
  ]
  edge [
    source 15
    target 1104
  ]
  edge [
    source 15
    target 483
  ]
  edge [
    source 15
    target 1105
  ]
  edge [
    source 15
    target 1106
  ]
  edge [
    source 15
    target 1107
  ]
  edge [
    source 15
    target 448
  ]
  edge [
    source 15
    target 1108
  ]
  edge [
    source 15
    target 1109
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 1110
  ]
  edge [
    source 15
    target 1111
  ]
  edge [
    source 15
    target 1112
  ]
]
