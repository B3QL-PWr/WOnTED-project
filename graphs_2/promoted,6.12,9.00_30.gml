graph [
  node [
    id 0
    label "dana"
    origin "text"
  ]
  node [
    id 1
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 3
    label "badanie"
    origin "text"
  ]
  node [
    id 4
    label "anemia"
    origin "text"
  ]
  node [
    id 5
    label "sierpowaty"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "sprawdza&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "francja"
    origin "text"
  ]
  node [
    id 10
    label "prawie"
    origin "text"
  ]
  node [
    id 11
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 12
    label "dziecko"
    origin "text"
  ]
  node [
    id 13
    label "pochodzenie"
    origin "text"
  ]
  node [
    id 14
    label "pozaeuropejski"
    origin "text"
  ]
  node [
    id 15
    label "buddyzm"
  ]
  node [
    id 16
    label "cnota"
  ]
  node [
    id 17
    label "dar"
  ]
  node [
    id 18
    label "dyspozycja"
  ]
  node [
    id 19
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 20
    label "da&#324;"
  ]
  node [
    id 21
    label "faculty"
  ]
  node [
    id 22
    label "stygmat"
  ]
  node [
    id 23
    label "dobro"
  ]
  node [
    id 24
    label "rzecz"
  ]
  node [
    id 25
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 26
    label "dobro&#263;"
  ]
  node [
    id 27
    label "aretologia"
  ]
  node [
    id 28
    label "zaleta"
  ]
  node [
    id 29
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "honesty"
  ]
  node [
    id 32
    label "cecha"
  ]
  node [
    id 33
    label "panie&#324;stwo"
  ]
  node [
    id 34
    label "kalpa"
  ]
  node [
    id 35
    label "lampka_ma&#347;lana"
  ]
  node [
    id 36
    label "Buddhism"
  ]
  node [
    id 37
    label "mahajana"
  ]
  node [
    id 38
    label "asura"
  ]
  node [
    id 39
    label "wad&#378;rajana"
  ]
  node [
    id 40
    label "bonzo"
  ]
  node [
    id 41
    label "therawada"
  ]
  node [
    id 42
    label "tantryzm"
  ]
  node [
    id 43
    label "hinajana"
  ]
  node [
    id 44
    label "bardo"
  ]
  node [
    id 45
    label "maja"
  ]
  node [
    id 46
    label "arahant"
  ]
  node [
    id 47
    label "religia"
  ]
  node [
    id 48
    label "ahinsa"
  ]
  node [
    id 49
    label "li"
  ]
  node [
    id 50
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 51
    label "realize"
  ]
  node [
    id 52
    label "promocja"
  ]
  node [
    id 53
    label "zrobi&#263;"
  ]
  node [
    id 54
    label "make"
  ]
  node [
    id 55
    label "wytworzy&#263;"
  ]
  node [
    id 56
    label "give_birth"
  ]
  node [
    id 57
    label "post&#261;pi&#263;"
  ]
  node [
    id 58
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 59
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 60
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 61
    label "zorganizowa&#263;"
  ]
  node [
    id 62
    label "appoint"
  ]
  node [
    id 63
    label "wystylizowa&#263;"
  ]
  node [
    id 64
    label "cause"
  ]
  node [
    id 65
    label "przerobi&#263;"
  ]
  node [
    id 66
    label "nabra&#263;"
  ]
  node [
    id 67
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 68
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 69
    label "wydali&#263;"
  ]
  node [
    id 70
    label "manufacture"
  ]
  node [
    id 71
    label "damka"
  ]
  node [
    id 72
    label "warcaby"
  ]
  node [
    id 73
    label "promotion"
  ]
  node [
    id 74
    label "impreza"
  ]
  node [
    id 75
    label "sprzeda&#380;"
  ]
  node [
    id 76
    label "zamiana"
  ]
  node [
    id 77
    label "udzieli&#263;"
  ]
  node [
    id 78
    label "brief"
  ]
  node [
    id 79
    label "decyzja"
  ]
  node [
    id 80
    label "&#347;wiadectwo"
  ]
  node [
    id 81
    label "akcja"
  ]
  node [
    id 82
    label "bran&#380;a"
  ]
  node [
    id 83
    label "commencement"
  ]
  node [
    id 84
    label "okazja"
  ]
  node [
    id 85
    label "informacja"
  ]
  node [
    id 86
    label "klasa"
  ]
  node [
    id 87
    label "promowa&#263;"
  ]
  node [
    id 88
    label "graduacja"
  ]
  node [
    id 89
    label "nominacja"
  ]
  node [
    id 90
    label "szachy"
  ]
  node [
    id 91
    label "popularyzacja"
  ]
  node [
    id 92
    label "wypromowa&#263;"
  ]
  node [
    id 93
    label "gradation"
  ]
  node [
    id 94
    label "obserwowanie"
  ]
  node [
    id 95
    label "zrecenzowanie"
  ]
  node [
    id 96
    label "kontrola"
  ]
  node [
    id 97
    label "analysis"
  ]
  node [
    id 98
    label "rektalny"
  ]
  node [
    id 99
    label "ustalenie"
  ]
  node [
    id 100
    label "macanie"
  ]
  node [
    id 101
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 102
    label "usi&#322;owanie"
  ]
  node [
    id 103
    label "udowadnianie"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "bia&#322;a_niedziela"
  ]
  node [
    id 106
    label "diagnostyka"
  ]
  node [
    id 107
    label "dociekanie"
  ]
  node [
    id 108
    label "rezultat"
  ]
  node [
    id 109
    label "sprawdzanie"
  ]
  node [
    id 110
    label "penetrowanie"
  ]
  node [
    id 111
    label "czynno&#347;&#263;"
  ]
  node [
    id 112
    label "krytykowanie"
  ]
  node [
    id 113
    label "omawianie"
  ]
  node [
    id 114
    label "ustalanie"
  ]
  node [
    id 115
    label "rozpatrywanie"
  ]
  node [
    id 116
    label "investigation"
  ]
  node [
    id 117
    label "wziernikowanie"
  ]
  node [
    id 118
    label "examination"
  ]
  node [
    id 119
    label "discussion"
  ]
  node [
    id 120
    label "dyskutowanie"
  ]
  node [
    id 121
    label "temat"
  ]
  node [
    id 122
    label "czepianie_si&#281;"
  ]
  node [
    id 123
    label "opiniowanie"
  ]
  node [
    id 124
    label "ocenianie"
  ]
  node [
    id 125
    label "zaopiniowanie"
  ]
  node [
    id 126
    label "przeszukiwanie"
  ]
  node [
    id 127
    label "docieranie"
  ]
  node [
    id 128
    label "penetration"
  ]
  node [
    id 129
    label "umocnienie"
  ]
  node [
    id 130
    label "appointment"
  ]
  node [
    id 131
    label "spowodowanie"
  ]
  node [
    id 132
    label "localization"
  ]
  node [
    id 133
    label "zdecydowanie"
  ]
  node [
    id 134
    label "zrobienie"
  ]
  node [
    id 135
    label "colony"
  ]
  node [
    id 136
    label "powodowanie"
  ]
  node [
    id 137
    label "robienie"
  ]
  node [
    id 138
    label "colonization"
  ]
  node [
    id 139
    label "decydowanie"
  ]
  node [
    id 140
    label "umacnianie"
  ]
  node [
    id 141
    label "liquidation"
  ]
  node [
    id 142
    label "przemy&#347;liwanie"
  ]
  node [
    id 143
    label "dzia&#322;anie"
  ]
  node [
    id 144
    label "typ"
  ]
  node [
    id 145
    label "event"
  ]
  node [
    id 146
    label "przyczyna"
  ]
  node [
    id 147
    label "legalizacja_ponowna"
  ]
  node [
    id 148
    label "instytucja"
  ]
  node [
    id 149
    label "w&#322;adza"
  ]
  node [
    id 150
    label "perlustracja"
  ]
  node [
    id 151
    label "legalizacja_pierwotna"
  ]
  node [
    id 152
    label "activity"
  ]
  node [
    id 153
    label "bezproblemowy"
  ]
  node [
    id 154
    label "wydarzenie"
  ]
  node [
    id 155
    label "podejmowanie"
  ]
  node [
    id 156
    label "effort"
  ]
  node [
    id 157
    label "staranie_si&#281;"
  ]
  node [
    id 158
    label "essay"
  ]
  node [
    id 159
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 160
    label "redagowanie"
  ]
  node [
    id 161
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 162
    label "przymierzanie"
  ]
  node [
    id 163
    label "przymierzenie"
  ]
  node [
    id 164
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 165
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 166
    label "najem"
  ]
  node [
    id 167
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 168
    label "zak&#322;ad"
  ]
  node [
    id 169
    label "stosunek_pracy"
  ]
  node [
    id 170
    label "benedykty&#324;ski"
  ]
  node [
    id 171
    label "poda&#380;_pracy"
  ]
  node [
    id 172
    label "pracowanie"
  ]
  node [
    id 173
    label "tyrka"
  ]
  node [
    id 174
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 175
    label "wytw&#243;r"
  ]
  node [
    id 176
    label "miejsce"
  ]
  node [
    id 177
    label "zaw&#243;d"
  ]
  node [
    id 178
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 179
    label "tynkarski"
  ]
  node [
    id 180
    label "pracowa&#263;"
  ]
  node [
    id 181
    label "zmiana"
  ]
  node [
    id 182
    label "czynnik_produkcji"
  ]
  node [
    id 183
    label "zobowi&#261;zanie"
  ]
  node [
    id 184
    label "kierownictwo"
  ]
  node [
    id 185
    label "siedziba"
  ]
  node [
    id 186
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 187
    label "analizowanie"
  ]
  node [
    id 188
    label "uzasadnianie"
  ]
  node [
    id 189
    label "presentation"
  ]
  node [
    id 190
    label "pokazywanie"
  ]
  node [
    id 191
    label "endoscopy"
  ]
  node [
    id 192
    label "rozmy&#347;lanie"
  ]
  node [
    id 193
    label "quest"
  ]
  node [
    id 194
    label "dop&#322;ywanie"
  ]
  node [
    id 195
    label "examen"
  ]
  node [
    id 196
    label "diagnosis"
  ]
  node [
    id 197
    label "medycyna"
  ]
  node [
    id 198
    label "anamneza"
  ]
  node [
    id 199
    label "dotykanie"
  ]
  node [
    id 200
    label "dr&#243;b"
  ]
  node [
    id 201
    label "pomacanie"
  ]
  node [
    id 202
    label "feel"
  ]
  node [
    id 203
    label "palpation"
  ]
  node [
    id 204
    label "namacanie"
  ]
  node [
    id 205
    label "hodowanie"
  ]
  node [
    id 206
    label "patrzenie"
  ]
  node [
    id 207
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 208
    label "doszukanie_si&#281;"
  ]
  node [
    id 209
    label "dostrzeganie"
  ]
  node [
    id 210
    label "poobserwowanie"
  ]
  node [
    id 211
    label "observation"
  ]
  node [
    id 212
    label "bocianie_gniazdo"
  ]
  node [
    id 213
    label "schorzenie"
  ]
  node [
    id 214
    label "hipersplenizm"
  ]
  node [
    id 215
    label "ognisko"
  ]
  node [
    id 216
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 217
    label "powalenie"
  ]
  node [
    id 218
    label "odezwanie_si&#281;"
  ]
  node [
    id 219
    label "atakowanie"
  ]
  node [
    id 220
    label "grupa_ryzyka"
  ]
  node [
    id 221
    label "przypadek"
  ]
  node [
    id 222
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 223
    label "nabawienie_si&#281;"
  ]
  node [
    id 224
    label "inkubacja"
  ]
  node [
    id 225
    label "kryzys"
  ]
  node [
    id 226
    label "powali&#263;"
  ]
  node [
    id 227
    label "remisja"
  ]
  node [
    id 228
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 229
    label "zajmowa&#263;"
  ]
  node [
    id 230
    label "zaburzenie"
  ]
  node [
    id 231
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 232
    label "badanie_histopatologiczne"
  ]
  node [
    id 233
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 234
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 235
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 236
    label "odzywanie_si&#281;"
  ]
  node [
    id 237
    label "diagnoza"
  ]
  node [
    id 238
    label "atakowa&#263;"
  ]
  node [
    id 239
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 240
    label "nabawianie_si&#281;"
  ]
  node [
    id 241
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 242
    label "zajmowanie"
  ]
  node [
    id 243
    label "leukocytopenia"
  ]
  node [
    id 244
    label "ma&#322;op&#322;ytkowo&#347;&#263;"
  ]
  node [
    id 245
    label "sierpowato"
  ]
  node [
    id 246
    label "wygi&#281;ty"
  ]
  node [
    id 247
    label "skrzywienie_si&#281;"
  ]
  node [
    id 248
    label "zakrzywiony"
  ]
  node [
    id 249
    label "krzywienie"
  ]
  node [
    id 250
    label "krzywienie_si&#281;"
  ]
  node [
    id 251
    label "skrzywienie"
  ]
  node [
    id 252
    label "krzywo"
  ]
  node [
    id 253
    label "examine"
  ]
  node [
    id 254
    label "robi&#263;"
  ]
  node [
    id 255
    label "szpiegowa&#263;"
  ]
  node [
    id 256
    label "organizowa&#263;"
  ]
  node [
    id 257
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 258
    label "czyni&#263;"
  ]
  node [
    id 259
    label "give"
  ]
  node [
    id 260
    label "stylizowa&#263;"
  ]
  node [
    id 261
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 262
    label "falowa&#263;"
  ]
  node [
    id 263
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 264
    label "peddle"
  ]
  node [
    id 265
    label "wydala&#263;"
  ]
  node [
    id 266
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 267
    label "tentegowa&#263;"
  ]
  node [
    id 268
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 269
    label "urz&#261;dza&#263;"
  ]
  node [
    id 270
    label "oszukiwa&#263;"
  ]
  node [
    id 271
    label "work"
  ]
  node [
    id 272
    label "ukazywa&#263;"
  ]
  node [
    id 273
    label "przerabia&#263;"
  ]
  node [
    id 274
    label "act"
  ]
  node [
    id 275
    label "post&#281;powa&#263;"
  ]
  node [
    id 276
    label "chase"
  ]
  node [
    id 277
    label "wy&#322;&#261;czny"
  ]
  node [
    id 278
    label "w&#322;asny"
  ]
  node [
    id 279
    label "unikatowy"
  ]
  node [
    id 280
    label "jedyny"
  ]
  node [
    id 281
    label "utulenie"
  ]
  node [
    id 282
    label "pediatra"
  ]
  node [
    id 283
    label "dzieciak"
  ]
  node [
    id 284
    label "utulanie"
  ]
  node [
    id 285
    label "dzieciarnia"
  ]
  node [
    id 286
    label "cz&#322;owiek"
  ]
  node [
    id 287
    label "niepe&#322;noletni"
  ]
  node [
    id 288
    label "organizm"
  ]
  node [
    id 289
    label "utula&#263;"
  ]
  node [
    id 290
    label "cz&#322;owieczek"
  ]
  node [
    id 291
    label "fledgling"
  ]
  node [
    id 292
    label "zwierz&#281;"
  ]
  node [
    id 293
    label "utuli&#263;"
  ]
  node [
    id 294
    label "m&#322;odzik"
  ]
  node [
    id 295
    label "pedofil"
  ]
  node [
    id 296
    label "m&#322;odziak"
  ]
  node [
    id 297
    label "potomek"
  ]
  node [
    id 298
    label "entliczek-pentliczek"
  ]
  node [
    id 299
    label "potomstwo"
  ]
  node [
    id 300
    label "sraluch"
  ]
  node [
    id 301
    label "zbi&#243;r"
  ]
  node [
    id 302
    label "czeladka"
  ]
  node [
    id 303
    label "dzietno&#347;&#263;"
  ]
  node [
    id 304
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 305
    label "bawienie_si&#281;"
  ]
  node [
    id 306
    label "pomiot"
  ]
  node [
    id 307
    label "grupa"
  ]
  node [
    id 308
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 309
    label "kinderbal"
  ]
  node [
    id 310
    label "krewny"
  ]
  node [
    id 311
    label "ludzko&#347;&#263;"
  ]
  node [
    id 312
    label "asymilowanie"
  ]
  node [
    id 313
    label "wapniak"
  ]
  node [
    id 314
    label "asymilowa&#263;"
  ]
  node [
    id 315
    label "os&#322;abia&#263;"
  ]
  node [
    id 316
    label "posta&#263;"
  ]
  node [
    id 317
    label "hominid"
  ]
  node [
    id 318
    label "podw&#322;adny"
  ]
  node [
    id 319
    label "os&#322;abianie"
  ]
  node [
    id 320
    label "g&#322;owa"
  ]
  node [
    id 321
    label "figura"
  ]
  node [
    id 322
    label "portrecista"
  ]
  node [
    id 323
    label "dwun&#243;g"
  ]
  node [
    id 324
    label "profanum"
  ]
  node [
    id 325
    label "mikrokosmos"
  ]
  node [
    id 326
    label "nasada"
  ]
  node [
    id 327
    label "duch"
  ]
  node [
    id 328
    label "antropochoria"
  ]
  node [
    id 329
    label "osoba"
  ]
  node [
    id 330
    label "wz&#243;r"
  ]
  node [
    id 331
    label "senior"
  ]
  node [
    id 332
    label "oddzia&#322;ywanie"
  ]
  node [
    id 333
    label "Adam"
  ]
  node [
    id 334
    label "homo_sapiens"
  ]
  node [
    id 335
    label "polifag"
  ]
  node [
    id 336
    label "ma&#322;oletny"
  ]
  node [
    id 337
    label "m&#322;ody"
  ]
  node [
    id 338
    label "p&#322;aszczyzna"
  ]
  node [
    id 339
    label "odwadnia&#263;"
  ]
  node [
    id 340
    label "przyswoi&#263;"
  ]
  node [
    id 341
    label "sk&#243;ra"
  ]
  node [
    id 342
    label "odwodni&#263;"
  ]
  node [
    id 343
    label "ewoluowanie"
  ]
  node [
    id 344
    label "staw"
  ]
  node [
    id 345
    label "ow&#322;osienie"
  ]
  node [
    id 346
    label "unerwienie"
  ]
  node [
    id 347
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 348
    label "reakcja"
  ]
  node [
    id 349
    label "wyewoluowanie"
  ]
  node [
    id 350
    label "przyswajanie"
  ]
  node [
    id 351
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 352
    label "wyewoluowa&#263;"
  ]
  node [
    id 353
    label "biorytm"
  ]
  node [
    id 354
    label "ewoluowa&#263;"
  ]
  node [
    id 355
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 356
    label "istota_&#380;ywa"
  ]
  node [
    id 357
    label "otworzy&#263;"
  ]
  node [
    id 358
    label "otwiera&#263;"
  ]
  node [
    id 359
    label "czynnik_biotyczny"
  ]
  node [
    id 360
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 361
    label "otworzenie"
  ]
  node [
    id 362
    label "otwieranie"
  ]
  node [
    id 363
    label "individual"
  ]
  node [
    id 364
    label "ty&#322;"
  ]
  node [
    id 365
    label "szkielet"
  ]
  node [
    id 366
    label "obiekt"
  ]
  node [
    id 367
    label "przyswaja&#263;"
  ]
  node [
    id 368
    label "przyswojenie"
  ]
  node [
    id 369
    label "odwadnianie"
  ]
  node [
    id 370
    label "odwodnienie"
  ]
  node [
    id 371
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 372
    label "starzenie_si&#281;"
  ]
  node [
    id 373
    label "uk&#322;ad"
  ]
  node [
    id 374
    label "prz&#243;d"
  ]
  node [
    id 375
    label "temperatura"
  ]
  node [
    id 376
    label "l&#281;d&#378;wie"
  ]
  node [
    id 377
    label "cia&#322;o"
  ]
  node [
    id 378
    label "cz&#322;onek"
  ]
  node [
    id 379
    label "degenerat"
  ]
  node [
    id 380
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 381
    label "zwyrol"
  ]
  node [
    id 382
    label "czerniak"
  ]
  node [
    id 383
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 384
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 385
    label "paszcza"
  ]
  node [
    id 386
    label "popapraniec"
  ]
  node [
    id 387
    label "skuba&#263;"
  ]
  node [
    id 388
    label "skubanie"
  ]
  node [
    id 389
    label "skubni&#281;cie"
  ]
  node [
    id 390
    label "agresja"
  ]
  node [
    id 391
    label "zwierz&#281;ta"
  ]
  node [
    id 392
    label "fukni&#281;cie"
  ]
  node [
    id 393
    label "farba"
  ]
  node [
    id 394
    label "fukanie"
  ]
  node [
    id 395
    label "gad"
  ]
  node [
    id 396
    label "siedzie&#263;"
  ]
  node [
    id 397
    label "oswaja&#263;"
  ]
  node [
    id 398
    label "tresowa&#263;"
  ]
  node [
    id 399
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 400
    label "poligamia"
  ]
  node [
    id 401
    label "oz&#243;r"
  ]
  node [
    id 402
    label "skubn&#261;&#263;"
  ]
  node [
    id 403
    label "wios&#322;owa&#263;"
  ]
  node [
    id 404
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 405
    label "le&#380;enie"
  ]
  node [
    id 406
    label "niecz&#322;owiek"
  ]
  node [
    id 407
    label "wios&#322;owanie"
  ]
  node [
    id 408
    label "napasienie_si&#281;"
  ]
  node [
    id 409
    label "wiwarium"
  ]
  node [
    id 410
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 411
    label "animalista"
  ]
  node [
    id 412
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 413
    label "budowa"
  ]
  node [
    id 414
    label "hodowla"
  ]
  node [
    id 415
    label "pasienie_si&#281;"
  ]
  node [
    id 416
    label "sodomita"
  ]
  node [
    id 417
    label "monogamia"
  ]
  node [
    id 418
    label "przyssawka"
  ]
  node [
    id 419
    label "zachowanie"
  ]
  node [
    id 420
    label "budowa_cia&#322;a"
  ]
  node [
    id 421
    label "okrutnik"
  ]
  node [
    id 422
    label "grzbiet"
  ]
  node [
    id 423
    label "weterynarz"
  ]
  node [
    id 424
    label "&#322;eb"
  ]
  node [
    id 425
    label "wylinka"
  ]
  node [
    id 426
    label "bestia"
  ]
  node [
    id 427
    label "poskramia&#263;"
  ]
  node [
    id 428
    label "fauna"
  ]
  node [
    id 429
    label "treser"
  ]
  node [
    id 430
    label "siedzenie"
  ]
  node [
    id 431
    label "le&#380;e&#263;"
  ]
  node [
    id 432
    label "uspokojenie"
  ]
  node [
    id 433
    label "utulenie_si&#281;"
  ]
  node [
    id 434
    label "u&#347;pienie"
  ]
  node [
    id 435
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 436
    label "uspokoi&#263;"
  ]
  node [
    id 437
    label "utulanie_si&#281;"
  ]
  node [
    id 438
    label "usypianie"
  ]
  node [
    id 439
    label "pocieszanie"
  ]
  node [
    id 440
    label "uspokajanie"
  ]
  node [
    id 441
    label "usypia&#263;"
  ]
  node [
    id 442
    label "uspokaja&#263;"
  ]
  node [
    id 443
    label "wyliczanka"
  ]
  node [
    id 444
    label "specjalista"
  ]
  node [
    id 445
    label "harcerz"
  ]
  node [
    id 446
    label "ch&#322;opta&#347;"
  ]
  node [
    id 447
    label "zawodnik"
  ]
  node [
    id 448
    label "go&#322;ow&#261;s"
  ]
  node [
    id 449
    label "m&#322;ode"
  ]
  node [
    id 450
    label "stopie&#324;_harcerski"
  ]
  node [
    id 451
    label "g&#243;wniarz"
  ]
  node [
    id 452
    label "beniaminek"
  ]
  node [
    id 453
    label "dewiant"
  ]
  node [
    id 454
    label "istotka"
  ]
  node [
    id 455
    label "bech"
  ]
  node [
    id 456
    label "dziecinny"
  ]
  node [
    id 457
    label "naiwniak"
  ]
  node [
    id 458
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 459
    label "zaczynanie_si&#281;"
  ]
  node [
    id 460
    label "str&#243;j"
  ]
  node [
    id 461
    label "wynikanie"
  ]
  node [
    id 462
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 463
    label "origin"
  ]
  node [
    id 464
    label "background"
  ]
  node [
    id 465
    label "czas"
  ]
  node [
    id 466
    label "geneza"
  ]
  node [
    id 467
    label "beginning"
  ]
  node [
    id 468
    label "okazywanie_si&#281;"
  ]
  node [
    id 469
    label "powstawanie"
  ]
  node [
    id 470
    label "implication"
  ]
  node [
    id 471
    label "flux"
  ]
  node [
    id 472
    label "czynnik"
  ]
  node [
    id 473
    label "proces"
  ]
  node [
    id 474
    label "zesp&#243;&#322;"
  ]
  node [
    id 475
    label "rodny"
  ]
  node [
    id 476
    label "powstanie"
  ]
  node [
    id 477
    label "monogeneza"
  ]
  node [
    id 478
    label "zaistnienie"
  ]
  node [
    id 479
    label "pocz&#261;tek"
  ]
  node [
    id 480
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 481
    label "poprzedzanie"
  ]
  node [
    id 482
    label "czasoprzestrze&#324;"
  ]
  node [
    id 483
    label "laba"
  ]
  node [
    id 484
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 485
    label "chronometria"
  ]
  node [
    id 486
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 487
    label "rachuba_czasu"
  ]
  node [
    id 488
    label "przep&#322;ywanie"
  ]
  node [
    id 489
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 490
    label "czasokres"
  ]
  node [
    id 491
    label "odczyt"
  ]
  node [
    id 492
    label "chwila"
  ]
  node [
    id 493
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 494
    label "dzieje"
  ]
  node [
    id 495
    label "kategoria_gramatyczna"
  ]
  node [
    id 496
    label "poprzedzenie"
  ]
  node [
    id 497
    label "trawienie"
  ]
  node [
    id 498
    label "pochodzi&#263;"
  ]
  node [
    id 499
    label "period"
  ]
  node [
    id 500
    label "okres_czasu"
  ]
  node [
    id 501
    label "poprzedza&#263;"
  ]
  node [
    id 502
    label "schy&#322;ek"
  ]
  node [
    id 503
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 504
    label "odwlekanie_si&#281;"
  ]
  node [
    id 505
    label "zegar"
  ]
  node [
    id 506
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 507
    label "czwarty_wymiar"
  ]
  node [
    id 508
    label "koniugacja"
  ]
  node [
    id 509
    label "Zeitgeist"
  ]
  node [
    id 510
    label "trawi&#263;"
  ]
  node [
    id 511
    label "pogoda"
  ]
  node [
    id 512
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 513
    label "poprzedzi&#263;"
  ]
  node [
    id 514
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 515
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 516
    label "time_period"
  ]
  node [
    id 517
    label "r&#243;&#380;norodno&#347;&#263;"
  ]
  node [
    id 518
    label "gorset"
  ]
  node [
    id 519
    label "zrzucenie"
  ]
  node [
    id 520
    label "znoszenie"
  ]
  node [
    id 521
    label "kr&#243;j"
  ]
  node [
    id 522
    label "struktura"
  ]
  node [
    id 523
    label "ubranie_si&#281;"
  ]
  node [
    id 524
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 525
    label "znosi&#263;"
  ]
  node [
    id 526
    label "zrzuci&#263;"
  ]
  node [
    id 527
    label "pasmanteria"
  ]
  node [
    id 528
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 529
    label "odzie&#380;"
  ]
  node [
    id 530
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 531
    label "wyko&#324;czenie"
  ]
  node [
    id 532
    label "nosi&#263;"
  ]
  node [
    id 533
    label "zasada"
  ]
  node [
    id 534
    label "w&#322;o&#380;enie"
  ]
  node [
    id 535
    label "garderoba"
  ]
  node [
    id 536
    label "odziewek"
  ]
  node [
    id 537
    label "pocz&#261;tki"
  ]
  node [
    id 538
    label "kontekst"
  ]
  node [
    id 539
    label "nieeuropejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 12
    target 283
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 290
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 14
    target 539
  ]
]
