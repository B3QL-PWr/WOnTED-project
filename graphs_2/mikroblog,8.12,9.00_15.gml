graph [
  node [
    id 0
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "moja"
    origin "text"
  ]
  node [
    id 2
    label "stracona"
    origin "text"
  ]
  node [
    id 3
    label "marzenia"
    origin "text"
  ]
  node [
    id 4
    label "naiwny"
    origin "text"
  ]
  node [
    id 5
    label "czas"
    origin "text"
  ]
  node [
    id 6
    label "naiwnie"
  ]
  node [
    id 7
    label "g&#322;upi"
  ]
  node [
    id 8
    label "poczciwy"
  ]
  node [
    id 9
    label "prostoduszny"
  ]
  node [
    id 10
    label "g&#322;upienie"
  ]
  node [
    id 11
    label "uprzykrzony"
  ]
  node [
    id 12
    label "istota_&#380;ywa"
  ]
  node [
    id 13
    label "niem&#261;dry"
  ]
  node [
    id 14
    label "bezcelowy"
  ]
  node [
    id 15
    label "ma&#322;y"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "g&#322;uptas"
  ]
  node [
    id 18
    label "mondzio&#322;"
  ]
  node [
    id 19
    label "nierozwa&#380;ny"
  ]
  node [
    id 20
    label "bezmy&#347;lny"
  ]
  node [
    id 21
    label "bezsensowny"
  ]
  node [
    id 22
    label "bezwolny"
  ]
  node [
    id 23
    label "&#347;mieszny"
  ]
  node [
    id 24
    label "nadaremny"
  ]
  node [
    id 25
    label "g&#322;upiec"
  ]
  node [
    id 26
    label "g&#322;upio"
  ]
  node [
    id 27
    label "niezr&#281;czny"
  ]
  node [
    id 28
    label "niewa&#380;ny"
  ]
  node [
    id 29
    label "zg&#322;upienie"
  ]
  node [
    id 30
    label "prostodusznie"
  ]
  node [
    id 31
    label "szczery"
  ]
  node [
    id 32
    label "sympatyczny"
  ]
  node [
    id 33
    label "prosty"
  ]
  node [
    id 34
    label "poczciwie"
  ]
  node [
    id 35
    label "dobry"
  ]
  node [
    id 36
    label "chronometria"
  ]
  node [
    id 37
    label "odczyt"
  ]
  node [
    id 38
    label "laba"
  ]
  node [
    id 39
    label "czasoprzestrze&#324;"
  ]
  node [
    id 40
    label "time_period"
  ]
  node [
    id 41
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 42
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 43
    label "Zeitgeist"
  ]
  node [
    id 44
    label "pochodzenie"
  ]
  node [
    id 45
    label "przep&#322;ywanie"
  ]
  node [
    id 46
    label "schy&#322;ek"
  ]
  node [
    id 47
    label "czwarty_wymiar"
  ]
  node [
    id 48
    label "kategoria_gramatyczna"
  ]
  node [
    id 49
    label "poprzedzi&#263;"
  ]
  node [
    id 50
    label "pogoda"
  ]
  node [
    id 51
    label "czasokres"
  ]
  node [
    id 52
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 53
    label "poprzedzenie"
  ]
  node [
    id 54
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 55
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 56
    label "dzieje"
  ]
  node [
    id 57
    label "zegar"
  ]
  node [
    id 58
    label "koniugacja"
  ]
  node [
    id 59
    label "trawi&#263;"
  ]
  node [
    id 60
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 61
    label "poprzedza&#263;"
  ]
  node [
    id 62
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 63
    label "trawienie"
  ]
  node [
    id 64
    label "chwila"
  ]
  node [
    id 65
    label "rachuba_czasu"
  ]
  node [
    id 66
    label "poprzedzanie"
  ]
  node [
    id 67
    label "okres_czasu"
  ]
  node [
    id 68
    label "period"
  ]
  node [
    id 69
    label "odwlekanie_si&#281;"
  ]
  node [
    id 70
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 71
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 72
    label "pochodzi&#263;"
  ]
  node [
    id 73
    label "time"
  ]
  node [
    id 74
    label "blok"
  ]
  node [
    id 75
    label "reading"
  ]
  node [
    id 76
    label "handout"
  ]
  node [
    id 77
    label "podawanie"
  ]
  node [
    id 78
    label "wyk&#322;ad"
  ]
  node [
    id 79
    label "lecture"
  ]
  node [
    id 80
    label "pomiar"
  ]
  node [
    id 81
    label "meteorology"
  ]
  node [
    id 82
    label "warunki"
  ]
  node [
    id 83
    label "weather"
  ]
  node [
    id 84
    label "zjawisko"
  ]
  node [
    id 85
    label "pok&#243;j"
  ]
  node [
    id 86
    label "atak"
  ]
  node [
    id 87
    label "prognoza_meteorologiczna"
  ]
  node [
    id 88
    label "potrzyma&#263;"
  ]
  node [
    id 89
    label "program"
  ]
  node [
    id 90
    label "czas_wolny"
  ]
  node [
    id 91
    label "metrologia"
  ]
  node [
    id 92
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 93
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 94
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 95
    label "czasomierz"
  ]
  node [
    id 96
    label "tyka&#263;"
  ]
  node [
    id 97
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 98
    label "tykn&#261;&#263;"
  ]
  node [
    id 99
    label "nabicie"
  ]
  node [
    id 100
    label "bicie"
  ]
  node [
    id 101
    label "kotwica"
  ]
  node [
    id 102
    label "godzinnik"
  ]
  node [
    id 103
    label "werk"
  ]
  node [
    id 104
    label "urz&#261;dzenie"
  ]
  node [
    id 105
    label "wahad&#322;o"
  ]
  node [
    id 106
    label "kurant"
  ]
  node [
    id 107
    label "cyferblat"
  ]
  node [
    id 108
    label "liczba"
  ]
  node [
    id 109
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 110
    label "osoba"
  ]
  node [
    id 111
    label "czasownik"
  ]
  node [
    id 112
    label "tryb"
  ]
  node [
    id 113
    label "coupling"
  ]
  node [
    id 114
    label "fleksja"
  ]
  node [
    id 115
    label "orz&#281;sek"
  ]
  node [
    id 116
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 117
    label "background"
  ]
  node [
    id 118
    label "str&#243;j"
  ]
  node [
    id 119
    label "wynikanie"
  ]
  node [
    id 120
    label "origin"
  ]
  node [
    id 121
    label "zaczynanie_si&#281;"
  ]
  node [
    id 122
    label "beginning"
  ]
  node [
    id 123
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 124
    label "geneza"
  ]
  node [
    id 125
    label "marnowanie"
  ]
  node [
    id 126
    label "unicestwianie"
  ]
  node [
    id 127
    label "sp&#281;dzanie"
  ]
  node [
    id 128
    label "digestion"
  ]
  node [
    id 129
    label "perystaltyka"
  ]
  node [
    id 130
    label "proces_fizjologiczny"
  ]
  node [
    id 131
    label "rozk&#322;adanie"
  ]
  node [
    id 132
    label "przetrawianie"
  ]
  node [
    id 133
    label "contemplation"
  ]
  node [
    id 134
    label "proceed"
  ]
  node [
    id 135
    label "pour"
  ]
  node [
    id 136
    label "mija&#263;"
  ]
  node [
    id 137
    label "sail"
  ]
  node [
    id 138
    label "przebywa&#263;"
  ]
  node [
    id 139
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 140
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 141
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 142
    label "carry"
  ]
  node [
    id 143
    label "go&#347;ci&#263;"
  ]
  node [
    id 144
    label "zanikni&#281;cie"
  ]
  node [
    id 145
    label "departure"
  ]
  node [
    id 146
    label "odej&#347;cie"
  ]
  node [
    id 147
    label "opuszczenie"
  ]
  node [
    id 148
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 149
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 150
    label "ciecz"
  ]
  node [
    id 151
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 152
    label "oddalenie_si&#281;"
  ]
  node [
    id 153
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 154
    label "cross"
  ]
  node [
    id 155
    label "swimming"
  ]
  node [
    id 156
    label "min&#261;&#263;"
  ]
  node [
    id 157
    label "przeby&#263;"
  ]
  node [
    id 158
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 159
    label "zago&#347;ci&#263;"
  ]
  node [
    id 160
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 161
    label "overwhelm"
  ]
  node [
    id 162
    label "zrobi&#263;"
  ]
  node [
    id 163
    label "opatrzy&#263;"
  ]
  node [
    id 164
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 165
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 166
    label "opatrywa&#263;"
  ]
  node [
    id 167
    label "poby&#263;"
  ]
  node [
    id 168
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 169
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 170
    label "bolt"
  ]
  node [
    id 171
    label "uda&#263;_si&#281;"
  ]
  node [
    id 172
    label "date"
  ]
  node [
    id 173
    label "fall"
  ]
  node [
    id 174
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 175
    label "spowodowa&#263;"
  ]
  node [
    id 176
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 177
    label "wynika&#263;"
  ]
  node [
    id 178
    label "zdarzenie_si&#281;"
  ]
  node [
    id 179
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 180
    label "progress"
  ]
  node [
    id 181
    label "opatrzenie"
  ]
  node [
    id 182
    label "opatrywanie"
  ]
  node [
    id 183
    label "przebycie"
  ]
  node [
    id 184
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 185
    label "mini&#281;cie"
  ]
  node [
    id 186
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 187
    label "zaistnienie"
  ]
  node [
    id 188
    label "doznanie"
  ]
  node [
    id 189
    label "cruise"
  ]
  node [
    id 190
    label "lutowa&#263;"
  ]
  node [
    id 191
    label "metal"
  ]
  node [
    id 192
    label "przetrawia&#263;"
  ]
  node [
    id 193
    label "poch&#322;ania&#263;"
  ]
  node [
    id 194
    label "marnowa&#263;"
  ]
  node [
    id 195
    label "digest"
  ]
  node [
    id 196
    label "usuwa&#263;"
  ]
  node [
    id 197
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 198
    label "sp&#281;dza&#263;"
  ]
  node [
    id 199
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 200
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 201
    label "zjawianie_si&#281;"
  ]
  node [
    id 202
    label "mijanie"
  ]
  node [
    id 203
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 204
    label "przebywanie"
  ]
  node [
    id 205
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 206
    label "flux"
  ]
  node [
    id 207
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 208
    label "zaznawanie"
  ]
  node [
    id 209
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 210
    label "charakter"
  ]
  node [
    id 211
    label "epoka"
  ]
  node [
    id 212
    label "ciota"
  ]
  node [
    id 213
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 214
    label "flow"
  ]
  node [
    id 215
    label "choroba_przyrodzona"
  ]
  node [
    id 216
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 217
    label "kres"
  ]
  node [
    id 218
    label "przestrze&#324;"
  ]
  node [
    id 219
    label "przesz&#322;o&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
]
