graph [
  node [
    id 0
    label "u&#380;ytek"
    origin "text"
  ]
  node [
    id 1
    label "niniejszy"
    origin "text"
  ]
  node [
    id 2
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 3
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 6
    label "definicja"
    origin "text"
  ]
  node [
    id 7
    label "kopia"
  ]
  node [
    id 8
    label "function"
  ]
  node [
    id 9
    label "cel"
  ]
  node [
    id 10
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 11
    label "odbitka"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "miniatura"
  ]
  node [
    id 14
    label "extra"
  ]
  node [
    id 15
    label "chor&#261;giew"
  ]
  node [
    id 16
    label "formacja"
  ]
  node [
    id 17
    label "wytw&#243;r"
  ]
  node [
    id 18
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 19
    label "egzemplarz"
  ]
  node [
    id 20
    label "picture"
  ]
  node [
    id 21
    label "rzecz"
  ]
  node [
    id 22
    label "punkt"
  ]
  node [
    id 23
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 24
    label "thing"
  ]
  node [
    id 25
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 26
    label "rezultat"
  ]
  node [
    id 27
    label "miejsce"
  ]
  node [
    id 28
    label "ten"
  ]
  node [
    id 29
    label "okre&#347;lony"
  ]
  node [
    id 30
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 31
    label "akt"
  ]
  node [
    id 32
    label "ordonans"
  ]
  node [
    id 33
    label "zarz&#261;dzenie"
  ]
  node [
    id 34
    label "rule"
  ]
  node [
    id 35
    label "polecenie"
  ]
  node [
    id 36
    label "arrangement"
  ]
  node [
    id 37
    label "commission"
  ]
  node [
    id 38
    label "stipulation"
  ]
  node [
    id 39
    label "danie"
  ]
  node [
    id 40
    label "poj&#281;cie"
  ]
  node [
    id 41
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 42
    label "erotyka"
  ]
  node [
    id 43
    label "fragment"
  ]
  node [
    id 44
    label "podniecanie"
  ]
  node [
    id 45
    label "po&#380;ycie"
  ]
  node [
    id 46
    label "dokument"
  ]
  node [
    id 47
    label "baraszki"
  ]
  node [
    id 48
    label "numer"
  ]
  node [
    id 49
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 50
    label "certificate"
  ]
  node [
    id 51
    label "ruch_frykcyjny"
  ]
  node [
    id 52
    label "wydarzenie"
  ]
  node [
    id 53
    label "ontologia"
  ]
  node [
    id 54
    label "wzw&#243;d"
  ]
  node [
    id 55
    label "czynno&#347;&#263;"
  ]
  node [
    id 56
    label "scena"
  ]
  node [
    id 57
    label "seks"
  ]
  node [
    id 58
    label "pozycja_misjonarska"
  ]
  node [
    id 59
    label "rozmna&#380;anie"
  ]
  node [
    id 60
    label "arystotelizm"
  ]
  node [
    id 61
    label "zwyczaj"
  ]
  node [
    id 62
    label "urzeczywistnienie"
  ]
  node [
    id 63
    label "z&#322;&#261;czenie"
  ]
  node [
    id 64
    label "funkcja"
  ]
  node [
    id 65
    label "act"
  ]
  node [
    id 66
    label "imisja"
  ]
  node [
    id 67
    label "podniecenie"
  ]
  node [
    id 68
    label "podnieca&#263;"
  ]
  node [
    id 69
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 70
    label "fascyku&#322;"
  ]
  node [
    id 71
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 72
    label "nago&#347;&#263;"
  ]
  node [
    id 73
    label "gra_wst&#281;pna"
  ]
  node [
    id 74
    label "po&#380;&#261;danie"
  ]
  node [
    id 75
    label "podnieci&#263;"
  ]
  node [
    id 76
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 77
    label "na_pieska"
  ]
  node [
    id 78
    label "consign"
  ]
  node [
    id 79
    label "pognanie"
  ]
  node [
    id 80
    label "recommendation"
  ]
  node [
    id 81
    label "pobiegni&#281;cie"
  ]
  node [
    id 82
    label "powierzenie"
  ]
  node [
    id 83
    label "doradzenie"
  ]
  node [
    id 84
    label "education"
  ]
  node [
    id 85
    label "wypowied&#378;"
  ]
  node [
    id 86
    label "zaordynowanie"
  ]
  node [
    id 87
    label "przesadzenie"
  ]
  node [
    id 88
    label "ukaz"
  ]
  node [
    id 89
    label "rekomendacja"
  ]
  node [
    id 90
    label "statement"
  ]
  node [
    id 91
    label "zadanie"
  ]
  node [
    id 92
    label "dekret"
  ]
  node [
    id 93
    label "u&#380;ywa&#263;"
  ]
  node [
    id 94
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 95
    label "korzysta&#263;"
  ]
  node [
    id 96
    label "doznawa&#263;"
  ]
  node [
    id 97
    label "distribute"
  ]
  node [
    id 98
    label "give"
  ]
  node [
    id 99
    label "bash"
  ]
  node [
    id 100
    label "wiadomy"
  ]
  node [
    id 101
    label "definition"
  ]
  node [
    id 102
    label "obja&#347;nienie"
  ]
  node [
    id 103
    label "definiens"
  ]
  node [
    id 104
    label "definiendum"
  ]
  node [
    id 105
    label "zrozumia&#322;y"
  ]
  node [
    id 106
    label "remark"
  ]
  node [
    id 107
    label "report"
  ]
  node [
    id 108
    label "przedstawienie"
  ]
  node [
    id 109
    label "poinformowanie"
  ]
  node [
    id 110
    label "informacja"
  ]
  node [
    id 111
    label "explanation"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
]
