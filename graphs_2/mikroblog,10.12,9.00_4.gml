graph [
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "serdecznie"
    origin "text"
  ]
  node [
    id 2
    label "nasi"
    origin "text"
  ]
  node [
    id 3
    label "bohater"
    origin "text"
  ]
  node [
    id 4
    label "bez"
    origin "text"
  ]
  node [
    id 5
    label "peleryna"
    origin "text"
  ]
  node [
    id 6
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "marta"
    origin "text"
  ]
  node [
    id 9
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 10
    label "zbi&#243;rka"
    origin "text"
  ]
  node [
    id 11
    label "https"
    origin "text"
  ]
  node [
    id 12
    label "greet"
  ]
  node [
    id 13
    label "welcome"
  ]
  node [
    id 14
    label "pozdrawia&#263;"
  ]
  node [
    id 15
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 16
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 17
    label "robi&#263;"
  ]
  node [
    id 18
    label "obchodzi&#263;"
  ]
  node [
    id 19
    label "bless"
  ]
  node [
    id 20
    label "szczerze"
  ]
  node [
    id 21
    label "serdeczny"
  ]
  node [
    id 22
    label "mi&#322;o"
  ]
  node [
    id 23
    label "siarczy&#347;cie"
  ]
  node [
    id 24
    label "przyja&#378;nie"
  ]
  node [
    id 25
    label "przychylnie"
  ]
  node [
    id 26
    label "mi&#322;y"
  ]
  node [
    id 27
    label "dobrze"
  ]
  node [
    id 28
    label "pleasantly"
  ]
  node [
    id 29
    label "deliciously"
  ]
  node [
    id 30
    label "przyjemny"
  ]
  node [
    id 31
    label "gratifyingly"
  ]
  node [
    id 32
    label "szczery"
  ]
  node [
    id 33
    label "s&#322;usznie"
  ]
  node [
    id 34
    label "szczero"
  ]
  node [
    id 35
    label "hojnie"
  ]
  node [
    id 36
    label "honestly"
  ]
  node [
    id 37
    label "przekonuj&#261;co"
  ]
  node [
    id 38
    label "outspokenly"
  ]
  node [
    id 39
    label "artlessly"
  ]
  node [
    id 40
    label "uczciwie"
  ]
  node [
    id 41
    label "bluffly"
  ]
  node [
    id 42
    label "dosadnie"
  ]
  node [
    id 43
    label "silnie"
  ]
  node [
    id 44
    label "siarczysty"
  ]
  node [
    id 45
    label "drogi"
  ]
  node [
    id 46
    label "ciep&#322;y"
  ]
  node [
    id 47
    label "&#380;yczliwy"
  ]
  node [
    id 48
    label "Messi"
  ]
  node [
    id 49
    label "Herkules_Poirot"
  ]
  node [
    id 50
    label "cz&#322;owiek"
  ]
  node [
    id 51
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 52
    label "Achilles"
  ]
  node [
    id 53
    label "Harry_Potter"
  ]
  node [
    id 54
    label "Casanova"
  ]
  node [
    id 55
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 56
    label "Zgredek"
  ]
  node [
    id 57
    label "Asterix"
  ]
  node [
    id 58
    label "Winnetou"
  ]
  node [
    id 59
    label "uczestnik"
  ]
  node [
    id 60
    label "posta&#263;"
  ]
  node [
    id 61
    label "&#347;mia&#322;ek"
  ]
  node [
    id 62
    label "Mario"
  ]
  node [
    id 63
    label "Borewicz"
  ]
  node [
    id 64
    label "Quasimodo"
  ]
  node [
    id 65
    label "Sherlock_Holmes"
  ]
  node [
    id 66
    label "Wallenrod"
  ]
  node [
    id 67
    label "Herkules"
  ]
  node [
    id 68
    label "bohaterski"
  ]
  node [
    id 69
    label "Don_Juan"
  ]
  node [
    id 70
    label "podmiot"
  ]
  node [
    id 71
    label "Don_Kiszot"
  ]
  node [
    id 72
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 73
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 74
    label "Hamlet"
  ]
  node [
    id 75
    label "Werter"
  ]
  node [
    id 76
    label "Szwejk"
  ]
  node [
    id 77
    label "charakterystyka"
  ]
  node [
    id 78
    label "zaistnie&#263;"
  ]
  node [
    id 79
    label "cecha"
  ]
  node [
    id 80
    label "Osjan"
  ]
  node [
    id 81
    label "kto&#347;"
  ]
  node [
    id 82
    label "wygl&#261;d"
  ]
  node [
    id 83
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 84
    label "osobowo&#347;&#263;"
  ]
  node [
    id 85
    label "wytw&#243;r"
  ]
  node [
    id 86
    label "trim"
  ]
  node [
    id 87
    label "poby&#263;"
  ]
  node [
    id 88
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 89
    label "Aspazja"
  ]
  node [
    id 90
    label "punkt_widzenia"
  ]
  node [
    id 91
    label "kompleksja"
  ]
  node [
    id 92
    label "wytrzyma&#263;"
  ]
  node [
    id 93
    label "budowa"
  ]
  node [
    id 94
    label "formacja"
  ]
  node [
    id 95
    label "pozosta&#263;"
  ]
  node [
    id 96
    label "point"
  ]
  node [
    id 97
    label "przedstawienie"
  ]
  node [
    id 98
    label "go&#347;&#263;"
  ]
  node [
    id 99
    label "ludzko&#347;&#263;"
  ]
  node [
    id 100
    label "asymilowanie"
  ]
  node [
    id 101
    label "wapniak"
  ]
  node [
    id 102
    label "asymilowa&#263;"
  ]
  node [
    id 103
    label "os&#322;abia&#263;"
  ]
  node [
    id 104
    label "hominid"
  ]
  node [
    id 105
    label "podw&#322;adny"
  ]
  node [
    id 106
    label "os&#322;abianie"
  ]
  node [
    id 107
    label "g&#322;owa"
  ]
  node [
    id 108
    label "figura"
  ]
  node [
    id 109
    label "portrecista"
  ]
  node [
    id 110
    label "dwun&#243;g"
  ]
  node [
    id 111
    label "profanum"
  ]
  node [
    id 112
    label "mikrokosmos"
  ]
  node [
    id 113
    label "nasada"
  ]
  node [
    id 114
    label "duch"
  ]
  node [
    id 115
    label "antropochoria"
  ]
  node [
    id 116
    label "osoba"
  ]
  node [
    id 117
    label "wz&#243;r"
  ]
  node [
    id 118
    label "senior"
  ]
  node [
    id 119
    label "oddzia&#322;ywanie"
  ]
  node [
    id 120
    label "Adam"
  ]
  node [
    id 121
    label "homo_sapiens"
  ]
  node [
    id 122
    label "polifag"
  ]
  node [
    id 123
    label "zuch"
  ]
  node [
    id 124
    label "rycerzyk"
  ]
  node [
    id 125
    label "odwa&#380;ny"
  ]
  node [
    id 126
    label "ryzykant"
  ]
  node [
    id 127
    label "morowiec"
  ]
  node [
    id 128
    label "trawa"
  ]
  node [
    id 129
    label "ro&#347;lina"
  ]
  node [
    id 130
    label "twardziel"
  ]
  node [
    id 131
    label "owsowe"
  ]
  node [
    id 132
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 133
    label "byt"
  ]
  node [
    id 134
    label "organizacja"
  ]
  node [
    id 135
    label "prawo"
  ]
  node [
    id 136
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 137
    label "nauka_prawa"
  ]
  node [
    id 138
    label "Szekspir"
  ]
  node [
    id 139
    label "Mickiewicz"
  ]
  node [
    id 140
    label "cierpienie"
  ]
  node [
    id 141
    label "Atomowa_Pch&#322;a"
  ]
  node [
    id 142
    label "bohatersko"
  ]
  node [
    id 143
    label "dzielny"
  ]
  node [
    id 144
    label "silny"
  ]
  node [
    id 145
    label "waleczny"
  ]
  node [
    id 146
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 147
    label "krzew"
  ]
  node [
    id 148
    label "delfinidyna"
  ]
  node [
    id 149
    label "pi&#380;maczkowate"
  ]
  node [
    id 150
    label "ki&#347;&#263;"
  ]
  node [
    id 151
    label "hy&#263;ka"
  ]
  node [
    id 152
    label "pestkowiec"
  ]
  node [
    id 153
    label "kwiat"
  ]
  node [
    id 154
    label "owoc"
  ]
  node [
    id 155
    label "oliwkowate"
  ]
  node [
    id 156
    label "lilac"
  ]
  node [
    id 157
    label "flakon"
  ]
  node [
    id 158
    label "przykoronek"
  ]
  node [
    id 159
    label "kielich"
  ]
  node [
    id 160
    label "dno_kwiatowe"
  ]
  node [
    id 161
    label "organ_ro&#347;linny"
  ]
  node [
    id 162
    label "ogon"
  ]
  node [
    id 163
    label "warga"
  ]
  node [
    id 164
    label "korona"
  ]
  node [
    id 165
    label "rurka"
  ]
  node [
    id 166
    label "ozdoba"
  ]
  node [
    id 167
    label "kostka"
  ]
  node [
    id 168
    label "kita"
  ]
  node [
    id 169
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 170
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 171
    label "d&#322;o&#324;"
  ]
  node [
    id 172
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 173
    label "powerball"
  ]
  node [
    id 174
    label "&#380;ubr"
  ]
  node [
    id 175
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 176
    label "p&#281;k"
  ]
  node [
    id 177
    label "r&#281;ka"
  ]
  node [
    id 178
    label "zako&#324;czenie"
  ]
  node [
    id 179
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 180
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 181
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 182
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 183
    label "&#322;yko"
  ]
  node [
    id 184
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 185
    label "karczowa&#263;"
  ]
  node [
    id 186
    label "wykarczowanie"
  ]
  node [
    id 187
    label "skupina"
  ]
  node [
    id 188
    label "wykarczowa&#263;"
  ]
  node [
    id 189
    label "karczowanie"
  ]
  node [
    id 190
    label "fanerofit"
  ]
  node [
    id 191
    label "zbiorowisko"
  ]
  node [
    id 192
    label "ro&#347;liny"
  ]
  node [
    id 193
    label "p&#281;d"
  ]
  node [
    id 194
    label "wegetowanie"
  ]
  node [
    id 195
    label "zadziorek"
  ]
  node [
    id 196
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 197
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 198
    label "do&#322;owa&#263;"
  ]
  node [
    id 199
    label "wegetacja"
  ]
  node [
    id 200
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 201
    label "strzyc"
  ]
  node [
    id 202
    label "w&#322;&#243;kno"
  ]
  node [
    id 203
    label "g&#322;uszenie"
  ]
  node [
    id 204
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 205
    label "fitotron"
  ]
  node [
    id 206
    label "bulwka"
  ]
  node [
    id 207
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 208
    label "odn&#243;&#380;ka"
  ]
  node [
    id 209
    label "epiderma"
  ]
  node [
    id 210
    label "gumoza"
  ]
  node [
    id 211
    label "strzy&#380;enie"
  ]
  node [
    id 212
    label "wypotnik"
  ]
  node [
    id 213
    label "flawonoid"
  ]
  node [
    id 214
    label "wyro&#347;le"
  ]
  node [
    id 215
    label "do&#322;owanie"
  ]
  node [
    id 216
    label "g&#322;uszy&#263;"
  ]
  node [
    id 217
    label "pora&#380;a&#263;"
  ]
  node [
    id 218
    label "fitocenoza"
  ]
  node [
    id 219
    label "hodowla"
  ]
  node [
    id 220
    label "fotoautotrof"
  ]
  node [
    id 221
    label "nieuleczalnie_chory"
  ]
  node [
    id 222
    label "wegetowa&#263;"
  ]
  node [
    id 223
    label "pochewka"
  ]
  node [
    id 224
    label "sok"
  ]
  node [
    id 225
    label "system_korzeniowy"
  ]
  node [
    id 226
    label "zawi&#261;zek"
  ]
  node [
    id 227
    label "mi&#261;&#380;sz"
  ]
  node [
    id 228
    label "frukt"
  ]
  node [
    id 229
    label "drylowanie"
  ]
  node [
    id 230
    label "produkt"
  ]
  node [
    id 231
    label "owocnia"
  ]
  node [
    id 232
    label "fruktoza"
  ]
  node [
    id 233
    label "obiekt"
  ]
  node [
    id 234
    label "gniazdo_nasienne"
  ]
  node [
    id 235
    label "rezultat"
  ]
  node [
    id 236
    label "glukoza"
  ]
  node [
    id 237
    label "pestka"
  ]
  node [
    id 238
    label "antocyjanidyn"
  ]
  node [
    id 239
    label "szczeciowce"
  ]
  node [
    id 240
    label "jasnotowce"
  ]
  node [
    id 241
    label "Oleaceae"
  ]
  node [
    id 242
    label "wielkopolski"
  ]
  node [
    id 243
    label "bez_czarny"
  ]
  node [
    id 244
    label "p&#322;aszcz"
  ]
  node [
    id 245
    label "okrycie"
  ]
  node [
    id 246
    label "zanadrze"
  ]
  node [
    id 247
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 248
    label "przedmiot"
  ]
  node [
    id 249
    label "cover"
  ]
  node [
    id 250
    label "kapota"
  ]
  node [
    id 251
    label "screen"
  ]
  node [
    id 252
    label "otoczenie"
  ]
  node [
    id 253
    label "podpinka"
  ]
  node [
    id 254
    label "element"
  ]
  node [
    id 255
    label "owini&#281;cie"
  ]
  node [
    id 256
    label "overcoat"
  ]
  node [
    id 257
    label "przysporzenie"
  ]
  node [
    id 258
    label "przykrycie"
  ]
  node [
    id 259
    label "&#347;piw&#243;r"
  ]
  node [
    id 260
    label "r&#281;kaw"
  ]
  node [
    id 261
    label "pow&#322;oka"
  ]
  node [
    id 262
    label "os&#322;ona"
  ]
  node [
    id 263
    label "podszewka"
  ]
  node [
    id 264
    label "formu&#322;owa&#263;"
  ]
  node [
    id 265
    label "ozdabia&#263;"
  ]
  node [
    id 266
    label "stawia&#263;"
  ]
  node [
    id 267
    label "spell"
  ]
  node [
    id 268
    label "styl"
  ]
  node [
    id 269
    label "skryba"
  ]
  node [
    id 270
    label "read"
  ]
  node [
    id 271
    label "donosi&#263;"
  ]
  node [
    id 272
    label "code"
  ]
  node [
    id 273
    label "tekst"
  ]
  node [
    id 274
    label "dysgrafia"
  ]
  node [
    id 275
    label "dysortografia"
  ]
  node [
    id 276
    label "tworzy&#263;"
  ]
  node [
    id 277
    label "prasa"
  ]
  node [
    id 278
    label "pope&#322;nia&#263;"
  ]
  node [
    id 279
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 280
    label "wytwarza&#263;"
  ]
  node [
    id 281
    label "get"
  ]
  node [
    id 282
    label "consist"
  ]
  node [
    id 283
    label "stanowi&#263;"
  ]
  node [
    id 284
    label "raise"
  ]
  node [
    id 285
    label "spill_the_beans"
  ]
  node [
    id 286
    label "przeby&#263;"
  ]
  node [
    id 287
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 288
    label "zanosi&#263;"
  ]
  node [
    id 289
    label "inform"
  ]
  node [
    id 290
    label "give"
  ]
  node [
    id 291
    label "zu&#380;y&#263;"
  ]
  node [
    id 292
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 293
    label "introduce"
  ]
  node [
    id 294
    label "render"
  ]
  node [
    id 295
    label "ci&#261;&#380;a"
  ]
  node [
    id 296
    label "informowa&#263;"
  ]
  node [
    id 297
    label "komunikowa&#263;"
  ]
  node [
    id 298
    label "convey"
  ]
  node [
    id 299
    label "pozostawia&#263;"
  ]
  node [
    id 300
    label "czyni&#263;"
  ]
  node [
    id 301
    label "wydawa&#263;"
  ]
  node [
    id 302
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 303
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 304
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 305
    label "przewidywa&#263;"
  ]
  node [
    id 306
    label "przyznawa&#263;"
  ]
  node [
    id 307
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 308
    label "go"
  ]
  node [
    id 309
    label "obstawia&#263;"
  ]
  node [
    id 310
    label "umieszcza&#263;"
  ]
  node [
    id 311
    label "ocenia&#263;"
  ]
  node [
    id 312
    label "zastawia&#263;"
  ]
  node [
    id 313
    label "stanowisko"
  ]
  node [
    id 314
    label "znak"
  ]
  node [
    id 315
    label "wskazywa&#263;"
  ]
  node [
    id 316
    label "uruchamia&#263;"
  ]
  node [
    id 317
    label "fundowa&#263;"
  ]
  node [
    id 318
    label "zmienia&#263;"
  ]
  node [
    id 319
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 320
    label "deliver"
  ]
  node [
    id 321
    label "powodowa&#263;"
  ]
  node [
    id 322
    label "wyznacza&#263;"
  ]
  node [
    id 323
    label "przedstawia&#263;"
  ]
  node [
    id 324
    label "wydobywa&#263;"
  ]
  node [
    id 325
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 326
    label "gryzipi&#243;rek"
  ]
  node [
    id 327
    label "pisarz"
  ]
  node [
    id 328
    label "ekscerpcja"
  ]
  node [
    id 329
    label "j&#281;zykowo"
  ]
  node [
    id 330
    label "wypowied&#378;"
  ]
  node [
    id 331
    label "redakcja"
  ]
  node [
    id 332
    label "pomini&#281;cie"
  ]
  node [
    id 333
    label "dzie&#322;o"
  ]
  node [
    id 334
    label "preparacja"
  ]
  node [
    id 335
    label "odmianka"
  ]
  node [
    id 336
    label "opu&#347;ci&#263;"
  ]
  node [
    id 337
    label "koniektura"
  ]
  node [
    id 338
    label "obelga"
  ]
  node [
    id 339
    label "zesp&#243;&#322;"
  ]
  node [
    id 340
    label "t&#322;oczysko"
  ]
  node [
    id 341
    label "depesza"
  ]
  node [
    id 342
    label "maszyna"
  ]
  node [
    id 343
    label "media"
  ]
  node [
    id 344
    label "napisa&#263;"
  ]
  node [
    id 345
    label "czasopismo"
  ]
  node [
    id 346
    label "dziennikarz_prasowy"
  ]
  node [
    id 347
    label "kiosk"
  ]
  node [
    id 348
    label "maszyna_rolnicza"
  ]
  node [
    id 349
    label "gazeta"
  ]
  node [
    id 350
    label "trzonek"
  ]
  node [
    id 351
    label "reakcja"
  ]
  node [
    id 352
    label "narz&#281;dzie"
  ]
  node [
    id 353
    label "spos&#243;b"
  ]
  node [
    id 354
    label "zbi&#243;r"
  ]
  node [
    id 355
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 356
    label "zachowanie"
  ]
  node [
    id 357
    label "stylik"
  ]
  node [
    id 358
    label "dyscyplina_sportowa"
  ]
  node [
    id 359
    label "handle"
  ]
  node [
    id 360
    label "stroke"
  ]
  node [
    id 361
    label "line"
  ]
  node [
    id 362
    label "charakter"
  ]
  node [
    id 363
    label "natural_language"
  ]
  node [
    id 364
    label "kanon"
  ]
  node [
    id 365
    label "behawior"
  ]
  node [
    id 366
    label "dysleksja"
  ]
  node [
    id 367
    label "pisanie"
  ]
  node [
    id 368
    label "dysgraphia"
  ]
  node [
    id 369
    label "kognicja"
  ]
  node [
    id 370
    label "object"
  ]
  node [
    id 371
    label "rozprawa"
  ]
  node [
    id 372
    label "temat"
  ]
  node [
    id 373
    label "wydarzenie"
  ]
  node [
    id 374
    label "szczeg&#243;&#322;"
  ]
  node [
    id 375
    label "proposition"
  ]
  node [
    id 376
    label "przes&#322;anka"
  ]
  node [
    id 377
    label "rzecz"
  ]
  node [
    id 378
    label "idea"
  ]
  node [
    id 379
    label "przebiec"
  ]
  node [
    id 380
    label "czynno&#347;&#263;"
  ]
  node [
    id 381
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 382
    label "motyw"
  ]
  node [
    id 383
    label "przebiegni&#281;cie"
  ]
  node [
    id 384
    label "fabu&#322;a"
  ]
  node [
    id 385
    label "ideologia"
  ]
  node [
    id 386
    label "intelekt"
  ]
  node [
    id 387
    label "Kant"
  ]
  node [
    id 388
    label "p&#322;&#243;d"
  ]
  node [
    id 389
    label "cel"
  ]
  node [
    id 390
    label "poj&#281;cie"
  ]
  node [
    id 391
    label "istota"
  ]
  node [
    id 392
    label "pomys&#322;"
  ]
  node [
    id 393
    label "ideacja"
  ]
  node [
    id 394
    label "wpadni&#281;cie"
  ]
  node [
    id 395
    label "mienie"
  ]
  node [
    id 396
    label "przyroda"
  ]
  node [
    id 397
    label "kultura"
  ]
  node [
    id 398
    label "wpa&#347;&#263;"
  ]
  node [
    id 399
    label "wpadanie"
  ]
  node [
    id 400
    label "wpada&#263;"
  ]
  node [
    id 401
    label "s&#261;d"
  ]
  node [
    id 402
    label "rozumowanie"
  ]
  node [
    id 403
    label "opracowanie"
  ]
  node [
    id 404
    label "proces"
  ]
  node [
    id 405
    label "obrady"
  ]
  node [
    id 406
    label "cytat"
  ]
  node [
    id 407
    label "obja&#347;nienie"
  ]
  node [
    id 408
    label "s&#261;dzenie"
  ]
  node [
    id 409
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 410
    label "niuansowa&#263;"
  ]
  node [
    id 411
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 412
    label "sk&#322;adnik"
  ]
  node [
    id 413
    label "zniuansowa&#263;"
  ]
  node [
    id 414
    label "fakt"
  ]
  node [
    id 415
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 416
    label "przyczyna"
  ]
  node [
    id 417
    label "wnioskowanie"
  ]
  node [
    id 418
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 419
    label "wyraz_pochodny"
  ]
  node [
    id 420
    label "zboczenie"
  ]
  node [
    id 421
    label "om&#243;wienie"
  ]
  node [
    id 422
    label "omawia&#263;"
  ]
  node [
    id 423
    label "fraza"
  ]
  node [
    id 424
    label "tre&#347;&#263;"
  ]
  node [
    id 425
    label "entity"
  ]
  node [
    id 426
    label "forum"
  ]
  node [
    id 427
    label "topik"
  ]
  node [
    id 428
    label "tematyka"
  ]
  node [
    id 429
    label "w&#261;tek"
  ]
  node [
    id 430
    label "zbaczanie"
  ]
  node [
    id 431
    label "forma"
  ]
  node [
    id 432
    label "om&#243;wi&#263;"
  ]
  node [
    id 433
    label "omawianie"
  ]
  node [
    id 434
    label "melodia"
  ]
  node [
    id 435
    label "otoczka"
  ]
  node [
    id 436
    label "zbacza&#263;"
  ]
  node [
    id 437
    label "zboczy&#263;"
  ]
  node [
    id 438
    label "dawny"
  ]
  node [
    id 439
    label "zestarzenie_si&#281;"
  ]
  node [
    id 440
    label "starzenie_si&#281;"
  ]
  node [
    id 441
    label "archaicznie"
  ]
  node [
    id 442
    label "zgrzybienie"
  ]
  node [
    id 443
    label "niedzisiejszy"
  ]
  node [
    id 444
    label "przestarzale"
  ]
  node [
    id 445
    label "stary"
  ]
  node [
    id 446
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 447
    label "ojciec"
  ]
  node [
    id 448
    label "nienowoczesny"
  ]
  node [
    id 449
    label "gruba_ryba"
  ]
  node [
    id 450
    label "poprzedni"
  ]
  node [
    id 451
    label "dawno"
  ]
  node [
    id 452
    label "staro"
  ]
  node [
    id 453
    label "m&#261;&#380;"
  ]
  node [
    id 454
    label "starzy"
  ]
  node [
    id 455
    label "dotychczasowy"
  ]
  node [
    id 456
    label "p&#243;&#378;ny"
  ]
  node [
    id 457
    label "d&#322;ugoletni"
  ]
  node [
    id 458
    label "charakterystyczny"
  ]
  node [
    id 459
    label "brat"
  ]
  node [
    id 460
    label "po_staro&#347;wiecku"
  ]
  node [
    id 461
    label "zwierzchnik"
  ]
  node [
    id 462
    label "znajomy"
  ]
  node [
    id 463
    label "odleg&#322;y"
  ]
  node [
    id 464
    label "starczo"
  ]
  node [
    id 465
    label "dawniej"
  ]
  node [
    id 466
    label "niegdysiejszy"
  ]
  node [
    id 467
    label "dojrza&#322;y"
  ]
  node [
    id 468
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 469
    label "przestarza&#322;y"
  ]
  node [
    id 470
    label "przesz&#322;y"
  ]
  node [
    id 471
    label "od_dawna"
  ]
  node [
    id 472
    label "anachroniczny"
  ]
  node [
    id 473
    label "wcze&#347;niejszy"
  ]
  node [
    id 474
    label "kombatant"
  ]
  node [
    id 475
    label "niemodnie"
  ]
  node [
    id 476
    label "zdezaktualizowanie_si&#281;"
  ]
  node [
    id 477
    label "zniedo&#322;&#281;&#380;nienie"
  ]
  node [
    id 478
    label "zramolenie"
  ]
  node [
    id 479
    label "postarzenie_si&#281;"
  ]
  node [
    id 480
    label "archaiczny"
  ]
  node [
    id 481
    label "kwestarz"
  ]
  node [
    id 482
    label "kwestowanie"
  ]
  node [
    id 483
    label "apel"
  ]
  node [
    id 484
    label "recoil"
  ]
  node [
    id 485
    label "collection"
  ]
  node [
    id 486
    label "spotkanie"
  ]
  node [
    id 487
    label "koszyk&#243;wka"
  ]
  node [
    id 488
    label "chwyt"
  ]
  node [
    id 489
    label "doznanie"
  ]
  node [
    id 490
    label "gathering"
  ]
  node [
    id 491
    label "zawarcie"
  ]
  node [
    id 492
    label "powitanie"
  ]
  node [
    id 493
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 494
    label "spowodowanie"
  ]
  node [
    id 495
    label "zdarzenie_si&#281;"
  ]
  node [
    id 496
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 497
    label "znalezienie"
  ]
  node [
    id 498
    label "match"
  ]
  node [
    id 499
    label "employment"
  ]
  node [
    id 500
    label "po&#380;egnanie"
  ]
  node [
    id 501
    label "gather"
  ]
  node [
    id 502
    label "spotykanie"
  ]
  node [
    id 503
    label "spotkanie_si&#281;"
  ]
  node [
    id 504
    label "activity"
  ]
  node [
    id 505
    label "bezproblemowy"
  ]
  node [
    id 506
    label "kompozycja"
  ]
  node [
    id 507
    label "zacisk"
  ]
  node [
    id 508
    label "strategia"
  ]
  node [
    id 509
    label "zabieg"
  ]
  node [
    id 510
    label "ruch"
  ]
  node [
    id 511
    label "uchwyt"
  ]
  node [
    id 512
    label "uj&#281;cie"
  ]
  node [
    id 513
    label "zakon_&#380;ebraczy"
  ]
  node [
    id 514
    label "wolontariusz"
  ]
  node [
    id 515
    label "zakonnik"
  ]
  node [
    id 516
    label "uczestniczenie"
  ]
  node [
    id 517
    label "pro&#347;ba"
  ]
  node [
    id 518
    label "zdyscyplinowanie"
  ]
  node [
    id 519
    label "uderzenie"
  ]
  node [
    id 520
    label "pies_my&#347;liwski"
  ]
  node [
    id 521
    label "bid"
  ]
  node [
    id 522
    label "sygna&#322;"
  ]
  node [
    id 523
    label "szermierka"
  ]
  node [
    id 524
    label "koz&#322;owa&#263;"
  ]
  node [
    id 525
    label "kroki"
  ]
  node [
    id 526
    label "kosz"
  ]
  node [
    id 527
    label "pi&#322;ka"
  ]
  node [
    id 528
    label "przelobowa&#263;"
  ]
  node [
    id 529
    label "strefa_podkoszowa"
  ]
  node [
    id 530
    label "przelobowanie"
  ]
  node [
    id 531
    label "wsad"
  ]
  node [
    id 532
    label "dwutakt"
  ]
  node [
    id 533
    label "koz&#322;owanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
]
