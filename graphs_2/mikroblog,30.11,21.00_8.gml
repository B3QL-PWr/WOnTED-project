graph [
  node [
    id 0
    label "nawi&#261;zanie"
    origin "text"
  ]
  node [
    id 1
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 2
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "connection"
  ]
  node [
    id 4
    label "zasugerowa&#263;"
  ]
  node [
    id 5
    label "zawi&#261;zanie_si&#281;"
  ]
  node [
    id 6
    label "zasugerowanie"
  ]
  node [
    id 7
    label "wskaz&#243;wka"
  ]
  node [
    id 8
    label "prequel"
  ]
  node [
    id 9
    label "sugerowa&#263;"
  ]
  node [
    id 10
    label "przyczepienie"
  ]
  node [
    id 11
    label "zaczerpni&#281;cie"
  ]
  node [
    id 12
    label "zacz&#281;cie"
  ]
  node [
    id 13
    label "wzmianka"
  ]
  node [
    id 14
    label "spin-off"
  ]
  node [
    id 15
    label "sugerowanie"
  ]
  node [
    id 16
    label "woda"
  ]
  node [
    id 17
    label "u&#380;ycie"
  ]
  node [
    id 18
    label "pickings"
  ]
  node [
    id 19
    label "wzi&#281;cie"
  ]
  node [
    id 20
    label "przymocowanie"
  ]
  node [
    id 21
    label "informacja"
  ]
  node [
    id 22
    label "message"
  ]
  node [
    id 23
    label "fakt"
  ]
  node [
    id 24
    label "tarcza"
  ]
  node [
    id 25
    label "zegar"
  ]
  node [
    id 26
    label "solucja"
  ]
  node [
    id 27
    label "implikowa&#263;"
  ]
  node [
    id 28
    label "discourtesy"
  ]
  node [
    id 29
    label "odj&#281;cie"
  ]
  node [
    id 30
    label "post&#261;pienie"
  ]
  node [
    id 31
    label "opening"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 34
    label "wyra&#380;enie"
  ]
  node [
    id 35
    label "zrobienie"
  ]
  node [
    id 36
    label "sugestia"
  ]
  node [
    id 37
    label "delivery"
  ]
  node [
    id 38
    label "podpowiedzenie"
  ]
  node [
    id 39
    label "sformu&#322;owanie"
  ]
  node [
    id 40
    label "inkling"
  ]
  node [
    id 41
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 42
    label "podpowiedzie&#263;"
  ]
  node [
    id 43
    label "push"
  ]
  node [
    id 44
    label "indicate"
  ]
  node [
    id 45
    label "formu&#322;owa&#263;"
  ]
  node [
    id 46
    label "podpowiada&#263;"
  ]
  node [
    id 47
    label "nasuwa&#263;"
  ]
  node [
    id 48
    label "hint"
  ]
  node [
    id 49
    label "render"
  ]
  node [
    id 50
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 51
    label "podpowiadanie"
  ]
  node [
    id 52
    label "formu&#322;owanie"
  ]
  node [
    id 53
    label "&#347;wiadczenie"
  ]
  node [
    id 54
    label "przedsi&#281;biorczo&#347;&#263;_akademicka"
  ]
  node [
    id 55
    label "utw&#243;r"
  ]
  node [
    id 56
    label "firma"
  ]
  node [
    id 57
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 58
    label "przebiec"
  ]
  node [
    id 59
    label "charakter"
  ]
  node [
    id 60
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 61
    label "motyw"
  ]
  node [
    id 62
    label "przebiegni&#281;cie"
  ]
  node [
    id 63
    label "fabu&#322;a"
  ]
  node [
    id 64
    label "sk&#322;adnik"
  ]
  node [
    id 65
    label "warunki"
  ]
  node [
    id 66
    label "sytuacja"
  ]
  node [
    id 67
    label "wydarzenie"
  ]
  node [
    id 68
    label "w&#261;tek"
  ]
  node [
    id 69
    label "w&#281;ze&#322;"
  ]
  node [
    id 70
    label "perypetia"
  ]
  node [
    id 71
    label "opowiadanie"
  ]
  node [
    id 72
    label "fraza"
  ]
  node [
    id 73
    label "temat"
  ]
  node [
    id 74
    label "melodia"
  ]
  node [
    id 75
    label "cecha"
  ]
  node [
    id 76
    label "przyczyna"
  ]
  node [
    id 77
    label "ozdoba"
  ]
  node [
    id 78
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 79
    label "przeby&#263;"
  ]
  node [
    id 80
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 81
    label "run"
  ]
  node [
    id 82
    label "proceed"
  ]
  node [
    id 83
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 84
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 85
    label "przemierzy&#263;"
  ]
  node [
    id 86
    label "fly"
  ]
  node [
    id 87
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 88
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 89
    label "przesun&#261;&#263;"
  ]
  node [
    id 90
    label "activity"
  ]
  node [
    id 91
    label "bezproblemowy"
  ]
  node [
    id 92
    label "przedmiot"
  ]
  node [
    id 93
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 94
    label "zbi&#243;r"
  ]
  node [
    id 95
    label "cz&#322;owiek"
  ]
  node [
    id 96
    label "osobowo&#347;&#263;"
  ]
  node [
    id 97
    label "psychika"
  ]
  node [
    id 98
    label "posta&#263;"
  ]
  node [
    id 99
    label "kompleksja"
  ]
  node [
    id 100
    label "fizjonomia"
  ]
  node [
    id 101
    label "zjawisko"
  ]
  node [
    id 102
    label "entity"
  ]
  node [
    id 103
    label "przemkni&#281;cie"
  ]
  node [
    id 104
    label "zabrzmienie"
  ]
  node [
    id 105
    label "przebycie"
  ]
  node [
    id 106
    label "zdarzenie_si&#281;"
  ]
  node [
    id 107
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 108
    label "powiada&#263;"
  ]
  node [
    id 109
    label "komunikowa&#263;"
  ]
  node [
    id 110
    label "inform"
  ]
  node [
    id 111
    label "communicate"
  ]
  node [
    id 112
    label "powodowa&#263;"
  ]
  node [
    id 113
    label "mawia&#263;"
  ]
  node [
    id 114
    label "m&#243;wi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
]
