graph [
  node [
    id 0
    label "skazany"
    origin "text"
  ]
  node [
    id 1
    label "dania"
    origin "text"
  ]
  node [
    id 2
    label "przest&#281;pstwo"
    origin "text"
  ]
  node [
    id 3
    label "imigrant"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "osadza&#263;"
    origin "text"
  ]
  node [
    id 6
    label "specjalny"
    origin "text"
  ]
  node [
    id 7
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 8
    label "deportacyjny"
    origin "text"
  ]
  node [
    id 9
    label "jeden"
    origin "text"
  ]
  node [
    id 10
    label "niezamieszka&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "du&#324;ski"
    origin "text"
  ]
  node [
    id 12
    label "wyspa"
    origin "text"
  ]
  node [
    id 13
    label "s&#261;d"
  ]
  node [
    id 14
    label "dysponowanie"
  ]
  node [
    id 15
    label "dysponowa&#263;"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "ludzko&#347;&#263;"
  ]
  node [
    id 18
    label "asymilowanie"
  ]
  node [
    id 19
    label "wapniak"
  ]
  node [
    id 20
    label "asymilowa&#263;"
  ]
  node [
    id 21
    label "os&#322;abia&#263;"
  ]
  node [
    id 22
    label "posta&#263;"
  ]
  node [
    id 23
    label "hominid"
  ]
  node [
    id 24
    label "podw&#322;adny"
  ]
  node [
    id 25
    label "os&#322;abianie"
  ]
  node [
    id 26
    label "g&#322;owa"
  ]
  node [
    id 27
    label "figura"
  ]
  node [
    id 28
    label "portrecista"
  ]
  node [
    id 29
    label "dwun&#243;g"
  ]
  node [
    id 30
    label "profanum"
  ]
  node [
    id 31
    label "mikrokosmos"
  ]
  node [
    id 32
    label "nasada"
  ]
  node [
    id 33
    label "duch"
  ]
  node [
    id 34
    label "antropochoria"
  ]
  node [
    id 35
    label "osoba"
  ]
  node [
    id 36
    label "wz&#243;r"
  ]
  node [
    id 37
    label "senior"
  ]
  node [
    id 38
    label "oddzia&#322;ywanie"
  ]
  node [
    id 39
    label "Adam"
  ]
  node [
    id 40
    label "homo_sapiens"
  ]
  node [
    id 41
    label "polifag"
  ]
  node [
    id 42
    label "dispose"
  ]
  node [
    id 43
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 44
    label "namaszczenie_chorych"
  ]
  node [
    id 45
    label "control"
  ]
  node [
    id 46
    label "rozporz&#261;dza&#263;"
  ]
  node [
    id 47
    label "przygotowywa&#263;"
  ]
  node [
    id 48
    label "command"
  ]
  node [
    id 49
    label "ekonomia"
  ]
  node [
    id 50
    label "zesp&#243;&#322;"
  ]
  node [
    id 51
    label "podejrzany"
  ]
  node [
    id 52
    label "s&#261;downictwo"
  ]
  node [
    id 53
    label "system"
  ]
  node [
    id 54
    label "biuro"
  ]
  node [
    id 55
    label "wytw&#243;r"
  ]
  node [
    id 56
    label "court"
  ]
  node [
    id 57
    label "forum"
  ]
  node [
    id 58
    label "bronienie"
  ]
  node [
    id 59
    label "urz&#261;d"
  ]
  node [
    id 60
    label "wydarzenie"
  ]
  node [
    id 61
    label "oskar&#380;yciel"
  ]
  node [
    id 62
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 63
    label "post&#281;powanie"
  ]
  node [
    id 64
    label "broni&#263;"
  ]
  node [
    id 65
    label "my&#347;l"
  ]
  node [
    id 66
    label "pods&#261;dny"
  ]
  node [
    id 67
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 68
    label "obrona"
  ]
  node [
    id 69
    label "wypowied&#378;"
  ]
  node [
    id 70
    label "instytucja"
  ]
  node [
    id 71
    label "antylogizm"
  ]
  node [
    id 72
    label "konektyw"
  ]
  node [
    id 73
    label "&#347;wiadek"
  ]
  node [
    id 74
    label "procesowicz"
  ]
  node [
    id 75
    label "strona"
  ]
  node [
    id 76
    label "dysponowanie_si&#281;"
  ]
  node [
    id 77
    label "rozdysponowywanie"
  ]
  node [
    id 78
    label "zarz&#261;dzanie"
  ]
  node [
    id 79
    label "disposal"
  ]
  node [
    id 80
    label "rozporz&#261;dzanie"
  ]
  node [
    id 81
    label "przygotowywanie"
  ]
  node [
    id 82
    label "brudny"
  ]
  node [
    id 83
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 84
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 85
    label "crime"
  ]
  node [
    id 86
    label "sprawstwo"
  ]
  node [
    id 87
    label "nies&#322;uszno&#347;&#263;"
  ]
  node [
    id 88
    label "bias"
  ]
  node [
    id 89
    label "strata"
  ]
  node [
    id 90
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 91
    label "obelga"
  ]
  node [
    id 92
    label "post&#281;pek"
  ]
  node [
    id 93
    label "injustice"
  ]
  node [
    id 94
    label "criminalism"
  ]
  node [
    id 95
    label "gang"
  ]
  node [
    id 96
    label "bezprawie"
  ]
  node [
    id 97
    label "problem_spo&#322;eczny"
  ]
  node [
    id 98
    label "patologia"
  ]
  node [
    id 99
    label "banda"
  ]
  node [
    id 100
    label "nieprzyjemny"
  ]
  node [
    id 101
    label "nielegalny"
  ]
  node [
    id 102
    label "brudno"
  ]
  node [
    id 103
    label "brudzenie_si&#281;"
  ]
  node [
    id 104
    label "z&#322;amany"
  ]
  node [
    id 105
    label "nieprzyzwoity"
  ]
  node [
    id 106
    label "nieporz&#261;dny"
  ]
  node [
    id 107
    label "brudzenie"
  ]
  node [
    id 108
    label "ciemny"
  ]
  node [
    id 109
    label "flejtuchowaty"
  ]
  node [
    id 110
    label "zabrudzenie_si&#281;"
  ]
  node [
    id 111
    label "nieczysty"
  ]
  node [
    id 112
    label "u&#380;ywany"
  ]
  node [
    id 113
    label "cudzoziemiec"
  ]
  node [
    id 114
    label "przybysz"
  ]
  node [
    id 115
    label "migrant"
  ]
  node [
    id 116
    label "imigracja"
  ]
  node [
    id 117
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 118
    label "nowy"
  ]
  node [
    id 119
    label "miesi&#261;c_ksi&#281;&#380;ycowy"
  ]
  node [
    id 120
    label "obcy"
  ]
  node [
    id 121
    label "obcokrajowy"
  ]
  node [
    id 122
    label "etran&#380;er"
  ]
  node [
    id 123
    label "mieszkaniec"
  ]
  node [
    id 124
    label "zagraniczny"
  ]
  node [
    id 125
    label "cudzoziemski"
  ]
  node [
    id 126
    label "nap&#322;yw"
  ]
  node [
    id 127
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 128
    label "immigration"
  ]
  node [
    id 129
    label "migracja"
  ]
  node [
    id 130
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 131
    label "mie&#263;_miejsce"
  ]
  node [
    id 132
    label "equal"
  ]
  node [
    id 133
    label "trwa&#263;"
  ]
  node [
    id 134
    label "chodzi&#263;"
  ]
  node [
    id 135
    label "si&#281;ga&#263;"
  ]
  node [
    id 136
    label "stan"
  ]
  node [
    id 137
    label "obecno&#347;&#263;"
  ]
  node [
    id 138
    label "stand"
  ]
  node [
    id 139
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 140
    label "uczestniczy&#263;"
  ]
  node [
    id 141
    label "participate"
  ]
  node [
    id 142
    label "robi&#263;"
  ]
  node [
    id 143
    label "istnie&#263;"
  ]
  node [
    id 144
    label "pozostawa&#263;"
  ]
  node [
    id 145
    label "zostawa&#263;"
  ]
  node [
    id 146
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 147
    label "adhere"
  ]
  node [
    id 148
    label "compass"
  ]
  node [
    id 149
    label "korzysta&#263;"
  ]
  node [
    id 150
    label "appreciation"
  ]
  node [
    id 151
    label "osi&#261;ga&#263;"
  ]
  node [
    id 152
    label "dociera&#263;"
  ]
  node [
    id 153
    label "get"
  ]
  node [
    id 154
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 155
    label "mierzy&#263;"
  ]
  node [
    id 156
    label "u&#380;ywa&#263;"
  ]
  node [
    id 157
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 158
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 159
    label "exsert"
  ]
  node [
    id 160
    label "being"
  ]
  node [
    id 161
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 162
    label "cecha"
  ]
  node [
    id 163
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 164
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 165
    label "p&#322;ywa&#263;"
  ]
  node [
    id 166
    label "run"
  ]
  node [
    id 167
    label "bangla&#263;"
  ]
  node [
    id 168
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 169
    label "przebiega&#263;"
  ]
  node [
    id 170
    label "wk&#322;ada&#263;"
  ]
  node [
    id 171
    label "proceed"
  ]
  node [
    id 172
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 173
    label "carry"
  ]
  node [
    id 174
    label "bywa&#263;"
  ]
  node [
    id 175
    label "dziama&#263;"
  ]
  node [
    id 176
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 177
    label "stara&#263;_si&#281;"
  ]
  node [
    id 178
    label "para"
  ]
  node [
    id 179
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 180
    label "str&#243;j"
  ]
  node [
    id 181
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 182
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 183
    label "krok"
  ]
  node [
    id 184
    label "tryb"
  ]
  node [
    id 185
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 186
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 187
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 188
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 189
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 190
    label "continue"
  ]
  node [
    id 191
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 192
    label "Ohio"
  ]
  node [
    id 193
    label "wci&#281;cie"
  ]
  node [
    id 194
    label "Nowy_York"
  ]
  node [
    id 195
    label "warstwa"
  ]
  node [
    id 196
    label "samopoczucie"
  ]
  node [
    id 197
    label "Illinois"
  ]
  node [
    id 198
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 199
    label "state"
  ]
  node [
    id 200
    label "Jukatan"
  ]
  node [
    id 201
    label "Kalifornia"
  ]
  node [
    id 202
    label "Wirginia"
  ]
  node [
    id 203
    label "wektor"
  ]
  node [
    id 204
    label "Teksas"
  ]
  node [
    id 205
    label "Goa"
  ]
  node [
    id 206
    label "Waszyngton"
  ]
  node [
    id 207
    label "miejsce"
  ]
  node [
    id 208
    label "Massachusetts"
  ]
  node [
    id 209
    label "Alaska"
  ]
  node [
    id 210
    label "Arakan"
  ]
  node [
    id 211
    label "Hawaje"
  ]
  node [
    id 212
    label "Maryland"
  ]
  node [
    id 213
    label "punkt"
  ]
  node [
    id 214
    label "Michigan"
  ]
  node [
    id 215
    label "Arizona"
  ]
  node [
    id 216
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 217
    label "Georgia"
  ]
  node [
    id 218
    label "poziom"
  ]
  node [
    id 219
    label "Pensylwania"
  ]
  node [
    id 220
    label "shape"
  ]
  node [
    id 221
    label "Luizjana"
  ]
  node [
    id 222
    label "Nowy_Meksyk"
  ]
  node [
    id 223
    label "Alabama"
  ]
  node [
    id 224
    label "ilo&#347;&#263;"
  ]
  node [
    id 225
    label "Kansas"
  ]
  node [
    id 226
    label "Oregon"
  ]
  node [
    id 227
    label "Floryda"
  ]
  node [
    id 228
    label "Oklahoma"
  ]
  node [
    id 229
    label "jednostka_administracyjna"
  ]
  node [
    id 230
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 231
    label "przymocowywa&#263;"
  ]
  node [
    id 232
    label "wytwarza&#263;"
  ]
  node [
    id 233
    label "zatrzymywa&#263;"
  ]
  node [
    id 234
    label "umieszcza&#263;"
  ]
  node [
    id 235
    label "settle"
  ]
  node [
    id 236
    label "plasowa&#263;"
  ]
  node [
    id 237
    label "umie&#347;ci&#263;"
  ]
  node [
    id 238
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 239
    label "pomieszcza&#263;"
  ]
  node [
    id 240
    label "accommodate"
  ]
  node [
    id 241
    label "zmienia&#263;"
  ]
  node [
    id 242
    label "powodowa&#263;"
  ]
  node [
    id 243
    label "venture"
  ]
  node [
    id 244
    label "wpiernicza&#263;"
  ]
  node [
    id 245
    label "okre&#347;la&#263;"
  ]
  node [
    id 246
    label "pin"
  ]
  node [
    id 247
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 248
    label "komornik"
  ]
  node [
    id 249
    label "suspend"
  ]
  node [
    id 250
    label "zabiera&#263;"
  ]
  node [
    id 251
    label "zgarnia&#263;"
  ]
  node [
    id 252
    label "throng"
  ]
  node [
    id 253
    label "unieruchamia&#263;"
  ]
  node [
    id 254
    label "przerywa&#263;"
  ]
  node [
    id 255
    label "przechowywa&#263;"
  ]
  node [
    id 256
    label "&#322;apa&#263;"
  ]
  node [
    id 257
    label "przetrzymywa&#263;"
  ]
  node [
    id 258
    label "allude"
  ]
  node [
    id 259
    label "zaczepia&#263;"
  ]
  node [
    id 260
    label "prosecute"
  ]
  node [
    id 261
    label "hold"
  ]
  node [
    id 262
    label "zamyka&#263;"
  ]
  node [
    id 263
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 264
    label "create"
  ]
  node [
    id 265
    label "give"
  ]
  node [
    id 266
    label "intencjonalny"
  ]
  node [
    id 267
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 268
    label "niedorozw&#243;j"
  ]
  node [
    id 269
    label "szczeg&#243;lny"
  ]
  node [
    id 270
    label "specjalnie"
  ]
  node [
    id 271
    label "nieetatowy"
  ]
  node [
    id 272
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 273
    label "nienormalny"
  ]
  node [
    id 274
    label "umy&#347;lnie"
  ]
  node [
    id 275
    label "odpowiedni"
  ]
  node [
    id 276
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 277
    label "nienormalnie"
  ]
  node [
    id 278
    label "anormalnie"
  ]
  node [
    id 279
    label "schizol"
  ]
  node [
    id 280
    label "pochytany"
  ]
  node [
    id 281
    label "popaprany"
  ]
  node [
    id 282
    label "niestandardowy"
  ]
  node [
    id 283
    label "chory_psychicznie"
  ]
  node [
    id 284
    label "nieprawid&#322;owy"
  ]
  node [
    id 285
    label "dziwny"
  ]
  node [
    id 286
    label "psychol"
  ]
  node [
    id 287
    label "powalony"
  ]
  node [
    id 288
    label "stracenie_rozumu"
  ]
  node [
    id 289
    label "chory"
  ]
  node [
    id 290
    label "z&#322;y"
  ]
  node [
    id 291
    label "nieprzypadkowy"
  ]
  node [
    id 292
    label "intencjonalnie"
  ]
  node [
    id 293
    label "szczeg&#243;lnie"
  ]
  node [
    id 294
    label "wyj&#261;tkowy"
  ]
  node [
    id 295
    label "zaburzenie"
  ]
  node [
    id 296
    label "niedoskona&#322;o&#347;&#263;"
  ]
  node [
    id 297
    label "wada"
  ]
  node [
    id 298
    label "zacofanie"
  ]
  node [
    id 299
    label "g&#322;upek"
  ]
  node [
    id 300
    label "zesp&#243;&#322;_Downa"
  ]
  node [
    id 301
    label "idiotyzm"
  ]
  node [
    id 302
    label "umy&#347;lny"
  ]
  node [
    id 303
    label "zdarzony"
  ]
  node [
    id 304
    label "odpowiednio"
  ]
  node [
    id 305
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 306
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 307
    label "nale&#380;ny"
  ]
  node [
    id 308
    label "nale&#380;yty"
  ]
  node [
    id 309
    label "stosownie"
  ]
  node [
    id 310
    label "odpowiadanie"
  ]
  node [
    id 311
    label "niesprawno&#347;&#263;"
  ]
  node [
    id 312
    label "nieetatowo"
  ]
  node [
    id 313
    label "nieoficjalny"
  ]
  node [
    id 314
    label "inny"
  ]
  node [
    id 315
    label "zatrudniony"
  ]
  node [
    id 316
    label "&#347;rodek"
  ]
  node [
    id 317
    label "skupisko"
  ]
  node [
    id 318
    label "zal&#261;&#380;ek"
  ]
  node [
    id 319
    label "otoczenie"
  ]
  node [
    id 320
    label "Hollywood"
  ]
  node [
    id 321
    label "warunki"
  ]
  node [
    id 322
    label "center"
  ]
  node [
    id 323
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 324
    label "status"
  ]
  node [
    id 325
    label "sytuacja"
  ]
  node [
    id 326
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 327
    label "okrycie"
  ]
  node [
    id 328
    label "class"
  ]
  node [
    id 329
    label "spowodowanie"
  ]
  node [
    id 330
    label "background"
  ]
  node [
    id 331
    label "zdarzenie_si&#281;"
  ]
  node [
    id 332
    label "grupa"
  ]
  node [
    id 333
    label "crack"
  ]
  node [
    id 334
    label "cortege"
  ]
  node [
    id 335
    label "okolica"
  ]
  node [
    id 336
    label "czynno&#347;&#263;"
  ]
  node [
    id 337
    label "huczek"
  ]
  node [
    id 338
    label "zrobienie"
  ]
  node [
    id 339
    label "Wielki_Atraktor"
  ]
  node [
    id 340
    label "zbi&#243;r"
  ]
  node [
    id 341
    label "warunek_lokalowy"
  ]
  node [
    id 342
    label "plac"
  ]
  node [
    id 343
    label "location"
  ]
  node [
    id 344
    label "uwaga"
  ]
  node [
    id 345
    label "przestrze&#324;"
  ]
  node [
    id 346
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 347
    label "chwila"
  ]
  node [
    id 348
    label "cia&#322;o"
  ]
  node [
    id 349
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 350
    label "praca"
  ]
  node [
    id 351
    label "rz&#261;d"
  ]
  node [
    id 352
    label "spos&#243;b"
  ]
  node [
    id 353
    label "abstrakcja"
  ]
  node [
    id 354
    label "czas"
  ]
  node [
    id 355
    label "chemikalia"
  ]
  node [
    id 356
    label "substancja"
  ]
  node [
    id 357
    label "osoba_prawna"
  ]
  node [
    id 358
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 359
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 360
    label "poj&#281;cie"
  ]
  node [
    id 361
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 362
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 363
    label "organizacja"
  ]
  node [
    id 364
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 365
    label "Fundusze_Unijne"
  ]
  node [
    id 366
    label "establishment"
  ]
  node [
    id 367
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 368
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 369
    label "afiliowa&#263;"
  ]
  node [
    id 370
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 371
    label "standard"
  ]
  node [
    id 372
    label "zamykanie"
  ]
  node [
    id 373
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 374
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 375
    label "organ"
  ]
  node [
    id 376
    label "zar&#243;d&#378;"
  ]
  node [
    id 377
    label "pocz&#261;tek"
  ]
  node [
    id 378
    label "integument"
  ]
  node [
    id 379
    label "Los_Angeles"
  ]
  node [
    id 380
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 381
    label "shot"
  ]
  node [
    id 382
    label "jednakowy"
  ]
  node [
    id 383
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 384
    label "ujednolicenie"
  ]
  node [
    id 385
    label "jaki&#347;"
  ]
  node [
    id 386
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 387
    label "jednolicie"
  ]
  node [
    id 388
    label "kieliszek"
  ]
  node [
    id 389
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 390
    label "w&#243;dka"
  ]
  node [
    id 391
    label "ten"
  ]
  node [
    id 392
    label "szk&#322;o"
  ]
  node [
    id 393
    label "zawarto&#347;&#263;"
  ]
  node [
    id 394
    label "naczynie"
  ]
  node [
    id 395
    label "alkohol"
  ]
  node [
    id 396
    label "sznaps"
  ]
  node [
    id 397
    label "nap&#243;j"
  ]
  node [
    id 398
    label "gorza&#322;ka"
  ]
  node [
    id 399
    label "mohorycz"
  ]
  node [
    id 400
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 401
    label "zr&#243;wnanie"
  ]
  node [
    id 402
    label "mundurowanie"
  ]
  node [
    id 403
    label "taki&#380;"
  ]
  node [
    id 404
    label "jednakowo"
  ]
  node [
    id 405
    label "mundurowa&#263;"
  ]
  node [
    id 406
    label "zr&#243;wnywanie"
  ]
  node [
    id 407
    label "identyczny"
  ]
  node [
    id 408
    label "okre&#347;lony"
  ]
  node [
    id 409
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 410
    label "z&#322;o&#380;ony"
  ]
  node [
    id 411
    label "przyzwoity"
  ]
  node [
    id 412
    label "ciekawy"
  ]
  node [
    id 413
    label "jako&#347;"
  ]
  node [
    id 414
    label "jako_tako"
  ]
  node [
    id 415
    label "niez&#322;y"
  ]
  node [
    id 416
    label "charakterystyczny"
  ]
  node [
    id 417
    label "g&#322;&#281;bszy"
  ]
  node [
    id 418
    label "drink"
  ]
  node [
    id 419
    label "jednolity"
  ]
  node [
    id 420
    label "upodobnienie"
  ]
  node [
    id 421
    label "calibration"
  ]
  node [
    id 422
    label "odludny"
  ]
  node [
    id 423
    label "odludnie"
  ]
  node [
    id 424
    label "nietowarzyski"
  ]
  node [
    id 425
    label "samotny"
  ]
  node [
    id 426
    label "Danish"
  ]
  node [
    id 427
    label "j&#281;zyk_germa&#324;ski"
  ]
  node [
    id 428
    label "po_du&#324;sku"
  ]
  node [
    id 429
    label "skandynawski"
  ]
  node [
    id 430
    label "j&#281;zyk"
  ]
  node [
    id 431
    label "p&#243;&#322;nocnoeuropejski"
  ]
  node [
    id 432
    label "europejski"
  ]
  node [
    id 433
    label "po_skandynawsku"
  ]
  node [
    id 434
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 435
    label "artykulator"
  ]
  node [
    id 436
    label "kod"
  ]
  node [
    id 437
    label "kawa&#322;ek"
  ]
  node [
    id 438
    label "przedmiot"
  ]
  node [
    id 439
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 440
    label "gramatyka"
  ]
  node [
    id 441
    label "stylik"
  ]
  node [
    id 442
    label "przet&#322;umaczenie"
  ]
  node [
    id 443
    label "formalizowanie"
  ]
  node [
    id 444
    label "ssa&#263;"
  ]
  node [
    id 445
    label "ssanie"
  ]
  node [
    id 446
    label "language"
  ]
  node [
    id 447
    label "liza&#263;"
  ]
  node [
    id 448
    label "napisa&#263;"
  ]
  node [
    id 449
    label "konsonantyzm"
  ]
  node [
    id 450
    label "wokalizm"
  ]
  node [
    id 451
    label "pisa&#263;"
  ]
  node [
    id 452
    label "fonetyka"
  ]
  node [
    id 453
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 454
    label "jeniec"
  ]
  node [
    id 455
    label "but"
  ]
  node [
    id 456
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 457
    label "po_koroniarsku"
  ]
  node [
    id 458
    label "kultura_duchowa"
  ]
  node [
    id 459
    label "t&#322;umaczenie"
  ]
  node [
    id 460
    label "m&#243;wienie"
  ]
  node [
    id 461
    label "pype&#263;"
  ]
  node [
    id 462
    label "lizanie"
  ]
  node [
    id 463
    label "pismo"
  ]
  node [
    id 464
    label "formalizowa&#263;"
  ]
  node [
    id 465
    label "rozumie&#263;"
  ]
  node [
    id 466
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 467
    label "rozumienie"
  ]
  node [
    id 468
    label "makroglosja"
  ]
  node [
    id 469
    label "m&#243;wi&#263;"
  ]
  node [
    id 470
    label "jama_ustna"
  ]
  node [
    id 471
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 472
    label "formacja_geologiczna"
  ]
  node [
    id 473
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 474
    label "natural_language"
  ]
  node [
    id 475
    label "s&#322;ownictwo"
  ]
  node [
    id 476
    label "urz&#261;dzenie"
  ]
  node [
    id 477
    label "Itaka"
  ]
  node [
    id 478
    label "Rugia"
  ]
  node [
    id 479
    label "Uznam"
  ]
  node [
    id 480
    label "Paros"
  ]
  node [
    id 481
    label "Ibiza"
  ]
  node [
    id 482
    label "Cebu"
  ]
  node [
    id 483
    label "Borneo"
  ]
  node [
    id 484
    label "Wolin"
  ]
  node [
    id 485
    label "Okinawa"
  ]
  node [
    id 486
    label "Tobago"
  ]
  node [
    id 487
    label "Barbados"
  ]
  node [
    id 488
    label "Bonaire"
  ]
  node [
    id 489
    label "Zelandia"
  ]
  node [
    id 490
    label "Bali"
  ]
  node [
    id 491
    label "Sint_Eustatius"
  ]
  node [
    id 492
    label "Bornholm"
  ]
  node [
    id 493
    label "Timor"
  ]
  node [
    id 494
    label "Portoryko"
  ]
  node [
    id 495
    label "Gozo"
  ]
  node [
    id 496
    label "Anguilla"
  ]
  node [
    id 497
    label "mebel"
  ]
  node [
    id 498
    label "Tahiti"
  ]
  node [
    id 499
    label "Irlandia"
  ]
  node [
    id 500
    label "Sycylia"
  ]
  node [
    id 501
    label "Trynidad"
  ]
  node [
    id 502
    label "Sardynia"
  ]
  node [
    id 503
    label "Tasmania"
  ]
  node [
    id 504
    label "Tajwan"
  ]
  node [
    id 505
    label "Wielka_Brytania"
  ]
  node [
    id 506
    label "l&#261;d"
  ]
  node [
    id 507
    label "Malta"
  ]
  node [
    id 508
    label "Eubea"
  ]
  node [
    id 509
    label "Nowa_Gwinea"
  ]
  node [
    id 510
    label "Korsyka"
  ]
  node [
    id 511
    label "Grenlandia"
  ]
  node [
    id 512
    label "Madagaskar"
  ]
  node [
    id 513
    label "Rodos"
  ]
  node [
    id 514
    label "Sint_Maarten"
  ]
  node [
    id 515
    label "Cura&#231;ao"
  ]
  node [
    id 516
    label "Man"
  ]
  node [
    id 517
    label "Jawa"
  ]
  node [
    id 518
    label "Sumatra"
  ]
  node [
    id 519
    label "Cuszima"
  ]
  node [
    id 520
    label "Kuba"
  ]
  node [
    id 521
    label "Samotraka"
  ]
  node [
    id 522
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 523
    label "Saba"
  ]
  node [
    id 524
    label "Gotlandia"
  ]
  node [
    id 525
    label "Wyspa_&#346;wi&#281;tej_Heleny"
  ]
  node [
    id 526
    label "Salamina"
  ]
  node [
    id 527
    label "Nowa_Fundlandia"
  ]
  node [
    id 528
    label "Portland"
  ]
  node [
    id 529
    label "Wielka_Bahama"
  ]
  node [
    id 530
    label "Kreta"
  ]
  node [
    id 531
    label "Nowa_Zelandia"
  ]
  node [
    id 532
    label "Palau"
  ]
  node [
    id 533
    label "Cypr"
  ]
  node [
    id 534
    label "Jamajka"
  ]
  node [
    id 535
    label "Eolia"
  ]
  node [
    id 536
    label "obszar"
  ]
  node [
    id 537
    label "Majorka"
  ]
  node [
    id 538
    label "Ajon"
  ]
  node [
    id 539
    label "Cejlon"
  ]
  node [
    id 540
    label "kresom&#243;zgowie"
  ]
  node [
    id 541
    label "Sachalin"
  ]
  node [
    id 542
    label "Helgoland"
  ]
  node [
    id 543
    label "Samos"
  ]
  node [
    id 544
    label "Lesbos"
  ]
  node [
    id 545
    label "Montserrat"
  ]
  node [
    id 546
    label "Ostr&#243;w_Lednicki"
  ]
  node [
    id 547
    label "Haiti"
  ]
  node [
    id 548
    label "cia&#322;o_modzelowate"
  ]
  node [
    id 549
    label "j&#261;dro_podstawne"
  ]
  node [
    id 550
    label "bruzda_przed&#347;rodkowa"
  ]
  node [
    id 551
    label "p&#243;&#322;kula_m&#243;zgu"
  ]
  node [
    id 552
    label "p&#322;at_skroniowy"
  ]
  node [
    id 553
    label "w&#281;chom&#243;zgowie"
  ]
  node [
    id 554
    label "m&#243;zg"
  ]
  node [
    id 555
    label "cerebrum"
  ]
  node [
    id 556
    label "p&#322;at_czo&#322;owy"
  ]
  node [
    id 557
    label "p&#322;at_ciemieniowy"
  ]
  node [
    id 558
    label "p&#322;at_potyliczny"
  ]
  node [
    id 559
    label "ga&#322;ka_blada"
  ]
  node [
    id 560
    label "kresom&#243;zgowie_&#347;rodkowe"
  ]
  node [
    id 561
    label "ramiak"
  ]
  node [
    id 562
    label "przeszklenie"
  ]
  node [
    id 563
    label "obudowanie"
  ]
  node [
    id 564
    label "obudowywa&#263;"
  ]
  node [
    id 565
    label "obudowa&#263;"
  ]
  node [
    id 566
    label "sprz&#281;t"
  ]
  node [
    id 567
    label "gzyms"
  ]
  node [
    id 568
    label "nadstawa"
  ]
  node [
    id 569
    label "element_wyposa&#380;enia"
  ]
  node [
    id 570
    label "obudowywanie"
  ]
  node [
    id 571
    label "umeblowanie"
  ]
  node [
    id 572
    label "p&#243;&#322;noc"
  ]
  node [
    id 573
    label "Kosowo"
  ]
  node [
    id 574
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 575
    label "Zab&#322;ocie"
  ]
  node [
    id 576
    label "zach&#243;d"
  ]
  node [
    id 577
    label "po&#322;udnie"
  ]
  node [
    id 578
    label "Pow&#261;zki"
  ]
  node [
    id 579
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 580
    label "Piotrowo"
  ]
  node [
    id 581
    label "Olszanica"
  ]
  node [
    id 582
    label "Ruda_Pabianicka"
  ]
  node [
    id 583
    label "holarktyka"
  ]
  node [
    id 584
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 585
    label "Ludwin&#243;w"
  ]
  node [
    id 586
    label "Arktyka"
  ]
  node [
    id 587
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 588
    label "Zabu&#380;e"
  ]
  node [
    id 589
    label "antroposfera"
  ]
  node [
    id 590
    label "Neogea"
  ]
  node [
    id 591
    label "terytorium"
  ]
  node [
    id 592
    label "Syberia_Zachodnia"
  ]
  node [
    id 593
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 594
    label "zakres"
  ]
  node [
    id 595
    label "pas_planetoid"
  ]
  node [
    id 596
    label "Syberia_Wschodnia"
  ]
  node [
    id 597
    label "Antarktyka"
  ]
  node [
    id 598
    label "Rakowice"
  ]
  node [
    id 599
    label "akrecja"
  ]
  node [
    id 600
    label "wymiar"
  ]
  node [
    id 601
    label "&#321;&#281;g"
  ]
  node [
    id 602
    label "Kresy_Zachodnie"
  ]
  node [
    id 603
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 604
    label "wsch&#243;d"
  ]
  node [
    id 605
    label "Notogea"
  ]
  node [
    id 606
    label "pojazd"
  ]
  node [
    id 607
    label "skorupa_ziemska"
  ]
  node [
    id 608
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 609
    label "Trynidad_i_Tobago"
  ]
  node [
    id 610
    label "Ocean_Indyjski"
  ]
  node [
    id 611
    label "Australia"
  ]
  node [
    id 612
    label "Filipiny"
  ]
  node [
    id 613
    label "dolar"
  ]
  node [
    id 614
    label "Karaiby"
  ]
  node [
    id 615
    label "W&#322;ochy"
  ]
  node [
    id 616
    label "Antyle_Holenderskie"
  ]
  node [
    id 617
    label "Wyspy_Jo&#324;skie"
  ]
  node [
    id 618
    label "ibiza"
  ]
  node [
    id 619
    label "Seat"
  ]
  node [
    id 620
    label "Polska"
  ]
  node [
    id 621
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 622
    label "Grecja"
  ]
  node [
    id 623
    label "dolar_Barbadosu"
  ]
  node [
    id 624
    label "Antyle"
  ]
  node [
    id 625
    label "Atlantyk"
  ]
  node [
    id 626
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 627
    label "Ulster"
  ]
  node [
    id 628
    label "funt_irlandzki"
  ]
  node [
    id 629
    label "strefa_euro"
  ]
  node [
    id 630
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 631
    label "Holandia"
  ]
  node [
    id 632
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 633
    label "Pozna&#324;"
  ]
  node [
    id 634
    label "lira_malta&#324;ska"
  ]
  node [
    id 635
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 636
    label "Hiszpania"
  ]
  node [
    id 637
    label "Ahlbeck"
  ]
  node [
    id 638
    label "Niemcy"
  ]
  node [
    id 639
    label "Oceania"
  ]
  node [
    id 640
    label "Szwecja"
  ]
  node [
    id 641
    label "funt_cypryjski"
  ]
  node [
    id 642
    label "Afrodyzje"
  ]
  node [
    id 643
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 644
    label "Anglia"
  ]
  node [
    id 645
    label "Ocean_Spokojny"
  ]
  node [
    id 646
    label "Azja_Wschodnia"
  ]
  node [
    id 647
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 648
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 649
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 650
    label "peso_kuba&#324;skie"
  ]
  node [
    id 651
    label "Dania"
  ]
  node [
    id 652
    label "lodowiec_kontynentalny"
  ]
  node [
    id 653
    label "nearktyka"
  ]
  node [
    id 654
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 655
    label "kraj_nordycki"
  ]
  node [
    id 656
    label "Japonia"
  ]
  node [
    id 657
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 658
    label "gourde"
  ]
  node [
    id 659
    label "Dominikana"
  ]
  node [
    id 660
    label "Malezja"
  ]
  node [
    id 661
    label "Brunei"
  ]
  node [
    id 662
    label "Archipelag_Malajski"
  ]
  node [
    id 663
    label "Indonezja"
  ]
  node [
    id 664
    label "Azja_Mniejsza"
  ]
  node [
    id 665
    label "dolar_wschodniokaraibski"
  ]
  node [
    id 666
    label "ariary"
  ]
  node [
    id 667
    label "Afryka_Wschodnia"
  ]
  node [
    id 668
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 669
    label "frank_malgaski"
  ]
  node [
    id 670
    label "Rosja"
  ]
  node [
    id 671
    label "kafar"
  ]
  node [
    id 672
    label "dolar_jamajski"
  ]
  node [
    id 673
    label "Francja"
  ]
  node [
    id 674
    label "Sundajczyk"
  ]
  node [
    id 675
    label "Morze_Egejskie"
  ]
  node [
    id 676
    label "uniwersytet"
  ]
  node [
    id 677
    label "techniczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 676
    target 677
  ]
]
