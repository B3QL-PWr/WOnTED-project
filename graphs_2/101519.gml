graph [
  node [
    id 0
    label "wtedy"
    origin "text"
  ]
  node [
    id 1
    label "dusza"
    origin "text"
  ]
  node [
    id 2
    label "strach"
    origin "text"
  ]
  node [
    id 3
    label "zakrzep&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "ten"
    origin "text"
  ]
  node [
    id 5
    label "lit"
    origin "text"
  ]
  node [
    id 6
    label "kamie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "kiedy&#347;"
  ]
  node [
    id 8
    label "piek&#322;o"
  ]
  node [
    id 9
    label "&#380;elazko"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "pi&#243;ro"
  ]
  node [
    id 12
    label "odwaga"
  ]
  node [
    id 13
    label "sfera_afektywna"
  ]
  node [
    id 14
    label "deformowanie"
  ]
  node [
    id 15
    label "core"
  ]
  node [
    id 16
    label "mind"
  ]
  node [
    id 17
    label "sumienie"
  ]
  node [
    id 18
    label "sztabka"
  ]
  node [
    id 19
    label "deformowa&#263;"
  ]
  node [
    id 20
    label "rdze&#324;"
  ]
  node [
    id 21
    label "osobowo&#347;&#263;"
  ]
  node [
    id 22
    label "schody"
  ]
  node [
    id 23
    label "pupa"
  ]
  node [
    id 24
    label "sztuka"
  ]
  node [
    id 25
    label "klocek"
  ]
  node [
    id 26
    label "instrument_smyczkowy"
  ]
  node [
    id 27
    label "seksualno&#347;&#263;"
  ]
  node [
    id 28
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 29
    label "byt"
  ]
  node [
    id 30
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 31
    label "lina"
  ]
  node [
    id 32
    label "ego"
  ]
  node [
    id 33
    label "charakter"
  ]
  node [
    id 34
    label "kompleks"
  ]
  node [
    id 35
    label "shape"
  ]
  node [
    id 36
    label "motor"
  ]
  node [
    id 37
    label "mikrokosmos"
  ]
  node [
    id 38
    label "przestrze&#324;"
  ]
  node [
    id 39
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 40
    label "mi&#281;kisz"
  ]
  node [
    id 41
    label "marrow"
  ]
  node [
    id 42
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 43
    label "rozdzielanie"
  ]
  node [
    id 44
    label "bezbrze&#380;e"
  ]
  node [
    id 45
    label "punkt"
  ]
  node [
    id 46
    label "czasoprzestrze&#324;"
  ]
  node [
    id 47
    label "zbi&#243;r"
  ]
  node [
    id 48
    label "niezmierzony"
  ]
  node [
    id 49
    label "przedzielenie"
  ]
  node [
    id 50
    label "nielito&#347;ciwy"
  ]
  node [
    id 51
    label "rozdziela&#263;"
  ]
  node [
    id 52
    label "oktant"
  ]
  node [
    id 53
    label "miejsce"
  ]
  node [
    id 54
    label "przedzieli&#263;"
  ]
  node [
    id 55
    label "przestw&#243;r"
  ]
  node [
    id 56
    label "ka&#322;"
  ]
  node [
    id 57
    label "k&#322;oda"
  ]
  node [
    id 58
    label "zabawka"
  ]
  node [
    id 59
    label "reaktor_j&#261;drowy"
  ]
  node [
    id 60
    label "magnes"
  ]
  node [
    id 61
    label "morfem"
  ]
  node [
    id 62
    label "spowalniacz"
  ]
  node [
    id 63
    label "transformator"
  ]
  node [
    id 64
    label "pocisk"
  ]
  node [
    id 65
    label "pr&#281;t_kontrolny"
  ]
  node [
    id 66
    label "wn&#281;trze"
  ]
  node [
    id 67
    label "istota"
  ]
  node [
    id 68
    label "procesor"
  ]
  node [
    id 69
    label "odlewnictwo"
  ]
  node [
    id 70
    label "ch&#322;odziwo"
  ]
  node [
    id 71
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 72
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 73
    label "forma"
  ]
  node [
    id 74
    label "surowiak"
  ]
  node [
    id 75
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 76
    label "cake"
  ]
  node [
    id 77
    label "atom"
  ]
  node [
    id 78
    label "odbicie"
  ]
  node [
    id 79
    label "przyroda"
  ]
  node [
    id 80
    label "Ziemia"
  ]
  node [
    id 81
    label "kosmos"
  ]
  node [
    id 82
    label "miniatura"
  ]
  node [
    id 83
    label "mi&#261;&#380;sz"
  ]
  node [
    id 84
    label "tkanka_sta&#322;a"
  ]
  node [
    id 85
    label "parenchyma"
  ]
  node [
    id 86
    label "perycykl"
  ]
  node [
    id 87
    label "utrzymywanie"
  ]
  node [
    id 88
    label "bycie"
  ]
  node [
    id 89
    label "entity"
  ]
  node [
    id 90
    label "subsystencja"
  ]
  node [
    id 91
    label "utrzyma&#263;"
  ]
  node [
    id 92
    label "egzystencja"
  ]
  node [
    id 93
    label "wy&#380;ywienie"
  ]
  node [
    id 94
    label "ontologicznie"
  ]
  node [
    id 95
    label "utrzymanie"
  ]
  node [
    id 96
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 97
    label "potencja"
  ]
  node [
    id 98
    label "utrzymywa&#263;"
  ]
  node [
    id 99
    label "biblioteka"
  ]
  node [
    id 100
    label "pojazd_drogowy"
  ]
  node [
    id 101
    label "wyci&#261;garka"
  ]
  node [
    id 102
    label "gondola_silnikowa"
  ]
  node [
    id 103
    label "aerosanie"
  ]
  node [
    id 104
    label "dwuko&#322;owiec"
  ]
  node [
    id 105
    label "wiatrochron"
  ]
  node [
    id 106
    label "rz&#281;&#380;enie"
  ]
  node [
    id 107
    label "podgrzewacz"
  ]
  node [
    id 108
    label "wirnik"
  ]
  node [
    id 109
    label "kosz"
  ]
  node [
    id 110
    label "motogodzina"
  ]
  node [
    id 111
    label "&#322;a&#324;cuch"
  ]
  node [
    id 112
    label "motoszybowiec"
  ]
  node [
    id 113
    label "program"
  ]
  node [
    id 114
    label "gniazdo_zaworowe"
  ]
  node [
    id 115
    label "mechanizm"
  ]
  node [
    id 116
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 117
    label "engine"
  ]
  node [
    id 118
    label "dociera&#263;"
  ]
  node [
    id 119
    label "samoch&#243;d"
  ]
  node [
    id 120
    label "dotarcie"
  ]
  node [
    id 121
    label "nap&#281;d"
  ]
  node [
    id 122
    label "motor&#243;wka"
  ]
  node [
    id 123
    label "rz&#281;zi&#263;"
  ]
  node [
    id 124
    label "czynnik"
  ]
  node [
    id 125
    label "perpetuum_mobile"
  ]
  node [
    id 126
    label "kierownica"
  ]
  node [
    id 127
    label "docieranie"
  ]
  node [
    id 128
    label "bombowiec"
  ]
  node [
    id 129
    label "dotrze&#263;"
  ]
  node [
    id 130
    label "radiator"
  ]
  node [
    id 131
    label "przedmiot"
  ]
  node [
    id 132
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 133
    label "wydarzenie"
  ]
  node [
    id 134
    label "psychika"
  ]
  node [
    id 135
    label "posta&#263;"
  ]
  node [
    id 136
    label "kompleksja"
  ]
  node [
    id 137
    label "fizjonomia"
  ]
  node [
    id 138
    label "zjawisko"
  ]
  node [
    id 139
    label "cecha"
  ]
  node [
    id 140
    label "pr&#243;bowanie"
  ]
  node [
    id 141
    label "rola"
  ]
  node [
    id 142
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 143
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 144
    label "realizacja"
  ]
  node [
    id 145
    label "scena"
  ]
  node [
    id 146
    label "didaskalia"
  ]
  node [
    id 147
    label "czyn"
  ]
  node [
    id 148
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 149
    label "environment"
  ]
  node [
    id 150
    label "head"
  ]
  node [
    id 151
    label "scenariusz"
  ]
  node [
    id 152
    label "egzemplarz"
  ]
  node [
    id 153
    label "jednostka"
  ]
  node [
    id 154
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 155
    label "utw&#243;r"
  ]
  node [
    id 156
    label "kultura_duchowa"
  ]
  node [
    id 157
    label "fortel"
  ]
  node [
    id 158
    label "theatrical_performance"
  ]
  node [
    id 159
    label "ambala&#380;"
  ]
  node [
    id 160
    label "sprawno&#347;&#263;"
  ]
  node [
    id 161
    label "kobieta"
  ]
  node [
    id 162
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 163
    label "Faust"
  ]
  node [
    id 164
    label "scenografia"
  ]
  node [
    id 165
    label "ods&#322;ona"
  ]
  node [
    id 166
    label "turn"
  ]
  node [
    id 167
    label "pokaz"
  ]
  node [
    id 168
    label "ilo&#347;&#263;"
  ]
  node [
    id 169
    label "przedstawienie"
  ]
  node [
    id 170
    label "przedstawi&#263;"
  ]
  node [
    id 171
    label "Apollo"
  ]
  node [
    id 172
    label "kultura"
  ]
  node [
    id 173
    label "przedstawianie"
  ]
  node [
    id 174
    label "przedstawia&#263;"
  ]
  node [
    id 175
    label "towar"
  ]
  node [
    id 176
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 177
    label "napotka&#263;"
  ]
  node [
    id 178
    label "subiekcja"
  ]
  node [
    id 179
    label "akrobacja_lotnicza"
  ]
  node [
    id 180
    label "balustrada"
  ]
  node [
    id 181
    label "k&#322;opotliwy"
  ]
  node [
    id 182
    label "napotkanie"
  ]
  node [
    id 183
    label "stopie&#324;"
  ]
  node [
    id 184
    label "obstacle"
  ]
  node [
    id 185
    label "gradation"
  ]
  node [
    id 186
    label "przycie&#347;"
  ]
  node [
    id 187
    label "klatka_schodowa"
  ]
  node [
    id 188
    label "konstrukcja"
  ]
  node [
    id 189
    label "sytuacja"
  ]
  node [
    id 190
    label "wyluzowanie"
  ]
  node [
    id 191
    label "skr&#281;tka"
  ]
  node [
    id 192
    label "pika-pina"
  ]
  node [
    id 193
    label "bom"
  ]
  node [
    id 194
    label "abaka"
  ]
  node [
    id 195
    label "wyluzowa&#263;"
  ]
  node [
    id 196
    label "stal&#243;wka"
  ]
  node [
    id 197
    label "wyrostek"
  ]
  node [
    id 198
    label "stylo"
  ]
  node [
    id 199
    label "przybory_do_pisania"
  ]
  node [
    id 200
    label "obsadka"
  ]
  node [
    id 201
    label "ptak"
  ]
  node [
    id 202
    label "wypisanie"
  ]
  node [
    id 203
    label "pir&#243;g"
  ]
  node [
    id 204
    label "pierze"
  ]
  node [
    id 205
    label "wypisa&#263;"
  ]
  node [
    id 206
    label "pisarstwo"
  ]
  node [
    id 207
    label "element"
  ]
  node [
    id 208
    label "element_anatomiczny"
  ]
  node [
    id 209
    label "autor"
  ]
  node [
    id 210
    label "artyku&#322;"
  ]
  node [
    id 211
    label "p&#322;askownik"
  ]
  node [
    id 212
    label "upierzenie"
  ]
  node [
    id 213
    label "atrament"
  ]
  node [
    id 214
    label "magierka"
  ]
  node [
    id 215
    label "quill"
  ]
  node [
    id 216
    label "pi&#243;ropusz"
  ]
  node [
    id 217
    label "stosina"
  ]
  node [
    id 218
    label "wyst&#281;p"
  ]
  node [
    id 219
    label "g&#322;ownia"
  ]
  node [
    id 220
    label "resor_pi&#243;rowy"
  ]
  node [
    id 221
    label "pen"
  ]
  node [
    id 222
    label "sprz&#281;t_AGD"
  ]
  node [
    id 223
    label "urz&#261;dzenie_domowe"
  ]
  node [
    id 224
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 225
    label "stopa"
  ]
  node [
    id 226
    label "prasowa&#263;"
  ]
  node [
    id 227
    label "mentalno&#347;&#263;"
  ]
  node [
    id 228
    label "podmiot"
  ]
  node [
    id 229
    label "superego"
  ]
  node [
    id 230
    label "wyj&#261;tkowy"
  ]
  node [
    id 231
    label "self"
  ]
  node [
    id 232
    label "ludzko&#347;&#263;"
  ]
  node [
    id 233
    label "asymilowanie"
  ]
  node [
    id 234
    label "wapniak"
  ]
  node [
    id 235
    label "asymilowa&#263;"
  ]
  node [
    id 236
    label "os&#322;abia&#263;"
  ]
  node [
    id 237
    label "hominid"
  ]
  node [
    id 238
    label "podw&#322;adny"
  ]
  node [
    id 239
    label "os&#322;abianie"
  ]
  node [
    id 240
    label "g&#322;owa"
  ]
  node [
    id 241
    label "figura"
  ]
  node [
    id 242
    label "portrecista"
  ]
  node [
    id 243
    label "dwun&#243;g"
  ]
  node [
    id 244
    label "profanum"
  ]
  node [
    id 245
    label "nasada"
  ]
  node [
    id 246
    label "duch"
  ]
  node [
    id 247
    label "antropochoria"
  ]
  node [
    id 248
    label "osoba"
  ]
  node [
    id 249
    label "wz&#243;r"
  ]
  node [
    id 250
    label "senior"
  ]
  node [
    id 251
    label "oddzia&#322;ywanie"
  ]
  node [
    id 252
    label "Adam"
  ]
  node [
    id 253
    label "homo_sapiens"
  ]
  node [
    id 254
    label "polifag"
  ]
  node [
    id 255
    label "courage"
  ]
  node [
    id 256
    label "stan"
  ]
  node [
    id 257
    label "Freud"
  ]
  node [
    id 258
    label "psychoanaliza"
  ]
  node [
    id 259
    label "sempiterna"
  ]
  node [
    id 260
    label "ty&#322;"
  ]
  node [
    id 261
    label "dupa"
  ]
  node [
    id 262
    label "tu&#322;&#243;w"
  ]
  node [
    id 263
    label "poczucie_w&#322;asnej_warto&#347;ci"
  ]
  node [
    id 264
    label "_id"
  ]
  node [
    id 265
    label "ignorantness"
  ]
  node [
    id 266
    label "niewiedza"
  ]
  node [
    id 267
    label "unconsciousness"
  ]
  node [
    id 268
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 269
    label "zmienianie"
  ]
  node [
    id 270
    label "distortion"
  ]
  node [
    id 271
    label "contortion"
  ]
  node [
    id 272
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 273
    label "zmienia&#263;"
  ]
  node [
    id 274
    label "corrupt"
  ]
  node [
    id 275
    label "struktura"
  ]
  node [
    id 276
    label "group"
  ]
  node [
    id 277
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 278
    label "ligand"
  ]
  node [
    id 279
    label "sum"
  ]
  node [
    id 280
    label "band"
  ]
  node [
    id 281
    label "wi&#261;zanie_koordynacyjne"
  ]
  node [
    id 282
    label "pokutowanie"
  ]
  node [
    id 283
    label "szeol"
  ]
  node [
    id 284
    label "za&#347;wiaty"
  ]
  node [
    id 285
    label "horror"
  ]
  node [
    id 286
    label "spirit"
  ]
  node [
    id 287
    label "emocja"
  ]
  node [
    id 288
    label "zjawa"
  ]
  node [
    id 289
    label "straszyd&#322;o"
  ]
  node [
    id 290
    label "zastraszanie"
  ]
  node [
    id 291
    label "phobia"
  ]
  node [
    id 292
    label "zastraszenie"
  ]
  node [
    id 293
    label "akatyzja"
  ]
  node [
    id 294
    label "ba&#263;_si&#281;"
  ]
  node [
    id 295
    label "zesp&#243;&#322;_chorobowy"
  ]
  node [
    id 296
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 297
    label "ogrom"
  ]
  node [
    id 298
    label "iskrzy&#263;"
  ]
  node [
    id 299
    label "d&#322;awi&#263;"
  ]
  node [
    id 300
    label "ostygn&#261;&#263;"
  ]
  node [
    id 301
    label "stygn&#261;&#263;"
  ]
  node [
    id 302
    label "temperatura"
  ]
  node [
    id 303
    label "wpa&#347;&#263;"
  ]
  node [
    id 304
    label "afekt"
  ]
  node [
    id 305
    label "wpada&#263;"
  ]
  node [
    id 306
    label "stw&#243;r"
  ]
  node [
    id 307
    label "szkarada"
  ]
  node [
    id 308
    label "istota_fantastyczna"
  ]
  node [
    id 309
    label "refleksja"
  ]
  node [
    id 310
    label "widziad&#322;o"
  ]
  node [
    id 311
    label "l&#281;k"
  ]
  node [
    id 312
    label "bullying"
  ]
  node [
    id 313
    label "oddzia&#322;anie"
  ]
  node [
    id 314
    label "presja"
  ]
  node [
    id 315
    label "stwardnia&#322;y"
  ]
  node [
    id 316
    label "sta&#322;y"
  ]
  node [
    id 317
    label "nieruchomy"
  ]
  node [
    id 318
    label "nieruchomo"
  ]
  node [
    id 319
    label "unieruchamianie"
  ]
  node [
    id 320
    label "stacjonarnie"
  ]
  node [
    id 321
    label "unieruchomienie"
  ]
  node [
    id 322
    label "nieruchomienie"
  ]
  node [
    id 323
    label "znieruchomienie"
  ]
  node [
    id 324
    label "surowy"
  ]
  node [
    id 325
    label "twardy"
  ]
  node [
    id 326
    label "regularny"
  ]
  node [
    id 327
    label "jednakowy"
  ]
  node [
    id 328
    label "zwyk&#322;y"
  ]
  node [
    id 329
    label "stale"
  ]
  node [
    id 330
    label "okre&#347;lony"
  ]
  node [
    id 331
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 332
    label "wiadomy"
  ]
  node [
    id 333
    label "jednostka_monetarna"
  ]
  node [
    id 334
    label "Litwa"
  ]
  node [
    id 335
    label "lithium"
  ]
  node [
    id 336
    label "cent"
  ]
  node [
    id 337
    label "litowiec"
  ]
  node [
    id 338
    label "metal"
  ]
  node [
    id 339
    label "pierwiastek"
  ]
  node [
    id 340
    label "moneta"
  ]
  node [
    id 341
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 342
    label "Unia_Europejska"
  ]
  node [
    id 343
    label "Wile&#324;szczyzna"
  ]
  node [
    id 344
    label "Windawa"
  ]
  node [
    id 345
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 346
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 347
    label "&#379;mud&#378;"
  ]
  node [
    id 348
    label "NATO"
  ]
  node [
    id 349
    label "Had&#380;ar"
  ]
  node [
    id 350
    label "kamienienie"
  ]
  node [
    id 351
    label "oczko"
  ]
  node [
    id 352
    label "ska&#322;a"
  ]
  node [
    id 353
    label "osad"
  ]
  node [
    id 354
    label "ci&#281;&#380;ar"
  ]
  node [
    id 355
    label "p&#322;ytka"
  ]
  node [
    id 356
    label "skamienienie"
  ]
  node [
    id 357
    label "cube"
  ]
  node [
    id 358
    label "funt"
  ]
  node [
    id 359
    label "mad&#380;ong"
  ]
  node [
    id 360
    label "tworzywo"
  ]
  node [
    id 361
    label "jednostka_avoirdupois"
  ]
  node [
    id 362
    label "domino"
  ]
  node [
    id 363
    label "rock"
  ]
  node [
    id 364
    label "z&#322;&#243;g"
  ]
  node [
    id 365
    label "lapidarium"
  ]
  node [
    id 366
    label "autografia"
  ]
  node [
    id 367
    label "rekwizyt_do_gry"
  ]
  node [
    id 368
    label "minera&#322;_barwny"
  ]
  node [
    id 369
    label "sedymentacja"
  ]
  node [
    id 370
    label "kompakcja"
  ]
  node [
    id 371
    label "terygeniczny"
  ]
  node [
    id 372
    label "wspomnienie"
  ]
  node [
    id 373
    label "warstwa"
  ]
  node [
    id 374
    label "kolmatacja"
  ]
  node [
    id 375
    label "deposit"
  ]
  node [
    id 376
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 377
    label "uskoczenie"
  ]
  node [
    id 378
    label "mieszanina"
  ]
  node [
    id 379
    label "zmetamorfizowanie"
  ]
  node [
    id 380
    label "soczewa"
  ]
  node [
    id 381
    label "opoka"
  ]
  node [
    id 382
    label "uskakiwa&#263;"
  ]
  node [
    id 383
    label "sklerometr"
  ]
  node [
    id 384
    label "&#322;upkowato&#347;&#263;"
  ]
  node [
    id 385
    label "uskakiwanie"
  ]
  node [
    id 386
    label "uskoczy&#263;"
  ]
  node [
    id 387
    label "obiekt"
  ]
  node [
    id 388
    label "porwak"
  ]
  node [
    id 389
    label "bloczno&#347;&#263;"
  ]
  node [
    id 390
    label "zmetamorfizowa&#263;"
  ]
  node [
    id 391
    label "lepiszcze_skalne"
  ]
  node [
    id 392
    label "rygiel"
  ]
  node [
    id 393
    label "lamina"
  ]
  node [
    id 394
    label "wyg&#322;ad_lodowcowy"
  ]
  node [
    id 395
    label "blaszka"
  ]
  node [
    id 396
    label "plate"
  ]
  node [
    id 397
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 398
    label "p&#322;yta"
  ]
  node [
    id 399
    label "dysk_optyczny"
  ]
  node [
    id 400
    label "zmiana"
  ]
  node [
    id 401
    label "warto&#347;&#263;"
  ]
  node [
    id 402
    label "przeszkoda"
  ]
  node [
    id 403
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 404
    label "hantla"
  ]
  node [
    id 405
    label "hazard"
  ]
  node [
    id 406
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 407
    label "zawa&#380;y&#263;"
  ]
  node [
    id 408
    label "wym&#243;g"
  ]
  node [
    id 409
    label "obarczy&#263;"
  ]
  node [
    id 410
    label "zawa&#380;enie"
  ]
  node [
    id 411
    label "weight"
  ]
  node [
    id 412
    label "powinno&#347;&#263;"
  ]
  node [
    id 413
    label "load"
  ]
  node [
    id 414
    label "substancja"
  ]
  node [
    id 415
    label "pens_brytyjski"
  ]
  node [
    id 416
    label "Falklandy"
  ]
  node [
    id 417
    label "Wielka_Brytania"
  ]
  node [
    id 418
    label "Wyspa_Man"
  ]
  node [
    id 419
    label "uncja"
  ]
  node [
    id 420
    label "cetnar"
  ]
  node [
    id 421
    label "Wyspa_&#346;wi&#281;tej_Heleny"
  ]
  node [
    id 422
    label "Guernsey"
  ]
  node [
    id 423
    label "marka"
  ]
  node [
    id 424
    label "haczyk"
  ]
  node [
    id 425
    label "&#347;cieg"
  ]
  node [
    id 426
    label "ekoton"
  ]
  node [
    id 427
    label "staw"
  ]
  node [
    id 428
    label "rzecz"
  ]
  node [
    id 429
    label "ozdoba"
  ]
  node [
    id 430
    label "ladder"
  ]
  node [
    id 431
    label "szczep"
  ]
  node [
    id 432
    label "stawon&#243;g"
  ]
  node [
    id 433
    label "p&#281;telka"
  ]
  node [
    id 434
    label "organ"
  ]
  node [
    id 435
    label "eye"
  ]
  node [
    id 436
    label "czcionka"
  ]
  node [
    id 437
    label "k&#243;&#322;ko"
  ]
  node [
    id 438
    label "pier&#347;cionek"
  ]
  node [
    id 439
    label "p&#261;k"
  ]
  node [
    id 440
    label "ros&#243;&#322;"
  ]
  node [
    id 441
    label "kostka"
  ]
  node [
    id 442
    label "wypowied&#378;"
  ]
  node [
    id 443
    label "uk&#322;ad"
  ]
  node [
    id 444
    label "oko"
  ]
  node [
    id 445
    label "ziemniak"
  ]
  node [
    id 446
    label "ogr&#243;d_wodny"
  ]
  node [
    id 447
    label "gra_w_karty"
  ]
  node [
    id 448
    label "muzeum"
  ]
  node [
    id 449
    label "gra_towarzyska"
  ]
  node [
    id 450
    label "gra"
  ]
  node [
    id 451
    label "kaptur"
  ]
  node [
    id 452
    label "p&#322;aszcz"
  ]
  node [
    id 453
    label "kostium"
  ]
  node [
    id 454
    label "oboj&#281;tnienie"
  ]
  node [
    id 455
    label "twardnienie"
  ]
  node [
    id 456
    label "petrifaction"
  ]
  node [
    id 457
    label "stawanie_si&#281;"
  ]
  node [
    id 458
    label "technika_litograficzna"
  ]
  node [
    id 459
    label "napis"
  ]
  node [
    id 460
    label "reprodukcja"
  ]
  node [
    id 461
    label "przenoszenie"
  ]
  node [
    id 462
    label "zoboj&#281;tnienie"
  ]
  node [
    id 463
    label "stwardnienie"
  ]
  node [
    id 464
    label "stanie_si&#281;"
  ]
  node [
    id 465
    label "riff"
  ]
  node [
    id 466
    label "muzyka_rozrywkowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
]
