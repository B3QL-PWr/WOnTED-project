graph [
  node [
    id 0
    label "rpo"
    origin "text"
  ]
  node [
    id 1
    label "analizowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dewiza"
    origin "text"
  ]
  node [
    id 3
    label "powinny"
    origin "text"
  ]
  node [
    id 4
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "dokument"
    origin "text"
  ]
  node [
    id 7
    label "consider"
  ]
  node [
    id 8
    label "badany"
  ]
  node [
    id 9
    label "poddawa&#263;"
  ]
  node [
    id 10
    label "bada&#263;"
  ]
  node [
    id 11
    label "rozpatrywa&#263;"
  ]
  node [
    id 12
    label "podpowiada&#263;"
  ]
  node [
    id 13
    label "render"
  ]
  node [
    id 14
    label "decydowa&#263;"
  ]
  node [
    id 15
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 16
    label "rezygnowa&#263;"
  ]
  node [
    id 17
    label "use"
  ]
  node [
    id 18
    label "sprawdza&#263;"
  ]
  node [
    id 19
    label "poznawa&#263;"
  ]
  node [
    id 20
    label "sondowa&#263;"
  ]
  node [
    id 21
    label "robi&#263;"
  ]
  node [
    id 22
    label "wypytywa&#263;"
  ]
  node [
    id 23
    label "examine"
  ]
  node [
    id 24
    label "przeprowadza&#263;"
  ]
  node [
    id 25
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 26
    label "pacjent"
  ]
  node [
    id 27
    label "uczestnik"
  ]
  node [
    id 28
    label "zasada"
  ]
  node [
    id 29
    label "powiedzenie"
  ]
  node [
    id 30
    label "idea"
  ]
  node [
    id 31
    label "rozwleczenie"
  ]
  node [
    id 32
    label "wyznanie"
  ]
  node [
    id 33
    label "przepowiedzenie"
  ]
  node [
    id 34
    label "podanie"
  ]
  node [
    id 35
    label "wydanie"
  ]
  node [
    id 36
    label "wypowiedzenie"
  ]
  node [
    id 37
    label "zapeszenie"
  ]
  node [
    id 38
    label "wypowied&#378;"
  ]
  node [
    id 39
    label "dodanie"
  ]
  node [
    id 40
    label "wydobycie"
  ]
  node [
    id 41
    label "proverb"
  ]
  node [
    id 42
    label "ozwanie_si&#281;"
  ]
  node [
    id 43
    label "nazwanie"
  ]
  node [
    id 44
    label "statement"
  ]
  node [
    id 45
    label "notification"
  ]
  node [
    id 46
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 47
    label "wyra&#380;enie"
  ]
  node [
    id 48
    label "doprowadzenie"
  ]
  node [
    id 49
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 50
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 51
    label "regu&#322;a_Allena"
  ]
  node [
    id 52
    label "base"
  ]
  node [
    id 53
    label "umowa"
  ]
  node [
    id 54
    label "obserwacja"
  ]
  node [
    id 55
    label "zasada_d'Alemberta"
  ]
  node [
    id 56
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 57
    label "normalizacja"
  ]
  node [
    id 58
    label "moralno&#347;&#263;"
  ]
  node [
    id 59
    label "criterion"
  ]
  node [
    id 60
    label "opis"
  ]
  node [
    id 61
    label "regu&#322;a_Glogera"
  ]
  node [
    id 62
    label "prawo_Mendla"
  ]
  node [
    id 63
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 64
    label "twierdzenie"
  ]
  node [
    id 65
    label "prawo"
  ]
  node [
    id 66
    label "standard"
  ]
  node [
    id 67
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 68
    label "spos&#243;b"
  ]
  node [
    id 69
    label "dominion"
  ]
  node [
    id 70
    label "qualification"
  ]
  node [
    id 71
    label "occupation"
  ]
  node [
    id 72
    label "podstawa"
  ]
  node [
    id 73
    label "substancja"
  ]
  node [
    id 74
    label "prawid&#322;o"
  ]
  node [
    id 75
    label "ideologia"
  ]
  node [
    id 76
    label "byt"
  ]
  node [
    id 77
    label "intelekt"
  ]
  node [
    id 78
    label "Kant"
  ]
  node [
    id 79
    label "p&#322;&#243;d"
  ]
  node [
    id 80
    label "cel"
  ]
  node [
    id 81
    label "poj&#281;cie"
  ]
  node [
    id 82
    label "istota"
  ]
  node [
    id 83
    label "pomys&#322;"
  ]
  node [
    id 84
    label "ideacja"
  ]
  node [
    id 85
    label "nale&#380;ny"
  ]
  node [
    id 86
    label "nale&#380;nie"
  ]
  node [
    id 87
    label "nale&#380;yty"
  ]
  node [
    id 88
    label "godny"
  ]
  node [
    id 89
    label "przynale&#380;ny"
  ]
  node [
    id 90
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 91
    label "pozyska&#263;"
  ]
  node [
    id 92
    label "oceni&#263;"
  ]
  node [
    id 93
    label "devise"
  ]
  node [
    id 94
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 95
    label "dozna&#263;"
  ]
  node [
    id 96
    label "wykry&#263;"
  ]
  node [
    id 97
    label "odzyska&#263;"
  ]
  node [
    id 98
    label "znaj&#347;&#263;"
  ]
  node [
    id 99
    label "invent"
  ]
  node [
    id 100
    label "wymy&#347;li&#263;"
  ]
  node [
    id 101
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 102
    label "stage"
  ]
  node [
    id 103
    label "uzyska&#263;"
  ]
  node [
    id 104
    label "wytworzy&#263;"
  ]
  node [
    id 105
    label "give_birth"
  ]
  node [
    id 106
    label "feel"
  ]
  node [
    id 107
    label "discover"
  ]
  node [
    id 108
    label "okre&#347;li&#263;"
  ]
  node [
    id 109
    label "dostrzec"
  ]
  node [
    id 110
    label "odkry&#263;"
  ]
  node [
    id 111
    label "concoct"
  ]
  node [
    id 112
    label "sta&#263;_si&#281;"
  ]
  node [
    id 113
    label "zrobi&#263;"
  ]
  node [
    id 114
    label "recapture"
  ]
  node [
    id 115
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 116
    label "visualize"
  ]
  node [
    id 117
    label "wystawi&#263;"
  ]
  node [
    id 118
    label "evaluate"
  ]
  node [
    id 119
    label "pomy&#347;le&#263;"
  ]
  node [
    id 120
    label "zapis"
  ]
  node [
    id 121
    label "&#347;wiadectwo"
  ]
  node [
    id 122
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 123
    label "wytw&#243;r"
  ]
  node [
    id 124
    label "parafa"
  ]
  node [
    id 125
    label "plik"
  ]
  node [
    id 126
    label "raport&#243;wka"
  ]
  node [
    id 127
    label "utw&#243;r"
  ]
  node [
    id 128
    label "record"
  ]
  node [
    id 129
    label "registratura"
  ]
  node [
    id 130
    label "dokumentacja"
  ]
  node [
    id 131
    label "fascyku&#322;"
  ]
  node [
    id 132
    label "artyku&#322;"
  ]
  node [
    id 133
    label "writing"
  ]
  node [
    id 134
    label "sygnatariusz"
  ]
  node [
    id 135
    label "dow&#243;d"
  ]
  node [
    id 136
    label "o&#347;wiadczenie"
  ]
  node [
    id 137
    label "za&#347;wiadczenie"
  ]
  node [
    id 138
    label "certificate"
  ]
  node [
    id 139
    label "promocja"
  ]
  node [
    id 140
    label "entrance"
  ]
  node [
    id 141
    label "czynno&#347;&#263;"
  ]
  node [
    id 142
    label "wpis"
  ]
  node [
    id 143
    label "obrazowanie"
  ]
  node [
    id 144
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 145
    label "organ"
  ]
  node [
    id 146
    label "tre&#347;&#263;"
  ]
  node [
    id 147
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 148
    label "part"
  ]
  node [
    id 149
    label "element_anatomiczny"
  ]
  node [
    id 150
    label "tekst"
  ]
  node [
    id 151
    label "komunikat"
  ]
  node [
    id 152
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 153
    label "przedmiot"
  ]
  node [
    id 154
    label "work"
  ]
  node [
    id 155
    label "rezultat"
  ]
  node [
    id 156
    label "podkatalog"
  ]
  node [
    id 157
    label "nadpisa&#263;"
  ]
  node [
    id 158
    label "nadpisanie"
  ]
  node [
    id 159
    label "bundle"
  ]
  node [
    id 160
    label "folder"
  ]
  node [
    id 161
    label "nadpisywanie"
  ]
  node [
    id 162
    label "paczka"
  ]
  node [
    id 163
    label "nadpisywa&#263;"
  ]
  node [
    id 164
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 165
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 166
    label "przedstawiciel"
  ]
  node [
    id 167
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 168
    label "biuro"
  ]
  node [
    id 169
    label "register"
  ]
  node [
    id 170
    label "zbi&#243;r"
  ]
  node [
    id 171
    label "ekscerpcja"
  ]
  node [
    id 172
    label "materia&#322;"
  ]
  node [
    id 173
    label "operat"
  ]
  node [
    id 174
    label "kosztorys"
  ]
  node [
    id 175
    label "torba"
  ]
  node [
    id 176
    label "paraph"
  ]
  node [
    id 177
    label "podpis"
  ]
  node [
    id 178
    label "blok"
  ]
  node [
    id 179
    label "prawda"
  ]
  node [
    id 180
    label "znak_j&#281;zykowy"
  ]
  node [
    id 181
    label "nag&#322;&#243;wek"
  ]
  node [
    id 182
    label "szkic"
  ]
  node [
    id 183
    label "line"
  ]
  node [
    id 184
    label "fragment"
  ]
  node [
    id 185
    label "wyr&#243;b"
  ]
  node [
    id 186
    label "rodzajnik"
  ]
  node [
    id 187
    label "towar"
  ]
  node [
    id 188
    label "paragraf"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
]
