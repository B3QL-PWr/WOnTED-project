graph [
  node [
    id 0
    label "charakter"
    origin "text"
  ]
  node [
    id 1
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 2
    label "policjant"
    origin "text"
  ]
  node [
    id 3
    label "zadanie"
    origin "text"
  ]
  node [
    id 4
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "nara&#380;enie"
    origin "text"
  ]
  node [
    id 6
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 7
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 8
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 9
    label "wymoga"
    origin "text"
  ]
  node [
    id 10
    label "obywatelstwo"
    origin "text"
  ]
  node [
    id 11
    label "polski"
    origin "text"
  ]
  node [
    id 12
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zachowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "przedmiot"
  ]
  node [
    id 15
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 16
    label "zbi&#243;r"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "osobowo&#347;&#263;"
  ]
  node [
    id 20
    label "psychika"
  ]
  node [
    id 21
    label "posta&#263;"
  ]
  node [
    id 22
    label "kompleksja"
  ]
  node [
    id 23
    label "fizjonomia"
  ]
  node [
    id 24
    label "zjawisko"
  ]
  node [
    id 25
    label "cecha"
  ]
  node [
    id 26
    label "entity"
  ]
  node [
    id 27
    label "egzemplarz"
  ]
  node [
    id 28
    label "series"
  ]
  node [
    id 29
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 30
    label "uprawianie"
  ]
  node [
    id 31
    label "praca_rolnicza"
  ]
  node [
    id 32
    label "collection"
  ]
  node [
    id 33
    label "dane"
  ]
  node [
    id 34
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 35
    label "pakiet_klimatyczny"
  ]
  node [
    id 36
    label "poj&#281;cie"
  ]
  node [
    id 37
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 38
    label "sum"
  ]
  node [
    id 39
    label "gathering"
  ]
  node [
    id 40
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 41
    label "album"
  ]
  node [
    id 42
    label "ludzko&#347;&#263;"
  ]
  node [
    id 43
    label "asymilowanie"
  ]
  node [
    id 44
    label "wapniak"
  ]
  node [
    id 45
    label "asymilowa&#263;"
  ]
  node [
    id 46
    label "os&#322;abia&#263;"
  ]
  node [
    id 47
    label "hominid"
  ]
  node [
    id 48
    label "podw&#322;adny"
  ]
  node [
    id 49
    label "os&#322;abianie"
  ]
  node [
    id 50
    label "g&#322;owa"
  ]
  node [
    id 51
    label "figura"
  ]
  node [
    id 52
    label "portrecista"
  ]
  node [
    id 53
    label "dwun&#243;g"
  ]
  node [
    id 54
    label "profanum"
  ]
  node [
    id 55
    label "mikrokosmos"
  ]
  node [
    id 56
    label "nasada"
  ]
  node [
    id 57
    label "duch"
  ]
  node [
    id 58
    label "antropochoria"
  ]
  node [
    id 59
    label "osoba"
  ]
  node [
    id 60
    label "wz&#243;r"
  ]
  node [
    id 61
    label "senior"
  ]
  node [
    id 62
    label "oddzia&#322;ywanie"
  ]
  node [
    id 63
    label "Adam"
  ]
  node [
    id 64
    label "homo_sapiens"
  ]
  node [
    id 65
    label "polifag"
  ]
  node [
    id 66
    label "charakterystyka"
  ]
  node [
    id 67
    label "m&#322;ot"
  ]
  node [
    id 68
    label "znak"
  ]
  node [
    id 69
    label "drzewo"
  ]
  node [
    id 70
    label "pr&#243;ba"
  ]
  node [
    id 71
    label "attribute"
  ]
  node [
    id 72
    label "marka"
  ]
  node [
    id 73
    label "zaistnie&#263;"
  ]
  node [
    id 74
    label "Osjan"
  ]
  node [
    id 75
    label "kto&#347;"
  ]
  node [
    id 76
    label "wygl&#261;d"
  ]
  node [
    id 77
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 78
    label "wytw&#243;r"
  ]
  node [
    id 79
    label "trim"
  ]
  node [
    id 80
    label "poby&#263;"
  ]
  node [
    id 81
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 82
    label "Aspazja"
  ]
  node [
    id 83
    label "punkt_widzenia"
  ]
  node [
    id 84
    label "wytrzyma&#263;"
  ]
  node [
    id 85
    label "budowa"
  ]
  node [
    id 86
    label "formacja"
  ]
  node [
    id 87
    label "pozosta&#263;"
  ]
  node [
    id 88
    label "point"
  ]
  node [
    id 89
    label "przedstawienie"
  ]
  node [
    id 90
    label "go&#347;&#263;"
  ]
  node [
    id 91
    label "zboczenie"
  ]
  node [
    id 92
    label "om&#243;wienie"
  ]
  node [
    id 93
    label "sponiewieranie"
  ]
  node [
    id 94
    label "discipline"
  ]
  node [
    id 95
    label "rzecz"
  ]
  node [
    id 96
    label "omawia&#263;"
  ]
  node [
    id 97
    label "kr&#261;&#380;enie"
  ]
  node [
    id 98
    label "tre&#347;&#263;"
  ]
  node [
    id 99
    label "robienie"
  ]
  node [
    id 100
    label "sponiewiera&#263;"
  ]
  node [
    id 101
    label "element"
  ]
  node [
    id 102
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 103
    label "tematyka"
  ]
  node [
    id 104
    label "w&#261;tek"
  ]
  node [
    id 105
    label "zbaczanie"
  ]
  node [
    id 106
    label "program_nauczania"
  ]
  node [
    id 107
    label "om&#243;wi&#263;"
  ]
  node [
    id 108
    label "omawianie"
  ]
  node [
    id 109
    label "thing"
  ]
  node [
    id 110
    label "kultura"
  ]
  node [
    id 111
    label "istota"
  ]
  node [
    id 112
    label "zbacza&#263;"
  ]
  node [
    id 113
    label "zboczy&#263;"
  ]
  node [
    id 114
    label "przebiec"
  ]
  node [
    id 115
    label "czynno&#347;&#263;"
  ]
  node [
    id 116
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 117
    label "motyw"
  ]
  node [
    id 118
    label "przebiegni&#281;cie"
  ]
  node [
    id 119
    label "fabu&#322;a"
  ]
  node [
    id 120
    label "proces"
  ]
  node [
    id 121
    label "boski"
  ]
  node [
    id 122
    label "krajobraz"
  ]
  node [
    id 123
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 124
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 125
    label "przywidzenie"
  ]
  node [
    id 126
    label "presence"
  ]
  node [
    id 127
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 128
    label "seksualno&#347;&#263;"
  ]
  node [
    id 129
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 130
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 131
    label "deformowa&#263;"
  ]
  node [
    id 132
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 133
    label "ego"
  ]
  node [
    id 134
    label "sfera_afektywna"
  ]
  node [
    id 135
    label "deformowanie"
  ]
  node [
    id 136
    label "kompleks"
  ]
  node [
    id 137
    label "sumienie"
  ]
  node [
    id 138
    label "facjata"
  ]
  node [
    id 139
    label "twarz"
  ]
  node [
    id 140
    label "mentalno&#347;&#263;"
  ]
  node [
    id 141
    label "podmiot"
  ]
  node [
    id 142
    label "byt"
  ]
  node [
    id 143
    label "superego"
  ]
  node [
    id 144
    label "wyj&#261;tkowy"
  ]
  node [
    id 145
    label "wn&#281;trze"
  ]
  node [
    id 146
    label "self"
  ]
  node [
    id 147
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 148
    label "zesp&#243;&#322;"
  ]
  node [
    id 149
    label "instytucja"
  ]
  node [
    id 150
    label "wys&#322;uga"
  ]
  node [
    id 151
    label "service"
  ]
  node [
    id 152
    label "czworak"
  ]
  node [
    id 153
    label "ZOMO"
  ]
  node [
    id 154
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 155
    label "praca"
  ]
  node [
    id 156
    label "moneta"
  ]
  node [
    id 157
    label "szesnastowieczny"
  ]
  node [
    id 158
    label "dom_wielorodzinny"
  ]
  node [
    id 159
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 160
    label "Mazowsze"
  ]
  node [
    id 161
    label "odm&#322;adzanie"
  ]
  node [
    id 162
    label "&#346;wietliki"
  ]
  node [
    id 163
    label "whole"
  ]
  node [
    id 164
    label "skupienie"
  ]
  node [
    id 165
    label "The_Beatles"
  ]
  node [
    id 166
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 167
    label "odm&#322;adza&#263;"
  ]
  node [
    id 168
    label "zabudowania"
  ]
  node [
    id 169
    label "group"
  ]
  node [
    id 170
    label "zespolik"
  ]
  node [
    id 171
    label "schorzenie"
  ]
  node [
    id 172
    label "ro&#347;lina"
  ]
  node [
    id 173
    label "grupa"
  ]
  node [
    id 174
    label "Depeche_Mode"
  ]
  node [
    id 175
    label "batch"
  ]
  node [
    id 176
    label "odm&#322;odzenie"
  ]
  node [
    id 177
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 178
    label "najem"
  ]
  node [
    id 179
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 180
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 181
    label "zak&#322;ad"
  ]
  node [
    id 182
    label "stosunek_pracy"
  ]
  node [
    id 183
    label "benedykty&#324;ski"
  ]
  node [
    id 184
    label "poda&#380;_pracy"
  ]
  node [
    id 185
    label "pracowanie"
  ]
  node [
    id 186
    label "tyrka"
  ]
  node [
    id 187
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 188
    label "miejsce"
  ]
  node [
    id 189
    label "zaw&#243;d"
  ]
  node [
    id 190
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 191
    label "tynkarski"
  ]
  node [
    id 192
    label "pracowa&#263;"
  ]
  node [
    id 193
    label "zmiana"
  ]
  node [
    id 194
    label "czynnik_produkcji"
  ]
  node [
    id 195
    label "zobowi&#261;zanie"
  ]
  node [
    id 196
    label "kierownictwo"
  ]
  node [
    id 197
    label "siedziba"
  ]
  node [
    id 198
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 199
    label "osoba_prawna"
  ]
  node [
    id 200
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 201
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 202
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 203
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 204
    label "biuro"
  ]
  node [
    id 205
    label "organizacja"
  ]
  node [
    id 206
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 207
    label "Fundusze_Unijne"
  ]
  node [
    id 208
    label "zamyka&#263;"
  ]
  node [
    id 209
    label "establishment"
  ]
  node [
    id 210
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 211
    label "urz&#261;d"
  ]
  node [
    id 212
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 213
    label "afiliowa&#263;"
  ]
  node [
    id 214
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 215
    label "standard"
  ]
  node [
    id 216
    label "zamykanie"
  ]
  node [
    id 217
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 218
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 219
    label "s&#322;uga"
  ]
  node [
    id 220
    label "s&#322;u&#380;ebnik"
  ]
  node [
    id 221
    label "ochmistrzyni"
  ]
  node [
    id 222
    label "s&#322;u&#380;ebnica"
  ]
  node [
    id 223
    label "milicja_obywatelska"
  ]
  node [
    id 224
    label "policja"
  ]
  node [
    id 225
    label "blacharz"
  ]
  node [
    id 226
    label "str&#243;&#380;"
  ]
  node [
    id 227
    label "pa&#322;a"
  ]
  node [
    id 228
    label "mundurowy"
  ]
  node [
    id 229
    label "glina"
  ]
  node [
    id 230
    label "organ"
  ]
  node [
    id 231
    label "komisariat"
  ]
  node [
    id 232
    label "posterunek"
  ]
  node [
    id 233
    label "psiarnia"
  ]
  node [
    id 234
    label "&#380;o&#322;nierz"
  ]
  node [
    id 235
    label "funkcjonariusz"
  ]
  node [
    id 236
    label "nosiciel"
  ]
  node [
    id 237
    label "stra&#380;nik"
  ]
  node [
    id 238
    label "anio&#322;"
  ]
  node [
    id 239
    label "obro&#324;ca"
  ]
  node [
    id 240
    label "opiekun"
  ]
  node [
    id 241
    label "rzemie&#347;lnik"
  ]
  node [
    id 242
    label "przybitka"
  ]
  node [
    id 243
    label "gleba"
  ]
  node [
    id 244
    label "conk"
  ]
  node [
    id 245
    label "penis"
  ]
  node [
    id 246
    label "cios"
  ]
  node [
    id 247
    label "ciul"
  ]
  node [
    id 248
    label "wyzwisko"
  ]
  node [
    id 249
    label "niedostateczny"
  ]
  node [
    id 250
    label "mak&#243;wka"
  ]
  node [
    id 251
    label "&#322;eb"
  ]
  node [
    id 252
    label "skurwysyn"
  ]
  node [
    id 253
    label "istota_&#380;ywa"
  ]
  node [
    id 254
    label "dupek"
  ]
  node [
    id 255
    label "czaszka"
  ]
  node [
    id 256
    label "dynia"
  ]
  node [
    id 257
    label "zaj&#281;cie"
  ]
  node [
    id 258
    label "yield"
  ]
  node [
    id 259
    label "zaszkodzenie"
  ]
  node [
    id 260
    label "za&#322;o&#380;enie"
  ]
  node [
    id 261
    label "duty"
  ]
  node [
    id 262
    label "powierzanie"
  ]
  node [
    id 263
    label "work"
  ]
  node [
    id 264
    label "problem"
  ]
  node [
    id 265
    label "przepisanie"
  ]
  node [
    id 266
    label "nakarmienie"
  ]
  node [
    id 267
    label "przepisa&#263;"
  ]
  node [
    id 268
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 269
    label "activity"
  ]
  node [
    id 270
    label "bezproblemowy"
  ]
  node [
    id 271
    label "danie"
  ]
  node [
    id 272
    label "feed"
  ]
  node [
    id 273
    label "zaspokojenie"
  ]
  node [
    id 274
    label "podwini&#281;cie"
  ]
  node [
    id 275
    label "zap&#322;acenie"
  ]
  node [
    id 276
    label "przyodzianie"
  ]
  node [
    id 277
    label "budowla"
  ]
  node [
    id 278
    label "pokrycie"
  ]
  node [
    id 279
    label "rozebranie"
  ]
  node [
    id 280
    label "zak&#322;adka"
  ]
  node [
    id 281
    label "struktura"
  ]
  node [
    id 282
    label "poubieranie"
  ]
  node [
    id 283
    label "infliction"
  ]
  node [
    id 284
    label "spowodowanie"
  ]
  node [
    id 285
    label "pozak&#322;adanie"
  ]
  node [
    id 286
    label "program"
  ]
  node [
    id 287
    label "przebranie"
  ]
  node [
    id 288
    label "przywdzianie"
  ]
  node [
    id 289
    label "obleczenie_si&#281;"
  ]
  node [
    id 290
    label "utworzenie"
  ]
  node [
    id 291
    label "str&#243;j"
  ]
  node [
    id 292
    label "twierdzenie"
  ]
  node [
    id 293
    label "obleczenie"
  ]
  node [
    id 294
    label "umieszczenie"
  ]
  node [
    id 295
    label "przygotowywanie"
  ]
  node [
    id 296
    label "przymierzenie"
  ]
  node [
    id 297
    label "wyko&#324;czenie"
  ]
  node [
    id 298
    label "przygotowanie"
  ]
  node [
    id 299
    label "proposition"
  ]
  node [
    id 300
    label "przewidzenie"
  ]
  node [
    id 301
    label "zrobienie"
  ]
  node [
    id 302
    label "stosunek_prawny"
  ]
  node [
    id 303
    label "oblig"
  ]
  node [
    id 304
    label "uregulowa&#263;"
  ]
  node [
    id 305
    label "oddzia&#322;anie"
  ]
  node [
    id 306
    label "occupation"
  ]
  node [
    id 307
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 308
    label "zapowied&#378;"
  ]
  node [
    id 309
    label "obowi&#261;zek"
  ]
  node [
    id 310
    label "statement"
  ]
  node [
    id 311
    label "zapewnienie"
  ]
  node [
    id 312
    label "sprawa"
  ]
  node [
    id 313
    label "subiekcja"
  ]
  node [
    id 314
    label "problemat"
  ]
  node [
    id 315
    label "jajko_Kolumba"
  ]
  node [
    id 316
    label "obstruction"
  ]
  node [
    id 317
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 318
    label "problematyka"
  ]
  node [
    id 319
    label "trudno&#347;&#263;"
  ]
  node [
    id 320
    label "pierepa&#322;ka"
  ]
  node [
    id 321
    label "ambaras"
  ]
  node [
    id 322
    label "damage"
  ]
  node [
    id 323
    label "podniesienie"
  ]
  node [
    id 324
    label "zniesienie"
  ]
  node [
    id 325
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 326
    label "ulepszenie"
  ]
  node [
    id 327
    label "heave"
  ]
  node [
    id 328
    label "raise"
  ]
  node [
    id 329
    label "odbudowanie"
  ]
  node [
    id 330
    label "care"
  ]
  node [
    id 331
    label "zdarzenie_si&#281;"
  ]
  node [
    id 332
    label "career"
  ]
  node [
    id 333
    label "anektowanie"
  ]
  node [
    id 334
    label "dostarczenie"
  ]
  node [
    id 335
    label "u&#380;ycie"
  ]
  node [
    id 336
    label "klasyfikacja"
  ]
  node [
    id 337
    label "wzi&#281;cie"
  ]
  node [
    id 338
    label "wzbudzenie"
  ]
  node [
    id 339
    label "wype&#322;nienie"
  ]
  node [
    id 340
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 341
    label "zapanowanie"
  ]
  node [
    id 342
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 343
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 344
    label "pozajmowanie"
  ]
  node [
    id 345
    label "ulokowanie_si&#281;"
  ]
  node [
    id 346
    label "usytuowanie_si&#281;"
  ]
  node [
    id 347
    label "obj&#281;cie"
  ]
  node [
    id 348
    label "zabranie"
  ]
  node [
    id 349
    label "oddawanie"
  ]
  node [
    id 350
    label "stanowisko"
  ]
  node [
    id 351
    label "zlecanie"
  ]
  node [
    id 352
    label "ufanie"
  ]
  node [
    id 353
    label "wyznawanie"
  ]
  node [
    id 354
    label "szko&#322;a"
  ]
  node [
    id 355
    label "przekazanie"
  ]
  node [
    id 356
    label "skopiowanie"
  ]
  node [
    id 357
    label "arrangement"
  ]
  node [
    id 358
    label "przeniesienie"
  ]
  node [
    id 359
    label "testament"
  ]
  node [
    id 360
    label "lekarstwo"
  ]
  node [
    id 361
    label "answer"
  ]
  node [
    id 362
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 363
    label "transcription"
  ]
  node [
    id 364
    label "klasa"
  ]
  node [
    id 365
    label "zalecenie"
  ]
  node [
    id 366
    label "przekaza&#263;"
  ]
  node [
    id 367
    label "supply"
  ]
  node [
    id 368
    label "zaleci&#263;"
  ]
  node [
    id 369
    label "rewrite"
  ]
  node [
    id 370
    label "zrzec_si&#281;"
  ]
  node [
    id 371
    label "skopiowa&#263;"
  ]
  node [
    id 372
    label "przenie&#347;&#263;"
  ]
  node [
    id 373
    label "rola"
  ]
  node [
    id 374
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 375
    label "robi&#263;"
  ]
  node [
    id 376
    label "wytwarza&#263;"
  ]
  node [
    id 377
    label "create"
  ]
  node [
    id 378
    label "muzyka"
  ]
  node [
    id 379
    label "organizowa&#263;"
  ]
  node [
    id 380
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 381
    label "czyni&#263;"
  ]
  node [
    id 382
    label "give"
  ]
  node [
    id 383
    label "stylizowa&#263;"
  ]
  node [
    id 384
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 385
    label "falowa&#263;"
  ]
  node [
    id 386
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 387
    label "peddle"
  ]
  node [
    id 388
    label "wydala&#263;"
  ]
  node [
    id 389
    label "tentegowa&#263;"
  ]
  node [
    id 390
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 391
    label "urz&#261;dza&#263;"
  ]
  node [
    id 392
    label "oszukiwa&#263;"
  ]
  node [
    id 393
    label "ukazywa&#263;"
  ]
  node [
    id 394
    label "przerabia&#263;"
  ]
  node [
    id 395
    label "act"
  ]
  node [
    id 396
    label "post&#281;powa&#263;"
  ]
  node [
    id 397
    label "wokalistyka"
  ]
  node [
    id 398
    label "wykonywanie"
  ]
  node [
    id 399
    label "muza"
  ]
  node [
    id 400
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 401
    label "beatbox"
  ]
  node [
    id 402
    label "komponowa&#263;"
  ]
  node [
    id 403
    label "komponowanie"
  ]
  node [
    id 404
    label "pasa&#380;"
  ]
  node [
    id 405
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 406
    label "notacja_muzyczna"
  ]
  node [
    id 407
    label "kontrapunkt"
  ]
  node [
    id 408
    label "nauka"
  ]
  node [
    id 409
    label "sztuka"
  ]
  node [
    id 410
    label "instrumentalistyka"
  ]
  node [
    id 411
    label "harmonia"
  ]
  node [
    id 412
    label "set"
  ]
  node [
    id 413
    label "wys&#322;uchanie"
  ]
  node [
    id 414
    label "kapela"
  ]
  node [
    id 415
    label "britpop"
  ]
  node [
    id 416
    label "uprawienie"
  ]
  node [
    id 417
    label "kszta&#322;t"
  ]
  node [
    id 418
    label "dialog"
  ]
  node [
    id 419
    label "p&#322;osa"
  ]
  node [
    id 420
    label "plik"
  ]
  node [
    id 421
    label "ziemia"
  ]
  node [
    id 422
    label "czyn"
  ]
  node [
    id 423
    label "ustawienie"
  ]
  node [
    id 424
    label "scenariusz"
  ]
  node [
    id 425
    label "pole"
  ]
  node [
    id 426
    label "gospodarstwo"
  ]
  node [
    id 427
    label "uprawi&#263;"
  ]
  node [
    id 428
    label "function"
  ]
  node [
    id 429
    label "zreinterpretowa&#263;"
  ]
  node [
    id 430
    label "zastosowanie"
  ]
  node [
    id 431
    label "reinterpretowa&#263;"
  ]
  node [
    id 432
    label "wrench"
  ]
  node [
    id 433
    label "irygowanie"
  ]
  node [
    id 434
    label "ustawi&#263;"
  ]
  node [
    id 435
    label "irygowa&#263;"
  ]
  node [
    id 436
    label "zreinterpretowanie"
  ]
  node [
    id 437
    label "cel"
  ]
  node [
    id 438
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 439
    label "gra&#263;"
  ]
  node [
    id 440
    label "aktorstwo"
  ]
  node [
    id 441
    label "kostium"
  ]
  node [
    id 442
    label "zagon"
  ]
  node [
    id 443
    label "znaczenie"
  ]
  node [
    id 444
    label "zagra&#263;"
  ]
  node [
    id 445
    label "reinterpretowanie"
  ]
  node [
    id 446
    label "sk&#322;ad"
  ]
  node [
    id 447
    label "tekst"
  ]
  node [
    id 448
    label "zagranie"
  ]
  node [
    id 449
    label "radlina"
  ]
  node [
    id 450
    label "granie"
  ]
  node [
    id 451
    label "wystawienie"
  ]
  node [
    id 452
    label "expose"
  ]
  node [
    id 453
    label "wypisanie"
  ]
  node [
    id 454
    label "wyniesienie"
  ]
  node [
    id 455
    label "narration"
  ]
  node [
    id 456
    label "exhibit"
  ]
  node [
    id 457
    label "display"
  ]
  node [
    id 458
    label "zaproponowanie"
  ]
  node [
    id 459
    label "wyj&#281;cie"
  ]
  node [
    id 460
    label "wychylenie"
  ]
  node [
    id 461
    label "pies_my&#347;liwski"
  ]
  node [
    id 462
    label "upolowanie"
  ]
  node [
    id 463
    label "zbudowanie"
  ]
  node [
    id 464
    label "dopuszczenie"
  ]
  node [
    id 465
    label "wyeksponowanie"
  ]
  node [
    id 466
    label "atestowanie"
  ]
  node [
    id 467
    label "wskazanie"
  ]
  node [
    id 468
    label "polowanie"
  ]
  node [
    id 469
    label "wysuni&#281;cie"
  ]
  node [
    id 470
    label "asygnowanie"
  ]
  node [
    id 471
    label "raj_utracony"
  ]
  node [
    id 472
    label "umieranie"
  ]
  node [
    id 473
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 474
    label "prze&#380;ywanie"
  ]
  node [
    id 475
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 476
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 477
    label "po&#322;&#243;g"
  ]
  node [
    id 478
    label "umarcie"
  ]
  node [
    id 479
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 480
    label "subsistence"
  ]
  node [
    id 481
    label "power"
  ]
  node [
    id 482
    label "okres_noworodkowy"
  ]
  node [
    id 483
    label "prze&#380;ycie"
  ]
  node [
    id 484
    label "wiek_matuzalemowy"
  ]
  node [
    id 485
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 486
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 487
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 488
    label "do&#380;ywanie"
  ]
  node [
    id 489
    label "dzieci&#324;stwo"
  ]
  node [
    id 490
    label "andropauza"
  ]
  node [
    id 491
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 492
    label "rozw&#243;j"
  ]
  node [
    id 493
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 494
    label "czas"
  ]
  node [
    id 495
    label "menopauza"
  ]
  node [
    id 496
    label "&#347;mier&#263;"
  ]
  node [
    id 497
    label "koleje_losu"
  ]
  node [
    id 498
    label "bycie"
  ]
  node [
    id 499
    label "zegar_biologiczny"
  ]
  node [
    id 500
    label "szwung"
  ]
  node [
    id 501
    label "przebywanie"
  ]
  node [
    id 502
    label "warunki"
  ]
  node [
    id 503
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 504
    label "niemowl&#281;ctwo"
  ]
  node [
    id 505
    label "&#380;ywy"
  ]
  node [
    id 506
    label "life"
  ]
  node [
    id 507
    label "staro&#347;&#263;"
  ]
  node [
    id 508
    label "energy"
  ]
  node [
    id 509
    label "wra&#380;enie"
  ]
  node [
    id 510
    label "przej&#347;cie"
  ]
  node [
    id 511
    label "doznanie"
  ]
  node [
    id 512
    label "poradzenie_sobie"
  ]
  node [
    id 513
    label "przetrwanie"
  ]
  node [
    id 514
    label "survival"
  ]
  node [
    id 515
    label "przechodzenie"
  ]
  node [
    id 516
    label "wytrzymywanie"
  ]
  node [
    id 517
    label "zaznawanie"
  ]
  node [
    id 518
    label "trwanie"
  ]
  node [
    id 519
    label "obejrzenie"
  ]
  node [
    id 520
    label "widzenie"
  ]
  node [
    id 521
    label "urzeczywistnianie"
  ]
  node [
    id 522
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 523
    label "produkowanie"
  ]
  node [
    id 524
    label "przeszkodzenie"
  ]
  node [
    id 525
    label "being"
  ]
  node [
    id 526
    label "znikni&#281;cie"
  ]
  node [
    id 527
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 528
    label "przeszkadzanie"
  ]
  node [
    id 529
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 530
    label "wyprodukowanie"
  ]
  node [
    id 531
    label "utrzymywanie"
  ]
  node [
    id 532
    label "subsystencja"
  ]
  node [
    id 533
    label "utrzyma&#263;"
  ]
  node [
    id 534
    label "egzystencja"
  ]
  node [
    id 535
    label "wy&#380;ywienie"
  ]
  node [
    id 536
    label "ontologicznie"
  ]
  node [
    id 537
    label "utrzymanie"
  ]
  node [
    id 538
    label "potencja"
  ]
  node [
    id 539
    label "utrzymywa&#263;"
  ]
  node [
    id 540
    label "status"
  ]
  node [
    id 541
    label "sytuacja"
  ]
  node [
    id 542
    label "poprzedzanie"
  ]
  node [
    id 543
    label "czasoprzestrze&#324;"
  ]
  node [
    id 544
    label "laba"
  ]
  node [
    id 545
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 546
    label "chronometria"
  ]
  node [
    id 547
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 548
    label "rachuba_czasu"
  ]
  node [
    id 549
    label "przep&#322;ywanie"
  ]
  node [
    id 550
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 551
    label "czasokres"
  ]
  node [
    id 552
    label "odczyt"
  ]
  node [
    id 553
    label "chwila"
  ]
  node [
    id 554
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 555
    label "dzieje"
  ]
  node [
    id 556
    label "kategoria_gramatyczna"
  ]
  node [
    id 557
    label "poprzedzenie"
  ]
  node [
    id 558
    label "trawienie"
  ]
  node [
    id 559
    label "pochodzi&#263;"
  ]
  node [
    id 560
    label "period"
  ]
  node [
    id 561
    label "okres_czasu"
  ]
  node [
    id 562
    label "poprzedza&#263;"
  ]
  node [
    id 563
    label "schy&#322;ek"
  ]
  node [
    id 564
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 565
    label "odwlekanie_si&#281;"
  ]
  node [
    id 566
    label "zegar"
  ]
  node [
    id 567
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 568
    label "czwarty_wymiar"
  ]
  node [
    id 569
    label "pochodzenie"
  ]
  node [
    id 570
    label "koniugacja"
  ]
  node [
    id 571
    label "Zeitgeist"
  ]
  node [
    id 572
    label "trawi&#263;"
  ]
  node [
    id 573
    label "pogoda"
  ]
  node [
    id 574
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 575
    label "poprzedzi&#263;"
  ]
  node [
    id 576
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 577
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 578
    label "time_period"
  ]
  node [
    id 579
    label "ocieranie_si&#281;"
  ]
  node [
    id 580
    label "otoczenie_si&#281;"
  ]
  node [
    id 581
    label "posiedzenie"
  ]
  node [
    id 582
    label "otarcie_si&#281;"
  ]
  node [
    id 583
    label "atakowanie"
  ]
  node [
    id 584
    label "otaczanie_si&#281;"
  ]
  node [
    id 585
    label "wyj&#347;cie"
  ]
  node [
    id 586
    label "zmierzanie"
  ]
  node [
    id 587
    label "residency"
  ]
  node [
    id 588
    label "sojourn"
  ]
  node [
    id 589
    label "wychodzenie"
  ]
  node [
    id 590
    label "tkwienie"
  ]
  node [
    id 591
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 592
    label "absolutorium"
  ]
  node [
    id 593
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 594
    label "dzia&#322;anie"
  ]
  node [
    id 595
    label "ton"
  ]
  node [
    id 596
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 597
    label "odumarcie"
  ]
  node [
    id 598
    label "przestanie"
  ]
  node [
    id 599
    label "martwy"
  ]
  node [
    id 600
    label "dysponowanie_si&#281;"
  ]
  node [
    id 601
    label "pomarcie"
  ]
  node [
    id 602
    label "die"
  ]
  node [
    id 603
    label "sko&#324;czenie"
  ]
  node [
    id 604
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 605
    label "zdechni&#281;cie"
  ]
  node [
    id 606
    label "zabicie"
  ]
  node [
    id 607
    label "korkowanie"
  ]
  node [
    id 608
    label "death"
  ]
  node [
    id 609
    label "zabijanie"
  ]
  node [
    id 610
    label "przestawanie"
  ]
  node [
    id 611
    label "odumieranie"
  ]
  node [
    id 612
    label "zdychanie"
  ]
  node [
    id 613
    label "stan"
  ]
  node [
    id 614
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 615
    label "zanikanie"
  ]
  node [
    id 616
    label "ko&#324;czenie"
  ]
  node [
    id 617
    label "nieuleczalnie_chory"
  ]
  node [
    id 618
    label "ciekawy"
  ]
  node [
    id 619
    label "szybki"
  ]
  node [
    id 620
    label "&#380;ywotny"
  ]
  node [
    id 621
    label "naturalny"
  ]
  node [
    id 622
    label "&#380;ywo"
  ]
  node [
    id 623
    label "o&#380;ywianie"
  ]
  node [
    id 624
    label "silny"
  ]
  node [
    id 625
    label "g&#322;&#281;boki"
  ]
  node [
    id 626
    label "wyra&#378;ny"
  ]
  node [
    id 627
    label "czynny"
  ]
  node [
    id 628
    label "aktualny"
  ]
  node [
    id 629
    label "zgrabny"
  ]
  node [
    id 630
    label "prawdziwy"
  ]
  node [
    id 631
    label "realistyczny"
  ]
  node [
    id 632
    label "energiczny"
  ]
  node [
    id 633
    label "procedura"
  ]
  node [
    id 634
    label "proces_biologiczny"
  ]
  node [
    id 635
    label "z&#322;ote_czasy"
  ]
  node [
    id 636
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 637
    label "process"
  ]
  node [
    id 638
    label "cycle"
  ]
  node [
    id 639
    label "defenestracja"
  ]
  node [
    id 640
    label "agonia"
  ]
  node [
    id 641
    label "kres"
  ]
  node [
    id 642
    label "mogi&#322;a"
  ]
  node [
    id 643
    label "kres_&#380;ycia"
  ]
  node [
    id 644
    label "upadek"
  ]
  node [
    id 645
    label "szeol"
  ]
  node [
    id 646
    label "pogrzebanie"
  ]
  node [
    id 647
    label "istota_nadprzyrodzona"
  ]
  node [
    id 648
    label "&#380;a&#322;oba"
  ]
  node [
    id 649
    label "pogrzeb"
  ]
  node [
    id 650
    label "majority"
  ]
  node [
    id 651
    label "wiek"
  ]
  node [
    id 652
    label "osiemnastoletni"
  ]
  node [
    id 653
    label "age"
  ]
  node [
    id 654
    label "rozwi&#261;zanie"
  ]
  node [
    id 655
    label "zlec"
  ]
  node [
    id 656
    label "zlegni&#281;cie"
  ]
  node [
    id 657
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 658
    label "dzieci&#281;ctwo"
  ]
  node [
    id 659
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 660
    label "kobieta"
  ]
  node [
    id 661
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 662
    label "przekwitanie"
  ]
  node [
    id 663
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 664
    label "adolescence"
  ]
  node [
    id 665
    label "zielone_lata"
  ]
  node [
    id 666
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 667
    label "energia"
  ]
  node [
    id 668
    label "zapa&#322;"
  ]
  node [
    id 669
    label "by&#263;"
  ]
  node [
    id 670
    label "claim"
  ]
  node [
    id 671
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 672
    label "zmusza&#263;"
  ]
  node [
    id 673
    label "take"
  ]
  node [
    id 674
    label "force"
  ]
  node [
    id 675
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 676
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 677
    label "sandbag"
  ]
  node [
    id 678
    label "powodowa&#263;"
  ]
  node [
    id 679
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 680
    label "mie&#263;_miejsce"
  ]
  node [
    id 681
    label "equal"
  ]
  node [
    id 682
    label "trwa&#263;"
  ]
  node [
    id 683
    label "chodzi&#263;"
  ]
  node [
    id 684
    label "si&#281;ga&#263;"
  ]
  node [
    id 685
    label "obecno&#347;&#263;"
  ]
  node [
    id 686
    label "stand"
  ]
  node [
    id 687
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 688
    label "uczestniczy&#263;"
  ]
  node [
    id 689
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 690
    label "woo"
  ]
  node [
    id 691
    label "chcie&#263;"
  ]
  node [
    id 692
    label "indygenat"
  ]
  node [
    id 693
    label "prawa_cz&#322;owieka"
  ]
  node [
    id 694
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 695
    label "po&#322;o&#380;enie"
  ]
  node [
    id 696
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 697
    label "rodowo&#347;&#263;"
  ]
  node [
    id 698
    label "patent"
  ]
  node [
    id 699
    label "mienie"
  ]
  node [
    id 700
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 701
    label "dobra"
  ]
  node [
    id 702
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 703
    label "przej&#347;&#263;"
  ]
  node [
    id 704
    label "possession"
  ]
  node [
    id 705
    label "organizacyjnie"
  ]
  node [
    id 706
    label "zwi&#261;zek"
  ]
  node [
    id 707
    label "prawo"
  ]
  node [
    id 708
    label "Polish"
  ]
  node [
    id 709
    label "goniony"
  ]
  node [
    id 710
    label "oberek"
  ]
  node [
    id 711
    label "ryba_po_grecku"
  ]
  node [
    id 712
    label "sztajer"
  ]
  node [
    id 713
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 714
    label "krakowiak"
  ]
  node [
    id 715
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 716
    label "pierogi_ruskie"
  ]
  node [
    id 717
    label "lacki"
  ]
  node [
    id 718
    label "polak"
  ]
  node [
    id 719
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 720
    label "chodzony"
  ]
  node [
    id 721
    label "po_polsku"
  ]
  node [
    id 722
    label "mazur"
  ]
  node [
    id 723
    label "polsko"
  ]
  node [
    id 724
    label "skoczny"
  ]
  node [
    id 725
    label "drabant"
  ]
  node [
    id 726
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 727
    label "j&#281;zyk"
  ]
  node [
    id 728
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 729
    label "artykulator"
  ]
  node [
    id 730
    label "kod"
  ]
  node [
    id 731
    label "kawa&#322;ek"
  ]
  node [
    id 732
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 733
    label "gramatyka"
  ]
  node [
    id 734
    label "stylik"
  ]
  node [
    id 735
    label "przet&#322;umaczenie"
  ]
  node [
    id 736
    label "formalizowanie"
  ]
  node [
    id 737
    label "ssanie"
  ]
  node [
    id 738
    label "ssa&#263;"
  ]
  node [
    id 739
    label "language"
  ]
  node [
    id 740
    label "liza&#263;"
  ]
  node [
    id 741
    label "napisa&#263;"
  ]
  node [
    id 742
    label "konsonantyzm"
  ]
  node [
    id 743
    label "wokalizm"
  ]
  node [
    id 744
    label "pisa&#263;"
  ]
  node [
    id 745
    label "fonetyka"
  ]
  node [
    id 746
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 747
    label "jeniec"
  ]
  node [
    id 748
    label "but"
  ]
  node [
    id 749
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 750
    label "po_koroniarsku"
  ]
  node [
    id 751
    label "kultura_duchowa"
  ]
  node [
    id 752
    label "t&#322;umaczenie"
  ]
  node [
    id 753
    label "m&#243;wienie"
  ]
  node [
    id 754
    label "pype&#263;"
  ]
  node [
    id 755
    label "lizanie"
  ]
  node [
    id 756
    label "pismo"
  ]
  node [
    id 757
    label "formalizowa&#263;"
  ]
  node [
    id 758
    label "rozumie&#263;"
  ]
  node [
    id 759
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 760
    label "rozumienie"
  ]
  node [
    id 761
    label "spos&#243;b"
  ]
  node [
    id 762
    label "makroglosja"
  ]
  node [
    id 763
    label "m&#243;wi&#263;"
  ]
  node [
    id 764
    label "jama_ustna"
  ]
  node [
    id 765
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 766
    label "formacja_geologiczna"
  ]
  node [
    id 767
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 768
    label "natural_language"
  ]
  node [
    id 769
    label "s&#322;ownictwo"
  ]
  node [
    id 770
    label "urz&#261;dzenie"
  ]
  node [
    id 771
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 772
    label "wschodnioeuropejski"
  ]
  node [
    id 773
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 774
    label "poga&#324;ski"
  ]
  node [
    id 775
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 776
    label "topielec"
  ]
  node [
    id 777
    label "europejski"
  ]
  node [
    id 778
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 779
    label "langosz"
  ]
  node [
    id 780
    label "gwardzista"
  ]
  node [
    id 781
    label "melodia"
  ]
  node [
    id 782
    label "taniec"
  ]
  node [
    id 783
    label "taniec_ludowy"
  ]
  node [
    id 784
    label "&#347;redniowieczny"
  ]
  node [
    id 785
    label "specjalny"
  ]
  node [
    id 786
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 787
    label "weso&#322;y"
  ]
  node [
    id 788
    label "sprawny"
  ]
  node [
    id 789
    label "rytmiczny"
  ]
  node [
    id 790
    label "skocznie"
  ]
  node [
    id 791
    label "lendler"
  ]
  node [
    id 792
    label "austriacki"
  ]
  node [
    id 793
    label "polka"
  ]
  node [
    id 794
    label "europejsko"
  ]
  node [
    id 795
    label "przytup"
  ]
  node [
    id 796
    label "ho&#322;ubiec"
  ]
  node [
    id 797
    label "wodzi&#263;"
  ]
  node [
    id 798
    label "ludowy"
  ]
  node [
    id 799
    label "pie&#347;&#324;"
  ]
  node [
    id 800
    label "mieszkaniec"
  ]
  node [
    id 801
    label "centu&#347;"
  ]
  node [
    id 802
    label "lalka"
  ]
  node [
    id 803
    label "Ma&#322;opolanin"
  ]
  node [
    id 804
    label "krakauer"
  ]
  node [
    id 805
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 806
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 807
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 808
    label "osta&#263;_si&#281;"
  ]
  node [
    id 809
    label "change"
  ]
  node [
    id 810
    label "catch"
  ]
  node [
    id 811
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 812
    label "proceed"
  ]
  node [
    id 813
    label "support"
  ]
  node [
    id 814
    label "prze&#380;y&#263;"
  ]
  node [
    id 815
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 816
    label "post&#261;pi&#263;"
  ]
  node [
    id 817
    label "tajemnica"
  ]
  node [
    id 818
    label "pami&#281;&#263;"
  ]
  node [
    id 819
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 820
    label "zdyscyplinowanie"
  ]
  node [
    id 821
    label "post"
  ]
  node [
    id 822
    label "zrobi&#263;"
  ]
  node [
    id 823
    label "przechowa&#263;"
  ]
  node [
    id 824
    label "preserve"
  ]
  node [
    id 825
    label "dieta"
  ]
  node [
    id 826
    label "bury"
  ]
  node [
    id 827
    label "podtrzyma&#263;"
  ]
  node [
    id 828
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 829
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 830
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 831
    label "zorganizowa&#263;"
  ]
  node [
    id 832
    label "appoint"
  ]
  node [
    id 833
    label "wystylizowa&#263;"
  ]
  node [
    id 834
    label "cause"
  ]
  node [
    id 835
    label "przerobi&#263;"
  ]
  node [
    id 836
    label "nabra&#263;"
  ]
  node [
    id 837
    label "make"
  ]
  node [
    id 838
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 839
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 840
    label "wydali&#263;"
  ]
  node [
    id 841
    label "pocieszy&#263;"
  ]
  node [
    id 842
    label "foster"
  ]
  node [
    id 843
    label "spowodowa&#263;"
  ]
  node [
    id 844
    label "unie&#347;&#263;"
  ]
  node [
    id 845
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 846
    label "advance"
  ]
  node [
    id 847
    label "see"
  ]
  node [
    id 848
    label "uchroni&#263;"
  ]
  node [
    id 849
    label "ukry&#263;"
  ]
  node [
    id 850
    label "continue"
  ]
  node [
    id 851
    label "wypaplanie"
  ]
  node [
    id 852
    label "enigmat"
  ]
  node [
    id 853
    label "wiedza"
  ]
  node [
    id 854
    label "zachowanie"
  ]
  node [
    id 855
    label "zachowywanie"
  ]
  node [
    id 856
    label "secret"
  ]
  node [
    id 857
    label "wydawa&#263;"
  ]
  node [
    id 858
    label "dyskrecja"
  ]
  node [
    id 859
    label "informacja"
  ]
  node [
    id 860
    label "wyda&#263;"
  ]
  node [
    id 861
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 862
    label "taj&#324;"
  ]
  node [
    id 863
    label "zachowywa&#263;"
  ]
  node [
    id 864
    label "podporz&#261;dkowanie"
  ]
  node [
    id 865
    label "porz&#261;dek"
  ]
  node [
    id 866
    label "mores"
  ]
  node [
    id 867
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 868
    label "nauczenie"
  ]
  node [
    id 869
    label "chart"
  ]
  node [
    id 870
    label "wynagrodzenie"
  ]
  node [
    id 871
    label "regimen"
  ]
  node [
    id 872
    label "r&#243;wnowarto&#347;&#263;"
  ]
  node [
    id 873
    label "terapia"
  ]
  node [
    id 874
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 875
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 876
    label "rok_ko&#347;cielny"
  ]
  node [
    id 877
    label "praktyka"
  ]
  node [
    id 878
    label "hipokamp"
  ]
  node [
    id 879
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 880
    label "memory"
  ]
  node [
    id 881
    label "umys&#322;"
  ]
  node [
    id 882
    label "komputer"
  ]
  node [
    id 883
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 884
    label "wymazanie"
  ]
  node [
    id 885
    label "brunatny"
  ]
  node [
    id 886
    label "przygn&#281;biaj&#261;cy"
  ]
  node [
    id 887
    label "ciemnoszary"
  ]
  node [
    id 888
    label "brudnoszary"
  ]
  node [
    id 889
    label "buro"
  ]
  node [
    id 890
    label "komisja"
  ]
  node [
    id 891
    label "dospraw"
  ]
  node [
    id 892
    label "petycja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 890
    target 891
  ]
  edge [
    source 890
    target 892
  ]
  edge [
    source 891
    target 892
  ]
]
