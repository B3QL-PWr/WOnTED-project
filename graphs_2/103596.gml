graph [
  node [
    id 0
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "niemal"
    origin "text"
  ]
  node [
    id 3
    label "dok&#322;adnie"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "temu"
    origin "text"
  ]
  node [
    id 6
    label "podczas"
    origin "text"
  ]
  node [
    id 7
    label "msza"
    origin "text"
  ]
  node [
    id 8
    label "rezurekcyjny"
    origin "text"
  ]
  node [
    id 9
    label "kazan"
    origin "text"
  ]
  node [
    id 10
    label "wyg&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "bierzmowanie"
    origin "text"
  ]
  node [
    id 15
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 16
    label "bardzo"
    origin "text"
  ]
  node [
    id 17
    label "dobre"
    origin "text"
  ]
  node [
    id 18
    label "zdanie"
    origin "text"
  ]
  node [
    id 19
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "rozmawia&#263;"
    origin "text"
  ]
  node [
    id 21
    label "inteligentnie"
    origin "text"
  ]
  node [
    id 22
    label "m&#322;ode"
    origin "text"
  ]
  node [
    id 23
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 24
    label "poczucie"
    origin "text"
  ]
  node [
    id 25
    label "humor"
    origin "text"
  ]
  node [
    id 26
    label "bez"
    origin "text"
  ]
  node [
    id 27
    label "zb&#281;dny"
    origin "text"
  ]
  node [
    id 28
    label "patos"
    origin "text"
  ]
  node [
    id 29
    label "tymczasem"
    origin "text"
  ]
  node [
    id 30
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 31
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 32
    label "te&#380;"
    origin "text"
  ]
  node [
    id 33
    label "druga"
    origin "text"
  ]
  node [
    id 34
    label "oblicze"
    origin "text"
  ]
  node [
    id 35
    label "pierwsza"
    origin "text"
  ]
  node [
    id 36
    label "by&#263;"
    origin "text"
  ]
  node [
    id 37
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 38
    label "niepodobny"
    origin "text"
  ]
  node [
    id 39
    label "ale"
    origin "text"
  ]
  node [
    id 40
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 41
    label "przerwa"
    origin "text"
  ]
  node [
    id 42
    label "uderza&#263;"
    origin "text"
  ]
  node [
    id 43
    label "moherowy"
    origin "text"
  ]
  node [
    id 44
    label "ton"
    origin "text"
  ]
  node [
    id 45
    label "niestety"
    origin "text"
  ]
  node [
    id 46
    label "wtedy"
    origin "text"
  ]
  node [
    id 47
    label "jeszcze"
    origin "text"
  ]
  node [
    id 48
    label "my&#347;le&#263;by&#263;"
    origin "text"
  ]
  node [
    id 49
    label "prowadzenie"
    origin "text"
  ]
  node [
    id 50
    label "blog"
    origin "text"
  ]
  node [
    id 51
    label "wszystko"
    origin "text"
  ]
  node [
    id 52
    label "zapami&#281;ta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 53
    label "tylko"
    origin "text"
  ]
  node [
    id 54
    label "zszokowa&#263;"
    origin "text"
  ]
  node [
    id 55
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 56
    label "co&#347;"
    origin "text"
  ]
  node [
    id 57
    label "taki"
    origin "text"
  ]
  node [
    id 58
    label "artyleria"
  ]
  node [
    id 59
    label "laweta"
  ]
  node [
    id 60
    label "cannon"
  ]
  node [
    id 61
    label "waln&#261;&#263;"
  ]
  node [
    id 62
    label "bateria_artylerii"
  ]
  node [
    id 63
    label "oporopowrotnik"
  ]
  node [
    id 64
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 65
    label "przedmuchiwacz"
  ]
  node [
    id 66
    label "bateria"
  ]
  node [
    id 67
    label "bro&#324;"
  ]
  node [
    id 68
    label "amunicja"
  ]
  node [
    id 69
    label "karta_przetargowa"
  ]
  node [
    id 70
    label "rozbroi&#263;"
  ]
  node [
    id 71
    label "rozbrojenie"
  ]
  node [
    id 72
    label "osprz&#281;t"
  ]
  node [
    id 73
    label "uzbrojenie"
  ]
  node [
    id 74
    label "przyrz&#261;d"
  ]
  node [
    id 75
    label "rozbrajanie"
  ]
  node [
    id 76
    label "rozbraja&#263;"
  ]
  node [
    id 77
    label "or&#281;&#380;"
  ]
  node [
    id 78
    label "powrotnik"
  ]
  node [
    id 79
    label "przyczepa"
  ]
  node [
    id 80
    label "podstawa"
  ]
  node [
    id 81
    label "lemiesz"
  ]
  node [
    id 82
    label "urz&#261;dzenie"
  ]
  node [
    id 83
    label "formacja"
  ]
  node [
    id 84
    label "armia"
  ]
  node [
    id 85
    label "artillery"
  ]
  node [
    id 86
    label "munition"
  ]
  node [
    id 87
    label "zaw&#243;r"
  ]
  node [
    id 88
    label "zbi&#243;r"
  ]
  node [
    id 89
    label "kolekcja"
  ]
  node [
    id 90
    label "oficer_ogniowy"
  ]
  node [
    id 91
    label "dywizjon_artylerii"
  ]
  node [
    id 92
    label "kran"
  ]
  node [
    id 93
    label "pododdzia&#322;"
  ]
  node [
    id 94
    label "cell"
  ]
  node [
    id 95
    label "pluton"
  ]
  node [
    id 96
    label "odkr&#281;ca&#263;_wod&#281;"
  ]
  node [
    id 97
    label "odkr&#281;ci&#263;_wod&#281;"
  ]
  node [
    id 98
    label "dzia&#322;obitnia"
  ]
  node [
    id 99
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 100
    label "sygn&#261;&#263;"
  ]
  node [
    id 101
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 102
    label "peddle"
  ]
  node [
    id 103
    label "jebn&#261;&#263;"
  ]
  node [
    id 104
    label "fall"
  ]
  node [
    id 105
    label "rap"
  ]
  node [
    id 106
    label "uderzy&#263;"
  ]
  node [
    id 107
    label "zrobi&#263;"
  ]
  node [
    id 108
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 109
    label "zmieni&#263;"
  ]
  node [
    id 110
    label "majdn&#261;&#263;"
  ]
  node [
    id 111
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 112
    label "paln&#261;&#263;"
  ]
  node [
    id 113
    label "strzeli&#263;"
  ]
  node [
    id 114
    label "lumber"
  ]
  node [
    id 115
    label "meticulously"
  ]
  node [
    id 116
    label "punctiliously"
  ]
  node [
    id 117
    label "precyzyjnie"
  ]
  node [
    id 118
    label "dok&#322;adny"
  ]
  node [
    id 119
    label "rzetelnie"
  ]
  node [
    id 120
    label "sprecyzowanie"
  ]
  node [
    id 121
    label "precyzyjny"
  ]
  node [
    id 122
    label "miliamperomierz"
  ]
  node [
    id 123
    label "precyzowanie"
  ]
  node [
    id 124
    label "rzetelny"
  ]
  node [
    id 125
    label "przekonuj&#261;co"
  ]
  node [
    id 126
    label "porz&#261;dnie"
  ]
  node [
    id 127
    label "p&#243;&#322;rocze"
  ]
  node [
    id 128
    label "martwy_sezon"
  ]
  node [
    id 129
    label "kalendarz"
  ]
  node [
    id 130
    label "cykl_astronomiczny"
  ]
  node [
    id 131
    label "lata"
  ]
  node [
    id 132
    label "pora_roku"
  ]
  node [
    id 133
    label "stulecie"
  ]
  node [
    id 134
    label "kurs"
  ]
  node [
    id 135
    label "czas"
  ]
  node [
    id 136
    label "jubileusz"
  ]
  node [
    id 137
    label "grupa"
  ]
  node [
    id 138
    label "kwarta&#322;"
  ]
  node [
    id 139
    label "miesi&#261;c"
  ]
  node [
    id 140
    label "summer"
  ]
  node [
    id 141
    label "odm&#322;adzanie"
  ]
  node [
    id 142
    label "liga"
  ]
  node [
    id 143
    label "jednostka_systematyczna"
  ]
  node [
    id 144
    label "asymilowanie"
  ]
  node [
    id 145
    label "gromada"
  ]
  node [
    id 146
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 147
    label "asymilowa&#263;"
  ]
  node [
    id 148
    label "egzemplarz"
  ]
  node [
    id 149
    label "Entuzjastki"
  ]
  node [
    id 150
    label "kompozycja"
  ]
  node [
    id 151
    label "Terranie"
  ]
  node [
    id 152
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 153
    label "category"
  ]
  node [
    id 154
    label "pakiet_klimatyczny"
  ]
  node [
    id 155
    label "oddzia&#322;"
  ]
  node [
    id 156
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 157
    label "cz&#261;steczka"
  ]
  node [
    id 158
    label "stage_set"
  ]
  node [
    id 159
    label "type"
  ]
  node [
    id 160
    label "specgrupa"
  ]
  node [
    id 161
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 162
    label "&#346;wietliki"
  ]
  node [
    id 163
    label "odm&#322;odzenie"
  ]
  node [
    id 164
    label "Eurogrupa"
  ]
  node [
    id 165
    label "odm&#322;adza&#263;"
  ]
  node [
    id 166
    label "formacja_geologiczna"
  ]
  node [
    id 167
    label "harcerze_starsi"
  ]
  node [
    id 168
    label "poprzedzanie"
  ]
  node [
    id 169
    label "czasoprzestrze&#324;"
  ]
  node [
    id 170
    label "laba"
  ]
  node [
    id 171
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 172
    label "chronometria"
  ]
  node [
    id 173
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 174
    label "rachuba_czasu"
  ]
  node [
    id 175
    label "przep&#322;ywanie"
  ]
  node [
    id 176
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 177
    label "czasokres"
  ]
  node [
    id 178
    label "odczyt"
  ]
  node [
    id 179
    label "chwila"
  ]
  node [
    id 180
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 181
    label "dzieje"
  ]
  node [
    id 182
    label "kategoria_gramatyczna"
  ]
  node [
    id 183
    label "poprzedzenie"
  ]
  node [
    id 184
    label "trawienie"
  ]
  node [
    id 185
    label "pochodzi&#263;"
  ]
  node [
    id 186
    label "period"
  ]
  node [
    id 187
    label "okres_czasu"
  ]
  node [
    id 188
    label "poprzedza&#263;"
  ]
  node [
    id 189
    label "schy&#322;ek"
  ]
  node [
    id 190
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 191
    label "odwlekanie_si&#281;"
  ]
  node [
    id 192
    label "zegar"
  ]
  node [
    id 193
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 194
    label "czwarty_wymiar"
  ]
  node [
    id 195
    label "pochodzenie"
  ]
  node [
    id 196
    label "koniugacja"
  ]
  node [
    id 197
    label "Zeitgeist"
  ]
  node [
    id 198
    label "trawi&#263;"
  ]
  node [
    id 199
    label "pogoda"
  ]
  node [
    id 200
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 201
    label "poprzedzi&#263;"
  ]
  node [
    id 202
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 203
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 204
    label "time_period"
  ]
  node [
    id 205
    label "tydzie&#324;"
  ]
  node [
    id 206
    label "miech"
  ]
  node [
    id 207
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 208
    label "kalendy"
  ]
  node [
    id 209
    label "term"
  ]
  node [
    id 210
    label "rok_akademicki"
  ]
  node [
    id 211
    label "rok_szkolny"
  ]
  node [
    id 212
    label "semester"
  ]
  node [
    id 213
    label "anniwersarz"
  ]
  node [
    id 214
    label "rocznica"
  ]
  node [
    id 215
    label "obszar"
  ]
  node [
    id 216
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 217
    label "long_time"
  ]
  node [
    id 218
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 219
    label "almanac"
  ]
  node [
    id 220
    label "rozk&#322;ad"
  ]
  node [
    id 221
    label "wydawnictwo"
  ]
  node [
    id 222
    label "Juliusz_Cezar"
  ]
  node [
    id 223
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 224
    label "zwy&#380;kowanie"
  ]
  node [
    id 225
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 226
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 227
    label "zaj&#281;cia"
  ]
  node [
    id 228
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 229
    label "trasa"
  ]
  node [
    id 230
    label "przeorientowywanie"
  ]
  node [
    id 231
    label "przejazd"
  ]
  node [
    id 232
    label "kierunek"
  ]
  node [
    id 233
    label "przeorientowywa&#263;"
  ]
  node [
    id 234
    label "nauka"
  ]
  node [
    id 235
    label "przeorientowanie"
  ]
  node [
    id 236
    label "klasa"
  ]
  node [
    id 237
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 238
    label "przeorientowa&#263;"
  ]
  node [
    id 239
    label "manner"
  ]
  node [
    id 240
    label "course"
  ]
  node [
    id 241
    label "passage"
  ]
  node [
    id 242
    label "zni&#380;kowanie"
  ]
  node [
    id 243
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 244
    label "seria"
  ]
  node [
    id 245
    label "stawka"
  ]
  node [
    id 246
    label "way"
  ]
  node [
    id 247
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 248
    label "spos&#243;b"
  ]
  node [
    id 249
    label "deprecjacja"
  ]
  node [
    id 250
    label "cedu&#322;a"
  ]
  node [
    id 251
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 252
    label "drive"
  ]
  node [
    id 253
    label "bearing"
  ]
  node [
    id 254
    label "Lira"
  ]
  node [
    id 255
    label "Mass"
  ]
  node [
    id 256
    label "dzie&#322;o"
  ]
  node [
    id 257
    label "katolicyzm"
  ]
  node [
    id 258
    label "utw&#243;r"
  ]
  node [
    id 259
    label "ofertorium"
  ]
  node [
    id 260
    label "komunia"
  ]
  node [
    id 261
    label "prefacja"
  ]
  node [
    id 262
    label "ofiarowanie"
  ]
  node [
    id 263
    label "prezbiter"
  ]
  node [
    id 264
    label "przeistoczenie"
  ]
  node [
    id 265
    label "gloria"
  ]
  node [
    id 266
    label "confiteor"
  ]
  node [
    id 267
    label "ewangelia"
  ]
  node [
    id 268
    label "sekreta"
  ]
  node [
    id 269
    label "podniesienie"
  ]
  node [
    id 270
    label "credo"
  ]
  node [
    id 271
    label "episto&#322;a"
  ]
  node [
    id 272
    label "czytanie"
  ]
  node [
    id 273
    label "prawos&#322;awie"
  ]
  node [
    id 274
    label "kazanie"
  ]
  node [
    id 275
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 276
    label "kanon"
  ]
  node [
    id 277
    label "kolekta"
  ]
  node [
    id 278
    label "devotion"
  ]
  node [
    id 279
    label "powa&#380;anie"
  ]
  node [
    id 280
    label "dba&#322;o&#347;&#263;"
  ]
  node [
    id 281
    label "obrz&#281;d"
  ]
  node [
    id 282
    label "obrazowanie"
  ]
  node [
    id 283
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 284
    label "organ"
  ]
  node [
    id 285
    label "tre&#347;&#263;"
  ]
  node [
    id 286
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 287
    label "part"
  ]
  node [
    id 288
    label "element_anatomiczny"
  ]
  node [
    id 289
    label "tekst"
  ]
  node [
    id 290
    label "komunikat"
  ]
  node [
    id 291
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 292
    label "dorobek"
  ]
  node [
    id 293
    label "forma"
  ]
  node [
    id 294
    label "retrospektywa"
  ]
  node [
    id 295
    label "works"
  ]
  node [
    id 296
    label "creation"
  ]
  node [
    id 297
    label "tetralogia"
  ]
  node [
    id 298
    label "praca"
  ]
  node [
    id 299
    label "modlitwa"
  ]
  node [
    id 300
    label "wyznanie"
  ]
  node [
    id 301
    label "manifest"
  ]
  node [
    id 302
    label "spowied&#378;_powszechna"
  ]
  node [
    id 303
    label "bia&#322;y_tydzie&#324;"
  ]
  node [
    id 304
    label "przedmiot"
  ]
  node [
    id 305
    label "association"
  ]
  node [
    id 306
    label "praktyka"
  ]
  node [
    id 307
    label "zgoda"
  ]
  node [
    id 308
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 309
    label "sakrament"
  ]
  node [
    id 310
    label "zwi&#261;zek"
  ]
  node [
    id 311
    label "recitation"
  ]
  node [
    id 312
    label "dysleksja"
  ]
  node [
    id 313
    label "wczytywanie_si&#281;"
  ]
  node [
    id 314
    label "poczytanie"
  ]
  node [
    id 315
    label "zaczytanie_si&#281;"
  ]
  node [
    id 316
    label "doczytywanie"
  ]
  node [
    id 317
    label "przepowiadanie"
  ]
  node [
    id 318
    label "obrz&#261;dek"
  ]
  node [
    id 319
    label "czytywanie"
  ]
  node [
    id 320
    label "Biblia"
  ]
  node [
    id 321
    label "pokazywanie"
  ]
  node [
    id 322
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 323
    label "wczytywanie"
  ]
  node [
    id 324
    label "reading"
  ]
  node [
    id 325
    label "bycie_w_stanie"
  ]
  node [
    id 326
    label "lektor"
  ]
  node [
    id 327
    label "poznawanie"
  ]
  node [
    id 328
    label "wyczytywanie"
  ]
  node [
    id 329
    label "oczytywanie_si&#281;"
  ]
  node [
    id 330
    label "zasada"
  ]
  node [
    id 331
    label "powiedzenie"
  ]
  node [
    id 332
    label "idea"
  ]
  node [
    id 333
    label "conversion"
  ]
  node [
    id 334
    label "odmienienie"
  ]
  node [
    id 335
    label "kwestarz"
  ]
  node [
    id 336
    label "kwestowanie"
  ]
  node [
    id 337
    label "collection"
  ]
  node [
    id 338
    label "czynno&#347;&#263;"
  ]
  node [
    id 339
    label "model"
  ]
  node [
    id 340
    label "stopie&#324;_pisma"
  ]
  node [
    id 341
    label "dekalog"
  ]
  node [
    id 342
    label "&#347;wi&#281;to&#347;&#263;"
  ]
  node [
    id 343
    label "styl"
  ]
  node [
    id 344
    label "criterion"
  ]
  node [
    id 345
    label "prawo"
  ]
  node [
    id 346
    label "znaczek_pocztowy"
  ]
  node [
    id 347
    label "epistolografia"
  ]
  node [
    id 348
    label "poczta"
  ]
  node [
    id 349
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 350
    label "przesy&#322;ka"
  ]
  node [
    id 351
    label "powi&#281;kszenie"
  ]
  node [
    id 352
    label "movement"
  ]
  node [
    id 353
    label "raise"
  ]
  node [
    id 354
    label "przewr&#243;cenie"
  ]
  node [
    id 355
    label "pomo&#380;enie"
  ]
  node [
    id 356
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 357
    label "erection"
  ]
  node [
    id 358
    label "za&#322;apanie"
  ]
  node [
    id 359
    label "spowodowanie"
  ]
  node [
    id 360
    label "ulepszenie"
  ]
  node [
    id 361
    label "policzenie"
  ]
  node [
    id 362
    label "przybli&#380;enie"
  ]
  node [
    id 363
    label "erecting"
  ]
  node [
    id 364
    label "przemieszczenie"
  ]
  node [
    id 365
    label "pochwalenie"
  ]
  node [
    id 366
    label "wywy&#380;szenie"
  ]
  node [
    id 367
    label "wy&#380;szy"
  ]
  node [
    id 368
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 369
    label "zacz&#281;cie"
  ]
  node [
    id 370
    label "zmienienie"
  ]
  node [
    id 371
    label "odbudowanie"
  ]
  node [
    id 372
    label "apokryf"
  ]
  node [
    id 373
    label "ewanielia"
  ]
  node [
    id 374
    label "nakazywanie"
  ]
  node [
    id 375
    label "krytyka"
  ]
  node [
    id 376
    label "wyg&#322;oszenie"
  ]
  node [
    id 377
    label "zmuszanie"
  ]
  node [
    id 378
    label "wyg&#322;aszanie"
  ]
  node [
    id 379
    label "nakazanie"
  ]
  node [
    id 380
    label "wymaganie"
  ]
  node [
    id 381
    label "zmuszenie"
  ]
  node [
    id 382
    label "sermon"
  ]
  node [
    id 383
    label "command"
  ]
  node [
    id 384
    label "order"
  ]
  node [
    id 385
    label "&#347;piew"
  ]
  node [
    id 386
    label "sacrifice"
  ]
  node [
    id 387
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 388
    label "podarowanie"
  ]
  node [
    id 389
    label "zaproponowanie"
  ]
  node [
    id 390
    label "oferowanie"
  ]
  node [
    id 391
    label "duch"
  ]
  node [
    id 392
    label "ofiarowywanie"
  ]
  node [
    id 393
    label "forfeit"
  ]
  node [
    id 394
    label "crack"
  ]
  node [
    id 395
    label "b&#243;g"
  ]
  node [
    id 396
    label "deklarowanie"
  ]
  node [
    id 397
    label "B&#243;g"
  ]
  node [
    id 398
    label "zdeklarowanie"
  ]
  node [
    id 399
    label "nimbus"
  ]
  node [
    id 400
    label "&#347;wi&#281;ty"
  ]
  node [
    id 401
    label "k&#243;&#322;ko"
  ]
  node [
    id 402
    label "hyr"
  ]
  node [
    id 403
    label "renoma"
  ]
  node [
    id 404
    label "zjawisko"
  ]
  node [
    id 405
    label "atrybut"
  ]
  node [
    id 406
    label "Tischner"
  ]
  node [
    id 407
    label "zwierzchnik"
  ]
  node [
    id 408
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 409
    label "Tadeusz_Rydzyk"
  ]
  node [
    id 410
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 411
    label "odprawia&#263;"
  ]
  node [
    id 412
    label "kap&#322;an"
  ]
  node [
    id 413
    label "oratorianie"
  ]
  node [
    id 414
    label "chrze&#347;cija&#324;stwo"
  ]
  node [
    id 415
    label "starokatolicyzm"
  ]
  node [
    id 416
    label "chrze&#347;cija&#324;stwo_zachodnie"
  ]
  node [
    id 417
    label "objawienie"
  ]
  node [
    id 418
    label "koncyliaryzm"
  ]
  node [
    id 419
    label "grekokatolicyzm"
  ]
  node [
    id 420
    label "katolicyzm_dynamiczny"
  ]
  node [
    id 421
    label "staroprawos&#322;awie"
  ]
  node [
    id 422
    label "stauropigia"
  ]
  node [
    id 423
    label "talk"
  ]
  node [
    id 424
    label "wypowiada&#263;"
  ]
  node [
    id 425
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 426
    label "express"
  ]
  node [
    id 427
    label "werbalizowa&#263;"
  ]
  node [
    id 428
    label "typify"
  ]
  node [
    id 429
    label "say"
  ]
  node [
    id 430
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 431
    label "wydobywa&#263;"
  ]
  node [
    id 432
    label "kosmetyk"
  ]
  node [
    id 433
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 434
    label "ksi&#281;&#380;a"
  ]
  node [
    id 435
    label "rozgrzeszanie"
  ]
  node [
    id 436
    label "duszpasterstwo"
  ]
  node [
    id 437
    label "eklezjasta"
  ]
  node [
    id 438
    label "duchowny"
  ]
  node [
    id 439
    label "rozgrzesza&#263;"
  ]
  node [
    id 440
    label "seminarzysta"
  ]
  node [
    id 441
    label "klecha"
  ]
  node [
    id 442
    label "pasterz"
  ]
  node [
    id 443
    label "kol&#281;da"
  ]
  node [
    id 444
    label "duchowie&#324;stwo"
  ]
  node [
    id 445
    label "&#347;rodowisko"
  ]
  node [
    id 446
    label "stowarzyszenie_religijne"
  ]
  node [
    id 447
    label "apostolstwo"
  ]
  node [
    id 448
    label "kaznodziejstwo"
  ]
  node [
    id 449
    label "rozsiewca"
  ]
  node [
    id 450
    label "chor&#261;&#380;y"
  ]
  node [
    id 451
    label "tuba"
  ]
  node [
    id 452
    label "zwolennik"
  ]
  node [
    id 453
    label "popularyzator"
  ]
  node [
    id 454
    label "wyznawca"
  ]
  node [
    id 455
    label "Luter"
  ]
  node [
    id 456
    label "religia"
  ]
  node [
    id 457
    label "Bayes"
  ]
  node [
    id 458
    label "sekularyzacja"
  ]
  node [
    id 459
    label "tonsura"
  ]
  node [
    id 460
    label "Hus"
  ]
  node [
    id 461
    label "religijny"
  ]
  node [
    id 462
    label "przedstawiciel"
  ]
  node [
    id 463
    label "&#347;w"
  ]
  node [
    id 464
    label "kongregacja"
  ]
  node [
    id 465
    label "wie&#347;niak"
  ]
  node [
    id 466
    label "Pan"
  ]
  node [
    id 467
    label "szpak"
  ]
  node [
    id 468
    label "hodowca"
  ]
  node [
    id 469
    label "pracownik_fizyczny"
  ]
  node [
    id 470
    label "Grek"
  ]
  node [
    id 471
    label "obywatel"
  ]
  node [
    id 472
    label "eklezja"
  ]
  node [
    id 473
    label "kaznodzieja"
  ]
  node [
    id 474
    label "odwiedziny"
  ]
  node [
    id 475
    label "pie&#347;&#324;"
  ]
  node [
    id 476
    label "kol&#281;dnik"
  ]
  node [
    id 477
    label "zwyczaj_ludowy"
  ]
  node [
    id 478
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 479
    label "robi&#263;"
  ]
  node [
    id 480
    label "wybacza&#263;"
  ]
  node [
    id 481
    label "grzesznik"
  ]
  node [
    id 482
    label "udziela&#263;"
  ]
  node [
    id 483
    label "wybaczanie"
  ]
  node [
    id 484
    label "robienie"
  ]
  node [
    id 485
    label "udzielanie"
  ]
  node [
    id 486
    label "t&#322;umaczenie"
  ]
  node [
    id 487
    label "spowiadanie"
  ]
  node [
    id 488
    label "uczestnik"
  ]
  node [
    id 489
    label "ucze&#324;"
  ]
  node [
    id 490
    label "student"
  ]
  node [
    id 491
    label "sposobi&#263;"
  ]
  node [
    id 492
    label "wytwarza&#263;"
  ]
  node [
    id 493
    label "usposabia&#263;"
  ]
  node [
    id 494
    label "train"
  ]
  node [
    id 495
    label "arrange"
  ]
  node [
    id 496
    label "szkoli&#263;"
  ]
  node [
    id 497
    label "wykonywa&#263;"
  ]
  node [
    id 498
    label "pryczy&#263;"
  ]
  node [
    id 499
    label "organizowa&#263;"
  ]
  node [
    id 500
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 501
    label "czyni&#263;"
  ]
  node [
    id 502
    label "give"
  ]
  node [
    id 503
    label "stylizowa&#263;"
  ]
  node [
    id 504
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 505
    label "falowa&#263;"
  ]
  node [
    id 506
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 507
    label "wydala&#263;"
  ]
  node [
    id 508
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 509
    label "tentegowa&#263;"
  ]
  node [
    id 510
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 511
    label "urz&#261;dza&#263;"
  ]
  node [
    id 512
    label "oszukiwa&#263;"
  ]
  node [
    id 513
    label "work"
  ]
  node [
    id 514
    label "ukazywa&#263;"
  ]
  node [
    id 515
    label "przerabia&#263;"
  ]
  node [
    id 516
    label "act"
  ]
  node [
    id 517
    label "post&#281;powa&#263;"
  ]
  node [
    id 518
    label "prowadzi&#263;"
  ]
  node [
    id 519
    label "doskonali&#263;"
  ]
  node [
    id 520
    label "o&#347;wieca&#263;"
  ]
  node [
    id 521
    label "pomaga&#263;"
  ]
  node [
    id 522
    label "create"
  ]
  node [
    id 523
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 524
    label "predispose"
  ]
  node [
    id 525
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 526
    label "muzyka"
  ]
  node [
    id 527
    label "rola"
  ]
  node [
    id 528
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 529
    label "d&#378;wiga&#263;"
  ]
  node [
    id 530
    label "gotowa&#263;"
  ]
  node [
    id 531
    label "bra&#263;"
  ]
  node [
    id 532
    label "ratification"
  ]
  node [
    id 533
    label "confirmation"
  ]
  node [
    id 534
    label "inicjacja"
  ]
  node [
    id 535
    label "konfirmacja"
  ]
  node [
    id 536
    label "poj&#281;cie"
  ]
  node [
    id 537
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 538
    label "knowledgeability"
  ]
  node [
    id 539
    label "initiation"
  ]
  node [
    id 540
    label "poznanie"
  ]
  node [
    id 541
    label "pocz&#261;tek"
  ]
  node [
    id 542
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 543
    label "wydarzenie"
  ]
  node [
    id 544
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 545
    label "egzaltacja"
  ]
  node [
    id 546
    label "atmosfera"
  ]
  node [
    id 547
    label "cecha"
  ]
  node [
    id 548
    label "fabrication"
  ]
  node [
    id 549
    label "porobienie"
  ]
  node [
    id 550
    label "bycie"
  ]
  node [
    id 551
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 552
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 553
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 554
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 555
    label "tentegowanie"
  ]
  node [
    id 556
    label "activity"
  ]
  node [
    id 557
    label "bezproblemowy"
  ]
  node [
    id 558
    label "zezwalanie"
  ]
  node [
    id 559
    label "dawanie"
  ]
  node [
    id 560
    label "communication"
  ]
  node [
    id 561
    label "przyznawanie"
  ]
  node [
    id 562
    label "odst&#281;powanie"
  ]
  node [
    id 563
    label "udost&#281;pnianie"
  ]
  node [
    id 564
    label "rozumowanie"
  ]
  node [
    id 565
    label "judaizm"
  ]
  node [
    id 566
    label "akceptacja"
  ]
  node [
    id 567
    label "certification"
  ]
  node [
    id 568
    label "potwierdzenie"
  ]
  node [
    id 569
    label "proszek"
  ]
  node [
    id 570
    label "tablet"
  ]
  node [
    id 571
    label "dawka"
  ]
  node [
    id 572
    label "blister"
  ]
  node [
    id 573
    label "lekarstwo"
  ]
  node [
    id 574
    label "w_chuj"
  ]
  node [
    id 575
    label "szko&#322;a"
  ]
  node [
    id 576
    label "fraza"
  ]
  node [
    id 577
    label "przekazanie"
  ]
  node [
    id 578
    label "stanowisko"
  ]
  node [
    id 579
    label "wypowiedzenie"
  ]
  node [
    id 580
    label "prison_term"
  ]
  node [
    id 581
    label "system"
  ]
  node [
    id 582
    label "okres"
  ]
  node [
    id 583
    label "przedstawienie"
  ]
  node [
    id 584
    label "wyra&#380;enie"
  ]
  node [
    id 585
    label "zaliczenie"
  ]
  node [
    id 586
    label "antylogizm"
  ]
  node [
    id 587
    label "konektyw"
  ]
  node [
    id 588
    label "attitude"
  ]
  node [
    id 589
    label "powierzenie"
  ]
  node [
    id 590
    label "adjudication"
  ]
  node [
    id 591
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 592
    label "pass"
  ]
  node [
    id 593
    label "spe&#322;nienie"
  ]
  node [
    id 594
    label "wliczenie"
  ]
  node [
    id 595
    label "zaliczanie"
  ]
  node [
    id 596
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 597
    label "zadanie"
  ]
  node [
    id 598
    label "odb&#281;bnienie"
  ]
  node [
    id 599
    label "ocenienie"
  ]
  node [
    id 600
    label "number"
  ]
  node [
    id 601
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 602
    label "przeklasyfikowanie"
  ]
  node [
    id 603
    label "zaliczanie_si&#281;"
  ]
  node [
    id 604
    label "wzi&#281;cie"
  ]
  node [
    id 605
    label "dor&#281;czenie"
  ]
  node [
    id 606
    label "wys&#322;anie"
  ]
  node [
    id 607
    label "podanie"
  ]
  node [
    id 608
    label "delivery"
  ]
  node [
    id 609
    label "transfer"
  ]
  node [
    id 610
    label "wp&#322;acenie"
  ]
  node [
    id 611
    label "z&#322;o&#380;enie"
  ]
  node [
    id 612
    label "sygna&#322;"
  ]
  node [
    id 613
    label "zrobienie"
  ]
  node [
    id 614
    label "leksem"
  ]
  node [
    id 615
    label "sformu&#322;owanie"
  ]
  node [
    id 616
    label "zdarzenie_si&#281;"
  ]
  node [
    id 617
    label "poinformowanie"
  ]
  node [
    id 618
    label "wording"
  ]
  node [
    id 619
    label "oznaczenie"
  ]
  node [
    id 620
    label "znak_j&#281;zykowy"
  ]
  node [
    id 621
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 622
    label "ozdobnik"
  ]
  node [
    id 623
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 624
    label "grupa_imienna"
  ]
  node [
    id 625
    label "jednostka_leksykalna"
  ]
  node [
    id 626
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 627
    label "ujawnienie"
  ]
  node [
    id 628
    label "affirmation"
  ]
  node [
    id 629
    label "zapisanie"
  ]
  node [
    id 630
    label "rzucenie"
  ]
  node [
    id 631
    label "pr&#243;bowanie"
  ]
  node [
    id 632
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 633
    label "zademonstrowanie"
  ]
  node [
    id 634
    label "report"
  ]
  node [
    id 635
    label "obgadanie"
  ]
  node [
    id 636
    label "realizacja"
  ]
  node [
    id 637
    label "scena"
  ]
  node [
    id 638
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 639
    label "narration"
  ]
  node [
    id 640
    label "cyrk"
  ]
  node [
    id 641
    label "wytw&#243;r"
  ]
  node [
    id 642
    label "posta&#263;"
  ]
  node [
    id 643
    label "theatrical_performance"
  ]
  node [
    id 644
    label "opisanie"
  ]
  node [
    id 645
    label "malarstwo"
  ]
  node [
    id 646
    label "scenografia"
  ]
  node [
    id 647
    label "teatr"
  ]
  node [
    id 648
    label "ukazanie"
  ]
  node [
    id 649
    label "zapoznanie"
  ]
  node [
    id 650
    label "pokaz"
  ]
  node [
    id 651
    label "ods&#322;ona"
  ]
  node [
    id 652
    label "exhibit"
  ]
  node [
    id 653
    label "pokazanie"
  ]
  node [
    id 654
    label "wyst&#261;pienie"
  ]
  node [
    id 655
    label "przedstawi&#263;"
  ]
  node [
    id 656
    label "przedstawianie"
  ]
  node [
    id 657
    label "przedstawia&#263;"
  ]
  node [
    id 658
    label "constraint"
  ]
  node [
    id 659
    label "zadanie_gwa&#322;tu"
  ]
  node [
    id 660
    label "oddzia&#322;anie"
  ]
  node [
    id 661
    label "force"
  ]
  node [
    id 662
    label "pop&#281;dzenie_"
  ]
  node [
    id 663
    label "konwersja"
  ]
  node [
    id 664
    label "notice"
  ]
  node [
    id 665
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 666
    label "przepowiedzenie"
  ]
  node [
    id 667
    label "rozwi&#261;zanie"
  ]
  node [
    id 668
    label "generowa&#263;"
  ]
  node [
    id 669
    label "wydanie"
  ]
  node [
    id 670
    label "message"
  ]
  node [
    id 671
    label "generowanie"
  ]
  node [
    id 672
    label "wydobycie"
  ]
  node [
    id 673
    label "zwerbalizowanie"
  ]
  node [
    id 674
    label "szyk"
  ]
  node [
    id 675
    label "notification"
  ]
  node [
    id 676
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 677
    label "denunciation"
  ]
  node [
    id 678
    label "po&#322;o&#380;enie"
  ]
  node [
    id 679
    label "punkt"
  ]
  node [
    id 680
    label "pogl&#261;d"
  ]
  node [
    id 681
    label "wojsko"
  ]
  node [
    id 682
    label "awansowa&#263;"
  ]
  node [
    id 683
    label "stawia&#263;"
  ]
  node [
    id 684
    label "uprawianie"
  ]
  node [
    id 685
    label "wakowa&#263;"
  ]
  node [
    id 686
    label "powierzanie"
  ]
  node [
    id 687
    label "postawi&#263;"
  ]
  node [
    id 688
    label "miejsce"
  ]
  node [
    id 689
    label "awansowanie"
  ]
  node [
    id 690
    label "zlecenie"
  ]
  node [
    id 691
    label "ufanie"
  ]
  node [
    id 692
    label "commitment"
  ]
  node [
    id 693
    label "perpetration"
  ]
  node [
    id 694
    label "oddanie"
  ]
  node [
    id 695
    label "do&#347;wiadczenie"
  ]
  node [
    id 696
    label "teren_szko&#322;y"
  ]
  node [
    id 697
    label "wiedza"
  ]
  node [
    id 698
    label "Mickiewicz"
  ]
  node [
    id 699
    label "kwalifikacje"
  ]
  node [
    id 700
    label "podr&#281;cznik"
  ]
  node [
    id 701
    label "absolwent"
  ]
  node [
    id 702
    label "school"
  ]
  node [
    id 703
    label "zda&#263;"
  ]
  node [
    id 704
    label "gabinet"
  ]
  node [
    id 705
    label "urszulanki"
  ]
  node [
    id 706
    label "sztuba"
  ]
  node [
    id 707
    label "&#322;awa_szkolna"
  ]
  node [
    id 708
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 709
    label "przepisa&#263;"
  ]
  node [
    id 710
    label "form"
  ]
  node [
    id 711
    label "lekcja"
  ]
  node [
    id 712
    label "metoda"
  ]
  node [
    id 713
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 714
    label "przepisanie"
  ]
  node [
    id 715
    label "skolaryzacja"
  ]
  node [
    id 716
    label "stopek"
  ]
  node [
    id 717
    label "sekretariat"
  ]
  node [
    id 718
    label "ideologia"
  ]
  node [
    id 719
    label "lesson"
  ]
  node [
    id 720
    label "instytucja"
  ]
  node [
    id 721
    label "niepokalanki"
  ]
  node [
    id 722
    label "siedziba"
  ]
  node [
    id 723
    label "szkolenie"
  ]
  node [
    id 724
    label "kara"
  ]
  node [
    id 725
    label "tablica"
  ]
  node [
    id 726
    label "s&#261;d"
  ]
  node [
    id 727
    label "funktor"
  ]
  node [
    id 728
    label "j&#261;dro"
  ]
  node [
    id 729
    label "systemik"
  ]
  node [
    id 730
    label "rozprz&#261;c"
  ]
  node [
    id 731
    label "oprogramowanie"
  ]
  node [
    id 732
    label "systemat"
  ]
  node [
    id 733
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 734
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 735
    label "struktura"
  ]
  node [
    id 736
    label "usenet"
  ]
  node [
    id 737
    label "porz&#261;dek"
  ]
  node [
    id 738
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 739
    label "przyn&#281;ta"
  ]
  node [
    id 740
    label "p&#322;&#243;d"
  ]
  node [
    id 741
    label "net"
  ]
  node [
    id 742
    label "w&#281;dkarstwo"
  ]
  node [
    id 743
    label "eratem"
  ]
  node [
    id 744
    label "doktryna"
  ]
  node [
    id 745
    label "pulpit"
  ]
  node [
    id 746
    label "konstelacja"
  ]
  node [
    id 747
    label "jednostka_geologiczna"
  ]
  node [
    id 748
    label "o&#347;"
  ]
  node [
    id 749
    label "podsystem"
  ]
  node [
    id 750
    label "ryba"
  ]
  node [
    id 751
    label "Leopard"
  ]
  node [
    id 752
    label "Android"
  ]
  node [
    id 753
    label "zachowanie"
  ]
  node [
    id 754
    label "cybernetyk"
  ]
  node [
    id 755
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 756
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 757
    label "method"
  ]
  node [
    id 758
    label "sk&#322;ad"
  ]
  node [
    id 759
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 760
    label "relacja_logiczna"
  ]
  node [
    id 761
    label "okres_amazo&#324;ski"
  ]
  node [
    id 762
    label "stater"
  ]
  node [
    id 763
    label "flow"
  ]
  node [
    id 764
    label "choroba_przyrodzona"
  ]
  node [
    id 765
    label "postglacja&#322;"
  ]
  node [
    id 766
    label "sylur"
  ]
  node [
    id 767
    label "kreda"
  ]
  node [
    id 768
    label "ordowik"
  ]
  node [
    id 769
    label "okres_hesperyjski"
  ]
  node [
    id 770
    label "paleogen"
  ]
  node [
    id 771
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 772
    label "okres_halsztacki"
  ]
  node [
    id 773
    label "riak"
  ]
  node [
    id 774
    label "czwartorz&#281;d"
  ]
  node [
    id 775
    label "podokres"
  ]
  node [
    id 776
    label "trzeciorz&#281;d"
  ]
  node [
    id 777
    label "kalim"
  ]
  node [
    id 778
    label "fala"
  ]
  node [
    id 779
    label "perm"
  ]
  node [
    id 780
    label "retoryka"
  ]
  node [
    id 781
    label "prekambr"
  ]
  node [
    id 782
    label "faza"
  ]
  node [
    id 783
    label "neogen"
  ]
  node [
    id 784
    label "pulsacja"
  ]
  node [
    id 785
    label "proces_fizjologiczny"
  ]
  node [
    id 786
    label "kambr"
  ]
  node [
    id 787
    label "kriogen"
  ]
  node [
    id 788
    label "orosir"
  ]
  node [
    id 789
    label "poprzednik"
  ]
  node [
    id 790
    label "spell"
  ]
  node [
    id 791
    label "interstadia&#322;"
  ]
  node [
    id 792
    label "ektas"
  ]
  node [
    id 793
    label "sider"
  ]
  node [
    id 794
    label "epoka"
  ]
  node [
    id 795
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 796
    label "cykl"
  ]
  node [
    id 797
    label "ciota"
  ]
  node [
    id 798
    label "pierwszorz&#281;d"
  ]
  node [
    id 799
    label "okres_noachijski"
  ]
  node [
    id 800
    label "ediakar"
  ]
  node [
    id 801
    label "nast&#281;pnik"
  ]
  node [
    id 802
    label "condition"
  ]
  node [
    id 803
    label "jura"
  ]
  node [
    id 804
    label "glacja&#322;"
  ]
  node [
    id 805
    label "sten"
  ]
  node [
    id 806
    label "era"
  ]
  node [
    id 807
    label "trias"
  ]
  node [
    id 808
    label "p&#243;&#322;okres"
  ]
  node [
    id 809
    label "dewon"
  ]
  node [
    id 810
    label "karbon"
  ]
  node [
    id 811
    label "izochronizm"
  ]
  node [
    id 812
    label "preglacja&#322;"
  ]
  node [
    id 813
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 814
    label "drugorz&#281;d"
  ]
  node [
    id 815
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 816
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 817
    label "motyw"
  ]
  node [
    id 818
    label "can"
  ]
  node [
    id 819
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 820
    label "umie&#263;"
  ]
  node [
    id 821
    label "cope"
  ]
  node [
    id 822
    label "potrafia&#263;"
  ]
  node [
    id 823
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 824
    label "wiedzie&#263;"
  ]
  node [
    id 825
    label "m&#243;c"
  ]
  node [
    id 826
    label "gaworzy&#263;"
  ]
  node [
    id 827
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 828
    label "chatter"
  ]
  node [
    id 829
    label "m&#243;wi&#263;"
  ]
  node [
    id 830
    label "niemowl&#281;"
  ]
  node [
    id 831
    label "dobrze"
  ]
  node [
    id 832
    label "zmy&#347;lnie"
  ]
  node [
    id 833
    label "inteligentny"
  ]
  node [
    id 834
    label "m&#261;drze"
  ]
  node [
    id 835
    label "pomys&#322;owo"
  ]
  node [
    id 836
    label "zaskakuj&#261;co"
  ]
  node [
    id 837
    label "zmy&#347;lny"
  ]
  node [
    id 838
    label "nieg&#322;upio"
  ]
  node [
    id 839
    label "m&#261;dry"
  ]
  node [
    id 840
    label "skomplikowanie"
  ]
  node [
    id 841
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 842
    label "odpowiednio"
  ]
  node [
    id 843
    label "dobroczynnie"
  ]
  node [
    id 844
    label "moralnie"
  ]
  node [
    id 845
    label "korzystnie"
  ]
  node [
    id 846
    label "pozytywnie"
  ]
  node [
    id 847
    label "lepiej"
  ]
  node [
    id 848
    label "wiele"
  ]
  node [
    id 849
    label "skutecznie"
  ]
  node [
    id 850
    label "pomy&#347;lnie"
  ]
  node [
    id 851
    label "dobry"
  ]
  node [
    id 852
    label "my&#347;l&#261;cy"
  ]
  node [
    id 853
    label "wysokich_lot&#243;w"
  ]
  node [
    id 854
    label "organizm"
  ]
  node [
    id 855
    label "fledgling"
  ]
  node [
    id 856
    label "zwierz&#281;"
  ]
  node [
    id 857
    label "m&#322;odziak"
  ]
  node [
    id 858
    label "potomstwo"
  ]
  node [
    id 859
    label "czeladka"
  ]
  node [
    id 860
    label "dzietno&#347;&#263;"
  ]
  node [
    id 861
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 862
    label "bawienie_si&#281;"
  ]
  node [
    id 863
    label "pomiot"
  ]
  node [
    id 864
    label "degenerat"
  ]
  node [
    id 865
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 866
    label "zwyrol"
  ]
  node [
    id 867
    label "czerniak"
  ]
  node [
    id 868
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 869
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 870
    label "paszcza"
  ]
  node [
    id 871
    label "popapraniec"
  ]
  node [
    id 872
    label "skuba&#263;"
  ]
  node [
    id 873
    label "skubanie"
  ]
  node [
    id 874
    label "skubni&#281;cie"
  ]
  node [
    id 875
    label "agresja"
  ]
  node [
    id 876
    label "zwierz&#281;ta"
  ]
  node [
    id 877
    label "fukni&#281;cie"
  ]
  node [
    id 878
    label "farba"
  ]
  node [
    id 879
    label "fukanie"
  ]
  node [
    id 880
    label "istota_&#380;ywa"
  ]
  node [
    id 881
    label "gad"
  ]
  node [
    id 882
    label "siedzie&#263;"
  ]
  node [
    id 883
    label "oswaja&#263;"
  ]
  node [
    id 884
    label "tresowa&#263;"
  ]
  node [
    id 885
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 886
    label "poligamia"
  ]
  node [
    id 887
    label "oz&#243;r"
  ]
  node [
    id 888
    label "skubn&#261;&#263;"
  ]
  node [
    id 889
    label "wios&#322;owa&#263;"
  ]
  node [
    id 890
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 891
    label "le&#380;enie"
  ]
  node [
    id 892
    label "niecz&#322;owiek"
  ]
  node [
    id 893
    label "wios&#322;owanie"
  ]
  node [
    id 894
    label "napasienie_si&#281;"
  ]
  node [
    id 895
    label "wiwarium"
  ]
  node [
    id 896
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 897
    label "animalista"
  ]
  node [
    id 898
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 899
    label "budowa"
  ]
  node [
    id 900
    label "hodowla"
  ]
  node [
    id 901
    label "pasienie_si&#281;"
  ]
  node [
    id 902
    label "sodomita"
  ]
  node [
    id 903
    label "monogamia"
  ]
  node [
    id 904
    label "przyssawka"
  ]
  node [
    id 905
    label "budowa_cia&#322;a"
  ]
  node [
    id 906
    label "okrutnik"
  ]
  node [
    id 907
    label "grzbiet"
  ]
  node [
    id 908
    label "weterynarz"
  ]
  node [
    id 909
    label "&#322;eb"
  ]
  node [
    id 910
    label "wylinka"
  ]
  node [
    id 911
    label "bestia"
  ]
  node [
    id 912
    label "poskramia&#263;"
  ]
  node [
    id 913
    label "fauna"
  ]
  node [
    id 914
    label "treser"
  ]
  node [
    id 915
    label "siedzenie"
  ]
  node [
    id 916
    label "le&#380;e&#263;"
  ]
  node [
    id 917
    label "p&#322;aszczyzna"
  ]
  node [
    id 918
    label "odwadnia&#263;"
  ]
  node [
    id 919
    label "przyswoi&#263;"
  ]
  node [
    id 920
    label "sk&#243;ra"
  ]
  node [
    id 921
    label "odwodni&#263;"
  ]
  node [
    id 922
    label "ewoluowanie"
  ]
  node [
    id 923
    label "staw"
  ]
  node [
    id 924
    label "ow&#322;osienie"
  ]
  node [
    id 925
    label "unerwienie"
  ]
  node [
    id 926
    label "reakcja"
  ]
  node [
    id 927
    label "wyewoluowanie"
  ]
  node [
    id 928
    label "przyswajanie"
  ]
  node [
    id 929
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 930
    label "wyewoluowa&#263;"
  ]
  node [
    id 931
    label "biorytm"
  ]
  node [
    id 932
    label "ewoluowa&#263;"
  ]
  node [
    id 933
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 934
    label "otworzy&#263;"
  ]
  node [
    id 935
    label "otwiera&#263;"
  ]
  node [
    id 936
    label "czynnik_biotyczny"
  ]
  node [
    id 937
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 938
    label "otworzenie"
  ]
  node [
    id 939
    label "otwieranie"
  ]
  node [
    id 940
    label "individual"
  ]
  node [
    id 941
    label "szkielet"
  ]
  node [
    id 942
    label "ty&#322;"
  ]
  node [
    id 943
    label "obiekt"
  ]
  node [
    id 944
    label "przyswaja&#263;"
  ]
  node [
    id 945
    label "przyswojenie"
  ]
  node [
    id 946
    label "odwadnianie"
  ]
  node [
    id 947
    label "odwodnienie"
  ]
  node [
    id 948
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 949
    label "starzenie_si&#281;"
  ]
  node [
    id 950
    label "prz&#243;d"
  ]
  node [
    id 951
    label "uk&#322;ad"
  ]
  node [
    id 952
    label "temperatura"
  ]
  node [
    id 953
    label "l&#281;d&#378;wie"
  ]
  node [
    id 954
    label "cia&#322;o"
  ]
  node [
    id 955
    label "cz&#322;onek"
  ]
  node [
    id 956
    label "ch&#322;opta&#347;"
  ]
  node [
    id 957
    label "niepe&#322;noletni"
  ]
  node [
    id 958
    label "go&#322;ow&#261;s"
  ]
  node [
    id 959
    label "g&#243;wniarz"
  ]
  node [
    id 960
    label "ludzko&#347;&#263;"
  ]
  node [
    id 961
    label "wapniak"
  ]
  node [
    id 962
    label "os&#322;abia&#263;"
  ]
  node [
    id 963
    label "hominid"
  ]
  node [
    id 964
    label "podw&#322;adny"
  ]
  node [
    id 965
    label "os&#322;abianie"
  ]
  node [
    id 966
    label "g&#322;owa"
  ]
  node [
    id 967
    label "figura"
  ]
  node [
    id 968
    label "portrecista"
  ]
  node [
    id 969
    label "dwun&#243;g"
  ]
  node [
    id 970
    label "profanum"
  ]
  node [
    id 971
    label "mikrokosmos"
  ]
  node [
    id 972
    label "nasada"
  ]
  node [
    id 973
    label "antropochoria"
  ]
  node [
    id 974
    label "osoba"
  ]
  node [
    id 975
    label "wz&#243;r"
  ]
  node [
    id 976
    label "senior"
  ]
  node [
    id 977
    label "oddzia&#322;ywanie"
  ]
  node [
    id 978
    label "Adam"
  ]
  node [
    id 979
    label "homo_sapiens"
  ]
  node [
    id 980
    label "polifag"
  ]
  node [
    id 981
    label "konsument"
  ]
  node [
    id 982
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 983
    label "cz&#322;owiekowate"
  ]
  node [
    id 984
    label "pracownik"
  ]
  node [
    id 985
    label "Chocho&#322;"
  ]
  node [
    id 986
    label "Herkules_Poirot"
  ]
  node [
    id 987
    label "Edyp"
  ]
  node [
    id 988
    label "parali&#380;owa&#263;"
  ]
  node [
    id 989
    label "Harry_Potter"
  ]
  node [
    id 990
    label "Casanova"
  ]
  node [
    id 991
    label "Zgredek"
  ]
  node [
    id 992
    label "Gargantua"
  ]
  node [
    id 993
    label "Winnetou"
  ]
  node [
    id 994
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 995
    label "Dulcynea"
  ]
  node [
    id 996
    label "person"
  ]
  node [
    id 997
    label "Plastu&#347;"
  ]
  node [
    id 998
    label "Quasimodo"
  ]
  node [
    id 999
    label "Sherlock_Holmes"
  ]
  node [
    id 1000
    label "Faust"
  ]
  node [
    id 1001
    label "Wallenrod"
  ]
  node [
    id 1002
    label "Dwukwiat"
  ]
  node [
    id 1003
    label "Don_Juan"
  ]
  node [
    id 1004
    label "Don_Kiszot"
  ]
  node [
    id 1005
    label "Hamlet"
  ]
  node [
    id 1006
    label "Werter"
  ]
  node [
    id 1007
    label "istota"
  ]
  node [
    id 1008
    label "Szwejk"
  ]
  node [
    id 1009
    label "doros&#322;y"
  ]
  node [
    id 1010
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 1011
    label "jajko"
  ]
  node [
    id 1012
    label "rodzic"
  ]
  node [
    id 1013
    label "wapniaki"
  ]
  node [
    id 1014
    label "feuda&#322;"
  ]
  node [
    id 1015
    label "starzec"
  ]
  node [
    id 1016
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 1017
    label "zawodnik"
  ]
  node [
    id 1018
    label "komendancja"
  ]
  node [
    id 1019
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 1020
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1021
    label "absorption"
  ]
  node [
    id 1022
    label "pobieranie"
  ]
  node [
    id 1023
    label "czerpanie"
  ]
  node [
    id 1024
    label "acquisition"
  ]
  node [
    id 1025
    label "zmienianie"
  ]
  node [
    id 1026
    label "assimilation"
  ]
  node [
    id 1027
    label "upodabnianie"
  ]
  node [
    id 1028
    label "g&#322;oska"
  ]
  node [
    id 1029
    label "kultura"
  ]
  node [
    id 1030
    label "podobny"
  ]
  node [
    id 1031
    label "fonetyka"
  ]
  node [
    id 1032
    label "suppress"
  ]
  node [
    id 1033
    label "os&#322;abienie"
  ]
  node [
    id 1034
    label "kondycja_fizyczna"
  ]
  node [
    id 1035
    label "os&#322;abi&#263;"
  ]
  node [
    id 1036
    label "zdrowie"
  ]
  node [
    id 1037
    label "powodowa&#263;"
  ]
  node [
    id 1038
    label "zmniejsza&#263;"
  ]
  node [
    id 1039
    label "bate"
  ]
  node [
    id 1040
    label "de-escalation"
  ]
  node [
    id 1041
    label "powodowanie"
  ]
  node [
    id 1042
    label "debilitation"
  ]
  node [
    id 1043
    label "zmniejszanie"
  ]
  node [
    id 1044
    label "s&#322;abszy"
  ]
  node [
    id 1045
    label "pogarszanie"
  ]
  node [
    id 1046
    label "assimilate"
  ]
  node [
    id 1047
    label "dostosowywa&#263;"
  ]
  node [
    id 1048
    label "dostosowa&#263;"
  ]
  node [
    id 1049
    label "przejmowa&#263;"
  ]
  node [
    id 1050
    label "upodobni&#263;"
  ]
  node [
    id 1051
    label "przej&#261;&#263;"
  ]
  node [
    id 1052
    label "upodabnia&#263;"
  ]
  node [
    id 1053
    label "pobiera&#263;"
  ]
  node [
    id 1054
    label "pobra&#263;"
  ]
  node [
    id 1055
    label "zapis"
  ]
  node [
    id 1056
    label "figure"
  ]
  node [
    id 1057
    label "typ"
  ]
  node [
    id 1058
    label "mildew"
  ]
  node [
    id 1059
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1060
    label "ideal"
  ]
  node [
    id 1061
    label "rule"
  ]
  node [
    id 1062
    label "ruch"
  ]
  node [
    id 1063
    label "dekal"
  ]
  node [
    id 1064
    label "projekt"
  ]
  node [
    id 1065
    label "charakterystyka"
  ]
  node [
    id 1066
    label "zaistnie&#263;"
  ]
  node [
    id 1067
    label "Osjan"
  ]
  node [
    id 1068
    label "kto&#347;"
  ]
  node [
    id 1069
    label "wygl&#261;d"
  ]
  node [
    id 1070
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1071
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1072
    label "trim"
  ]
  node [
    id 1073
    label "poby&#263;"
  ]
  node [
    id 1074
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1075
    label "Aspazja"
  ]
  node [
    id 1076
    label "punkt_widzenia"
  ]
  node [
    id 1077
    label "kompleksja"
  ]
  node [
    id 1078
    label "wytrzyma&#263;"
  ]
  node [
    id 1079
    label "pozosta&#263;"
  ]
  node [
    id 1080
    label "point"
  ]
  node [
    id 1081
    label "go&#347;&#263;"
  ]
  node [
    id 1082
    label "fotograf"
  ]
  node [
    id 1083
    label "malarz"
  ]
  node [
    id 1084
    label "artysta"
  ]
  node [
    id 1085
    label "hipnotyzowanie"
  ]
  node [
    id 1086
    label "&#347;lad"
  ]
  node [
    id 1087
    label "docieranie"
  ]
  node [
    id 1088
    label "natural_process"
  ]
  node [
    id 1089
    label "reakcja_chemiczna"
  ]
  node [
    id 1090
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1091
    label "rezultat"
  ]
  node [
    id 1092
    label "lobbysta"
  ]
  node [
    id 1093
    label "pryncypa&#322;"
  ]
  node [
    id 1094
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1095
    label "kszta&#322;t"
  ]
  node [
    id 1096
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1097
    label "kierowa&#263;"
  ]
  node [
    id 1098
    label "alkohol"
  ]
  node [
    id 1099
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1100
    label "&#380;ycie"
  ]
  node [
    id 1101
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1102
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1103
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1104
    label "sztuka"
  ]
  node [
    id 1105
    label "dekiel"
  ]
  node [
    id 1106
    label "ro&#347;lina"
  ]
  node [
    id 1107
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1108
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1109
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1110
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1111
    label "noosfera"
  ]
  node [
    id 1112
    label "byd&#322;o"
  ]
  node [
    id 1113
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1114
    label "makrocefalia"
  ]
  node [
    id 1115
    label "ucho"
  ]
  node [
    id 1116
    label "g&#243;ra"
  ]
  node [
    id 1117
    label "m&#243;zg"
  ]
  node [
    id 1118
    label "kierownictwo"
  ]
  node [
    id 1119
    label "fryzura"
  ]
  node [
    id 1120
    label "umys&#322;"
  ]
  node [
    id 1121
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1122
    label "czaszka"
  ]
  node [
    id 1123
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1124
    label "allochoria"
  ]
  node [
    id 1125
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1126
    label "bierka_szachowa"
  ]
  node [
    id 1127
    label "obiekt_matematyczny"
  ]
  node [
    id 1128
    label "gestaltyzm"
  ]
  node [
    id 1129
    label "obraz"
  ]
  node [
    id 1130
    label "rzecz"
  ]
  node [
    id 1131
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1132
    label "character"
  ]
  node [
    id 1133
    label "rze&#378;ba"
  ]
  node [
    id 1134
    label "stylistyka"
  ]
  node [
    id 1135
    label "antycypacja"
  ]
  node [
    id 1136
    label "ornamentyka"
  ]
  node [
    id 1137
    label "informacja"
  ]
  node [
    id 1138
    label "facet"
  ]
  node [
    id 1139
    label "popis"
  ]
  node [
    id 1140
    label "wiersz"
  ]
  node [
    id 1141
    label "symetria"
  ]
  node [
    id 1142
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1143
    label "karta"
  ]
  node [
    id 1144
    label "shape"
  ]
  node [
    id 1145
    label "podzbi&#243;r"
  ]
  node [
    id 1146
    label "perspektywa"
  ]
  node [
    id 1147
    label "dziedzina"
  ]
  node [
    id 1148
    label "nak&#322;adka"
  ]
  node [
    id 1149
    label "li&#347;&#263;"
  ]
  node [
    id 1150
    label "jama_gard&#322;owa"
  ]
  node [
    id 1151
    label "rezonator"
  ]
  node [
    id 1152
    label "base"
  ]
  node [
    id 1153
    label "piek&#322;o"
  ]
  node [
    id 1154
    label "human_body"
  ]
  node [
    id 1155
    label "sfera_afektywna"
  ]
  node [
    id 1156
    label "nekromancja"
  ]
  node [
    id 1157
    label "Po&#347;wist"
  ]
  node [
    id 1158
    label "podekscytowanie"
  ]
  node [
    id 1159
    label "deformowanie"
  ]
  node [
    id 1160
    label "sumienie"
  ]
  node [
    id 1161
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1162
    label "deformowa&#263;"
  ]
  node [
    id 1163
    label "psychika"
  ]
  node [
    id 1164
    label "zjawa"
  ]
  node [
    id 1165
    label "zmar&#322;y"
  ]
  node [
    id 1166
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1167
    label "power"
  ]
  node [
    id 1168
    label "entity"
  ]
  node [
    id 1169
    label "ofiarowywa&#263;"
  ]
  node [
    id 1170
    label "oddech"
  ]
  node [
    id 1171
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1172
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1173
    label "byt"
  ]
  node [
    id 1174
    label "si&#322;a"
  ]
  node [
    id 1175
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1176
    label "ego"
  ]
  node [
    id 1177
    label "charakter"
  ]
  node [
    id 1178
    label "fizjonomia"
  ]
  node [
    id 1179
    label "kompleks"
  ]
  node [
    id 1180
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1181
    label "T&#281;sknica"
  ]
  node [
    id 1182
    label "ofiarowa&#263;"
  ]
  node [
    id 1183
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1184
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1185
    label "passion"
  ]
  node [
    id 1186
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1187
    label "odbicie"
  ]
  node [
    id 1188
    label "atom"
  ]
  node [
    id 1189
    label "przyroda"
  ]
  node [
    id 1190
    label "Ziemia"
  ]
  node [
    id 1191
    label "kosmos"
  ]
  node [
    id 1192
    label "miniatura"
  ]
  node [
    id 1193
    label "ekstraspekcja"
  ]
  node [
    id 1194
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1195
    label "feeling"
  ]
  node [
    id 1196
    label "doznanie"
  ]
  node [
    id 1197
    label "smell"
  ]
  node [
    id 1198
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1199
    label "opanowanie"
  ]
  node [
    id 1200
    label "os&#322;upienie"
  ]
  node [
    id 1201
    label "zareagowanie"
  ]
  node [
    id 1202
    label "intuition"
  ]
  node [
    id 1203
    label "cognition"
  ]
  node [
    id 1204
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1205
    label "intelekt"
  ]
  node [
    id 1206
    label "pozwolenie"
  ]
  node [
    id 1207
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1208
    label "zaawansowanie"
  ]
  node [
    id 1209
    label "wykszta&#322;cenie"
  ]
  node [
    id 1210
    label "obserwacja"
  ]
  node [
    id 1211
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1212
    label "zmys&#322;"
  ]
  node [
    id 1213
    label "spotkanie"
  ]
  node [
    id 1214
    label "czucie"
  ]
  node [
    id 1215
    label "przeczulica"
  ]
  node [
    id 1216
    label "wyniesienie"
  ]
  node [
    id 1217
    label "podporz&#261;dkowanie"
  ]
  node [
    id 1218
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 1219
    label "dostanie"
  ]
  node [
    id 1220
    label "zr&#243;wnowa&#380;enie"
  ]
  node [
    id 1221
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 1222
    label "nauczenie_si&#281;"
  ]
  node [
    id 1223
    label "control"
  ]
  node [
    id 1224
    label "nasilenie_si&#281;"
  ]
  node [
    id 1225
    label "powstrzymanie"
  ]
  node [
    id 1226
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1227
    label "convention"
  ]
  node [
    id 1228
    label "po&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1229
    label "bezruch"
  ]
  node [
    id 1230
    label "oznaka"
  ]
  node [
    id 1231
    label "znieruchomienie"
  ]
  node [
    id 1232
    label "zdziwienie"
  ]
  node [
    id 1233
    label "discouragement"
  ]
  node [
    id 1234
    label "temper"
  ]
  node [
    id 1235
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 1236
    label "stan"
  ]
  node [
    id 1237
    label "samopoczucie"
  ]
  node [
    id 1238
    label "mechanizm_obronny"
  ]
  node [
    id 1239
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1240
    label "fondness"
  ]
  node [
    id 1241
    label "nastr&#243;j"
  ]
  node [
    id 1242
    label "state"
  ]
  node [
    id 1243
    label "wstyd"
  ]
  node [
    id 1244
    label "upokorzenie"
  ]
  node [
    id 1245
    label "klimat"
  ]
  node [
    id 1246
    label "kwas"
  ]
  node [
    id 1247
    label "Ohio"
  ]
  node [
    id 1248
    label "wci&#281;cie"
  ]
  node [
    id 1249
    label "Nowy_York"
  ]
  node [
    id 1250
    label "warstwa"
  ]
  node [
    id 1251
    label "Illinois"
  ]
  node [
    id 1252
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1253
    label "Jukatan"
  ]
  node [
    id 1254
    label "Kalifornia"
  ]
  node [
    id 1255
    label "Wirginia"
  ]
  node [
    id 1256
    label "wektor"
  ]
  node [
    id 1257
    label "Goa"
  ]
  node [
    id 1258
    label "Teksas"
  ]
  node [
    id 1259
    label "Waszyngton"
  ]
  node [
    id 1260
    label "Massachusetts"
  ]
  node [
    id 1261
    label "Alaska"
  ]
  node [
    id 1262
    label "Arakan"
  ]
  node [
    id 1263
    label "Hawaje"
  ]
  node [
    id 1264
    label "Maryland"
  ]
  node [
    id 1265
    label "Michigan"
  ]
  node [
    id 1266
    label "Arizona"
  ]
  node [
    id 1267
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1268
    label "Georgia"
  ]
  node [
    id 1269
    label "poziom"
  ]
  node [
    id 1270
    label "Pensylwania"
  ]
  node [
    id 1271
    label "Luizjana"
  ]
  node [
    id 1272
    label "Nowy_Meksyk"
  ]
  node [
    id 1273
    label "Alabama"
  ]
  node [
    id 1274
    label "ilo&#347;&#263;"
  ]
  node [
    id 1275
    label "Kansas"
  ]
  node [
    id 1276
    label "Oregon"
  ]
  node [
    id 1277
    label "Oklahoma"
  ]
  node [
    id 1278
    label "Floryda"
  ]
  node [
    id 1279
    label "jednostka_administracyjna"
  ]
  node [
    id 1280
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1281
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1282
    label "dyspozycja"
  ]
  node [
    id 1283
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 1284
    label "krzew"
  ]
  node [
    id 1285
    label "delfinidyna"
  ]
  node [
    id 1286
    label "pi&#380;maczkowate"
  ]
  node [
    id 1287
    label "ki&#347;&#263;"
  ]
  node [
    id 1288
    label "hy&#263;ka"
  ]
  node [
    id 1289
    label "pestkowiec"
  ]
  node [
    id 1290
    label "kwiat"
  ]
  node [
    id 1291
    label "owoc"
  ]
  node [
    id 1292
    label "oliwkowate"
  ]
  node [
    id 1293
    label "lilac"
  ]
  node [
    id 1294
    label "kostka"
  ]
  node [
    id 1295
    label "kita"
  ]
  node [
    id 1296
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 1297
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 1298
    label "d&#322;o&#324;"
  ]
  node [
    id 1299
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 1300
    label "powerball"
  ]
  node [
    id 1301
    label "&#380;ubr"
  ]
  node [
    id 1302
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 1303
    label "p&#281;k"
  ]
  node [
    id 1304
    label "r&#281;ka"
  ]
  node [
    id 1305
    label "ogon"
  ]
  node [
    id 1306
    label "zako&#324;czenie"
  ]
  node [
    id 1307
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 1308
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 1309
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 1310
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 1311
    label "flakon"
  ]
  node [
    id 1312
    label "przykoronek"
  ]
  node [
    id 1313
    label "kielich"
  ]
  node [
    id 1314
    label "dno_kwiatowe"
  ]
  node [
    id 1315
    label "organ_ro&#347;linny"
  ]
  node [
    id 1316
    label "warga"
  ]
  node [
    id 1317
    label "korona"
  ]
  node [
    id 1318
    label "rurka"
  ]
  node [
    id 1319
    label "ozdoba"
  ]
  node [
    id 1320
    label "&#322;yko"
  ]
  node [
    id 1321
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 1322
    label "karczowa&#263;"
  ]
  node [
    id 1323
    label "wykarczowanie"
  ]
  node [
    id 1324
    label "skupina"
  ]
  node [
    id 1325
    label "wykarczowa&#263;"
  ]
  node [
    id 1326
    label "karczowanie"
  ]
  node [
    id 1327
    label "fanerofit"
  ]
  node [
    id 1328
    label "zbiorowisko"
  ]
  node [
    id 1329
    label "ro&#347;liny"
  ]
  node [
    id 1330
    label "p&#281;d"
  ]
  node [
    id 1331
    label "wegetowanie"
  ]
  node [
    id 1332
    label "zadziorek"
  ]
  node [
    id 1333
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1334
    label "do&#322;owa&#263;"
  ]
  node [
    id 1335
    label "wegetacja"
  ]
  node [
    id 1336
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1337
    label "strzyc"
  ]
  node [
    id 1338
    label "w&#322;&#243;kno"
  ]
  node [
    id 1339
    label "g&#322;uszenie"
  ]
  node [
    id 1340
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1341
    label "fitotron"
  ]
  node [
    id 1342
    label "bulwka"
  ]
  node [
    id 1343
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1344
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1345
    label "epiderma"
  ]
  node [
    id 1346
    label "gumoza"
  ]
  node [
    id 1347
    label "strzy&#380;enie"
  ]
  node [
    id 1348
    label "wypotnik"
  ]
  node [
    id 1349
    label "flawonoid"
  ]
  node [
    id 1350
    label "wyro&#347;le"
  ]
  node [
    id 1351
    label "do&#322;owanie"
  ]
  node [
    id 1352
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1353
    label "pora&#380;a&#263;"
  ]
  node [
    id 1354
    label "fitocenoza"
  ]
  node [
    id 1355
    label "fotoautotrof"
  ]
  node [
    id 1356
    label "nieuleczalnie_chory"
  ]
  node [
    id 1357
    label "wegetowa&#263;"
  ]
  node [
    id 1358
    label "pochewka"
  ]
  node [
    id 1359
    label "sok"
  ]
  node [
    id 1360
    label "system_korzeniowy"
  ]
  node [
    id 1361
    label "zawi&#261;zek"
  ]
  node [
    id 1362
    label "pestka"
  ]
  node [
    id 1363
    label "mi&#261;&#380;sz"
  ]
  node [
    id 1364
    label "frukt"
  ]
  node [
    id 1365
    label "drylowanie"
  ]
  node [
    id 1366
    label "produkt"
  ]
  node [
    id 1367
    label "owocnia"
  ]
  node [
    id 1368
    label "fruktoza"
  ]
  node [
    id 1369
    label "gniazdo_nasienne"
  ]
  node [
    id 1370
    label "glukoza"
  ]
  node [
    id 1371
    label "antocyjanidyn"
  ]
  node [
    id 1372
    label "szczeciowce"
  ]
  node [
    id 1373
    label "jasnotowce"
  ]
  node [
    id 1374
    label "Oleaceae"
  ]
  node [
    id 1375
    label "wielkopolski"
  ]
  node [
    id 1376
    label "bez_czarny"
  ]
  node [
    id 1377
    label "nadmiarowy"
  ]
  node [
    id 1378
    label "zb&#281;dnie"
  ]
  node [
    id 1379
    label "odej&#347;cie"
  ]
  node [
    id 1380
    label "odchodzenie"
  ]
  node [
    id 1381
    label "superfluously"
  ]
  node [
    id 1382
    label "uselessly"
  ]
  node [
    id 1383
    label "nadmiarowo"
  ]
  node [
    id 1384
    label "korkowanie"
  ]
  node [
    id 1385
    label "death"
  ]
  node [
    id 1386
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 1387
    label "przestawanie"
  ]
  node [
    id 1388
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 1389
    label "zdychanie"
  ]
  node [
    id 1390
    label "spisywanie_"
  ]
  node [
    id 1391
    label "usuwanie"
  ]
  node [
    id 1392
    label "tracenie"
  ]
  node [
    id 1393
    label "ko&#324;czenie"
  ]
  node [
    id 1394
    label "odrzut"
  ]
  node [
    id 1395
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1396
    label "opuszczanie"
  ]
  node [
    id 1397
    label "wydalanie"
  ]
  node [
    id 1398
    label "odrzucanie"
  ]
  node [
    id 1399
    label "odstawianie"
  ]
  node [
    id 1400
    label "martwy"
  ]
  node [
    id 1401
    label "ust&#281;powanie"
  ]
  node [
    id 1402
    label "egress"
  ]
  node [
    id 1403
    label "zrzekanie_si&#281;"
  ]
  node [
    id 1404
    label "dzianie_si&#281;"
  ]
  node [
    id 1405
    label "oddzielanie_si&#281;"
  ]
  node [
    id 1406
    label "wyruszanie"
  ]
  node [
    id 1407
    label "odumieranie"
  ]
  node [
    id 1408
    label "odstawanie"
  ]
  node [
    id 1409
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1410
    label "mijanie"
  ]
  node [
    id 1411
    label "wracanie"
  ]
  node [
    id 1412
    label "oddalanie_si&#281;"
  ]
  node [
    id 1413
    label "kursowanie"
  ]
  node [
    id 1414
    label "mini&#281;cie"
  ]
  node [
    id 1415
    label "odumarcie"
  ]
  node [
    id 1416
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1417
    label "ruszenie"
  ]
  node [
    id 1418
    label "ust&#261;pienie"
  ]
  node [
    id 1419
    label "mogi&#322;a"
  ]
  node [
    id 1420
    label "pomarcie"
  ]
  node [
    id 1421
    label "opuszczenie"
  ]
  node [
    id 1422
    label "spisanie_"
  ]
  node [
    id 1423
    label "oddalenie_si&#281;"
  ]
  node [
    id 1424
    label "defenestracja"
  ]
  node [
    id 1425
    label "danie_sobie_spokoju"
  ]
  node [
    id 1426
    label "kres_&#380;ycia"
  ]
  node [
    id 1427
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1428
    label "zdechni&#281;cie"
  ]
  node [
    id 1429
    label "exit"
  ]
  node [
    id 1430
    label "stracenie"
  ]
  node [
    id 1431
    label "przestanie"
  ]
  node [
    id 1432
    label "wr&#243;cenie"
  ]
  node [
    id 1433
    label "szeol"
  ]
  node [
    id 1434
    label "die"
  ]
  node [
    id 1435
    label "oddzielenie_si&#281;"
  ]
  node [
    id 1436
    label "deviation"
  ]
  node [
    id 1437
    label "wydalenie"
  ]
  node [
    id 1438
    label "pogrzebanie"
  ]
  node [
    id 1439
    label "&#380;a&#322;oba"
  ]
  node [
    id 1440
    label "sko&#324;czenie"
  ]
  node [
    id 1441
    label "withdrawal"
  ]
  node [
    id 1442
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 1443
    label "zabicie"
  ]
  node [
    id 1444
    label "agonia"
  ]
  node [
    id 1445
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 1446
    label "kres"
  ]
  node [
    id 1447
    label "usuni&#281;cie"
  ]
  node [
    id 1448
    label "relinquishment"
  ]
  node [
    id 1449
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1450
    label "poniechanie"
  ]
  node [
    id 1451
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1452
    label "wypisanie_si&#281;"
  ]
  node [
    id 1453
    label "pompatyczno&#347;&#263;"
  ]
  node [
    id 1454
    label "pathos"
  ]
  node [
    id 1455
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 1456
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 1457
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 1458
    label "deklamowa&#263;"
  ]
  node [
    id 1459
    label "deklamowanie"
  ]
  node [
    id 1460
    label "m&#322;ot"
  ]
  node [
    id 1461
    label "znak"
  ]
  node [
    id 1462
    label "drzewo"
  ]
  node [
    id 1463
    label "pr&#243;ba"
  ]
  node [
    id 1464
    label "attribute"
  ]
  node [
    id 1465
    label "marka"
  ]
  node [
    id 1466
    label "trzonek"
  ]
  node [
    id 1467
    label "narz&#281;dzie"
  ]
  node [
    id 1468
    label "stylik"
  ]
  node [
    id 1469
    label "dyscyplina_sportowa"
  ]
  node [
    id 1470
    label "handle"
  ]
  node [
    id 1471
    label "stroke"
  ]
  node [
    id 1472
    label "line"
  ]
  node [
    id 1473
    label "napisa&#263;"
  ]
  node [
    id 1474
    label "natural_language"
  ]
  node [
    id 1475
    label "pisa&#263;"
  ]
  node [
    id 1476
    label "behawior"
  ]
  node [
    id 1477
    label "artificiality"
  ]
  node [
    id 1478
    label "fa&#322;szywo&#347;&#263;"
  ]
  node [
    id 1479
    label "grandilokwencja"
  ]
  node [
    id 1480
    label "pretensjonalno&#347;&#263;"
  ]
  node [
    id 1481
    label "wypowiadanie"
  ]
  node [
    id 1482
    label "repetition"
  ]
  node [
    id 1483
    label "declamation"
  ]
  node [
    id 1484
    label "m&#243;wienie"
  ]
  node [
    id 1485
    label "tell"
  ]
  node [
    id 1486
    label "czasowo"
  ]
  node [
    id 1487
    label "czasowy"
  ]
  node [
    id 1488
    label "temporarily"
  ]
  node [
    id 1489
    label "kiedy&#347;"
  ]
  node [
    id 1490
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 1491
    label "testify"
  ]
  node [
    id 1492
    label "pokaza&#263;"
  ]
  node [
    id 1493
    label "poda&#263;"
  ]
  node [
    id 1494
    label "poinformowa&#263;"
  ]
  node [
    id 1495
    label "udowodni&#263;"
  ]
  node [
    id 1496
    label "spowodowa&#263;"
  ]
  node [
    id 1497
    label "wyrazi&#263;"
  ]
  node [
    id 1498
    label "przeszkoli&#263;"
  ]
  node [
    id 1499
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 1500
    label "indicate"
  ]
  node [
    id 1501
    label "czyj&#347;"
  ]
  node [
    id 1502
    label "m&#261;&#380;"
  ]
  node [
    id 1503
    label "prywatny"
  ]
  node [
    id 1504
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1505
    label "ch&#322;op"
  ]
  node [
    id 1506
    label "pan_m&#322;ody"
  ]
  node [
    id 1507
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1508
    label "&#347;lubny"
  ]
  node [
    id 1509
    label "pan_domu"
  ]
  node [
    id 1510
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1511
    label "stary"
  ]
  node [
    id 1512
    label "godzina"
  ]
  node [
    id 1513
    label "time"
  ]
  node [
    id 1514
    label "doba"
  ]
  node [
    id 1515
    label "p&#243;&#322;godzina"
  ]
  node [
    id 1516
    label "jednostka_czasu"
  ]
  node [
    id 1517
    label "minuta"
  ]
  node [
    id 1518
    label "kwadrans"
  ]
  node [
    id 1519
    label "comeliness"
  ]
  node [
    id 1520
    label "face"
  ]
  node [
    id 1521
    label "twarz"
  ]
  node [
    id 1522
    label "facjata"
  ]
  node [
    id 1523
    label "postarzenie"
  ]
  node [
    id 1524
    label "postarzanie"
  ]
  node [
    id 1525
    label "brzydota"
  ]
  node [
    id 1526
    label "postarza&#263;"
  ]
  node [
    id 1527
    label "nadawanie"
  ]
  node [
    id 1528
    label "postarzy&#263;"
  ]
  node [
    id 1529
    label "widok"
  ]
  node [
    id 1530
    label "prostota"
  ]
  node [
    id 1531
    label "ubarwienie"
  ]
  node [
    id 1532
    label "cera"
  ]
  node [
    id 1533
    label "wielko&#347;&#263;"
  ]
  node [
    id 1534
    label "rys"
  ]
  node [
    id 1535
    label "profil"
  ]
  node [
    id 1536
    label "p&#322;e&#263;"
  ]
  node [
    id 1537
    label "zas&#322;ona"
  ]
  node [
    id 1538
    label "p&#243;&#322;profil"
  ]
  node [
    id 1539
    label "policzek"
  ]
  node [
    id 1540
    label "brew"
  ]
  node [
    id 1541
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1542
    label "uj&#281;cie"
  ]
  node [
    id 1543
    label "micha"
  ]
  node [
    id 1544
    label "reputacja"
  ]
  node [
    id 1545
    label "wyraz_twarzy"
  ]
  node [
    id 1546
    label "powieka"
  ]
  node [
    id 1547
    label "czo&#322;o"
  ]
  node [
    id 1548
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 1549
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 1550
    label "twarzyczka"
  ]
  node [
    id 1551
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 1552
    label "usta"
  ]
  node [
    id 1553
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 1554
    label "dzi&#243;b"
  ]
  node [
    id 1555
    label "oko"
  ]
  node [
    id 1556
    label "nos"
  ]
  node [
    id 1557
    label "podbr&#243;dek"
  ]
  node [
    id 1558
    label "liczko"
  ]
  node [
    id 1559
    label "pysk"
  ]
  node [
    id 1560
    label "maskowato&#347;&#263;"
  ]
  node [
    id 1561
    label "strych"
  ]
  node [
    id 1562
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1563
    label "mie&#263;_miejsce"
  ]
  node [
    id 1564
    label "equal"
  ]
  node [
    id 1565
    label "trwa&#263;"
  ]
  node [
    id 1566
    label "chodzi&#263;"
  ]
  node [
    id 1567
    label "si&#281;ga&#263;"
  ]
  node [
    id 1568
    label "obecno&#347;&#263;"
  ]
  node [
    id 1569
    label "stand"
  ]
  node [
    id 1570
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1571
    label "uczestniczy&#263;"
  ]
  node [
    id 1572
    label "participate"
  ]
  node [
    id 1573
    label "istnie&#263;"
  ]
  node [
    id 1574
    label "pozostawa&#263;"
  ]
  node [
    id 1575
    label "zostawa&#263;"
  ]
  node [
    id 1576
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1577
    label "adhere"
  ]
  node [
    id 1578
    label "compass"
  ]
  node [
    id 1579
    label "korzysta&#263;"
  ]
  node [
    id 1580
    label "appreciation"
  ]
  node [
    id 1581
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1582
    label "dociera&#263;"
  ]
  node [
    id 1583
    label "get"
  ]
  node [
    id 1584
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1585
    label "mierzy&#263;"
  ]
  node [
    id 1586
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1587
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1588
    label "exsert"
  ]
  node [
    id 1589
    label "being"
  ]
  node [
    id 1590
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1591
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1592
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1593
    label "run"
  ]
  node [
    id 1594
    label "bangla&#263;"
  ]
  node [
    id 1595
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1596
    label "przebiega&#263;"
  ]
  node [
    id 1597
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1598
    label "proceed"
  ]
  node [
    id 1599
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1600
    label "carry"
  ]
  node [
    id 1601
    label "bywa&#263;"
  ]
  node [
    id 1602
    label "dziama&#263;"
  ]
  node [
    id 1603
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1604
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1605
    label "para"
  ]
  node [
    id 1606
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1607
    label "str&#243;j"
  ]
  node [
    id 1608
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1609
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1610
    label "krok"
  ]
  node [
    id 1611
    label "tryb"
  ]
  node [
    id 1612
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1613
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1614
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1615
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1616
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1617
    label "continue"
  ]
  node [
    id 1618
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1619
    label "daleki"
  ]
  node [
    id 1620
    label "d&#322;ugo"
  ]
  node [
    id 1621
    label "mechanika"
  ]
  node [
    id 1622
    label "utrzymywanie"
  ]
  node [
    id 1623
    label "move"
  ]
  node [
    id 1624
    label "poruszenie"
  ]
  node [
    id 1625
    label "myk"
  ]
  node [
    id 1626
    label "utrzyma&#263;"
  ]
  node [
    id 1627
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1628
    label "utrzymanie"
  ]
  node [
    id 1629
    label "travel"
  ]
  node [
    id 1630
    label "kanciasty"
  ]
  node [
    id 1631
    label "commercial_enterprise"
  ]
  node [
    id 1632
    label "strumie&#324;"
  ]
  node [
    id 1633
    label "proces"
  ]
  node [
    id 1634
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1635
    label "kr&#243;tki"
  ]
  node [
    id 1636
    label "taktyka"
  ]
  node [
    id 1637
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1638
    label "apraksja"
  ]
  node [
    id 1639
    label "utrzymywa&#263;"
  ]
  node [
    id 1640
    label "dyssypacja_energii"
  ]
  node [
    id 1641
    label "tumult"
  ]
  node [
    id 1642
    label "zmiana"
  ]
  node [
    id 1643
    label "manewr"
  ]
  node [
    id 1644
    label "lokomocja"
  ]
  node [
    id 1645
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1646
    label "komunikacja"
  ]
  node [
    id 1647
    label "drift"
  ]
  node [
    id 1648
    label "dawny"
  ]
  node [
    id 1649
    label "ogl&#281;dny"
  ]
  node [
    id 1650
    label "du&#380;y"
  ]
  node [
    id 1651
    label "daleko"
  ]
  node [
    id 1652
    label "odleg&#322;y"
  ]
  node [
    id 1653
    label "zwi&#261;zany"
  ]
  node [
    id 1654
    label "r&#243;&#380;ny"
  ]
  node [
    id 1655
    label "s&#322;aby"
  ]
  node [
    id 1656
    label "odlegle"
  ]
  node [
    id 1657
    label "oddalony"
  ]
  node [
    id 1658
    label "g&#322;&#281;boki"
  ]
  node [
    id 1659
    label "obcy"
  ]
  node [
    id 1660
    label "nieobecny"
  ]
  node [
    id 1661
    label "przysz&#322;y"
  ]
  node [
    id 1662
    label "nietypowy"
  ]
  node [
    id 1663
    label "niepodobnie"
  ]
  node [
    id 1664
    label "inny"
  ]
  node [
    id 1665
    label "jaki&#347;"
  ]
  node [
    id 1666
    label "r&#243;&#380;nie"
  ]
  node [
    id 1667
    label "nietypowo"
  ]
  node [
    id 1668
    label "piwo"
  ]
  node [
    id 1669
    label "uwarzenie"
  ]
  node [
    id 1670
    label "warzenie"
  ]
  node [
    id 1671
    label "nap&#243;j"
  ]
  node [
    id 1672
    label "bacik"
  ]
  node [
    id 1673
    label "wyj&#347;cie"
  ]
  node [
    id 1674
    label "uwarzy&#263;"
  ]
  node [
    id 1675
    label "birofilia"
  ]
  node [
    id 1676
    label "warzy&#263;"
  ]
  node [
    id 1677
    label "nawarzy&#263;"
  ]
  node [
    id 1678
    label "browarnia"
  ]
  node [
    id 1679
    label "nawarzenie"
  ]
  node [
    id 1680
    label "anta&#322;"
  ]
  node [
    id 1681
    label "pieski"
  ]
  node [
    id 1682
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 1683
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 1684
    label "niekorzystny"
  ]
  node [
    id 1685
    label "z&#322;oszczenie"
  ]
  node [
    id 1686
    label "sierdzisty"
  ]
  node [
    id 1687
    label "niegrzeczny"
  ]
  node [
    id 1688
    label "zez&#322;oszczenie"
  ]
  node [
    id 1689
    label "zdenerwowany"
  ]
  node [
    id 1690
    label "negatywny"
  ]
  node [
    id 1691
    label "rozgniewanie"
  ]
  node [
    id 1692
    label "gniewanie"
  ]
  node [
    id 1693
    label "niemoralny"
  ]
  node [
    id 1694
    label "&#378;le"
  ]
  node [
    id 1695
    label "niepomy&#347;lny"
  ]
  node [
    id 1696
    label "syf"
  ]
  node [
    id 1697
    label "niespokojny"
  ]
  node [
    id 1698
    label "niekorzystnie"
  ]
  node [
    id 1699
    label "ujemny"
  ]
  node [
    id 1700
    label "nagannie"
  ]
  node [
    id 1701
    label "niemoralnie"
  ]
  node [
    id 1702
    label "nieprzyzwoity"
  ]
  node [
    id 1703
    label "niepomy&#347;lnie"
  ]
  node [
    id 1704
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 1705
    label "nieodpowiednio"
  ]
  node [
    id 1706
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 1707
    label "swoisty"
  ]
  node [
    id 1708
    label "nienale&#380;yty"
  ]
  node [
    id 1709
    label "dziwny"
  ]
  node [
    id 1710
    label "niezno&#347;ny"
  ]
  node [
    id 1711
    label "niegrzecznie"
  ]
  node [
    id 1712
    label "trudny"
  ]
  node [
    id 1713
    label "niestosowny"
  ]
  node [
    id 1714
    label "brzydal"
  ]
  node [
    id 1715
    label "niepos&#322;uszny"
  ]
  node [
    id 1716
    label "negatywnie"
  ]
  node [
    id 1717
    label "nieprzyjemny"
  ]
  node [
    id 1718
    label "ujemnie"
  ]
  node [
    id 1719
    label "gniewny"
  ]
  node [
    id 1720
    label "serdeczny"
  ]
  node [
    id 1721
    label "srogi"
  ]
  node [
    id 1722
    label "sierdzi&#347;cie"
  ]
  node [
    id 1723
    label "piesko"
  ]
  node [
    id 1724
    label "rozgniewanie_si&#281;"
  ]
  node [
    id 1725
    label "zagniewanie"
  ]
  node [
    id 1726
    label "wzbudzenie"
  ]
  node [
    id 1727
    label "wzbudzanie"
  ]
  node [
    id 1728
    label "z&#322;oszczenie_si&#281;"
  ]
  node [
    id 1729
    label "gniewanie_si&#281;"
  ]
  node [
    id 1730
    label "niezgodnie"
  ]
  node [
    id 1731
    label "gorzej"
  ]
  node [
    id 1732
    label "jako&#347;&#263;"
  ]
  node [
    id 1733
    label "syphilis"
  ]
  node [
    id 1734
    label "tragedia"
  ]
  node [
    id 1735
    label "nieporz&#261;dek"
  ]
  node [
    id 1736
    label "kr&#281;tek_blady"
  ]
  node [
    id 1737
    label "krosta"
  ]
  node [
    id 1738
    label "choroba_dworska"
  ]
  node [
    id 1739
    label "choroba_bakteryjna"
  ]
  node [
    id 1740
    label "zabrudzenie"
  ]
  node [
    id 1741
    label "substancja"
  ]
  node [
    id 1742
    label "choroba_weneryczna"
  ]
  node [
    id 1743
    label "ki&#322;a_wrodzona"
  ]
  node [
    id 1744
    label "szankier_twardy"
  ]
  node [
    id 1745
    label "spot"
  ]
  node [
    id 1746
    label "zanieczyszczenie"
  ]
  node [
    id 1747
    label "pauza"
  ]
  node [
    id 1748
    label "przedzia&#322;"
  ]
  node [
    id 1749
    label "warunek_lokalowy"
  ]
  node [
    id 1750
    label "plac"
  ]
  node [
    id 1751
    label "location"
  ]
  node [
    id 1752
    label "uwaga"
  ]
  node [
    id 1753
    label "przestrze&#324;"
  ]
  node [
    id 1754
    label "status"
  ]
  node [
    id 1755
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1756
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1757
    label "rz&#261;d"
  ]
  node [
    id 1758
    label "przegroda"
  ]
  node [
    id 1759
    label "miejsce_le&#380;&#261;ce"
  ]
  node [
    id 1760
    label "podzia&#322;"
  ]
  node [
    id 1761
    label "pomieszczenie"
  ]
  node [
    id 1762
    label "skala"
  ]
  node [
    id 1763
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 1764
    label "farewell"
  ]
  node [
    id 1765
    label "hyphen"
  ]
  node [
    id 1766
    label "znak_muzyczny"
  ]
  node [
    id 1767
    label "znak_graficzny"
  ]
  node [
    id 1768
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 1769
    label "strike"
  ]
  node [
    id 1770
    label "hopka&#263;"
  ]
  node [
    id 1771
    label "woo"
  ]
  node [
    id 1772
    label "take"
  ]
  node [
    id 1773
    label "napierdziela&#263;"
  ]
  node [
    id 1774
    label "wstrzeliwa&#263;_si&#281;"
  ]
  node [
    id 1775
    label "ofensywny"
  ]
  node [
    id 1776
    label "funkcjonowa&#263;"
  ]
  node [
    id 1777
    label "sztacha&#263;"
  ]
  node [
    id 1778
    label "go"
  ]
  node [
    id 1779
    label "rwa&#263;"
  ]
  node [
    id 1780
    label "zadawa&#263;"
  ]
  node [
    id 1781
    label "konkurowa&#263;"
  ]
  node [
    id 1782
    label "blend"
  ]
  node [
    id 1783
    label "uderza&#263;_do_panny"
  ]
  node [
    id 1784
    label "startowa&#263;"
  ]
  node [
    id 1785
    label "rani&#263;"
  ]
  node [
    id 1786
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1787
    label "krytykowa&#263;"
  ]
  node [
    id 1788
    label "walczy&#263;"
  ]
  node [
    id 1789
    label "break"
  ]
  node [
    id 1790
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 1791
    label "przypieprza&#263;"
  ]
  node [
    id 1792
    label "napada&#263;"
  ]
  node [
    id 1793
    label "chop"
  ]
  node [
    id 1794
    label "nast&#281;powa&#263;"
  ]
  node [
    id 1795
    label "dotyka&#263;"
  ]
  node [
    id 1796
    label "injury"
  ]
  node [
    id 1797
    label "uci&#261;&#263;"
  ]
  node [
    id 1798
    label "zrani&#263;"
  ]
  node [
    id 1799
    label "transgress"
  ]
  node [
    id 1800
    label "uszkodzi&#263;"
  ]
  node [
    id 1801
    label "wzbudza&#263;"
  ]
  node [
    id 1802
    label "kaleczy&#263;"
  ]
  node [
    id 1803
    label "wyszczerbi&#263;"
  ]
  node [
    id 1804
    label "szczerbi&#263;"
  ]
  node [
    id 1805
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1806
    label "motywowa&#263;"
  ]
  node [
    id 1807
    label "os&#261;dza&#263;"
  ]
  node [
    id 1808
    label "opiniowa&#263;"
  ]
  node [
    id 1809
    label "treat"
  ]
  node [
    id 1810
    label "podnosi&#263;"
  ]
  node [
    id 1811
    label "spotyka&#263;"
  ]
  node [
    id 1812
    label "s&#261;siadowa&#263;"
  ]
  node [
    id 1813
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1814
    label "fit"
  ]
  node [
    id 1815
    label "rusza&#263;"
  ]
  node [
    id 1816
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 1817
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1818
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 1819
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 1820
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1821
    label "fight"
  ]
  node [
    id 1822
    label "wrestle"
  ]
  node [
    id 1823
    label "zawody"
  ]
  node [
    id 1824
    label "contend"
  ]
  node [
    id 1825
    label "argue"
  ]
  node [
    id 1826
    label "my&#347;lenie"
  ]
  node [
    id 1827
    label "deal"
  ]
  node [
    id 1828
    label "zajmowa&#263;"
  ]
  node [
    id 1829
    label "karmi&#263;"
  ]
  node [
    id 1830
    label "szkodzi&#263;"
  ]
  node [
    id 1831
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 1832
    label "inflict"
  ]
  node [
    id 1833
    label "share"
  ]
  node [
    id 1834
    label "pose"
  ]
  node [
    id 1835
    label "zak&#322;ada&#263;"
  ]
  node [
    id 1836
    label "winnings"
  ]
  node [
    id 1837
    label "goban"
  ]
  node [
    id 1838
    label "gra_planszowa"
  ]
  node [
    id 1839
    label "sport_umys&#322;owy"
  ]
  node [
    id 1840
    label "chi&#324;ski"
  ]
  node [
    id 1841
    label "hiphopowiec"
  ]
  node [
    id 1842
    label "skejt"
  ]
  node [
    id 1843
    label "taniec"
  ]
  node [
    id 1844
    label "krzycze&#263;"
  ]
  node [
    id 1845
    label "skaka&#263;"
  ]
  node [
    id 1846
    label "chyba&#263;"
  ]
  node [
    id 1847
    label "rise"
  ]
  node [
    id 1848
    label "bole&#263;"
  ]
  node [
    id 1849
    label "naciska&#263;"
  ]
  node [
    id 1850
    label "strzela&#263;"
  ]
  node [
    id 1851
    label "popyla&#263;"
  ]
  node [
    id 1852
    label "gada&#263;"
  ]
  node [
    id 1853
    label "harowa&#263;"
  ]
  node [
    id 1854
    label "bi&#263;"
  ]
  node [
    id 1855
    label "gra&#263;"
  ]
  node [
    id 1856
    label "psu&#263;_si&#281;"
  ]
  node [
    id 1857
    label "atakowa&#263;"
  ]
  node [
    id 1858
    label "alternate"
  ]
  node [
    id 1859
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 1860
    label "chance"
  ]
  node [
    id 1861
    label "wyrywa&#263;"
  ]
  node [
    id 1862
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 1863
    label "amuse"
  ]
  node [
    id 1864
    label "niszczy&#263;"
  ]
  node [
    id 1865
    label "zbiera&#263;"
  ]
  node [
    id 1866
    label "strive"
  ]
  node [
    id 1867
    label "p&#281;dzi&#263;"
  ]
  node [
    id 1868
    label "dzieli&#263;"
  ]
  node [
    id 1869
    label "zaczyna&#263;"
  ]
  node [
    id 1870
    label "katapultowa&#263;"
  ]
  node [
    id 1871
    label "odchodzi&#263;"
  ]
  node [
    id 1872
    label "samolot"
  ]
  node [
    id 1873
    label "begin"
  ]
  node [
    id 1874
    label "intensywny"
  ]
  node [
    id 1875
    label "niebezpieczny"
  ]
  node [
    id 1876
    label "odwa&#380;ny"
  ]
  node [
    id 1877
    label "silny"
  ]
  node [
    id 1878
    label "nieneutralny"
  ]
  node [
    id 1879
    label "czynny"
  ]
  node [
    id 1880
    label "ofensywnie"
  ]
  node [
    id 1881
    label "agresywny"
  ]
  node [
    id 1882
    label "wrogi"
  ]
  node [
    id 1883
    label "opresyjny"
  ]
  node [
    id 1884
    label "attack"
  ]
  node [
    id 1885
    label "piratowa&#263;"
  ]
  node [
    id 1886
    label "dopada&#263;"
  ]
  node [
    id 1887
    label "we&#322;niany"
  ]
  node [
    id 1888
    label "w&#322;&#243;czkowy"
  ]
  node [
    id 1889
    label "tkaninowy"
  ]
  node [
    id 1890
    label "naturalny"
  ]
  node [
    id 1891
    label "d&#380;ellaba"
  ]
  node [
    id 1892
    label "wieloton"
  ]
  node [
    id 1893
    label "tu&#324;czyk"
  ]
  node [
    id 1894
    label "zabarwienie"
  ]
  node [
    id 1895
    label "interwa&#322;"
  ]
  node [
    id 1896
    label "modalizm"
  ]
  node [
    id 1897
    label "note"
  ]
  node [
    id 1898
    label "formality"
  ]
  node [
    id 1899
    label "glinka"
  ]
  node [
    id 1900
    label "jednostka"
  ]
  node [
    id 1901
    label "sound"
  ]
  node [
    id 1902
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1903
    label "zwyczaj"
  ]
  node [
    id 1904
    label "neoproterozoik"
  ]
  node [
    id 1905
    label "solmizacja"
  ]
  node [
    id 1906
    label "tone"
  ]
  node [
    id 1907
    label "kolorystyka"
  ]
  node [
    id 1908
    label "r&#243;&#380;nica"
  ]
  node [
    id 1909
    label "akcent"
  ]
  node [
    id 1910
    label "repetycja"
  ]
  node [
    id 1911
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1912
    label "heksachord"
  ]
  node [
    id 1913
    label "rejestr"
  ]
  node [
    id 1914
    label "phone"
  ]
  node [
    id 1915
    label "wpadni&#281;cie"
  ]
  node [
    id 1916
    label "wydawa&#263;"
  ]
  node [
    id 1917
    label "wyda&#263;"
  ]
  node [
    id 1918
    label "intonacja"
  ]
  node [
    id 1919
    label "wpa&#347;&#263;"
  ]
  node [
    id 1920
    label "onomatopeja"
  ]
  node [
    id 1921
    label "nadlecenie"
  ]
  node [
    id 1922
    label "wpada&#263;"
  ]
  node [
    id 1923
    label "dobiec"
  ]
  node [
    id 1924
    label "transmiter"
  ]
  node [
    id 1925
    label "brzmienie"
  ]
  node [
    id 1926
    label "wpadanie"
  ]
  node [
    id 1927
    label "podkre&#347;lenie"
  ]
  node [
    id 1928
    label "implozja"
  ]
  node [
    id 1929
    label "znak_diakrytyczny"
  ]
  node [
    id 1930
    label "stress"
  ]
  node [
    id 1931
    label "wymowa"
  ]
  node [
    id 1932
    label "plozja"
  ]
  node [
    id 1933
    label "r&#243;&#380;nokolorowy"
  ]
  node [
    id 1934
    label "barwny"
  ]
  node [
    id 1935
    label "przybranie"
  ]
  node [
    id 1936
    label "color"
  ]
  node [
    id 1937
    label "tu&#324;czyki"
  ]
  node [
    id 1938
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 1939
    label "kultura_duchowa"
  ]
  node [
    id 1940
    label "ceremony"
  ]
  node [
    id 1941
    label "r&#243;&#380;nienie"
  ]
  node [
    id 1942
    label "kontrastowy"
  ]
  node [
    id 1943
    label "discord"
  ]
  node [
    id 1944
    label "wynik"
  ]
  node [
    id 1945
    label "one"
  ]
  node [
    id 1946
    label "supremum"
  ]
  node [
    id 1947
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1948
    label "przeliczy&#263;"
  ]
  node [
    id 1949
    label "matematyka"
  ]
  node [
    id 1950
    label "rzut"
  ]
  node [
    id 1951
    label "liczba_naturalna"
  ]
  node [
    id 1952
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1953
    label "przeliczanie"
  ]
  node [
    id 1954
    label "funkcja"
  ]
  node [
    id 1955
    label "przelicza&#263;"
  ]
  node [
    id 1956
    label "infimum"
  ]
  node [
    id 1957
    label "przeliczenie"
  ]
  node [
    id 1958
    label "clay"
  ]
  node [
    id 1959
    label "ska&#322;a_osadowa"
  ]
  node [
    id 1960
    label "zabarwienie_si&#281;"
  ]
  node [
    id 1961
    label "okraszenie"
  ]
  node [
    id 1962
    label "nacechowanie"
  ]
  node [
    id 1963
    label "hue"
  ]
  node [
    id 1964
    label "nadanie"
  ]
  node [
    id 1965
    label "herezja"
  ]
  node [
    id 1966
    label "monarchianizm"
  ]
  node [
    id 1967
    label "set"
  ]
  node [
    id 1968
    label "przebieg"
  ]
  node [
    id 1969
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 1970
    label "komplet"
  ]
  node [
    id 1971
    label "sekwencja"
  ]
  node [
    id 1972
    label "zestawienie"
  ]
  node [
    id 1973
    label "partia"
  ]
  node [
    id 1974
    label "produkcja"
  ]
  node [
    id 1975
    label "ci&#261;g"
  ]
  node [
    id 1976
    label "instrument_muzyczny"
  ]
  node [
    id 1977
    label "era_eozoiczna"
  ]
  node [
    id 1978
    label "powt&#243;rka"
  ]
  node [
    id 1979
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 1980
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1981
    label "odmawia&#263;"
  ]
  node [
    id 1982
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 1983
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1984
    label "thank"
  ]
  node [
    id 1985
    label "etykieta"
  ]
  node [
    id 1986
    label "catalog"
  ]
  node [
    id 1987
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 1988
    label "przycisk"
  ]
  node [
    id 1989
    label "pozycja"
  ]
  node [
    id 1990
    label "stock"
  ]
  node [
    id 1991
    label "regestr"
  ]
  node [
    id 1992
    label "sumariusz"
  ]
  node [
    id 1993
    label "procesor"
  ]
  node [
    id 1994
    label "figurowa&#263;"
  ]
  node [
    id 1995
    label "book"
  ]
  node [
    id 1996
    label "wyliczanka"
  ]
  node [
    id 1997
    label "organy"
  ]
  node [
    id 1998
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1999
    label "ambitus"
  ]
  node [
    id 2000
    label "abcug"
  ]
  node [
    id 2001
    label "ci&#261;gle"
  ]
  node [
    id 2002
    label "stale"
  ]
  node [
    id 2003
    label "ci&#261;g&#322;y"
  ]
  node [
    id 2004
    label "nieprzerwanie"
  ]
  node [
    id 2005
    label "dysponowanie"
  ]
  node [
    id 2006
    label "sterowanie"
  ]
  node [
    id 2007
    label "management"
  ]
  node [
    id 2008
    label "kierowanie"
  ]
  node [
    id 2009
    label "ukierunkowywanie"
  ]
  node [
    id 2010
    label "przywodzenie"
  ]
  node [
    id 2011
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 2012
    label "doprowadzanie"
  ]
  node [
    id 2013
    label "kre&#347;lenie"
  ]
  node [
    id 2014
    label "lead"
  ]
  node [
    id 2015
    label "krzywa"
  ]
  node [
    id 2016
    label "eksponowanie"
  ]
  node [
    id 2017
    label "linia_melodyczna"
  ]
  node [
    id 2018
    label "prowadzanie"
  ]
  node [
    id 2019
    label "wprowadzanie"
  ]
  node [
    id 2020
    label "doprowadzenie"
  ]
  node [
    id 2021
    label "poprowadzenie"
  ]
  node [
    id 2022
    label "kszta&#322;towanie"
  ]
  node [
    id 2023
    label "aim"
  ]
  node [
    id 2024
    label "zwracanie"
  ]
  node [
    id 2025
    label "przecinanie"
  ]
  node [
    id 2026
    label "ta&#324;czenie"
  ]
  node [
    id 2027
    label "przewy&#380;szanie"
  ]
  node [
    id 2028
    label "g&#243;rowanie"
  ]
  node [
    id 2029
    label "zaprowadzanie"
  ]
  node [
    id 2030
    label "trzymanie"
  ]
  node [
    id 2031
    label "oprowadzanie"
  ]
  node [
    id 2032
    label "wprowadzenie"
  ]
  node [
    id 2033
    label "oprowadzenie"
  ]
  node [
    id 2034
    label "przeci&#281;cie"
  ]
  node [
    id 2035
    label "przeci&#261;ganie"
  ]
  node [
    id 2036
    label "pozarz&#261;dzanie"
  ]
  node [
    id 2037
    label "granie"
  ]
  node [
    id 2038
    label "uniewa&#380;nianie"
  ]
  node [
    id 2039
    label "skre&#347;lanie"
  ]
  node [
    id 2040
    label "pokre&#347;lenie"
  ]
  node [
    id 2041
    label "anointing"
  ]
  node [
    id 2042
    label "sporz&#261;dzanie"
  ]
  node [
    id 2043
    label "opowiadanie"
  ]
  node [
    id 2044
    label "rozdysponowywanie"
  ]
  node [
    id 2045
    label "zarz&#261;dzanie"
  ]
  node [
    id 2046
    label "namaszczenie_chorych"
  ]
  node [
    id 2047
    label "disposal"
  ]
  node [
    id 2048
    label "skazany"
  ]
  node [
    id 2049
    label "rozporz&#261;dzanie"
  ]
  node [
    id 2050
    label "przygotowywanie"
  ]
  node [
    id 2051
    label "radiation"
  ]
  node [
    id 2052
    label "podkre&#347;lanie"
  ]
  node [
    id 2053
    label "uwydatnianie_si&#281;"
  ]
  node [
    id 2054
    label "demonstrowanie"
  ]
  node [
    id 2055
    label "napromieniowywanie"
  ]
  node [
    id 2056
    label "orientation"
  ]
  node [
    id 2057
    label "oznaczanie"
  ]
  node [
    id 2058
    label "dominance"
  ]
  node [
    id 2059
    label "lepszy"
  ]
  node [
    id 2060
    label "wygrywanie"
  ]
  node [
    id 2061
    label "obs&#322;ugiwanie"
  ]
  node [
    id 2062
    label "przesterowanie"
  ]
  node [
    id 2063
    label "steering"
  ]
  node [
    id 2064
    label "haftowanie"
  ]
  node [
    id 2065
    label "vomit"
  ]
  node [
    id 2066
    label "przeznaczanie"
  ]
  node [
    id 2067
    label "przekazywanie"
  ]
  node [
    id 2068
    label "powracanie"
  ]
  node [
    id 2069
    label "rendition"
  ]
  node [
    id 2070
    label "ustawianie"
  ]
  node [
    id 2071
    label "supply"
  ]
  node [
    id 2072
    label "znajdowanie_si&#281;"
  ]
  node [
    id 2073
    label "spe&#322;nianie"
  ]
  node [
    id 2074
    label "provision"
  ]
  node [
    id 2075
    label "montowanie"
  ]
  node [
    id 2076
    label "formation"
  ]
  node [
    id 2077
    label "rozwijanie"
  ]
  node [
    id 2078
    label "training"
  ]
  node [
    id 2079
    label "cause"
  ]
  node [
    id 2080
    label "causal_agent"
  ]
  node [
    id 2081
    label "wysy&#322;anie"
  ]
  node [
    id 2082
    label "wyre&#380;yserowanie"
  ]
  node [
    id 2083
    label "re&#380;yserowanie"
  ]
  node [
    id 2084
    label "nakierowanie_si&#281;"
  ]
  node [
    id 2085
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 2086
    label "figura_geometryczna"
  ]
  node [
    id 2087
    label "linia"
  ]
  node [
    id 2088
    label "poprowadzi&#263;"
  ]
  node [
    id 2089
    label "curvature"
  ]
  node [
    id 2090
    label "curve"
  ]
  node [
    id 2091
    label "sterczenie"
  ]
  node [
    id 2092
    label "&#380;y&#263;"
  ]
  node [
    id 2093
    label "g&#243;rowa&#263;"
  ]
  node [
    id 2094
    label "tworzy&#263;"
  ]
  node [
    id 2095
    label "string"
  ]
  node [
    id 2096
    label "ukierunkowywa&#263;"
  ]
  node [
    id 2097
    label "sterowa&#263;"
  ]
  node [
    id 2098
    label "kre&#347;li&#263;"
  ]
  node [
    id 2099
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 2100
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 2101
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 2102
    label "eksponowa&#263;"
  ]
  node [
    id 2103
    label "navigate"
  ]
  node [
    id 2104
    label "manipulate"
  ]
  node [
    id 2105
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 2106
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 2107
    label "przesuwa&#263;"
  ]
  node [
    id 2108
    label "partner"
  ]
  node [
    id 2109
    label "artyku&#322;"
  ]
  node [
    id 2110
    label "akapit"
  ]
  node [
    id 2111
    label "zesp&#243;&#322;"
  ]
  node [
    id 2112
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 2113
    label "p&#322;acenie"
  ]
  node [
    id 2114
    label "umo&#380;liwianie"
  ]
  node [
    id 2115
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 2116
    label "puszczanie_si&#281;"
  ]
  node [
    id 2117
    label "&#322;adowanie"
  ]
  node [
    id 2118
    label "nalewanie"
  ]
  node [
    id 2119
    label "urz&#261;dzanie"
  ]
  node [
    id 2120
    label "wyst&#281;powanie"
  ]
  node [
    id 2121
    label "dodawanie"
  ]
  node [
    id 2122
    label "giving"
  ]
  node [
    id 2123
    label "pra&#380;enie"
  ]
  node [
    id 2124
    label "emission"
  ]
  node [
    id 2125
    label "dostarczanie"
  ]
  node [
    id 2126
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 2127
    label "obiecywanie"
  ]
  node [
    id 2128
    label "administration"
  ]
  node [
    id 2129
    label "wymienianie_si&#281;"
  ]
  node [
    id 2130
    label "bycie_w_posiadaniu"
  ]
  node [
    id 2131
    label "instrumentalizacja"
  ]
  node [
    id 2132
    label "nagranie_si&#281;"
  ]
  node [
    id 2133
    label "wykonywanie"
  ]
  node [
    id 2134
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 2135
    label "pasowanie"
  ]
  node [
    id 2136
    label "staranie_si&#281;"
  ]
  node [
    id 2137
    label "wybijanie"
  ]
  node [
    id 2138
    label "odegranie_si&#281;"
  ]
  node [
    id 2139
    label "dogrywanie"
  ]
  node [
    id 2140
    label "rozgrywanie"
  ]
  node [
    id 2141
    label "przygrywanie"
  ]
  node [
    id 2142
    label "lewa"
  ]
  node [
    id 2143
    label "uderzenie"
  ]
  node [
    id 2144
    label "zwalczenie"
  ]
  node [
    id 2145
    label "gra_w_karty"
  ]
  node [
    id 2146
    label "mienienie_si&#281;"
  ]
  node [
    id 2147
    label "wydawanie"
  ]
  node [
    id 2148
    label "pretense"
  ]
  node [
    id 2149
    label "prezentowanie"
  ]
  node [
    id 2150
    label "na&#347;ladowanie"
  ]
  node [
    id 2151
    label "dogranie"
  ]
  node [
    id 2152
    label "wybicie"
  ]
  node [
    id 2153
    label "playing"
  ]
  node [
    id 2154
    label "rozegranie_si&#281;"
  ]
  node [
    id 2155
    label "otwarcie"
  ]
  node [
    id 2156
    label "glitter"
  ]
  node [
    id 2157
    label "igranie"
  ]
  node [
    id 2158
    label "odgrywanie_si&#281;"
  ]
  node [
    id 2159
    label "pogranie"
  ]
  node [
    id 2160
    label "wyr&#243;wnywanie"
  ]
  node [
    id 2161
    label "szczekanie"
  ]
  node [
    id 2162
    label "wyr&#243;wnanie"
  ]
  node [
    id 2163
    label "grywanie"
  ]
  node [
    id 2164
    label "migotanie"
  ]
  node [
    id 2165
    label "&#347;ciganie"
  ]
  node [
    id 2166
    label "nakre&#347;lenie"
  ]
  node [
    id 2167
    label "guidance"
  ]
  node [
    id 2168
    label "proverb"
  ]
  node [
    id 2169
    label "wytyczenie"
  ]
  node [
    id 2170
    label "gibanie"
  ]
  node [
    id 2171
    label "przeta&#324;czenie"
  ]
  node [
    id 2172
    label "nata&#324;czenie_si&#281;"
  ]
  node [
    id 2173
    label "rozta&#324;czenie_si&#281;"
  ]
  node [
    id 2174
    label "pota&#324;czenie"
  ]
  node [
    id 2175
    label "poruszanie_si&#281;"
  ]
  node [
    id 2176
    label "chwianie_si&#281;"
  ]
  node [
    id 2177
    label "dancing"
  ]
  node [
    id 2178
    label "rynek"
  ]
  node [
    id 2179
    label "umieszczanie"
  ]
  node [
    id 2180
    label "w&#322;&#261;czanie"
  ]
  node [
    id 2181
    label "zak&#322;&#243;canie"
  ]
  node [
    id 2182
    label "zapoznawanie"
  ]
  node [
    id 2183
    label "zaczynanie"
  ]
  node [
    id 2184
    label "trigger"
  ]
  node [
    id 2185
    label "wpisywanie"
  ]
  node [
    id 2186
    label "mental_hospital"
  ]
  node [
    id 2187
    label "wchodzenie"
  ]
  node [
    id 2188
    label "retraction"
  ]
  node [
    id 2189
    label "przewietrzanie"
  ]
  node [
    id 2190
    label "pos&#322;anie"
  ]
  node [
    id 2191
    label "znalezienie_si&#281;"
  ]
  node [
    id 2192
    label "introduction"
  ]
  node [
    id 2193
    label "sp&#281;dzenie"
  ]
  node [
    id 2194
    label "zainstalowanie"
  ]
  node [
    id 2195
    label "przypominanie"
  ]
  node [
    id 2196
    label "kojarzenie_si&#281;"
  ]
  node [
    id 2197
    label "sk&#322;anianie"
  ]
  node [
    id 2198
    label "adduction"
  ]
  node [
    id 2199
    label "nuklearyzacja"
  ]
  node [
    id 2200
    label "deduction"
  ]
  node [
    id 2201
    label "entrance"
  ]
  node [
    id 2202
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 2203
    label "wst&#281;p"
  ]
  node [
    id 2204
    label "wej&#347;cie"
  ]
  node [
    id 2205
    label "issue"
  ]
  node [
    id 2206
    label "umieszczenie"
  ]
  node [
    id 2207
    label "umo&#380;liwienie"
  ]
  node [
    id 2208
    label "wpisanie"
  ]
  node [
    id 2209
    label "podstawy"
  ]
  node [
    id 2210
    label "evocation"
  ]
  node [
    id 2211
    label "w&#322;&#261;czenie"
  ]
  node [
    id 2212
    label "przewietrzenie"
  ]
  node [
    id 2213
    label "pull"
  ]
  node [
    id 2214
    label "przesuwanie"
  ]
  node [
    id 2215
    label "j&#261;kanie"
  ]
  node [
    id 2216
    label "przetykanie"
  ]
  node [
    id 2217
    label "zaci&#261;ganie"
  ]
  node [
    id 2218
    label "przymocowywanie"
  ]
  node [
    id 2219
    label "przemieszczanie"
  ]
  node [
    id 2220
    label "przed&#322;u&#380;anie"
  ]
  node [
    id 2221
    label "rozci&#261;ganie"
  ]
  node [
    id 2222
    label "wymawianie"
  ]
  node [
    id 2223
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 2224
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 2225
    label "poprzecinanie"
  ]
  node [
    id 2226
    label "przerwanie"
  ]
  node [
    id 2227
    label "carving"
  ]
  node [
    id 2228
    label "cut"
  ]
  node [
    id 2229
    label "w&#281;ze&#322;"
  ]
  node [
    id 2230
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 2231
    label "zranienie"
  ]
  node [
    id 2232
    label "snub"
  ]
  node [
    id 2233
    label "podzielenie"
  ]
  node [
    id 2234
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2235
    label "dzielenie"
  ]
  node [
    id 2236
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2237
    label "kaleczenie"
  ]
  node [
    id 2238
    label "film_editing"
  ]
  node [
    id 2239
    label "intersection"
  ]
  node [
    id 2240
    label "przerywanie"
  ]
  node [
    id 2241
    label "wypuszczenie"
  ]
  node [
    id 2242
    label "niesienie"
  ]
  node [
    id 2243
    label "potrzymanie"
  ]
  node [
    id 2244
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 2245
    label "sprawowanie"
  ]
  node [
    id 2246
    label "wypuszczanie"
  ]
  node [
    id 2247
    label "poise"
  ]
  node [
    id 2248
    label "retention"
  ]
  node [
    id 2249
    label "dzier&#380;enie"
  ]
  node [
    id 2250
    label "noszenie"
  ]
  node [
    id 2251
    label "uniemo&#380;liwianie"
  ]
  node [
    id 2252
    label "podtrzymywanie"
  ]
  node [
    id 2253
    label "zachowywanie"
  ]
  node [
    id 2254
    label "clasp"
  ]
  node [
    id 2255
    label "przetrzymywanie"
  ]
  node [
    id 2256
    label "detention"
  ]
  node [
    id 2257
    label "przetrzymanie"
  ]
  node [
    id 2258
    label "hodowanie"
  ]
  node [
    id 2259
    label "komcio"
  ]
  node [
    id 2260
    label "blogosfera"
  ]
  node [
    id 2261
    label "pami&#281;tnik"
  ]
  node [
    id 2262
    label "strona"
  ]
  node [
    id 2263
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 2264
    label "pami&#261;tka"
  ]
  node [
    id 2265
    label "notes"
  ]
  node [
    id 2266
    label "zapiski"
  ]
  node [
    id 2267
    label "raptularz"
  ]
  node [
    id 2268
    label "album"
  ]
  node [
    id 2269
    label "utw&#243;r_epicki"
  ]
  node [
    id 2270
    label "kartka"
  ]
  node [
    id 2271
    label "logowanie"
  ]
  node [
    id 2272
    label "plik"
  ]
  node [
    id 2273
    label "adres_internetowy"
  ]
  node [
    id 2274
    label "serwis_internetowy"
  ]
  node [
    id 2275
    label "bok"
  ]
  node [
    id 2276
    label "skr&#281;canie"
  ]
  node [
    id 2277
    label "skr&#281;ca&#263;"
  ]
  node [
    id 2278
    label "orientowanie"
  ]
  node [
    id 2279
    label "skr&#281;ci&#263;"
  ]
  node [
    id 2280
    label "zorientowanie"
  ]
  node [
    id 2281
    label "fragment"
  ]
  node [
    id 2282
    label "layout"
  ]
  node [
    id 2283
    label "zorientowa&#263;"
  ]
  node [
    id 2284
    label "pagina"
  ]
  node [
    id 2285
    label "podmiot"
  ]
  node [
    id 2286
    label "orientowa&#263;"
  ]
  node [
    id 2287
    label "voice"
  ]
  node [
    id 2288
    label "orientacja"
  ]
  node [
    id 2289
    label "internet"
  ]
  node [
    id 2290
    label "powierzchnia"
  ]
  node [
    id 2291
    label "skr&#281;cenie"
  ]
  node [
    id 2292
    label "komentarz"
  ]
  node [
    id 2293
    label "lock"
  ]
  node [
    id 2294
    label "absolut"
  ]
  node [
    id 2295
    label "integer"
  ]
  node [
    id 2296
    label "liczba"
  ]
  node [
    id 2297
    label "zlewanie_si&#281;"
  ]
  node [
    id 2298
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 2299
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 2300
    label "pe&#322;ny"
  ]
  node [
    id 2301
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 2302
    label "olejek_eteryczny"
  ]
  node [
    id 2303
    label "zdziwi&#263;"
  ]
  node [
    id 2304
    label "oburzy&#263;"
  ]
  node [
    id 2305
    label "przerazi&#263;"
  ]
  node [
    id 2306
    label "shock"
  ]
  node [
    id 2307
    label "poruszy&#263;"
  ]
  node [
    id 2308
    label "wzbudzi&#263;"
  ]
  node [
    id 2309
    label "infuriate"
  ]
  node [
    id 2310
    label "zdenerwowa&#263;"
  ]
  node [
    id 2311
    label "dismay"
  ]
  node [
    id 2312
    label "przestraszy&#263;"
  ]
  node [
    id 2313
    label "motivate"
  ]
  node [
    id 2314
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 2315
    label "allude"
  ]
  node [
    id 2316
    label "stimulate"
  ]
  node [
    id 2317
    label "wywo&#322;a&#263;"
  ]
  node [
    id 2318
    label "arouse"
  ]
  node [
    id 2319
    label "overwhelm"
  ]
  node [
    id 2320
    label "podziwi&#263;"
  ]
  node [
    id 2321
    label "discover"
  ]
  node [
    id 2322
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 2323
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 2324
    label "wydoby&#263;"
  ]
  node [
    id 2325
    label "okre&#347;li&#263;"
  ]
  node [
    id 2326
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 2327
    label "rzekn&#261;&#263;"
  ]
  node [
    id 2328
    label "unwrap"
  ]
  node [
    id 2329
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 2330
    label "convey"
  ]
  node [
    id 2331
    label "tenis"
  ]
  node [
    id 2332
    label "da&#263;"
  ]
  node [
    id 2333
    label "ustawi&#263;"
  ]
  node [
    id 2334
    label "siatk&#243;wka"
  ]
  node [
    id 2335
    label "zagra&#263;"
  ]
  node [
    id 2336
    label "jedzenie"
  ]
  node [
    id 2337
    label "introduce"
  ]
  node [
    id 2338
    label "nafaszerowa&#263;"
  ]
  node [
    id 2339
    label "zaserwowa&#263;"
  ]
  node [
    id 2340
    label "draw"
  ]
  node [
    id 2341
    label "doby&#263;"
  ]
  node [
    id 2342
    label "g&#243;rnictwo"
  ]
  node [
    id 2343
    label "wyeksploatowa&#263;"
  ]
  node [
    id 2344
    label "extract"
  ]
  node [
    id 2345
    label "obtain"
  ]
  node [
    id 2346
    label "wyj&#261;&#263;"
  ]
  node [
    id 2347
    label "ocali&#263;"
  ]
  node [
    id 2348
    label "uzyska&#263;"
  ]
  node [
    id 2349
    label "wydosta&#263;"
  ]
  node [
    id 2350
    label "uwydatni&#263;"
  ]
  node [
    id 2351
    label "distill"
  ]
  node [
    id 2352
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 2353
    label "zakomunikowa&#263;"
  ]
  node [
    id 2354
    label "oznaczy&#263;"
  ]
  node [
    id 2355
    label "vent"
  ]
  node [
    id 2356
    label "zdecydowa&#263;"
  ]
  node [
    id 2357
    label "situate"
  ]
  node [
    id 2358
    label "nominate"
  ]
  node [
    id 2359
    label "thing"
  ]
  node [
    id 2360
    label "cosik"
  ]
  node [
    id 2361
    label "okre&#347;lony"
  ]
  node [
    id 2362
    label "przyzwoity"
  ]
  node [
    id 2363
    label "ciekawy"
  ]
  node [
    id 2364
    label "jako&#347;"
  ]
  node [
    id 2365
    label "jako_tako"
  ]
  node [
    id 2366
    label "niez&#322;y"
  ]
  node [
    id 2367
    label "charakterystyczny"
  ]
  node [
    id 2368
    label "wiadomy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 338
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 304
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 296
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 547
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 381
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 394
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 361
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 368
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 643
  ]
  edge [
    source 18
    target 644
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 359
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 331
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 298
  ]
  edge [
    source 18
    target 300
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 306
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 236
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 339
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 423
  ]
  edge [
    source 20
    target 826
  ]
  edge [
    source 20
    target 827
  ]
  edge [
    source 20
    target 425
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 830
  ]
  edge [
    source 20
    target 432
  ]
  edge [
    source 20
    target 433
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 831
  ]
  edge [
    source 21
    target 832
  ]
  edge [
    source 21
    target 833
  ]
  edge [
    source 21
    target 834
  ]
  edge [
    source 21
    target 835
  ]
  edge [
    source 21
    target 836
  ]
  edge [
    source 21
    target 837
  ]
  edge [
    source 21
    target 838
  ]
  edge [
    source 21
    target 839
  ]
  edge [
    source 21
    target 840
  ]
  edge [
    source 21
    target 841
  ]
  edge [
    source 21
    target 842
  ]
  edge [
    source 21
    target 843
  ]
  edge [
    source 21
    target 844
  ]
  edge [
    source 21
    target 845
  ]
  edge [
    source 21
    target 846
  ]
  edge [
    source 21
    target 847
  ]
  edge [
    source 21
    target 848
  ]
  edge [
    source 21
    target 849
  ]
  edge [
    source 21
    target 850
  ]
  edge [
    source 21
    target 851
  ]
  edge [
    source 21
    target 852
  ]
  edge [
    source 21
    target 853
  ]
  edge [
    source 21
    target 48
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 854
  ]
  edge [
    source 22
    target 855
  ]
  edge [
    source 22
    target 856
  ]
  edge [
    source 22
    target 857
  ]
  edge [
    source 22
    target 858
  ]
  edge [
    source 22
    target 88
  ]
  edge [
    source 22
    target 859
  ]
  edge [
    source 22
    target 860
  ]
  edge [
    source 22
    target 861
  ]
  edge [
    source 22
    target 862
  ]
  edge [
    source 22
    target 863
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 864
  ]
  edge [
    source 22
    target 865
  ]
  edge [
    source 22
    target 866
  ]
  edge [
    source 22
    target 867
  ]
  edge [
    source 22
    target 868
  ]
  edge [
    source 22
    target 869
  ]
  edge [
    source 22
    target 870
  ]
  edge [
    source 22
    target 871
  ]
  edge [
    source 22
    target 872
  ]
  edge [
    source 22
    target 873
  ]
  edge [
    source 22
    target 874
  ]
  edge [
    source 22
    target 875
  ]
  edge [
    source 22
    target 876
  ]
  edge [
    source 22
    target 877
  ]
  edge [
    source 22
    target 878
  ]
  edge [
    source 22
    target 879
  ]
  edge [
    source 22
    target 880
  ]
  edge [
    source 22
    target 881
  ]
  edge [
    source 22
    target 882
  ]
  edge [
    source 22
    target 883
  ]
  edge [
    source 22
    target 884
  ]
  edge [
    source 22
    target 885
  ]
  edge [
    source 22
    target 886
  ]
  edge [
    source 22
    target 887
  ]
  edge [
    source 22
    target 888
  ]
  edge [
    source 22
    target 889
  ]
  edge [
    source 22
    target 890
  ]
  edge [
    source 22
    target 891
  ]
  edge [
    source 22
    target 892
  ]
  edge [
    source 22
    target 893
  ]
  edge [
    source 22
    target 894
  ]
  edge [
    source 22
    target 895
  ]
  edge [
    source 22
    target 896
  ]
  edge [
    source 22
    target 897
  ]
  edge [
    source 22
    target 898
  ]
  edge [
    source 22
    target 899
  ]
  edge [
    source 22
    target 900
  ]
  edge [
    source 22
    target 901
  ]
  edge [
    source 22
    target 902
  ]
  edge [
    source 22
    target 903
  ]
  edge [
    source 22
    target 904
  ]
  edge [
    source 22
    target 753
  ]
  edge [
    source 22
    target 905
  ]
  edge [
    source 22
    target 906
  ]
  edge [
    source 22
    target 907
  ]
  edge [
    source 22
    target 908
  ]
  edge [
    source 22
    target 909
  ]
  edge [
    source 22
    target 910
  ]
  edge [
    source 22
    target 911
  ]
  edge [
    source 22
    target 912
  ]
  edge [
    source 22
    target 913
  ]
  edge [
    source 22
    target 914
  ]
  edge [
    source 22
    target 915
  ]
  edge [
    source 22
    target 916
  ]
  edge [
    source 22
    target 917
  ]
  edge [
    source 22
    target 918
  ]
  edge [
    source 22
    target 919
  ]
  edge [
    source 22
    target 920
  ]
  edge [
    source 22
    target 921
  ]
  edge [
    source 22
    target 922
  ]
  edge [
    source 22
    target 923
  ]
  edge [
    source 22
    target 924
  ]
  edge [
    source 22
    target 925
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 926
  ]
  edge [
    source 22
    target 927
  ]
  edge [
    source 22
    target 928
  ]
  edge [
    source 22
    target 929
  ]
  edge [
    source 22
    target 930
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 931
  ]
  edge [
    source 22
    target 932
  ]
  edge [
    source 22
    target 933
  ]
  edge [
    source 22
    target 934
  ]
  edge [
    source 22
    target 935
  ]
  edge [
    source 22
    target 936
  ]
  edge [
    source 22
    target 937
  ]
  edge [
    source 22
    target 938
  ]
  edge [
    source 22
    target 939
  ]
  edge [
    source 22
    target 940
  ]
  edge [
    source 22
    target 941
  ]
  edge [
    source 22
    target 942
  ]
  edge [
    source 22
    target 943
  ]
  edge [
    source 22
    target 944
  ]
  edge [
    source 22
    target 945
  ]
  edge [
    source 22
    target 946
  ]
  edge [
    source 22
    target 947
  ]
  edge [
    source 22
    target 948
  ]
  edge [
    source 22
    target 949
  ]
  edge [
    source 22
    target 950
  ]
  edge [
    source 22
    target 951
  ]
  edge [
    source 22
    target 952
  ]
  edge [
    source 22
    target 953
  ]
  edge [
    source 22
    target 954
  ]
  edge [
    source 22
    target 955
  ]
  edge [
    source 22
    target 956
  ]
  edge [
    source 22
    target 957
  ]
  edge [
    source 22
    target 958
  ]
  edge [
    source 22
    target 959
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 960
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 961
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 962
  ]
  edge [
    source 23
    target 642
  ]
  edge [
    source 23
    target 963
  ]
  edge [
    source 23
    target 964
  ]
  edge [
    source 23
    target 965
  ]
  edge [
    source 23
    target 966
  ]
  edge [
    source 23
    target 967
  ]
  edge [
    source 23
    target 968
  ]
  edge [
    source 23
    target 969
  ]
  edge [
    source 23
    target 970
  ]
  edge [
    source 23
    target 971
  ]
  edge [
    source 23
    target 972
  ]
  edge [
    source 23
    target 391
  ]
  edge [
    source 23
    target 973
  ]
  edge [
    source 23
    target 974
  ]
  edge [
    source 23
    target 975
  ]
  edge [
    source 23
    target 976
  ]
  edge [
    source 23
    target 977
  ]
  edge [
    source 23
    target 978
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 980
  ]
  edge [
    source 23
    target 981
  ]
  edge [
    source 23
    target 982
  ]
  edge [
    source 23
    target 983
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 989
  ]
  edge [
    source 23
    target 990
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 992
  ]
  edge [
    source 23
    target 993
  ]
  edge [
    source 23
    target 994
  ]
  edge [
    source 23
    target 995
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 997
  ]
  edge [
    source 23
    target 998
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1002
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 1004
  ]
  edge [
    source 23
    target 1005
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 1009
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1011
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 407
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 854
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 479
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 817
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 547
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 641
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 899
  ]
  edge [
    source 23
    target 83
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 583
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 404
  ]
  edge [
    source 23
    target 516
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 697
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 943
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 954
  ]
  edge [
    source 23
    target 955
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 917
  ]
  edge [
    source 23
    target 304
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 23
    target 343
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 1130
  ]
  edge [
    source 23
    target 1131
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 23
    target 1133
  ]
  edge [
    source 23
    target 1134
  ]
  edge [
    source 23
    target 688
  ]
  edge [
    source 23
    target 1135
  ]
  edge [
    source 23
    target 1136
  ]
  edge [
    source 23
    target 1137
  ]
  edge [
    source 23
    target 1138
  ]
  edge [
    source 23
    target 1139
  ]
  edge [
    source 23
    target 1140
  ]
  edge [
    source 23
    target 1141
  ]
  edge [
    source 23
    target 1142
  ]
  edge [
    source 23
    target 1143
  ]
  edge [
    source 23
    target 1144
  ]
  edge [
    source 23
    target 1145
  ]
  edge [
    source 23
    target 1146
  ]
  edge [
    source 23
    target 1147
  ]
  edge [
    source 23
    target 1148
  ]
  edge [
    source 23
    target 1149
  ]
  edge [
    source 23
    target 1150
  ]
  edge [
    source 23
    target 1151
  ]
  edge [
    source 23
    target 80
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 23
    target 1153
  ]
  edge [
    source 23
    target 1154
  ]
  edge [
    source 23
    target 392
  ]
  edge [
    source 23
    target 1155
  ]
  edge [
    source 23
    target 1156
  ]
  edge [
    source 23
    target 1157
  ]
  edge [
    source 23
    target 1158
  ]
  edge [
    source 23
    target 1159
  ]
  edge [
    source 23
    target 1160
  ]
  edge [
    source 23
    target 1161
  ]
  edge [
    source 23
    target 1162
  ]
  edge [
    source 23
    target 1163
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 1166
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 262
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 23
    target 1182
  ]
  edge [
    source 23
    target 1183
  ]
  edge [
    source 23
    target 1184
  ]
  edge [
    source 23
    target 1185
  ]
  edge [
    source 23
    target 1186
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1190
  ]
  edge [
    source 23
    target 1191
  ]
  edge [
    source 23
    target 1192
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 697
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 616
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 359
  ]
  edge [
    source 24
    target 1222
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 24
    target 1226
  ]
  edge [
    source 24
    target 1227
  ]
  edge [
    source 24
    target 1228
  ]
  edge [
    source 24
    target 613
  ]
  edge [
    source 24
    target 1229
  ]
  edge [
    source 24
    target 1230
  ]
  edge [
    source 24
    target 1231
  ]
  edge [
    source 24
    target 1232
  ]
  edge [
    source 24
    target 1233
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 547
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1177
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 36
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 688
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
  edge [
    source 25
    target 679
  ]
  edge [
    source 25
    target 1265
  ]
  edge [
    source 25
    target 1266
  ]
  edge [
    source 25
    target 1267
  ]
  edge [
    source 25
    target 1268
  ]
  edge [
    source 25
    target 1269
  ]
  edge [
    source 25
    target 1270
  ]
  edge [
    source 25
    target 1144
  ]
  edge [
    source 25
    target 1271
  ]
  edge [
    source 25
    target 1272
  ]
  edge [
    source 25
    target 1273
  ]
  edge [
    source 25
    target 1274
  ]
  edge [
    source 25
    target 1275
  ]
  edge [
    source 25
    target 1276
  ]
  edge [
    source 25
    target 1277
  ]
  edge [
    source 25
    target 1278
  ]
  edge [
    source 25
    target 1279
  ]
  edge [
    source 25
    target 1280
  ]
  edge [
    source 25
    target 1281
  ]
  edge [
    source 25
    target 1282
  ]
  edge [
    source 25
    target 293
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 1283
  ]
  edge [
    source 26
    target 1284
  ]
  edge [
    source 26
    target 1285
  ]
  edge [
    source 26
    target 1286
  ]
  edge [
    source 26
    target 1287
  ]
  edge [
    source 26
    target 1288
  ]
  edge [
    source 26
    target 1289
  ]
  edge [
    source 26
    target 1290
  ]
  edge [
    source 26
    target 1106
  ]
  edge [
    source 26
    target 1291
  ]
  edge [
    source 26
    target 1292
  ]
  edge [
    source 26
    target 1293
  ]
  edge [
    source 26
    target 1294
  ]
  edge [
    source 26
    target 1295
  ]
  edge [
    source 26
    target 1296
  ]
  edge [
    source 26
    target 1297
  ]
  edge [
    source 26
    target 1298
  ]
  edge [
    source 26
    target 1299
  ]
  edge [
    source 26
    target 1300
  ]
  edge [
    source 26
    target 1301
  ]
  edge [
    source 26
    target 1302
  ]
  edge [
    source 26
    target 1303
  ]
  edge [
    source 26
    target 1304
  ]
  edge [
    source 26
    target 1305
  ]
  edge [
    source 26
    target 1306
  ]
  edge [
    source 26
    target 1307
  ]
  edge [
    source 26
    target 1308
  ]
  edge [
    source 26
    target 1309
  ]
  edge [
    source 26
    target 1310
  ]
  edge [
    source 26
    target 1311
  ]
  edge [
    source 26
    target 1312
  ]
  edge [
    source 26
    target 1313
  ]
  edge [
    source 26
    target 1314
  ]
  edge [
    source 26
    target 1315
  ]
  edge [
    source 26
    target 1316
  ]
  edge [
    source 26
    target 1317
  ]
  edge [
    source 26
    target 1318
  ]
  edge [
    source 26
    target 1319
  ]
  edge [
    source 26
    target 1320
  ]
  edge [
    source 26
    target 1321
  ]
  edge [
    source 26
    target 1322
  ]
  edge [
    source 26
    target 1323
  ]
  edge [
    source 26
    target 1324
  ]
  edge [
    source 26
    target 1325
  ]
  edge [
    source 26
    target 1326
  ]
  edge [
    source 26
    target 1327
  ]
  edge [
    source 26
    target 1328
  ]
  edge [
    source 26
    target 1329
  ]
  edge [
    source 26
    target 1330
  ]
  edge [
    source 26
    target 1331
  ]
  edge [
    source 26
    target 1332
  ]
  edge [
    source 26
    target 868
  ]
  edge [
    source 26
    target 1333
  ]
  edge [
    source 26
    target 1334
  ]
  edge [
    source 26
    target 1335
  ]
  edge [
    source 26
    target 1336
  ]
  edge [
    source 26
    target 1337
  ]
  edge [
    source 26
    target 1338
  ]
  edge [
    source 26
    target 1339
  ]
  edge [
    source 26
    target 1340
  ]
  edge [
    source 26
    target 1341
  ]
  edge [
    source 26
    target 1342
  ]
  edge [
    source 26
    target 1343
  ]
  edge [
    source 26
    target 1344
  ]
  edge [
    source 26
    target 1345
  ]
  edge [
    source 26
    target 1346
  ]
  edge [
    source 26
    target 1347
  ]
  edge [
    source 26
    target 1348
  ]
  edge [
    source 26
    target 1349
  ]
  edge [
    source 26
    target 1350
  ]
  edge [
    source 26
    target 1351
  ]
  edge [
    source 26
    target 1352
  ]
  edge [
    source 26
    target 1353
  ]
  edge [
    source 26
    target 1354
  ]
  edge [
    source 26
    target 900
  ]
  edge [
    source 26
    target 1355
  ]
  edge [
    source 26
    target 1356
  ]
  edge [
    source 26
    target 1357
  ]
  edge [
    source 26
    target 1358
  ]
  edge [
    source 26
    target 1359
  ]
  edge [
    source 26
    target 1360
  ]
  edge [
    source 26
    target 1361
  ]
  edge [
    source 26
    target 1362
  ]
  edge [
    source 26
    target 1363
  ]
  edge [
    source 26
    target 1364
  ]
  edge [
    source 26
    target 1365
  ]
  edge [
    source 26
    target 1366
  ]
  edge [
    source 26
    target 1367
  ]
  edge [
    source 26
    target 1368
  ]
  edge [
    source 26
    target 943
  ]
  edge [
    source 26
    target 1369
  ]
  edge [
    source 26
    target 1091
  ]
  edge [
    source 26
    target 1370
  ]
  edge [
    source 26
    target 1371
  ]
  edge [
    source 26
    target 1372
  ]
  edge [
    source 26
    target 1373
  ]
  edge [
    source 26
    target 1374
  ]
  edge [
    source 26
    target 1375
  ]
  edge [
    source 26
    target 1376
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1377
  ]
  edge [
    source 27
    target 1378
  ]
  edge [
    source 27
    target 1379
  ]
  edge [
    source 27
    target 1380
  ]
  edge [
    source 27
    target 1381
  ]
  edge [
    source 27
    target 1382
  ]
  edge [
    source 27
    target 1383
  ]
  edge [
    source 27
    target 1384
  ]
  edge [
    source 27
    target 1385
  ]
  edge [
    source 27
    target 1386
  ]
  edge [
    source 27
    target 1387
  ]
  edge [
    source 27
    target 1388
  ]
  edge [
    source 27
    target 1389
  ]
  edge [
    source 27
    target 1390
  ]
  edge [
    source 27
    target 1391
  ]
  edge [
    source 27
    target 1392
  ]
  edge [
    source 27
    target 1393
  ]
  edge [
    source 27
    target 1394
  ]
  edge [
    source 27
    target 1395
  ]
  edge [
    source 27
    target 1100
  ]
  edge [
    source 27
    target 484
  ]
  edge [
    source 27
    target 1396
  ]
  edge [
    source 27
    target 1397
  ]
  edge [
    source 27
    target 1398
  ]
  edge [
    source 27
    target 1399
  ]
  edge [
    source 27
    target 1400
  ]
  edge [
    source 27
    target 1401
  ]
  edge [
    source 27
    target 1402
  ]
  edge [
    source 27
    target 1403
  ]
  edge [
    source 27
    target 1404
  ]
  edge [
    source 27
    target 1405
  ]
  edge [
    source 27
    target 550
  ]
  edge [
    source 27
    target 1406
  ]
  edge [
    source 27
    target 1407
  ]
  edge [
    source 27
    target 1408
  ]
  edge [
    source 27
    target 1409
  ]
  edge [
    source 27
    target 1410
  ]
  edge [
    source 27
    target 1411
  ]
  edge [
    source 27
    target 1412
  ]
  edge [
    source 27
    target 1413
  ]
  edge [
    source 27
    target 1414
  ]
  edge [
    source 27
    target 1415
  ]
  edge [
    source 27
    target 1416
  ]
  edge [
    source 27
    target 1417
  ]
  edge [
    source 27
    target 1418
  ]
  edge [
    source 27
    target 1419
  ]
  edge [
    source 27
    target 1420
  ]
  edge [
    source 27
    target 1421
  ]
  edge [
    source 27
    target 1422
  ]
  edge [
    source 27
    target 1423
  ]
  edge [
    source 27
    target 1424
  ]
  edge [
    source 27
    target 1425
  ]
  edge [
    source 27
    target 1426
  ]
  edge [
    source 27
    target 1427
  ]
  edge [
    source 27
    target 1428
  ]
  edge [
    source 27
    target 1429
  ]
  edge [
    source 27
    target 1430
  ]
  edge [
    source 27
    target 1431
  ]
  edge [
    source 27
    target 1432
  ]
  edge [
    source 27
    target 1433
  ]
  edge [
    source 27
    target 1434
  ]
  edge [
    source 27
    target 1435
  ]
  edge [
    source 27
    target 1436
  ]
  edge [
    source 27
    target 1437
  ]
  edge [
    source 27
    target 1438
  ]
  edge [
    source 27
    target 1439
  ]
  edge [
    source 27
    target 1440
  ]
  edge [
    source 27
    target 1441
  ]
  edge [
    source 27
    target 1442
  ]
  edge [
    source 27
    target 1443
  ]
  edge [
    source 27
    target 1444
  ]
  edge [
    source 27
    target 1445
  ]
  edge [
    source 27
    target 1446
  ]
  edge [
    source 27
    target 1447
  ]
  edge [
    source 27
    target 1448
  ]
  edge [
    source 27
    target 1449
  ]
  edge [
    source 27
    target 1450
  ]
  edge [
    source 27
    target 1306
  ]
  edge [
    source 27
    target 1451
  ]
  edge [
    source 27
    target 1452
  ]
  edge [
    source 27
    target 613
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1453
  ]
  edge [
    source 28
    target 1454
  ]
  edge [
    source 28
    target 1455
  ]
  edge [
    source 28
    target 1456
  ]
  edge [
    source 28
    target 343
  ]
  edge [
    source 28
    target 1457
  ]
  edge [
    source 28
    target 1458
  ]
  edge [
    source 28
    target 1459
  ]
  edge [
    source 28
    target 547
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1460
  ]
  edge [
    source 28
    target 1461
  ]
  edge [
    source 28
    target 1462
  ]
  edge [
    source 28
    target 1463
  ]
  edge [
    source 28
    target 1464
  ]
  edge [
    source 28
    target 1465
  ]
  edge [
    source 28
    target 1466
  ]
  edge [
    source 28
    target 926
  ]
  edge [
    source 28
    target 1467
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 88
  ]
  edge [
    source 28
    target 1125
  ]
  edge [
    source 28
    target 753
  ]
  edge [
    source 28
    target 1468
  ]
  edge [
    source 28
    target 1469
  ]
  edge [
    source 28
    target 1470
  ]
  edge [
    source 28
    target 1471
  ]
  edge [
    source 28
    target 1472
  ]
  edge [
    source 28
    target 1473
  ]
  edge [
    source 28
    target 1177
  ]
  edge [
    source 28
    target 1474
  ]
  edge [
    source 28
    target 1475
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 1476
  ]
  edge [
    source 28
    target 1477
  ]
  edge [
    source 28
    target 1478
  ]
  edge [
    source 28
    target 1479
  ]
  edge [
    source 28
    target 1480
  ]
  edge [
    source 28
    target 546
  ]
  edge [
    source 28
    target 545
  ]
  edge [
    source 28
    target 1481
  ]
  edge [
    source 28
    target 1482
  ]
  edge [
    source 28
    target 1483
  ]
  edge [
    source 28
    target 1484
  ]
  edge [
    source 28
    target 1485
  ]
  edge [
    source 28
    target 424
  ]
  edge [
    source 28
    target 829
  ]
  edge [
    source 28
    target 429
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1486
  ]
  edge [
    source 29
    target 46
  ]
  edge [
    source 29
    target 1487
  ]
  edge [
    source 29
    target 1488
  ]
  edge [
    source 29
    target 1489
  ]
  edge [
    source 30
    target 1490
  ]
  edge [
    source 30
    target 502
  ]
  edge [
    source 30
    target 1491
  ]
  edge [
    source 30
    target 1492
  ]
  edge [
    source 30
    target 1080
  ]
  edge [
    source 30
    target 655
  ]
  edge [
    source 30
    target 1493
  ]
  edge [
    source 30
    target 1494
  ]
  edge [
    source 30
    target 1495
  ]
  edge [
    source 30
    target 1496
  ]
  edge [
    source 30
    target 1497
  ]
  edge [
    source 30
    target 1498
  ]
  edge [
    source 30
    target 1499
  ]
  edge [
    source 30
    target 1500
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1501
  ]
  edge [
    source 31
    target 1502
  ]
  edge [
    source 31
    target 1503
  ]
  edge [
    source 31
    target 1504
  ]
  edge [
    source 31
    target 537
  ]
  edge [
    source 31
    target 1505
  ]
  edge [
    source 31
    target 1506
  ]
  edge [
    source 31
    target 1507
  ]
  edge [
    source 31
    target 1508
  ]
  edge [
    source 31
    target 1509
  ]
  edge [
    source 31
    target 1510
  ]
  edge [
    source 31
    target 1511
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1512
  ]
  edge [
    source 33
    target 1513
  ]
  edge [
    source 33
    target 1514
  ]
  edge [
    source 33
    target 1515
  ]
  edge [
    source 33
    target 1516
  ]
  edge [
    source 33
    target 135
  ]
  edge [
    source 33
    target 1517
  ]
  edge [
    source 33
    target 1518
  ]
  edge [
    source 34
    target 1069
  ]
  edge [
    source 34
    target 1519
  ]
  edge [
    source 34
    target 1520
  ]
  edge [
    source 34
    target 1177
  ]
  edge [
    source 34
    target 1521
  ]
  edge [
    source 34
    target 1522
  ]
  edge [
    source 34
    target 1523
  ]
  edge [
    source 34
    target 1095
  ]
  edge [
    source 34
    target 1524
  ]
  edge [
    source 34
    target 1525
  ]
  edge [
    source 34
    target 968
  ]
  edge [
    source 34
    target 1526
  ]
  edge [
    source 34
    target 1527
  ]
  edge [
    source 34
    target 1528
  ]
  edge [
    source 34
    target 547
  ]
  edge [
    source 34
    target 1529
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1144
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 462
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 642
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 1538
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1541
  ]
  edge [
    source 34
    target 1542
  ]
  edge [
    source 34
    target 1543
  ]
  edge [
    source 34
    target 1544
  ]
  edge [
    source 34
    target 1545
  ]
  edge [
    source 34
    target 1546
  ]
  edge [
    source 34
    target 1547
  ]
  edge [
    source 34
    target 1548
  ]
  edge [
    source 34
    target 1549
  ]
  edge [
    source 34
    target 1550
  ]
  edge [
    source 34
    target 1551
  ]
  edge [
    source 34
    target 1115
  ]
  edge [
    source 34
    target 1552
  ]
  edge [
    source 34
    target 1553
  ]
  edge [
    source 34
    target 1554
  ]
  edge [
    source 34
    target 950
  ]
  edge [
    source 34
    target 1555
  ]
  edge [
    source 34
    target 1556
  ]
  edge [
    source 34
    target 1557
  ]
  edge [
    source 34
    target 1558
  ]
  edge [
    source 34
    target 1559
  ]
  edge [
    source 34
    target 1560
  ]
  edge [
    source 34
    target 304
  ]
  edge [
    source 34
    target 1161
  ]
  edge [
    source 34
    target 88
  ]
  edge [
    source 34
    target 543
  ]
  edge [
    source 34
    target 1071
  ]
  edge [
    source 34
    target 1163
  ]
  edge [
    source 34
    target 1077
  ]
  edge [
    source 34
    target 1178
  ]
  edge [
    source 34
    target 404
  ]
  edge [
    source 34
    target 1168
  ]
  edge [
    source 34
    target 960
  ]
  edge [
    source 34
    target 144
  ]
  edge [
    source 34
    target 961
  ]
  edge [
    source 34
    target 147
  ]
  edge [
    source 34
    target 962
  ]
  edge [
    source 34
    target 963
  ]
  edge [
    source 34
    target 964
  ]
  edge [
    source 34
    target 965
  ]
  edge [
    source 34
    target 966
  ]
  edge [
    source 34
    target 967
  ]
  edge [
    source 34
    target 969
  ]
  edge [
    source 34
    target 970
  ]
  edge [
    source 34
    target 971
  ]
  edge [
    source 34
    target 972
  ]
  edge [
    source 34
    target 391
  ]
  edge [
    source 34
    target 973
  ]
  edge [
    source 34
    target 974
  ]
  edge [
    source 34
    target 975
  ]
  edge [
    source 34
    target 976
  ]
  edge [
    source 34
    target 977
  ]
  edge [
    source 34
    target 978
  ]
  edge [
    source 34
    target 979
  ]
  edge [
    source 34
    target 980
  ]
  edge [
    source 34
    target 1561
  ]
  edge [
    source 34
    target 54
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1512
  ]
  edge [
    source 35
    target 1513
  ]
  edge [
    source 35
    target 1514
  ]
  edge [
    source 35
    target 1515
  ]
  edge [
    source 35
    target 1516
  ]
  edge [
    source 35
    target 135
  ]
  edge [
    source 35
    target 1517
  ]
  edge [
    source 35
    target 1518
  ]
  edge [
    source 36
    target 1562
  ]
  edge [
    source 36
    target 1563
  ]
  edge [
    source 36
    target 1564
  ]
  edge [
    source 36
    target 1565
  ]
  edge [
    source 36
    target 1566
  ]
  edge [
    source 36
    target 1567
  ]
  edge [
    source 36
    target 1236
  ]
  edge [
    source 36
    target 1568
  ]
  edge [
    source 36
    target 1569
  ]
  edge [
    source 36
    target 1570
  ]
  edge [
    source 36
    target 1571
  ]
  edge [
    source 36
    target 1572
  ]
  edge [
    source 36
    target 479
  ]
  edge [
    source 36
    target 1573
  ]
  edge [
    source 36
    target 1574
  ]
  edge [
    source 36
    target 1575
  ]
  edge [
    source 36
    target 1576
  ]
  edge [
    source 36
    target 1577
  ]
  edge [
    source 36
    target 1578
  ]
  edge [
    source 36
    target 1579
  ]
  edge [
    source 36
    target 1580
  ]
  edge [
    source 36
    target 1581
  ]
  edge [
    source 36
    target 1582
  ]
  edge [
    source 36
    target 1583
  ]
  edge [
    source 36
    target 1584
  ]
  edge [
    source 36
    target 1585
  ]
  edge [
    source 36
    target 1586
  ]
  edge [
    source 36
    target 173
  ]
  edge [
    source 36
    target 1587
  ]
  edge [
    source 36
    target 1588
  ]
  edge [
    source 36
    target 1589
  ]
  edge [
    source 36
    target 1590
  ]
  edge [
    source 36
    target 547
  ]
  edge [
    source 36
    target 1281
  ]
  edge [
    source 36
    target 1591
  ]
  edge [
    source 36
    target 1592
  ]
  edge [
    source 36
    target 1593
  ]
  edge [
    source 36
    target 1594
  ]
  edge [
    source 36
    target 1595
  ]
  edge [
    source 36
    target 1596
  ]
  edge [
    source 36
    target 1597
  ]
  edge [
    source 36
    target 1598
  ]
  edge [
    source 36
    target 1599
  ]
  edge [
    source 36
    target 1600
  ]
  edge [
    source 36
    target 1601
  ]
  edge [
    source 36
    target 1602
  ]
  edge [
    source 36
    target 1603
  ]
  edge [
    source 36
    target 1604
  ]
  edge [
    source 36
    target 1605
  ]
  edge [
    source 36
    target 1606
  ]
  edge [
    source 36
    target 1607
  ]
  edge [
    source 36
    target 1608
  ]
  edge [
    source 36
    target 1609
  ]
  edge [
    source 36
    target 1610
  ]
  edge [
    source 36
    target 1611
  ]
  edge [
    source 36
    target 1612
  ]
  edge [
    source 36
    target 1613
  ]
  edge [
    source 36
    target 1614
  ]
  edge [
    source 36
    target 1615
  ]
  edge [
    source 36
    target 1616
  ]
  edge [
    source 36
    target 1617
  ]
  edge [
    source 36
    target 1618
  ]
  edge [
    source 36
    target 1247
  ]
  edge [
    source 36
    target 1248
  ]
  edge [
    source 36
    target 1249
  ]
  edge [
    source 36
    target 1250
  ]
  edge [
    source 36
    target 1237
  ]
  edge [
    source 36
    target 1251
  ]
  edge [
    source 36
    target 1252
  ]
  edge [
    source 36
    target 1242
  ]
  edge [
    source 36
    target 1253
  ]
  edge [
    source 36
    target 1254
  ]
  edge [
    source 36
    target 1255
  ]
  edge [
    source 36
    target 1256
  ]
  edge [
    source 36
    target 1257
  ]
  edge [
    source 36
    target 1258
  ]
  edge [
    source 36
    target 1259
  ]
  edge [
    source 36
    target 688
  ]
  edge [
    source 36
    target 1260
  ]
  edge [
    source 36
    target 1261
  ]
  edge [
    source 36
    target 1262
  ]
  edge [
    source 36
    target 1263
  ]
  edge [
    source 36
    target 1264
  ]
  edge [
    source 36
    target 679
  ]
  edge [
    source 36
    target 1265
  ]
  edge [
    source 36
    target 1266
  ]
  edge [
    source 36
    target 1267
  ]
  edge [
    source 36
    target 1268
  ]
  edge [
    source 36
    target 1269
  ]
  edge [
    source 36
    target 1270
  ]
  edge [
    source 36
    target 1144
  ]
  edge [
    source 36
    target 1271
  ]
  edge [
    source 36
    target 1272
  ]
  edge [
    source 36
    target 1273
  ]
  edge [
    source 36
    target 1274
  ]
  edge [
    source 36
    target 1275
  ]
  edge [
    source 36
    target 1276
  ]
  edge [
    source 36
    target 1277
  ]
  edge [
    source 36
    target 1278
  ]
  edge [
    source 36
    target 1279
  ]
  edge [
    source 36
    target 1280
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1619
  ]
  edge [
    source 37
    target 1062
  ]
  edge [
    source 37
    target 1620
  ]
  edge [
    source 37
    target 1621
  ]
  edge [
    source 37
    target 1622
  ]
  edge [
    source 37
    target 1623
  ]
  edge [
    source 37
    target 1624
  ]
  edge [
    source 37
    target 352
  ]
  edge [
    source 37
    target 1625
  ]
  edge [
    source 37
    target 1626
  ]
  edge [
    source 37
    target 1627
  ]
  edge [
    source 37
    target 404
  ]
  edge [
    source 37
    target 1628
  ]
  edge [
    source 37
    target 1629
  ]
  edge [
    source 37
    target 1630
  ]
  edge [
    source 37
    target 1631
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 1632
  ]
  edge [
    source 37
    target 1633
  ]
  edge [
    source 37
    target 1634
  ]
  edge [
    source 37
    target 1635
  ]
  edge [
    source 37
    target 1636
  ]
  edge [
    source 37
    target 1637
  ]
  edge [
    source 37
    target 1638
  ]
  edge [
    source 37
    target 1088
  ]
  edge [
    source 37
    target 1639
  ]
  edge [
    source 37
    target 543
  ]
  edge [
    source 37
    target 1640
  ]
  edge [
    source 37
    target 1641
  ]
  edge [
    source 37
    target 716
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 1642
  ]
  edge [
    source 37
    target 1643
  ]
  edge [
    source 37
    target 1644
  ]
  edge [
    source 37
    target 1645
  ]
  edge [
    source 37
    target 1646
  ]
  edge [
    source 37
    target 1647
  ]
  edge [
    source 37
    target 1648
  ]
  edge [
    source 37
    target 1649
  ]
  edge [
    source 37
    target 1650
  ]
  edge [
    source 37
    target 1651
  ]
  edge [
    source 37
    target 1652
  ]
  edge [
    source 37
    target 1653
  ]
  edge [
    source 37
    target 1654
  ]
  edge [
    source 37
    target 1655
  ]
  edge [
    source 37
    target 1656
  ]
  edge [
    source 37
    target 1657
  ]
  edge [
    source 37
    target 1658
  ]
  edge [
    source 37
    target 1659
  ]
  edge [
    source 37
    target 1660
  ]
  edge [
    source 37
    target 1661
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1654
  ]
  edge [
    source 38
    target 1662
  ]
  edge [
    source 38
    target 1663
  ]
  edge [
    source 38
    target 1664
  ]
  edge [
    source 38
    target 1665
  ]
  edge [
    source 38
    target 1666
  ]
  edge [
    source 38
    target 1667
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1668
  ]
  edge [
    source 39
    target 1669
  ]
  edge [
    source 39
    target 1670
  ]
  edge [
    source 39
    target 1098
  ]
  edge [
    source 39
    target 1671
  ]
  edge [
    source 39
    target 1672
  ]
  edge [
    source 39
    target 1673
  ]
  edge [
    source 39
    target 1674
  ]
  edge [
    source 39
    target 1675
  ]
  edge [
    source 39
    target 1676
  ]
  edge [
    source 39
    target 1677
  ]
  edge [
    source 39
    target 1678
  ]
  edge [
    source 39
    target 1679
  ]
  edge [
    source 39
    target 1680
  ]
  edge [
    source 40
    target 1681
  ]
  edge [
    source 40
    target 1682
  ]
  edge [
    source 40
    target 1683
  ]
  edge [
    source 40
    target 1684
  ]
  edge [
    source 40
    target 1685
  ]
  edge [
    source 40
    target 1686
  ]
  edge [
    source 40
    target 1687
  ]
  edge [
    source 40
    target 1688
  ]
  edge [
    source 40
    target 1689
  ]
  edge [
    source 40
    target 1690
  ]
  edge [
    source 40
    target 1691
  ]
  edge [
    source 40
    target 1692
  ]
  edge [
    source 40
    target 1693
  ]
  edge [
    source 40
    target 1694
  ]
  edge [
    source 40
    target 1695
  ]
  edge [
    source 40
    target 1696
  ]
  edge [
    source 40
    target 1697
  ]
  edge [
    source 40
    target 1698
  ]
  edge [
    source 40
    target 1699
  ]
  edge [
    source 40
    target 1700
  ]
  edge [
    source 40
    target 1701
  ]
  edge [
    source 40
    target 1702
  ]
  edge [
    source 40
    target 1703
  ]
  edge [
    source 40
    target 1704
  ]
  edge [
    source 40
    target 1705
  ]
  edge [
    source 40
    target 1654
  ]
  edge [
    source 40
    target 1706
  ]
  edge [
    source 40
    target 1707
  ]
  edge [
    source 40
    target 1708
  ]
  edge [
    source 40
    target 1709
  ]
  edge [
    source 40
    target 1710
  ]
  edge [
    source 40
    target 1711
  ]
  edge [
    source 40
    target 1712
  ]
  edge [
    source 40
    target 1713
  ]
  edge [
    source 40
    target 1714
  ]
  edge [
    source 40
    target 1715
  ]
  edge [
    source 40
    target 1716
  ]
  edge [
    source 40
    target 1717
  ]
  edge [
    source 40
    target 1718
  ]
  edge [
    source 40
    target 1719
  ]
  edge [
    source 40
    target 1720
  ]
  edge [
    source 40
    target 1721
  ]
  edge [
    source 40
    target 1722
  ]
  edge [
    source 40
    target 1723
  ]
  edge [
    source 40
    target 1724
  ]
  edge [
    source 40
    target 1725
  ]
  edge [
    source 40
    target 1726
  ]
  edge [
    source 40
    target 1727
  ]
  edge [
    source 40
    target 1728
  ]
  edge [
    source 40
    target 1729
  ]
  edge [
    source 40
    target 1730
  ]
  edge [
    source 40
    target 1731
  ]
  edge [
    source 40
    target 1732
  ]
  edge [
    source 40
    target 1733
  ]
  edge [
    source 40
    target 1734
  ]
  edge [
    source 40
    target 1735
  ]
  edge [
    source 40
    target 1736
  ]
  edge [
    source 40
    target 1737
  ]
  edge [
    source 40
    target 1738
  ]
  edge [
    source 40
    target 1739
  ]
  edge [
    source 40
    target 1740
  ]
  edge [
    source 40
    target 1741
  ]
  edge [
    source 40
    target 758
  ]
  edge [
    source 40
    target 1742
  ]
  edge [
    source 40
    target 1743
  ]
  edge [
    source 40
    target 1744
  ]
  edge [
    source 40
    target 1745
  ]
  edge [
    source 40
    target 1746
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 688
  ]
  edge [
    source 41
    target 1747
  ]
  edge [
    source 41
    target 135
  ]
  edge [
    source 41
    target 1748
  ]
  edge [
    source 41
    target 1749
  ]
  edge [
    source 41
    target 1750
  ]
  edge [
    source 41
    target 1751
  ]
  edge [
    source 41
    target 1752
  ]
  edge [
    source 41
    target 1753
  ]
  edge [
    source 41
    target 1754
  ]
  edge [
    source 41
    target 1755
  ]
  edge [
    source 41
    target 179
  ]
  edge [
    source 41
    target 954
  ]
  edge [
    source 41
    target 547
  ]
  edge [
    source 41
    target 1756
  ]
  edge [
    source 41
    target 298
  ]
  edge [
    source 41
    target 1757
  ]
  edge [
    source 41
    target 168
  ]
  edge [
    source 41
    target 169
  ]
  edge [
    source 41
    target 170
  ]
  edge [
    source 41
    target 171
  ]
  edge [
    source 41
    target 172
  ]
  edge [
    source 41
    target 173
  ]
  edge [
    source 41
    target 174
  ]
  edge [
    source 41
    target 175
  ]
  edge [
    source 41
    target 176
  ]
  edge [
    source 41
    target 177
  ]
  edge [
    source 41
    target 178
  ]
  edge [
    source 41
    target 180
  ]
  edge [
    source 41
    target 181
  ]
  edge [
    source 41
    target 182
  ]
  edge [
    source 41
    target 183
  ]
  edge [
    source 41
    target 184
  ]
  edge [
    source 41
    target 185
  ]
  edge [
    source 41
    target 186
  ]
  edge [
    source 41
    target 187
  ]
  edge [
    source 41
    target 188
  ]
  edge [
    source 41
    target 189
  ]
  edge [
    source 41
    target 190
  ]
  edge [
    source 41
    target 191
  ]
  edge [
    source 41
    target 192
  ]
  edge [
    source 41
    target 193
  ]
  edge [
    source 41
    target 194
  ]
  edge [
    source 41
    target 195
  ]
  edge [
    source 41
    target 196
  ]
  edge [
    source 41
    target 197
  ]
  edge [
    source 41
    target 198
  ]
  edge [
    source 41
    target 199
  ]
  edge [
    source 41
    target 200
  ]
  edge [
    source 41
    target 201
  ]
  edge [
    source 41
    target 202
  ]
  edge [
    source 41
    target 203
  ]
  edge [
    source 41
    target 204
  ]
  edge [
    source 41
    target 1758
  ]
  edge [
    source 41
    target 88
  ]
  edge [
    source 41
    target 1759
  ]
  edge [
    source 41
    target 287
  ]
  edge [
    source 41
    target 1760
  ]
  edge [
    source 41
    target 1761
  ]
  edge [
    source 41
    target 1762
  ]
  edge [
    source 41
    target 1763
  ]
  edge [
    source 41
    target 1764
  ]
  edge [
    source 41
    target 1765
  ]
  edge [
    source 41
    target 1766
  ]
  edge [
    source 41
    target 1767
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1768
  ]
  edge [
    source 42
    target 1769
  ]
  edge [
    source 42
    target 1563
  ]
  edge [
    source 42
    target 479
  ]
  edge [
    source 42
    target 1770
  ]
  edge [
    source 42
    target 1771
  ]
  edge [
    source 42
    target 1772
  ]
  edge [
    source 42
    target 1773
  ]
  edge [
    source 42
    target 1774
  ]
  edge [
    source 42
    target 1775
  ]
  edge [
    source 42
    target 1776
  ]
  edge [
    source 42
    target 1777
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 1779
  ]
  edge [
    source 42
    target 1780
  ]
  edge [
    source 42
    target 1781
  ]
  edge [
    source 42
    target 1604
  ]
  edge [
    source 42
    target 1782
  ]
  edge [
    source 42
    target 1783
  ]
  edge [
    source 42
    target 1784
  ]
  edge [
    source 42
    target 1785
  ]
  edge [
    source 42
    target 1786
  ]
  edge [
    source 42
    target 1787
  ]
  edge [
    source 42
    target 1788
  ]
  edge [
    source 42
    target 1789
  ]
  edge [
    source 42
    target 1790
  ]
  edge [
    source 42
    target 1791
  ]
  edge [
    source 42
    target 1614
  ]
  edge [
    source 42
    target 1792
  ]
  edge [
    source 42
    target 1037
  ]
  edge [
    source 42
    target 1793
  ]
  edge [
    source 42
    target 1794
  ]
  edge [
    source 42
    target 1795
  ]
  edge [
    source 42
    target 1796
  ]
  edge [
    source 42
    target 1797
  ]
  edge [
    source 42
    target 1798
  ]
  edge [
    source 42
    target 1799
  ]
  edge [
    source 42
    target 1800
  ]
  edge [
    source 42
    target 1801
  ]
  edge [
    source 42
    target 1802
  ]
  edge [
    source 42
    target 1803
  ]
  edge [
    source 42
    target 1804
  ]
  edge [
    source 42
    target 1805
  ]
  edge [
    source 42
    target 1806
  ]
  edge [
    source 42
    target 516
  ]
  edge [
    source 42
    target 523
  ]
  edge [
    source 42
    target 105
  ]
  edge [
    source 42
    target 1807
  ]
  edge [
    source 42
    target 1808
  ]
  edge [
    source 42
    target 1809
  ]
  edge [
    source 42
    target 1810
  ]
  edge [
    source 42
    target 1298
  ]
  edge [
    source 42
    target 1811
  ]
  edge [
    source 42
    target 104
  ]
  edge [
    source 42
    target 252
  ]
  edge [
    source 42
    target 1812
  ]
  edge [
    source 42
    target 1813
  ]
  edge [
    source 42
    target 1814
  ]
  edge [
    source 42
    target 1815
  ]
  edge [
    source 42
    target 1602
  ]
  edge [
    source 42
    target 1594
  ]
  edge [
    source 42
    target 1611
  ]
  edge [
    source 42
    target 499
  ]
  edge [
    source 42
    target 500
  ]
  edge [
    source 42
    target 501
  ]
  edge [
    source 42
    target 502
  ]
  edge [
    source 42
    target 503
  ]
  edge [
    source 42
    target 504
  ]
  edge [
    source 42
    target 505
  ]
  edge [
    source 42
    target 506
  ]
  edge [
    source 42
    target 102
  ]
  edge [
    source 42
    target 298
  ]
  edge [
    source 42
    target 507
  ]
  edge [
    source 42
    target 508
  ]
  edge [
    source 42
    target 509
  ]
  edge [
    source 42
    target 510
  ]
  edge [
    source 42
    target 511
  ]
  edge [
    source 42
    target 512
  ]
  edge [
    source 42
    target 513
  ]
  edge [
    source 42
    target 514
  ]
  edge [
    source 42
    target 515
  ]
  edge [
    source 42
    target 517
  ]
  edge [
    source 42
    target 1816
  ]
  edge [
    source 42
    target 1817
  ]
  edge [
    source 42
    target 1818
  ]
  edge [
    source 42
    target 1819
  ]
  edge [
    source 42
    target 1820
  ]
  edge [
    source 42
    target 1821
  ]
  edge [
    source 42
    target 1822
  ]
  edge [
    source 42
    target 1823
  ]
  edge [
    source 42
    target 821
  ]
  edge [
    source 42
    target 1824
  ]
  edge [
    source 42
    target 1825
  ]
  edge [
    source 42
    target 1826
  ]
  edge [
    source 42
    target 1827
  ]
  edge [
    source 42
    target 1828
  ]
  edge [
    source 42
    target 1829
  ]
  edge [
    source 42
    target 1830
  ]
  edge [
    source 42
    target 1831
  ]
  edge [
    source 42
    target 1832
  ]
  edge [
    source 42
    target 1833
  ]
  edge [
    source 42
    target 529
  ]
  edge [
    source 42
    target 1834
  ]
  edge [
    source 42
    target 1835
  ]
  edge [
    source 42
    target 1836
  ]
  edge [
    source 42
    target 1567
  ]
  edge [
    source 42
    target 1582
  ]
  edge [
    source 42
    target 173
  ]
  edge [
    source 42
    target 1581
  ]
  edge [
    source 42
    target 1588
  ]
  edge [
    source 42
    target 1837
  ]
  edge [
    source 42
    target 1838
  ]
  edge [
    source 42
    target 1839
  ]
  edge [
    source 42
    target 1840
  ]
  edge [
    source 42
    target 1841
  ]
  edge [
    source 42
    target 1842
  ]
  edge [
    source 42
    target 1843
  ]
  edge [
    source 42
    target 1844
  ]
  edge [
    source 42
    target 1845
  ]
  edge [
    source 42
    target 1846
  ]
  edge [
    source 42
    target 1847
  ]
  edge [
    source 42
    target 1848
  ]
  edge [
    source 42
    target 1849
  ]
  edge [
    source 42
    target 1850
  ]
  edge [
    source 42
    target 1851
  ]
  edge [
    source 42
    target 1852
  ]
  edge [
    source 42
    target 1853
  ]
  edge [
    source 42
    target 1854
  ]
  edge [
    source 42
    target 1855
  ]
  edge [
    source 42
    target 1856
  ]
  edge [
    source 42
    target 1857
  ]
  edge [
    source 42
    target 1858
  ]
  edge [
    source 42
    target 1859
  ]
  edge [
    source 42
    target 1860
  ]
  edge [
    source 42
    target 1861
  ]
  edge [
    source 42
    target 1862
  ]
  edge [
    source 42
    target 531
  ]
  edge [
    source 42
    target 1863
  ]
  edge [
    source 42
    target 1864
  ]
  edge [
    source 42
    target 1865
  ]
  edge [
    source 42
    target 1866
  ]
  edge [
    source 42
    target 1867
  ]
  edge [
    source 42
    target 1868
  ]
  edge [
    source 42
    target 872
  ]
  edge [
    source 42
    target 1617
  ]
  edge [
    source 42
    target 1869
  ]
  edge [
    source 42
    target 1870
  ]
  edge [
    source 42
    target 1871
  ]
  edge [
    source 42
    target 1872
  ]
  edge [
    source 42
    target 1873
  ]
  edge [
    source 42
    target 1571
  ]
  edge [
    source 42
    target 1874
  ]
  edge [
    source 42
    target 1875
  ]
  edge [
    source 42
    target 1876
  ]
  edge [
    source 42
    target 1687
  ]
  edge [
    source 42
    target 1877
  ]
  edge [
    source 42
    target 1878
  ]
  edge [
    source 42
    target 1879
  ]
  edge [
    source 42
    target 1880
  ]
  edge [
    source 42
    target 1881
  ]
  edge [
    source 42
    target 1882
  ]
  edge [
    source 42
    target 1883
  ]
  edge [
    source 42
    target 1884
  ]
  edge [
    source 42
    target 1885
  ]
  edge [
    source 42
    target 829
  ]
  edge [
    source 42
    target 1886
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1887
  ]
  edge [
    source 43
    target 1888
  ]
  edge [
    source 43
    target 1889
  ]
  edge [
    source 43
    target 1890
  ]
  edge [
    source 43
    target 1891
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1892
  ]
  edge [
    source 44
    target 1893
  ]
  edge [
    source 44
    target 547
  ]
  edge [
    source 44
    target 1131
  ]
  edge [
    source 44
    target 1894
  ]
  edge [
    source 44
    target 1895
  ]
  edge [
    source 44
    target 1896
  ]
  edge [
    source 44
    target 1531
  ]
  edge [
    source 44
    target 1897
  ]
  edge [
    source 44
    target 1898
  ]
  edge [
    source 44
    target 1899
  ]
  edge [
    source 44
    target 1900
  ]
  edge [
    source 44
    target 1901
  ]
  edge [
    source 44
    target 1902
  ]
  edge [
    source 44
    target 1903
  ]
  edge [
    source 44
    target 1904
  ]
  edge [
    source 44
    target 1905
  ]
  edge [
    source 44
    target 244
  ]
  edge [
    source 44
    target 1906
  ]
  edge [
    source 44
    target 1907
  ]
  edge [
    source 44
    target 1908
  ]
  edge [
    source 44
    target 1909
  ]
  edge [
    source 44
    target 1910
  ]
  edge [
    source 44
    target 1911
  ]
  edge [
    source 44
    target 1912
  ]
  edge [
    source 44
    target 1913
  ]
  edge [
    source 44
    target 1914
  ]
  edge [
    source 44
    target 1915
  ]
  edge [
    source 44
    target 1916
  ]
  edge [
    source 44
    target 404
  ]
  edge [
    source 44
    target 1917
  ]
  edge [
    source 44
    target 1918
  ]
  edge [
    source 44
    target 1919
  ]
  edge [
    source 44
    target 1920
  ]
  edge [
    source 44
    target 1921
  ]
  edge [
    source 44
    target 1922
  ]
  edge [
    source 44
    target 1923
  ]
  edge [
    source 44
    target 1924
  ]
  edge [
    source 44
    target 669
  ]
  edge [
    source 44
    target 1925
  ]
  edge [
    source 44
    target 1926
  ]
  edge [
    source 44
    target 1927
  ]
  edge [
    source 44
    target 1928
  ]
  edge [
    source 44
    target 1929
  ]
  edge [
    source 44
    target 1930
  ]
  edge [
    source 44
    target 1931
  ]
  edge [
    source 44
    target 1932
  ]
  edge [
    source 44
    target 1069
  ]
  edge [
    source 44
    target 1933
  ]
  edge [
    source 44
    target 1934
  ]
  edge [
    source 44
    target 1935
  ]
  edge [
    source 44
    target 1936
  ]
  edge [
    source 44
    target 750
  ]
  edge [
    source 44
    target 1937
  ]
  edge [
    source 44
    target 1938
  ]
  edge [
    source 44
    target 753
  ]
  edge [
    source 44
    target 1939
  ]
  edge [
    source 44
    target 1029
  ]
  edge [
    source 44
    target 1940
  ]
  edge [
    source 44
    target 1065
  ]
  edge [
    source 44
    target 1460
  ]
  edge [
    source 44
    target 1461
  ]
  edge [
    source 44
    target 1462
  ]
  edge [
    source 44
    target 1463
  ]
  edge [
    source 44
    target 1464
  ]
  edge [
    source 44
    target 1465
  ]
  edge [
    source 44
    target 1941
  ]
  edge [
    source 44
    target 1942
  ]
  edge [
    source 44
    target 1943
  ]
  edge [
    source 44
    target 1944
  ]
  edge [
    source 44
    target 919
  ]
  edge [
    source 44
    target 960
  ]
  edge [
    source 44
    target 1945
  ]
  edge [
    source 44
    target 536
  ]
  edge [
    source 44
    target 922
  ]
  edge [
    source 44
    target 1946
  ]
  edge [
    source 44
    target 1762
  ]
  edge [
    source 44
    target 146
  ]
  edge [
    source 44
    target 928
  ]
  edge [
    source 44
    target 927
  ]
  edge [
    source 44
    target 926
  ]
  edge [
    source 44
    target 1947
  ]
  edge [
    source 44
    target 1948
  ]
  edge [
    source 44
    target 930
  ]
  edge [
    source 44
    target 932
  ]
  edge [
    source 44
    target 1949
  ]
  edge [
    source 44
    target 933
  ]
  edge [
    source 44
    target 1950
  ]
  edge [
    source 44
    target 1951
  ]
  edge [
    source 44
    target 936
  ]
  edge [
    source 44
    target 966
  ]
  edge [
    source 44
    target 967
  ]
  edge [
    source 44
    target 940
  ]
  edge [
    source 44
    target 968
  ]
  edge [
    source 44
    target 943
  ]
  edge [
    source 44
    target 944
  ]
  edge [
    source 44
    target 945
  ]
  edge [
    source 44
    target 1952
  ]
  edge [
    source 44
    target 970
  ]
  edge [
    source 44
    target 971
  ]
  edge [
    source 44
    target 949
  ]
  edge [
    source 44
    target 391
  ]
  edge [
    source 44
    target 1953
  ]
  edge [
    source 44
    target 974
  ]
  edge [
    source 44
    target 977
  ]
  edge [
    source 44
    target 973
  ]
  edge [
    source 44
    target 1954
  ]
  edge [
    source 44
    target 979
  ]
  edge [
    source 44
    target 1955
  ]
  edge [
    source 44
    target 1756
  ]
  edge [
    source 44
    target 1956
  ]
  edge [
    source 44
    target 1957
  ]
  edge [
    source 44
    target 1958
  ]
  edge [
    source 44
    target 1959
  ]
  edge [
    source 44
    target 1960
  ]
  edge [
    source 44
    target 1961
  ]
  edge [
    source 44
    target 1962
  ]
  edge [
    source 44
    target 1963
  ]
  edge [
    source 44
    target 1964
  ]
  edge [
    source 44
    target 1965
  ]
  edge [
    source 44
    target 735
  ]
  edge [
    source 44
    target 1966
  ]
  edge [
    source 44
    target 1967
  ]
  edge [
    source 44
    target 1968
  ]
  edge [
    source 44
    target 88
  ]
  edge [
    source 44
    target 143
  ]
  edge [
    source 44
    target 158
  ]
  edge [
    source 44
    target 1969
  ]
  edge [
    source 44
    target 1970
  ]
  edge [
    source 44
    target 1472
  ]
  edge [
    source 44
    target 1971
  ]
  edge [
    source 44
    target 1972
  ]
  edge [
    source 44
    target 1973
  ]
  edge [
    source 44
    target 1974
  ]
  edge [
    source 44
    target 1975
  ]
  edge [
    source 44
    target 1976
  ]
  edge [
    source 44
    target 1977
  ]
  edge [
    source 44
    target 806
  ]
  edge [
    source 44
    target 800
  ]
  edge [
    source 44
    target 787
  ]
  edge [
    source 44
    target 385
  ]
  edge [
    source 44
    target 1766
  ]
  edge [
    source 44
    target 1978
  ]
  edge [
    source 44
    target 1979
  ]
  edge [
    source 44
    target 1980
  ]
  edge [
    source 44
    target 1981
  ]
  edge [
    source 44
    target 1982
  ]
  edge [
    source 44
    target 1983
  ]
  edge [
    source 44
    target 1984
  ]
  edge [
    source 44
    target 1985
  ]
  edge [
    source 44
    target 1986
  ]
  edge [
    source 44
    target 1987
  ]
  edge [
    source 44
    target 1988
  ]
  edge [
    source 44
    target 1989
  ]
  edge [
    source 44
    target 1990
  ]
  edge [
    source 44
    target 289
  ]
  edge [
    source 44
    target 1991
  ]
  edge [
    source 44
    target 1992
  ]
  edge [
    source 44
    target 1993
  ]
  edge [
    source 44
    target 1994
  ]
  edge [
    source 44
    target 1995
  ]
  edge [
    source 44
    target 1996
  ]
  edge [
    source 44
    target 82
  ]
  edge [
    source 44
    target 1997
  ]
  edge [
    source 44
    target 1998
  ]
  edge [
    source 44
    target 1999
  ]
  edge [
    source 44
    target 688
  ]
  edge [
    source 44
    target 2000
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1489
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 2001
  ]
  edge [
    source 47
    target 2002
  ]
  edge [
    source 47
    target 2003
  ]
  edge [
    source 47
    target 2004
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 2005
  ]
  edge [
    source 49
    target 2006
  ]
  edge [
    source 49
    target 1041
  ]
  edge [
    source 49
    target 2007
  ]
  edge [
    source 49
    target 2008
  ]
  edge [
    source 49
    target 2009
  ]
  edge [
    source 49
    target 2010
  ]
  edge [
    source 49
    target 2011
  ]
  edge [
    source 49
    target 2012
  ]
  edge [
    source 49
    target 2013
  ]
  edge [
    source 49
    target 2014
  ]
  edge [
    source 49
    target 2015
  ]
  edge [
    source 49
    target 2016
  ]
  edge [
    source 49
    target 2017
  ]
  edge [
    source 49
    target 484
  ]
  edge [
    source 49
    target 518
  ]
  edge [
    source 49
    target 2018
  ]
  edge [
    source 49
    target 2019
  ]
  edge [
    source 49
    target 2020
  ]
  edge [
    source 49
    target 2021
  ]
  edge [
    source 49
    target 2022
  ]
  edge [
    source 49
    target 2023
  ]
  edge [
    source 49
    target 2024
  ]
  edge [
    source 49
    target 2025
  ]
  edge [
    source 49
    target 338
  ]
  edge [
    source 49
    target 2026
  ]
  edge [
    source 49
    target 2027
  ]
  edge [
    source 49
    target 2028
  ]
  edge [
    source 49
    target 2029
  ]
  edge [
    source 49
    target 559
  ]
  edge [
    source 49
    target 2030
  ]
  edge [
    source 49
    target 2031
  ]
  edge [
    source 49
    target 2032
  ]
  edge [
    source 49
    target 252
  ]
  edge [
    source 49
    target 2033
  ]
  edge [
    source 49
    target 2034
  ]
  edge [
    source 49
    target 2035
  ]
  edge [
    source 49
    target 2036
  ]
  edge [
    source 49
    target 2037
  ]
  edge [
    source 49
    target 2038
  ]
  edge [
    source 49
    target 2039
  ]
  edge [
    source 49
    target 2040
  ]
  edge [
    source 49
    target 2041
  ]
  edge [
    source 49
    target 1391
  ]
  edge [
    source 49
    target 2042
  ]
  edge [
    source 49
    target 2043
  ]
  edge [
    source 49
    target 1416
  ]
  edge [
    source 49
    target 2044
  ]
  edge [
    source 49
    target 2045
  ]
  edge [
    source 49
    target 2046
  ]
  edge [
    source 49
    target 2047
  ]
  edge [
    source 49
    target 2048
  ]
  edge [
    source 49
    target 2049
  ]
  edge [
    source 49
    target 2050
  ]
  edge [
    source 49
    target 548
  ]
  edge [
    source 49
    target 304
  ]
  edge [
    source 49
    target 550
  ]
  edge [
    source 49
    target 551
  ]
  edge [
    source 49
    target 552
  ]
  edge [
    source 49
    target 296
  ]
  edge [
    source 49
    target 554
  ]
  edge [
    source 49
    target 553
  ]
  edge [
    source 49
    target 516
  ]
  edge [
    source 49
    target 549
  ]
  edge [
    source 49
    target 555
  ]
  edge [
    source 49
    target 556
  ]
  edge [
    source 49
    target 557
  ]
  edge [
    source 49
    target 543
  ]
  edge [
    source 49
    target 2051
  ]
  edge [
    source 49
    target 2052
  ]
  edge [
    source 49
    target 2053
  ]
  edge [
    source 49
    target 2054
  ]
  edge [
    source 49
    target 2055
  ]
  edge [
    source 49
    target 235
  ]
  edge [
    source 49
    target 2056
  ]
  edge [
    source 49
    target 2057
  ]
  edge [
    source 49
    target 1769
  ]
  edge [
    source 49
    target 2058
  ]
  edge [
    source 49
    target 2059
  ]
  edge [
    source 49
    target 2060
  ]
  edge [
    source 49
    target 1223
  ]
  edge [
    source 49
    target 2061
  ]
  edge [
    source 49
    target 2062
  ]
  edge [
    source 49
    target 2063
  ]
  edge [
    source 49
    target 2064
  ]
  edge [
    source 49
    target 2065
  ]
  edge [
    source 49
    target 2066
  ]
  edge [
    source 49
    target 2067
  ]
  edge [
    source 49
    target 2068
  ]
  edge [
    source 49
    target 2069
  ]
  edge [
    source 49
    target 2070
  ]
  edge [
    source 49
    target 1397
  ]
  edge [
    source 49
    target 2071
  ]
  edge [
    source 49
    target 2072
  ]
  edge [
    source 49
    target 2073
  ]
  edge [
    source 49
    target 2074
  ]
  edge [
    source 49
    target 1727
  ]
  edge [
    source 49
    target 2075
  ]
  edge [
    source 49
    target 2076
  ]
  edge [
    source 49
    target 2077
  ]
  edge [
    source 49
    target 2078
  ]
  edge [
    source 49
    target 2079
  ]
  edge [
    source 49
    target 2080
  ]
  edge [
    source 49
    target 2081
  ]
  edge [
    source 49
    target 2082
  ]
  edge [
    source 49
    target 977
  ]
  edge [
    source 49
    target 2083
  ]
  edge [
    source 49
    target 2084
  ]
  edge [
    source 49
    target 2085
  ]
  edge [
    source 49
    target 2086
  ]
  edge [
    source 49
    target 2087
  ]
  edge [
    source 49
    target 2088
  ]
  edge [
    source 49
    target 2089
  ]
  edge [
    source 49
    target 2090
  ]
  edge [
    source 49
    target 2091
  ]
  edge [
    source 49
    target 2092
  ]
  edge [
    source 49
    target 479
  ]
  edge [
    source 49
    target 1097
  ]
  edge [
    source 49
    target 2093
  ]
  edge [
    source 49
    target 2094
  ]
  edge [
    source 49
    target 2095
  ]
  edge [
    source 49
    target 1541
  ]
  edge [
    source 49
    target 2096
  ]
  edge [
    source 49
    target 2097
  ]
  edge [
    source 49
    target 2098
  ]
  edge [
    source 49
    target 2099
  ]
  edge [
    source 49
    target 508
  ]
  edge [
    source 49
    target 670
  ]
  edge [
    source 49
    target 2100
  ]
  edge [
    source 49
    target 2101
  ]
  edge [
    source 49
    target 2102
  ]
  edge [
    source 49
    target 2103
  ]
  edge [
    source 49
    target 2104
  ]
  edge [
    source 49
    target 2105
  ]
  edge [
    source 49
    target 2106
  ]
  edge [
    source 49
    target 2107
  ]
  edge [
    source 49
    target 2108
  ]
  edge [
    source 49
    target 1037
  ]
  edge [
    source 49
    target 2109
  ]
  edge [
    source 49
    target 2110
  ]
  edge [
    source 49
    target 2111
  ]
  edge [
    source 49
    target 2112
  ]
  edge [
    source 49
    target 2113
  ]
  edge [
    source 49
    target 2114
  ]
  edge [
    source 49
    target 2115
  ]
  edge [
    source 49
    target 2116
  ]
  edge [
    source 49
    target 2117
  ]
  edge [
    source 49
    target 558
  ]
  edge [
    source 49
    target 2118
  ]
  edge [
    source 49
    target 560
  ]
  edge [
    source 49
    target 2119
  ]
  edge [
    source 49
    target 2120
  ]
  edge [
    source 49
    target 563
  ]
  edge [
    source 49
    target 2121
  ]
  edge [
    source 49
    target 2122
  ]
  edge [
    source 49
    target 2123
  ]
  edge [
    source 49
    target 686
  ]
  edge [
    source 49
    target 2124
  ]
  edge [
    source 49
    target 2125
  ]
  edge [
    source 49
    target 2126
  ]
  edge [
    source 49
    target 2127
  ]
  edge [
    source 49
    target 562
  ]
  edge [
    source 49
    target 2128
  ]
  edge [
    source 49
    target 2129
  ]
  edge [
    source 49
    target 2130
  ]
  edge [
    source 49
    target 631
  ]
  edge [
    source 49
    target 2131
  ]
  edge [
    source 49
    target 2132
  ]
  edge [
    source 49
    target 2133
  ]
  edge [
    source 49
    target 2134
  ]
  edge [
    source 49
    target 2135
  ]
  edge [
    source 49
    target 2136
  ]
  edge [
    source 49
    target 2137
  ]
  edge [
    source 49
    target 2138
  ]
  edge [
    source 49
    target 1976
  ]
  edge [
    source 49
    target 2139
  ]
  edge [
    source 49
    target 2140
  ]
  edge [
    source 49
    target 2141
  ]
  edge [
    source 49
    target 2142
  ]
  edge [
    source 49
    target 2143
  ]
  edge [
    source 49
    target 2144
  ]
  edge [
    source 49
    target 2145
  ]
  edge [
    source 49
    target 2146
  ]
  edge [
    source 49
    target 2147
  ]
  edge [
    source 49
    target 2148
  ]
  edge [
    source 49
    target 2149
  ]
  edge [
    source 49
    target 2150
  ]
  edge [
    source 49
    target 2151
  ]
  edge [
    source 49
    target 2152
  ]
  edge [
    source 49
    target 2153
  ]
  edge [
    source 49
    target 2154
  ]
  edge [
    source 49
    target 1401
  ]
  edge [
    source 49
    target 1404
  ]
  edge [
    source 49
    target 2155
  ]
  edge [
    source 49
    target 2156
  ]
  edge [
    source 49
    target 2157
  ]
  edge [
    source 49
    target 2158
  ]
  edge [
    source 49
    target 2159
  ]
  edge [
    source 49
    target 2160
  ]
  edge [
    source 49
    target 2161
  ]
  edge [
    source 49
    target 1925
  ]
  edge [
    source 49
    target 656
  ]
  edge [
    source 49
    target 2162
  ]
  edge [
    source 49
    target 527
  ]
  edge [
    source 49
    target 2163
  ]
  edge [
    source 49
    target 2164
  ]
  edge [
    source 49
    target 2165
  ]
  edge [
    source 49
    target 2166
  ]
  edge [
    source 49
    target 2167
  ]
  edge [
    source 49
    target 2168
  ]
  edge [
    source 49
    target 2169
  ]
  edge [
    source 49
    target 2170
  ]
  edge [
    source 49
    target 2171
  ]
  edge [
    source 49
    target 2172
  ]
  edge [
    source 49
    target 2173
  ]
  edge [
    source 49
    target 2174
  ]
  edge [
    source 49
    target 2175
  ]
  edge [
    source 49
    target 2176
  ]
  edge [
    source 49
    target 2177
  ]
  edge [
    source 49
    target 321
  ]
  edge [
    source 49
    target 2178
  ]
  edge [
    source 49
    target 2179
  ]
  edge [
    source 49
    target 2180
  ]
  edge [
    source 49
    target 539
  ]
  edge [
    source 49
    target 2181
  ]
  edge [
    source 49
    target 2182
  ]
  edge [
    source 49
    target 2183
  ]
  edge [
    source 49
    target 2184
  ]
  edge [
    source 49
    target 2185
  ]
  edge [
    source 49
    target 2186
  ]
  edge [
    source 49
    target 2187
  ]
  edge [
    source 49
    target 2188
  ]
  edge [
    source 49
    target 2189
  ]
  edge [
    source 49
    target 593
  ]
  edge [
    source 49
    target 1726
  ]
  edge [
    source 49
    target 359
  ]
  edge [
    source 49
    target 2190
  ]
  edge [
    source 49
    target 2191
  ]
  edge [
    source 49
    target 2192
  ]
  edge [
    source 49
    target 2193
  ]
  edge [
    source 49
    target 2194
  ]
  edge [
    source 49
    target 2195
  ]
  edge [
    source 49
    target 2196
  ]
  edge [
    source 49
    target 2197
  ]
  edge [
    source 49
    target 2198
  ]
  edge [
    source 49
    target 653
  ]
  edge [
    source 49
    target 2199
  ]
  edge [
    source 49
    target 2200
  ]
  edge [
    source 49
    target 2201
  ]
  edge [
    source 49
    target 2202
  ]
  edge [
    source 49
    target 2203
  ]
  edge [
    source 49
    target 2204
  ]
  edge [
    source 49
    target 2205
  ]
  edge [
    source 49
    target 2206
  ]
  edge [
    source 49
    target 2207
  ]
  edge [
    source 49
    target 2208
  ]
  edge [
    source 49
    target 2209
  ]
  edge [
    source 49
    target 2210
  ]
  edge [
    source 49
    target 649
  ]
  edge [
    source 49
    target 2211
  ]
  edge [
    source 49
    target 369
  ]
  edge [
    source 49
    target 2212
  ]
  edge [
    source 49
    target 613
  ]
  edge [
    source 49
    target 2213
  ]
  edge [
    source 49
    target 2214
  ]
  edge [
    source 49
    target 2215
  ]
  edge [
    source 49
    target 2216
  ]
  edge [
    source 49
    target 2217
  ]
  edge [
    source 49
    target 2218
  ]
  edge [
    source 49
    target 2219
  ]
  edge [
    source 49
    target 2220
  ]
  edge [
    source 49
    target 180
  ]
  edge [
    source 49
    target 2221
  ]
  edge [
    source 49
    target 2222
  ]
  edge [
    source 49
    target 2223
  ]
  edge [
    source 49
    target 2224
  ]
  edge [
    source 49
    target 2225
  ]
  edge [
    source 49
    target 2226
  ]
  edge [
    source 49
    target 688
  ]
  edge [
    source 49
    target 2227
  ]
  edge [
    source 49
    target 2228
  ]
  edge [
    source 49
    target 2229
  ]
  edge [
    source 49
    target 2230
  ]
  edge [
    source 49
    target 2231
  ]
  edge [
    source 49
    target 2232
  ]
  edge [
    source 49
    target 2233
  ]
  edge [
    source 49
    target 2234
  ]
  edge [
    source 49
    target 1513
  ]
  edge [
    source 49
    target 2235
  ]
  edge [
    source 49
    target 2236
  ]
  edge [
    source 49
    target 2237
  ]
  edge [
    source 49
    target 2238
  ]
  edge [
    source 49
    target 2239
  ]
  edge [
    source 49
    target 2240
  ]
  edge [
    source 49
    target 2241
  ]
  edge [
    source 49
    target 2242
  ]
  edge [
    source 49
    target 1622
  ]
  edge [
    source 49
    target 2243
  ]
  edge [
    source 49
    target 2244
  ]
  edge [
    source 49
    target 1628
  ]
  edge [
    source 49
    target 2245
  ]
  edge [
    source 49
    target 2246
  ]
  edge [
    source 49
    target 2247
  ]
  edge [
    source 49
    target 2248
  ]
  edge [
    source 49
    target 2249
  ]
  edge [
    source 49
    target 2250
  ]
  edge [
    source 49
    target 2251
  ]
  edge [
    source 49
    target 2252
  ]
  edge [
    source 49
    target 2253
  ]
  edge [
    source 49
    target 2254
  ]
  edge [
    source 49
    target 2255
  ]
  edge [
    source 49
    target 2256
  ]
  edge [
    source 49
    target 2257
  ]
  edge [
    source 49
    target 377
  ]
  edge [
    source 49
    target 2258
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 2259
  ]
  edge [
    source 50
    target 2260
  ]
  edge [
    source 50
    target 2261
  ]
  edge [
    source 50
    target 2262
  ]
  edge [
    source 50
    target 2263
  ]
  edge [
    source 50
    target 2264
  ]
  edge [
    source 50
    target 2265
  ]
  edge [
    source 50
    target 2266
  ]
  edge [
    source 50
    target 2267
  ]
  edge [
    source 50
    target 2268
  ]
  edge [
    source 50
    target 2269
  ]
  edge [
    source 50
    target 2270
  ]
  edge [
    source 50
    target 1281
  ]
  edge [
    source 50
    target 2271
  ]
  edge [
    source 50
    target 2272
  ]
  edge [
    source 50
    target 726
  ]
  edge [
    source 50
    target 2273
  ]
  edge [
    source 50
    target 2087
  ]
  edge [
    source 50
    target 2274
  ]
  edge [
    source 50
    target 642
  ]
  edge [
    source 50
    target 2275
  ]
  edge [
    source 50
    target 2276
  ]
  edge [
    source 50
    target 2277
  ]
  edge [
    source 50
    target 2278
  ]
  edge [
    source 50
    target 2279
  ]
  edge [
    source 50
    target 1542
  ]
  edge [
    source 50
    target 2280
  ]
  edge [
    source 50
    target 942
  ]
  edge [
    source 50
    target 1755
  ]
  edge [
    source 50
    target 2281
  ]
  edge [
    source 50
    target 2282
  ]
  edge [
    source 50
    target 943
  ]
  edge [
    source 50
    target 2283
  ]
  edge [
    source 50
    target 2284
  ]
  edge [
    source 50
    target 2285
  ]
  edge [
    source 50
    target 1116
  ]
  edge [
    source 50
    target 2286
  ]
  edge [
    source 50
    target 2287
  ]
  edge [
    source 50
    target 2288
  ]
  edge [
    source 50
    target 950
  ]
  edge [
    source 50
    target 2289
  ]
  edge [
    source 50
    target 2290
  ]
  edge [
    source 50
    target 1756
  ]
  edge [
    source 50
    target 293
  ]
  edge [
    source 50
    target 2291
  ]
  edge [
    source 50
    target 88
  ]
  edge [
    source 50
    target 2292
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 2293
  ]
  edge [
    source 51
    target 2294
  ]
  edge [
    source 51
    target 146
  ]
  edge [
    source 51
    target 2295
  ]
  edge [
    source 51
    target 2296
  ]
  edge [
    source 51
    target 2297
  ]
  edge [
    source 51
    target 1274
  ]
  edge [
    source 51
    target 951
  ]
  edge [
    source 51
    target 2298
  ]
  edge [
    source 51
    target 2299
  ]
  edge [
    source 51
    target 2300
  ]
  edge [
    source 51
    target 2301
  ]
  edge [
    source 51
    target 2302
  ]
  edge [
    source 51
    target 1173
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 2303
  ]
  edge [
    source 54
    target 2304
  ]
  edge [
    source 54
    target 2305
  ]
  edge [
    source 54
    target 2306
  ]
  edge [
    source 54
    target 2307
  ]
  edge [
    source 54
    target 2308
  ]
  edge [
    source 54
    target 2309
  ]
  edge [
    source 54
    target 2310
  ]
  edge [
    source 54
    target 2311
  ]
  edge [
    source 54
    target 2312
  ]
  edge [
    source 54
    target 2313
  ]
  edge [
    source 54
    target 2314
  ]
  edge [
    source 54
    target 1778
  ]
  edge [
    source 54
    target 107
  ]
  edge [
    source 54
    target 2315
  ]
  edge [
    source 54
    target 1496
  ]
  edge [
    source 54
    target 2316
  ]
  edge [
    source 54
    target 2317
  ]
  edge [
    source 54
    target 2318
  ]
  edge [
    source 54
    target 2319
  ]
  edge [
    source 54
    target 2320
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 2321
  ]
  edge [
    source 55
    target 2322
  ]
  edge [
    source 55
    target 2323
  ]
  edge [
    source 55
    target 2324
  ]
  edge [
    source 55
    target 1493
  ]
  edge [
    source 55
    target 2325
  ]
  edge [
    source 55
    target 426
  ]
  edge [
    source 55
    target 2326
  ]
  edge [
    source 55
    target 1497
  ]
  edge [
    source 55
    target 2327
  ]
  edge [
    source 55
    target 2328
  ]
  edge [
    source 55
    target 2329
  ]
  edge [
    source 55
    target 2330
  ]
  edge [
    source 55
    target 2331
  ]
  edge [
    source 55
    target 2071
  ]
  edge [
    source 55
    target 2332
  ]
  edge [
    source 55
    target 2333
  ]
  edge [
    source 55
    target 2334
  ]
  edge [
    source 55
    target 502
  ]
  edge [
    source 55
    target 2335
  ]
  edge [
    source 55
    target 2336
  ]
  edge [
    source 55
    target 1494
  ]
  edge [
    source 55
    target 2337
  ]
  edge [
    source 55
    target 2338
  ]
  edge [
    source 55
    target 2339
  ]
  edge [
    source 55
    target 2340
  ]
  edge [
    source 55
    target 2341
  ]
  edge [
    source 55
    target 2342
  ]
  edge [
    source 55
    target 2343
  ]
  edge [
    source 55
    target 2344
  ]
  edge [
    source 55
    target 2345
  ]
  edge [
    source 55
    target 2346
  ]
  edge [
    source 55
    target 2347
  ]
  edge [
    source 55
    target 2348
  ]
  edge [
    source 55
    target 1917
  ]
  edge [
    source 55
    target 2349
  ]
  edge [
    source 55
    target 2350
  ]
  edge [
    source 55
    target 2351
  ]
  edge [
    source 55
    target 353
  ]
  edge [
    source 55
    target 2352
  ]
  edge [
    source 55
    target 1491
  ]
  edge [
    source 55
    target 2353
  ]
  edge [
    source 55
    target 2354
  ]
  edge [
    source 55
    target 1490
  ]
  edge [
    source 55
    target 2355
  ]
  edge [
    source 55
    target 2356
  ]
  edge [
    source 55
    target 107
  ]
  edge [
    source 55
    target 1496
  ]
  edge [
    source 55
    target 2357
  ]
  edge [
    source 55
    target 2358
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 2359
  ]
  edge [
    source 56
    target 2360
  ]
  edge [
    source 57
    target 2361
  ]
  edge [
    source 57
    target 1665
  ]
  edge [
    source 57
    target 2362
  ]
  edge [
    source 57
    target 2363
  ]
  edge [
    source 57
    target 2364
  ]
  edge [
    source 57
    target 2365
  ]
  edge [
    source 57
    target 2366
  ]
  edge [
    source 57
    target 1709
  ]
  edge [
    source 57
    target 2367
  ]
  edge [
    source 57
    target 2368
  ]
]
