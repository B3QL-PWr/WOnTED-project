graph [
  node [
    id 0
    label "luigi"
    origin "text"
  ]
  node [
    id 1
    label "scala&#263;"
    origin "text"
  ]
  node [
    id 2
    label "consort"
  ]
  node [
    id 3
    label "jednoczy&#263;"
  ]
  node [
    id 4
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 5
    label "robi&#263;"
  ]
  node [
    id 6
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 7
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 8
    label "powodowa&#263;"
  ]
  node [
    id 9
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 10
    label "relate"
  ]
  node [
    id 11
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 12
    label "Luigi"
  ]
  node [
    id 13
    label "Vico"
  ]
  node [
    id 14
    label "Equense"
  ]
  node [
    id 15
    label "mistrzostwo"
  ]
  node [
    id 16
    label "&#347;wiat"
  ]
  node [
    id 17
    label "&#8211;"
  ]
  node [
    id 18
    label "lucerna"
  ]
  node [
    id 19
    label "2001"
  ]
  node [
    id 20
    label "Sewilla"
  ]
  node [
    id 21
    label "2002"
  ]
  node [
    id 22
    label "Mediolan"
  ]
  node [
    id 23
    label "2003"
  ]
  node [
    id 24
    label "Gifu"
  ]
  node [
    id 25
    label "2005"
  ]
  node [
    id 26
    label "Eton"
  ]
  node [
    id 27
    label "2006"
  ]
  node [
    id 28
    label "Monachium"
  ]
  node [
    id 29
    label "2007"
  ]
  node [
    id 30
    label "poznanie"
  ]
  node [
    id 31
    label "2009"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 26
  ]
  edge [
    source 15
    target 27
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 30
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 29
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
]
