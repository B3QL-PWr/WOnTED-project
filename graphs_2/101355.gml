graph [
  node [
    id 0
    label "newgrange"
    origin "text"
  ]
  node [
    id 1
    label "pa&#322;ac"
  ]
  node [
    id 2
    label "nad"
  ]
  node [
    id 3
    label "Boyne"
  ]
  node [
    id 4
    label "Br&#250;"
  ]
  node [
    id 5
    label "na"
  ]
  node [
    id 6
    label "B&#243;inne"
  ]
  node [
    id 7
    label "D&#250;n"
  ]
  node [
    id 8
    label "Fhearghusa"
  ]
  node [
    id 9
    label "wielki"
  ]
  node [
    id 10
    label "piramida"
  ]
  node [
    id 11
    label "Brian"
  ]
  node [
    id 12
    label "OKellyego"
  ]
  node [
    id 13
    label "instytut"
  ]
  node [
    id 14
    label "archeologia"
  ]
  node [
    id 15
    label "college"
  ]
  node [
    id 16
    label "University"
  ]
  node [
    id 17
    label "National"
  ]
  node [
    id 18
    label "of"
  ]
  node [
    id 19
    label "Ireland"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
]
