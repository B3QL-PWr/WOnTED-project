graph [
  node [
    id 0
    label "awaria"
    origin "text"
  ]
  node [
    id 1
    label "pompa"
    origin "text"
  ]
  node [
    id 2
    label "hydrauliczny"
    origin "text"
  ]
  node [
    id 3
    label "gridfin&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "lotka"
    origin "text"
  ]
  node [
    id 5
    label "ostatni"
    origin "text"
  ]
  node [
    id 6
    label "faza"
    origin "text"
  ]
  node [
    id 7
    label "l&#261;dowanie"
    origin "text"
  ]
  node [
    id 8
    label "silnik"
    origin "text"
  ]
  node [
    id 9
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 10
    label "ustabilizowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "rakieta"
    origin "text"
  ]
  node [
    id 12
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 13
    label "zatrzymanie"
    origin "text"
  ]
  node [
    id 14
    label "obr&#243;t"
    origin "text"
  ]
  node [
    id 15
    label "katapultowa&#263;"
  ]
  node [
    id 16
    label "failure"
  ]
  node [
    id 17
    label "katapultowanie"
  ]
  node [
    id 18
    label "wydarzenie"
  ]
  node [
    id 19
    label "przebiec"
  ]
  node [
    id 20
    label "charakter"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 23
    label "motyw"
  ]
  node [
    id 24
    label "przebiegni&#281;cie"
  ]
  node [
    id 25
    label "fabu&#322;a"
  ]
  node [
    id 26
    label "wyrzucenie"
  ]
  node [
    id 27
    label "wylatywanie"
  ]
  node [
    id 28
    label "wyrzucanie"
  ]
  node [
    id 29
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 30
    label "katapultowanie_si&#281;"
  ]
  node [
    id 31
    label "wyrzuca&#263;"
  ]
  node [
    id 32
    label "wyrzuci&#263;"
  ]
  node [
    id 33
    label "wylatywa&#263;"
  ]
  node [
    id 34
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 35
    label "rozmach"
  ]
  node [
    id 36
    label "maszyna_hydrauliczna"
  ]
  node [
    id 37
    label "nurnik"
  ]
  node [
    id 38
    label "przepompownia"
  ]
  node [
    id 39
    label "ulewa"
  ]
  node [
    id 40
    label "smok"
  ]
  node [
    id 41
    label "precipitation"
  ]
  node [
    id 42
    label "deszcz"
  ]
  node [
    id 43
    label "atmosfera"
  ]
  node [
    id 44
    label "patos"
  ]
  node [
    id 45
    label "egzaltacja"
  ]
  node [
    id 46
    label "cecha"
  ]
  node [
    id 47
    label "energia"
  ]
  node [
    id 48
    label "wyobra&#378;nia"
  ]
  node [
    id 49
    label "&#322;apczywiec"
  ]
  node [
    id 50
    label "smok_wawelski"
  ]
  node [
    id 51
    label "po&#380;eracz"
  ]
  node [
    id 52
    label "urwis"
  ]
  node [
    id 53
    label "dragon"
  ]
  node [
    id 54
    label "sito"
  ]
  node [
    id 55
    label "benzyniak"
  ]
  node [
    id 56
    label "brzydula"
  ]
  node [
    id 57
    label "potw&#243;r"
  ]
  node [
    id 58
    label "ptak_wodny"
  ]
  node [
    id 59
    label "alki"
  ]
  node [
    id 60
    label "t&#322;ok"
  ]
  node [
    id 61
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 62
    label "hydraulicznie"
  ]
  node [
    id 63
    label "pi&#243;ro"
  ]
  node [
    id 64
    label "sterolotka"
  ]
  node [
    id 65
    label "skrzyd&#322;o"
  ]
  node [
    id 66
    label "badminton"
  ]
  node [
    id 67
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 68
    label "rekwizyt_do_gry"
  ]
  node [
    id 69
    label "stal&#243;wka"
  ]
  node [
    id 70
    label "wyrostek"
  ]
  node [
    id 71
    label "stylo"
  ]
  node [
    id 72
    label "przybory_do_pisania"
  ]
  node [
    id 73
    label "obsadka"
  ]
  node [
    id 74
    label "ptak"
  ]
  node [
    id 75
    label "wypisanie"
  ]
  node [
    id 76
    label "pir&#243;g"
  ]
  node [
    id 77
    label "pierze"
  ]
  node [
    id 78
    label "wypisa&#263;"
  ]
  node [
    id 79
    label "pisarstwo"
  ]
  node [
    id 80
    label "element"
  ]
  node [
    id 81
    label "element_anatomiczny"
  ]
  node [
    id 82
    label "autor"
  ]
  node [
    id 83
    label "artyku&#322;"
  ]
  node [
    id 84
    label "p&#322;askownik"
  ]
  node [
    id 85
    label "dusza"
  ]
  node [
    id 86
    label "upierzenie"
  ]
  node [
    id 87
    label "atrament"
  ]
  node [
    id 88
    label "magierka"
  ]
  node [
    id 89
    label "quill"
  ]
  node [
    id 90
    label "pi&#243;ropusz"
  ]
  node [
    id 91
    label "stosina"
  ]
  node [
    id 92
    label "wyst&#281;p"
  ]
  node [
    id 93
    label "g&#322;ownia"
  ]
  node [
    id 94
    label "resor_pi&#243;rowy"
  ]
  node [
    id 95
    label "pen"
  ]
  node [
    id 96
    label "Rzym_Zachodni"
  ]
  node [
    id 97
    label "whole"
  ]
  node [
    id 98
    label "ilo&#347;&#263;"
  ]
  node [
    id 99
    label "Rzym_Wschodni"
  ]
  node [
    id 100
    label "urz&#261;dzenie"
  ]
  node [
    id 101
    label "sport_rakietowy"
  ]
  node [
    id 102
    label "deblowy"
  ]
  node [
    id 103
    label "mikst"
  ]
  node [
    id 104
    label "speed_badminton"
  ]
  node [
    id 105
    label "deblista"
  ]
  node [
    id 106
    label "singlowy"
  ]
  node [
    id 107
    label "miksista"
  ]
  node [
    id 108
    label "babington"
  ]
  node [
    id 109
    label "singlista"
  ]
  node [
    id 110
    label "powierzchnia_sterowa"
  ]
  node [
    id 111
    label "ster"
  ]
  node [
    id 112
    label "szybowiec"
  ]
  node [
    id 113
    label "wo&#322;owina"
  ]
  node [
    id 114
    label "dywizjon_lotniczy"
  ]
  node [
    id 115
    label "drzwi"
  ]
  node [
    id 116
    label "strz&#281;pina"
  ]
  node [
    id 117
    label "&#380;y&#322;kowanie"
  ]
  node [
    id 118
    label "mi&#281;so"
  ]
  node [
    id 119
    label "winglet"
  ]
  node [
    id 120
    label "brama"
  ]
  node [
    id 121
    label "zbroja"
  ]
  node [
    id 122
    label "wing"
  ]
  node [
    id 123
    label "organizacja"
  ]
  node [
    id 124
    label "skrzele"
  ]
  node [
    id 125
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 126
    label "wirolot"
  ]
  node [
    id 127
    label "budynek"
  ]
  node [
    id 128
    label "samolot"
  ]
  node [
    id 129
    label "oddzia&#322;"
  ]
  node [
    id 130
    label "grupa"
  ]
  node [
    id 131
    label "okno"
  ]
  node [
    id 132
    label "o&#322;tarz"
  ]
  node [
    id 133
    label "p&#243;&#322;tusza"
  ]
  node [
    id 134
    label "tuszka"
  ]
  node [
    id 135
    label "klapa"
  ]
  node [
    id 136
    label "szyk"
  ]
  node [
    id 137
    label "boisko"
  ]
  node [
    id 138
    label "dr&#243;b"
  ]
  node [
    id 139
    label "narz&#261;d_ruchu"
  ]
  node [
    id 140
    label "husarz"
  ]
  node [
    id 141
    label "skrzyd&#322;owiec"
  ]
  node [
    id 142
    label "dr&#243;bka"
  ]
  node [
    id 143
    label "keson"
  ]
  node [
    id 144
    label "husaria"
  ]
  node [
    id 145
    label "ugrupowanie"
  ]
  node [
    id 146
    label "si&#322;y_powietrzne"
  ]
  node [
    id 147
    label "kolejny"
  ]
  node [
    id 148
    label "cz&#322;owiek"
  ]
  node [
    id 149
    label "niedawno"
  ]
  node [
    id 150
    label "poprzedni"
  ]
  node [
    id 151
    label "pozosta&#322;y"
  ]
  node [
    id 152
    label "ostatnio"
  ]
  node [
    id 153
    label "sko&#324;czony"
  ]
  node [
    id 154
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 155
    label "aktualny"
  ]
  node [
    id 156
    label "najgorszy"
  ]
  node [
    id 157
    label "istota_&#380;ywa"
  ]
  node [
    id 158
    label "w&#261;tpliwy"
  ]
  node [
    id 159
    label "nast&#281;pnie"
  ]
  node [
    id 160
    label "inny"
  ]
  node [
    id 161
    label "nastopny"
  ]
  node [
    id 162
    label "kolejno"
  ]
  node [
    id 163
    label "kt&#243;ry&#347;"
  ]
  node [
    id 164
    label "przesz&#322;y"
  ]
  node [
    id 165
    label "wcze&#347;niejszy"
  ]
  node [
    id 166
    label "poprzednio"
  ]
  node [
    id 167
    label "w&#261;tpliwie"
  ]
  node [
    id 168
    label "pozorny"
  ]
  node [
    id 169
    label "&#380;ywy"
  ]
  node [
    id 170
    label "ostateczny"
  ]
  node [
    id 171
    label "wa&#380;ny"
  ]
  node [
    id 172
    label "ludzko&#347;&#263;"
  ]
  node [
    id 173
    label "asymilowanie"
  ]
  node [
    id 174
    label "wapniak"
  ]
  node [
    id 175
    label "asymilowa&#263;"
  ]
  node [
    id 176
    label "os&#322;abia&#263;"
  ]
  node [
    id 177
    label "posta&#263;"
  ]
  node [
    id 178
    label "hominid"
  ]
  node [
    id 179
    label "podw&#322;adny"
  ]
  node [
    id 180
    label "os&#322;abianie"
  ]
  node [
    id 181
    label "g&#322;owa"
  ]
  node [
    id 182
    label "figura"
  ]
  node [
    id 183
    label "portrecista"
  ]
  node [
    id 184
    label "dwun&#243;g"
  ]
  node [
    id 185
    label "profanum"
  ]
  node [
    id 186
    label "mikrokosmos"
  ]
  node [
    id 187
    label "nasada"
  ]
  node [
    id 188
    label "duch"
  ]
  node [
    id 189
    label "antropochoria"
  ]
  node [
    id 190
    label "osoba"
  ]
  node [
    id 191
    label "wz&#243;r"
  ]
  node [
    id 192
    label "senior"
  ]
  node [
    id 193
    label "oddzia&#322;ywanie"
  ]
  node [
    id 194
    label "Adam"
  ]
  node [
    id 195
    label "homo_sapiens"
  ]
  node [
    id 196
    label "polifag"
  ]
  node [
    id 197
    label "wykszta&#322;cony"
  ]
  node [
    id 198
    label "dyplomowany"
  ]
  node [
    id 199
    label "wykwalifikowany"
  ]
  node [
    id 200
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 201
    label "kompletny"
  ]
  node [
    id 202
    label "sko&#324;czenie"
  ]
  node [
    id 203
    label "okre&#347;lony"
  ]
  node [
    id 204
    label "wielki"
  ]
  node [
    id 205
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 206
    label "aktualnie"
  ]
  node [
    id 207
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 208
    label "aktualizowanie"
  ]
  node [
    id 209
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 210
    label "uaktualnienie"
  ]
  node [
    id 211
    label "cykl_astronomiczny"
  ]
  node [
    id 212
    label "coil"
  ]
  node [
    id 213
    label "zjawisko"
  ]
  node [
    id 214
    label "fotoelement"
  ]
  node [
    id 215
    label "komutowanie"
  ]
  node [
    id 216
    label "stan_skupienia"
  ]
  node [
    id 217
    label "nastr&#243;j"
  ]
  node [
    id 218
    label "przerywacz"
  ]
  node [
    id 219
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 220
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 221
    label "kraw&#281;d&#378;"
  ]
  node [
    id 222
    label "obsesja"
  ]
  node [
    id 223
    label "dw&#243;jnik"
  ]
  node [
    id 224
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 225
    label "okres"
  ]
  node [
    id 226
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 227
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 228
    label "przew&#243;d"
  ]
  node [
    id 229
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 230
    label "czas"
  ]
  node [
    id 231
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 232
    label "obw&#243;d"
  ]
  node [
    id 233
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 234
    label "degree"
  ]
  node [
    id 235
    label "komutowa&#263;"
  ]
  node [
    id 236
    label "proces"
  ]
  node [
    id 237
    label "boski"
  ]
  node [
    id 238
    label "krajobraz"
  ]
  node [
    id 239
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 240
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 241
    label "przywidzenie"
  ]
  node [
    id 242
    label "presence"
  ]
  node [
    id 243
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 244
    label "state"
  ]
  node [
    id 245
    label "klimat"
  ]
  node [
    id 246
    label "stan"
  ]
  node [
    id 247
    label "samopoczucie"
  ]
  node [
    id 248
    label "kwas"
  ]
  node [
    id 249
    label "graf"
  ]
  node [
    id 250
    label "para"
  ]
  node [
    id 251
    label "narta"
  ]
  node [
    id 252
    label "ochraniacz"
  ]
  node [
    id 253
    label "poj&#281;cie"
  ]
  node [
    id 254
    label "end"
  ]
  node [
    id 255
    label "koniec"
  ]
  node [
    id 256
    label "sytuacja"
  ]
  node [
    id 257
    label "network"
  ]
  node [
    id 258
    label "opornik"
  ]
  node [
    id 259
    label "lampa_elektronowa"
  ]
  node [
    id 260
    label "cewka"
  ]
  node [
    id 261
    label "linia"
  ]
  node [
    id 262
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 263
    label "&#263;wiczenie"
  ]
  node [
    id 264
    label "pa&#324;stwo"
  ]
  node [
    id 265
    label "bezpiecznik"
  ]
  node [
    id 266
    label "rozmiar"
  ]
  node [
    id 267
    label "tranzystor"
  ]
  node [
    id 268
    label "okr&#281;g_przemys&#322;owy"
  ]
  node [
    id 269
    label "kondensator"
  ]
  node [
    id 270
    label "circumference"
  ]
  node [
    id 271
    label "styk"
  ]
  node [
    id 272
    label "region"
  ]
  node [
    id 273
    label "uk&#322;ad"
  ]
  node [
    id 274
    label "cyrkumferencja"
  ]
  node [
    id 275
    label "trening"
  ]
  node [
    id 276
    label "sekwencja"
  ]
  node [
    id 277
    label "jednostka_administracyjna"
  ]
  node [
    id 278
    label "poprzedzanie"
  ]
  node [
    id 279
    label "czasoprzestrze&#324;"
  ]
  node [
    id 280
    label "laba"
  ]
  node [
    id 281
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 282
    label "chronometria"
  ]
  node [
    id 283
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 284
    label "rachuba_czasu"
  ]
  node [
    id 285
    label "przep&#322;ywanie"
  ]
  node [
    id 286
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 287
    label "czasokres"
  ]
  node [
    id 288
    label "odczyt"
  ]
  node [
    id 289
    label "chwila"
  ]
  node [
    id 290
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 291
    label "dzieje"
  ]
  node [
    id 292
    label "kategoria_gramatyczna"
  ]
  node [
    id 293
    label "poprzedzenie"
  ]
  node [
    id 294
    label "trawienie"
  ]
  node [
    id 295
    label "pochodzi&#263;"
  ]
  node [
    id 296
    label "period"
  ]
  node [
    id 297
    label "okres_czasu"
  ]
  node [
    id 298
    label "poprzedza&#263;"
  ]
  node [
    id 299
    label "schy&#322;ek"
  ]
  node [
    id 300
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 301
    label "odwlekanie_si&#281;"
  ]
  node [
    id 302
    label "zegar"
  ]
  node [
    id 303
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 304
    label "czwarty_wymiar"
  ]
  node [
    id 305
    label "pochodzenie"
  ]
  node [
    id 306
    label "koniugacja"
  ]
  node [
    id 307
    label "Zeitgeist"
  ]
  node [
    id 308
    label "trawi&#263;"
  ]
  node [
    id 309
    label "pogoda"
  ]
  node [
    id 310
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 311
    label "poprzedzi&#263;"
  ]
  node [
    id 312
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 313
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 314
    label "time_period"
  ]
  node [
    id 315
    label "zapami&#281;tanie"
  ]
  node [
    id 316
    label "oznaka"
  ]
  node [
    id 317
    label "pierdolec"
  ]
  node [
    id 318
    label "temper"
  ]
  node [
    id 319
    label "szajba"
  ]
  node [
    id 320
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 321
    label "ciecz"
  ]
  node [
    id 322
    label "faza_termodynamiczna"
  ]
  node [
    id 323
    label "roztw&#243;r"
  ]
  node [
    id 324
    label "commutation"
  ]
  node [
    id 325
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 326
    label "prze&#322;&#261;czanie"
  ]
  node [
    id 327
    label "prze&#322;&#261;cza&#263;"
  ]
  node [
    id 328
    label "okres_amazo&#324;ski"
  ]
  node [
    id 329
    label "stater"
  ]
  node [
    id 330
    label "flow"
  ]
  node [
    id 331
    label "choroba_przyrodzona"
  ]
  node [
    id 332
    label "ordowik"
  ]
  node [
    id 333
    label "postglacja&#322;"
  ]
  node [
    id 334
    label "kreda"
  ]
  node [
    id 335
    label "okres_hesperyjski"
  ]
  node [
    id 336
    label "sylur"
  ]
  node [
    id 337
    label "paleogen"
  ]
  node [
    id 338
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 339
    label "okres_halsztacki"
  ]
  node [
    id 340
    label "riak"
  ]
  node [
    id 341
    label "czwartorz&#281;d"
  ]
  node [
    id 342
    label "podokres"
  ]
  node [
    id 343
    label "trzeciorz&#281;d"
  ]
  node [
    id 344
    label "kalim"
  ]
  node [
    id 345
    label "fala"
  ]
  node [
    id 346
    label "perm"
  ]
  node [
    id 347
    label "retoryka"
  ]
  node [
    id 348
    label "prekambr"
  ]
  node [
    id 349
    label "neogen"
  ]
  node [
    id 350
    label "pulsacja"
  ]
  node [
    id 351
    label "proces_fizjologiczny"
  ]
  node [
    id 352
    label "kambr"
  ]
  node [
    id 353
    label "kriogen"
  ]
  node [
    id 354
    label "jednostka_geologiczna"
  ]
  node [
    id 355
    label "ton"
  ]
  node [
    id 356
    label "orosir"
  ]
  node [
    id 357
    label "poprzednik"
  ]
  node [
    id 358
    label "spell"
  ]
  node [
    id 359
    label "sider"
  ]
  node [
    id 360
    label "interstadia&#322;"
  ]
  node [
    id 361
    label "ektas"
  ]
  node [
    id 362
    label "epoka"
  ]
  node [
    id 363
    label "rok_akademicki"
  ]
  node [
    id 364
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 365
    label "cykl"
  ]
  node [
    id 366
    label "ciota"
  ]
  node [
    id 367
    label "okres_noachijski"
  ]
  node [
    id 368
    label "pierwszorz&#281;d"
  ]
  node [
    id 369
    label "ediakar"
  ]
  node [
    id 370
    label "zdanie"
  ]
  node [
    id 371
    label "nast&#281;pnik"
  ]
  node [
    id 372
    label "condition"
  ]
  node [
    id 373
    label "jura"
  ]
  node [
    id 374
    label "glacja&#322;"
  ]
  node [
    id 375
    label "sten"
  ]
  node [
    id 376
    label "era"
  ]
  node [
    id 377
    label "trias"
  ]
  node [
    id 378
    label "p&#243;&#322;okres"
  ]
  node [
    id 379
    label "rok_szkolny"
  ]
  node [
    id 380
    label "dewon"
  ]
  node [
    id 381
    label "karbon"
  ]
  node [
    id 382
    label "izochronizm"
  ]
  node [
    id 383
    label "preglacja&#322;"
  ]
  node [
    id 384
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 385
    label "drugorz&#281;d"
  ]
  node [
    id 386
    label "semester"
  ]
  node [
    id 387
    label "kognicja"
  ]
  node [
    id 388
    label "przy&#322;&#261;cze"
  ]
  node [
    id 389
    label "rozprawa"
  ]
  node [
    id 390
    label "organ"
  ]
  node [
    id 391
    label "przes&#322;anka"
  ]
  node [
    id 392
    label "post&#281;powanie"
  ]
  node [
    id 393
    label "przewodnictwo"
  ]
  node [
    id 394
    label "tr&#243;jnik"
  ]
  node [
    id 395
    label "wtyczka"
  ]
  node [
    id 396
    label "&#380;y&#322;a"
  ]
  node [
    id 397
    label "duct"
  ]
  node [
    id 398
    label "k&#261;piel_fotograficzna"
  ]
  node [
    id 399
    label "interrupter"
  ]
  node [
    id 400
    label "lot"
  ]
  node [
    id 401
    label "descent"
  ]
  node [
    id 402
    label "trafienie"
  ]
  node [
    id 403
    label "trafianie"
  ]
  node [
    id 404
    label "przybycie"
  ]
  node [
    id 405
    label "radzenie_sobie"
  ]
  node [
    id 406
    label "poradzenie_sobie"
  ]
  node [
    id 407
    label "dobijanie"
  ]
  node [
    id 408
    label "skok"
  ]
  node [
    id 409
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 410
    label "lecenie"
  ]
  node [
    id 411
    label "przybywanie"
  ]
  node [
    id 412
    label "dobicie"
  ]
  node [
    id 413
    label "zjawienie_si&#281;"
  ]
  node [
    id 414
    label "dolecenie"
  ]
  node [
    id 415
    label "punkt"
  ]
  node [
    id 416
    label "rozgrywka"
  ]
  node [
    id 417
    label "strike"
  ]
  node [
    id 418
    label "dostanie_si&#281;"
  ]
  node [
    id 419
    label "wpadni&#281;cie"
  ]
  node [
    id 420
    label "spowodowanie"
  ]
  node [
    id 421
    label "pocisk"
  ]
  node [
    id 422
    label "hit"
  ]
  node [
    id 423
    label "zdarzenie_si&#281;"
  ]
  node [
    id 424
    label "sukces"
  ]
  node [
    id 425
    label "znalezienie_si&#281;"
  ]
  node [
    id 426
    label "znalezienie"
  ]
  node [
    id 427
    label "dopasowanie_si&#281;"
  ]
  node [
    id 428
    label "dotarcie"
  ]
  node [
    id 429
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 430
    label "gather"
  ]
  node [
    id 431
    label "dostanie"
  ]
  node [
    id 432
    label "dosi&#281;ganie"
  ]
  node [
    id 433
    label "dopasowywanie_si&#281;"
  ]
  node [
    id 434
    label "zjawianie_si&#281;"
  ]
  node [
    id 435
    label "wpadanie"
  ]
  node [
    id 436
    label "pojawianie_si&#281;"
  ]
  node [
    id 437
    label "dostawanie"
  ]
  node [
    id 438
    label "docieranie"
  ]
  node [
    id 439
    label "aim"
  ]
  node [
    id 440
    label "dolatywanie"
  ]
  node [
    id 441
    label "znajdowanie"
  ]
  node [
    id 442
    label "dzianie_si&#281;"
  ]
  node [
    id 443
    label "dostawanie_si&#281;"
  ]
  node [
    id 444
    label "meeting"
  ]
  node [
    id 445
    label "zwi&#281;kszanie_si&#281;"
  ]
  node [
    id 446
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 447
    label "powi&#281;kszenie_si&#281;"
  ]
  node [
    id 448
    label "arrival"
  ]
  node [
    id 449
    label "derail"
  ]
  node [
    id 450
    label "noga"
  ]
  node [
    id 451
    label "naskok"
  ]
  node [
    id 452
    label "struktura_anatomiczna"
  ]
  node [
    id 453
    label "wybicie"
  ]
  node [
    id 454
    label "konkurencja"
  ]
  node [
    id 455
    label "caper"
  ]
  node [
    id 456
    label "stroke"
  ]
  node [
    id 457
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 458
    label "zaj&#261;c"
  ]
  node [
    id 459
    label "ruch"
  ]
  node [
    id 460
    label "&#322;apa"
  ]
  node [
    id 461
    label "zmiana"
  ]
  node [
    id 462
    label "napad"
  ]
  node [
    id 463
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 464
    label "chronometra&#380;ysta"
  ]
  node [
    id 465
    label "odlot"
  ]
  node [
    id 466
    label "start"
  ]
  node [
    id 467
    label "podr&#243;&#380;"
  ]
  node [
    id 468
    label "ci&#261;g"
  ]
  node [
    id 469
    label "flight"
  ]
  node [
    id 470
    label "rozlewanie_si&#281;"
  ]
  node [
    id 471
    label "biegni&#281;cie"
  ]
  node [
    id 472
    label "nadlatywanie"
  ]
  node [
    id 473
    label "planowanie"
  ]
  node [
    id 474
    label "nurkowanie"
  ]
  node [
    id 475
    label "gallop"
  ]
  node [
    id 476
    label "zlatywanie"
  ]
  node [
    id 477
    label "przelecenie"
  ]
  node [
    id 478
    label "oblatywanie"
  ]
  node [
    id 479
    label "przylatywanie"
  ]
  node [
    id 480
    label "przylecenie"
  ]
  node [
    id 481
    label "nadlecenie"
  ]
  node [
    id 482
    label "zlecenie"
  ]
  node [
    id 483
    label "sikanie"
  ]
  node [
    id 484
    label "odkr&#281;canie_wody"
  ]
  node [
    id 485
    label "przelatywanie"
  ]
  node [
    id 486
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 487
    label "rozlanie_si&#281;"
  ]
  node [
    id 488
    label "zanurkowanie"
  ]
  node [
    id 489
    label "podlecenie"
  ]
  node [
    id 490
    label "odkr&#281;cenie_wody"
  ]
  node [
    id 491
    label "przelanie_si&#281;"
  ]
  node [
    id 492
    label "oblecenie"
  ]
  node [
    id 493
    label "odlecenie"
  ]
  node [
    id 494
    label "rise"
  ]
  node [
    id 495
    label "sp&#322;ywanie"
  ]
  node [
    id 496
    label "rozbicie_si&#281;"
  ]
  node [
    id 497
    label "odlatywanie"
  ]
  node [
    id 498
    label "przelewanie_si&#281;"
  ]
  node [
    id 499
    label "gnanie"
  ]
  node [
    id 500
    label "hike"
  ]
  node [
    id 501
    label "zaw&#281;drowanie"
  ]
  node [
    id 502
    label "bezwizowy"
  ]
  node [
    id 503
    label "zatrzymanie_si&#281;"
  ]
  node [
    id 504
    label "zje&#380;d&#380;enie"
  ]
  node [
    id 505
    label "impression"
  ]
  node [
    id 506
    label "dokuczenie"
  ]
  node [
    id 507
    label "dorobienie"
  ]
  node [
    id 508
    label "og&#322;oszenie_drukiem"
  ]
  node [
    id 509
    label "nail"
  ]
  node [
    id 510
    label "zawini&#281;cie"
  ]
  node [
    id 511
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 512
    label "statek"
  ]
  node [
    id 513
    label "doci&#347;ni&#281;cie"
  ]
  node [
    id 514
    label "przygn&#281;bienie"
  ]
  node [
    id 515
    label "zacumowanie"
  ]
  node [
    id 516
    label "adjudication"
  ]
  node [
    id 517
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 518
    label "zabicie"
  ]
  node [
    id 519
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 520
    label "zawijanie"
  ]
  node [
    id 521
    label "dociskanie"
  ]
  node [
    id 522
    label "zabijanie"
  ]
  node [
    id 523
    label "dop&#322;ywanie"
  ]
  node [
    id 524
    label "dokuczanie"
  ]
  node [
    id 525
    label "przygn&#281;bianie"
  ]
  node [
    id 526
    label "cumowanie"
  ]
  node [
    id 527
    label "biblioteka"
  ]
  node [
    id 528
    label "radiator"
  ]
  node [
    id 529
    label "wyci&#261;garka"
  ]
  node [
    id 530
    label "gondola_silnikowa"
  ]
  node [
    id 531
    label "aerosanie"
  ]
  node [
    id 532
    label "podgrzewacz"
  ]
  node [
    id 533
    label "motogodzina"
  ]
  node [
    id 534
    label "motoszybowiec"
  ]
  node [
    id 535
    label "program"
  ]
  node [
    id 536
    label "gniazdo_zaworowe"
  ]
  node [
    id 537
    label "mechanizm"
  ]
  node [
    id 538
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 539
    label "dociera&#263;"
  ]
  node [
    id 540
    label "samoch&#243;d"
  ]
  node [
    id 541
    label "nap&#281;d"
  ]
  node [
    id 542
    label "motor&#243;wka"
  ]
  node [
    id 543
    label "rz&#281;zi&#263;"
  ]
  node [
    id 544
    label "perpetuum_mobile"
  ]
  node [
    id 545
    label "bombowiec"
  ]
  node [
    id 546
    label "dotrze&#263;"
  ]
  node [
    id 547
    label "rz&#281;&#380;enie"
  ]
  node [
    id 548
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 549
    label "zbi&#243;r"
  ]
  node [
    id 550
    label "czytelnia"
  ]
  node [
    id 551
    label "kolekcja"
  ]
  node [
    id 552
    label "instytucja"
  ]
  node [
    id 553
    label "rewers"
  ]
  node [
    id 554
    label "library"
  ]
  node [
    id 555
    label "programowanie"
  ]
  node [
    id 556
    label "pok&#243;j"
  ]
  node [
    id 557
    label "informatorium"
  ]
  node [
    id 558
    label "czytelnik"
  ]
  node [
    id 559
    label "instalowa&#263;"
  ]
  node [
    id 560
    label "oprogramowanie"
  ]
  node [
    id 561
    label "odinstalowywa&#263;"
  ]
  node [
    id 562
    label "spis"
  ]
  node [
    id 563
    label "zaprezentowanie"
  ]
  node [
    id 564
    label "podprogram"
  ]
  node [
    id 565
    label "ogranicznik_referencyjny"
  ]
  node [
    id 566
    label "course_of_study"
  ]
  node [
    id 567
    label "booklet"
  ]
  node [
    id 568
    label "dzia&#322;"
  ]
  node [
    id 569
    label "odinstalowanie"
  ]
  node [
    id 570
    label "broszura"
  ]
  node [
    id 571
    label "wytw&#243;r"
  ]
  node [
    id 572
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 573
    label "kana&#322;"
  ]
  node [
    id 574
    label "teleferie"
  ]
  node [
    id 575
    label "zainstalowanie"
  ]
  node [
    id 576
    label "struktura_organizacyjna"
  ]
  node [
    id 577
    label "pirat"
  ]
  node [
    id 578
    label "zaprezentowa&#263;"
  ]
  node [
    id 579
    label "prezentowanie"
  ]
  node [
    id 580
    label "prezentowa&#263;"
  ]
  node [
    id 581
    label "interfejs"
  ]
  node [
    id 582
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 583
    label "blok"
  ]
  node [
    id 584
    label "folder"
  ]
  node [
    id 585
    label "zainstalowa&#263;"
  ]
  node [
    id 586
    label "za&#322;o&#380;enie"
  ]
  node [
    id 587
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 588
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 589
    label "ram&#243;wka"
  ]
  node [
    id 590
    label "tryb"
  ]
  node [
    id 591
    label "emitowa&#263;"
  ]
  node [
    id 592
    label "emitowanie"
  ]
  node [
    id 593
    label "odinstalowywanie"
  ]
  node [
    id 594
    label "instrukcja"
  ]
  node [
    id 595
    label "informatyka"
  ]
  node [
    id 596
    label "deklaracja"
  ]
  node [
    id 597
    label "sekcja_krytyczna"
  ]
  node [
    id 598
    label "menu"
  ]
  node [
    id 599
    label "furkacja"
  ]
  node [
    id 600
    label "podstawa"
  ]
  node [
    id 601
    label "instalowanie"
  ]
  node [
    id 602
    label "oferta"
  ]
  node [
    id 603
    label "odinstalowa&#263;"
  ]
  node [
    id 604
    label "most"
  ]
  node [
    id 605
    label "propulsion"
  ]
  node [
    id 606
    label "spos&#243;b"
  ]
  node [
    id 607
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 608
    label "maszyneria"
  ]
  node [
    id 609
    label "maszyna"
  ]
  node [
    id 610
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 611
    label "fondue"
  ]
  node [
    id 612
    label "atrapa"
  ]
  node [
    id 613
    label "wzmacniacz"
  ]
  node [
    id 614
    label "regulator"
  ]
  node [
    id 615
    label "pojazd_drogowy"
  ]
  node [
    id 616
    label "spryskiwacz"
  ]
  node [
    id 617
    label "baga&#380;nik"
  ]
  node [
    id 618
    label "dachowanie"
  ]
  node [
    id 619
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 620
    label "pompa_wodna"
  ]
  node [
    id 621
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 622
    label "poduszka_powietrzna"
  ]
  node [
    id 623
    label "tempomat"
  ]
  node [
    id 624
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 625
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 626
    label "deska_rozdzielcza"
  ]
  node [
    id 627
    label "immobilizer"
  ]
  node [
    id 628
    label "t&#322;umik"
  ]
  node [
    id 629
    label "kierownica"
  ]
  node [
    id 630
    label "ABS"
  ]
  node [
    id 631
    label "bak"
  ]
  node [
    id 632
    label "dwu&#347;lad"
  ]
  node [
    id 633
    label "poci&#261;g_drogowy"
  ]
  node [
    id 634
    label "wycieraczka"
  ]
  node [
    id 635
    label "kabina"
  ]
  node [
    id 636
    label "eskadra_bombowa"
  ]
  node [
    id 637
    label "bomba"
  ]
  node [
    id 638
    label "&#347;mig&#322;o"
  ]
  node [
    id 639
    label "samolot_bojowy"
  ]
  node [
    id 640
    label "dywizjon_bombowy"
  ]
  node [
    id 641
    label "podwozie"
  ]
  node [
    id 642
    label "sanie"
  ]
  node [
    id 643
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 644
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 645
    label "dopasowywa&#263;"
  ]
  node [
    id 646
    label "g&#322;adzi&#263;"
  ]
  node [
    id 647
    label "boost"
  ]
  node [
    id 648
    label "dorabia&#263;"
  ]
  node [
    id 649
    label "get"
  ]
  node [
    id 650
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 651
    label "trze&#263;"
  ]
  node [
    id 652
    label "znajdowa&#263;"
  ]
  node [
    id 653
    label "utarcie"
  ]
  node [
    id 654
    label "wyg&#322;adzenie"
  ]
  node [
    id 655
    label "dopasowanie"
  ]
  node [
    id 656
    label "range"
  ]
  node [
    id 657
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 658
    label "zgrzyta&#263;"
  ]
  node [
    id 659
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 660
    label "wheeze"
  ]
  node [
    id 661
    label "wy&#263;"
  ]
  node [
    id 662
    label "&#347;wista&#263;"
  ]
  node [
    id 663
    label "os&#322;uchiwanie"
  ]
  node [
    id 664
    label "oddycha&#263;"
  ]
  node [
    id 665
    label "warcze&#263;"
  ]
  node [
    id 666
    label "rattle"
  ]
  node [
    id 667
    label "kaszlak"
  ]
  node [
    id 668
    label "p&#322;uca"
  ]
  node [
    id 669
    label "wydobywa&#263;"
  ]
  node [
    id 670
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 671
    label "powodowanie"
  ]
  node [
    id 672
    label "dorabianie"
  ]
  node [
    id 673
    label "tarcie"
  ]
  node [
    id 674
    label "dopasowywanie"
  ]
  node [
    id 675
    label "g&#322;adzenie"
  ]
  node [
    id 676
    label "oddychanie"
  ]
  node [
    id 677
    label "wydobywanie"
  ]
  node [
    id 678
    label "brzmienie"
  ]
  node [
    id 679
    label "wydawanie"
  ]
  node [
    id 680
    label "jednostka_czasu"
  ]
  node [
    id 681
    label "utrze&#263;"
  ]
  node [
    id 682
    label "znale&#378;&#263;"
  ]
  node [
    id 683
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 684
    label "catch"
  ]
  node [
    id 685
    label "dopasowa&#263;"
  ]
  node [
    id 686
    label "advance"
  ]
  node [
    id 687
    label "spowodowa&#263;"
  ]
  node [
    id 688
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 689
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 690
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 691
    label "dorobi&#263;"
  ]
  node [
    id 692
    label "become"
  ]
  node [
    id 693
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 694
    label "pofolgowa&#263;"
  ]
  node [
    id 695
    label "assent"
  ]
  node [
    id 696
    label "uzna&#263;"
  ]
  node [
    id 697
    label "leave"
  ]
  node [
    id 698
    label "oceni&#263;"
  ]
  node [
    id 699
    label "przyzna&#263;"
  ]
  node [
    id 700
    label "stwierdzi&#263;"
  ]
  node [
    id 701
    label "rede"
  ]
  node [
    id 702
    label "see"
  ]
  node [
    id 703
    label "permit"
  ]
  node [
    id 704
    label "zagrza&#263;"
  ]
  node [
    id 705
    label "stabilize"
  ]
  node [
    id 706
    label "uregulowa&#263;"
  ]
  node [
    id 707
    label "zach&#281;ci&#263;"
  ]
  node [
    id 708
    label "heating_system"
  ]
  node [
    id 709
    label "temperatura"
  ]
  node [
    id 710
    label "podnie&#347;&#263;"
  ]
  node [
    id 711
    label "heat"
  ]
  node [
    id 712
    label "supply"
  ]
  node [
    id 713
    label "op&#322;aci&#263;"
  ]
  node [
    id 714
    label "ulepszy&#263;"
  ]
  node [
    id 715
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 716
    label "zobowi&#261;zanie"
  ]
  node [
    id 717
    label "normalize"
  ]
  node [
    id 718
    label "statek_kosmiczny"
  ]
  node [
    id 719
    label "szybki"
  ]
  node [
    id 720
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 721
    label "naci&#261;g"
  ]
  node [
    id 722
    label "silnik_rakietowy"
  ]
  node [
    id 723
    label "but"
  ]
  node [
    id 724
    label "&#322;&#261;cznik"
  ]
  node [
    id 725
    label "przyrz&#261;d"
  ]
  node [
    id 726
    label "pojazd"
  ]
  node [
    id 727
    label "g&#322;&#243;wka"
  ]
  node [
    id 728
    label "&#347;nieg"
  ]
  node [
    id 729
    label "pojazd_lataj&#261;cy"
  ]
  node [
    id 730
    label "pocisk_odrzutowy"
  ]
  node [
    id 731
    label "tenisista"
  ]
  node [
    id 732
    label "utensylia"
  ]
  node [
    id 733
    label "narz&#281;dzie"
  ]
  node [
    id 734
    label "odholowa&#263;"
  ]
  node [
    id 735
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 736
    label "tabor"
  ]
  node [
    id 737
    label "przyholowywanie"
  ]
  node [
    id 738
    label "przyholowa&#263;"
  ]
  node [
    id 739
    label "przyholowanie"
  ]
  node [
    id 740
    label "fukni&#281;cie"
  ]
  node [
    id 741
    label "l&#261;d"
  ]
  node [
    id 742
    label "zielona_karta"
  ]
  node [
    id 743
    label "fukanie"
  ]
  node [
    id 744
    label "przyholowywa&#263;"
  ]
  node [
    id 745
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 746
    label "woda"
  ]
  node [
    id 747
    label "przeszklenie"
  ]
  node [
    id 748
    label "test_zderzeniowy"
  ]
  node [
    id 749
    label "powietrze"
  ]
  node [
    id 750
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 751
    label "odzywka"
  ]
  node [
    id 752
    label "nadwozie"
  ]
  node [
    id 753
    label "odholowanie"
  ]
  node [
    id 754
    label "prowadzenie_si&#281;"
  ]
  node [
    id 755
    label "odholowywa&#263;"
  ]
  node [
    id 756
    label "pod&#322;oga"
  ]
  node [
    id 757
    label "odholowywanie"
  ]
  node [
    id 758
    label "hamulec"
  ]
  node [
    id 759
    label "zapi&#281;tek"
  ]
  node [
    id 760
    label "sznurowad&#322;o"
  ]
  node [
    id 761
    label "rozbijarka"
  ]
  node [
    id 762
    label "podeszwa"
  ]
  node [
    id 763
    label "obcas"
  ]
  node [
    id 764
    label "wzu&#263;"
  ]
  node [
    id 765
    label "wzuwanie"
  ]
  node [
    id 766
    label "przyszwa"
  ]
  node [
    id 767
    label "raki"
  ]
  node [
    id 768
    label "cholewa"
  ]
  node [
    id 769
    label "cholewka"
  ]
  node [
    id 770
    label "zel&#243;wka"
  ]
  node [
    id 771
    label "obuwie"
  ]
  node [
    id 772
    label "j&#281;zyk"
  ]
  node [
    id 773
    label "napi&#281;tek"
  ]
  node [
    id 774
    label "wzucie"
  ]
  node [
    id 775
    label "kszta&#322;t"
  ]
  node [
    id 776
    label "uderzenie"
  ]
  node [
    id 777
    label "ceg&#322;a"
  ]
  node [
    id 778
    label "obiekt"
  ]
  node [
    id 779
    label "zako&#324;czenie"
  ]
  node [
    id 780
    label "ro&#347;lina"
  ]
  node [
    id 781
    label "knob"
  ]
  node [
    id 782
    label "kapelusz"
  ]
  node [
    id 783
    label "b&#281;ben"
  ]
  node [
    id 784
    label "pull"
  ]
  node [
    id 785
    label "si&#322;a"
  ]
  node [
    id 786
    label "komplet"
  ]
  node [
    id 787
    label "membrana"
  ]
  node [
    id 788
    label "instrument_strunowy"
  ]
  node [
    id 789
    label "napr&#281;&#380;enie"
  ]
  node [
    id 790
    label "droga"
  ]
  node [
    id 791
    label "kreska"
  ]
  node [
    id 792
    label "zwiadowca"
  ]
  node [
    id 793
    label "znak_pisarski"
  ]
  node [
    id 794
    label "czynnik"
  ]
  node [
    id 795
    label "fuga"
  ]
  node [
    id 796
    label "przew&#243;d_wiertniczy"
  ]
  node [
    id 797
    label "znak_muzyczny"
  ]
  node [
    id 798
    label "pogoniec"
  ]
  node [
    id 799
    label "S&#261;deczanka"
  ]
  node [
    id 800
    label "ligature"
  ]
  node [
    id 801
    label "orzeczenie"
  ]
  node [
    id 802
    label "napastnik"
  ]
  node [
    id 803
    label "po&#347;rednik"
  ]
  node [
    id 804
    label "hyphen"
  ]
  node [
    id 805
    label "kontakt"
  ]
  node [
    id 806
    label "dobud&#243;wka"
  ]
  node [
    id 807
    label "intensywny"
  ]
  node [
    id 808
    label "prosty"
  ]
  node [
    id 809
    label "kr&#243;tki"
  ]
  node [
    id 810
    label "temperamentny"
  ]
  node [
    id 811
    label "bystrolotny"
  ]
  node [
    id 812
    label "dynamiczny"
  ]
  node [
    id 813
    label "szybko"
  ]
  node [
    id 814
    label "sprawny"
  ]
  node [
    id 815
    label "bezpo&#347;redni"
  ]
  node [
    id 816
    label "energiczny"
  ]
  node [
    id 817
    label "opad"
  ]
  node [
    id 818
    label "sypn&#261;&#263;"
  ]
  node [
    id 819
    label "kokaina"
  ]
  node [
    id 820
    label "sypni&#281;cie"
  ]
  node [
    id 821
    label "pow&#322;oka"
  ]
  node [
    id 822
    label "gracz"
  ]
  node [
    id 823
    label "sportowiec"
  ]
  node [
    id 824
    label "og&#243;lnie"
  ]
  node [
    id 825
    label "w_pizdu"
  ]
  node [
    id 826
    label "ca&#322;y"
  ]
  node [
    id 827
    label "kompletnie"
  ]
  node [
    id 828
    label "&#322;&#261;czny"
  ]
  node [
    id 829
    label "zupe&#322;nie"
  ]
  node [
    id 830
    label "pe&#322;ny"
  ]
  node [
    id 831
    label "jedyny"
  ]
  node [
    id 832
    label "du&#380;y"
  ]
  node [
    id 833
    label "zdr&#243;w"
  ]
  node [
    id 834
    label "calu&#347;ko"
  ]
  node [
    id 835
    label "podobny"
  ]
  node [
    id 836
    label "ca&#322;o"
  ]
  node [
    id 837
    label "&#322;&#261;cznie"
  ]
  node [
    id 838
    label "zbiorczy"
  ]
  node [
    id 839
    label "nadrz&#281;dnie"
  ]
  node [
    id 840
    label "og&#243;lny"
  ]
  node [
    id 841
    label "posp&#243;lnie"
  ]
  node [
    id 842
    label "zbiorowo"
  ]
  node [
    id 843
    label "generalny"
  ]
  node [
    id 844
    label "nieograniczony"
  ]
  node [
    id 845
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 846
    label "satysfakcja"
  ]
  node [
    id 847
    label "bezwzgl&#281;dny"
  ]
  node [
    id 848
    label "otwarty"
  ]
  node [
    id 849
    label "wype&#322;nienie"
  ]
  node [
    id 850
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 851
    label "pe&#322;no"
  ]
  node [
    id 852
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 853
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 854
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 855
    label "zupe&#322;ny"
  ]
  node [
    id 856
    label "r&#243;wny"
  ]
  node [
    id 857
    label "wniwecz"
  ]
  node [
    id 858
    label "przefiltrowanie"
  ]
  node [
    id 859
    label "zamkni&#281;cie"
  ]
  node [
    id 860
    label "career"
  ]
  node [
    id 861
    label "zaaresztowanie"
  ]
  node [
    id 862
    label "przechowanie"
  ]
  node [
    id 863
    label "closure"
  ]
  node [
    id 864
    label "observation"
  ]
  node [
    id 865
    label "funkcjonowanie"
  ]
  node [
    id 866
    label "uniemo&#380;liwienie"
  ]
  node [
    id 867
    label "pochowanie"
  ]
  node [
    id 868
    label "discontinuance"
  ]
  node [
    id 869
    label "przerwanie"
  ]
  node [
    id 870
    label "zaczepienie"
  ]
  node [
    id 871
    label "pozajmowanie"
  ]
  node [
    id 872
    label "hipostaza"
  ]
  node [
    id 873
    label "capture"
  ]
  node [
    id 874
    label "przetrzymanie"
  ]
  node [
    id 875
    label "oddzia&#322;anie"
  ]
  node [
    id 876
    label "&#322;apanie"
  ]
  node [
    id 877
    label "z&#322;apanie"
  ]
  node [
    id 878
    label "check"
  ]
  node [
    id 879
    label "unieruchomienie"
  ]
  node [
    id 880
    label "zabranie"
  ]
  node [
    id 881
    label "przestanie"
  ]
  node [
    id 882
    label "reply"
  ]
  node [
    id 883
    label "zahipnotyzowanie"
  ]
  node [
    id 884
    label "chemia"
  ]
  node [
    id 885
    label "wdarcie_si&#281;"
  ]
  node [
    id 886
    label "reakcja_chemiczna"
  ]
  node [
    id 887
    label "activity"
  ]
  node [
    id 888
    label "bezproblemowy"
  ]
  node [
    id 889
    label "campaign"
  ]
  node [
    id 890
    label "causing"
  ]
  node [
    id 891
    label "zaj&#281;cie"
  ]
  node [
    id 892
    label "wyniesienie"
  ]
  node [
    id 893
    label "pickings"
  ]
  node [
    id 894
    label "pozabieranie"
  ]
  node [
    id 895
    label "pousuwanie"
  ]
  node [
    id 896
    label "przesuni&#281;cie"
  ]
  node [
    id 897
    label "doprowadzenie"
  ]
  node [
    id 898
    label "przeniesienie"
  ]
  node [
    id 899
    label "zniewolenie"
  ]
  node [
    id 900
    label "zniesienie"
  ]
  node [
    id 901
    label "coitus_interruptus"
  ]
  node [
    id 902
    label "zrobienie"
  ]
  node [
    id 903
    label "udanie_si&#281;"
  ]
  node [
    id 904
    label "wywiezienie"
  ]
  node [
    id 905
    label "removal"
  ]
  node [
    id 906
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 907
    label "wzi&#281;cie"
  ]
  node [
    id 908
    label "przeszkodzenie"
  ]
  node [
    id 909
    label "ukr&#281;cenie_"
  ]
  node [
    id 910
    label "frustration"
  ]
  node [
    id 911
    label "immobilization"
  ]
  node [
    id 912
    label "wstrzymanie"
  ]
  node [
    id 913
    label "nieruchomy"
  ]
  node [
    id 914
    label "porozrywanie"
  ]
  node [
    id 915
    label "przerywa&#263;"
  ]
  node [
    id 916
    label "rozerwanie"
  ]
  node [
    id 917
    label "poprzerywanie"
  ]
  node [
    id 918
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 919
    label "severance"
  ]
  node [
    id 920
    label "przerywanie"
  ]
  node [
    id 921
    label "przerzedzenie"
  ]
  node [
    id 922
    label "przerwa&#263;"
  ]
  node [
    id 923
    label "odpoczywanie"
  ]
  node [
    id 924
    label "przedziurawienie"
  ]
  node [
    id 925
    label "wada_wrodzona"
  ]
  node [
    id 926
    label "urwanie"
  ]
  node [
    id 927
    label "cutoff"
  ]
  node [
    id 928
    label "kultywar"
  ]
  node [
    id 929
    label "clang"
  ]
  node [
    id 930
    label "cause"
  ]
  node [
    id 931
    label "causal_agent"
  ]
  node [
    id 932
    label "uzyskanie"
  ]
  node [
    id 933
    label "zaskoczenie"
  ]
  node [
    id 934
    label "zara&#380;enie_si&#281;"
  ]
  node [
    id 935
    label "porwanie"
  ]
  node [
    id 936
    label "chwycenie"
  ]
  node [
    id 937
    label "na&#322;apanie"
  ]
  node [
    id 938
    label "zabieranie"
  ]
  node [
    id 939
    label "rozumienie"
  ]
  node [
    id 940
    label "branie"
  ]
  node [
    id 941
    label "d&#322;o&#324;"
  ]
  node [
    id 942
    label "wy&#322;awianie"
  ]
  node [
    id 943
    label "na&#322;apanie_si&#281;"
  ]
  node [
    id 944
    label "zara&#380;anie_si&#281;"
  ]
  node [
    id 945
    label "porywanie"
  ]
  node [
    id 946
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 947
    label "uniemo&#380;liwianie"
  ]
  node [
    id 948
    label "ogarnianie"
  ]
  node [
    id 949
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 950
    label "oduczenie"
  ]
  node [
    id 951
    label "disavowal"
  ]
  node [
    id 952
    label "cessation"
  ]
  node [
    id 953
    label "przeczekanie"
  ]
  node [
    id 954
    label "commitment"
  ]
  node [
    id 955
    label "zgarni&#281;cie"
  ]
  node [
    id 956
    label "ukrycie"
  ]
  node [
    id 957
    label "retention"
  ]
  node [
    id 958
    label "preserve"
  ]
  node [
    id 959
    label "zmagazynowanie"
  ]
  node [
    id 960
    label "podtrzymanie"
  ]
  node [
    id 961
    label "uchronienie"
  ]
  node [
    id 962
    label "potrzymanie"
  ]
  node [
    id 963
    label "pozostanie"
  ]
  node [
    id 964
    label "pokonanie"
  ]
  node [
    id 965
    label "utrzymanie"
  ]
  node [
    id 966
    label "experience"
  ]
  node [
    id 967
    label "przepuszczenie"
  ]
  node [
    id 968
    label "nerka"
  ]
  node [
    id 969
    label "oczyszczenie"
  ]
  node [
    id 970
    label "usuni&#281;cie"
  ]
  node [
    id 971
    label "poumieszczanie"
  ]
  node [
    id 972
    label "burying"
  ]
  node [
    id 973
    label "powk&#322;adanie"
  ]
  node [
    id 974
    label "zw&#322;oki"
  ]
  node [
    id 975
    label "burial"
  ]
  node [
    id 976
    label "w&#322;o&#380;enie"
  ]
  node [
    id 977
    label "gr&#243;b"
  ]
  node [
    id 978
    label "spocz&#281;cie"
  ]
  node [
    id 979
    label "zagadni&#281;cie"
  ]
  node [
    id 980
    label "napadni&#281;cie"
  ]
  node [
    id 981
    label "attack"
  ]
  node [
    id 982
    label "zaatakowanie"
  ]
  node [
    id 983
    label "odwiedzenie"
  ]
  node [
    id 984
    label "przymocowanie"
  ]
  node [
    id 985
    label "clasp"
  ]
  node [
    id 986
    label "zaczepianie"
  ]
  node [
    id 987
    label "podtrzymywanie"
  ]
  node [
    id 988
    label "w&#322;&#261;czanie"
  ]
  node [
    id 989
    label "uruchamianie"
  ]
  node [
    id 990
    label "nakr&#281;cenie"
  ]
  node [
    id 991
    label "uruchomienie"
  ]
  node [
    id 992
    label "funkcja"
  ]
  node [
    id 993
    label "nakr&#281;canie"
  ]
  node [
    id 994
    label "impact"
  ]
  node [
    id 995
    label "tr&#243;jstronny"
  ]
  node [
    id 996
    label "w&#322;&#261;czenie"
  ]
  node [
    id 997
    label "pogl&#261;d"
  ]
  node [
    id 998
    label "Jezus_Chrystus"
  ]
  node [
    id 999
    label "B&#243;g_Ojciec"
  ]
  node [
    id 1000
    label "Plotyn"
  ]
  node [
    id 1001
    label "Tr&#243;jca_&#346;wi&#281;ta"
  ]
  node [
    id 1002
    label "figura_my&#347;li"
  ]
  node [
    id 1003
    label "hypostasis"
  ]
  node [
    id 1004
    label "Duch_&#346;wi&#281;ty"
  ]
  node [
    id 1005
    label "wcielenie"
  ]
  node [
    id 1006
    label "stani&#281;cie"
  ]
  node [
    id 1007
    label "przedmiot"
  ]
  node [
    id 1008
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1009
    label "przyskrzynienie"
  ]
  node [
    id 1010
    label "przygaszenie"
  ]
  node [
    id 1011
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 1012
    label "profligacy"
  ]
  node [
    id 1013
    label "dissolution"
  ]
  node [
    id 1014
    label "miejsce"
  ]
  node [
    id 1015
    label "completion"
  ]
  node [
    id 1016
    label "uj&#281;cie"
  ]
  node [
    id 1017
    label "exit"
  ]
  node [
    id 1018
    label "rozwi&#261;zanie"
  ]
  node [
    id 1019
    label "zawarcie"
  ]
  node [
    id 1020
    label "closing"
  ]
  node [
    id 1021
    label "z&#322;o&#380;enie"
  ]
  node [
    id 1022
    label "release"
  ]
  node [
    id 1023
    label "umieszczenie"
  ]
  node [
    id 1024
    label "zablokowanie"
  ]
  node [
    id 1025
    label "pozamykanie"
  ]
  node [
    id 1026
    label "obieg"
  ]
  node [
    id 1027
    label "turn"
  ]
  node [
    id 1028
    label "proces_ekonomiczny"
  ]
  node [
    id 1029
    label "sprzeda&#380;"
  ]
  node [
    id 1030
    label "round"
  ]
  node [
    id 1031
    label "wp&#322;yw"
  ]
  node [
    id 1032
    label "rewizja"
  ]
  node [
    id 1033
    label "passage"
  ]
  node [
    id 1034
    label "change"
  ]
  node [
    id 1035
    label "ferment"
  ]
  node [
    id 1036
    label "anatomopatolog"
  ]
  node [
    id 1037
    label "zmianka"
  ]
  node [
    id 1038
    label "amendment"
  ]
  node [
    id 1039
    label "praca"
  ]
  node [
    id 1040
    label "odmienianie"
  ]
  node [
    id 1041
    label "tura"
  ]
  node [
    id 1042
    label "kwota"
  ]
  node [
    id 1043
    label "&#347;lad"
  ]
  node [
    id 1044
    label "rezultat"
  ]
  node [
    id 1045
    label "lobbysta"
  ]
  node [
    id 1046
    label "doch&#243;d_narodowy"
  ]
  node [
    id 1047
    label "przep&#322;yw"
  ]
  node [
    id 1048
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 1049
    label "tour"
  ]
  node [
    id 1050
    label "circulation"
  ]
  node [
    id 1051
    label "obecno&#347;&#263;"
  ]
  node [
    id 1052
    label "mechanika"
  ]
  node [
    id 1053
    label "utrzymywanie"
  ]
  node [
    id 1054
    label "move"
  ]
  node [
    id 1055
    label "poruszenie"
  ]
  node [
    id 1056
    label "movement"
  ]
  node [
    id 1057
    label "myk"
  ]
  node [
    id 1058
    label "utrzyma&#263;"
  ]
  node [
    id 1059
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1060
    label "travel"
  ]
  node [
    id 1061
    label "kanciasty"
  ]
  node [
    id 1062
    label "commercial_enterprise"
  ]
  node [
    id 1063
    label "model"
  ]
  node [
    id 1064
    label "strumie&#324;"
  ]
  node [
    id 1065
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1066
    label "taktyka"
  ]
  node [
    id 1067
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1068
    label "apraksja"
  ]
  node [
    id 1069
    label "natural_process"
  ]
  node [
    id 1070
    label "utrzymywa&#263;"
  ]
  node [
    id 1071
    label "d&#322;ugi"
  ]
  node [
    id 1072
    label "dyssypacja_energii"
  ]
  node [
    id 1073
    label "tumult"
  ]
  node [
    id 1074
    label "stopek"
  ]
  node [
    id 1075
    label "manewr"
  ]
  node [
    id 1076
    label "lokomocja"
  ]
  node [
    id 1077
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1078
    label "komunikacja"
  ]
  node [
    id 1079
    label "drift"
  ]
  node [
    id 1080
    label "przeniesienie_praw"
  ]
  node [
    id 1081
    label "przeda&#380;"
  ]
  node [
    id 1082
    label "transakcja"
  ]
  node [
    id 1083
    label "sprzedaj&#261;cy"
  ]
  node [
    id 1084
    label "rabat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 201
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 972
  ]
  edge [
    source 13
    target 973
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 975
  ]
  edge [
    source 13
    target 976
  ]
  edge [
    source 13
    target 977
  ]
  edge [
    source 13
    target 978
  ]
  edge [
    source 13
    target 979
  ]
  edge [
    source 13
    target 980
  ]
  edge [
    source 13
    target 981
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 983
  ]
  edge [
    source 13
    target 984
  ]
  edge [
    source 13
    target 985
  ]
  edge [
    source 13
    target 986
  ]
  edge [
    source 13
    target 987
  ]
  edge [
    source 13
    target 988
  ]
  edge [
    source 13
    target 989
  ]
  edge [
    source 13
    target 990
  ]
  edge [
    source 13
    target 991
  ]
  edge [
    source 13
    target 992
  ]
  edge [
    source 13
    target 993
  ]
  edge [
    source 13
    target 994
  ]
  edge [
    source 13
    target 995
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 996
  ]
  edge [
    source 13
    target 997
  ]
  edge [
    source 13
    target 998
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 999
  ]
  edge [
    source 13
    target 1000
  ]
  edge [
    source 13
    target 1001
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 1002
  ]
  edge [
    source 13
    target 1003
  ]
  edge [
    source 13
    target 1004
  ]
  edge [
    source 13
    target 1005
  ]
  edge [
    source 13
    target 1006
  ]
  edge [
    source 13
    target 1007
  ]
  edge [
    source 13
    target 1008
  ]
  edge [
    source 13
    target 1009
  ]
  edge [
    source 13
    target 1010
  ]
  edge [
    source 13
    target 1011
  ]
  edge [
    source 13
    target 1012
  ]
  edge [
    source 13
    target 1013
  ]
  edge [
    source 13
    target 1014
  ]
  edge [
    source 13
    target 1015
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 1016
  ]
  edge [
    source 13
    target 1017
  ]
  edge [
    source 13
    target 1018
  ]
  edge [
    source 13
    target 1019
  ]
  edge [
    source 13
    target 1020
  ]
  edge [
    source 13
    target 1021
  ]
  edge [
    source 13
    target 1022
  ]
  edge [
    source 13
    target 1023
  ]
  edge [
    source 13
    target 1024
  ]
  edge [
    source 13
    target 1025
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 14
    target 1029
  ]
  edge [
    source 14
    target 1030
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 1031
  ]
  edge [
    source 14
    target 1032
  ]
  edge [
    source 14
    target 1033
  ]
  edge [
    source 14
    target 316
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 236
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
]
