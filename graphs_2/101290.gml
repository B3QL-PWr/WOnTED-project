graph [
  node [
    id 0
    label "step"
    origin "text"
  ]
  node [
    id 1
    label "closer"
    origin "text"
  ]
  node [
    id 2
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 3
    label "biom"
  ]
  node [
    id 4
    label "teren"
  ]
  node [
    id 5
    label "r&#243;wnina"
  ]
  node [
    id 6
    label "formacja_ro&#347;linna"
  ]
  node [
    id 7
    label "taniec"
  ]
  node [
    id 8
    label "trawa"
  ]
  node [
    id 9
    label "Abakan"
  ]
  node [
    id 10
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 11
    label "Wielki_Step"
  ]
  node [
    id 12
    label "krok_taneczny"
  ]
  node [
    id 13
    label "zbi&#243;r"
  ]
  node [
    id 14
    label "karnet"
  ]
  node [
    id 15
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 16
    label "ruch"
  ]
  node [
    id 17
    label "parkiet"
  ]
  node [
    id 18
    label "choreologia"
  ]
  node [
    id 19
    label "czynno&#347;&#263;"
  ]
  node [
    id 20
    label "szata_ro&#347;linna"
  ]
  node [
    id 21
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 22
    label "przyroda"
  ]
  node [
    id 23
    label "zielono&#347;&#263;"
  ]
  node [
    id 24
    label "pi&#281;tro"
  ]
  node [
    id 25
    label "plant"
  ]
  node [
    id 26
    label "ro&#347;lina"
  ]
  node [
    id 27
    label "geosystem"
  ]
  node [
    id 28
    label "wymiar"
  ]
  node [
    id 29
    label "zakres"
  ]
  node [
    id 30
    label "kontekst"
  ]
  node [
    id 31
    label "miejsce_pracy"
  ]
  node [
    id 32
    label "nation"
  ]
  node [
    id 33
    label "krajobraz"
  ]
  node [
    id 34
    label "obszar"
  ]
  node [
    id 35
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 36
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 37
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "w&#322;adza"
  ]
  node [
    id 39
    label "fauna"
  ]
  node [
    id 40
    label "zbiorowisko"
  ]
  node [
    id 41
    label "wiechlinowate"
  ]
  node [
    id 42
    label "ziarno"
  ]
  node [
    id 43
    label "przestrze&#324;"
  ]
  node [
    id 44
    label "rastaman"
  ]
  node [
    id 45
    label "narkotyk_mi&#281;kki"
  ]
  node [
    id 46
    label "koleoryza"
  ]
  node [
    id 47
    label "p&#322;aszczyzna"
  ]
  node [
    id 48
    label "degree"
  ]
  node [
    id 49
    label "l&#261;d"
  ]
  node [
    id 50
    label "ukszta&#322;towanie"
  ]
  node [
    id 51
    label "obrazowanie"
  ]
  node [
    id 52
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 53
    label "organ"
  ]
  node [
    id 54
    label "tre&#347;&#263;"
  ]
  node [
    id 55
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 56
    label "part"
  ]
  node [
    id 57
    label "element_anatomiczny"
  ]
  node [
    id 58
    label "tekst"
  ]
  node [
    id 59
    label "komunikat"
  ]
  node [
    id 60
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 61
    label "dorobek"
  ]
  node [
    id 62
    label "tworzenie"
  ]
  node [
    id 63
    label "kreacja"
  ]
  node [
    id 64
    label "creation"
  ]
  node [
    id 65
    label "kultura"
  ]
  node [
    id 66
    label "ekscerpcja"
  ]
  node [
    id 67
    label "j&#281;zykowo"
  ]
  node [
    id 68
    label "wypowied&#378;"
  ]
  node [
    id 69
    label "redakcja"
  ]
  node [
    id 70
    label "wytw&#243;r"
  ]
  node [
    id 71
    label "pomini&#281;cie"
  ]
  node [
    id 72
    label "dzie&#322;o"
  ]
  node [
    id 73
    label "preparacja"
  ]
  node [
    id 74
    label "odmianka"
  ]
  node [
    id 75
    label "opu&#347;ci&#263;"
  ]
  node [
    id 76
    label "koniektura"
  ]
  node [
    id 77
    label "pisa&#263;"
  ]
  node [
    id 78
    label "obelga"
  ]
  node [
    id 79
    label "communication"
  ]
  node [
    id 80
    label "kreacjonista"
  ]
  node [
    id 81
    label "roi&#263;_si&#281;"
  ]
  node [
    id 82
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 83
    label "imaging"
  ]
  node [
    id 84
    label "przedstawianie"
  ]
  node [
    id 85
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 86
    label "temat"
  ]
  node [
    id 87
    label "istota"
  ]
  node [
    id 88
    label "informacja"
  ]
  node [
    id 89
    label "zawarto&#347;&#263;"
  ]
  node [
    id 90
    label "tkanka"
  ]
  node [
    id 91
    label "jednostka_organizacyjna"
  ]
  node [
    id 92
    label "budowa"
  ]
  node [
    id 93
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 94
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 95
    label "tw&#243;r"
  ]
  node [
    id 96
    label "organogeneza"
  ]
  node [
    id 97
    label "zesp&#243;&#322;"
  ]
  node [
    id 98
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 99
    label "struktura_anatomiczna"
  ]
  node [
    id 100
    label "uk&#322;ad"
  ]
  node [
    id 101
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 102
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 103
    label "Izba_Konsyliarska"
  ]
  node [
    id 104
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 105
    label "stomia"
  ]
  node [
    id 106
    label "dekortykacja"
  ]
  node [
    id 107
    label "okolica"
  ]
  node [
    id 108
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 109
    label "Komitet_Region&#243;w"
  ]
  node [
    id 110
    label "ta&#347;ma"
  ]
  node [
    id 111
    label "plecionka"
  ]
  node [
    id 112
    label "parciak"
  ]
  node [
    id 113
    label "p&#322;&#243;tno"
  ]
  node [
    id 114
    label "on"
  ]
  node [
    id 115
    label "Closer"
  ]
  node [
    id 116
    label "How"
  ]
  node [
    id 117
    label "to"
  ]
  node [
    id 118
    label "Dismantle"
  ]
  node [
    id 119
    label "an"
  ]
  node [
    id 120
    label "Atomic"
  ]
  node [
    id 121
    label "bomba"
  ]
  node [
    id 122
    label "Noela"
  ]
  node [
    id 123
    label "Gallaghera"
  ]
  node [
    id 124
    label "b&#243;b"
  ]
  node [
    id 125
    label "Hewsonie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 118
  ]
  edge [
    source 116
    target 119
  ]
  edge [
    source 116
    target 120
  ]
  edge [
    source 116
    target 121
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 119
  ]
  edge [
    source 117
    target 120
  ]
  edge [
    source 117
    target 121
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 120
  ]
  edge [
    source 118
    target 121
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 121
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 124
    target 125
  ]
]
