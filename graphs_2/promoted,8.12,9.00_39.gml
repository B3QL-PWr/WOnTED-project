graph [
  node [
    id 0
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "grosz"
    origin "text"
  ]
  node [
    id 4
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 5
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 6
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 7
    label "dysfonia"
  ]
  node [
    id 8
    label "prawi&#263;"
  ]
  node [
    id 9
    label "remark"
  ]
  node [
    id 10
    label "express"
  ]
  node [
    id 11
    label "chew_the_fat"
  ]
  node [
    id 12
    label "talk"
  ]
  node [
    id 13
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 14
    label "say"
  ]
  node [
    id 15
    label "wyra&#380;a&#263;"
  ]
  node [
    id 16
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 17
    label "j&#281;zyk"
  ]
  node [
    id 18
    label "tell"
  ]
  node [
    id 19
    label "informowa&#263;"
  ]
  node [
    id 20
    label "rozmawia&#263;"
  ]
  node [
    id 21
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "powiada&#263;"
  ]
  node [
    id 23
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 24
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 25
    label "okre&#347;la&#263;"
  ]
  node [
    id 26
    label "u&#380;ywa&#263;"
  ]
  node [
    id 27
    label "gaworzy&#263;"
  ]
  node [
    id 28
    label "formu&#322;owa&#263;"
  ]
  node [
    id 29
    label "dziama&#263;"
  ]
  node [
    id 30
    label "umie&#263;"
  ]
  node [
    id 31
    label "wydobywa&#263;"
  ]
  node [
    id 32
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 33
    label "korzysta&#263;"
  ]
  node [
    id 34
    label "doznawa&#263;"
  ]
  node [
    id 35
    label "distribute"
  ]
  node [
    id 36
    label "give"
  ]
  node [
    id 37
    label "bash"
  ]
  node [
    id 38
    label "style"
  ]
  node [
    id 39
    label "decydowa&#263;"
  ]
  node [
    id 40
    label "powodowa&#263;"
  ]
  node [
    id 41
    label "signify"
  ]
  node [
    id 42
    label "komunikowa&#263;"
  ]
  node [
    id 43
    label "inform"
  ]
  node [
    id 44
    label "oznacza&#263;"
  ]
  node [
    id 45
    label "znaczy&#263;"
  ]
  node [
    id 46
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 47
    label "convey"
  ]
  node [
    id 48
    label "arouse"
  ]
  node [
    id 49
    label "represent"
  ]
  node [
    id 50
    label "give_voice"
  ]
  node [
    id 51
    label "robi&#263;"
  ]
  node [
    id 52
    label "reakcja_chemiczna"
  ]
  node [
    id 53
    label "determine"
  ]
  node [
    id 54
    label "work"
  ]
  node [
    id 55
    label "wyjmowa&#263;"
  ]
  node [
    id 56
    label "wydostawa&#263;"
  ]
  node [
    id 57
    label "wydawa&#263;"
  ]
  node [
    id 58
    label "g&#243;rnictwo"
  ]
  node [
    id 59
    label "dobywa&#263;"
  ]
  node [
    id 60
    label "uwydatnia&#263;"
  ]
  node [
    id 61
    label "eksploatowa&#263;"
  ]
  node [
    id 62
    label "excavate"
  ]
  node [
    id 63
    label "raise"
  ]
  node [
    id 64
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 65
    label "train"
  ]
  node [
    id 66
    label "ocala&#263;"
  ]
  node [
    id 67
    label "uzyskiwa&#263;"
  ]
  node [
    id 68
    label "m&#243;c"
  ]
  node [
    id 69
    label "can"
  ]
  node [
    id 70
    label "wiedzie&#263;"
  ]
  node [
    id 71
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 72
    label "szczeka&#263;"
  ]
  node [
    id 73
    label "rozumie&#263;"
  ]
  node [
    id 74
    label "funkcjonowa&#263;"
  ]
  node [
    id 75
    label "mawia&#263;"
  ]
  node [
    id 76
    label "opowiada&#263;"
  ]
  node [
    id 77
    label "chatter"
  ]
  node [
    id 78
    label "niemowl&#281;"
  ]
  node [
    id 79
    label "kosmetyk"
  ]
  node [
    id 80
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 81
    label "stanowisko_archeologiczne"
  ]
  node [
    id 82
    label "pisa&#263;"
  ]
  node [
    id 83
    label "kod"
  ]
  node [
    id 84
    label "pype&#263;"
  ]
  node [
    id 85
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 86
    label "gramatyka"
  ]
  node [
    id 87
    label "language"
  ]
  node [
    id 88
    label "fonetyka"
  ]
  node [
    id 89
    label "t&#322;umaczenie"
  ]
  node [
    id 90
    label "artykulator"
  ]
  node [
    id 91
    label "rozumienie"
  ]
  node [
    id 92
    label "jama_ustna"
  ]
  node [
    id 93
    label "urz&#261;dzenie"
  ]
  node [
    id 94
    label "organ"
  ]
  node [
    id 95
    label "ssanie"
  ]
  node [
    id 96
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 97
    label "lizanie"
  ]
  node [
    id 98
    label "przedmiot"
  ]
  node [
    id 99
    label "liza&#263;"
  ]
  node [
    id 100
    label "makroglosja"
  ]
  node [
    id 101
    label "natural_language"
  ]
  node [
    id 102
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 103
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 104
    label "napisa&#263;"
  ]
  node [
    id 105
    label "m&#243;wienie"
  ]
  node [
    id 106
    label "s&#322;ownictwo"
  ]
  node [
    id 107
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 108
    label "konsonantyzm"
  ]
  node [
    id 109
    label "ssa&#263;"
  ]
  node [
    id 110
    label "wokalizm"
  ]
  node [
    id 111
    label "kultura_duchowa"
  ]
  node [
    id 112
    label "formalizowanie"
  ]
  node [
    id 113
    label "jeniec"
  ]
  node [
    id 114
    label "kawa&#322;ek"
  ]
  node [
    id 115
    label "po_koroniarsku"
  ]
  node [
    id 116
    label "stylik"
  ]
  node [
    id 117
    label "przet&#322;umaczenie"
  ]
  node [
    id 118
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 119
    label "formacja_geologiczna"
  ]
  node [
    id 120
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 121
    label "spos&#243;b"
  ]
  node [
    id 122
    label "but"
  ]
  node [
    id 123
    label "pismo"
  ]
  node [
    id 124
    label "formalizowa&#263;"
  ]
  node [
    id 125
    label "dysleksja"
  ]
  node [
    id 126
    label "dysphonia"
  ]
  node [
    id 127
    label "kieliszek"
  ]
  node [
    id 128
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 129
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 130
    label "w&#243;dka"
  ]
  node [
    id 131
    label "ujednolicenie"
  ]
  node [
    id 132
    label "ten"
  ]
  node [
    id 133
    label "jaki&#347;"
  ]
  node [
    id 134
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 135
    label "jednakowy"
  ]
  node [
    id 136
    label "jednolicie"
  ]
  node [
    id 137
    label "shot"
  ]
  node [
    id 138
    label "naczynie"
  ]
  node [
    id 139
    label "zawarto&#347;&#263;"
  ]
  node [
    id 140
    label "szk&#322;o"
  ]
  node [
    id 141
    label "mohorycz"
  ]
  node [
    id 142
    label "gorza&#322;ka"
  ]
  node [
    id 143
    label "alkohol"
  ]
  node [
    id 144
    label "sznaps"
  ]
  node [
    id 145
    label "nap&#243;j"
  ]
  node [
    id 146
    label "taki&#380;"
  ]
  node [
    id 147
    label "identyczny"
  ]
  node [
    id 148
    label "zr&#243;wnanie"
  ]
  node [
    id 149
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 150
    label "zr&#243;wnywanie"
  ]
  node [
    id 151
    label "mundurowanie"
  ]
  node [
    id 152
    label "mundurowa&#263;"
  ]
  node [
    id 153
    label "jednakowo"
  ]
  node [
    id 154
    label "okre&#347;lony"
  ]
  node [
    id 155
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 156
    label "z&#322;o&#380;ony"
  ]
  node [
    id 157
    label "jako&#347;"
  ]
  node [
    id 158
    label "niez&#322;y"
  ]
  node [
    id 159
    label "charakterystyczny"
  ]
  node [
    id 160
    label "jako_tako"
  ]
  node [
    id 161
    label "ciekawy"
  ]
  node [
    id 162
    label "dziwny"
  ]
  node [
    id 163
    label "przyzwoity"
  ]
  node [
    id 164
    label "g&#322;&#281;bszy"
  ]
  node [
    id 165
    label "drink"
  ]
  node [
    id 166
    label "jednolity"
  ]
  node [
    id 167
    label "upodobnienie"
  ]
  node [
    id 168
    label "calibration"
  ]
  node [
    id 169
    label "groszak"
  ]
  node [
    id 170
    label "z&#322;oty"
  ]
  node [
    id 171
    label "kwota"
  ]
  node [
    id 172
    label "szyling_austryjacki"
  ]
  node [
    id 173
    label "moneta"
  ]
  node [
    id 174
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 175
    label "rewers"
  ]
  node [
    id 176
    label "legenda"
  ]
  node [
    id 177
    label "liga"
  ]
  node [
    id 178
    label "balansjerka"
  ]
  node [
    id 179
    label "awers"
  ]
  node [
    id 180
    label "egzerga"
  ]
  node [
    id 181
    label "otok"
  ]
  node [
    id 182
    label "pieni&#261;dze"
  ]
  node [
    id 183
    label "wynie&#347;&#263;"
  ]
  node [
    id 184
    label "ilo&#347;&#263;"
  ]
  node [
    id 185
    label "limit"
  ]
  node [
    id 186
    label "wynosi&#263;"
  ]
  node [
    id 187
    label "drobniak"
  ]
  node [
    id 188
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 189
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 190
    label "doskona&#322;y"
  ]
  node [
    id 191
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 192
    label "szlachetny"
  ]
  node [
    id 193
    label "utytu&#322;owany"
  ]
  node [
    id 194
    label "kochany"
  ]
  node [
    id 195
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 196
    label "Polska"
  ]
  node [
    id 197
    label "z&#322;ocenie"
  ]
  node [
    id 198
    label "metaliczny"
  ]
  node [
    id 199
    label "jednostka_monetarna"
  ]
  node [
    id 200
    label "wspania&#322;y"
  ]
  node [
    id 201
    label "poz&#322;ocenie"
  ]
  node [
    id 202
    label "nijaki"
  ]
  node [
    id 203
    label "bezbarwnie"
  ]
  node [
    id 204
    label "oboj&#281;tny"
  ]
  node [
    id 205
    label "szarzenie"
  ]
  node [
    id 206
    label "zwyczajny"
  ]
  node [
    id 207
    label "neutralny"
  ]
  node [
    id 208
    label "poszarzenie"
  ]
  node [
    id 209
    label "niezabawny"
  ]
  node [
    id 210
    label "nijak"
  ]
  node [
    id 211
    label "nieciekawy"
  ]
  node [
    id 212
    label "zdewaluowa&#263;"
  ]
  node [
    id 213
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 214
    label "rozmieni&#263;"
  ]
  node [
    id 215
    label "rozmienianie"
  ]
  node [
    id 216
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 217
    label "dewaluowa&#263;"
  ]
  node [
    id 218
    label "numizmat"
  ]
  node [
    id 219
    label "zdewaluowanie"
  ]
  node [
    id 220
    label "dewaluowanie"
  ]
  node [
    id 221
    label "nomina&#322;"
  ]
  node [
    id 222
    label "rozmienia&#263;"
  ]
  node [
    id 223
    label "wytw&#243;r"
  ]
  node [
    id 224
    label "rozmienienie"
  ]
  node [
    id 225
    label "coin"
  ]
  node [
    id 226
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 227
    label "moniak"
  ]
  node [
    id 228
    label "rezultat"
  ]
  node [
    id 229
    label "p&#322;&#243;d"
  ]
  node [
    id 230
    label "drobne"
  ]
  node [
    id 231
    label "numismatics"
  ]
  node [
    id 232
    label "medal"
  ]
  node [
    id 233
    label "okaz"
  ]
  node [
    id 234
    label "cena"
  ]
  node [
    id 235
    label "par_value"
  ]
  node [
    id 236
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 237
    label "znaczek"
  ]
  node [
    id 238
    label "warto&#347;&#263;"
  ]
  node [
    id 239
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 240
    label "zast&#281;powanie"
  ]
  node [
    id 241
    label "wymienienie"
  ]
  node [
    id 242
    label "change"
  ]
  node [
    id 243
    label "zmieni&#263;"
  ]
  node [
    id 244
    label "zmienia&#263;"
  ]
  node [
    id 245
    label "alternate"
  ]
  node [
    id 246
    label "umniejszanie"
  ]
  node [
    id 247
    label "obni&#380;anie"
  ]
  node [
    id 248
    label "devaluation"
  ]
  node [
    id 249
    label "knock"
  ]
  node [
    id 250
    label "umniejsza&#263;"
  ]
  node [
    id 251
    label "obni&#380;a&#263;"
  ]
  node [
    id 252
    label "devalue"
  ]
  node [
    id 253
    label "umniejszy&#263;"
  ]
  node [
    id 254
    label "obni&#380;y&#263;"
  ]
  node [
    id 255
    label "depreciate"
  ]
  node [
    id 256
    label "obni&#380;enie"
  ]
  node [
    id 257
    label "adulteration"
  ]
  node [
    id 258
    label "umniejszenie"
  ]
  node [
    id 259
    label "kapanie"
  ]
  node [
    id 260
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 261
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 262
    label "kapn&#261;&#263;"
  ]
  node [
    id 263
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 264
    label "portfel"
  ]
  node [
    id 265
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 266
    label "forsa"
  ]
  node [
    id 267
    label "kapa&#263;"
  ]
  node [
    id 268
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 269
    label "kapita&#322;"
  ]
  node [
    id 270
    label "kapni&#281;cie"
  ]
  node [
    id 271
    label "wyda&#263;"
  ]
  node [
    id 272
    label "hajs"
  ]
  node [
    id 273
    label "dydki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
]
