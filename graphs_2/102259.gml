graph [
  node [
    id 0
    label "wroc&#322;awski"
    origin "text"
  ]
  node [
    id 1
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 2
    label "digit"
    origin "text"
  ]
  node [
    id 3
    label "all"
    origin "text"
  ]
  node [
    id 4
    label "love"
    origin "text"
  ]
  node [
    id 5
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 6
    label "p&#322;yta"
    origin "text"
  ]
  node [
    id 7
    label "dobry"
    origin "text"
  ]
  node [
    id 8
    label "muzyka"
    origin "text"
  ]
  node [
    id 9
    label "kilka"
    origin "text"
  ]
  node [
    id 10
    label "kawa&#322;ek"
    origin "text"
  ]
  node [
    id 11
    label "wideo"
    origin "text"
  ]
  node [
    id 12
    label "licencja"
    origin "text"
  ]
  node [
    id 13
    label "creative"
    origin "text"
  ]
  node [
    id 14
    label "commons"
    origin "text"
  ]
  node [
    id 15
    label "po_wroc&#322;awsku"
  ]
  node [
    id 16
    label "dolno&#347;l&#261;ski"
  ]
  node [
    id 17
    label "po_dolno&#347;l&#261;sku"
  ]
  node [
    id 18
    label "&#347;l&#261;ski"
  ]
  node [
    id 19
    label "Mazowsze"
  ]
  node [
    id 20
    label "odm&#322;adzanie"
  ]
  node [
    id 21
    label "&#346;wietliki"
  ]
  node [
    id 22
    label "zbi&#243;r"
  ]
  node [
    id 23
    label "whole"
  ]
  node [
    id 24
    label "skupienie"
  ]
  node [
    id 25
    label "The_Beatles"
  ]
  node [
    id 26
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 27
    label "odm&#322;adza&#263;"
  ]
  node [
    id 28
    label "zabudowania"
  ]
  node [
    id 29
    label "group"
  ]
  node [
    id 30
    label "zespolik"
  ]
  node [
    id 31
    label "schorzenie"
  ]
  node [
    id 32
    label "ro&#347;lina"
  ]
  node [
    id 33
    label "grupa"
  ]
  node [
    id 34
    label "Depeche_Mode"
  ]
  node [
    id 35
    label "batch"
  ]
  node [
    id 36
    label "odm&#322;odzenie"
  ]
  node [
    id 37
    label "liga"
  ]
  node [
    id 38
    label "jednostka_systematyczna"
  ]
  node [
    id 39
    label "asymilowanie"
  ]
  node [
    id 40
    label "gromada"
  ]
  node [
    id 41
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 42
    label "asymilowa&#263;"
  ]
  node [
    id 43
    label "egzemplarz"
  ]
  node [
    id 44
    label "Entuzjastki"
  ]
  node [
    id 45
    label "kompozycja"
  ]
  node [
    id 46
    label "Terranie"
  ]
  node [
    id 47
    label "category"
  ]
  node [
    id 48
    label "pakiet_klimatyczny"
  ]
  node [
    id 49
    label "oddzia&#322;"
  ]
  node [
    id 50
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 51
    label "cz&#261;steczka"
  ]
  node [
    id 52
    label "stage_set"
  ]
  node [
    id 53
    label "type"
  ]
  node [
    id 54
    label "specgrupa"
  ]
  node [
    id 55
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 56
    label "Eurogrupa"
  ]
  node [
    id 57
    label "formacja_geologiczna"
  ]
  node [
    id 58
    label "harcerze_starsi"
  ]
  node [
    id 59
    label "ognisko"
  ]
  node [
    id 60
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 61
    label "powalenie"
  ]
  node [
    id 62
    label "odezwanie_si&#281;"
  ]
  node [
    id 63
    label "atakowanie"
  ]
  node [
    id 64
    label "grupa_ryzyka"
  ]
  node [
    id 65
    label "przypadek"
  ]
  node [
    id 66
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 67
    label "nabawienie_si&#281;"
  ]
  node [
    id 68
    label "inkubacja"
  ]
  node [
    id 69
    label "kryzys"
  ]
  node [
    id 70
    label "powali&#263;"
  ]
  node [
    id 71
    label "remisja"
  ]
  node [
    id 72
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 73
    label "zajmowa&#263;"
  ]
  node [
    id 74
    label "zaburzenie"
  ]
  node [
    id 75
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 76
    label "badanie_histopatologiczne"
  ]
  node [
    id 77
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 78
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 79
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 80
    label "odzywanie_si&#281;"
  ]
  node [
    id 81
    label "diagnoza"
  ]
  node [
    id 82
    label "atakowa&#263;"
  ]
  node [
    id 83
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 84
    label "nabawianie_si&#281;"
  ]
  node [
    id 85
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 86
    label "zajmowanie"
  ]
  node [
    id 87
    label "series"
  ]
  node [
    id 88
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 89
    label "uprawianie"
  ]
  node [
    id 90
    label "praca_rolnicza"
  ]
  node [
    id 91
    label "collection"
  ]
  node [
    id 92
    label "dane"
  ]
  node [
    id 93
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 94
    label "poj&#281;cie"
  ]
  node [
    id 95
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 96
    label "sum"
  ]
  node [
    id 97
    label "gathering"
  ]
  node [
    id 98
    label "album"
  ]
  node [
    id 99
    label "agglomeration"
  ]
  node [
    id 100
    label "uwaga"
  ]
  node [
    id 101
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 102
    label "przegrupowanie"
  ]
  node [
    id 103
    label "spowodowanie"
  ]
  node [
    id 104
    label "congestion"
  ]
  node [
    id 105
    label "zgromadzenie"
  ]
  node [
    id 106
    label "kupienie"
  ]
  node [
    id 107
    label "z&#322;&#261;czenie"
  ]
  node [
    id 108
    label "czynno&#347;&#263;"
  ]
  node [
    id 109
    label "po&#322;&#261;czenie"
  ]
  node [
    id 110
    label "concentration"
  ]
  node [
    id 111
    label "kompleks"
  ]
  node [
    id 112
    label "obszar"
  ]
  node [
    id 113
    label "Polska"
  ]
  node [
    id 114
    label "Kurpie"
  ]
  node [
    id 115
    label "Mogielnica"
  ]
  node [
    id 116
    label "odtwarzanie"
  ]
  node [
    id 117
    label "uatrakcyjnianie"
  ]
  node [
    id 118
    label "zast&#281;powanie"
  ]
  node [
    id 119
    label "odbudowywanie"
  ]
  node [
    id 120
    label "rejuvenation"
  ]
  node [
    id 121
    label "m&#322;odszy"
  ]
  node [
    id 122
    label "odbudowywa&#263;"
  ]
  node [
    id 123
    label "m&#322;odzi&#263;"
  ]
  node [
    id 124
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 125
    label "przewietrza&#263;"
  ]
  node [
    id 126
    label "wymienia&#263;"
  ]
  node [
    id 127
    label "odtwarza&#263;"
  ]
  node [
    id 128
    label "uatrakcyjni&#263;"
  ]
  node [
    id 129
    label "przewietrzy&#263;"
  ]
  node [
    id 130
    label "regenerate"
  ]
  node [
    id 131
    label "odtworzy&#263;"
  ]
  node [
    id 132
    label "wymieni&#263;"
  ]
  node [
    id 133
    label "odbudowa&#263;"
  ]
  node [
    id 134
    label "wymienienie"
  ]
  node [
    id 135
    label "uatrakcyjnienie"
  ]
  node [
    id 136
    label "odbudowanie"
  ]
  node [
    id 137
    label "odtworzenie"
  ]
  node [
    id 138
    label "zbiorowisko"
  ]
  node [
    id 139
    label "ro&#347;liny"
  ]
  node [
    id 140
    label "p&#281;d"
  ]
  node [
    id 141
    label "wegetowanie"
  ]
  node [
    id 142
    label "zadziorek"
  ]
  node [
    id 143
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 144
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 145
    label "do&#322;owa&#263;"
  ]
  node [
    id 146
    label "wegetacja"
  ]
  node [
    id 147
    label "owoc"
  ]
  node [
    id 148
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 149
    label "strzyc"
  ]
  node [
    id 150
    label "w&#322;&#243;kno"
  ]
  node [
    id 151
    label "g&#322;uszenie"
  ]
  node [
    id 152
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 153
    label "fitotron"
  ]
  node [
    id 154
    label "bulwka"
  ]
  node [
    id 155
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 156
    label "odn&#243;&#380;ka"
  ]
  node [
    id 157
    label "epiderma"
  ]
  node [
    id 158
    label "gumoza"
  ]
  node [
    id 159
    label "strzy&#380;enie"
  ]
  node [
    id 160
    label "wypotnik"
  ]
  node [
    id 161
    label "flawonoid"
  ]
  node [
    id 162
    label "wyro&#347;le"
  ]
  node [
    id 163
    label "do&#322;owanie"
  ]
  node [
    id 164
    label "g&#322;uszy&#263;"
  ]
  node [
    id 165
    label "pora&#380;a&#263;"
  ]
  node [
    id 166
    label "fitocenoza"
  ]
  node [
    id 167
    label "hodowla"
  ]
  node [
    id 168
    label "fotoautotrof"
  ]
  node [
    id 169
    label "nieuleczalnie_chory"
  ]
  node [
    id 170
    label "wegetowa&#263;"
  ]
  node [
    id 171
    label "pochewka"
  ]
  node [
    id 172
    label "sok"
  ]
  node [
    id 173
    label "system_korzeniowy"
  ]
  node [
    id 174
    label "zawi&#261;zek"
  ]
  node [
    id 175
    label "powierzy&#263;"
  ]
  node [
    id 176
    label "pieni&#261;dze"
  ]
  node [
    id 177
    label "plon"
  ]
  node [
    id 178
    label "give"
  ]
  node [
    id 179
    label "skojarzy&#263;"
  ]
  node [
    id 180
    label "d&#378;wi&#281;k"
  ]
  node [
    id 181
    label "zadenuncjowa&#263;"
  ]
  node [
    id 182
    label "impart"
  ]
  node [
    id 183
    label "da&#263;"
  ]
  node [
    id 184
    label "reszta"
  ]
  node [
    id 185
    label "zapach"
  ]
  node [
    id 186
    label "wydawnictwo"
  ]
  node [
    id 187
    label "zrobi&#263;"
  ]
  node [
    id 188
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 189
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 190
    label "wiano"
  ]
  node [
    id 191
    label "produkcja"
  ]
  node [
    id 192
    label "translate"
  ]
  node [
    id 193
    label "picture"
  ]
  node [
    id 194
    label "poda&#263;"
  ]
  node [
    id 195
    label "wprowadzi&#263;"
  ]
  node [
    id 196
    label "wytworzy&#263;"
  ]
  node [
    id 197
    label "dress"
  ]
  node [
    id 198
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 199
    label "tajemnica"
  ]
  node [
    id 200
    label "panna_na_wydaniu"
  ]
  node [
    id 201
    label "supply"
  ]
  node [
    id 202
    label "ujawni&#263;"
  ]
  node [
    id 203
    label "rynek"
  ]
  node [
    id 204
    label "doprowadzi&#263;"
  ]
  node [
    id 205
    label "testify"
  ]
  node [
    id 206
    label "insert"
  ]
  node [
    id 207
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 208
    label "wpisa&#263;"
  ]
  node [
    id 209
    label "zapozna&#263;"
  ]
  node [
    id 210
    label "wej&#347;&#263;"
  ]
  node [
    id 211
    label "spowodowa&#263;"
  ]
  node [
    id 212
    label "zej&#347;&#263;"
  ]
  node [
    id 213
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 214
    label "umie&#347;ci&#263;"
  ]
  node [
    id 215
    label "zacz&#261;&#263;"
  ]
  node [
    id 216
    label "indicate"
  ]
  node [
    id 217
    label "post&#261;pi&#263;"
  ]
  node [
    id 218
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 219
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 220
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 221
    label "zorganizowa&#263;"
  ]
  node [
    id 222
    label "appoint"
  ]
  node [
    id 223
    label "wystylizowa&#263;"
  ]
  node [
    id 224
    label "cause"
  ]
  node [
    id 225
    label "przerobi&#263;"
  ]
  node [
    id 226
    label "nabra&#263;"
  ]
  node [
    id 227
    label "make"
  ]
  node [
    id 228
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 229
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 230
    label "wydali&#263;"
  ]
  node [
    id 231
    label "manufacture"
  ]
  node [
    id 232
    label "tenis"
  ]
  node [
    id 233
    label "ustawi&#263;"
  ]
  node [
    id 234
    label "siatk&#243;wka"
  ]
  node [
    id 235
    label "zagra&#263;"
  ]
  node [
    id 236
    label "jedzenie"
  ]
  node [
    id 237
    label "poinformowa&#263;"
  ]
  node [
    id 238
    label "introduce"
  ]
  node [
    id 239
    label "nafaszerowa&#263;"
  ]
  node [
    id 240
    label "zaserwowa&#263;"
  ]
  node [
    id 241
    label "donie&#347;&#263;"
  ]
  node [
    id 242
    label "denounce"
  ]
  node [
    id 243
    label "discover"
  ]
  node [
    id 244
    label "objawi&#263;"
  ]
  node [
    id 245
    label "dostrzec"
  ]
  node [
    id 246
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 247
    label "obieca&#263;"
  ]
  node [
    id 248
    label "pozwoli&#263;"
  ]
  node [
    id 249
    label "odst&#261;pi&#263;"
  ]
  node [
    id 250
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 251
    label "przywali&#263;"
  ]
  node [
    id 252
    label "wyrzec_si&#281;"
  ]
  node [
    id 253
    label "sztachn&#261;&#263;"
  ]
  node [
    id 254
    label "rap"
  ]
  node [
    id 255
    label "feed"
  ]
  node [
    id 256
    label "convey"
  ]
  node [
    id 257
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 258
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 259
    label "udost&#281;pni&#263;"
  ]
  node [
    id 260
    label "przeznaczy&#263;"
  ]
  node [
    id 261
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 262
    label "zada&#263;"
  ]
  node [
    id 263
    label "dostarczy&#263;"
  ]
  node [
    id 264
    label "przekaza&#263;"
  ]
  node [
    id 265
    label "doda&#263;"
  ]
  node [
    id 266
    label "zap&#322;aci&#263;"
  ]
  node [
    id 267
    label "consort"
  ]
  node [
    id 268
    label "powi&#261;za&#263;"
  ]
  node [
    id 269
    label "swat"
  ]
  node [
    id 270
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 271
    label "confide"
  ]
  node [
    id 272
    label "charge"
  ]
  node [
    id 273
    label "ufa&#263;"
  ]
  node [
    id 274
    label "odda&#263;"
  ]
  node [
    id 275
    label "entrust"
  ]
  node [
    id 276
    label "wyzna&#263;"
  ]
  node [
    id 277
    label "zleci&#263;"
  ]
  node [
    id 278
    label "consign"
  ]
  node [
    id 279
    label "phone"
  ]
  node [
    id 280
    label "wpadni&#281;cie"
  ]
  node [
    id 281
    label "wydawa&#263;"
  ]
  node [
    id 282
    label "zjawisko"
  ]
  node [
    id 283
    label "intonacja"
  ]
  node [
    id 284
    label "wpa&#347;&#263;"
  ]
  node [
    id 285
    label "note"
  ]
  node [
    id 286
    label "onomatopeja"
  ]
  node [
    id 287
    label "modalizm"
  ]
  node [
    id 288
    label "nadlecenie"
  ]
  node [
    id 289
    label "sound"
  ]
  node [
    id 290
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 291
    label "wpada&#263;"
  ]
  node [
    id 292
    label "solmizacja"
  ]
  node [
    id 293
    label "seria"
  ]
  node [
    id 294
    label "dobiec"
  ]
  node [
    id 295
    label "transmiter"
  ]
  node [
    id 296
    label "heksachord"
  ]
  node [
    id 297
    label "akcent"
  ]
  node [
    id 298
    label "wydanie"
  ]
  node [
    id 299
    label "repetycja"
  ]
  node [
    id 300
    label "brzmienie"
  ]
  node [
    id 301
    label "wpadanie"
  ]
  node [
    id 302
    label "liczba_kwantowa"
  ]
  node [
    id 303
    label "kosmetyk"
  ]
  node [
    id 304
    label "ciasto"
  ]
  node [
    id 305
    label "aromat"
  ]
  node [
    id 306
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 307
    label "puff"
  ]
  node [
    id 308
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 309
    label "przyprawa"
  ]
  node [
    id 310
    label "upojno&#347;&#263;"
  ]
  node [
    id 311
    label "owiewanie"
  ]
  node [
    id 312
    label "smak"
  ]
  node [
    id 313
    label "impreza"
  ]
  node [
    id 314
    label "realizacja"
  ]
  node [
    id 315
    label "tingel-tangel"
  ]
  node [
    id 316
    label "numer"
  ]
  node [
    id 317
    label "monta&#380;"
  ]
  node [
    id 318
    label "postprodukcja"
  ]
  node [
    id 319
    label "performance"
  ]
  node [
    id 320
    label "fabrication"
  ]
  node [
    id 321
    label "product"
  ]
  node [
    id 322
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 323
    label "uzysk"
  ]
  node [
    id 324
    label "rozw&#243;j"
  ]
  node [
    id 325
    label "dorobek"
  ]
  node [
    id 326
    label "kreacja"
  ]
  node [
    id 327
    label "trema"
  ]
  node [
    id 328
    label "creation"
  ]
  node [
    id 329
    label "kooperowa&#263;"
  ]
  node [
    id 330
    label "debit"
  ]
  node [
    id 331
    label "redaktor"
  ]
  node [
    id 332
    label "druk"
  ]
  node [
    id 333
    label "publikacja"
  ]
  node [
    id 334
    label "redakcja"
  ]
  node [
    id 335
    label "szata_graficzna"
  ]
  node [
    id 336
    label "firma"
  ]
  node [
    id 337
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 338
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 339
    label "poster"
  ]
  node [
    id 340
    label "return"
  ]
  node [
    id 341
    label "metr"
  ]
  node [
    id 342
    label "rezultat"
  ]
  node [
    id 343
    label "naturalia"
  ]
  node [
    id 344
    label "wypaplanie"
  ]
  node [
    id 345
    label "enigmat"
  ]
  node [
    id 346
    label "wiedza"
  ]
  node [
    id 347
    label "spos&#243;b"
  ]
  node [
    id 348
    label "zachowanie"
  ]
  node [
    id 349
    label "zachowywanie"
  ]
  node [
    id 350
    label "secret"
  ]
  node [
    id 351
    label "obowi&#261;zek"
  ]
  node [
    id 352
    label "dyskrecja"
  ]
  node [
    id 353
    label "informacja"
  ]
  node [
    id 354
    label "rzecz"
  ]
  node [
    id 355
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 356
    label "taj&#324;"
  ]
  node [
    id 357
    label "zachowa&#263;"
  ]
  node [
    id 358
    label "zachowywa&#263;"
  ]
  node [
    id 359
    label "portfel"
  ]
  node [
    id 360
    label "kwota"
  ]
  node [
    id 361
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 362
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 363
    label "forsa"
  ]
  node [
    id 364
    label "kapanie"
  ]
  node [
    id 365
    label "kapn&#261;&#263;"
  ]
  node [
    id 366
    label "kapa&#263;"
  ]
  node [
    id 367
    label "kapita&#322;"
  ]
  node [
    id 368
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 369
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 370
    label "kapni&#281;cie"
  ]
  node [
    id 371
    label "hajs"
  ]
  node [
    id 372
    label "dydki"
  ]
  node [
    id 373
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 374
    label "remainder"
  ]
  node [
    id 375
    label "pozosta&#322;y"
  ]
  node [
    id 376
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 377
    label "posa&#380;ek"
  ]
  node [
    id 378
    label "mienie"
  ]
  node [
    id 379
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 380
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 381
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 382
    label "kuchnia"
  ]
  node [
    id 383
    label "przedmiot"
  ]
  node [
    id 384
    label "nagranie"
  ]
  node [
    id 385
    label "AGD"
  ]
  node [
    id 386
    label "p&#322;ytoteka"
  ]
  node [
    id 387
    label "no&#347;nik_danych"
  ]
  node [
    id 388
    label "miejsce"
  ]
  node [
    id 389
    label "plate"
  ]
  node [
    id 390
    label "sheet"
  ]
  node [
    id 391
    label "dysk"
  ]
  node [
    id 392
    label "phonograph_record"
  ]
  node [
    id 393
    label "warunek_lokalowy"
  ]
  node [
    id 394
    label "plac"
  ]
  node [
    id 395
    label "location"
  ]
  node [
    id 396
    label "przestrze&#324;"
  ]
  node [
    id 397
    label "status"
  ]
  node [
    id 398
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 399
    label "chwila"
  ]
  node [
    id 400
    label "cia&#322;o"
  ]
  node [
    id 401
    label "cecha"
  ]
  node [
    id 402
    label "praca"
  ]
  node [
    id 403
    label "rz&#261;d"
  ]
  node [
    id 404
    label "zboczenie"
  ]
  node [
    id 405
    label "om&#243;wienie"
  ]
  node [
    id 406
    label "sponiewieranie"
  ]
  node [
    id 407
    label "discipline"
  ]
  node [
    id 408
    label "omawia&#263;"
  ]
  node [
    id 409
    label "kr&#261;&#380;enie"
  ]
  node [
    id 410
    label "tre&#347;&#263;"
  ]
  node [
    id 411
    label "robienie"
  ]
  node [
    id 412
    label "sponiewiera&#263;"
  ]
  node [
    id 413
    label "element"
  ]
  node [
    id 414
    label "entity"
  ]
  node [
    id 415
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 416
    label "tematyka"
  ]
  node [
    id 417
    label "w&#261;tek"
  ]
  node [
    id 418
    label "charakter"
  ]
  node [
    id 419
    label "zbaczanie"
  ]
  node [
    id 420
    label "program_nauczania"
  ]
  node [
    id 421
    label "om&#243;wi&#263;"
  ]
  node [
    id 422
    label "omawianie"
  ]
  node [
    id 423
    label "thing"
  ]
  node [
    id 424
    label "kultura"
  ]
  node [
    id 425
    label "istota"
  ]
  node [
    id 426
    label "zbacza&#263;"
  ]
  node [
    id 427
    label "zboczy&#263;"
  ]
  node [
    id 428
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 429
    label "wytw&#243;r"
  ]
  node [
    id 430
    label "wys&#322;uchanie"
  ]
  node [
    id 431
    label "utrwalenie"
  ]
  node [
    id 432
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 433
    label "recording"
  ]
  node [
    id 434
    label "kolekcja"
  ]
  node [
    id 435
    label "fonoteka"
  ]
  node [
    id 436
    label "zaj&#281;cie"
  ]
  node [
    id 437
    label "instytucja"
  ]
  node [
    id 438
    label "tajniki"
  ]
  node [
    id 439
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 440
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 441
    label "zaplecze"
  ]
  node [
    id 442
    label "pomieszczenie"
  ]
  node [
    id 443
    label "zlewozmywak"
  ]
  node [
    id 444
    label "gotowa&#263;"
  ]
  node [
    id 445
    label "ko&#322;o"
  ]
  node [
    id 446
    label "pami&#281;&#263;"
  ]
  node [
    id 447
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 448
    label "hard_disc"
  ]
  node [
    id 449
    label "klaster_dyskowy"
  ]
  node [
    id 450
    label "pier&#347;cie&#324;_w&#322;&#243;knisty"
  ]
  node [
    id 451
    label "komputer"
  ]
  node [
    id 452
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 453
    label "j&#261;dro_mia&#380;d&#380;yste"
  ]
  node [
    id 454
    label "chrz&#261;stka"
  ]
  node [
    id 455
    label "dobroczynny"
  ]
  node [
    id 456
    label "czw&#243;rka"
  ]
  node [
    id 457
    label "spokojny"
  ]
  node [
    id 458
    label "skuteczny"
  ]
  node [
    id 459
    label "&#347;mieszny"
  ]
  node [
    id 460
    label "mi&#322;y"
  ]
  node [
    id 461
    label "grzeczny"
  ]
  node [
    id 462
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 463
    label "powitanie"
  ]
  node [
    id 464
    label "dobrze"
  ]
  node [
    id 465
    label "ca&#322;y"
  ]
  node [
    id 466
    label "zwrot"
  ]
  node [
    id 467
    label "pomy&#347;lny"
  ]
  node [
    id 468
    label "moralny"
  ]
  node [
    id 469
    label "drogi"
  ]
  node [
    id 470
    label "pozytywny"
  ]
  node [
    id 471
    label "odpowiedni"
  ]
  node [
    id 472
    label "korzystny"
  ]
  node [
    id 473
    label "pos&#322;uszny"
  ]
  node [
    id 474
    label "moralnie"
  ]
  node [
    id 475
    label "warto&#347;ciowy"
  ]
  node [
    id 476
    label "etycznie"
  ]
  node [
    id 477
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 478
    label "nale&#380;ny"
  ]
  node [
    id 479
    label "nale&#380;yty"
  ]
  node [
    id 480
    label "typowy"
  ]
  node [
    id 481
    label "uprawniony"
  ]
  node [
    id 482
    label "zasadniczy"
  ]
  node [
    id 483
    label "stosownie"
  ]
  node [
    id 484
    label "taki"
  ]
  node [
    id 485
    label "charakterystyczny"
  ]
  node [
    id 486
    label "prawdziwy"
  ]
  node [
    id 487
    label "ten"
  ]
  node [
    id 488
    label "pozytywnie"
  ]
  node [
    id 489
    label "fajny"
  ]
  node [
    id 490
    label "dodatnio"
  ]
  node [
    id 491
    label "przyjemny"
  ]
  node [
    id 492
    label "po&#380;&#261;dany"
  ]
  node [
    id 493
    label "niepowa&#380;ny"
  ]
  node [
    id 494
    label "o&#347;mieszanie"
  ]
  node [
    id 495
    label "&#347;miesznie"
  ]
  node [
    id 496
    label "bawny"
  ]
  node [
    id 497
    label "o&#347;mieszenie"
  ]
  node [
    id 498
    label "dziwny"
  ]
  node [
    id 499
    label "nieadekwatny"
  ]
  node [
    id 500
    label "zale&#380;ny"
  ]
  node [
    id 501
    label "uleg&#322;y"
  ]
  node [
    id 502
    label "pos&#322;usznie"
  ]
  node [
    id 503
    label "grzecznie"
  ]
  node [
    id 504
    label "stosowny"
  ]
  node [
    id 505
    label "niewinny"
  ]
  node [
    id 506
    label "konserwatywny"
  ]
  node [
    id 507
    label "nijaki"
  ]
  node [
    id 508
    label "wolny"
  ]
  node [
    id 509
    label "uspokajanie_si&#281;"
  ]
  node [
    id 510
    label "bezproblemowy"
  ]
  node [
    id 511
    label "spokojnie"
  ]
  node [
    id 512
    label "uspokojenie_si&#281;"
  ]
  node [
    id 513
    label "cicho"
  ]
  node [
    id 514
    label "uspokojenie"
  ]
  node [
    id 515
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 516
    label "nietrudny"
  ]
  node [
    id 517
    label "uspokajanie"
  ]
  node [
    id 518
    label "korzystnie"
  ]
  node [
    id 519
    label "drogo"
  ]
  node [
    id 520
    label "cz&#322;owiek"
  ]
  node [
    id 521
    label "bliski"
  ]
  node [
    id 522
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 523
    label "przyjaciel"
  ]
  node [
    id 524
    label "jedyny"
  ]
  node [
    id 525
    label "du&#380;y"
  ]
  node [
    id 526
    label "zdr&#243;w"
  ]
  node [
    id 527
    label "calu&#347;ko"
  ]
  node [
    id 528
    label "kompletny"
  ]
  node [
    id 529
    label "&#380;ywy"
  ]
  node [
    id 530
    label "pe&#322;ny"
  ]
  node [
    id 531
    label "podobny"
  ]
  node [
    id 532
    label "ca&#322;o"
  ]
  node [
    id 533
    label "poskutkowanie"
  ]
  node [
    id 534
    label "sprawny"
  ]
  node [
    id 535
    label "skutecznie"
  ]
  node [
    id 536
    label "skutkowanie"
  ]
  node [
    id 537
    label "pomy&#347;lnie"
  ]
  node [
    id 538
    label "toto-lotek"
  ]
  node [
    id 539
    label "trafienie"
  ]
  node [
    id 540
    label "arkusz_drukarski"
  ]
  node [
    id 541
    label "&#322;&#243;dka"
  ]
  node [
    id 542
    label "four"
  ]
  node [
    id 543
    label "&#263;wiartka"
  ]
  node [
    id 544
    label "hotel"
  ]
  node [
    id 545
    label "cyfra"
  ]
  node [
    id 546
    label "pok&#243;j"
  ]
  node [
    id 547
    label "stopie&#324;"
  ]
  node [
    id 548
    label "obiekt"
  ]
  node [
    id 549
    label "minialbum"
  ]
  node [
    id 550
    label "osada"
  ]
  node [
    id 551
    label "p&#322;yta_winylowa"
  ]
  node [
    id 552
    label "blotka"
  ]
  node [
    id 553
    label "zaprz&#281;g"
  ]
  node [
    id 554
    label "przedtrzonowiec"
  ]
  node [
    id 555
    label "punkt"
  ]
  node [
    id 556
    label "turn"
  ]
  node [
    id 557
    label "turning"
  ]
  node [
    id 558
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 559
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 560
    label "skr&#281;t"
  ]
  node [
    id 561
    label "obr&#243;t"
  ]
  node [
    id 562
    label "fraza_czasownikowa"
  ]
  node [
    id 563
    label "jednostka_leksykalna"
  ]
  node [
    id 564
    label "zmiana"
  ]
  node [
    id 565
    label "wyra&#380;enie"
  ]
  node [
    id 566
    label "welcome"
  ]
  node [
    id 567
    label "spotkanie"
  ]
  node [
    id 568
    label "pozdrowienie"
  ]
  node [
    id 569
    label "zwyczaj"
  ]
  node [
    id 570
    label "greeting"
  ]
  node [
    id 571
    label "zdarzony"
  ]
  node [
    id 572
    label "odpowiednio"
  ]
  node [
    id 573
    label "odpowiadanie"
  ]
  node [
    id 574
    label "specjalny"
  ]
  node [
    id 575
    label "kochanek"
  ]
  node [
    id 576
    label "sk&#322;onny"
  ]
  node [
    id 577
    label "wybranek"
  ]
  node [
    id 578
    label "umi&#322;owany"
  ]
  node [
    id 579
    label "przyjemnie"
  ]
  node [
    id 580
    label "mi&#322;o"
  ]
  node [
    id 581
    label "kochanie"
  ]
  node [
    id 582
    label "dyplomata"
  ]
  node [
    id 583
    label "dobroczynnie"
  ]
  node [
    id 584
    label "lepiej"
  ]
  node [
    id 585
    label "wiele"
  ]
  node [
    id 586
    label "spo&#322;eczny"
  ]
  node [
    id 587
    label "wokalistyka"
  ]
  node [
    id 588
    label "wykonywanie"
  ]
  node [
    id 589
    label "muza"
  ]
  node [
    id 590
    label "wykonywa&#263;"
  ]
  node [
    id 591
    label "beatbox"
  ]
  node [
    id 592
    label "komponowa&#263;"
  ]
  node [
    id 593
    label "szko&#322;a"
  ]
  node [
    id 594
    label "komponowanie"
  ]
  node [
    id 595
    label "pasa&#380;"
  ]
  node [
    id 596
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 597
    label "notacja_muzyczna"
  ]
  node [
    id 598
    label "kontrapunkt"
  ]
  node [
    id 599
    label "nauka"
  ]
  node [
    id 600
    label "sztuka"
  ]
  node [
    id 601
    label "instrumentalistyka"
  ]
  node [
    id 602
    label "harmonia"
  ]
  node [
    id 603
    label "set"
  ]
  node [
    id 604
    label "kapela"
  ]
  node [
    id 605
    label "britpop"
  ]
  node [
    id 606
    label "p&#322;&#243;d"
  ]
  node [
    id 607
    label "work"
  ]
  node [
    id 608
    label "pr&#243;bowanie"
  ]
  node [
    id 609
    label "rola"
  ]
  node [
    id 610
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 611
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 612
    label "scena"
  ]
  node [
    id 613
    label "didaskalia"
  ]
  node [
    id 614
    label "czyn"
  ]
  node [
    id 615
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 616
    label "environment"
  ]
  node [
    id 617
    label "head"
  ]
  node [
    id 618
    label "scenariusz"
  ]
  node [
    id 619
    label "jednostka"
  ]
  node [
    id 620
    label "utw&#243;r"
  ]
  node [
    id 621
    label "kultura_duchowa"
  ]
  node [
    id 622
    label "fortel"
  ]
  node [
    id 623
    label "theatrical_performance"
  ]
  node [
    id 624
    label "ambala&#380;"
  ]
  node [
    id 625
    label "sprawno&#347;&#263;"
  ]
  node [
    id 626
    label "kobieta"
  ]
  node [
    id 627
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 628
    label "Faust"
  ]
  node [
    id 629
    label "scenografia"
  ]
  node [
    id 630
    label "ods&#322;ona"
  ]
  node [
    id 631
    label "pokaz"
  ]
  node [
    id 632
    label "ilo&#347;&#263;"
  ]
  node [
    id 633
    label "przedstawienie"
  ]
  node [
    id 634
    label "przedstawi&#263;"
  ]
  node [
    id 635
    label "Apollo"
  ]
  node [
    id 636
    label "przedstawianie"
  ]
  node [
    id 637
    label "przedstawia&#263;"
  ]
  node [
    id 638
    label "towar"
  ]
  node [
    id 639
    label "miasteczko_rowerowe"
  ]
  node [
    id 640
    label "porada"
  ]
  node [
    id 641
    label "fotowoltaika"
  ]
  node [
    id 642
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 643
    label "przem&#243;wienie"
  ]
  node [
    id 644
    label "nauki_o_poznaniu"
  ]
  node [
    id 645
    label "nomotetyczny"
  ]
  node [
    id 646
    label "systematyka"
  ]
  node [
    id 647
    label "proces"
  ]
  node [
    id 648
    label "typologia"
  ]
  node [
    id 649
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 650
    label "&#322;awa_szkolna"
  ]
  node [
    id 651
    label "nauki_penalne"
  ]
  node [
    id 652
    label "dziedzina"
  ]
  node [
    id 653
    label "imagineskopia"
  ]
  node [
    id 654
    label "teoria_naukowa"
  ]
  node [
    id 655
    label "inwentyka"
  ]
  node [
    id 656
    label "metodologia"
  ]
  node [
    id 657
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 658
    label "nauki_o_Ziemi"
  ]
  node [
    id 659
    label "boski"
  ]
  node [
    id 660
    label "krajobraz"
  ]
  node [
    id 661
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 662
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 663
    label "przywidzenie"
  ]
  node [
    id 664
    label "presence"
  ]
  node [
    id 665
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 666
    label "technika"
  ]
  node [
    id 667
    label "rytm"
  ]
  node [
    id 668
    label "pianistyka"
  ]
  node [
    id 669
    label "wiolinistyka"
  ]
  node [
    id 670
    label "pie&#347;niarstwo"
  ]
  node [
    id 671
    label "piosenkarstwo"
  ]
  node [
    id 672
    label "ch&#243;ralistyka"
  ]
  node [
    id 673
    label "polifonia"
  ]
  node [
    id 674
    label "linia_melodyczna"
  ]
  node [
    id 675
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 676
    label "pasowa&#263;"
  ]
  node [
    id 677
    label "pi&#281;kno"
  ]
  node [
    id 678
    label "wsp&#243;&#322;brzmienie"
  ]
  node [
    id 679
    label "porz&#261;dek"
  ]
  node [
    id 680
    label "instrument_d&#281;ty"
  ]
  node [
    id 681
    label "zgodno&#347;&#263;"
  ]
  node [
    id 682
    label "cisza"
  ]
  node [
    id 683
    label "harmonia_r&#281;czna"
  ]
  node [
    id 684
    label "zgoda"
  ]
  node [
    id 685
    label "g&#322;os"
  ]
  node [
    id 686
    label "unison"
  ]
  node [
    id 687
    label "inspiratorka"
  ]
  node [
    id 688
    label "banan"
  ]
  node [
    id 689
    label "talent"
  ]
  node [
    id 690
    label "Melpomena"
  ]
  node [
    id 691
    label "natchnienie"
  ]
  node [
    id 692
    label "bogini"
  ]
  node [
    id 693
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 694
    label "palma"
  ]
  node [
    id 695
    label "fold"
  ]
  node [
    id 696
    label "uk&#322;ada&#263;"
  ]
  node [
    id 697
    label "tworzy&#263;"
  ]
  node [
    id 698
    label "pos&#322;uchanie"
  ]
  node [
    id 699
    label "spe&#322;nienie"
  ]
  node [
    id 700
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 701
    label "hearing"
  ]
  node [
    id 702
    label "composing"
  ]
  node [
    id 703
    label "uk&#322;adanie"
  ]
  node [
    id 704
    label "tworzenie"
  ]
  node [
    id 705
    label "gem"
  ]
  node [
    id 706
    label "runda"
  ]
  node [
    id 707
    label "zestaw"
  ]
  node [
    id 708
    label "spe&#322;ni&#263;"
  ]
  node [
    id 709
    label "do&#347;wiadczenie"
  ]
  node [
    id 710
    label "teren_szko&#322;y"
  ]
  node [
    id 711
    label "Mickiewicz"
  ]
  node [
    id 712
    label "kwalifikacje"
  ]
  node [
    id 713
    label "podr&#281;cznik"
  ]
  node [
    id 714
    label "absolwent"
  ]
  node [
    id 715
    label "praktyka"
  ]
  node [
    id 716
    label "school"
  ]
  node [
    id 717
    label "system"
  ]
  node [
    id 718
    label "zda&#263;"
  ]
  node [
    id 719
    label "gabinet"
  ]
  node [
    id 720
    label "urszulanki"
  ]
  node [
    id 721
    label "sztuba"
  ]
  node [
    id 722
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 723
    label "przepisa&#263;"
  ]
  node [
    id 724
    label "form"
  ]
  node [
    id 725
    label "klasa"
  ]
  node [
    id 726
    label "lekcja"
  ]
  node [
    id 727
    label "metoda"
  ]
  node [
    id 728
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 729
    label "przepisanie"
  ]
  node [
    id 730
    label "czas"
  ]
  node [
    id 731
    label "skolaryzacja"
  ]
  node [
    id 732
    label "zdanie"
  ]
  node [
    id 733
    label "stopek"
  ]
  node [
    id 734
    label "sekretariat"
  ]
  node [
    id 735
    label "ideologia"
  ]
  node [
    id 736
    label "lesson"
  ]
  node [
    id 737
    label "niepokalanki"
  ]
  node [
    id 738
    label "siedziba"
  ]
  node [
    id 739
    label "szkolenie"
  ]
  node [
    id 740
    label "kara"
  ]
  node [
    id 741
    label "tablica"
  ]
  node [
    id 742
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 743
    label "robi&#263;"
  ]
  node [
    id 744
    label "wytwarza&#263;"
  ]
  node [
    id 745
    label "create"
  ]
  node [
    id 746
    label "zarz&#261;dzanie"
  ]
  node [
    id 747
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 748
    label "dopracowanie"
  ]
  node [
    id 749
    label "dzia&#322;anie"
  ]
  node [
    id 750
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 751
    label "urzeczywistnianie"
  ]
  node [
    id 752
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 753
    label "zaprz&#281;ganie"
  ]
  node [
    id 754
    label "pojawianie_si&#281;"
  ]
  node [
    id 755
    label "realization"
  ]
  node [
    id 756
    label "wyrabianie"
  ]
  node [
    id 757
    label "use"
  ]
  node [
    id 758
    label "gospodarka"
  ]
  node [
    id 759
    label "przepracowanie"
  ]
  node [
    id 760
    label "przepracowywanie"
  ]
  node [
    id 761
    label "awansowanie"
  ]
  node [
    id 762
    label "zapracowanie"
  ]
  node [
    id 763
    label "wyrobienie"
  ]
  node [
    id 764
    label "przej&#347;cie"
  ]
  node [
    id 765
    label "przep&#322;yw"
  ]
  node [
    id 766
    label "eskalator"
  ]
  node [
    id 767
    label "figura"
  ]
  node [
    id 768
    label "ozdobnik"
  ]
  node [
    id 769
    label "przenoszenie"
  ]
  node [
    id 770
    label "uje&#380;d&#380;anie"
  ]
  node [
    id 771
    label "koloratura"
  ]
  node [
    id 772
    label "centrum_handlowe"
  ]
  node [
    id 773
    label "posiew"
  ]
  node [
    id 774
    label "ryba"
  ]
  node [
    id 775
    label "&#347;ledziowate"
  ]
  node [
    id 776
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 777
    label "kr&#281;gowiec"
  ]
  node [
    id 778
    label "systemik"
  ]
  node [
    id 779
    label "doniczkowiec"
  ]
  node [
    id 780
    label "mi&#281;so"
  ]
  node [
    id 781
    label "patroszy&#263;"
  ]
  node [
    id 782
    label "rakowato&#347;&#263;"
  ]
  node [
    id 783
    label "w&#281;dkarstwo"
  ]
  node [
    id 784
    label "ryby"
  ]
  node [
    id 785
    label "fish"
  ]
  node [
    id 786
    label "linia_boczna"
  ]
  node [
    id 787
    label "tar&#322;o"
  ]
  node [
    id 788
    label "wyrostek_filtracyjny"
  ]
  node [
    id 789
    label "m&#281;tnooki"
  ]
  node [
    id 790
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 791
    label "pokrywa_skrzelowa"
  ]
  node [
    id 792
    label "ikra"
  ]
  node [
    id 793
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 794
    label "szczelina_skrzelowa"
  ]
  node [
    id 795
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 796
    label "kawa&#322;"
  ]
  node [
    id 797
    label "plot"
  ]
  node [
    id 798
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 799
    label "piece"
  ]
  node [
    id 800
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 801
    label "podp&#322;ywanie"
  ]
  node [
    id 802
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 803
    label "play"
  ]
  node [
    id 804
    label "&#380;art"
  ]
  node [
    id 805
    label "koncept"
  ]
  node [
    id 806
    label "sp&#322;ache&#263;"
  ]
  node [
    id 807
    label "spalenie"
  ]
  node [
    id 808
    label "mn&#243;stwo"
  ]
  node [
    id 809
    label "raptularz"
  ]
  node [
    id 810
    label "palenie"
  ]
  node [
    id 811
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 812
    label "gryps"
  ]
  node [
    id 813
    label "opowiadanie"
  ]
  node [
    id 814
    label "anecdote"
  ]
  node [
    id 815
    label "obrazowanie"
  ]
  node [
    id 816
    label "organ"
  ]
  node [
    id 817
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 818
    label "part"
  ]
  node [
    id 819
    label "element_anatomiczny"
  ]
  node [
    id 820
    label "tekst"
  ]
  node [
    id 821
    label "komunikat"
  ]
  node [
    id 822
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 823
    label "Rzym_Zachodni"
  ]
  node [
    id 824
    label "Rzym_Wschodni"
  ]
  node [
    id 825
    label "urz&#261;dzenie"
  ]
  node [
    id 826
    label "narracja"
  ]
  node [
    id 827
    label "przeby&#263;"
  ]
  node [
    id 828
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 829
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 830
    label "przebywanie"
  ]
  node [
    id 831
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 832
    label "dostawanie_si&#281;"
  ]
  node [
    id 833
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 834
    label "przebywa&#263;"
  ]
  node [
    id 835
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 836
    label "dostanie_si&#281;"
  ]
  node [
    id 837
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 838
    label "przebycie"
  ]
  node [
    id 839
    label "film"
  ]
  node [
    id 840
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 841
    label "wideokaseta"
  ]
  node [
    id 842
    label "odtwarzacz"
  ]
  node [
    id 843
    label "telekomunikacja"
  ]
  node [
    id 844
    label "cywilizacja"
  ]
  node [
    id 845
    label "engineering"
  ]
  node [
    id 846
    label "teletechnika"
  ]
  node [
    id 847
    label "mechanika_precyzyjna"
  ]
  node [
    id 848
    label "technologia"
  ]
  node [
    id 849
    label "animatronika"
  ]
  node [
    id 850
    label "odczulenie"
  ]
  node [
    id 851
    label "odczula&#263;"
  ]
  node [
    id 852
    label "blik"
  ]
  node [
    id 853
    label "odczuli&#263;"
  ]
  node [
    id 854
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 855
    label "block"
  ]
  node [
    id 856
    label "trawiarnia"
  ]
  node [
    id 857
    label "sklejarka"
  ]
  node [
    id 858
    label "uj&#281;cie"
  ]
  node [
    id 859
    label "filmoteka"
  ]
  node [
    id 860
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 861
    label "klatka"
  ]
  node [
    id 862
    label "rozbieg&#243;wka"
  ]
  node [
    id 863
    label "napisy"
  ]
  node [
    id 864
    label "ta&#347;ma"
  ]
  node [
    id 865
    label "odczulanie"
  ]
  node [
    id 866
    label "anamorfoza"
  ]
  node [
    id 867
    label "ty&#322;&#243;wka"
  ]
  node [
    id 868
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 869
    label "b&#322;ona"
  ]
  node [
    id 870
    label "emulsja_fotograficzna"
  ]
  node [
    id 871
    label "photograph"
  ]
  node [
    id 872
    label "czo&#322;&#243;wka"
  ]
  node [
    id 873
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 874
    label "sprz&#281;t_audio"
  ]
  node [
    id 875
    label "magnetowid"
  ]
  node [
    id 876
    label "wideoteka"
  ]
  node [
    id 877
    label "wypo&#380;yczalnia_wideo"
  ]
  node [
    id 878
    label "kaseta"
  ]
  node [
    id 879
    label "zezwolenie"
  ]
  node [
    id 880
    label "za&#347;wiadczenie"
  ]
  node [
    id 881
    label "licencjonowa&#263;"
  ]
  node [
    id 882
    label "rasowy"
  ]
  node [
    id 883
    label "pozwolenie"
  ]
  node [
    id 884
    label "prawo"
  ]
  node [
    id 885
    label "license"
  ]
  node [
    id 886
    label "decyzja"
  ]
  node [
    id 887
    label "zwalnianie_si&#281;"
  ]
  node [
    id 888
    label "authorization"
  ]
  node [
    id 889
    label "koncesjonowanie"
  ]
  node [
    id 890
    label "zwolnienie_si&#281;"
  ]
  node [
    id 891
    label "pozwole&#324;stwo"
  ]
  node [
    id 892
    label "bycie_w_stanie"
  ]
  node [
    id 893
    label "odwieszenie"
  ]
  node [
    id 894
    label "odpowied&#378;"
  ]
  node [
    id 895
    label "pofolgowanie"
  ]
  node [
    id 896
    label "franchise"
  ]
  node [
    id 897
    label "umo&#380;liwienie"
  ]
  node [
    id 898
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 899
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 900
    label "dokument"
  ]
  node [
    id 901
    label "uznanie"
  ]
  node [
    id 902
    label "zrobienie"
  ]
  node [
    id 903
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 904
    label "umocowa&#263;"
  ]
  node [
    id 905
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 906
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 907
    label "procesualistyka"
  ]
  node [
    id 908
    label "regu&#322;a_Allena"
  ]
  node [
    id 909
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 910
    label "kryminalistyka"
  ]
  node [
    id 911
    label "struktura"
  ]
  node [
    id 912
    label "kierunek"
  ]
  node [
    id 913
    label "zasada_d'Alemberta"
  ]
  node [
    id 914
    label "obserwacja"
  ]
  node [
    id 915
    label "normatywizm"
  ]
  node [
    id 916
    label "jurisprudence"
  ]
  node [
    id 917
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 918
    label "przepis"
  ]
  node [
    id 919
    label "prawo_karne_procesowe"
  ]
  node [
    id 920
    label "criterion"
  ]
  node [
    id 921
    label "kazuistyka"
  ]
  node [
    id 922
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 923
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 924
    label "kryminologia"
  ]
  node [
    id 925
    label "opis"
  ]
  node [
    id 926
    label "regu&#322;a_Glogera"
  ]
  node [
    id 927
    label "prawo_Mendla"
  ]
  node [
    id 928
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 929
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 930
    label "prawo_karne"
  ]
  node [
    id 931
    label "legislacyjnie"
  ]
  node [
    id 932
    label "twierdzenie"
  ]
  node [
    id 933
    label "cywilistyka"
  ]
  node [
    id 934
    label "judykatura"
  ]
  node [
    id 935
    label "kanonistyka"
  ]
  node [
    id 936
    label "standard"
  ]
  node [
    id 937
    label "nauka_prawa"
  ]
  node [
    id 938
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 939
    label "podmiot"
  ]
  node [
    id 940
    label "law"
  ]
  node [
    id 941
    label "qualification"
  ]
  node [
    id 942
    label "dominion"
  ]
  node [
    id 943
    label "wykonawczy"
  ]
  node [
    id 944
    label "zasada"
  ]
  node [
    id 945
    label "normalizacja"
  ]
  node [
    id 946
    label "potwierdzenie"
  ]
  node [
    id 947
    label "certificate"
  ]
  node [
    id 948
    label "akceptowa&#263;"
  ]
  node [
    id 949
    label "udzieli&#263;"
  ]
  node [
    id 950
    label "udziela&#263;"
  ]
  node [
    id 951
    label "potrzymanie"
  ]
  node [
    id 952
    label "rolnictwo"
  ]
  node [
    id 953
    label "pod&#243;j"
  ]
  node [
    id 954
    label "filiacja"
  ]
  node [
    id 955
    label "licencjonowanie"
  ]
  node [
    id 956
    label "opasa&#263;"
  ]
  node [
    id 957
    label "ch&#243;w"
  ]
  node [
    id 958
    label "sokolarnia"
  ]
  node [
    id 959
    label "potrzyma&#263;"
  ]
  node [
    id 960
    label "rozp&#322;&#243;d"
  ]
  node [
    id 961
    label "grupa_organizm&#243;w"
  ]
  node [
    id 962
    label "wypas"
  ]
  node [
    id 963
    label "wychowalnia"
  ]
  node [
    id 964
    label "pstr&#261;garnia"
  ]
  node [
    id 965
    label "krzy&#380;owanie"
  ]
  node [
    id 966
    label "odch&#243;w"
  ]
  node [
    id 967
    label "tucz"
  ]
  node [
    id 968
    label "ud&#243;j"
  ]
  node [
    id 969
    label "opasienie"
  ]
  node [
    id 970
    label "wych&#243;w"
  ]
  node [
    id 971
    label "obrz&#261;dek"
  ]
  node [
    id 972
    label "opasanie"
  ]
  node [
    id 973
    label "polish"
  ]
  node [
    id 974
    label "akwarium"
  ]
  node [
    id 975
    label "biotechnika"
  ]
  node [
    id 976
    label "wspania&#322;y"
  ]
  node [
    id 977
    label "wytrawny"
  ]
  node [
    id 978
    label "szlachetny"
  ]
  node [
    id 979
    label "arystokratycznie"
  ]
  node [
    id 980
    label "rasowo"
  ]
  node [
    id 981
    label "markowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 13
    target 14
  ]
]
