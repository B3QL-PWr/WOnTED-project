graph [
  node [
    id 0
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 1
    label "wczoraj"
    origin "text"
  ]
  node [
    id 2
    label "min&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "bardzo"
    origin "text"
  ]
  node [
    id 6
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 8
    label "byby&#263;"
    origin "text"
  ]
  node [
    id 9
    label "deklarowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 12
    label "forma"
    origin "text"
  ]
  node [
    id 13
    label "tylko"
    origin "text"
  ]
  node [
    id 14
    label "us&#322;ysze&#263;by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "nie"
    origin "text"
  ]
  node [
    id 16
    label "dama"
    origin "text"
  ]
  node [
    id 17
    label "taki"
    origin "text"
  ]
  node [
    id 18
    label "z&#322;apa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "realny"
  ]
  node [
    id 20
    label "naprawd&#281;"
  ]
  node [
    id 21
    label "prawdziwy"
  ]
  node [
    id 22
    label "podobny"
  ]
  node [
    id 23
    label "mo&#380;liwy"
  ]
  node [
    id 24
    label "realnie"
  ]
  node [
    id 25
    label "dawno"
  ]
  node [
    id 26
    label "doba"
  ]
  node [
    id 27
    label "niedawno"
  ]
  node [
    id 28
    label "aktualnie"
  ]
  node [
    id 29
    label "ostatni"
  ]
  node [
    id 30
    label "dawny"
  ]
  node [
    id 31
    label "d&#322;ugotrwale"
  ]
  node [
    id 32
    label "wcze&#347;niej"
  ]
  node [
    id 33
    label "ongi&#347;"
  ]
  node [
    id 34
    label "dawnie"
  ]
  node [
    id 35
    label "tydzie&#324;"
  ]
  node [
    id 36
    label "noc"
  ]
  node [
    id 37
    label "dzie&#324;"
  ]
  node [
    id 38
    label "czas"
  ]
  node [
    id 39
    label "godzina"
  ]
  node [
    id 40
    label "long_time"
  ]
  node [
    id 41
    label "jednostka_geologiczna"
  ]
  node [
    id 42
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 43
    label "przesta&#263;"
  ]
  node [
    id 44
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 45
    label "run"
  ]
  node [
    id 46
    label "die"
  ]
  node [
    id 47
    label "przej&#347;&#263;"
  ]
  node [
    id 48
    label "spowodowa&#263;"
  ]
  node [
    id 49
    label "overwhelm"
  ]
  node [
    id 50
    label "omin&#261;&#263;"
  ]
  node [
    id 51
    label "coating"
  ]
  node [
    id 52
    label "drop"
  ]
  node [
    id 53
    label "sko&#324;czy&#263;"
  ]
  node [
    id 54
    label "leave_office"
  ]
  node [
    id 55
    label "zrobi&#263;"
  ]
  node [
    id 56
    label "fail"
  ]
  node [
    id 57
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 58
    label "act"
  ]
  node [
    id 59
    label "ustawa"
  ]
  node [
    id 60
    label "podlec"
  ]
  node [
    id 61
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 62
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 63
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 64
    label "zaliczy&#263;"
  ]
  node [
    id 65
    label "zmieni&#263;"
  ]
  node [
    id 66
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 67
    label "przeby&#263;"
  ]
  node [
    id 68
    label "dozna&#263;"
  ]
  node [
    id 69
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 70
    label "zacz&#261;&#263;"
  ]
  node [
    id 71
    label "happen"
  ]
  node [
    id 72
    label "pass"
  ]
  node [
    id 73
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 74
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 75
    label "beat"
  ]
  node [
    id 76
    label "mienie"
  ]
  node [
    id 77
    label "absorb"
  ]
  node [
    id 78
    label "przerobi&#263;"
  ]
  node [
    id 79
    label "pique"
  ]
  node [
    id 80
    label "pomin&#261;&#263;"
  ]
  node [
    id 81
    label "wymin&#261;&#263;"
  ]
  node [
    id 82
    label "sidestep"
  ]
  node [
    id 83
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 84
    label "unikn&#261;&#263;"
  ]
  node [
    id 85
    label "obej&#347;&#263;"
  ]
  node [
    id 86
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 87
    label "opu&#347;ci&#263;"
  ]
  node [
    id 88
    label "straci&#263;"
  ]
  node [
    id 89
    label "shed"
  ]
  node [
    id 90
    label "popyt"
  ]
  node [
    id 91
    label "p&#243;&#322;rocze"
  ]
  node [
    id 92
    label "martwy_sezon"
  ]
  node [
    id 93
    label "kalendarz"
  ]
  node [
    id 94
    label "cykl_astronomiczny"
  ]
  node [
    id 95
    label "lata"
  ]
  node [
    id 96
    label "pora_roku"
  ]
  node [
    id 97
    label "stulecie"
  ]
  node [
    id 98
    label "kurs"
  ]
  node [
    id 99
    label "jubileusz"
  ]
  node [
    id 100
    label "grupa"
  ]
  node [
    id 101
    label "kwarta&#322;"
  ]
  node [
    id 102
    label "miesi&#261;c"
  ]
  node [
    id 103
    label "summer"
  ]
  node [
    id 104
    label "odm&#322;adzanie"
  ]
  node [
    id 105
    label "liga"
  ]
  node [
    id 106
    label "jednostka_systematyczna"
  ]
  node [
    id 107
    label "asymilowanie"
  ]
  node [
    id 108
    label "gromada"
  ]
  node [
    id 109
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 110
    label "asymilowa&#263;"
  ]
  node [
    id 111
    label "egzemplarz"
  ]
  node [
    id 112
    label "Entuzjastki"
  ]
  node [
    id 113
    label "zbi&#243;r"
  ]
  node [
    id 114
    label "kompozycja"
  ]
  node [
    id 115
    label "Terranie"
  ]
  node [
    id 116
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 117
    label "category"
  ]
  node [
    id 118
    label "pakiet_klimatyczny"
  ]
  node [
    id 119
    label "oddzia&#322;"
  ]
  node [
    id 120
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 121
    label "cz&#261;steczka"
  ]
  node [
    id 122
    label "stage_set"
  ]
  node [
    id 123
    label "type"
  ]
  node [
    id 124
    label "specgrupa"
  ]
  node [
    id 125
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 126
    label "&#346;wietliki"
  ]
  node [
    id 127
    label "odm&#322;odzenie"
  ]
  node [
    id 128
    label "Eurogrupa"
  ]
  node [
    id 129
    label "odm&#322;adza&#263;"
  ]
  node [
    id 130
    label "formacja_geologiczna"
  ]
  node [
    id 131
    label "harcerze_starsi"
  ]
  node [
    id 132
    label "poprzedzanie"
  ]
  node [
    id 133
    label "czasoprzestrze&#324;"
  ]
  node [
    id 134
    label "laba"
  ]
  node [
    id 135
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 136
    label "chronometria"
  ]
  node [
    id 137
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 138
    label "rachuba_czasu"
  ]
  node [
    id 139
    label "przep&#322;ywanie"
  ]
  node [
    id 140
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 141
    label "czasokres"
  ]
  node [
    id 142
    label "odczyt"
  ]
  node [
    id 143
    label "chwila"
  ]
  node [
    id 144
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 145
    label "dzieje"
  ]
  node [
    id 146
    label "kategoria_gramatyczna"
  ]
  node [
    id 147
    label "poprzedzenie"
  ]
  node [
    id 148
    label "trawienie"
  ]
  node [
    id 149
    label "pochodzi&#263;"
  ]
  node [
    id 150
    label "period"
  ]
  node [
    id 151
    label "okres_czasu"
  ]
  node [
    id 152
    label "poprzedza&#263;"
  ]
  node [
    id 153
    label "schy&#322;ek"
  ]
  node [
    id 154
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 155
    label "odwlekanie_si&#281;"
  ]
  node [
    id 156
    label "zegar"
  ]
  node [
    id 157
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 158
    label "czwarty_wymiar"
  ]
  node [
    id 159
    label "pochodzenie"
  ]
  node [
    id 160
    label "koniugacja"
  ]
  node [
    id 161
    label "Zeitgeist"
  ]
  node [
    id 162
    label "trawi&#263;"
  ]
  node [
    id 163
    label "pogoda"
  ]
  node [
    id 164
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 165
    label "poprzedzi&#263;"
  ]
  node [
    id 166
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 167
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 168
    label "time_period"
  ]
  node [
    id 169
    label "term"
  ]
  node [
    id 170
    label "rok_akademicki"
  ]
  node [
    id 171
    label "rok_szkolny"
  ]
  node [
    id 172
    label "semester"
  ]
  node [
    id 173
    label "anniwersarz"
  ]
  node [
    id 174
    label "rocznica"
  ]
  node [
    id 175
    label "obszar"
  ]
  node [
    id 176
    label "miech"
  ]
  node [
    id 177
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 178
    label "kalendy"
  ]
  node [
    id 179
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 180
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 181
    label "almanac"
  ]
  node [
    id 182
    label "rozk&#322;ad"
  ]
  node [
    id 183
    label "wydawnictwo"
  ]
  node [
    id 184
    label "Juliusz_Cezar"
  ]
  node [
    id 185
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 186
    label "zwy&#380;kowanie"
  ]
  node [
    id 187
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 188
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 189
    label "zaj&#281;cia"
  ]
  node [
    id 190
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 191
    label "trasa"
  ]
  node [
    id 192
    label "przeorientowywanie"
  ]
  node [
    id 193
    label "przejazd"
  ]
  node [
    id 194
    label "kierunek"
  ]
  node [
    id 195
    label "przeorientowywa&#263;"
  ]
  node [
    id 196
    label "nauka"
  ]
  node [
    id 197
    label "przeorientowanie"
  ]
  node [
    id 198
    label "klasa"
  ]
  node [
    id 199
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 200
    label "przeorientowa&#263;"
  ]
  node [
    id 201
    label "manner"
  ]
  node [
    id 202
    label "course"
  ]
  node [
    id 203
    label "passage"
  ]
  node [
    id 204
    label "zni&#380;kowanie"
  ]
  node [
    id 205
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 206
    label "seria"
  ]
  node [
    id 207
    label "stawka"
  ]
  node [
    id 208
    label "way"
  ]
  node [
    id 209
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 210
    label "spos&#243;b"
  ]
  node [
    id 211
    label "deprecjacja"
  ]
  node [
    id 212
    label "cedu&#322;a"
  ]
  node [
    id 213
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 214
    label "drive"
  ]
  node [
    id 215
    label "bearing"
  ]
  node [
    id 216
    label "Lira"
  ]
  node [
    id 217
    label "cognizance"
  ]
  node [
    id 218
    label "w_chuj"
  ]
  node [
    id 219
    label "stworzy&#263;"
  ]
  node [
    id 220
    label "read"
  ]
  node [
    id 221
    label "styl"
  ]
  node [
    id 222
    label "postawi&#263;"
  ]
  node [
    id 223
    label "write"
  ]
  node [
    id 224
    label "donie&#347;&#263;"
  ]
  node [
    id 225
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 226
    label "prasa"
  ]
  node [
    id 227
    label "zafundowa&#263;"
  ]
  node [
    id 228
    label "budowla"
  ]
  node [
    id 229
    label "wyda&#263;"
  ]
  node [
    id 230
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 231
    label "plant"
  ]
  node [
    id 232
    label "uruchomi&#263;"
  ]
  node [
    id 233
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 234
    label "pozostawi&#263;"
  ]
  node [
    id 235
    label "obra&#263;"
  ]
  node [
    id 236
    label "peddle"
  ]
  node [
    id 237
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 238
    label "obstawi&#263;"
  ]
  node [
    id 239
    label "post"
  ]
  node [
    id 240
    label "wyznaczy&#263;"
  ]
  node [
    id 241
    label "oceni&#263;"
  ]
  node [
    id 242
    label "stanowisko"
  ]
  node [
    id 243
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 244
    label "uczyni&#263;"
  ]
  node [
    id 245
    label "znak"
  ]
  node [
    id 246
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 247
    label "wytworzy&#263;"
  ]
  node [
    id 248
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 249
    label "umie&#347;ci&#263;"
  ]
  node [
    id 250
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 251
    label "set"
  ]
  node [
    id 252
    label "wskaza&#263;"
  ]
  node [
    id 253
    label "przyzna&#263;"
  ]
  node [
    id 254
    label "wydoby&#263;"
  ]
  node [
    id 255
    label "przedstawi&#263;"
  ]
  node [
    id 256
    label "establish"
  ]
  node [
    id 257
    label "stawi&#263;"
  ]
  node [
    id 258
    label "create"
  ]
  node [
    id 259
    label "specjalista_od_public_relations"
  ]
  node [
    id 260
    label "wizerunek"
  ]
  node [
    id 261
    label "przygotowa&#263;"
  ]
  node [
    id 262
    label "zakomunikowa&#263;"
  ]
  node [
    id 263
    label "testify"
  ]
  node [
    id 264
    label "przytacha&#263;"
  ]
  node [
    id 265
    label "yield"
  ]
  node [
    id 266
    label "zanie&#347;&#263;"
  ]
  node [
    id 267
    label "inform"
  ]
  node [
    id 268
    label "poinformowa&#263;"
  ]
  node [
    id 269
    label "get"
  ]
  node [
    id 270
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 271
    label "denounce"
  ]
  node [
    id 272
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 273
    label "serve"
  ]
  node [
    id 274
    label "trzonek"
  ]
  node [
    id 275
    label "reakcja"
  ]
  node [
    id 276
    label "narz&#281;dzie"
  ]
  node [
    id 277
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 278
    label "zachowanie"
  ]
  node [
    id 279
    label "stylik"
  ]
  node [
    id 280
    label "dyscyplina_sportowa"
  ]
  node [
    id 281
    label "handle"
  ]
  node [
    id 282
    label "stroke"
  ]
  node [
    id 283
    label "line"
  ]
  node [
    id 284
    label "charakter"
  ]
  node [
    id 285
    label "natural_language"
  ]
  node [
    id 286
    label "pisa&#263;"
  ]
  node [
    id 287
    label "kanon"
  ]
  node [
    id 288
    label "behawior"
  ]
  node [
    id 289
    label "zesp&#243;&#322;"
  ]
  node [
    id 290
    label "t&#322;oczysko"
  ]
  node [
    id 291
    label "depesza"
  ]
  node [
    id 292
    label "maszyna"
  ]
  node [
    id 293
    label "media"
  ]
  node [
    id 294
    label "czasopismo"
  ]
  node [
    id 295
    label "dziennikarz_prasowy"
  ]
  node [
    id 296
    label "kiosk"
  ]
  node [
    id 297
    label "maszyna_rolnicza"
  ]
  node [
    id 298
    label "gazeta"
  ]
  node [
    id 299
    label "echo"
  ]
  node [
    id 300
    label "pilnowa&#263;"
  ]
  node [
    id 301
    label "robi&#263;"
  ]
  node [
    id 302
    label "recall"
  ]
  node [
    id 303
    label "si&#281;ga&#263;"
  ]
  node [
    id 304
    label "take_care"
  ]
  node [
    id 305
    label "troska&#263;_si&#281;"
  ]
  node [
    id 306
    label "chowa&#263;"
  ]
  node [
    id 307
    label "zachowywa&#263;"
  ]
  node [
    id 308
    label "zna&#263;"
  ]
  node [
    id 309
    label "think"
  ]
  node [
    id 310
    label "report"
  ]
  node [
    id 311
    label "hide"
  ]
  node [
    id 312
    label "znosi&#263;"
  ]
  node [
    id 313
    label "czu&#263;"
  ]
  node [
    id 314
    label "train"
  ]
  node [
    id 315
    label "przetrzymywa&#263;"
  ]
  node [
    id 316
    label "hodowa&#263;"
  ]
  node [
    id 317
    label "meliniarz"
  ]
  node [
    id 318
    label "umieszcza&#263;"
  ]
  node [
    id 319
    label "ukrywa&#263;"
  ]
  node [
    id 320
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 321
    label "continue"
  ]
  node [
    id 322
    label "wk&#322;ada&#263;"
  ]
  node [
    id 323
    label "tajemnica"
  ]
  node [
    id 324
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 325
    label "zdyscyplinowanie"
  ]
  node [
    id 326
    label "podtrzymywa&#263;"
  ]
  node [
    id 327
    label "control"
  ]
  node [
    id 328
    label "przechowywa&#263;"
  ]
  node [
    id 329
    label "behave"
  ]
  node [
    id 330
    label "dieta"
  ]
  node [
    id 331
    label "hold"
  ]
  node [
    id 332
    label "post&#281;powa&#263;"
  ]
  node [
    id 333
    label "compass"
  ]
  node [
    id 334
    label "korzysta&#263;"
  ]
  node [
    id 335
    label "appreciation"
  ]
  node [
    id 336
    label "osi&#261;ga&#263;"
  ]
  node [
    id 337
    label "dociera&#263;"
  ]
  node [
    id 338
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 339
    label "mierzy&#263;"
  ]
  node [
    id 340
    label "u&#380;ywa&#263;"
  ]
  node [
    id 341
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 342
    label "exsert"
  ]
  node [
    id 343
    label "organizowa&#263;"
  ]
  node [
    id 344
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 345
    label "czyni&#263;"
  ]
  node [
    id 346
    label "give"
  ]
  node [
    id 347
    label "stylizowa&#263;"
  ]
  node [
    id 348
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 349
    label "falowa&#263;"
  ]
  node [
    id 350
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 351
    label "praca"
  ]
  node [
    id 352
    label "wydala&#263;"
  ]
  node [
    id 353
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 354
    label "tentegowa&#263;"
  ]
  node [
    id 355
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 356
    label "urz&#261;dza&#263;"
  ]
  node [
    id 357
    label "oszukiwa&#263;"
  ]
  node [
    id 358
    label "work"
  ]
  node [
    id 359
    label "ukazywa&#263;"
  ]
  node [
    id 360
    label "przerabia&#263;"
  ]
  node [
    id 361
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 362
    label "resonance"
  ]
  node [
    id 363
    label "zjawisko"
  ]
  node [
    id 364
    label "obiecywa&#263;"
  ]
  node [
    id 365
    label "podawa&#263;"
  ]
  node [
    id 366
    label "poda&#263;"
  ]
  node [
    id 367
    label "zapewnia&#263;"
  ]
  node [
    id 368
    label "obieca&#263;"
  ]
  node [
    id 369
    label "sign"
  ]
  node [
    id 370
    label "bespeak"
  ]
  node [
    id 371
    label "zapewni&#263;"
  ]
  node [
    id 372
    label "harbinger"
  ]
  node [
    id 373
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 374
    label "pledge"
  ]
  node [
    id 375
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 376
    label "vow"
  ]
  node [
    id 377
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 378
    label "dostarcza&#263;"
  ]
  node [
    id 379
    label "informowa&#263;"
  ]
  node [
    id 380
    label "deliver"
  ]
  node [
    id 381
    label "utrzymywa&#263;"
  ]
  node [
    id 382
    label "translate"
  ]
  node [
    id 383
    label "tenis"
  ]
  node [
    id 384
    label "supply"
  ]
  node [
    id 385
    label "da&#263;"
  ]
  node [
    id 386
    label "ustawi&#263;"
  ]
  node [
    id 387
    label "siatk&#243;wka"
  ]
  node [
    id 388
    label "zagra&#263;"
  ]
  node [
    id 389
    label "jedzenie"
  ]
  node [
    id 390
    label "introduce"
  ]
  node [
    id 391
    label "nafaszerowa&#263;"
  ]
  node [
    id 392
    label "zaserwowa&#263;"
  ]
  node [
    id 393
    label "deal"
  ]
  node [
    id 394
    label "dawa&#263;"
  ]
  node [
    id 395
    label "stawia&#263;"
  ]
  node [
    id 396
    label "rozgrywa&#263;"
  ]
  node [
    id 397
    label "kelner"
  ]
  node [
    id 398
    label "cover"
  ]
  node [
    id 399
    label "tender"
  ]
  node [
    id 400
    label "faszerowa&#263;"
  ]
  node [
    id 401
    label "serwowa&#263;"
  ]
  node [
    id 402
    label "point"
  ]
  node [
    id 403
    label "udowodni&#263;"
  ]
  node [
    id 404
    label "wyrazi&#263;"
  ]
  node [
    id 405
    label "przeszkoli&#263;"
  ]
  node [
    id 406
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 407
    label "indicate"
  ]
  node [
    id 408
    label "pom&#243;c"
  ]
  node [
    id 409
    label "uzasadni&#263;"
  ]
  node [
    id 410
    label "oznaczy&#263;"
  ]
  node [
    id 411
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 412
    label "vent"
  ]
  node [
    id 413
    label "ukaza&#263;"
  ]
  node [
    id 414
    label "przedstawienie"
  ]
  node [
    id 415
    label "zapozna&#263;"
  ]
  node [
    id 416
    label "express"
  ]
  node [
    id 417
    label "represent"
  ]
  node [
    id 418
    label "zaproponowa&#263;"
  ]
  node [
    id 419
    label "zademonstrowa&#263;"
  ]
  node [
    id 420
    label "typify"
  ]
  node [
    id 421
    label "opisa&#263;"
  ]
  node [
    id 422
    label "kszta&#322;t"
  ]
  node [
    id 423
    label "temat"
  ]
  node [
    id 424
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 425
    label "poznanie"
  ]
  node [
    id 426
    label "leksem"
  ]
  node [
    id 427
    label "dzie&#322;o"
  ]
  node [
    id 428
    label "stan"
  ]
  node [
    id 429
    label "blaszka"
  ]
  node [
    id 430
    label "poj&#281;cie"
  ]
  node [
    id 431
    label "kantyzm"
  ]
  node [
    id 432
    label "zdolno&#347;&#263;"
  ]
  node [
    id 433
    label "cecha"
  ]
  node [
    id 434
    label "do&#322;ek"
  ]
  node [
    id 435
    label "zawarto&#347;&#263;"
  ]
  node [
    id 436
    label "gwiazda"
  ]
  node [
    id 437
    label "formality"
  ]
  node [
    id 438
    label "struktura"
  ]
  node [
    id 439
    label "wygl&#261;d"
  ]
  node [
    id 440
    label "mode"
  ]
  node [
    id 441
    label "morfem"
  ]
  node [
    id 442
    label "rdze&#324;"
  ]
  node [
    id 443
    label "posta&#263;"
  ]
  node [
    id 444
    label "kielich"
  ]
  node [
    id 445
    label "ornamentyka"
  ]
  node [
    id 446
    label "pasmo"
  ]
  node [
    id 447
    label "zwyczaj"
  ]
  node [
    id 448
    label "punkt_widzenia"
  ]
  node [
    id 449
    label "g&#322;owa"
  ]
  node [
    id 450
    label "naczynie"
  ]
  node [
    id 451
    label "p&#322;at"
  ]
  node [
    id 452
    label "maszyna_drukarska"
  ]
  node [
    id 453
    label "obiekt"
  ]
  node [
    id 454
    label "style"
  ]
  node [
    id 455
    label "linearno&#347;&#263;"
  ]
  node [
    id 456
    label "wyra&#380;enie"
  ]
  node [
    id 457
    label "formacja"
  ]
  node [
    id 458
    label "spirala"
  ]
  node [
    id 459
    label "dyspozycja"
  ]
  node [
    id 460
    label "odmiana"
  ]
  node [
    id 461
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 462
    label "wz&#243;r"
  ]
  node [
    id 463
    label "October"
  ]
  node [
    id 464
    label "creation"
  ]
  node [
    id 465
    label "p&#281;tla"
  ]
  node [
    id 466
    label "arystotelizm"
  ]
  node [
    id 467
    label "szablon"
  ]
  node [
    id 468
    label "miniatura"
  ]
  node [
    id 469
    label "acquaintance"
  ]
  node [
    id 470
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 471
    label "spotkanie"
  ]
  node [
    id 472
    label "nauczenie_si&#281;"
  ]
  node [
    id 473
    label "poczucie"
  ]
  node [
    id 474
    label "knowing"
  ]
  node [
    id 475
    label "zapoznanie_si&#281;"
  ]
  node [
    id 476
    label "wy&#347;wiadczenie"
  ]
  node [
    id 477
    label "inclusion"
  ]
  node [
    id 478
    label "zrozumienie"
  ]
  node [
    id 479
    label "zawarcie"
  ]
  node [
    id 480
    label "designation"
  ]
  node [
    id 481
    label "umo&#380;liwienie"
  ]
  node [
    id 482
    label "sensing"
  ]
  node [
    id 483
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 484
    label "gathering"
  ]
  node [
    id 485
    label "czynno&#347;&#263;"
  ]
  node [
    id 486
    label "zapoznanie"
  ]
  node [
    id 487
    label "znajomy"
  ]
  node [
    id 488
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 489
    label "zrobienie"
  ]
  node [
    id 490
    label "obrazowanie"
  ]
  node [
    id 491
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 492
    label "dorobek"
  ]
  node [
    id 493
    label "tre&#347;&#263;"
  ]
  node [
    id 494
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 495
    label "retrospektywa"
  ]
  node [
    id 496
    label "works"
  ]
  node [
    id 497
    label "tekst"
  ]
  node [
    id 498
    label "tetralogia"
  ]
  node [
    id 499
    label "komunikat"
  ]
  node [
    id 500
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 501
    label "mutant"
  ]
  node [
    id 502
    label "rewizja"
  ]
  node [
    id 503
    label "gramatyka"
  ]
  node [
    id 504
    label "typ"
  ]
  node [
    id 505
    label "paradygmat"
  ]
  node [
    id 506
    label "change"
  ]
  node [
    id 507
    label "podgatunek"
  ]
  node [
    id 508
    label "ferment"
  ]
  node [
    id 509
    label "rasa"
  ]
  node [
    id 510
    label "pos&#322;uchanie"
  ]
  node [
    id 511
    label "skumanie"
  ]
  node [
    id 512
    label "orientacja"
  ]
  node [
    id 513
    label "wytw&#243;r"
  ]
  node [
    id 514
    label "teoria"
  ]
  node [
    id 515
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 516
    label "clasp"
  ]
  node [
    id 517
    label "przem&#243;wienie"
  ]
  node [
    id 518
    label "zorientowanie"
  ]
  node [
    id 519
    label "idealizm"
  ]
  node [
    id 520
    label "szko&#322;a"
  ]
  node [
    id 521
    label "koncepcja"
  ]
  node [
    id 522
    label "imperatyw_kategoryczny"
  ]
  node [
    id 523
    label "przedmiot"
  ]
  node [
    id 524
    label "kopia"
  ]
  node [
    id 525
    label "utw&#243;r"
  ]
  node [
    id 526
    label "obraz"
  ]
  node [
    id 527
    label "ilustracja"
  ]
  node [
    id 528
    label "miniature"
  ]
  node [
    id 529
    label "roztruchan"
  ]
  node [
    id 530
    label "dzia&#322;ka"
  ]
  node [
    id 531
    label "kwiat"
  ]
  node [
    id 532
    label "puch_kielichowy"
  ]
  node [
    id 533
    label "Graal"
  ]
  node [
    id 534
    label "akrobacja_lotnicza"
  ]
  node [
    id 535
    label "whirl"
  ]
  node [
    id 536
    label "krzywa"
  ]
  node [
    id 537
    label "spiralny"
  ]
  node [
    id 538
    label "nagromadzenie"
  ]
  node [
    id 539
    label "wk&#322;adka"
  ]
  node [
    id 540
    label "spirograf"
  ]
  node [
    id 541
    label "spiral"
  ]
  node [
    id 542
    label "organizacja"
  ]
  node [
    id 543
    label "postarzenie"
  ]
  node [
    id 544
    label "postarzanie"
  ]
  node [
    id 545
    label "brzydota"
  ]
  node [
    id 546
    label "portrecista"
  ]
  node [
    id 547
    label "postarza&#263;"
  ]
  node [
    id 548
    label "nadawanie"
  ]
  node [
    id 549
    label "postarzy&#263;"
  ]
  node [
    id 550
    label "widok"
  ]
  node [
    id 551
    label "prostota"
  ]
  node [
    id 552
    label "ubarwienie"
  ]
  node [
    id 553
    label "shape"
  ]
  node [
    id 554
    label "comeliness"
  ]
  node [
    id 555
    label "face"
  ]
  node [
    id 556
    label "sid&#322;a"
  ]
  node [
    id 557
    label "ko&#322;o"
  ]
  node [
    id 558
    label "p&#281;tlica"
  ]
  node [
    id 559
    label "hank"
  ]
  node [
    id 560
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 561
    label "zawi&#261;zywanie"
  ]
  node [
    id 562
    label "z&#322;&#261;czenie"
  ]
  node [
    id 563
    label "zawi&#261;zanie"
  ]
  node [
    id 564
    label "arrest"
  ]
  node [
    id 565
    label "zawi&#261;za&#263;"
  ]
  node [
    id 566
    label "koniec"
  ]
  node [
    id 567
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 568
    label "signal"
  ]
  node [
    id 569
    label "li&#347;&#263;"
  ]
  node [
    id 570
    label "tw&#243;r"
  ]
  node [
    id 571
    label "odznaczenie"
  ]
  node [
    id 572
    label "kapelusz"
  ]
  node [
    id 573
    label "kawa&#322;ek"
  ]
  node [
    id 574
    label "centrop&#322;at"
  ]
  node [
    id 575
    label "organ"
  ]
  node [
    id 576
    label "airfoil"
  ]
  node [
    id 577
    label "&#347;mig&#322;owiec"
  ]
  node [
    id 578
    label "samolot"
  ]
  node [
    id 579
    label "piece"
  ]
  node [
    id 580
    label "plaster"
  ]
  node [
    id 581
    label "Arktur"
  ]
  node [
    id 582
    label "Gwiazda_Polarna"
  ]
  node [
    id 583
    label "agregatka"
  ]
  node [
    id 584
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 585
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 586
    label "S&#322;o&#324;ce"
  ]
  node [
    id 587
    label "Nibiru"
  ]
  node [
    id 588
    label "konstelacja"
  ]
  node [
    id 589
    label "ornament"
  ]
  node [
    id 590
    label "delta_Scuti"
  ]
  node [
    id 591
    label "&#347;wiat&#322;o"
  ]
  node [
    id 592
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 593
    label "s&#322;awa"
  ]
  node [
    id 594
    label "promie&#324;"
  ]
  node [
    id 595
    label "star"
  ]
  node [
    id 596
    label "gwiazdosz"
  ]
  node [
    id 597
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 598
    label "asocjacja_gwiazd"
  ]
  node [
    id 599
    label "supergrupa"
  ]
  node [
    id 600
    label "pryncypa&#322;"
  ]
  node [
    id 601
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 602
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 603
    label "wiedza"
  ]
  node [
    id 604
    label "cz&#322;owiek"
  ]
  node [
    id 605
    label "kierowa&#263;"
  ]
  node [
    id 606
    label "alkohol"
  ]
  node [
    id 607
    label "&#380;ycie"
  ]
  node [
    id 608
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 609
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 610
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 611
    label "sztuka"
  ]
  node [
    id 612
    label "dekiel"
  ]
  node [
    id 613
    label "ro&#347;lina"
  ]
  node [
    id 614
    label "&#347;ci&#281;cie"
  ]
  node [
    id 615
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 616
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 617
    label "&#347;ci&#281;gno"
  ]
  node [
    id 618
    label "noosfera"
  ]
  node [
    id 619
    label "byd&#322;o"
  ]
  node [
    id 620
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 621
    label "makrocefalia"
  ]
  node [
    id 622
    label "ucho"
  ]
  node [
    id 623
    label "g&#243;ra"
  ]
  node [
    id 624
    label "m&#243;zg"
  ]
  node [
    id 625
    label "kierownictwo"
  ]
  node [
    id 626
    label "fryzura"
  ]
  node [
    id 627
    label "umys&#322;"
  ]
  node [
    id 628
    label "cia&#322;o"
  ]
  node [
    id 629
    label "cz&#322;onek"
  ]
  node [
    id 630
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 631
    label "czaszka"
  ]
  node [
    id 632
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 633
    label "przebieg"
  ]
  node [
    id 634
    label "wydarzenie"
  ]
  node [
    id 635
    label "pas"
  ]
  node [
    id 636
    label "swath"
  ]
  node [
    id 637
    label "streak"
  ]
  node [
    id 638
    label "kana&#322;"
  ]
  node [
    id 639
    label "strip"
  ]
  node [
    id 640
    label "ulica"
  ]
  node [
    id 641
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 642
    label "sk&#322;ada&#263;"
  ]
  node [
    id 643
    label "odmawia&#263;"
  ]
  node [
    id 644
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 645
    label "wyra&#380;a&#263;"
  ]
  node [
    id 646
    label "thank"
  ]
  node [
    id 647
    label "etykieta"
  ]
  node [
    id 648
    label "areszt"
  ]
  node [
    id 649
    label "golf"
  ]
  node [
    id 650
    label "faza"
  ]
  node [
    id 651
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 652
    label "l&#261;d"
  ]
  node [
    id 653
    label "depressive_disorder"
  ]
  node [
    id 654
    label "bruzda"
  ]
  node [
    id 655
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 656
    label "Pampa"
  ]
  node [
    id 657
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 658
    label "odlewnictwo"
  ]
  node [
    id 659
    label "za&#322;amanie"
  ]
  node [
    id 660
    label "tomizm"
  ]
  node [
    id 661
    label "potencja"
  ]
  node [
    id 662
    label "akt"
  ]
  node [
    id 663
    label "kalokagatia"
  ]
  node [
    id 664
    label "wordnet"
  ]
  node [
    id 665
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 666
    label "wypowiedzenie"
  ]
  node [
    id 667
    label "s&#322;ownictwo"
  ]
  node [
    id 668
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 669
    label "wykrzyknik"
  ]
  node [
    id 670
    label "pole_semantyczne"
  ]
  node [
    id 671
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 672
    label "pisanie_si&#281;"
  ]
  node [
    id 673
    label "nag&#322;os"
  ]
  node [
    id 674
    label "wyg&#322;os"
  ]
  node [
    id 675
    label "jednostka_leksykalna"
  ]
  node [
    id 676
    label "charakterystyka"
  ]
  node [
    id 677
    label "zaistnie&#263;"
  ]
  node [
    id 678
    label "Osjan"
  ]
  node [
    id 679
    label "kto&#347;"
  ]
  node [
    id 680
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 681
    label "osobowo&#347;&#263;"
  ]
  node [
    id 682
    label "trim"
  ]
  node [
    id 683
    label "poby&#263;"
  ]
  node [
    id 684
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 685
    label "Aspazja"
  ]
  node [
    id 686
    label "kompleksja"
  ]
  node [
    id 687
    label "wytrzyma&#263;"
  ]
  node [
    id 688
    label "budowa"
  ]
  node [
    id 689
    label "pozosta&#263;"
  ]
  node [
    id 690
    label "go&#347;&#263;"
  ]
  node [
    id 691
    label "ilo&#347;&#263;"
  ]
  node [
    id 692
    label "wn&#281;trze"
  ]
  node [
    id 693
    label "informacja"
  ]
  node [
    id 694
    label "Ohio"
  ]
  node [
    id 695
    label "wci&#281;cie"
  ]
  node [
    id 696
    label "Nowy_York"
  ]
  node [
    id 697
    label "warstwa"
  ]
  node [
    id 698
    label "samopoczucie"
  ]
  node [
    id 699
    label "Illinois"
  ]
  node [
    id 700
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 701
    label "state"
  ]
  node [
    id 702
    label "Jukatan"
  ]
  node [
    id 703
    label "Kalifornia"
  ]
  node [
    id 704
    label "Wirginia"
  ]
  node [
    id 705
    label "wektor"
  ]
  node [
    id 706
    label "by&#263;"
  ]
  node [
    id 707
    label "Teksas"
  ]
  node [
    id 708
    label "Goa"
  ]
  node [
    id 709
    label "Waszyngton"
  ]
  node [
    id 710
    label "miejsce"
  ]
  node [
    id 711
    label "Massachusetts"
  ]
  node [
    id 712
    label "Alaska"
  ]
  node [
    id 713
    label "Arakan"
  ]
  node [
    id 714
    label "Hawaje"
  ]
  node [
    id 715
    label "Maryland"
  ]
  node [
    id 716
    label "punkt"
  ]
  node [
    id 717
    label "Michigan"
  ]
  node [
    id 718
    label "Arizona"
  ]
  node [
    id 719
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 720
    label "Georgia"
  ]
  node [
    id 721
    label "poziom"
  ]
  node [
    id 722
    label "Pensylwania"
  ]
  node [
    id 723
    label "Luizjana"
  ]
  node [
    id 724
    label "Nowy_Meksyk"
  ]
  node [
    id 725
    label "Alabama"
  ]
  node [
    id 726
    label "Kansas"
  ]
  node [
    id 727
    label "Oregon"
  ]
  node [
    id 728
    label "Floryda"
  ]
  node [
    id 729
    label "Oklahoma"
  ]
  node [
    id 730
    label "jednostka_administracyjna"
  ]
  node [
    id 731
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 732
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 733
    label "vessel"
  ]
  node [
    id 734
    label "sprz&#281;t"
  ]
  node [
    id 735
    label "statki"
  ]
  node [
    id 736
    label "rewaskularyzacja"
  ]
  node [
    id 737
    label "ceramika"
  ]
  node [
    id 738
    label "drewno"
  ]
  node [
    id 739
    label "przew&#243;d"
  ]
  node [
    id 740
    label "unaczyni&#263;"
  ]
  node [
    id 741
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 742
    label "receptacle"
  ]
  node [
    id 743
    label "co&#347;"
  ]
  node [
    id 744
    label "budynek"
  ]
  node [
    id 745
    label "thing"
  ]
  node [
    id 746
    label "program"
  ]
  node [
    id 747
    label "rzecz"
  ]
  node [
    id 748
    label "strona"
  ]
  node [
    id 749
    label "posiada&#263;"
  ]
  node [
    id 750
    label "potencja&#322;"
  ]
  node [
    id 751
    label "zapomina&#263;"
  ]
  node [
    id 752
    label "zapomnienie"
  ]
  node [
    id 753
    label "zapominanie"
  ]
  node [
    id 754
    label "ability"
  ]
  node [
    id 755
    label "obliczeniowo"
  ]
  node [
    id 756
    label "zapomnie&#263;"
  ]
  node [
    id 757
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 758
    label "kultura_duchowa"
  ]
  node [
    id 759
    label "kultura"
  ]
  node [
    id 760
    label "ceremony"
  ]
  node [
    id 761
    label "sformu&#322;owanie"
  ]
  node [
    id 762
    label "zdarzenie_si&#281;"
  ]
  node [
    id 763
    label "poinformowanie"
  ]
  node [
    id 764
    label "wording"
  ]
  node [
    id 765
    label "oznaczenie"
  ]
  node [
    id 766
    label "znak_j&#281;zykowy"
  ]
  node [
    id 767
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 768
    label "ozdobnik"
  ]
  node [
    id 769
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 770
    label "grupa_imienna"
  ]
  node [
    id 771
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 772
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 773
    label "ujawnienie"
  ]
  node [
    id 774
    label "affirmation"
  ]
  node [
    id 775
    label "zapisanie"
  ]
  node [
    id 776
    label "rzucenie"
  ]
  node [
    id 777
    label "zapis"
  ]
  node [
    id 778
    label "figure"
  ]
  node [
    id 779
    label "mildew"
  ]
  node [
    id 780
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 781
    label "ideal"
  ]
  node [
    id 782
    label "rule"
  ]
  node [
    id 783
    label "ruch"
  ]
  node [
    id 784
    label "dekal"
  ]
  node [
    id 785
    label "motyw"
  ]
  node [
    id 786
    label "projekt"
  ]
  node [
    id 787
    label "m&#322;ot"
  ]
  node [
    id 788
    label "drzewo"
  ]
  node [
    id 789
    label "pr&#243;ba"
  ]
  node [
    id 790
    label "attribute"
  ]
  node [
    id 791
    label "marka"
  ]
  node [
    id 792
    label "mechanika"
  ]
  node [
    id 793
    label "o&#347;"
  ]
  node [
    id 794
    label "usenet"
  ]
  node [
    id 795
    label "rozprz&#261;c"
  ]
  node [
    id 796
    label "cybernetyk"
  ]
  node [
    id 797
    label "podsystem"
  ]
  node [
    id 798
    label "system"
  ]
  node [
    id 799
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 800
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 801
    label "sk&#322;ad"
  ]
  node [
    id 802
    label "systemat"
  ]
  node [
    id 803
    label "konstrukcja"
  ]
  node [
    id 804
    label "model"
  ]
  node [
    id 805
    label "jig"
  ]
  node [
    id 806
    label "drabina_analgetyczna"
  ]
  node [
    id 807
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 808
    label "C"
  ]
  node [
    id 809
    label "D"
  ]
  node [
    id 810
    label "exemplar"
  ]
  node [
    id 811
    label "sprawa"
  ]
  node [
    id 812
    label "wyraz_pochodny"
  ]
  node [
    id 813
    label "zboczenie"
  ]
  node [
    id 814
    label "om&#243;wienie"
  ]
  node [
    id 815
    label "omawia&#263;"
  ]
  node [
    id 816
    label "fraza"
  ]
  node [
    id 817
    label "entity"
  ]
  node [
    id 818
    label "forum"
  ]
  node [
    id 819
    label "topik"
  ]
  node [
    id 820
    label "tematyka"
  ]
  node [
    id 821
    label "w&#261;tek"
  ]
  node [
    id 822
    label "zbaczanie"
  ]
  node [
    id 823
    label "om&#243;wi&#263;"
  ]
  node [
    id 824
    label "omawianie"
  ]
  node [
    id 825
    label "melodia"
  ]
  node [
    id 826
    label "otoczka"
  ]
  node [
    id 827
    label "istota"
  ]
  node [
    id 828
    label "zbacza&#263;"
  ]
  node [
    id 829
    label "zboczy&#263;"
  ]
  node [
    id 830
    label "morpheme"
  ]
  node [
    id 831
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 832
    label "figura_stylistyczna"
  ]
  node [
    id 833
    label "decoration"
  ]
  node [
    id 834
    label "dekoracja"
  ]
  node [
    id 835
    label "reaktor_j&#261;drowy"
  ]
  node [
    id 836
    label "magnes"
  ]
  node [
    id 837
    label "spowalniacz"
  ]
  node [
    id 838
    label "transformator"
  ]
  node [
    id 839
    label "mi&#281;kisz"
  ]
  node [
    id 840
    label "marrow"
  ]
  node [
    id 841
    label "pocisk"
  ]
  node [
    id 842
    label "pr&#281;t_kontrolny"
  ]
  node [
    id 843
    label "procesor"
  ]
  node [
    id 844
    label "ch&#322;odziwo"
  ]
  node [
    id 845
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 846
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 847
    label "surowiak"
  ]
  node [
    id 848
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 849
    label "core"
  ]
  node [
    id 850
    label "plan"
  ]
  node [
    id 851
    label "kondycja"
  ]
  node [
    id 852
    label "polecenie"
  ]
  node [
    id 853
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 854
    label "capability"
  ]
  node [
    id 855
    label "prawo"
  ]
  node [
    id 856
    label "Bund"
  ]
  node [
    id 857
    label "Mazowsze"
  ]
  node [
    id 858
    label "PPR"
  ]
  node [
    id 859
    label "Jakobici"
  ]
  node [
    id 860
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 861
    label "SLD"
  ]
  node [
    id 862
    label "zespolik"
  ]
  node [
    id 863
    label "Razem"
  ]
  node [
    id 864
    label "PiS"
  ]
  node [
    id 865
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 866
    label "partia"
  ]
  node [
    id 867
    label "Kuomintang"
  ]
  node [
    id 868
    label "ZSL"
  ]
  node [
    id 869
    label "jednostka"
  ]
  node [
    id 870
    label "proces"
  ]
  node [
    id 871
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 872
    label "rugby"
  ]
  node [
    id 873
    label "AWS"
  ]
  node [
    id 874
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 875
    label "blok"
  ]
  node [
    id 876
    label "PO"
  ]
  node [
    id 877
    label "si&#322;a"
  ]
  node [
    id 878
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 879
    label "Federali&#347;ci"
  ]
  node [
    id 880
    label "PSL"
  ]
  node [
    id 881
    label "wojsko"
  ]
  node [
    id 882
    label "Wigowie"
  ]
  node [
    id 883
    label "ZChN"
  ]
  node [
    id 884
    label "egzekutywa"
  ]
  node [
    id 885
    label "rocznik"
  ]
  node [
    id 886
    label "The_Beatles"
  ]
  node [
    id 887
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 888
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 889
    label "unit"
  ]
  node [
    id 890
    label "Depeche_Mode"
  ]
  node [
    id 891
    label "sprzeciw"
  ]
  node [
    id 892
    label "czerwona_kartka"
  ]
  node [
    id 893
    label "protestacja"
  ]
  node [
    id 894
    label "figura_karciana"
  ]
  node [
    id 895
    label "szlachcianka"
  ]
  node [
    id 896
    label "warcaby"
  ]
  node [
    id 897
    label "kobieta"
  ]
  node [
    id 898
    label "promocja"
  ]
  node [
    id 899
    label "strzelec"
  ]
  node [
    id 900
    label "pion"
  ]
  node [
    id 901
    label "doros&#322;y"
  ]
  node [
    id 902
    label "&#380;ona"
  ]
  node [
    id 903
    label "samica"
  ]
  node [
    id 904
    label "uleganie"
  ]
  node [
    id 905
    label "ulec"
  ]
  node [
    id 906
    label "m&#281;&#380;yna"
  ]
  node [
    id 907
    label "partnerka"
  ]
  node [
    id 908
    label "ulegni&#281;cie"
  ]
  node [
    id 909
    label "pa&#324;stwo"
  ]
  node [
    id 910
    label "&#322;ono"
  ]
  node [
    id 911
    label "menopauza"
  ]
  node [
    id 912
    label "przekwitanie"
  ]
  node [
    id 913
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 914
    label "babka"
  ]
  node [
    id 915
    label "ulega&#263;"
  ]
  node [
    id 916
    label "jednostka_organizacyjna"
  ]
  node [
    id 917
    label "urz&#261;d"
  ]
  node [
    id 918
    label "whole"
  ]
  node [
    id 919
    label "miejsce_pracy"
  ]
  node [
    id 920
    label "insourcing"
  ]
  node [
    id 921
    label "instalacja"
  ]
  node [
    id 922
    label "bierka_szachowa"
  ]
  node [
    id 923
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 924
    label "bierka"
  ]
  node [
    id 925
    label "dzia&#322;"
  ]
  node [
    id 926
    label "mezon"
  ]
  node [
    id 927
    label "hierarchia"
  ]
  node [
    id 928
    label "&#380;o&#322;nierz"
  ]
  node [
    id 929
    label "figura"
  ]
  node [
    id 930
    label "futbolista"
  ]
  node [
    id 931
    label "sportowiec"
  ]
  node [
    id 932
    label "Renata_Mauer"
  ]
  node [
    id 933
    label "ludzko&#347;&#263;"
  ]
  node [
    id 934
    label "wapniak"
  ]
  node [
    id 935
    label "os&#322;abia&#263;"
  ]
  node [
    id 936
    label "hominid"
  ]
  node [
    id 937
    label "podw&#322;adny"
  ]
  node [
    id 938
    label "os&#322;abianie"
  ]
  node [
    id 939
    label "dwun&#243;g"
  ]
  node [
    id 940
    label "profanum"
  ]
  node [
    id 941
    label "mikrokosmos"
  ]
  node [
    id 942
    label "nasada"
  ]
  node [
    id 943
    label "duch"
  ]
  node [
    id 944
    label "antropochoria"
  ]
  node [
    id 945
    label "osoba"
  ]
  node [
    id 946
    label "senior"
  ]
  node [
    id 947
    label "oddzia&#322;ywanie"
  ]
  node [
    id 948
    label "Adam"
  ]
  node [
    id 949
    label "homo_sapiens"
  ]
  node [
    id 950
    label "polifag"
  ]
  node [
    id 951
    label "damka"
  ]
  node [
    id 952
    label "warcabnica"
  ]
  node [
    id 953
    label "sport_umys&#322;owy"
  ]
  node [
    id 954
    label "gra_planszowa"
  ]
  node [
    id 955
    label "promotion"
  ]
  node [
    id 956
    label "impreza"
  ]
  node [
    id 957
    label "sprzeda&#380;"
  ]
  node [
    id 958
    label "zamiana"
  ]
  node [
    id 959
    label "udzieli&#263;"
  ]
  node [
    id 960
    label "brief"
  ]
  node [
    id 961
    label "decyzja"
  ]
  node [
    id 962
    label "&#347;wiadectwo"
  ]
  node [
    id 963
    label "akcja"
  ]
  node [
    id 964
    label "bran&#380;a"
  ]
  node [
    id 965
    label "commencement"
  ]
  node [
    id 966
    label "okazja"
  ]
  node [
    id 967
    label "promowa&#263;"
  ]
  node [
    id 968
    label "graduacja"
  ]
  node [
    id 969
    label "nominacja"
  ]
  node [
    id 970
    label "szachy"
  ]
  node [
    id 971
    label "popularyzacja"
  ]
  node [
    id 972
    label "wypromowa&#263;"
  ]
  node [
    id 973
    label "gradation"
  ]
  node [
    id 974
    label "uzyska&#263;"
  ]
  node [
    id 975
    label "okre&#347;lony"
  ]
  node [
    id 976
    label "jaki&#347;"
  ]
  node [
    id 977
    label "przyzwoity"
  ]
  node [
    id 978
    label "ciekawy"
  ]
  node [
    id 979
    label "jako&#347;"
  ]
  node [
    id 980
    label "jako_tako"
  ]
  node [
    id 981
    label "niez&#322;y"
  ]
  node [
    id 982
    label "dziwny"
  ]
  node [
    id 983
    label "charakterystyczny"
  ]
  node [
    id 984
    label "wiadomy"
  ]
  node [
    id 985
    label "attack"
  ]
  node [
    id 986
    label "dorwa&#263;"
  ]
  node [
    id 987
    label "capture"
  ]
  node [
    id 988
    label "ensnare"
  ]
  node [
    id 989
    label "zarazi&#263;_si&#281;"
  ]
  node [
    id 990
    label "chwyci&#263;"
  ]
  node [
    id 991
    label "fascinate"
  ]
  node [
    id 992
    label "zaskoczy&#263;"
  ]
  node [
    id 993
    label "ogarn&#261;&#263;"
  ]
  node [
    id 994
    label "dupn&#261;&#263;"
  ]
  node [
    id 995
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 996
    label "manipulate"
  ]
  node [
    id 997
    label "otoczy&#263;"
  ]
  node [
    id 998
    label "spotka&#263;"
  ]
  node [
    id 999
    label "visit"
  ]
  node [
    id 1000
    label "involve"
  ]
  node [
    id 1001
    label "environment"
  ]
  node [
    id 1002
    label "dotkn&#261;&#263;"
  ]
  node [
    id 1003
    label "zdziwi&#263;"
  ]
  node [
    id 1004
    label "zrozumie&#263;"
  ]
  node [
    id 1005
    label "catch"
  ]
  node [
    id 1006
    label "wpa&#347;&#263;"
  ]
  node [
    id 1007
    label "zerwa&#263;"
  ]
  node [
    id 1008
    label "zaatakowa&#263;"
  ]
  node [
    id 1009
    label "porwa&#263;"
  ]
  node [
    id 1010
    label "znale&#378;&#263;"
  ]
  node [
    id 1011
    label "realize"
  ]
  node [
    id 1012
    label "make"
  ]
  node [
    id 1013
    label "give_birth"
  ]
  node [
    id 1014
    label "przeszkodzi&#263;"
  ]
  node [
    id 1015
    label "wzi&#261;&#263;"
  ]
  node [
    id 1016
    label "notice"
  ]
  node [
    id 1017
    label "wybuchn&#261;&#263;"
  ]
  node [
    id 1018
    label "spa&#347;&#263;"
  ]
  node [
    id 1019
    label "uderzy&#263;"
  ]
  node [
    id 1020
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 1021
    label "konus"
  ]
  node [
    id 1022
    label "McDonald"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 275
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 462
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 198
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 1021
    target 1022
  ]
]
