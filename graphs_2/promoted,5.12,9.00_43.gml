graph [
  node [
    id 0
    label "publikacja"
    origin "text"
  ]
  node [
    id 1
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "autor"
    origin "text"
  ]
  node [
    id 3
    label "zarzuca&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 5
    label "henryk"
    origin "text"
  ]
  node [
    id 6
    label "jankowski"
    origin "text"
  ]
  node [
    id 7
    label "pedofilia"
    origin "text"
  ]
  node [
    id 8
    label "wzbudzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wielki"
    origin "text"
  ]
  node [
    id 10
    label "emocja"
    origin "text"
  ]
  node [
    id 11
    label "opinia"
    origin "text"
  ]
  node [
    id 12
    label "publiczny"
    origin "text"
  ]
  node [
    id 13
    label "tekst"
  ]
  node [
    id 14
    label "druk"
  ]
  node [
    id 15
    label "produkcja"
  ]
  node [
    id 16
    label "notification"
  ]
  node [
    id 17
    label "ekscerpcja"
  ]
  node [
    id 18
    label "j&#281;zykowo"
  ]
  node [
    id 19
    label "wypowied&#378;"
  ]
  node [
    id 20
    label "redakcja"
  ]
  node [
    id 21
    label "wytw&#243;r"
  ]
  node [
    id 22
    label "pomini&#281;cie"
  ]
  node [
    id 23
    label "dzie&#322;o"
  ]
  node [
    id 24
    label "preparacja"
  ]
  node [
    id 25
    label "odmianka"
  ]
  node [
    id 26
    label "opu&#347;ci&#263;"
  ]
  node [
    id 27
    label "koniektura"
  ]
  node [
    id 28
    label "pisa&#263;"
  ]
  node [
    id 29
    label "obelga"
  ]
  node [
    id 30
    label "impreza"
  ]
  node [
    id 31
    label "realizacja"
  ]
  node [
    id 32
    label "tingel-tangel"
  ]
  node [
    id 33
    label "wydawa&#263;"
  ]
  node [
    id 34
    label "numer"
  ]
  node [
    id 35
    label "monta&#380;"
  ]
  node [
    id 36
    label "wyda&#263;"
  ]
  node [
    id 37
    label "postprodukcja"
  ]
  node [
    id 38
    label "performance"
  ]
  node [
    id 39
    label "fabrication"
  ]
  node [
    id 40
    label "zbi&#243;r"
  ]
  node [
    id 41
    label "product"
  ]
  node [
    id 42
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 43
    label "uzysk"
  ]
  node [
    id 44
    label "rozw&#243;j"
  ]
  node [
    id 45
    label "odtworzenie"
  ]
  node [
    id 46
    label "dorobek"
  ]
  node [
    id 47
    label "kreacja"
  ]
  node [
    id 48
    label "trema"
  ]
  node [
    id 49
    label "creation"
  ]
  node [
    id 50
    label "kooperowa&#263;"
  ]
  node [
    id 51
    label "technika"
  ]
  node [
    id 52
    label "impression"
  ]
  node [
    id 53
    label "pismo"
  ]
  node [
    id 54
    label "glif"
  ]
  node [
    id 55
    label "dese&#324;"
  ]
  node [
    id 56
    label "prohibita"
  ]
  node [
    id 57
    label "cymelium"
  ]
  node [
    id 58
    label "tkanina"
  ]
  node [
    id 59
    label "zaproszenie"
  ]
  node [
    id 60
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 61
    label "formatowa&#263;"
  ]
  node [
    id 62
    label "formatowanie"
  ]
  node [
    id 63
    label "zdobnik"
  ]
  node [
    id 64
    label "character"
  ]
  node [
    id 65
    label "printing"
  ]
  node [
    id 66
    label "kszta&#322;ciciel"
  ]
  node [
    id 67
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 68
    label "tworzyciel"
  ]
  node [
    id 69
    label "wykonawca"
  ]
  node [
    id 70
    label "pomys&#322;odawca"
  ]
  node [
    id 71
    label "&#347;w"
  ]
  node [
    id 72
    label "inicjator"
  ]
  node [
    id 73
    label "podmiot_gospodarczy"
  ]
  node [
    id 74
    label "artysta"
  ]
  node [
    id 75
    label "cz&#322;owiek"
  ]
  node [
    id 76
    label "muzyk"
  ]
  node [
    id 77
    label "nauczyciel"
  ]
  node [
    id 78
    label "intrude"
  ]
  node [
    id 79
    label "twierdzi&#263;"
  ]
  node [
    id 80
    label "przeznacza&#263;"
  ]
  node [
    id 81
    label "przestawa&#263;"
  ]
  node [
    id 82
    label "bequeath"
  ]
  node [
    id 83
    label "odchodzi&#263;"
  ]
  node [
    id 84
    label "inflict"
  ]
  node [
    id 85
    label "umieszcza&#263;"
  ]
  node [
    id 86
    label "prosecute"
  ]
  node [
    id 87
    label "ustala&#263;"
  ]
  node [
    id 88
    label "robi&#263;"
  ]
  node [
    id 89
    label "indicate"
  ]
  node [
    id 90
    label "blend"
  ]
  node [
    id 91
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 92
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 93
    label "opuszcza&#263;"
  ]
  node [
    id 94
    label "impart"
  ]
  node [
    id 95
    label "wyrusza&#263;"
  ]
  node [
    id 96
    label "odrzut"
  ]
  node [
    id 97
    label "go"
  ]
  node [
    id 98
    label "seclude"
  ]
  node [
    id 99
    label "gasn&#261;&#263;"
  ]
  node [
    id 100
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 101
    label "odstawa&#263;"
  ]
  node [
    id 102
    label "rezygnowa&#263;"
  ]
  node [
    id 103
    label "i&#347;&#263;"
  ]
  node [
    id 104
    label "mija&#263;"
  ]
  node [
    id 105
    label "proceed"
  ]
  node [
    id 106
    label "&#380;y&#263;"
  ]
  node [
    id 107
    label "coating"
  ]
  node [
    id 108
    label "przebywa&#263;"
  ]
  node [
    id 109
    label "determine"
  ]
  node [
    id 110
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 111
    label "ko&#324;czy&#263;"
  ]
  node [
    id 112
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 113
    label "finish_up"
  ]
  node [
    id 114
    label "plasowa&#263;"
  ]
  node [
    id 115
    label "umie&#347;ci&#263;"
  ]
  node [
    id 116
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 117
    label "pomieszcza&#263;"
  ]
  node [
    id 118
    label "accommodate"
  ]
  node [
    id 119
    label "zmienia&#263;"
  ]
  node [
    id 120
    label "powodowa&#263;"
  ]
  node [
    id 121
    label "venture"
  ]
  node [
    id 122
    label "wpiernicza&#263;"
  ]
  node [
    id 123
    label "okre&#347;la&#263;"
  ]
  node [
    id 124
    label "oznajmia&#263;"
  ]
  node [
    id 125
    label "zapewnia&#263;"
  ]
  node [
    id 126
    label "attest"
  ]
  node [
    id 127
    label "komunikowa&#263;"
  ]
  node [
    id 128
    label "argue"
  ]
  node [
    id 129
    label "ksi&#281;&#380;a"
  ]
  node [
    id 130
    label "rozgrzeszanie"
  ]
  node [
    id 131
    label "duszpasterstwo"
  ]
  node [
    id 132
    label "eklezjasta"
  ]
  node [
    id 133
    label "duchowny"
  ]
  node [
    id 134
    label "rozgrzesza&#263;"
  ]
  node [
    id 135
    label "seminarzysta"
  ]
  node [
    id 136
    label "klecha"
  ]
  node [
    id 137
    label "pasterz"
  ]
  node [
    id 138
    label "kol&#281;da"
  ]
  node [
    id 139
    label "kap&#322;an"
  ]
  node [
    id 140
    label "duchowie&#324;stwo"
  ]
  node [
    id 141
    label "&#347;rodowisko"
  ]
  node [
    id 142
    label "stowarzyszenie_religijne"
  ]
  node [
    id 143
    label "apostolstwo"
  ]
  node [
    id 144
    label "kaznodziejstwo"
  ]
  node [
    id 145
    label "chor&#261;&#380;y"
  ]
  node [
    id 146
    label "rozsiewca"
  ]
  node [
    id 147
    label "tuba"
  ]
  node [
    id 148
    label "zwolennik"
  ]
  node [
    id 149
    label "wyznawca"
  ]
  node [
    id 150
    label "popularyzator"
  ]
  node [
    id 151
    label "Luter"
  ]
  node [
    id 152
    label "religia"
  ]
  node [
    id 153
    label "Bayes"
  ]
  node [
    id 154
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 155
    label "sekularyzacja"
  ]
  node [
    id 156
    label "tonsura"
  ]
  node [
    id 157
    label "Hus"
  ]
  node [
    id 158
    label "religijny"
  ]
  node [
    id 159
    label "przedstawiciel"
  ]
  node [
    id 160
    label "kongregacja"
  ]
  node [
    id 161
    label "wie&#347;niak"
  ]
  node [
    id 162
    label "Pan"
  ]
  node [
    id 163
    label "szpak"
  ]
  node [
    id 164
    label "hodowca"
  ]
  node [
    id 165
    label "pracownik_fizyczny"
  ]
  node [
    id 166
    label "Grek"
  ]
  node [
    id 167
    label "obywatel"
  ]
  node [
    id 168
    label "eklezja"
  ]
  node [
    id 169
    label "kaznodzieja"
  ]
  node [
    id 170
    label "odwiedziny"
  ]
  node [
    id 171
    label "pie&#347;&#324;"
  ]
  node [
    id 172
    label "kol&#281;dnik"
  ]
  node [
    id 173
    label "zwyczaj_ludowy"
  ]
  node [
    id 174
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 175
    label "wybacza&#263;"
  ]
  node [
    id 176
    label "grzesznik"
  ]
  node [
    id 177
    label "udziela&#263;"
  ]
  node [
    id 178
    label "wybaczanie"
  ]
  node [
    id 179
    label "robienie"
  ]
  node [
    id 180
    label "udzielanie"
  ]
  node [
    id 181
    label "t&#322;umaczenie"
  ]
  node [
    id 182
    label "spowiadanie"
  ]
  node [
    id 183
    label "czynno&#347;&#263;"
  ]
  node [
    id 184
    label "uczestnik"
  ]
  node [
    id 185
    label "ucze&#324;"
  ]
  node [
    id 186
    label "student"
  ]
  node [
    id 187
    label "pedophilia"
  ]
  node [
    id 188
    label "parafilia"
  ]
  node [
    id 189
    label "przest&#281;pstwo"
  ]
  node [
    id 190
    label "dewiant"
  ]
  node [
    id 191
    label "zaburzenie_seksualne"
  ]
  node [
    id 192
    label "paraphilia"
  ]
  node [
    id 193
    label "brudny"
  ]
  node [
    id 194
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 195
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 196
    label "crime"
  ]
  node [
    id 197
    label "sprawstwo"
  ]
  node [
    id 198
    label "wywo&#322;a&#263;"
  ]
  node [
    id 199
    label "arouse"
  ]
  node [
    id 200
    label "poleci&#263;"
  ]
  node [
    id 201
    label "train"
  ]
  node [
    id 202
    label "wezwa&#263;"
  ]
  node [
    id 203
    label "trip"
  ]
  node [
    id 204
    label "spowodowa&#263;"
  ]
  node [
    id 205
    label "oznajmi&#263;"
  ]
  node [
    id 206
    label "revolutionize"
  ]
  node [
    id 207
    label "przetworzy&#263;"
  ]
  node [
    id 208
    label "wydali&#263;"
  ]
  node [
    id 209
    label "znaczny"
  ]
  node [
    id 210
    label "wyj&#261;tkowy"
  ]
  node [
    id 211
    label "nieprzeci&#281;tny"
  ]
  node [
    id 212
    label "wysoce"
  ]
  node [
    id 213
    label "wa&#380;ny"
  ]
  node [
    id 214
    label "prawdziwy"
  ]
  node [
    id 215
    label "wybitny"
  ]
  node [
    id 216
    label "dupny"
  ]
  node [
    id 217
    label "wysoki"
  ]
  node [
    id 218
    label "intensywnie"
  ]
  node [
    id 219
    label "niespotykany"
  ]
  node [
    id 220
    label "wydatny"
  ]
  node [
    id 221
    label "wspania&#322;y"
  ]
  node [
    id 222
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 223
    label "&#347;wietny"
  ]
  node [
    id 224
    label "imponuj&#261;cy"
  ]
  node [
    id 225
    label "wybitnie"
  ]
  node [
    id 226
    label "celny"
  ]
  node [
    id 227
    label "nieprzeci&#281;tnie"
  ]
  node [
    id 228
    label "wyj&#261;tkowo"
  ]
  node [
    id 229
    label "inny"
  ]
  node [
    id 230
    label "&#380;ywny"
  ]
  node [
    id 231
    label "szczery"
  ]
  node [
    id 232
    label "naturalny"
  ]
  node [
    id 233
    label "naprawd&#281;"
  ]
  node [
    id 234
    label "realnie"
  ]
  node [
    id 235
    label "podobny"
  ]
  node [
    id 236
    label "zgodny"
  ]
  node [
    id 237
    label "m&#261;dry"
  ]
  node [
    id 238
    label "prawdziwie"
  ]
  node [
    id 239
    label "znacznie"
  ]
  node [
    id 240
    label "zauwa&#380;alny"
  ]
  node [
    id 241
    label "wynios&#322;y"
  ]
  node [
    id 242
    label "dono&#347;ny"
  ]
  node [
    id 243
    label "silny"
  ]
  node [
    id 244
    label "wa&#380;nie"
  ]
  node [
    id 245
    label "istotnie"
  ]
  node [
    id 246
    label "eksponowany"
  ]
  node [
    id 247
    label "dobry"
  ]
  node [
    id 248
    label "do_dupy"
  ]
  node [
    id 249
    label "z&#322;y"
  ]
  node [
    id 250
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 251
    label "ogrom"
  ]
  node [
    id 252
    label "iskrzy&#263;"
  ]
  node [
    id 253
    label "d&#322;awi&#263;"
  ]
  node [
    id 254
    label "ostygn&#261;&#263;"
  ]
  node [
    id 255
    label "stygn&#261;&#263;"
  ]
  node [
    id 256
    label "stan"
  ]
  node [
    id 257
    label "temperatura"
  ]
  node [
    id 258
    label "wpa&#347;&#263;"
  ]
  node [
    id 259
    label "afekt"
  ]
  node [
    id 260
    label "wpada&#263;"
  ]
  node [
    id 261
    label "Ohio"
  ]
  node [
    id 262
    label "wci&#281;cie"
  ]
  node [
    id 263
    label "Nowy_York"
  ]
  node [
    id 264
    label "warstwa"
  ]
  node [
    id 265
    label "samopoczucie"
  ]
  node [
    id 266
    label "Illinois"
  ]
  node [
    id 267
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 268
    label "state"
  ]
  node [
    id 269
    label "Jukatan"
  ]
  node [
    id 270
    label "Kalifornia"
  ]
  node [
    id 271
    label "Wirginia"
  ]
  node [
    id 272
    label "wektor"
  ]
  node [
    id 273
    label "by&#263;"
  ]
  node [
    id 274
    label "Teksas"
  ]
  node [
    id 275
    label "Goa"
  ]
  node [
    id 276
    label "Waszyngton"
  ]
  node [
    id 277
    label "miejsce"
  ]
  node [
    id 278
    label "Massachusetts"
  ]
  node [
    id 279
    label "Alaska"
  ]
  node [
    id 280
    label "Arakan"
  ]
  node [
    id 281
    label "Hawaje"
  ]
  node [
    id 282
    label "Maryland"
  ]
  node [
    id 283
    label "punkt"
  ]
  node [
    id 284
    label "Michigan"
  ]
  node [
    id 285
    label "Arizona"
  ]
  node [
    id 286
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 287
    label "Georgia"
  ]
  node [
    id 288
    label "poziom"
  ]
  node [
    id 289
    label "Pensylwania"
  ]
  node [
    id 290
    label "shape"
  ]
  node [
    id 291
    label "Luizjana"
  ]
  node [
    id 292
    label "Nowy_Meksyk"
  ]
  node [
    id 293
    label "Alabama"
  ]
  node [
    id 294
    label "ilo&#347;&#263;"
  ]
  node [
    id 295
    label "Kansas"
  ]
  node [
    id 296
    label "Oregon"
  ]
  node [
    id 297
    label "Floryda"
  ]
  node [
    id 298
    label "Oklahoma"
  ]
  node [
    id 299
    label "jednostka_administracyjna"
  ]
  node [
    id 300
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 301
    label "wyraz"
  ]
  node [
    id 302
    label "afekcja"
  ]
  node [
    id 303
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 304
    label "strike"
  ]
  node [
    id 305
    label "ulec"
  ]
  node [
    id 306
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 307
    label "collapse"
  ]
  node [
    id 308
    label "rzecz"
  ]
  node [
    id 309
    label "d&#378;wi&#281;k"
  ]
  node [
    id 310
    label "fall_upon"
  ]
  node [
    id 311
    label "ponie&#347;&#263;"
  ]
  node [
    id 312
    label "zapach"
  ]
  node [
    id 313
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 314
    label "uderzy&#263;"
  ]
  node [
    id 315
    label "wymy&#347;li&#263;"
  ]
  node [
    id 316
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 317
    label "decline"
  ]
  node [
    id 318
    label "&#347;wiat&#322;o"
  ]
  node [
    id 319
    label "fall"
  ]
  node [
    id 320
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 321
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 322
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 323
    label "spotka&#263;"
  ]
  node [
    id 324
    label "odwiedzi&#263;"
  ]
  node [
    id 325
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 326
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 327
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 328
    label "zaziera&#263;"
  ]
  node [
    id 329
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 330
    label "czu&#263;"
  ]
  node [
    id 331
    label "spotyka&#263;"
  ]
  node [
    id 332
    label "drop"
  ]
  node [
    id 333
    label "pogo"
  ]
  node [
    id 334
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 335
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 336
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 337
    label "popada&#263;"
  ]
  node [
    id 338
    label "odwiedza&#263;"
  ]
  node [
    id 339
    label "wymy&#347;la&#263;"
  ]
  node [
    id 340
    label "przypomina&#263;"
  ]
  node [
    id 341
    label "ujmowa&#263;"
  ]
  node [
    id 342
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 343
    label "chowa&#263;"
  ]
  node [
    id 344
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 345
    label "demaskowa&#263;"
  ]
  node [
    id 346
    label "ulega&#263;"
  ]
  node [
    id 347
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 348
    label "flatten"
  ]
  node [
    id 349
    label "zanikn&#261;&#263;"
  ]
  node [
    id 350
    label "och&#322;odzi&#263;_si&#281;"
  ]
  node [
    id 351
    label "uspokoi&#263;_si&#281;"
  ]
  node [
    id 352
    label "odczucia"
  ]
  node [
    id 353
    label "&#347;wieci&#263;"
  ]
  node [
    id 354
    label "mie&#263;_miejsce"
  ]
  node [
    id 355
    label "dawa&#263;"
  ]
  node [
    id 356
    label "flash"
  ]
  node [
    id 357
    label "twinkle"
  ]
  node [
    id 358
    label "mieni&#263;_si&#281;"
  ]
  node [
    id 359
    label "wyr&#243;&#380;nia&#263;_si&#281;"
  ]
  node [
    id 360
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 361
    label "b&#322;yszcze&#263;"
  ]
  node [
    id 362
    label "glitter"
  ]
  node [
    id 363
    label "tryska&#263;"
  ]
  node [
    id 364
    label "cool"
  ]
  node [
    id 365
    label "ch&#322;odzi&#263;_si&#281;"
  ]
  node [
    id 366
    label "zanika&#263;"
  ]
  node [
    id 367
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 368
    label "tautochrona"
  ]
  node [
    id 369
    label "denga"
  ]
  node [
    id 370
    label "termoelektryczno&#347;&#263;"
  ]
  node [
    id 371
    label "zapalenie_opon_m&#243;zgowo-rdzeniowych"
  ]
  node [
    id 372
    label "oznaka"
  ]
  node [
    id 373
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 374
    label "hotness"
  ]
  node [
    id 375
    label "choroba_bosto&#324;ska"
  ]
  node [
    id 376
    label "atmosfera"
  ]
  node [
    id 377
    label "rozpalony"
  ]
  node [
    id 378
    label "cia&#322;o"
  ]
  node [
    id 379
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 380
    label "zagrza&#263;"
  ]
  node [
    id 381
    label "termoczu&#322;y"
  ]
  node [
    id 382
    label "gasi&#263;"
  ]
  node [
    id 383
    label "oddech"
  ]
  node [
    id 384
    label "mute"
  ]
  node [
    id 385
    label "uciska&#263;"
  ]
  node [
    id 386
    label "accelerator"
  ]
  node [
    id 387
    label "urge"
  ]
  node [
    id 388
    label "zmniejsza&#263;"
  ]
  node [
    id 389
    label "restrict"
  ]
  node [
    id 390
    label "hamowa&#263;"
  ]
  node [
    id 391
    label "hesitate"
  ]
  node [
    id 392
    label "dusi&#263;"
  ]
  node [
    id 393
    label "rozleg&#322;o&#347;&#263;"
  ]
  node [
    id 394
    label "wielko&#347;&#263;"
  ]
  node [
    id 395
    label "clutter"
  ]
  node [
    id 396
    label "mn&#243;stwo"
  ]
  node [
    id 397
    label "intensywno&#347;&#263;"
  ]
  node [
    id 398
    label "reputacja"
  ]
  node [
    id 399
    label "pogl&#261;d"
  ]
  node [
    id 400
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 401
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 402
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 403
    label "sofcik"
  ]
  node [
    id 404
    label "kryterium"
  ]
  node [
    id 405
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 406
    label "ekspertyza"
  ]
  node [
    id 407
    label "cecha"
  ]
  node [
    id 408
    label "informacja"
  ]
  node [
    id 409
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 410
    label "dokument"
  ]
  node [
    id 411
    label "appraisal"
  ]
  node [
    id 412
    label "charakterystyka"
  ]
  node [
    id 413
    label "m&#322;ot"
  ]
  node [
    id 414
    label "znak"
  ]
  node [
    id 415
    label "drzewo"
  ]
  node [
    id 416
    label "pr&#243;ba"
  ]
  node [
    id 417
    label "attribute"
  ]
  node [
    id 418
    label "marka"
  ]
  node [
    id 419
    label "znaczenie"
  ]
  node [
    id 420
    label "badanie"
  ]
  node [
    id 421
    label "sketch"
  ]
  node [
    id 422
    label "ocena"
  ]
  node [
    id 423
    label "survey"
  ]
  node [
    id 424
    label "zapis"
  ]
  node [
    id 425
    label "&#347;wiadectwo"
  ]
  node [
    id 426
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 427
    label "parafa"
  ]
  node [
    id 428
    label "plik"
  ]
  node [
    id 429
    label "raport&#243;wka"
  ]
  node [
    id 430
    label "utw&#243;r"
  ]
  node [
    id 431
    label "record"
  ]
  node [
    id 432
    label "fascyku&#322;"
  ]
  node [
    id 433
    label "dokumentacja"
  ]
  node [
    id 434
    label "registratura"
  ]
  node [
    id 435
    label "artyku&#322;"
  ]
  node [
    id 436
    label "writing"
  ]
  node [
    id 437
    label "sygnatariusz"
  ]
  node [
    id 438
    label "teologicznie"
  ]
  node [
    id 439
    label "s&#261;d"
  ]
  node [
    id 440
    label "belief"
  ]
  node [
    id 441
    label "zderzenie_si&#281;"
  ]
  node [
    id 442
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 443
    label "teoria_Arrheniusa"
  ]
  node [
    id 444
    label "wiedza"
  ]
  node [
    id 445
    label "doj&#347;cie"
  ]
  node [
    id 446
    label "obiega&#263;"
  ]
  node [
    id 447
    label "powzi&#281;cie"
  ]
  node [
    id 448
    label "dane"
  ]
  node [
    id 449
    label "obiegni&#281;cie"
  ]
  node [
    id 450
    label "sygna&#322;"
  ]
  node [
    id 451
    label "obieganie"
  ]
  node [
    id 452
    label "powzi&#261;&#263;"
  ]
  node [
    id 453
    label "obiec"
  ]
  node [
    id 454
    label "doj&#347;&#263;"
  ]
  node [
    id 455
    label "warunek_lokalowy"
  ]
  node [
    id 456
    label "rozmiar"
  ]
  node [
    id 457
    label "liczba"
  ]
  node [
    id 458
    label "rzadko&#347;&#263;"
  ]
  node [
    id 459
    label "zaleta"
  ]
  node [
    id 460
    label "measure"
  ]
  node [
    id 461
    label "dymensja"
  ]
  node [
    id 462
    label "poj&#281;cie"
  ]
  node [
    id 463
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 464
    label "zdolno&#347;&#263;"
  ]
  node [
    id 465
    label "potencja"
  ]
  node [
    id 466
    label "property"
  ]
  node [
    id 467
    label "pornografia_mi&#281;kka"
  ]
  node [
    id 468
    label "drobiazg"
  ]
  node [
    id 469
    label "pornografia"
  ]
  node [
    id 470
    label "czynnik"
  ]
  node [
    id 471
    label "upublicznianie"
  ]
  node [
    id 472
    label "jawny"
  ]
  node [
    id 473
    label "upublicznienie"
  ]
  node [
    id 474
    label "publicznie"
  ]
  node [
    id 475
    label "jawnie"
  ]
  node [
    id 476
    label "udost&#281;pnianie"
  ]
  node [
    id 477
    label "udost&#281;pnienie"
  ]
  node [
    id 478
    label "ujawnienie_si&#281;"
  ]
  node [
    id 479
    label "ujawnianie_si&#281;"
  ]
  node [
    id 480
    label "zdecydowany"
  ]
  node [
    id 481
    label "znajomy"
  ]
  node [
    id 482
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 483
    label "ujawnienie"
  ]
  node [
    id 484
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 485
    label "ujawnianie"
  ]
  node [
    id 486
    label "ewidentny"
  ]
  node [
    id 487
    label "Henryk"
  ]
  node [
    id 488
    label "Jankowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 487
    target 488
  ]
]
