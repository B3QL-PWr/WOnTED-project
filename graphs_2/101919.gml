graph [
  node [
    id 0
    label "finanse"
    origin "text"
  ]
  node [
    id 1
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 2
    label "terytorialny"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "integralny"
    origin "text"
  ]
  node [
    id 5
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "publiczny"
    origin "text"
  ]
  node [
    id 7
    label "system"
    origin "text"
  ]
  node [
    id 8
    label "finansowy"
    origin "text"
  ]
  node [
    id 9
    label "obejmowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "proces"
    origin "text"
  ]
  node [
    id 11
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 12
    label "gromadzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 14
    label "rozdysponowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "odpowiednio"
    origin "text"
  ]
  node [
    id 16
    label "poj&#281;cie"
    origin "text"
  ]
  node [
    id 17
    label "okre&#347;la&#263;"
    origin "text"
  ]
  node [
    id 18
    label "jako"
    origin "text"
  ]
  node [
    id 19
    label "zasoby"
    origin "text"
  ]
  node [
    id 20
    label "pieni&#281;&#380;ny"
    origin "text"
  ]
  node [
    id 21
    label "operacja"
    origin "text"
  ]
  node [
    id 22
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 23
    label "doch&#243;d"
    origin "text"
  ]
  node [
    id 24
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 25
    label "wyr&#243;wnawczy"
    origin "text"
  ]
  node [
    id 26
    label "przychod"
    origin "text"
  ]
  node [
    id 27
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 28
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "wydatek"
    origin "text"
  ]
  node [
    id 30
    label "rozch&#243;d"
    origin "text"
  ]
  node [
    id 31
    label "przez"
    origin "text"
  ]
  node [
    id 32
    label "jednostka"
    origin "text"
  ]
  node [
    id 33
    label "cela"
    origin "text"
  ]
  node [
    id 34
    label "sfinansowa&#263;"
    origin "text"
  ]
  node [
    id 35
    label "zadanie"
    origin "text"
  ]
  node [
    id 36
    label "zleci&#263;"
    origin "text"
  ]
  node [
    id 37
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 38
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 39
    label "absolutorium"
  ]
  node [
    id 40
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 41
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 42
    label "nap&#322;ywanie"
  ]
  node [
    id 43
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 44
    label "mienie"
  ]
  node [
    id 45
    label "nauka_ekonomiczna"
  ]
  node [
    id 46
    label "podupadanie"
  ]
  node [
    id 47
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 48
    label "podupada&#263;"
  ]
  node [
    id 49
    label "kwestor"
  ]
  node [
    id 50
    label "uruchomienie"
  ]
  node [
    id 51
    label "supernadz&#243;r"
  ]
  node [
    id 52
    label "uruchamia&#263;"
  ]
  node [
    id 53
    label "uruchamianie"
  ]
  node [
    id 54
    label "czynnik_produkcji"
  ]
  node [
    id 55
    label "przej&#347;cie"
  ]
  node [
    id 56
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 57
    label "rodowo&#347;&#263;"
  ]
  node [
    id 58
    label "patent"
  ]
  node [
    id 59
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 60
    label "dobra"
  ]
  node [
    id 61
    label "stan"
  ]
  node [
    id 62
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 63
    label "przej&#347;&#263;"
  ]
  node [
    id 64
    label "possession"
  ]
  node [
    id 65
    label "urz&#281;dnik"
  ]
  node [
    id 66
    label "ksi&#281;gowy"
  ]
  node [
    id 67
    label "kapita&#322;"
  ]
  node [
    id 68
    label "kwestura"
  ]
  node [
    id 69
    label "Katon"
  ]
  node [
    id 70
    label "polityk"
  ]
  node [
    id 71
    label "bankowo&#347;&#263;"
  ]
  node [
    id 72
    label "nadz&#243;r"
  ]
  node [
    id 73
    label "upadanie"
  ]
  node [
    id 74
    label "s&#322;abni&#281;cie"
  ]
  node [
    id 75
    label "begin"
  ]
  node [
    id 76
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 77
    label "zaczyna&#263;"
  ]
  node [
    id 78
    label "zasilenie"
  ]
  node [
    id 79
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 80
    label "opanowanie"
  ]
  node [
    id 81
    label "zebranie_si&#281;"
  ]
  node [
    id 82
    label "dotarcie"
  ]
  node [
    id 83
    label "nasilenie_si&#281;"
  ]
  node [
    id 84
    label "bulge"
  ]
  node [
    id 85
    label "shoot"
  ]
  node [
    id 86
    label "pour"
  ]
  node [
    id 87
    label "dane"
  ]
  node [
    id 88
    label "zasila&#263;"
  ]
  node [
    id 89
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 90
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 91
    label "meet"
  ]
  node [
    id 92
    label "dociera&#263;"
  ]
  node [
    id 93
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 94
    label "wzbiera&#263;"
  ]
  node [
    id 95
    label "ogarnia&#263;"
  ]
  node [
    id 96
    label "wype&#322;nia&#263;"
  ]
  node [
    id 97
    label "powodowanie"
  ]
  node [
    id 98
    label "w&#322;&#261;czanie"
  ]
  node [
    id 99
    label "robienie"
  ]
  node [
    id 100
    label "zaczynanie"
  ]
  node [
    id 101
    label "funkcjonowanie"
  ]
  node [
    id 102
    label "czynno&#347;&#263;"
  ]
  node [
    id 103
    label "graduation"
  ]
  node [
    id 104
    label "uko&#324;czenie"
  ]
  node [
    id 105
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 106
    label "ocena"
  ]
  node [
    id 107
    label "decline"
  ]
  node [
    id 108
    label "traci&#263;"
  ]
  node [
    id 109
    label "fall"
  ]
  node [
    id 110
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 111
    label "wype&#322;ni&#263;"
  ]
  node [
    id 112
    label "mount"
  ]
  node [
    id 113
    label "zasili&#263;"
  ]
  node [
    id 114
    label "wax"
  ]
  node [
    id 115
    label "dotrze&#263;"
  ]
  node [
    id 116
    label "zebra&#263;_si&#281;"
  ]
  node [
    id 117
    label "zgromadzi&#263;_si&#281;"
  ]
  node [
    id 118
    label "rise"
  ]
  node [
    id 119
    label "ogarn&#261;&#263;"
  ]
  node [
    id 120
    label "saddle_horse"
  ]
  node [
    id 121
    label "wezbra&#263;"
  ]
  node [
    id 122
    label "gromadzenie_si&#281;"
  ]
  node [
    id 123
    label "zbieranie_si&#281;"
  ]
  node [
    id 124
    label "zasilanie"
  ]
  node [
    id 125
    label "docieranie"
  ]
  node [
    id 126
    label "t&#281;&#380;enie"
  ]
  node [
    id 127
    label "nawiewanie"
  ]
  node [
    id 128
    label "nadmuchanie"
  ]
  node [
    id 129
    label "ogarnianie"
  ]
  node [
    id 130
    label "spowodowanie"
  ]
  node [
    id 131
    label "zacz&#281;cie"
  ]
  node [
    id 132
    label "w&#322;&#261;czenie"
  ]
  node [
    id 133
    label "propulsion"
  ]
  node [
    id 134
    label "zrobienie"
  ]
  node [
    id 135
    label "autonomy"
  ]
  node [
    id 136
    label "organ"
  ]
  node [
    id 137
    label "tkanka"
  ]
  node [
    id 138
    label "jednostka_organizacyjna"
  ]
  node [
    id 139
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 140
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 141
    label "tw&#243;r"
  ]
  node [
    id 142
    label "organogeneza"
  ]
  node [
    id 143
    label "zesp&#243;&#322;"
  ]
  node [
    id 144
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 145
    label "struktura_anatomiczna"
  ]
  node [
    id 146
    label "uk&#322;ad"
  ]
  node [
    id 147
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 148
    label "dekortykacja"
  ]
  node [
    id 149
    label "Izba_Konsyliarska"
  ]
  node [
    id 150
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 151
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 152
    label "stomia"
  ]
  node [
    id 153
    label "budowa"
  ]
  node [
    id 154
    label "okolica"
  ]
  node [
    id 155
    label "Komitet_Region&#243;w"
  ]
  node [
    id 156
    label "terytorialnie"
  ]
  node [
    id 157
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 158
    label "mie&#263;_miejsce"
  ]
  node [
    id 159
    label "equal"
  ]
  node [
    id 160
    label "trwa&#263;"
  ]
  node [
    id 161
    label "chodzi&#263;"
  ]
  node [
    id 162
    label "si&#281;ga&#263;"
  ]
  node [
    id 163
    label "obecno&#347;&#263;"
  ]
  node [
    id 164
    label "stand"
  ]
  node [
    id 165
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "uczestniczy&#263;"
  ]
  node [
    id 167
    label "participate"
  ]
  node [
    id 168
    label "robi&#263;"
  ]
  node [
    id 169
    label "istnie&#263;"
  ]
  node [
    id 170
    label "pozostawa&#263;"
  ]
  node [
    id 171
    label "zostawa&#263;"
  ]
  node [
    id 172
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 173
    label "adhere"
  ]
  node [
    id 174
    label "compass"
  ]
  node [
    id 175
    label "korzysta&#263;"
  ]
  node [
    id 176
    label "appreciation"
  ]
  node [
    id 177
    label "osi&#261;ga&#263;"
  ]
  node [
    id 178
    label "get"
  ]
  node [
    id 179
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 180
    label "mierzy&#263;"
  ]
  node [
    id 181
    label "u&#380;ywa&#263;"
  ]
  node [
    id 182
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 183
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 184
    label "exsert"
  ]
  node [
    id 185
    label "being"
  ]
  node [
    id 186
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 187
    label "cecha"
  ]
  node [
    id 188
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 189
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 190
    label "p&#322;ywa&#263;"
  ]
  node [
    id 191
    label "run"
  ]
  node [
    id 192
    label "bangla&#263;"
  ]
  node [
    id 193
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 194
    label "przebiega&#263;"
  ]
  node [
    id 195
    label "wk&#322;ada&#263;"
  ]
  node [
    id 196
    label "proceed"
  ]
  node [
    id 197
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 198
    label "carry"
  ]
  node [
    id 199
    label "bywa&#263;"
  ]
  node [
    id 200
    label "dziama&#263;"
  ]
  node [
    id 201
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 202
    label "stara&#263;_si&#281;"
  ]
  node [
    id 203
    label "para"
  ]
  node [
    id 204
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 205
    label "str&#243;j"
  ]
  node [
    id 206
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 207
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 208
    label "krok"
  ]
  node [
    id 209
    label "tryb"
  ]
  node [
    id 210
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 211
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 212
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 213
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 214
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 215
    label "continue"
  ]
  node [
    id 216
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 217
    label "Ohio"
  ]
  node [
    id 218
    label "wci&#281;cie"
  ]
  node [
    id 219
    label "Nowy_York"
  ]
  node [
    id 220
    label "warstwa"
  ]
  node [
    id 221
    label "samopoczucie"
  ]
  node [
    id 222
    label "Illinois"
  ]
  node [
    id 223
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 224
    label "state"
  ]
  node [
    id 225
    label "Jukatan"
  ]
  node [
    id 226
    label "Kalifornia"
  ]
  node [
    id 227
    label "Wirginia"
  ]
  node [
    id 228
    label "wektor"
  ]
  node [
    id 229
    label "Teksas"
  ]
  node [
    id 230
    label "Goa"
  ]
  node [
    id 231
    label "Waszyngton"
  ]
  node [
    id 232
    label "miejsce"
  ]
  node [
    id 233
    label "Massachusetts"
  ]
  node [
    id 234
    label "Alaska"
  ]
  node [
    id 235
    label "Arakan"
  ]
  node [
    id 236
    label "Hawaje"
  ]
  node [
    id 237
    label "Maryland"
  ]
  node [
    id 238
    label "punkt"
  ]
  node [
    id 239
    label "Michigan"
  ]
  node [
    id 240
    label "Arizona"
  ]
  node [
    id 241
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 242
    label "Georgia"
  ]
  node [
    id 243
    label "poziom"
  ]
  node [
    id 244
    label "Pensylwania"
  ]
  node [
    id 245
    label "shape"
  ]
  node [
    id 246
    label "Luizjana"
  ]
  node [
    id 247
    label "Nowy_Meksyk"
  ]
  node [
    id 248
    label "Alabama"
  ]
  node [
    id 249
    label "ilo&#347;&#263;"
  ]
  node [
    id 250
    label "Kansas"
  ]
  node [
    id 251
    label "Oregon"
  ]
  node [
    id 252
    label "Floryda"
  ]
  node [
    id 253
    label "Oklahoma"
  ]
  node [
    id 254
    label "jednostka_administracyjna"
  ]
  node [
    id 255
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 256
    label "wszystek"
  ]
  node [
    id 257
    label "integralnie"
  ]
  node [
    id 258
    label "niepodzielnie"
  ]
  node [
    id 259
    label "ca&#322;y"
  ]
  node [
    id 260
    label "niepodzielny"
  ]
  node [
    id 261
    label "zupe&#322;nie"
  ]
  node [
    id 262
    label "strukturalnie"
  ]
  node [
    id 263
    label "Rzym_Zachodni"
  ]
  node [
    id 264
    label "whole"
  ]
  node [
    id 265
    label "element"
  ]
  node [
    id 266
    label "Rzym_Wschodni"
  ]
  node [
    id 267
    label "urz&#261;dzenie"
  ]
  node [
    id 268
    label "r&#243;&#380;niczka"
  ]
  node [
    id 269
    label "&#347;rodowisko"
  ]
  node [
    id 270
    label "przedmiot"
  ]
  node [
    id 271
    label "materia"
  ]
  node [
    id 272
    label "szambo"
  ]
  node [
    id 273
    label "aspo&#322;eczny"
  ]
  node [
    id 274
    label "component"
  ]
  node [
    id 275
    label "szkodnik"
  ]
  node [
    id 276
    label "gangsterski"
  ]
  node [
    id 277
    label "underworld"
  ]
  node [
    id 278
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 279
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 280
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 281
    label "rozmiar"
  ]
  node [
    id 282
    label "part"
  ]
  node [
    id 283
    label "kom&#243;rka"
  ]
  node [
    id 284
    label "furnishing"
  ]
  node [
    id 285
    label "zabezpieczenie"
  ]
  node [
    id 286
    label "wyrz&#261;dzenie"
  ]
  node [
    id 287
    label "zagospodarowanie"
  ]
  node [
    id 288
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 289
    label "ig&#322;a"
  ]
  node [
    id 290
    label "narz&#281;dzie"
  ]
  node [
    id 291
    label "wirnik"
  ]
  node [
    id 292
    label "aparatura"
  ]
  node [
    id 293
    label "system_energetyczny"
  ]
  node [
    id 294
    label "impulsator"
  ]
  node [
    id 295
    label "mechanizm"
  ]
  node [
    id 296
    label "sprz&#281;t"
  ]
  node [
    id 297
    label "blokowanie"
  ]
  node [
    id 298
    label "set"
  ]
  node [
    id 299
    label "zablokowanie"
  ]
  node [
    id 300
    label "przygotowanie"
  ]
  node [
    id 301
    label "komora"
  ]
  node [
    id 302
    label "j&#281;zyk"
  ]
  node [
    id 303
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 304
    label "upublicznianie"
  ]
  node [
    id 305
    label "jawny"
  ]
  node [
    id 306
    label "upublicznienie"
  ]
  node [
    id 307
    label "publicznie"
  ]
  node [
    id 308
    label "jawnie"
  ]
  node [
    id 309
    label "udost&#281;pnianie"
  ]
  node [
    id 310
    label "udost&#281;pnienie"
  ]
  node [
    id 311
    label "ujawnienie_si&#281;"
  ]
  node [
    id 312
    label "ujawnianie_si&#281;"
  ]
  node [
    id 313
    label "zdecydowany"
  ]
  node [
    id 314
    label "znajomy"
  ]
  node [
    id 315
    label "ujawnienie"
  ]
  node [
    id 316
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 317
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 318
    label "ujawnianie"
  ]
  node [
    id 319
    label "ewidentny"
  ]
  node [
    id 320
    label "j&#261;dro"
  ]
  node [
    id 321
    label "systemik"
  ]
  node [
    id 322
    label "rozprz&#261;c"
  ]
  node [
    id 323
    label "oprogramowanie"
  ]
  node [
    id 324
    label "systemat"
  ]
  node [
    id 325
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 326
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 327
    label "model"
  ]
  node [
    id 328
    label "struktura"
  ]
  node [
    id 329
    label "usenet"
  ]
  node [
    id 330
    label "s&#261;d"
  ]
  node [
    id 331
    label "zbi&#243;r"
  ]
  node [
    id 332
    label "porz&#261;dek"
  ]
  node [
    id 333
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 334
    label "przyn&#281;ta"
  ]
  node [
    id 335
    label "p&#322;&#243;d"
  ]
  node [
    id 336
    label "net"
  ]
  node [
    id 337
    label "w&#281;dkarstwo"
  ]
  node [
    id 338
    label "eratem"
  ]
  node [
    id 339
    label "oddzia&#322;"
  ]
  node [
    id 340
    label "doktryna"
  ]
  node [
    id 341
    label "pulpit"
  ]
  node [
    id 342
    label "konstelacja"
  ]
  node [
    id 343
    label "jednostka_geologiczna"
  ]
  node [
    id 344
    label "o&#347;"
  ]
  node [
    id 345
    label "podsystem"
  ]
  node [
    id 346
    label "metoda"
  ]
  node [
    id 347
    label "ryba"
  ]
  node [
    id 348
    label "Leopard"
  ]
  node [
    id 349
    label "spos&#243;b"
  ]
  node [
    id 350
    label "Android"
  ]
  node [
    id 351
    label "zachowanie"
  ]
  node [
    id 352
    label "cybernetyk"
  ]
  node [
    id 353
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 354
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 355
    label "method"
  ]
  node [
    id 356
    label "sk&#322;ad"
  ]
  node [
    id 357
    label "podstawa"
  ]
  node [
    id 358
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 359
    label "nature"
  ]
  node [
    id 360
    label "pot&#281;ga"
  ]
  node [
    id 361
    label "documentation"
  ]
  node [
    id 362
    label "column"
  ]
  node [
    id 363
    label "zasadzenie"
  ]
  node [
    id 364
    label "za&#322;o&#380;enie"
  ]
  node [
    id 365
    label "punkt_odniesienia"
  ]
  node [
    id 366
    label "zasadzi&#263;"
  ]
  node [
    id 367
    label "bok"
  ]
  node [
    id 368
    label "d&#243;&#322;"
  ]
  node [
    id 369
    label "dzieci&#281;ctwo"
  ]
  node [
    id 370
    label "background"
  ]
  node [
    id 371
    label "podstawowy"
  ]
  node [
    id 372
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 373
    label "strategia"
  ]
  node [
    id 374
    label "pomys&#322;"
  ]
  node [
    id 375
    label "&#347;ciana"
  ]
  node [
    id 376
    label "relacja"
  ]
  node [
    id 377
    label "zasada"
  ]
  node [
    id 378
    label "styl_architektoniczny"
  ]
  node [
    id 379
    label "normalizacja"
  ]
  node [
    id 380
    label "pos&#322;uchanie"
  ]
  node [
    id 381
    label "skumanie"
  ]
  node [
    id 382
    label "orientacja"
  ]
  node [
    id 383
    label "wytw&#243;r"
  ]
  node [
    id 384
    label "zorientowanie"
  ]
  node [
    id 385
    label "teoria"
  ]
  node [
    id 386
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 387
    label "clasp"
  ]
  node [
    id 388
    label "forma"
  ]
  node [
    id 389
    label "przem&#243;wienie"
  ]
  node [
    id 390
    label "egzemplarz"
  ]
  node [
    id 391
    label "series"
  ]
  node [
    id 392
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 393
    label "uprawianie"
  ]
  node [
    id 394
    label "praca_rolnicza"
  ]
  node [
    id 395
    label "collection"
  ]
  node [
    id 396
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 397
    label "pakiet_klimatyczny"
  ]
  node [
    id 398
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 399
    label "sum"
  ]
  node [
    id 400
    label "gathering"
  ]
  node [
    id 401
    label "album"
  ]
  node [
    id 402
    label "system_komputerowy"
  ]
  node [
    id 403
    label "mechanika"
  ]
  node [
    id 404
    label "konstrukcja"
  ]
  node [
    id 405
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 406
    label "moczownik"
  ]
  node [
    id 407
    label "embryo"
  ]
  node [
    id 408
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 409
    label "zarodek"
  ]
  node [
    id 410
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 411
    label "latawiec"
  ]
  node [
    id 412
    label "reengineering"
  ]
  node [
    id 413
    label "program"
  ]
  node [
    id 414
    label "integer"
  ]
  node [
    id 415
    label "liczba"
  ]
  node [
    id 416
    label "zlewanie_si&#281;"
  ]
  node [
    id 417
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 418
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 419
    label "pe&#322;ny"
  ]
  node [
    id 420
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 421
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 422
    label "grupa_dyskusyjna"
  ]
  node [
    id 423
    label "doctrine"
  ]
  node [
    id 424
    label "urozmaicenie"
  ]
  node [
    id 425
    label "pu&#322;apka"
  ]
  node [
    id 426
    label "pon&#281;ta"
  ]
  node [
    id 427
    label "wabik"
  ]
  node [
    id 428
    label "sport"
  ]
  node [
    id 429
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 430
    label "kr&#281;gowiec"
  ]
  node [
    id 431
    label "cz&#322;owiek"
  ]
  node [
    id 432
    label "doniczkowiec"
  ]
  node [
    id 433
    label "mi&#281;so"
  ]
  node [
    id 434
    label "patroszy&#263;"
  ]
  node [
    id 435
    label "rakowato&#347;&#263;"
  ]
  node [
    id 436
    label "ryby"
  ]
  node [
    id 437
    label "fish"
  ]
  node [
    id 438
    label "linia_boczna"
  ]
  node [
    id 439
    label "tar&#322;o"
  ]
  node [
    id 440
    label "wyrostek_filtracyjny"
  ]
  node [
    id 441
    label "m&#281;tnooki"
  ]
  node [
    id 442
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 443
    label "pokrywa_skrzelowa"
  ]
  node [
    id 444
    label "ikra"
  ]
  node [
    id 445
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 446
    label "szczelina_skrzelowa"
  ]
  node [
    id 447
    label "blat"
  ]
  node [
    id 448
    label "interfejs"
  ]
  node [
    id 449
    label "okno"
  ]
  node [
    id 450
    label "obszar"
  ]
  node [
    id 451
    label "ikona"
  ]
  node [
    id 452
    label "system_operacyjny"
  ]
  node [
    id 453
    label "mebel"
  ]
  node [
    id 454
    label "zdolno&#347;&#263;"
  ]
  node [
    id 455
    label "relaxation"
  ]
  node [
    id 456
    label "os&#322;abienie"
  ]
  node [
    id 457
    label "oswobodzenie"
  ]
  node [
    id 458
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 459
    label "zdezorganizowanie"
  ]
  node [
    id 460
    label "reakcja"
  ]
  node [
    id 461
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 462
    label "tajemnica"
  ]
  node [
    id 463
    label "wydarzenie"
  ]
  node [
    id 464
    label "pochowanie"
  ]
  node [
    id 465
    label "zdyscyplinowanie"
  ]
  node [
    id 466
    label "post&#261;pienie"
  ]
  node [
    id 467
    label "post"
  ]
  node [
    id 468
    label "bearing"
  ]
  node [
    id 469
    label "zwierz&#281;"
  ]
  node [
    id 470
    label "behawior"
  ]
  node [
    id 471
    label "observation"
  ]
  node [
    id 472
    label "dieta"
  ]
  node [
    id 473
    label "podtrzymanie"
  ]
  node [
    id 474
    label "etolog"
  ]
  node [
    id 475
    label "przechowanie"
  ]
  node [
    id 476
    label "oswobodzi&#263;"
  ]
  node [
    id 477
    label "os&#322;abi&#263;"
  ]
  node [
    id 478
    label "disengage"
  ]
  node [
    id 479
    label "zdezorganizowa&#263;"
  ]
  node [
    id 480
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 481
    label "naukowiec"
  ]
  node [
    id 482
    label "provider"
  ]
  node [
    id 483
    label "b&#322;&#261;d"
  ]
  node [
    id 484
    label "hipertekst"
  ]
  node [
    id 485
    label "cyberprzestrze&#324;"
  ]
  node [
    id 486
    label "mem"
  ]
  node [
    id 487
    label "gra_sieciowa"
  ]
  node [
    id 488
    label "grooming"
  ]
  node [
    id 489
    label "media"
  ]
  node [
    id 490
    label "biznes_elektroniczny"
  ]
  node [
    id 491
    label "sie&#263;_komputerowa"
  ]
  node [
    id 492
    label "punkt_dost&#281;pu"
  ]
  node [
    id 493
    label "us&#322;uga_internetowa"
  ]
  node [
    id 494
    label "netbook"
  ]
  node [
    id 495
    label "e-hazard"
  ]
  node [
    id 496
    label "podcast"
  ]
  node [
    id 497
    label "strona"
  ]
  node [
    id 498
    label "prezenter"
  ]
  node [
    id 499
    label "typ"
  ]
  node [
    id 500
    label "mildew"
  ]
  node [
    id 501
    label "zi&#243;&#322;ko"
  ]
  node [
    id 502
    label "motif"
  ]
  node [
    id 503
    label "pozowanie"
  ]
  node [
    id 504
    label "ideal"
  ]
  node [
    id 505
    label "wz&#243;r"
  ]
  node [
    id 506
    label "matryca"
  ]
  node [
    id 507
    label "adaptation"
  ]
  node [
    id 508
    label "ruch"
  ]
  node [
    id 509
    label "pozowa&#263;"
  ]
  node [
    id 510
    label "imitacja"
  ]
  node [
    id 511
    label "orygina&#322;"
  ]
  node [
    id 512
    label "facet"
  ]
  node [
    id 513
    label "miniatura"
  ]
  node [
    id 514
    label "podejrzany"
  ]
  node [
    id 515
    label "s&#261;downictwo"
  ]
  node [
    id 516
    label "biuro"
  ]
  node [
    id 517
    label "court"
  ]
  node [
    id 518
    label "forum"
  ]
  node [
    id 519
    label "bronienie"
  ]
  node [
    id 520
    label "urz&#261;d"
  ]
  node [
    id 521
    label "oskar&#380;yciel"
  ]
  node [
    id 522
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 523
    label "skazany"
  ]
  node [
    id 524
    label "post&#281;powanie"
  ]
  node [
    id 525
    label "broni&#263;"
  ]
  node [
    id 526
    label "my&#347;l"
  ]
  node [
    id 527
    label "pods&#261;dny"
  ]
  node [
    id 528
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 529
    label "obrona"
  ]
  node [
    id 530
    label "wypowied&#378;"
  ]
  node [
    id 531
    label "instytucja"
  ]
  node [
    id 532
    label "antylogizm"
  ]
  node [
    id 533
    label "konektyw"
  ]
  node [
    id 534
    label "&#347;wiadek"
  ]
  node [
    id 535
    label "procesowicz"
  ]
  node [
    id 536
    label "lias"
  ]
  node [
    id 537
    label "dzia&#322;"
  ]
  node [
    id 538
    label "pi&#281;tro"
  ]
  node [
    id 539
    label "klasa"
  ]
  node [
    id 540
    label "filia"
  ]
  node [
    id 541
    label "malm"
  ]
  node [
    id 542
    label "dogger"
  ]
  node [
    id 543
    label "promocja"
  ]
  node [
    id 544
    label "kurs"
  ]
  node [
    id 545
    label "bank"
  ]
  node [
    id 546
    label "formacja"
  ]
  node [
    id 547
    label "ajencja"
  ]
  node [
    id 548
    label "wojsko"
  ]
  node [
    id 549
    label "siedziba"
  ]
  node [
    id 550
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 551
    label "agencja"
  ]
  node [
    id 552
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 553
    label "szpital"
  ]
  node [
    id 554
    label "algebra_liniowa"
  ]
  node [
    id 555
    label "macierz_j&#261;drowa"
  ]
  node [
    id 556
    label "atom"
  ]
  node [
    id 557
    label "nukleon"
  ]
  node [
    id 558
    label "kariokineza"
  ]
  node [
    id 559
    label "core"
  ]
  node [
    id 560
    label "chemia_j&#261;drowa"
  ]
  node [
    id 561
    label "anorchizm"
  ]
  node [
    id 562
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 563
    label "nasieniak"
  ]
  node [
    id 564
    label "wn&#281;trostwo"
  ]
  node [
    id 565
    label "ziarno"
  ]
  node [
    id 566
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 567
    label "j&#261;derko"
  ]
  node [
    id 568
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 569
    label "jajo"
  ]
  node [
    id 570
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 571
    label "chromosom"
  ]
  node [
    id 572
    label "organellum"
  ]
  node [
    id 573
    label "moszna"
  ]
  node [
    id 574
    label "przeciwobraz"
  ]
  node [
    id 575
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 576
    label "protoplazma"
  ]
  node [
    id 577
    label "znaczenie"
  ]
  node [
    id 578
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 579
    label "nukleosynteza"
  ]
  node [
    id 580
    label "subsystem"
  ]
  node [
    id 581
    label "ko&#322;o"
  ]
  node [
    id 582
    label "granica"
  ]
  node [
    id 583
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 584
    label "suport"
  ]
  node [
    id 585
    label "prosta"
  ]
  node [
    id 586
    label "o&#347;rodek"
  ]
  node [
    id 587
    label "eonotem"
  ]
  node [
    id 588
    label "constellation"
  ]
  node [
    id 589
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 590
    label "Ptak_Rajski"
  ]
  node [
    id 591
    label "W&#281;&#380;ownik"
  ]
  node [
    id 592
    label "Panna"
  ]
  node [
    id 593
    label "W&#261;&#380;"
  ]
  node [
    id 594
    label "blokada"
  ]
  node [
    id 595
    label "hurtownia"
  ]
  node [
    id 596
    label "pomieszczenie"
  ]
  node [
    id 597
    label "pole"
  ]
  node [
    id 598
    label "pas"
  ]
  node [
    id 599
    label "basic"
  ]
  node [
    id 600
    label "sk&#322;adnik"
  ]
  node [
    id 601
    label "sklep"
  ]
  node [
    id 602
    label "obr&#243;bka"
  ]
  node [
    id 603
    label "constitution"
  ]
  node [
    id 604
    label "fabryka"
  ]
  node [
    id 605
    label "&#347;wiat&#322;o"
  ]
  node [
    id 606
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 607
    label "syf"
  ]
  node [
    id 608
    label "rank_and_file"
  ]
  node [
    id 609
    label "tabulacja"
  ]
  node [
    id 610
    label "tekst"
  ]
  node [
    id 611
    label "finansowo"
  ]
  node [
    id 612
    label "mi&#281;dzybankowy"
  ]
  node [
    id 613
    label "pozamaterialny"
  ]
  node [
    id 614
    label "materjalny"
  ]
  node [
    id 615
    label "fizyczny"
  ]
  node [
    id 616
    label "materialny"
  ]
  node [
    id 617
    label "niematerialnie"
  ]
  node [
    id 618
    label "financially"
  ]
  node [
    id 619
    label "fiscally"
  ]
  node [
    id 620
    label "bytowo"
  ]
  node [
    id 621
    label "ekonomicznie"
  ]
  node [
    id 622
    label "pracownik"
  ]
  node [
    id 623
    label "fizykalnie"
  ]
  node [
    id 624
    label "materializowanie"
  ]
  node [
    id 625
    label "fizycznie"
  ]
  node [
    id 626
    label "namacalny"
  ]
  node [
    id 627
    label "widoczny"
  ]
  node [
    id 628
    label "zmaterializowanie"
  ]
  node [
    id 629
    label "organiczny"
  ]
  node [
    id 630
    label "gimnastyczny"
  ]
  node [
    id 631
    label "zaskakiwa&#263;"
  ]
  node [
    id 632
    label "fold"
  ]
  node [
    id 633
    label "podejmowa&#263;"
  ]
  node [
    id 634
    label "cover"
  ]
  node [
    id 635
    label "rozumie&#263;"
  ]
  node [
    id 636
    label "senator"
  ]
  node [
    id 637
    label "mie&#263;"
  ]
  node [
    id 638
    label "obj&#261;&#263;"
  ]
  node [
    id 639
    label "obejmowanie"
  ]
  node [
    id 640
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 641
    label "powodowa&#263;"
  ]
  node [
    id 642
    label "involve"
  ]
  node [
    id 643
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 644
    label "dotyczy&#263;"
  ]
  node [
    id 645
    label "zagarnia&#263;"
  ]
  node [
    id 646
    label "embrace"
  ]
  node [
    id 647
    label "dotyka&#263;"
  ]
  node [
    id 648
    label "podnosi&#263;"
  ]
  node [
    id 649
    label "draw"
  ]
  node [
    id 650
    label "drive"
  ]
  node [
    id 651
    label "zmienia&#263;"
  ]
  node [
    id 652
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 653
    label "admit"
  ]
  node [
    id 654
    label "reagowa&#263;"
  ]
  node [
    id 655
    label "treat"
  ]
  node [
    id 656
    label "d&#322;o&#324;"
  ]
  node [
    id 657
    label "spotyka&#263;"
  ]
  node [
    id 658
    label "rani&#263;"
  ]
  node [
    id 659
    label "s&#261;siadowa&#263;"
  ]
  node [
    id 660
    label "fit"
  ]
  node [
    id 661
    label "rusza&#263;"
  ]
  node [
    id 662
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 663
    label "motywowa&#263;"
  ]
  node [
    id 664
    label "act"
  ]
  node [
    id 665
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 666
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 667
    label "bargain"
  ]
  node [
    id 668
    label "tycze&#263;"
  ]
  node [
    id 669
    label "hide"
  ]
  node [
    id 670
    label "czu&#263;"
  ]
  node [
    id 671
    label "support"
  ]
  node [
    id 672
    label "need"
  ]
  node [
    id 673
    label "wiedzie&#263;"
  ]
  node [
    id 674
    label "kuma&#263;"
  ]
  node [
    id 675
    label "give"
  ]
  node [
    id 676
    label "match"
  ]
  node [
    id 677
    label "empatia"
  ]
  node [
    id 678
    label "odbiera&#263;"
  ]
  node [
    id 679
    label "see"
  ]
  node [
    id 680
    label "zna&#263;"
  ]
  node [
    id 681
    label "kawa&#322;ek"
  ]
  node [
    id 682
    label "aran&#380;acja"
  ]
  node [
    id 683
    label "patrycjat"
  ]
  node [
    id 684
    label "parlamentarzysta"
  ]
  node [
    id 685
    label "samorz&#261;dowiec"
  ]
  node [
    id 686
    label "przedstawiciel"
  ]
  node [
    id 687
    label "senat"
  ]
  node [
    id 688
    label "klubista"
  ]
  node [
    id 689
    label "manipulate"
  ]
  node [
    id 690
    label "assume"
  ]
  node [
    id 691
    label "podj&#261;&#263;"
  ]
  node [
    id 692
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 693
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 694
    label "skuma&#263;"
  ]
  node [
    id 695
    label "zagarn&#261;&#263;"
  ]
  node [
    id 696
    label "obj&#281;cie"
  ]
  node [
    id 697
    label "spowodowa&#263;"
  ]
  node [
    id 698
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 699
    label "do"
  ]
  node [
    id 700
    label "zacz&#261;&#263;"
  ]
  node [
    id 701
    label "dotkn&#261;&#263;"
  ]
  node [
    id 702
    label "ujmowa&#263;"
  ]
  node [
    id 703
    label "zabiera&#263;"
  ]
  node [
    id 704
    label "porywa&#263;"
  ]
  node [
    id 705
    label "gyp"
  ]
  node [
    id 706
    label "scoop"
  ]
  node [
    id 707
    label "przysuwa&#263;"
  ]
  node [
    id 708
    label "odnoszenie_si&#281;"
  ]
  node [
    id 709
    label "wdarcie_si&#281;"
  ]
  node [
    id 710
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 711
    label "zagarnianie"
  ]
  node [
    id 712
    label "nadp&#322;ywanie"
  ]
  node [
    id 713
    label "encompassment"
  ]
  node [
    id 714
    label "inclusion"
  ]
  node [
    id 715
    label "dotykanie"
  ]
  node [
    id 716
    label "podpadanie"
  ]
  node [
    id 717
    label "podejmowanie"
  ]
  node [
    id 718
    label "czucie"
  ]
  node [
    id 719
    label "nadp&#322;yni&#281;cie"
  ]
  node [
    id 720
    label "dzianie_si&#281;"
  ]
  node [
    id 721
    label "rozumienie"
  ]
  node [
    id 722
    label "zobowi&#261;zywanie_si&#281;"
  ]
  node [
    id 723
    label "wdzieranie_si&#281;"
  ]
  node [
    id 724
    label "bycie_w_posiadaniu"
  ]
  node [
    id 725
    label "dziwi&#263;"
  ]
  node [
    id 726
    label "surprise"
  ]
  node [
    id 727
    label "Fox"
  ]
  node [
    id 728
    label "wpada&#263;"
  ]
  node [
    id 729
    label "kognicja"
  ]
  node [
    id 730
    label "przebieg"
  ]
  node [
    id 731
    label "rozprawa"
  ]
  node [
    id 732
    label "legislacyjnie"
  ]
  node [
    id 733
    label "przes&#322;anka"
  ]
  node [
    id 734
    label "zjawisko"
  ]
  node [
    id 735
    label "nast&#281;pstwo"
  ]
  node [
    id 736
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 737
    label "przebiec"
  ]
  node [
    id 738
    label "charakter"
  ]
  node [
    id 739
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 740
    label "motyw"
  ]
  node [
    id 741
    label "przebiegni&#281;cie"
  ]
  node [
    id 742
    label "fabu&#322;a"
  ]
  node [
    id 743
    label "rozumowanie"
  ]
  node [
    id 744
    label "opracowanie"
  ]
  node [
    id 745
    label "obrady"
  ]
  node [
    id 746
    label "cytat"
  ]
  node [
    id 747
    label "obja&#347;nienie"
  ]
  node [
    id 748
    label "s&#261;dzenie"
  ]
  node [
    id 749
    label "linia"
  ]
  node [
    id 750
    label "procedura"
  ]
  node [
    id 751
    label "room"
  ]
  node [
    id 752
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 753
    label "sequence"
  ]
  node [
    id 754
    label "praca"
  ]
  node [
    id 755
    label "cycle"
  ]
  node [
    id 756
    label "fakt"
  ]
  node [
    id 757
    label "przyczyna"
  ]
  node [
    id 758
    label "wnioskowanie"
  ]
  node [
    id 759
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 760
    label "prawo"
  ]
  node [
    id 761
    label "odczuwa&#263;"
  ]
  node [
    id 762
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 763
    label "wydziedziczy&#263;"
  ]
  node [
    id 764
    label "skrupienie_si&#281;"
  ]
  node [
    id 765
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 766
    label "wydziedziczenie"
  ]
  node [
    id 767
    label "odczucie"
  ]
  node [
    id 768
    label "pocz&#261;tek"
  ]
  node [
    id 769
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 770
    label "koszula_Dejaniry"
  ]
  node [
    id 771
    label "kolejno&#347;&#263;"
  ]
  node [
    id 772
    label "odczuwanie"
  ]
  node [
    id 773
    label "event"
  ]
  node [
    id 774
    label "rezultat"
  ]
  node [
    id 775
    label "skrupianie_si&#281;"
  ]
  node [
    id 776
    label "odczu&#263;"
  ]
  node [
    id 777
    label "boski"
  ]
  node [
    id 778
    label "krajobraz"
  ]
  node [
    id 779
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 780
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 781
    label "przywidzenie"
  ]
  node [
    id 782
    label "presence"
  ]
  node [
    id 783
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 784
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 785
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 786
    label "w&#281;ze&#322;"
  ]
  node [
    id 787
    label "consort"
  ]
  node [
    id 788
    label "cement"
  ]
  node [
    id 789
    label "opakowa&#263;"
  ]
  node [
    id 790
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 791
    label "relate"
  ]
  node [
    id 792
    label "form"
  ]
  node [
    id 793
    label "tobo&#322;ek"
  ]
  node [
    id 794
    label "unify"
  ]
  node [
    id 795
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 796
    label "incorporate"
  ]
  node [
    id 797
    label "wi&#281;&#378;"
  ]
  node [
    id 798
    label "bind"
  ]
  node [
    id 799
    label "zawi&#261;za&#263;"
  ]
  node [
    id 800
    label "zaprawa"
  ]
  node [
    id 801
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 802
    label "powi&#261;za&#263;"
  ]
  node [
    id 803
    label "scali&#263;"
  ]
  node [
    id 804
    label "zatrzyma&#263;"
  ]
  node [
    id 805
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 806
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 807
    label "komornik"
  ]
  node [
    id 808
    label "suspend"
  ]
  node [
    id 809
    label "zaczepi&#263;"
  ]
  node [
    id 810
    label "bury"
  ]
  node [
    id 811
    label "bankrupt"
  ]
  node [
    id 812
    label "zabra&#263;"
  ]
  node [
    id 813
    label "zamkn&#261;&#263;"
  ]
  node [
    id 814
    label "przechowa&#263;"
  ]
  node [
    id 815
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 816
    label "zaaresztowa&#263;"
  ]
  node [
    id 817
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 818
    label "przerwa&#263;"
  ]
  node [
    id 819
    label "unieruchomi&#263;"
  ]
  node [
    id 820
    label "anticipate"
  ]
  node [
    id 821
    label "p&#281;tla"
  ]
  node [
    id 822
    label "zawi&#261;zek"
  ]
  node [
    id 823
    label "wytworzy&#263;"
  ]
  node [
    id 824
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 825
    label "zjednoczy&#263;"
  ]
  node [
    id 826
    label "ally"
  ]
  node [
    id 827
    label "connect"
  ]
  node [
    id 828
    label "obowi&#261;za&#263;"
  ]
  node [
    id 829
    label "perpetrate"
  ]
  node [
    id 830
    label "articulation"
  ]
  node [
    id 831
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 832
    label "catch"
  ]
  node [
    id 833
    label "zrobi&#263;"
  ]
  node [
    id 834
    label "dokoptowa&#263;"
  ]
  node [
    id 835
    label "stworzy&#263;"
  ]
  node [
    id 836
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 837
    label "po&#322;&#261;czenie"
  ]
  node [
    id 838
    label "pack"
  ]
  node [
    id 839
    label "owin&#261;&#263;"
  ]
  node [
    id 840
    label "zwielokrotni&#263;_si&#281;"
  ]
  node [
    id 841
    label "sta&#263;_si&#281;"
  ]
  node [
    id 842
    label "clot"
  ]
  node [
    id 843
    label "przybra&#263;_na_sile"
  ]
  node [
    id 844
    label "narosn&#261;&#263;"
  ]
  node [
    id 845
    label "stwardnie&#263;"
  ]
  node [
    id 846
    label "solidify"
  ]
  node [
    id 847
    label "znieruchomie&#263;"
  ]
  node [
    id 848
    label "zg&#281;stnie&#263;"
  ]
  node [
    id 849
    label "porazi&#263;"
  ]
  node [
    id 850
    label "overwhelm"
  ]
  node [
    id 851
    label "zwi&#261;zanie"
  ]
  node [
    id 852
    label "zgrupowanie"
  ]
  node [
    id 853
    label "materia&#322;_budowlany"
  ]
  node [
    id 854
    label "mortar"
  ]
  node [
    id 855
    label "podk&#322;ad"
  ]
  node [
    id 856
    label "training"
  ]
  node [
    id 857
    label "mieszanina"
  ]
  node [
    id 858
    label "&#263;wiczenie"
  ]
  node [
    id 859
    label "s&#322;oik"
  ]
  node [
    id 860
    label "przyprawa"
  ]
  node [
    id 861
    label "kastra"
  ]
  node [
    id 862
    label "wi&#261;za&#263;"
  ]
  node [
    id 863
    label "przetw&#243;r"
  ]
  node [
    id 864
    label "obw&#243;d"
  ]
  node [
    id 865
    label "praktyka"
  ]
  node [
    id 866
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 867
    label "wi&#261;zanie"
  ]
  node [
    id 868
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 869
    label "bratnia_dusza"
  ]
  node [
    id 870
    label "trasa"
  ]
  node [
    id 871
    label "uczesanie"
  ]
  node [
    id 872
    label "orbita"
  ]
  node [
    id 873
    label "kryszta&#322;"
  ]
  node [
    id 874
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 875
    label "graf"
  ]
  node [
    id 876
    label "hitch"
  ]
  node [
    id 877
    label "akcja"
  ]
  node [
    id 878
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 879
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 880
    label "marriage"
  ]
  node [
    id 881
    label "ekliptyka"
  ]
  node [
    id 882
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 883
    label "problem"
  ]
  node [
    id 884
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 885
    label "fala_stoj&#261;ca"
  ]
  node [
    id 886
    label "tying"
  ]
  node [
    id 887
    label "argument"
  ]
  node [
    id 888
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 889
    label "mila_morska"
  ]
  node [
    id 890
    label "skupienie"
  ]
  node [
    id 891
    label "zgrubienie"
  ]
  node [
    id 892
    label "pismo_klinowe"
  ]
  node [
    id 893
    label "przeci&#281;cie"
  ]
  node [
    id 894
    label "band"
  ]
  node [
    id 895
    label "zwi&#261;zek"
  ]
  node [
    id 896
    label "marketing_afiliacyjny"
  ]
  node [
    id 897
    label "tob&#243;&#322;"
  ]
  node [
    id 898
    label "alga"
  ]
  node [
    id 899
    label "tobo&#322;ki"
  ]
  node [
    id 900
    label "wiciowiec"
  ]
  node [
    id 901
    label "spoiwo"
  ]
  node [
    id 902
    label "wertebroplastyka"
  ]
  node [
    id 903
    label "wype&#322;nienie"
  ]
  node [
    id 904
    label "tworzywo"
  ]
  node [
    id 905
    label "z&#261;b"
  ]
  node [
    id 906
    label "tkanka_kostna"
  ]
  node [
    id 907
    label "posiada&#263;"
  ]
  node [
    id 908
    label "poci&#261;ga&#263;"
  ]
  node [
    id 909
    label "zbiera&#263;"
  ]
  node [
    id 910
    label "congregate"
  ]
  node [
    id 911
    label "organizowa&#263;"
  ]
  node [
    id 912
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 913
    label "czyni&#263;"
  ]
  node [
    id 914
    label "stylizowa&#263;"
  ]
  node [
    id 915
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 916
    label "falowa&#263;"
  ]
  node [
    id 917
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 918
    label "peddle"
  ]
  node [
    id 919
    label "wydala&#263;"
  ]
  node [
    id 920
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 921
    label "tentegowa&#263;"
  ]
  node [
    id 922
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 923
    label "urz&#261;dza&#263;"
  ]
  node [
    id 924
    label "oszukiwa&#263;"
  ]
  node [
    id 925
    label "work"
  ]
  node [
    id 926
    label "ukazywa&#263;"
  ]
  node [
    id 927
    label "przerabia&#263;"
  ]
  node [
    id 928
    label "post&#281;powa&#263;"
  ]
  node [
    id 929
    label "przejmowa&#263;"
  ]
  node [
    id 930
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 931
    label "bra&#263;"
  ]
  node [
    id 932
    label "pozyskiwa&#263;"
  ]
  node [
    id 933
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 934
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 935
    label "dostawa&#263;"
  ]
  node [
    id 936
    label "consolidate"
  ]
  node [
    id 937
    label "umieszcza&#263;"
  ]
  node [
    id 938
    label "uk&#322;ada&#263;"
  ]
  node [
    id 939
    label "zawiera&#263;"
  ]
  node [
    id 940
    label "keep_open"
  ]
  node [
    id 941
    label "pull"
  ]
  node [
    id 942
    label "upija&#263;"
  ]
  node [
    id 943
    label "wsysa&#263;"
  ]
  node [
    id 944
    label "przechyla&#263;"
  ]
  node [
    id 945
    label "pokrywa&#263;"
  ]
  node [
    id 946
    label "trail"
  ]
  node [
    id 947
    label "przesuwa&#263;"
  ]
  node [
    id 948
    label "skutkowa&#263;"
  ]
  node [
    id 949
    label "nos"
  ]
  node [
    id 950
    label "powiewa&#263;"
  ]
  node [
    id 951
    label "katar"
  ]
  node [
    id 952
    label "mani&#263;"
  ]
  node [
    id 953
    label "force"
  ]
  node [
    id 954
    label "abstrakcja"
  ]
  node [
    id 955
    label "czas"
  ]
  node [
    id 956
    label "chemikalia"
  ]
  node [
    id 957
    label "substancja"
  ]
  node [
    id 958
    label "poprzedzanie"
  ]
  node [
    id 959
    label "czasoprzestrze&#324;"
  ]
  node [
    id 960
    label "laba"
  ]
  node [
    id 961
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 962
    label "chronometria"
  ]
  node [
    id 963
    label "rachuba_czasu"
  ]
  node [
    id 964
    label "przep&#322;ywanie"
  ]
  node [
    id 965
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 966
    label "czasokres"
  ]
  node [
    id 967
    label "odczyt"
  ]
  node [
    id 968
    label "chwila"
  ]
  node [
    id 969
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 970
    label "dzieje"
  ]
  node [
    id 971
    label "kategoria_gramatyczna"
  ]
  node [
    id 972
    label "poprzedzenie"
  ]
  node [
    id 973
    label "trawienie"
  ]
  node [
    id 974
    label "pochodzi&#263;"
  ]
  node [
    id 975
    label "period"
  ]
  node [
    id 976
    label "okres_czasu"
  ]
  node [
    id 977
    label "poprzedza&#263;"
  ]
  node [
    id 978
    label "schy&#322;ek"
  ]
  node [
    id 979
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 980
    label "odwlekanie_si&#281;"
  ]
  node [
    id 981
    label "zegar"
  ]
  node [
    id 982
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 983
    label "czwarty_wymiar"
  ]
  node [
    id 984
    label "pochodzenie"
  ]
  node [
    id 985
    label "koniugacja"
  ]
  node [
    id 986
    label "Zeitgeist"
  ]
  node [
    id 987
    label "trawi&#263;"
  ]
  node [
    id 988
    label "pogoda"
  ]
  node [
    id 989
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 990
    label "poprzedzi&#263;"
  ]
  node [
    id 991
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 992
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 993
    label "time_period"
  ]
  node [
    id 994
    label "po&#322;o&#380;enie"
  ]
  node [
    id 995
    label "sprawa"
  ]
  node [
    id 996
    label "ust&#281;p"
  ]
  node [
    id 997
    label "plan"
  ]
  node [
    id 998
    label "obiekt_matematyczny"
  ]
  node [
    id 999
    label "problemat"
  ]
  node [
    id 1000
    label "plamka"
  ]
  node [
    id 1001
    label "stopie&#324;_pisma"
  ]
  node [
    id 1002
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1003
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1004
    label "mark"
  ]
  node [
    id 1005
    label "problematyka"
  ]
  node [
    id 1006
    label "obiekt"
  ]
  node [
    id 1007
    label "zapunktowa&#263;"
  ]
  node [
    id 1008
    label "podpunkt"
  ]
  node [
    id 1009
    label "kres"
  ]
  node [
    id 1010
    label "przestrze&#324;"
  ]
  node [
    id 1011
    label "point"
  ]
  node [
    id 1012
    label "pozycja"
  ]
  node [
    id 1013
    label "warunek_lokalowy"
  ]
  node [
    id 1014
    label "plac"
  ]
  node [
    id 1015
    label "location"
  ]
  node [
    id 1016
    label "uwaga"
  ]
  node [
    id 1017
    label "status"
  ]
  node [
    id 1018
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1019
    label "cia&#322;o"
  ]
  node [
    id 1020
    label "rz&#261;d"
  ]
  node [
    id 1021
    label "przenikanie"
  ]
  node [
    id 1022
    label "byt"
  ]
  node [
    id 1023
    label "cz&#261;steczka"
  ]
  node [
    id 1024
    label "temperatura_krytyczna"
  ]
  node [
    id 1025
    label "przenika&#263;"
  ]
  node [
    id 1026
    label "smolisty"
  ]
  node [
    id 1027
    label "proces_my&#347;lowy"
  ]
  node [
    id 1028
    label "abstractedness"
  ]
  node [
    id 1029
    label "abstraction"
  ]
  node [
    id 1030
    label "obraz"
  ]
  node [
    id 1031
    label "sytuacja"
  ]
  node [
    id 1032
    label "spalenie"
  ]
  node [
    id 1033
    label "spalanie"
  ]
  node [
    id 1034
    label "rozdzieli&#263;"
  ]
  node [
    id 1035
    label "allocate"
  ]
  node [
    id 1036
    label "rozda&#263;"
  ]
  node [
    id 1037
    label "oddzieli&#263;"
  ]
  node [
    id 1038
    label "distribute"
  ]
  node [
    id 1039
    label "transgress"
  ]
  node [
    id 1040
    label "break"
  ]
  node [
    id 1041
    label "podzieli&#263;"
  ]
  node [
    id 1042
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 1043
    label "odr&#243;&#380;ni&#263;"
  ]
  node [
    id 1044
    label "amputate"
  ]
  node [
    id 1045
    label "separate"
  ]
  node [
    id 1046
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1047
    label "nale&#380;nie"
  ]
  node [
    id 1048
    label "stosowny"
  ]
  node [
    id 1049
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1050
    label "nale&#380;ycie"
  ]
  node [
    id 1051
    label "odpowiedni"
  ]
  node [
    id 1052
    label "nale&#380;ny"
  ]
  node [
    id 1053
    label "charakterystycznie"
  ]
  node [
    id 1054
    label "dobrze"
  ]
  node [
    id 1055
    label "prawdziwie"
  ]
  node [
    id 1056
    label "przystojnie"
  ]
  node [
    id 1057
    label "nale&#380;yty"
  ]
  node [
    id 1058
    label "zadowalaj&#261;co"
  ]
  node [
    id 1059
    label "rz&#261;dnie"
  ]
  node [
    id 1060
    label "typowy"
  ]
  node [
    id 1061
    label "uprawniony"
  ]
  node [
    id 1062
    label "zasadniczy"
  ]
  node [
    id 1063
    label "stosownie"
  ]
  node [
    id 1064
    label "taki"
  ]
  node [
    id 1065
    label "charakterystyczny"
  ]
  node [
    id 1066
    label "prawdziwy"
  ]
  node [
    id 1067
    label "ten"
  ]
  node [
    id 1068
    label "dobry"
  ]
  node [
    id 1069
    label "zdarzony"
  ]
  node [
    id 1070
    label "odpowiadanie"
  ]
  node [
    id 1071
    label "specjalny"
  ]
  node [
    id 1072
    label "teologicznie"
  ]
  node [
    id 1073
    label "wiedza"
  ]
  node [
    id 1074
    label "belief"
  ]
  node [
    id 1075
    label "zderzenie_si&#281;"
  ]
  node [
    id 1076
    label "twierdzenie"
  ]
  node [
    id 1077
    label "teoria_Dowa"
  ]
  node [
    id 1078
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 1079
    label "przypuszczenie"
  ]
  node [
    id 1080
    label "teoria_Fishera"
  ]
  node [
    id 1081
    label "teoria_Arrheniusa"
  ]
  node [
    id 1082
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1083
    label "spotkanie"
  ]
  node [
    id 1084
    label "wys&#322;uchanie"
  ]
  node [
    id 1085
    label "porobienie"
  ]
  node [
    id 1086
    label "audience"
  ]
  node [
    id 1087
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1088
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 1089
    label "kierunek"
  ]
  node [
    id 1090
    label "zorientowanie_si&#281;"
  ]
  node [
    id 1091
    label "pogubienie_si&#281;"
  ]
  node [
    id 1092
    label "orientation"
  ]
  node [
    id 1093
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 1094
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 1095
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1096
    label "gubienie_si&#281;"
  ]
  node [
    id 1097
    label "kszta&#322;t"
  ]
  node [
    id 1098
    label "temat"
  ]
  node [
    id 1099
    label "jednostka_systematyczna"
  ]
  node [
    id 1100
    label "poznanie"
  ]
  node [
    id 1101
    label "leksem"
  ]
  node [
    id 1102
    label "dzie&#322;o"
  ]
  node [
    id 1103
    label "blaszka"
  ]
  node [
    id 1104
    label "kantyzm"
  ]
  node [
    id 1105
    label "do&#322;ek"
  ]
  node [
    id 1106
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1107
    label "gwiazda"
  ]
  node [
    id 1108
    label "formality"
  ]
  node [
    id 1109
    label "wygl&#261;d"
  ]
  node [
    id 1110
    label "mode"
  ]
  node [
    id 1111
    label "morfem"
  ]
  node [
    id 1112
    label "rdze&#324;"
  ]
  node [
    id 1113
    label "posta&#263;"
  ]
  node [
    id 1114
    label "kielich"
  ]
  node [
    id 1115
    label "ornamentyka"
  ]
  node [
    id 1116
    label "pasmo"
  ]
  node [
    id 1117
    label "zwyczaj"
  ]
  node [
    id 1118
    label "punkt_widzenia"
  ]
  node [
    id 1119
    label "g&#322;owa"
  ]
  node [
    id 1120
    label "naczynie"
  ]
  node [
    id 1121
    label "p&#322;at"
  ]
  node [
    id 1122
    label "maszyna_drukarska"
  ]
  node [
    id 1123
    label "style"
  ]
  node [
    id 1124
    label "linearno&#347;&#263;"
  ]
  node [
    id 1125
    label "wyra&#380;enie"
  ]
  node [
    id 1126
    label "spirala"
  ]
  node [
    id 1127
    label "dyspozycja"
  ]
  node [
    id 1128
    label "odmiana"
  ]
  node [
    id 1129
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1130
    label "October"
  ]
  node [
    id 1131
    label "creation"
  ]
  node [
    id 1132
    label "arystotelizm"
  ]
  node [
    id 1133
    label "szablon"
  ]
  node [
    id 1134
    label "zrozumienie"
  ]
  node [
    id 1135
    label "obronienie"
  ]
  node [
    id 1136
    label "wydanie"
  ]
  node [
    id 1137
    label "wyg&#322;oszenie"
  ]
  node [
    id 1138
    label "oddzia&#322;anie"
  ]
  node [
    id 1139
    label "address"
  ]
  node [
    id 1140
    label "wydobycie"
  ]
  node [
    id 1141
    label "wyst&#261;pienie"
  ]
  node [
    id 1142
    label "talk"
  ]
  node [
    id 1143
    label "odzyskanie"
  ]
  node [
    id 1144
    label "sermon"
  ]
  node [
    id 1145
    label "wyznaczenie"
  ]
  node [
    id 1146
    label "przyczynienie_si&#281;"
  ]
  node [
    id 1147
    label "zwr&#243;cenie"
  ]
  node [
    id 1148
    label "decydowa&#263;"
  ]
  node [
    id 1149
    label "signify"
  ]
  node [
    id 1150
    label "decide"
  ]
  node [
    id 1151
    label "klasyfikator"
  ]
  node [
    id 1152
    label "mean"
  ]
  node [
    id 1153
    label "zasoby_kopalin"
  ]
  node [
    id 1154
    label "podmiot_gospodarczy"
  ]
  node [
    id 1155
    label "z&#322;o&#380;e"
  ]
  node [
    id 1156
    label "zaleganie"
  ]
  node [
    id 1157
    label "bilansowo&#347;&#263;"
  ]
  node [
    id 1158
    label "zalega&#263;"
  ]
  node [
    id 1159
    label "wychodnia"
  ]
  node [
    id 1160
    label "monetarny"
  ]
  node [
    id 1161
    label "liczenie"
  ]
  node [
    id 1162
    label "czyn"
  ]
  node [
    id 1163
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1164
    label "supremum"
  ]
  node [
    id 1165
    label "laparotomia"
  ]
  node [
    id 1166
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1167
    label "matematyka"
  ]
  node [
    id 1168
    label "rzut"
  ]
  node [
    id 1169
    label "liczy&#263;"
  ]
  node [
    id 1170
    label "torakotomia"
  ]
  node [
    id 1171
    label "chirurg"
  ]
  node [
    id 1172
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 1173
    label "zabieg"
  ]
  node [
    id 1174
    label "szew"
  ]
  node [
    id 1175
    label "funkcja"
  ]
  node [
    id 1176
    label "mathematical_process"
  ]
  node [
    id 1177
    label "infimum"
  ]
  node [
    id 1178
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1179
    label "gra"
  ]
  node [
    id 1180
    label "pocz&#261;tki"
  ]
  node [
    id 1181
    label "wzorzec_projektowy"
  ]
  node [
    id 1182
    label "dziedzina"
  ]
  node [
    id 1183
    label "wrinkle"
  ]
  node [
    id 1184
    label "dokument"
  ]
  node [
    id 1185
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1186
    label "rachunek_operatorowy"
  ]
  node [
    id 1187
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1188
    label "kryptologia"
  ]
  node [
    id 1189
    label "logicyzm"
  ]
  node [
    id 1190
    label "logika"
  ]
  node [
    id 1191
    label "matematyka_czysta"
  ]
  node [
    id 1192
    label "forsing"
  ]
  node [
    id 1193
    label "modelowanie_matematyczne"
  ]
  node [
    id 1194
    label "matma"
  ]
  node [
    id 1195
    label "teoria_katastrof"
  ]
  node [
    id 1196
    label "fizyka_matematyczna"
  ]
  node [
    id 1197
    label "teoria_graf&#243;w"
  ]
  node [
    id 1198
    label "rachunki"
  ]
  node [
    id 1199
    label "topologia_algebraiczna"
  ]
  node [
    id 1200
    label "matematyka_stosowana"
  ]
  node [
    id 1201
    label "leczenie"
  ]
  node [
    id 1202
    label "operation"
  ]
  node [
    id 1203
    label "thoracotomy"
  ]
  node [
    id 1204
    label "medycyna"
  ]
  node [
    id 1205
    label "otwarcie"
  ]
  node [
    id 1206
    label "jama_brzuszna"
  ]
  node [
    id 1207
    label "laparotomy"
  ]
  node [
    id 1208
    label "pokolcowate"
  ]
  node [
    id 1209
    label "specjalista"
  ]
  node [
    id 1210
    label "spawanie"
  ]
  node [
    id 1211
    label "chirurgia"
  ]
  node [
    id 1212
    label "nitowanie"
  ]
  node [
    id 1213
    label "kokon"
  ]
  node [
    id 1214
    label "zszycie"
  ]
  node [
    id 1215
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1216
    label "blizna"
  ]
  node [
    id 1217
    label "suture"
  ]
  node [
    id 1218
    label "badanie"
  ]
  node [
    id 1219
    label "rachowanie"
  ]
  node [
    id 1220
    label "dyskalkulia"
  ]
  node [
    id 1221
    label "wynagrodzenie"
  ]
  node [
    id 1222
    label "rozliczanie"
  ]
  node [
    id 1223
    label "wymienianie"
  ]
  node [
    id 1224
    label "oznaczanie"
  ]
  node [
    id 1225
    label "wychodzenie"
  ]
  node [
    id 1226
    label "naliczenie_si&#281;"
  ]
  node [
    id 1227
    label "wyznaczanie"
  ]
  node [
    id 1228
    label "dodawanie"
  ]
  node [
    id 1229
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1230
    label "bang"
  ]
  node [
    id 1231
    label "spodziewanie_si&#281;"
  ]
  node [
    id 1232
    label "kwotowanie"
  ]
  node [
    id 1233
    label "rozliczenie"
  ]
  node [
    id 1234
    label "mierzenie"
  ]
  node [
    id 1235
    label "count"
  ]
  node [
    id 1236
    label "wycenianie"
  ]
  node [
    id 1237
    label "branie"
  ]
  node [
    id 1238
    label "sprowadzanie"
  ]
  node [
    id 1239
    label "przeliczanie"
  ]
  node [
    id 1240
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 1241
    label "odliczanie"
  ]
  node [
    id 1242
    label "przeliczenie"
  ]
  node [
    id 1243
    label "przyswoi&#263;"
  ]
  node [
    id 1244
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1245
    label "one"
  ]
  node [
    id 1246
    label "ewoluowanie"
  ]
  node [
    id 1247
    label "skala"
  ]
  node [
    id 1248
    label "przyswajanie"
  ]
  node [
    id 1249
    label "wyewoluowanie"
  ]
  node [
    id 1250
    label "przeliczy&#263;"
  ]
  node [
    id 1251
    label "wyewoluowa&#263;"
  ]
  node [
    id 1252
    label "ewoluowa&#263;"
  ]
  node [
    id 1253
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1254
    label "liczba_naturalna"
  ]
  node [
    id 1255
    label "czynnik_biotyczny"
  ]
  node [
    id 1256
    label "figura"
  ]
  node [
    id 1257
    label "individual"
  ]
  node [
    id 1258
    label "portrecista"
  ]
  node [
    id 1259
    label "przyswaja&#263;"
  ]
  node [
    id 1260
    label "przyswojenie"
  ]
  node [
    id 1261
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1262
    label "profanum"
  ]
  node [
    id 1263
    label "mikrokosmos"
  ]
  node [
    id 1264
    label "starzenie_si&#281;"
  ]
  node [
    id 1265
    label "duch"
  ]
  node [
    id 1266
    label "osoba"
  ]
  node [
    id 1267
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1268
    label "antropochoria"
  ]
  node [
    id 1269
    label "homo_sapiens"
  ]
  node [
    id 1270
    label "przelicza&#263;"
  ]
  node [
    id 1271
    label "ograniczenie"
  ]
  node [
    id 1272
    label "addytywno&#347;&#263;"
  ]
  node [
    id 1273
    label "function"
  ]
  node [
    id 1274
    label "zastosowanie"
  ]
  node [
    id 1275
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 1276
    label "powierzanie"
  ]
  node [
    id 1277
    label "cel"
  ]
  node [
    id 1278
    label "przeciwdziedzina"
  ]
  node [
    id 1279
    label "awansowa&#263;"
  ]
  node [
    id 1280
    label "stawia&#263;"
  ]
  node [
    id 1281
    label "wakowa&#263;"
  ]
  node [
    id 1282
    label "postawi&#263;"
  ]
  node [
    id 1283
    label "awansowanie"
  ]
  node [
    id 1284
    label "armia"
  ]
  node [
    id 1285
    label "nawr&#243;t_choroby"
  ]
  node [
    id 1286
    label "potomstwo"
  ]
  node [
    id 1287
    label "odwzorowanie"
  ]
  node [
    id 1288
    label "rysunek"
  ]
  node [
    id 1289
    label "scene"
  ]
  node [
    id 1290
    label "throw"
  ]
  node [
    id 1291
    label "float"
  ]
  node [
    id 1292
    label "projection"
  ]
  node [
    id 1293
    label "injection"
  ]
  node [
    id 1294
    label "blow"
  ]
  node [
    id 1295
    label "k&#322;ad"
  ]
  node [
    id 1296
    label "mold"
  ]
  node [
    id 1297
    label "report"
  ]
  node [
    id 1298
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1299
    label "wymienia&#263;"
  ]
  node [
    id 1300
    label "wycenia&#263;"
  ]
  node [
    id 1301
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1302
    label "rachowa&#263;"
  ]
  node [
    id 1303
    label "tell"
  ]
  node [
    id 1304
    label "odlicza&#263;"
  ]
  node [
    id 1305
    label "dodawa&#263;"
  ]
  node [
    id 1306
    label "wyznacza&#263;"
  ]
  node [
    id 1307
    label "policza&#263;"
  ]
  node [
    id 1308
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1309
    label "consist"
  ]
  node [
    id 1310
    label "ufa&#263;"
  ]
  node [
    id 1311
    label "trust"
  ]
  node [
    id 1312
    label "wierza&#263;"
  ]
  node [
    id 1313
    label "powierzy&#263;"
  ]
  node [
    id 1314
    label "chowa&#263;"
  ]
  node [
    id 1315
    label "powierza&#263;"
  ]
  node [
    id 1316
    label "monopol"
  ]
  node [
    id 1317
    label "income"
  ]
  node [
    id 1318
    label "stopa_procentowa"
  ]
  node [
    id 1319
    label "krzywa_Engla"
  ]
  node [
    id 1320
    label "korzy&#347;&#263;"
  ]
  node [
    id 1321
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1322
    label "wp&#322;yw"
  ]
  node [
    id 1323
    label "zaleta"
  ]
  node [
    id 1324
    label "dobro"
  ]
  node [
    id 1325
    label "kwota"
  ]
  node [
    id 1326
    label "&#347;lad"
  ]
  node [
    id 1327
    label "lobbysta"
  ]
  node [
    id 1328
    label "doch&#243;d_narodowy"
  ]
  node [
    id 1329
    label "make"
  ]
  node [
    id 1330
    label "determine"
  ]
  node [
    id 1331
    label "przestawa&#263;"
  ]
  node [
    id 1332
    label "&#380;y&#263;"
  ]
  node [
    id 1333
    label "coating"
  ]
  node [
    id 1334
    label "przebywa&#263;"
  ]
  node [
    id 1335
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 1336
    label "ko&#324;czy&#263;"
  ]
  node [
    id 1337
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1338
    label "finish_up"
  ]
  node [
    id 1339
    label "koszt"
  ]
  node [
    id 1340
    label "wych&#243;d"
  ]
  node [
    id 1341
    label "nak&#322;ad"
  ]
  node [
    id 1342
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 1343
    label "wyj&#347;cie"
  ]
  node [
    id 1344
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 1345
    label "sumpt"
  ]
  node [
    id 1346
    label "ubytek"
  ]
  node [
    id 1347
    label "r&#243;&#380;nica"
  ]
  node [
    id 1348
    label "spadek"
  ]
  node [
    id 1349
    label "pr&#243;chnica"
  ]
  node [
    id 1350
    label "brak"
  ]
  node [
    id 1351
    label "otw&#243;r"
  ]
  node [
    id 1352
    label "co&#347;"
  ]
  node [
    id 1353
    label "budynek"
  ]
  node [
    id 1354
    label "thing"
  ]
  node [
    id 1355
    label "rzecz"
  ]
  node [
    id 1356
    label "Chocho&#322;"
  ]
  node [
    id 1357
    label "Herkules_Poirot"
  ]
  node [
    id 1358
    label "Edyp"
  ]
  node [
    id 1359
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1360
    label "Harry_Potter"
  ]
  node [
    id 1361
    label "Casanova"
  ]
  node [
    id 1362
    label "Zgredek"
  ]
  node [
    id 1363
    label "Gargantua"
  ]
  node [
    id 1364
    label "Winnetou"
  ]
  node [
    id 1365
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1366
    label "Dulcynea"
  ]
  node [
    id 1367
    label "person"
  ]
  node [
    id 1368
    label "Plastu&#347;"
  ]
  node [
    id 1369
    label "Quasimodo"
  ]
  node [
    id 1370
    label "Sherlock_Holmes"
  ]
  node [
    id 1371
    label "Faust"
  ]
  node [
    id 1372
    label "Wallenrod"
  ]
  node [
    id 1373
    label "Dwukwiat"
  ]
  node [
    id 1374
    label "Don_Juan"
  ]
  node [
    id 1375
    label "Don_Kiszot"
  ]
  node [
    id 1376
    label "Hamlet"
  ]
  node [
    id 1377
    label "Werter"
  ]
  node [
    id 1378
    label "istota"
  ]
  node [
    id 1379
    label "Szwejk"
  ]
  node [
    id 1380
    label "masztab"
  ]
  node [
    id 1381
    label "kreska"
  ]
  node [
    id 1382
    label "podzia&#322;ka"
  ]
  node [
    id 1383
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 1384
    label "wielko&#347;&#263;"
  ]
  node [
    id 1385
    label "zero"
  ]
  node [
    id 1386
    label "interwa&#322;"
  ]
  node [
    id 1387
    label "przymiar"
  ]
  node [
    id 1388
    label "sfera"
  ]
  node [
    id 1389
    label "dominanta"
  ]
  node [
    id 1390
    label "tetrachord"
  ]
  node [
    id 1391
    label "scale"
  ]
  node [
    id 1392
    label "przedzia&#322;"
  ]
  node [
    id 1393
    label "podzakres"
  ]
  node [
    id 1394
    label "proporcja"
  ]
  node [
    id 1395
    label "rejestr"
  ]
  node [
    id 1396
    label "subdominanta"
  ]
  node [
    id 1397
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1398
    label "odbicie"
  ]
  node [
    id 1399
    label "przyroda"
  ]
  node [
    id 1400
    label "Ziemia"
  ]
  node [
    id 1401
    label "kosmos"
  ]
  node [
    id 1402
    label "wymienienie"
  ]
  node [
    id 1403
    label "przerachowanie"
  ]
  node [
    id 1404
    label "skontrolowanie"
  ]
  node [
    id 1405
    label "sprawdzanie"
  ]
  node [
    id 1406
    label "zast&#281;powanie"
  ]
  node [
    id 1407
    label "przerachowywanie"
  ]
  node [
    id 1408
    label "sprawdza&#263;"
  ]
  node [
    id 1409
    label "przerachowywa&#263;"
  ]
  node [
    id 1410
    label "sprawdzi&#263;"
  ]
  node [
    id 1411
    label "change"
  ]
  node [
    id 1412
    label "przerachowa&#263;"
  ]
  node [
    id 1413
    label "zmieni&#263;"
  ]
  node [
    id 1414
    label "organizm"
  ]
  node [
    id 1415
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 1416
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 1417
    label "czerpanie"
  ]
  node [
    id 1418
    label "uczenie_si&#281;"
  ]
  node [
    id 1419
    label "pobieranie"
  ]
  node [
    id 1420
    label "acquisition"
  ]
  node [
    id 1421
    label "od&#380;ywianie"
  ]
  node [
    id 1422
    label "kultura"
  ]
  node [
    id 1423
    label "wynoszenie"
  ]
  node [
    id 1424
    label "absorption"
  ]
  node [
    id 1425
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 1426
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 1427
    label "translate"
  ]
  node [
    id 1428
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1429
    label "pobra&#263;"
  ]
  node [
    id 1430
    label "thrill"
  ]
  node [
    id 1431
    label "pobranie"
  ]
  node [
    id 1432
    label "wyniesienie"
  ]
  node [
    id 1433
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 1434
    label "assimilation"
  ]
  node [
    id 1435
    label "emotion"
  ]
  node [
    id 1436
    label "nauczenie_si&#281;"
  ]
  node [
    id 1437
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1438
    label "mechanizm_obronny"
  ]
  node [
    id 1439
    label "convention"
  ]
  node [
    id 1440
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 1441
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1442
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 1443
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 1444
    label "react"
  ]
  node [
    id 1445
    label "reaction"
  ]
  node [
    id 1446
    label "rozmowa"
  ]
  node [
    id 1447
    label "response"
  ]
  node [
    id 1448
    label "respondent"
  ]
  node [
    id 1449
    label "czerpa&#263;"
  ]
  node [
    id 1450
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 1451
    label "pobiera&#263;"
  ]
  node [
    id 1452
    label "rede"
  ]
  node [
    id 1453
    label "fotograf"
  ]
  node [
    id 1454
    label "malarz"
  ]
  node [
    id 1455
    label "artysta"
  ]
  node [
    id 1456
    label "hipnotyzowanie"
  ]
  node [
    id 1457
    label "natural_process"
  ]
  node [
    id 1458
    label "reakcja_chemiczna"
  ]
  node [
    id 1459
    label "pryncypa&#322;"
  ]
  node [
    id 1460
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1461
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1462
    label "kierowa&#263;"
  ]
  node [
    id 1463
    label "alkohol"
  ]
  node [
    id 1464
    label "&#380;ycie"
  ]
  node [
    id 1465
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1466
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1467
    label "sztuka"
  ]
  node [
    id 1468
    label "dekiel"
  ]
  node [
    id 1469
    label "ro&#347;lina"
  ]
  node [
    id 1470
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1471
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1472
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1473
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1474
    label "noosfera"
  ]
  node [
    id 1475
    label "byd&#322;o"
  ]
  node [
    id 1476
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1477
    label "makrocefalia"
  ]
  node [
    id 1478
    label "ucho"
  ]
  node [
    id 1479
    label "g&#243;ra"
  ]
  node [
    id 1480
    label "m&#243;zg"
  ]
  node [
    id 1481
    label "kierownictwo"
  ]
  node [
    id 1482
    label "fryzura"
  ]
  node [
    id 1483
    label "umys&#322;"
  ]
  node [
    id 1484
    label "cz&#322;onek"
  ]
  node [
    id 1485
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1486
    label "czaszka"
  ]
  node [
    id 1487
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1488
    label "allochoria"
  ]
  node [
    id 1489
    label "charakterystyka"
  ]
  node [
    id 1490
    label "p&#322;aszczyzna"
  ]
  node [
    id 1491
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1492
    label "bierka_szachowa"
  ]
  node [
    id 1493
    label "gestaltyzm"
  ]
  node [
    id 1494
    label "styl"
  ]
  node [
    id 1495
    label "Osjan"
  ]
  node [
    id 1496
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1497
    label "character"
  ]
  node [
    id 1498
    label "kto&#347;"
  ]
  node [
    id 1499
    label "rze&#378;ba"
  ]
  node [
    id 1500
    label "stylistyka"
  ]
  node [
    id 1501
    label "figure"
  ]
  node [
    id 1502
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1503
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1504
    label "antycypacja"
  ]
  node [
    id 1505
    label "informacja"
  ]
  node [
    id 1506
    label "Aspazja"
  ]
  node [
    id 1507
    label "popis"
  ]
  node [
    id 1508
    label "wiersz"
  ]
  node [
    id 1509
    label "kompleksja"
  ]
  node [
    id 1510
    label "symetria"
  ]
  node [
    id 1511
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1512
    label "karta"
  ]
  node [
    id 1513
    label "podzbi&#243;r"
  ]
  node [
    id 1514
    label "przedstawienie"
  ]
  node [
    id 1515
    label "perspektywa"
  ]
  node [
    id 1516
    label "piek&#322;o"
  ]
  node [
    id 1517
    label "human_body"
  ]
  node [
    id 1518
    label "ofiarowywanie"
  ]
  node [
    id 1519
    label "sfera_afektywna"
  ]
  node [
    id 1520
    label "nekromancja"
  ]
  node [
    id 1521
    label "Po&#347;wist"
  ]
  node [
    id 1522
    label "podekscytowanie"
  ]
  node [
    id 1523
    label "deformowanie"
  ]
  node [
    id 1524
    label "sumienie"
  ]
  node [
    id 1525
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1526
    label "deformowa&#263;"
  ]
  node [
    id 1527
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1528
    label "psychika"
  ]
  node [
    id 1529
    label "zjawa"
  ]
  node [
    id 1530
    label "zmar&#322;y"
  ]
  node [
    id 1531
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1532
    label "power"
  ]
  node [
    id 1533
    label "entity"
  ]
  node [
    id 1534
    label "ofiarowywa&#263;"
  ]
  node [
    id 1535
    label "oddech"
  ]
  node [
    id 1536
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1537
    label "si&#322;a"
  ]
  node [
    id 1538
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1539
    label "ego"
  ]
  node [
    id 1540
    label "ofiarowanie"
  ]
  node [
    id 1541
    label "fizjonomia"
  ]
  node [
    id 1542
    label "kompleks"
  ]
  node [
    id 1543
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1544
    label "T&#281;sknica"
  ]
  node [
    id 1545
    label "ofiarowa&#263;"
  ]
  node [
    id 1546
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1547
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1548
    label "passion"
  ]
  node [
    id 1549
    label "klasztor"
  ]
  node [
    id 1550
    label "amfilada"
  ]
  node [
    id 1551
    label "front"
  ]
  node [
    id 1552
    label "apartment"
  ]
  node [
    id 1553
    label "pod&#322;oga"
  ]
  node [
    id 1554
    label "sklepienie"
  ]
  node [
    id 1555
    label "sufit"
  ]
  node [
    id 1556
    label "umieszczenie"
  ]
  node [
    id 1557
    label "zakamarek"
  ]
  node [
    id 1558
    label "wirydarz"
  ]
  node [
    id 1559
    label "kustodia"
  ]
  node [
    id 1560
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 1561
    label "zakon"
  ]
  node [
    id 1562
    label "refektarz"
  ]
  node [
    id 1563
    label "kapitularz"
  ]
  node [
    id 1564
    label "oratorium"
  ]
  node [
    id 1565
    label "&#321;agiewniki"
  ]
  node [
    id 1566
    label "pay"
  ]
  node [
    id 1567
    label "zap&#322;aci&#263;"
  ]
  node [
    id 1568
    label "wy&#322;oi&#263;"
  ]
  node [
    id 1569
    label "picture"
  ]
  node [
    id 1570
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1571
    label "zabuli&#263;"
  ]
  node [
    id 1572
    label "wyda&#263;"
  ]
  node [
    id 1573
    label "zaj&#281;cie"
  ]
  node [
    id 1574
    label "yield"
  ]
  node [
    id 1575
    label "zaszkodzenie"
  ]
  node [
    id 1576
    label "duty"
  ]
  node [
    id 1577
    label "przepisanie"
  ]
  node [
    id 1578
    label "nakarmienie"
  ]
  node [
    id 1579
    label "przepisa&#263;"
  ]
  node [
    id 1580
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 1581
    label "zobowi&#261;zanie"
  ]
  node [
    id 1582
    label "activity"
  ]
  node [
    id 1583
    label "bezproblemowy"
  ]
  node [
    id 1584
    label "danie"
  ]
  node [
    id 1585
    label "feed"
  ]
  node [
    id 1586
    label "zaspokojenie"
  ]
  node [
    id 1587
    label "podwini&#281;cie"
  ]
  node [
    id 1588
    label "zap&#322;acenie"
  ]
  node [
    id 1589
    label "przyodzianie"
  ]
  node [
    id 1590
    label "budowla"
  ]
  node [
    id 1591
    label "pokrycie"
  ]
  node [
    id 1592
    label "rozebranie"
  ]
  node [
    id 1593
    label "zak&#322;adka"
  ]
  node [
    id 1594
    label "poubieranie"
  ]
  node [
    id 1595
    label "infliction"
  ]
  node [
    id 1596
    label "pozak&#322;adanie"
  ]
  node [
    id 1597
    label "przebranie"
  ]
  node [
    id 1598
    label "przywdzianie"
  ]
  node [
    id 1599
    label "obleczenie_si&#281;"
  ]
  node [
    id 1600
    label "utworzenie"
  ]
  node [
    id 1601
    label "obleczenie"
  ]
  node [
    id 1602
    label "przygotowywanie"
  ]
  node [
    id 1603
    label "przymierzenie"
  ]
  node [
    id 1604
    label "wyko&#324;czenie"
  ]
  node [
    id 1605
    label "proposition"
  ]
  node [
    id 1606
    label "przewidzenie"
  ]
  node [
    id 1607
    label "stosunek_prawny"
  ]
  node [
    id 1608
    label "oblig"
  ]
  node [
    id 1609
    label "uregulowa&#263;"
  ]
  node [
    id 1610
    label "occupation"
  ]
  node [
    id 1611
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 1612
    label "zapowied&#378;"
  ]
  node [
    id 1613
    label "obowi&#261;zek"
  ]
  node [
    id 1614
    label "statement"
  ]
  node [
    id 1615
    label "zapewnienie"
  ]
  node [
    id 1616
    label "subiekcja"
  ]
  node [
    id 1617
    label "jajko_Kolumba"
  ]
  node [
    id 1618
    label "obstruction"
  ]
  node [
    id 1619
    label "trudno&#347;&#263;"
  ]
  node [
    id 1620
    label "pierepa&#322;ka"
  ]
  node [
    id 1621
    label "ambaras"
  ]
  node [
    id 1622
    label "damage"
  ]
  node [
    id 1623
    label "podniesienie"
  ]
  node [
    id 1624
    label "zniesienie"
  ]
  node [
    id 1625
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 1626
    label "ulepszenie"
  ]
  node [
    id 1627
    label "heave"
  ]
  node [
    id 1628
    label "raise"
  ]
  node [
    id 1629
    label "odbudowanie"
  ]
  node [
    id 1630
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1631
    label "care"
  ]
  node [
    id 1632
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1633
    label "benedykty&#324;ski"
  ]
  node [
    id 1634
    label "career"
  ]
  node [
    id 1635
    label "anektowanie"
  ]
  node [
    id 1636
    label "dostarczenie"
  ]
  node [
    id 1637
    label "u&#380;ycie"
  ]
  node [
    id 1638
    label "klasyfikacja"
  ]
  node [
    id 1639
    label "wzi&#281;cie"
  ]
  node [
    id 1640
    label "wzbudzenie"
  ]
  node [
    id 1641
    label "tynkarski"
  ]
  node [
    id 1642
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 1643
    label "zapanowanie"
  ]
  node [
    id 1644
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1645
    label "zmiana"
  ]
  node [
    id 1646
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 1647
    label "pozajmowanie"
  ]
  node [
    id 1648
    label "ulokowanie_si&#281;"
  ]
  node [
    id 1649
    label "usytuowanie_si&#281;"
  ]
  node [
    id 1650
    label "zabranie"
  ]
  node [
    id 1651
    label "oddawanie"
  ]
  node [
    id 1652
    label "stanowisko"
  ]
  node [
    id 1653
    label "zlecanie"
  ]
  node [
    id 1654
    label "ufanie"
  ]
  node [
    id 1655
    label "wyznawanie"
  ]
  node [
    id 1656
    label "szko&#322;a"
  ]
  node [
    id 1657
    label "przekazanie"
  ]
  node [
    id 1658
    label "skopiowanie"
  ]
  node [
    id 1659
    label "arrangement"
  ]
  node [
    id 1660
    label "przeniesienie"
  ]
  node [
    id 1661
    label "testament"
  ]
  node [
    id 1662
    label "lekarstwo"
  ]
  node [
    id 1663
    label "answer"
  ]
  node [
    id 1664
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1665
    label "transcription"
  ]
  node [
    id 1666
    label "zalecenie"
  ]
  node [
    id 1667
    label "przekaza&#263;"
  ]
  node [
    id 1668
    label "supply"
  ]
  node [
    id 1669
    label "zaleci&#263;"
  ]
  node [
    id 1670
    label "rewrite"
  ]
  node [
    id 1671
    label "zrzec_si&#281;"
  ]
  node [
    id 1672
    label "skopiowa&#263;"
  ]
  node [
    id 1673
    label "przenie&#347;&#263;"
  ]
  node [
    id 1674
    label "poleci&#263;"
  ]
  node [
    id 1675
    label "teach"
  ]
  node [
    id 1676
    label "doradzi&#263;"
  ]
  node [
    id 1677
    label "commend"
  ]
  node [
    id 1678
    label "charge"
  ]
  node [
    id 1679
    label "zada&#263;"
  ]
  node [
    id 1680
    label "zaordynowa&#263;"
  ]
  node [
    id 1681
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1682
    label "Katar"
  ]
  node [
    id 1683
    label "Libia"
  ]
  node [
    id 1684
    label "Gwatemala"
  ]
  node [
    id 1685
    label "Ekwador"
  ]
  node [
    id 1686
    label "Afganistan"
  ]
  node [
    id 1687
    label "Tad&#380;ykistan"
  ]
  node [
    id 1688
    label "Bhutan"
  ]
  node [
    id 1689
    label "Argentyna"
  ]
  node [
    id 1690
    label "D&#380;ibuti"
  ]
  node [
    id 1691
    label "Wenezuela"
  ]
  node [
    id 1692
    label "Gabon"
  ]
  node [
    id 1693
    label "Ukraina"
  ]
  node [
    id 1694
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1695
    label "Rwanda"
  ]
  node [
    id 1696
    label "Liechtenstein"
  ]
  node [
    id 1697
    label "organizacja"
  ]
  node [
    id 1698
    label "Sri_Lanka"
  ]
  node [
    id 1699
    label "Madagaskar"
  ]
  node [
    id 1700
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1701
    label "Kongo"
  ]
  node [
    id 1702
    label "Tonga"
  ]
  node [
    id 1703
    label "Bangladesz"
  ]
  node [
    id 1704
    label "Kanada"
  ]
  node [
    id 1705
    label "Wehrlen"
  ]
  node [
    id 1706
    label "Algieria"
  ]
  node [
    id 1707
    label "Uganda"
  ]
  node [
    id 1708
    label "Surinam"
  ]
  node [
    id 1709
    label "Sahara_Zachodnia"
  ]
  node [
    id 1710
    label "Chile"
  ]
  node [
    id 1711
    label "W&#281;gry"
  ]
  node [
    id 1712
    label "Birma"
  ]
  node [
    id 1713
    label "Kazachstan"
  ]
  node [
    id 1714
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1715
    label "Armenia"
  ]
  node [
    id 1716
    label "Tuwalu"
  ]
  node [
    id 1717
    label "Timor_Wschodni"
  ]
  node [
    id 1718
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1719
    label "Izrael"
  ]
  node [
    id 1720
    label "Estonia"
  ]
  node [
    id 1721
    label "Komory"
  ]
  node [
    id 1722
    label "Kamerun"
  ]
  node [
    id 1723
    label "Haiti"
  ]
  node [
    id 1724
    label "Belize"
  ]
  node [
    id 1725
    label "Sierra_Leone"
  ]
  node [
    id 1726
    label "Luksemburg"
  ]
  node [
    id 1727
    label "USA"
  ]
  node [
    id 1728
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1729
    label "Barbados"
  ]
  node [
    id 1730
    label "San_Marino"
  ]
  node [
    id 1731
    label "Bu&#322;garia"
  ]
  node [
    id 1732
    label "Indonezja"
  ]
  node [
    id 1733
    label "Wietnam"
  ]
  node [
    id 1734
    label "Malawi"
  ]
  node [
    id 1735
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1736
    label "Francja"
  ]
  node [
    id 1737
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1738
    label "partia"
  ]
  node [
    id 1739
    label "Zambia"
  ]
  node [
    id 1740
    label "Angola"
  ]
  node [
    id 1741
    label "Grenada"
  ]
  node [
    id 1742
    label "Nepal"
  ]
  node [
    id 1743
    label "Panama"
  ]
  node [
    id 1744
    label "Rumunia"
  ]
  node [
    id 1745
    label "Czarnog&#243;ra"
  ]
  node [
    id 1746
    label "Malediwy"
  ]
  node [
    id 1747
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1748
    label "S&#322;owacja"
  ]
  node [
    id 1749
    label "Egipt"
  ]
  node [
    id 1750
    label "zwrot"
  ]
  node [
    id 1751
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1752
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1753
    label "Mozambik"
  ]
  node [
    id 1754
    label "Kolumbia"
  ]
  node [
    id 1755
    label "Laos"
  ]
  node [
    id 1756
    label "Burundi"
  ]
  node [
    id 1757
    label "Suazi"
  ]
  node [
    id 1758
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1759
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1760
    label "Czechy"
  ]
  node [
    id 1761
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1762
    label "Wyspy_Marshalla"
  ]
  node [
    id 1763
    label "Dominika"
  ]
  node [
    id 1764
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1765
    label "Syria"
  ]
  node [
    id 1766
    label "Palau"
  ]
  node [
    id 1767
    label "Gwinea_Bissau"
  ]
  node [
    id 1768
    label "Liberia"
  ]
  node [
    id 1769
    label "Jamajka"
  ]
  node [
    id 1770
    label "Zimbabwe"
  ]
  node [
    id 1771
    label "Polska"
  ]
  node [
    id 1772
    label "Dominikana"
  ]
  node [
    id 1773
    label "Senegal"
  ]
  node [
    id 1774
    label "Togo"
  ]
  node [
    id 1775
    label "Gujana"
  ]
  node [
    id 1776
    label "Gruzja"
  ]
  node [
    id 1777
    label "Albania"
  ]
  node [
    id 1778
    label "Zair"
  ]
  node [
    id 1779
    label "Meksyk"
  ]
  node [
    id 1780
    label "Macedonia"
  ]
  node [
    id 1781
    label "Chorwacja"
  ]
  node [
    id 1782
    label "Kambod&#380;a"
  ]
  node [
    id 1783
    label "Monako"
  ]
  node [
    id 1784
    label "Mauritius"
  ]
  node [
    id 1785
    label "Gwinea"
  ]
  node [
    id 1786
    label "Mali"
  ]
  node [
    id 1787
    label "Nigeria"
  ]
  node [
    id 1788
    label "Kostaryka"
  ]
  node [
    id 1789
    label "Hanower"
  ]
  node [
    id 1790
    label "Paragwaj"
  ]
  node [
    id 1791
    label "W&#322;ochy"
  ]
  node [
    id 1792
    label "Seszele"
  ]
  node [
    id 1793
    label "Wyspy_Salomona"
  ]
  node [
    id 1794
    label "Hiszpania"
  ]
  node [
    id 1795
    label "Boliwia"
  ]
  node [
    id 1796
    label "Kirgistan"
  ]
  node [
    id 1797
    label "Irlandia"
  ]
  node [
    id 1798
    label "Czad"
  ]
  node [
    id 1799
    label "Irak"
  ]
  node [
    id 1800
    label "Lesoto"
  ]
  node [
    id 1801
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1802
    label "Malta"
  ]
  node [
    id 1803
    label "Andora"
  ]
  node [
    id 1804
    label "Chiny"
  ]
  node [
    id 1805
    label "Filipiny"
  ]
  node [
    id 1806
    label "Antarktis"
  ]
  node [
    id 1807
    label "Niemcy"
  ]
  node [
    id 1808
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1809
    label "Pakistan"
  ]
  node [
    id 1810
    label "terytorium"
  ]
  node [
    id 1811
    label "Nikaragua"
  ]
  node [
    id 1812
    label "Brazylia"
  ]
  node [
    id 1813
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1814
    label "Maroko"
  ]
  node [
    id 1815
    label "Portugalia"
  ]
  node [
    id 1816
    label "Niger"
  ]
  node [
    id 1817
    label "Kenia"
  ]
  node [
    id 1818
    label "Botswana"
  ]
  node [
    id 1819
    label "Fid&#380;i"
  ]
  node [
    id 1820
    label "Tunezja"
  ]
  node [
    id 1821
    label "Australia"
  ]
  node [
    id 1822
    label "Tajlandia"
  ]
  node [
    id 1823
    label "Burkina_Faso"
  ]
  node [
    id 1824
    label "interior"
  ]
  node [
    id 1825
    label "Tanzania"
  ]
  node [
    id 1826
    label "Benin"
  ]
  node [
    id 1827
    label "Indie"
  ]
  node [
    id 1828
    label "&#321;otwa"
  ]
  node [
    id 1829
    label "Kiribati"
  ]
  node [
    id 1830
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1831
    label "Rodezja"
  ]
  node [
    id 1832
    label "Cypr"
  ]
  node [
    id 1833
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1834
    label "Peru"
  ]
  node [
    id 1835
    label "Austria"
  ]
  node [
    id 1836
    label "Urugwaj"
  ]
  node [
    id 1837
    label "Jordania"
  ]
  node [
    id 1838
    label "Grecja"
  ]
  node [
    id 1839
    label "Azerbejd&#380;an"
  ]
  node [
    id 1840
    label "Turcja"
  ]
  node [
    id 1841
    label "Samoa"
  ]
  node [
    id 1842
    label "Sudan"
  ]
  node [
    id 1843
    label "Oman"
  ]
  node [
    id 1844
    label "ziemia"
  ]
  node [
    id 1845
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1846
    label "Uzbekistan"
  ]
  node [
    id 1847
    label "Portoryko"
  ]
  node [
    id 1848
    label "Honduras"
  ]
  node [
    id 1849
    label "Mongolia"
  ]
  node [
    id 1850
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1851
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1852
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1853
    label "Serbia"
  ]
  node [
    id 1854
    label "Tajwan"
  ]
  node [
    id 1855
    label "Wielka_Brytania"
  ]
  node [
    id 1856
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1857
    label "Liban"
  ]
  node [
    id 1858
    label "Japonia"
  ]
  node [
    id 1859
    label "Ghana"
  ]
  node [
    id 1860
    label "Belgia"
  ]
  node [
    id 1861
    label "Bahrajn"
  ]
  node [
    id 1862
    label "Mikronezja"
  ]
  node [
    id 1863
    label "Etiopia"
  ]
  node [
    id 1864
    label "Kuwejt"
  ]
  node [
    id 1865
    label "grupa"
  ]
  node [
    id 1866
    label "Bahamy"
  ]
  node [
    id 1867
    label "Rosja"
  ]
  node [
    id 1868
    label "Mo&#322;dawia"
  ]
  node [
    id 1869
    label "Litwa"
  ]
  node [
    id 1870
    label "S&#322;owenia"
  ]
  node [
    id 1871
    label "Szwajcaria"
  ]
  node [
    id 1872
    label "Erytrea"
  ]
  node [
    id 1873
    label "Arabia_Saudyjska"
  ]
  node [
    id 1874
    label "Kuba"
  ]
  node [
    id 1875
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1876
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1877
    label "Malezja"
  ]
  node [
    id 1878
    label "Korea"
  ]
  node [
    id 1879
    label "Jemen"
  ]
  node [
    id 1880
    label "Nowa_Zelandia"
  ]
  node [
    id 1881
    label "Namibia"
  ]
  node [
    id 1882
    label "Nauru"
  ]
  node [
    id 1883
    label "holoarktyka"
  ]
  node [
    id 1884
    label "Brunei"
  ]
  node [
    id 1885
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1886
    label "Khitai"
  ]
  node [
    id 1887
    label "Mauretania"
  ]
  node [
    id 1888
    label "Iran"
  ]
  node [
    id 1889
    label "Gambia"
  ]
  node [
    id 1890
    label "Somalia"
  ]
  node [
    id 1891
    label "Holandia"
  ]
  node [
    id 1892
    label "Turkmenistan"
  ]
  node [
    id 1893
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1894
    label "Salwador"
  ]
  node [
    id 1895
    label "pair"
  ]
  node [
    id 1896
    label "odparowywanie"
  ]
  node [
    id 1897
    label "gaz_cieplarniany"
  ]
  node [
    id 1898
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1899
    label "poker"
  ]
  node [
    id 1900
    label "moneta"
  ]
  node [
    id 1901
    label "parowanie"
  ]
  node [
    id 1902
    label "damp"
  ]
  node [
    id 1903
    label "nale&#380;e&#263;"
  ]
  node [
    id 1904
    label "odparowanie"
  ]
  node [
    id 1905
    label "odparowa&#263;"
  ]
  node [
    id 1906
    label "dodatek"
  ]
  node [
    id 1907
    label "jednostka_monetarna"
  ]
  node [
    id 1908
    label "smoke"
  ]
  node [
    id 1909
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1910
    label "odparowywa&#263;"
  ]
  node [
    id 1911
    label "gaz"
  ]
  node [
    id 1912
    label "wyparowanie"
  ]
  node [
    id 1913
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1914
    label "Jukon"
  ]
  node [
    id 1915
    label "podmiot"
  ]
  node [
    id 1916
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1917
    label "TOPR"
  ]
  node [
    id 1918
    label "endecki"
  ]
  node [
    id 1919
    label "od&#322;am"
  ]
  node [
    id 1920
    label "przedstawicielstwo"
  ]
  node [
    id 1921
    label "Cepelia"
  ]
  node [
    id 1922
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1923
    label "ZBoWiD"
  ]
  node [
    id 1924
    label "organization"
  ]
  node [
    id 1925
    label "centrala"
  ]
  node [
    id 1926
    label "GOPR"
  ]
  node [
    id 1927
    label "ZOMO"
  ]
  node [
    id 1928
    label "ZMP"
  ]
  node [
    id 1929
    label "komitet_koordynacyjny"
  ]
  node [
    id 1930
    label "przybud&#243;wka"
  ]
  node [
    id 1931
    label "boj&#243;wka"
  ]
  node [
    id 1932
    label "turn"
  ]
  node [
    id 1933
    label "turning"
  ]
  node [
    id 1934
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1935
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1936
    label "skr&#281;t"
  ]
  node [
    id 1937
    label "obr&#243;t"
  ]
  node [
    id 1938
    label "fraza_czasownikowa"
  ]
  node [
    id 1939
    label "jednostka_leksykalna"
  ]
  node [
    id 1940
    label "odm&#322;adzanie"
  ]
  node [
    id 1941
    label "liga"
  ]
  node [
    id 1942
    label "asymilowanie"
  ]
  node [
    id 1943
    label "gromada"
  ]
  node [
    id 1944
    label "asymilowa&#263;"
  ]
  node [
    id 1945
    label "Entuzjastki"
  ]
  node [
    id 1946
    label "kompozycja"
  ]
  node [
    id 1947
    label "Terranie"
  ]
  node [
    id 1948
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1949
    label "category"
  ]
  node [
    id 1950
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1951
    label "stage_set"
  ]
  node [
    id 1952
    label "type"
  ]
  node [
    id 1953
    label "specgrupa"
  ]
  node [
    id 1954
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1955
    label "&#346;wietliki"
  ]
  node [
    id 1956
    label "odm&#322;odzenie"
  ]
  node [
    id 1957
    label "Eurogrupa"
  ]
  node [
    id 1958
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1959
    label "formacja_geologiczna"
  ]
  node [
    id 1960
    label "harcerze_starsi"
  ]
  node [
    id 1961
    label "Bund"
  ]
  node [
    id 1962
    label "PPR"
  ]
  node [
    id 1963
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1964
    label "wybranek"
  ]
  node [
    id 1965
    label "Jakobici"
  ]
  node [
    id 1966
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1967
    label "SLD"
  ]
  node [
    id 1968
    label "Razem"
  ]
  node [
    id 1969
    label "PiS"
  ]
  node [
    id 1970
    label "package"
  ]
  node [
    id 1971
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1972
    label "Kuomintang"
  ]
  node [
    id 1973
    label "ZSL"
  ]
  node [
    id 1974
    label "AWS"
  ]
  node [
    id 1975
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1976
    label "game"
  ]
  node [
    id 1977
    label "blok"
  ]
  node [
    id 1978
    label "materia&#322;"
  ]
  node [
    id 1979
    label "PO"
  ]
  node [
    id 1980
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1981
    label "niedoczas"
  ]
  node [
    id 1982
    label "Federali&#347;ci"
  ]
  node [
    id 1983
    label "PSL"
  ]
  node [
    id 1984
    label "Wigowie"
  ]
  node [
    id 1985
    label "ZChN"
  ]
  node [
    id 1986
    label "egzekutywa"
  ]
  node [
    id 1987
    label "aktyw"
  ]
  node [
    id 1988
    label "wybranka"
  ]
  node [
    id 1989
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1990
    label "unit"
  ]
  node [
    id 1991
    label "biom"
  ]
  node [
    id 1992
    label "szata_ro&#347;linna"
  ]
  node [
    id 1993
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1994
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1995
    label "zielono&#347;&#263;"
  ]
  node [
    id 1996
    label "plant"
  ]
  node [
    id 1997
    label "geosystem"
  ]
  node [
    id 1998
    label "teren"
  ]
  node [
    id 1999
    label "Kaszmir"
  ]
  node [
    id 2000
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 2001
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 2002
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 2003
    label "Pend&#380;ab"
  ]
  node [
    id 2004
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 2005
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 2006
    label "funt_liba&#324;ski"
  ]
  node [
    id 2007
    label "strefa_euro"
  ]
  node [
    id 2008
    label "Pozna&#324;"
  ]
  node [
    id 2009
    label "lira_malta&#324;ska"
  ]
  node [
    id 2010
    label "Gozo"
  ]
  node [
    id 2011
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 2012
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 2013
    label "Afryka_Zachodnia"
  ]
  node [
    id 2014
    label "Afryka_Wschodnia"
  ]
  node [
    id 2015
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 2016
    label "dolar_namibijski"
  ]
  node [
    id 2017
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 2018
    label "milrejs"
  ]
  node [
    id 2019
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 2020
    label "NATO"
  ]
  node [
    id 2021
    label "escudo_portugalskie"
  ]
  node [
    id 2022
    label "dolar_bahamski"
  ]
  node [
    id 2023
    label "Wielka_Bahama"
  ]
  node [
    id 2024
    label "Karaiby"
  ]
  node [
    id 2025
    label "dolar_liberyjski"
  ]
  node [
    id 2026
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 2027
    label "riel"
  ]
  node [
    id 2028
    label "Karelia"
  ]
  node [
    id 2029
    label "Mari_El"
  ]
  node [
    id 2030
    label "Inguszetia"
  ]
  node [
    id 2031
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 2032
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 2033
    label "Udmurcja"
  ]
  node [
    id 2034
    label "Newa"
  ]
  node [
    id 2035
    label "&#321;adoga"
  ]
  node [
    id 2036
    label "Czeczenia"
  ]
  node [
    id 2037
    label "Anadyr"
  ]
  node [
    id 2038
    label "Syberia"
  ]
  node [
    id 2039
    label "Tatarstan"
  ]
  node [
    id 2040
    label "Wszechrosja"
  ]
  node [
    id 2041
    label "Azja"
  ]
  node [
    id 2042
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 2043
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 2044
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 2045
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 2046
    label "Europa_Wschodnia"
  ]
  node [
    id 2047
    label "Witim"
  ]
  node [
    id 2048
    label "Kamczatka"
  ]
  node [
    id 2049
    label "Jama&#322;"
  ]
  node [
    id 2050
    label "Dagestan"
  ]
  node [
    id 2051
    label "Baszkiria"
  ]
  node [
    id 2052
    label "Tuwa"
  ]
  node [
    id 2053
    label "car"
  ]
  node [
    id 2054
    label "Komi"
  ]
  node [
    id 2055
    label "Czuwaszja"
  ]
  node [
    id 2056
    label "Chakasja"
  ]
  node [
    id 2057
    label "Perm"
  ]
  node [
    id 2058
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 2059
    label "Ajon"
  ]
  node [
    id 2060
    label "Adygeja"
  ]
  node [
    id 2061
    label "Dniepr"
  ]
  node [
    id 2062
    label "rubel_rosyjski"
  ]
  node [
    id 2063
    label "Don"
  ]
  node [
    id 2064
    label "Mordowia"
  ]
  node [
    id 2065
    label "s&#322;owianofilstwo"
  ]
  node [
    id 2066
    label "lew"
  ]
  node [
    id 2067
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 2068
    label "Dobrud&#380;a"
  ]
  node [
    id 2069
    label "Unia_Europejska"
  ]
  node [
    id 2070
    label "lira_izraelska"
  ]
  node [
    id 2071
    label "szekel"
  ]
  node [
    id 2072
    label "Galilea"
  ]
  node [
    id 2073
    label "Judea"
  ]
  node [
    id 2074
    label "Luksemburgia"
  ]
  node [
    id 2075
    label "frank_belgijski"
  ]
  node [
    id 2076
    label "Limburgia"
  ]
  node [
    id 2077
    label "Walonia"
  ]
  node [
    id 2078
    label "Brabancja"
  ]
  node [
    id 2079
    label "Flandria"
  ]
  node [
    id 2080
    label "Niderlandy"
  ]
  node [
    id 2081
    label "dinar_iracki"
  ]
  node [
    id 2082
    label "Maghreb"
  ]
  node [
    id 2083
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 2084
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 2085
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 2086
    label "szyling_ugandyjski"
  ]
  node [
    id 2087
    label "kafar"
  ]
  node [
    id 2088
    label "dolar_jamajski"
  ]
  node [
    id 2089
    label "ringgit"
  ]
  node [
    id 2090
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 2091
    label "Borneo"
  ]
  node [
    id 2092
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 2093
    label "dolar_surinamski"
  ]
  node [
    id 2094
    label "funt_suda&#324;ski"
  ]
  node [
    id 2095
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 2096
    label "dolar_guja&#324;ski"
  ]
  node [
    id 2097
    label "Manica"
  ]
  node [
    id 2098
    label "escudo_mozambickie"
  ]
  node [
    id 2099
    label "Cabo_Delgado"
  ]
  node [
    id 2100
    label "Inhambane"
  ]
  node [
    id 2101
    label "Maputo"
  ]
  node [
    id 2102
    label "Gaza"
  ]
  node [
    id 2103
    label "Niasa"
  ]
  node [
    id 2104
    label "Nampula"
  ]
  node [
    id 2105
    label "metical"
  ]
  node [
    id 2106
    label "Sahara"
  ]
  node [
    id 2107
    label "inti"
  ]
  node [
    id 2108
    label "sol"
  ]
  node [
    id 2109
    label "kip"
  ]
  node [
    id 2110
    label "Pireneje"
  ]
  node [
    id 2111
    label "euro"
  ]
  node [
    id 2112
    label "kwacha_zambijska"
  ]
  node [
    id 2113
    label "tugrik"
  ]
  node [
    id 2114
    label "Azja_Wschodnia"
  ]
  node [
    id 2115
    label "Buriaci"
  ]
  node [
    id 2116
    label "ajmak"
  ]
  node [
    id 2117
    label "balboa"
  ]
  node [
    id 2118
    label "Ameryka_Centralna"
  ]
  node [
    id 2119
    label "dolar"
  ]
  node [
    id 2120
    label "gulden"
  ]
  node [
    id 2121
    label "Zelandia"
  ]
  node [
    id 2122
    label "manat_turkme&#324;ski"
  ]
  node [
    id 2123
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 2124
    label "Polinezja"
  ]
  node [
    id 2125
    label "dolar_Tuvalu"
  ]
  node [
    id 2126
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 2127
    label "zair"
  ]
  node [
    id 2128
    label "Katanga"
  ]
  node [
    id 2129
    label "Europa_Zachodnia"
  ]
  node [
    id 2130
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 2131
    label "frank_szwajcarski"
  ]
  node [
    id 2132
    label "dolar_Belize"
  ]
  node [
    id 2133
    label "colon"
  ]
  node [
    id 2134
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 2135
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 2136
    label "Dyja"
  ]
  node [
    id 2137
    label "korona_czeska"
  ]
  node [
    id 2138
    label "Izera"
  ]
  node [
    id 2139
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 2140
    label "Lasko"
  ]
  node [
    id 2141
    label "ugija"
  ]
  node [
    id 2142
    label "szyling_kenijski"
  ]
  node [
    id 2143
    label "Nachiczewan"
  ]
  node [
    id 2144
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 2145
    label "manat_azerski"
  ]
  node [
    id 2146
    label "Karabach"
  ]
  node [
    id 2147
    label "Bengal"
  ]
  node [
    id 2148
    label "taka"
  ]
  node [
    id 2149
    label "Ocean_Spokojny"
  ]
  node [
    id 2150
    label "dolar_Kiribati"
  ]
  node [
    id 2151
    label "peso_filipi&#324;skie"
  ]
  node [
    id 2152
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 2153
    label "Cebu"
  ]
  node [
    id 2154
    label "Atlantyk"
  ]
  node [
    id 2155
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 2156
    label "Ulster"
  ]
  node [
    id 2157
    label "funt_irlandzki"
  ]
  node [
    id 2158
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 2159
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 2160
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 2161
    label "cedi"
  ]
  node [
    id 2162
    label "ariary"
  ]
  node [
    id 2163
    label "Ocean_Indyjski"
  ]
  node [
    id 2164
    label "frank_malgaski"
  ]
  node [
    id 2165
    label "Estremadura"
  ]
  node [
    id 2166
    label "Andaluzja"
  ]
  node [
    id 2167
    label "Kastylia"
  ]
  node [
    id 2168
    label "Galicja"
  ]
  node [
    id 2169
    label "Aragonia"
  ]
  node [
    id 2170
    label "hacjender"
  ]
  node [
    id 2171
    label "Asturia"
  ]
  node [
    id 2172
    label "Baskonia"
  ]
  node [
    id 2173
    label "Majorka"
  ]
  node [
    id 2174
    label "Walencja"
  ]
  node [
    id 2175
    label "peseta"
  ]
  node [
    id 2176
    label "Katalonia"
  ]
  node [
    id 2177
    label "peso_chilijskie"
  ]
  node [
    id 2178
    label "Indie_Zachodnie"
  ]
  node [
    id 2179
    label "Sikkim"
  ]
  node [
    id 2180
    label "Asam"
  ]
  node [
    id 2181
    label "rupia_indyjska"
  ]
  node [
    id 2182
    label "Indie_Portugalskie"
  ]
  node [
    id 2183
    label "Indie_Wschodnie"
  ]
  node [
    id 2184
    label "Kerala"
  ]
  node [
    id 2185
    label "Bollywood"
  ]
  node [
    id 2186
    label "jen"
  ]
  node [
    id 2187
    label "jinja"
  ]
  node [
    id 2188
    label "Okinawa"
  ]
  node [
    id 2189
    label "Japonica"
  ]
  node [
    id 2190
    label "Rugia"
  ]
  node [
    id 2191
    label "Saksonia"
  ]
  node [
    id 2192
    label "Dolna_Saksonia"
  ]
  node [
    id 2193
    label "Anglosas"
  ]
  node [
    id 2194
    label "Hesja"
  ]
  node [
    id 2195
    label "Szlezwik"
  ]
  node [
    id 2196
    label "Wirtembergia"
  ]
  node [
    id 2197
    label "Po&#322;abie"
  ]
  node [
    id 2198
    label "Germania"
  ]
  node [
    id 2199
    label "Frankonia"
  ]
  node [
    id 2200
    label "Badenia"
  ]
  node [
    id 2201
    label "Holsztyn"
  ]
  node [
    id 2202
    label "Bawaria"
  ]
  node [
    id 2203
    label "marka"
  ]
  node [
    id 2204
    label "Szwabia"
  ]
  node [
    id 2205
    label "Brandenburgia"
  ]
  node [
    id 2206
    label "Niemcy_Zachodnie"
  ]
  node [
    id 2207
    label "Nadrenia"
  ]
  node [
    id 2208
    label "Westfalia"
  ]
  node [
    id 2209
    label "Turyngia"
  ]
  node [
    id 2210
    label "Helgoland"
  ]
  node [
    id 2211
    label "Karlsbad"
  ]
  node [
    id 2212
    label "Niemcy_Wschodnie"
  ]
  node [
    id 2213
    label "Piemont"
  ]
  node [
    id 2214
    label "Lombardia"
  ]
  node [
    id 2215
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 2216
    label "Kalabria"
  ]
  node [
    id 2217
    label "Sardynia"
  ]
  node [
    id 2218
    label "Italia"
  ]
  node [
    id 2219
    label "Ok&#281;cie"
  ]
  node [
    id 2220
    label "Kampania"
  ]
  node [
    id 2221
    label "Karyntia"
  ]
  node [
    id 2222
    label "Umbria"
  ]
  node [
    id 2223
    label "Romania"
  ]
  node [
    id 2224
    label "Warszawa"
  ]
  node [
    id 2225
    label "lir"
  ]
  node [
    id 2226
    label "Toskania"
  ]
  node [
    id 2227
    label "Apulia"
  ]
  node [
    id 2228
    label "Liguria"
  ]
  node [
    id 2229
    label "Sycylia"
  ]
  node [
    id 2230
    label "Dacja"
  ]
  node [
    id 2231
    label "lej_rumu&#324;ski"
  ]
  node [
    id 2232
    label "Siedmiogr&#243;d"
  ]
  node [
    id 2233
    label "Ba&#322;kany"
  ]
  node [
    id 2234
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 2235
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 2236
    label "funt_syryjski"
  ]
  node [
    id 2237
    label "alawizm"
  ]
  node [
    id 2238
    label "frank_rwandyjski"
  ]
  node [
    id 2239
    label "dinar_Bahrajnu"
  ]
  node [
    id 2240
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 2241
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 2242
    label "frank_luksemburski"
  ]
  node [
    id 2243
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 2244
    label "peso_kuba&#324;skie"
  ]
  node [
    id 2245
    label "frank_monakijski"
  ]
  node [
    id 2246
    label "dinar_algierski"
  ]
  node [
    id 2247
    label "Kabylia"
  ]
  node [
    id 2248
    label "Oceania"
  ]
  node [
    id 2249
    label "Wojwodina"
  ]
  node [
    id 2250
    label "Sand&#380;ak"
  ]
  node [
    id 2251
    label "dinar_serbski"
  ]
  node [
    id 2252
    label "boliwar"
  ]
  node [
    id 2253
    label "Orinoko"
  ]
  node [
    id 2254
    label "tenge"
  ]
  node [
    id 2255
    label "lek"
  ]
  node [
    id 2256
    label "frank_alba&#324;ski"
  ]
  node [
    id 2257
    label "dolar_Barbadosu"
  ]
  node [
    id 2258
    label "Antyle"
  ]
  node [
    id 2259
    label "kyat"
  ]
  node [
    id 2260
    label "c&#243;rdoba"
  ]
  node [
    id 2261
    label "Paros"
  ]
  node [
    id 2262
    label "Epir"
  ]
  node [
    id 2263
    label "panhellenizm"
  ]
  node [
    id 2264
    label "Eubea"
  ]
  node [
    id 2265
    label "Rodos"
  ]
  node [
    id 2266
    label "Achaja"
  ]
  node [
    id 2267
    label "Termopile"
  ]
  node [
    id 2268
    label "Attyka"
  ]
  node [
    id 2269
    label "Hellada"
  ]
  node [
    id 2270
    label "Etolia"
  ]
  node [
    id 2271
    label "palestra"
  ]
  node [
    id 2272
    label "Kreta"
  ]
  node [
    id 2273
    label "drachma"
  ]
  node [
    id 2274
    label "Olimp"
  ]
  node [
    id 2275
    label "Tesalia"
  ]
  node [
    id 2276
    label "Peloponez"
  ]
  node [
    id 2277
    label "Eolia"
  ]
  node [
    id 2278
    label "Beocja"
  ]
  node [
    id 2279
    label "Parnas"
  ]
  node [
    id 2280
    label "Lesbos"
  ]
  node [
    id 2281
    label "Mariany"
  ]
  node [
    id 2282
    label "Salzburg"
  ]
  node [
    id 2283
    label "Rakuzy"
  ]
  node [
    id 2284
    label "Tyrol"
  ]
  node [
    id 2285
    label "konsulent"
  ]
  node [
    id 2286
    label "szyling_austryjacki"
  ]
  node [
    id 2287
    label "Amhara"
  ]
  node [
    id 2288
    label "birr"
  ]
  node [
    id 2289
    label "Syjon"
  ]
  node [
    id 2290
    label "negus"
  ]
  node [
    id 2291
    label "Jawa"
  ]
  node [
    id 2292
    label "Sumatra"
  ]
  node [
    id 2293
    label "rupia_indonezyjska"
  ]
  node [
    id 2294
    label "Nowa_Gwinea"
  ]
  node [
    id 2295
    label "Moluki"
  ]
  node [
    id 2296
    label "boliviano"
  ]
  node [
    id 2297
    label "Lotaryngia"
  ]
  node [
    id 2298
    label "Bordeaux"
  ]
  node [
    id 2299
    label "Pikardia"
  ]
  node [
    id 2300
    label "Masyw_Centralny"
  ]
  node [
    id 2301
    label "Akwitania"
  ]
  node [
    id 2302
    label "Alzacja"
  ]
  node [
    id 2303
    label "Sekwana"
  ]
  node [
    id 2304
    label "Langwedocja"
  ]
  node [
    id 2305
    label "Armagnac"
  ]
  node [
    id 2306
    label "Martynika"
  ]
  node [
    id 2307
    label "Bretania"
  ]
  node [
    id 2308
    label "Sabaudia"
  ]
  node [
    id 2309
    label "Korsyka"
  ]
  node [
    id 2310
    label "Normandia"
  ]
  node [
    id 2311
    label "Gaskonia"
  ]
  node [
    id 2312
    label "Burgundia"
  ]
  node [
    id 2313
    label "frank_francuski"
  ]
  node [
    id 2314
    label "Wandea"
  ]
  node [
    id 2315
    label "Prowansja"
  ]
  node [
    id 2316
    label "Gwadelupa"
  ]
  node [
    id 2317
    label "somoni"
  ]
  node [
    id 2318
    label "Melanezja"
  ]
  node [
    id 2319
    label "dolar_Fid&#380;i"
  ]
  node [
    id 2320
    label "funt_cypryjski"
  ]
  node [
    id 2321
    label "Afrodyzje"
  ]
  node [
    id 2322
    label "peso_dominika&#324;skie"
  ]
  node [
    id 2323
    label "Fryburg"
  ]
  node [
    id 2324
    label "Bazylea"
  ]
  node [
    id 2325
    label "Alpy"
  ]
  node [
    id 2326
    label "Helwecja"
  ]
  node [
    id 2327
    label "Berno"
  ]
  node [
    id 2328
    label "Karaka&#322;pacja"
  ]
  node [
    id 2329
    label "Kurlandia"
  ]
  node [
    id 2330
    label "Windawa"
  ]
  node [
    id 2331
    label "&#322;at"
  ]
  node [
    id 2332
    label "Liwonia"
  ]
  node [
    id 2333
    label "rubel_&#322;otewski"
  ]
  node [
    id 2334
    label "Inflanty"
  ]
  node [
    id 2335
    label "&#379;mud&#378;"
  ]
  node [
    id 2336
    label "lit"
  ]
  node [
    id 2337
    label "frank_tunezyjski"
  ]
  node [
    id 2338
    label "dinar_tunezyjski"
  ]
  node [
    id 2339
    label "lempira"
  ]
  node [
    id 2340
    label "korona_w&#281;gierska"
  ]
  node [
    id 2341
    label "forint"
  ]
  node [
    id 2342
    label "Lipt&#243;w"
  ]
  node [
    id 2343
    label "dong"
  ]
  node [
    id 2344
    label "Annam"
  ]
  node [
    id 2345
    label "Tonkin"
  ]
  node [
    id 2346
    label "lud"
  ]
  node [
    id 2347
    label "frank_kongijski"
  ]
  node [
    id 2348
    label "szyling_somalijski"
  ]
  node [
    id 2349
    label "cruzado"
  ]
  node [
    id 2350
    label "real"
  ]
  node [
    id 2351
    label "Podole"
  ]
  node [
    id 2352
    label "Ukraina_Zachodnia"
  ]
  node [
    id 2353
    label "Wsch&#243;d"
  ]
  node [
    id 2354
    label "Zakarpacie"
  ]
  node [
    id 2355
    label "Naddnieprze"
  ]
  node [
    id 2356
    label "Ma&#322;orosja"
  ]
  node [
    id 2357
    label "Wo&#322;y&#324;"
  ]
  node [
    id 2358
    label "Nadbu&#380;e"
  ]
  node [
    id 2359
    label "hrywna"
  ]
  node [
    id 2360
    label "Zaporo&#380;e"
  ]
  node [
    id 2361
    label "Krym"
  ]
  node [
    id 2362
    label "Dniestr"
  ]
  node [
    id 2363
    label "Przykarpacie"
  ]
  node [
    id 2364
    label "Kozaczyzna"
  ]
  node [
    id 2365
    label "karbowaniec"
  ]
  node [
    id 2366
    label "Tasmania"
  ]
  node [
    id 2367
    label "Nowy_&#346;wiat"
  ]
  node [
    id 2368
    label "dolar_australijski"
  ]
  node [
    id 2369
    label "gourde"
  ]
  node [
    id 2370
    label "escudo_angolskie"
  ]
  node [
    id 2371
    label "kwanza"
  ]
  node [
    id 2372
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 2373
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 2374
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 2375
    label "Ad&#380;aria"
  ]
  node [
    id 2376
    label "lari"
  ]
  node [
    id 2377
    label "naira"
  ]
  node [
    id 2378
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 2379
    label "P&#243;&#322;noc"
  ]
  node [
    id 2380
    label "Po&#322;udnie"
  ]
  node [
    id 2381
    label "zielona_karta"
  ]
  node [
    id 2382
    label "stan_wolny"
  ]
  node [
    id 2383
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 2384
    label "Wuj_Sam"
  ]
  node [
    id 2385
    label "Zach&#243;d"
  ]
  node [
    id 2386
    label "Hudson"
  ]
  node [
    id 2387
    label "som"
  ]
  node [
    id 2388
    label "peso_urugwajskie"
  ]
  node [
    id 2389
    label "denar_macedo&#324;ski"
  ]
  node [
    id 2390
    label "dolar_Brunei"
  ]
  node [
    id 2391
    label "rial_ira&#324;ski"
  ]
  node [
    id 2392
    label "mu&#322;&#322;a"
  ]
  node [
    id 2393
    label "Persja"
  ]
  node [
    id 2394
    label "d&#380;amahirijja"
  ]
  node [
    id 2395
    label "dinar_libijski"
  ]
  node [
    id 2396
    label "nakfa"
  ]
  node [
    id 2397
    label "rial_katarski"
  ]
  node [
    id 2398
    label "quetzal"
  ]
  node [
    id 2399
    label "won"
  ]
  node [
    id 2400
    label "rial_jeme&#324;ski"
  ]
  node [
    id 2401
    label "peso_argenty&#324;skie"
  ]
  node [
    id 2402
    label "guarani"
  ]
  node [
    id 2403
    label "perper"
  ]
  node [
    id 2404
    label "dinar_kuwejcki"
  ]
  node [
    id 2405
    label "dalasi"
  ]
  node [
    id 2406
    label "dolar_Zimbabwe"
  ]
  node [
    id 2407
    label "Szantung"
  ]
  node [
    id 2408
    label "Chiny_Zachodnie"
  ]
  node [
    id 2409
    label "Kuantung"
  ]
  node [
    id 2410
    label "D&#380;ungaria"
  ]
  node [
    id 2411
    label "yuan"
  ]
  node [
    id 2412
    label "Hongkong"
  ]
  node [
    id 2413
    label "Chiny_Wschodnie"
  ]
  node [
    id 2414
    label "Guangdong"
  ]
  node [
    id 2415
    label "Junnan"
  ]
  node [
    id 2416
    label "Mand&#380;uria"
  ]
  node [
    id 2417
    label "Syczuan"
  ]
  node [
    id 2418
    label "Mazowsze"
  ]
  node [
    id 2419
    label "Pa&#322;uki"
  ]
  node [
    id 2420
    label "Pomorze_Zachodnie"
  ]
  node [
    id 2421
    label "Powi&#347;le"
  ]
  node [
    id 2422
    label "Wolin"
  ]
  node [
    id 2423
    label "z&#322;oty"
  ]
  node [
    id 2424
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 2425
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 2426
    label "So&#322;a"
  ]
  node [
    id 2427
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 2428
    label "Opolskie"
  ]
  node [
    id 2429
    label "Suwalszczyzna"
  ]
  node [
    id 2430
    label "Krajna"
  ]
  node [
    id 2431
    label "barwy_polskie"
  ]
  node [
    id 2432
    label "Podlasie"
  ]
  node [
    id 2433
    label "Ma&#322;opolska"
  ]
  node [
    id 2434
    label "Warmia"
  ]
  node [
    id 2435
    label "Mazury"
  ]
  node [
    id 2436
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 2437
    label "Lubelszczyzna"
  ]
  node [
    id 2438
    label "Kaczawa"
  ]
  node [
    id 2439
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 2440
    label "Kielecczyzna"
  ]
  node [
    id 2441
    label "Lubuskie"
  ]
  node [
    id 2442
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 2443
    label "&#321;&#243;dzkie"
  ]
  node [
    id 2444
    label "Kujawy"
  ]
  node [
    id 2445
    label "Podkarpacie"
  ]
  node [
    id 2446
    label "Wielkopolska"
  ]
  node [
    id 2447
    label "Wis&#322;a"
  ]
  node [
    id 2448
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 2449
    label "Bory_Tucholskie"
  ]
  node [
    id 2450
    label "Ujgur"
  ]
  node [
    id 2451
    label "Azja_Mniejsza"
  ]
  node [
    id 2452
    label "lira_turecka"
  ]
  node [
    id 2453
    label "kuna"
  ]
  node [
    id 2454
    label "dram"
  ]
  node [
    id 2455
    label "tala"
  ]
  node [
    id 2456
    label "korona_s&#322;owacka"
  ]
  node [
    id 2457
    label "Turiec"
  ]
  node [
    id 2458
    label "Himalaje"
  ]
  node [
    id 2459
    label "rupia_nepalska"
  ]
  node [
    id 2460
    label "frank_gwinejski"
  ]
  node [
    id 2461
    label "korona_esto&#324;ska"
  ]
  node [
    id 2462
    label "Skandynawia"
  ]
  node [
    id 2463
    label "marka_esto&#324;ska"
  ]
  node [
    id 2464
    label "Quebec"
  ]
  node [
    id 2465
    label "dolar_kanadyjski"
  ]
  node [
    id 2466
    label "Nowa_Fundlandia"
  ]
  node [
    id 2467
    label "Zanzibar"
  ]
  node [
    id 2468
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 2469
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 2470
    label "&#346;wite&#378;"
  ]
  node [
    id 2471
    label "peso_kolumbijskie"
  ]
  node [
    id 2472
    label "Synaj"
  ]
  node [
    id 2473
    label "paraszyt"
  ]
  node [
    id 2474
    label "funt_egipski"
  ]
  node [
    id 2475
    label "szach"
  ]
  node [
    id 2476
    label "Baktria"
  ]
  node [
    id 2477
    label "afgani"
  ]
  node [
    id 2478
    label "baht"
  ]
  node [
    id 2479
    label "tolar"
  ]
  node [
    id 2480
    label "lej_mo&#322;dawski"
  ]
  node [
    id 2481
    label "Naddniestrze"
  ]
  node [
    id 2482
    label "Gagauzja"
  ]
  node [
    id 2483
    label "Anglia"
  ]
  node [
    id 2484
    label "Amazonia"
  ]
  node [
    id 2485
    label "plantowa&#263;"
  ]
  node [
    id 2486
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 2487
    label "zapadnia"
  ]
  node [
    id 2488
    label "Zamojszczyzna"
  ]
  node [
    id 2489
    label "skorupa_ziemska"
  ]
  node [
    id 2490
    label "Turkiestan"
  ]
  node [
    id 2491
    label "Noworosja"
  ]
  node [
    id 2492
    label "Mezoameryka"
  ]
  node [
    id 2493
    label "glinowanie"
  ]
  node [
    id 2494
    label "Kurdystan"
  ]
  node [
    id 2495
    label "martwica"
  ]
  node [
    id 2496
    label "Szkocja"
  ]
  node [
    id 2497
    label "litosfera"
  ]
  node [
    id 2498
    label "penetrator"
  ]
  node [
    id 2499
    label "glinowa&#263;"
  ]
  node [
    id 2500
    label "Zabajkale"
  ]
  node [
    id 2501
    label "domain"
  ]
  node [
    id 2502
    label "Bojkowszczyzna"
  ]
  node [
    id 2503
    label "podglebie"
  ]
  node [
    id 2504
    label "kompleks_sorpcyjny"
  ]
  node [
    id 2505
    label "Pamir"
  ]
  node [
    id 2506
    label "Indochiny"
  ]
  node [
    id 2507
    label "Kurpie"
  ]
  node [
    id 2508
    label "S&#261;decczyzna"
  ]
  node [
    id 2509
    label "kort"
  ]
  node [
    id 2510
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 2511
    label "Huculszczyzna"
  ]
  node [
    id 2512
    label "pojazd"
  ]
  node [
    id 2513
    label "powierzchnia"
  ]
  node [
    id 2514
    label "Podhale"
  ]
  node [
    id 2515
    label "Hercegowina"
  ]
  node [
    id 2516
    label "Walia"
  ]
  node [
    id 2517
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 2518
    label "ryzosfera"
  ]
  node [
    id 2519
    label "Kaukaz"
  ]
  node [
    id 2520
    label "Biskupizna"
  ]
  node [
    id 2521
    label "Bo&#347;nia"
  ]
  node [
    id 2522
    label "dotleni&#263;"
  ]
  node [
    id 2523
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 2524
    label "Podbeskidzie"
  ]
  node [
    id 2525
    label "&#321;emkowszczyzna"
  ]
  node [
    id 2526
    label "Opolszczyzna"
  ]
  node [
    id 2527
    label "Kaszuby"
  ]
  node [
    id 2528
    label "Ko&#322;yma"
  ]
  node [
    id 2529
    label "glej"
  ]
  node [
    id 2530
    label "posadzka"
  ]
  node [
    id 2531
    label "Polesie"
  ]
  node [
    id 2532
    label "Palestyna"
  ]
  node [
    id 2533
    label "Lauda"
  ]
  node [
    id 2534
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 2535
    label "Laponia"
  ]
  node [
    id 2536
    label "Yorkshire"
  ]
  node [
    id 2537
    label "Zag&#243;rze"
  ]
  node [
    id 2538
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 2539
    label "&#379;ywiecczyzna"
  ]
  node [
    id 2540
    label "Oksytania"
  ]
  node [
    id 2541
    label "Kociewie"
  ]
  node [
    id 2542
    label "spolny"
  ]
  node [
    id 2543
    label "wsp&#243;lnie"
  ]
  node [
    id 2544
    label "sp&#243;lny"
  ]
  node [
    id 2545
    label "jeden"
  ]
  node [
    id 2546
    label "uwsp&#243;lnienie"
  ]
  node [
    id 2547
    label "uwsp&#243;lnianie"
  ]
  node [
    id 2548
    label "sp&#243;lnie"
  ]
  node [
    id 2549
    label "dostosowanie"
  ]
  node [
    id 2550
    label "dostosowywanie"
  ]
  node [
    id 2551
    label "shot"
  ]
  node [
    id 2552
    label "jednakowy"
  ]
  node [
    id 2553
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 2554
    label "ujednolicenie"
  ]
  node [
    id 2555
    label "jaki&#347;"
  ]
  node [
    id 2556
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 2557
    label "jednolicie"
  ]
  node [
    id 2558
    label "kieliszek"
  ]
  node [
    id 2559
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 2560
    label "w&#243;dka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 238
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 972
  ]
  edge [
    source 13
    target 973
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 975
  ]
  edge [
    source 13
    target 976
  ]
  edge [
    source 13
    target 977
  ]
  edge [
    source 13
    target 978
  ]
  edge [
    source 13
    target 979
  ]
  edge [
    source 13
    target 980
  ]
  edge [
    source 13
    target 981
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 983
  ]
  edge [
    source 13
    target 984
  ]
  edge [
    source 13
    target 985
  ]
  edge [
    source 13
    target 986
  ]
  edge [
    source 13
    target 987
  ]
  edge [
    source 13
    target 988
  ]
  edge [
    source 13
    target 989
  ]
  edge [
    source 13
    target 990
  ]
  edge [
    source 13
    target 991
  ]
  edge [
    source 13
    target 992
  ]
  edge [
    source 13
    target 993
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 994
  ]
  edge [
    source 13
    target 995
  ]
  edge [
    source 13
    target 996
  ]
  edge [
    source 13
    target 997
  ]
  edge [
    source 13
    target 998
  ]
  edge [
    source 13
    target 999
  ]
  edge [
    source 13
    target 1000
  ]
  edge [
    source 13
    target 1001
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 1002
  ]
  edge [
    source 13
    target 1003
  ]
  edge [
    source 13
    target 1004
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 1005
  ]
  edge [
    source 13
    target 1006
  ]
  edge [
    source 13
    target 1007
  ]
  edge [
    source 13
    target 1008
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 1009
  ]
  edge [
    source 13
    target 1010
  ]
  edge [
    source 13
    target 1011
  ]
  edge [
    source 13
    target 1012
  ]
  edge [
    source 13
    target 1013
  ]
  edge [
    source 13
    target 1014
  ]
  edge [
    source 13
    target 1015
  ]
  edge [
    source 13
    target 1016
  ]
  edge [
    source 13
    target 1017
  ]
  edge [
    source 13
    target 1018
  ]
  edge [
    source 13
    target 1019
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 1020
  ]
  edge [
    source 13
    target 1021
  ]
  edge [
    source 13
    target 1022
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 1023
  ]
  edge [
    source 13
    target 1024
  ]
  edge [
    source 13
    target 1025
  ]
  edge [
    source 13
    target 1026
  ]
  edge [
    source 13
    target 1027
  ]
  edge [
    source 13
    target 1028
  ]
  edge [
    source 13
    target 1029
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 1030
  ]
  edge [
    source 13
    target 1031
  ]
  edge [
    source 13
    target 1032
  ]
  edge [
    source 13
    target 1033
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1046
  ]
  edge [
    source 15
    target 1047
  ]
  edge [
    source 15
    target 1048
  ]
  edge [
    source 15
    target 1049
  ]
  edge [
    source 15
    target 1050
  ]
  edge [
    source 15
    target 1051
  ]
  edge [
    source 15
    target 1052
  ]
  edge [
    source 15
    target 1053
  ]
  edge [
    source 15
    target 1054
  ]
  edge [
    source 15
    target 1055
  ]
  edge [
    source 15
    target 1056
  ]
  edge [
    source 15
    target 1057
  ]
  edge [
    source 15
    target 1058
  ]
  edge [
    source 15
    target 1059
  ]
  edge [
    source 15
    target 1060
  ]
  edge [
    source 15
    target 1061
  ]
  edge [
    source 15
    target 1062
  ]
  edge [
    source 15
    target 1063
  ]
  edge [
    source 15
    target 1064
  ]
  edge [
    source 15
    target 1065
  ]
  edge [
    source 15
    target 1066
  ]
  edge [
    source 15
    target 1067
  ]
  edge [
    source 15
    target 1068
  ]
  edge [
    source 15
    target 1069
  ]
  edge [
    source 15
    target 1070
  ]
  edge [
    source 15
    target 1071
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 330
  ]
  edge [
    source 16
    target 1072
  ]
  edge [
    source 16
    target 1073
  ]
  edge [
    source 16
    target 1074
  ]
  edge [
    source 16
    target 1075
  ]
  edge [
    source 16
    target 1076
  ]
  edge [
    source 16
    target 1077
  ]
  edge [
    source 16
    target 1078
  ]
  edge [
    source 16
    target 1079
  ]
  edge [
    source 16
    target 1080
  ]
  edge [
    source 16
    target 1081
  ]
  edge [
    source 16
    target 461
  ]
  edge [
    source 16
    target 1082
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 1083
  ]
  edge [
    source 16
    target 1084
  ]
  edge [
    source 16
    target 1085
  ]
  edge [
    source 16
    target 1086
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 270
  ]
  edge [
    source 16
    target 335
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 1087
  ]
  edge [
    source 16
    target 1088
  ]
  edge [
    source 16
    target 1089
  ]
  edge [
    source 16
    target 1090
  ]
  edge [
    source 16
    target 1091
  ]
  edge [
    source 16
    target 468
  ]
  edge [
    source 16
    target 1092
  ]
  edge [
    source 16
    target 1093
  ]
  edge [
    source 16
    target 1094
  ]
  edge [
    source 16
    target 1095
  ]
  edge [
    source 16
    target 1096
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 1097
  ]
  edge [
    source 16
    target 1098
  ]
  edge [
    source 16
    target 188
  ]
  edge [
    source 16
    target 1099
  ]
  edge [
    source 16
    target 1100
  ]
  edge [
    source 16
    target 1101
  ]
  edge [
    source 16
    target 1102
  ]
  edge [
    source 16
    target 61
  ]
  edge [
    source 16
    target 1103
  ]
  edge [
    source 16
    target 1104
  ]
  edge [
    source 16
    target 187
  ]
  edge [
    source 16
    target 1105
  ]
  edge [
    source 16
    target 1106
  ]
  edge [
    source 16
    target 1107
  ]
  edge [
    source 16
    target 1108
  ]
  edge [
    source 16
    target 328
  ]
  edge [
    source 16
    target 1109
  ]
  edge [
    source 16
    target 1110
  ]
  edge [
    source 16
    target 1111
  ]
  edge [
    source 16
    target 1112
  ]
  edge [
    source 16
    target 1113
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 1115
  ]
  edge [
    source 16
    target 1116
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 505
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 513
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 16
    target 1136
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1141
  ]
  edge [
    source 16
    target 1142
  ]
  edge [
    source 16
    target 1143
  ]
  edge [
    source 16
    target 1144
  ]
  edge [
    source 16
    target 1145
  ]
  edge [
    source 16
    target 1146
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 35
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 280
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 19
    target 415
  ]
  edge [
    source 19
    target 416
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 146
  ]
  edge [
    source 19
    target 417
  ]
  edge [
    source 19
    target 418
  ]
  edge [
    source 19
    target 419
  ]
  edge [
    source 19
    target 420
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 890
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1160
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 373
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 21
    target 1172
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1174
  ]
  edge [
    source 21
    target 1175
  ]
  edge [
    source 21
    target 1176
  ]
  edge [
    source 21
    target 1177
  ]
  edge [
    source 21
    target 1178
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 346
  ]
  edge [
    source 21
    target 1179
  ]
  edge [
    source 21
    target 1180
  ]
  edge [
    source 21
    target 1181
  ]
  edge [
    source 21
    target 1182
  ]
  edge [
    source 21
    target 413
  ]
  edge [
    source 21
    target 340
  ]
  edge [
    source 21
    target 1183
  ]
  edge [
    source 21
    target 1184
  ]
  edge [
    source 21
    target 1185
  ]
  edge [
    source 21
    target 1186
  ]
  edge [
    source 21
    target 270
  ]
  edge [
    source 21
    target 1187
  ]
  edge [
    source 21
    target 1188
  ]
  edge [
    source 21
    target 1189
  ]
  edge [
    source 21
    target 1190
  ]
  edge [
    source 21
    target 1191
  ]
  edge [
    source 21
    target 1192
  ]
  edge [
    source 21
    target 1193
  ]
  edge [
    source 21
    target 1194
  ]
  edge [
    source 21
    target 1195
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1196
  ]
  edge [
    source 21
    target 1197
  ]
  edge [
    source 21
    target 1198
  ]
  edge [
    source 21
    target 1199
  ]
  edge [
    source 21
    target 1200
  ]
  edge [
    source 21
    target 1201
  ]
  edge [
    source 21
    target 1202
  ]
  edge [
    source 21
    target 664
  ]
  edge [
    source 21
    target 1203
  ]
  edge [
    source 21
    target 1204
  ]
  edge [
    source 21
    target 1205
  ]
  edge [
    source 21
    target 1206
  ]
  edge [
    source 21
    target 1207
  ]
  edge [
    source 21
    target 102
  ]
  edge [
    source 21
    target 347
  ]
  edge [
    source 21
    target 1208
  ]
  edge [
    source 21
    target 1209
  ]
  edge [
    source 21
    target 1210
  ]
  edge [
    source 21
    target 1211
  ]
  edge [
    source 21
    target 1212
  ]
  edge [
    source 21
    target 1213
  ]
  edge [
    source 21
    target 1214
  ]
  edge [
    source 21
    target 1215
  ]
  edge [
    source 21
    target 1216
  ]
  edge [
    source 21
    target 1217
  ]
  edge [
    source 21
    target 1218
  ]
  edge [
    source 21
    target 1219
  ]
  edge [
    source 21
    target 1220
  ]
  edge [
    source 21
    target 1221
  ]
  edge [
    source 21
    target 1222
  ]
  edge [
    source 21
    target 1223
  ]
  edge [
    source 21
    target 1224
  ]
  edge [
    source 21
    target 1225
  ]
  edge [
    source 21
    target 1226
  ]
  edge [
    source 21
    target 1227
  ]
  edge [
    source 21
    target 1228
  ]
  edge [
    source 21
    target 1229
  ]
  edge [
    source 21
    target 1230
  ]
  edge [
    source 21
    target 1231
  ]
  edge [
    source 21
    target 1232
  ]
  edge [
    source 21
    target 1233
  ]
  edge [
    source 21
    target 1234
  ]
  edge [
    source 21
    target 1235
  ]
  edge [
    source 21
    target 1236
  ]
  edge [
    source 21
    target 1237
  ]
  edge [
    source 21
    target 1238
  ]
  edge [
    source 21
    target 1239
  ]
  edge [
    source 21
    target 1240
  ]
  edge [
    source 21
    target 1241
  ]
  edge [
    source 21
    target 1242
  ]
  edge [
    source 21
    target 1243
  ]
  edge [
    source 21
    target 1244
  ]
  edge [
    source 21
    target 1245
  ]
  edge [
    source 21
    target 1246
  ]
  edge [
    source 21
    target 1247
  ]
  edge [
    source 21
    target 280
  ]
  edge [
    source 21
    target 1248
  ]
  edge [
    source 21
    target 1249
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 21
    target 1250
  ]
  edge [
    source 21
    target 1251
  ]
  edge [
    source 21
    target 1252
  ]
  edge [
    source 21
    target 1253
  ]
  edge [
    source 21
    target 1254
  ]
  edge [
    source 21
    target 1255
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1256
  ]
  edge [
    source 21
    target 1257
  ]
  edge [
    source 21
    target 1258
  ]
  edge [
    source 21
    target 1006
  ]
  edge [
    source 21
    target 1259
  ]
  edge [
    source 21
    target 1260
  ]
  edge [
    source 21
    target 1261
  ]
  edge [
    source 21
    target 1262
  ]
  edge [
    source 21
    target 1263
  ]
  edge [
    source 21
    target 1264
  ]
  edge [
    source 21
    target 1265
  ]
  edge [
    source 21
    target 1266
  ]
  edge [
    source 21
    target 1267
  ]
  edge [
    source 21
    target 1268
  ]
  edge [
    source 21
    target 1269
  ]
  edge [
    source 21
    target 1270
  ]
  edge [
    source 21
    target 1271
  ]
  edge [
    source 21
    target 1272
  ]
  edge [
    source 21
    target 1273
  ]
  edge [
    source 21
    target 1274
  ]
  edge [
    source 21
    target 101
  ]
  edge [
    source 21
    target 754
  ]
  edge [
    source 21
    target 1275
  ]
  edge [
    source 21
    target 1276
  ]
  edge [
    source 21
    target 1277
  ]
  edge [
    source 21
    target 1278
  ]
  edge [
    source 21
    target 1279
  ]
  edge [
    source 21
    target 1280
  ]
  edge [
    source 21
    target 1281
  ]
  edge [
    source 21
    target 577
  ]
  edge [
    source 21
    target 1282
  ]
  edge [
    source 21
    target 1283
  ]
  edge [
    source 21
    target 1284
  ]
  edge [
    source 21
    target 1285
  ]
  edge [
    source 21
    target 1286
  ]
  edge [
    source 21
    target 1287
  ]
  edge [
    source 21
    target 1288
  ]
  edge [
    source 21
    target 1289
  ]
  edge [
    source 21
    target 1290
  ]
  edge [
    source 21
    target 1291
  ]
  edge [
    source 21
    target 238
  ]
  edge [
    source 21
    target 1292
  ]
  edge [
    source 21
    target 1293
  ]
  edge [
    source 21
    target 1294
  ]
  edge [
    source 21
    target 374
  ]
  edge [
    source 21
    target 508
  ]
  edge [
    source 21
    target 1295
  ]
  edge [
    source 21
    target 1296
  ]
  edge [
    source 21
    target 1297
  ]
  edge [
    source 21
    target 1298
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 653
  ]
  edge [
    source 21
    target 1307
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 670
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 1317
  ]
  edge [
    source 23
    target 1318
  ]
  edge [
    source 23
    target 1319
  ]
  edge [
    source 23
    target 1320
  ]
  edge [
    source 23
    target 1321
  ]
  edge [
    source 23
    target 1322
  ]
  edge [
    source 23
    target 1323
  ]
  edge [
    source 23
    target 1324
  ]
  edge [
    source 23
    target 1325
  ]
  edge [
    source 23
    target 1326
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 1327
  ]
  edge [
    source 23
    target 1328
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1329
  ]
  edge [
    source 28
    target 168
  ]
  edge [
    source 28
    target 1330
  ]
  edge [
    source 28
    target 1331
  ]
  edge [
    source 28
    target 911
  ]
  edge [
    source 28
    target 912
  ]
  edge [
    source 28
    target 913
  ]
  edge [
    source 28
    target 675
  ]
  edge [
    source 28
    target 914
  ]
  edge [
    source 28
    target 915
  ]
  edge [
    source 28
    target 916
  ]
  edge [
    source 28
    target 917
  ]
  edge [
    source 28
    target 918
  ]
  edge [
    source 28
    target 754
  ]
  edge [
    source 28
    target 919
  ]
  edge [
    source 28
    target 920
  ]
  edge [
    source 28
    target 921
  ]
  edge [
    source 28
    target 922
  ]
  edge [
    source 28
    target 923
  ]
  edge [
    source 28
    target 924
  ]
  edge [
    source 28
    target 925
  ]
  edge [
    source 28
    target 926
  ]
  edge [
    source 28
    target 927
  ]
  edge [
    source 28
    target 664
  ]
  edge [
    source 28
    target 928
  ]
  edge [
    source 28
    target 1332
  ]
  edge [
    source 28
    target 1333
  ]
  edge [
    source 28
    target 1334
  ]
  edge [
    source 28
    target 1335
  ]
  edge [
    source 28
    target 1336
  ]
  edge [
    source 28
    target 1337
  ]
  edge [
    source 28
    target 1338
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1339
  ]
  edge [
    source 29
    target 1340
  ]
  edge [
    source 29
    target 1341
  ]
  edge [
    source 29
    target 249
  ]
  edge [
    source 29
    target 1325
  ]
  edge [
    source 29
    target 415
  ]
  edge [
    source 29
    target 1342
  ]
  edge [
    source 29
    target 1155
  ]
  edge [
    source 29
    target 1343
  ]
  edge [
    source 29
    target 1344
  ]
  edge [
    source 29
    target 1345
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1346
  ]
  edge [
    source 30
    target 1339
  ]
  edge [
    source 30
    target 1340
  ]
  edge [
    source 30
    target 1341
  ]
  edge [
    source 30
    target 1347
  ]
  edge [
    source 30
    target 1348
  ]
  edge [
    source 30
    target 1349
  ]
  edge [
    source 30
    target 905
  ]
  edge [
    source 30
    target 1350
  ]
  edge [
    source 30
    target 1351
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 32
    target 1243
  ]
  edge [
    source 32
    target 1244
  ]
  edge [
    source 32
    target 1245
  ]
  edge [
    source 32
    target 1246
  ]
  edge [
    source 32
    target 1164
  ]
  edge [
    source 32
    target 1247
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 1248
  ]
  edge [
    source 32
    target 1249
  ]
  edge [
    source 32
    target 460
  ]
  edge [
    source 32
    target 1166
  ]
  edge [
    source 32
    target 1250
  ]
  edge [
    source 32
    target 1251
  ]
  edge [
    source 32
    target 1252
  ]
  edge [
    source 32
    target 1167
  ]
  edge [
    source 32
    target 1253
  ]
  edge [
    source 32
    target 1168
  ]
  edge [
    source 32
    target 1254
  ]
  edge [
    source 32
    target 1255
  ]
  edge [
    source 32
    target 1119
  ]
  edge [
    source 32
    target 1256
  ]
  edge [
    source 32
    target 1257
  ]
  edge [
    source 32
    target 1258
  ]
  edge [
    source 32
    target 1006
  ]
  edge [
    source 32
    target 1259
  ]
  edge [
    source 32
    target 1260
  ]
  edge [
    source 32
    target 1261
  ]
  edge [
    source 32
    target 1262
  ]
  edge [
    source 32
    target 1263
  ]
  edge [
    source 32
    target 1264
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1239
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1175
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1177
  ]
  edge [
    source 32
    target 1242
  ]
  edge [
    source 32
    target 380
  ]
  edge [
    source 32
    target 381
  ]
  edge [
    source 32
    target 382
  ]
  edge [
    source 32
    target 383
  ]
  edge [
    source 32
    target 385
  ]
  edge [
    source 32
    target 386
  ]
  edge [
    source 32
    target 387
  ]
  edge [
    source 32
    target 389
  ]
  edge [
    source 32
    target 388
  ]
  edge [
    source 32
    target 384
  ]
  edge [
    source 32
    target 1352
  ]
  edge [
    source 32
    target 1353
  ]
  edge [
    source 32
    target 1354
  ]
  edge [
    source 32
    target 413
  ]
  edge [
    source 32
    target 1355
  ]
  edge [
    source 32
    target 497
  ]
  edge [
    source 32
    target 1356
  ]
  edge [
    source 32
    target 1357
  ]
  edge [
    source 32
    target 1358
  ]
  edge [
    source 32
    target 1359
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 1365
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 1366
  ]
  edge [
    source 32
    target 971
  ]
  edge [
    source 32
    target 1367
  ]
  edge [
    source 32
    target 1368
  ]
  edge [
    source 32
    target 1369
  ]
  edge [
    source 32
    target 1370
  ]
  edge [
    source 32
    target 1371
  ]
  edge [
    source 32
    target 1372
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 1374
  ]
  edge [
    source 32
    target 985
  ]
  edge [
    source 32
    target 1375
  ]
  edge [
    source 32
    target 1376
  ]
  edge [
    source 32
    target 1377
  ]
  edge [
    source 32
    target 1378
  ]
  edge [
    source 32
    target 1379
  ]
  edge [
    source 32
    target 263
  ]
  edge [
    source 32
    target 264
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 265
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 414
  ]
  edge [
    source 32
    target 415
  ]
  edge [
    source 32
    target 416
  ]
  edge [
    source 32
    target 146
  ]
  edge [
    source 32
    target 417
  ]
  edge [
    source 32
    target 418
  ]
  edge [
    source 32
    target 419
  ]
  edge [
    source 32
    target 420
  ]
  edge [
    source 32
    target 1380
  ]
  edge [
    source 32
    target 1381
  ]
  edge [
    source 32
    target 1382
  ]
  edge [
    source 32
    target 1383
  ]
  edge [
    source 32
    target 1384
  ]
  edge [
    source 32
    target 1385
  ]
  edge [
    source 32
    target 1386
  ]
  edge [
    source 32
    target 1387
  ]
  edge [
    source 32
    target 328
  ]
  edge [
    source 32
    target 1388
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 1389
  ]
  edge [
    source 32
    target 1390
  ]
  edge [
    source 32
    target 1391
  ]
  edge [
    source 32
    target 1392
  ]
  edge [
    source 32
    target 1393
  ]
  edge [
    source 32
    target 1394
  ]
  edge [
    source 32
    target 1182
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 1395
  ]
  edge [
    source 32
    target 1396
  ]
  edge [
    source 32
    target 1397
  ]
  edge [
    source 32
    target 431
  ]
  edge [
    source 32
    target 1398
  ]
  edge [
    source 32
    target 556
  ]
  edge [
    source 32
    target 1399
  ]
  edge [
    source 32
    target 1400
  ]
  edge [
    source 32
    target 1401
  ]
  edge [
    source 32
    target 513
  ]
  edge [
    source 32
    target 1402
  ]
  edge [
    source 32
    target 1403
  ]
  edge [
    source 32
    target 1404
  ]
  edge [
    source 32
    target 1235
  ]
  edge [
    source 32
    target 1186
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 1187
  ]
  edge [
    source 32
    target 1188
  ]
  edge [
    source 32
    target 1189
  ]
  edge [
    source 32
    target 1190
  ]
  edge [
    source 32
    target 1191
  ]
  edge [
    source 32
    target 1192
  ]
  edge [
    source 32
    target 1193
  ]
  edge [
    source 32
    target 1194
  ]
  edge [
    source 32
    target 1195
  ]
  edge [
    source 32
    target 1089
  ]
  edge [
    source 32
    target 1196
  ]
  edge [
    source 32
    target 1197
  ]
  edge [
    source 32
    target 1198
  ]
  edge [
    source 32
    target 1199
  ]
  edge [
    source 32
    target 1200
  ]
  edge [
    source 32
    target 1405
  ]
  edge [
    source 32
    target 1406
  ]
  edge [
    source 32
    target 1407
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1162
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 101
  ]
  edge [
    source 32
    target 754
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 577
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 238
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 374
  ]
  edge [
    source 32
    target 508
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 32
    target 1408
  ]
  edge [
    source 32
    target 651
  ]
  edge [
    source 32
    target 1409
  ]
  edge [
    source 32
    target 1410
  ]
  edge [
    source 32
    target 1411
  ]
  edge [
    source 32
    target 1412
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1414
  ]
  edge [
    source 32
    target 1415
  ]
  edge [
    source 32
    target 1416
  ]
  edge [
    source 32
    target 1417
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 32
    target 1419
  ]
  edge [
    source 32
    target 1420
  ]
  edge [
    source 32
    target 1421
  ]
  edge [
    source 32
    target 1422
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1424
  ]
  edge [
    source 32
    target 1425
  ]
  edge [
    source 32
    target 1426
  ]
  edge [
    source 32
    target 1427
  ]
  edge [
    source 32
    target 1428
  ]
  edge [
    source 32
    target 1429
  ]
  edge [
    source 32
    target 1430
  ]
  edge [
    source 32
    target 1431
  ]
  edge [
    source 32
    target 1432
  ]
  edge [
    source 32
    target 1433
  ]
  edge [
    source 32
    target 1434
  ]
  edge [
    source 32
    target 1435
  ]
  edge [
    source 32
    target 1436
  ]
  edge [
    source 32
    target 1437
  ]
  edge [
    source 32
    target 1438
  ]
  edge [
    source 32
    target 1439
  ]
  edge [
    source 32
    target 1440
  ]
  edge [
    source 32
    target 1441
  ]
  edge [
    source 32
    target 1442
  ]
  edge [
    source 32
    target 1443
  ]
  edge [
    source 32
    target 1444
  ]
  edge [
    source 32
    target 351
  ]
  edge [
    source 32
    target 1445
  ]
  edge [
    source 32
    target 1446
  ]
  edge [
    source 32
    target 1447
  ]
  edge [
    source 32
    target 774
  ]
  edge [
    source 32
    target 1448
  ]
  edge [
    source 32
    target 655
  ]
  edge [
    source 32
    target 1449
  ]
  edge [
    source 32
    target 1450
  ]
  edge [
    source 32
    target 1451
  ]
  edge [
    source 32
    target 1452
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 1453
  ]
  edge [
    source 32
    target 1454
  ]
  edge [
    source 32
    target 1455
  ]
  edge [
    source 32
    target 97
  ]
  edge [
    source 32
    target 1456
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 125
  ]
  edge [
    source 32
    target 1457
  ]
  edge [
    source 32
    target 1458
  ]
  edge [
    source 32
    target 723
  ]
  edge [
    source 32
    target 734
  ]
  edge [
    source 32
    target 664
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1459
  ]
  edge [
    source 32
    target 1460
  ]
  edge [
    source 32
    target 1097
  ]
  edge [
    source 32
    target 1461
  ]
  edge [
    source 32
    target 1073
  ]
  edge [
    source 32
    target 1462
  ]
  edge [
    source 32
    target 1463
  ]
  edge [
    source 32
    target 454
  ]
  edge [
    source 32
    target 187
  ]
  edge [
    source 32
    target 1464
  ]
  edge [
    source 32
    target 1465
  ]
  edge [
    source 32
    target 1466
  ]
  edge [
    source 32
    target 879
  ]
  edge [
    source 32
    target 1467
  ]
  edge [
    source 32
    target 1468
  ]
  edge [
    source 32
    target 1469
  ]
  edge [
    source 32
    target 1470
  ]
  edge [
    source 32
    target 1471
  ]
  edge [
    source 32
    target 1472
  ]
  edge [
    source 32
    target 1473
  ]
  edge [
    source 32
    target 1474
  ]
  edge [
    source 32
    target 1475
  ]
  edge [
    source 32
    target 1476
  ]
  edge [
    source 32
    target 1477
  ]
  edge [
    source 32
    target 1478
  ]
  edge [
    source 32
    target 1479
  ]
  edge [
    source 32
    target 1480
  ]
  edge [
    source 32
    target 1481
  ]
  edge [
    source 32
    target 1482
  ]
  edge [
    source 32
    target 1483
  ]
  edge [
    source 32
    target 1019
  ]
  edge [
    source 32
    target 1484
  ]
  edge [
    source 32
    target 1485
  ]
  edge [
    source 32
    target 1486
  ]
  edge [
    source 32
    target 1487
  ]
  edge [
    source 32
    target 1488
  ]
  edge [
    source 32
    target 1489
  ]
  edge [
    source 32
    target 1490
  ]
  edge [
    source 32
    target 1491
  ]
  edge [
    source 32
    target 1492
  ]
  edge [
    source 32
    target 998
  ]
  edge [
    source 32
    target 1493
  ]
  edge [
    source 32
    target 1494
  ]
  edge [
    source 32
    target 1030
  ]
  edge [
    source 32
    target 1495
  ]
  edge [
    source 32
    target 1496
  ]
  edge [
    source 32
    target 1497
  ]
  edge [
    source 32
    target 1498
  ]
  edge [
    source 32
    target 1499
  ]
  edge [
    source 32
    target 1500
  ]
  edge [
    source 32
    target 1501
  ]
  edge [
    source 32
    target 1502
  ]
  edge [
    source 32
    target 1503
  ]
  edge [
    source 32
    target 232
  ]
  edge [
    source 32
    target 1504
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 32
    target 1505
  ]
  edge [
    source 32
    target 1506
  ]
  edge [
    source 32
    target 512
  ]
  edge [
    source 32
    target 1507
  ]
  edge [
    source 32
    target 1508
  ]
  edge [
    source 32
    target 1509
  ]
  edge [
    source 32
    target 153
  ]
  edge [
    source 32
    target 1510
  ]
  edge [
    source 32
    target 1511
  ]
  edge [
    source 32
    target 1512
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 1513
  ]
  edge [
    source 32
    target 1514
  ]
  edge [
    source 32
    target 1011
  ]
  edge [
    source 32
    target 1515
  ]
  edge [
    source 32
    target 1516
  ]
  edge [
    source 32
    target 1517
  ]
  edge [
    source 32
    target 1518
  ]
  edge [
    source 32
    target 1519
  ]
  edge [
    source 32
    target 1520
  ]
  edge [
    source 32
    target 1521
  ]
  edge [
    source 32
    target 1522
  ]
  edge [
    source 32
    target 1523
  ]
  edge [
    source 32
    target 1524
  ]
  edge [
    source 32
    target 1525
  ]
  edge [
    source 32
    target 1526
  ]
  edge [
    source 32
    target 1527
  ]
  edge [
    source 32
    target 1528
  ]
  edge [
    source 32
    target 1529
  ]
  edge [
    source 32
    target 1530
  ]
  edge [
    source 32
    target 1531
  ]
  edge [
    source 32
    target 1532
  ]
  edge [
    source 32
    target 1533
  ]
  edge [
    source 32
    target 1534
  ]
  edge [
    source 32
    target 1535
  ]
  edge [
    source 32
    target 1087
  ]
  edge [
    source 32
    target 1536
  ]
  edge [
    source 32
    target 1022
  ]
  edge [
    source 32
    target 1537
  ]
  edge [
    source 32
    target 1538
  ]
  edge [
    source 32
    target 1539
  ]
  edge [
    source 32
    target 1540
  ]
  edge [
    source 32
    target 738
  ]
  edge [
    source 32
    target 1541
  ]
  edge [
    source 32
    target 1542
  ]
  edge [
    source 32
    target 1543
  ]
  edge [
    source 32
    target 1544
  ]
  edge [
    source 32
    target 1545
  ]
  edge [
    source 32
    target 1546
  ]
  edge [
    source 32
    target 1547
  ]
  edge [
    source 32
    target 1548
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1549
  ]
  edge [
    source 33
    target 596
  ]
  edge [
    source 33
    target 1550
  ]
  edge [
    source 33
    target 1551
  ]
  edge [
    source 33
    target 1552
  ]
  edge [
    source 33
    target 310
  ]
  edge [
    source 33
    target 1553
  ]
  edge [
    source 33
    target 232
  ]
  edge [
    source 33
    target 1554
  ]
  edge [
    source 33
    target 1555
  ]
  edge [
    source 33
    target 1556
  ]
  edge [
    source 33
    target 1557
  ]
  edge [
    source 33
    target 549
  ]
  edge [
    source 33
    target 1558
  ]
  edge [
    source 33
    target 1559
  ]
  edge [
    source 33
    target 1560
  ]
  edge [
    source 33
    target 1561
  ]
  edge [
    source 33
    target 1562
  ]
  edge [
    source 33
    target 1563
  ]
  edge [
    source 33
    target 1564
  ]
  edge [
    source 33
    target 1565
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1566
  ]
  edge [
    source 34
    target 1567
  ]
  edge [
    source 34
    target 1568
  ]
  edge [
    source 34
    target 1569
  ]
  edge [
    source 34
    target 1570
  ]
  edge [
    source 34
    target 1571
  ]
  edge [
    source 34
    target 1572
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 1573
  ]
  edge [
    source 35
    target 1574
  ]
  edge [
    source 35
    target 331
  ]
  edge [
    source 35
    target 1575
  ]
  edge [
    source 35
    target 364
  ]
  edge [
    source 35
    target 1576
  ]
  edge [
    source 35
    target 1276
  ]
  edge [
    source 35
    target 925
  ]
  edge [
    source 35
    target 883
  ]
  edge [
    source 35
    target 1577
  ]
  edge [
    source 35
    target 1578
  ]
  edge [
    source 35
    target 1579
  ]
  edge [
    source 35
    target 1580
  ]
  edge [
    source 35
    target 102
  ]
  edge [
    source 35
    target 1581
  ]
  edge [
    source 35
    target 1582
  ]
  edge [
    source 35
    target 1583
  ]
  edge [
    source 35
    target 463
  ]
  edge [
    source 35
    target 1584
  ]
  edge [
    source 35
    target 1585
  ]
  edge [
    source 35
    target 1586
  ]
  edge [
    source 35
    target 1587
  ]
  edge [
    source 35
    target 1588
  ]
  edge [
    source 35
    target 1589
  ]
  edge [
    source 35
    target 1590
  ]
  edge [
    source 35
    target 1591
  ]
  edge [
    source 35
    target 1592
  ]
  edge [
    source 35
    target 1593
  ]
  edge [
    source 35
    target 328
  ]
  edge [
    source 35
    target 1594
  ]
  edge [
    source 35
    target 1595
  ]
  edge [
    source 35
    target 130
  ]
  edge [
    source 35
    target 1596
  ]
  edge [
    source 35
    target 413
  ]
  edge [
    source 35
    target 1597
  ]
  edge [
    source 35
    target 1598
  ]
  edge [
    source 35
    target 1599
  ]
  edge [
    source 35
    target 1600
  ]
  edge [
    source 35
    target 205
  ]
  edge [
    source 35
    target 1076
  ]
  edge [
    source 35
    target 1601
  ]
  edge [
    source 35
    target 1556
  ]
  edge [
    source 35
    target 1602
  ]
  edge [
    source 35
    target 1603
  ]
  edge [
    source 35
    target 1604
  ]
  edge [
    source 35
    target 1011
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 1605
  ]
  edge [
    source 35
    target 1606
  ]
  edge [
    source 35
    target 134
  ]
  edge [
    source 35
    target 1607
  ]
  edge [
    source 35
    target 1608
  ]
  edge [
    source 35
    target 1609
  ]
  edge [
    source 35
    target 1138
  ]
  edge [
    source 35
    target 1610
  ]
  edge [
    source 35
    target 1611
  ]
  edge [
    source 35
    target 1612
  ]
  edge [
    source 35
    target 1613
  ]
  edge [
    source 35
    target 1614
  ]
  edge [
    source 35
    target 1615
  ]
  edge [
    source 35
    target 390
  ]
  edge [
    source 35
    target 391
  ]
  edge [
    source 35
    target 392
  ]
  edge [
    source 35
    target 393
  ]
  edge [
    source 35
    target 394
  ]
  edge [
    source 35
    target 395
  ]
  edge [
    source 35
    target 87
  ]
  edge [
    source 35
    target 396
  ]
  edge [
    source 35
    target 397
  ]
  edge [
    source 35
    target 398
  ]
  edge [
    source 35
    target 399
  ]
  edge [
    source 35
    target 400
  ]
  edge [
    source 35
    target 280
  ]
  edge [
    source 35
    target 401
  ]
  edge [
    source 35
    target 995
  ]
  edge [
    source 35
    target 1616
  ]
  edge [
    source 35
    target 999
  ]
  edge [
    source 35
    target 1617
  ]
  edge [
    source 35
    target 1618
  ]
  edge [
    source 35
    target 1002
  ]
  edge [
    source 35
    target 1005
  ]
  edge [
    source 35
    target 1619
  ]
  edge [
    source 35
    target 1620
  ]
  edge [
    source 35
    target 1621
  ]
  edge [
    source 35
    target 1622
  ]
  edge [
    source 35
    target 1623
  ]
  edge [
    source 35
    target 1624
  ]
  edge [
    source 35
    target 1625
  ]
  edge [
    source 35
    target 1626
  ]
  edge [
    source 35
    target 1627
  ]
  edge [
    source 35
    target 1628
  ]
  edge [
    source 35
    target 1629
  ]
  edge [
    source 35
    target 1630
  ]
  edge [
    source 35
    target 1631
  ]
  edge [
    source 35
    target 1632
  ]
  edge [
    source 35
    target 1633
  ]
  edge [
    source 35
    target 1634
  ]
  edge [
    source 35
    target 1635
  ]
  edge [
    source 35
    target 1636
  ]
  edge [
    source 35
    target 1637
  ]
  edge [
    source 35
    target 1638
  ]
  edge [
    source 35
    target 1639
  ]
  edge [
    source 35
    target 1640
  ]
  edge [
    source 35
    target 1641
  ]
  edge [
    source 35
    target 903
  ]
  edge [
    source 35
    target 1642
  ]
  edge [
    source 35
    target 1643
  ]
  edge [
    source 35
    target 1644
  ]
  edge [
    source 35
    target 1645
  ]
  edge [
    source 35
    target 54
  ]
  edge [
    source 35
    target 1646
  ]
  edge [
    source 35
    target 1647
  ]
  edge [
    source 35
    target 1648
  ]
  edge [
    source 35
    target 1649
  ]
  edge [
    source 35
    target 696
  ]
  edge [
    source 35
    target 1650
  ]
  edge [
    source 35
    target 1651
  ]
  edge [
    source 35
    target 1652
  ]
  edge [
    source 35
    target 1653
  ]
  edge [
    source 35
    target 1654
  ]
  edge [
    source 35
    target 1655
  ]
  edge [
    source 35
    target 1656
  ]
  edge [
    source 35
    target 1657
  ]
  edge [
    source 35
    target 1658
  ]
  edge [
    source 35
    target 1659
  ]
  edge [
    source 35
    target 1660
  ]
  edge [
    source 35
    target 1661
  ]
  edge [
    source 35
    target 1662
  ]
  edge [
    source 35
    target 1663
  ]
  edge [
    source 35
    target 1664
  ]
  edge [
    source 35
    target 1665
  ]
  edge [
    source 35
    target 539
  ]
  edge [
    source 35
    target 1666
  ]
  edge [
    source 35
    target 1667
  ]
  edge [
    source 35
    target 1668
  ]
  edge [
    source 35
    target 1669
  ]
  edge [
    source 35
    target 1670
  ]
  edge [
    source 35
    target 1671
  ]
  edge [
    source 35
    target 1672
  ]
  edge [
    source 35
    target 1673
  ]
  edge [
    source 36
    target 1674
  ]
  edge [
    source 36
    target 1675
  ]
  edge [
    source 36
    target 1313
  ]
  edge [
    source 36
    target 1572
  ]
  edge [
    source 36
    target 1676
  ]
  edge [
    source 36
    target 1677
  ]
  edge [
    source 36
    target 1678
  ]
  edge [
    source 36
    target 1679
  ]
  edge [
    source 36
    target 1680
  ]
  edge [
    source 37
    target 1681
  ]
  edge [
    source 37
    target 1682
  ]
  edge [
    source 37
    target 1683
  ]
  edge [
    source 37
    target 1684
  ]
  edge [
    source 37
    target 1685
  ]
  edge [
    source 37
    target 1686
  ]
  edge [
    source 37
    target 1687
  ]
  edge [
    source 37
    target 1688
  ]
  edge [
    source 37
    target 1689
  ]
  edge [
    source 37
    target 1690
  ]
  edge [
    source 37
    target 1691
  ]
  edge [
    source 37
    target 1692
  ]
  edge [
    source 37
    target 1693
  ]
  edge [
    source 37
    target 1694
  ]
  edge [
    source 37
    target 1695
  ]
  edge [
    source 37
    target 1696
  ]
  edge [
    source 37
    target 1697
  ]
  edge [
    source 37
    target 1698
  ]
  edge [
    source 37
    target 1699
  ]
  edge [
    source 37
    target 1700
  ]
  edge [
    source 37
    target 1701
  ]
  edge [
    source 37
    target 1702
  ]
  edge [
    source 37
    target 1703
  ]
  edge [
    source 37
    target 1704
  ]
  edge [
    source 37
    target 1705
  ]
  edge [
    source 37
    target 1706
  ]
  edge [
    source 37
    target 1707
  ]
  edge [
    source 37
    target 1708
  ]
  edge [
    source 37
    target 1709
  ]
  edge [
    source 37
    target 1710
  ]
  edge [
    source 37
    target 1711
  ]
  edge [
    source 37
    target 1712
  ]
  edge [
    source 37
    target 1713
  ]
  edge [
    source 37
    target 1714
  ]
  edge [
    source 37
    target 1715
  ]
  edge [
    source 37
    target 1716
  ]
  edge [
    source 37
    target 1717
  ]
  edge [
    source 37
    target 1718
  ]
  edge [
    source 37
    target 1719
  ]
  edge [
    source 37
    target 1720
  ]
  edge [
    source 37
    target 1721
  ]
  edge [
    source 37
    target 1722
  ]
  edge [
    source 37
    target 1723
  ]
  edge [
    source 37
    target 1724
  ]
  edge [
    source 37
    target 1725
  ]
  edge [
    source 37
    target 1726
  ]
  edge [
    source 37
    target 1727
  ]
  edge [
    source 37
    target 1728
  ]
  edge [
    source 37
    target 1729
  ]
  edge [
    source 37
    target 1730
  ]
  edge [
    source 37
    target 1731
  ]
  edge [
    source 37
    target 1732
  ]
  edge [
    source 37
    target 1733
  ]
  edge [
    source 37
    target 1734
  ]
  edge [
    source 37
    target 1735
  ]
  edge [
    source 37
    target 1736
  ]
  edge [
    source 37
    target 1737
  ]
  edge [
    source 37
    target 1738
  ]
  edge [
    source 37
    target 1739
  ]
  edge [
    source 37
    target 1740
  ]
  edge [
    source 37
    target 1741
  ]
  edge [
    source 37
    target 1742
  ]
  edge [
    source 37
    target 1743
  ]
  edge [
    source 37
    target 1744
  ]
  edge [
    source 37
    target 1745
  ]
  edge [
    source 37
    target 1746
  ]
  edge [
    source 37
    target 1747
  ]
  edge [
    source 37
    target 1748
  ]
  edge [
    source 37
    target 203
  ]
  edge [
    source 37
    target 1749
  ]
  edge [
    source 37
    target 1750
  ]
  edge [
    source 37
    target 1751
  ]
  edge [
    source 37
    target 1752
  ]
  edge [
    source 37
    target 1753
  ]
  edge [
    source 37
    target 1754
  ]
  edge [
    source 37
    target 1755
  ]
  edge [
    source 37
    target 1756
  ]
  edge [
    source 37
    target 1757
  ]
  edge [
    source 37
    target 1758
  ]
  edge [
    source 37
    target 1759
  ]
  edge [
    source 37
    target 1760
  ]
  edge [
    source 37
    target 1761
  ]
  edge [
    source 37
    target 1762
  ]
  edge [
    source 37
    target 1763
  ]
  edge [
    source 37
    target 1764
  ]
  edge [
    source 37
    target 1765
  ]
  edge [
    source 37
    target 1766
  ]
  edge [
    source 37
    target 1767
  ]
  edge [
    source 37
    target 1768
  ]
  edge [
    source 37
    target 1769
  ]
  edge [
    source 37
    target 1770
  ]
  edge [
    source 37
    target 1771
  ]
  edge [
    source 37
    target 1772
  ]
  edge [
    source 37
    target 1773
  ]
  edge [
    source 37
    target 1774
  ]
  edge [
    source 37
    target 1775
  ]
  edge [
    source 37
    target 1776
  ]
  edge [
    source 37
    target 1777
  ]
  edge [
    source 37
    target 1778
  ]
  edge [
    source 37
    target 1779
  ]
  edge [
    source 37
    target 1780
  ]
  edge [
    source 37
    target 1781
  ]
  edge [
    source 37
    target 1782
  ]
  edge [
    source 37
    target 1783
  ]
  edge [
    source 37
    target 1784
  ]
  edge [
    source 37
    target 1785
  ]
  edge [
    source 37
    target 1786
  ]
  edge [
    source 37
    target 1787
  ]
  edge [
    source 37
    target 1788
  ]
  edge [
    source 37
    target 1789
  ]
  edge [
    source 37
    target 1790
  ]
  edge [
    source 37
    target 1791
  ]
  edge [
    source 37
    target 1792
  ]
  edge [
    source 37
    target 1793
  ]
  edge [
    source 37
    target 1794
  ]
  edge [
    source 37
    target 1795
  ]
  edge [
    source 37
    target 1796
  ]
  edge [
    source 37
    target 1797
  ]
  edge [
    source 37
    target 1798
  ]
  edge [
    source 37
    target 1799
  ]
  edge [
    source 37
    target 1800
  ]
  edge [
    source 37
    target 1801
  ]
  edge [
    source 37
    target 1802
  ]
  edge [
    source 37
    target 1803
  ]
  edge [
    source 37
    target 1804
  ]
  edge [
    source 37
    target 1805
  ]
  edge [
    source 37
    target 1806
  ]
  edge [
    source 37
    target 1807
  ]
  edge [
    source 37
    target 1808
  ]
  edge [
    source 37
    target 1809
  ]
  edge [
    source 37
    target 1810
  ]
  edge [
    source 37
    target 1811
  ]
  edge [
    source 37
    target 1812
  ]
  edge [
    source 37
    target 1813
  ]
  edge [
    source 37
    target 1814
  ]
  edge [
    source 37
    target 1815
  ]
  edge [
    source 37
    target 1816
  ]
  edge [
    source 37
    target 1817
  ]
  edge [
    source 37
    target 1818
  ]
  edge [
    source 37
    target 1819
  ]
  edge [
    source 37
    target 1820
  ]
  edge [
    source 37
    target 1821
  ]
  edge [
    source 37
    target 1822
  ]
  edge [
    source 37
    target 1823
  ]
  edge [
    source 37
    target 1824
  ]
  edge [
    source 37
    target 1825
  ]
  edge [
    source 37
    target 1826
  ]
  edge [
    source 37
    target 1827
  ]
  edge [
    source 37
    target 1828
  ]
  edge [
    source 37
    target 1829
  ]
  edge [
    source 37
    target 1830
  ]
  edge [
    source 37
    target 1831
  ]
  edge [
    source 37
    target 1832
  ]
  edge [
    source 37
    target 1833
  ]
  edge [
    source 37
    target 1834
  ]
  edge [
    source 37
    target 1835
  ]
  edge [
    source 37
    target 1836
  ]
  edge [
    source 37
    target 1837
  ]
  edge [
    source 37
    target 1838
  ]
  edge [
    source 37
    target 1839
  ]
  edge [
    source 37
    target 1840
  ]
  edge [
    source 37
    target 1841
  ]
  edge [
    source 37
    target 1842
  ]
  edge [
    source 37
    target 1843
  ]
  edge [
    source 37
    target 1844
  ]
  edge [
    source 37
    target 1845
  ]
  edge [
    source 37
    target 1846
  ]
  edge [
    source 37
    target 1847
  ]
  edge [
    source 37
    target 1848
  ]
  edge [
    source 37
    target 1849
  ]
  edge [
    source 37
    target 1850
  ]
  edge [
    source 37
    target 1851
  ]
  edge [
    source 37
    target 1852
  ]
  edge [
    source 37
    target 1853
  ]
  edge [
    source 37
    target 1854
  ]
  edge [
    source 37
    target 1855
  ]
  edge [
    source 37
    target 1856
  ]
  edge [
    source 37
    target 1857
  ]
  edge [
    source 37
    target 1858
  ]
  edge [
    source 37
    target 1859
  ]
  edge [
    source 37
    target 1860
  ]
  edge [
    source 37
    target 1861
  ]
  edge [
    source 37
    target 1862
  ]
  edge [
    source 37
    target 1863
  ]
  edge [
    source 37
    target 1864
  ]
  edge [
    source 37
    target 1865
  ]
  edge [
    source 37
    target 1866
  ]
  edge [
    source 37
    target 1867
  ]
  edge [
    source 37
    target 1868
  ]
  edge [
    source 37
    target 1869
  ]
  edge [
    source 37
    target 1870
  ]
  edge [
    source 37
    target 1871
  ]
  edge [
    source 37
    target 1872
  ]
  edge [
    source 37
    target 1873
  ]
  edge [
    source 37
    target 1874
  ]
  edge [
    source 37
    target 1875
  ]
  edge [
    source 37
    target 1876
  ]
  edge [
    source 37
    target 1877
  ]
  edge [
    source 37
    target 1878
  ]
  edge [
    source 37
    target 1879
  ]
  edge [
    source 37
    target 1880
  ]
  edge [
    source 37
    target 1881
  ]
  edge [
    source 37
    target 1882
  ]
  edge [
    source 37
    target 1883
  ]
  edge [
    source 37
    target 1884
  ]
  edge [
    source 37
    target 1885
  ]
  edge [
    source 37
    target 1886
  ]
  edge [
    source 37
    target 1887
  ]
  edge [
    source 37
    target 1888
  ]
  edge [
    source 37
    target 1889
  ]
  edge [
    source 37
    target 1890
  ]
  edge [
    source 37
    target 1891
  ]
  edge [
    source 37
    target 1892
  ]
  edge [
    source 37
    target 1893
  ]
  edge [
    source 37
    target 1894
  ]
  edge [
    source 37
    target 1895
  ]
  edge [
    source 37
    target 143
  ]
  edge [
    source 37
    target 1896
  ]
  edge [
    source 37
    target 1897
  ]
  edge [
    source 37
    target 161
  ]
  edge [
    source 37
    target 1898
  ]
  edge [
    source 37
    target 1899
  ]
  edge [
    source 37
    target 1900
  ]
  edge [
    source 37
    target 1901
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 1902
  ]
  edge [
    source 37
    target 1903
  ]
  edge [
    source 37
    target 1467
  ]
  edge [
    source 37
    target 1904
  ]
  edge [
    source 37
    target 1905
  ]
  edge [
    source 37
    target 1906
  ]
  edge [
    source 37
    target 1907
  ]
  edge [
    source 37
    target 1908
  ]
  edge [
    source 37
    target 1909
  ]
  edge [
    source 37
    target 1910
  ]
  edge [
    source 37
    target 146
  ]
  edge [
    source 37
    target 1911
  ]
  edge [
    source 37
    target 1912
  ]
  edge [
    source 37
    target 450
  ]
  edge [
    source 37
    target 1913
  ]
  edge [
    source 37
    target 254
  ]
  edge [
    source 37
    target 1914
  ]
  edge [
    source 37
    target 1915
  ]
  edge [
    source 37
    target 138
  ]
  edge [
    source 37
    target 328
  ]
  edge [
    source 37
    target 1916
  ]
  edge [
    source 37
    target 1917
  ]
  edge [
    source 37
    target 1918
  ]
  edge [
    source 37
    target 1919
  ]
  edge [
    source 37
    target 1920
  ]
  edge [
    source 37
    target 1921
  ]
  edge [
    source 37
    target 1922
  ]
  edge [
    source 37
    target 1923
  ]
  edge [
    source 37
    target 1924
  ]
  edge [
    source 37
    target 1925
  ]
  edge [
    source 37
    target 1926
  ]
  edge [
    source 37
    target 1927
  ]
  edge [
    source 37
    target 1928
  ]
  edge [
    source 37
    target 1929
  ]
  edge [
    source 37
    target 1930
  ]
  edge [
    source 37
    target 1931
  ]
  edge [
    source 37
    target 238
  ]
  edge [
    source 37
    target 1932
  ]
  edge [
    source 37
    target 1933
  ]
  edge [
    source 37
    target 1934
  ]
  edge [
    source 37
    target 1935
  ]
  edge [
    source 37
    target 1936
  ]
  edge [
    source 37
    target 1937
  ]
  edge [
    source 37
    target 1938
  ]
  edge [
    source 37
    target 1939
  ]
  edge [
    source 37
    target 1645
  ]
  edge [
    source 37
    target 1125
  ]
  edge [
    source 37
    target 1940
  ]
  edge [
    source 37
    target 1941
  ]
  edge [
    source 37
    target 1099
  ]
  edge [
    source 37
    target 1942
  ]
  edge [
    source 37
    target 1943
  ]
  edge [
    source 37
    target 280
  ]
  edge [
    source 37
    target 1944
  ]
  edge [
    source 37
    target 390
  ]
  edge [
    source 37
    target 1945
  ]
  edge [
    source 37
    target 1946
  ]
  edge [
    source 37
    target 1947
  ]
  edge [
    source 37
    target 1948
  ]
  edge [
    source 37
    target 1949
  ]
  edge [
    source 37
    target 397
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 1950
  ]
  edge [
    source 37
    target 1023
  ]
  edge [
    source 37
    target 1951
  ]
  edge [
    source 37
    target 1952
  ]
  edge [
    source 37
    target 1953
  ]
  edge [
    source 37
    target 1954
  ]
  edge [
    source 37
    target 1955
  ]
  edge [
    source 37
    target 1956
  ]
  edge [
    source 37
    target 1957
  ]
  edge [
    source 37
    target 1958
  ]
  edge [
    source 37
    target 1959
  ]
  edge [
    source 37
    target 1960
  ]
  edge [
    source 37
    target 1961
  ]
  edge [
    source 37
    target 1962
  ]
  edge [
    source 37
    target 1963
  ]
  edge [
    source 37
    target 1964
  ]
  edge [
    source 37
    target 1965
  ]
  edge [
    source 37
    target 1966
  ]
  edge [
    source 37
    target 1967
  ]
  edge [
    source 37
    target 1968
  ]
  edge [
    source 37
    target 1969
  ]
  edge [
    source 37
    target 1970
  ]
  edge [
    source 37
    target 1971
  ]
  edge [
    source 37
    target 1972
  ]
  edge [
    source 37
    target 1973
  ]
  edge [
    source 37
    target 1974
  ]
  edge [
    source 37
    target 1179
  ]
  edge [
    source 37
    target 1975
  ]
  edge [
    source 37
    target 1976
  ]
  edge [
    source 37
    target 1977
  ]
  edge [
    source 37
    target 1978
  ]
  edge [
    source 37
    target 1979
  ]
  edge [
    source 37
    target 1537
  ]
  edge [
    source 37
    target 1980
  ]
  edge [
    source 37
    target 1981
  ]
  edge [
    source 37
    target 1982
  ]
  edge [
    source 37
    target 1983
  ]
  edge [
    source 37
    target 1984
  ]
  edge [
    source 37
    target 1985
  ]
  edge [
    source 37
    target 1986
  ]
  edge [
    source 37
    target 1987
  ]
  edge [
    source 37
    target 1988
  ]
  edge [
    source 37
    target 1989
  ]
  edge [
    source 37
    target 1990
  ]
  edge [
    source 37
    target 1991
  ]
  edge [
    source 37
    target 1992
  ]
  edge [
    source 37
    target 1993
  ]
  edge [
    source 37
    target 1994
  ]
  edge [
    source 37
    target 1399
  ]
  edge [
    source 37
    target 1995
  ]
  edge [
    source 37
    target 538
  ]
  edge [
    source 37
    target 1996
  ]
  edge [
    source 37
    target 1469
  ]
  edge [
    source 37
    target 1997
  ]
  edge [
    source 37
    target 1998
  ]
  edge [
    source 37
    target 1999
  ]
  edge [
    source 37
    target 2000
  ]
  edge [
    source 37
    target 2001
  ]
  edge [
    source 37
    target 2002
  ]
  edge [
    source 37
    target 2003
  ]
  edge [
    source 37
    target 2004
  ]
  edge [
    source 37
    target 2005
  ]
  edge [
    source 37
    target 2006
  ]
  edge [
    source 37
    target 2007
  ]
  edge [
    source 37
    target 2008
  ]
  edge [
    source 37
    target 2009
  ]
  edge [
    source 37
    target 2010
  ]
  edge [
    source 37
    target 2011
  ]
  edge [
    source 37
    target 2012
  ]
  edge [
    source 37
    target 2013
  ]
  edge [
    source 37
    target 2014
  ]
  edge [
    source 37
    target 2015
  ]
  edge [
    source 37
    target 2016
  ]
  edge [
    source 37
    target 2017
  ]
  edge [
    source 37
    target 2018
  ]
  edge [
    source 37
    target 2019
  ]
  edge [
    source 37
    target 2020
  ]
  edge [
    source 37
    target 2021
  ]
  edge [
    source 37
    target 2022
  ]
  edge [
    source 37
    target 2023
  ]
  edge [
    source 37
    target 2024
  ]
  edge [
    source 37
    target 2025
  ]
  edge [
    source 37
    target 2026
  ]
  edge [
    source 37
    target 2027
  ]
  edge [
    source 37
    target 2028
  ]
  edge [
    source 37
    target 2029
  ]
  edge [
    source 37
    target 2030
  ]
  edge [
    source 37
    target 2031
  ]
  edge [
    source 37
    target 2032
  ]
  edge [
    source 37
    target 2033
  ]
  edge [
    source 37
    target 2034
  ]
  edge [
    source 37
    target 2035
  ]
  edge [
    source 37
    target 2036
  ]
  edge [
    source 37
    target 2037
  ]
  edge [
    source 37
    target 2038
  ]
  edge [
    source 37
    target 2039
  ]
  edge [
    source 37
    target 2040
  ]
  edge [
    source 37
    target 2041
  ]
  edge [
    source 37
    target 2042
  ]
  edge [
    source 37
    target 2043
  ]
  edge [
    source 37
    target 2044
  ]
  edge [
    source 37
    target 2045
  ]
  edge [
    source 37
    target 2046
  ]
  edge [
    source 37
    target 2047
  ]
  edge [
    source 37
    target 2048
  ]
  edge [
    source 37
    target 2049
  ]
  edge [
    source 37
    target 2050
  ]
  edge [
    source 37
    target 2051
  ]
  edge [
    source 37
    target 2052
  ]
  edge [
    source 37
    target 2053
  ]
  edge [
    source 37
    target 2054
  ]
  edge [
    source 37
    target 2055
  ]
  edge [
    source 37
    target 2056
  ]
  edge [
    source 37
    target 2057
  ]
  edge [
    source 37
    target 2058
  ]
  edge [
    source 37
    target 2059
  ]
  edge [
    source 37
    target 2060
  ]
  edge [
    source 37
    target 2061
  ]
  edge [
    source 37
    target 2062
  ]
  edge [
    source 37
    target 2063
  ]
  edge [
    source 37
    target 2064
  ]
  edge [
    source 37
    target 2065
  ]
  edge [
    source 37
    target 2066
  ]
  edge [
    source 37
    target 2067
  ]
  edge [
    source 37
    target 2068
  ]
  edge [
    source 37
    target 2069
  ]
  edge [
    source 37
    target 2070
  ]
  edge [
    source 37
    target 2071
  ]
  edge [
    source 37
    target 2072
  ]
  edge [
    source 37
    target 2073
  ]
  edge [
    source 37
    target 2074
  ]
  edge [
    source 37
    target 2075
  ]
  edge [
    source 37
    target 2076
  ]
  edge [
    source 37
    target 2077
  ]
  edge [
    source 37
    target 2078
  ]
  edge [
    source 37
    target 2079
  ]
  edge [
    source 37
    target 2080
  ]
  edge [
    source 37
    target 2081
  ]
  edge [
    source 37
    target 2082
  ]
  edge [
    source 37
    target 2083
  ]
  edge [
    source 37
    target 2084
  ]
  edge [
    source 37
    target 2085
  ]
  edge [
    source 37
    target 2086
  ]
  edge [
    source 37
    target 2087
  ]
  edge [
    source 37
    target 2088
  ]
  edge [
    source 37
    target 2089
  ]
  edge [
    source 37
    target 2090
  ]
  edge [
    source 37
    target 2091
  ]
  edge [
    source 37
    target 2092
  ]
  edge [
    source 37
    target 2093
  ]
  edge [
    source 37
    target 2094
  ]
  edge [
    source 37
    target 2095
  ]
  edge [
    source 37
    target 2096
  ]
  edge [
    source 37
    target 2097
  ]
  edge [
    source 37
    target 2098
  ]
  edge [
    source 37
    target 2099
  ]
  edge [
    source 37
    target 2100
  ]
  edge [
    source 37
    target 2101
  ]
  edge [
    source 37
    target 2102
  ]
  edge [
    source 37
    target 2103
  ]
  edge [
    source 37
    target 2104
  ]
  edge [
    source 37
    target 2105
  ]
  edge [
    source 37
    target 2106
  ]
  edge [
    source 37
    target 2107
  ]
  edge [
    source 37
    target 2108
  ]
  edge [
    source 37
    target 2109
  ]
  edge [
    source 37
    target 2110
  ]
  edge [
    source 37
    target 2111
  ]
  edge [
    source 37
    target 2112
  ]
  edge [
    source 37
    target 2113
  ]
  edge [
    source 37
    target 2114
  ]
  edge [
    source 37
    target 2115
  ]
  edge [
    source 37
    target 2116
  ]
  edge [
    source 37
    target 2117
  ]
  edge [
    source 37
    target 2118
  ]
  edge [
    source 37
    target 2119
  ]
  edge [
    source 37
    target 2120
  ]
  edge [
    source 37
    target 2121
  ]
  edge [
    source 37
    target 2122
  ]
  edge [
    source 37
    target 2123
  ]
  edge [
    source 37
    target 2124
  ]
  edge [
    source 37
    target 2125
  ]
  edge [
    source 37
    target 2126
  ]
  edge [
    source 37
    target 2127
  ]
  edge [
    source 37
    target 2128
  ]
  edge [
    source 37
    target 2129
  ]
  edge [
    source 37
    target 2130
  ]
  edge [
    source 37
    target 2131
  ]
  edge [
    source 37
    target 225
  ]
  edge [
    source 37
    target 2132
  ]
  edge [
    source 37
    target 2133
  ]
  edge [
    source 37
    target 2134
  ]
  edge [
    source 37
    target 2135
  ]
  edge [
    source 37
    target 2136
  ]
  edge [
    source 37
    target 2137
  ]
  edge [
    source 37
    target 2138
  ]
  edge [
    source 37
    target 2139
  ]
  edge [
    source 37
    target 2140
  ]
  edge [
    source 37
    target 2141
  ]
  edge [
    source 37
    target 2142
  ]
  edge [
    source 37
    target 2143
  ]
  edge [
    source 37
    target 2144
  ]
  edge [
    source 37
    target 2145
  ]
  edge [
    source 37
    target 2146
  ]
  edge [
    source 37
    target 2147
  ]
  edge [
    source 37
    target 2148
  ]
  edge [
    source 37
    target 2149
  ]
  edge [
    source 37
    target 2150
  ]
  edge [
    source 37
    target 2151
  ]
  edge [
    source 37
    target 2152
  ]
  edge [
    source 37
    target 2153
  ]
  edge [
    source 37
    target 2154
  ]
  edge [
    source 37
    target 2155
  ]
  edge [
    source 37
    target 2156
  ]
  edge [
    source 37
    target 2157
  ]
  edge [
    source 37
    target 2158
  ]
  edge [
    source 37
    target 2159
  ]
  edge [
    source 37
    target 2160
  ]
  edge [
    source 37
    target 2161
  ]
  edge [
    source 37
    target 2162
  ]
  edge [
    source 37
    target 2163
  ]
  edge [
    source 37
    target 2164
  ]
  edge [
    source 37
    target 2165
  ]
  edge [
    source 37
    target 2166
  ]
  edge [
    source 37
    target 2167
  ]
  edge [
    source 37
    target 2168
  ]
  edge [
    source 37
    target 263
  ]
  edge [
    source 37
    target 2169
  ]
  edge [
    source 37
    target 2170
  ]
  edge [
    source 37
    target 2171
  ]
  edge [
    source 37
    target 2172
  ]
  edge [
    source 37
    target 2173
  ]
  edge [
    source 37
    target 2174
  ]
  edge [
    source 37
    target 2175
  ]
  edge [
    source 37
    target 2176
  ]
  edge [
    source 37
    target 2177
  ]
  edge [
    source 37
    target 2178
  ]
  edge [
    source 37
    target 2179
  ]
  edge [
    source 37
    target 2180
  ]
  edge [
    source 37
    target 2181
  ]
  edge [
    source 37
    target 2182
  ]
  edge [
    source 37
    target 2183
  ]
  edge [
    source 37
    target 2184
  ]
  edge [
    source 37
    target 2185
  ]
  edge [
    source 37
    target 2186
  ]
  edge [
    source 37
    target 2187
  ]
  edge [
    source 37
    target 2188
  ]
  edge [
    source 37
    target 2189
  ]
  edge [
    source 37
    target 2190
  ]
  edge [
    source 37
    target 2191
  ]
  edge [
    source 37
    target 2192
  ]
  edge [
    source 37
    target 2193
  ]
  edge [
    source 37
    target 2194
  ]
  edge [
    source 37
    target 2195
  ]
  edge [
    source 37
    target 2196
  ]
  edge [
    source 37
    target 2197
  ]
  edge [
    source 37
    target 2198
  ]
  edge [
    source 37
    target 2199
  ]
  edge [
    source 37
    target 2200
  ]
  edge [
    source 37
    target 2201
  ]
  edge [
    source 37
    target 2202
  ]
  edge [
    source 37
    target 2203
  ]
  edge [
    source 37
    target 2204
  ]
  edge [
    source 37
    target 2205
  ]
  edge [
    source 37
    target 2206
  ]
  edge [
    source 37
    target 2207
  ]
  edge [
    source 37
    target 2208
  ]
  edge [
    source 37
    target 2209
  ]
  edge [
    source 37
    target 2210
  ]
  edge [
    source 37
    target 2211
  ]
  edge [
    source 37
    target 2212
  ]
  edge [
    source 37
    target 2213
  ]
  edge [
    source 37
    target 2214
  ]
  edge [
    source 37
    target 2215
  ]
  edge [
    source 37
    target 2216
  ]
  edge [
    source 37
    target 2217
  ]
  edge [
    source 37
    target 2218
  ]
  edge [
    source 37
    target 2219
  ]
  edge [
    source 37
    target 2220
  ]
  edge [
    source 37
    target 2221
  ]
  edge [
    source 37
    target 2222
  ]
  edge [
    source 37
    target 2223
  ]
  edge [
    source 37
    target 2224
  ]
  edge [
    source 37
    target 2225
  ]
  edge [
    source 37
    target 2226
  ]
  edge [
    source 37
    target 2227
  ]
  edge [
    source 37
    target 2228
  ]
  edge [
    source 37
    target 2229
  ]
  edge [
    source 37
    target 2230
  ]
  edge [
    source 37
    target 2231
  ]
  edge [
    source 37
    target 2232
  ]
  edge [
    source 37
    target 2233
  ]
  edge [
    source 37
    target 2234
  ]
  edge [
    source 37
    target 2235
  ]
  edge [
    source 37
    target 2236
  ]
  edge [
    source 37
    target 2237
  ]
  edge [
    source 37
    target 2238
  ]
  edge [
    source 37
    target 2239
  ]
  edge [
    source 37
    target 2240
  ]
  edge [
    source 37
    target 2241
  ]
  edge [
    source 37
    target 2242
  ]
  edge [
    source 37
    target 2243
  ]
  edge [
    source 37
    target 2244
  ]
  edge [
    source 37
    target 2245
  ]
  edge [
    source 37
    target 2246
  ]
  edge [
    source 37
    target 2247
  ]
  edge [
    source 37
    target 2248
  ]
  edge [
    source 37
    target 2249
  ]
  edge [
    source 37
    target 2250
  ]
  edge [
    source 37
    target 2251
  ]
  edge [
    source 37
    target 2252
  ]
  edge [
    source 37
    target 2253
  ]
  edge [
    source 37
    target 2254
  ]
  edge [
    source 37
    target 2255
  ]
  edge [
    source 37
    target 2256
  ]
  edge [
    source 37
    target 2257
  ]
  edge [
    source 37
    target 2258
  ]
  edge [
    source 37
    target 2259
  ]
  edge [
    source 37
    target 235
  ]
  edge [
    source 37
    target 2260
  ]
  edge [
    source 37
    target 2261
  ]
  edge [
    source 37
    target 2262
  ]
  edge [
    source 37
    target 2263
  ]
  edge [
    source 37
    target 2264
  ]
  edge [
    source 37
    target 2265
  ]
  edge [
    source 37
    target 2266
  ]
  edge [
    source 37
    target 2267
  ]
  edge [
    source 37
    target 2268
  ]
  edge [
    source 37
    target 2269
  ]
  edge [
    source 37
    target 2270
  ]
  edge [
    source 37
    target 2271
  ]
  edge [
    source 37
    target 2272
  ]
  edge [
    source 37
    target 2273
  ]
  edge [
    source 37
    target 2274
  ]
  edge [
    source 37
    target 2275
  ]
  edge [
    source 37
    target 2276
  ]
  edge [
    source 37
    target 2277
  ]
  edge [
    source 37
    target 2278
  ]
  edge [
    source 37
    target 2279
  ]
  edge [
    source 37
    target 2280
  ]
  edge [
    source 37
    target 2281
  ]
  edge [
    source 37
    target 2282
  ]
  edge [
    source 37
    target 2283
  ]
  edge [
    source 37
    target 2284
  ]
  edge [
    source 37
    target 2285
  ]
  edge [
    source 37
    target 2286
  ]
  edge [
    source 37
    target 2287
  ]
  edge [
    source 37
    target 2288
  ]
  edge [
    source 37
    target 2289
  ]
  edge [
    source 37
    target 2290
  ]
  edge [
    source 37
    target 2291
  ]
  edge [
    source 37
    target 2292
  ]
  edge [
    source 37
    target 2293
  ]
  edge [
    source 37
    target 2294
  ]
  edge [
    source 37
    target 2295
  ]
  edge [
    source 37
    target 2296
  ]
  edge [
    source 37
    target 2297
  ]
  edge [
    source 37
    target 2298
  ]
  edge [
    source 37
    target 2299
  ]
  edge [
    source 37
    target 2300
  ]
  edge [
    source 37
    target 2301
  ]
  edge [
    source 37
    target 2302
  ]
  edge [
    source 37
    target 2303
  ]
  edge [
    source 37
    target 2304
  ]
  edge [
    source 37
    target 2305
  ]
  edge [
    source 37
    target 2306
  ]
  edge [
    source 37
    target 2307
  ]
  edge [
    source 37
    target 2308
  ]
  edge [
    source 37
    target 2309
  ]
  edge [
    source 37
    target 2310
  ]
  edge [
    source 37
    target 2311
  ]
  edge [
    source 37
    target 2312
  ]
  edge [
    source 37
    target 2313
  ]
  edge [
    source 37
    target 2314
  ]
  edge [
    source 37
    target 2315
  ]
  edge [
    source 37
    target 2316
  ]
  edge [
    source 37
    target 2317
  ]
  edge [
    source 37
    target 2318
  ]
  edge [
    source 37
    target 2319
  ]
  edge [
    source 37
    target 2320
  ]
  edge [
    source 37
    target 2321
  ]
  edge [
    source 37
    target 2322
  ]
  edge [
    source 37
    target 2323
  ]
  edge [
    source 37
    target 2324
  ]
  edge [
    source 37
    target 2325
  ]
  edge [
    source 37
    target 2326
  ]
  edge [
    source 37
    target 2327
  ]
  edge [
    source 37
    target 399
  ]
  edge [
    source 37
    target 2328
  ]
  edge [
    source 37
    target 2329
  ]
  edge [
    source 37
    target 2330
  ]
  edge [
    source 37
    target 2331
  ]
  edge [
    source 37
    target 2332
  ]
  edge [
    source 37
    target 2333
  ]
  edge [
    source 37
    target 2334
  ]
  edge [
    source 37
    target 2335
  ]
  edge [
    source 37
    target 2336
  ]
  edge [
    source 37
    target 2337
  ]
  edge [
    source 37
    target 2338
  ]
  edge [
    source 37
    target 2339
  ]
  edge [
    source 37
    target 2340
  ]
  edge [
    source 37
    target 2341
  ]
  edge [
    source 37
    target 2342
  ]
  edge [
    source 37
    target 2343
  ]
  edge [
    source 37
    target 2344
  ]
  edge [
    source 37
    target 2345
  ]
  edge [
    source 37
    target 2346
  ]
  edge [
    source 37
    target 2347
  ]
  edge [
    source 37
    target 2348
  ]
  edge [
    source 37
    target 2349
  ]
  edge [
    source 37
    target 2350
  ]
  edge [
    source 37
    target 2351
  ]
  edge [
    source 37
    target 2352
  ]
  edge [
    source 37
    target 2353
  ]
  edge [
    source 37
    target 2354
  ]
  edge [
    source 37
    target 2355
  ]
  edge [
    source 37
    target 2356
  ]
  edge [
    source 37
    target 2357
  ]
  edge [
    source 37
    target 2358
  ]
  edge [
    source 37
    target 2359
  ]
  edge [
    source 37
    target 2360
  ]
  edge [
    source 37
    target 2361
  ]
  edge [
    source 37
    target 2362
  ]
  edge [
    source 37
    target 2363
  ]
  edge [
    source 37
    target 2364
  ]
  edge [
    source 37
    target 2365
  ]
  edge [
    source 37
    target 2366
  ]
  edge [
    source 37
    target 2367
  ]
  edge [
    source 37
    target 2368
  ]
  edge [
    source 37
    target 2369
  ]
  edge [
    source 37
    target 2370
  ]
  edge [
    source 37
    target 2371
  ]
  edge [
    source 37
    target 2372
  ]
  edge [
    source 37
    target 2373
  ]
  edge [
    source 37
    target 2374
  ]
  edge [
    source 37
    target 2375
  ]
  edge [
    source 37
    target 2376
  ]
  edge [
    source 37
    target 2377
  ]
  edge [
    source 37
    target 2378
  ]
  edge [
    source 37
    target 217
  ]
  edge [
    source 37
    target 2379
  ]
  edge [
    source 37
    target 219
  ]
  edge [
    source 37
    target 222
  ]
  edge [
    source 37
    target 2380
  ]
  edge [
    source 37
    target 226
  ]
  edge [
    source 37
    target 227
  ]
  edge [
    source 37
    target 229
  ]
  edge [
    source 37
    target 231
  ]
  edge [
    source 37
    target 2381
  ]
  edge [
    source 37
    target 233
  ]
  edge [
    source 37
    target 234
  ]
  edge [
    source 37
    target 236
  ]
  edge [
    source 37
    target 237
  ]
  edge [
    source 37
    target 239
  ]
  edge [
    source 37
    target 240
  ]
  edge [
    source 37
    target 242
  ]
  edge [
    source 37
    target 2382
  ]
  edge [
    source 37
    target 2383
  ]
  edge [
    source 37
    target 244
  ]
  edge [
    source 37
    target 246
  ]
  edge [
    source 37
    target 247
  ]
  edge [
    source 37
    target 2384
  ]
  edge [
    source 37
    target 248
  ]
  edge [
    source 37
    target 250
  ]
  edge [
    source 37
    target 251
  ]
  edge [
    source 37
    target 2385
  ]
  edge [
    source 37
    target 252
  ]
  edge [
    source 37
    target 253
  ]
  edge [
    source 37
    target 2386
  ]
  edge [
    source 37
    target 2387
  ]
  edge [
    source 37
    target 2388
  ]
  edge [
    source 37
    target 2389
  ]
  edge [
    source 37
    target 2390
  ]
  edge [
    source 37
    target 2391
  ]
  edge [
    source 37
    target 2392
  ]
  edge [
    source 37
    target 2393
  ]
  edge [
    source 37
    target 2394
  ]
  edge [
    source 37
    target 2395
  ]
  edge [
    source 37
    target 2396
  ]
  edge [
    source 37
    target 2397
  ]
  edge [
    source 37
    target 2398
  ]
  edge [
    source 37
    target 2399
  ]
  edge [
    source 37
    target 2400
  ]
  edge [
    source 37
    target 2401
  ]
  edge [
    source 37
    target 2402
  ]
  edge [
    source 37
    target 2403
  ]
  edge [
    source 37
    target 2404
  ]
  edge [
    source 37
    target 2405
  ]
  edge [
    source 37
    target 2406
  ]
  edge [
    source 37
    target 2407
  ]
  edge [
    source 37
    target 2408
  ]
  edge [
    source 37
    target 2409
  ]
  edge [
    source 37
    target 2410
  ]
  edge [
    source 37
    target 2411
  ]
  edge [
    source 37
    target 2412
  ]
  edge [
    source 37
    target 2413
  ]
  edge [
    source 37
    target 2414
  ]
  edge [
    source 37
    target 2415
  ]
  edge [
    source 37
    target 2416
  ]
  edge [
    source 37
    target 2417
  ]
  edge [
    source 37
    target 2418
  ]
  edge [
    source 37
    target 2419
  ]
  edge [
    source 37
    target 2420
  ]
  edge [
    source 37
    target 2421
  ]
  edge [
    source 37
    target 2422
  ]
  edge [
    source 37
    target 2423
  ]
  edge [
    source 37
    target 2424
  ]
  edge [
    source 37
    target 2425
  ]
  edge [
    source 37
    target 2426
  ]
  edge [
    source 37
    target 2427
  ]
  edge [
    source 37
    target 2428
  ]
  edge [
    source 37
    target 2429
  ]
  edge [
    source 37
    target 2430
  ]
  edge [
    source 37
    target 2431
  ]
  edge [
    source 37
    target 2432
  ]
  edge [
    source 37
    target 2433
  ]
  edge [
    source 37
    target 2434
  ]
  edge [
    source 37
    target 2435
  ]
  edge [
    source 37
    target 2436
  ]
  edge [
    source 37
    target 2437
  ]
  edge [
    source 37
    target 2438
  ]
  edge [
    source 37
    target 2439
  ]
  edge [
    source 37
    target 2440
  ]
  edge [
    source 37
    target 2441
  ]
  edge [
    source 37
    target 2442
  ]
  edge [
    source 37
    target 2443
  ]
  edge [
    source 37
    target 2444
  ]
  edge [
    source 37
    target 2445
  ]
  edge [
    source 37
    target 2446
  ]
  edge [
    source 37
    target 2447
  ]
  edge [
    source 37
    target 2448
  ]
  edge [
    source 37
    target 2449
  ]
  edge [
    source 37
    target 2450
  ]
  edge [
    source 37
    target 2451
  ]
  edge [
    source 37
    target 2452
  ]
  edge [
    source 37
    target 2453
  ]
  edge [
    source 37
    target 2454
  ]
  edge [
    source 37
    target 2455
  ]
  edge [
    source 37
    target 2456
  ]
  edge [
    source 37
    target 2457
  ]
  edge [
    source 37
    target 2458
  ]
  edge [
    source 37
    target 2459
  ]
  edge [
    source 37
    target 2460
  ]
  edge [
    source 37
    target 2461
  ]
  edge [
    source 37
    target 2462
  ]
  edge [
    source 37
    target 2463
  ]
  edge [
    source 37
    target 2464
  ]
  edge [
    source 37
    target 2465
  ]
  edge [
    source 37
    target 2466
  ]
  edge [
    source 37
    target 2467
  ]
  edge [
    source 37
    target 2468
  ]
  edge [
    source 37
    target 2469
  ]
  edge [
    source 37
    target 2470
  ]
  edge [
    source 37
    target 2471
  ]
  edge [
    source 37
    target 2472
  ]
  edge [
    source 37
    target 2473
  ]
  edge [
    source 37
    target 2474
  ]
  edge [
    source 37
    target 2475
  ]
  edge [
    source 37
    target 2476
  ]
  edge [
    source 37
    target 2477
  ]
  edge [
    source 37
    target 2478
  ]
  edge [
    source 37
    target 2479
  ]
  edge [
    source 37
    target 2480
  ]
  edge [
    source 37
    target 2481
  ]
  edge [
    source 37
    target 2482
  ]
  edge [
    source 37
    target 2483
  ]
  edge [
    source 37
    target 2484
  ]
  edge [
    source 37
    target 2485
  ]
  edge [
    source 37
    target 2486
  ]
  edge [
    source 37
    target 2487
  ]
  edge [
    source 37
    target 2488
  ]
  edge [
    source 37
    target 1353
  ]
  edge [
    source 37
    target 2489
  ]
  edge [
    source 37
    target 2490
  ]
  edge [
    source 37
    target 2491
  ]
  edge [
    source 37
    target 2492
  ]
  edge [
    source 37
    target 2493
  ]
  edge [
    source 37
    target 2494
  ]
  edge [
    source 37
    target 2495
  ]
  edge [
    source 37
    target 2496
  ]
  edge [
    source 37
    target 2497
  ]
  edge [
    source 37
    target 2498
  ]
  edge [
    source 37
    target 2499
  ]
  edge [
    source 37
    target 2500
  ]
  edge [
    source 37
    target 2501
  ]
  edge [
    source 37
    target 2502
  ]
  edge [
    source 37
    target 2503
  ]
  edge [
    source 37
    target 2504
  ]
  edge [
    source 37
    target 2505
  ]
  edge [
    source 37
    target 2506
  ]
  edge [
    source 37
    target 232
  ]
  edge [
    source 37
    target 2507
  ]
  edge [
    source 37
    target 2508
  ]
  edge [
    source 37
    target 2509
  ]
  edge [
    source 37
    target 54
  ]
  edge [
    source 37
    target 2510
  ]
  edge [
    source 37
    target 2511
  ]
  edge [
    source 37
    target 2512
  ]
  edge [
    source 37
    target 2513
  ]
  edge [
    source 37
    target 2514
  ]
  edge [
    source 37
    target 1349
  ]
  edge [
    source 37
    target 2515
  ]
  edge [
    source 37
    target 2516
  ]
  edge [
    source 37
    target 596
  ]
  edge [
    source 37
    target 2517
  ]
  edge [
    source 37
    target 2518
  ]
  edge [
    source 37
    target 2519
  ]
  edge [
    source 37
    target 2520
  ]
  edge [
    source 37
    target 2521
  ]
  edge [
    source 37
    target 1490
  ]
  edge [
    source 37
    target 2522
  ]
  edge [
    source 37
    target 2523
  ]
  edge [
    source 37
    target 2524
  ]
  edge [
    source 37
    target 2525
  ]
  edge [
    source 37
    target 2526
  ]
  edge [
    source 37
    target 2527
  ]
  edge [
    source 37
    target 2528
  ]
  edge [
    source 37
    target 2529
  ]
  edge [
    source 37
    target 2530
  ]
  edge [
    source 37
    target 2531
  ]
  edge [
    source 37
    target 2532
  ]
  edge [
    source 37
    target 2533
  ]
  edge [
    source 37
    target 2534
  ]
  edge [
    source 37
    target 2535
  ]
  edge [
    source 37
    target 2536
  ]
  edge [
    source 37
    target 2537
  ]
  edge [
    source 37
    target 2538
  ]
  edge [
    source 37
    target 2539
  ]
  edge [
    source 37
    target 2540
  ]
  edge [
    source 37
    target 1010
  ]
  edge [
    source 37
    target 2541
  ]
  edge [
    source 38
    target 2542
  ]
  edge [
    source 38
    target 2543
  ]
  edge [
    source 38
    target 2544
  ]
  edge [
    source 38
    target 2545
  ]
  edge [
    source 38
    target 2546
  ]
  edge [
    source 38
    target 2547
  ]
  edge [
    source 38
    target 2548
  ]
  edge [
    source 38
    target 2549
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 38
    target 309
  ]
  edge [
    source 38
    target 2550
  ]
  edge [
    source 38
    target 2551
  ]
  edge [
    source 38
    target 2552
  ]
  edge [
    source 38
    target 2553
  ]
  edge [
    source 38
    target 2554
  ]
  edge [
    source 38
    target 2555
  ]
  edge [
    source 38
    target 2556
  ]
  edge [
    source 38
    target 2557
  ]
  edge [
    source 38
    target 2558
  ]
  edge [
    source 38
    target 2559
  ]
  edge [
    source 38
    target 2560
  ]
  edge [
    source 38
    target 1067
  ]
]
