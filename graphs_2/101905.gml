graph [
  node [
    id 0
    label "wynik"
    origin "text"
  ]
  node [
    id 1
    label "etap"
    origin "text"
  ]
  node [
    id 2
    label "okre&#347;la&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wymagania"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "informatyczny"
    origin "text"
  ]
  node [
    id 6
    label "zaokr&#261;glenie"
  ]
  node [
    id 7
    label "dzia&#322;anie"
  ]
  node [
    id 8
    label "typ"
  ]
  node [
    id 9
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 10
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 11
    label "rezultat"
  ]
  node [
    id 12
    label "event"
  ]
  node [
    id 13
    label "przyczyna"
  ]
  node [
    id 14
    label "przybli&#380;enie"
  ]
  node [
    id 15
    label "rounding"
  ]
  node [
    id 16
    label "liczenie"
  ]
  node [
    id 17
    label "zaokr&#261;glenie_si&#281;"
  ]
  node [
    id 18
    label "okr&#261;g&#322;y"
  ]
  node [
    id 19
    label "zaokr&#261;glony"
  ]
  node [
    id 20
    label "element"
  ]
  node [
    id 21
    label "ukszta&#322;towanie"
  ]
  node [
    id 22
    label "labializacja"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "zrobienie"
  ]
  node [
    id 25
    label "round"
  ]
  node [
    id 26
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 27
    label "zrobi&#263;"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "facet"
  ]
  node [
    id 30
    label "jednostka_systematyczna"
  ]
  node [
    id 31
    label "kr&#243;lestwo"
  ]
  node [
    id 32
    label "autorament"
  ]
  node [
    id 33
    label "variety"
  ]
  node [
    id 34
    label "antycypacja"
  ]
  node [
    id 35
    label "przypuszczenie"
  ]
  node [
    id 36
    label "cynk"
  ]
  node [
    id 37
    label "obstawia&#263;"
  ]
  node [
    id 38
    label "gromada"
  ]
  node [
    id 39
    label "sztuka"
  ]
  node [
    id 40
    label "design"
  ]
  node [
    id 41
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 42
    label "subject"
  ]
  node [
    id 43
    label "czynnik"
  ]
  node [
    id 44
    label "matuszka"
  ]
  node [
    id 45
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 46
    label "geneza"
  ]
  node [
    id 47
    label "poci&#261;ganie"
  ]
  node [
    id 48
    label "infimum"
  ]
  node [
    id 49
    label "powodowanie"
  ]
  node [
    id 50
    label "skutek"
  ]
  node [
    id 51
    label "podzia&#322;anie"
  ]
  node [
    id 52
    label "supremum"
  ]
  node [
    id 53
    label "kampania"
  ]
  node [
    id 54
    label "uruchamianie"
  ]
  node [
    id 55
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 56
    label "operacja"
  ]
  node [
    id 57
    label "jednostka"
  ]
  node [
    id 58
    label "hipnotyzowanie"
  ]
  node [
    id 59
    label "robienie"
  ]
  node [
    id 60
    label "uruchomienie"
  ]
  node [
    id 61
    label "nakr&#281;canie"
  ]
  node [
    id 62
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 63
    label "matematyka"
  ]
  node [
    id 64
    label "reakcja_chemiczna"
  ]
  node [
    id 65
    label "tr&#243;jstronny"
  ]
  node [
    id 66
    label "natural_process"
  ]
  node [
    id 67
    label "nakr&#281;cenie"
  ]
  node [
    id 68
    label "zatrzymanie"
  ]
  node [
    id 69
    label "wp&#322;yw"
  ]
  node [
    id 70
    label "rzut"
  ]
  node [
    id 71
    label "podtrzymywanie"
  ]
  node [
    id 72
    label "w&#322;&#261;czanie"
  ]
  node [
    id 73
    label "liczy&#263;"
  ]
  node [
    id 74
    label "operation"
  ]
  node [
    id 75
    label "dzianie_si&#281;"
  ]
  node [
    id 76
    label "zadzia&#322;anie"
  ]
  node [
    id 77
    label "priorytet"
  ]
  node [
    id 78
    label "bycie"
  ]
  node [
    id 79
    label "kres"
  ]
  node [
    id 80
    label "rozpocz&#281;cie"
  ]
  node [
    id 81
    label "docieranie"
  ]
  node [
    id 82
    label "funkcja"
  ]
  node [
    id 83
    label "czynny"
  ]
  node [
    id 84
    label "impact"
  ]
  node [
    id 85
    label "oferta"
  ]
  node [
    id 86
    label "zako&#324;czenie"
  ]
  node [
    id 87
    label "act"
  ]
  node [
    id 88
    label "wdzieranie_si&#281;"
  ]
  node [
    id 89
    label "w&#322;&#261;czenie"
  ]
  node [
    id 90
    label "wydarzenie"
  ]
  node [
    id 91
    label "miejsce"
  ]
  node [
    id 92
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 93
    label "odcinek"
  ]
  node [
    id 94
    label "czas"
  ]
  node [
    id 95
    label "warunek_lokalowy"
  ]
  node [
    id 96
    label "plac"
  ]
  node [
    id 97
    label "location"
  ]
  node [
    id 98
    label "uwaga"
  ]
  node [
    id 99
    label "przestrze&#324;"
  ]
  node [
    id 100
    label "status"
  ]
  node [
    id 101
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 102
    label "chwila"
  ]
  node [
    id 103
    label "cia&#322;o"
  ]
  node [
    id 104
    label "cecha"
  ]
  node [
    id 105
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 106
    label "praca"
  ]
  node [
    id 107
    label "rz&#261;d"
  ]
  node [
    id 108
    label "poprzedzanie"
  ]
  node [
    id 109
    label "czasoprzestrze&#324;"
  ]
  node [
    id 110
    label "laba"
  ]
  node [
    id 111
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 112
    label "chronometria"
  ]
  node [
    id 113
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 114
    label "rachuba_czasu"
  ]
  node [
    id 115
    label "przep&#322;ywanie"
  ]
  node [
    id 116
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 117
    label "czasokres"
  ]
  node [
    id 118
    label "odczyt"
  ]
  node [
    id 119
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 120
    label "dzieje"
  ]
  node [
    id 121
    label "kategoria_gramatyczna"
  ]
  node [
    id 122
    label "poprzedzenie"
  ]
  node [
    id 123
    label "trawienie"
  ]
  node [
    id 124
    label "pochodzi&#263;"
  ]
  node [
    id 125
    label "period"
  ]
  node [
    id 126
    label "okres_czasu"
  ]
  node [
    id 127
    label "poprzedza&#263;"
  ]
  node [
    id 128
    label "schy&#322;ek"
  ]
  node [
    id 129
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 130
    label "odwlekanie_si&#281;"
  ]
  node [
    id 131
    label "zegar"
  ]
  node [
    id 132
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 133
    label "czwarty_wymiar"
  ]
  node [
    id 134
    label "pochodzenie"
  ]
  node [
    id 135
    label "koniugacja"
  ]
  node [
    id 136
    label "Zeitgeist"
  ]
  node [
    id 137
    label "trawi&#263;"
  ]
  node [
    id 138
    label "pogoda"
  ]
  node [
    id 139
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 140
    label "poprzedzi&#263;"
  ]
  node [
    id 141
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 142
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 143
    label "time_period"
  ]
  node [
    id 144
    label "teren"
  ]
  node [
    id 145
    label "pole"
  ]
  node [
    id 146
    label "kawa&#322;ek"
  ]
  node [
    id 147
    label "part"
  ]
  node [
    id 148
    label "line"
  ]
  node [
    id 149
    label "coupon"
  ]
  node [
    id 150
    label "fragment"
  ]
  node [
    id 151
    label "pokwitowanie"
  ]
  node [
    id 152
    label "moneta"
  ]
  node [
    id 153
    label "epizod"
  ]
  node [
    id 154
    label "decydowa&#263;"
  ]
  node [
    id 155
    label "signify"
  ]
  node [
    id 156
    label "style"
  ]
  node [
    id 157
    label "powodowa&#263;"
  ]
  node [
    id 158
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 159
    label "decide"
  ]
  node [
    id 160
    label "klasyfikator"
  ]
  node [
    id 161
    label "mean"
  ]
  node [
    id 162
    label "mie&#263;_miejsce"
  ]
  node [
    id 163
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 164
    label "motywowa&#263;"
  ]
  node [
    id 165
    label "j&#261;dro"
  ]
  node [
    id 166
    label "systemik"
  ]
  node [
    id 167
    label "rozprz&#261;c"
  ]
  node [
    id 168
    label "oprogramowanie"
  ]
  node [
    id 169
    label "poj&#281;cie"
  ]
  node [
    id 170
    label "systemat"
  ]
  node [
    id 171
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 172
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 173
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 174
    label "model"
  ]
  node [
    id 175
    label "struktura"
  ]
  node [
    id 176
    label "usenet"
  ]
  node [
    id 177
    label "s&#261;d"
  ]
  node [
    id 178
    label "zbi&#243;r"
  ]
  node [
    id 179
    label "porz&#261;dek"
  ]
  node [
    id 180
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 181
    label "przyn&#281;ta"
  ]
  node [
    id 182
    label "p&#322;&#243;d"
  ]
  node [
    id 183
    label "net"
  ]
  node [
    id 184
    label "w&#281;dkarstwo"
  ]
  node [
    id 185
    label "eratem"
  ]
  node [
    id 186
    label "oddzia&#322;"
  ]
  node [
    id 187
    label "doktryna"
  ]
  node [
    id 188
    label "pulpit"
  ]
  node [
    id 189
    label "konstelacja"
  ]
  node [
    id 190
    label "jednostka_geologiczna"
  ]
  node [
    id 191
    label "o&#347;"
  ]
  node [
    id 192
    label "podsystem"
  ]
  node [
    id 193
    label "metoda"
  ]
  node [
    id 194
    label "ryba"
  ]
  node [
    id 195
    label "Leopard"
  ]
  node [
    id 196
    label "spos&#243;b"
  ]
  node [
    id 197
    label "Android"
  ]
  node [
    id 198
    label "zachowanie"
  ]
  node [
    id 199
    label "cybernetyk"
  ]
  node [
    id 200
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 201
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 202
    label "method"
  ]
  node [
    id 203
    label "sk&#322;ad"
  ]
  node [
    id 204
    label "podstawa"
  ]
  node [
    id 205
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 206
    label "pot&#281;ga"
  ]
  node [
    id 207
    label "documentation"
  ]
  node [
    id 208
    label "przedmiot"
  ]
  node [
    id 209
    label "column"
  ]
  node [
    id 210
    label "zasadzi&#263;"
  ]
  node [
    id 211
    label "za&#322;o&#380;enie"
  ]
  node [
    id 212
    label "punkt_odniesienia"
  ]
  node [
    id 213
    label "zasadzenie"
  ]
  node [
    id 214
    label "bok"
  ]
  node [
    id 215
    label "d&#243;&#322;"
  ]
  node [
    id 216
    label "dzieci&#281;ctwo"
  ]
  node [
    id 217
    label "background"
  ]
  node [
    id 218
    label "podstawowy"
  ]
  node [
    id 219
    label "strategia"
  ]
  node [
    id 220
    label "pomys&#322;"
  ]
  node [
    id 221
    label "&#347;ciana"
  ]
  node [
    id 222
    label "narz&#281;dzie"
  ]
  node [
    id 223
    label "tryb"
  ]
  node [
    id 224
    label "nature"
  ]
  node [
    id 225
    label "relacja"
  ]
  node [
    id 226
    label "uk&#322;ad"
  ]
  node [
    id 227
    label "stan"
  ]
  node [
    id 228
    label "zasada"
  ]
  node [
    id 229
    label "styl_architektoniczny"
  ]
  node [
    id 230
    label "normalizacja"
  ]
  node [
    id 231
    label "egzemplarz"
  ]
  node [
    id 232
    label "series"
  ]
  node [
    id 233
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 234
    label "uprawianie"
  ]
  node [
    id 235
    label "praca_rolnicza"
  ]
  node [
    id 236
    label "collection"
  ]
  node [
    id 237
    label "dane"
  ]
  node [
    id 238
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 239
    label "pakiet_klimatyczny"
  ]
  node [
    id 240
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 241
    label "sum"
  ]
  node [
    id 242
    label "gathering"
  ]
  node [
    id 243
    label "album"
  ]
  node [
    id 244
    label "pos&#322;uchanie"
  ]
  node [
    id 245
    label "skumanie"
  ]
  node [
    id 246
    label "orientacja"
  ]
  node [
    id 247
    label "wytw&#243;r"
  ]
  node [
    id 248
    label "zorientowanie"
  ]
  node [
    id 249
    label "teoria"
  ]
  node [
    id 250
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 251
    label "clasp"
  ]
  node [
    id 252
    label "forma"
  ]
  node [
    id 253
    label "przem&#243;wienie"
  ]
  node [
    id 254
    label "mechanika"
  ]
  node [
    id 255
    label "konstrukcja"
  ]
  node [
    id 256
    label "system_komputerowy"
  ]
  node [
    id 257
    label "sprz&#281;t"
  ]
  node [
    id 258
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 259
    label "moczownik"
  ]
  node [
    id 260
    label "embryo"
  ]
  node [
    id 261
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 262
    label "zarodek"
  ]
  node [
    id 263
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 264
    label "latawiec"
  ]
  node [
    id 265
    label "reengineering"
  ]
  node [
    id 266
    label "program"
  ]
  node [
    id 267
    label "integer"
  ]
  node [
    id 268
    label "liczba"
  ]
  node [
    id 269
    label "zlewanie_si&#281;"
  ]
  node [
    id 270
    label "ilo&#347;&#263;"
  ]
  node [
    id 271
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 272
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 273
    label "pe&#322;ny"
  ]
  node [
    id 274
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 275
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 276
    label "grupa_dyskusyjna"
  ]
  node [
    id 277
    label "doctrine"
  ]
  node [
    id 278
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 279
    label "kr&#281;gowiec"
  ]
  node [
    id 280
    label "doniczkowiec"
  ]
  node [
    id 281
    label "mi&#281;so"
  ]
  node [
    id 282
    label "patroszy&#263;"
  ]
  node [
    id 283
    label "rakowato&#347;&#263;"
  ]
  node [
    id 284
    label "ryby"
  ]
  node [
    id 285
    label "fish"
  ]
  node [
    id 286
    label "linia_boczna"
  ]
  node [
    id 287
    label "tar&#322;o"
  ]
  node [
    id 288
    label "wyrostek_filtracyjny"
  ]
  node [
    id 289
    label "m&#281;tnooki"
  ]
  node [
    id 290
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 291
    label "pokrywa_skrzelowa"
  ]
  node [
    id 292
    label "ikra"
  ]
  node [
    id 293
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 294
    label "szczelina_skrzelowa"
  ]
  node [
    id 295
    label "sport"
  ]
  node [
    id 296
    label "urozmaicenie"
  ]
  node [
    id 297
    label "pu&#322;apka"
  ]
  node [
    id 298
    label "pon&#281;ta"
  ]
  node [
    id 299
    label "wabik"
  ]
  node [
    id 300
    label "blat"
  ]
  node [
    id 301
    label "interfejs"
  ]
  node [
    id 302
    label "okno"
  ]
  node [
    id 303
    label "obszar"
  ]
  node [
    id 304
    label "ikona"
  ]
  node [
    id 305
    label "system_operacyjny"
  ]
  node [
    id 306
    label "mebel"
  ]
  node [
    id 307
    label "zdolno&#347;&#263;"
  ]
  node [
    id 308
    label "oswobodzi&#263;"
  ]
  node [
    id 309
    label "os&#322;abi&#263;"
  ]
  node [
    id 310
    label "disengage"
  ]
  node [
    id 311
    label "zdezorganizowa&#263;"
  ]
  node [
    id 312
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 313
    label "reakcja"
  ]
  node [
    id 314
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 315
    label "tajemnica"
  ]
  node [
    id 316
    label "pochowanie"
  ]
  node [
    id 317
    label "zdyscyplinowanie"
  ]
  node [
    id 318
    label "post&#261;pienie"
  ]
  node [
    id 319
    label "post"
  ]
  node [
    id 320
    label "bearing"
  ]
  node [
    id 321
    label "zwierz&#281;"
  ]
  node [
    id 322
    label "behawior"
  ]
  node [
    id 323
    label "observation"
  ]
  node [
    id 324
    label "dieta"
  ]
  node [
    id 325
    label "podtrzymanie"
  ]
  node [
    id 326
    label "etolog"
  ]
  node [
    id 327
    label "przechowanie"
  ]
  node [
    id 328
    label "relaxation"
  ]
  node [
    id 329
    label "os&#322;abienie"
  ]
  node [
    id 330
    label "oswobodzenie"
  ]
  node [
    id 331
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 332
    label "zdezorganizowanie"
  ]
  node [
    id 333
    label "naukowiec"
  ]
  node [
    id 334
    label "provider"
  ]
  node [
    id 335
    label "b&#322;&#261;d"
  ]
  node [
    id 336
    label "hipertekst"
  ]
  node [
    id 337
    label "cyberprzestrze&#324;"
  ]
  node [
    id 338
    label "mem"
  ]
  node [
    id 339
    label "gra_sieciowa"
  ]
  node [
    id 340
    label "grooming"
  ]
  node [
    id 341
    label "media"
  ]
  node [
    id 342
    label "biznes_elektroniczny"
  ]
  node [
    id 343
    label "sie&#263;_komputerowa"
  ]
  node [
    id 344
    label "punkt_dost&#281;pu"
  ]
  node [
    id 345
    label "us&#322;uga_internetowa"
  ]
  node [
    id 346
    label "netbook"
  ]
  node [
    id 347
    label "e-hazard"
  ]
  node [
    id 348
    label "podcast"
  ]
  node [
    id 349
    label "strona"
  ]
  node [
    id 350
    label "prezenter"
  ]
  node [
    id 351
    label "mildew"
  ]
  node [
    id 352
    label "zi&#243;&#322;ko"
  ]
  node [
    id 353
    label "motif"
  ]
  node [
    id 354
    label "pozowanie"
  ]
  node [
    id 355
    label "ideal"
  ]
  node [
    id 356
    label "wz&#243;r"
  ]
  node [
    id 357
    label "matryca"
  ]
  node [
    id 358
    label "adaptation"
  ]
  node [
    id 359
    label "ruch"
  ]
  node [
    id 360
    label "pozowa&#263;"
  ]
  node [
    id 361
    label "imitacja"
  ]
  node [
    id 362
    label "orygina&#322;"
  ]
  node [
    id 363
    label "miniatura"
  ]
  node [
    id 364
    label "zesp&#243;&#322;"
  ]
  node [
    id 365
    label "podejrzany"
  ]
  node [
    id 366
    label "s&#261;downictwo"
  ]
  node [
    id 367
    label "biuro"
  ]
  node [
    id 368
    label "court"
  ]
  node [
    id 369
    label "forum"
  ]
  node [
    id 370
    label "bronienie"
  ]
  node [
    id 371
    label "urz&#261;d"
  ]
  node [
    id 372
    label "oskar&#380;yciel"
  ]
  node [
    id 373
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 374
    label "skazany"
  ]
  node [
    id 375
    label "post&#281;powanie"
  ]
  node [
    id 376
    label "broni&#263;"
  ]
  node [
    id 377
    label "my&#347;l"
  ]
  node [
    id 378
    label "pods&#261;dny"
  ]
  node [
    id 379
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 380
    label "obrona"
  ]
  node [
    id 381
    label "wypowied&#378;"
  ]
  node [
    id 382
    label "instytucja"
  ]
  node [
    id 383
    label "antylogizm"
  ]
  node [
    id 384
    label "konektyw"
  ]
  node [
    id 385
    label "&#347;wiadek"
  ]
  node [
    id 386
    label "procesowicz"
  ]
  node [
    id 387
    label "lias"
  ]
  node [
    id 388
    label "dzia&#322;"
  ]
  node [
    id 389
    label "pi&#281;tro"
  ]
  node [
    id 390
    label "klasa"
  ]
  node [
    id 391
    label "filia"
  ]
  node [
    id 392
    label "malm"
  ]
  node [
    id 393
    label "whole"
  ]
  node [
    id 394
    label "dogger"
  ]
  node [
    id 395
    label "poziom"
  ]
  node [
    id 396
    label "promocja"
  ]
  node [
    id 397
    label "kurs"
  ]
  node [
    id 398
    label "bank"
  ]
  node [
    id 399
    label "formacja"
  ]
  node [
    id 400
    label "ajencja"
  ]
  node [
    id 401
    label "wojsko"
  ]
  node [
    id 402
    label "siedziba"
  ]
  node [
    id 403
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 404
    label "agencja"
  ]
  node [
    id 405
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 406
    label "szpital"
  ]
  node [
    id 407
    label "algebra_liniowa"
  ]
  node [
    id 408
    label "macierz_j&#261;drowa"
  ]
  node [
    id 409
    label "atom"
  ]
  node [
    id 410
    label "nukleon"
  ]
  node [
    id 411
    label "kariokineza"
  ]
  node [
    id 412
    label "core"
  ]
  node [
    id 413
    label "chemia_j&#261;drowa"
  ]
  node [
    id 414
    label "anorchizm"
  ]
  node [
    id 415
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 416
    label "nasieniak"
  ]
  node [
    id 417
    label "wn&#281;trostwo"
  ]
  node [
    id 418
    label "ziarno"
  ]
  node [
    id 419
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 420
    label "j&#261;derko"
  ]
  node [
    id 421
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 422
    label "jajo"
  ]
  node [
    id 423
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 424
    label "chromosom"
  ]
  node [
    id 425
    label "organellum"
  ]
  node [
    id 426
    label "moszna"
  ]
  node [
    id 427
    label "przeciwobraz"
  ]
  node [
    id 428
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 429
    label "&#347;rodek"
  ]
  node [
    id 430
    label "protoplazma"
  ]
  node [
    id 431
    label "znaczenie"
  ]
  node [
    id 432
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 433
    label "nukleosynteza"
  ]
  node [
    id 434
    label "subsystem"
  ]
  node [
    id 435
    label "ko&#322;o"
  ]
  node [
    id 436
    label "granica"
  ]
  node [
    id 437
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 438
    label "suport"
  ]
  node [
    id 439
    label "prosta"
  ]
  node [
    id 440
    label "o&#347;rodek"
  ]
  node [
    id 441
    label "eonotem"
  ]
  node [
    id 442
    label "constellation"
  ]
  node [
    id 443
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 444
    label "Ptak_Rajski"
  ]
  node [
    id 445
    label "W&#281;&#380;ownik"
  ]
  node [
    id 446
    label "Panna"
  ]
  node [
    id 447
    label "W&#261;&#380;"
  ]
  node [
    id 448
    label "blokada"
  ]
  node [
    id 449
    label "hurtownia"
  ]
  node [
    id 450
    label "pomieszczenie"
  ]
  node [
    id 451
    label "pas"
  ]
  node [
    id 452
    label "basic"
  ]
  node [
    id 453
    label "sk&#322;adnik"
  ]
  node [
    id 454
    label "sklep"
  ]
  node [
    id 455
    label "obr&#243;bka"
  ]
  node [
    id 456
    label "constitution"
  ]
  node [
    id 457
    label "fabryka"
  ]
  node [
    id 458
    label "&#347;wiat&#322;o"
  ]
  node [
    id 459
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 460
    label "syf"
  ]
  node [
    id 461
    label "rank_and_file"
  ]
  node [
    id 462
    label "set"
  ]
  node [
    id 463
    label "tabulacja"
  ]
  node [
    id 464
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
]
