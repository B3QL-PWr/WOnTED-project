graph [
  node [
    id 0
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 1
    label "debit"
  ]
  node [
    id 2
    label "redaktor"
  ]
  node [
    id 3
    label "druk"
  ]
  node [
    id 4
    label "publikacja"
  ]
  node [
    id 5
    label "nadtytu&#322;"
  ]
  node [
    id 6
    label "szata_graficzna"
  ]
  node [
    id 7
    label "tytulatura"
  ]
  node [
    id 8
    label "wydawa&#263;"
  ]
  node [
    id 9
    label "elevation"
  ]
  node [
    id 10
    label "wyda&#263;"
  ]
  node [
    id 11
    label "mianowaniec"
  ]
  node [
    id 12
    label "poster"
  ]
  node [
    id 13
    label "nazwa"
  ]
  node [
    id 14
    label "podtytu&#322;"
  ]
  node [
    id 15
    label "technika"
  ]
  node [
    id 16
    label "impression"
  ]
  node [
    id 17
    label "pismo"
  ]
  node [
    id 18
    label "glif"
  ]
  node [
    id 19
    label "dese&#324;"
  ]
  node [
    id 20
    label "prohibita"
  ]
  node [
    id 21
    label "cymelium"
  ]
  node [
    id 22
    label "wytw&#243;r"
  ]
  node [
    id 23
    label "tkanina"
  ]
  node [
    id 24
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 25
    label "zaproszenie"
  ]
  node [
    id 26
    label "tekst"
  ]
  node [
    id 27
    label "formatowanie"
  ]
  node [
    id 28
    label "formatowa&#263;"
  ]
  node [
    id 29
    label "zdobnik"
  ]
  node [
    id 30
    label "character"
  ]
  node [
    id 31
    label "printing"
  ]
  node [
    id 32
    label "produkcja"
  ]
  node [
    id 33
    label "notification"
  ]
  node [
    id 34
    label "term"
  ]
  node [
    id 35
    label "wezwanie"
  ]
  node [
    id 36
    label "patron"
  ]
  node [
    id 37
    label "leksem"
  ]
  node [
    id 38
    label "prawo"
  ]
  node [
    id 39
    label "wydawnictwo"
  ]
  node [
    id 40
    label "robi&#263;"
  ]
  node [
    id 41
    label "mie&#263;_miejsce"
  ]
  node [
    id 42
    label "plon"
  ]
  node [
    id 43
    label "give"
  ]
  node [
    id 44
    label "surrender"
  ]
  node [
    id 45
    label "kojarzy&#263;"
  ]
  node [
    id 46
    label "d&#378;wi&#281;k"
  ]
  node [
    id 47
    label "impart"
  ]
  node [
    id 48
    label "dawa&#263;"
  ]
  node [
    id 49
    label "reszta"
  ]
  node [
    id 50
    label "zapach"
  ]
  node [
    id 51
    label "wiano"
  ]
  node [
    id 52
    label "wprowadza&#263;"
  ]
  node [
    id 53
    label "podawa&#263;"
  ]
  node [
    id 54
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 55
    label "ujawnia&#263;"
  ]
  node [
    id 56
    label "placard"
  ]
  node [
    id 57
    label "powierza&#263;"
  ]
  node [
    id 58
    label "denuncjowa&#263;"
  ]
  node [
    id 59
    label "tajemnica"
  ]
  node [
    id 60
    label "panna_na_wydaniu"
  ]
  node [
    id 61
    label "wytwarza&#263;"
  ]
  node [
    id 62
    label "train"
  ]
  node [
    id 63
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 64
    label "cz&#322;owiek"
  ]
  node [
    id 65
    label "redakcja"
  ]
  node [
    id 66
    label "bran&#380;owiec"
  ]
  node [
    id 67
    label "edytor"
  ]
  node [
    id 68
    label "powierzy&#263;"
  ]
  node [
    id 69
    label "pieni&#261;dze"
  ]
  node [
    id 70
    label "skojarzy&#263;"
  ]
  node [
    id 71
    label "zadenuncjowa&#263;"
  ]
  node [
    id 72
    label "da&#263;"
  ]
  node [
    id 73
    label "zrobi&#263;"
  ]
  node [
    id 74
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 75
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 76
    label "translate"
  ]
  node [
    id 77
    label "picture"
  ]
  node [
    id 78
    label "poda&#263;"
  ]
  node [
    id 79
    label "wprowadzi&#263;"
  ]
  node [
    id 80
    label "wytworzy&#263;"
  ]
  node [
    id 81
    label "dress"
  ]
  node [
    id 82
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 83
    label "supply"
  ]
  node [
    id 84
    label "ujawni&#263;"
  ]
  node [
    id 85
    label "urz&#261;d"
  ]
  node [
    id 86
    label "stanowisko"
  ]
  node [
    id 87
    label "mandatariusz"
  ]
  node [
    id 88
    label "afisz"
  ]
  node [
    id 89
    label "dane"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
]
