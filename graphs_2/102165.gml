graph [
  node [
    id 0
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dokument"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
  ]
  node [
    id 3
    label "claim"
  ]
  node [
    id 4
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 5
    label "zmusza&#263;"
  ]
  node [
    id 6
    label "take"
  ]
  node [
    id 7
    label "force"
  ]
  node [
    id 8
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 9
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 10
    label "sandbag"
  ]
  node [
    id 11
    label "powodowa&#263;"
  ]
  node [
    id 12
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 13
    label "mie&#263;_miejsce"
  ]
  node [
    id 14
    label "equal"
  ]
  node [
    id 15
    label "trwa&#263;"
  ]
  node [
    id 16
    label "chodzi&#263;"
  ]
  node [
    id 17
    label "si&#281;ga&#263;"
  ]
  node [
    id 18
    label "stan"
  ]
  node [
    id 19
    label "obecno&#347;&#263;"
  ]
  node [
    id 20
    label "stand"
  ]
  node [
    id 21
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "uczestniczy&#263;"
  ]
  node [
    id 23
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 24
    label "woo"
  ]
  node [
    id 25
    label "chcie&#263;"
  ]
  node [
    id 26
    label "zapis"
  ]
  node [
    id 27
    label "&#347;wiadectwo"
  ]
  node [
    id 28
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 29
    label "wytw&#243;r"
  ]
  node [
    id 30
    label "parafa"
  ]
  node [
    id 31
    label "plik"
  ]
  node [
    id 32
    label "raport&#243;wka"
  ]
  node [
    id 33
    label "utw&#243;r"
  ]
  node [
    id 34
    label "record"
  ]
  node [
    id 35
    label "registratura"
  ]
  node [
    id 36
    label "dokumentacja"
  ]
  node [
    id 37
    label "fascyku&#322;"
  ]
  node [
    id 38
    label "artyku&#322;"
  ]
  node [
    id 39
    label "writing"
  ]
  node [
    id 40
    label "sygnatariusz"
  ]
  node [
    id 41
    label "dow&#243;d"
  ]
  node [
    id 42
    label "o&#347;wiadczenie"
  ]
  node [
    id 43
    label "za&#347;wiadczenie"
  ]
  node [
    id 44
    label "certificate"
  ]
  node [
    id 45
    label "promocja"
  ]
  node [
    id 46
    label "spos&#243;b"
  ]
  node [
    id 47
    label "entrance"
  ]
  node [
    id 48
    label "czynno&#347;&#263;"
  ]
  node [
    id 49
    label "wpis"
  ]
  node [
    id 50
    label "normalizacja"
  ]
  node [
    id 51
    label "obrazowanie"
  ]
  node [
    id 52
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 53
    label "organ"
  ]
  node [
    id 54
    label "tre&#347;&#263;"
  ]
  node [
    id 55
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 56
    label "part"
  ]
  node [
    id 57
    label "element_anatomiczny"
  ]
  node [
    id 58
    label "tekst"
  ]
  node [
    id 59
    label "komunikat"
  ]
  node [
    id 60
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 61
    label "przedmiot"
  ]
  node [
    id 62
    label "p&#322;&#243;d"
  ]
  node [
    id 63
    label "work"
  ]
  node [
    id 64
    label "rezultat"
  ]
  node [
    id 65
    label "podkatalog"
  ]
  node [
    id 66
    label "nadpisa&#263;"
  ]
  node [
    id 67
    label "nadpisanie"
  ]
  node [
    id 68
    label "bundle"
  ]
  node [
    id 69
    label "folder"
  ]
  node [
    id 70
    label "nadpisywanie"
  ]
  node [
    id 71
    label "paczka"
  ]
  node [
    id 72
    label "nadpisywa&#263;"
  ]
  node [
    id 73
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 74
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 75
    label "przedstawiciel"
  ]
  node [
    id 76
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 77
    label "wydanie"
  ]
  node [
    id 78
    label "zbi&#243;r"
  ]
  node [
    id 79
    label "torba"
  ]
  node [
    id 80
    label "ekscerpcja"
  ]
  node [
    id 81
    label "materia&#322;"
  ]
  node [
    id 82
    label "operat"
  ]
  node [
    id 83
    label "kosztorys"
  ]
  node [
    id 84
    label "biuro"
  ]
  node [
    id 85
    label "register"
  ]
  node [
    id 86
    label "blok"
  ]
  node [
    id 87
    label "prawda"
  ]
  node [
    id 88
    label "znak_j&#281;zykowy"
  ]
  node [
    id 89
    label "nag&#322;&#243;wek"
  ]
  node [
    id 90
    label "szkic"
  ]
  node [
    id 91
    label "line"
  ]
  node [
    id 92
    label "fragment"
  ]
  node [
    id 93
    label "wyr&#243;b"
  ]
  node [
    id 94
    label "rodzajnik"
  ]
  node [
    id 95
    label "towar"
  ]
  node [
    id 96
    label "paragraf"
  ]
  node [
    id 97
    label "paraph"
  ]
  node [
    id 98
    label "podpis"
  ]
  node [
    id 99
    label "urz&#261;d"
  ]
  node [
    id 100
    label "m&#281;ski"
  ]
  node [
    id 101
    label "starszy"
  ]
  node [
    id 102
    label "warszawa"
  ]
  node [
    id 103
    label "wydzia&#322;"
  ]
  node [
    id 104
    label "obs&#322;uga"
  ]
  node [
    id 105
    label "mieszkaniec"
  ]
  node [
    id 106
    label "cywilny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 99
    target 102
  ]
  edge [
    source 99
    target 106
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 100
    target 106
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 106
  ]
  edge [
    source 102
    target 106
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 104
    target 105
  ]
]
