graph [
  node [
    id 0
    label "tajemnica"
    origin "text"
  ]
  node [
    id 1
    label "lord"
    origin "text"
  ]
  node [
    id 2
    label "singelworth"
    origin "text"
  ]
  node [
    id 3
    label "wypaplanie"
  ]
  node [
    id 4
    label "enigmat"
  ]
  node [
    id 5
    label "spos&#243;b"
  ]
  node [
    id 6
    label "wiedza"
  ]
  node [
    id 7
    label "zachowanie"
  ]
  node [
    id 8
    label "zachowywanie"
  ]
  node [
    id 9
    label "secret"
  ]
  node [
    id 10
    label "wydawa&#263;"
  ]
  node [
    id 11
    label "obowi&#261;zek"
  ]
  node [
    id 12
    label "dyskrecja"
  ]
  node [
    id 13
    label "informacja"
  ]
  node [
    id 14
    label "wyda&#263;"
  ]
  node [
    id 15
    label "rzecz"
  ]
  node [
    id 16
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 17
    label "taj&#324;"
  ]
  node [
    id 18
    label "zachowa&#263;"
  ]
  node [
    id 19
    label "zachowywa&#263;"
  ]
  node [
    id 20
    label "cognition"
  ]
  node [
    id 21
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 22
    label "intelekt"
  ]
  node [
    id 23
    label "pozwolenie"
  ]
  node [
    id 24
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 25
    label "zaawansowanie"
  ]
  node [
    id 26
    label "wykszta&#322;cenie"
  ]
  node [
    id 27
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 28
    label "model"
  ]
  node [
    id 29
    label "narz&#281;dzie"
  ]
  node [
    id 30
    label "zbi&#243;r"
  ]
  node [
    id 31
    label "tryb"
  ]
  node [
    id 32
    label "nature"
  ]
  node [
    id 33
    label "punkt"
  ]
  node [
    id 34
    label "publikacja"
  ]
  node [
    id 35
    label "obiega&#263;"
  ]
  node [
    id 36
    label "powzi&#281;cie"
  ]
  node [
    id 37
    label "dane"
  ]
  node [
    id 38
    label "obiegni&#281;cie"
  ]
  node [
    id 39
    label "sygna&#322;"
  ]
  node [
    id 40
    label "obieganie"
  ]
  node [
    id 41
    label "powzi&#261;&#263;"
  ]
  node [
    id 42
    label "obiec"
  ]
  node [
    id 43
    label "doj&#347;cie"
  ]
  node [
    id 44
    label "doj&#347;&#263;"
  ]
  node [
    id 45
    label "object"
  ]
  node [
    id 46
    label "przedmiot"
  ]
  node [
    id 47
    label "temat"
  ]
  node [
    id 48
    label "wpadni&#281;cie"
  ]
  node [
    id 49
    label "mienie"
  ]
  node [
    id 50
    label "przyroda"
  ]
  node [
    id 51
    label "istota"
  ]
  node [
    id 52
    label "obiekt"
  ]
  node [
    id 53
    label "kultura"
  ]
  node [
    id 54
    label "wpa&#347;&#263;"
  ]
  node [
    id 55
    label "wpadanie"
  ]
  node [
    id 56
    label "wpada&#263;"
  ]
  node [
    id 57
    label "milczenie"
  ]
  node [
    id 58
    label "nieznaczno&#347;&#263;"
  ]
  node [
    id 59
    label "takt"
  ]
  node [
    id 60
    label "prostota"
  ]
  node [
    id 61
    label "discretion"
  ]
  node [
    id 62
    label "znoszenie"
  ]
  node [
    id 63
    label "nap&#322;ywanie"
  ]
  node [
    id 64
    label "communication"
  ]
  node [
    id 65
    label "signal"
  ]
  node [
    id 66
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 67
    label "znosi&#263;"
  ]
  node [
    id 68
    label "znie&#347;&#263;"
  ]
  node [
    id 69
    label "zniesienie"
  ]
  node [
    id 70
    label "zarys"
  ]
  node [
    id 71
    label "komunikat"
  ]
  node [
    id 72
    label "depesza_emska"
  ]
  node [
    id 73
    label "duty"
  ]
  node [
    id 74
    label "wym&#243;g"
  ]
  node [
    id 75
    label "obarczy&#263;"
  ]
  node [
    id 76
    label "powinno&#347;&#263;"
  ]
  node [
    id 77
    label "zadanie"
  ]
  node [
    id 78
    label "post&#261;pi&#263;"
  ]
  node [
    id 79
    label "pami&#281;&#263;"
  ]
  node [
    id 80
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "zdyscyplinowanie"
  ]
  node [
    id 82
    label "post"
  ]
  node [
    id 83
    label "zrobi&#263;"
  ]
  node [
    id 84
    label "przechowa&#263;"
  ]
  node [
    id 85
    label "preserve"
  ]
  node [
    id 86
    label "dieta"
  ]
  node [
    id 87
    label "bury"
  ]
  node [
    id 88
    label "podtrzyma&#263;"
  ]
  node [
    id 89
    label "robi&#263;"
  ]
  node [
    id 90
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 91
    label "podtrzymywa&#263;"
  ]
  node [
    id 92
    label "control"
  ]
  node [
    id 93
    label "przechowywa&#263;"
  ]
  node [
    id 94
    label "behave"
  ]
  node [
    id 95
    label "hold"
  ]
  node [
    id 96
    label "post&#281;powa&#263;"
  ]
  node [
    id 97
    label "podtrzymywanie"
  ]
  node [
    id 98
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 99
    label "robienie"
  ]
  node [
    id 100
    label "conservation"
  ]
  node [
    id 101
    label "post&#281;powanie"
  ]
  node [
    id 102
    label "pami&#281;tanie"
  ]
  node [
    id 103
    label "czynno&#347;&#263;"
  ]
  node [
    id 104
    label "przechowywanie"
  ]
  node [
    id 105
    label "mie&#263;_miejsce"
  ]
  node [
    id 106
    label "plon"
  ]
  node [
    id 107
    label "give"
  ]
  node [
    id 108
    label "surrender"
  ]
  node [
    id 109
    label "kojarzy&#263;"
  ]
  node [
    id 110
    label "d&#378;wi&#281;k"
  ]
  node [
    id 111
    label "impart"
  ]
  node [
    id 112
    label "dawa&#263;"
  ]
  node [
    id 113
    label "reszta"
  ]
  node [
    id 114
    label "zapach"
  ]
  node [
    id 115
    label "wydawnictwo"
  ]
  node [
    id 116
    label "wiano"
  ]
  node [
    id 117
    label "produkcja"
  ]
  node [
    id 118
    label "wprowadza&#263;"
  ]
  node [
    id 119
    label "podawa&#263;"
  ]
  node [
    id 120
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 121
    label "ujawnia&#263;"
  ]
  node [
    id 122
    label "placard"
  ]
  node [
    id 123
    label "powierza&#263;"
  ]
  node [
    id 124
    label "denuncjowa&#263;"
  ]
  node [
    id 125
    label "panna_na_wydaniu"
  ]
  node [
    id 126
    label "wytwarza&#263;"
  ]
  node [
    id 127
    label "train"
  ]
  node [
    id 128
    label "reakcja"
  ]
  node [
    id 129
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 130
    label "struktura"
  ]
  node [
    id 131
    label "wydarzenie"
  ]
  node [
    id 132
    label "pochowanie"
  ]
  node [
    id 133
    label "post&#261;pienie"
  ]
  node [
    id 134
    label "bearing"
  ]
  node [
    id 135
    label "zwierz&#281;"
  ]
  node [
    id 136
    label "behawior"
  ]
  node [
    id 137
    label "observation"
  ]
  node [
    id 138
    label "podtrzymanie"
  ]
  node [
    id 139
    label "etolog"
  ]
  node [
    id 140
    label "przechowanie"
  ]
  node [
    id 141
    label "zrobienie"
  ]
  node [
    id 142
    label "ujawnienie"
  ]
  node [
    id 143
    label "powierzy&#263;"
  ]
  node [
    id 144
    label "pieni&#261;dze"
  ]
  node [
    id 145
    label "skojarzy&#263;"
  ]
  node [
    id 146
    label "zadenuncjowa&#263;"
  ]
  node [
    id 147
    label "da&#263;"
  ]
  node [
    id 148
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 149
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 150
    label "translate"
  ]
  node [
    id 151
    label "picture"
  ]
  node [
    id 152
    label "poda&#263;"
  ]
  node [
    id 153
    label "wprowadzi&#263;"
  ]
  node [
    id 154
    label "wytworzy&#263;"
  ]
  node [
    id 155
    label "dress"
  ]
  node [
    id 156
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 157
    label "supply"
  ]
  node [
    id 158
    label "ujawni&#263;"
  ]
  node [
    id 159
    label "riddle"
  ]
  node [
    id 160
    label "lordostwo"
  ]
  node [
    id 161
    label "arystokrata"
  ]
  node [
    id 162
    label "tytu&#322;"
  ]
  node [
    id 163
    label "milord"
  ]
  node [
    id 164
    label "dostojnik"
  ]
  node [
    id 165
    label "debit"
  ]
  node [
    id 166
    label "redaktor"
  ]
  node [
    id 167
    label "druk"
  ]
  node [
    id 168
    label "nadtytu&#322;"
  ]
  node [
    id 169
    label "szata_graficzna"
  ]
  node [
    id 170
    label "tytulatura"
  ]
  node [
    id 171
    label "elevation"
  ]
  node [
    id 172
    label "mianowaniec"
  ]
  node [
    id 173
    label "poster"
  ]
  node [
    id 174
    label "nazwa"
  ]
  node [
    id 175
    label "podtytu&#322;"
  ]
  node [
    id 176
    label "arystokracja"
  ]
  node [
    id 177
    label "szlachcic"
  ]
  node [
    id 178
    label "urz&#281;dnik"
  ]
  node [
    id 179
    label "notabl"
  ]
  node [
    id 180
    label "oficja&#322;"
  ]
  node [
    id 181
    label "zwrot"
  ]
  node [
    id 182
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 183
    label "urz&#261;d"
  ]
  node [
    id 184
    label "zarozumia&#322;o&#347;&#263;"
  ]
  node [
    id 185
    label "jednostka_administracyjna"
  ]
  node [
    id 186
    label "maj&#261;tek"
  ]
  node [
    id 187
    label "pyszno&#347;&#263;"
  ]
  node [
    id 188
    label "grupa"
  ]
  node [
    id 189
    label "lew"
  ]
  node [
    id 190
    label "&#8212;"
  ]
  node [
    id 191
    label "bia&#322;y"
  ]
  node [
    id 192
    label "Antonio"
  ]
  node [
    id 193
    label "della"
  ]
  node [
    id 194
    label "Brenta"
  ]
  node [
    id 195
    label "di"
  ]
  node [
    id 196
    label "bono"
  ]
  node [
    id 197
    label "Grazia"
  ]
  node [
    id 198
    label "san"
  ]
  node [
    id 199
    label "Luca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 189
    target 190
  ]
  edge [
    source 189
    target 191
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 194
  ]
  edge [
    source 193
    target 194
  ]
  edge [
    source 195
    target 196
  ]
  edge [
    source 195
    target 197
  ]
  edge [
    source 195
    target 198
  ]
  edge [
    source 195
    target 199
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 198
    target 199
  ]
]
