graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "praca"
    origin "text"
  ]
  node [
    id 4
    label "polityka"
    origin "text"
  ]
  node [
    id 5
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 6
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "program_informacyjny"
  ]
  node [
    id 8
    label "journal"
  ]
  node [
    id 9
    label "diariusz"
  ]
  node [
    id 10
    label "spis"
  ]
  node [
    id 11
    label "ksi&#281;ga"
  ]
  node [
    id 12
    label "sheet"
  ]
  node [
    id 13
    label "pami&#281;tnik"
  ]
  node [
    id 14
    label "gazeta"
  ]
  node [
    id 15
    label "tytu&#322;"
  ]
  node [
    id 16
    label "redakcja"
  ]
  node [
    id 17
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 18
    label "czasopismo"
  ]
  node [
    id 19
    label "prasa"
  ]
  node [
    id 20
    label "rozdzia&#322;"
  ]
  node [
    id 21
    label "pismo"
  ]
  node [
    id 22
    label "Ewangelia"
  ]
  node [
    id 23
    label "book"
  ]
  node [
    id 24
    label "dokument"
  ]
  node [
    id 25
    label "tome"
  ]
  node [
    id 26
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 27
    label "pami&#261;tka"
  ]
  node [
    id 28
    label "notes"
  ]
  node [
    id 29
    label "zapiski"
  ]
  node [
    id 30
    label "raptularz"
  ]
  node [
    id 31
    label "album"
  ]
  node [
    id 32
    label "utw&#243;r_epicki"
  ]
  node [
    id 33
    label "zbi&#243;r"
  ]
  node [
    id 34
    label "catalog"
  ]
  node [
    id 35
    label "pozycja"
  ]
  node [
    id 36
    label "akt"
  ]
  node [
    id 37
    label "tekst"
  ]
  node [
    id 38
    label "sumariusz"
  ]
  node [
    id 39
    label "stock"
  ]
  node [
    id 40
    label "figurowa&#263;"
  ]
  node [
    id 41
    label "czynno&#347;&#263;"
  ]
  node [
    id 42
    label "wyliczanka"
  ]
  node [
    id 43
    label "oficjalny"
  ]
  node [
    id 44
    label "urz&#281;dowo"
  ]
  node [
    id 45
    label "formalny"
  ]
  node [
    id 46
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 47
    label "formalizowanie"
  ]
  node [
    id 48
    label "formalnie"
  ]
  node [
    id 49
    label "oficjalnie"
  ]
  node [
    id 50
    label "jawny"
  ]
  node [
    id 51
    label "legalny"
  ]
  node [
    id 52
    label "sformalizowanie"
  ]
  node [
    id 53
    label "pozorny"
  ]
  node [
    id 54
    label "kompletny"
  ]
  node [
    id 55
    label "prawdziwy"
  ]
  node [
    id 56
    label "prawomocny"
  ]
  node [
    id 57
    label "dostojnik"
  ]
  node [
    id 58
    label "Goebbels"
  ]
  node [
    id 59
    label "Sto&#322;ypin"
  ]
  node [
    id 60
    label "rz&#261;d"
  ]
  node [
    id 61
    label "przybli&#380;enie"
  ]
  node [
    id 62
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 63
    label "kategoria"
  ]
  node [
    id 64
    label "szpaler"
  ]
  node [
    id 65
    label "lon&#380;a"
  ]
  node [
    id 66
    label "uporz&#261;dkowanie"
  ]
  node [
    id 67
    label "instytucja"
  ]
  node [
    id 68
    label "jednostka_systematyczna"
  ]
  node [
    id 69
    label "egzekutywa"
  ]
  node [
    id 70
    label "premier"
  ]
  node [
    id 71
    label "Londyn"
  ]
  node [
    id 72
    label "gabinet_cieni"
  ]
  node [
    id 73
    label "gromada"
  ]
  node [
    id 74
    label "number"
  ]
  node [
    id 75
    label "Konsulat"
  ]
  node [
    id 76
    label "tract"
  ]
  node [
    id 77
    label "klasa"
  ]
  node [
    id 78
    label "w&#322;adza"
  ]
  node [
    id 79
    label "urz&#281;dnik"
  ]
  node [
    id 80
    label "notabl"
  ]
  node [
    id 81
    label "oficja&#322;"
  ]
  node [
    id 82
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 83
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 84
    label "najem"
  ]
  node [
    id 85
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 86
    label "zak&#322;ad"
  ]
  node [
    id 87
    label "stosunek_pracy"
  ]
  node [
    id 88
    label "benedykty&#324;ski"
  ]
  node [
    id 89
    label "poda&#380;_pracy"
  ]
  node [
    id 90
    label "pracowanie"
  ]
  node [
    id 91
    label "tyrka"
  ]
  node [
    id 92
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 93
    label "wytw&#243;r"
  ]
  node [
    id 94
    label "miejsce"
  ]
  node [
    id 95
    label "zaw&#243;d"
  ]
  node [
    id 96
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 97
    label "tynkarski"
  ]
  node [
    id 98
    label "pracowa&#263;"
  ]
  node [
    id 99
    label "zmiana"
  ]
  node [
    id 100
    label "czynnik_produkcji"
  ]
  node [
    id 101
    label "zobowi&#261;zanie"
  ]
  node [
    id 102
    label "kierownictwo"
  ]
  node [
    id 103
    label "siedziba"
  ]
  node [
    id 104
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 105
    label "przedmiot"
  ]
  node [
    id 106
    label "p&#322;&#243;d"
  ]
  node [
    id 107
    label "work"
  ]
  node [
    id 108
    label "rezultat"
  ]
  node [
    id 109
    label "activity"
  ]
  node [
    id 110
    label "bezproblemowy"
  ]
  node [
    id 111
    label "wydarzenie"
  ]
  node [
    id 112
    label "warunek_lokalowy"
  ]
  node [
    id 113
    label "plac"
  ]
  node [
    id 114
    label "location"
  ]
  node [
    id 115
    label "uwaga"
  ]
  node [
    id 116
    label "przestrze&#324;"
  ]
  node [
    id 117
    label "status"
  ]
  node [
    id 118
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 119
    label "chwila"
  ]
  node [
    id 120
    label "cia&#322;o"
  ]
  node [
    id 121
    label "cecha"
  ]
  node [
    id 122
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 123
    label "stosunek_prawny"
  ]
  node [
    id 124
    label "oblig"
  ]
  node [
    id 125
    label "uregulowa&#263;"
  ]
  node [
    id 126
    label "oddzia&#322;anie"
  ]
  node [
    id 127
    label "occupation"
  ]
  node [
    id 128
    label "duty"
  ]
  node [
    id 129
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 130
    label "zapowied&#378;"
  ]
  node [
    id 131
    label "obowi&#261;zek"
  ]
  node [
    id 132
    label "statement"
  ]
  node [
    id 133
    label "zapewnienie"
  ]
  node [
    id 134
    label "miejsce_pracy"
  ]
  node [
    id 135
    label "&#321;ubianka"
  ]
  node [
    id 136
    label "dzia&#322;_personalny"
  ]
  node [
    id 137
    label "Kreml"
  ]
  node [
    id 138
    label "Bia&#322;y_Dom"
  ]
  node [
    id 139
    label "budynek"
  ]
  node [
    id 140
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 141
    label "sadowisko"
  ]
  node [
    id 142
    label "zak&#322;adka"
  ]
  node [
    id 143
    label "jednostka_organizacyjna"
  ]
  node [
    id 144
    label "wyko&#324;czenie"
  ]
  node [
    id 145
    label "firma"
  ]
  node [
    id 146
    label "czyn"
  ]
  node [
    id 147
    label "company"
  ]
  node [
    id 148
    label "instytut"
  ]
  node [
    id 149
    label "umowa"
  ]
  node [
    id 150
    label "cierpliwy"
  ]
  node [
    id 151
    label "mozolny"
  ]
  node [
    id 152
    label "wytrwa&#322;y"
  ]
  node [
    id 153
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 154
    label "benedykty&#324;sko"
  ]
  node [
    id 155
    label "typowy"
  ]
  node [
    id 156
    label "po_benedykty&#324;sku"
  ]
  node [
    id 157
    label "rewizja"
  ]
  node [
    id 158
    label "passage"
  ]
  node [
    id 159
    label "oznaka"
  ]
  node [
    id 160
    label "change"
  ]
  node [
    id 161
    label "ferment"
  ]
  node [
    id 162
    label "komplet"
  ]
  node [
    id 163
    label "anatomopatolog"
  ]
  node [
    id 164
    label "zmianka"
  ]
  node [
    id 165
    label "czas"
  ]
  node [
    id 166
    label "zjawisko"
  ]
  node [
    id 167
    label "amendment"
  ]
  node [
    id 168
    label "odmienianie"
  ]
  node [
    id 169
    label "tura"
  ]
  node [
    id 170
    label "przepracowanie_si&#281;"
  ]
  node [
    id 171
    label "zarz&#261;dzanie"
  ]
  node [
    id 172
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 173
    label "podlizanie_si&#281;"
  ]
  node [
    id 174
    label "dopracowanie"
  ]
  node [
    id 175
    label "podlizywanie_si&#281;"
  ]
  node [
    id 176
    label "uruchamianie"
  ]
  node [
    id 177
    label "dzia&#322;anie"
  ]
  node [
    id 178
    label "d&#261;&#380;enie"
  ]
  node [
    id 179
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 180
    label "uruchomienie"
  ]
  node [
    id 181
    label "nakr&#281;canie"
  ]
  node [
    id 182
    label "funkcjonowanie"
  ]
  node [
    id 183
    label "tr&#243;jstronny"
  ]
  node [
    id 184
    label "postaranie_si&#281;"
  ]
  node [
    id 185
    label "odpocz&#281;cie"
  ]
  node [
    id 186
    label "nakr&#281;cenie"
  ]
  node [
    id 187
    label "zatrzymanie"
  ]
  node [
    id 188
    label "spracowanie_si&#281;"
  ]
  node [
    id 189
    label "skakanie"
  ]
  node [
    id 190
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 191
    label "podtrzymywanie"
  ]
  node [
    id 192
    label "w&#322;&#261;czanie"
  ]
  node [
    id 193
    label "zaprz&#281;ganie"
  ]
  node [
    id 194
    label "podejmowanie"
  ]
  node [
    id 195
    label "maszyna"
  ]
  node [
    id 196
    label "wyrabianie"
  ]
  node [
    id 197
    label "dzianie_si&#281;"
  ]
  node [
    id 198
    label "use"
  ]
  node [
    id 199
    label "przepracowanie"
  ]
  node [
    id 200
    label "poruszanie_si&#281;"
  ]
  node [
    id 201
    label "funkcja"
  ]
  node [
    id 202
    label "impact"
  ]
  node [
    id 203
    label "przepracowywanie"
  ]
  node [
    id 204
    label "awansowanie"
  ]
  node [
    id 205
    label "courtship"
  ]
  node [
    id 206
    label "zapracowanie"
  ]
  node [
    id 207
    label "wyrobienie"
  ]
  node [
    id 208
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 209
    label "w&#322;&#261;czenie"
  ]
  node [
    id 210
    label "zawodoznawstwo"
  ]
  node [
    id 211
    label "emocja"
  ]
  node [
    id 212
    label "office"
  ]
  node [
    id 213
    label "kwalifikacje"
  ]
  node [
    id 214
    label "craft"
  ]
  node [
    id 215
    label "transakcja"
  ]
  node [
    id 216
    label "endeavor"
  ]
  node [
    id 217
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 218
    label "mie&#263;_miejsce"
  ]
  node [
    id 219
    label "podejmowa&#263;"
  ]
  node [
    id 220
    label "dziama&#263;"
  ]
  node [
    id 221
    label "do"
  ]
  node [
    id 222
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 223
    label "bangla&#263;"
  ]
  node [
    id 224
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 225
    label "dzia&#322;a&#263;"
  ]
  node [
    id 226
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 227
    label "tryb"
  ]
  node [
    id 228
    label "funkcjonowa&#263;"
  ]
  node [
    id 229
    label "biuro"
  ]
  node [
    id 230
    label "lead"
  ]
  node [
    id 231
    label "zesp&#243;&#322;"
  ]
  node [
    id 232
    label "metoda"
  ]
  node [
    id 233
    label "policy"
  ]
  node [
    id 234
    label "dyplomacja"
  ]
  node [
    id 235
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 236
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 237
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 238
    label "method"
  ]
  node [
    id 239
    label "spos&#243;b"
  ]
  node [
    id 240
    label "doktryna"
  ]
  node [
    id 241
    label "absolutorium"
  ]
  node [
    id 242
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 243
    label "statesmanship"
  ]
  node [
    id 244
    label "notyfikowa&#263;"
  ]
  node [
    id 245
    label "corps"
  ]
  node [
    id 246
    label "notyfikowanie"
  ]
  node [
    id 247
    label "korpus_dyplomatyczny"
  ]
  node [
    id 248
    label "nastawienie"
  ]
  node [
    id 249
    label "grupa"
  ]
  node [
    id 250
    label "spo&#322;ecznie"
  ]
  node [
    id 251
    label "publiczny"
  ]
  node [
    id 252
    label "niepubliczny"
  ]
  node [
    id 253
    label "publicznie"
  ]
  node [
    id 254
    label "upublicznianie"
  ]
  node [
    id 255
    label "upublicznienie"
  ]
  node [
    id 256
    label "miesi&#261;c"
  ]
  node [
    id 257
    label "tydzie&#324;"
  ]
  node [
    id 258
    label "miech"
  ]
  node [
    id 259
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 260
    label "rok"
  ]
  node [
    id 261
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
]
