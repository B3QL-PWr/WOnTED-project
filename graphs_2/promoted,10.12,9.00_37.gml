graph [
  node [
    id 0
    label "blisko"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;a"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "trwa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#347;ledztwo"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 7
    label "szef"
    origin "text"
  ]
  node [
    id 8
    label "lubuski"
    origin "text"
  ]
  node [
    id 9
    label "antyterrorysta"
    origin "text"
  ]
  node [
    id 10
    label "adam"
    origin "text"
  ]
  node [
    id 11
    label "pawlak"
    origin "text"
  ]
  node [
    id 12
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 13
    label "&#347;winouj&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 15
    label "bliski"
  ]
  node [
    id 16
    label "dok&#322;adnie"
  ]
  node [
    id 17
    label "silnie"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "znajomy"
  ]
  node [
    id 20
    label "zwi&#261;zany"
  ]
  node [
    id 21
    label "przesz&#322;y"
  ]
  node [
    id 22
    label "silny"
  ]
  node [
    id 23
    label "zbli&#380;enie"
  ]
  node [
    id 24
    label "kr&#243;tki"
  ]
  node [
    id 25
    label "oddalony"
  ]
  node [
    id 26
    label "dok&#322;adny"
  ]
  node [
    id 27
    label "nieodleg&#322;y"
  ]
  node [
    id 28
    label "przysz&#322;y"
  ]
  node [
    id 29
    label "gotowy"
  ]
  node [
    id 30
    label "ma&#322;y"
  ]
  node [
    id 31
    label "punctiliously"
  ]
  node [
    id 32
    label "meticulously"
  ]
  node [
    id 33
    label "precyzyjnie"
  ]
  node [
    id 34
    label "rzetelnie"
  ]
  node [
    id 35
    label "mocny"
  ]
  node [
    id 36
    label "zajebi&#347;cie"
  ]
  node [
    id 37
    label "przekonuj&#261;co"
  ]
  node [
    id 38
    label "powerfully"
  ]
  node [
    id 39
    label "konkretnie"
  ]
  node [
    id 40
    label "niepodwa&#380;alnie"
  ]
  node [
    id 41
    label "zdecydowanie"
  ]
  node [
    id 42
    label "dusznie"
  ]
  node [
    id 43
    label "intensywnie"
  ]
  node [
    id 44
    label "strongly"
  ]
  node [
    id 45
    label "fragment"
  ]
  node [
    id 46
    label "str&#243;j"
  ]
  node [
    id 47
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 48
    label "utw&#243;r"
  ]
  node [
    id 49
    label "gorset"
  ]
  node [
    id 50
    label "zrzucenie"
  ]
  node [
    id 51
    label "znoszenie"
  ]
  node [
    id 52
    label "kr&#243;j"
  ]
  node [
    id 53
    label "struktura"
  ]
  node [
    id 54
    label "ubranie_si&#281;"
  ]
  node [
    id 55
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 56
    label "znosi&#263;"
  ]
  node [
    id 57
    label "pochodzi&#263;"
  ]
  node [
    id 58
    label "zrzuci&#263;"
  ]
  node [
    id 59
    label "pasmanteria"
  ]
  node [
    id 60
    label "pochodzenie"
  ]
  node [
    id 61
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 62
    label "odzie&#380;"
  ]
  node [
    id 63
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 64
    label "wyko&#324;czenie"
  ]
  node [
    id 65
    label "nosi&#263;"
  ]
  node [
    id 66
    label "zasada"
  ]
  node [
    id 67
    label "w&#322;o&#380;enie"
  ]
  node [
    id 68
    label "garderoba"
  ]
  node [
    id 69
    label "odziewek"
  ]
  node [
    id 70
    label "p&#243;&#322;rocze"
  ]
  node [
    id 71
    label "martwy_sezon"
  ]
  node [
    id 72
    label "kalendarz"
  ]
  node [
    id 73
    label "cykl_astronomiczny"
  ]
  node [
    id 74
    label "lata"
  ]
  node [
    id 75
    label "pora_roku"
  ]
  node [
    id 76
    label "stulecie"
  ]
  node [
    id 77
    label "kurs"
  ]
  node [
    id 78
    label "czas"
  ]
  node [
    id 79
    label "jubileusz"
  ]
  node [
    id 80
    label "grupa"
  ]
  node [
    id 81
    label "kwarta&#322;"
  ]
  node [
    id 82
    label "miesi&#261;c"
  ]
  node [
    id 83
    label "summer"
  ]
  node [
    id 84
    label "odm&#322;adzanie"
  ]
  node [
    id 85
    label "liga"
  ]
  node [
    id 86
    label "jednostka_systematyczna"
  ]
  node [
    id 87
    label "asymilowanie"
  ]
  node [
    id 88
    label "gromada"
  ]
  node [
    id 89
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 90
    label "asymilowa&#263;"
  ]
  node [
    id 91
    label "egzemplarz"
  ]
  node [
    id 92
    label "Entuzjastki"
  ]
  node [
    id 93
    label "zbi&#243;r"
  ]
  node [
    id 94
    label "kompozycja"
  ]
  node [
    id 95
    label "Terranie"
  ]
  node [
    id 96
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 97
    label "category"
  ]
  node [
    id 98
    label "pakiet_klimatyczny"
  ]
  node [
    id 99
    label "oddzia&#322;"
  ]
  node [
    id 100
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 101
    label "cz&#261;steczka"
  ]
  node [
    id 102
    label "stage_set"
  ]
  node [
    id 103
    label "type"
  ]
  node [
    id 104
    label "specgrupa"
  ]
  node [
    id 105
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 106
    label "&#346;wietliki"
  ]
  node [
    id 107
    label "odm&#322;odzenie"
  ]
  node [
    id 108
    label "Eurogrupa"
  ]
  node [
    id 109
    label "odm&#322;adza&#263;"
  ]
  node [
    id 110
    label "formacja_geologiczna"
  ]
  node [
    id 111
    label "harcerze_starsi"
  ]
  node [
    id 112
    label "poprzedzanie"
  ]
  node [
    id 113
    label "czasoprzestrze&#324;"
  ]
  node [
    id 114
    label "laba"
  ]
  node [
    id 115
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 116
    label "chronometria"
  ]
  node [
    id 117
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 118
    label "rachuba_czasu"
  ]
  node [
    id 119
    label "przep&#322;ywanie"
  ]
  node [
    id 120
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 121
    label "czasokres"
  ]
  node [
    id 122
    label "odczyt"
  ]
  node [
    id 123
    label "chwila"
  ]
  node [
    id 124
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 125
    label "dzieje"
  ]
  node [
    id 126
    label "kategoria_gramatyczna"
  ]
  node [
    id 127
    label "poprzedzenie"
  ]
  node [
    id 128
    label "trawienie"
  ]
  node [
    id 129
    label "period"
  ]
  node [
    id 130
    label "okres_czasu"
  ]
  node [
    id 131
    label "poprzedza&#263;"
  ]
  node [
    id 132
    label "schy&#322;ek"
  ]
  node [
    id 133
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 134
    label "odwlekanie_si&#281;"
  ]
  node [
    id 135
    label "zegar"
  ]
  node [
    id 136
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 137
    label "czwarty_wymiar"
  ]
  node [
    id 138
    label "koniugacja"
  ]
  node [
    id 139
    label "Zeitgeist"
  ]
  node [
    id 140
    label "trawi&#263;"
  ]
  node [
    id 141
    label "pogoda"
  ]
  node [
    id 142
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 143
    label "poprzedzi&#263;"
  ]
  node [
    id 144
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 145
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 146
    label "time_period"
  ]
  node [
    id 147
    label "term"
  ]
  node [
    id 148
    label "rok_akademicki"
  ]
  node [
    id 149
    label "rok_szkolny"
  ]
  node [
    id 150
    label "semester"
  ]
  node [
    id 151
    label "anniwersarz"
  ]
  node [
    id 152
    label "rocznica"
  ]
  node [
    id 153
    label "obszar"
  ]
  node [
    id 154
    label "tydzie&#324;"
  ]
  node [
    id 155
    label "miech"
  ]
  node [
    id 156
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 157
    label "kalendy"
  ]
  node [
    id 158
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 159
    label "long_time"
  ]
  node [
    id 160
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 161
    label "almanac"
  ]
  node [
    id 162
    label "rozk&#322;ad"
  ]
  node [
    id 163
    label "wydawnictwo"
  ]
  node [
    id 164
    label "Juliusz_Cezar"
  ]
  node [
    id 165
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 166
    label "zwy&#380;kowanie"
  ]
  node [
    id 167
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 168
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 169
    label "zaj&#281;cia"
  ]
  node [
    id 170
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 171
    label "trasa"
  ]
  node [
    id 172
    label "przeorientowywanie"
  ]
  node [
    id 173
    label "przejazd"
  ]
  node [
    id 174
    label "kierunek"
  ]
  node [
    id 175
    label "przeorientowywa&#263;"
  ]
  node [
    id 176
    label "nauka"
  ]
  node [
    id 177
    label "przeorientowanie"
  ]
  node [
    id 178
    label "klasa"
  ]
  node [
    id 179
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 180
    label "przeorientowa&#263;"
  ]
  node [
    id 181
    label "manner"
  ]
  node [
    id 182
    label "course"
  ]
  node [
    id 183
    label "passage"
  ]
  node [
    id 184
    label "zni&#380;kowanie"
  ]
  node [
    id 185
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 186
    label "seria"
  ]
  node [
    id 187
    label "stawka"
  ]
  node [
    id 188
    label "way"
  ]
  node [
    id 189
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 190
    label "spos&#243;b"
  ]
  node [
    id 191
    label "deprecjacja"
  ]
  node [
    id 192
    label "cedu&#322;a"
  ]
  node [
    id 193
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 194
    label "drive"
  ]
  node [
    id 195
    label "bearing"
  ]
  node [
    id 196
    label "Lira"
  ]
  node [
    id 197
    label "istnie&#263;"
  ]
  node [
    id 198
    label "pozostawa&#263;"
  ]
  node [
    id 199
    label "zostawa&#263;"
  ]
  node [
    id 200
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 201
    label "stand"
  ]
  node [
    id 202
    label "adhere"
  ]
  node [
    id 203
    label "blend"
  ]
  node [
    id 204
    label "by&#263;"
  ]
  node [
    id 205
    label "stop"
  ]
  node [
    id 206
    label "przebywa&#263;"
  ]
  node [
    id 207
    label "change"
  ]
  node [
    id 208
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 209
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 210
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 211
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 212
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 213
    label "support"
  ]
  node [
    id 214
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 215
    label "examination"
  ]
  node [
    id 216
    label "detektyw"
  ]
  node [
    id 217
    label "wydarzenie"
  ]
  node [
    id 218
    label "przebiec"
  ]
  node [
    id 219
    label "charakter"
  ]
  node [
    id 220
    label "czynno&#347;&#263;"
  ]
  node [
    id 221
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 222
    label "motyw"
  ]
  node [
    id 223
    label "przebiegni&#281;cie"
  ]
  node [
    id 224
    label "fabu&#322;a"
  ]
  node [
    id 225
    label "Herkules_Poirot"
  ]
  node [
    id 226
    label "Sherlock_Holmes"
  ]
  node [
    id 227
    label "agent"
  ]
  node [
    id 228
    label "policjant"
  ]
  node [
    id 229
    label "kognicja"
  ]
  node [
    id 230
    label "object"
  ]
  node [
    id 231
    label "rozprawa"
  ]
  node [
    id 232
    label "temat"
  ]
  node [
    id 233
    label "szczeg&#243;&#322;"
  ]
  node [
    id 234
    label "proposition"
  ]
  node [
    id 235
    label "przes&#322;anka"
  ]
  node [
    id 236
    label "rzecz"
  ]
  node [
    id 237
    label "idea"
  ]
  node [
    id 238
    label "ideologia"
  ]
  node [
    id 239
    label "byt"
  ]
  node [
    id 240
    label "intelekt"
  ]
  node [
    id 241
    label "Kant"
  ]
  node [
    id 242
    label "p&#322;&#243;d"
  ]
  node [
    id 243
    label "cel"
  ]
  node [
    id 244
    label "poj&#281;cie"
  ]
  node [
    id 245
    label "istota"
  ]
  node [
    id 246
    label "pomys&#322;"
  ]
  node [
    id 247
    label "ideacja"
  ]
  node [
    id 248
    label "przedmiot"
  ]
  node [
    id 249
    label "wpadni&#281;cie"
  ]
  node [
    id 250
    label "mienie"
  ]
  node [
    id 251
    label "przyroda"
  ]
  node [
    id 252
    label "obiekt"
  ]
  node [
    id 253
    label "kultura"
  ]
  node [
    id 254
    label "wpa&#347;&#263;"
  ]
  node [
    id 255
    label "wpadanie"
  ]
  node [
    id 256
    label "wpada&#263;"
  ]
  node [
    id 257
    label "s&#261;d"
  ]
  node [
    id 258
    label "rozumowanie"
  ]
  node [
    id 259
    label "opracowanie"
  ]
  node [
    id 260
    label "proces"
  ]
  node [
    id 261
    label "obrady"
  ]
  node [
    id 262
    label "cytat"
  ]
  node [
    id 263
    label "tekst"
  ]
  node [
    id 264
    label "obja&#347;nienie"
  ]
  node [
    id 265
    label "s&#261;dzenie"
  ]
  node [
    id 266
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 267
    label "niuansowa&#263;"
  ]
  node [
    id 268
    label "element"
  ]
  node [
    id 269
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 270
    label "sk&#322;adnik"
  ]
  node [
    id 271
    label "zniuansowa&#263;"
  ]
  node [
    id 272
    label "fakt"
  ]
  node [
    id 273
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 274
    label "przyczyna"
  ]
  node [
    id 275
    label "wnioskowanie"
  ]
  node [
    id 276
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 277
    label "wyraz_pochodny"
  ]
  node [
    id 278
    label "zboczenie"
  ]
  node [
    id 279
    label "om&#243;wienie"
  ]
  node [
    id 280
    label "cecha"
  ]
  node [
    id 281
    label "omawia&#263;"
  ]
  node [
    id 282
    label "fraza"
  ]
  node [
    id 283
    label "tre&#347;&#263;"
  ]
  node [
    id 284
    label "entity"
  ]
  node [
    id 285
    label "forum"
  ]
  node [
    id 286
    label "topik"
  ]
  node [
    id 287
    label "tematyka"
  ]
  node [
    id 288
    label "w&#261;tek"
  ]
  node [
    id 289
    label "zbaczanie"
  ]
  node [
    id 290
    label "forma"
  ]
  node [
    id 291
    label "om&#243;wi&#263;"
  ]
  node [
    id 292
    label "omawianie"
  ]
  node [
    id 293
    label "melodia"
  ]
  node [
    id 294
    label "otoczka"
  ]
  node [
    id 295
    label "zbacza&#263;"
  ]
  node [
    id 296
    label "zboczy&#263;"
  ]
  node [
    id 297
    label "defenestracja"
  ]
  node [
    id 298
    label "agonia"
  ]
  node [
    id 299
    label "kres"
  ]
  node [
    id 300
    label "mogi&#322;a"
  ]
  node [
    id 301
    label "&#380;ycie"
  ]
  node [
    id 302
    label "kres_&#380;ycia"
  ]
  node [
    id 303
    label "upadek"
  ]
  node [
    id 304
    label "szeol"
  ]
  node [
    id 305
    label "pogrzebanie"
  ]
  node [
    id 306
    label "istota_nadprzyrodzona"
  ]
  node [
    id 307
    label "&#380;a&#322;oba"
  ]
  node [
    id 308
    label "pogrzeb"
  ]
  node [
    id 309
    label "zabicie"
  ]
  node [
    id 310
    label "ostatnie_podrygi"
  ]
  node [
    id 311
    label "punkt"
  ]
  node [
    id 312
    label "dzia&#322;anie"
  ]
  node [
    id 313
    label "koniec"
  ]
  node [
    id 314
    label "gleba"
  ]
  node [
    id 315
    label "kondycja"
  ]
  node [
    id 316
    label "ruch"
  ]
  node [
    id 317
    label "pogorszenie"
  ]
  node [
    id 318
    label "inclination"
  ]
  node [
    id 319
    label "death"
  ]
  node [
    id 320
    label "zmierzch"
  ]
  node [
    id 321
    label "stan"
  ]
  node [
    id 322
    label "nieuleczalnie_chory"
  ]
  node [
    id 323
    label "spocz&#261;&#263;"
  ]
  node [
    id 324
    label "spocz&#281;cie"
  ]
  node [
    id 325
    label "pochowanie"
  ]
  node [
    id 326
    label "spoczywa&#263;"
  ]
  node [
    id 327
    label "chowanie"
  ]
  node [
    id 328
    label "park_sztywnych"
  ]
  node [
    id 329
    label "pomnik"
  ]
  node [
    id 330
    label "nagrobek"
  ]
  node [
    id 331
    label "prochowisko"
  ]
  node [
    id 332
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 333
    label "spoczywanie"
  ]
  node [
    id 334
    label "za&#347;wiaty"
  ]
  node [
    id 335
    label "piek&#322;o"
  ]
  node [
    id 336
    label "judaizm"
  ]
  node [
    id 337
    label "destruction"
  ]
  node [
    id 338
    label "zabrzmienie"
  ]
  node [
    id 339
    label "skrzywdzenie"
  ]
  node [
    id 340
    label "pozabijanie"
  ]
  node [
    id 341
    label "zniszczenie"
  ]
  node [
    id 342
    label "zaszkodzenie"
  ]
  node [
    id 343
    label "usuni&#281;cie"
  ]
  node [
    id 344
    label "spowodowanie"
  ]
  node [
    id 345
    label "killing"
  ]
  node [
    id 346
    label "zdarzenie_si&#281;"
  ]
  node [
    id 347
    label "czyn"
  ]
  node [
    id 348
    label "umarcie"
  ]
  node [
    id 349
    label "granie"
  ]
  node [
    id 350
    label "zamkni&#281;cie"
  ]
  node [
    id 351
    label "compaction"
  ]
  node [
    id 352
    label "&#380;al"
  ]
  node [
    id 353
    label "paznokie&#263;"
  ]
  node [
    id 354
    label "symbol"
  ]
  node [
    id 355
    label "kir"
  ]
  node [
    id 356
    label "brud"
  ]
  node [
    id 357
    label "wyrzucenie"
  ]
  node [
    id 358
    label "defenestration"
  ]
  node [
    id 359
    label "zaj&#347;cie"
  ]
  node [
    id 360
    label "burying"
  ]
  node [
    id 361
    label "zasypanie"
  ]
  node [
    id 362
    label "zw&#322;oki"
  ]
  node [
    id 363
    label "burial"
  ]
  node [
    id 364
    label "porobienie"
  ]
  node [
    id 365
    label "gr&#243;b"
  ]
  node [
    id 366
    label "uniemo&#380;liwienie"
  ]
  node [
    id 367
    label "niepowodzenie"
  ]
  node [
    id 368
    label "stypa"
  ]
  node [
    id 369
    label "pusta_noc"
  ]
  node [
    id 370
    label "grabarz"
  ]
  node [
    id 371
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 372
    label "obrz&#281;d"
  ]
  node [
    id 373
    label "raj_utracony"
  ]
  node [
    id 374
    label "umieranie"
  ]
  node [
    id 375
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 376
    label "prze&#380;ywanie"
  ]
  node [
    id 377
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 378
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 379
    label "po&#322;&#243;g"
  ]
  node [
    id 380
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 381
    label "subsistence"
  ]
  node [
    id 382
    label "power"
  ]
  node [
    id 383
    label "okres_noworodkowy"
  ]
  node [
    id 384
    label "prze&#380;ycie"
  ]
  node [
    id 385
    label "wiek_matuzalemowy"
  ]
  node [
    id 386
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 387
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 388
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 389
    label "do&#380;ywanie"
  ]
  node [
    id 390
    label "dzieci&#324;stwo"
  ]
  node [
    id 391
    label "andropauza"
  ]
  node [
    id 392
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 393
    label "rozw&#243;j"
  ]
  node [
    id 394
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 395
    label "menopauza"
  ]
  node [
    id 396
    label "koleje_losu"
  ]
  node [
    id 397
    label "bycie"
  ]
  node [
    id 398
    label "zegar_biologiczny"
  ]
  node [
    id 399
    label "szwung"
  ]
  node [
    id 400
    label "przebywanie"
  ]
  node [
    id 401
    label "warunki"
  ]
  node [
    id 402
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 403
    label "niemowl&#281;ctwo"
  ]
  node [
    id 404
    label "&#380;ywy"
  ]
  node [
    id 405
    label "life"
  ]
  node [
    id 406
    label "staro&#347;&#263;"
  ]
  node [
    id 407
    label "energy"
  ]
  node [
    id 408
    label "pryncypa&#322;"
  ]
  node [
    id 409
    label "kierownictwo"
  ]
  node [
    id 410
    label "kierowa&#263;"
  ]
  node [
    id 411
    label "zwrot"
  ]
  node [
    id 412
    label "turn"
  ]
  node [
    id 413
    label "turning"
  ]
  node [
    id 414
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 415
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 416
    label "skr&#281;t"
  ]
  node [
    id 417
    label "obr&#243;t"
  ]
  node [
    id 418
    label "fraza_czasownikowa"
  ]
  node [
    id 419
    label "jednostka_leksykalna"
  ]
  node [
    id 420
    label "zmiana"
  ]
  node [
    id 421
    label "wyra&#380;enie"
  ]
  node [
    id 422
    label "ludzko&#347;&#263;"
  ]
  node [
    id 423
    label "wapniak"
  ]
  node [
    id 424
    label "os&#322;abia&#263;"
  ]
  node [
    id 425
    label "posta&#263;"
  ]
  node [
    id 426
    label "hominid"
  ]
  node [
    id 427
    label "podw&#322;adny"
  ]
  node [
    id 428
    label "os&#322;abianie"
  ]
  node [
    id 429
    label "g&#322;owa"
  ]
  node [
    id 430
    label "figura"
  ]
  node [
    id 431
    label "portrecista"
  ]
  node [
    id 432
    label "dwun&#243;g"
  ]
  node [
    id 433
    label "profanum"
  ]
  node [
    id 434
    label "mikrokosmos"
  ]
  node [
    id 435
    label "nasada"
  ]
  node [
    id 436
    label "duch"
  ]
  node [
    id 437
    label "antropochoria"
  ]
  node [
    id 438
    label "osoba"
  ]
  node [
    id 439
    label "wz&#243;r"
  ]
  node [
    id 440
    label "senior"
  ]
  node [
    id 441
    label "oddzia&#322;ywanie"
  ]
  node [
    id 442
    label "Adam"
  ]
  node [
    id 443
    label "homo_sapiens"
  ]
  node [
    id 444
    label "polifag"
  ]
  node [
    id 445
    label "biuro"
  ]
  node [
    id 446
    label "lead"
  ]
  node [
    id 447
    label "zesp&#243;&#322;"
  ]
  node [
    id 448
    label "siedziba"
  ]
  node [
    id 449
    label "praca"
  ]
  node [
    id 450
    label "w&#322;adza"
  ]
  node [
    id 451
    label "g&#322;os"
  ]
  node [
    id 452
    label "zwierzchnik"
  ]
  node [
    id 453
    label "sterowa&#263;"
  ]
  node [
    id 454
    label "wysy&#322;a&#263;"
  ]
  node [
    id 455
    label "manipulate"
  ]
  node [
    id 456
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 457
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 458
    label "ustawia&#263;"
  ]
  node [
    id 459
    label "give"
  ]
  node [
    id 460
    label "przeznacza&#263;"
  ]
  node [
    id 461
    label "control"
  ]
  node [
    id 462
    label "match"
  ]
  node [
    id 463
    label "motywowa&#263;"
  ]
  node [
    id 464
    label "administrowa&#263;"
  ]
  node [
    id 465
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 466
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 467
    label "order"
  ]
  node [
    id 468
    label "indicate"
  ]
  node [
    id 469
    label "antyterroryzm"
  ]
  node [
    id 470
    label "szturmowiec"
  ]
  node [
    id 471
    label "&#380;o&#322;nierz"
  ]
  node [
    id 472
    label "szturm&#243;wka"
  ]
  node [
    id 473
    label "my&#347;liwiec"
  ]
  node [
    id 474
    label "komandos"
  ]
  node [
    id 475
    label "&#347;rodek"
  ]
  node [
    id 476
    label "skupisko"
  ]
  node [
    id 477
    label "zal&#261;&#380;ek"
  ]
  node [
    id 478
    label "instytucja"
  ]
  node [
    id 479
    label "otoczenie"
  ]
  node [
    id 480
    label "Hollywood"
  ]
  node [
    id 481
    label "miejsce"
  ]
  node [
    id 482
    label "center"
  ]
  node [
    id 483
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 484
    label "status"
  ]
  node [
    id 485
    label "sytuacja"
  ]
  node [
    id 486
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 487
    label "okrycie"
  ]
  node [
    id 488
    label "class"
  ]
  node [
    id 489
    label "background"
  ]
  node [
    id 490
    label "crack"
  ]
  node [
    id 491
    label "cortege"
  ]
  node [
    id 492
    label "okolica"
  ]
  node [
    id 493
    label "huczek"
  ]
  node [
    id 494
    label "zrobienie"
  ]
  node [
    id 495
    label "Wielki_Atraktor"
  ]
  node [
    id 496
    label "warunek_lokalowy"
  ]
  node [
    id 497
    label "plac"
  ]
  node [
    id 498
    label "location"
  ]
  node [
    id 499
    label "uwaga"
  ]
  node [
    id 500
    label "przestrze&#324;"
  ]
  node [
    id 501
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 502
    label "cia&#322;o"
  ]
  node [
    id 503
    label "rz&#261;d"
  ]
  node [
    id 504
    label "abstrakcja"
  ]
  node [
    id 505
    label "chemikalia"
  ]
  node [
    id 506
    label "substancja"
  ]
  node [
    id 507
    label "osoba_prawna"
  ]
  node [
    id 508
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 509
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 510
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 511
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 512
    label "organizacja"
  ]
  node [
    id 513
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 514
    label "Fundusze_Unijne"
  ]
  node [
    id 515
    label "zamyka&#263;"
  ]
  node [
    id 516
    label "establishment"
  ]
  node [
    id 517
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 518
    label "urz&#261;d"
  ]
  node [
    id 519
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 520
    label "afiliowa&#263;"
  ]
  node [
    id 521
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 522
    label "standard"
  ]
  node [
    id 523
    label "zamykanie"
  ]
  node [
    id 524
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 525
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 526
    label "organ"
  ]
  node [
    id 527
    label "zar&#243;d&#378;"
  ]
  node [
    id 528
    label "pocz&#261;tek"
  ]
  node [
    id 529
    label "integument"
  ]
  node [
    id 530
    label "Los_Angeles"
  ]
  node [
    id 531
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 532
    label "Pawlak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 220
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 47
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 442
    target 532
  ]
]
