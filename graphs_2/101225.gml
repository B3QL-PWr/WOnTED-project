graph [
  node [
    id 0
    label "sto&#380;ek"
    origin "text"
  ]
  node [
    id 1
    label "ma&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "beskid"
    origin "text"
  ]
  node [
    id 3
    label "&#347;l&#261;sk"
    origin "text"
  ]
  node [
    id 4
    label "cone"
  ]
  node [
    id 5
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 6
    label "sto&#380;ek_&#347;ci&#281;ty"
  ]
  node [
    id 7
    label "sterta"
  ]
  node [
    id 8
    label "g&#243;ra"
  ]
  node [
    id 9
    label "kszta&#322;t"
  ]
  node [
    id 10
    label "knoll"
  ]
  node [
    id 11
    label "szybki"
  ]
  node [
    id 12
    label "nieznaczny"
  ]
  node [
    id 13
    label "przeci&#281;tny"
  ]
  node [
    id 14
    label "wstydliwy"
  ]
  node [
    id 15
    label "s&#322;aby"
  ]
  node [
    id 16
    label "niewa&#380;ny"
  ]
  node [
    id 17
    label "ch&#322;opiec"
  ]
  node [
    id 18
    label "m&#322;ody"
  ]
  node [
    id 19
    label "ma&#322;o"
  ]
  node [
    id 20
    label "marny"
  ]
  node [
    id 21
    label "nieliczny"
  ]
  node [
    id 22
    label "n&#281;dznie"
  ]
  node [
    id 23
    label "nieznacznie"
  ]
  node [
    id 24
    label "drobnostkowy"
  ]
  node [
    id 25
    label "g&#243;wniarz"
  ]
  node [
    id 26
    label "synek"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "boyfriend"
  ]
  node [
    id 29
    label "okrzos"
  ]
  node [
    id 30
    label "dziecko"
  ]
  node [
    id 31
    label "sympatia"
  ]
  node [
    id 32
    label "usynowienie"
  ]
  node [
    id 33
    label "pomocnik"
  ]
  node [
    id 34
    label "kawaler"
  ]
  node [
    id 35
    label "pederasta"
  ]
  node [
    id 36
    label "m&#322;odzieniec"
  ]
  node [
    id 37
    label "kajtek"
  ]
  node [
    id 38
    label "&#347;l&#261;ski"
  ]
  node [
    id 39
    label "usynawianie"
  ]
  node [
    id 40
    label "wstydliwie"
  ]
  node [
    id 41
    label "g&#322;upi"
  ]
  node [
    id 42
    label "nie&#347;mia&#322;y"
  ]
  node [
    id 43
    label "nietrwa&#322;y"
  ]
  node [
    id 44
    label "mizerny"
  ]
  node [
    id 45
    label "marnie"
  ]
  node [
    id 46
    label "delikatny"
  ]
  node [
    id 47
    label "po&#347;ledni"
  ]
  node [
    id 48
    label "niezdrowy"
  ]
  node [
    id 49
    label "z&#322;y"
  ]
  node [
    id 50
    label "nieumiej&#281;tny"
  ]
  node [
    id 51
    label "s&#322;abo"
  ]
  node [
    id 52
    label "lura"
  ]
  node [
    id 53
    label "nieudany"
  ]
  node [
    id 54
    label "s&#322;abowity"
  ]
  node [
    id 55
    label "zawodny"
  ]
  node [
    id 56
    label "&#322;agodny"
  ]
  node [
    id 57
    label "md&#322;y"
  ]
  node [
    id 58
    label "niedoskona&#322;y"
  ]
  node [
    id 59
    label "przemijaj&#261;cy"
  ]
  node [
    id 60
    label "niemocny"
  ]
  node [
    id 61
    label "niefajny"
  ]
  node [
    id 62
    label "kiepsko"
  ]
  node [
    id 63
    label "ja&#322;owy"
  ]
  node [
    id 64
    label "nieskuteczny"
  ]
  node [
    id 65
    label "kiepski"
  ]
  node [
    id 66
    label "nadaremnie"
  ]
  node [
    id 67
    label "m&#322;odo"
  ]
  node [
    id 68
    label "nowy"
  ]
  node [
    id 69
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 70
    label "nowo&#380;eniec"
  ]
  node [
    id 71
    label "nie&#380;onaty"
  ]
  node [
    id 72
    label "wczesny"
  ]
  node [
    id 73
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 74
    label "m&#261;&#380;"
  ]
  node [
    id 75
    label "charakterystyczny"
  ]
  node [
    id 76
    label "intensywny"
  ]
  node [
    id 77
    label "prosty"
  ]
  node [
    id 78
    label "kr&#243;tki"
  ]
  node [
    id 79
    label "temperamentny"
  ]
  node [
    id 80
    label "bystrolotny"
  ]
  node [
    id 81
    label "dynamiczny"
  ]
  node [
    id 82
    label "szybko"
  ]
  node [
    id 83
    label "sprawny"
  ]
  node [
    id 84
    label "bezpo&#347;redni"
  ]
  node [
    id 85
    label "energiczny"
  ]
  node [
    id 86
    label "uniewa&#380;nianie_si&#281;"
  ]
  node [
    id 87
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 88
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 89
    label "nieistotnie"
  ]
  node [
    id 90
    label "orientacyjny"
  ]
  node [
    id 91
    label "przeci&#281;tnie"
  ]
  node [
    id 92
    label "zwyczajny"
  ]
  node [
    id 93
    label "&#347;rednio"
  ]
  node [
    id 94
    label "taki_sobie"
  ]
  node [
    id 95
    label "nielicznie"
  ]
  node [
    id 96
    label "pomiernie"
  ]
  node [
    id 97
    label "kr&#243;tko"
  ]
  node [
    id 98
    label "mikroskopijnie"
  ]
  node [
    id 99
    label "mo&#380;liwie"
  ]
  node [
    id 100
    label "n&#281;dzny"
  ]
  node [
    id 101
    label "sm&#281;tnie"
  ]
  node [
    id 102
    label "vilely"
  ]
  node [
    id 103
    label "despicably"
  ]
  node [
    id 104
    label "biednie"
  ]
  node [
    id 105
    label "Beskid"
  ]
  node [
    id 106
    label "wielki"
  ]
  node [
    id 107
    label "pasmo"
  ]
  node [
    id 108
    label "Czantorii"
  ]
  node [
    id 109
    label "Mal&#253;"
  ]
  node [
    id 110
    label "Sto&#382;ek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 38
    target 105
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 109
    target 110
  ]
]
