graph [
  node [
    id 0
    label "podpis"
    origin "text"
  ]
  node [
    id 1
    label "prezydent"
    origin "text"
  ]
  node [
    id 2
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nowelizacja"
    origin "text"
  ]
  node [
    id 4
    label "ustawa"
    origin "text"
  ]
  node [
    id 5
    label "krajowy"
    origin "text"
  ]
  node [
    id 6
    label "administracja"
    origin "text"
  ]
  node [
    id 7
    label "skarbowy"
    origin "text"
  ]
  node [
    id 8
    label "napis"
  ]
  node [
    id 9
    label "signal"
  ]
  node [
    id 10
    label "znak"
  ]
  node [
    id 11
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 12
    label "potwierdzenie"
  ]
  node [
    id 13
    label "sign"
  ]
  node [
    id 14
    label "obja&#347;nienie"
  ]
  node [
    id 15
    label "dow&#243;d"
  ]
  node [
    id 16
    label "oznakowanie"
  ]
  node [
    id 17
    label "fakt"
  ]
  node [
    id 18
    label "stawia&#263;"
  ]
  node [
    id 19
    label "wytw&#243;r"
  ]
  node [
    id 20
    label "point"
  ]
  node [
    id 21
    label "kodzik"
  ]
  node [
    id 22
    label "postawi&#263;"
  ]
  node [
    id 23
    label "mark"
  ]
  node [
    id 24
    label "herb"
  ]
  node [
    id 25
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 26
    label "attribute"
  ]
  node [
    id 27
    label "implikowa&#263;"
  ]
  node [
    id 28
    label "autografia"
  ]
  node [
    id 29
    label "tekst"
  ]
  node [
    id 30
    label "expressive_style"
  ]
  node [
    id 31
    label "informacja"
  ]
  node [
    id 32
    label "explanation"
  ]
  node [
    id 33
    label "remark"
  ]
  node [
    id 34
    label "report"
  ]
  node [
    id 35
    label "zrozumia&#322;y"
  ]
  node [
    id 36
    label "przedstawienie"
  ]
  node [
    id 37
    label "poinformowanie"
  ]
  node [
    id 38
    label "o&#347;wiadczenie"
  ]
  node [
    id 39
    label "certificate"
  ]
  node [
    id 40
    label "zgodzenie_si&#281;"
  ]
  node [
    id 41
    label "stwierdzenie"
  ]
  node [
    id 42
    label "sanction"
  ]
  node [
    id 43
    label "przy&#347;wiadczenie"
  ]
  node [
    id 44
    label "dokument"
  ]
  node [
    id 45
    label "kontrasygnowanie"
  ]
  node [
    id 46
    label "w&#322;asnor&#281;cznie"
  ]
  node [
    id 47
    label "gruba_ryba"
  ]
  node [
    id 48
    label "Gorbaczow"
  ]
  node [
    id 49
    label "zwierzchnik"
  ]
  node [
    id 50
    label "Putin"
  ]
  node [
    id 51
    label "Tito"
  ]
  node [
    id 52
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 53
    label "Naser"
  ]
  node [
    id 54
    label "de_Gaulle"
  ]
  node [
    id 55
    label "Nixon"
  ]
  node [
    id 56
    label "Kemal"
  ]
  node [
    id 57
    label "Clinton"
  ]
  node [
    id 58
    label "Bierut"
  ]
  node [
    id 59
    label "Roosevelt"
  ]
  node [
    id 60
    label "samorz&#261;dowiec"
  ]
  node [
    id 61
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 62
    label "Jelcyn"
  ]
  node [
    id 63
    label "dostojnik"
  ]
  node [
    id 64
    label "pryncypa&#322;"
  ]
  node [
    id 65
    label "kierowa&#263;"
  ]
  node [
    id 66
    label "kierownictwo"
  ]
  node [
    id 67
    label "cz&#322;owiek"
  ]
  node [
    id 68
    label "urz&#281;dnik"
  ]
  node [
    id 69
    label "notabl"
  ]
  node [
    id 70
    label "oficja&#322;"
  ]
  node [
    id 71
    label "samorz&#261;d"
  ]
  node [
    id 72
    label "polityk"
  ]
  node [
    id 73
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 74
    label "komuna"
  ]
  node [
    id 75
    label "pauzowa&#263;"
  ]
  node [
    id 76
    label "by&#263;"
  ]
  node [
    id 77
    label "oczekiwa&#263;"
  ]
  node [
    id 78
    label "decydowa&#263;"
  ]
  node [
    id 79
    label "sp&#281;dza&#263;"
  ]
  node [
    id 80
    label "look"
  ]
  node [
    id 81
    label "hold"
  ]
  node [
    id 82
    label "anticipate"
  ]
  node [
    id 83
    label "wygl&#261;d"
  ]
  node [
    id 84
    label "stylizacja"
  ]
  node [
    id 85
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 86
    label "mie&#263;_miejsce"
  ]
  node [
    id 87
    label "equal"
  ]
  node [
    id 88
    label "trwa&#263;"
  ]
  node [
    id 89
    label "chodzi&#263;"
  ]
  node [
    id 90
    label "si&#281;ga&#263;"
  ]
  node [
    id 91
    label "stan"
  ]
  node [
    id 92
    label "obecno&#347;&#263;"
  ]
  node [
    id 93
    label "stand"
  ]
  node [
    id 94
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 95
    label "uczestniczy&#263;"
  ]
  node [
    id 96
    label "hesitate"
  ]
  node [
    id 97
    label "odpoczywa&#263;"
  ]
  node [
    id 98
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 99
    label "decide"
  ]
  node [
    id 100
    label "klasyfikator"
  ]
  node [
    id 101
    label "mean"
  ]
  node [
    id 102
    label "robi&#263;"
  ]
  node [
    id 103
    label "usuwa&#263;"
  ]
  node [
    id 104
    label "base_on_balls"
  ]
  node [
    id 105
    label "przykrzy&#263;"
  ]
  node [
    id 106
    label "p&#281;dzi&#263;"
  ]
  node [
    id 107
    label "przep&#281;dza&#263;"
  ]
  node [
    id 108
    label "doprowadza&#263;"
  ]
  node [
    id 109
    label "authorize"
  ]
  node [
    id 110
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 111
    label "chcie&#263;"
  ]
  node [
    id 112
    label "modyfikacja"
  ]
  node [
    id 113
    label "story"
  ]
  node [
    id 114
    label "amendment"
  ]
  node [
    id 115
    label "modification"
  ]
  node [
    id 116
    label "wyraz_pochodny"
  ]
  node [
    id 117
    label "przer&#243;bka"
  ]
  node [
    id 118
    label "przystosowanie"
  ]
  node [
    id 119
    label "zmiana"
  ]
  node [
    id 120
    label "Karta_Nauczyciela"
  ]
  node [
    id 121
    label "przej&#347;cie"
  ]
  node [
    id 122
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 123
    label "akt"
  ]
  node [
    id 124
    label "przej&#347;&#263;"
  ]
  node [
    id 125
    label "charter"
  ]
  node [
    id 126
    label "marc&#243;wka"
  ]
  node [
    id 127
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 128
    label "podnieci&#263;"
  ]
  node [
    id 129
    label "scena"
  ]
  node [
    id 130
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 131
    label "numer"
  ]
  node [
    id 132
    label "po&#380;ycie"
  ]
  node [
    id 133
    label "poj&#281;cie"
  ]
  node [
    id 134
    label "podniecenie"
  ]
  node [
    id 135
    label "nago&#347;&#263;"
  ]
  node [
    id 136
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 137
    label "fascyku&#322;"
  ]
  node [
    id 138
    label "seks"
  ]
  node [
    id 139
    label "podniecanie"
  ]
  node [
    id 140
    label "imisja"
  ]
  node [
    id 141
    label "zwyczaj"
  ]
  node [
    id 142
    label "rozmna&#380;anie"
  ]
  node [
    id 143
    label "ruch_frykcyjny"
  ]
  node [
    id 144
    label "ontologia"
  ]
  node [
    id 145
    label "wydarzenie"
  ]
  node [
    id 146
    label "na_pieska"
  ]
  node [
    id 147
    label "pozycja_misjonarska"
  ]
  node [
    id 148
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 149
    label "fragment"
  ]
  node [
    id 150
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 151
    label "z&#322;&#261;czenie"
  ]
  node [
    id 152
    label "czynno&#347;&#263;"
  ]
  node [
    id 153
    label "gra_wst&#281;pna"
  ]
  node [
    id 154
    label "erotyka"
  ]
  node [
    id 155
    label "urzeczywistnienie"
  ]
  node [
    id 156
    label "baraszki"
  ]
  node [
    id 157
    label "po&#380;&#261;danie"
  ]
  node [
    id 158
    label "wzw&#243;d"
  ]
  node [
    id 159
    label "funkcja"
  ]
  node [
    id 160
    label "act"
  ]
  node [
    id 161
    label "arystotelizm"
  ]
  node [
    id 162
    label "podnieca&#263;"
  ]
  node [
    id 163
    label "zabory"
  ]
  node [
    id 164
    label "ci&#281;&#380;arna"
  ]
  node [
    id 165
    label "rozwi&#261;zanie"
  ]
  node [
    id 166
    label "mini&#281;cie"
  ]
  node [
    id 167
    label "wymienienie"
  ]
  node [
    id 168
    label "zaliczenie"
  ]
  node [
    id 169
    label "traversal"
  ]
  node [
    id 170
    label "zdarzenie_si&#281;"
  ]
  node [
    id 171
    label "przewy&#380;szenie"
  ]
  node [
    id 172
    label "experience"
  ]
  node [
    id 173
    label "przepuszczenie"
  ]
  node [
    id 174
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 175
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 176
    label "strain"
  ]
  node [
    id 177
    label "faza"
  ]
  node [
    id 178
    label "przerobienie"
  ]
  node [
    id 179
    label "wydeptywanie"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "crack"
  ]
  node [
    id 182
    label "wydeptanie"
  ]
  node [
    id 183
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 184
    label "wstawka"
  ]
  node [
    id 185
    label "prze&#380;ycie"
  ]
  node [
    id 186
    label "uznanie"
  ]
  node [
    id 187
    label "doznanie"
  ]
  node [
    id 188
    label "dostanie_si&#281;"
  ]
  node [
    id 189
    label "trwanie"
  ]
  node [
    id 190
    label "przebycie"
  ]
  node [
    id 191
    label "wytyczenie"
  ]
  node [
    id 192
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 193
    label "przepojenie"
  ]
  node [
    id 194
    label "nas&#261;czenie"
  ]
  node [
    id 195
    label "nale&#380;enie"
  ]
  node [
    id 196
    label "mienie"
  ]
  node [
    id 197
    label "odmienienie"
  ]
  node [
    id 198
    label "przedostanie_si&#281;"
  ]
  node [
    id 199
    label "przemokni&#281;cie"
  ]
  node [
    id 200
    label "nasycenie_si&#281;"
  ]
  node [
    id 201
    label "zacz&#281;cie"
  ]
  node [
    id 202
    label "stanie_si&#281;"
  ]
  node [
    id 203
    label "offense"
  ]
  node [
    id 204
    label "przestanie"
  ]
  node [
    id 205
    label "podlec"
  ]
  node [
    id 206
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 207
    label "min&#261;&#263;"
  ]
  node [
    id 208
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 209
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 210
    label "zaliczy&#263;"
  ]
  node [
    id 211
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 212
    label "zmieni&#263;"
  ]
  node [
    id 213
    label "przeby&#263;"
  ]
  node [
    id 214
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 215
    label "die"
  ]
  node [
    id 216
    label "dozna&#263;"
  ]
  node [
    id 217
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 218
    label "zacz&#261;&#263;"
  ]
  node [
    id 219
    label "happen"
  ]
  node [
    id 220
    label "pass"
  ]
  node [
    id 221
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 222
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 223
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 224
    label "beat"
  ]
  node [
    id 225
    label "absorb"
  ]
  node [
    id 226
    label "przerobi&#263;"
  ]
  node [
    id 227
    label "pique"
  ]
  node [
    id 228
    label "przesta&#263;"
  ]
  node [
    id 229
    label "odnaj&#281;cie"
  ]
  node [
    id 230
    label "naj&#281;cie"
  ]
  node [
    id 231
    label "rodzimy"
  ]
  node [
    id 232
    label "w&#322;asny"
  ]
  node [
    id 233
    label "tutejszy"
  ]
  node [
    id 234
    label "biuro"
  ]
  node [
    id 235
    label "struktura"
  ]
  node [
    id 236
    label "siedziba"
  ]
  node [
    id 237
    label "zarz&#261;d"
  ]
  node [
    id 238
    label "petent"
  ]
  node [
    id 239
    label "dziekanat"
  ]
  node [
    id 240
    label "gospodarka"
  ]
  node [
    id 241
    label "mechanika"
  ]
  node [
    id 242
    label "o&#347;"
  ]
  node [
    id 243
    label "usenet"
  ]
  node [
    id 244
    label "rozprz&#261;c"
  ]
  node [
    id 245
    label "zachowanie"
  ]
  node [
    id 246
    label "cybernetyk"
  ]
  node [
    id 247
    label "podsystem"
  ]
  node [
    id 248
    label "system"
  ]
  node [
    id 249
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 250
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 251
    label "sk&#322;ad"
  ]
  node [
    id 252
    label "systemat"
  ]
  node [
    id 253
    label "cecha"
  ]
  node [
    id 254
    label "konstrukcja"
  ]
  node [
    id 255
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 256
    label "konstelacja"
  ]
  node [
    id 257
    label "&#321;ubianka"
  ]
  node [
    id 258
    label "miejsce_pracy"
  ]
  node [
    id 259
    label "dzia&#322;_personalny"
  ]
  node [
    id 260
    label "Kreml"
  ]
  node [
    id 261
    label "Bia&#322;y_Dom"
  ]
  node [
    id 262
    label "budynek"
  ]
  node [
    id 263
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 264
    label "sadowisko"
  ]
  node [
    id 265
    label "s&#261;d"
  ]
  node [
    id 266
    label "boks"
  ]
  node [
    id 267
    label "biurko"
  ]
  node [
    id 268
    label "instytucja"
  ]
  node [
    id 269
    label "palestra"
  ]
  node [
    id 270
    label "Biuro_Lustracyjne"
  ]
  node [
    id 271
    label "agency"
  ]
  node [
    id 272
    label "board"
  ]
  node [
    id 273
    label "dzia&#322;"
  ]
  node [
    id 274
    label "pomieszczenie"
  ]
  node [
    id 275
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 276
    label "Bruksela"
  ]
  node [
    id 277
    label "organizacja"
  ]
  node [
    id 278
    label "administration"
  ]
  node [
    id 279
    label "centrala"
  ]
  node [
    id 280
    label "w&#322;adza"
  ]
  node [
    id 281
    label "inwentarz"
  ]
  node [
    id 282
    label "rynek"
  ]
  node [
    id 283
    label "mieszkalnictwo"
  ]
  node [
    id 284
    label "agregat_ekonomiczny"
  ]
  node [
    id 285
    label "produkowanie"
  ]
  node [
    id 286
    label "farmaceutyka"
  ]
  node [
    id 287
    label "rolnictwo"
  ]
  node [
    id 288
    label "transport"
  ]
  node [
    id 289
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 290
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 291
    label "obronno&#347;&#263;"
  ]
  node [
    id 292
    label "sektor_prywatny"
  ]
  node [
    id 293
    label "sch&#322;adza&#263;"
  ]
  node [
    id 294
    label "czerwona_strefa"
  ]
  node [
    id 295
    label "pole"
  ]
  node [
    id 296
    label "sektor_publiczny"
  ]
  node [
    id 297
    label "bankowo&#347;&#263;"
  ]
  node [
    id 298
    label "gospodarowanie"
  ]
  node [
    id 299
    label "obora"
  ]
  node [
    id 300
    label "gospodarka_wodna"
  ]
  node [
    id 301
    label "gospodarka_le&#347;na"
  ]
  node [
    id 302
    label "gospodarowa&#263;"
  ]
  node [
    id 303
    label "fabryka"
  ]
  node [
    id 304
    label "wytw&#243;rnia"
  ]
  node [
    id 305
    label "stodo&#322;a"
  ]
  node [
    id 306
    label "przemys&#322;"
  ]
  node [
    id 307
    label "spichlerz"
  ]
  node [
    id 308
    label "sch&#322;adzanie"
  ]
  node [
    id 309
    label "sch&#322;odzenie"
  ]
  node [
    id 310
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 311
    label "zasada"
  ]
  node [
    id 312
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 313
    label "regulacja_cen"
  ]
  node [
    id 314
    label "szkolnictwo"
  ]
  node [
    id 315
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 316
    label "jednostka_administracyjna"
  ]
  node [
    id 317
    label "diecezja"
  ]
  node [
    id 318
    label "klient"
  ]
  node [
    id 319
    label "fiscal"
  ]
  node [
    id 320
    label "o"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
]
