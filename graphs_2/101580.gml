graph [
  node [
    id 0
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "drugi"
    origin "text"
  ]
  node [
    id 4
    label "czytanie"
    origin "text"
  ]
  node [
    id 5
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przed&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 7
    label "projekt"
    origin "text"
  ]
  node [
    id 8
    label "ustawa"
    origin "text"
  ]
  node [
    id 9
    label "poprawka"
    origin "text"
  ]
  node [
    id 10
    label "proponowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "aby"
    origin "text"
  ]
  node [
    id 12
    label "sejm"
    origin "text"
  ]
  node [
    id 13
    label "ponownie"
    origin "text"
  ]
  node [
    id 14
    label "skierowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "komisja"
    origin "text"
  ]
  node [
    id 17
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "prawy"
    origin "text"
  ]
  node [
    id 19
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 20
    label "cela"
    origin "text"
  ]
  node [
    id 21
    label "przedstawienie"
    origin "text"
  ]
  node [
    id 22
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 23
    label "odwadnia&#263;"
  ]
  node [
    id 24
    label "wi&#261;zanie"
  ]
  node [
    id 25
    label "odwodni&#263;"
  ]
  node [
    id 26
    label "bratnia_dusza"
  ]
  node [
    id 27
    label "powi&#261;zanie"
  ]
  node [
    id 28
    label "zwi&#261;zanie"
  ]
  node [
    id 29
    label "konstytucja"
  ]
  node [
    id 30
    label "organizacja"
  ]
  node [
    id 31
    label "marriage"
  ]
  node [
    id 32
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 33
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 34
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 35
    label "zwi&#261;za&#263;"
  ]
  node [
    id 36
    label "odwadnianie"
  ]
  node [
    id 37
    label "odwodnienie"
  ]
  node [
    id 38
    label "marketing_afiliacyjny"
  ]
  node [
    id 39
    label "substancja_chemiczna"
  ]
  node [
    id 40
    label "koligacja"
  ]
  node [
    id 41
    label "bearing"
  ]
  node [
    id 42
    label "lokant"
  ]
  node [
    id 43
    label "azeotrop"
  ]
  node [
    id 44
    label "odprowadza&#263;"
  ]
  node [
    id 45
    label "drain"
  ]
  node [
    id 46
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 47
    label "cia&#322;o"
  ]
  node [
    id 48
    label "powodowa&#263;"
  ]
  node [
    id 49
    label "osusza&#263;"
  ]
  node [
    id 50
    label "odci&#261;ga&#263;"
  ]
  node [
    id 51
    label "odsuwa&#263;"
  ]
  node [
    id 52
    label "struktura"
  ]
  node [
    id 53
    label "zbi&#243;r"
  ]
  node [
    id 54
    label "akt"
  ]
  node [
    id 55
    label "cezar"
  ]
  node [
    id 56
    label "dokument"
  ]
  node [
    id 57
    label "budowa"
  ]
  node [
    id 58
    label "uchwa&#322;a"
  ]
  node [
    id 59
    label "numeracja"
  ]
  node [
    id 60
    label "odprowadzanie"
  ]
  node [
    id 61
    label "powodowanie"
  ]
  node [
    id 62
    label "odci&#261;ganie"
  ]
  node [
    id 63
    label "dehydratacja"
  ]
  node [
    id 64
    label "osuszanie"
  ]
  node [
    id 65
    label "proces_chemiczny"
  ]
  node [
    id 66
    label "odsuwanie"
  ]
  node [
    id 67
    label "spowodowa&#263;"
  ]
  node [
    id 68
    label "odsun&#261;&#263;"
  ]
  node [
    id 69
    label "odprowadzi&#263;"
  ]
  node [
    id 70
    label "osuszy&#263;"
  ]
  node [
    id 71
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 72
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 73
    label "dehydration"
  ]
  node [
    id 74
    label "oznaka"
  ]
  node [
    id 75
    label "osuszenie"
  ]
  node [
    id 76
    label "spowodowanie"
  ]
  node [
    id 77
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 78
    label "odprowadzenie"
  ]
  node [
    id 79
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 80
    label "odsuni&#281;cie"
  ]
  node [
    id 81
    label "narta"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "podwi&#261;zywanie"
  ]
  node [
    id 84
    label "dressing"
  ]
  node [
    id 85
    label "socket"
  ]
  node [
    id 86
    label "szermierka"
  ]
  node [
    id 87
    label "przywi&#261;zywanie"
  ]
  node [
    id 88
    label "pakowanie"
  ]
  node [
    id 89
    label "my&#347;lenie"
  ]
  node [
    id 90
    label "do&#322;&#261;czanie"
  ]
  node [
    id 91
    label "communication"
  ]
  node [
    id 92
    label "wytwarzanie"
  ]
  node [
    id 93
    label "cement"
  ]
  node [
    id 94
    label "ceg&#322;a"
  ]
  node [
    id 95
    label "combination"
  ]
  node [
    id 96
    label "zobowi&#261;zywanie"
  ]
  node [
    id 97
    label "szcz&#281;ka"
  ]
  node [
    id 98
    label "anga&#380;owanie"
  ]
  node [
    id 99
    label "wi&#261;za&#263;"
  ]
  node [
    id 100
    label "twardnienie"
  ]
  node [
    id 101
    label "tobo&#322;ek"
  ]
  node [
    id 102
    label "podwi&#261;zanie"
  ]
  node [
    id 103
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 104
    label "przywi&#261;zanie"
  ]
  node [
    id 105
    label "przymocowywanie"
  ]
  node [
    id 106
    label "scalanie"
  ]
  node [
    id 107
    label "mezomeria"
  ]
  node [
    id 108
    label "wi&#281;&#378;"
  ]
  node [
    id 109
    label "fusion"
  ]
  node [
    id 110
    label "kojarzenie_si&#281;"
  ]
  node [
    id 111
    label "&#322;&#261;czenie"
  ]
  node [
    id 112
    label "uchwyt"
  ]
  node [
    id 113
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 114
    label "rozmieszczenie"
  ]
  node [
    id 115
    label "zmiana"
  ]
  node [
    id 116
    label "element_konstrukcyjny"
  ]
  node [
    id 117
    label "obezw&#322;adnianie"
  ]
  node [
    id 118
    label "manewr"
  ]
  node [
    id 119
    label "miecz"
  ]
  node [
    id 120
    label "oddzia&#322;ywanie"
  ]
  node [
    id 121
    label "obwi&#261;zanie"
  ]
  node [
    id 122
    label "zawi&#261;zek"
  ]
  node [
    id 123
    label "obwi&#261;zywanie"
  ]
  node [
    id 124
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 125
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 126
    label "w&#281;ze&#322;"
  ]
  node [
    id 127
    label "consort"
  ]
  node [
    id 128
    label "opakowa&#263;"
  ]
  node [
    id 129
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 130
    label "relate"
  ]
  node [
    id 131
    label "form"
  ]
  node [
    id 132
    label "unify"
  ]
  node [
    id 133
    label "incorporate"
  ]
  node [
    id 134
    label "bind"
  ]
  node [
    id 135
    label "zawi&#261;za&#263;"
  ]
  node [
    id 136
    label "zaprawa"
  ]
  node [
    id 137
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 138
    label "powi&#261;za&#263;"
  ]
  node [
    id 139
    label "scali&#263;"
  ]
  node [
    id 140
    label "zatrzyma&#263;"
  ]
  node [
    id 141
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 142
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 143
    label "ograniczenie"
  ]
  node [
    id 144
    label "po&#322;&#261;czenie"
  ]
  node [
    id 145
    label "do&#322;&#261;czenie"
  ]
  node [
    id 146
    label "opakowanie"
  ]
  node [
    id 147
    label "attachment"
  ]
  node [
    id 148
    label "obezw&#322;adnienie"
  ]
  node [
    id 149
    label "zawi&#261;zanie"
  ]
  node [
    id 150
    label "tying"
  ]
  node [
    id 151
    label "st&#281;&#380;enie"
  ]
  node [
    id 152
    label "affiliation"
  ]
  node [
    id 153
    label "fastening"
  ]
  node [
    id 154
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 155
    label "z&#322;&#261;czenie"
  ]
  node [
    id 156
    label "zobowi&#261;zanie"
  ]
  node [
    id 157
    label "roztw&#243;r"
  ]
  node [
    id 158
    label "podmiot"
  ]
  node [
    id 159
    label "jednostka_organizacyjna"
  ]
  node [
    id 160
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 161
    label "TOPR"
  ]
  node [
    id 162
    label "endecki"
  ]
  node [
    id 163
    label "zesp&#243;&#322;"
  ]
  node [
    id 164
    label "przedstawicielstwo"
  ]
  node [
    id 165
    label "od&#322;am"
  ]
  node [
    id 166
    label "Cepelia"
  ]
  node [
    id 167
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 168
    label "ZBoWiD"
  ]
  node [
    id 169
    label "organization"
  ]
  node [
    id 170
    label "centrala"
  ]
  node [
    id 171
    label "GOPR"
  ]
  node [
    id 172
    label "ZOMO"
  ]
  node [
    id 173
    label "ZMP"
  ]
  node [
    id 174
    label "komitet_koordynacyjny"
  ]
  node [
    id 175
    label "przybud&#243;wka"
  ]
  node [
    id 176
    label "boj&#243;wka"
  ]
  node [
    id 177
    label "zrelatywizowa&#263;"
  ]
  node [
    id 178
    label "zrelatywizowanie"
  ]
  node [
    id 179
    label "mention"
  ]
  node [
    id 180
    label "pomy&#347;lenie"
  ]
  node [
    id 181
    label "relatywizowa&#263;"
  ]
  node [
    id 182
    label "relatywizowanie"
  ]
  node [
    id 183
    label "kontakt"
  ]
  node [
    id 184
    label "poprzedzanie"
  ]
  node [
    id 185
    label "czasoprzestrze&#324;"
  ]
  node [
    id 186
    label "laba"
  ]
  node [
    id 187
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 188
    label "chronometria"
  ]
  node [
    id 189
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 190
    label "rachuba_czasu"
  ]
  node [
    id 191
    label "przep&#322;ywanie"
  ]
  node [
    id 192
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 193
    label "czasokres"
  ]
  node [
    id 194
    label "odczyt"
  ]
  node [
    id 195
    label "chwila"
  ]
  node [
    id 196
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 197
    label "dzieje"
  ]
  node [
    id 198
    label "kategoria_gramatyczna"
  ]
  node [
    id 199
    label "poprzedzenie"
  ]
  node [
    id 200
    label "trawienie"
  ]
  node [
    id 201
    label "pochodzi&#263;"
  ]
  node [
    id 202
    label "period"
  ]
  node [
    id 203
    label "okres_czasu"
  ]
  node [
    id 204
    label "poprzedza&#263;"
  ]
  node [
    id 205
    label "schy&#322;ek"
  ]
  node [
    id 206
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 207
    label "odwlekanie_si&#281;"
  ]
  node [
    id 208
    label "zegar"
  ]
  node [
    id 209
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 210
    label "czwarty_wymiar"
  ]
  node [
    id 211
    label "pochodzenie"
  ]
  node [
    id 212
    label "koniugacja"
  ]
  node [
    id 213
    label "Zeitgeist"
  ]
  node [
    id 214
    label "trawi&#263;"
  ]
  node [
    id 215
    label "pogoda"
  ]
  node [
    id 216
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 217
    label "poprzedzi&#263;"
  ]
  node [
    id 218
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 219
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 220
    label "time_period"
  ]
  node [
    id 221
    label "time"
  ]
  node [
    id 222
    label "blok"
  ]
  node [
    id 223
    label "handout"
  ]
  node [
    id 224
    label "pomiar"
  ]
  node [
    id 225
    label "lecture"
  ]
  node [
    id 226
    label "reading"
  ]
  node [
    id 227
    label "podawanie"
  ]
  node [
    id 228
    label "wyk&#322;ad"
  ]
  node [
    id 229
    label "potrzyma&#263;"
  ]
  node [
    id 230
    label "warunki"
  ]
  node [
    id 231
    label "pok&#243;j"
  ]
  node [
    id 232
    label "atak"
  ]
  node [
    id 233
    label "program"
  ]
  node [
    id 234
    label "zjawisko"
  ]
  node [
    id 235
    label "meteorology"
  ]
  node [
    id 236
    label "weather"
  ]
  node [
    id 237
    label "prognoza_meteorologiczna"
  ]
  node [
    id 238
    label "czas_wolny"
  ]
  node [
    id 239
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 240
    label "metrologia"
  ]
  node [
    id 241
    label "godzinnik"
  ]
  node [
    id 242
    label "bicie"
  ]
  node [
    id 243
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 244
    label "wahad&#322;o"
  ]
  node [
    id 245
    label "kurant"
  ]
  node [
    id 246
    label "cyferblat"
  ]
  node [
    id 247
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 248
    label "nabicie"
  ]
  node [
    id 249
    label "werk"
  ]
  node [
    id 250
    label "czasomierz"
  ]
  node [
    id 251
    label "tyka&#263;"
  ]
  node [
    id 252
    label "tykn&#261;&#263;"
  ]
  node [
    id 253
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 254
    label "urz&#261;dzenie"
  ]
  node [
    id 255
    label "kotwica"
  ]
  node [
    id 256
    label "fleksja"
  ]
  node [
    id 257
    label "liczba"
  ]
  node [
    id 258
    label "coupling"
  ]
  node [
    id 259
    label "osoba"
  ]
  node [
    id 260
    label "tryb"
  ]
  node [
    id 261
    label "czasownik"
  ]
  node [
    id 262
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 263
    label "orz&#281;sek"
  ]
  node [
    id 264
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 265
    label "zaczynanie_si&#281;"
  ]
  node [
    id 266
    label "str&#243;j"
  ]
  node [
    id 267
    label "wynikanie"
  ]
  node [
    id 268
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 269
    label "origin"
  ]
  node [
    id 270
    label "background"
  ]
  node [
    id 271
    label "geneza"
  ]
  node [
    id 272
    label "beginning"
  ]
  node [
    id 273
    label "digestion"
  ]
  node [
    id 274
    label "unicestwianie"
  ]
  node [
    id 275
    label "sp&#281;dzanie"
  ]
  node [
    id 276
    label "contemplation"
  ]
  node [
    id 277
    label "rozk&#322;adanie"
  ]
  node [
    id 278
    label "marnowanie"
  ]
  node [
    id 279
    label "proces_fizjologiczny"
  ]
  node [
    id 280
    label "przetrawianie"
  ]
  node [
    id 281
    label "perystaltyka"
  ]
  node [
    id 282
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 283
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 284
    label "przebywa&#263;"
  ]
  node [
    id 285
    label "pour"
  ]
  node [
    id 286
    label "carry"
  ]
  node [
    id 287
    label "sail"
  ]
  node [
    id 288
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 289
    label "go&#347;ci&#263;"
  ]
  node [
    id 290
    label "mija&#263;"
  ]
  node [
    id 291
    label "proceed"
  ]
  node [
    id 292
    label "odej&#347;cie"
  ]
  node [
    id 293
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 294
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 295
    label "zanikni&#281;cie"
  ]
  node [
    id 296
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 297
    label "ciecz"
  ]
  node [
    id 298
    label "opuszczenie"
  ]
  node [
    id 299
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 300
    label "departure"
  ]
  node [
    id 301
    label "oddalenie_si&#281;"
  ]
  node [
    id 302
    label "przeby&#263;"
  ]
  node [
    id 303
    label "min&#261;&#263;"
  ]
  node [
    id 304
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 305
    label "swimming"
  ]
  node [
    id 306
    label "zago&#347;ci&#263;"
  ]
  node [
    id 307
    label "cross"
  ]
  node [
    id 308
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 309
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 310
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 311
    label "zrobi&#263;"
  ]
  node [
    id 312
    label "opatrzy&#263;"
  ]
  node [
    id 313
    label "overwhelm"
  ]
  node [
    id 314
    label "opatrywa&#263;"
  ]
  node [
    id 315
    label "date"
  ]
  node [
    id 316
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 317
    label "wynika&#263;"
  ]
  node [
    id 318
    label "fall"
  ]
  node [
    id 319
    label "poby&#263;"
  ]
  node [
    id 320
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 321
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 322
    label "bolt"
  ]
  node [
    id 323
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 324
    label "uda&#263;_si&#281;"
  ]
  node [
    id 325
    label "opatrzenie"
  ]
  node [
    id 326
    label "zdarzenie_si&#281;"
  ]
  node [
    id 327
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 328
    label "progress"
  ]
  node [
    id 329
    label "opatrywanie"
  ]
  node [
    id 330
    label "mini&#281;cie"
  ]
  node [
    id 331
    label "doznanie"
  ]
  node [
    id 332
    label "zaistnienie"
  ]
  node [
    id 333
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 334
    label "przebycie"
  ]
  node [
    id 335
    label "cruise"
  ]
  node [
    id 336
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 337
    label "usuwa&#263;"
  ]
  node [
    id 338
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 339
    label "lutowa&#263;"
  ]
  node [
    id 340
    label "marnowa&#263;"
  ]
  node [
    id 341
    label "przetrawia&#263;"
  ]
  node [
    id 342
    label "poch&#322;ania&#263;"
  ]
  node [
    id 343
    label "digest"
  ]
  node [
    id 344
    label "metal"
  ]
  node [
    id 345
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 346
    label "sp&#281;dza&#263;"
  ]
  node [
    id 347
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 348
    label "zjawianie_si&#281;"
  ]
  node [
    id 349
    label "przebywanie"
  ]
  node [
    id 350
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 351
    label "mijanie"
  ]
  node [
    id 352
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 353
    label "zaznawanie"
  ]
  node [
    id 354
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 355
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 356
    label "flux"
  ]
  node [
    id 357
    label "epoka"
  ]
  node [
    id 358
    label "charakter"
  ]
  node [
    id 359
    label "flow"
  ]
  node [
    id 360
    label "choroba_przyrodzona"
  ]
  node [
    id 361
    label "ciota"
  ]
  node [
    id 362
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 363
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 364
    label "kres"
  ]
  node [
    id 365
    label "przestrze&#324;"
  ]
  node [
    id 366
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 367
    label "kolejny"
  ]
  node [
    id 368
    label "sw&#243;j"
  ]
  node [
    id 369
    label "przeciwny"
  ]
  node [
    id 370
    label "wt&#243;ry"
  ]
  node [
    id 371
    label "dzie&#324;"
  ]
  node [
    id 372
    label "inny"
  ]
  node [
    id 373
    label "odwrotnie"
  ]
  node [
    id 374
    label "podobny"
  ]
  node [
    id 375
    label "osobno"
  ]
  node [
    id 376
    label "r&#243;&#380;ny"
  ]
  node [
    id 377
    label "inszy"
  ]
  node [
    id 378
    label "inaczej"
  ]
  node [
    id 379
    label "ludzko&#347;&#263;"
  ]
  node [
    id 380
    label "asymilowanie"
  ]
  node [
    id 381
    label "wapniak"
  ]
  node [
    id 382
    label "asymilowa&#263;"
  ]
  node [
    id 383
    label "os&#322;abia&#263;"
  ]
  node [
    id 384
    label "posta&#263;"
  ]
  node [
    id 385
    label "hominid"
  ]
  node [
    id 386
    label "podw&#322;adny"
  ]
  node [
    id 387
    label "os&#322;abianie"
  ]
  node [
    id 388
    label "g&#322;owa"
  ]
  node [
    id 389
    label "figura"
  ]
  node [
    id 390
    label "portrecista"
  ]
  node [
    id 391
    label "dwun&#243;g"
  ]
  node [
    id 392
    label "profanum"
  ]
  node [
    id 393
    label "mikrokosmos"
  ]
  node [
    id 394
    label "nasada"
  ]
  node [
    id 395
    label "duch"
  ]
  node [
    id 396
    label "antropochoria"
  ]
  node [
    id 397
    label "wz&#243;r"
  ]
  node [
    id 398
    label "senior"
  ]
  node [
    id 399
    label "Adam"
  ]
  node [
    id 400
    label "homo_sapiens"
  ]
  node [
    id 401
    label "polifag"
  ]
  node [
    id 402
    label "nast&#281;pnie"
  ]
  node [
    id 403
    label "nastopny"
  ]
  node [
    id 404
    label "kolejno"
  ]
  node [
    id 405
    label "kt&#243;ry&#347;"
  ]
  node [
    id 406
    label "ranek"
  ]
  node [
    id 407
    label "doba"
  ]
  node [
    id 408
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 409
    label "noc"
  ]
  node [
    id 410
    label "podwiecz&#243;r"
  ]
  node [
    id 411
    label "po&#322;udnie"
  ]
  node [
    id 412
    label "godzina"
  ]
  node [
    id 413
    label "przedpo&#322;udnie"
  ]
  node [
    id 414
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 415
    label "long_time"
  ]
  node [
    id 416
    label "wiecz&#243;r"
  ]
  node [
    id 417
    label "t&#322;usty_czwartek"
  ]
  node [
    id 418
    label "popo&#322;udnie"
  ]
  node [
    id 419
    label "walentynki"
  ]
  node [
    id 420
    label "czynienie_si&#281;"
  ]
  node [
    id 421
    label "s&#322;o&#324;ce"
  ]
  node [
    id 422
    label "rano"
  ]
  node [
    id 423
    label "tydzie&#324;"
  ]
  node [
    id 424
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 425
    label "wzej&#347;cie"
  ]
  node [
    id 426
    label "wsta&#263;"
  ]
  node [
    id 427
    label "day"
  ]
  node [
    id 428
    label "termin"
  ]
  node [
    id 429
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 430
    label "wstanie"
  ]
  node [
    id 431
    label "przedwiecz&#243;r"
  ]
  node [
    id 432
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 433
    label "Sylwester"
  ]
  node [
    id 434
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 435
    label "odmienny"
  ]
  node [
    id 436
    label "po_przeciwnej_stronie"
  ]
  node [
    id 437
    label "przeciwnie"
  ]
  node [
    id 438
    label "niech&#281;tny"
  ]
  node [
    id 439
    label "samodzielny"
  ]
  node [
    id 440
    label "swojak"
  ]
  node [
    id 441
    label "odpowiedni"
  ]
  node [
    id 442
    label "bli&#378;ni"
  ]
  node [
    id 443
    label "przypominanie"
  ]
  node [
    id 444
    label "podobnie"
  ]
  node [
    id 445
    label "upodabnianie_si&#281;"
  ]
  node [
    id 446
    label "upodobnienie"
  ]
  node [
    id 447
    label "taki"
  ]
  node [
    id 448
    label "charakterystyczny"
  ]
  node [
    id 449
    label "upodobnienie_si&#281;"
  ]
  node [
    id 450
    label "zasymilowanie"
  ]
  node [
    id 451
    label "na_abarot"
  ]
  node [
    id 452
    label "odmiennie"
  ]
  node [
    id 453
    label "odwrotny"
  ]
  node [
    id 454
    label "recitation"
  ]
  node [
    id 455
    label "dysleksja"
  ]
  node [
    id 456
    label "wczytywanie_si&#281;"
  ]
  node [
    id 457
    label "poczytanie"
  ]
  node [
    id 458
    label "zaczytanie_si&#281;"
  ]
  node [
    id 459
    label "doczytywanie"
  ]
  node [
    id 460
    label "przepowiadanie"
  ]
  node [
    id 461
    label "obrz&#261;dek"
  ]
  node [
    id 462
    label "czytywanie"
  ]
  node [
    id 463
    label "Biblia"
  ]
  node [
    id 464
    label "pokazywanie"
  ]
  node [
    id 465
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 466
    label "wczytywanie"
  ]
  node [
    id 467
    label "bycie_w_stanie"
  ]
  node [
    id 468
    label "lektor"
  ]
  node [
    id 469
    label "poznawanie"
  ]
  node [
    id 470
    label "wyczytywanie"
  ]
  node [
    id 471
    label "oczytywanie_si&#281;"
  ]
  node [
    id 472
    label "auspicje"
  ]
  node [
    id 473
    label "przewidywanie"
  ]
  node [
    id 474
    label "powtarzanie"
  ]
  node [
    id 475
    label "divination"
  ]
  node [
    id 476
    label "profetyzm"
  ]
  node [
    id 477
    label "prorokowanie"
  ]
  node [
    id 478
    label "warto&#347;&#263;"
  ]
  node [
    id 479
    label "kszta&#322;cenie"
  ]
  node [
    id 480
    label "command"
  ]
  node [
    id 481
    label "wyraz"
  ]
  node [
    id 482
    label "obnoszenie"
  ]
  node [
    id 483
    label "robienie"
  ]
  node [
    id 484
    label "znaczenie"
  ]
  node [
    id 485
    label "show"
  ]
  node [
    id 486
    label "appearance"
  ]
  node [
    id 487
    label "przedstawianie"
  ]
  node [
    id 488
    label "informowanie"
  ]
  node [
    id 489
    label "czynno&#347;&#263;"
  ]
  node [
    id 490
    label "okazywanie"
  ]
  node [
    id 491
    label "&#347;wiadczenie"
  ]
  node [
    id 492
    label "uczenie_si&#281;"
  ]
  node [
    id 493
    label "cognition"
  ]
  node [
    id 494
    label "proces"
  ]
  node [
    id 495
    label "znajomy"
  ]
  node [
    id 496
    label "umo&#380;liwianie"
  ]
  node [
    id 497
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 498
    label "rozr&#243;&#380;nianie"
  ]
  node [
    id 499
    label "zapoznawanie"
  ]
  node [
    id 500
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 501
    label "designation"
  ]
  node [
    id 502
    label "inclusion"
  ]
  node [
    id 503
    label "czucie"
  ]
  node [
    id 504
    label "recognition"
  ]
  node [
    id 505
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 506
    label "spotykanie"
  ]
  node [
    id 507
    label "merging"
  ]
  node [
    id 508
    label "zawieranie"
  ]
  node [
    id 509
    label "kult"
  ]
  node [
    id 510
    label "liturgika"
  ]
  node [
    id 511
    label "porz&#261;dek"
  ]
  node [
    id 512
    label "praca_rolnicza"
  ]
  node [
    id 513
    label "function"
  ]
  node [
    id 514
    label "mod&#322;y"
  ]
  node [
    id 515
    label "hodowla"
  ]
  node [
    id 516
    label "modlitwa"
  ]
  node [
    id 517
    label "ceremony"
  ]
  node [
    id 518
    label "apokryf"
  ]
  node [
    id 519
    label "perykopa"
  ]
  node [
    id 520
    label "Biblia_Tysi&#261;clecia"
  ]
  node [
    id 521
    label "Stary_Testament"
  ]
  node [
    id 522
    label "paginator"
  ]
  node [
    id 523
    label "Nowy_Testament"
  ]
  node [
    id 524
    label "posta&#263;_biblijna"
  ]
  node [
    id 525
    label "Biblia_Jakuba_Wujka"
  ]
  node [
    id 526
    label "S&#322;owo_Bo&#380;e"
  ]
  node [
    id 527
    label "mamotrept"
  ]
  node [
    id 528
    label "nauczyciel"
  ]
  node [
    id 529
    label "prezenter"
  ]
  node [
    id 530
    label "ministrant"
  ]
  node [
    id 531
    label "seminarzysta"
  ]
  node [
    id 532
    label "dysfunkcja"
  ]
  node [
    id 533
    label "dyslexia"
  ]
  node [
    id 534
    label "czyta&#263;"
  ]
  node [
    id 535
    label "wprowadzanie"
  ]
  node [
    id 536
    label "uznanie"
  ]
  node [
    id 537
    label "ko&#324;czenie"
  ]
  node [
    id 538
    label "report"
  ]
  node [
    id 539
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 540
    label "poinformowa&#263;"
  ]
  node [
    id 541
    label "write"
  ]
  node [
    id 542
    label "announce"
  ]
  node [
    id 543
    label "nastawi&#263;"
  ]
  node [
    id 544
    label "draw"
  ]
  node [
    id 545
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 546
    label "obejrze&#263;"
  ]
  node [
    id 547
    label "impersonate"
  ]
  node [
    id 548
    label "dokoptowa&#263;"
  ]
  node [
    id 549
    label "prosecute"
  ]
  node [
    id 550
    label "uruchomi&#263;"
  ]
  node [
    id 551
    label "umie&#347;ci&#263;"
  ]
  node [
    id 552
    label "zacz&#261;&#263;"
  ]
  node [
    id 553
    label "inform"
  ]
  node [
    id 554
    label "zakomunikowa&#263;"
  ]
  node [
    id 555
    label "umowa"
  ]
  node [
    id 556
    label "cover"
  ]
  node [
    id 557
    label "translate"
  ]
  node [
    id 558
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 559
    label "express"
  ]
  node [
    id 560
    label "zaproponowa&#263;"
  ]
  node [
    id 561
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 562
    label "zach&#281;ci&#263;"
  ]
  node [
    id 563
    label "volunteer"
  ]
  node [
    id 564
    label "kandydatura"
  ]
  node [
    id 565
    label "indicate"
  ]
  node [
    id 566
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 567
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 568
    label "perform"
  ]
  node [
    id 569
    label "wyj&#347;&#263;"
  ]
  node [
    id 570
    label "zrezygnowa&#263;"
  ]
  node [
    id 571
    label "odst&#261;pi&#263;"
  ]
  node [
    id 572
    label "nak&#322;oni&#263;"
  ]
  node [
    id 573
    label "appear"
  ]
  node [
    id 574
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 575
    label "happen"
  ]
  node [
    id 576
    label "clear"
  ]
  node [
    id 577
    label "przedstawi&#263;"
  ]
  node [
    id 578
    label "explain"
  ]
  node [
    id 579
    label "poja&#347;ni&#263;"
  ]
  node [
    id 580
    label "intencja"
  ]
  node [
    id 581
    label "plan"
  ]
  node [
    id 582
    label "device"
  ]
  node [
    id 583
    label "program_u&#380;ytkowy"
  ]
  node [
    id 584
    label "pomys&#322;"
  ]
  node [
    id 585
    label "dokumentacja"
  ]
  node [
    id 586
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 587
    label "agreement"
  ]
  node [
    id 588
    label "wytw&#243;r"
  ]
  node [
    id 589
    label "thinking"
  ]
  node [
    id 590
    label "model"
  ]
  node [
    id 591
    label "punkt"
  ]
  node [
    id 592
    label "rysunek"
  ]
  node [
    id 593
    label "miejsce_pracy"
  ]
  node [
    id 594
    label "obraz"
  ]
  node [
    id 595
    label "reprezentacja"
  ]
  node [
    id 596
    label "dekoracja"
  ]
  node [
    id 597
    label "perspektywa"
  ]
  node [
    id 598
    label "ekscerpcja"
  ]
  node [
    id 599
    label "materia&#322;"
  ]
  node [
    id 600
    label "operat"
  ]
  node [
    id 601
    label "kosztorys"
  ]
  node [
    id 602
    label "zapis"
  ]
  node [
    id 603
    label "&#347;wiadectwo"
  ]
  node [
    id 604
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 605
    label "parafa"
  ]
  node [
    id 606
    label "plik"
  ]
  node [
    id 607
    label "raport&#243;wka"
  ]
  node [
    id 608
    label "utw&#243;r"
  ]
  node [
    id 609
    label "record"
  ]
  node [
    id 610
    label "fascyku&#322;"
  ]
  node [
    id 611
    label "registratura"
  ]
  node [
    id 612
    label "artyku&#322;"
  ]
  node [
    id 613
    label "writing"
  ]
  node [
    id 614
    label "sygnatariusz"
  ]
  node [
    id 615
    label "idea"
  ]
  node [
    id 616
    label "pocz&#261;tki"
  ]
  node [
    id 617
    label "ukradzenie"
  ]
  node [
    id 618
    label "ukra&#347;&#263;"
  ]
  node [
    id 619
    label "system"
  ]
  node [
    id 620
    label "Karta_Nauczyciela"
  ]
  node [
    id 621
    label "przej&#347;cie"
  ]
  node [
    id 622
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 623
    label "przej&#347;&#263;"
  ]
  node [
    id 624
    label "charter"
  ]
  node [
    id 625
    label "marc&#243;wka"
  ]
  node [
    id 626
    label "podnieci&#263;"
  ]
  node [
    id 627
    label "scena"
  ]
  node [
    id 628
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 629
    label "numer"
  ]
  node [
    id 630
    label "po&#380;ycie"
  ]
  node [
    id 631
    label "poj&#281;cie"
  ]
  node [
    id 632
    label "podniecenie"
  ]
  node [
    id 633
    label "nago&#347;&#263;"
  ]
  node [
    id 634
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 635
    label "seks"
  ]
  node [
    id 636
    label "podniecanie"
  ]
  node [
    id 637
    label "imisja"
  ]
  node [
    id 638
    label "zwyczaj"
  ]
  node [
    id 639
    label "rozmna&#380;anie"
  ]
  node [
    id 640
    label "ruch_frykcyjny"
  ]
  node [
    id 641
    label "ontologia"
  ]
  node [
    id 642
    label "wydarzenie"
  ]
  node [
    id 643
    label "na_pieska"
  ]
  node [
    id 644
    label "pozycja_misjonarska"
  ]
  node [
    id 645
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 646
    label "fragment"
  ]
  node [
    id 647
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 648
    label "gra_wst&#281;pna"
  ]
  node [
    id 649
    label "erotyka"
  ]
  node [
    id 650
    label "urzeczywistnienie"
  ]
  node [
    id 651
    label "baraszki"
  ]
  node [
    id 652
    label "certificate"
  ]
  node [
    id 653
    label "po&#380;&#261;danie"
  ]
  node [
    id 654
    label "wzw&#243;d"
  ]
  node [
    id 655
    label "funkcja"
  ]
  node [
    id 656
    label "act"
  ]
  node [
    id 657
    label "arystotelizm"
  ]
  node [
    id 658
    label "podnieca&#263;"
  ]
  node [
    id 659
    label "zabory"
  ]
  node [
    id 660
    label "ci&#281;&#380;arna"
  ]
  node [
    id 661
    label "rozwi&#261;zanie"
  ]
  node [
    id 662
    label "podlec"
  ]
  node [
    id 663
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 664
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 665
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 666
    label "zaliczy&#263;"
  ]
  node [
    id 667
    label "zmieni&#263;"
  ]
  node [
    id 668
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 669
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 670
    label "die"
  ]
  node [
    id 671
    label "dozna&#263;"
  ]
  node [
    id 672
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 673
    label "pass"
  ]
  node [
    id 674
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 675
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 676
    label "beat"
  ]
  node [
    id 677
    label "mienie"
  ]
  node [
    id 678
    label "absorb"
  ]
  node [
    id 679
    label "przerobi&#263;"
  ]
  node [
    id 680
    label "pique"
  ]
  node [
    id 681
    label "przesta&#263;"
  ]
  node [
    id 682
    label "wymienienie"
  ]
  node [
    id 683
    label "zaliczenie"
  ]
  node [
    id 684
    label "traversal"
  ]
  node [
    id 685
    label "przewy&#380;szenie"
  ]
  node [
    id 686
    label "experience"
  ]
  node [
    id 687
    label "przepuszczenie"
  ]
  node [
    id 688
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 689
    label "strain"
  ]
  node [
    id 690
    label "faza"
  ]
  node [
    id 691
    label "przerobienie"
  ]
  node [
    id 692
    label "wydeptywanie"
  ]
  node [
    id 693
    label "miejsce"
  ]
  node [
    id 694
    label "crack"
  ]
  node [
    id 695
    label "wydeptanie"
  ]
  node [
    id 696
    label "wstawka"
  ]
  node [
    id 697
    label "prze&#380;ycie"
  ]
  node [
    id 698
    label "dostanie_si&#281;"
  ]
  node [
    id 699
    label "trwanie"
  ]
  node [
    id 700
    label "wytyczenie"
  ]
  node [
    id 701
    label "przepojenie"
  ]
  node [
    id 702
    label "nas&#261;czenie"
  ]
  node [
    id 703
    label "nale&#380;enie"
  ]
  node [
    id 704
    label "odmienienie"
  ]
  node [
    id 705
    label "przedostanie_si&#281;"
  ]
  node [
    id 706
    label "przemokni&#281;cie"
  ]
  node [
    id 707
    label "nasycenie_si&#281;"
  ]
  node [
    id 708
    label "zacz&#281;cie"
  ]
  node [
    id 709
    label "stanie_si&#281;"
  ]
  node [
    id 710
    label "offense"
  ]
  node [
    id 711
    label "przestanie"
  ]
  node [
    id 712
    label "odnaj&#281;cie"
  ]
  node [
    id 713
    label "naj&#281;cie"
  ]
  node [
    id 714
    label "egzamin"
  ]
  node [
    id 715
    label "poprawa"
  ]
  node [
    id 716
    label "alternation"
  ]
  node [
    id 717
    label "modyfikacja"
  ]
  node [
    id 718
    label "oblewanie"
  ]
  node [
    id 719
    label "sesja_egzaminacyjna"
  ]
  node [
    id 720
    label "oblewa&#263;"
  ]
  node [
    id 721
    label "praca_pisemna"
  ]
  node [
    id 722
    label "sprawdzian"
  ]
  node [
    id 723
    label "magiel"
  ]
  node [
    id 724
    label "pr&#243;ba"
  ]
  node [
    id 725
    label "arkusz"
  ]
  node [
    id 726
    label "examination"
  ]
  node [
    id 727
    label "alteration"
  ]
  node [
    id 728
    label "ulepszenie"
  ]
  node [
    id 729
    label "modification"
  ]
  node [
    id 730
    label "wyraz_pochodny"
  ]
  node [
    id 731
    label "przer&#243;bka"
  ]
  node [
    id 732
    label "przystosowanie"
  ]
  node [
    id 733
    label "rozmiar"
  ]
  node [
    id 734
    label "zrewaluowa&#263;"
  ]
  node [
    id 735
    label "zmienna"
  ]
  node [
    id 736
    label "wskazywanie"
  ]
  node [
    id 737
    label "rewaluowanie"
  ]
  node [
    id 738
    label "cel"
  ]
  node [
    id 739
    label "wskazywa&#263;"
  ]
  node [
    id 740
    label "korzy&#347;&#263;"
  ]
  node [
    id 741
    label "worth"
  ]
  node [
    id 742
    label "cecha"
  ]
  node [
    id 743
    label "zrewaluowanie"
  ]
  node [
    id 744
    label "rewaluowa&#263;"
  ]
  node [
    id 745
    label "wabik"
  ]
  node [
    id 746
    label "strona"
  ]
  node [
    id 747
    label "wnioskowa&#263;"
  ]
  node [
    id 748
    label "zach&#281;ca&#263;"
  ]
  node [
    id 749
    label "suggest"
  ]
  node [
    id 750
    label "informowa&#263;"
  ]
  node [
    id 751
    label "sk&#322;ada&#263;"
  ]
  node [
    id 752
    label "prosi&#263;"
  ]
  node [
    id 753
    label "dochodzi&#263;"
  ]
  node [
    id 754
    label "argue"
  ]
  node [
    id 755
    label "raise"
  ]
  node [
    id 756
    label "powiada&#263;"
  ]
  node [
    id 757
    label "komunikowa&#263;"
  ]
  node [
    id 758
    label "pozyskiwa&#263;"
  ]
  node [
    id 759
    label "zaproponowanie"
  ]
  node [
    id 760
    label "proponowanie"
  ]
  node [
    id 761
    label "campaigning"
  ]
  node [
    id 762
    label "wniosek"
  ]
  node [
    id 763
    label "troch&#281;"
  ]
  node [
    id 764
    label "parlament"
  ]
  node [
    id 765
    label "izba_ni&#380;sza"
  ]
  node [
    id 766
    label "lewica"
  ]
  node [
    id 767
    label "siedziba"
  ]
  node [
    id 768
    label "parliament"
  ]
  node [
    id 769
    label "obrady"
  ]
  node [
    id 770
    label "prawica"
  ]
  node [
    id 771
    label "zgromadzenie"
  ]
  node [
    id 772
    label "centrum"
  ]
  node [
    id 773
    label "grupa"
  ]
  node [
    id 774
    label "&#321;ubianka"
  ]
  node [
    id 775
    label "dzia&#322;_personalny"
  ]
  node [
    id 776
    label "Kreml"
  ]
  node [
    id 777
    label "Bia&#322;y_Dom"
  ]
  node [
    id 778
    label "budynek"
  ]
  node [
    id 779
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 780
    label "sadowisko"
  ]
  node [
    id 781
    label "odm&#322;adzanie"
  ]
  node [
    id 782
    label "liga"
  ]
  node [
    id 783
    label "jednostka_systematyczna"
  ]
  node [
    id 784
    label "gromada"
  ]
  node [
    id 785
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 786
    label "egzemplarz"
  ]
  node [
    id 787
    label "Entuzjastki"
  ]
  node [
    id 788
    label "kompozycja"
  ]
  node [
    id 789
    label "Terranie"
  ]
  node [
    id 790
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 791
    label "category"
  ]
  node [
    id 792
    label "pakiet_klimatyczny"
  ]
  node [
    id 793
    label "oddzia&#322;"
  ]
  node [
    id 794
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 795
    label "cz&#261;steczka"
  ]
  node [
    id 796
    label "stage_set"
  ]
  node [
    id 797
    label "type"
  ]
  node [
    id 798
    label "specgrupa"
  ]
  node [
    id 799
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 800
    label "&#346;wietliki"
  ]
  node [
    id 801
    label "odm&#322;odzenie"
  ]
  node [
    id 802
    label "Eurogrupa"
  ]
  node [
    id 803
    label "odm&#322;adza&#263;"
  ]
  node [
    id 804
    label "formacja_geologiczna"
  ]
  node [
    id 805
    label "harcerze_starsi"
  ]
  node [
    id 806
    label "dyskusja"
  ]
  node [
    id 807
    label "conference"
  ]
  node [
    id 808
    label "konsylium"
  ]
  node [
    id 809
    label "concourse"
  ]
  node [
    id 810
    label "gathering"
  ]
  node [
    id 811
    label "skupienie"
  ]
  node [
    id 812
    label "wsp&#243;lnota"
  ]
  node [
    id 813
    label "spotkanie"
  ]
  node [
    id 814
    label "organ"
  ]
  node [
    id 815
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 816
    label "gromadzenie"
  ]
  node [
    id 817
    label "templum"
  ]
  node [
    id 818
    label "konwentykiel"
  ]
  node [
    id 819
    label "klasztor"
  ]
  node [
    id 820
    label "caucus"
  ]
  node [
    id 821
    label "pozyskanie"
  ]
  node [
    id 822
    label "kongregacja"
  ]
  node [
    id 823
    label "szko&#322;a"
  ]
  node [
    id 824
    label "left"
  ]
  node [
    id 825
    label "hand"
  ]
  node [
    id 826
    label "Hollywood"
  ]
  node [
    id 827
    label "centrolew"
  ]
  node [
    id 828
    label "o&#347;rodek"
  ]
  node [
    id 829
    label "centroprawica"
  ]
  node [
    id 830
    label "core"
  ]
  node [
    id 831
    label "urz&#261;d"
  ]
  node [
    id 832
    label "europarlament"
  ]
  node [
    id 833
    label "plankton_polityczny"
  ]
  node [
    id 834
    label "grupa_bilateralna"
  ]
  node [
    id 835
    label "ustawodawca"
  ]
  node [
    id 836
    label "znowu"
  ]
  node [
    id 837
    label "ponowny"
  ]
  node [
    id 838
    label "wznawianie"
  ]
  node [
    id 839
    label "wznowienie_si&#281;"
  ]
  node [
    id 840
    label "wznawianie_si&#281;"
  ]
  node [
    id 841
    label "dalszy"
  ]
  node [
    id 842
    label "nawrotny"
  ]
  node [
    id 843
    label "wznowienie"
  ]
  node [
    id 844
    label "set"
  ]
  node [
    id 845
    label "return"
  ]
  node [
    id 846
    label "dispatch"
  ]
  node [
    id 847
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 848
    label "przeznaczy&#263;"
  ]
  node [
    id 849
    label "ustawi&#263;"
  ]
  node [
    id 850
    label "wys&#322;a&#263;"
  ]
  node [
    id 851
    label "direct"
  ]
  node [
    id 852
    label "podpowiedzie&#263;"
  ]
  node [
    id 853
    label "precede"
  ]
  node [
    id 854
    label "poprawi&#263;"
  ]
  node [
    id 855
    label "nada&#263;"
  ]
  node [
    id 856
    label "peddle"
  ]
  node [
    id 857
    label "marshal"
  ]
  node [
    id 858
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 859
    label "wyznaczy&#263;"
  ]
  node [
    id 860
    label "stanowisko"
  ]
  node [
    id 861
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 862
    label "zabezpieczy&#263;"
  ]
  node [
    id 863
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 864
    label "zinterpretowa&#263;"
  ]
  node [
    id 865
    label "wskaza&#263;"
  ]
  node [
    id 866
    label "przyzna&#263;"
  ]
  node [
    id 867
    label "sk&#322;oni&#263;"
  ]
  node [
    id 868
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 869
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 870
    label "zdecydowa&#263;"
  ]
  node [
    id 871
    label "accommodate"
  ]
  node [
    id 872
    label "ustali&#263;"
  ]
  node [
    id 873
    label "situate"
  ]
  node [
    id 874
    label "rola"
  ]
  node [
    id 875
    label "sta&#263;_si&#281;"
  ]
  node [
    id 876
    label "appoint"
  ]
  node [
    id 877
    label "oblat"
  ]
  node [
    id 878
    label "nakaza&#263;"
  ]
  node [
    id 879
    label "przekaza&#263;"
  ]
  node [
    id 880
    label "ship"
  ]
  node [
    id 881
    label "post"
  ]
  node [
    id 882
    label "line"
  ]
  node [
    id 883
    label "wytworzy&#263;"
  ]
  node [
    id 884
    label "convey"
  ]
  node [
    id 885
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 886
    label "prompt"
  ]
  node [
    id 887
    label "motivate"
  ]
  node [
    id 888
    label "doradzi&#263;"
  ]
  node [
    id 889
    label "powiedzie&#263;"
  ]
  node [
    id 890
    label "pom&#243;c"
  ]
  node [
    id 891
    label "go"
  ]
  node [
    id 892
    label "travel"
  ]
  node [
    id 893
    label "gem"
  ]
  node [
    id 894
    label "runda"
  ]
  node [
    id 895
    label "muzyka"
  ]
  node [
    id 896
    label "zestaw"
  ]
  node [
    id 897
    label "okre&#347;lony"
  ]
  node [
    id 898
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 899
    label "wiadomy"
  ]
  node [
    id 900
    label "podkomisja"
  ]
  node [
    id 901
    label "Komisja_Europejska"
  ]
  node [
    id 902
    label "Mazowsze"
  ]
  node [
    id 903
    label "whole"
  ]
  node [
    id 904
    label "The_Beatles"
  ]
  node [
    id 905
    label "zabudowania"
  ]
  node [
    id 906
    label "group"
  ]
  node [
    id 907
    label "zespolik"
  ]
  node [
    id 908
    label "schorzenie"
  ]
  node [
    id 909
    label "ro&#347;lina"
  ]
  node [
    id 910
    label "Depeche_Mode"
  ]
  node [
    id 911
    label "batch"
  ]
  node [
    id 912
    label "tkanka"
  ]
  node [
    id 913
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 914
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 915
    label "tw&#243;r"
  ]
  node [
    id 916
    label "organogeneza"
  ]
  node [
    id 917
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 918
    label "struktura_anatomiczna"
  ]
  node [
    id 919
    label "uk&#322;ad"
  ]
  node [
    id 920
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 921
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 922
    label "Izba_Konsyliarska"
  ]
  node [
    id 923
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 924
    label "stomia"
  ]
  node [
    id 925
    label "dekortykacja"
  ]
  node [
    id 926
    label "okolica"
  ]
  node [
    id 927
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 928
    label "Komitet_Region&#243;w"
  ]
  node [
    id 929
    label "subcommittee"
  ]
  node [
    id 930
    label "nemezis"
  ]
  node [
    id 931
    label "konsekwencja"
  ]
  node [
    id 932
    label "punishment"
  ]
  node [
    id 933
    label "righteousness"
  ]
  node [
    id 934
    label "roboty_przymusowe"
  ]
  node [
    id 935
    label "odczuwa&#263;"
  ]
  node [
    id 936
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 937
    label "skrupienie_si&#281;"
  ]
  node [
    id 938
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 939
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 940
    label "odczucie"
  ]
  node [
    id 941
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 942
    label "koszula_Dejaniry"
  ]
  node [
    id 943
    label "odczuwanie"
  ]
  node [
    id 944
    label "event"
  ]
  node [
    id 945
    label "rezultat"
  ]
  node [
    id 946
    label "skrupianie_si&#281;"
  ]
  node [
    id 947
    label "odczu&#263;"
  ]
  node [
    id 948
    label "charakterystyka"
  ]
  node [
    id 949
    label "m&#322;ot"
  ]
  node [
    id 950
    label "znak"
  ]
  node [
    id 951
    label "drzewo"
  ]
  node [
    id 952
    label "attribute"
  ]
  node [
    id 953
    label "marka"
  ]
  node [
    id 954
    label "kara"
  ]
  node [
    id 955
    label "w_prawo"
  ]
  node [
    id 956
    label "s&#322;uszny"
  ]
  node [
    id 957
    label "naturalny"
  ]
  node [
    id 958
    label "chwalebny"
  ]
  node [
    id 959
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 960
    label "zgodnie_z_prawem"
  ]
  node [
    id 961
    label "zacny"
  ]
  node [
    id 962
    label "moralny"
  ]
  node [
    id 963
    label "prawicowy"
  ]
  node [
    id 964
    label "na_prawo"
  ]
  node [
    id 965
    label "cnotliwy"
  ]
  node [
    id 966
    label "legalny"
  ]
  node [
    id 967
    label "z_prawa"
  ]
  node [
    id 968
    label "gajny"
  ]
  node [
    id 969
    label "legalnie"
  ]
  node [
    id 970
    label "pochwalny"
  ]
  node [
    id 971
    label "wspania&#322;y"
  ]
  node [
    id 972
    label "szlachetny"
  ]
  node [
    id 973
    label "powa&#380;ny"
  ]
  node [
    id 974
    label "chwalebnie"
  ]
  node [
    id 975
    label "moralnie"
  ]
  node [
    id 976
    label "warto&#347;ciowy"
  ]
  node [
    id 977
    label "etycznie"
  ]
  node [
    id 978
    label "dobry"
  ]
  node [
    id 979
    label "s&#322;usznie"
  ]
  node [
    id 980
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 981
    label "zasadny"
  ]
  node [
    id 982
    label "nale&#380;yty"
  ]
  node [
    id 983
    label "prawdziwy"
  ]
  node [
    id 984
    label "solidny"
  ]
  node [
    id 985
    label "skromny"
  ]
  node [
    id 986
    label "niewinny"
  ]
  node [
    id 987
    label "cny"
  ]
  node [
    id 988
    label "cnotliwie"
  ]
  node [
    id 989
    label "prostolinijny"
  ]
  node [
    id 990
    label "zacnie"
  ]
  node [
    id 991
    label "szczery"
  ]
  node [
    id 992
    label "zrozumia&#322;y"
  ]
  node [
    id 993
    label "immanentny"
  ]
  node [
    id 994
    label "zwyczajny"
  ]
  node [
    id 995
    label "bezsporny"
  ]
  node [
    id 996
    label "organicznie"
  ]
  node [
    id 997
    label "pierwotny"
  ]
  node [
    id 998
    label "neutralny"
  ]
  node [
    id 999
    label "normalny"
  ]
  node [
    id 1000
    label "rzeczywisty"
  ]
  node [
    id 1001
    label "naturalnie"
  ]
  node [
    id 1002
    label "prawicowo"
  ]
  node [
    id 1003
    label "prawoskr&#281;tny"
  ]
  node [
    id 1004
    label "konserwatywny"
  ]
  node [
    id 1005
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 1006
    label "cz&#322;owiekowate"
  ]
  node [
    id 1007
    label "konsument"
  ]
  node [
    id 1008
    label "istota_&#380;ywa"
  ]
  node [
    id 1009
    label "pracownik"
  ]
  node [
    id 1010
    label "Chocho&#322;"
  ]
  node [
    id 1011
    label "Herkules_Poirot"
  ]
  node [
    id 1012
    label "Edyp"
  ]
  node [
    id 1013
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1014
    label "Harry_Potter"
  ]
  node [
    id 1015
    label "Casanova"
  ]
  node [
    id 1016
    label "Gargantua"
  ]
  node [
    id 1017
    label "Zgredek"
  ]
  node [
    id 1018
    label "Winnetou"
  ]
  node [
    id 1019
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1020
    label "Dulcynea"
  ]
  node [
    id 1021
    label "person"
  ]
  node [
    id 1022
    label "Sherlock_Holmes"
  ]
  node [
    id 1023
    label "Quasimodo"
  ]
  node [
    id 1024
    label "Plastu&#347;"
  ]
  node [
    id 1025
    label "Faust"
  ]
  node [
    id 1026
    label "Wallenrod"
  ]
  node [
    id 1027
    label "Dwukwiat"
  ]
  node [
    id 1028
    label "Don_Juan"
  ]
  node [
    id 1029
    label "Don_Kiszot"
  ]
  node [
    id 1030
    label "Hamlet"
  ]
  node [
    id 1031
    label "Werter"
  ]
  node [
    id 1032
    label "istota"
  ]
  node [
    id 1033
    label "Szwejk"
  ]
  node [
    id 1034
    label "doros&#322;y"
  ]
  node [
    id 1035
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 1036
    label "jajko"
  ]
  node [
    id 1037
    label "rodzic"
  ]
  node [
    id 1038
    label "wapniaki"
  ]
  node [
    id 1039
    label "zwierzchnik"
  ]
  node [
    id 1040
    label "feuda&#322;"
  ]
  node [
    id 1041
    label "starzec"
  ]
  node [
    id 1042
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 1043
    label "zawodnik"
  ]
  node [
    id 1044
    label "komendancja"
  ]
  node [
    id 1045
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 1046
    label "de-escalation"
  ]
  node [
    id 1047
    label "os&#322;abienie"
  ]
  node [
    id 1048
    label "kondycja_fizyczna"
  ]
  node [
    id 1049
    label "os&#322;abi&#263;"
  ]
  node [
    id 1050
    label "debilitation"
  ]
  node [
    id 1051
    label "zdrowie"
  ]
  node [
    id 1052
    label "zmniejszanie"
  ]
  node [
    id 1053
    label "s&#322;abszy"
  ]
  node [
    id 1054
    label "pogarszanie"
  ]
  node [
    id 1055
    label "suppress"
  ]
  node [
    id 1056
    label "robi&#263;"
  ]
  node [
    id 1057
    label "zmniejsza&#263;"
  ]
  node [
    id 1058
    label "bate"
  ]
  node [
    id 1059
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1060
    label "absorption"
  ]
  node [
    id 1061
    label "pobieranie"
  ]
  node [
    id 1062
    label "czerpanie"
  ]
  node [
    id 1063
    label "acquisition"
  ]
  node [
    id 1064
    label "zmienianie"
  ]
  node [
    id 1065
    label "organizm"
  ]
  node [
    id 1066
    label "assimilation"
  ]
  node [
    id 1067
    label "upodabnianie"
  ]
  node [
    id 1068
    label "g&#322;oska"
  ]
  node [
    id 1069
    label "kultura"
  ]
  node [
    id 1070
    label "fonetyka"
  ]
  node [
    id 1071
    label "assimilate"
  ]
  node [
    id 1072
    label "dostosowywa&#263;"
  ]
  node [
    id 1073
    label "dostosowa&#263;"
  ]
  node [
    id 1074
    label "przejmowa&#263;"
  ]
  node [
    id 1075
    label "upodobni&#263;"
  ]
  node [
    id 1076
    label "przej&#261;&#263;"
  ]
  node [
    id 1077
    label "upodabnia&#263;"
  ]
  node [
    id 1078
    label "pobiera&#263;"
  ]
  node [
    id 1079
    label "pobra&#263;"
  ]
  node [
    id 1080
    label "zaistnie&#263;"
  ]
  node [
    id 1081
    label "Osjan"
  ]
  node [
    id 1082
    label "kto&#347;"
  ]
  node [
    id 1083
    label "wygl&#261;d"
  ]
  node [
    id 1084
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1085
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1086
    label "trim"
  ]
  node [
    id 1087
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1088
    label "Aspazja"
  ]
  node [
    id 1089
    label "punkt_widzenia"
  ]
  node [
    id 1090
    label "kompleksja"
  ]
  node [
    id 1091
    label "wytrzyma&#263;"
  ]
  node [
    id 1092
    label "formacja"
  ]
  node [
    id 1093
    label "pozosta&#263;"
  ]
  node [
    id 1094
    label "point"
  ]
  node [
    id 1095
    label "go&#347;&#263;"
  ]
  node [
    id 1096
    label "figure"
  ]
  node [
    id 1097
    label "typ"
  ]
  node [
    id 1098
    label "spos&#243;b"
  ]
  node [
    id 1099
    label "mildew"
  ]
  node [
    id 1100
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1101
    label "ideal"
  ]
  node [
    id 1102
    label "rule"
  ]
  node [
    id 1103
    label "ruch"
  ]
  node [
    id 1104
    label "dekal"
  ]
  node [
    id 1105
    label "motyw"
  ]
  node [
    id 1106
    label "pryncypa&#322;"
  ]
  node [
    id 1107
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1108
    label "kszta&#322;t"
  ]
  node [
    id 1109
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1110
    label "wiedza"
  ]
  node [
    id 1111
    label "kierowa&#263;"
  ]
  node [
    id 1112
    label "alkohol"
  ]
  node [
    id 1113
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1114
    label "&#380;ycie"
  ]
  node [
    id 1115
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1116
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1117
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1118
    label "sztuka"
  ]
  node [
    id 1119
    label "dekiel"
  ]
  node [
    id 1120
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1121
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1122
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1123
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1124
    label "noosfera"
  ]
  node [
    id 1125
    label "byd&#322;o"
  ]
  node [
    id 1126
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1127
    label "makrocefalia"
  ]
  node [
    id 1128
    label "obiekt"
  ]
  node [
    id 1129
    label "ucho"
  ]
  node [
    id 1130
    label "g&#243;ra"
  ]
  node [
    id 1131
    label "m&#243;zg"
  ]
  node [
    id 1132
    label "kierownictwo"
  ]
  node [
    id 1133
    label "fryzura"
  ]
  node [
    id 1134
    label "umys&#322;"
  ]
  node [
    id 1135
    label "cz&#322;onek"
  ]
  node [
    id 1136
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1137
    label "czaszka"
  ]
  node [
    id 1138
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1139
    label "dziedzina"
  ]
  node [
    id 1140
    label "hipnotyzowanie"
  ]
  node [
    id 1141
    label "&#347;lad"
  ]
  node [
    id 1142
    label "docieranie"
  ]
  node [
    id 1143
    label "natural_process"
  ]
  node [
    id 1144
    label "reakcja_chemiczna"
  ]
  node [
    id 1145
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1146
    label "lobbysta"
  ]
  node [
    id 1147
    label "allochoria"
  ]
  node [
    id 1148
    label "fotograf"
  ]
  node [
    id 1149
    label "malarz"
  ]
  node [
    id 1150
    label "artysta"
  ]
  node [
    id 1151
    label "p&#322;aszczyzna"
  ]
  node [
    id 1152
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1153
    label "bierka_szachowa"
  ]
  node [
    id 1154
    label "obiekt_matematyczny"
  ]
  node [
    id 1155
    label "gestaltyzm"
  ]
  node [
    id 1156
    label "styl"
  ]
  node [
    id 1157
    label "rzecz"
  ]
  node [
    id 1158
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1159
    label "character"
  ]
  node [
    id 1160
    label "rze&#378;ba"
  ]
  node [
    id 1161
    label "stylistyka"
  ]
  node [
    id 1162
    label "antycypacja"
  ]
  node [
    id 1163
    label "ornamentyka"
  ]
  node [
    id 1164
    label "informacja"
  ]
  node [
    id 1165
    label "facet"
  ]
  node [
    id 1166
    label "popis"
  ]
  node [
    id 1167
    label "wiersz"
  ]
  node [
    id 1168
    label "symetria"
  ]
  node [
    id 1169
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1170
    label "karta"
  ]
  node [
    id 1171
    label "shape"
  ]
  node [
    id 1172
    label "podzbi&#243;r"
  ]
  node [
    id 1173
    label "nak&#322;adka"
  ]
  node [
    id 1174
    label "li&#347;&#263;"
  ]
  node [
    id 1175
    label "jama_gard&#322;owa"
  ]
  node [
    id 1176
    label "rezonator"
  ]
  node [
    id 1177
    label "podstawa"
  ]
  node [
    id 1178
    label "base"
  ]
  node [
    id 1179
    label "piek&#322;o"
  ]
  node [
    id 1180
    label "human_body"
  ]
  node [
    id 1181
    label "ofiarowywanie"
  ]
  node [
    id 1182
    label "sfera_afektywna"
  ]
  node [
    id 1183
    label "nekromancja"
  ]
  node [
    id 1184
    label "Po&#347;wist"
  ]
  node [
    id 1185
    label "podekscytowanie"
  ]
  node [
    id 1186
    label "deformowanie"
  ]
  node [
    id 1187
    label "sumienie"
  ]
  node [
    id 1188
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1189
    label "deformowa&#263;"
  ]
  node [
    id 1190
    label "psychika"
  ]
  node [
    id 1191
    label "zjawa"
  ]
  node [
    id 1192
    label "zmar&#322;y"
  ]
  node [
    id 1193
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1194
    label "power"
  ]
  node [
    id 1195
    label "entity"
  ]
  node [
    id 1196
    label "ofiarowywa&#263;"
  ]
  node [
    id 1197
    label "oddech"
  ]
  node [
    id 1198
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1199
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1200
    label "byt"
  ]
  node [
    id 1201
    label "si&#322;a"
  ]
  node [
    id 1202
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1203
    label "ego"
  ]
  node [
    id 1204
    label "ofiarowanie"
  ]
  node [
    id 1205
    label "fizjonomia"
  ]
  node [
    id 1206
    label "kompleks"
  ]
  node [
    id 1207
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1208
    label "T&#281;sknica"
  ]
  node [
    id 1209
    label "ofiarowa&#263;"
  ]
  node [
    id 1210
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1211
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1212
    label "passion"
  ]
  node [
    id 1213
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1214
    label "odbicie"
  ]
  node [
    id 1215
    label "atom"
  ]
  node [
    id 1216
    label "przyroda"
  ]
  node [
    id 1217
    label "Ziemia"
  ]
  node [
    id 1218
    label "kosmos"
  ]
  node [
    id 1219
    label "miniatura"
  ]
  node [
    id 1220
    label "pomieszczenie"
  ]
  node [
    id 1221
    label "amfilada"
  ]
  node [
    id 1222
    label "front"
  ]
  node [
    id 1223
    label "apartment"
  ]
  node [
    id 1224
    label "udost&#281;pnienie"
  ]
  node [
    id 1225
    label "pod&#322;oga"
  ]
  node [
    id 1226
    label "sklepienie"
  ]
  node [
    id 1227
    label "sufit"
  ]
  node [
    id 1228
    label "umieszczenie"
  ]
  node [
    id 1229
    label "zakamarek"
  ]
  node [
    id 1230
    label "oratorium"
  ]
  node [
    id 1231
    label "wirydarz"
  ]
  node [
    id 1232
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 1233
    label "zakon"
  ]
  node [
    id 1234
    label "refektarz"
  ]
  node [
    id 1235
    label "kapitularz"
  ]
  node [
    id 1236
    label "kustodia"
  ]
  node [
    id 1237
    label "&#321;agiewniki"
  ]
  node [
    id 1238
    label "pr&#243;bowanie"
  ]
  node [
    id 1239
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1240
    label "zademonstrowanie"
  ]
  node [
    id 1241
    label "obgadanie"
  ]
  node [
    id 1242
    label "realizacja"
  ]
  node [
    id 1243
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1244
    label "narration"
  ]
  node [
    id 1245
    label "cyrk"
  ]
  node [
    id 1246
    label "theatrical_performance"
  ]
  node [
    id 1247
    label "opisanie"
  ]
  node [
    id 1248
    label "malarstwo"
  ]
  node [
    id 1249
    label "scenografia"
  ]
  node [
    id 1250
    label "teatr"
  ]
  node [
    id 1251
    label "ukazanie"
  ]
  node [
    id 1252
    label "zapoznanie"
  ]
  node [
    id 1253
    label "pokaz"
  ]
  node [
    id 1254
    label "podanie"
  ]
  node [
    id 1255
    label "ods&#322;ona"
  ]
  node [
    id 1256
    label "exhibit"
  ]
  node [
    id 1257
    label "pokazanie"
  ]
  node [
    id 1258
    label "wyst&#261;pienie"
  ]
  node [
    id 1259
    label "przedstawia&#263;"
  ]
  node [
    id 1260
    label "wolty&#380;erka"
  ]
  node [
    id 1261
    label "repryza"
  ]
  node [
    id 1262
    label "ekwilibrystyka"
  ]
  node [
    id 1263
    label "tresura"
  ]
  node [
    id 1264
    label "nied&#378;wiednik"
  ]
  node [
    id 1265
    label "skandal"
  ]
  node [
    id 1266
    label "instytucja"
  ]
  node [
    id 1267
    label "hipodrom"
  ]
  node [
    id 1268
    label "namiot"
  ]
  node [
    id 1269
    label "circus"
  ]
  node [
    id 1270
    label "heca"
  ]
  node [
    id 1271
    label "arena"
  ]
  node [
    id 1272
    label "klownada"
  ]
  node [
    id 1273
    label "akrobacja"
  ]
  node [
    id 1274
    label "amfiteatr"
  ]
  node [
    id 1275
    label "trybuna"
  ]
  node [
    id 1276
    label "portrayal"
  ]
  node [
    id 1277
    label "figura_my&#347;li"
  ]
  node [
    id 1278
    label "zinterpretowanie"
  ]
  node [
    id 1279
    label "portrait"
  ]
  node [
    id 1280
    label "p&#322;&#243;d"
  ]
  node [
    id 1281
    label "work"
  ]
  node [
    id 1282
    label "ustawienie"
  ]
  node [
    id 1283
    label "danie"
  ]
  node [
    id 1284
    label "narrative"
  ]
  node [
    id 1285
    label "pismo"
  ]
  node [
    id 1286
    label "nafaszerowanie"
  ]
  node [
    id 1287
    label "tenis"
  ]
  node [
    id 1288
    label "prayer"
  ]
  node [
    id 1289
    label "siatk&#243;wka"
  ]
  node [
    id 1290
    label "pi&#322;ka"
  ]
  node [
    id 1291
    label "give"
  ]
  node [
    id 1292
    label "myth"
  ]
  node [
    id 1293
    label "service"
  ]
  node [
    id 1294
    label "jedzenie"
  ]
  node [
    id 1295
    label "zagranie"
  ]
  node [
    id 1296
    label "poinformowanie"
  ]
  node [
    id 1297
    label "zaserwowanie"
  ]
  node [
    id 1298
    label "opowie&#347;&#263;"
  ]
  node [
    id 1299
    label "udowodnienie"
  ]
  node [
    id 1300
    label "obniesienie"
  ]
  node [
    id 1301
    label "meaning"
  ]
  node [
    id 1302
    label "nauczenie"
  ]
  node [
    id 1303
    label "wczytanie"
  ]
  node [
    id 1304
    label "wyra&#380;enie"
  ]
  node [
    id 1305
    label "zrobienie"
  ]
  node [
    id 1306
    label "narz&#281;dzie"
  ]
  node [
    id 1307
    label "nature"
  ]
  node [
    id 1308
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1309
    label "representation"
  ]
  node [
    id 1310
    label "zawarcie"
  ]
  node [
    id 1311
    label "obznajomienie"
  ]
  node [
    id 1312
    label "umo&#380;liwienie"
  ]
  node [
    id 1313
    label "knowing"
  ]
  node [
    id 1314
    label "nak&#322;onienie"
  ]
  node [
    id 1315
    label "case"
  ]
  node [
    id 1316
    label "odst&#261;pienie"
  ]
  node [
    id 1317
    label "naznaczenie"
  ]
  node [
    id 1318
    label "wyst&#281;p"
  ]
  node [
    id 1319
    label "happening"
  ]
  node [
    id 1320
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1321
    label "porobienie_si&#281;"
  ]
  node [
    id 1322
    label "wyj&#347;cie"
  ]
  node [
    id 1323
    label "zrezygnowanie"
  ]
  node [
    id 1324
    label "pojawienie_si&#281;"
  ]
  node [
    id 1325
    label "egress"
  ]
  node [
    id 1326
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 1327
    label "performance"
  ]
  node [
    id 1328
    label "exit"
  ]
  node [
    id 1329
    label "przepisanie_si&#281;"
  ]
  node [
    id 1330
    label "pokaz&#243;wka"
  ]
  node [
    id 1331
    label "impreza"
  ]
  node [
    id 1332
    label "fabrication"
  ]
  node [
    id 1333
    label "scheduling"
  ]
  node [
    id 1334
    label "operacja"
  ]
  node [
    id 1335
    label "dzie&#322;o"
  ]
  node [
    id 1336
    label "kreacja"
  ]
  node [
    id 1337
    label "monta&#380;"
  ]
  node [
    id 1338
    label "postprodukcja"
  ]
  node [
    id 1339
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1340
    label "podwy&#380;szenie"
  ]
  node [
    id 1341
    label "kurtyna"
  ]
  node [
    id 1342
    label "widzownia"
  ]
  node [
    id 1343
    label "sznurownia"
  ]
  node [
    id 1344
    label "dramaturgy"
  ]
  node [
    id 1345
    label "sphere"
  ]
  node [
    id 1346
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1347
    label "budka_suflera"
  ]
  node [
    id 1348
    label "epizod"
  ]
  node [
    id 1349
    label "film"
  ]
  node [
    id 1350
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1351
    label "kiesze&#324;"
  ]
  node [
    id 1352
    label "stadium"
  ]
  node [
    id 1353
    label "podest"
  ]
  node [
    id 1354
    label "horyzont"
  ]
  node [
    id 1355
    label "teren"
  ]
  node [
    id 1356
    label "proscenium"
  ]
  node [
    id 1357
    label "nadscenie"
  ]
  node [
    id 1358
    label "antyteatr"
  ]
  node [
    id 1359
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 1360
    label "mansjon"
  ]
  node [
    id 1361
    label "modelatornia"
  ]
  node [
    id 1362
    label "uprawienie"
  ]
  node [
    id 1363
    label "dialog"
  ]
  node [
    id 1364
    label "p&#322;osa"
  ]
  node [
    id 1365
    label "wykonywanie"
  ]
  node [
    id 1366
    label "ziemia"
  ]
  node [
    id 1367
    label "wykonywa&#263;"
  ]
  node [
    id 1368
    label "czyn"
  ]
  node [
    id 1369
    label "scenariusz"
  ]
  node [
    id 1370
    label "pole"
  ]
  node [
    id 1371
    label "gospodarstwo"
  ]
  node [
    id 1372
    label "uprawi&#263;"
  ]
  node [
    id 1373
    label "zreinterpretowa&#263;"
  ]
  node [
    id 1374
    label "zastosowanie"
  ]
  node [
    id 1375
    label "reinterpretowa&#263;"
  ]
  node [
    id 1376
    label "wrench"
  ]
  node [
    id 1377
    label "irygowanie"
  ]
  node [
    id 1378
    label "irygowa&#263;"
  ]
  node [
    id 1379
    label "zreinterpretowanie"
  ]
  node [
    id 1380
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 1381
    label "gra&#263;"
  ]
  node [
    id 1382
    label "aktorstwo"
  ]
  node [
    id 1383
    label "kostium"
  ]
  node [
    id 1384
    label "zagon"
  ]
  node [
    id 1385
    label "zagra&#263;"
  ]
  node [
    id 1386
    label "reinterpretowanie"
  ]
  node [
    id 1387
    label "sk&#322;ad"
  ]
  node [
    id 1388
    label "tekst"
  ]
  node [
    id 1389
    label "radlina"
  ]
  node [
    id 1390
    label "granie"
  ]
  node [
    id 1391
    label "play"
  ]
  node [
    id 1392
    label "gra"
  ]
  node [
    id 1393
    label "deski"
  ]
  node [
    id 1394
    label "sala"
  ]
  node [
    id 1395
    label "literatura"
  ]
  node [
    id 1396
    label "dekoratornia"
  ]
  node [
    id 1397
    label "podawa&#263;"
  ]
  node [
    id 1398
    label "display"
  ]
  node [
    id 1399
    label "pokazywa&#263;"
  ]
  node [
    id 1400
    label "demonstrowa&#263;"
  ]
  node [
    id 1401
    label "zapoznawa&#263;"
  ]
  node [
    id 1402
    label "opisywa&#263;"
  ]
  node [
    id 1403
    label "ukazywa&#263;"
  ]
  node [
    id 1404
    label "represent"
  ]
  node [
    id 1405
    label "zg&#322;asza&#263;"
  ]
  node [
    id 1406
    label "typify"
  ]
  node [
    id 1407
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1408
    label "attest"
  ]
  node [
    id 1409
    label "stanowi&#263;"
  ]
  node [
    id 1410
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1411
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 1412
    label "sprawdza&#263;"
  ]
  node [
    id 1413
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1414
    label "feel"
  ]
  node [
    id 1415
    label "try"
  ]
  node [
    id 1416
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 1417
    label "kosztowa&#263;"
  ]
  node [
    id 1418
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 1419
    label "medialno&#347;&#263;"
  ]
  node [
    id 1420
    label "ukaza&#263;"
  ]
  node [
    id 1421
    label "pokaza&#263;"
  ]
  node [
    id 1422
    label "poda&#263;"
  ]
  node [
    id 1423
    label "zapozna&#263;"
  ]
  node [
    id 1424
    label "zademonstrowa&#263;"
  ]
  node [
    id 1425
    label "opisa&#263;"
  ]
  node [
    id 1426
    label "badanie"
  ]
  node [
    id 1427
    label "podejmowanie"
  ]
  node [
    id 1428
    label "usi&#322;owanie"
  ]
  node [
    id 1429
    label "tasting"
  ]
  node [
    id 1430
    label "kiperstwo"
  ]
  node [
    id 1431
    label "staranie_si&#281;"
  ]
  node [
    id 1432
    label "przygotowywanie_si&#281;"
  ]
  node [
    id 1433
    label "essay"
  ]
  node [
    id 1434
    label "opisywanie"
  ]
  node [
    id 1435
    label "bycie"
  ]
  node [
    id 1436
    label "obgadywanie"
  ]
  node [
    id 1437
    label "wyst&#281;powanie"
  ]
  node [
    id 1438
    label "ukazywanie"
  ]
  node [
    id 1439
    label "demonstrowanie"
  ]
  node [
    id 1440
    label "presentation"
  ]
  node [
    id 1441
    label "syntetyzm"
  ]
  node [
    id 1442
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1443
    label "skr&#243;t_perspektywiczny"
  ]
  node [
    id 1444
    label "linearyzm"
  ]
  node [
    id 1445
    label "tempera"
  ]
  node [
    id 1446
    label "gwasz"
  ]
  node [
    id 1447
    label "plastyka"
  ]
  node [
    id 1448
    label "skrytykowanie"
  ]
  node [
    id 1449
    label "temat"
  ]
  node [
    id 1450
    label "smear"
  ]
  node [
    id 1451
    label "sformu&#322;owanie"
  ]
  node [
    id 1452
    label "discussion"
  ]
  node [
    id 1453
    label "wypowied&#378;"
  ]
  node [
    id 1454
    label "message"
  ]
  node [
    id 1455
    label "korespondent"
  ]
  node [
    id 1456
    label "sprawko"
  ]
  node [
    id 1457
    label "pos&#322;uchanie"
  ]
  node [
    id 1458
    label "s&#261;d"
  ]
  node [
    id 1459
    label "sparafrazowanie"
  ]
  node [
    id 1460
    label "strawestowa&#263;"
  ]
  node [
    id 1461
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1462
    label "trawestowa&#263;"
  ]
  node [
    id 1463
    label "sparafrazowa&#263;"
  ]
  node [
    id 1464
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1465
    label "parafrazowanie"
  ]
  node [
    id 1466
    label "ozdobnik"
  ]
  node [
    id 1467
    label "delimitacja"
  ]
  node [
    id 1468
    label "parafrazowa&#263;"
  ]
  node [
    id 1469
    label "stylizacja"
  ]
  node [
    id 1470
    label "komunikat"
  ]
  node [
    id 1471
    label "trawestowanie"
  ]
  node [
    id 1472
    label "strawestowanie"
  ]
  node [
    id 1473
    label "relacja"
  ]
  node [
    id 1474
    label "reporter"
  ]
  node [
    id 1475
    label "i"
  ]
  node [
    id 1476
    label "ojciec"
  ]
  node [
    id 1477
    label "sejmowy"
  ]
  node [
    id 1478
    label "&#347;ledczy"
  ]
  node [
    id 1479
    label "Roberto"
  ]
  node [
    id 1480
    label "w&#281;grzyn"
  ]
  node [
    id 1481
    label "Roberta"
  ]
  node [
    id 1482
    label "biuro"
  ]
  node [
    id 1483
    label "analiza"
  ]
  node [
    id 1484
    label "Jaros&#322;awa"
  ]
  node [
    id 1485
    label "Kalinowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 1476
  ]
  edge [
    source 8
    target 1477
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 1478
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 222
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 844
  ]
  edge [
    source 14
    target 845
  ]
  edge [
    source 14
    target 846
  ]
  edge [
    source 14
    target 847
  ]
  edge [
    source 14
    target 848
  ]
  edge [
    source 14
    target 849
  ]
  edge [
    source 14
    target 850
  ]
  edge [
    source 14
    target 851
  ]
  edge [
    source 14
    target 852
  ]
  edge [
    source 14
    target 853
  ]
  edge [
    source 14
    target 854
  ]
  edge [
    source 14
    target 855
  ]
  edge [
    source 14
    target 856
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 311
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 886
  ]
  edge [
    source 14
    target 887
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 1475
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 1476
  ]
  edge [
    source 16
    target 1477
  ]
  edge [
    source 16
    target 1478
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 1475
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1475
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 379
  ]
  edge [
    source 19
    target 380
  ]
  edge [
    source 19
    target 381
  ]
  edge [
    source 19
    target 382
  ]
  edge [
    source 19
    target 383
  ]
  edge [
    source 19
    target 384
  ]
  edge [
    source 19
    target 385
  ]
  edge [
    source 19
    target 386
  ]
  edge [
    source 19
    target 387
  ]
  edge [
    source 19
    target 388
  ]
  edge [
    source 19
    target 389
  ]
  edge [
    source 19
    target 390
  ]
  edge [
    source 19
    target 391
  ]
  edge [
    source 19
    target 392
  ]
  edge [
    source 19
    target 393
  ]
  edge [
    source 19
    target 394
  ]
  edge [
    source 19
    target 395
  ]
  edge [
    source 19
    target 396
  ]
  edge [
    source 19
    target 259
  ]
  edge [
    source 19
    target 397
  ]
  edge [
    source 19
    target 398
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 399
  ]
  edge [
    source 19
    target 400
  ]
  edge [
    source 19
    target 401
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 61
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 48
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 374
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 742
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 588
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 319
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 57
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 602
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 656
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 82
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 594
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 693
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 597
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 358
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1475
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 819
  ]
  edge [
    source 20
    target 1220
  ]
  edge [
    source 20
    target 1221
  ]
  edge [
    source 20
    target 1222
  ]
  edge [
    source 20
    target 1223
  ]
  edge [
    source 20
    target 1224
  ]
  edge [
    source 20
    target 1225
  ]
  edge [
    source 20
    target 693
  ]
  edge [
    source 20
    target 1226
  ]
  edge [
    source 20
    target 1227
  ]
  edge [
    source 20
    target 1228
  ]
  edge [
    source 20
    target 1229
  ]
  edge [
    source 20
    target 1230
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 1231
  ]
  edge [
    source 20
    target 1232
  ]
  edge [
    source 20
    target 1233
  ]
  edge [
    source 20
    target 1234
  ]
  edge [
    source 20
    target 1235
  ]
  edge [
    source 20
    target 1236
  ]
  edge [
    source 20
    target 1237
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1238
  ]
  edge [
    source 21
    target 1239
  ]
  edge [
    source 21
    target 1240
  ]
  edge [
    source 21
    target 538
  ]
  edge [
    source 21
    target 1241
  ]
  edge [
    source 21
    target 1242
  ]
  edge [
    source 21
    target 627
  ]
  edge [
    source 21
    target 1243
  ]
  edge [
    source 21
    target 1244
  ]
  edge [
    source 21
    target 1245
  ]
  edge [
    source 21
    target 588
  ]
  edge [
    source 21
    target 384
  ]
  edge [
    source 21
    target 1246
  ]
  edge [
    source 21
    target 1247
  ]
  edge [
    source 21
    target 1248
  ]
  edge [
    source 21
    target 1249
  ]
  edge [
    source 21
    target 1250
  ]
  edge [
    source 21
    target 1251
  ]
  edge [
    source 21
    target 1252
  ]
  edge [
    source 21
    target 1253
  ]
  edge [
    source 21
    target 1254
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1255
  ]
  edge [
    source 21
    target 1256
  ]
  edge [
    source 21
    target 1257
  ]
  edge [
    source 21
    target 1258
  ]
  edge [
    source 21
    target 577
  ]
  edge [
    source 21
    target 487
  ]
  edge [
    source 21
    target 1259
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 1260
  ]
  edge [
    source 21
    target 1261
  ]
  edge [
    source 21
    target 1262
  ]
  edge [
    source 21
    target 1263
  ]
  edge [
    source 21
    target 1264
  ]
  edge [
    source 21
    target 1265
  ]
  edge [
    source 21
    target 1266
  ]
  edge [
    source 21
    target 1267
  ]
  edge [
    source 21
    target 1268
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 1269
  ]
  edge [
    source 21
    target 1270
  ]
  edge [
    source 21
    target 1271
  ]
  edge [
    source 21
    target 1272
  ]
  edge [
    source 21
    target 1273
  ]
  edge [
    source 21
    target 773
  ]
  edge [
    source 21
    target 1274
  ]
  edge [
    source 21
    target 1275
  ]
  edge [
    source 21
    target 1276
  ]
  edge [
    source 21
    target 1277
  ]
  edge [
    source 21
    target 1278
  ]
  edge [
    source 21
    target 1279
  ]
  edge [
    source 21
    target 82
  ]
  edge [
    source 21
    target 1280
  ]
  edge [
    source 21
    target 1281
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 1282
  ]
  edge [
    source 21
    target 1283
  ]
  edge [
    source 21
    target 1284
  ]
  edge [
    source 21
    target 1285
  ]
  edge [
    source 21
    target 1286
  ]
  edge [
    source 21
    target 1287
  ]
  edge [
    source 21
    target 1288
  ]
  edge [
    source 21
    target 1289
  ]
  edge [
    source 21
    target 1290
  ]
  edge [
    source 21
    target 1291
  ]
  edge [
    source 21
    target 1292
  ]
  edge [
    source 21
    target 1293
  ]
  edge [
    source 21
    target 1294
  ]
  edge [
    source 21
    target 1295
  ]
  edge [
    source 21
    target 1296
  ]
  edge [
    source 21
    target 1297
  ]
  edge [
    source 21
    target 1298
  ]
  edge [
    source 21
    target 673
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 76
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 590
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 53
  ]
  edge [
    source 21
    target 260
  ]
  edge [
    source 21
    target 1307
  ]
  edge [
    source 21
    target 1308
  ]
  edge [
    source 21
    target 1309
  ]
  edge [
    source 21
    target 1310
  ]
  edge [
    source 21
    target 495
  ]
  edge [
    source 21
    target 1311
  ]
  edge [
    source 21
    target 497
  ]
  edge [
    source 21
    target 1312
  ]
  edge [
    source 21
    target 810
  ]
  edge [
    source 21
    target 1313
  ]
  edge [
    source 21
    target 1314
  ]
  edge [
    source 21
    target 1315
  ]
  edge [
    source 21
    target 1316
  ]
  edge [
    source 21
    target 1317
  ]
  edge [
    source 21
    target 1318
  ]
  edge [
    source 21
    target 1319
  ]
  edge [
    source 21
    target 1320
  ]
  edge [
    source 21
    target 1321
  ]
  edge [
    source 21
    target 1322
  ]
  edge [
    source 21
    target 1323
  ]
  edge [
    source 21
    target 326
  ]
  edge [
    source 21
    target 1324
  ]
  edge [
    source 21
    target 486
  ]
  edge [
    source 21
    target 1325
  ]
  edge [
    source 21
    target 708
  ]
  edge [
    source 21
    target 1326
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 1327
  ]
  edge [
    source 21
    target 1328
  ]
  edge [
    source 21
    target 1329
  ]
  edge [
    source 21
    target 1330
  ]
  edge [
    source 21
    target 529
  ]
  edge [
    source 21
    target 642
  ]
  edge [
    source 21
    target 481
  ]
  edge [
    source 21
    target 1331
  ]
  edge [
    source 21
    target 485
  ]
  edge [
    source 21
    target 1332
  ]
  edge [
    source 21
    target 1333
  ]
  edge [
    source 21
    target 1334
  ]
  edge [
    source 21
    target 494
  ]
  edge [
    source 21
    target 1335
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 1336
  ]
  edge [
    source 21
    target 1337
  ]
  edge [
    source 21
    target 1338
  ]
  edge [
    source 21
    target 1339
  ]
  edge [
    source 21
    target 1340
  ]
  edge [
    source 21
    target 1341
  ]
  edge [
    source 21
    target 54
  ]
  edge [
    source 21
    target 1342
  ]
  edge [
    source 21
    target 1343
  ]
  edge [
    source 21
    target 1344
  ]
  edge [
    source 21
    target 1345
  ]
  edge [
    source 21
    target 1346
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1347
  ]
  edge [
    source 21
    target 1348
  ]
  edge [
    source 21
    target 1349
  ]
  edge [
    source 21
    target 646
  ]
  edge [
    source 21
    target 1350
  ]
  edge [
    source 21
    target 1351
  ]
  edge [
    source 21
    target 1352
  ]
  edge [
    source 21
    target 1353
  ]
  edge [
    source 21
    target 1354
  ]
  edge [
    source 21
    target 1355
  ]
  edge [
    source 21
    target 1356
  ]
  edge [
    source 21
    target 1357
  ]
  edge [
    source 21
    target 1358
  ]
  edge [
    source 21
    target 1359
  ]
  edge [
    source 21
    target 1360
  ]
  edge [
    source 21
    target 1361
  ]
  edge [
    source 21
    target 596
  ]
  edge [
    source 21
    target 1362
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1363
  ]
  edge [
    source 21
    target 1364
  ]
  edge [
    source 21
    target 1365
  ]
  edge [
    source 21
    target 606
  ]
  edge [
    source 21
    target 1366
  ]
  edge [
    source 21
    target 1367
  ]
  edge [
    source 21
    target 1368
  ]
  edge [
    source 21
    target 1369
  ]
  edge [
    source 21
    target 1370
  ]
  edge [
    source 21
    target 1371
  ]
  edge [
    source 21
    target 1372
  ]
  edge [
    source 21
    target 513
  ]
  edge [
    source 21
    target 1373
  ]
  edge [
    source 21
    target 1374
  ]
  edge [
    source 21
    target 1375
  ]
  edge [
    source 21
    target 1376
  ]
  edge [
    source 21
    target 1377
  ]
  edge [
    source 21
    target 849
  ]
  edge [
    source 21
    target 1378
  ]
  edge [
    source 21
    target 1379
  ]
  edge [
    source 21
    target 738
  ]
  edge [
    source 21
    target 1380
  ]
  edge [
    source 21
    target 1381
  ]
  edge [
    source 21
    target 1382
  ]
  edge [
    source 21
    target 1383
  ]
  edge [
    source 21
    target 1384
  ]
  edge [
    source 21
    target 484
  ]
  edge [
    source 21
    target 1385
  ]
  edge [
    source 21
    target 1386
  ]
  edge [
    source 21
    target 1387
  ]
  edge [
    source 21
    target 1388
  ]
  edge [
    source 21
    target 1389
  ]
  edge [
    source 21
    target 1390
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 742
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 319
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 57
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1391
  ]
  edge [
    source 21
    target 1392
  ]
  edge [
    source 21
    target 1393
  ]
  edge [
    source 21
    target 1394
  ]
  edge [
    source 21
    target 1395
  ]
  edge [
    source 21
    target 1396
  ]
  edge [
    source 21
    target 1397
  ]
  edge [
    source 21
    target 1398
  ]
  edge [
    source 21
    target 1399
  ]
  edge [
    source 21
    target 1400
  ]
  edge [
    source 21
    target 1401
  ]
  edge [
    source 21
    target 1402
  ]
  edge [
    source 21
    target 1403
  ]
  edge [
    source 21
    target 1404
  ]
  edge [
    source 21
    target 1405
  ]
  edge [
    source 21
    target 1406
  ]
  edge [
    source 21
    target 1407
  ]
  edge [
    source 21
    target 1408
  ]
  edge [
    source 21
    target 1409
  ]
  edge [
    source 21
    target 1410
  ]
  edge [
    source 21
    target 1411
  ]
  edge [
    source 21
    target 1412
  ]
  edge [
    source 21
    target 1413
  ]
  edge [
    source 21
    target 1414
  ]
  edge [
    source 21
    target 1415
  ]
  edge [
    source 21
    target 1416
  ]
  edge [
    source 21
    target 1417
  ]
  edge [
    source 21
    target 1418
  ]
  edge [
    source 21
    target 1419
  ]
  edge [
    source 21
    target 1420
  ]
  edge [
    source 21
    target 1421
  ]
  edge [
    source 21
    target 1422
  ]
  edge [
    source 21
    target 1423
  ]
  edge [
    source 21
    target 559
  ]
  edge [
    source 21
    target 560
  ]
  edge [
    source 21
    target 1424
  ]
  edge [
    source 21
    target 561
  ]
  edge [
    source 21
    target 1425
  ]
  edge [
    source 21
    target 1426
  ]
  edge [
    source 21
    target 1427
  ]
  edge [
    source 21
    target 1428
  ]
  edge [
    source 21
    target 1429
  ]
  edge [
    source 21
    target 1430
  ]
  edge [
    source 21
    target 1431
  ]
  edge [
    source 21
    target 353
  ]
  edge [
    source 21
    target 1432
  ]
  edge [
    source 21
    target 1433
  ]
  edge [
    source 21
    target 1434
  ]
  edge [
    source 21
    target 1435
  ]
  edge [
    source 21
    target 1436
  ]
  edge [
    source 21
    target 499
  ]
  edge [
    source 21
    target 1437
  ]
  edge [
    source 21
    target 1438
  ]
  edge [
    source 21
    target 464
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 1439
  ]
  edge [
    source 21
    target 1440
  ]
  edge [
    source 21
    target 555
  ]
  edge [
    source 21
    target 556
  ]
  edge [
    source 21
    target 1441
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 1442
  ]
  edge [
    source 21
    target 1443
  ]
  edge [
    source 21
    target 1444
  ]
  edge [
    source 21
    target 1445
  ]
  edge [
    source 21
    target 1446
  ]
  edge [
    source 21
    target 1447
  ]
  edge [
    source 21
    target 1448
  ]
  edge [
    source 21
    target 1449
  ]
  edge [
    source 21
    target 1450
  ]
  edge [
    source 21
    target 1451
  ]
  edge [
    source 21
    target 1452
  ]
  edge [
    source 22
    target 1453
  ]
  edge [
    source 22
    target 1454
  ]
  edge [
    source 22
    target 1455
  ]
  edge [
    source 22
    target 1456
  ]
  edge [
    source 22
    target 1457
  ]
  edge [
    source 22
    target 1458
  ]
  edge [
    source 22
    target 1459
  ]
  edge [
    source 22
    target 1460
  ]
  edge [
    source 22
    target 1461
  ]
  edge [
    source 22
    target 1462
  ]
  edge [
    source 22
    target 1463
  ]
  edge [
    source 22
    target 1464
  ]
  edge [
    source 22
    target 1451
  ]
  edge [
    source 22
    target 1465
  ]
  edge [
    source 22
    target 1466
  ]
  edge [
    source 22
    target 1467
  ]
  edge [
    source 22
    target 1468
  ]
  edge [
    source 22
    target 1469
  ]
  edge [
    source 22
    target 1470
  ]
  edge [
    source 22
    target 1471
  ]
  edge [
    source 22
    target 1472
  ]
  edge [
    source 22
    target 945
  ]
  edge [
    source 22
    target 1473
  ]
  edge [
    source 22
    target 1474
  ]
  edge [
    source 1476
    target 1477
  ]
  edge [
    source 1476
    target 1478
  ]
  edge [
    source 1477
    target 1478
  ]
  edge [
    source 1477
    target 1482
  ]
  edge [
    source 1477
    target 1483
  ]
  edge [
    source 1479
    target 1480
  ]
  edge [
    source 1480
    target 1481
  ]
  edge [
    source 1482
    target 1483
  ]
  edge [
    source 1484
    target 1485
  ]
]
