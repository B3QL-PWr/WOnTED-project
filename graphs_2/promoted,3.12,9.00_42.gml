graph [
  node [
    id 0
    label "ksi&#261;&#380;&#281;"
    origin "text"
  ]
  node [
    id 1
    label "koronny"
    origin "text"
  ]
  node [
    id 2
    label "arabia"
    origin "text"
  ]
  node [
    id 3
    label "saudyjski"
    origin "text"
  ]
  node [
    id 4
    label "mohammad"
    origin "text"
  ]
  node [
    id 5
    label "bin"
    origin "text"
  ]
  node [
    id 6
    label "salman"
    origin "text"
  ]
  node [
    id 7
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przynajmniej"
    origin "text"
  ]
  node [
    id 9
    label "wiadomo&#347;ci"
    origin "text"
  ]
  node [
    id 10
    label "swoje"
    origin "text"
  ]
  node [
    id 11
    label "doradca"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "nadzorowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "operacja"
    origin "text"
  ]
  node [
    id 15
    label "morderstwo"
    origin "text"
  ]
  node [
    id 16
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 17
    label "d&#380;amala"
    origin "text"
  ]
  node [
    id 18
    label "chaszukd&#380;iego"
    origin "text"
  ]
  node [
    id 19
    label "zaledwie"
    origin "text"
  ]
  node [
    id 20
    label "kilka"
    origin "text"
  ]
  node [
    id 21
    label "godzina"
    origin "text"
  ]
  node [
    id 22
    label "przed"
    origin "text"
  ]
  node [
    id 23
    label "zbrodnia"
    origin "text"
  ]
  node [
    id 24
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wall"
    origin "text"
  ]
  node [
    id 26
    label "street"
    origin "text"
  ]
  node [
    id 27
    label "journal"
    origin "text"
  ]
  node [
    id 28
    label "powo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "si&#281;"
    origin "text"
  ]
  node [
    id 30
    label "dokument"
    origin "text"
  ]
  node [
    id 31
    label "arystokrata"
  ]
  node [
    id 32
    label "fircyk"
  ]
  node [
    id 33
    label "tytu&#322;"
  ]
  node [
    id 34
    label "Herman"
  ]
  node [
    id 35
    label "Bismarck"
  ]
  node [
    id 36
    label "w&#322;adca"
  ]
  node [
    id 37
    label "Kazimierz_II_Sprawiedliwy"
  ]
  node [
    id 38
    label "Hamlet"
  ]
  node [
    id 39
    label "kochanie"
  ]
  node [
    id 40
    label "Piast"
  ]
  node [
    id 41
    label "Mieszko_I"
  ]
  node [
    id 42
    label "arystokracja"
  ]
  node [
    id 43
    label "szlachcic"
  ]
  node [
    id 44
    label "rz&#261;dzenie"
  ]
  node [
    id 45
    label "przyw&#243;dca"
  ]
  node [
    id 46
    label "w&#322;odarz"
  ]
  node [
    id 47
    label "Midas"
  ]
  node [
    id 48
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 49
    label "narcyz"
  ]
  node [
    id 50
    label "cz&#322;owiek"
  ]
  node [
    id 51
    label "elegant"
  ]
  node [
    id 52
    label "sza&#322;aput"
  ]
  node [
    id 53
    label "istota_&#380;ywa"
  ]
  node [
    id 54
    label "luzak"
  ]
  node [
    id 55
    label "debit"
  ]
  node [
    id 56
    label "redaktor"
  ]
  node [
    id 57
    label "druk"
  ]
  node [
    id 58
    label "publikacja"
  ]
  node [
    id 59
    label "nadtytu&#322;"
  ]
  node [
    id 60
    label "szata_graficzna"
  ]
  node [
    id 61
    label "tytulatura"
  ]
  node [
    id 62
    label "wydawa&#263;"
  ]
  node [
    id 63
    label "elevation"
  ]
  node [
    id 64
    label "wyda&#263;"
  ]
  node [
    id 65
    label "mianowaniec"
  ]
  node [
    id 66
    label "poster"
  ]
  node [
    id 67
    label "nazwa"
  ]
  node [
    id 68
    label "podtytu&#322;"
  ]
  node [
    id 69
    label "mi&#322;owanie"
  ]
  node [
    id 70
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 71
    label "love"
  ]
  node [
    id 72
    label "zwrot"
  ]
  node [
    id 73
    label "chowanie"
  ]
  node [
    id 74
    label "czucie"
  ]
  node [
    id 75
    label "patrzenie_"
  ]
  node [
    id 76
    label "Szekspir"
  ]
  node [
    id 77
    label "Piastowie"
  ]
  node [
    id 78
    label "najwa&#380;niejszy"
  ]
  node [
    id 79
    label "arabski"
  ]
  node [
    id 80
    label "d&#380;ellaba"
  ]
  node [
    id 81
    label "&#380;uaw"
  ]
  node [
    id 82
    label "abaja"
  ]
  node [
    id 83
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 84
    label "harira"
  ]
  node [
    id 85
    label "typowy"
  ]
  node [
    id 86
    label "arabsko"
  ]
  node [
    id 87
    label "Arabic"
  ]
  node [
    id 88
    label "po_arabsku"
  ]
  node [
    id 89
    label "agal"
  ]
  node [
    id 90
    label "druz"
  ]
  node [
    id 91
    label "j&#281;zyk"
  ]
  node [
    id 92
    label "j&#281;zyk_semicki"
  ]
  node [
    id 93
    label "taniec_brzucha"
  ]
  node [
    id 94
    label "nakaza&#263;"
  ]
  node [
    id 95
    label "przekaza&#263;"
  ]
  node [
    id 96
    label "ship"
  ]
  node [
    id 97
    label "post"
  ]
  node [
    id 98
    label "line"
  ]
  node [
    id 99
    label "wytworzy&#263;"
  ]
  node [
    id 100
    label "convey"
  ]
  node [
    id 101
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 102
    label "poleci&#263;"
  ]
  node [
    id 103
    label "order"
  ]
  node [
    id 104
    label "zapakowa&#263;"
  ]
  node [
    id 105
    label "cause"
  ]
  node [
    id 106
    label "manufacture"
  ]
  node [
    id 107
    label "zrobi&#263;"
  ]
  node [
    id 108
    label "sheathe"
  ]
  node [
    id 109
    label "pieni&#261;dze"
  ]
  node [
    id 110
    label "translate"
  ]
  node [
    id 111
    label "give"
  ]
  node [
    id 112
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 113
    label "wyj&#261;&#263;"
  ]
  node [
    id 114
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 115
    label "range"
  ]
  node [
    id 116
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 117
    label "propagate"
  ]
  node [
    id 118
    label "wp&#322;aci&#263;"
  ]
  node [
    id 119
    label "transfer"
  ]
  node [
    id 120
    label "poda&#263;"
  ]
  node [
    id 121
    label "sygna&#322;"
  ]
  node [
    id 122
    label "impart"
  ]
  node [
    id 123
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 124
    label "zachowanie"
  ]
  node [
    id 125
    label "zachowywanie"
  ]
  node [
    id 126
    label "rok_ko&#347;cielny"
  ]
  node [
    id 127
    label "tekst"
  ]
  node [
    id 128
    label "czas"
  ]
  node [
    id 129
    label "praktyka"
  ]
  node [
    id 130
    label "zachowa&#263;"
  ]
  node [
    id 131
    label "zachowywa&#263;"
  ]
  node [
    id 132
    label "czynnik"
  ]
  node [
    id 133
    label "radziciel"
  ]
  node [
    id 134
    label "pomocnik"
  ]
  node [
    id 135
    label "kredens"
  ]
  node [
    id 136
    label "zawodnik"
  ]
  node [
    id 137
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 138
    label "bylina"
  ]
  node [
    id 139
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 140
    label "gracz"
  ]
  node [
    id 141
    label "r&#281;ka"
  ]
  node [
    id 142
    label "pomoc"
  ]
  node [
    id 143
    label "wrzosowate"
  ]
  node [
    id 144
    label "pomagacz"
  ]
  node [
    id 145
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 146
    label "divisor"
  ]
  node [
    id 147
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 148
    label "faktor"
  ]
  node [
    id 149
    label "agent"
  ]
  node [
    id 150
    label "ekspozycja"
  ]
  node [
    id 151
    label "iloczyn"
  ]
  node [
    id 152
    label "pracowa&#263;"
  ]
  node [
    id 153
    label "manipulate"
  ]
  node [
    id 154
    label "endeavor"
  ]
  node [
    id 155
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 156
    label "mie&#263;_miejsce"
  ]
  node [
    id 157
    label "podejmowa&#263;"
  ]
  node [
    id 158
    label "dziama&#263;"
  ]
  node [
    id 159
    label "do"
  ]
  node [
    id 160
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 161
    label "bangla&#263;"
  ]
  node [
    id 162
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 163
    label "work"
  ]
  node [
    id 164
    label "maszyna"
  ]
  node [
    id 165
    label "dzia&#322;a&#263;"
  ]
  node [
    id 166
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 167
    label "tryb"
  ]
  node [
    id 168
    label "funkcjonowa&#263;"
  ]
  node [
    id 169
    label "praca"
  ]
  node [
    id 170
    label "proces_my&#347;lowy"
  ]
  node [
    id 171
    label "liczenie"
  ]
  node [
    id 172
    label "czyn"
  ]
  node [
    id 173
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 174
    label "supremum"
  ]
  node [
    id 175
    label "laparotomia"
  ]
  node [
    id 176
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 177
    label "jednostka"
  ]
  node [
    id 178
    label "matematyka"
  ]
  node [
    id 179
    label "rzut"
  ]
  node [
    id 180
    label "liczy&#263;"
  ]
  node [
    id 181
    label "strategia"
  ]
  node [
    id 182
    label "torakotomia"
  ]
  node [
    id 183
    label "chirurg"
  ]
  node [
    id 184
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 185
    label "zabieg"
  ]
  node [
    id 186
    label "szew"
  ]
  node [
    id 187
    label "funkcja"
  ]
  node [
    id 188
    label "mathematical_process"
  ]
  node [
    id 189
    label "infimum"
  ]
  node [
    id 190
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 191
    label "plan"
  ]
  node [
    id 192
    label "metoda"
  ]
  node [
    id 193
    label "gra"
  ]
  node [
    id 194
    label "pocz&#261;tki"
  ]
  node [
    id 195
    label "wzorzec_projektowy"
  ]
  node [
    id 196
    label "dziedzina"
  ]
  node [
    id 197
    label "program"
  ]
  node [
    id 198
    label "doktryna"
  ]
  node [
    id 199
    label "wrinkle"
  ]
  node [
    id 200
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 201
    label "rachunek_operatorowy"
  ]
  node [
    id 202
    label "przedmiot"
  ]
  node [
    id 203
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 204
    label "kryptologia"
  ]
  node [
    id 205
    label "logicyzm"
  ]
  node [
    id 206
    label "logika"
  ]
  node [
    id 207
    label "matematyka_czysta"
  ]
  node [
    id 208
    label "forsing"
  ]
  node [
    id 209
    label "modelowanie_matematyczne"
  ]
  node [
    id 210
    label "matma"
  ]
  node [
    id 211
    label "teoria_katastrof"
  ]
  node [
    id 212
    label "kierunek"
  ]
  node [
    id 213
    label "fizyka_matematyczna"
  ]
  node [
    id 214
    label "teoria_graf&#243;w"
  ]
  node [
    id 215
    label "rachunki"
  ]
  node [
    id 216
    label "topologia_algebraiczna"
  ]
  node [
    id 217
    label "matematyka_stosowana"
  ]
  node [
    id 218
    label "leczenie"
  ]
  node [
    id 219
    label "operation"
  ]
  node [
    id 220
    label "act"
  ]
  node [
    id 221
    label "thoracotomy"
  ]
  node [
    id 222
    label "medycyna"
  ]
  node [
    id 223
    label "otwarcie"
  ]
  node [
    id 224
    label "jama_brzuszna"
  ]
  node [
    id 225
    label "laparotomy"
  ]
  node [
    id 226
    label "czynno&#347;&#263;"
  ]
  node [
    id 227
    label "ryba"
  ]
  node [
    id 228
    label "pokolcowate"
  ]
  node [
    id 229
    label "specjalista"
  ]
  node [
    id 230
    label "spawanie"
  ]
  node [
    id 231
    label "chirurgia"
  ]
  node [
    id 232
    label "nitowanie"
  ]
  node [
    id 233
    label "kokon"
  ]
  node [
    id 234
    label "zszycie"
  ]
  node [
    id 235
    label "z&#322;&#261;czenie"
  ]
  node [
    id 236
    label "blizna"
  ]
  node [
    id 237
    label "suture"
  ]
  node [
    id 238
    label "addytywno&#347;&#263;"
  ]
  node [
    id 239
    label "function"
  ]
  node [
    id 240
    label "zastosowanie"
  ]
  node [
    id 241
    label "funkcjonowanie"
  ]
  node [
    id 242
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 243
    label "powierzanie"
  ]
  node [
    id 244
    label "cel"
  ]
  node [
    id 245
    label "przeciwdziedzina"
  ]
  node [
    id 246
    label "awansowa&#263;"
  ]
  node [
    id 247
    label "stawia&#263;"
  ]
  node [
    id 248
    label "wakowa&#263;"
  ]
  node [
    id 249
    label "znaczenie"
  ]
  node [
    id 250
    label "postawi&#263;"
  ]
  node [
    id 251
    label "awansowanie"
  ]
  node [
    id 252
    label "report"
  ]
  node [
    id 253
    label "dyskalkulia"
  ]
  node [
    id 254
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 255
    label "wynagrodzenie"
  ]
  node [
    id 256
    label "osi&#261;ga&#263;"
  ]
  node [
    id 257
    label "wymienia&#263;"
  ]
  node [
    id 258
    label "posiada&#263;"
  ]
  node [
    id 259
    label "wycenia&#263;"
  ]
  node [
    id 260
    label "bra&#263;"
  ]
  node [
    id 261
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 262
    label "mierzy&#263;"
  ]
  node [
    id 263
    label "rachowa&#263;"
  ]
  node [
    id 264
    label "count"
  ]
  node [
    id 265
    label "tell"
  ]
  node [
    id 266
    label "odlicza&#263;"
  ]
  node [
    id 267
    label "dodawa&#263;"
  ]
  node [
    id 268
    label "wyznacza&#263;"
  ]
  node [
    id 269
    label "admit"
  ]
  node [
    id 270
    label "policza&#263;"
  ]
  node [
    id 271
    label "okre&#347;la&#263;"
  ]
  node [
    id 272
    label "przyswoi&#263;"
  ]
  node [
    id 273
    label "ludzko&#347;&#263;"
  ]
  node [
    id 274
    label "one"
  ]
  node [
    id 275
    label "poj&#281;cie"
  ]
  node [
    id 276
    label "ewoluowanie"
  ]
  node [
    id 277
    label "skala"
  ]
  node [
    id 278
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 279
    label "przyswajanie"
  ]
  node [
    id 280
    label "wyewoluowanie"
  ]
  node [
    id 281
    label "reakcja"
  ]
  node [
    id 282
    label "przeliczy&#263;"
  ]
  node [
    id 283
    label "wyewoluowa&#263;"
  ]
  node [
    id 284
    label "ewoluowa&#263;"
  ]
  node [
    id 285
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 286
    label "liczba_naturalna"
  ]
  node [
    id 287
    label "czynnik_biotyczny"
  ]
  node [
    id 288
    label "g&#322;owa"
  ]
  node [
    id 289
    label "figura"
  ]
  node [
    id 290
    label "individual"
  ]
  node [
    id 291
    label "portrecista"
  ]
  node [
    id 292
    label "obiekt"
  ]
  node [
    id 293
    label "przyswaja&#263;"
  ]
  node [
    id 294
    label "przyswojenie"
  ]
  node [
    id 295
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 296
    label "profanum"
  ]
  node [
    id 297
    label "mikrokosmos"
  ]
  node [
    id 298
    label "starzenie_si&#281;"
  ]
  node [
    id 299
    label "duch"
  ]
  node [
    id 300
    label "przeliczanie"
  ]
  node [
    id 301
    label "osoba"
  ]
  node [
    id 302
    label "oddzia&#322;ywanie"
  ]
  node [
    id 303
    label "antropochoria"
  ]
  node [
    id 304
    label "homo_sapiens"
  ]
  node [
    id 305
    label "przelicza&#263;"
  ]
  node [
    id 306
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 307
    label "przeliczenie"
  ]
  node [
    id 308
    label "ograniczenie"
  ]
  node [
    id 309
    label "armia"
  ]
  node [
    id 310
    label "nawr&#243;t_choroby"
  ]
  node [
    id 311
    label "potomstwo"
  ]
  node [
    id 312
    label "odwzorowanie"
  ]
  node [
    id 313
    label "rysunek"
  ]
  node [
    id 314
    label "scene"
  ]
  node [
    id 315
    label "throw"
  ]
  node [
    id 316
    label "float"
  ]
  node [
    id 317
    label "punkt"
  ]
  node [
    id 318
    label "projection"
  ]
  node [
    id 319
    label "injection"
  ]
  node [
    id 320
    label "blow"
  ]
  node [
    id 321
    label "pomys&#322;"
  ]
  node [
    id 322
    label "ruch"
  ]
  node [
    id 323
    label "k&#322;ad"
  ]
  node [
    id 324
    label "mold"
  ]
  node [
    id 325
    label "badanie"
  ]
  node [
    id 326
    label "rachowanie"
  ]
  node [
    id 327
    label "rozliczanie"
  ]
  node [
    id 328
    label "wymienianie"
  ]
  node [
    id 329
    label "oznaczanie"
  ]
  node [
    id 330
    label "wychodzenie"
  ]
  node [
    id 331
    label "naliczenie_si&#281;"
  ]
  node [
    id 332
    label "wyznaczanie"
  ]
  node [
    id 333
    label "dodawanie"
  ]
  node [
    id 334
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 335
    label "bang"
  ]
  node [
    id 336
    label "spodziewanie_si&#281;"
  ]
  node [
    id 337
    label "rozliczenie"
  ]
  node [
    id 338
    label "kwotowanie"
  ]
  node [
    id 339
    label "mierzenie"
  ]
  node [
    id 340
    label "wycenianie"
  ]
  node [
    id 341
    label "branie"
  ]
  node [
    id 342
    label "sprowadzanie"
  ]
  node [
    id 343
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 344
    label "odliczanie"
  ]
  node [
    id 345
    label "przest&#281;pstwo"
  ]
  node [
    id 346
    label "zabicie"
  ]
  node [
    id 347
    label "brudny"
  ]
  node [
    id 348
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 349
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 350
    label "crime"
  ]
  node [
    id 351
    label "sprawstwo"
  ]
  node [
    id 352
    label "&#347;mier&#263;"
  ]
  node [
    id 353
    label "destruction"
  ]
  node [
    id 354
    label "zabrzmienie"
  ]
  node [
    id 355
    label "skrzywdzenie"
  ]
  node [
    id 356
    label "pozabijanie"
  ]
  node [
    id 357
    label "zniszczenie"
  ]
  node [
    id 358
    label "zaszkodzenie"
  ]
  node [
    id 359
    label "usuni&#281;cie"
  ]
  node [
    id 360
    label "spowodowanie"
  ]
  node [
    id 361
    label "killing"
  ]
  node [
    id 362
    label "zdarzenie_si&#281;"
  ]
  node [
    id 363
    label "umarcie"
  ]
  node [
    id 364
    label "granie"
  ]
  node [
    id 365
    label "zamkni&#281;cie"
  ]
  node [
    id 366
    label "compaction"
  ]
  node [
    id 367
    label "publicysta"
  ]
  node [
    id 368
    label "nowiniarz"
  ]
  node [
    id 369
    label "bran&#380;owiec"
  ]
  node [
    id 370
    label "akredytowanie"
  ]
  node [
    id 371
    label "akredytowa&#263;"
  ]
  node [
    id 372
    label "Korwin"
  ]
  node [
    id 373
    label "Michnik"
  ]
  node [
    id 374
    label "Conrad"
  ]
  node [
    id 375
    label "intelektualista"
  ]
  node [
    id 376
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 377
    label "autor"
  ]
  node [
    id 378
    label "Gogol"
  ]
  node [
    id 379
    label "pracownik"
  ]
  node [
    id 380
    label "fachowiec"
  ]
  node [
    id 381
    label "zwi&#261;zkowiec"
  ]
  node [
    id 382
    label "nowinkarz"
  ]
  node [
    id 383
    label "uwiarygodnia&#263;"
  ]
  node [
    id 384
    label "pozwoli&#263;"
  ]
  node [
    id 385
    label "attest"
  ]
  node [
    id 386
    label "uwiarygodni&#263;"
  ]
  node [
    id 387
    label "zezwala&#263;"
  ]
  node [
    id 388
    label "upowa&#380;nia&#263;"
  ]
  node [
    id 389
    label "upowa&#380;ni&#263;"
  ]
  node [
    id 390
    label "zezwalanie"
  ]
  node [
    id 391
    label "uwiarygodnienie"
  ]
  node [
    id 392
    label "akredytowanie_si&#281;"
  ]
  node [
    id 393
    label "upowa&#380;nianie"
  ]
  node [
    id 394
    label "accreditation"
  ]
  node [
    id 395
    label "pozwolenie"
  ]
  node [
    id 396
    label "uwiarygodnianie"
  ]
  node [
    id 397
    label "upowa&#380;nienie"
  ]
  node [
    id 398
    label "&#347;ledziowate"
  ]
  node [
    id 399
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 400
    label "kr&#281;gowiec"
  ]
  node [
    id 401
    label "systemik"
  ]
  node [
    id 402
    label "doniczkowiec"
  ]
  node [
    id 403
    label "mi&#281;so"
  ]
  node [
    id 404
    label "system"
  ]
  node [
    id 405
    label "patroszy&#263;"
  ]
  node [
    id 406
    label "rakowato&#347;&#263;"
  ]
  node [
    id 407
    label "w&#281;dkarstwo"
  ]
  node [
    id 408
    label "ryby"
  ]
  node [
    id 409
    label "fish"
  ]
  node [
    id 410
    label "linia_boczna"
  ]
  node [
    id 411
    label "tar&#322;o"
  ]
  node [
    id 412
    label "wyrostek_filtracyjny"
  ]
  node [
    id 413
    label "m&#281;tnooki"
  ]
  node [
    id 414
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 415
    label "pokrywa_skrzelowa"
  ]
  node [
    id 416
    label "ikra"
  ]
  node [
    id 417
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 418
    label "szczelina_skrzelowa"
  ]
  node [
    id 419
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 420
    label "time"
  ]
  node [
    id 421
    label "doba"
  ]
  node [
    id 422
    label "p&#243;&#322;godzina"
  ]
  node [
    id 423
    label "jednostka_czasu"
  ]
  node [
    id 424
    label "minuta"
  ]
  node [
    id 425
    label "kwadrans"
  ]
  node [
    id 426
    label "poprzedzanie"
  ]
  node [
    id 427
    label "czasoprzestrze&#324;"
  ]
  node [
    id 428
    label "laba"
  ]
  node [
    id 429
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 430
    label "chronometria"
  ]
  node [
    id 431
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 432
    label "rachuba_czasu"
  ]
  node [
    id 433
    label "przep&#322;ywanie"
  ]
  node [
    id 434
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 435
    label "czasokres"
  ]
  node [
    id 436
    label "odczyt"
  ]
  node [
    id 437
    label "chwila"
  ]
  node [
    id 438
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 439
    label "dzieje"
  ]
  node [
    id 440
    label "kategoria_gramatyczna"
  ]
  node [
    id 441
    label "poprzedzenie"
  ]
  node [
    id 442
    label "trawienie"
  ]
  node [
    id 443
    label "pochodzi&#263;"
  ]
  node [
    id 444
    label "period"
  ]
  node [
    id 445
    label "okres_czasu"
  ]
  node [
    id 446
    label "poprzedza&#263;"
  ]
  node [
    id 447
    label "schy&#322;ek"
  ]
  node [
    id 448
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 449
    label "odwlekanie_si&#281;"
  ]
  node [
    id 450
    label "zegar"
  ]
  node [
    id 451
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 452
    label "czwarty_wymiar"
  ]
  node [
    id 453
    label "pochodzenie"
  ]
  node [
    id 454
    label "koniugacja"
  ]
  node [
    id 455
    label "Zeitgeist"
  ]
  node [
    id 456
    label "trawi&#263;"
  ]
  node [
    id 457
    label "pogoda"
  ]
  node [
    id 458
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 459
    label "poprzedzi&#263;"
  ]
  node [
    id 460
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 461
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 462
    label "time_period"
  ]
  node [
    id 463
    label "zapis"
  ]
  node [
    id 464
    label "sekunda"
  ]
  node [
    id 465
    label "stopie&#324;"
  ]
  node [
    id 466
    label "design"
  ]
  node [
    id 467
    label "tydzie&#324;"
  ]
  node [
    id 468
    label "noc"
  ]
  node [
    id 469
    label "dzie&#324;"
  ]
  node [
    id 470
    label "long_time"
  ]
  node [
    id 471
    label "jednostka_geologiczna"
  ]
  node [
    id 472
    label "post&#281;pek"
  ]
  node [
    id 473
    label "action"
  ]
  node [
    id 474
    label "rzecz"
  ]
  node [
    id 475
    label "powiada&#263;"
  ]
  node [
    id 476
    label "komunikowa&#263;"
  ]
  node [
    id 477
    label "inform"
  ]
  node [
    id 478
    label "communicate"
  ]
  node [
    id 479
    label "powodowa&#263;"
  ]
  node [
    id 480
    label "mawia&#263;"
  ]
  node [
    id 481
    label "m&#243;wi&#263;"
  ]
  node [
    id 482
    label "profil"
  ]
  node [
    id 483
    label "facebook"
  ]
  node [
    id 484
    label "listwa"
  ]
  node [
    id 485
    label "profile"
  ]
  node [
    id 486
    label "konto"
  ]
  node [
    id 487
    label "przekr&#243;j"
  ]
  node [
    id 488
    label "podgl&#261;d"
  ]
  node [
    id 489
    label "sylwetka"
  ]
  node [
    id 490
    label "obw&#243;dka"
  ]
  node [
    id 491
    label "dominanta"
  ]
  node [
    id 492
    label "section"
  ]
  node [
    id 493
    label "seria"
  ]
  node [
    id 494
    label "rzeczywisto&#347;&#263;_wirtualna"
  ]
  node [
    id 495
    label "kontur"
  ]
  node [
    id 496
    label "charakter"
  ]
  node [
    id 497
    label "faseta"
  ]
  node [
    id 498
    label "twarz"
  ]
  node [
    id 499
    label "awatar"
  ]
  node [
    id 500
    label "element_konstrukcyjny"
  ]
  node [
    id 501
    label "ozdoba"
  ]
  node [
    id 502
    label "call"
  ]
  node [
    id 503
    label "poborowy"
  ]
  node [
    id 504
    label "wskazywa&#263;"
  ]
  node [
    id 505
    label "warto&#347;&#263;"
  ]
  node [
    id 506
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 507
    label "set"
  ]
  node [
    id 508
    label "podkre&#347;la&#263;"
  ]
  node [
    id 509
    label "by&#263;"
  ]
  node [
    id 510
    label "podawa&#263;"
  ]
  node [
    id 511
    label "wyraz"
  ]
  node [
    id 512
    label "pokazywa&#263;"
  ]
  node [
    id 513
    label "wybiera&#263;"
  ]
  node [
    id 514
    label "signify"
  ]
  node [
    id 515
    label "represent"
  ]
  node [
    id 516
    label "indicate"
  ]
  node [
    id 517
    label "&#380;o&#322;nierz"
  ]
  node [
    id 518
    label "powo&#322;ywanie"
  ]
  node [
    id 519
    label "powo&#322;anie"
  ]
  node [
    id 520
    label "powo&#322;a&#263;"
  ]
  node [
    id 521
    label "rekrut"
  ]
  node [
    id 522
    label "&#347;wiadectwo"
  ]
  node [
    id 523
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 524
    label "wytw&#243;r"
  ]
  node [
    id 525
    label "parafa"
  ]
  node [
    id 526
    label "plik"
  ]
  node [
    id 527
    label "raport&#243;wka"
  ]
  node [
    id 528
    label "utw&#243;r"
  ]
  node [
    id 529
    label "record"
  ]
  node [
    id 530
    label "fascyku&#322;"
  ]
  node [
    id 531
    label "dokumentacja"
  ]
  node [
    id 532
    label "registratura"
  ]
  node [
    id 533
    label "artyku&#322;"
  ]
  node [
    id 534
    label "writing"
  ]
  node [
    id 535
    label "sygnatariusz"
  ]
  node [
    id 536
    label "dow&#243;d"
  ]
  node [
    id 537
    label "o&#347;wiadczenie"
  ]
  node [
    id 538
    label "za&#347;wiadczenie"
  ]
  node [
    id 539
    label "certificate"
  ]
  node [
    id 540
    label "promocja"
  ]
  node [
    id 541
    label "spos&#243;b"
  ]
  node [
    id 542
    label "entrance"
  ]
  node [
    id 543
    label "wpis"
  ]
  node [
    id 544
    label "normalizacja"
  ]
  node [
    id 545
    label "obrazowanie"
  ]
  node [
    id 546
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 547
    label "organ"
  ]
  node [
    id 548
    label "tre&#347;&#263;"
  ]
  node [
    id 549
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 550
    label "part"
  ]
  node [
    id 551
    label "element_anatomiczny"
  ]
  node [
    id 552
    label "komunikat"
  ]
  node [
    id 553
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 554
    label "p&#322;&#243;d"
  ]
  node [
    id 555
    label "rezultat"
  ]
  node [
    id 556
    label "podkatalog"
  ]
  node [
    id 557
    label "nadpisa&#263;"
  ]
  node [
    id 558
    label "nadpisanie"
  ]
  node [
    id 559
    label "bundle"
  ]
  node [
    id 560
    label "folder"
  ]
  node [
    id 561
    label "nadpisywanie"
  ]
  node [
    id 562
    label "paczka"
  ]
  node [
    id 563
    label "nadpisywa&#263;"
  ]
  node [
    id 564
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 565
    label "przedstawiciel"
  ]
  node [
    id 566
    label "wydanie"
  ]
  node [
    id 567
    label "zbi&#243;r"
  ]
  node [
    id 568
    label "torba"
  ]
  node [
    id 569
    label "ekscerpcja"
  ]
  node [
    id 570
    label "materia&#322;"
  ]
  node [
    id 571
    label "operat"
  ]
  node [
    id 572
    label "kosztorys"
  ]
  node [
    id 573
    label "biuro"
  ]
  node [
    id 574
    label "register"
  ]
  node [
    id 575
    label "blok"
  ]
  node [
    id 576
    label "prawda"
  ]
  node [
    id 577
    label "znak_j&#281;zykowy"
  ]
  node [
    id 578
    label "nag&#322;&#243;wek"
  ]
  node [
    id 579
    label "szkic"
  ]
  node [
    id 580
    label "fragment"
  ]
  node [
    id 581
    label "wyr&#243;b"
  ]
  node [
    id 582
    label "rodzajnik"
  ]
  node [
    id 583
    label "towar"
  ]
  node [
    id 584
    label "paragraf"
  ]
  node [
    id 585
    label "paraph"
  ]
  node [
    id 586
    label "podpis"
  ]
  node [
    id 587
    label "Arabia"
  ]
  node [
    id 588
    label "D&#380;amala"
  ]
  node [
    id 589
    label "Chaszukd&#380;iego"
  ]
  node [
    id 590
    label "Wall"
  ]
  node [
    id 591
    label "Street"
  ]
  node [
    id 592
    label "Journal"
  ]
  node [
    id 593
    label "Mohammad"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 30
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 218
  ]
  edge [
    source 14
    target 219
  ]
  edge [
    source 14
    target 220
  ]
  edge [
    source 14
    target 221
  ]
  edge [
    source 14
    target 222
  ]
  edge [
    source 14
    target 223
  ]
  edge [
    source 14
    target 224
  ]
  edge [
    source 14
    target 225
  ]
  edge [
    source 14
    target 226
  ]
  edge [
    source 14
    target 227
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 231
  ]
  edge [
    source 14
    target 232
  ]
  edge [
    source 14
    target 233
  ]
  edge [
    source 14
    target 234
  ]
  edge [
    source 14
    target 235
  ]
  edge [
    source 14
    target 236
  ]
  edge [
    source 14
    target 237
  ]
  edge [
    source 14
    target 238
  ]
  edge [
    source 14
    target 239
  ]
  edge [
    source 14
    target 240
  ]
  edge [
    source 14
    target 241
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 242
  ]
  edge [
    source 14
    target 243
  ]
  edge [
    source 14
    target 244
  ]
  edge [
    source 14
    target 245
  ]
  edge [
    source 14
    target 246
  ]
  edge [
    source 14
    target 247
  ]
  edge [
    source 14
    target 248
  ]
  edge [
    source 14
    target 249
  ]
  edge [
    source 14
    target 250
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 252
  ]
  edge [
    source 14
    target 253
  ]
  edge [
    source 14
    target 254
  ]
  edge [
    source 14
    target 255
  ]
  edge [
    source 14
    target 256
  ]
  edge [
    source 14
    target 257
  ]
  edge [
    source 14
    target 258
  ]
  edge [
    source 14
    target 259
  ]
  edge [
    source 14
    target 260
  ]
  edge [
    source 14
    target 261
  ]
  edge [
    source 14
    target 262
  ]
  edge [
    source 14
    target 263
  ]
  edge [
    source 14
    target 264
  ]
  edge [
    source 14
    target 265
  ]
  edge [
    source 14
    target 266
  ]
  edge [
    source 14
    target 267
  ]
  edge [
    source 14
    target 268
  ]
  edge [
    source 14
    target 269
  ]
  edge [
    source 14
    target 270
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 273
  ]
  edge [
    source 14
    target 274
  ]
  edge [
    source 14
    target 275
  ]
  edge [
    source 14
    target 276
  ]
  edge [
    source 14
    target 277
  ]
  edge [
    source 14
    target 278
  ]
  edge [
    source 14
    target 279
  ]
  edge [
    source 14
    target 280
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 282
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 289
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 291
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 293
  ]
  edge [
    source 14
    target 294
  ]
  edge [
    source 14
    target 295
  ]
  edge [
    source 14
    target 296
  ]
  edge [
    source 14
    target 297
  ]
  edge [
    source 14
    target 298
  ]
  edge [
    source 14
    target 299
  ]
  edge [
    source 14
    target 300
  ]
  edge [
    source 14
    target 301
  ]
  edge [
    source 14
    target 302
  ]
  edge [
    source 14
    target 303
  ]
  edge [
    source 14
    target 304
  ]
  edge [
    source 14
    target 305
  ]
  edge [
    source 14
    target 306
  ]
  edge [
    source 14
    target 307
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 310
  ]
  edge [
    source 14
    target 311
  ]
  edge [
    source 14
    target 312
  ]
  edge [
    source 14
    target 313
  ]
  edge [
    source 14
    target 314
  ]
  edge [
    source 14
    target 315
  ]
  edge [
    source 14
    target 316
  ]
  edge [
    source 14
    target 317
  ]
  edge [
    source 14
    target 318
  ]
  edge [
    source 14
    target 319
  ]
  edge [
    source 14
    target 320
  ]
  edge [
    source 14
    target 321
  ]
  edge [
    source 14
    target 322
  ]
  edge [
    source 14
    target 323
  ]
  edge [
    source 14
    target 324
  ]
  edge [
    source 14
    target 325
  ]
  edge [
    source 14
    target 326
  ]
  edge [
    source 14
    target 327
  ]
  edge [
    source 14
    target 328
  ]
  edge [
    source 14
    target 329
  ]
  edge [
    source 14
    target 330
  ]
  edge [
    source 14
    target 331
  ]
  edge [
    source 14
    target 332
  ]
  edge [
    source 14
    target 333
  ]
  edge [
    source 14
    target 334
  ]
  edge [
    source 14
    target 335
  ]
  edge [
    source 14
    target 336
  ]
  edge [
    source 14
    target 337
  ]
  edge [
    source 14
    target 338
  ]
  edge [
    source 14
    target 339
  ]
  edge [
    source 14
    target 340
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 342
  ]
  edge [
    source 14
    target 343
  ]
  edge [
    source 14
    target 344
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 345
  ]
  edge [
    source 15
    target 346
  ]
  edge [
    source 15
    target 347
  ]
  edge [
    source 15
    target 348
  ]
  edge [
    source 15
    target 349
  ]
  edge [
    source 15
    target 350
  ]
  edge [
    source 15
    target 351
  ]
  edge [
    source 15
    target 352
  ]
  edge [
    source 15
    target 353
  ]
  edge [
    source 15
    target 354
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 356
  ]
  edge [
    source 15
    target 357
  ]
  edge [
    source 15
    target 358
  ]
  edge [
    source 15
    target 359
  ]
  edge [
    source 15
    target 360
  ]
  edge [
    source 15
    target 361
  ]
  edge [
    source 15
    target 362
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 363
  ]
  edge [
    source 15
    target 364
  ]
  edge [
    source 15
    target 365
  ]
  edge [
    source 15
    target 366
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 399
  ]
  edge [
    source 20
    target 400
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 20
    target 401
  ]
  edge [
    source 20
    target 402
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 405
  ]
  edge [
    source 20
    target 406
  ]
  edge [
    source 20
    target 407
  ]
  edge [
    source 20
    target 408
  ]
  edge [
    source 20
    target 409
  ]
  edge [
    source 20
    target 410
  ]
  edge [
    source 20
    target 411
  ]
  edge [
    source 20
    target 412
  ]
  edge [
    source 20
    target 413
  ]
  edge [
    source 20
    target 414
  ]
  edge [
    source 20
    target 415
  ]
  edge [
    source 20
    target 416
  ]
  edge [
    source 20
    target 417
  ]
  edge [
    source 20
    target 418
  ]
  edge [
    source 20
    target 419
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 420
  ]
  edge [
    source 21
    target 421
  ]
  edge [
    source 21
    target 422
  ]
  edge [
    source 21
    target 423
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 424
  ]
  edge [
    source 21
    target 425
  ]
  edge [
    source 21
    target 426
  ]
  edge [
    source 21
    target 427
  ]
  edge [
    source 21
    target 428
  ]
  edge [
    source 21
    target 429
  ]
  edge [
    source 21
    target 430
  ]
  edge [
    source 21
    target 431
  ]
  edge [
    source 21
    target 432
  ]
  edge [
    source 21
    target 433
  ]
  edge [
    source 21
    target 434
  ]
  edge [
    source 21
    target 435
  ]
  edge [
    source 21
    target 436
  ]
  edge [
    source 21
    target 437
  ]
  edge [
    source 21
    target 438
  ]
  edge [
    source 21
    target 439
  ]
  edge [
    source 21
    target 440
  ]
  edge [
    source 21
    target 441
  ]
  edge [
    source 21
    target 442
  ]
  edge [
    source 21
    target 443
  ]
  edge [
    source 21
    target 444
  ]
  edge [
    source 21
    target 445
  ]
  edge [
    source 21
    target 446
  ]
  edge [
    source 21
    target 447
  ]
  edge [
    source 21
    target 448
  ]
  edge [
    source 21
    target 449
  ]
  edge [
    source 21
    target 450
  ]
  edge [
    source 21
    target 451
  ]
  edge [
    source 21
    target 452
  ]
  edge [
    source 21
    target 453
  ]
  edge [
    source 21
    target 454
  ]
  edge [
    source 21
    target 455
  ]
  edge [
    source 21
    target 456
  ]
  edge [
    source 21
    target 457
  ]
  edge [
    source 21
    target 458
  ]
  edge [
    source 21
    target 459
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 21
    target 461
  ]
  edge [
    source 21
    target 462
  ]
  edge [
    source 21
    target 463
  ]
  edge [
    source 21
    target 464
  ]
  edge [
    source 21
    target 177
  ]
  edge [
    source 21
    target 465
  ]
  edge [
    source 21
    target 466
  ]
  edge [
    source 21
    target 467
  ]
  edge [
    source 21
    target 468
  ]
  edge [
    source 21
    target 469
  ]
  edge [
    source 21
    target 470
  ]
  edge [
    source 21
    target 471
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 350
  ]
  edge [
    source 23
    target 472
  ]
  edge [
    source 23
    target 345
  ]
  edge [
    source 23
    target 347
  ]
  edge [
    source 23
    target 348
  ]
  edge [
    source 23
    target 349
  ]
  edge [
    source 23
    target 351
  ]
  edge [
    source 23
    target 473
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 474
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 475
  ]
  edge [
    source 24
    target 476
  ]
  edge [
    source 24
    target 477
  ]
  edge [
    source 24
    target 478
  ]
  edge [
    source 24
    target 479
  ]
  edge [
    source 24
    target 480
  ]
  edge [
    source 24
    target 481
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 482
  ]
  edge [
    source 25
    target 483
  ]
  edge [
    source 25
    target 484
  ]
  edge [
    source 25
    target 485
  ]
  edge [
    source 25
    target 486
  ]
  edge [
    source 25
    target 487
  ]
  edge [
    source 25
    target 488
  ]
  edge [
    source 25
    target 489
  ]
  edge [
    source 25
    target 490
  ]
  edge [
    source 25
    target 491
  ]
  edge [
    source 25
    target 492
  ]
  edge [
    source 25
    target 493
  ]
  edge [
    source 25
    target 494
  ]
  edge [
    source 25
    target 495
  ]
  edge [
    source 25
    target 496
  ]
  edge [
    source 25
    target 497
  ]
  edge [
    source 25
    target 498
  ]
  edge [
    source 25
    target 499
  ]
  edge [
    source 25
    target 500
  ]
  edge [
    source 25
    target 501
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 502
  ]
  edge [
    source 28
    target 503
  ]
  edge [
    source 28
    target 504
  ]
  edge [
    source 28
    target 505
  ]
  edge [
    source 28
    target 506
  ]
  edge [
    source 28
    target 507
  ]
  edge [
    source 28
    target 508
  ]
  edge [
    source 28
    target 509
  ]
  edge [
    source 28
    target 510
  ]
  edge [
    source 28
    target 511
  ]
  edge [
    source 28
    target 512
  ]
  edge [
    source 28
    target 513
  ]
  edge [
    source 28
    target 514
  ]
  edge [
    source 28
    target 515
  ]
  edge [
    source 28
    target 516
  ]
  edge [
    source 28
    target 517
  ]
  edge [
    source 28
    target 518
  ]
  edge [
    source 28
    target 519
  ]
  edge [
    source 28
    target 520
  ]
  edge [
    source 28
    target 521
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 463
  ]
  edge [
    source 30
    target 522
  ]
  edge [
    source 30
    target 523
  ]
  edge [
    source 30
    target 524
  ]
  edge [
    source 30
    target 525
  ]
  edge [
    source 30
    target 526
  ]
  edge [
    source 30
    target 527
  ]
  edge [
    source 30
    target 528
  ]
  edge [
    source 30
    target 529
  ]
  edge [
    source 30
    target 530
  ]
  edge [
    source 30
    target 531
  ]
  edge [
    source 30
    target 532
  ]
  edge [
    source 30
    target 533
  ]
  edge [
    source 30
    target 534
  ]
  edge [
    source 30
    target 535
  ]
  edge [
    source 30
    target 536
  ]
  edge [
    source 30
    target 537
  ]
  edge [
    source 30
    target 538
  ]
  edge [
    source 30
    target 539
  ]
  edge [
    source 30
    target 540
  ]
  edge [
    source 30
    target 541
  ]
  edge [
    source 30
    target 542
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 30
    target 543
  ]
  edge [
    source 30
    target 544
  ]
  edge [
    source 30
    target 545
  ]
  edge [
    source 30
    target 546
  ]
  edge [
    source 30
    target 547
  ]
  edge [
    source 30
    target 548
  ]
  edge [
    source 30
    target 549
  ]
  edge [
    source 30
    target 550
  ]
  edge [
    source 30
    target 551
  ]
  edge [
    source 30
    target 127
  ]
  edge [
    source 30
    target 552
  ]
  edge [
    source 30
    target 553
  ]
  edge [
    source 30
    target 202
  ]
  edge [
    source 30
    target 554
  ]
  edge [
    source 30
    target 163
  ]
  edge [
    source 30
    target 555
  ]
  edge [
    source 30
    target 556
  ]
  edge [
    source 30
    target 557
  ]
  edge [
    source 30
    target 558
  ]
  edge [
    source 30
    target 559
  ]
  edge [
    source 30
    target 560
  ]
  edge [
    source 30
    target 561
  ]
  edge [
    source 30
    target 562
  ]
  edge [
    source 30
    target 563
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 30
    target 564
  ]
  edge [
    source 30
    target 565
  ]
  edge [
    source 30
    target 137
  ]
  edge [
    source 30
    target 566
  ]
  edge [
    source 30
    target 567
  ]
  edge [
    source 30
    target 568
  ]
  edge [
    source 30
    target 569
  ]
  edge [
    source 30
    target 570
  ]
  edge [
    source 30
    target 571
  ]
  edge [
    source 30
    target 572
  ]
  edge [
    source 30
    target 573
  ]
  edge [
    source 30
    target 574
  ]
  edge [
    source 30
    target 575
  ]
  edge [
    source 30
    target 576
  ]
  edge [
    source 30
    target 577
  ]
  edge [
    source 30
    target 578
  ]
  edge [
    source 30
    target 579
  ]
  edge [
    source 30
    target 98
  ]
  edge [
    source 30
    target 580
  ]
  edge [
    source 30
    target 581
  ]
  edge [
    source 30
    target 582
  ]
  edge [
    source 30
    target 583
  ]
  edge [
    source 30
    target 584
  ]
  edge [
    source 30
    target 585
  ]
  edge [
    source 30
    target 586
  ]
  edge [
    source 587
    target 593
  ]
  edge [
    source 588
    target 589
  ]
  edge [
    source 590
    target 591
  ]
  edge [
    source 590
    target 592
  ]
  edge [
    source 591
    target 592
  ]
]
