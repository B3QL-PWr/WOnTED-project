graph [
  node [
    id 0
    label "stanis&#322;aw"
    origin "text"
  ]
  node [
    id 1
    label "gajewski"
    origin "text"
  ]
  node [
    id 2
    label "Stanis&#322;awa"
  ]
  node [
    id 3
    label "Gajewski"
  ]
  node [
    id 4
    label "Wanda"
  ]
  node [
    id 5
    label "zeszyt"
  ]
  node [
    id 6
    label "dom"
  ]
  node [
    id 7
    label "Landau"
  ]
  node [
    id 8
    label "uniwersytet"
  ]
  node [
    id 9
    label "jagiello&#324;ski"
  ]
  node [
    id 10
    label "wyspa"
  ]
  node [
    id 11
    label "Lozanna"
  ]
  node [
    id 12
    label "Joachima"
  ]
  node [
    id 13
    label "Lelewel"
  ]
  node [
    id 14
    label "warszawski"
  ]
  node [
    id 15
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 16
    label "Winawera"
  ]
  node [
    id 17
    label "ii"
  ]
  node [
    id 18
    label "wojna"
  ]
  node [
    id 19
    label "&#347;wiatowy"
  ]
  node [
    id 20
    label "powsta&#263;"
  ]
  node [
    id 21
    label "ministerstwo"
  ]
  node [
    id 22
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 23
    label "Leona"
  ]
  node [
    id 24
    label "Chajna"
  ]
  node [
    id 25
    label "ambasada"
  ]
  node [
    id 26
    label "RP"
  ]
  node [
    id 27
    label "PRL"
  ]
  node [
    id 28
    label "wydzia&#322;"
  ]
  node [
    id 29
    label "konsularny"
  ]
  node [
    id 30
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 31
    label "komisja"
  ]
  node [
    id 32
    label "kraj"
  ]
  node [
    id 33
    label "neutralny"
  ]
  node [
    id 34
    label "prezydium"
  ]
  node [
    id 35
    label "sejm"
  ]
  node [
    id 36
    label "Janina"
  ]
  node [
    id 37
    label "Milejkowsk&#261;"
  ]
  node [
    id 38
    label "Zofia"
  ]
  node [
    id 39
    label "Ch&#261;dzy&#324;skiej"
  ]
  node [
    id 40
    label "legia"
  ]
  node [
    id 41
    label "honorowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
]
