graph [
  node [
    id 0
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dziedzictwo"
    origin "text"
  ]
  node [
    id 2
    label "prlu"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 5
    label "obci&#261;&#380;enie"
    origin "text"
  ]
  node [
    id 6
    label "opozycyjno&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "bycie"
    origin "text"
  ]
  node [
    id 8
    label "opozycja"
    origin "text"
  ]
  node [
    id 9
    label "wyostrza&#263;"
    origin "text"
  ]
  node [
    id 10
    label "promowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "osoba"
    origin "text"
  ]
  node [
    id 12
    label "dobre"
    origin "text"
  ]
  node [
    id 13
    label "destrukcja"
    origin "text"
  ]
  node [
    id 14
    label "krytykowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zerojedynkowy"
    origin "text"
  ]
  node [
    id 16
    label "stawianie"
    origin "text"
  ]
  node [
    id 17
    label "sprawa"
    origin "text"
  ]
  node [
    id 18
    label "ostrz"
    origin "text"
  ]
  node [
    id 19
    label "n&#243;&#380;"
    origin "text"
  ]
  node [
    id 20
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 21
    label "wspiera&#263;"
    origin "text"
  ]
  node [
    id 22
    label "cech"
    origin "text"
  ]
  node [
    id 23
    label "pomaga&#263;"
    origin "text"
  ]
  node [
    id 24
    label "budowa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 26
    label "pan"
    origin "text"
  ]
  node [
    id 27
    label "tata"
    origin "text"
  ]
  node [
    id 28
    label "racja"
    origin "text"
  ]
  node [
    id 29
    label "gaworzy&#263;"
  ]
  node [
    id 30
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 31
    label "remark"
  ]
  node [
    id 32
    label "rozmawia&#263;"
  ]
  node [
    id 33
    label "wyra&#380;a&#263;"
  ]
  node [
    id 34
    label "umie&#263;"
  ]
  node [
    id 35
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 36
    label "dziama&#263;"
  ]
  node [
    id 37
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 38
    label "formu&#322;owa&#263;"
  ]
  node [
    id 39
    label "dysfonia"
  ]
  node [
    id 40
    label "express"
  ]
  node [
    id 41
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 42
    label "talk"
  ]
  node [
    id 43
    label "u&#380;ywa&#263;"
  ]
  node [
    id 44
    label "prawi&#263;"
  ]
  node [
    id 45
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 46
    label "powiada&#263;"
  ]
  node [
    id 47
    label "tell"
  ]
  node [
    id 48
    label "chew_the_fat"
  ]
  node [
    id 49
    label "say"
  ]
  node [
    id 50
    label "j&#281;zyk"
  ]
  node [
    id 51
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 52
    label "informowa&#263;"
  ]
  node [
    id 53
    label "wydobywa&#263;"
  ]
  node [
    id 54
    label "okre&#347;la&#263;"
  ]
  node [
    id 55
    label "korzysta&#263;"
  ]
  node [
    id 56
    label "distribute"
  ]
  node [
    id 57
    label "give"
  ]
  node [
    id 58
    label "bash"
  ]
  node [
    id 59
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 60
    label "doznawa&#263;"
  ]
  node [
    id 61
    label "decydowa&#263;"
  ]
  node [
    id 62
    label "signify"
  ]
  node [
    id 63
    label "style"
  ]
  node [
    id 64
    label "powodowa&#263;"
  ]
  node [
    id 65
    label "komunikowa&#263;"
  ]
  node [
    id 66
    label "inform"
  ]
  node [
    id 67
    label "znaczy&#263;"
  ]
  node [
    id 68
    label "give_voice"
  ]
  node [
    id 69
    label "oznacza&#263;"
  ]
  node [
    id 70
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 71
    label "represent"
  ]
  node [
    id 72
    label "convey"
  ]
  node [
    id 73
    label "arouse"
  ]
  node [
    id 74
    label "robi&#263;"
  ]
  node [
    id 75
    label "determine"
  ]
  node [
    id 76
    label "work"
  ]
  node [
    id 77
    label "reakcja_chemiczna"
  ]
  node [
    id 78
    label "uwydatnia&#263;"
  ]
  node [
    id 79
    label "eksploatowa&#263;"
  ]
  node [
    id 80
    label "uzyskiwa&#263;"
  ]
  node [
    id 81
    label "wydostawa&#263;"
  ]
  node [
    id 82
    label "wyjmowa&#263;"
  ]
  node [
    id 83
    label "train"
  ]
  node [
    id 84
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 85
    label "wydawa&#263;"
  ]
  node [
    id 86
    label "dobywa&#263;"
  ]
  node [
    id 87
    label "ocala&#263;"
  ]
  node [
    id 88
    label "excavate"
  ]
  node [
    id 89
    label "g&#243;rnictwo"
  ]
  node [
    id 90
    label "raise"
  ]
  node [
    id 91
    label "wiedzie&#263;"
  ]
  node [
    id 92
    label "can"
  ]
  node [
    id 93
    label "m&#243;c"
  ]
  node [
    id 94
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 95
    label "rozumie&#263;"
  ]
  node [
    id 96
    label "szczeka&#263;"
  ]
  node [
    id 97
    label "funkcjonowa&#263;"
  ]
  node [
    id 98
    label "mawia&#263;"
  ]
  node [
    id 99
    label "opowiada&#263;"
  ]
  node [
    id 100
    label "chatter"
  ]
  node [
    id 101
    label "niemowl&#281;"
  ]
  node [
    id 102
    label "kosmetyk"
  ]
  node [
    id 103
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 104
    label "stanowisko_archeologiczne"
  ]
  node [
    id 105
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 106
    label "artykulator"
  ]
  node [
    id 107
    label "kod"
  ]
  node [
    id 108
    label "kawa&#322;ek"
  ]
  node [
    id 109
    label "przedmiot"
  ]
  node [
    id 110
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 111
    label "gramatyka"
  ]
  node [
    id 112
    label "stylik"
  ]
  node [
    id 113
    label "przet&#322;umaczenie"
  ]
  node [
    id 114
    label "formalizowanie"
  ]
  node [
    id 115
    label "ssanie"
  ]
  node [
    id 116
    label "ssa&#263;"
  ]
  node [
    id 117
    label "language"
  ]
  node [
    id 118
    label "liza&#263;"
  ]
  node [
    id 119
    label "napisa&#263;"
  ]
  node [
    id 120
    label "konsonantyzm"
  ]
  node [
    id 121
    label "wokalizm"
  ]
  node [
    id 122
    label "pisa&#263;"
  ]
  node [
    id 123
    label "fonetyka"
  ]
  node [
    id 124
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 125
    label "jeniec"
  ]
  node [
    id 126
    label "but"
  ]
  node [
    id 127
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 128
    label "po_koroniarsku"
  ]
  node [
    id 129
    label "kultura_duchowa"
  ]
  node [
    id 130
    label "t&#322;umaczenie"
  ]
  node [
    id 131
    label "m&#243;wienie"
  ]
  node [
    id 132
    label "pype&#263;"
  ]
  node [
    id 133
    label "lizanie"
  ]
  node [
    id 134
    label "pismo"
  ]
  node [
    id 135
    label "formalizowa&#263;"
  ]
  node [
    id 136
    label "organ"
  ]
  node [
    id 137
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 138
    label "rozumienie"
  ]
  node [
    id 139
    label "spos&#243;b"
  ]
  node [
    id 140
    label "makroglosja"
  ]
  node [
    id 141
    label "jama_ustna"
  ]
  node [
    id 142
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 143
    label "formacja_geologiczna"
  ]
  node [
    id 144
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 145
    label "natural_language"
  ]
  node [
    id 146
    label "s&#322;ownictwo"
  ]
  node [
    id 147
    label "urz&#261;dzenie"
  ]
  node [
    id 148
    label "dysphonia"
  ]
  node [
    id 149
    label "dysleksja"
  ]
  node [
    id 150
    label "zachowek"
  ]
  node [
    id 151
    label "mienie"
  ]
  node [
    id 152
    label "wydziedziczenie"
  ]
  node [
    id 153
    label "scheda_spadkowa"
  ]
  node [
    id 154
    label "sukcesja"
  ]
  node [
    id 155
    label "wydziedziczy&#263;"
  ]
  node [
    id 156
    label "prawo"
  ]
  node [
    id 157
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 158
    label "umocowa&#263;"
  ]
  node [
    id 159
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 160
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 161
    label "procesualistyka"
  ]
  node [
    id 162
    label "regu&#322;a_Allena"
  ]
  node [
    id 163
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 164
    label "kryminalistyka"
  ]
  node [
    id 165
    label "struktura"
  ]
  node [
    id 166
    label "szko&#322;a"
  ]
  node [
    id 167
    label "kierunek"
  ]
  node [
    id 168
    label "zasada_d'Alemberta"
  ]
  node [
    id 169
    label "obserwacja"
  ]
  node [
    id 170
    label "normatywizm"
  ]
  node [
    id 171
    label "jurisprudence"
  ]
  node [
    id 172
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 173
    label "przepis"
  ]
  node [
    id 174
    label "prawo_karne_procesowe"
  ]
  node [
    id 175
    label "criterion"
  ]
  node [
    id 176
    label "kazuistyka"
  ]
  node [
    id 177
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 178
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 179
    label "kryminologia"
  ]
  node [
    id 180
    label "opis"
  ]
  node [
    id 181
    label "regu&#322;a_Glogera"
  ]
  node [
    id 182
    label "prawo_Mendla"
  ]
  node [
    id 183
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 184
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 185
    label "prawo_karne"
  ]
  node [
    id 186
    label "legislacyjnie"
  ]
  node [
    id 187
    label "twierdzenie"
  ]
  node [
    id 188
    label "cywilistyka"
  ]
  node [
    id 189
    label "judykatura"
  ]
  node [
    id 190
    label "kanonistyka"
  ]
  node [
    id 191
    label "standard"
  ]
  node [
    id 192
    label "nauka_prawa"
  ]
  node [
    id 193
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 194
    label "podmiot"
  ]
  node [
    id 195
    label "law"
  ]
  node [
    id 196
    label "qualification"
  ]
  node [
    id 197
    label "dominion"
  ]
  node [
    id 198
    label "wykonawczy"
  ]
  node [
    id 199
    label "zasada"
  ]
  node [
    id 200
    label "normalizacja"
  ]
  node [
    id 201
    label "przej&#347;cie"
  ]
  node [
    id 202
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 203
    label "rodowo&#347;&#263;"
  ]
  node [
    id 204
    label "patent"
  ]
  node [
    id 205
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 206
    label "dobra"
  ]
  node [
    id 207
    label "stan"
  ]
  node [
    id 208
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 209
    label "przej&#347;&#263;"
  ]
  node [
    id 210
    label "possession"
  ]
  node [
    id 211
    label "spadek"
  ]
  node [
    id 212
    label "zabranie"
  ]
  node [
    id 213
    label "disinheritance"
  ]
  node [
    id 214
    label "zabra&#263;"
  ]
  node [
    id 215
    label "disinherit"
  ]
  node [
    id 216
    label "proces_biologiczny"
  ]
  node [
    id 217
    label "seniorat"
  ]
  node [
    id 218
    label "cz&#281;sty"
  ]
  node [
    id 219
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 220
    label "wielokrotnie"
  ]
  node [
    id 221
    label "psucie_si&#281;"
  ]
  node [
    id 222
    label "oskar&#380;enie"
  ]
  node [
    id 223
    label "zaszkodzenie"
  ]
  node [
    id 224
    label "baga&#380;"
  ]
  node [
    id 225
    label "loading"
  ]
  node [
    id 226
    label "charge"
  ]
  node [
    id 227
    label "hindrance"
  ]
  node [
    id 228
    label "na&#322;o&#380;enie"
  ]
  node [
    id 229
    label "zawada"
  ]
  node [
    id 230
    label "encumbrance"
  ]
  node [
    id 231
    label "zobowi&#261;zanie"
  ]
  node [
    id 232
    label "hazard"
  ]
  node [
    id 233
    label "przeszkoda"
  ]
  node [
    id 234
    label "nadbaga&#380;"
  ]
  node [
    id 235
    label "torba"
  ]
  node [
    id 236
    label "pakunek"
  ]
  node [
    id 237
    label "zas&#243;b"
  ]
  node [
    id 238
    label "baga&#380;&#243;wka"
  ]
  node [
    id 239
    label "stosunek_prawny"
  ]
  node [
    id 240
    label "oblig"
  ]
  node [
    id 241
    label "uregulowa&#263;"
  ]
  node [
    id 242
    label "oddzia&#322;anie"
  ]
  node [
    id 243
    label "occupation"
  ]
  node [
    id 244
    label "duty"
  ]
  node [
    id 245
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 246
    label "zapowied&#378;"
  ]
  node [
    id 247
    label "obowi&#261;zek"
  ]
  node [
    id 248
    label "statement"
  ]
  node [
    id 249
    label "zapewnienie"
  ]
  node [
    id 250
    label "po&#322;o&#380;enie"
  ]
  node [
    id 251
    label "obleczenie_si&#281;"
  ]
  node [
    id 252
    label "poubieranie"
  ]
  node [
    id 253
    label "przyodzianie"
  ]
  node [
    id 254
    label "measurement"
  ]
  node [
    id 255
    label "str&#243;j"
  ]
  node [
    id 256
    label "infliction"
  ]
  node [
    id 257
    label "spowodowanie"
  ]
  node [
    id 258
    label "obleczenie"
  ]
  node [
    id 259
    label "umieszczenie"
  ]
  node [
    id 260
    label "rozebranie"
  ]
  node [
    id 261
    label "przywdzianie"
  ]
  node [
    id 262
    label "przebranie"
  ]
  node [
    id 263
    label "czynno&#347;&#263;"
  ]
  node [
    id 264
    label "przymierzenie"
  ]
  node [
    id 265
    label "s&#261;d"
  ]
  node [
    id 266
    label "skar&#380;yciel"
  ]
  node [
    id 267
    label "wypowied&#378;"
  ]
  node [
    id 268
    label "suspicja"
  ]
  node [
    id 269
    label "poj&#281;cie"
  ]
  node [
    id 270
    label "post&#281;powanie"
  ]
  node [
    id 271
    label "ocenienie"
  ]
  node [
    id 272
    label "ocena"
  ]
  node [
    id 273
    label "strona"
  ]
  node [
    id 274
    label "damage"
  ]
  node [
    id 275
    label "zrobienie"
  ]
  node [
    id 276
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 277
    label "sprzecznia"
  ]
  node [
    id 278
    label "przeciwie&#324;stwo"
  ]
  node [
    id 279
    label "relacja"
  ]
  node [
    id 280
    label "sprzeczno&#347;&#263;"
  ]
  node [
    id 281
    label "zjawisko"
  ]
  node [
    id 282
    label "obejrzenie"
  ]
  node [
    id 283
    label "widzenie"
  ]
  node [
    id 284
    label "urzeczywistnianie"
  ]
  node [
    id 285
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 286
    label "byt"
  ]
  node [
    id 287
    label "przeszkodzenie"
  ]
  node [
    id 288
    label "produkowanie"
  ]
  node [
    id 289
    label "being"
  ]
  node [
    id 290
    label "znikni&#281;cie"
  ]
  node [
    id 291
    label "robienie"
  ]
  node [
    id 292
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 293
    label "przeszkadzanie"
  ]
  node [
    id 294
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 295
    label "wyprodukowanie"
  ]
  node [
    id 296
    label "aprobowanie"
  ]
  node [
    id 297
    label "uznawanie"
  ]
  node [
    id 298
    label "przywidzenie"
  ]
  node [
    id 299
    label "ogl&#261;danie"
  ]
  node [
    id 300
    label "visit"
  ]
  node [
    id 301
    label "dostrzeganie"
  ]
  node [
    id 302
    label "wychodzenie"
  ]
  node [
    id 303
    label "zobaczenie"
  ]
  node [
    id 304
    label "&#347;nienie"
  ]
  node [
    id 305
    label "wyobra&#380;anie_sobie"
  ]
  node [
    id 306
    label "malenie"
  ]
  node [
    id 307
    label "ocenianie"
  ]
  node [
    id 308
    label "vision"
  ]
  node [
    id 309
    label "u&#322;uda"
  ]
  node [
    id 310
    label "patrzenie"
  ]
  node [
    id 311
    label "odwiedziny"
  ]
  node [
    id 312
    label "postrzeganie"
  ]
  node [
    id 313
    label "wzrok"
  ]
  node [
    id 314
    label "punkt_widzenia"
  ]
  node [
    id 315
    label "w&#322;&#261;czanie"
  ]
  node [
    id 316
    label "zmalenie"
  ]
  node [
    id 317
    label "przegl&#261;danie"
  ]
  node [
    id 318
    label "zwi&#281;kszanie_si&#281;"
  ]
  node [
    id 319
    label "sojourn"
  ]
  node [
    id 320
    label "realization"
  ]
  node [
    id 321
    label "view"
  ]
  node [
    id 322
    label "reagowanie"
  ]
  node [
    id 323
    label "przejrzenie"
  ]
  node [
    id 324
    label "widywanie"
  ]
  node [
    id 325
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 326
    label "pojmowanie"
  ]
  node [
    id 327
    label "zapoznanie_si&#281;"
  ]
  node [
    id 328
    label "position"
  ]
  node [
    id 329
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 330
    label "w&#322;&#261;czenie"
  ]
  node [
    id 331
    label "utrudnienie"
  ]
  node [
    id 332
    label "prevention"
  ]
  node [
    id 333
    label "utrudnianie"
  ]
  node [
    id 334
    label "obstruction"
  ]
  node [
    id 335
    label "ha&#322;asowanie"
  ]
  node [
    id 336
    label "wadzenie"
  ]
  node [
    id 337
    label "utrzymywanie"
  ]
  node [
    id 338
    label "entity"
  ]
  node [
    id 339
    label "subsystencja"
  ]
  node [
    id 340
    label "utrzyma&#263;"
  ]
  node [
    id 341
    label "egzystencja"
  ]
  node [
    id 342
    label "wy&#380;ywienie"
  ]
  node [
    id 343
    label "ontologicznie"
  ]
  node [
    id 344
    label "utrzymanie"
  ]
  node [
    id 345
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 346
    label "potencja"
  ]
  node [
    id 347
    label "utrzymywa&#263;"
  ]
  node [
    id 348
    label "establishment"
  ]
  node [
    id 349
    label "fabrication"
  ]
  node [
    id 350
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 351
    label "pojawianie_si&#281;"
  ]
  node [
    id 352
    label "tworzenie"
  ]
  node [
    id 353
    label "gospodarka"
  ]
  node [
    id 354
    label "ubycie"
  ]
  node [
    id 355
    label "disappearance"
  ]
  node [
    id 356
    label "usuni&#281;cie"
  ]
  node [
    id 357
    label "poznikanie"
  ]
  node [
    id 358
    label "evanescence"
  ]
  node [
    id 359
    label "wyj&#347;cie"
  ]
  node [
    id 360
    label "die"
  ]
  node [
    id 361
    label "przepadni&#281;cie"
  ]
  node [
    id 362
    label "ukradzenie"
  ]
  node [
    id 363
    label "niewidoczny"
  ]
  node [
    id 364
    label "stanie_si&#281;"
  ]
  node [
    id 365
    label "zgini&#281;cie"
  ]
  node [
    id 366
    label "stworzenie"
  ]
  node [
    id 367
    label "devising"
  ]
  node [
    id 368
    label "pojawienie_si&#281;"
  ]
  node [
    id 369
    label "shuffle"
  ]
  node [
    id 370
    label "spe&#322;nianie"
  ]
  node [
    id 371
    label "fulfillment"
  ]
  node [
    id 372
    label "powodowanie"
  ]
  node [
    id 373
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 374
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 375
    label "creation"
  ]
  node [
    id 376
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 377
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 378
    label "act"
  ]
  node [
    id 379
    label "porobienie"
  ]
  node [
    id 380
    label "tentegowanie"
  ]
  node [
    id 381
    label "ustawienie"
  ]
  node [
    id 382
    label "opposition"
  ]
  node [
    id 383
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 384
    label "protestacja"
  ]
  node [
    id 385
    label "czerwona_kartka"
  ]
  node [
    id 386
    label "grupa"
  ]
  node [
    id 387
    label "partia"
  ]
  node [
    id 388
    label "reakcja"
  ]
  node [
    id 389
    label "react"
  ]
  node [
    id 390
    label "zachowanie"
  ]
  node [
    id 391
    label "reaction"
  ]
  node [
    id 392
    label "organizm"
  ]
  node [
    id 393
    label "rozmowa"
  ]
  node [
    id 394
    label "response"
  ]
  node [
    id 395
    label "rezultat"
  ]
  node [
    id 396
    label "respondent"
  ]
  node [
    id 397
    label "reverse"
  ]
  node [
    id 398
    label "odwrotny"
  ]
  node [
    id 399
    label "odm&#322;adzanie"
  ]
  node [
    id 400
    label "liga"
  ]
  node [
    id 401
    label "jednostka_systematyczna"
  ]
  node [
    id 402
    label "asymilowanie"
  ]
  node [
    id 403
    label "gromada"
  ]
  node [
    id 404
    label "asymilowa&#263;"
  ]
  node [
    id 405
    label "egzemplarz"
  ]
  node [
    id 406
    label "Entuzjastki"
  ]
  node [
    id 407
    label "zbi&#243;r"
  ]
  node [
    id 408
    label "kompozycja"
  ]
  node [
    id 409
    label "Terranie"
  ]
  node [
    id 410
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 411
    label "category"
  ]
  node [
    id 412
    label "pakiet_klimatyczny"
  ]
  node [
    id 413
    label "oddzia&#322;"
  ]
  node [
    id 414
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 415
    label "cz&#261;steczka"
  ]
  node [
    id 416
    label "stage_set"
  ]
  node [
    id 417
    label "type"
  ]
  node [
    id 418
    label "specgrupa"
  ]
  node [
    id 419
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 420
    label "&#346;wietliki"
  ]
  node [
    id 421
    label "odm&#322;odzenie"
  ]
  node [
    id 422
    label "Eurogrupa"
  ]
  node [
    id 423
    label "odm&#322;adza&#263;"
  ]
  node [
    id 424
    label "harcerze_starsi"
  ]
  node [
    id 425
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 426
    label "ustosunkowywa&#263;"
  ]
  node [
    id 427
    label "wi&#261;zanie"
  ]
  node [
    id 428
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 429
    label "sprawko"
  ]
  node [
    id 430
    label "bratnia_dusza"
  ]
  node [
    id 431
    label "trasa"
  ]
  node [
    id 432
    label "zwi&#261;zanie"
  ]
  node [
    id 433
    label "ustosunkowywanie"
  ]
  node [
    id 434
    label "marriage"
  ]
  node [
    id 435
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 436
    label "message"
  ]
  node [
    id 437
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 438
    label "ustosunkowa&#263;"
  ]
  node [
    id 439
    label "korespondent"
  ]
  node [
    id 440
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 441
    label "zwi&#261;za&#263;"
  ]
  node [
    id 442
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 443
    label "podzbi&#243;r"
  ]
  node [
    id 444
    label "ustosunkowanie"
  ]
  node [
    id 445
    label "zwi&#261;zek"
  ]
  node [
    id 446
    label "proces"
  ]
  node [
    id 447
    label "boski"
  ]
  node [
    id 448
    label "krajobraz"
  ]
  node [
    id 449
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 450
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 451
    label "presence"
  ]
  node [
    id 452
    label "charakter"
  ]
  node [
    id 453
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 454
    label "u&#322;o&#380;enie"
  ]
  node [
    id 455
    label "ustalenie"
  ]
  node [
    id 456
    label "erection"
  ]
  node [
    id 457
    label "setup"
  ]
  node [
    id 458
    label "erecting"
  ]
  node [
    id 459
    label "rozmieszczenie"
  ]
  node [
    id 460
    label "poustawianie"
  ]
  node [
    id 461
    label "zinterpretowanie"
  ]
  node [
    id 462
    label "porozstawianie"
  ]
  node [
    id 463
    label "rola"
  ]
  node [
    id 464
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 465
    label "sprzeciw"
  ]
  node [
    id 466
    label "Bund"
  ]
  node [
    id 467
    label "PPR"
  ]
  node [
    id 468
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 469
    label "wybranek"
  ]
  node [
    id 470
    label "Jakobici"
  ]
  node [
    id 471
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 472
    label "SLD"
  ]
  node [
    id 473
    label "Razem"
  ]
  node [
    id 474
    label "PiS"
  ]
  node [
    id 475
    label "package"
  ]
  node [
    id 476
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 477
    label "Kuomintang"
  ]
  node [
    id 478
    label "ZSL"
  ]
  node [
    id 479
    label "organizacja"
  ]
  node [
    id 480
    label "AWS"
  ]
  node [
    id 481
    label "gra"
  ]
  node [
    id 482
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 483
    label "game"
  ]
  node [
    id 484
    label "blok"
  ]
  node [
    id 485
    label "materia&#322;"
  ]
  node [
    id 486
    label "PO"
  ]
  node [
    id 487
    label "si&#322;a"
  ]
  node [
    id 488
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 489
    label "niedoczas"
  ]
  node [
    id 490
    label "Federali&#347;ci"
  ]
  node [
    id 491
    label "PSL"
  ]
  node [
    id 492
    label "Wigowie"
  ]
  node [
    id 493
    label "ZChN"
  ]
  node [
    id 494
    label "egzekutywa"
  ]
  node [
    id 495
    label "aktyw"
  ]
  node [
    id 496
    label "wybranka"
  ]
  node [
    id 497
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 498
    label "unit"
  ]
  node [
    id 499
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 500
    label "nadawa&#263;"
  ]
  node [
    id 501
    label "wypromowywa&#263;"
  ]
  node [
    id 502
    label "nada&#263;"
  ]
  node [
    id 503
    label "rozpowszechnia&#263;"
  ]
  node [
    id 504
    label "zach&#281;ca&#263;"
  ]
  node [
    id 505
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 506
    label "promocja"
  ]
  node [
    id 507
    label "advance"
  ]
  node [
    id 508
    label "udzieli&#263;"
  ]
  node [
    id 509
    label "udziela&#263;"
  ]
  node [
    id 510
    label "reklama"
  ]
  node [
    id 511
    label "doprowadza&#263;"
  ]
  node [
    id 512
    label "generalize"
  ]
  node [
    id 513
    label "sprawia&#263;"
  ]
  node [
    id 514
    label "pozyskiwa&#263;"
  ]
  node [
    id 515
    label "rig"
  ]
  node [
    id 516
    label "wykonywa&#263;"
  ]
  node [
    id 517
    label "prowadzi&#263;"
  ]
  node [
    id 518
    label "deliver"
  ]
  node [
    id 519
    label "wzbudza&#263;"
  ]
  node [
    id 520
    label "moderate"
  ]
  node [
    id 521
    label "wprowadza&#263;"
  ]
  node [
    id 522
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 523
    label "odst&#281;powa&#263;"
  ]
  node [
    id 524
    label "dawa&#263;"
  ]
  node [
    id 525
    label "assign"
  ]
  node [
    id 526
    label "render"
  ]
  node [
    id 527
    label "accord"
  ]
  node [
    id 528
    label "zezwala&#263;"
  ]
  node [
    id 529
    label "przyznawa&#263;"
  ]
  node [
    id 530
    label "da&#263;"
  ]
  node [
    id 531
    label "udost&#281;pni&#263;"
  ]
  node [
    id 532
    label "przyzna&#263;"
  ]
  node [
    id 533
    label "picture"
  ]
  node [
    id 534
    label "odst&#261;pi&#263;"
  ]
  node [
    id 535
    label "mie&#263;_miejsce"
  ]
  node [
    id 536
    label "aid"
  ]
  node [
    id 537
    label "u&#322;atwia&#263;"
  ]
  node [
    id 538
    label "concur"
  ]
  node [
    id 539
    label "sprzyja&#263;"
  ]
  node [
    id 540
    label "skutkowa&#263;"
  ]
  node [
    id 541
    label "digest"
  ]
  node [
    id 542
    label "Warszawa"
  ]
  node [
    id 543
    label "back"
  ]
  node [
    id 544
    label "gada&#263;"
  ]
  node [
    id 545
    label "donosi&#263;"
  ]
  node [
    id 546
    label "rekomendowa&#263;"
  ]
  node [
    id 547
    label "za&#322;atwia&#263;"
  ]
  node [
    id 548
    label "obgadywa&#263;"
  ]
  node [
    id 549
    label "przesy&#322;a&#263;"
  ]
  node [
    id 550
    label "za&#322;atwi&#263;"
  ]
  node [
    id 551
    label "zarekomendowa&#263;"
  ]
  node [
    id 552
    label "spowodowa&#263;"
  ]
  node [
    id 553
    label "przes&#322;a&#263;"
  ]
  node [
    id 554
    label "donie&#347;&#263;"
  ]
  node [
    id 555
    label "damka"
  ]
  node [
    id 556
    label "warcaby"
  ]
  node [
    id 557
    label "promotion"
  ]
  node [
    id 558
    label "impreza"
  ]
  node [
    id 559
    label "sprzeda&#380;"
  ]
  node [
    id 560
    label "zamiana"
  ]
  node [
    id 561
    label "brief"
  ]
  node [
    id 562
    label "decyzja"
  ]
  node [
    id 563
    label "&#347;wiadectwo"
  ]
  node [
    id 564
    label "akcja"
  ]
  node [
    id 565
    label "bran&#380;a"
  ]
  node [
    id 566
    label "commencement"
  ]
  node [
    id 567
    label "okazja"
  ]
  node [
    id 568
    label "informacja"
  ]
  node [
    id 569
    label "klasa"
  ]
  node [
    id 570
    label "graduacja"
  ]
  node [
    id 571
    label "nominacja"
  ]
  node [
    id 572
    label "szachy"
  ]
  node [
    id 573
    label "popularyzacja"
  ]
  node [
    id 574
    label "wypromowa&#263;"
  ]
  node [
    id 575
    label "gradation"
  ]
  node [
    id 576
    label "uzyska&#263;"
  ]
  node [
    id 577
    label "copywriting"
  ]
  node [
    id 578
    label "samplowanie"
  ]
  node [
    id 579
    label "tekst"
  ]
  node [
    id 580
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 581
    label "Chocho&#322;"
  ]
  node [
    id 582
    label "Herkules_Poirot"
  ]
  node [
    id 583
    label "Edyp"
  ]
  node [
    id 584
    label "ludzko&#347;&#263;"
  ]
  node [
    id 585
    label "parali&#380;owa&#263;"
  ]
  node [
    id 586
    label "Harry_Potter"
  ]
  node [
    id 587
    label "Casanova"
  ]
  node [
    id 588
    label "Zgredek"
  ]
  node [
    id 589
    label "Gargantua"
  ]
  node [
    id 590
    label "Winnetou"
  ]
  node [
    id 591
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 592
    label "posta&#263;"
  ]
  node [
    id 593
    label "Dulcynea"
  ]
  node [
    id 594
    label "kategoria_gramatyczna"
  ]
  node [
    id 595
    label "g&#322;owa"
  ]
  node [
    id 596
    label "figura"
  ]
  node [
    id 597
    label "portrecista"
  ]
  node [
    id 598
    label "person"
  ]
  node [
    id 599
    label "Plastu&#347;"
  ]
  node [
    id 600
    label "Quasimodo"
  ]
  node [
    id 601
    label "Sherlock_Holmes"
  ]
  node [
    id 602
    label "Faust"
  ]
  node [
    id 603
    label "Wallenrod"
  ]
  node [
    id 604
    label "Dwukwiat"
  ]
  node [
    id 605
    label "Don_Juan"
  ]
  node [
    id 606
    label "profanum"
  ]
  node [
    id 607
    label "koniugacja"
  ]
  node [
    id 608
    label "Don_Kiszot"
  ]
  node [
    id 609
    label "mikrokosmos"
  ]
  node [
    id 610
    label "duch"
  ]
  node [
    id 611
    label "antropochoria"
  ]
  node [
    id 612
    label "oddzia&#322;ywanie"
  ]
  node [
    id 613
    label "Hamlet"
  ]
  node [
    id 614
    label "Werter"
  ]
  node [
    id 615
    label "istota"
  ]
  node [
    id 616
    label "Szwejk"
  ]
  node [
    id 617
    label "homo_sapiens"
  ]
  node [
    id 618
    label "mentalno&#347;&#263;"
  ]
  node [
    id 619
    label "superego"
  ]
  node [
    id 620
    label "psychika"
  ]
  node [
    id 621
    label "znaczenie"
  ]
  node [
    id 622
    label "wn&#281;trze"
  ]
  node [
    id 623
    label "cecha"
  ]
  node [
    id 624
    label "charakterystyka"
  ]
  node [
    id 625
    label "cz&#322;owiek"
  ]
  node [
    id 626
    label "zaistnie&#263;"
  ]
  node [
    id 627
    label "Osjan"
  ]
  node [
    id 628
    label "kto&#347;"
  ]
  node [
    id 629
    label "wygl&#261;d"
  ]
  node [
    id 630
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 631
    label "osobowo&#347;&#263;"
  ]
  node [
    id 632
    label "wytw&#243;r"
  ]
  node [
    id 633
    label "trim"
  ]
  node [
    id 634
    label "poby&#263;"
  ]
  node [
    id 635
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 636
    label "Aspazja"
  ]
  node [
    id 637
    label "kompleksja"
  ]
  node [
    id 638
    label "wytrzyma&#263;"
  ]
  node [
    id 639
    label "budowa"
  ]
  node [
    id 640
    label "formacja"
  ]
  node [
    id 641
    label "pozosta&#263;"
  ]
  node [
    id 642
    label "point"
  ]
  node [
    id 643
    label "przedstawienie"
  ]
  node [
    id 644
    label "go&#347;&#263;"
  ]
  node [
    id 645
    label "hamper"
  ]
  node [
    id 646
    label "spasm"
  ]
  node [
    id 647
    label "mrozi&#263;"
  ]
  node [
    id 648
    label "pora&#380;a&#263;"
  ]
  node [
    id 649
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 650
    label "fleksja"
  ]
  node [
    id 651
    label "liczba"
  ]
  node [
    id 652
    label "coupling"
  ]
  node [
    id 653
    label "tryb"
  ]
  node [
    id 654
    label "czas"
  ]
  node [
    id 655
    label "czasownik"
  ]
  node [
    id 656
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 657
    label "orz&#281;sek"
  ]
  node [
    id 658
    label "fotograf"
  ]
  node [
    id 659
    label "malarz"
  ]
  node [
    id 660
    label "artysta"
  ]
  node [
    id 661
    label "hipnotyzowanie"
  ]
  node [
    id 662
    label "&#347;lad"
  ]
  node [
    id 663
    label "docieranie"
  ]
  node [
    id 664
    label "natural_process"
  ]
  node [
    id 665
    label "wdzieranie_si&#281;"
  ]
  node [
    id 666
    label "lobbysta"
  ]
  node [
    id 667
    label "pryncypa&#322;"
  ]
  node [
    id 668
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 669
    label "kszta&#322;t"
  ]
  node [
    id 670
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 671
    label "wiedza"
  ]
  node [
    id 672
    label "kierowa&#263;"
  ]
  node [
    id 673
    label "alkohol"
  ]
  node [
    id 674
    label "zdolno&#347;&#263;"
  ]
  node [
    id 675
    label "&#380;ycie"
  ]
  node [
    id 676
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 677
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 678
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 679
    label "sztuka"
  ]
  node [
    id 680
    label "dekiel"
  ]
  node [
    id 681
    label "ro&#347;lina"
  ]
  node [
    id 682
    label "&#347;ci&#281;cie"
  ]
  node [
    id 683
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 684
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 685
    label "&#347;ci&#281;gno"
  ]
  node [
    id 686
    label "noosfera"
  ]
  node [
    id 687
    label "byd&#322;o"
  ]
  node [
    id 688
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 689
    label "makrocefalia"
  ]
  node [
    id 690
    label "obiekt"
  ]
  node [
    id 691
    label "ucho"
  ]
  node [
    id 692
    label "g&#243;ra"
  ]
  node [
    id 693
    label "m&#243;zg"
  ]
  node [
    id 694
    label "kierownictwo"
  ]
  node [
    id 695
    label "fryzura"
  ]
  node [
    id 696
    label "umys&#322;"
  ]
  node [
    id 697
    label "cia&#322;o"
  ]
  node [
    id 698
    label "cz&#322;onek"
  ]
  node [
    id 699
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 700
    label "czaszka"
  ]
  node [
    id 701
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 702
    label "allochoria"
  ]
  node [
    id 703
    label "p&#322;aszczyzna"
  ]
  node [
    id 704
    label "bierka_szachowa"
  ]
  node [
    id 705
    label "obiekt_matematyczny"
  ]
  node [
    id 706
    label "gestaltyzm"
  ]
  node [
    id 707
    label "styl"
  ]
  node [
    id 708
    label "obraz"
  ]
  node [
    id 709
    label "rzecz"
  ]
  node [
    id 710
    label "d&#378;wi&#281;k"
  ]
  node [
    id 711
    label "character"
  ]
  node [
    id 712
    label "rze&#378;ba"
  ]
  node [
    id 713
    label "stylistyka"
  ]
  node [
    id 714
    label "figure"
  ]
  node [
    id 715
    label "miejsce"
  ]
  node [
    id 716
    label "antycypacja"
  ]
  node [
    id 717
    label "ornamentyka"
  ]
  node [
    id 718
    label "facet"
  ]
  node [
    id 719
    label "popis"
  ]
  node [
    id 720
    label "wiersz"
  ]
  node [
    id 721
    label "symetria"
  ]
  node [
    id 722
    label "lingwistyka_kognitywna"
  ]
  node [
    id 723
    label "karta"
  ]
  node [
    id 724
    label "shape"
  ]
  node [
    id 725
    label "perspektywa"
  ]
  node [
    id 726
    label "dziedzina"
  ]
  node [
    id 727
    label "Szekspir"
  ]
  node [
    id 728
    label "Mickiewicz"
  ]
  node [
    id 729
    label "cierpienie"
  ]
  node [
    id 730
    label "piek&#322;o"
  ]
  node [
    id 731
    label "human_body"
  ]
  node [
    id 732
    label "ofiarowywanie"
  ]
  node [
    id 733
    label "sfera_afektywna"
  ]
  node [
    id 734
    label "nekromancja"
  ]
  node [
    id 735
    label "Po&#347;wist"
  ]
  node [
    id 736
    label "podekscytowanie"
  ]
  node [
    id 737
    label "deformowanie"
  ]
  node [
    id 738
    label "sumienie"
  ]
  node [
    id 739
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 740
    label "deformowa&#263;"
  ]
  node [
    id 741
    label "zjawa"
  ]
  node [
    id 742
    label "zmar&#322;y"
  ]
  node [
    id 743
    label "istota_nadprzyrodzona"
  ]
  node [
    id 744
    label "power"
  ]
  node [
    id 745
    label "ofiarowywa&#263;"
  ]
  node [
    id 746
    label "oddech"
  ]
  node [
    id 747
    label "seksualno&#347;&#263;"
  ]
  node [
    id 748
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 749
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 750
    label "ego"
  ]
  node [
    id 751
    label "ofiarowanie"
  ]
  node [
    id 752
    label "fizjonomia"
  ]
  node [
    id 753
    label "kompleks"
  ]
  node [
    id 754
    label "zapalno&#347;&#263;"
  ]
  node [
    id 755
    label "T&#281;sknica"
  ]
  node [
    id 756
    label "ofiarowa&#263;"
  ]
  node [
    id 757
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 758
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 759
    label "passion"
  ]
  node [
    id 760
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 761
    label "atom"
  ]
  node [
    id 762
    label "odbicie"
  ]
  node [
    id 763
    label "przyroda"
  ]
  node [
    id 764
    label "Ziemia"
  ]
  node [
    id 765
    label "kosmos"
  ]
  node [
    id 766
    label "miniatura"
  ]
  node [
    id 767
    label "zmiana"
  ]
  node [
    id 768
    label "zniszczenie"
  ]
  node [
    id 769
    label "collapse"
  ]
  node [
    id 770
    label "wear"
  ]
  node [
    id 771
    label "destruction"
  ]
  node [
    id 772
    label "zu&#380;ycie"
  ]
  node [
    id 773
    label "attrition"
  ]
  node [
    id 774
    label "os&#322;abienie"
  ]
  node [
    id 775
    label "podpalenie"
  ]
  node [
    id 776
    label "strata"
  ]
  node [
    id 777
    label "kondycja_fizyczna"
  ]
  node [
    id 778
    label "spl&#261;drowanie"
  ]
  node [
    id 779
    label "zdrowie"
  ]
  node [
    id 780
    label "poniszczenie"
  ]
  node [
    id 781
    label "ruin"
  ]
  node [
    id 782
    label "poniszczenie_si&#281;"
  ]
  node [
    id 783
    label "rewizja"
  ]
  node [
    id 784
    label "passage"
  ]
  node [
    id 785
    label "oznaka"
  ]
  node [
    id 786
    label "change"
  ]
  node [
    id 787
    label "ferment"
  ]
  node [
    id 788
    label "komplet"
  ]
  node [
    id 789
    label "anatomopatolog"
  ]
  node [
    id 790
    label "zmianka"
  ]
  node [
    id 791
    label "amendment"
  ]
  node [
    id 792
    label "praca"
  ]
  node [
    id 793
    label "odmienianie"
  ]
  node [
    id 794
    label "tura"
  ]
  node [
    id 795
    label "strike"
  ]
  node [
    id 796
    label "rap"
  ]
  node [
    id 797
    label "os&#261;dza&#263;"
  ]
  node [
    id 798
    label "opiniowa&#263;"
  ]
  node [
    id 799
    label "stwierdza&#263;"
  ]
  node [
    id 800
    label "s&#261;dzi&#263;"
  ]
  node [
    id 801
    label "znajdowa&#263;"
  ]
  node [
    id 802
    label "hold"
  ]
  node [
    id 803
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 804
    label "muzyka_rozrywkowa"
  ]
  node [
    id 805
    label "karpiowate"
  ]
  node [
    id 806
    label "ryba"
  ]
  node [
    id 807
    label "czarna_muzyka"
  ]
  node [
    id 808
    label "drapie&#380;nik"
  ]
  node [
    id 809
    label "asp"
  ]
  node [
    id 810
    label "zero-jedynkowy"
  ]
  node [
    id 811
    label "konkretny"
  ]
  node [
    id 812
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 813
    label "formation"
  ]
  node [
    id 814
    label "umieszczanie"
  ]
  node [
    id 815
    label "rozmieszczanie"
  ]
  node [
    id 816
    label "postawienie"
  ]
  node [
    id 817
    label "podstawianie"
  ]
  node [
    id 818
    label "spinanie"
  ]
  node [
    id 819
    label "kupowanie"
  ]
  node [
    id 820
    label "formu&#322;owanie"
  ]
  node [
    id 821
    label "sponsorship"
  ]
  node [
    id 822
    label "zostawianie"
  ]
  node [
    id 823
    label "podstawienie"
  ]
  node [
    id 824
    label "zabudowywanie"
  ]
  node [
    id 825
    label "przebudowanie_si&#281;"
  ]
  node [
    id 826
    label "gotowanie_si&#281;"
  ]
  node [
    id 827
    label "nastawianie_si&#281;"
  ]
  node [
    id 828
    label "upami&#281;tnianie"
  ]
  node [
    id 829
    label "spi&#281;cie"
  ]
  node [
    id 830
    label "nastawianie"
  ]
  node [
    id 831
    label "przebudowanie"
  ]
  node [
    id 832
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 833
    label "przestawianie"
  ]
  node [
    id 834
    label "przebudowywanie"
  ]
  node [
    id 835
    label "typowanie"
  ]
  node [
    id 836
    label "podbudowanie"
  ]
  node [
    id 837
    label "podbudowywanie"
  ]
  node [
    id 838
    label "dawanie"
  ]
  node [
    id 839
    label "fundator"
  ]
  node [
    id 840
    label "wyrastanie"
  ]
  node [
    id 841
    label "przebudowywanie_si&#281;"
  ]
  node [
    id 842
    label "przestawienie"
  ]
  node [
    id 843
    label "odbudowanie"
  ]
  node [
    id 844
    label "conceptualization"
  ]
  node [
    id 845
    label "rozwlekanie"
  ]
  node [
    id 846
    label "zauwa&#380;anie"
  ]
  node [
    id 847
    label "komunikowanie"
  ]
  node [
    id 848
    label "formu&#322;owanie_si&#281;"
  ]
  node [
    id 849
    label "rzucanie"
  ]
  node [
    id 850
    label "p&#322;acenie"
  ]
  node [
    id 851
    label "umo&#380;liwianie"
  ]
  node [
    id 852
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 853
    label "przekazywanie"
  ]
  node [
    id 854
    label "puszczanie_si&#281;"
  ]
  node [
    id 855
    label "rendition"
  ]
  node [
    id 856
    label "&#322;adowanie"
  ]
  node [
    id 857
    label "zezwalanie"
  ]
  node [
    id 858
    label "nalewanie"
  ]
  node [
    id 859
    label "communication"
  ]
  node [
    id 860
    label "urz&#261;dzanie"
  ]
  node [
    id 861
    label "wyst&#281;powanie"
  ]
  node [
    id 862
    label "udost&#281;pnianie"
  ]
  node [
    id 863
    label "dodawanie"
  ]
  node [
    id 864
    label "giving"
  ]
  node [
    id 865
    label "pra&#380;enie"
  ]
  node [
    id 866
    label "powierzanie"
  ]
  node [
    id 867
    label "emission"
  ]
  node [
    id 868
    label "dostarczanie"
  ]
  node [
    id 869
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 870
    label "przeznaczanie"
  ]
  node [
    id 871
    label "obiecywanie"
  ]
  node [
    id 872
    label "odst&#281;powanie"
  ]
  node [
    id 873
    label "administration"
  ]
  node [
    id 874
    label "wymienianie_si&#281;"
  ]
  node [
    id 875
    label "bycie_w_posiadaniu"
  ]
  node [
    id 876
    label "wkupienie_si&#281;"
  ]
  node [
    id 877
    label "cena"
  ]
  node [
    id 878
    label "kupienie"
  ]
  node [
    id 879
    label "purchase"
  ]
  node [
    id 880
    label "buying"
  ]
  node [
    id 881
    label "wkupywanie_si&#281;"
  ]
  node [
    id 882
    label "wierzenie"
  ]
  node [
    id 883
    label "wykupywanie"
  ]
  node [
    id 884
    label "handlowanie"
  ]
  node [
    id 885
    label "pozyskiwanie"
  ]
  node [
    id 886
    label "ustawianie"
  ]
  node [
    id 887
    label "importowanie"
  ]
  node [
    id 888
    label "granie"
  ]
  node [
    id 889
    label "utrwalanie"
  ]
  node [
    id 890
    label "delay"
  ]
  node [
    id 891
    label "dzianie_si&#281;"
  ]
  node [
    id 892
    label "zachowywanie"
  ]
  node [
    id 893
    label "rezygnowanie"
  ]
  node [
    id 894
    label "pozostawanie"
  ]
  node [
    id 895
    label "umieranie"
  ]
  node [
    id 896
    label "zostawanie"
  ]
  node [
    id 897
    label "krzywdzenie"
  ]
  node [
    id 898
    label "cause"
  ]
  node [
    id 899
    label "causal_agent"
  ]
  node [
    id 900
    label "powo&#322;ywanie"
  ]
  node [
    id 901
    label "election"
  ]
  node [
    id 902
    label "przewidywanie"
  ]
  node [
    id 903
    label "appointment"
  ]
  node [
    id 904
    label "optowanie"
  ]
  node [
    id 905
    label "okre&#347;lanie"
  ]
  node [
    id 906
    label "pope&#322;nianie"
  ]
  node [
    id 907
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 908
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 909
    label "stanowienie"
  ]
  node [
    id 910
    label "structure"
  ]
  node [
    id 911
    label "development"
  ]
  node [
    id 912
    label "exploitation"
  ]
  node [
    id 913
    label "lokowanie_si&#281;"
  ]
  node [
    id 914
    label "ustalanie"
  ]
  node [
    id 915
    label "pomieszczanie"
  ]
  node [
    id 916
    label "plasowanie"
  ]
  node [
    id 917
    label "zakrywanie"
  ]
  node [
    id 918
    label "fitting"
  ]
  node [
    id 919
    label "prze&#322;adowywanie"
  ]
  node [
    id 920
    label "nabudowanie"
  ]
  node [
    id 921
    label "budowla"
  ]
  node [
    id 922
    label "sformu&#322;owanie"
  ]
  node [
    id 923
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 924
    label "Irish_pound"
  ]
  node [
    id 925
    label "danie"
  ]
  node [
    id 926
    label "utworzenie"
  ]
  node [
    id 927
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 928
    label "set"
  ]
  node [
    id 929
    label "podniesienie"
  ]
  node [
    id 930
    label "fundowanie"
  ]
  node [
    id 931
    label "construction"
  ]
  node [
    id 932
    label "wytypowanie"
  ]
  node [
    id 933
    label "upami&#281;tnienie"
  ]
  node [
    id 934
    label "reorganization"
  ]
  node [
    id 935
    label "zmienianie"
  ]
  node [
    id 936
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 937
    label "time"
  ]
  node [
    id 938
    label "clash"
  ]
  node [
    id 939
    label "buckle"
  ]
  node [
    id 940
    label "komera&#380;"
  ]
  node [
    id 941
    label "konflikt"
  ]
  node [
    id 942
    label "awaria"
  ]
  node [
    id 943
    label "clasp"
  ]
  node [
    id 944
    label "z&#322;&#261;czenie"
  ]
  node [
    id 945
    label "po&#322;&#261;czenie"
  ]
  node [
    id 946
    label "wype&#322;nianie"
  ]
  node [
    id 947
    label "zas&#322;anianie"
  ]
  node [
    id 948
    label "budowanie"
  ]
  node [
    id 949
    label "podpieranie"
  ]
  node [
    id 950
    label "dyskutowanie"
  ]
  node [
    id 951
    label "dodawanie_otuchy"
  ]
  node [
    id 952
    label "motivation"
  ]
  node [
    id 953
    label "dodanie_otuchy"
  ]
  node [
    id 954
    label "wyja&#347;nienie"
  ]
  node [
    id 955
    label "podparcie"
  ]
  node [
    id 956
    label "gossip"
  ]
  node [
    id 957
    label "rebuilding"
  ]
  node [
    id 958
    label "odtworzenie"
  ]
  node [
    id 959
    label "przywr&#243;cenie"
  ]
  node [
    id 960
    label "uzupe&#322;nienie"
  ]
  node [
    id 961
    label "spinal_column"
  ]
  node [
    id 962
    label "zaciskanie"
  ]
  node [
    id 963
    label "scalanie"
  ]
  node [
    id 964
    label "zmienienie"
  ]
  node [
    id 965
    label "poprzestawianie"
  ]
  node [
    id 966
    label "invert"
  ]
  node [
    id 967
    label "transposition"
  ]
  node [
    id 968
    label "odmienienie"
  ]
  node [
    id 969
    label "przemieszczenie"
  ]
  node [
    id 970
    label "nastawienie"
  ]
  node [
    id 971
    label "przemieszczanie"
  ]
  node [
    id 972
    label "oszukiwanie"
  ]
  node [
    id 973
    label "zbli&#380;anie"
  ]
  node [
    id 974
    label "substitution"
  ]
  node [
    id 975
    label "zast&#281;powanie"
  ]
  node [
    id 976
    label "doprowadzanie"
  ]
  node [
    id 977
    label "wprowadzanie"
  ]
  node [
    id 978
    label "narobienie"
  ]
  node [
    id 979
    label "z&#322;amanie"
  ]
  node [
    id 980
    label "orientation"
  ]
  node [
    id 981
    label "collation"
  ]
  node [
    id 982
    label "sk&#322;adanie"
  ]
  node [
    id 983
    label "oznaczanie"
  ]
  node [
    id 984
    label "przeorientowanie"
  ]
  node [
    id 985
    label "wprowadzenie"
  ]
  node [
    id 986
    label "zbli&#380;enie"
  ]
  node [
    id 987
    label "wymienienie"
  ]
  node [
    id 988
    label "refilling"
  ]
  node [
    id 989
    label "doprowadzenie"
  ]
  node [
    id 990
    label "powstawanie"
  ]
  node [
    id 991
    label "wychowywanie"
  ]
  node [
    id 992
    label "zaczynanie_si&#281;"
  ]
  node [
    id 993
    label "ciasto"
  ]
  node [
    id 994
    label "sterczenie"
  ]
  node [
    id 995
    label "origin"
  ]
  node [
    id 996
    label "doro&#347;lenie"
  ]
  node [
    id 997
    label "zjawianie_si&#281;"
  ]
  node [
    id 998
    label "dominance"
  ]
  node [
    id 999
    label "growth"
  ]
  node [
    id 1000
    label "ro&#347;ni&#281;cie"
  ]
  node [
    id 1001
    label "pulchnienie"
  ]
  node [
    id 1002
    label "stawanie_si&#281;"
  ]
  node [
    id 1003
    label "partycypant"
  ]
  node [
    id 1004
    label "za&#322;o&#380;yciel"
  ]
  node [
    id 1005
    label "darczy&#324;ca"
  ]
  node [
    id 1006
    label "kognicja"
  ]
  node [
    id 1007
    label "object"
  ]
  node [
    id 1008
    label "rozprawa"
  ]
  node [
    id 1009
    label "temat"
  ]
  node [
    id 1010
    label "wydarzenie"
  ]
  node [
    id 1011
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1012
    label "proposition"
  ]
  node [
    id 1013
    label "przes&#322;anka"
  ]
  node [
    id 1014
    label "idea"
  ]
  node [
    id 1015
    label "przebiec"
  ]
  node [
    id 1016
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1017
    label "motyw"
  ]
  node [
    id 1018
    label "przebiegni&#281;cie"
  ]
  node [
    id 1019
    label "fabu&#322;a"
  ]
  node [
    id 1020
    label "ideologia"
  ]
  node [
    id 1021
    label "intelekt"
  ]
  node [
    id 1022
    label "Kant"
  ]
  node [
    id 1023
    label "p&#322;&#243;d"
  ]
  node [
    id 1024
    label "cel"
  ]
  node [
    id 1025
    label "pomys&#322;"
  ]
  node [
    id 1026
    label "ideacja"
  ]
  node [
    id 1027
    label "wpadni&#281;cie"
  ]
  node [
    id 1028
    label "kultura"
  ]
  node [
    id 1029
    label "wpa&#347;&#263;"
  ]
  node [
    id 1030
    label "wpadanie"
  ]
  node [
    id 1031
    label "wpada&#263;"
  ]
  node [
    id 1032
    label "rozumowanie"
  ]
  node [
    id 1033
    label "opracowanie"
  ]
  node [
    id 1034
    label "obrady"
  ]
  node [
    id 1035
    label "cytat"
  ]
  node [
    id 1036
    label "obja&#347;nienie"
  ]
  node [
    id 1037
    label "s&#261;dzenie"
  ]
  node [
    id 1038
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1039
    label "niuansowa&#263;"
  ]
  node [
    id 1040
    label "element"
  ]
  node [
    id 1041
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1042
    label "sk&#322;adnik"
  ]
  node [
    id 1043
    label "zniuansowa&#263;"
  ]
  node [
    id 1044
    label "fakt"
  ]
  node [
    id 1045
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1046
    label "przyczyna"
  ]
  node [
    id 1047
    label "wnioskowanie"
  ]
  node [
    id 1048
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 1049
    label "wyraz_pochodny"
  ]
  node [
    id 1050
    label "zboczenie"
  ]
  node [
    id 1051
    label "om&#243;wienie"
  ]
  node [
    id 1052
    label "omawia&#263;"
  ]
  node [
    id 1053
    label "fraza"
  ]
  node [
    id 1054
    label "tre&#347;&#263;"
  ]
  node [
    id 1055
    label "forum"
  ]
  node [
    id 1056
    label "topik"
  ]
  node [
    id 1057
    label "tematyka"
  ]
  node [
    id 1058
    label "w&#261;tek"
  ]
  node [
    id 1059
    label "zbaczanie"
  ]
  node [
    id 1060
    label "forma"
  ]
  node [
    id 1061
    label "om&#243;wi&#263;"
  ]
  node [
    id 1062
    label "omawianie"
  ]
  node [
    id 1063
    label "melodia"
  ]
  node [
    id 1064
    label "otoczka"
  ]
  node [
    id 1065
    label "zbacza&#263;"
  ]
  node [
    id 1066
    label "zboczy&#263;"
  ]
  node [
    id 1067
    label "pikuty"
  ]
  node [
    id 1068
    label "narz&#281;dzie"
  ]
  node [
    id 1069
    label "sztuciec"
  ]
  node [
    id 1070
    label "ostrze"
  ]
  node [
    id 1071
    label "maszyna"
  ]
  node [
    id 1072
    label "kosa"
  ]
  node [
    id 1073
    label "knife"
  ]
  node [
    id 1074
    label "Rzym_Zachodni"
  ]
  node [
    id 1075
    label "whole"
  ]
  node [
    id 1076
    label "ilo&#347;&#263;"
  ]
  node [
    id 1077
    label "Rzym_Wschodni"
  ]
  node [
    id 1078
    label "&#347;rodek"
  ]
  node [
    id 1079
    label "niezb&#281;dnik"
  ]
  node [
    id 1080
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 1081
    label "tylec"
  ]
  node [
    id 1082
    label "zastawa"
  ]
  node [
    id 1083
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 1084
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 1085
    label "tuleja"
  ]
  node [
    id 1086
    label "pracowanie"
  ]
  node [
    id 1087
    label "kad&#322;ub"
  ]
  node [
    id 1088
    label "b&#281;benek"
  ]
  node [
    id 1089
    label "wa&#322;"
  ]
  node [
    id 1090
    label "maszyneria"
  ]
  node [
    id 1091
    label "prototypownia"
  ]
  node [
    id 1092
    label "trawers"
  ]
  node [
    id 1093
    label "deflektor"
  ]
  node [
    id 1094
    label "kolumna"
  ]
  node [
    id 1095
    label "mechanizm"
  ]
  node [
    id 1096
    label "wa&#322;ek"
  ]
  node [
    id 1097
    label "pracowa&#263;"
  ]
  node [
    id 1098
    label "b&#281;ben"
  ]
  node [
    id 1099
    label "rz&#281;zi&#263;"
  ]
  node [
    id 1100
    label "przyk&#322;adka"
  ]
  node [
    id 1101
    label "t&#322;ok"
  ]
  node [
    id 1102
    label "dehumanizacja"
  ]
  node [
    id 1103
    label "rami&#281;"
  ]
  node [
    id 1104
    label "rz&#281;&#380;enie"
  ]
  node [
    id 1105
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1106
    label "surowy"
  ]
  node [
    id 1107
    label "postrach"
  ]
  node [
    id 1108
    label "spit"
  ]
  node [
    id 1109
    label "&#380;y&#322;a"
  ]
  node [
    id 1110
    label "nauczycielka"
  ]
  node [
    id 1111
    label "nieznaczny"
  ]
  node [
    id 1112
    label "pomiernie"
  ]
  node [
    id 1113
    label "kr&#243;tko"
  ]
  node [
    id 1114
    label "mikroskopijnie"
  ]
  node [
    id 1115
    label "nieliczny"
  ]
  node [
    id 1116
    label "mo&#380;liwie"
  ]
  node [
    id 1117
    label "nieistotnie"
  ]
  node [
    id 1118
    label "ma&#322;y"
  ]
  node [
    id 1119
    label "niepowa&#380;nie"
  ]
  node [
    id 1120
    label "niewa&#380;ny"
  ]
  node [
    id 1121
    label "mo&#380;liwy"
  ]
  node [
    id 1122
    label "zno&#347;nie"
  ]
  node [
    id 1123
    label "kr&#243;tki"
  ]
  node [
    id 1124
    label "nieznacznie"
  ]
  node [
    id 1125
    label "drobnostkowy"
  ]
  node [
    id 1126
    label "malusie&#324;ko"
  ]
  node [
    id 1127
    label "mikroskopijny"
  ]
  node [
    id 1128
    label "bardzo"
  ]
  node [
    id 1129
    label "szybki"
  ]
  node [
    id 1130
    label "przeci&#281;tny"
  ]
  node [
    id 1131
    label "wstydliwy"
  ]
  node [
    id 1132
    label "s&#322;aby"
  ]
  node [
    id 1133
    label "ch&#322;opiec"
  ]
  node [
    id 1134
    label "m&#322;ody"
  ]
  node [
    id 1135
    label "marny"
  ]
  node [
    id 1136
    label "n&#281;dznie"
  ]
  node [
    id 1137
    label "nielicznie"
  ]
  node [
    id 1138
    label "licho"
  ]
  node [
    id 1139
    label "proporcjonalnie"
  ]
  node [
    id 1140
    label "pomierny"
  ]
  node [
    id 1141
    label "miernie"
  ]
  node [
    id 1142
    label "pociesza&#263;"
  ]
  node [
    id 1143
    label "opiera&#263;"
  ]
  node [
    id 1144
    label "czu&#263;"
  ]
  node [
    id 1145
    label "stanowi&#263;"
  ]
  node [
    id 1146
    label "chowa&#263;"
  ]
  node [
    id 1147
    label "&#322;atwi&#263;"
  ]
  node [
    id 1148
    label "ease"
  ]
  node [
    id 1149
    label "osnowywa&#263;"
  ]
  node [
    id 1150
    label "czerpa&#263;"
  ]
  node [
    id 1151
    label "stawia&#263;"
  ]
  node [
    id 1152
    label "cover"
  ]
  node [
    id 1153
    label "warszawa"
  ]
  node [
    id 1154
    label "Powi&#347;le"
  ]
  node [
    id 1155
    label "Wawa"
  ]
  node [
    id 1156
    label "syreni_gr&#243;d"
  ]
  node [
    id 1157
    label "Wawer"
  ]
  node [
    id 1158
    label "W&#322;ochy"
  ]
  node [
    id 1159
    label "Ursyn&#243;w"
  ]
  node [
    id 1160
    label "Bielany"
  ]
  node [
    id 1161
    label "Weso&#322;a"
  ]
  node [
    id 1162
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 1163
    label "Targ&#243;wek"
  ]
  node [
    id 1164
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1165
    label "Muran&#243;w"
  ]
  node [
    id 1166
    label "Warsiawa"
  ]
  node [
    id 1167
    label "Ursus"
  ]
  node [
    id 1168
    label "Ochota"
  ]
  node [
    id 1169
    label "Marymont"
  ]
  node [
    id 1170
    label "Ujazd&#243;w"
  ]
  node [
    id 1171
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 1172
    label "Solec"
  ]
  node [
    id 1173
    label "Bemowo"
  ]
  node [
    id 1174
    label "Mokot&#243;w"
  ]
  node [
    id 1175
    label "Wilan&#243;w"
  ]
  node [
    id 1176
    label "warszawka"
  ]
  node [
    id 1177
    label "varsaviana"
  ]
  node [
    id 1178
    label "Wola"
  ]
  node [
    id 1179
    label "Rembert&#243;w"
  ]
  node [
    id 1180
    label "Praga"
  ]
  node [
    id 1181
    label "&#379;oliborz"
  ]
  node [
    id 1182
    label "czeladnik"
  ]
  node [
    id 1183
    label "stowarzyszenie"
  ]
  node [
    id 1184
    label "club"
  ]
  node [
    id 1185
    label "cechmistrz"
  ]
  node [
    id 1186
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 1187
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1188
    label "Chewra_Kadisza"
  ]
  node [
    id 1189
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 1190
    label "fabianie"
  ]
  node [
    id 1191
    label "Rotary_International"
  ]
  node [
    id 1192
    label "Eleusis"
  ]
  node [
    id 1193
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 1194
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 1195
    label "Monar"
  ]
  node [
    id 1196
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 1197
    label "mistrz"
  ]
  node [
    id 1198
    label "rzemie&#347;lnik"
  ]
  node [
    id 1199
    label "ucze&#324;"
  ]
  node [
    id 1200
    label "rzemie&#347;lniczek"
  ]
  node [
    id 1201
    label "sprawdza&#263;_si&#281;"
  ]
  node [
    id 1202
    label "poci&#261;ga&#263;"
  ]
  node [
    id 1203
    label "przynosi&#263;"
  ]
  node [
    id 1204
    label "i&#347;&#263;_w_parze"
  ]
  node [
    id 1205
    label "organizowa&#263;"
  ]
  node [
    id 1206
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1207
    label "czyni&#263;"
  ]
  node [
    id 1208
    label "stylizowa&#263;"
  ]
  node [
    id 1209
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1210
    label "falowa&#263;"
  ]
  node [
    id 1211
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1212
    label "peddle"
  ]
  node [
    id 1213
    label "wydala&#263;"
  ]
  node [
    id 1214
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1215
    label "tentegowa&#263;"
  ]
  node [
    id 1216
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1217
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1218
    label "oszukiwa&#263;"
  ]
  node [
    id 1219
    label "ukazywa&#263;"
  ]
  node [
    id 1220
    label "przerabia&#263;"
  ]
  node [
    id 1221
    label "post&#281;powa&#263;"
  ]
  node [
    id 1222
    label "motywowa&#263;"
  ]
  node [
    id 1223
    label "planowa&#263;"
  ]
  node [
    id 1224
    label "wytwarza&#263;"
  ]
  node [
    id 1225
    label "consist"
  ]
  node [
    id 1226
    label "dodawa&#263;_otuchy"
  ]
  node [
    id 1227
    label "tworzy&#263;"
  ]
  node [
    id 1228
    label "by&#263;"
  ]
  node [
    id 1229
    label "decide"
  ]
  node [
    id 1230
    label "pies_my&#347;liwski"
  ]
  node [
    id 1231
    label "zatrzymywa&#263;"
  ]
  node [
    id 1232
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1233
    label "typify"
  ]
  node [
    id 1234
    label "create"
  ]
  node [
    id 1235
    label "pope&#322;nia&#263;"
  ]
  node [
    id 1236
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1237
    label "get"
  ]
  node [
    id 1238
    label "mean"
  ]
  node [
    id 1239
    label "lot_&#347;lizgowy"
  ]
  node [
    id 1240
    label "organize"
  ]
  node [
    id 1241
    label "project"
  ]
  node [
    id 1242
    label "my&#347;le&#263;"
  ]
  node [
    id 1243
    label "volunteer"
  ]
  node [
    id 1244
    label "opracowywa&#263;"
  ]
  node [
    id 1245
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 1246
    label "czyj&#347;"
  ]
  node [
    id 1247
    label "m&#261;&#380;"
  ]
  node [
    id 1248
    label "prywatny"
  ]
  node [
    id 1249
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1250
    label "ch&#322;op"
  ]
  node [
    id 1251
    label "pan_m&#322;ody"
  ]
  node [
    id 1252
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1253
    label "&#347;lubny"
  ]
  node [
    id 1254
    label "pan_domu"
  ]
  node [
    id 1255
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1256
    label "stary"
  ]
  node [
    id 1257
    label "belfer"
  ]
  node [
    id 1258
    label "murza"
  ]
  node [
    id 1259
    label "ojciec"
  ]
  node [
    id 1260
    label "samiec"
  ]
  node [
    id 1261
    label "androlog"
  ]
  node [
    id 1262
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 1263
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 1264
    label "efendi"
  ]
  node [
    id 1265
    label "opiekun"
  ]
  node [
    id 1266
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1267
    label "pa&#324;stwo"
  ]
  node [
    id 1268
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 1269
    label "bratek"
  ]
  node [
    id 1270
    label "Mieszko_I"
  ]
  node [
    id 1271
    label "Midas"
  ]
  node [
    id 1272
    label "bogaty"
  ]
  node [
    id 1273
    label "popularyzator"
  ]
  node [
    id 1274
    label "pracodawca"
  ]
  node [
    id 1275
    label "kszta&#322;ciciel"
  ]
  node [
    id 1276
    label "preceptor"
  ]
  node [
    id 1277
    label "nabab"
  ]
  node [
    id 1278
    label "pupil"
  ]
  node [
    id 1279
    label "andropauza"
  ]
  node [
    id 1280
    label "zwrot"
  ]
  node [
    id 1281
    label "przyw&#243;dca"
  ]
  node [
    id 1282
    label "doros&#322;y"
  ]
  node [
    id 1283
    label "pedagog"
  ]
  node [
    id 1284
    label "rz&#261;dzenie"
  ]
  node [
    id 1285
    label "jegomo&#347;&#263;"
  ]
  node [
    id 1286
    label "szkolnik"
  ]
  node [
    id 1287
    label "ch&#322;opina"
  ]
  node [
    id 1288
    label "w&#322;odarz"
  ]
  node [
    id 1289
    label "profesor"
  ]
  node [
    id 1290
    label "gra_w_karty"
  ]
  node [
    id 1291
    label "w&#322;adza"
  ]
  node [
    id 1292
    label "Fidel_Castro"
  ]
  node [
    id 1293
    label "Anders"
  ]
  node [
    id 1294
    label "Ko&#347;ciuszko"
  ]
  node [
    id 1295
    label "Tito"
  ]
  node [
    id 1296
    label "Miko&#322;ajczyk"
  ]
  node [
    id 1297
    label "lider"
  ]
  node [
    id 1298
    label "Mao"
  ]
  node [
    id 1299
    label "Sabataj_Cwi"
  ]
  node [
    id 1300
    label "p&#322;atnik"
  ]
  node [
    id 1301
    label "zwierzchnik"
  ]
  node [
    id 1302
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 1303
    label "nadzorca"
  ]
  node [
    id 1304
    label "funkcjonariusz"
  ]
  node [
    id 1305
    label "wykupienie"
  ]
  node [
    id 1306
    label "rozszerzyciel"
  ]
  node [
    id 1307
    label "wapniak"
  ]
  node [
    id 1308
    label "os&#322;abia&#263;"
  ]
  node [
    id 1309
    label "hominid"
  ]
  node [
    id 1310
    label "podw&#322;adny"
  ]
  node [
    id 1311
    label "os&#322;abianie"
  ]
  node [
    id 1312
    label "dwun&#243;g"
  ]
  node [
    id 1313
    label "nasada"
  ]
  node [
    id 1314
    label "wz&#243;r"
  ]
  node [
    id 1315
    label "senior"
  ]
  node [
    id 1316
    label "Adam"
  ]
  node [
    id 1317
    label "polifag"
  ]
  node [
    id 1318
    label "wydoro&#347;lenie"
  ]
  node [
    id 1319
    label "du&#380;y"
  ]
  node [
    id 1320
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1321
    label "&#378;ra&#322;y"
  ]
  node [
    id 1322
    label "doro&#347;le"
  ]
  node [
    id 1323
    label "dojrzale"
  ]
  node [
    id 1324
    label "dojrza&#322;y"
  ]
  node [
    id 1325
    label "m&#261;dry"
  ]
  node [
    id 1326
    label "doletni"
  ]
  node [
    id 1327
    label "punkt"
  ]
  node [
    id 1328
    label "turn"
  ]
  node [
    id 1329
    label "turning"
  ]
  node [
    id 1330
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1331
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1332
    label "skr&#281;t"
  ]
  node [
    id 1333
    label "obr&#243;t"
  ]
  node [
    id 1334
    label "fraza_czasownikowa"
  ]
  node [
    id 1335
    label "jednostka_leksykalna"
  ]
  node [
    id 1336
    label "wyra&#380;enie"
  ]
  node [
    id 1337
    label "starosta"
  ]
  node [
    id 1338
    label "zarz&#261;dca"
  ]
  node [
    id 1339
    label "w&#322;adca"
  ]
  node [
    id 1340
    label "nauczyciel"
  ]
  node [
    id 1341
    label "stopie&#324;_naukowy"
  ]
  node [
    id 1342
    label "nauczyciel_akademicki"
  ]
  node [
    id 1343
    label "tytu&#322;"
  ]
  node [
    id 1344
    label "profesura"
  ]
  node [
    id 1345
    label "konsulent"
  ]
  node [
    id 1346
    label "wirtuoz"
  ]
  node [
    id 1347
    label "autor"
  ]
  node [
    id 1348
    label "wyprawka"
  ]
  node [
    id 1349
    label "mundurek"
  ]
  node [
    id 1350
    label "tarcza"
  ]
  node [
    id 1351
    label "elew"
  ]
  node [
    id 1352
    label "absolwent"
  ]
  node [
    id 1353
    label "ekspert"
  ]
  node [
    id 1354
    label "ochotnik"
  ]
  node [
    id 1355
    label "pomocnik"
  ]
  node [
    id 1356
    label "student"
  ]
  node [
    id 1357
    label "nauczyciel_muzyki"
  ]
  node [
    id 1358
    label "zakonnik"
  ]
  node [
    id 1359
    label "urz&#281;dnik"
  ]
  node [
    id 1360
    label "bogacz"
  ]
  node [
    id 1361
    label "dostojnik"
  ]
  node [
    id 1362
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 1363
    label "kuwada"
  ]
  node [
    id 1364
    label "tworzyciel"
  ]
  node [
    id 1365
    label "rodzice"
  ]
  node [
    id 1366
    label "&#347;w"
  ]
  node [
    id 1367
    label "pomys&#322;odawca"
  ]
  node [
    id 1368
    label "rodzic"
  ]
  node [
    id 1369
    label "wykonawca"
  ]
  node [
    id 1370
    label "ojczym"
  ]
  node [
    id 1371
    label "przodek"
  ]
  node [
    id 1372
    label "papa"
  ]
  node [
    id 1373
    label "kochanek"
  ]
  node [
    id 1374
    label "fio&#322;ek"
  ]
  node [
    id 1375
    label "brat"
  ]
  node [
    id 1376
    label "zwierz&#281;"
  ]
  node [
    id 1377
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1378
    label "mo&#347;&#263;"
  ]
  node [
    id 1379
    label "Frygia"
  ]
  node [
    id 1380
    label "sprawowanie"
  ]
  node [
    id 1381
    label "dominowanie"
  ]
  node [
    id 1382
    label "reign"
  ]
  node [
    id 1383
    label "rule"
  ]
  node [
    id 1384
    label "zwierz&#281;_domowe"
  ]
  node [
    id 1385
    label "J&#281;drzejewicz"
  ]
  node [
    id 1386
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 1387
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 1388
    label "John_Dewey"
  ]
  node [
    id 1389
    label "specjalista"
  ]
  node [
    id 1390
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1391
    label "Turek"
  ]
  node [
    id 1392
    label "effendi"
  ]
  node [
    id 1393
    label "obfituj&#261;cy"
  ]
  node [
    id 1394
    label "r&#243;&#380;norodny"
  ]
  node [
    id 1395
    label "spania&#322;y"
  ]
  node [
    id 1396
    label "obficie"
  ]
  node [
    id 1397
    label "sytuowany"
  ]
  node [
    id 1398
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1399
    label "forsiasty"
  ]
  node [
    id 1400
    label "zapa&#347;ny"
  ]
  node [
    id 1401
    label "bogato"
  ]
  node [
    id 1402
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1403
    label "Katar"
  ]
  node [
    id 1404
    label "Libia"
  ]
  node [
    id 1405
    label "Gwatemala"
  ]
  node [
    id 1406
    label "Ekwador"
  ]
  node [
    id 1407
    label "Afganistan"
  ]
  node [
    id 1408
    label "Tad&#380;ykistan"
  ]
  node [
    id 1409
    label "Bhutan"
  ]
  node [
    id 1410
    label "Argentyna"
  ]
  node [
    id 1411
    label "D&#380;ibuti"
  ]
  node [
    id 1412
    label "Wenezuela"
  ]
  node [
    id 1413
    label "Gabon"
  ]
  node [
    id 1414
    label "Ukraina"
  ]
  node [
    id 1415
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1416
    label "Rwanda"
  ]
  node [
    id 1417
    label "Liechtenstein"
  ]
  node [
    id 1418
    label "Sri_Lanka"
  ]
  node [
    id 1419
    label "Madagaskar"
  ]
  node [
    id 1420
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1421
    label "Kongo"
  ]
  node [
    id 1422
    label "Tonga"
  ]
  node [
    id 1423
    label "Bangladesz"
  ]
  node [
    id 1424
    label "Kanada"
  ]
  node [
    id 1425
    label "Wehrlen"
  ]
  node [
    id 1426
    label "Algieria"
  ]
  node [
    id 1427
    label "Uganda"
  ]
  node [
    id 1428
    label "Surinam"
  ]
  node [
    id 1429
    label "Sahara_Zachodnia"
  ]
  node [
    id 1430
    label "Chile"
  ]
  node [
    id 1431
    label "W&#281;gry"
  ]
  node [
    id 1432
    label "Birma"
  ]
  node [
    id 1433
    label "Kazachstan"
  ]
  node [
    id 1434
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1435
    label "Armenia"
  ]
  node [
    id 1436
    label "Tuwalu"
  ]
  node [
    id 1437
    label "Timor_Wschodni"
  ]
  node [
    id 1438
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1439
    label "Izrael"
  ]
  node [
    id 1440
    label "Estonia"
  ]
  node [
    id 1441
    label "Komory"
  ]
  node [
    id 1442
    label "Kamerun"
  ]
  node [
    id 1443
    label "Haiti"
  ]
  node [
    id 1444
    label "Belize"
  ]
  node [
    id 1445
    label "Sierra_Leone"
  ]
  node [
    id 1446
    label "Luksemburg"
  ]
  node [
    id 1447
    label "USA"
  ]
  node [
    id 1448
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1449
    label "Barbados"
  ]
  node [
    id 1450
    label "San_Marino"
  ]
  node [
    id 1451
    label "Bu&#322;garia"
  ]
  node [
    id 1452
    label "Indonezja"
  ]
  node [
    id 1453
    label "Wietnam"
  ]
  node [
    id 1454
    label "Malawi"
  ]
  node [
    id 1455
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1456
    label "Francja"
  ]
  node [
    id 1457
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1458
    label "Zambia"
  ]
  node [
    id 1459
    label "Angola"
  ]
  node [
    id 1460
    label "Grenada"
  ]
  node [
    id 1461
    label "Nepal"
  ]
  node [
    id 1462
    label "Panama"
  ]
  node [
    id 1463
    label "Rumunia"
  ]
  node [
    id 1464
    label "Czarnog&#243;ra"
  ]
  node [
    id 1465
    label "Malediwy"
  ]
  node [
    id 1466
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1467
    label "S&#322;owacja"
  ]
  node [
    id 1468
    label "para"
  ]
  node [
    id 1469
    label "Egipt"
  ]
  node [
    id 1470
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1471
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1472
    label "Mozambik"
  ]
  node [
    id 1473
    label "Kolumbia"
  ]
  node [
    id 1474
    label "Laos"
  ]
  node [
    id 1475
    label "Burundi"
  ]
  node [
    id 1476
    label "Suazi"
  ]
  node [
    id 1477
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1478
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1479
    label "Czechy"
  ]
  node [
    id 1480
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1481
    label "Wyspy_Marshalla"
  ]
  node [
    id 1482
    label "Dominika"
  ]
  node [
    id 1483
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1484
    label "Syria"
  ]
  node [
    id 1485
    label "Palau"
  ]
  node [
    id 1486
    label "Gwinea_Bissau"
  ]
  node [
    id 1487
    label "Liberia"
  ]
  node [
    id 1488
    label "Jamajka"
  ]
  node [
    id 1489
    label "Zimbabwe"
  ]
  node [
    id 1490
    label "Polska"
  ]
  node [
    id 1491
    label "Dominikana"
  ]
  node [
    id 1492
    label "Senegal"
  ]
  node [
    id 1493
    label "Togo"
  ]
  node [
    id 1494
    label "Gujana"
  ]
  node [
    id 1495
    label "Gruzja"
  ]
  node [
    id 1496
    label "Albania"
  ]
  node [
    id 1497
    label "Zair"
  ]
  node [
    id 1498
    label "Meksyk"
  ]
  node [
    id 1499
    label "Macedonia"
  ]
  node [
    id 1500
    label "Chorwacja"
  ]
  node [
    id 1501
    label "Kambod&#380;a"
  ]
  node [
    id 1502
    label "Monako"
  ]
  node [
    id 1503
    label "Mauritius"
  ]
  node [
    id 1504
    label "Gwinea"
  ]
  node [
    id 1505
    label "Mali"
  ]
  node [
    id 1506
    label "Nigeria"
  ]
  node [
    id 1507
    label "Kostaryka"
  ]
  node [
    id 1508
    label "Hanower"
  ]
  node [
    id 1509
    label "Paragwaj"
  ]
  node [
    id 1510
    label "Seszele"
  ]
  node [
    id 1511
    label "Wyspy_Salomona"
  ]
  node [
    id 1512
    label "Hiszpania"
  ]
  node [
    id 1513
    label "Boliwia"
  ]
  node [
    id 1514
    label "Kirgistan"
  ]
  node [
    id 1515
    label "Irlandia"
  ]
  node [
    id 1516
    label "Czad"
  ]
  node [
    id 1517
    label "Irak"
  ]
  node [
    id 1518
    label "Lesoto"
  ]
  node [
    id 1519
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1520
    label "Malta"
  ]
  node [
    id 1521
    label "Andora"
  ]
  node [
    id 1522
    label "Chiny"
  ]
  node [
    id 1523
    label "Filipiny"
  ]
  node [
    id 1524
    label "Antarktis"
  ]
  node [
    id 1525
    label "Niemcy"
  ]
  node [
    id 1526
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1527
    label "Pakistan"
  ]
  node [
    id 1528
    label "terytorium"
  ]
  node [
    id 1529
    label "Nikaragua"
  ]
  node [
    id 1530
    label "Brazylia"
  ]
  node [
    id 1531
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1532
    label "Maroko"
  ]
  node [
    id 1533
    label "Portugalia"
  ]
  node [
    id 1534
    label "Niger"
  ]
  node [
    id 1535
    label "Kenia"
  ]
  node [
    id 1536
    label "Botswana"
  ]
  node [
    id 1537
    label "Fid&#380;i"
  ]
  node [
    id 1538
    label "Tunezja"
  ]
  node [
    id 1539
    label "Australia"
  ]
  node [
    id 1540
    label "Tajlandia"
  ]
  node [
    id 1541
    label "Burkina_Faso"
  ]
  node [
    id 1542
    label "interior"
  ]
  node [
    id 1543
    label "Tanzania"
  ]
  node [
    id 1544
    label "Benin"
  ]
  node [
    id 1545
    label "Indie"
  ]
  node [
    id 1546
    label "&#321;otwa"
  ]
  node [
    id 1547
    label "Kiribati"
  ]
  node [
    id 1548
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1549
    label "Rodezja"
  ]
  node [
    id 1550
    label "Cypr"
  ]
  node [
    id 1551
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1552
    label "Peru"
  ]
  node [
    id 1553
    label "Austria"
  ]
  node [
    id 1554
    label "Urugwaj"
  ]
  node [
    id 1555
    label "Jordania"
  ]
  node [
    id 1556
    label "Grecja"
  ]
  node [
    id 1557
    label "Azerbejd&#380;an"
  ]
  node [
    id 1558
    label "Turcja"
  ]
  node [
    id 1559
    label "Samoa"
  ]
  node [
    id 1560
    label "Sudan"
  ]
  node [
    id 1561
    label "Oman"
  ]
  node [
    id 1562
    label "ziemia"
  ]
  node [
    id 1563
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1564
    label "Uzbekistan"
  ]
  node [
    id 1565
    label "Portoryko"
  ]
  node [
    id 1566
    label "Honduras"
  ]
  node [
    id 1567
    label "Mongolia"
  ]
  node [
    id 1568
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1569
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1570
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1571
    label "Serbia"
  ]
  node [
    id 1572
    label "Tajwan"
  ]
  node [
    id 1573
    label "Wielka_Brytania"
  ]
  node [
    id 1574
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1575
    label "Liban"
  ]
  node [
    id 1576
    label "Japonia"
  ]
  node [
    id 1577
    label "Ghana"
  ]
  node [
    id 1578
    label "Belgia"
  ]
  node [
    id 1579
    label "Bahrajn"
  ]
  node [
    id 1580
    label "Mikronezja"
  ]
  node [
    id 1581
    label "Etiopia"
  ]
  node [
    id 1582
    label "Kuwejt"
  ]
  node [
    id 1583
    label "Bahamy"
  ]
  node [
    id 1584
    label "Rosja"
  ]
  node [
    id 1585
    label "Mo&#322;dawia"
  ]
  node [
    id 1586
    label "Litwa"
  ]
  node [
    id 1587
    label "S&#322;owenia"
  ]
  node [
    id 1588
    label "Szwajcaria"
  ]
  node [
    id 1589
    label "Erytrea"
  ]
  node [
    id 1590
    label "Arabia_Saudyjska"
  ]
  node [
    id 1591
    label "Kuba"
  ]
  node [
    id 1592
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1593
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1594
    label "Malezja"
  ]
  node [
    id 1595
    label "Korea"
  ]
  node [
    id 1596
    label "Jemen"
  ]
  node [
    id 1597
    label "Nowa_Zelandia"
  ]
  node [
    id 1598
    label "Namibia"
  ]
  node [
    id 1599
    label "Nauru"
  ]
  node [
    id 1600
    label "holoarktyka"
  ]
  node [
    id 1601
    label "Brunei"
  ]
  node [
    id 1602
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1603
    label "Khitai"
  ]
  node [
    id 1604
    label "Mauretania"
  ]
  node [
    id 1605
    label "Iran"
  ]
  node [
    id 1606
    label "Gambia"
  ]
  node [
    id 1607
    label "Somalia"
  ]
  node [
    id 1608
    label "Holandia"
  ]
  node [
    id 1609
    label "Turkmenistan"
  ]
  node [
    id 1610
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1611
    label "Salwador"
  ]
  node [
    id 1612
    label "starzy"
  ]
  node [
    id 1613
    label "pokolenie"
  ]
  node [
    id 1614
    label "wapniaki"
  ]
  node [
    id 1615
    label "ojcowie"
  ]
  node [
    id 1616
    label "linea&#380;"
  ]
  node [
    id 1617
    label "krewny"
  ]
  node [
    id 1618
    label "chodnik"
  ]
  node [
    id 1619
    label "w&#243;z"
  ]
  node [
    id 1620
    label "p&#322;ug"
  ]
  node [
    id 1621
    label "wyrobisko"
  ]
  node [
    id 1622
    label "dziad"
  ]
  node [
    id 1623
    label "antecesor"
  ]
  node [
    id 1624
    label "post&#281;p"
  ]
  node [
    id 1625
    label "rodzic_chrzestny"
  ]
  node [
    id 1626
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 1627
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 1628
    label "nienowoczesny"
  ]
  node [
    id 1629
    label "gruba_ryba"
  ]
  node [
    id 1630
    label "zestarzenie_si&#281;"
  ]
  node [
    id 1631
    label "poprzedni"
  ]
  node [
    id 1632
    label "dawno"
  ]
  node [
    id 1633
    label "staro"
  ]
  node [
    id 1634
    label "dotychczasowy"
  ]
  node [
    id 1635
    label "p&#243;&#378;ny"
  ]
  node [
    id 1636
    label "d&#322;ugoletni"
  ]
  node [
    id 1637
    label "charakterystyczny"
  ]
  node [
    id 1638
    label "po_staro&#347;wiecku"
  ]
  node [
    id 1639
    label "znajomy"
  ]
  node [
    id 1640
    label "odleg&#322;y"
  ]
  node [
    id 1641
    label "starzenie_si&#281;"
  ]
  node [
    id 1642
    label "starczo"
  ]
  node [
    id 1643
    label "dawniej"
  ]
  node [
    id 1644
    label "niegdysiejszy"
  ]
  node [
    id 1645
    label "materia&#322;_budowlany"
  ]
  node [
    id 1646
    label "twarz"
  ]
  node [
    id 1647
    label "gun_muzzle"
  ]
  node [
    id 1648
    label "izolacja"
  ]
  node [
    id 1649
    label "&#380;onaty"
  ]
  node [
    id 1650
    label "syndrom_kuwady"
  ]
  node [
    id 1651
    label "na&#347;ladownictwo"
  ]
  node [
    id 1652
    label "zwyczaj"
  ]
  node [
    id 1653
    label "ci&#261;&#380;a"
  ]
  node [
    id 1654
    label "prawda"
  ]
  node [
    id 1655
    label "porcja"
  ]
  node [
    id 1656
    label "argument"
  ]
  node [
    id 1657
    label "parametr"
  ]
  node [
    id 1658
    label "operand"
  ]
  node [
    id 1659
    label "dow&#243;d"
  ]
  node [
    id 1660
    label "zmienna"
  ]
  node [
    id 1661
    label "argumentacja"
  ]
  node [
    id 1662
    label "&#380;o&#322;d"
  ]
  node [
    id 1663
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1664
    label "nieprawdziwy"
  ]
  node [
    id 1665
    label "prawdziwy"
  ]
  node [
    id 1666
    label "truth"
  ]
  node [
    id 1667
    label "realia"
  ]
  node [
    id 1668
    label "zesp&#243;&#322;"
  ]
  node [
    id 1669
    label "podejrzany"
  ]
  node [
    id 1670
    label "s&#261;downictwo"
  ]
  node [
    id 1671
    label "system"
  ]
  node [
    id 1672
    label "biuro"
  ]
  node [
    id 1673
    label "court"
  ]
  node [
    id 1674
    label "bronienie"
  ]
  node [
    id 1675
    label "urz&#261;d"
  ]
  node [
    id 1676
    label "oskar&#380;yciel"
  ]
  node [
    id 1677
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1678
    label "skazany"
  ]
  node [
    id 1679
    label "broni&#263;"
  ]
  node [
    id 1680
    label "my&#347;l"
  ]
  node [
    id 1681
    label "pods&#261;dny"
  ]
  node [
    id 1682
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1683
    label "obrona"
  ]
  node [
    id 1684
    label "instytucja"
  ]
  node [
    id 1685
    label "antylogizm"
  ]
  node [
    id 1686
    label "konektyw"
  ]
  node [
    id 1687
    label "&#347;wiadek"
  ]
  node [
    id 1688
    label "procesowicz"
  ]
  node [
    id 1689
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1690
    label "subject"
  ]
  node [
    id 1691
    label "czynnik"
  ]
  node [
    id 1692
    label "matuszka"
  ]
  node [
    id 1693
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1694
    label "geneza"
  ]
  node [
    id 1695
    label "poci&#261;ganie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 795
  ]
  edge [
    source 14
    target 796
  ]
  edge [
    source 14
    target 797
  ]
  edge [
    source 14
    target 798
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 802
  ]
  edge [
    source 14
    target 803
  ]
  edge [
    source 14
    target 804
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 14
    target 808
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 328
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 349
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 263
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 297
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 302
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 460
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 257
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 259
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 275
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 16
    target 984
  ]
  edge [
    source 16
    target 985
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 997
  ]
  edge [
    source 16
    target 998
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 318
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 286
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 269
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 265
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 446
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 579
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 338
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 499
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 147
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 625
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 481
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1111
  ]
  edge [
    source 20
    target 1112
  ]
  edge [
    source 20
    target 1113
  ]
  edge [
    source 20
    target 1114
  ]
  edge [
    source 20
    target 1115
  ]
  edge [
    source 20
    target 1116
  ]
  edge [
    source 20
    target 1117
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1119
  ]
  edge [
    source 20
    target 1120
  ]
  edge [
    source 20
    target 1121
  ]
  edge [
    source 20
    target 1122
  ]
  edge [
    source 20
    target 1123
  ]
  edge [
    source 20
    target 1124
  ]
  edge [
    source 20
    target 1125
  ]
  edge [
    source 20
    target 1126
  ]
  edge [
    source 20
    target 1127
  ]
  edge [
    source 20
    target 1128
  ]
  edge [
    source 20
    target 1129
  ]
  edge [
    source 20
    target 1130
  ]
  edge [
    source 20
    target 1131
  ]
  edge [
    source 20
    target 1132
  ]
  edge [
    source 20
    target 1133
  ]
  edge [
    source 20
    target 1134
  ]
  edge [
    source 20
    target 1135
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 1137
  ]
  edge [
    source 20
    target 1138
  ]
  edge [
    source 20
    target 1139
  ]
  edge [
    source 20
    target 1140
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1142
  ]
  edge [
    source 21
    target 537
  ]
  edge [
    source 21
    target 539
  ]
  edge [
    source 21
    target 1143
  ]
  edge [
    source 21
    target 542
  ]
  edge [
    source 21
    target 543
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 1144
  ]
  edge [
    source 21
    target 1145
  ]
  edge [
    source 21
    target 1146
  ]
  edge [
    source 21
    target 1147
  ]
  edge [
    source 21
    target 64
  ]
  edge [
    source 21
    target 1148
  ]
  edge [
    source 21
    target 74
  ]
  edge [
    source 21
    target 1149
  ]
  edge [
    source 21
    target 1150
  ]
  edge [
    source 21
    target 1151
  ]
  edge [
    source 21
    target 541
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 21
    target 1172
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1174
  ]
  edge [
    source 21
    target 1175
  ]
  edge [
    source 21
    target 1176
  ]
  edge [
    source 21
    target 1177
  ]
  edge [
    source 21
    target 1178
  ]
  edge [
    source 21
    target 1179
  ]
  edge [
    source 21
    target 1180
  ]
  edge [
    source 21
    target 1181
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 479
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 386
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 22
    target 1199
  ]
  edge [
    source 22
    target 1200
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 535
  ]
  edge [
    source 23
    target 74
  ]
  edge [
    source 23
    target 536
  ]
  edge [
    source 23
    target 537
  ]
  edge [
    source 23
    target 538
  ]
  edge [
    source 23
    target 539
  ]
  edge [
    source 23
    target 540
  ]
  edge [
    source 23
    target 541
  ]
  edge [
    source 23
    target 542
  ]
  edge [
    source 23
    target 64
  ]
  edge [
    source 23
    target 543
  ]
  edge [
    source 23
    target 1144
  ]
  edge [
    source 23
    target 1145
  ]
  edge [
    source 23
    target 1146
  ]
  edge [
    source 23
    target 1147
  ]
  edge [
    source 23
    target 1148
  ]
  edge [
    source 23
    target 1201
  ]
  edge [
    source 23
    target 1202
  ]
  edge [
    source 23
    target 1203
  ]
  edge [
    source 23
    target 1204
  ]
  edge [
    source 23
    target 76
  ]
  edge [
    source 23
    target 45
  ]
  edge [
    source 23
    target 1205
  ]
  edge [
    source 23
    target 1206
  ]
  edge [
    source 23
    target 1207
  ]
  edge [
    source 23
    target 57
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 1209
  ]
  edge [
    source 23
    target 1210
  ]
  edge [
    source 23
    target 1211
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 792
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 23
    target 1217
  ]
  edge [
    source 23
    target 1218
  ]
  edge [
    source 23
    target 1219
  ]
  edge [
    source 23
    target 1220
  ]
  edge [
    source 23
    target 378
  ]
  edge [
    source 23
    target 1221
  ]
  edge [
    source 23
    target 1153
  ]
  edge [
    source 23
    target 1154
  ]
  edge [
    source 23
    target 1155
  ]
  edge [
    source 23
    target 1156
  ]
  edge [
    source 23
    target 1157
  ]
  edge [
    source 23
    target 1158
  ]
  edge [
    source 23
    target 1159
  ]
  edge [
    source 23
    target 1160
  ]
  edge [
    source 23
    target 1161
  ]
  edge [
    source 23
    target 1162
  ]
  edge [
    source 23
    target 1163
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 1166
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 23
    target 580
  ]
  edge [
    source 23
    target 1222
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 83
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 24
    target 1226
  ]
  edge [
    source 24
    target 1227
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 90
  ]
  edge [
    source 24
    target 1228
  ]
  edge [
    source 24
    target 1229
  ]
  edge [
    source 24
    target 1230
  ]
  edge [
    source 24
    target 61
  ]
  edge [
    source 24
    target 71
  ]
  edge [
    source 24
    target 1231
  ]
  edge [
    source 24
    target 1232
  ]
  edge [
    source 24
    target 1233
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 24
    target 1234
  ]
  edge [
    source 24
    target 57
  ]
  edge [
    source 24
    target 74
  ]
  edge [
    source 24
    target 1235
  ]
  edge [
    source 24
    target 1236
  ]
  edge [
    source 24
    target 1237
  ]
  edge [
    source 24
    target 1238
  ]
  edge [
    source 24
    target 1239
  ]
  edge [
    source 24
    target 1240
  ]
  edge [
    source 24
    target 1241
  ]
  edge [
    source 24
    target 1242
  ]
  edge [
    source 24
    target 1243
  ]
  edge [
    source 24
    target 1244
  ]
  edge [
    source 24
    target 1245
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 468
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 625
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1257
  ]
  edge [
    source 26
    target 1258
  ]
  edge [
    source 26
    target 625
  ]
  edge [
    source 26
    target 1259
  ]
  edge [
    source 26
    target 1260
  ]
  edge [
    source 26
    target 1261
  ]
  edge [
    source 26
    target 1262
  ]
  edge [
    source 26
    target 1263
  ]
  edge [
    source 26
    target 1264
  ]
  edge [
    source 26
    target 1265
  ]
  edge [
    source 26
    target 1266
  ]
  edge [
    source 26
    target 1267
  ]
  edge [
    source 26
    target 1268
  ]
  edge [
    source 26
    target 1269
  ]
  edge [
    source 26
    target 1270
  ]
  edge [
    source 26
    target 1271
  ]
  edge [
    source 26
    target 1247
  ]
  edge [
    source 26
    target 1272
  ]
  edge [
    source 26
    target 1273
  ]
  edge [
    source 26
    target 1274
  ]
  edge [
    source 26
    target 1275
  ]
  edge [
    source 26
    target 1276
  ]
  edge [
    source 26
    target 1277
  ]
  edge [
    source 26
    target 1278
  ]
  edge [
    source 26
    target 1279
  ]
  edge [
    source 26
    target 1280
  ]
  edge [
    source 26
    target 1281
  ]
  edge [
    source 26
    target 1282
  ]
  edge [
    source 26
    target 1283
  ]
  edge [
    source 26
    target 1284
  ]
  edge [
    source 26
    target 1285
  ]
  edge [
    source 26
    target 1286
  ]
  edge [
    source 26
    target 1287
  ]
  edge [
    source 26
    target 1288
  ]
  edge [
    source 26
    target 1289
  ]
  edge [
    source 26
    target 1290
  ]
  edge [
    source 26
    target 1291
  ]
  edge [
    source 26
    target 1292
  ]
  edge [
    source 26
    target 1293
  ]
  edge [
    source 26
    target 1294
  ]
  edge [
    source 26
    target 1295
  ]
  edge [
    source 26
    target 1296
  ]
  edge [
    source 26
    target 1297
  ]
  edge [
    source 26
    target 1298
  ]
  edge [
    source 26
    target 1299
  ]
  edge [
    source 26
    target 1300
  ]
  edge [
    source 26
    target 1301
  ]
  edge [
    source 26
    target 1302
  ]
  edge [
    source 26
    target 1303
  ]
  edge [
    source 26
    target 1304
  ]
  edge [
    source 26
    target 194
  ]
  edge [
    source 26
    target 1305
  ]
  edge [
    source 26
    target 875
  ]
  edge [
    source 26
    target 883
  ]
  edge [
    source 26
    target 1306
  ]
  edge [
    source 26
    target 584
  ]
  edge [
    source 26
    target 402
  ]
  edge [
    source 26
    target 1307
  ]
  edge [
    source 26
    target 404
  ]
  edge [
    source 26
    target 1308
  ]
  edge [
    source 26
    target 592
  ]
  edge [
    source 26
    target 1309
  ]
  edge [
    source 26
    target 1310
  ]
  edge [
    source 26
    target 1311
  ]
  edge [
    source 26
    target 595
  ]
  edge [
    source 26
    target 596
  ]
  edge [
    source 26
    target 597
  ]
  edge [
    source 26
    target 1312
  ]
  edge [
    source 26
    target 606
  ]
  edge [
    source 26
    target 609
  ]
  edge [
    source 26
    target 1313
  ]
  edge [
    source 26
    target 610
  ]
  edge [
    source 26
    target 611
  ]
  edge [
    source 26
    target 1314
  ]
  edge [
    source 26
    target 1315
  ]
  edge [
    source 26
    target 612
  ]
  edge [
    source 26
    target 1316
  ]
  edge [
    source 26
    target 617
  ]
  edge [
    source 26
    target 1317
  ]
  edge [
    source 26
    target 1318
  ]
  edge [
    source 26
    target 1319
  ]
  edge [
    source 26
    target 1320
  ]
  edge [
    source 26
    target 996
  ]
  edge [
    source 26
    target 1321
  ]
  edge [
    source 26
    target 1322
  ]
  edge [
    source 26
    target 1323
  ]
  edge [
    source 26
    target 1324
  ]
  edge [
    source 26
    target 1325
  ]
  edge [
    source 26
    target 1326
  ]
  edge [
    source 26
    target 1327
  ]
  edge [
    source 26
    target 1328
  ]
  edge [
    source 26
    target 1329
  ]
  edge [
    source 26
    target 1330
  ]
  edge [
    source 26
    target 1331
  ]
  edge [
    source 26
    target 1332
  ]
  edge [
    source 26
    target 1333
  ]
  edge [
    source 26
    target 1334
  ]
  edge [
    source 26
    target 1335
  ]
  edge [
    source 26
    target 767
  ]
  edge [
    source 26
    target 1336
  ]
  edge [
    source 26
    target 1337
  ]
  edge [
    source 26
    target 1338
  ]
  edge [
    source 26
    target 1339
  ]
  edge [
    source 26
    target 1340
  ]
  edge [
    source 26
    target 1341
  ]
  edge [
    source 26
    target 1342
  ]
  edge [
    source 26
    target 1343
  ]
  edge [
    source 26
    target 1344
  ]
  edge [
    source 26
    target 1345
  ]
  edge [
    source 26
    target 1346
  ]
  edge [
    source 26
    target 1347
  ]
  edge [
    source 26
    target 1348
  ]
  edge [
    source 26
    target 1349
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 1350
  ]
  edge [
    source 26
    target 1351
  ]
  edge [
    source 26
    target 1352
  ]
  edge [
    source 26
    target 569
  ]
  edge [
    source 26
    target 1353
  ]
  edge [
    source 26
    target 1354
  ]
  edge [
    source 26
    target 1355
  ]
  edge [
    source 26
    target 1356
  ]
  edge [
    source 26
    target 1357
  ]
  edge [
    source 26
    target 1358
  ]
  edge [
    source 26
    target 1359
  ]
  edge [
    source 26
    target 1360
  ]
  edge [
    source 26
    target 1361
  ]
  edge [
    source 26
    target 1362
  ]
  edge [
    source 26
    target 1363
  ]
  edge [
    source 26
    target 1364
  ]
  edge [
    source 26
    target 1365
  ]
  edge [
    source 26
    target 1252
  ]
  edge [
    source 26
    target 1366
  ]
  edge [
    source 26
    target 1367
  ]
  edge [
    source 26
    target 1368
  ]
  edge [
    source 26
    target 1369
  ]
  edge [
    source 26
    target 1370
  ]
  edge [
    source 26
    target 1371
  ]
  edge [
    source 26
    target 1372
  ]
  edge [
    source 26
    target 1256
  ]
  edge [
    source 26
    target 1373
  ]
  edge [
    source 26
    target 1374
  ]
  edge [
    source 26
    target 718
  ]
  edge [
    source 26
    target 1375
  ]
  edge [
    source 26
    target 1376
  ]
  edge [
    source 26
    target 1377
  ]
  edge [
    source 26
    target 1249
  ]
  edge [
    source 26
    target 468
  ]
  edge [
    source 26
    target 1250
  ]
  edge [
    source 26
    target 1251
  ]
  edge [
    source 26
    target 1253
  ]
  edge [
    source 26
    target 1254
  ]
  edge [
    source 26
    target 1255
  ]
  edge [
    source 26
    target 1378
  ]
  edge [
    source 26
    target 1379
  ]
  edge [
    source 26
    target 1380
  ]
  edge [
    source 26
    target 197
  ]
  edge [
    source 26
    target 1381
  ]
  edge [
    source 26
    target 1382
  ]
  edge [
    source 26
    target 1383
  ]
  edge [
    source 26
    target 1384
  ]
  edge [
    source 26
    target 1385
  ]
  edge [
    source 26
    target 1386
  ]
  edge [
    source 26
    target 1387
  ]
  edge [
    source 26
    target 1388
  ]
  edge [
    source 26
    target 1389
  ]
  edge [
    source 26
    target 675
  ]
  edge [
    source 26
    target 1390
  ]
  edge [
    source 26
    target 1391
  ]
  edge [
    source 26
    target 1392
  ]
  edge [
    source 26
    target 1393
  ]
  edge [
    source 26
    target 1394
  ]
  edge [
    source 26
    target 1395
  ]
  edge [
    source 26
    target 1396
  ]
  edge [
    source 26
    target 1397
  ]
  edge [
    source 26
    target 1398
  ]
  edge [
    source 26
    target 1399
  ]
  edge [
    source 26
    target 1400
  ]
  edge [
    source 26
    target 1401
  ]
  edge [
    source 26
    target 1402
  ]
  edge [
    source 26
    target 1403
  ]
  edge [
    source 26
    target 1404
  ]
  edge [
    source 26
    target 1405
  ]
  edge [
    source 26
    target 1406
  ]
  edge [
    source 26
    target 1407
  ]
  edge [
    source 26
    target 1408
  ]
  edge [
    source 26
    target 1409
  ]
  edge [
    source 26
    target 1410
  ]
  edge [
    source 26
    target 1411
  ]
  edge [
    source 26
    target 1412
  ]
  edge [
    source 26
    target 1413
  ]
  edge [
    source 26
    target 1414
  ]
  edge [
    source 26
    target 1415
  ]
  edge [
    source 26
    target 1416
  ]
  edge [
    source 26
    target 1417
  ]
  edge [
    source 26
    target 479
  ]
  edge [
    source 26
    target 1418
  ]
  edge [
    source 26
    target 1419
  ]
  edge [
    source 26
    target 1420
  ]
  edge [
    source 26
    target 1421
  ]
  edge [
    source 26
    target 1422
  ]
  edge [
    source 26
    target 1423
  ]
  edge [
    source 26
    target 1424
  ]
  edge [
    source 26
    target 1425
  ]
  edge [
    source 26
    target 1426
  ]
  edge [
    source 26
    target 1427
  ]
  edge [
    source 26
    target 1428
  ]
  edge [
    source 26
    target 1429
  ]
  edge [
    source 26
    target 1430
  ]
  edge [
    source 26
    target 1431
  ]
  edge [
    source 26
    target 1432
  ]
  edge [
    source 26
    target 1433
  ]
  edge [
    source 26
    target 1434
  ]
  edge [
    source 26
    target 1435
  ]
  edge [
    source 26
    target 1436
  ]
  edge [
    source 26
    target 1437
  ]
  edge [
    source 26
    target 1438
  ]
  edge [
    source 26
    target 1439
  ]
  edge [
    source 26
    target 1440
  ]
  edge [
    source 26
    target 1441
  ]
  edge [
    source 26
    target 1442
  ]
  edge [
    source 26
    target 1443
  ]
  edge [
    source 26
    target 1444
  ]
  edge [
    source 26
    target 1445
  ]
  edge [
    source 26
    target 1446
  ]
  edge [
    source 26
    target 1447
  ]
  edge [
    source 26
    target 1448
  ]
  edge [
    source 26
    target 1449
  ]
  edge [
    source 26
    target 1450
  ]
  edge [
    source 26
    target 1451
  ]
  edge [
    source 26
    target 1452
  ]
  edge [
    source 26
    target 1453
  ]
  edge [
    source 26
    target 1454
  ]
  edge [
    source 26
    target 1455
  ]
  edge [
    source 26
    target 1456
  ]
  edge [
    source 26
    target 1457
  ]
  edge [
    source 26
    target 387
  ]
  edge [
    source 26
    target 1458
  ]
  edge [
    source 26
    target 1459
  ]
  edge [
    source 26
    target 1460
  ]
  edge [
    source 26
    target 1461
  ]
  edge [
    source 26
    target 1462
  ]
  edge [
    source 26
    target 1463
  ]
  edge [
    source 26
    target 1464
  ]
  edge [
    source 26
    target 1465
  ]
  edge [
    source 26
    target 1466
  ]
  edge [
    source 26
    target 1467
  ]
  edge [
    source 26
    target 1468
  ]
  edge [
    source 26
    target 1469
  ]
  edge [
    source 26
    target 1470
  ]
  edge [
    source 26
    target 1471
  ]
  edge [
    source 26
    target 1472
  ]
  edge [
    source 26
    target 1473
  ]
  edge [
    source 26
    target 1474
  ]
  edge [
    source 26
    target 1475
  ]
  edge [
    source 26
    target 1476
  ]
  edge [
    source 26
    target 1477
  ]
  edge [
    source 26
    target 1478
  ]
  edge [
    source 26
    target 1479
  ]
  edge [
    source 26
    target 1480
  ]
  edge [
    source 26
    target 1481
  ]
  edge [
    source 26
    target 1482
  ]
  edge [
    source 26
    target 1483
  ]
  edge [
    source 26
    target 1484
  ]
  edge [
    source 26
    target 1485
  ]
  edge [
    source 26
    target 1486
  ]
  edge [
    source 26
    target 1487
  ]
  edge [
    source 26
    target 1488
  ]
  edge [
    source 26
    target 1489
  ]
  edge [
    source 26
    target 1490
  ]
  edge [
    source 26
    target 1491
  ]
  edge [
    source 26
    target 1492
  ]
  edge [
    source 26
    target 1493
  ]
  edge [
    source 26
    target 1494
  ]
  edge [
    source 26
    target 1495
  ]
  edge [
    source 26
    target 1496
  ]
  edge [
    source 26
    target 1497
  ]
  edge [
    source 26
    target 1498
  ]
  edge [
    source 26
    target 1499
  ]
  edge [
    source 26
    target 1500
  ]
  edge [
    source 26
    target 1501
  ]
  edge [
    source 26
    target 1502
  ]
  edge [
    source 26
    target 1503
  ]
  edge [
    source 26
    target 1504
  ]
  edge [
    source 26
    target 1505
  ]
  edge [
    source 26
    target 1506
  ]
  edge [
    source 26
    target 1507
  ]
  edge [
    source 26
    target 1508
  ]
  edge [
    source 26
    target 1509
  ]
  edge [
    source 26
    target 1158
  ]
  edge [
    source 26
    target 1510
  ]
  edge [
    source 26
    target 1511
  ]
  edge [
    source 26
    target 1512
  ]
  edge [
    source 26
    target 1513
  ]
  edge [
    source 26
    target 1514
  ]
  edge [
    source 26
    target 1515
  ]
  edge [
    source 26
    target 1516
  ]
  edge [
    source 26
    target 1517
  ]
  edge [
    source 26
    target 1518
  ]
  edge [
    source 26
    target 1519
  ]
  edge [
    source 26
    target 1520
  ]
  edge [
    source 26
    target 1521
  ]
  edge [
    source 26
    target 1522
  ]
  edge [
    source 26
    target 1523
  ]
  edge [
    source 26
    target 1524
  ]
  edge [
    source 26
    target 1525
  ]
  edge [
    source 26
    target 1526
  ]
  edge [
    source 26
    target 1527
  ]
  edge [
    source 26
    target 1528
  ]
  edge [
    source 26
    target 1529
  ]
  edge [
    source 26
    target 1530
  ]
  edge [
    source 26
    target 1531
  ]
  edge [
    source 26
    target 1532
  ]
  edge [
    source 26
    target 1533
  ]
  edge [
    source 26
    target 1534
  ]
  edge [
    source 26
    target 1535
  ]
  edge [
    source 26
    target 1536
  ]
  edge [
    source 26
    target 1537
  ]
  edge [
    source 26
    target 1538
  ]
  edge [
    source 26
    target 1539
  ]
  edge [
    source 26
    target 1540
  ]
  edge [
    source 26
    target 1541
  ]
  edge [
    source 26
    target 1542
  ]
  edge [
    source 26
    target 1543
  ]
  edge [
    source 26
    target 1544
  ]
  edge [
    source 26
    target 1545
  ]
  edge [
    source 26
    target 1546
  ]
  edge [
    source 26
    target 1547
  ]
  edge [
    source 26
    target 1548
  ]
  edge [
    source 26
    target 1549
  ]
  edge [
    source 26
    target 1550
  ]
  edge [
    source 26
    target 1551
  ]
  edge [
    source 26
    target 1552
  ]
  edge [
    source 26
    target 1553
  ]
  edge [
    source 26
    target 1554
  ]
  edge [
    source 26
    target 1555
  ]
  edge [
    source 26
    target 1556
  ]
  edge [
    source 26
    target 1557
  ]
  edge [
    source 26
    target 1558
  ]
  edge [
    source 26
    target 1559
  ]
  edge [
    source 26
    target 1560
  ]
  edge [
    source 26
    target 1561
  ]
  edge [
    source 26
    target 1562
  ]
  edge [
    source 26
    target 1563
  ]
  edge [
    source 26
    target 1564
  ]
  edge [
    source 26
    target 1565
  ]
  edge [
    source 26
    target 1566
  ]
  edge [
    source 26
    target 1567
  ]
  edge [
    source 26
    target 1568
  ]
  edge [
    source 26
    target 1569
  ]
  edge [
    source 26
    target 1570
  ]
  edge [
    source 26
    target 1571
  ]
  edge [
    source 26
    target 1572
  ]
  edge [
    source 26
    target 1573
  ]
  edge [
    source 26
    target 1574
  ]
  edge [
    source 26
    target 1575
  ]
  edge [
    source 26
    target 1576
  ]
  edge [
    source 26
    target 1577
  ]
  edge [
    source 26
    target 1578
  ]
  edge [
    source 26
    target 1579
  ]
  edge [
    source 26
    target 1580
  ]
  edge [
    source 26
    target 1581
  ]
  edge [
    source 26
    target 1582
  ]
  edge [
    source 26
    target 386
  ]
  edge [
    source 26
    target 1583
  ]
  edge [
    source 26
    target 1584
  ]
  edge [
    source 26
    target 1585
  ]
  edge [
    source 26
    target 1586
  ]
  edge [
    source 26
    target 1587
  ]
  edge [
    source 26
    target 1588
  ]
  edge [
    source 26
    target 1589
  ]
  edge [
    source 26
    target 1590
  ]
  edge [
    source 26
    target 1591
  ]
  edge [
    source 26
    target 1592
  ]
  edge [
    source 26
    target 1593
  ]
  edge [
    source 26
    target 1594
  ]
  edge [
    source 26
    target 1595
  ]
  edge [
    source 26
    target 1596
  ]
  edge [
    source 26
    target 1597
  ]
  edge [
    source 26
    target 1598
  ]
  edge [
    source 26
    target 1599
  ]
  edge [
    source 26
    target 1600
  ]
  edge [
    source 26
    target 1601
  ]
  edge [
    source 26
    target 1602
  ]
  edge [
    source 26
    target 1603
  ]
  edge [
    source 26
    target 1604
  ]
  edge [
    source 26
    target 1605
  ]
  edge [
    source 26
    target 1606
  ]
  edge [
    source 26
    target 1607
  ]
  edge [
    source 26
    target 1608
  ]
  edge [
    source 26
    target 1609
  ]
  edge [
    source 26
    target 1610
  ]
  edge [
    source 26
    target 1611
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1363
  ]
  edge [
    source 27
    target 1259
  ]
  edge [
    source 27
    target 1365
  ]
  edge [
    source 27
    target 1370
  ]
  edge [
    source 27
    target 1368
  ]
  edge [
    source 27
    target 1371
  ]
  edge [
    source 27
    target 1372
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 468
  ]
  edge [
    source 27
    target 1471
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 1362
  ]
  edge [
    source 27
    target 1364
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 1366
  ]
  edge [
    source 27
    target 1367
  ]
  edge [
    source 27
    target 1369
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 1358
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 27
    target 1307
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 1628
  ]
  edge [
    source 27
    target 1629
  ]
  edge [
    source 27
    target 1630
  ]
  edge [
    source 27
    target 1631
  ]
  edge [
    source 27
    target 1632
  ]
  edge [
    source 27
    target 1633
  ]
  edge [
    source 27
    target 1247
  ]
  edge [
    source 27
    target 1634
  ]
  edge [
    source 27
    target 1635
  ]
  edge [
    source 27
    target 1636
  ]
  edge [
    source 27
    target 1637
  ]
  edge [
    source 27
    target 1375
  ]
  edge [
    source 27
    target 1638
  ]
  edge [
    source 27
    target 1301
  ]
  edge [
    source 27
    target 1639
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 1641
  ]
  edge [
    source 27
    target 1642
  ]
  edge [
    source 27
    target 1643
  ]
  edge [
    source 27
    target 1644
  ]
  edge [
    source 27
    target 1324
  ]
  edge [
    source 27
    target 1645
  ]
  edge [
    source 27
    target 1646
  ]
  edge [
    source 27
    target 1647
  ]
  edge [
    source 27
    target 1648
  ]
  edge [
    source 27
    target 1649
  ]
  edge [
    source 27
    target 1650
  ]
  edge [
    source 27
    target 1651
  ]
  edge [
    source 27
    target 1652
  ]
  edge [
    source 27
    target 1653
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 1654
  ]
  edge [
    source 28
    target 1655
  ]
  edge [
    source 28
    target 1656
  ]
  edge [
    source 28
    target 1046
  ]
  edge [
    source 28
    target 1657
  ]
  edge [
    source 28
    target 1658
  ]
  edge [
    source 28
    target 1659
  ]
  edge [
    source 28
    target 1660
  ]
  edge [
    source 28
    target 1661
  ]
  edge [
    source 28
    target 709
  ]
  edge [
    source 28
    target 237
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1662
  ]
  edge [
    source 28
    target 1663
  ]
  edge [
    source 28
    target 1664
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 1666
  ]
  edge [
    source 28
    target 1667
  ]
  edge [
    source 28
    target 1668
  ]
  edge [
    source 28
    target 1669
  ]
  edge [
    source 28
    target 1670
  ]
  edge [
    source 28
    target 1671
  ]
  edge [
    source 28
    target 1672
  ]
  edge [
    source 28
    target 632
  ]
  edge [
    source 28
    target 1673
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1674
  ]
  edge [
    source 28
    target 1675
  ]
  edge [
    source 28
    target 1010
  ]
  edge [
    source 28
    target 1676
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 395
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 1695
  ]
]
