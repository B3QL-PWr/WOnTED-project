graph [
  node [
    id 0
    label "kot"
    origin "text"
  ]
  node [
    id 1
    label "maja"
    origin "text"
  ]
  node [
    id 2
    label "dobry"
    origin "text"
  ]
  node [
    id 3
    label "pami&#281;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "miaucze&#263;"
  ]
  node [
    id 5
    label "odk&#322;aczacz"
  ]
  node [
    id 6
    label "otrz&#281;siny"
  ]
  node [
    id 7
    label "pierwszoklasista"
  ]
  node [
    id 8
    label "czworon&#243;g"
  ]
  node [
    id 9
    label "zamiaucze&#263;"
  ]
  node [
    id 10
    label "miauczenie"
  ]
  node [
    id 11
    label "zamiauczenie"
  ]
  node [
    id 12
    label "kotowate"
  ]
  node [
    id 13
    label "trackball"
  ]
  node [
    id 14
    label "kabanos"
  ]
  node [
    id 15
    label "felinoterapia"
  ]
  node [
    id 16
    label "zaj&#261;c"
  ]
  node [
    id 17
    label "kotwica"
  ]
  node [
    id 18
    label "samiec"
  ]
  node [
    id 19
    label "rekrut"
  ]
  node [
    id 20
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 21
    label "miaukni&#281;cie"
  ]
  node [
    id 22
    label "fala"
  ]
  node [
    id 23
    label "laptop"
  ]
  node [
    id 24
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 25
    label "&#380;o&#322;nierz"
  ]
  node [
    id 26
    label "kantonista"
  ]
  node [
    id 27
    label "poborowy"
  ]
  node [
    id 28
    label "ucze&#324;"
  ]
  node [
    id 29
    label "pierwszoroczny"
  ]
  node [
    id 30
    label "dziecko"
  ]
  node [
    id 31
    label "kszta&#322;t"
  ]
  node [
    id 32
    label "pasemko"
  ]
  node [
    id 33
    label "znak_diakrytyczny"
  ]
  node [
    id 34
    label "zjawisko"
  ]
  node [
    id 35
    label "zafalowanie"
  ]
  node [
    id 36
    label "przemoc"
  ]
  node [
    id 37
    label "reakcja"
  ]
  node [
    id 38
    label "strumie&#324;"
  ]
  node [
    id 39
    label "karb"
  ]
  node [
    id 40
    label "mn&#243;stwo"
  ]
  node [
    id 41
    label "fit"
  ]
  node [
    id 42
    label "grzywa_fali"
  ]
  node [
    id 43
    label "woda"
  ]
  node [
    id 44
    label "efekt_Dopplera"
  ]
  node [
    id 45
    label "obcinka"
  ]
  node [
    id 46
    label "t&#322;um"
  ]
  node [
    id 47
    label "okres"
  ]
  node [
    id 48
    label "stream"
  ]
  node [
    id 49
    label "zafalowa&#263;"
  ]
  node [
    id 50
    label "rozbicie_si&#281;"
  ]
  node [
    id 51
    label "wojsko"
  ]
  node [
    id 52
    label "clutter"
  ]
  node [
    id 53
    label "rozbijanie_si&#281;"
  ]
  node [
    id 54
    label "czo&#322;o_fali"
  ]
  node [
    id 55
    label "zabawa"
  ]
  node [
    id 56
    label "zwyczaj"
  ]
  node [
    id 57
    label "przek&#261;ska"
  ]
  node [
    id 58
    label "w&#281;dzi&#263;"
  ]
  node [
    id 59
    label "przysmak"
  ]
  node [
    id 60
    label "pies"
  ]
  node [
    id 61
    label "kie&#322;basa"
  ]
  node [
    id 62
    label "cygaro"
  ]
  node [
    id 63
    label "substancja"
  ]
  node [
    id 64
    label "karma"
  ]
  node [
    id 65
    label "wydanie"
  ]
  node [
    id 66
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 67
    label "meow"
  ]
  node [
    id 68
    label "proszenie"
  ]
  node [
    id 69
    label "wyj&#281;czenie"
  ]
  node [
    id 70
    label "wydawanie"
  ]
  node [
    id 71
    label "whimper"
  ]
  node [
    id 72
    label "zooterapia"
  ]
  node [
    id 73
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 74
    label "prosi&#263;"
  ]
  node [
    id 75
    label "snivel"
  ]
  node [
    id 76
    label "critter"
  ]
  node [
    id 77
    label "zwierz&#281;_domowe"
  ]
  node [
    id 78
    label "kr&#281;gowiec"
  ]
  node [
    id 79
    label "tetrapody"
  ]
  node [
    id 80
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 81
    label "zwierz&#281;"
  ]
  node [
    id 82
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 83
    label "skrom"
  ]
  node [
    id 84
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 85
    label "trzeszcze"
  ]
  node [
    id 86
    label "zaj&#261;cowate"
  ]
  node [
    id 87
    label "kicaj"
  ]
  node [
    id 88
    label "omyk"
  ]
  node [
    id 89
    label "kopyra"
  ]
  node [
    id 90
    label "dziczyzna"
  ]
  node [
    id 91
    label "skok"
  ]
  node [
    id 92
    label "turzyca"
  ]
  node [
    id 93
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 94
    label "parkot"
  ]
  node [
    id 95
    label "narz&#281;dzie"
  ]
  node [
    id 96
    label "kotwiczy&#263;"
  ]
  node [
    id 97
    label "zakotwiczenie"
  ]
  node [
    id 98
    label "emocja"
  ]
  node [
    id 99
    label "wybieranie"
  ]
  node [
    id 100
    label "wybiera&#263;"
  ]
  node [
    id 101
    label "statek"
  ]
  node [
    id 102
    label "zegar"
  ]
  node [
    id 103
    label "zakotwiczy&#263;"
  ]
  node [
    id 104
    label "wybra&#263;"
  ]
  node [
    id 105
    label "wybranie"
  ]
  node [
    id 106
    label "kotwiczenie"
  ]
  node [
    id 107
    label "kotokszta&#322;tne"
  ]
  node [
    id 108
    label "energia"
  ]
  node [
    id 109
    label "wedyzm"
  ]
  node [
    id 110
    label "buddyzm"
  ]
  node [
    id 111
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 112
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 113
    label "egzergia"
  ]
  node [
    id 114
    label "emitowa&#263;"
  ]
  node [
    id 115
    label "kwant_energii"
  ]
  node [
    id 116
    label "szwung"
  ]
  node [
    id 117
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 118
    label "power"
  ]
  node [
    id 119
    label "cecha"
  ]
  node [
    id 120
    label "emitowanie"
  ]
  node [
    id 121
    label "energy"
  ]
  node [
    id 122
    label "kalpa"
  ]
  node [
    id 123
    label "lampka_ma&#347;lana"
  ]
  node [
    id 124
    label "Buddhism"
  ]
  node [
    id 125
    label "dana"
  ]
  node [
    id 126
    label "mahajana"
  ]
  node [
    id 127
    label "asura"
  ]
  node [
    id 128
    label "wad&#378;rajana"
  ]
  node [
    id 129
    label "bonzo"
  ]
  node [
    id 130
    label "therawada"
  ]
  node [
    id 131
    label "tantryzm"
  ]
  node [
    id 132
    label "hinajana"
  ]
  node [
    id 133
    label "bardo"
  ]
  node [
    id 134
    label "arahant"
  ]
  node [
    id 135
    label "religia"
  ]
  node [
    id 136
    label "ahinsa"
  ]
  node [
    id 137
    label "li"
  ]
  node [
    id 138
    label "hinduizm"
  ]
  node [
    id 139
    label "dobroczynny"
  ]
  node [
    id 140
    label "czw&#243;rka"
  ]
  node [
    id 141
    label "spokojny"
  ]
  node [
    id 142
    label "skuteczny"
  ]
  node [
    id 143
    label "&#347;mieszny"
  ]
  node [
    id 144
    label "mi&#322;y"
  ]
  node [
    id 145
    label "grzeczny"
  ]
  node [
    id 146
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 147
    label "powitanie"
  ]
  node [
    id 148
    label "dobrze"
  ]
  node [
    id 149
    label "ca&#322;y"
  ]
  node [
    id 150
    label "zwrot"
  ]
  node [
    id 151
    label "pomy&#347;lny"
  ]
  node [
    id 152
    label "moralny"
  ]
  node [
    id 153
    label "drogi"
  ]
  node [
    id 154
    label "pozytywny"
  ]
  node [
    id 155
    label "odpowiedni"
  ]
  node [
    id 156
    label "korzystny"
  ]
  node [
    id 157
    label "pos&#322;uszny"
  ]
  node [
    id 158
    label "moralnie"
  ]
  node [
    id 159
    label "warto&#347;ciowy"
  ]
  node [
    id 160
    label "etycznie"
  ]
  node [
    id 161
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 162
    label "nale&#380;ny"
  ]
  node [
    id 163
    label "nale&#380;yty"
  ]
  node [
    id 164
    label "typowy"
  ]
  node [
    id 165
    label "uprawniony"
  ]
  node [
    id 166
    label "zasadniczy"
  ]
  node [
    id 167
    label "stosownie"
  ]
  node [
    id 168
    label "taki"
  ]
  node [
    id 169
    label "charakterystyczny"
  ]
  node [
    id 170
    label "prawdziwy"
  ]
  node [
    id 171
    label "ten"
  ]
  node [
    id 172
    label "pozytywnie"
  ]
  node [
    id 173
    label "fajny"
  ]
  node [
    id 174
    label "dodatnio"
  ]
  node [
    id 175
    label "przyjemny"
  ]
  node [
    id 176
    label "po&#380;&#261;dany"
  ]
  node [
    id 177
    label "niepowa&#380;ny"
  ]
  node [
    id 178
    label "o&#347;mieszanie"
  ]
  node [
    id 179
    label "&#347;miesznie"
  ]
  node [
    id 180
    label "bawny"
  ]
  node [
    id 181
    label "o&#347;mieszenie"
  ]
  node [
    id 182
    label "dziwny"
  ]
  node [
    id 183
    label "nieadekwatny"
  ]
  node [
    id 184
    label "wolny"
  ]
  node [
    id 185
    label "uspokajanie_si&#281;"
  ]
  node [
    id 186
    label "bezproblemowy"
  ]
  node [
    id 187
    label "spokojnie"
  ]
  node [
    id 188
    label "uspokojenie_si&#281;"
  ]
  node [
    id 189
    label "cicho"
  ]
  node [
    id 190
    label "uspokojenie"
  ]
  node [
    id 191
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 192
    label "nietrudny"
  ]
  node [
    id 193
    label "uspokajanie"
  ]
  node [
    id 194
    label "zale&#380;ny"
  ]
  node [
    id 195
    label "uleg&#322;y"
  ]
  node [
    id 196
    label "pos&#322;usznie"
  ]
  node [
    id 197
    label "grzecznie"
  ]
  node [
    id 198
    label "stosowny"
  ]
  node [
    id 199
    label "niewinny"
  ]
  node [
    id 200
    label "konserwatywny"
  ]
  node [
    id 201
    label "nijaki"
  ]
  node [
    id 202
    label "korzystnie"
  ]
  node [
    id 203
    label "drogo"
  ]
  node [
    id 204
    label "cz&#322;owiek"
  ]
  node [
    id 205
    label "bliski"
  ]
  node [
    id 206
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 207
    label "przyjaciel"
  ]
  node [
    id 208
    label "jedyny"
  ]
  node [
    id 209
    label "du&#380;y"
  ]
  node [
    id 210
    label "zdr&#243;w"
  ]
  node [
    id 211
    label "calu&#347;ko"
  ]
  node [
    id 212
    label "kompletny"
  ]
  node [
    id 213
    label "&#380;ywy"
  ]
  node [
    id 214
    label "pe&#322;ny"
  ]
  node [
    id 215
    label "podobny"
  ]
  node [
    id 216
    label "ca&#322;o"
  ]
  node [
    id 217
    label "poskutkowanie"
  ]
  node [
    id 218
    label "sprawny"
  ]
  node [
    id 219
    label "skutecznie"
  ]
  node [
    id 220
    label "skutkowanie"
  ]
  node [
    id 221
    label "pomy&#347;lnie"
  ]
  node [
    id 222
    label "toto-lotek"
  ]
  node [
    id 223
    label "trafienie"
  ]
  node [
    id 224
    label "zbi&#243;r"
  ]
  node [
    id 225
    label "arkusz_drukarski"
  ]
  node [
    id 226
    label "&#322;&#243;dka"
  ]
  node [
    id 227
    label "four"
  ]
  node [
    id 228
    label "&#263;wiartka"
  ]
  node [
    id 229
    label "hotel"
  ]
  node [
    id 230
    label "cyfra"
  ]
  node [
    id 231
    label "pok&#243;j"
  ]
  node [
    id 232
    label "stopie&#324;"
  ]
  node [
    id 233
    label "obiekt"
  ]
  node [
    id 234
    label "minialbum"
  ]
  node [
    id 235
    label "osada"
  ]
  node [
    id 236
    label "p&#322;yta_winylowa"
  ]
  node [
    id 237
    label "blotka"
  ]
  node [
    id 238
    label "zaprz&#281;g"
  ]
  node [
    id 239
    label "przedtrzonowiec"
  ]
  node [
    id 240
    label "punkt"
  ]
  node [
    id 241
    label "turn"
  ]
  node [
    id 242
    label "turning"
  ]
  node [
    id 243
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 244
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 245
    label "skr&#281;t"
  ]
  node [
    id 246
    label "obr&#243;t"
  ]
  node [
    id 247
    label "fraza_czasownikowa"
  ]
  node [
    id 248
    label "jednostka_leksykalna"
  ]
  node [
    id 249
    label "zmiana"
  ]
  node [
    id 250
    label "wyra&#380;enie"
  ]
  node [
    id 251
    label "welcome"
  ]
  node [
    id 252
    label "spotkanie"
  ]
  node [
    id 253
    label "pozdrowienie"
  ]
  node [
    id 254
    label "greeting"
  ]
  node [
    id 255
    label "zdarzony"
  ]
  node [
    id 256
    label "odpowiednio"
  ]
  node [
    id 257
    label "odpowiadanie"
  ]
  node [
    id 258
    label "specjalny"
  ]
  node [
    id 259
    label "kochanek"
  ]
  node [
    id 260
    label "sk&#322;onny"
  ]
  node [
    id 261
    label "wybranek"
  ]
  node [
    id 262
    label "umi&#322;owany"
  ]
  node [
    id 263
    label "przyjemnie"
  ]
  node [
    id 264
    label "mi&#322;o"
  ]
  node [
    id 265
    label "kochanie"
  ]
  node [
    id 266
    label "dyplomata"
  ]
  node [
    id 267
    label "dobroczynnie"
  ]
  node [
    id 268
    label "lepiej"
  ]
  node [
    id 269
    label "wiele"
  ]
  node [
    id 270
    label "spo&#322;eczny"
  ]
  node [
    id 271
    label "hipokamp"
  ]
  node [
    id 272
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 273
    label "wytw&#243;r"
  ]
  node [
    id 274
    label "memory"
  ]
  node [
    id 275
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 276
    label "umys&#322;"
  ]
  node [
    id 277
    label "komputer"
  ]
  node [
    id 278
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 279
    label "zachowa&#263;"
  ]
  node [
    id 280
    label "wymazanie"
  ]
  node [
    id 281
    label "przedmiot"
  ]
  node [
    id 282
    label "p&#322;&#243;d"
  ]
  node [
    id 283
    label "work"
  ]
  node [
    id 284
    label "rezultat"
  ]
  node [
    id 285
    label "intelekt"
  ]
  node [
    id 286
    label "pomieszanie_si&#281;"
  ]
  node [
    id 287
    label "wn&#281;trze"
  ]
  node [
    id 288
    label "wyobra&#378;nia"
  ]
  node [
    id 289
    label "stacja_dysk&#243;w"
  ]
  node [
    id 290
    label "instalowa&#263;"
  ]
  node [
    id 291
    label "moc_obliczeniowa"
  ]
  node [
    id 292
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 293
    label "pad"
  ]
  node [
    id 294
    label "modem"
  ]
  node [
    id 295
    label "monitor"
  ]
  node [
    id 296
    label "zainstalowanie"
  ]
  node [
    id 297
    label "emulacja"
  ]
  node [
    id 298
    label "zainstalowa&#263;"
  ]
  node [
    id 299
    label "procesor"
  ]
  node [
    id 300
    label "maszyna_Turinga"
  ]
  node [
    id 301
    label "karta"
  ]
  node [
    id 302
    label "twardy_dysk"
  ]
  node [
    id 303
    label "klawiatura"
  ]
  node [
    id 304
    label "botnet"
  ]
  node [
    id 305
    label "mysz"
  ]
  node [
    id 306
    label "instalowanie"
  ]
  node [
    id 307
    label "radiator"
  ]
  node [
    id 308
    label "urz&#261;dzenie"
  ]
  node [
    id 309
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 310
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 311
    label "zakr&#281;t_z&#281;baty"
  ]
  node [
    id 312
    label "usuni&#281;cie"
  ]
  node [
    id 313
    label "erasure"
  ]
  node [
    id 314
    label "zabrudzenie"
  ]
  node [
    id 315
    label "expunction"
  ]
  node [
    id 316
    label "removal"
  ]
  node [
    id 317
    label "post&#261;pi&#263;"
  ]
  node [
    id 318
    label "tajemnica"
  ]
  node [
    id 319
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 320
    label "zdyscyplinowanie"
  ]
  node [
    id 321
    label "post"
  ]
  node [
    id 322
    label "zrobi&#263;"
  ]
  node [
    id 323
    label "przechowa&#263;"
  ]
  node [
    id 324
    label "preserve"
  ]
  node [
    id 325
    label "dieta"
  ]
  node [
    id 326
    label "bury"
  ]
  node [
    id 327
    label "podtrzyma&#263;"
  ]
  node [
    id 328
    label "Behavioural"
  ]
  node [
    id 329
    label "Processes"
  ]
  node [
    id 330
    label "Kyoto"
  ]
  node [
    id 331
    label "University"
  ]
  node [
    id 332
    label "John"
  ]
  node [
    id 333
    label "Paul"
  ]
  node [
    id 334
    label "Scott"
  ]
  node [
    id 335
    label "laboratorium"
  ]
  node [
    id 336
    label "zachowanie"
  ]
  node [
    id 337
    label "w"
  ]
  node [
    id 338
    label "bar"
  ]
  node [
    id 339
    label "Harbor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 81
    target 335
  ]
  edge [
    source 81
    target 336
  ]
  edge [
    source 81
    target 337
  ]
  edge [
    source 81
    target 338
  ]
  edge [
    source 81
    target 339
  ]
  edge [
    source 328
    target 329
  ]
  edge [
    source 330
    target 331
  ]
  edge [
    source 332
    target 333
  ]
  edge [
    source 332
    target 334
  ]
  edge [
    source 333
    target 334
  ]
  edge [
    source 335
    target 336
  ]
  edge [
    source 335
    target 337
  ]
  edge [
    source 335
    target 338
  ]
  edge [
    source 335
    target 339
  ]
  edge [
    source 336
    target 337
  ]
  edge [
    source 336
    target 338
  ]
  edge [
    source 336
    target 339
  ]
  edge [
    source 337
    target 338
  ]
  edge [
    source 337
    target 339
  ]
  edge [
    source 338
    target 339
  ]
]
