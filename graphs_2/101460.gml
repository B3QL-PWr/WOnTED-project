graph [
  node [
    id 0
    label "lista"
    origin "text"
  ]
  node [
    id 1
    label "planetoida"
    origin "text"
  ]
  node [
    id 2
    label "catalog"
  ]
  node [
    id 3
    label "figurowa&#263;"
  ]
  node [
    id 4
    label "tekst"
  ]
  node [
    id 5
    label "pozycja"
  ]
  node [
    id 6
    label "zbi&#243;r"
  ]
  node [
    id 7
    label "wyliczanka"
  ]
  node [
    id 8
    label "sumariusz"
  ]
  node [
    id 9
    label "stock"
  ]
  node [
    id 10
    label "book"
  ]
  node [
    id 11
    label "pisa&#263;"
  ]
  node [
    id 12
    label "j&#281;zykowo"
  ]
  node [
    id 13
    label "redakcja"
  ]
  node [
    id 14
    label "preparacja"
  ]
  node [
    id 15
    label "dzie&#322;o"
  ]
  node [
    id 16
    label "wypowied&#378;"
  ]
  node [
    id 17
    label "obelga"
  ]
  node [
    id 18
    label "wytw&#243;r"
  ]
  node [
    id 19
    label "odmianka"
  ]
  node [
    id 20
    label "opu&#347;ci&#263;"
  ]
  node [
    id 21
    label "pomini&#281;cie"
  ]
  node [
    id 22
    label "koniektura"
  ]
  node [
    id 23
    label "ekscerpcja"
  ]
  node [
    id 24
    label "poj&#281;cie"
  ]
  node [
    id 25
    label "pakiet_klimatyczny"
  ]
  node [
    id 26
    label "uprawianie"
  ]
  node [
    id 27
    label "collection"
  ]
  node [
    id 28
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 29
    label "gathering"
  ]
  node [
    id 30
    label "album"
  ]
  node [
    id 31
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 32
    label "praca_rolnicza"
  ]
  node [
    id 33
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 34
    label "sum"
  ]
  node [
    id 35
    label "egzemplarz"
  ]
  node [
    id 36
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 37
    label "series"
  ]
  node [
    id 38
    label "dane"
  ]
  node [
    id 39
    label "rz&#261;d"
  ]
  node [
    id 40
    label "spis"
  ]
  node [
    id 41
    label "awans"
  ]
  node [
    id 42
    label "debit"
  ]
  node [
    id 43
    label "sytuacja"
  ]
  node [
    id 44
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 45
    label "po&#322;o&#380;enie"
  ]
  node [
    id 46
    label "publikacja"
  ]
  node [
    id 47
    label "rozmieszczenie"
  ]
  node [
    id 48
    label "znaczenie"
  ]
  node [
    id 49
    label "miejsce"
  ]
  node [
    id 50
    label "szata_graficzna"
  ]
  node [
    id 51
    label "ustawienie"
  ]
  node [
    id 52
    label "awansowanie"
  ]
  node [
    id 53
    label "le&#380;e&#263;"
  ]
  node [
    id 54
    label "wydawa&#263;"
  ]
  node [
    id 55
    label "szermierka"
  ]
  node [
    id 56
    label "druk"
  ]
  node [
    id 57
    label "awansowa&#263;"
  ]
  node [
    id 58
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 59
    label "poster"
  ]
  node [
    id 60
    label "redaktor"
  ]
  node [
    id 61
    label "adres"
  ]
  node [
    id 62
    label "status"
  ]
  node [
    id 63
    label "bearing"
  ]
  node [
    id 64
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 65
    label "wojsko"
  ]
  node [
    id 66
    label "wyda&#263;"
  ]
  node [
    id 67
    label "entliczek"
  ]
  node [
    id 68
    label "wiersz"
  ]
  node [
    id 69
    label "zabawa"
  ]
  node [
    id 70
    label "pentliczek"
  ]
  node [
    id 71
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 72
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 73
    label "minor_planet"
  ]
  node [
    id 74
    label "Orkus"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
]
