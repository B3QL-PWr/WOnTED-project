graph [
  node [
    id 0
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 1
    label "blok"
  ]
  node [
    id 2
    label "prawda"
  ]
  node [
    id 3
    label "znak_j&#281;zykowy"
  ]
  node [
    id 4
    label "nag&#322;&#243;wek"
  ]
  node [
    id 5
    label "szkic"
  ]
  node [
    id 6
    label "line"
  ]
  node [
    id 7
    label "fragment"
  ]
  node [
    id 8
    label "tekst"
  ]
  node [
    id 9
    label "wyr&#243;b"
  ]
  node [
    id 10
    label "rodzajnik"
  ]
  node [
    id 11
    label "dokument"
  ]
  node [
    id 12
    label "towar"
  ]
  node [
    id 13
    label "paragraf"
  ]
  node [
    id 14
    label "ekscerpcja"
  ]
  node [
    id 15
    label "j&#281;zykowo"
  ]
  node [
    id 16
    label "wypowied&#378;"
  ]
  node [
    id 17
    label "redakcja"
  ]
  node [
    id 18
    label "wytw&#243;r"
  ]
  node [
    id 19
    label "pomini&#281;cie"
  ]
  node [
    id 20
    label "dzie&#322;o"
  ]
  node [
    id 21
    label "preparacja"
  ]
  node [
    id 22
    label "odmianka"
  ]
  node [
    id 23
    label "opu&#347;ci&#263;"
  ]
  node [
    id 24
    label "koniektura"
  ]
  node [
    id 25
    label "pisa&#263;"
  ]
  node [
    id 26
    label "obelga"
  ]
  node [
    id 27
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 28
    label "utw&#243;r"
  ]
  node [
    id 29
    label "s&#261;d"
  ]
  node [
    id 30
    label "za&#322;o&#380;enie"
  ]
  node [
    id 31
    label "nieprawdziwy"
  ]
  node [
    id 32
    label "prawdziwy"
  ]
  node [
    id 33
    label "truth"
  ]
  node [
    id 34
    label "realia"
  ]
  node [
    id 35
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 36
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 37
    label "produkt"
  ]
  node [
    id 38
    label "creation"
  ]
  node [
    id 39
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 40
    label "p&#322;uczkarnia"
  ]
  node [
    id 41
    label "znakowarka"
  ]
  node [
    id 42
    label "produkcja"
  ]
  node [
    id 43
    label "tytu&#322;"
  ]
  node [
    id 44
    label "head"
  ]
  node [
    id 45
    label "znak_pisarski"
  ]
  node [
    id 46
    label "przepis"
  ]
  node [
    id 47
    label "bajt"
  ]
  node [
    id 48
    label "bloking"
  ]
  node [
    id 49
    label "j&#261;kanie"
  ]
  node [
    id 50
    label "przeszkoda"
  ]
  node [
    id 51
    label "zesp&#243;&#322;"
  ]
  node [
    id 52
    label "blokada"
  ]
  node [
    id 53
    label "bry&#322;a"
  ]
  node [
    id 54
    label "dzia&#322;"
  ]
  node [
    id 55
    label "kontynent"
  ]
  node [
    id 56
    label "nastawnia"
  ]
  node [
    id 57
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 58
    label "blockage"
  ]
  node [
    id 59
    label "zbi&#243;r"
  ]
  node [
    id 60
    label "block"
  ]
  node [
    id 61
    label "organizacja"
  ]
  node [
    id 62
    label "budynek"
  ]
  node [
    id 63
    label "start"
  ]
  node [
    id 64
    label "skorupa_ziemska"
  ]
  node [
    id 65
    label "program"
  ]
  node [
    id 66
    label "zeszyt"
  ]
  node [
    id 67
    label "grupa"
  ]
  node [
    id 68
    label "blokowisko"
  ]
  node [
    id 69
    label "barak"
  ]
  node [
    id 70
    label "stok_kontynentalny"
  ]
  node [
    id 71
    label "whole"
  ]
  node [
    id 72
    label "square"
  ]
  node [
    id 73
    label "siatk&#243;wka"
  ]
  node [
    id 74
    label "kr&#261;g"
  ]
  node [
    id 75
    label "ram&#243;wka"
  ]
  node [
    id 76
    label "zamek"
  ]
  node [
    id 77
    label "obrona"
  ]
  node [
    id 78
    label "ok&#322;adka"
  ]
  node [
    id 79
    label "bie&#380;nia"
  ]
  node [
    id 80
    label "referat"
  ]
  node [
    id 81
    label "dom_wielorodzinny"
  ]
  node [
    id 82
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 83
    label "zapis"
  ]
  node [
    id 84
    label "&#347;wiadectwo"
  ]
  node [
    id 85
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 86
    label "parafa"
  ]
  node [
    id 87
    label "plik"
  ]
  node [
    id 88
    label "raport&#243;wka"
  ]
  node [
    id 89
    label "record"
  ]
  node [
    id 90
    label "registratura"
  ]
  node [
    id 91
    label "dokumentacja"
  ]
  node [
    id 92
    label "fascyku&#322;"
  ]
  node [
    id 93
    label "writing"
  ]
  node [
    id 94
    label "sygnatariusz"
  ]
  node [
    id 95
    label "rysunek"
  ]
  node [
    id 96
    label "szkicownik"
  ]
  node [
    id 97
    label "opracowanie"
  ]
  node [
    id 98
    label "sketch"
  ]
  node [
    id 99
    label "plot"
  ]
  node [
    id 100
    label "pomys&#322;"
  ]
  node [
    id 101
    label "opowiadanie"
  ]
  node [
    id 102
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 103
    label "metka"
  ]
  node [
    id 104
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 105
    label "cz&#322;owiek"
  ]
  node [
    id 106
    label "szprycowa&#263;"
  ]
  node [
    id 107
    label "naszprycowa&#263;"
  ]
  node [
    id 108
    label "rzuca&#263;"
  ]
  node [
    id 109
    label "tandeta"
  ]
  node [
    id 110
    label "obr&#243;t_handlowy"
  ]
  node [
    id 111
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 112
    label "rzuci&#263;"
  ]
  node [
    id 113
    label "naszprycowanie"
  ]
  node [
    id 114
    label "tkanina"
  ]
  node [
    id 115
    label "szprycowanie"
  ]
  node [
    id 116
    label "za&#322;adownia"
  ]
  node [
    id 117
    label "asortyment"
  ]
  node [
    id 118
    label "&#322;&#243;dzki"
  ]
  node [
    id 119
    label "narkobiznes"
  ]
  node [
    id 120
    label "rzucenie"
  ]
  node [
    id 121
    label "rzucanie"
  ]
  node [
    id 122
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 123
    label "transgraniczny"
  ]
  node [
    id 124
    label "PHARE"
  ]
  node [
    id 125
    label "perspektywa"
  ]
  node [
    id 126
    label "finansowy"
  ]
  node [
    id 127
    label "porozumie&#263;"
  ]
  node [
    id 128
    label "Mi&#281;dzyinstytucjonalnego"
  ]
  node [
    id 129
    label "europejski"
  ]
  node [
    id 130
    label "fundusz"
  ]
  node [
    id 131
    label "orientacja"
  ]
  node [
    id 132
    label "i"
  ]
  node [
    id 133
    label "gwarancja"
  ]
  node [
    id 134
    label "rolny"
  ]
  node [
    id 135
    label "sekcja"
  ]
  node [
    id 136
    label "wsp&#243;lnota"
  ]
  node [
    id 137
    label "rada"
  ]
  node [
    id 138
    label "stowarzyszy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 124
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 131
  ]
  edge [
    source 129
    target 132
  ]
  edge [
    source 129
    target 133
  ]
  edge [
    source 129
    target 134
  ]
  edge [
    source 129
    target 136
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 132
  ]
  edge [
    source 130
    target 133
  ]
  edge [
    source 130
    target 134
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 133
  ]
  edge [
    source 131
    target 134
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 132
    target 134
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 135
  ]
  edge [
    source 137
    target 138
  ]
]
