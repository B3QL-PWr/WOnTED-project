graph [
  node [
    id 0
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 2
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 3
    label "r&#281;czny"
    origin "text"
  ]
  node [
    id 4
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 5
    label "championship"
  ]
  node [
    id 6
    label "zawody"
  ]
  node [
    id 7
    label "Formu&#322;a_1"
  ]
  node [
    id 8
    label "tysi&#281;cznik"
  ]
  node [
    id 9
    label "impreza"
  ]
  node [
    id 10
    label "walczenie"
  ]
  node [
    id 11
    label "contest"
  ]
  node [
    id 12
    label "rywalizacja"
  ]
  node [
    id 13
    label "spadochroniarstwo"
  ]
  node [
    id 14
    label "kategoria_open"
  ]
  node [
    id 15
    label "champion"
  ]
  node [
    id 16
    label "walczy&#263;"
  ]
  node [
    id 17
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "przyroda"
  ]
  node [
    id 19
    label "Wsch&#243;d"
  ]
  node [
    id 20
    label "obszar"
  ]
  node [
    id 21
    label "morze"
  ]
  node [
    id 22
    label "kuchnia"
  ]
  node [
    id 23
    label "biosfera"
  ]
  node [
    id 24
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 25
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 26
    label "geotermia"
  ]
  node [
    id 27
    label "atmosfera"
  ]
  node [
    id 28
    label "ekosystem"
  ]
  node [
    id 29
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 30
    label "p&#243;&#322;kula"
  ]
  node [
    id 31
    label "class"
  ]
  node [
    id 32
    label "huczek"
  ]
  node [
    id 33
    label "planeta"
  ]
  node [
    id 34
    label "makrokosmos"
  ]
  node [
    id 35
    label "przestrze&#324;"
  ]
  node [
    id 36
    label "przej&#281;cie"
  ]
  node [
    id 37
    label "przej&#261;&#263;"
  ]
  node [
    id 38
    label "universe"
  ]
  node [
    id 39
    label "przedmiot"
  ]
  node [
    id 40
    label "biegun"
  ]
  node [
    id 41
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 42
    label "wszechstworzenie"
  ]
  node [
    id 43
    label "populace"
  ]
  node [
    id 44
    label "po&#322;udnie"
  ]
  node [
    id 45
    label "magnetosfera"
  ]
  node [
    id 46
    label "przejmowa&#263;"
  ]
  node [
    id 47
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 48
    label "zagranica"
  ]
  node [
    id 49
    label "fauna"
  ]
  node [
    id 50
    label "p&#243;&#322;noc"
  ]
  node [
    id 51
    label "stw&#243;r"
  ]
  node [
    id 52
    label "ekosfera"
  ]
  node [
    id 53
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 54
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 55
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 56
    label "litosfera"
  ]
  node [
    id 57
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 58
    label "zjawisko"
  ]
  node [
    id 59
    label "ciemna_materia"
  ]
  node [
    id 60
    label "teren"
  ]
  node [
    id 61
    label "asymilowanie_si&#281;"
  ]
  node [
    id 62
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 63
    label "Nowy_&#346;wiat"
  ]
  node [
    id 64
    label "barysfera"
  ]
  node [
    id 65
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 66
    label "grupa"
  ]
  node [
    id 67
    label "Ziemia"
  ]
  node [
    id 68
    label "hydrosfera"
  ]
  node [
    id 69
    label "rzecz"
  ]
  node [
    id 70
    label "Stary_&#346;wiat"
  ]
  node [
    id 71
    label "przejmowanie"
  ]
  node [
    id 72
    label "geosfera"
  ]
  node [
    id 73
    label "mikrokosmos"
  ]
  node [
    id 74
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 75
    label "woda"
  ]
  node [
    id 76
    label "biota"
  ]
  node [
    id 77
    label "rze&#378;ba"
  ]
  node [
    id 78
    label "environment"
  ]
  node [
    id 79
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 80
    label "obiekt_naturalny"
  ]
  node [
    id 81
    label "czarna_dziura"
  ]
  node [
    id 82
    label "ozonosfera"
  ]
  node [
    id 83
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 84
    label "geoida"
  ]
  node [
    id 85
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 86
    label "asymilowa&#263;"
  ]
  node [
    id 87
    label "kompozycja"
  ]
  node [
    id 88
    label "pakiet_klimatyczny"
  ]
  node [
    id 89
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 90
    label "type"
  ]
  node [
    id 91
    label "cz&#261;steczka"
  ]
  node [
    id 92
    label "gromada"
  ]
  node [
    id 93
    label "specgrupa"
  ]
  node [
    id 94
    label "egzemplarz"
  ]
  node [
    id 95
    label "stage_set"
  ]
  node [
    id 96
    label "asymilowanie"
  ]
  node [
    id 97
    label "zbi&#243;r"
  ]
  node [
    id 98
    label "odm&#322;odzenie"
  ]
  node [
    id 99
    label "odm&#322;adza&#263;"
  ]
  node [
    id 100
    label "harcerze_starsi"
  ]
  node [
    id 101
    label "jednostka_systematyczna"
  ]
  node [
    id 102
    label "oddzia&#322;"
  ]
  node [
    id 103
    label "category"
  ]
  node [
    id 104
    label "liga"
  ]
  node [
    id 105
    label "&#346;wietliki"
  ]
  node [
    id 106
    label "formacja_geologiczna"
  ]
  node [
    id 107
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 108
    label "Eurogrupa"
  ]
  node [
    id 109
    label "Terranie"
  ]
  node [
    id 110
    label "odm&#322;adzanie"
  ]
  node [
    id 111
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 112
    label "Entuzjastki"
  ]
  node [
    id 113
    label "Kosowo"
  ]
  node [
    id 114
    label "zach&#243;d"
  ]
  node [
    id 115
    label "Zabu&#380;e"
  ]
  node [
    id 116
    label "wymiar"
  ]
  node [
    id 117
    label "antroposfera"
  ]
  node [
    id 118
    label "Arktyka"
  ]
  node [
    id 119
    label "Notogea"
  ]
  node [
    id 120
    label "Piotrowo"
  ]
  node [
    id 121
    label "akrecja"
  ]
  node [
    id 122
    label "zakres"
  ]
  node [
    id 123
    label "Ruda_Pabianicka"
  ]
  node [
    id 124
    label "Ludwin&#243;w"
  ]
  node [
    id 125
    label "miejsce"
  ]
  node [
    id 126
    label "wsch&#243;d"
  ]
  node [
    id 127
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 128
    label "Pow&#261;zki"
  ]
  node [
    id 129
    label "&#321;&#281;g"
  ]
  node [
    id 130
    label "Rakowice"
  ]
  node [
    id 131
    label "Syberia_Wschodnia"
  ]
  node [
    id 132
    label "Zab&#322;ocie"
  ]
  node [
    id 133
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 134
    label "Kresy_Zachodnie"
  ]
  node [
    id 135
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 136
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 137
    label "holarktyka"
  ]
  node [
    id 138
    label "terytorium"
  ]
  node [
    id 139
    label "Antarktyka"
  ]
  node [
    id 140
    label "pas_planetoid"
  ]
  node [
    id 141
    label "Syberia_Zachodnia"
  ]
  node [
    id 142
    label "Neogea"
  ]
  node [
    id 143
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 144
    label "Olszanica"
  ]
  node [
    id 145
    label "liczba"
  ]
  node [
    id 146
    label "uk&#322;ad"
  ]
  node [
    id 147
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 148
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 149
    label "integer"
  ]
  node [
    id 150
    label "zlewanie_si&#281;"
  ]
  node [
    id 151
    label "ilo&#347;&#263;"
  ]
  node [
    id 152
    label "pe&#322;ny"
  ]
  node [
    id 153
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 154
    label "charakter"
  ]
  node [
    id 155
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 156
    label "proces"
  ]
  node [
    id 157
    label "przywidzenie"
  ]
  node [
    id 158
    label "boski"
  ]
  node [
    id 159
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 160
    label "krajobraz"
  ]
  node [
    id 161
    label "presence"
  ]
  node [
    id 162
    label "punkt"
  ]
  node [
    id 163
    label "oktant"
  ]
  node [
    id 164
    label "przedzielenie"
  ]
  node [
    id 165
    label "przedzieli&#263;"
  ]
  node [
    id 166
    label "przestw&#243;r"
  ]
  node [
    id 167
    label "rozdziela&#263;"
  ]
  node [
    id 168
    label "nielito&#347;ciwy"
  ]
  node [
    id 169
    label "czasoprzestrze&#324;"
  ]
  node [
    id 170
    label "niezmierzony"
  ]
  node [
    id 171
    label "bezbrze&#380;e"
  ]
  node [
    id 172
    label "rozdzielanie"
  ]
  node [
    id 173
    label "rura"
  ]
  node [
    id 174
    label "&#347;rodowisko"
  ]
  node [
    id 175
    label "grzebiuszka"
  ]
  node [
    id 176
    label "cz&#322;owiek"
  ]
  node [
    id 177
    label "miniatura"
  ]
  node [
    id 178
    label "odbicie"
  ]
  node [
    id 179
    label "atom"
  ]
  node [
    id 180
    label "kosmos"
  ]
  node [
    id 181
    label "potw&#243;r"
  ]
  node [
    id 182
    label "istota_&#380;ywa"
  ]
  node [
    id 183
    label "monster"
  ]
  node [
    id 184
    label "istota_fantastyczna"
  ]
  node [
    id 185
    label "niecz&#322;owiek"
  ]
  node [
    id 186
    label "smok_wawelski"
  ]
  node [
    id 187
    label "kultura"
  ]
  node [
    id 188
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 189
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 190
    label "aspekt"
  ]
  node [
    id 191
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 192
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 193
    label "powietrznia"
  ]
  node [
    id 194
    label "egzosfera"
  ]
  node [
    id 195
    label "mezopauza"
  ]
  node [
    id 196
    label "mezosfera"
  ]
  node [
    id 197
    label "powietrze"
  ]
  node [
    id 198
    label "pow&#322;oka"
  ]
  node [
    id 199
    label "termosfera"
  ]
  node [
    id 200
    label "stratosfera"
  ]
  node [
    id 201
    label "kwas"
  ]
  node [
    id 202
    label "atmosphere"
  ]
  node [
    id 203
    label "homosfera"
  ]
  node [
    id 204
    label "cecha"
  ]
  node [
    id 205
    label "metasfera"
  ]
  node [
    id 206
    label "tropopauza"
  ]
  node [
    id 207
    label "heterosfera"
  ]
  node [
    id 208
    label "klimat"
  ]
  node [
    id 209
    label "atmosferyki"
  ]
  node [
    id 210
    label "jonosfera"
  ]
  node [
    id 211
    label "troposfera"
  ]
  node [
    id 212
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 213
    label "energia_termiczna"
  ]
  node [
    id 214
    label "ciep&#322;o"
  ]
  node [
    id 215
    label "sferoida"
  ]
  node [
    id 216
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 217
    label "istota"
  ]
  node [
    id 218
    label "wpada&#263;"
  ]
  node [
    id 219
    label "object"
  ]
  node [
    id 220
    label "wpa&#347;&#263;"
  ]
  node [
    id 221
    label "mienie"
  ]
  node [
    id 222
    label "obiekt"
  ]
  node [
    id 223
    label "temat"
  ]
  node [
    id 224
    label "wpadni&#281;cie"
  ]
  node [
    id 225
    label "wpadanie"
  ]
  node [
    id 226
    label "interception"
  ]
  node [
    id 227
    label "zaczerpni&#281;cie"
  ]
  node [
    id 228
    label "wzbudzenie"
  ]
  node [
    id 229
    label "wzi&#281;cie"
  ]
  node [
    id 230
    label "movement"
  ]
  node [
    id 231
    label "wra&#380;enie"
  ]
  node [
    id 232
    label "emotion"
  ]
  node [
    id 233
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 234
    label "thrill"
  ]
  node [
    id 235
    label "ogarn&#261;&#263;"
  ]
  node [
    id 236
    label "bang"
  ]
  node [
    id 237
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 238
    label "wzi&#261;&#263;"
  ]
  node [
    id 239
    label "wzbudzi&#263;"
  ]
  node [
    id 240
    label "stimulate"
  ]
  node [
    id 241
    label "ogarnia&#263;"
  ]
  node [
    id 242
    label "handle"
  ]
  node [
    id 243
    label "treat"
  ]
  node [
    id 244
    label "czerpa&#263;"
  ]
  node [
    id 245
    label "go"
  ]
  node [
    id 246
    label "bra&#263;"
  ]
  node [
    id 247
    label "wzbudza&#263;"
  ]
  node [
    id 248
    label "branie"
  ]
  node [
    id 249
    label "acquisition"
  ]
  node [
    id 250
    label "czerpanie"
  ]
  node [
    id 251
    label "ogarnianie"
  ]
  node [
    id 252
    label "wzbudzanie"
  ]
  node [
    id 253
    label "caparison"
  ]
  node [
    id 254
    label "czynno&#347;&#263;"
  ]
  node [
    id 255
    label "discipline"
  ]
  node [
    id 256
    label "zboczy&#263;"
  ]
  node [
    id 257
    label "w&#261;tek"
  ]
  node [
    id 258
    label "entity"
  ]
  node [
    id 259
    label "sponiewiera&#263;"
  ]
  node [
    id 260
    label "zboczenie"
  ]
  node [
    id 261
    label "zbaczanie"
  ]
  node [
    id 262
    label "thing"
  ]
  node [
    id 263
    label "om&#243;wi&#263;"
  ]
  node [
    id 264
    label "tre&#347;&#263;"
  ]
  node [
    id 265
    label "element"
  ]
  node [
    id 266
    label "kr&#261;&#380;enie"
  ]
  node [
    id 267
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 268
    label "zbacza&#263;"
  ]
  node [
    id 269
    label "om&#243;wienie"
  ]
  node [
    id 270
    label "tematyka"
  ]
  node [
    id 271
    label "omawianie"
  ]
  node [
    id 272
    label "omawia&#263;"
  ]
  node [
    id 273
    label "robienie"
  ]
  node [
    id 274
    label "program_nauczania"
  ]
  node [
    id 275
    label "sponiewieranie"
  ]
  node [
    id 276
    label "performance"
  ]
  node [
    id 277
    label "sztuka"
  ]
  node [
    id 278
    label "granica_pa&#324;stwa"
  ]
  node [
    id 279
    label "strona_&#347;wiata"
  ]
  node [
    id 280
    label "p&#243;&#322;nocek"
  ]
  node [
    id 281
    label "noc"
  ]
  node [
    id 282
    label "Boreasz"
  ]
  node [
    id 283
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 284
    label "godzina"
  ]
  node [
    id 285
    label "&#347;rodek"
  ]
  node [
    id 286
    label "dwunasta"
  ]
  node [
    id 287
    label "dzie&#324;"
  ]
  node [
    id 288
    label "pora"
  ]
  node [
    id 289
    label "zawiasy"
  ]
  node [
    id 290
    label "brzeg"
  ]
  node [
    id 291
    label "p&#322;oza"
  ]
  node [
    id 292
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 293
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 294
    label "element_anatomiczny"
  ]
  node [
    id 295
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 296
    label "organ"
  ]
  node [
    id 297
    label "marina"
  ]
  node [
    id 298
    label "reda"
  ]
  node [
    id 299
    label "pe&#322;ne_morze"
  ]
  node [
    id 300
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 301
    label "Morze_Bia&#322;e"
  ]
  node [
    id 302
    label "przymorze"
  ]
  node [
    id 303
    label "Morze_Adriatyckie"
  ]
  node [
    id 304
    label "paliszcze"
  ]
  node [
    id 305
    label "talasoterapia"
  ]
  node [
    id 306
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 307
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 308
    label "bezmiar"
  ]
  node [
    id 309
    label "Morze_Egejskie"
  ]
  node [
    id 310
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 311
    label "latarnia_morska"
  ]
  node [
    id 312
    label "Neptun"
  ]
  node [
    id 313
    label "Morze_Czarne"
  ]
  node [
    id 314
    label "nereida"
  ]
  node [
    id 315
    label "laguna"
  ]
  node [
    id 316
    label "okeanida"
  ]
  node [
    id 317
    label "Morze_Czerwone"
  ]
  node [
    id 318
    label "zbiornik_wodny"
  ]
  node [
    id 319
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 320
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 321
    label "rze&#378;biarstwo"
  ]
  node [
    id 322
    label "planacja"
  ]
  node [
    id 323
    label "plastyka"
  ]
  node [
    id 324
    label "relief"
  ]
  node [
    id 325
    label "bozzetto"
  ]
  node [
    id 326
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 327
    label "sfera"
  ]
  node [
    id 328
    label "warstwa_perydotytowa"
  ]
  node [
    id 329
    label "gleba"
  ]
  node [
    id 330
    label "skorupa_ziemska"
  ]
  node [
    id 331
    label "warstwa"
  ]
  node [
    id 332
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 333
    label "warstwa_granitowa"
  ]
  node [
    id 334
    label "sialma"
  ]
  node [
    id 335
    label "kriosfera"
  ]
  node [
    id 336
    label "j&#261;dro"
  ]
  node [
    id 337
    label "lej_polarny"
  ]
  node [
    id 338
    label "kresom&#243;zgowie"
  ]
  node [
    id 339
    label "kula"
  ]
  node [
    id 340
    label "ozon"
  ]
  node [
    id 341
    label "przyra"
  ]
  node [
    id 342
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 343
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 344
    label "miejsce_pracy"
  ]
  node [
    id 345
    label "nation"
  ]
  node [
    id 346
    label "w&#322;adza"
  ]
  node [
    id 347
    label "kontekst"
  ]
  node [
    id 348
    label "cyprysowate"
  ]
  node [
    id 349
    label "iglak"
  ]
  node [
    id 350
    label "plant"
  ]
  node [
    id 351
    label "ro&#347;lina"
  ]
  node [
    id 352
    label "formacja_ro&#347;linna"
  ]
  node [
    id 353
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 354
    label "biom"
  ]
  node [
    id 355
    label "geosystem"
  ]
  node [
    id 356
    label "szata_ro&#347;linna"
  ]
  node [
    id 357
    label "zielono&#347;&#263;"
  ]
  node [
    id 358
    label "pi&#281;tro"
  ]
  node [
    id 359
    label "przybieranie"
  ]
  node [
    id 360
    label "pustka"
  ]
  node [
    id 361
    label "przybrze&#380;e"
  ]
  node [
    id 362
    label "woda_s&#322;odka"
  ]
  node [
    id 363
    label "utylizator"
  ]
  node [
    id 364
    label "spi&#281;trzenie"
  ]
  node [
    id 365
    label "wodnik"
  ]
  node [
    id 366
    label "water"
  ]
  node [
    id 367
    label "fala"
  ]
  node [
    id 368
    label "kryptodepresja"
  ]
  node [
    id 369
    label "klarownik"
  ]
  node [
    id 370
    label "tlenek"
  ]
  node [
    id 371
    label "l&#243;d"
  ]
  node [
    id 372
    label "nabranie"
  ]
  node [
    id 373
    label "chlastanie"
  ]
  node [
    id 374
    label "zrzut"
  ]
  node [
    id 375
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 376
    label "uci&#261;g"
  ]
  node [
    id 377
    label "nabra&#263;"
  ]
  node [
    id 378
    label "wybrze&#380;e"
  ]
  node [
    id 379
    label "p&#322;ycizna"
  ]
  node [
    id 380
    label "uj&#281;cie_wody"
  ]
  node [
    id 381
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 382
    label "ciecz"
  ]
  node [
    id 383
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 384
    label "Waruna"
  ]
  node [
    id 385
    label "bicie"
  ]
  node [
    id 386
    label "chlasta&#263;"
  ]
  node [
    id 387
    label "deklamacja"
  ]
  node [
    id 388
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 389
    label "spi&#281;trzanie"
  ]
  node [
    id 390
    label "wypowied&#378;"
  ]
  node [
    id 391
    label "spi&#281;trza&#263;"
  ]
  node [
    id 392
    label "wysi&#281;k"
  ]
  node [
    id 393
    label "dotleni&#263;"
  ]
  node [
    id 394
    label "pojazd"
  ]
  node [
    id 395
    label "nap&#243;j"
  ]
  node [
    id 396
    label "bombast"
  ]
  node [
    id 397
    label "biotop"
  ]
  node [
    id 398
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 399
    label "biocenoza"
  ]
  node [
    id 400
    label "awifauna"
  ]
  node [
    id 401
    label "ichtiofauna"
  ]
  node [
    id 402
    label "zlewozmywak"
  ]
  node [
    id 403
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 404
    label "zaplecze"
  ]
  node [
    id 405
    label "gotowa&#263;"
  ]
  node [
    id 406
    label "jedzenie"
  ]
  node [
    id 407
    label "tajniki"
  ]
  node [
    id 408
    label "zaj&#281;cie"
  ]
  node [
    id 409
    label "instytucja"
  ]
  node [
    id 410
    label "pomieszczenie"
  ]
  node [
    id 411
    label "Jowisz"
  ]
  node [
    id 412
    label "syzygia"
  ]
  node [
    id 413
    label "Uran"
  ]
  node [
    id 414
    label "Saturn"
  ]
  node [
    id 415
    label "strefa"
  ]
  node [
    id 416
    label "message"
  ]
  node [
    id 417
    label "dar"
  ]
  node [
    id 418
    label "real"
  ]
  node [
    id 419
    label "Ukraina"
  ]
  node [
    id 420
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 421
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 422
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 423
    label "blok_wschodni"
  ]
  node [
    id 424
    label "Europa_Wschodnia"
  ]
  node [
    id 425
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 426
    label "&#347;wieca"
  ]
  node [
    id 427
    label "aut"
  ]
  node [
    id 428
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 429
    label "sport"
  ]
  node [
    id 430
    label "zagrywka"
  ]
  node [
    id 431
    label "gra"
  ]
  node [
    id 432
    label "do&#347;rodkowywanie"
  ]
  node [
    id 433
    label "sport_zespo&#322;owy"
  ]
  node [
    id 434
    label "musket_ball"
  ]
  node [
    id 435
    label "zaserwowa&#263;"
  ]
  node [
    id 436
    label "serwowa&#263;"
  ]
  node [
    id 437
    label "rzucanka"
  ]
  node [
    id 438
    label "serwowanie"
  ]
  node [
    id 439
    label "orb"
  ]
  node [
    id 440
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 441
    label "zaserwowanie"
  ]
  node [
    id 442
    label "warstwa_kulista"
  ]
  node [
    id 443
    label "o&#322;&#243;w"
  ]
  node [
    id 444
    label "ball"
  ]
  node [
    id 445
    label "amunicja"
  ]
  node [
    id 446
    label "czasza"
  ]
  node [
    id 447
    label "bry&#322;a"
  ]
  node [
    id 448
    label "wycinek_kuli"
  ]
  node [
    id 449
    label "kartuza"
  ]
  node [
    id 450
    label "podpora"
  ]
  node [
    id 451
    label "komora_nabojowa"
  ]
  node [
    id 452
    label "ta&#347;ma"
  ]
  node [
    id 453
    label "pocisk"
  ]
  node [
    id 454
    label "nab&#243;j"
  ]
  node [
    id 455
    label "akwarium"
  ]
  node [
    id 456
    label "&#322;uska"
  ]
  node [
    id 457
    label "figura_heraldyczna"
  ]
  node [
    id 458
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 459
    label "pile"
  ]
  node [
    id 460
    label "p&#243;&#322;sfera"
  ]
  node [
    id 461
    label "uderzenie"
  ]
  node [
    id 462
    label "move"
  ]
  node [
    id 463
    label "myk"
  ]
  node [
    id 464
    label "mecz"
  ]
  node [
    id 465
    label "manewr"
  ]
  node [
    id 466
    label "rozgrywka"
  ]
  node [
    id 467
    label "gambit"
  ]
  node [
    id 468
    label "travel"
  ]
  node [
    id 469
    label "posuni&#281;cie"
  ]
  node [
    id 470
    label "gra_w_karty"
  ]
  node [
    id 471
    label "game"
  ]
  node [
    id 472
    label "zbijany"
  ]
  node [
    id 473
    label "zasada"
  ]
  node [
    id 474
    label "rekwizyt_do_gry"
  ]
  node [
    id 475
    label "odg&#322;os"
  ]
  node [
    id 476
    label "Pok&#233;mon"
  ]
  node [
    id 477
    label "wydarzenie"
  ]
  node [
    id 478
    label "komplet"
  ]
  node [
    id 479
    label "zabawa"
  ]
  node [
    id 480
    label "apparent_motion"
  ]
  node [
    id 481
    label "akcja"
  ]
  node [
    id 482
    label "synteza"
  ]
  node [
    id 483
    label "play"
  ]
  node [
    id 484
    label "odtworzenie"
  ]
  node [
    id 485
    label "zmienno&#347;&#263;"
  ]
  node [
    id 486
    label "post&#281;powanie"
  ]
  node [
    id 487
    label "atakowanie"
  ]
  node [
    id 488
    label "zaatakowanie"
  ]
  node [
    id 489
    label "kultura_fizyczna"
  ]
  node [
    id 490
    label "zgrupowanie"
  ]
  node [
    id 491
    label "atakowa&#263;"
  ]
  node [
    id 492
    label "usportowienie"
  ]
  node [
    id 493
    label "sokolstwo"
  ]
  node [
    id 494
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 495
    label "zaatakowa&#263;"
  ]
  node [
    id 496
    label "usportowi&#263;"
  ]
  node [
    id 497
    label "zaczynanie"
  ]
  node [
    id 498
    label "faszerowanie"
  ]
  node [
    id 499
    label "podawanie"
  ]
  node [
    id 500
    label "bufet"
  ]
  node [
    id 501
    label "administration"
  ]
  node [
    id 502
    label "stawianie"
  ]
  node [
    id 503
    label "podawa&#263;"
  ]
  node [
    id 504
    label "kierowa&#263;"
  ]
  node [
    id 505
    label "center"
  ]
  node [
    id 506
    label "nafaszerowa&#263;"
  ]
  node [
    id 507
    label "poda&#263;"
  ]
  node [
    id 508
    label "ustawi&#263;"
  ]
  node [
    id 509
    label "give"
  ]
  node [
    id 510
    label "centering"
  ]
  node [
    id 511
    label "zwracanie"
  ]
  node [
    id 512
    label "wyswobodzenie"
  ]
  node [
    id 513
    label "prototype"
  ]
  node [
    id 514
    label "spowodowanie"
  ]
  node [
    id 515
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 516
    label "odbicie_si&#281;"
  ]
  node [
    id 517
    label "odskoczenie"
  ]
  node [
    id 518
    label "wynagrodzenie"
  ]
  node [
    id 519
    label "blask"
  ]
  node [
    id 520
    label "uszkodzenie"
  ]
  node [
    id 521
    label "reflection"
  ]
  node [
    id 522
    label "zata&#324;czenie"
  ]
  node [
    id 523
    label "odegranie_si&#281;"
  ]
  node [
    id 524
    label "zwierciad&#322;o"
  ]
  node [
    id 525
    label "picture"
  ]
  node [
    id 526
    label "impression"
  ]
  node [
    id 527
    label "ut&#322;uczenie"
  ]
  node [
    id 528
    label "wymienienie"
  ]
  node [
    id 529
    label "zdarzenie_si&#281;"
  ]
  node [
    id 530
    label "obraz"
  ]
  node [
    id 531
    label "lustro"
  ]
  node [
    id 532
    label "oddalenie_si&#281;"
  ]
  node [
    id 533
    label "zabranie"
  ]
  node [
    id 534
    label "przelobowanie"
  ]
  node [
    id 535
    label "zachowanie"
  ]
  node [
    id 536
    label "stracenie_g&#322;owy"
  ]
  node [
    id 537
    label "stamp"
  ]
  node [
    id 538
    label "zostawienie"
  ]
  node [
    id 539
    label "recoil"
  ]
  node [
    id 540
    label "wybicie"
  ]
  node [
    id 541
    label "zrobienie"
  ]
  node [
    id 542
    label "skopiowanie"
  ]
  node [
    id 543
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 544
    label "reproduction"
  ]
  node [
    id 545
    label "podanie"
  ]
  node [
    id 546
    label "ustawienie"
  ]
  node [
    id 547
    label "service"
  ]
  node [
    id 548
    label "nafaszerowanie"
  ]
  node [
    id 549
    label "lot"
  ]
  node [
    id 550
    label "akrobacja_lotnicza"
  ]
  node [
    id 551
    label "gasid&#322;o"
  ]
  node [
    id 552
    label "s&#322;up"
  ]
  node [
    id 553
    label "sygnalizator"
  ]
  node [
    id 554
    label "silnik_spalinowy"
  ]
  node [
    id 555
    label "profitka"
  ]
  node [
    id 556
    label "&#263;wiczenie"
  ]
  node [
    id 557
    label "knot"
  ]
  node [
    id 558
    label "jednostka_nat&#281;&#380;enia_&#347;wiat&#322;a"
  ]
  node [
    id 559
    label "&#347;wiat&#322;o"
  ]
  node [
    id 560
    label "kelner"
  ]
  node [
    id 561
    label "zaczyna&#263;"
  ]
  node [
    id 562
    label "faszerowa&#263;"
  ]
  node [
    id 563
    label "stawia&#263;"
  ]
  node [
    id 564
    label "deal"
  ]
  node [
    id 565
    label "przedostanie_si&#281;"
  ]
  node [
    id 566
    label "out"
  ]
  node [
    id 567
    label "boisko"
  ]
  node [
    id 568
    label "wrzut"
  ]
  node [
    id 569
    label "r&#281;cznie"
  ]
  node [
    id 570
    label "manually"
  ]
  node [
    id 571
    label "pa&#324;stwo"
  ]
  node [
    id 572
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 573
    label "andropauza"
  ]
  node [
    id 574
    label "twardziel"
  ]
  node [
    id 575
    label "jegomo&#347;&#263;"
  ]
  node [
    id 576
    label "doros&#322;y"
  ]
  node [
    id 577
    label "ojciec"
  ]
  node [
    id 578
    label "samiec"
  ]
  node [
    id 579
    label "androlog"
  ]
  node [
    id 580
    label "bratek"
  ]
  node [
    id 581
    label "m&#261;&#380;"
  ]
  node [
    id 582
    label "ch&#322;opina"
  ]
  node [
    id 583
    label "Japonia"
  ]
  node [
    id 584
    label "Zair"
  ]
  node [
    id 585
    label "Belize"
  ]
  node [
    id 586
    label "San_Marino"
  ]
  node [
    id 587
    label "Tanzania"
  ]
  node [
    id 588
    label "Antigua_i_Barbuda"
  ]
  node [
    id 589
    label "Senegal"
  ]
  node [
    id 590
    label "Seszele"
  ]
  node [
    id 591
    label "Mauretania"
  ]
  node [
    id 592
    label "Indie"
  ]
  node [
    id 593
    label "Filipiny"
  ]
  node [
    id 594
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 595
    label "Zimbabwe"
  ]
  node [
    id 596
    label "Malezja"
  ]
  node [
    id 597
    label "Rumunia"
  ]
  node [
    id 598
    label "Surinam"
  ]
  node [
    id 599
    label "Syria"
  ]
  node [
    id 600
    label "Wyspy_Marshalla"
  ]
  node [
    id 601
    label "Burkina_Faso"
  ]
  node [
    id 602
    label "Grecja"
  ]
  node [
    id 603
    label "Polska"
  ]
  node [
    id 604
    label "Wenezuela"
  ]
  node [
    id 605
    label "Suazi"
  ]
  node [
    id 606
    label "Nepal"
  ]
  node [
    id 607
    label "S&#322;owacja"
  ]
  node [
    id 608
    label "Algieria"
  ]
  node [
    id 609
    label "Chiny"
  ]
  node [
    id 610
    label "Grenada"
  ]
  node [
    id 611
    label "Barbados"
  ]
  node [
    id 612
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 613
    label "Pakistan"
  ]
  node [
    id 614
    label "Niemcy"
  ]
  node [
    id 615
    label "Bahrajn"
  ]
  node [
    id 616
    label "Komory"
  ]
  node [
    id 617
    label "Australia"
  ]
  node [
    id 618
    label "Rodezja"
  ]
  node [
    id 619
    label "Malawi"
  ]
  node [
    id 620
    label "Gwinea"
  ]
  node [
    id 621
    label "Wehrlen"
  ]
  node [
    id 622
    label "Meksyk"
  ]
  node [
    id 623
    label "Liechtenstein"
  ]
  node [
    id 624
    label "Czarnog&#243;ra"
  ]
  node [
    id 625
    label "Wielka_Brytania"
  ]
  node [
    id 626
    label "Kuwejt"
  ]
  node [
    id 627
    label "Monako"
  ]
  node [
    id 628
    label "Angola"
  ]
  node [
    id 629
    label "Jemen"
  ]
  node [
    id 630
    label "Etiopia"
  ]
  node [
    id 631
    label "Madagaskar"
  ]
  node [
    id 632
    label "Kolumbia"
  ]
  node [
    id 633
    label "Portoryko"
  ]
  node [
    id 634
    label "Mauritius"
  ]
  node [
    id 635
    label "Kostaryka"
  ]
  node [
    id 636
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 637
    label "Tajlandia"
  ]
  node [
    id 638
    label "Argentyna"
  ]
  node [
    id 639
    label "Zambia"
  ]
  node [
    id 640
    label "Sri_Lanka"
  ]
  node [
    id 641
    label "Gwatemala"
  ]
  node [
    id 642
    label "Kirgistan"
  ]
  node [
    id 643
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 644
    label "Hiszpania"
  ]
  node [
    id 645
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 646
    label "Salwador"
  ]
  node [
    id 647
    label "Korea"
  ]
  node [
    id 648
    label "Macedonia"
  ]
  node [
    id 649
    label "Brunei"
  ]
  node [
    id 650
    label "Mozambik"
  ]
  node [
    id 651
    label "Turcja"
  ]
  node [
    id 652
    label "Kambod&#380;a"
  ]
  node [
    id 653
    label "Benin"
  ]
  node [
    id 654
    label "Bhutan"
  ]
  node [
    id 655
    label "Tunezja"
  ]
  node [
    id 656
    label "Austria"
  ]
  node [
    id 657
    label "Izrael"
  ]
  node [
    id 658
    label "Sierra_Leone"
  ]
  node [
    id 659
    label "Jamajka"
  ]
  node [
    id 660
    label "Rosja"
  ]
  node [
    id 661
    label "Rwanda"
  ]
  node [
    id 662
    label "holoarktyka"
  ]
  node [
    id 663
    label "Nigeria"
  ]
  node [
    id 664
    label "USA"
  ]
  node [
    id 665
    label "Oman"
  ]
  node [
    id 666
    label "Luksemburg"
  ]
  node [
    id 667
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 668
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 669
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 670
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 671
    label "Dominikana"
  ]
  node [
    id 672
    label "Irlandia"
  ]
  node [
    id 673
    label "Liban"
  ]
  node [
    id 674
    label "Hanower"
  ]
  node [
    id 675
    label "Estonia"
  ]
  node [
    id 676
    label "Iran"
  ]
  node [
    id 677
    label "Nowa_Zelandia"
  ]
  node [
    id 678
    label "Gabon"
  ]
  node [
    id 679
    label "Samoa"
  ]
  node [
    id 680
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 681
    label "S&#322;owenia"
  ]
  node [
    id 682
    label "Kiribati"
  ]
  node [
    id 683
    label "Egipt"
  ]
  node [
    id 684
    label "Togo"
  ]
  node [
    id 685
    label "Mongolia"
  ]
  node [
    id 686
    label "Sudan"
  ]
  node [
    id 687
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 688
    label "Bahamy"
  ]
  node [
    id 689
    label "Bangladesz"
  ]
  node [
    id 690
    label "partia"
  ]
  node [
    id 691
    label "Serbia"
  ]
  node [
    id 692
    label "Czechy"
  ]
  node [
    id 693
    label "Holandia"
  ]
  node [
    id 694
    label "Birma"
  ]
  node [
    id 695
    label "Albania"
  ]
  node [
    id 696
    label "Mikronezja"
  ]
  node [
    id 697
    label "Gambia"
  ]
  node [
    id 698
    label "Kazachstan"
  ]
  node [
    id 699
    label "interior"
  ]
  node [
    id 700
    label "Uzbekistan"
  ]
  node [
    id 701
    label "Malta"
  ]
  node [
    id 702
    label "Lesoto"
  ]
  node [
    id 703
    label "para"
  ]
  node [
    id 704
    label "Antarktis"
  ]
  node [
    id 705
    label "Andora"
  ]
  node [
    id 706
    label "Nauru"
  ]
  node [
    id 707
    label "Kuba"
  ]
  node [
    id 708
    label "Wietnam"
  ]
  node [
    id 709
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 710
    label "ziemia"
  ]
  node [
    id 711
    label "Kamerun"
  ]
  node [
    id 712
    label "Chorwacja"
  ]
  node [
    id 713
    label "Urugwaj"
  ]
  node [
    id 714
    label "Niger"
  ]
  node [
    id 715
    label "Turkmenistan"
  ]
  node [
    id 716
    label "Szwajcaria"
  ]
  node [
    id 717
    label "zwrot"
  ]
  node [
    id 718
    label "organizacja"
  ]
  node [
    id 719
    label "Palau"
  ]
  node [
    id 720
    label "Litwa"
  ]
  node [
    id 721
    label "Gruzja"
  ]
  node [
    id 722
    label "Tajwan"
  ]
  node [
    id 723
    label "Kongo"
  ]
  node [
    id 724
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 725
    label "Honduras"
  ]
  node [
    id 726
    label "Boliwia"
  ]
  node [
    id 727
    label "Uganda"
  ]
  node [
    id 728
    label "Namibia"
  ]
  node [
    id 729
    label "Azerbejd&#380;an"
  ]
  node [
    id 730
    label "Erytrea"
  ]
  node [
    id 731
    label "Gujana"
  ]
  node [
    id 732
    label "Panama"
  ]
  node [
    id 733
    label "Somalia"
  ]
  node [
    id 734
    label "Burundi"
  ]
  node [
    id 735
    label "Tuwalu"
  ]
  node [
    id 736
    label "Libia"
  ]
  node [
    id 737
    label "Katar"
  ]
  node [
    id 738
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 739
    label "Sahara_Zachodnia"
  ]
  node [
    id 740
    label "Trynidad_i_Tobago"
  ]
  node [
    id 741
    label "Gwinea_Bissau"
  ]
  node [
    id 742
    label "Bu&#322;garia"
  ]
  node [
    id 743
    label "Fid&#380;i"
  ]
  node [
    id 744
    label "Nikaragua"
  ]
  node [
    id 745
    label "Tonga"
  ]
  node [
    id 746
    label "Timor_Wschodni"
  ]
  node [
    id 747
    label "Laos"
  ]
  node [
    id 748
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 749
    label "Ghana"
  ]
  node [
    id 750
    label "Brazylia"
  ]
  node [
    id 751
    label "Belgia"
  ]
  node [
    id 752
    label "Irak"
  ]
  node [
    id 753
    label "Peru"
  ]
  node [
    id 754
    label "Arabia_Saudyjska"
  ]
  node [
    id 755
    label "Indonezja"
  ]
  node [
    id 756
    label "Malediwy"
  ]
  node [
    id 757
    label "Afganistan"
  ]
  node [
    id 758
    label "Jordania"
  ]
  node [
    id 759
    label "Kenia"
  ]
  node [
    id 760
    label "Czad"
  ]
  node [
    id 761
    label "Liberia"
  ]
  node [
    id 762
    label "W&#281;gry"
  ]
  node [
    id 763
    label "Chile"
  ]
  node [
    id 764
    label "Mali"
  ]
  node [
    id 765
    label "Armenia"
  ]
  node [
    id 766
    label "Kanada"
  ]
  node [
    id 767
    label "Cypr"
  ]
  node [
    id 768
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 769
    label "Ekwador"
  ]
  node [
    id 770
    label "Mo&#322;dawia"
  ]
  node [
    id 771
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 772
    label "W&#322;ochy"
  ]
  node [
    id 773
    label "Wyspy_Salomona"
  ]
  node [
    id 774
    label "&#321;otwa"
  ]
  node [
    id 775
    label "D&#380;ibuti"
  ]
  node [
    id 776
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 777
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 778
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 779
    label "Portugalia"
  ]
  node [
    id 780
    label "Botswana"
  ]
  node [
    id 781
    label "Maroko"
  ]
  node [
    id 782
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 783
    label "Francja"
  ]
  node [
    id 784
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 785
    label "Dominika"
  ]
  node [
    id 786
    label "Paragwaj"
  ]
  node [
    id 787
    label "Tad&#380;ykistan"
  ]
  node [
    id 788
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 789
    label "Haiti"
  ]
  node [
    id 790
    label "Khitai"
  ]
  node [
    id 791
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 792
    label "nasada"
  ]
  node [
    id 793
    label "profanum"
  ]
  node [
    id 794
    label "wz&#243;r"
  ]
  node [
    id 795
    label "senior"
  ]
  node [
    id 796
    label "os&#322;abia&#263;"
  ]
  node [
    id 797
    label "homo_sapiens"
  ]
  node [
    id 798
    label "osoba"
  ]
  node [
    id 799
    label "ludzko&#347;&#263;"
  ]
  node [
    id 800
    label "Adam"
  ]
  node [
    id 801
    label "hominid"
  ]
  node [
    id 802
    label "posta&#263;"
  ]
  node [
    id 803
    label "portrecista"
  ]
  node [
    id 804
    label "polifag"
  ]
  node [
    id 805
    label "podw&#322;adny"
  ]
  node [
    id 806
    label "dwun&#243;g"
  ]
  node [
    id 807
    label "wapniak"
  ]
  node [
    id 808
    label "duch"
  ]
  node [
    id 809
    label "os&#322;abianie"
  ]
  node [
    id 810
    label "antropochoria"
  ]
  node [
    id 811
    label "figura"
  ]
  node [
    id 812
    label "g&#322;owa"
  ]
  node [
    id 813
    label "oddzia&#322;ywanie"
  ]
  node [
    id 814
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 815
    label "du&#380;y"
  ]
  node [
    id 816
    label "dojrza&#322;y"
  ]
  node [
    id 817
    label "dojrzale"
  ]
  node [
    id 818
    label "wydoro&#347;lenie"
  ]
  node [
    id 819
    label "doro&#347;lenie"
  ]
  node [
    id 820
    label "m&#261;dry"
  ]
  node [
    id 821
    label "&#378;ra&#322;y"
  ]
  node [
    id 822
    label "doletni"
  ]
  node [
    id 823
    label "doro&#347;le"
  ]
  node [
    id 824
    label "heartwood"
  ]
  node [
    id 825
    label "pami&#281;&#263;"
  ]
  node [
    id 826
    label "komputer"
  ]
  node [
    id 827
    label "&#347;mia&#322;ek"
  ]
  node [
    id 828
    label "monolit"
  ]
  node [
    id 829
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 830
    label "zapalenie"
  ]
  node [
    id 831
    label "hard_disc"
  ]
  node [
    id 832
    label "klaster_dyskowy"
  ]
  node [
    id 833
    label "drewno_wt&#243;rne"
  ]
  node [
    id 834
    label "formacja_skalna"
  ]
  node [
    id 835
    label "powierzchnia_zr&#243;wnania"
  ]
  node [
    id 836
    label "mo&#347;&#263;"
  ]
  node [
    id 837
    label "&#347;w"
  ]
  node [
    id 838
    label "rodzic"
  ]
  node [
    id 839
    label "pomys&#322;odawca"
  ]
  node [
    id 840
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 841
    label "rodzice"
  ]
  node [
    id 842
    label "zakonnik"
  ]
  node [
    id 843
    label "wykonawca"
  ]
  node [
    id 844
    label "stary"
  ]
  node [
    id 845
    label "kszta&#322;ciciel"
  ]
  node [
    id 846
    label "kuwada"
  ]
  node [
    id 847
    label "ojczym"
  ]
  node [
    id 848
    label "papa"
  ]
  node [
    id 849
    label "przodek"
  ]
  node [
    id 850
    label "tworzyciel"
  ]
  node [
    id 851
    label "zwierz&#281;"
  ]
  node [
    id 852
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 853
    label "facet"
  ]
  node [
    id 854
    label "kochanek"
  ]
  node [
    id 855
    label "fio&#322;ek"
  ]
  node [
    id 856
    label "brat"
  ]
  node [
    id 857
    label "pan_i_w&#322;adca"
  ]
  node [
    id 858
    label "pan_m&#322;ody"
  ]
  node [
    id 859
    label "ch&#322;op"
  ]
  node [
    id 860
    label "&#347;lubny"
  ]
  node [
    id 861
    label "m&#243;j"
  ]
  node [
    id 862
    label "pan_domu"
  ]
  node [
    id 863
    label "ma&#322;&#380;onek"
  ]
  node [
    id 864
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 865
    label "specjalista"
  ]
  node [
    id 866
    label "&#380;ycie"
  ]
  node [
    id 867
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 868
    label "mistrzostwo"
  ]
  node [
    id 869
    label "wyspa"
  ]
  node [
    id 870
    label "1978"
  ]
  node [
    id 871
    label "niemiecki"
  ]
  node [
    id 872
    label "republika"
  ]
  node [
    id 873
    label "demokratyczny"
  ]
  node [
    id 874
    label "federalny"
  ]
  node [
    id 875
    label "niemiec"
  ]
  node [
    id 876
    label "zwi&#261;zka"
  ]
  node [
    id 877
    label "socjalistyczny"
  ]
  node [
    id 878
    label "radziecki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 868
  ]
  edge [
    source 1
    target 869
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 870
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 868
  ]
  edge [
    source 2
    target 869
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 870
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 868
  ]
  edge [
    source 3
    target 869
  ]
  edge [
    source 3
    target 870
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 782
  ]
  edge [
    source 4
    target 783
  ]
  edge [
    source 4
    target 784
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 786
  ]
  edge [
    source 4
    target 787
  ]
  edge [
    source 4
    target 788
  ]
  edge [
    source 4
    target 789
  ]
  edge [
    source 4
    target 790
  ]
  edge [
    source 4
    target 791
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 792
  ]
  edge [
    source 4
    target 793
  ]
  edge [
    source 4
    target 794
  ]
  edge [
    source 4
    target 795
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 796
  ]
  edge [
    source 4
    target 797
  ]
  edge [
    source 4
    target 798
  ]
  edge [
    source 4
    target 799
  ]
  edge [
    source 4
    target 800
  ]
  edge [
    source 4
    target 801
  ]
  edge [
    source 4
    target 802
  ]
  edge [
    source 4
    target 803
  ]
  edge [
    source 4
    target 804
  ]
  edge [
    source 4
    target 805
  ]
  edge [
    source 4
    target 806
  ]
  edge [
    source 4
    target 807
  ]
  edge [
    source 4
    target 808
  ]
  edge [
    source 4
    target 809
  ]
  edge [
    source 4
    target 810
  ]
  edge [
    source 4
    target 811
  ]
  edge [
    source 4
    target 812
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 813
  ]
  edge [
    source 4
    target 814
  ]
  edge [
    source 4
    target 815
  ]
  edge [
    source 4
    target 816
  ]
  edge [
    source 4
    target 817
  ]
  edge [
    source 4
    target 818
  ]
  edge [
    source 4
    target 819
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 4
    target 824
  ]
  edge [
    source 4
    target 825
  ]
  edge [
    source 4
    target 826
  ]
  edge [
    source 4
    target 827
  ]
  edge [
    source 4
    target 828
  ]
  edge [
    source 4
    target 829
  ]
  edge [
    source 4
    target 830
  ]
  edge [
    source 4
    target 831
  ]
  edge [
    source 4
    target 832
  ]
  edge [
    source 4
    target 833
  ]
  edge [
    source 4
    target 834
  ]
  edge [
    source 4
    target 835
  ]
  edge [
    source 4
    target 836
  ]
  edge [
    source 4
    target 837
  ]
  edge [
    source 4
    target 838
  ]
  edge [
    source 4
    target 839
  ]
  edge [
    source 4
    target 840
  ]
  edge [
    source 4
    target 841
  ]
  edge [
    source 4
    target 842
  ]
  edge [
    source 4
    target 843
  ]
  edge [
    source 4
    target 844
  ]
  edge [
    source 4
    target 845
  ]
  edge [
    source 4
    target 846
  ]
  edge [
    source 4
    target 847
  ]
  edge [
    source 4
    target 848
  ]
  edge [
    source 4
    target 849
  ]
  edge [
    source 4
    target 850
  ]
  edge [
    source 4
    target 851
  ]
  edge [
    source 4
    target 852
  ]
  edge [
    source 4
    target 853
  ]
  edge [
    source 4
    target 854
  ]
  edge [
    source 4
    target 855
  ]
  edge [
    source 4
    target 856
  ]
  edge [
    source 4
    target 857
  ]
  edge [
    source 4
    target 858
  ]
  edge [
    source 4
    target 859
  ]
  edge [
    source 4
    target 860
  ]
  edge [
    source 4
    target 861
  ]
  edge [
    source 4
    target 862
  ]
  edge [
    source 4
    target 863
  ]
  edge [
    source 4
    target 864
  ]
  edge [
    source 4
    target 865
  ]
  edge [
    source 4
    target 866
  ]
  edge [
    source 4
    target 867
  ]
  edge [
    source 4
    target 868
  ]
  edge [
    source 4
    target 869
  ]
  edge [
    source 4
    target 870
  ]
  edge [
    source 868
    target 869
  ]
  edge [
    source 868
    target 870
  ]
  edge [
    source 869
    target 870
  ]
  edge [
    source 871
    target 872
  ]
  edge [
    source 871
    target 873
  ]
  edge [
    source 872
    target 873
  ]
  edge [
    source 872
    target 874
  ]
  edge [
    source 872
    target 875
  ]
  edge [
    source 872
    target 876
  ]
  edge [
    source 872
    target 877
  ]
  edge [
    source 872
    target 878
  ]
  edge [
    source 874
    target 875
  ]
  edge [
    source 876
    target 877
  ]
  edge [
    source 876
    target 878
  ]
  edge [
    source 877
    target 878
  ]
]
