graph [
  node [
    id 0
    label "tarasek"
    origin "text"
  ]
  node [
    id 1
    label "sulejowski"
  ]
  node [
    id 2
    label "park"
  ]
  node [
    id 3
    label "krajobrazowy"
  ]
  node [
    id 4
    label "Tarasek"
  ]
  node [
    id 5
    label "g&#243;ry"
  ]
  node [
    id 6
    label "niwa"
  ]
  node [
    id 7
    label "og&#243;lnopolski"
  ]
  node [
    id 8
    label "nocny"
  ]
  node [
    id 9
    label "rajd"
  ]
  node [
    id 10
    label "mazowiecki"
  ]
  node [
    id 11
    label "Tarasce"
  ]
  node [
    id 12
    label "szlaka"
  ]
  node [
    id 13
    label "rekreacyjny"
  ]
  node [
    id 14
    label "rzeka"
  ]
  node [
    id 15
    label "Pilica"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
]
