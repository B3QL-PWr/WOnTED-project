graph [
  node [
    id 0
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 1
    label "tendencja"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "droga"
    origin "text"
  ]
  node [
    id 4
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 5
    label "drugi"
    origin "text"
  ]
  node [
    id 6
    label "potr&#261;ci&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pieszy"
    origin "text"
  ]
  node [
    id 8
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 9
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 10
    label "niebezpiecznie"
  ]
  node [
    id 11
    label "gro&#378;ny"
  ]
  node [
    id 12
    label "k&#322;opotliwy"
  ]
  node [
    id 13
    label "k&#322;opotliwie"
  ]
  node [
    id 14
    label "gro&#378;nie"
  ]
  node [
    id 15
    label "nieprzyjemny"
  ]
  node [
    id 16
    label "niewygodny"
  ]
  node [
    id 17
    label "nad&#261;sany"
  ]
  node [
    id 18
    label "ideologia"
  ]
  node [
    id 19
    label "podatno&#347;&#263;"
  ]
  node [
    id 20
    label "metoda"
  ]
  node [
    id 21
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 22
    label "praktyka"
  ]
  node [
    id 23
    label "idea"
  ]
  node [
    id 24
    label "system"
  ]
  node [
    id 25
    label "byt"
  ]
  node [
    id 26
    label "intelekt"
  ]
  node [
    id 27
    label "Kant"
  ]
  node [
    id 28
    label "p&#322;&#243;d"
  ]
  node [
    id 29
    label "cel"
  ]
  node [
    id 30
    label "poj&#281;cie"
  ]
  node [
    id 31
    label "istota"
  ]
  node [
    id 32
    label "pomys&#322;"
  ]
  node [
    id 33
    label "ideacja"
  ]
  node [
    id 34
    label "cecha"
  ]
  node [
    id 35
    label "j&#261;dro"
  ]
  node [
    id 36
    label "systemik"
  ]
  node [
    id 37
    label "rozprz&#261;c"
  ]
  node [
    id 38
    label "oprogramowanie"
  ]
  node [
    id 39
    label "systemat"
  ]
  node [
    id 40
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 41
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 42
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 43
    label "model"
  ]
  node [
    id 44
    label "struktura"
  ]
  node [
    id 45
    label "usenet"
  ]
  node [
    id 46
    label "s&#261;d"
  ]
  node [
    id 47
    label "zbi&#243;r"
  ]
  node [
    id 48
    label "porz&#261;dek"
  ]
  node [
    id 49
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 50
    label "przyn&#281;ta"
  ]
  node [
    id 51
    label "net"
  ]
  node [
    id 52
    label "w&#281;dkarstwo"
  ]
  node [
    id 53
    label "eratem"
  ]
  node [
    id 54
    label "oddzia&#322;"
  ]
  node [
    id 55
    label "doktryna"
  ]
  node [
    id 56
    label "pulpit"
  ]
  node [
    id 57
    label "konstelacja"
  ]
  node [
    id 58
    label "jednostka_geologiczna"
  ]
  node [
    id 59
    label "o&#347;"
  ]
  node [
    id 60
    label "podsystem"
  ]
  node [
    id 61
    label "ryba"
  ]
  node [
    id 62
    label "Leopard"
  ]
  node [
    id 63
    label "spos&#243;b"
  ]
  node [
    id 64
    label "Android"
  ]
  node [
    id 65
    label "zachowanie"
  ]
  node [
    id 66
    label "cybernetyk"
  ]
  node [
    id 67
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 68
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 69
    label "method"
  ]
  node [
    id 70
    label "sk&#322;ad"
  ]
  node [
    id 71
    label "podstawa"
  ]
  node [
    id 72
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 73
    label "practice"
  ]
  node [
    id 74
    label "wiedza"
  ]
  node [
    id 75
    label "znawstwo"
  ]
  node [
    id 76
    label "skill"
  ]
  node [
    id 77
    label "czyn"
  ]
  node [
    id 78
    label "nauka"
  ]
  node [
    id 79
    label "zwyczaj"
  ]
  node [
    id 80
    label "eksperiencja"
  ]
  node [
    id 81
    label "praca"
  ]
  node [
    id 82
    label "zasada"
  ]
  node [
    id 83
    label "relacja"
  ]
  node [
    id 84
    label "political_orientation"
  ]
  node [
    id 85
    label "szko&#322;a"
  ]
  node [
    id 86
    label "przedmiot"
  ]
  node [
    id 87
    label "Polish"
  ]
  node [
    id 88
    label "goniony"
  ]
  node [
    id 89
    label "oberek"
  ]
  node [
    id 90
    label "ryba_po_grecku"
  ]
  node [
    id 91
    label "sztajer"
  ]
  node [
    id 92
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 93
    label "krakowiak"
  ]
  node [
    id 94
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 95
    label "pierogi_ruskie"
  ]
  node [
    id 96
    label "lacki"
  ]
  node [
    id 97
    label "polak"
  ]
  node [
    id 98
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 99
    label "chodzony"
  ]
  node [
    id 100
    label "po_polsku"
  ]
  node [
    id 101
    label "mazur"
  ]
  node [
    id 102
    label "polsko"
  ]
  node [
    id 103
    label "skoczny"
  ]
  node [
    id 104
    label "drabant"
  ]
  node [
    id 105
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 106
    label "j&#281;zyk"
  ]
  node [
    id 107
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 108
    label "artykulator"
  ]
  node [
    id 109
    label "kod"
  ]
  node [
    id 110
    label "kawa&#322;ek"
  ]
  node [
    id 111
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 112
    label "gramatyka"
  ]
  node [
    id 113
    label "stylik"
  ]
  node [
    id 114
    label "przet&#322;umaczenie"
  ]
  node [
    id 115
    label "formalizowanie"
  ]
  node [
    id 116
    label "ssa&#263;"
  ]
  node [
    id 117
    label "ssanie"
  ]
  node [
    id 118
    label "language"
  ]
  node [
    id 119
    label "liza&#263;"
  ]
  node [
    id 120
    label "napisa&#263;"
  ]
  node [
    id 121
    label "konsonantyzm"
  ]
  node [
    id 122
    label "wokalizm"
  ]
  node [
    id 123
    label "pisa&#263;"
  ]
  node [
    id 124
    label "fonetyka"
  ]
  node [
    id 125
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 126
    label "jeniec"
  ]
  node [
    id 127
    label "but"
  ]
  node [
    id 128
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 129
    label "po_koroniarsku"
  ]
  node [
    id 130
    label "kultura_duchowa"
  ]
  node [
    id 131
    label "t&#322;umaczenie"
  ]
  node [
    id 132
    label "m&#243;wienie"
  ]
  node [
    id 133
    label "pype&#263;"
  ]
  node [
    id 134
    label "lizanie"
  ]
  node [
    id 135
    label "pismo"
  ]
  node [
    id 136
    label "formalizowa&#263;"
  ]
  node [
    id 137
    label "rozumie&#263;"
  ]
  node [
    id 138
    label "organ"
  ]
  node [
    id 139
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 140
    label "rozumienie"
  ]
  node [
    id 141
    label "makroglosja"
  ]
  node [
    id 142
    label "m&#243;wi&#263;"
  ]
  node [
    id 143
    label "jama_ustna"
  ]
  node [
    id 144
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 145
    label "formacja_geologiczna"
  ]
  node [
    id 146
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 147
    label "natural_language"
  ]
  node [
    id 148
    label "s&#322;ownictwo"
  ]
  node [
    id 149
    label "urz&#261;dzenie"
  ]
  node [
    id 150
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 151
    label "wschodnioeuropejski"
  ]
  node [
    id 152
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 153
    label "poga&#324;ski"
  ]
  node [
    id 154
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 155
    label "topielec"
  ]
  node [
    id 156
    label "europejski"
  ]
  node [
    id 157
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 158
    label "langosz"
  ]
  node [
    id 159
    label "zboczenie"
  ]
  node [
    id 160
    label "om&#243;wienie"
  ]
  node [
    id 161
    label "sponiewieranie"
  ]
  node [
    id 162
    label "discipline"
  ]
  node [
    id 163
    label "rzecz"
  ]
  node [
    id 164
    label "omawia&#263;"
  ]
  node [
    id 165
    label "kr&#261;&#380;enie"
  ]
  node [
    id 166
    label "tre&#347;&#263;"
  ]
  node [
    id 167
    label "robienie"
  ]
  node [
    id 168
    label "sponiewiera&#263;"
  ]
  node [
    id 169
    label "element"
  ]
  node [
    id 170
    label "entity"
  ]
  node [
    id 171
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 172
    label "tematyka"
  ]
  node [
    id 173
    label "w&#261;tek"
  ]
  node [
    id 174
    label "charakter"
  ]
  node [
    id 175
    label "zbaczanie"
  ]
  node [
    id 176
    label "program_nauczania"
  ]
  node [
    id 177
    label "om&#243;wi&#263;"
  ]
  node [
    id 178
    label "omawianie"
  ]
  node [
    id 179
    label "thing"
  ]
  node [
    id 180
    label "kultura"
  ]
  node [
    id 181
    label "zbacza&#263;"
  ]
  node [
    id 182
    label "zboczy&#263;"
  ]
  node [
    id 183
    label "gwardzista"
  ]
  node [
    id 184
    label "melodia"
  ]
  node [
    id 185
    label "taniec"
  ]
  node [
    id 186
    label "taniec_ludowy"
  ]
  node [
    id 187
    label "&#347;redniowieczny"
  ]
  node [
    id 188
    label "europejsko"
  ]
  node [
    id 189
    label "specjalny"
  ]
  node [
    id 190
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 191
    label "weso&#322;y"
  ]
  node [
    id 192
    label "sprawny"
  ]
  node [
    id 193
    label "rytmiczny"
  ]
  node [
    id 194
    label "skocznie"
  ]
  node [
    id 195
    label "energiczny"
  ]
  node [
    id 196
    label "przytup"
  ]
  node [
    id 197
    label "ho&#322;ubiec"
  ]
  node [
    id 198
    label "wodzi&#263;"
  ]
  node [
    id 199
    label "lendler"
  ]
  node [
    id 200
    label "austriacki"
  ]
  node [
    id 201
    label "polka"
  ]
  node [
    id 202
    label "ludowy"
  ]
  node [
    id 203
    label "pie&#347;&#324;"
  ]
  node [
    id 204
    label "mieszkaniec"
  ]
  node [
    id 205
    label "centu&#347;"
  ]
  node [
    id 206
    label "lalka"
  ]
  node [
    id 207
    label "Ma&#322;opolanin"
  ]
  node [
    id 208
    label "krakauer"
  ]
  node [
    id 209
    label "ekskursja"
  ]
  node [
    id 210
    label "bezsilnikowy"
  ]
  node [
    id 211
    label "budowla"
  ]
  node [
    id 212
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 213
    label "trasa"
  ]
  node [
    id 214
    label "podbieg"
  ]
  node [
    id 215
    label "turystyka"
  ]
  node [
    id 216
    label "nawierzchnia"
  ]
  node [
    id 217
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 218
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 219
    label "rajza"
  ]
  node [
    id 220
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 221
    label "korona_drogi"
  ]
  node [
    id 222
    label "passage"
  ]
  node [
    id 223
    label "wylot"
  ]
  node [
    id 224
    label "ekwipunek"
  ]
  node [
    id 225
    label "zbior&#243;wka"
  ]
  node [
    id 226
    label "marszrutyzacja"
  ]
  node [
    id 227
    label "wyb&#243;j"
  ]
  node [
    id 228
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 229
    label "drogowskaz"
  ]
  node [
    id 230
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 231
    label "pobocze"
  ]
  node [
    id 232
    label "journey"
  ]
  node [
    id 233
    label "ruch"
  ]
  node [
    id 234
    label "przebieg"
  ]
  node [
    id 235
    label "infrastruktura"
  ]
  node [
    id 236
    label "w&#281;ze&#322;"
  ]
  node [
    id 237
    label "obudowanie"
  ]
  node [
    id 238
    label "obudowywa&#263;"
  ]
  node [
    id 239
    label "zbudowa&#263;"
  ]
  node [
    id 240
    label "obudowa&#263;"
  ]
  node [
    id 241
    label "kolumnada"
  ]
  node [
    id 242
    label "korpus"
  ]
  node [
    id 243
    label "Sukiennice"
  ]
  node [
    id 244
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 245
    label "fundament"
  ]
  node [
    id 246
    label "postanie"
  ]
  node [
    id 247
    label "obudowywanie"
  ]
  node [
    id 248
    label "zbudowanie"
  ]
  node [
    id 249
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 250
    label "stan_surowy"
  ]
  node [
    id 251
    label "konstrukcja"
  ]
  node [
    id 252
    label "narz&#281;dzie"
  ]
  node [
    id 253
    label "tryb"
  ]
  node [
    id 254
    label "nature"
  ]
  node [
    id 255
    label "ton"
  ]
  node [
    id 256
    label "rozmiar"
  ]
  node [
    id 257
    label "odcinek"
  ]
  node [
    id 258
    label "ambitus"
  ]
  node [
    id 259
    label "czas"
  ]
  node [
    id 260
    label "skala"
  ]
  node [
    id 261
    label "mechanika"
  ]
  node [
    id 262
    label "utrzymywanie"
  ]
  node [
    id 263
    label "move"
  ]
  node [
    id 264
    label "poruszenie"
  ]
  node [
    id 265
    label "movement"
  ]
  node [
    id 266
    label "myk"
  ]
  node [
    id 267
    label "utrzyma&#263;"
  ]
  node [
    id 268
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 269
    label "zjawisko"
  ]
  node [
    id 270
    label "utrzymanie"
  ]
  node [
    id 271
    label "travel"
  ]
  node [
    id 272
    label "kanciasty"
  ]
  node [
    id 273
    label "commercial_enterprise"
  ]
  node [
    id 274
    label "strumie&#324;"
  ]
  node [
    id 275
    label "proces"
  ]
  node [
    id 276
    label "aktywno&#347;&#263;"
  ]
  node [
    id 277
    label "kr&#243;tki"
  ]
  node [
    id 278
    label "taktyka"
  ]
  node [
    id 279
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 280
    label "apraksja"
  ]
  node [
    id 281
    label "natural_process"
  ]
  node [
    id 282
    label "utrzymywa&#263;"
  ]
  node [
    id 283
    label "d&#322;ugi"
  ]
  node [
    id 284
    label "wydarzenie"
  ]
  node [
    id 285
    label "dyssypacja_energii"
  ]
  node [
    id 286
    label "tumult"
  ]
  node [
    id 287
    label "stopek"
  ]
  node [
    id 288
    label "czynno&#347;&#263;"
  ]
  node [
    id 289
    label "zmiana"
  ]
  node [
    id 290
    label "manewr"
  ]
  node [
    id 291
    label "lokomocja"
  ]
  node [
    id 292
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 293
    label "komunikacja"
  ]
  node [
    id 294
    label "drift"
  ]
  node [
    id 295
    label "r&#281;kaw"
  ]
  node [
    id 296
    label "kontusz"
  ]
  node [
    id 297
    label "koniec"
  ]
  node [
    id 298
    label "otw&#243;r"
  ]
  node [
    id 299
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 300
    label "warstwa"
  ]
  node [
    id 301
    label "pokrycie"
  ]
  node [
    id 302
    label "fingerpost"
  ]
  node [
    id 303
    label "tablica"
  ]
  node [
    id 304
    label "przydro&#380;e"
  ]
  node [
    id 305
    label "autostrada"
  ]
  node [
    id 306
    label "bieg"
  ]
  node [
    id 307
    label "operacja"
  ]
  node [
    id 308
    label "podr&#243;&#380;"
  ]
  node [
    id 309
    label "mieszanie_si&#281;"
  ]
  node [
    id 310
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 311
    label "chodzenie"
  ]
  node [
    id 312
    label "digress"
  ]
  node [
    id 313
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 314
    label "pozostawa&#263;"
  ]
  node [
    id 315
    label "s&#261;dzi&#263;"
  ]
  node [
    id 316
    label "chodzi&#263;"
  ]
  node [
    id 317
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 318
    label "stray"
  ]
  node [
    id 319
    label "kocher"
  ]
  node [
    id 320
    label "wyposa&#380;enie"
  ]
  node [
    id 321
    label "nie&#347;miertelnik"
  ]
  node [
    id 322
    label "moderunek"
  ]
  node [
    id 323
    label "dormitorium"
  ]
  node [
    id 324
    label "sk&#322;adanka"
  ]
  node [
    id 325
    label "wyprawa"
  ]
  node [
    id 326
    label "polowanie"
  ]
  node [
    id 327
    label "spis"
  ]
  node [
    id 328
    label "pomieszczenie"
  ]
  node [
    id 329
    label "fotografia"
  ]
  node [
    id 330
    label "beznap&#281;dowy"
  ]
  node [
    id 331
    label "ukochanie"
  ]
  node [
    id 332
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 333
    label "feblik"
  ]
  node [
    id 334
    label "podnieci&#263;"
  ]
  node [
    id 335
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 336
    label "numer"
  ]
  node [
    id 337
    label "po&#380;ycie"
  ]
  node [
    id 338
    label "tendency"
  ]
  node [
    id 339
    label "podniecenie"
  ]
  node [
    id 340
    label "afekt"
  ]
  node [
    id 341
    label "zakochanie"
  ]
  node [
    id 342
    label "zajawka"
  ]
  node [
    id 343
    label "seks"
  ]
  node [
    id 344
    label "podniecanie"
  ]
  node [
    id 345
    label "imisja"
  ]
  node [
    id 346
    label "love"
  ]
  node [
    id 347
    label "rozmna&#380;anie"
  ]
  node [
    id 348
    label "ruch_frykcyjny"
  ]
  node [
    id 349
    label "na_pieska"
  ]
  node [
    id 350
    label "serce"
  ]
  node [
    id 351
    label "pozycja_misjonarska"
  ]
  node [
    id 352
    label "wi&#281;&#378;"
  ]
  node [
    id 353
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 354
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 355
    label "z&#322;&#261;czenie"
  ]
  node [
    id 356
    label "gra_wst&#281;pna"
  ]
  node [
    id 357
    label "erotyka"
  ]
  node [
    id 358
    label "emocja"
  ]
  node [
    id 359
    label "baraszki"
  ]
  node [
    id 360
    label "drogi"
  ]
  node [
    id 361
    label "po&#380;&#261;danie"
  ]
  node [
    id 362
    label "wzw&#243;d"
  ]
  node [
    id 363
    label "podnieca&#263;"
  ]
  node [
    id 364
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 365
    label "kochanka"
  ]
  node [
    id 366
    label "kultura_fizyczna"
  ]
  node [
    id 367
    label "turyzm"
  ]
  node [
    id 368
    label "kolejny"
  ]
  node [
    id 369
    label "sw&#243;j"
  ]
  node [
    id 370
    label "przeciwny"
  ]
  node [
    id 371
    label "wt&#243;ry"
  ]
  node [
    id 372
    label "dzie&#324;"
  ]
  node [
    id 373
    label "inny"
  ]
  node [
    id 374
    label "odwrotnie"
  ]
  node [
    id 375
    label "podobny"
  ]
  node [
    id 376
    label "osobno"
  ]
  node [
    id 377
    label "r&#243;&#380;ny"
  ]
  node [
    id 378
    label "inszy"
  ]
  node [
    id 379
    label "inaczej"
  ]
  node [
    id 380
    label "ludzko&#347;&#263;"
  ]
  node [
    id 381
    label "asymilowanie"
  ]
  node [
    id 382
    label "wapniak"
  ]
  node [
    id 383
    label "asymilowa&#263;"
  ]
  node [
    id 384
    label "os&#322;abia&#263;"
  ]
  node [
    id 385
    label "posta&#263;"
  ]
  node [
    id 386
    label "hominid"
  ]
  node [
    id 387
    label "podw&#322;adny"
  ]
  node [
    id 388
    label "os&#322;abianie"
  ]
  node [
    id 389
    label "g&#322;owa"
  ]
  node [
    id 390
    label "figura"
  ]
  node [
    id 391
    label "portrecista"
  ]
  node [
    id 392
    label "dwun&#243;g"
  ]
  node [
    id 393
    label "profanum"
  ]
  node [
    id 394
    label "mikrokosmos"
  ]
  node [
    id 395
    label "nasada"
  ]
  node [
    id 396
    label "duch"
  ]
  node [
    id 397
    label "antropochoria"
  ]
  node [
    id 398
    label "osoba"
  ]
  node [
    id 399
    label "wz&#243;r"
  ]
  node [
    id 400
    label "senior"
  ]
  node [
    id 401
    label "oddzia&#322;ywanie"
  ]
  node [
    id 402
    label "Adam"
  ]
  node [
    id 403
    label "homo_sapiens"
  ]
  node [
    id 404
    label "polifag"
  ]
  node [
    id 405
    label "nast&#281;pnie"
  ]
  node [
    id 406
    label "nastopny"
  ]
  node [
    id 407
    label "kolejno"
  ]
  node [
    id 408
    label "kt&#243;ry&#347;"
  ]
  node [
    id 409
    label "ranek"
  ]
  node [
    id 410
    label "doba"
  ]
  node [
    id 411
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 412
    label "noc"
  ]
  node [
    id 413
    label "podwiecz&#243;r"
  ]
  node [
    id 414
    label "po&#322;udnie"
  ]
  node [
    id 415
    label "godzina"
  ]
  node [
    id 416
    label "przedpo&#322;udnie"
  ]
  node [
    id 417
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 418
    label "long_time"
  ]
  node [
    id 419
    label "wiecz&#243;r"
  ]
  node [
    id 420
    label "t&#322;usty_czwartek"
  ]
  node [
    id 421
    label "popo&#322;udnie"
  ]
  node [
    id 422
    label "walentynki"
  ]
  node [
    id 423
    label "czynienie_si&#281;"
  ]
  node [
    id 424
    label "s&#322;o&#324;ce"
  ]
  node [
    id 425
    label "rano"
  ]
  node [
    id 426
    label "tydzie&#324;"
  ]
  node [
    id 427
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 428
    label "wzej&#347;cie"
  ]
  node [
    id 429
    label "wsta&#263;"
  ]
  node [
    id 430
    label "day"
  ]
  node [
    id 431
    label "termin"
  ]
  node [
    id 432
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 433
    label "wstanie"
  ]
  node [
    id 434
    label "przedwiecz&#243;r"
  ]
  node [
    id 435
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 436
    label "Sylwester"
  ]
  node [
    id 437
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 438
    label "odmienny"
  ]
  node [
    id 439
    label "po_przeciwnej_stronie"
  ]
  node [
    id 440
    label "przeciwnie"
  ]
  node [
    id 441
    label "niech&#281;tny"
  ]
  node [
    id 442
    label "samodzielny"
  ]
  node [
    id 443
    label "swojak"
  ]
  node [
    id 444
    label "odpowiedni"
  ]
  node [
    id 445
    label "bli&#378;ni"
  ]
  node [
    id 446
    label "przypominanie"
  ]
  node [
    id 447
    label "podobnie"
  ]
  node [
    id 448
    label "upodabnianie_si&#281;"
  ]
  node [
    id 449
    label "upodobnienie"
  ]
  node [
    id 450
    label "taki"
  ]
  node [
    id 451
    label "charakterystyczny"
  ]
  node [
    id 452
    label "upodobnienie_si&#281;"
  ]
  node [
    id 453
    label "zasymilowanie"
  ]
  node [
    id 454
    label "na_abarot"
  ]
  node [
    id 455
    label "odmiennie"
  ]
  node [
    id 456
    label "odwrotny"
  ]
  node [
    id 457
    label "odliczy&#263;"
  ]
  node [
    id 458
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 459
    label "smell"
  ]
  node [
    id 460
    label "uderzy&#263;"
  ]
  node [
    id 461
    label "allude"
  ]
  node [
    id 462
    label "precipitate"
  ]
  node [
    id 463
    label "odmierzy&#263;"
  ]
  node [
    id 464
    label "policzy&#263;"
  ]
  node [
    id 465
    label "odj&#261;&#263;"
  ]
  node [
    id 466
    label "count"
  ]
  node [
    id 467
    label "urazi&#263;"
  ]
  node [
    id 468
    label "strike"
  ]
  node [
    id 469
    label "wystartowa&#263;"
  ]
  node [
    id 470
    label "przywali&#263;"
  ]
  node [
    id 471
    label "dupn&#261;&#263;"
  ]
  node [
    id 472
    label "skrytykowa&#263;"
  ]
  node [
    id 473
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 474
    label "nast&#261;pi&#263;"
  ]
  node [
    id 475
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 476
    label "sztachn&#261;&#263;"
  ]
  node [
    id 477
    label "rap"
  ]
  node [
    id 478
    label "zrobi&#263;"
  ]
  node [
    id 479
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 480
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 481
    label "crush"
  ]
  node [
    id 482
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 483
    label "postara&#263;_si&#281;"
  ]
  node [
    id 484
    label "fall"
  ]
  node [
    id 485
    label "hopn&#261;&#263;"
  ]
  node [
    id 486
    label "spowodowa&#263;"
  ]
  node [
    id 487
    label "zada&#263;"
  ]
  node [
    id 488
    label "uda&#263;_si&#281;"
  ]
  node [
    id 489
    label "dotkn&#261;&#263;"
  ]
  node [
    id 490
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 491
    label "anoint"
  ]
  node [
    id 492
    label "transgress"
  ]
  node [
    id 493
    label "chop"
  ]
  node [
    id 494
    label "jebn&#261;&#263;"
  ]
  node [
    id 495
    label "lumber"
  ]
  node [
    id 496
    label "pieszo"
  ]
  node [
    id 497
    label "chodnik"
  ]
  node [
    id 498
    label "piechotny"
  ]
  node [
    id 499
    label "w&#281;drowiec"
  ]
  node [
    id 500
    label "intencjonalny"
  ]
  node [
    id 501
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 502
    label "niedorozw&#243;j"
  ]
  node [
    id 503
    label "szczeg&#243;lny"
  ]
  node [
    id 504
    label "specjalnie"
  ]
  node [
    id 505
    label "nieetatowy"
  ]
  node [
    id 506
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 507
    label "nienormalny"
  ]
  node [
    id 508
    label "umy&#347;lnie"
  ]
  node [
    id 509
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 510
    label "na_pieszo"
  ]
  node [
    id 511
    label "z_buta"
  ]
  node [
    id 512
    label "na_pieszk&#281;"
  ]
  node [
    id 513
    label "podr&#243;&#380;nik"
  ]
  node [
    id 514
    label "kraw&#281;&#380;nik"
  ]
  node [
    id 515
    label "chody"
  ]
  node [
    id 516
    label "sztreka"
  ]
  node [
    id 517
    label "kostka_brukowa"
  ]
  node [
    id 518
    label "drzewo"
  ]
  node [
    id 519
    label "wyrobisko"
  ]
  node [
    id 520
    label "kornik"
  ]
  node [
    id 521
    label "dywanik"
  ]
  node [
    id 522
    label "ulica"
  ]
  node [
    id 523
    label "przodek"
  ]
  node [
    id 524
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 525
    label "konsument"
  ]
  node [
    id 526
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 527
    label "cz&#322;owiekowate"
  ]
  node [
    id 528
    label "istota_&#380;ywa"
  ]
  node [
    id 529
    label "pracownik"
  ]
  node [
    id 530
    label "Chocho&#322;"
  ]
  node [
    id 531
    label "Herkules_Poirot"
  ]
  node [
    id 532
    label "Edyp"
  ]
  node [
    id 533
    label "parali&#380;owa&#263;"
  ]
  node [
    id 534
    label "Harry_Potter"
  ]
  node [
    id 535
    label "Casanova"
  ]
  node [
    id 536
    label "Zgredek"
  ]
  node [
    id 537
    label "Gargantua"
  ]
  node [
    id 538
    label "Winnetou"
  ]
  node [
    id 539
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 540
    label "Dulcynea"
  ]
  node [
    id 541
    label "kategoria_gramatyczna"
  ]
  node [
    id 542
    label "person"
  ]
  node [
    id 543
    label "Plastu&#347;"
  ]
  node [
    id 544
    label "Quasimodo"
  ]
  node [
    id 545
    label "Sherlock_Holmes"
  ]
  node [
    id 546
    label "Faust"
  ]
  node [
    id 547
    label "Wallenrod"
  ]
  node [
    id 548
    label "Dwukwiat"
  ]
  node [
    id 549
    label "Don_Juan"
  ]
  node [
    id 550
    label "koniugacja"
  ]
  node [
    id 551
    label "Don_Kiszot"
  ]
  node [
    id 552
    label "Hamlet"
  ]
  node [
    id 553
    label "Werter"
  ]
  node [
    id 554
    label "Szwejk"
  ]
  node [
    id 555
    label "doros&#322;y"
  ]
  node [
    id 556
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 557
    label "jajko"
  ]
  node [
    id 558
    label "rodzic"
  ]
  node [
    id 559
    label "wapniaki"
  ]
  node [
    id 560
    label "zwierzchnik"
  ]
  node [
    id 561
    label "feuda&#322;"
  ]
  node [
    id 562
    label "starzec"
  ]
  node [
    id 563
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 564
    label "zawodnik"
  ]
  node [
    id 565
    label "komendancja"
  ]
  node [
    id 566
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 567
    label "asymilowanie_si&#281;"
  ]
  node [
    id 568
    label "absorption"
  ]
  node [
    id 569
    label "pobieranie"
  ]
  node [
    id 570
    label "czerpanie"
  ]
  node [
    id 571
    label "acquisition"
  ]
  node [
    id 572
    label "zmienianie"
  ]
  node [
    id 573
    label "organizm"
  ]
  node [
    id 574
    label "assimilation"
  ]
  node [
    id 575
    label "upodabnianie"
  ]
  node [
    id 576
    label "g&#322;oska"
  ]
  node [
    id 577
    label "grupa"
  ]
  node [
    id 578
    label "suppress"
  ]
  node [
    id 579
    label "robi&#263;"
  ]
  node [
    id 580
    label "os&#322;abienie"
  ]
  node [
    id 581
    label "kondycja_fizyczna"
  ]
  node [
    id 582
    label "os&#322;abi&#263;"
  ]
  node [
    id 583
    label "zdrowie"
  ]
  node [
    id 584
    label "powodowa&#263;"
  ]
  node [
    id 585
    label "zmniejsza&#263;"
  ]
  node [
    id 586
    label "bate"
  ]
  node [
    id 587
    label "de-escalation"
  ]
  node [
    id 588
    label "powodowanie"
  ]
  node [
    id 589
    label "debilitation"
  ]
  node [
    id 590
    label "zmniejszanie"
  ]
  node [
    id 591
    label "s&#322;abszy"
  ]
  node [
    id 592
    label "pogarszanie"
  ]
  node [
    id 593
    label "assimilate"
  ]
  node [
    id 594
    label "dostosowywa&#263;"
  ]
  node [
    id 595
    label "dostosowa&#263;"
  ]
  node [
    id 596
    label "przejmowa&#263;"
  ]
  node [
    id 597
    label "upodobni&#263;"
  ]
  node [
    id 598
    label "przej&#261;&#263;"
  ]
  node [
    id 599
    label "upodabnia&#263;"
  ]
  node [
    id 600
    label "pobiera&#263;"
  ]
  node [
    id 601
    label "pobra&#263;"
  ]
  node [
    id 602
    label "zapis"
  ]
  node [
    id 603
    label "figure"
  ]
  node [
    id 604
    label "typ"
  ]
  node [
    id 605
    label "mildew"
  ]
  node [
    id 606
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 607
    label "ideal"
  ]
  node [
    id 608
    label "rule"
  ]
  node [
    id 609
    label "dekal"
  ]
  node [
    id 610
    label "motyw"
  ]
  node [
    id 611
    label "projekt"
  ]
  node [
    id 612
    label "charakterystyka"
  ]
  node [
    id 613
    label "zaistnie&#263;"
  ]
  node [
    id 614
    label "Osjan"
  ]
  node [
    id 615
    label "kto&#347;"
  ]
  node [
    id 616
    label "wygl&#261;d"
  ]
  node [
    id 617
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 618
    label "osobowo&#347;&#263;"
  ]
  node [
    id 619
    label "wytw&#243;r"
  ]
  node [
    id 620
    label "trim"
  ]
  node [
    id 621
    label "poby&#263;"
  ]
  node [
    id 622
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 623
    label "Aspazja"
  ]
  node [
    id 624
    label "punkt_widzenia"
  ]
  node [
    id 625
    label "kompleksja"
  ]
  node [
    id 626
    label "wytrzyma&#263;"
  ]
  node [
    id 627
    label "budowa"
  ]
  node [
    id 628
    label "formacja"
  ]
  node [
    id 629
    label "pozosta&#263;"
  ]
  node [
    id 630
    label "point"
  ]
  node [
    id 631
    label "przedstawienie"
  ]
  node [
    id 632
    label "go&#347;&#263;"
  ]
  node [
    id 633
    label "fotograf"
  ]
  node [
    id 634
    label "malarz"
  ]
  node [
    id 635
    label "artysta"
  ]
  node [
    id 636
    label "hipnotyzowanie"
  ]
  node [
    id 637
    label "&#347;lad"
  ]
  node [
    id 638
    label "docieranie"
  ]
  node [
    id 639
    label "reakcja_chemiczna"
  ]
  node [
    id 640
    label "wdzieranie_si&#281;"
  ]
  node [
    id 641
    label "act"
  ]
  node [
    id 642
    label "rezultat"
  ]
  node [
    id 643
    label "lobbysta"
  ]
  node [
    id 644
    label "pryncypa&#322;"
  ]
  node [
    id 645
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 646
    label "kszta&#322;t"
  ]
  node [
    id 647
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 648
    label "kierowa&#263;"
  ]
  node [
    id 649
    label "alkohol"
  ]
  node [
    id 650
    label "zdolno&#347;&#263;"
  ]
  node [
    id 651
    label "&#380;ycie"
  ]
  node [
    id 652
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 653
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 654
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 655
    label "sztuka"
  ]
  node [
    id 656
    label "dekiel"
  ]
  node [
    id 657
    label "ro&#347;lina"
  ]
  node [
    id 658
    label "&#347;ci&#281;cie"
  ]
  node [
    id 659
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 660
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 661
    label "&#347;ci&#281;gno"
  ]
  node [
    id 662
    label "noosfera"
  ]
  node [
    id 663
    label "byd&#322;o"
  ]
  node [
    id 664
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 665
    label "makrocefalia"
  ]
  node [
    id 666
    label "obiekt"
  ]
  node [
    id 667
    label "ucho"
  ]
  node [
    id 668
    label "g&#243;ra"
  ]
  node [
    id 669
    label "m&#243;zg"
  ]
  node [
    id 670
    label "kierownictwo"
  ]
  node [
    id 671
    label "fryzura"
  ]
  node [
    id 672
    label "umys&#322;"
  ]
  node [
    id 673
    label "cia&#322;o"
  ]
  node [
    id 674
    label "cz&#322;onek"
  ]
  node [
    id 675
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 676
    label "czaszka"
  ]
  node [
    id 677
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 678
    label "allochoria"
  ]
  node [
    id 679
    label "p&#322;aszczyzna"
  ]
  node [
    id 680
    label "bierka_szachowa"
  ]
  node [
    id 681
    label "obiekt_matematyczny"
  ]
  node [
    id 682
    label "gestaltyzm"
  ]
  node [
    id 683
    label "styl"
  ]
  node [
    id 684
    label "obraz"
  ]
  node [
    id 685
    label "d&#378;wi&#281;k"
  ]
  node [
    id 686
    label "character"
  ]
  node [
    id 687
    label "rze&#378;ba"
  ]
  node [
    id 688
    label "stylistyka"
  ]
  node [
    id 689
    label "miejsce"
  ]
  node [
    id 690
    label "antycypacja"
  ]
  node [
    id 691
    label "ornamentyka"
  ]
  node [
    id 692
    label "informacja"
  ]
  node [
    id 693
    label "facet"
  ]
  node [
    id 694
    label "popis"
  ]
  node [
    id 695
    label "wiersz"
  ]
  node [
    id 696
    label "symetria"
  ]
  node [
    id 697
    label "lingwistyka_kognitywna"
  ]
  node [
    id 698
    label "karta"
  ]
  node [
    id 699
    label "shape"
  ]
  node [
    id 700
    label "podzbi&#243;r"
  ]
  node [
    id 701
    label "perspektywa"
  ]
  node [
    id 702
    label "dziedzina"
  ]
  node [
    id 703
    label "nak&#322;adka"
  ]
  node [
    id 704
    label "li&#347;&#263;"
  ]
  node [
    id 705
    label "jama_gard&#322;owa"
  ]
  node [
    id 706
    label "rezonator"
  ]
  node [
    id 707
    label "base"
  ]
  node [
    id 708
    label "piek&#322;o"
  ]
  node [
    id 709
    label "human_body"
  ]
  node [
    id 710
    label "ofiarowywanie"
  ]
  node [
    id 711
    label "sfera_afektywna"
  ]
  node [
    id 712
    label "nekromancja"
  ]
  node [
    id 713
    label "Po&#347;wist"
  ]
  node [
    id 714
    label "podekscytowanie"
  ]
  node [
    id 715
    label "deformowanie"
  ]
  node [
    id 716
    label "sumienie"
  ]
  node [
    id 717
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 718
    label "deformowa&#263;"
  ]
  node [
    id 719
    label "psychika"
  ]
  node [
    id 720
    label "zjawa"
  ]
  node [
    id 721
    label "zmar&#322;y"
  ]
  node [
    id 722
    label "istota_nadprzyrodzona"
  ]
  node [
    id 723
    label "power"
  ]
  node [
    id 724
    label "ofiarowywa&#263;"
  ]
  node [
    id 725
    label "oddech"
  ]
  node [
    id 726
    label "seksualno&#347;&#263;"
  ]
  node [
    id 727
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 728
    label "si&#322;a"
  ]
  node [
    id 729
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 730
    label "ego"
  ]
  node [
    id 731
    label "ofiarowanie"
  ]
  node [
    id 732
    label "fizjonomia"
  ]
  node [
    id 733
    label "kompleks"
  ]
  node [
    id 734
    label "zapalno&#347;&#263;"
  ]
  node [
    id 735
    label "T&#281;sknica"
  ]
  node [
    id 736
    label "ofiarowa&#263;"
  ]
  node [
    id 737
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 738
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 739
    label "passion"
  ]
  node [
    id 740
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 741
    label "atom"
  ]
  node [
    id 742
    label "odbicie"
  ]
  node [
    id 743
    label "przyroda"
  ]
  node [
    id 744
    label "Ziemia"
  ]
  node [
    id 745
    label "kosmos"
  ]
  node [
    id 746
    label "miniatura"
  ]
  node [
    id 747
    label "mini&#281;cie"
  ]
  node [
    id 748
    label "ustawa"
  ]
  node [
    id 749
    label "wymienienie"
  ]
  node [
    id 750
    label "zaliczenie"
  ]
  node [
    id 751
    label "traversal"
  ]
  node [
    id 752
    label "zdarzenie_si&#281;"
  ]
  node [
    id 753
    label "przewy&#380;szenie"
  ]
  node [
    id 754
    label "experience"
  ]
  node [
    id 755
    label "przepuszczenie"
  ]
  node [
    id 756
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 757
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 758
    label "strain"
  ]
  node [
    id 759
    label "faza"
  ]
  node [
    id 760
    label "przerobienie"
  ]
  node [
    id 761
    label "wydeptywanie"
  ]
  node [
    id 762
    label "crack"
  ]
  node [
    id 763
    label "wydeptanie"
  ]
  node [
    id 764
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 765
    label "wstawka"
  ]
  node [
    id 766
    label "prze&#380;ycie"
  ]
  node [
    id 767
    label "uznanie"
  ]
  node [
    id 768
    label "doznanie"
  ]
  node [
    id 769
    label "dostanie_si&#281;"
  ]
  node [
    id 770
    label "trwanie"
  ]
  node [
    id 771
    label "przebycie"
  ]
  node [
    id 772
    label "wytyczenie"
  ]
  node [
    id 773
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 774
    label "przepojenie"
  ]
  node [
    id 775
    label "nas&#261;czenie"
  ]
  node [
    id 776
    label "nale&#380;enie"
  ]
  node [
    id 777
    label "mienie"
  ]
  node [
    id 778
    label "odmienienie"
  ]
  node [
    id 779
    label "przedostanie_si&#281;"
  ]
  node [
    id 780
    label "przemokni&#281;cie"
  ]
  node [
    id 781
    label "nasycenie_si&#281;"
  ]
  node [
    id 782
    label "zacz&#281;cie"
  ]
  node [
    id 783
    label "stanie_si&#281;"
  ]
  node [
    id 784
    label "offense"
  ]
  node [
    id 785
    label "przestanie"
  ]
  node [
    id 786
    label "discourtesy"
  ]
  node [
    id 787
    label "odj&#281;cie"
  ]
  node [
    id 788
    label "post&#261;pienie"
  ]
  node [
    id 789
    label "opening"
  ]
  node [
    id 790
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 791
    label "wyra&#380;enie"
  ]
  node [
    id 792
    label "zrobienie"
  ]
  node [
    id 793
    label "conversion"
  ]
  node [
    id 794
    label "podanie"
  ]
  node [
    id 795
    label "exchange"
  ]
  node [
    id 796
    label "spisanie"
  ]
  node [
    id 797
    label "zape&#322;nienie"
  ]
  node [
    id 798
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 799
    label "skontaktowanie_si&#281;"
  ]
  node [
    id 800
    label "policzenie"
  ]
  node [
    id 801
    label "substytuowanie"
  ]
  node [
    id 802
    label "zaatakowanie"
  ]
  node [
    id 803
    label "odbycie"
  ]
  node [
    id 804
    label "theodolite"
  ]
  node [
    id 805
    label "wy&#347;wiadczenie"
  ]
  node [
    id 806
    label "zmys&#322;"
  ]
  node [
    id 807
    label "przeczulica"
  ]
  node [
    id 808
    label "spotkanie"
  ]
  node [
    id 809
    label "czucie"
  ]
  node [
    id 810
    label "poczucie"
  ]
  node [
    id 811
    label "oduczenie"
  ]
  node [
    id 812
    label "disavowal"
  ]
  node [
    id 813
    label "zako&#324;czenie"
  ]
  node [
    id 814
    label "cessation"
  ]
  node [
    id 815
    label "przeczekanie"
  ]
  node [
    id 816
    label "spe&#322;nienie"
  ]
  node [
    id 817
    label "wliczenie"
  ]
  node [
    id 818
    label "zaliczanie"
  ]
  node [
    id 819
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 820
    label "zadanie"
  ]
  node [
    id 821
    label "odb&#281;bnienie"
  ]
  node [
    id 822
    label "ocenienie"
  ]
  node [
    id 823
    label "number"
  ]
  node [
    id 824
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 825
    label "przeklasyfikowanie"
  ]
  node [
    id 826
    label "zaliczanie_si&#281;"
  ]
  node [
    id 827
    label "wzi&#281;cie"
  ]
  node [
    id 828
    label "warunek_lokalowy"
  ]
  node [
    id 829
    label "plac"
  ]
  node [
    id 830
    label "location"
  ]
  node [
    id 831
    label "uwaga"
  ]
  node [
    id 832
    label "przestrze&#324;"
  ]
  node [
    id 833
    label "status"
  ]
  node [
    id 834
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 835
    label "chwila"
  ]
  node [
    id 836
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 837
    label "rz&#261;d"
  ]
  node [
    id 838
    label "dodatek"
  ]
  node [
    id 839
    label "tekst"
  ]
  node [
    id 840
    label "cykl_astronomiczny"
  ]
  node [
    id 841
    label "coil"
  ]
  node [
    id 842
    label "fotoelement"
  ]
  node [
    id 843
    label "komutowanie"
  ]
  node [
    id 844
    label "stan_skupienia"
  ]
  node [
    id 845
    label "nastr&#243;j"
  ]
  node [
    id 846
    label "przerywacz"
  ]
  node [
    id 847
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 848
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 849
    label "kraw&#281;d&#378;"
  ]
  node [
    id 850
    label "obsesja"
  ]
  node [
    id 851
    label "dw&#243;jnik"
  ]
  node [
    id 852
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 853
    label "okres"
  ]
  node [
    id 854
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 855
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 856
    label "przew&#243;d"
  ]
  node [
    id 857
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 858
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 859
    label "obw&#243;d"
  ]
  node [
    id 860
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 861
    label "degree"
  ]
  node [
    id 862
    label "komutowa&#263;"
  ]
  node [
    id 863
    label "wra&#380;enie"
  ]
  node [
    id 864
    label "poradzenie_sobie"
  ]
  node [
    id 865
    label "przetrwanie"
  ]
  node [
    id 866
    label "survival"
  ]
  node [
    id 867
    label "battery"
  ]
  node [
    id 868
    label "wygranie"
  ]
  node [
    id 869
    label "lepszy"
  ]
  node [
    id 870
    label "breakdown"
  ]
  node [
    id 871
    label "gap"
  ]
  node [
    id 872
    label "kokaina"
  ]
  node [
    id 873
    label "program"
  ]
  node [
    id 874
    label "po&#322;o&#380;enie"
  ]
  node [
    id 875
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 876
    label "rodowo&#347;&#263;"
  ]
  node [
    id 877
    label "patent"
  ]
  node [
    id 878
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 879
    label "dobra"
  ]
  node [
    id 880
    label "stan"
  ]
  node [
    id 881
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 882
    label "przej&#347;&#263;"
  ]
  node [
    id 883
    label "possession"
  ]
  node [
    id 884
    label "organizacyjnie"
  ]
  node [
    id 885
    label "zwi&#261;zek"
  ]
  node [
    id 886
    label "przyjmowanie"
  ]
  node [
    id 887
    label "przechodzenie"
  ]
  node [
    id 888
    label "wpuszczenie"
  ]
  node [
    id 889
    label "puszczenie"
  ]
  node [
    id 890
    label "przeoczenie"
  ]
  node [
    id 891
    label "obrobienie"
  ]
  node [
    id 892
    label "oddzia&#322;anie"
  ]
  node [
    id 893
    label "spowodowanie"
  ]
  node [
    id 894
    label "ust&#261;pienie"
  ]
  node [
    id 895
    label "darowanie"
  ]
  node [
    id 896
    label "przenikni&#281;cie"
  ]
  node [
    id 897
    label "roztrwonienie"
  ]
  node [
    id 898
    label "przekroczenie"
  ]
  node [
    id 899
    label "zaistnienie"
  ]
  node [
    id 900
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 901
    label "cruise"
  ]
  node [
    id 902
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 903
    label "przep&#322;ywanie"
  ]
  node [
    id 904
    label "wlanie"
  ]
  node [
    id 905
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 906
    label "nasycenie"
  ]
  node [
    id 907
    label "przesycenie"
  ]
  node [
    id 908
    label "saturation"
  ]
  node [
    id 909
    label "impregnation"
  ]
  node [
    id 910
    label "opanowanie"
  ]
  node [
    id 911
    label "zmoczenie"
  ]
  node [
    id 912
    label "nadanie"
  ]
  node [
    id 913
    label "mokry"
  ]
  node [
    id 914
    label "zmokni&#281;cie"
  ]
  node [
    id 915
    label "bycie"
  ]
  node [
    id 916
    label "upieranie_si&#281;"
  ]
  node [
    id 917
    label "pozostawanie"
  ]
  node [
    id 918
    label "presence"
  ]
  node [
    id 919
    label "imperativeness"
  ]
  node [
    id 920
    label "standing"
  ]
  node [
    id 921
    label "zostawanie"
  ]
  node [
    id 922
    label "wytkni&#281;cie"
  ]
  node [
    id 923
    label "ustalenie"
  ]
  node [
    id 924
    label "wyznaczenie"
  ]
  node [
    id 925
    label "trace"
  ]
  node [
    id 926
    label "przeprowadzenie"
  ]
  node [
    id 927
    label "zniszczenie"
  ]
  node [
    id 928
    label "egress"
  ]
  node [
    id 929
    label "ukszta&#322;towanie"
  ]
  node [
    id 930
    label "skombinowanie"
  ]
  node [
    id 931
    label "niszczenie"
  ]
  node [
    id 932
    label "kszta&#322;towanie"
  ]
  node [
    id 933
    label "pozyskiwanie"
  ]
  node [
    id 934
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 935
    label "change"
  ]
  node [
    id 936
    label "reengineering"
  ]
  node [
    id 937
    label "zmienienie"
  ]
  node [
    id 938
    label "przeformu&#322;owanie"
  ]
  node [
    id 939
    label "przeformu&#322;owywanie"
  ]
  node [
    id 940
    label "Karta_Nauczyciela"
  ]
  node [
    id 941
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 942
    label "akt"
  ]
  node [
    id 943
    label "charter"
  ]
  node [
    id 944
    label "marc&#243;wka"
  ]
  node [
    id 945
    label "zaimponowanie"
  ]
  node [
    id 946
    label "honorowanie"
  ]
  node [
    id 947
    label "uszanowanie"
  ]
  node [
    id 948
    label "uhonorowa&#263;"
  ]
  node [
    id 949
    label "oznajmienie"
  ]
  node [
    id 950
    label "imponowanie"
  ]
  node [
    id 951
    label "uhonorowanie"
  ]
  node [
    id 952
    label "honorowa&#263;"
  ]
  node [
    id 953
    label "uszanowa&#263;"
  ]
  node [
    id 954
    label "mniemanie"
  ]
  node [
    id 955
    label "szacuneczek"
  ]
  node [
    id 956
    label "recognition"
  ]
  node [
    id 957
    label "rewerencja"
  ]
  node [
    id 958
    label "szanowa&#263;"
  ]
  node [
    id 959
    label "postawa"
  ]
  node [
    id 960
    label "acclaim"
  ]
  node [
    id 961
    label "zachwyt"
  ]
  node [
    id 962
    label "respect"
  ]
  node [
    id 963
    label "fame"
  ]
  node [
    id 964
    label "rework"
  ]
  node [
    id 965
    label "zg&#322;&#281;bienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 9
    target 896
  ]
  edge [
    source 9
    target 897
  ]
  edge [
    source 9
    target 898
  ]
  edge [
    source 9
    target 899
  ]
  edge [
    source 9
    target 900
  ]
  edge [
    source 9
    target 901
  ]
  edge [
    source 9
    target 902
  ]
  edge [
    source 9
    target 903
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 9
    target 906
  ]
  edge [
    source 9
    target 907
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 909
  ]
  edge [
    source 9
    target 910
  ]
  edge [
    source 9
    target 911
  ]
  edge [
    source 9
    target 912
  ]
  edge [
    source 9
    target 913
  ]
  edge [
    source 9
    target 914
  ]
  edge [
    source 9
    target 915
  ]
  edge [
    source 9
    target 916
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 942
  ]
  edge [
    source 9
    target 943
  ]
  edge [
    source 9
    target 944
  ]
  edge [
    source 9
    target 945
  ]
  edge [
    source 9
    target 946
  ]
  edge [
    source 9
    target 947
  ]
  edge [
    source 9
    target 948
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 9
    target 952
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 9
    target 962
  ]
  edge [
    source 9
    target 963
  ]
  edge [
    source 9
    target 964
  ]
  edge [
    source 9
    target 965
  ]
]
