graph [
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "krystyn"
    origin "text"
  ]
  node [
    id 2
    label "skowro&#324;ska"
    origin "text"
  ]
  node [
    id 3
    label "ablegat"
  ]
  node [
    id 4
    label "izba_ni&#380;sza"
  ]
  node [
    id 5
    label "Korwin"
  ]
  node [
    id 6
    label "dyscyplina_partyjna"
  ]
  node [
    id 7
    label "Miko&#322;ajczyk"
  ]
  node [
    id 8
    label "kurier_dyplomatyczny"
  ]
  node [
    id 9
    label "wys&#322;annik"
  ]
  node [
    id 10
    label "poselstwo"
  ]
  node [
    id 11
    label "parlamentarzysta"
  ]
  node [
    id 12
    label "przedstawiciel"
  ]
  node [
    id 13
    label "dyplomata"
  ]
  node [
    id 14
    label "klubista"
  ]
  node [
    id 15
    label "reprezentacja"
  ]
  node [
    id 16
    label "klub"
  ]
  node [
    id 17
    label "cz&#322;onek"
  ]
  node [
    id 18
    label "mandatariusz"
  ]
  node [
    id 19
    label "grupa_bilateralna"
  ]
  node [
    id 20
    label "polityk"
  ]
  node [
    id 21
    label "parlament"
  ]
  node [
    id 22
    label "mi&#322;y"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "korpus_dyplomatyczny"
  ]
  node [
    id 25
    label "dyplomatyczny"
  ]
  node [
    id 26
    label "takt"
  ]
  node [
    id 27
    label "Metternich"
  ]
  node [
    id 28
    label "dostojnik"
  ]
  node [
    id 29
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 30
    label "przyk&#322;ad"
  ]
  node [
    id 31
    label "substytuowa&#263;"
  ]
  node [
    id 32
    label "substytuowanie"
  ]
  node [
    id 33
    label "zast&#281;pca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
]
