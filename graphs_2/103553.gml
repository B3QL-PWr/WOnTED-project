graph [
  node [
    id 0
    label "pieg"
    origin "text"
  ]
  node [
    id 1
    label "plamka"
  ]
  node [
    id 2
    label "pieprzyk"
  ]
  node [
    id 3
    label "brodawka"
  ]
  node [
    id 4
    label "przyprawa"
  ]
  node [
    id 5
    label "cecha"
  ]
  node [
    id 6
    label "ziele"
  ]
  node [
    id 7
    label "zio&#322;o"
  ]
  node [
    id 8
    label "znami&#281;"
  ]
  node [
    id 9
    label "nieprzyzwoito&#347;&#263;"
  ]
  node [
    id 10
    label "sk&#243;ra"
  ]
  node [
    id 11
    label "rak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
]
