graph [
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "granica"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "na_przyk&#322;ad"
    origin "text"
  ]
  node [
    id 5
    label "wynik"
    origin "text"
  ]
  node [
    id 6
    label "rozw&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "rodzic"
    origin "text"
  ]
  node [
    id 8
    label "umieszcza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "rodzina"
    origin "text"
  ]
  node [
    id 11
    label "zast&#281;pczy"
    origin "text"
  ]
  node [
    id 12
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 13
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 15
    label "ten"
    origin "text"
  ]
  node [
    id 16
    label "sam"
    origin "text"
  ]
  node [
    id 17
    label "to&#380;samo&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "kulturowy"
    origin "text"
  ]
  node [
    id 19
    label "przedmiot"
  ]
  node [
    id 20
    label "Polish"
  ]
  node [
    id 21
    label "goniony"
  ]
  node [
    id 22
    label "oberek"
  ]
  node [
    id 23
    label "ryba_po_grecku"
  ]
  node [
    id 24
    label "sztajer"
  ]
  node [
    id 25
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 26
    label "krakowiak"
  ]
  node [
    id 27
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 28
    label "pierogi_ruskie"
  ]
  node [
    id 29
    label "lacki"
  ]
  node [
    id 30
    label "polak"
  ]
  node [
    id 31
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 32
    label "chodzony"
  ]
  node [
    id 33
    label "po_polsku"
  ]
  node [
    id 34
    label "mazur"
  ]
  node [
    id 35
    label "polsko"
  ]
  node [
    id 36
    label "skoczny"
  ]
  node [
    id 37
    label "drabant"
  ]
  node [
    id 38
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 39
    label "j&#281;zyk"
  ]
  node [
    id 40
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 41
    label "artykulator"
  ]
  node [
    id 42
    label "kod"
  ]
  node [
    id 43
    label "kawa&#322;ek"
  ]
  node [
    id 44
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 45
    label "gramatyka"
  ]
  node [
    id 46
    label "stylik"
  ]
  node [
    id 47
    label "przet&#322;umaczenie"
  ]
  node [
    id 48
    label "formalizowanie"
  ]
  node [
    id 49
    label "ssa&#263;"
  ]
  node [
    id 50
    label "ssanie"
  ]
  node [
    id 51
    label "language"
  ]
  node [
    id 52
    label "liza&#263;"
  ]
  node [
    id 53
    label "napisa&#263;"
  ]
  node [
    id 54
    label "konsonantyzm"
  ]
  node [
    id 55
    label "wokalizm"
  ]
  node [
    id 56
    label "pisa&#263;"
  ]
  node [
    id 57
    label "fonetyka"
  ]
  node [
    id 58
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 59
    label "jeniec"
  ]
  node [
    id 60
    label "but"
  ]
  node [
    id 61
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 62
    label "po_koroniarsku"
  ]
  node [
    id 63
    label "kultura_duchowa"
  ]
  node [
    id 64
    label "t&#322;umaczenie"
  ]
  node [
    id 65
    label "m&#243;wienie"
  ]
  node [
    id 66
    label "pype&#263;"
  ]
  node [
    id 67
    label "lizanie"
  ]
  node [
    id 68
    label "pismo"
  ]
  node [
    id 69
    label "formalizowa&#263;"
  ]
  node [
    id 70
    label "rozumie&#263;"
  ]
  node [
    id 71
    label "organ"
  ]
  node [
    id 72
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 73
    label "rozumienie"
  ]
  node [
    id 74
    label "spos&#243;b"
  ]
  node [
    id 75
    label "makroglosja"
  ]
  node [
    id 76
    label "m&#243;wi&#263;"
  ]
  node [
    id 77
    label "jama_ustna"
  ]
  node [
    id 78
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 79
    label "formacja_geologiczna"
  ]
  node [
    id 80
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 81
    label "natural_language"
  ]
  node [
    id 82
    label "s&#322;ownictwo"
  ]
  node [
    id 83
    label "urz&#261;dzenie"
  ]
  node [
    id 84
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 85
    label "wschodnioeuropejski"
  ]
  node [
    id 86
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 87
    label "poga&#324;ski"
  ]
  node [
    id 88
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 89
    label "topielec"
  ]
  node [
    id 90
    label "europejski"
  ]
  node [
    id 91
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 92
    label "langosz"
  ]
  node [
    id 93
    label "zboczenie"
  ]
  node [
    id 94
    label "om&#243;wienie"
  ]
  node [
    id 95
    label "sponiewieranie"
  ]
  node [
    id 96
    label "discipline"
  ]
  node [
    id 97
    label "rzecz"
  ]
  node [
    id 98
    label "omawia&#263;"
  ]
  node [
    id 99
    label "kr&#261;&#380;enie"
  ]
  node [
    id 100
    label "tre&#347;&#263;"
  ]
  node [
    id 101
    label "robienie"
  ]
  node [
    id 102
    label "sponiewiera&#263;"
  ]
  node [
    id 103
    label "element"
  ]
  node [
    id 104
    label "entity"
  ]
  node [
    id 105
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 106
    label "tematyka"
  ]
  node [
    id 107
    label "w&#261;tek"
  ]
  node [
    id 108
    label "charakter"
  ]
  node [
    id 109
    label "zbaczanie"
  ]
  node [
    id 110
    label "program_nauczania"
  ]
  node [
    id 111
    label "om&#243;wi&#263;"
  ]
  node [
    id 112
    label "omawianie"
  ]
  node [
    id 113
    label "thing"
  ]
  node [
    id 114
    label "kultura"
  ]
  node [
    id 115
    label "istota"
  ]
  node [
    id 116
    label "zbacza&#263;"
  ]
  node [
    id 117
    label "zboczy&#263;"
  ]
  node [
    id 118
    label "gwardzista"
  ]
  node [
    id 119
    label "melodia"
  ]
  node [
    id 120
    label "taniec"
  ]
  node [
    id 121
    label "taniec_ludowy"
  ]
  node [
    id 122
    label "&#347;redniowieczny"
  ]
  node [
    id 123
    label "europejsko"
  ]
  node [
    id 124
    label "specjalny"
  ]
  node [
    id 125
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 126
    label "weso&#322;y"
  ]
  node [
    id 127
    label "sprawny"
  ]
  node [
    id 128
    label "rytmiczny"
  ]
  node [
    id 129
    label "skocznie"
  ]
  node [
    id 130
    label "energiczny"
  ]
  node [
    id 131
    label "przytup"
  ]
  node [
    id 132
    label "ho&#322;ubiec"
  ]
  node [
    id 133
    label "wodzi&#263;"
  ]
  node [
    id 134
    label "lendler"
  ]
  node [
    id 135
    label "austriacki"
  ]
  node [
    id 136
    label "polka"
  ]
  node [
    id 137
    label "ludowy"
  ]
  node [
    id 138
    label "pie&#347;&#324;"
  ]
  node [
    id 139
    label "mieszkaniec"
  ]
  node [
    id 140
    label "centu&#347;"
  ]
  node [
    id 141
    label "lalka"
  ]
  node [
    id 142
    label "Ma&#322;opolanin"
  ]
  node [
    id 143
    label "krakauer"
  ]
  node [
    id 144
    label "utulenie"
  ]
  node [
    id 145
    label "pediatra"
  ]
  node [
    id 146
    label "dzieciak"
  ]
  node [
    id 147
    label "utulanie"
  ]
  node [
    id 148
    label "dzieciarnia"
  ]
  node [
    id 149
    label "cz&#322;owiek"
  ]
  node [
    id 150
    label "niepe&#322;noletni"
  ]
  node [
    id 151
    label "organizm"
  ]
  node [
    id 152
    label "utula&#263;"
  ]
  node [
    id 153
    label "cz&#322;owieczek"
  ]
  node [
    id 154
    label "fledgling"
  ]
  node [
    id 155
    label "zwierz&#281;"
  ]
  node [
    id 156
    label "utuli&#263;"
  ]
  node [
    id 157
    label "m&#322;odzik"
  ]
  node [
    id 158
    label "pedofil"
  ]
  node [
    id 159
    label "m&#322;odziak"
  ]
  node [
    id 160
    label "potomek"
  ]
  node [
    id 161
    label "entliczek-pentliczek"
  ]
  node [
    id 162
    label "potomstwo"
  ]
  node [
    id 163
    label "sraluch"
  ]
  node [
    id 164
    label "zbi&#243;r"
  ]
  node [
    id 165
    label "czeladka"
  ]
  node [
    id 166
    label "dzietno&#347;&#263;"
  ]
  node [
    id 167
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 168
    label "bawienie_si&#281;"
  ]
  node [
    id 169
    label "pomiot"
  ]
  node [
    id 170
    label "grupa"
  ]
  node [
    id 171
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 172
    label "kinderbal"
  ]
  node [
    id 173
    label "krewny"
  ]
  node [
    id 174
    label "ludzko&#347;&#263;"
  ]
  node [
    id 175
    label "asymilowanie"
  ]
  node [
    id 176
    label "wapniak"
  ]
  node [
    id 177
    label "asymilowa&#263;"
  ]
  node [
    id 178
    label "os&#322;abia&#263;"
  ]
  node [
    id 179
    label "posta&#263;"
  ]
  node [
    id 180
    label "hominid"
  ]
  node [
    id 181
    label "podw&#322;adny"
  ]
  node [
    id 182
    label "os&#322;abianie"
  ]
  node [
    id 183
    label "g&#322;owa"
  ]
  node [
    id 184
    label "figura"
  ]
  node [
    id 185
    label "portrecista"
  ]
  node [
    id 186
    label "dwun&#243;g"
  ]
  node [
    id 187
    label "profanum"
  ]
  node [
    id 188
    label "mikrokosmos"
  ]
  node [
    id 189
    label "nasada"
  ]
  node [
    id 190
    label "duch"
  ]
  node [
    id 191
    label "antropochoria"
  ]
  node [
    id 192
    label "osoba"
  ]
  node [
    id 193
    label "wz&#243;r"
  ]
  node [
    id 194
    label "senior"
  ]
  node [
    id 195
    label "oddzia&#322;ywanie"
  ]
  node [
    id 196
    label "Adam"
  ]
  node [
    id 197
    label "homo_sapiens"
  ]
  node [
    id 198
    label "polifag"
  ]
  node [
    id 199
    label "ma&#322;oletny"
  ]
  node [
    id 200
    label "m&#322;ody"
  ]
  node [
    id 201
    label "p&#322;aszczyzna"
  ]
  node [
    id 202
    label "odwadnia&#263;"
  ]
  node [
    id 203
    label "przyswoi&#263;"
  ]
  node [
    id 204
    label "sk&#243;ra"
  ]
  node [
    id 205
    label "odwodni&#263;"
  ]
  node [
    id 206
    label "ewoluowanie"
  ]
  node [
    id 207
    label "staw"
  ]
  node [
    id 208
    label "ow&#322;osienie"
  ]
  node [
    id 209
    label "unerwienie"
  ]
  node [
    id 210
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 211
    label "reakcja"
  ]
  node [
    id 212
    label "wyewoluowanie"
  ]
  node [
    id 213
    label "przyswajanie"
  ]
  node [
    id 214
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 215
    label "wyewoluowa&#263;"
  ]
  node [
    id 216
    label "miejsce"
  ]
  node [
    id 217
    label "biorytm"
  ]
  node [
    id 218
    label "ewoluowa&#263;"
  ]
  node [
    id 219
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 220
    label "istota_&#380;ywa"
  ]
  node [
    id 221
    label "otworzy&#263;"
  ]
  node [
    id 222
    label "otwiera&#263;"
  ]
  node [
    id 223
    label "czynnik_biotyczny"
  ]
  node [
    id 224
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 225
    label "otworzenie"
  ]
  node [
    id 226
    label "otwieranie"
  ]
  node [
    id 227
    label "individual"
  ]
  node [
    id 228
    label "szkielet"
  ]
  node [
    id 229
    label "ty&#322;"
  ]
  node [
    id 230
    label "obiekt"
  ]
  node [
    id 231
    label "przyswaja&#263;"
  ]
  node [
    id 232
    label "przyswojenie"
  ]
  node [
    id 233
    label "odwadnianie"
  ]
  node [
    id 234
    label "odwodnienie"
  ]
  node [
    id 235
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 236
    label "starzenie_si&#281;"
  ]
  node [
    id 237
    label "prz&#243;d"
  ]
  node [
    id 238
    label "uk&#322;ad"
  ]
  node [
    id 239
    label "temperatura"
  ]
  node [
    id 240
    label "l&#281;d&#378;wie"
  ]
  node [
    id 241
    label "cia&#322;o"
  ]
  node [
    id 242
    label "cz&#322;onek"
  ]
  node [
    id 243
    label "degenerat"
  ]
  node [
    id 244
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 245
    label "zwyrol"
  ]
  node [
    id 246
    label "czerniak"
  ]
  node [
    id 247
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 248
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 249
    label "paszcza"
  ]
  node [
    id 250
    label "popapraniec"
  ]
  node [
    id 251
    label "skuba&#263;"
  ]
  node [
    id 252
    label "skubanie"
  ]
  node [
    id 253
    label "skubni&#281;cie"
  ]
  node [
    id 254
    label "agresja"
  ]
  node [
    id 255
    label "zwierz&#281;ta"
  ]
  node [
    id 256
    label "fukni&#281;cie"
  ]
  node [
    id 257
    label "farba"
  ]
  node [
    id 258
    label "fukanie"
  ]
  node [
    id 259
    label "gad"
  ]
  node [
    id 260
    label "siedzie&#263;"
  ]
  node [
    id 261
    label "oswaja&#263;"
  ]
  node [
    id 262
    label "tresowa&#263;"
  ]
  node [
    id 263
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 264
    label "poligamia"
  ]
  node [
    id 265
    label "oz&#243;r"
  ]
  node [
    id 266
    label "skubn&#261;&#263;"
  ]
  node [
    id 267
    label "wios&#322;owa&#263;"
  ]
  node [
    id 268
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 269
    label "le&#380;enie"
  ]
  node [
    id 270
    label "niecz&#322;owiek"
  ]
  node [
    id 271
    label "wios&#322;owanie"
  ]
  node [
    id 272
    label "napasienie_si&#281;"
  ]
  node [
    id 273
    label "wiwarium"
  ]
  node [
    id 274
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 275
    label "animalista"
  ]
  node [
    id 276
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 277
    label "budowa"
  ]
  node [
    id 278
    label "hodowla"
  ]
  node [
    id 279
    label "pasienie_si&#281;"
  ]
  node [
    id 280
    label "sodomita"
  ]
  node [
    id 281
    label "monogamia"
  ]
  node [
    id 282
    label "przyssawka"
  ]
  node [
    id 283
    label "zachowanie"
  ]
  node [
    id 284
    label "budowa_cia&#322;a"
  ]
  node [
    id 285
    label "okrutnik"
  ]
  node [
    id 286
    label "grzbiet"
  ]
  node [
    id 287
    label "weterynarz"
  ]
  node [
    id 288
    label "&#322;eb"
  ]
  node [
    id 289
    label "wylinka"
  ]
  node [
    id 290
    label "bestia"
  ]
  node [
    id 291
    label "poskramia&#263;"
  ]
  node [
    id 292
    label "fauna"
  ]
  node [
    id 293
    label "treser"
  ]
  node [
    id 294
    label "siedzenie"
  ]
  node [
    id 295
    label "le&#380;e&#263;"
  ]
  node [
    id 296
    label "uspokojenie"
  ]
  node [
    id 297
    label "utulenie_si&#281;"
  ]
  node [
    id 298
    label "u&#347;pienie"
  ]
  node [
    id 299
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 300
    label "uspokoi&#263;"
  ]
  node [
    id 301
    label "utulanie_si&#281;"
  ]
  node [
    id 302
    label "usypianie"
  ]
  node [
    id 303
    label "pocieszanie"
  ]
  node [
    id 304
    label "uspokajanie"
  ]
  node [
    id 305
    label "usypia&#263;"
  ]
  node [
    id 306
    label "uspokaja&#263;"
  ]
  node [
    id 307
    label "wyliczanka"
  ]
  node [
    id 308
    label "specjalista"
  ]
  node [
    id 309
    label "harcerz"
  ]
  node [
    id 310
    label "ch&#322;opta&#347;"
  ]
  node [
    id 311
    label "zawodnik"
  ]
  node [
    id 312
    label "go&#322;ow&#261;s"
  ]
  node [
    id 313
    label "m&#322;ode"
  ]
  node [
    id 314
    label "stopie&#324;_harcerski"
  ]
  node [
    id 315
    label "g&#243;wniarz"
  ]
  node [
    id 316
    label "beniaminek"
  ]
  node [
    id 317
    label "dewiant"
  ]
  node [
    id 318
    label "istotka"
  ]
  node [
    id 319
    label "bech"
  ]
  node [
    id 320
    label "dziecinny"
  ]
  node [
    id 321
    label "naiwniak"
  ]
  node [
    id 322
    label "przej&#347;cie"
  ]
  node [
    id 323
    label "zakres"
  ]
  node [
    id 324
    label "kres"
  ]
  node [
    id 325
    label "granica_pa&#324;stwa"
  ]
  node [
    id 326
    label "Ural"
  ]
  node [
    id 327
    label "miara"
  ]
  node [
    id 328
    label "poj&#281;cie"
  ]
  node [
    id 329
    label "end"
  ]
  node [
    id 330
    label "pu&#322;ap"
  ]
  node [
    id 331
    label "koniec"
  ]
  node [
    id 332
    label "granice"
  ]
  node [
    id 333
    label "frontier"
  ]
  node [
    id 334
    label "mini&#281;cie"
  ]
  node [
    id 335
    label "ustawa"
  ]
  node [
    id 336
    label "wymienienie"
  ]
  node [
    id 337
    label "zaliczenie"
  ]
  node [
    id 338
    label "traversal"
  ]
  node [
    id 339
    label "zdarzenie_si&#281;"
  ]
  node [
    id 340
    label "przewy&#380;szenie"
  ]
  node [
    id 341
    label "experience"
  ]
  node [
    id 342
    label "przepuszczenie"
  ]
  node [
    id 343
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 344
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 345
    label "strain"
  ]
  node [
    id 346
    label "faza"
  ]
  node [
    id 347
    label "przerobienie"
  ]
  node [
    id 348
    label "wydeptywanie"
  ]
  node [
    id 349
    label "crack"
  ]
  node [
    id 350
    label "wydeptanie"
  ]
  node [
    id 351
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 352
    label "wstawka"
  ]
  node [
    id 353
    label "prze&#380;ycie"
  ]
  node [
    id 354
    label "uznanie"
  ]
  node [
    id 355
    label "doznanie"
  ]
  node [
    id 356
    label "dostanie_si&#281;"
  ]
  node [
    id 357
    label "trwanie"
  ]
  node [
    id 358
    label "przebycie"
  ]
  node [
    id 359
    label "wytyczenie"
  ]
  node [
    id 360
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 361
    label "przepojenie"
  ]
  node [
    id 362
    label "nas&#261;czenie"
  ]
  node [
    id 363
    label "nale&#380;enie"
  ]
  node [
    id 364
    label "mienie"
  ]
  node [
    id 365
    label "odmienienie"
  ]
  node [
    id 366
    label "przedostanie_si&#281;"
  ]
  node [
    id 367
    label "przemokni&#281;cie"
  ]
  node [
    id 368
    label "nasycenie_si&#281;"
  ]
  node [
    id 369
    label "zacz&#281;cie"
  ]
  node [
    id 370
    label "stanie_si&#281;"
  ]
  node [
    id 371
    label "offense"
  ]
  node [
    id 372
    label "przestanie"
  ]
  node [
    id 373
    label "pos&#322;uchanie"
  ]
  node [
    id 374
    label "skumanie"
  ]
  node [
    id 375
    label "orientacja"
  ]
  node [
    id 376
    label "wytw&#243;r"
  ]
  node [
    id 377
    label "zorientowanie"
  ]
  node [
    id 378
    label "teoria"
  ]
  node [
    id 379
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 380
    label "clasp"
  ]
  node [
    id 381
    label "forma"
  ]
  node [
    id 382
    label "przem&#243;wienie"
  ]
  node [
    id 383
    label "strop"
  ]
  node [
    id 384
    label "poziom"
  ]
  node [
    id 385
    label "powa&#322;a"
  ]
  node [
    id 386
    label "wysoko&#347;&#263;"
  ]
  node [
    id 387
    label "ostatnie_podrygi"
  ]
  node [
    id 388
    label "punkt"
  ]
  node [
    id 389
    label "dzia&#322;anie"
  ]
  node [
    id 390
    label "chwila"
  ]
  node [
    id 391
    label "visitation"
  ]
  node [
    id 392
    label "agonia"
  ]
  node [
    id 393
    label "defenestracja"
  ]
  node [
    id 394
    label "wydarzenie"
  ]
  node [
    id 395
    label "mogi&#322;a"
  ]
  node [
    id 396
    label "kres_&#380;ycia"
  ]
  node [
    id 397
    label "szereg"
  ]
  node [
    id 398
    label "szeol"
  ]
  node [
    id 399
    label "pogrzebanie"
  ]
  node [
    id 400
    label "&#380;a&#322;oba"
  ]
  node [
    id 401
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 402
    label "zabicie"
  ]
  node [
    id 403
    label "obszar"
  ]
  node [
    id 404
    label "proportion"
  ]
  node [
    id 405
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 406
    label "wielko&#347;&#263;"
  ]
  node [
    id 407
    label "continence"
  ]
  node [
    id 408
    label "supremum"
  ]
  node [
    id 409
    label "cecha"
  ]
  node [
    id 410
    label "skala"
  ]
  node [
    id 411
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 412
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 413
    label "jednostka"
  ]
  node [
    id 414
    label "przeliczy&#263;"
  ]
  node [
    id 415
    label "matematyka"
  ]
  node [
    id 416
    label "rzut"
  ]
  node [
    id 417
    label "odwiedziny"
  ]
  node [
    id 418
    label "liczba"
  ]
  node [
    id 419
    label "warunek_lokalowy"
  ]
  node [
    id 420
    label "ilo&#347;&#263;"
  ]
  node [
    id 421
    label "przeliczanie"
  ]
  node [
    id 422
    label "dymensja"
  ]
  node [
    id 423
    label "funkcja"
  ]
  node [
    id 424
    label "przelicza&#263;"
  ]
  node [
    id 425
    label "infimum"
  ]
  node [
    id 426
    label "przeliczenie"
  ]
  node [
    id 427
    label "sfera"
  ]
  node [
    id 428
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 429
    label "podzakres"
  ]
  node [
    id 430
    label "dziedzina"
  ]
  node [
    id 431
    label "desygnat"
  ]
  node [
    id 432
    label "circle"
  ]
  node [
    id 433
    label "Eurazja"
  ]
  node [
    id 434
    label "zaokr&#261;glenie"
  ]
  node [
    id 435
    label "typ"
  ]
  node [
    id 436
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 437
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 438
    label "rezultat"
  ]
  node [
    id 439
    label "event"
  ]
  node [
    id 440
    label "przyczyna"
  ]
  node [
    id 441
    label "round"
  ]
  node [
    id 442
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 443
    label "zrobi&#263;"
  ]
  node [
    id 444
    label "przybli&#380;enie"
  ]
  node [
    id 445
    label "rounding"
  ]
  node [
    id 446
    label "liczenie"
  ]
  node [
    id 447
    label "zaokr&#261;glenie_si&#281;"
  ]
  node [
    id 448
    label "okr&#261;g&#322;y"
  ]
  node [
    id 449
    label "zaokr&#261;glony"
  ]
  node [
    id 450
    label "ukszta&#322;towanie"
  ]
  node [
    id 451
    label "labializacja"
  ]
  node [
    id 452
    label "czynno&#347;&#263;"
  ]
  node [
    id 453
    label "zrobienie"
  ]
  node [
    id 454
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 455
    label "subject"
  ]
  node [
    id 456
    label "czynnik"
  ]
  node [
    id 457
    label "matuszka"
  ]
  node [
    id 458
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 459
    label "geneza"
  ]
  node [
    id 460
    label "poci&#261;ganie"
  ]
  node [
    id 461
    label "jednostka_systematyczna"
  ]
  node [
    id 462
    label "kr&#243;lestwo"
  ]
  node [
    id 463
    label "autorament"
  ]
  node [
    id 464
    label "variety"
  ]
  node [
    id 465
    label "antycypacja"
  ]
  node [
    id 466
    label "przypuszczenie"
  ]
  node [
    id 467
    label "cynk"
  ]
  node [
    id 468
    label "obstawia&#263;"
  ]
  node [
    id 469
    label "gromada"
  ]
  node [
    id 470
    label "sztuka"
  ]
  node [
    id 471
    label "facet"
  ]
  node [
    id 472
    label "design"
  ]
  node [
    id 473
    label "powodowanie"
  ]
  node [
    id 474
    label "skutek"
  ]
  node [
    id 475
    label "podzia&#322;anie"
  ]
  node [
    id 476
    label "kampania"
  ]
  node [
    id 477
    label "uruchamianie"
  ]
  node [
    id 478
    label "operacja"
  ]
  node [
    id 479
    label "hipnotyzowanie"
  ]
  node [
    id 480
    label "uruchomienie"
  ]
  node [
    id 481
    label "nakr&#281;canie"
  ]
  node [
    id 482
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 483
    label "reakcja_chemiczna"
  ]
  node [
    id 484
    label "tr&#243;jstronny"
  ]
  node [
    id 485
    label "natural_process"
  ]
  node [
    id 486
    label "nakr&#281;cenie"
  ]
  node [
    id 487
    label "zatrzymanie"
  ]
  node [
    id 488
    label "wp&#322;yw"
  ]
  node [
    id 489
    label "podtrzymywanie"
  ]
  node [
    id 490
    label "w&#322;&#261;czanie"
  ]
  node [
    id 491
    label "liczy&#263;"
  ]
  node [
    id 492
    label "operation"
  ]
  node [
    id 493
    label "dzianie_si&#281;"
  ]
  node [
    id 494
    label "zadzia&#322;anie"
  ]
  node [
    id 495
    label "priorytet"
  ]
  node [
    id 496
    label "bycie"
  ]
  node [
    id 497
    label "rozpocz&#281;cie"
  ]
  node [
    id 498
    label "docieranie"
  ]
  node [
    id 499
    label "czynny"
  ]
  node [
    id 500
    label "impact"
  ]
  node [
    id 501
    label "oferta"
  ]
  node [
    id 502
    label "zako&#324;czenie"
  ]
  node [
    id 503
    label "act"
  ]
  node [
    id 504
    label "wdzieranie_si&#281;"
  ]
  node [
    id 505
    label "w&#322;&#261;czenie"
  ]
  node [
    id 506
    label "rozstanie"
  ]
  node [
    id 507
    label "ekspartner"
  ]
  node [
    id 508
    label "rozbita_rodzina"
  ]
  node [
    id 509
    label "uniewa&#380;nienie"
  ]
  node [
    id 510
    label "separation"
  ]
  node [
    id 511
    label "retraction"
  ]
  node [
    id 512
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 513
    label "zerwanie"
  ]
  node [
    id 514
    label "konsekwencja"
  ]
  node [
    id 515
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 516
    label "partner"
  ]
  node [
    id 517
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 518
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 519
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 520
    label "opiekun"
  ]
  node [
    id 521
    label "rodzice"
  ]
  node [
    id 522
    label "rodzic_chrzestny"
  ]
  node [
    id 523
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 524
    label "starzy"
  ]
  node [
    id 525
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 526
    label "pokolenie"
  ]
  node [
    id 527
    label "wapniaki"
  ]
  node [
    id 528
    label "nadzorca"
  ]
  node [
    id 529
    label "funkcjonariusz"
  ]
  node [
    id 530
    label "doros&#322;y"
  ]
  node [
    id 531
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 532
    label "jajko"
  ]
  node [
    id 533
    label "plasowa&#263;"
  ]
  node [
    id 534
    label "umie&#347;ci&#263;"
  ]
  node [
    id 535
    label "robi&#263;"
  ]
  node [
    id 536
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 537
    label "pomieszcza&#263;"
  ]
  node [
    id 538
    label "accommodate"
  ]
  node [
    id 539
    label "zmienia&#263;"
  ]
  node [
    id 540
    label "powodowa&#263;"
  ]
  node [
    id 541
    label "venture"
  ]
  node [
    id 542
    label "wpiernicza&#263;"
  ]
  node [
    id 543
    label "okre&#347;la&#263;"
  ]
  node [
    id 544
    label "decydowa&#263;"
  ]
  node [
    id 545
    label "signify"
  ]
  node [
    id 546
    label "style"
  ]
  node [
    id 547
    label "organizowa&#263;"
  ]
  node [
    id 548
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 549
    label "czyni&#263;"
  ]
  node [
    id 550
    label "give"
  ]
  node [
    id 551
    label "stylizowa&#263;"
  ]
  node [
    id 552
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 553
    label "falowa&#263;"
  ]
  node [
    id 554
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 555
    label "peddle"
  ]
  node [
    id 556
    label "praca"
  ]
  node [
    id 557
    label "wydala&#263;"
  ]
  node [
    id 558
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 559
    label "tentegowa&#263;"
  ]
  node [
    id 560
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 561
    label "urz&#261;dza&#263;"
  ]
  node [
    id 562
    label "oszukiwa&#263;"
  ]
  node [
    id 563
    label "work"
  ]
  node [
    id 564
    label "ukazywa&#263;"
  ]
  node [
    id 565
    label "przerabia&#263;"
  ]
  node [
    id 566
    label "post&#281;powa&#263;"
  ]
  node [
    id 567
    label "traci&#263;"
  ]
  node [
    id 568
    label "alternate"
  ]
  node [
    id 569
    label "change"
  ]
  node [
    id 570
    label "reengineering"
  ]
  node [
    id 571
    label "zast&#281;powa&#263;"
  ]
  node [
    id 572
    label "sprawia&#263;"
  ]
  node [
    id 573
    label "zyskiwa&#263;"
  ]
  node [
    id 574
    label "przechodzi&#263;"
  ]
  node [
    id 575
    label "mie&#263;_miejsce"
  ]
  node [
    id 576
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 577
    label "motywowa&#263;"
  ]
  node [
    id 578
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 579
    label "wpl&#261;tywa&#263;"
  ]
  node [
    id 580
    label "je&#347;&#263;"
  ]
  node [
    id 581
    label "z&#322;o&#347;ci&#263;"
  ]
  node [
    id 582
    label "bi&#263;"
  ]
  node [
    id 583
    label "wpycha&#263;"
  ]
  node [
    id 584
    label "przeci&#261;&#380;a&#263;"
  ]
  node [
    id 585
    label "&#322;adowa&#263;"
  ]
  node [
    id 586
    label "przemieszcza&#263;"
  ]
  node [
    id 587
    label "overcharge"
  ]
  node [
    id 588
    label "overload"
  ]
  node [
    id 589
    label "load"
  ]
  node [
    id 590
    label "przesadza&#263;"
  ]
  node [
    id 591
    label "set"
  ]
  node [
    id 592
    label "put"
  ]
  node [
    id 593
    label "uplasowa&#263;"
  ]
  node [
    id 594
    label "wpierniczy&#263;"
  ]
  node [
    id 595
    label "okre&#347;li&#263;"
  ]
  node [
    id 596
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 597
    label "zmieni&#263;"
  ]
  node [
    id 598
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 599
    label "equal"
  ]
  node [
    id 600
    label "trwa&#263;"
  ]
  node [
    id 601
    label "chodzi&#263;"
  ]
  node [
    id 602
    label "si&#281;ga&#263;"
  ]
  node [
    id 603
    label "stan"
  ]
  node [
    id 604
    label "obecno&#347;&#263;"
  ]
  node [
    id 605
    label "stand"
  ]
  node [
    id 606
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 607
    label "uczestniczy&#263;"
  ]
  node [
    id 608
    label "participate"
  ]
  node [
    id 609
    label "istnie&#263;"
  ]
  node [
    id 610
    label "pozostawa&#263;"
  ]
  node [
    id 611
    label "zostawa&#263;"
  ]
  node [
    id 612
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 613
    label "adhere"
  ]
  node [
    id 614
    label "compass"
  ]
  node [
    id 615
    label "korzysta&#263;"
  ]
  node [
    id 616
    label "appreciation"
  ]
  node [
    id 617
    label "osi&#261;ga&#263;"
  ]
  node [
    id 618
    label "dociera&#263;"
  ]
  node [
    id 619
    label "get"
  ]
  node [
    id 620
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 621
    label "mierzy&#263;"
  ]
  node [
    id 622
    label "u&#380;ywa&#263;"
  ]
  node [
    id 623
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 624
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 625
    label "exsert"
  ]
  node [
    id 626
    label "being"
  ]
  node [
    id 627
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 628
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 629
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 630
    label "p&#322;ywa&#263;"
  ]
  node [
    id 631
    label "run"
  ]
  node [
    id 632
    label "bangla&#263;"
  ]
  node [
    id 633
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 634
    label "przebiega&#263;"
  ]
  node [
    id 635
    label "wk&#322;ada&#263;"
  ]
  node [
    id 636
    label "proceed"
  ]
  node [
    id 637
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 638
    label "carry"
  ]
  node [
    id 639
    label "bywa&#263;"
  ]
  node [
    id 640
    label "dziama&#263;"
  ]
  node [
    id 641
    label "stara&#263;_si&#281;"
  ]
  node [
    id 642
    label "para"
  ]
  node [
    id 643
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 644
    label "str&#243;j"
  ]
  node [
    id 645
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 646
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 647
    label "krok"
  ]
  node [
    id 648
    label "tryb"
  ]
  node [
    id 649
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 650
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 651
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 652
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 653
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 654
    label "continue"
  ]
  node [
    id 655
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 656
    label "Ohio"
  ]
  node [
    id 657
    label "wci&#281;cie"
  ]
  node [
    id 658
    label "Nowy_York"
  ]
  node [
    id 659
    label "warstwa"
  ]
  node [
    id 660
    label "samopoczucie"
  ]
  node [
    id 661
    label "Illinois"
  ]
  node [
    id 662
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 663
    label "state"
  ]
  node [
    id 664
    label "Jukatan"
  ]
  node [
    id 665
    label "Kalifornia"
  ]
  node [
    id 666
    label "Wirginia"
  ]
  node [
    id 667
    label "wektor"
  ]
  node [
    id 668
    label "Teksas"
  ]
  node [
    id 669
    label "Goa"
  ]
  node [
    id 670
    label "Waszyngton"
  ]
  node [
    id 671
    label "Massachusetts"
  ]
  node [
    id 672
    label "Alaska"
  ]
  node [
    id 673
    label "Arakan"
  ]
  node [
    id 674
    label "Hawaje"
  ]
  node [
    id 675
    label "Maryland"
  ]
  node [
    id 676
    label "Michigan"
  ]
  node [
    id 677
    label "Arizona"
  ]
  node [
    id 678
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 679
    label "Georgia"
  ]
  node [
    id 680
    label "Pensylwania"
  ]
  node [
    id 681
    label "shape"
  ]
  node [
    id 682
    label "Luizjana"
  ]
  node [
    id 683
    label "Nowy_Meksyk"
  ]
  node [
    id 684
    label "Alabama"
  ]
  node [
    id 685
    label "Kansas"
  ]
  node [
    id 686
    label "Oregon"
  ]
  node [
    id 687
    label "Floryda"
  ]
  node [
    id 688
    label "Oklahoma"
  ]
  node [
    id 689
    label "jednostka_administracyjna"
  ]
  node [
    id 690
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 691
    label "powinowaci"
  ]
  node [
    id 692
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 693
    label "rodze&#324;stwo"
  ]
  node [
    id 694
    label "krewni"
  ]
  node [
    id 695
    label "Ossoli&#324;scy"
  ]
  node [
    id 696
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 697
    label "theater"
  ]
  node [
    id 698
    label "Soplicowie"
  ]
  node [
    id 699
    label "kin"
  ]
  node [
    id 700
    label "family"
  ]
  node [
    id 701
    label "ordynacja"
  ]
  node [
    id 702
    label "dom_rodzinny"
  ]
  node [
    id 703
    label "Ostrogscy"
  ]
  node [
    id 704
    label "bliscy"
  ]
  node [
    id 705
    label "przyjaciel_domu"
  ]
  node [
    id 706
    label "dom"
  ]
  node [
    id 707
    label "rz&#261;d"
  ]
  node [
    id 708
    label "Firlejowie"
  ]
  node [
    id 709
    label "Kossakowie"
  ]
  node [
    id 710
    label "Czartoryscy"
  ]
  node [
    id 711
    label "Sapiehowie"
  ]
  node [
    id 712
    label "odm&#322;adzanie"
  ]
  node [
    id 713
    label "liga"
  ]
  node [
    id 714
    label "egzemplarz"
  ]
  node [
    id 715
    label "Entuzjastki"
  ]
  node [
    id 716
    label "kompozycja"
  ]
  node [
    id 717
    label "Terranie"
  ]
  node [
    id 718
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 719
    label "category"
  ]
  node [
    id 720
    label "pakiet_klimatyczny"
  ]
  node [
    id 721
    label "oddzia&#322;"
  ]
  node [
    id 722
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 723
    label "cz&#261;steczka"
  ]
  node [
    id 724
    label "stage_set"
  ]
  node [
    id 725
    label "type"
  ]
  node [
    id 726
    label "specgrupa"
  ]
  node [
    id 727
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 728
    label "&#346;wietliki"
  ]
  node [
    id 729
    label "odm&#322;odzenie"
  ]
  node [
    id 730
    label "Eurogrupa"
  ]
  node [
    id 731
    label "odm&#322;adza&#263;"
  ]
  node [
    id 732
    label "harcerze_starsi"
  ]
  node [
    id 733
    label "series"
  ]
  node [
    id 734
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 735
    label "uprawianie"
  ]
  node [
    id 736
    label "praca_rolnicza"
  ]
  node [
    id 737
    label "collection"
  ]
  node [
    id 738
    label "dane"
  ]
  node [
    id 739
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 740
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 741
    label "sum"
  ]
  node [
    id 742
    label "gathering"
  ]
  node [
    id 743
    label "album"
  ]
  node [
    id 744
    label "grono"
  ]
  node [
    id 745
    label "kuzynostwo"
  ]
  node [
    id 746
    label "stan_cywilny"
  ]
  node [
    id 747
    label "matrymonialny"
  ]
  node [
    id 748
    label "lewirat"
  ]
  node [
    id 749
    label "sakrament"
  ]
  node [
    id 750
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 751
    label "zwi&#261;zek"
  ]
  node [
    id 752
    label "partia"
  ]
  node [
    id 753
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 754
    label "substancja_mieszkaniowa"
  ]
  node [
    id 755
    label "instytucja"
  ]
  node [
    id 756
    label "siedziba"
  ]
  node [
    id 757
    label "budynek"
  ]
  node [
    id 758
    label "stead"
  ]
  node [
    id 759
    label "garderoba"
  ]
  node [
    id 760
    label "wiecha"
  ]
  node [
    id 761
    label "fratria"
  ]
  node [
    id 762
    label "maj&#261;tek_ziemski"
  ]
  node [
    id 763
    label "obrz&#281;d"
  ]
  node [
    id 764
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 765
    label "kategoria"
  ]
  node [
    id 766
    label "szpaler"
  ]
  node [
    id 767
    label "lon&#380;a"
  ]
  node [
    id 768
    label "uporz&#261;dkowanie"
  ]
  node [
    id 769
    label "egzekutywa"
  ]
  node [
    id 770
    label "premier"
  ]
  node [
    id 771
    label "Londyn"
  ]
  node [
    id 772
    label "gabinet_cieni"
  ]
  node [
    id 773
    label "number"
  ]
  node [
    id 774
    label "Konsulat"
  ]
  node [
    id 775
    label "tract"
  ]
  node [
    id 776
    label "klasa"
  ]
  node [
    id 777
    label "w&#322;adza"
  ]
  node [
    id 778
    label "folk_music"
  ]
  node [
    id 779
    label "drugi"
  ]
  node [
    id 780
    label "zast&#281;pczo"
  ]
  node [
    id 781
    label "przydatny"
  ]
  node [
    id 782
    label "potrzebny"
  ]
  node [
    id 783
    label "po&#380;&#261;dany"
  ]
  node [
    id 784
    label "przydatnie"
  ]
  node [
    id 785
    label "kolejny"
  ]
  node [
    id 786
    label "sw&#243;j"
  ]
  node [
    id 787
    label "przeciwny"
  ]
  node [
    id 788
    label "wt&#243;ry"
  ]
  node [
    id 789
    label "dzie&#324;"
  ]
  node [
    id 790
    label "inny"
  ]
  node [
    id 791
    label "odwrotnie"
  ]
  node [
    id 792
    label "podobny"
  ]
  node [
    id 793
    label "gotowy"
  ]
  node [
    id 794
    label "might"
  ]
  node [
    id 795
    label "uprawi&#263;"
  ]
  node [
    id 796
    label "public_treasury"
  ]
  node [
    id 797
    label "pole"
  ]
  node [
    id 798
    label "obrobi&#263;"
  ]
  node [
    id 799
    label "nietrze&#378;wy"
  ]
  node [
    id 800
    label "czekanie"
  ]
  node [
    id 801
    label "martwy"
  ]
  node [
    id 802
    label "bliski"
  ]
  node [
    id 803
    label "gotowo"
  ]
  node [
    id 804
    label "przygotowanie"
  ]
  node [
    id 805
    label "przygotowywanie"
  ]
  node [
    id 806
    label "dyspozycyjny"
  ]
  node [
    id 807
    label "zalany"
  ]
  node [
    id 808
    label "nieuchronny"
  ]
  node [
    id 809
    label "doj&#347;cie"
  ]
  node [
    id 810
    label "dolecie&#263;"
  ]
  node [
    id 811
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 812
    label "spotka&#263;"
  ]
  node [
    id 813
    label "przypasowa&#263;"
  ]
  node [
    id 814
    label "hit"
  ]
  node [
    id 815
    label "pocisk"
  ]
  node [
    id 816
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 817
    label "stumble"
  ]
  node [
    id 818
    label "dotrze&#263;"
  ]
  node [
    id 819
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 820
    label "wpa&#347;&#263;"
  ]
  node [
    id 821
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 822
    label "znale&#378;&#263;"
  ]
  node [
    id 823
    label "happen"
  ]
  node [
    id 824
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 825
    label "insert"
  ]
  node [
    id 826
    label "visualize"
  ]
  node [
    id 827
    label "pozna&#263;"
  ]
  node [
    id 828
    label "befall"
  ]
  node [
    id 829
    label "spowodowa&#263;"
  ]
  node [
    id 830
    label "go_steady"
  ]
  node [
    id 831
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 832
    label "strike"
  ]
  node [
    id 833
    label "ulec"
  ]
  node [
    id 834
    label "collapse"
  ]
  node [
    id 835
    label "d&#378;wi&#281;k"
  ]
  node [
    id 836
    label "fall_upon"
  ]
  node [
    id 837
    label "ponie&#347;&#263;"
  ]
  node [
    id 838
    label "ogrom"
  ]
  node [
    id 839
    label "zapach"
  ]
  node [
    id 840
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 841
    label "uderzy&#263;"
  ]
  node [
    id 842
    label "wymy&#347;li&#263;"
  ]
  node [
    id 843
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 844
    label "wpada&#263;"
  ]
  node [
    id 845
    label "decline"
  ]
  node [
    id 846
    label "&#347;wiat&#322;o"
  ]
  node [
    id 847
    label "fall"
  ]
  node [
    id 848
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 849
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 850
    label "emocja"
  ]
  node [
    id 851
    label "odwiedzi&#263;"
  ]
  node [
    id 852
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 853
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 854
    label "pozyska&#263;"
  ]
  node [
    id 855
    label "oceni&#263;"
  ]
  node [
    id 856
    label "devise"
  ]
  node [
    id 857
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 858
    label "dozna&#263;"
  ]
  node [
    id 859
    label "wykry&#263;"
  ]
  node [
    id 860
    label "odzyska&#263;"
  ]
  node [
    id 861
    label "znaj&#347;&#263;"
  ]
  node [
    id 862
    label "invent"
  ]
  node [
    id 863
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 864
    label "utrze&#263;"
  ]
  node [
    id 865
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 866
    label "silnik"
  ]
  node [
    id 867
    label "catch"
  ]
  node [
    id 868
    label "dopasowa&#263;"
  ]
  node [
    id 869
    label "advance"
  ]
  node [
    id 870
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 871
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 872
    label "dorobi&#263;"
  ]
  node [
    id 873
    label "become"
  ]
  node [
    id 874
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 875
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 876
    label "range"
  ]
  node [
    id 877
    label "flow"
  ]
  node [
    id 878
    label "doj&#347;&#263;"
  ]
  node [
    id 879
    label "moda"
  ]
  node [
    id 880
    label "popularny"
  ]
  node [
    id 881
    label "utw&#243;r"
  ]
  node [
    id 882
    label "sensacja"
  ]
  node [
    id 883
    label "nowina"
  ]
  node [
    id 884
    label "odkrycie"
  ]
  node [
    id 885
    label "amunicja"
  ]
  node [
    id 886
    label "g&#322;owica"
  ]
  node [
    id 887
    label "trafienie"
  ]
  node [
    id 888
    label "trafianie"
  ]
  node [
    id 889
    label "kulka"
  ]
  node [
    id 890
    label "rdze&#324;"
  ]
  node [
    id 891
    label "prochownia"
  ]
  node [
    id 892
    label "przeniesienie"
  ]
  node [
    id 893
    label "&#322;adunek_bojowy"
  ]
  node [
    id 894
    label "przenoszenie"
  ]
  node [
    id 895
    label "przenie&#347;&#263;"
  ]
  node [
    id 896
    label "trafia&#263;"
  ]
  node [
    id 897
    label "przenosi&#263;"
  ]
  node [
    id 898
    label "bro&#324;"
  ]
  node [
    id 899
    label "wy&#322;&#261;czny"
  ]
  node [
    id 900
    label "w&#322;asny"
  ]
  node [
    id 901
    label "unikatowy"
  ]
  node [
    id 902
    label "jedyny"
  ]
  node [
    id 903
    label "okre&#347;lony"
  ]
  node [
    id 904
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 905
    label "wiadomy"
  ]
  node [
    id 906
    label "sklep"
  ]
  node [
    id 907
    label "p&#243;&#322;ka"
  ]
  node [
    id 908
    label "firma"
  ]
  node [
    id 909
    label "stoisko"
  ]
  node [
    id 910
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 911
    label "sk&#322;ad"
  ]
  node [
    id 912
    label "obiekt_handlowy"
  ]
  node [
    id 913
    label "zaplecze"
  ]
  node [
    id 914
    label "witryna"
  ]
  node [
    id 915
    label "NN"
  ]
  node [
    id 916
    label "nazwisko"
  ]
  node [
    id 917
    label "identity"
  ]
  node [
    id 918
    label "self-consciousness"
  ]
  node [
    id 919
    label "r&#243;wno&#347;&#263;"
  ]
  node [
    id 920
    label "uniformizm"
  ]
  node [
    id 921
    label "adres"
  ]
  node [
    id 922
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 923
    label "imi&#281;"
  ]
  node [
    id 924
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 925
    label "pesel"
  ]
  node [
    id 926
    label "depersonalizacja"
  ]
  node [
    id 927
    label "edytowa&#263;"
  ]
  node [
    id 928
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 929
    label "spakowanie"
  ]
  node [
    id 930
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 931
    label "pakowa&#263;"
  ]
  node [
    id 932
    label "rekord"
  ]
  node [
    id 933
    label "korelator"
  ]
  node [
    id 934
    label "wyci&#261;ganie"
  ]
  node [
    id 935
    label "pakowanie"
  ]
  node [
    id 936
    label "sekwencjonowa&#263;"
  ]
  node [
    id 937
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 938
    label "jednostka_informacji"
  ]
  node [
    id 939
    label "evidence"
  ]
  node [
    id 940
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 941
    label "rozpakowywanie"
  ]
  node [
    id 942
    label "rozpakowanie"
  ]
  node [
    id 943
    label "informacja"
  ]
  node [
    id 944
    label "rozpakowywa&#263;"
  ]
  node [
    id 945
    label "konwersja"
  ]
  node [
    id 946
    label "nap&#322;ywanie"
  ]
  node [
    id 947
    label "rozpakowa&#263;"
  ]
  node [
    id 948
    label "spakowa&#263;"
  ]
  node [
    id 949
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 950
    label "edytowanie"
  ]
  node [
    id 951
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 952
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 953
    label "sekwencjonowanie"
  ]
  node [
    id 954
    label "podobie&#324;stwo"
  ]
  node [
    id 955
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 956
    label "g&#322;adko&#347;&#263;"
  ]
  node [
    id 957
    label "spok&#243;j"
  ]
  node [
    id 958
    label "ekstraspekcja"
  ]
  node [
    id 959
    label "feeling"
  ]
  node [
    id 960
    label "wiedza"
  ]
  node [
    id 961
    label "zemdle&#263;"
  ]
  node [
    id 962
    label "psychika"
  ]
  node [
    id 963
    label "Freud"
  ]
  node [
    id 964
    label "psychoanaliza"
  ]
  node [
    id 965
    label "conscience"
  ]
  node [
    id 966
    label "nazwa_w&#322;asna"
  ]
  node [
    id 967
    label "elevation"
  ]
  node [
    id 968
    label "osobisto&#347;&#263;"
  ]
  node [
    id 969
    label "personalia"
  ]
  node [
    id 970
    label "numer"
  ]
  node [
    id 971
    label "po&#322;o&#380;enie"
  ]
  node [
    id 972
    label "domena"
  ]
  node [
    id 973
    label "kod_pocztowy"
  ]
  node [
    id 974
    label "adres_elektroniczny"
  ]
  node [
    id 975
    label "przesy&#322;ka"
  ]
  node [
    id 976
    label "strona"
  ]
  node [
    id 977
    label "reputacja"
  ]
  node [
    id 978
    label "deklinacja"
  ]
  node [
    id 979
    label "term"
  ]
  node [
    id 980
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 981
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 982
    label "leksem"
  ]
  node [
    id 983
    label "wezwanie"
  ]
  node [
    id 984
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 985
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 986
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 987
    label "patron"
  ]
  node [
    id 988
    label "imiennictwo"
  ]
  node [
    id 989
    label "nieznany"
  ]
  node [
    id 990
    label "kto&#347;"
  ]
  node [
    id 991
    label "jednakowo&#347;&#263;"
  ]
  node [
    id 992
    label "kszta&#322;t"
  ]
  node [
    id 993
    label "strata"
  ]
  node [
    id 994
    label "samo&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 995
    label "kulturowo"
  ]
  node [
    id 996
    label "RMF"
  ]
  node [
    id 997
    label "FM"
  ]
  node [
    id 998
    label "Katarzyna"
  ]
  node [
    id 999
    label "Szyma&#324;ska"
  ]
  node [
    id 1000
    label "Borginon"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 328
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 373
  ]
  edge [
    source 17
    target 374
  ]
  edge [
    source 17
    target 375
  ]
  edge [
    source 17
    target 376
  ]
  edge [
    source 17
    target 377
  ]
  edge [
    source 17
    target 378
  ]
  edge [
    source 17
    target 379
  ]
  edge [
    source 17
    target 380
  ]
  edge [
    source 17
    target 381
  ]
  edge [
    source 17
    target 382
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 412
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 68
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 430
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 406
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 996
    target 997
  ]
  edge [
    source 996
    target 998
  ]
  edge [
    source 996
    target 999
  ]
  edge [
    source 996
    target 1000
  ]
  edge [
    source 997
    target 998
  ]
  edge [
    source 997
    target 999
  ]
  edge [
    source 997
    target 1000
  ]
  edge [
    source 998
    target 999
  ]
  edge [
    source 998
    target 1000
  ]
  edge [
    source 999
    target 1000
  ]
]
