graph [
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "dziadek"
    origin "text"
  ]
  node [
    id 2
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 4
    label "lato"
    origin "text"
  ]
  node [
    id 5
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "smartfon"
    origin "text"
  ]
  node [
    id 8
    label "czyj&#347;"
  ]
  node [
    id 9
    label "m&#261;&#380;"
  ]
  node [
    id 10
    label "prywatny"
  ]
  node [
    id 11
    label "ma&#322;&#380;onek"
  ]
  node [
    id 12
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 13
    label "ch&#322;op"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "pan_m&#322;ody"
  ]
  node [
    id 16
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 17
    label "&#347;lubny"
  ]
  node [
    id 18
    label "pan_domu"
  ]
  node [
    id 19
    label "pan_i_w&#322;adca"
  ]
  node [
    id 20
    label "stary"
  ]
  node [
    id 21
    label "czwarty"
  ]
  node [
    id 22
    label "bryd&#380;ysta"
  ]
  node [
    id 23
    label "dziadkowie"
  ]
  node [
    id 24
    label "posta&#263;"
  ]
  node [
    id 25
    label "rada_starc&#243;w"
  ]
  node [
    id 26
    label "dziadowina"
  ]
  node [
    id 27
    label "partner"
  ]
  node [
    id 28
    label "dziad"
  ]
  node [
    id 29
    label "dziadyga"
  ]
  node [
    id 30
    label "przodek"
  ]
  node [
    id 31
    label "starszyzna"
  ]
  node [
    id 32
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 33
    label "pracownik"
  ]
  node [
    id 34
    label "przedsi&#281;biorca"
  ]
  node [
    id 35
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 36
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 37
    label "kolaborator"
  ]
  node [
    id 38
    label "prowadzi&#263;"
  ]
  node [
    id 39
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 40
    label "sp&#243;lnik"
  ]
  node [
    id 41
    label "aktor"
  ]
  node [
    id 42
    label "uczestniczenie"
  ]
  node [
    id 43
    label "charakterystyka"
  ]
  node [
    id 44
    label "zaistnie&#263;"
  ]
  node [
    id 45
    label "cecha"
  ]
  node [
    id 46
    label "Osjan"
  ]
  node [
    id 47
    label "kto&#347;"
  ]
  node [
    id 48
    label "wygl&#261;d"
  ]
  node [
    id 49
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 50
    label "osobowo&#347;&#263;"
  ]
  node [
    id 51
    label "wytw&#243;r"
  ]
  node [
    id 52
    label "trim"
  ]
  node [
    id 53
    label "poby&#263;"
  ]
  node [
    id 54
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 55
    label "Aspazja"
  ]
  node [
    id 56
    label "punkt_widzenia"
  ]
  node [
    id 57
    label "kompleksja"
  ]
  node [
    id 58
    label "wytrzyma&#263;"
  ]
  node [
    id 59
    label "budowa"
  ]
  node [
    id 60
    label "formacja"
  ]
  node [
    id 61
    label "pozosta&#263;"
  ]
  node [
    id 62
    label "point"
  ]
  node [
    id 63
    label "przedstawienie"
  ]
  node [
    id 64
    label "go&#347;&#263;"
  ]
  node [
    id 65
    label "gracz"
  ]
  node [
    id 66
    label "ojcowie"
  ]
  node [
    id 67
    label "linea&#380;"
  ]
  node [
    id 68
    label "krewny"
  ]
  node [
    id 69
    label "chodnik"
  ]
  node [
    id 70
    label "w&#243;z"
  ]
  node [
    id 71
    label "p&#322;ug"
  ]
  node [
    id 72
    label "wyrobisko"
  ]
  node [
    id 73
    label "antecesor"
  ]
  node [
    id 74
    label "post&#281;p"
  ]
  node [
    id 75
    label "doros&#322;y"
  ]
  node [
    id 76
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 77
    label "ojciec"
  ]
  node [
    id 78
    label "jegomo&#347;&#263;"
  ]
  node [
    id 79
    label "andropauza"
  ]
  node [
    id 80
    label "pa&#324;stwo"
  ]
  node [
    id 81
    label "bratek"
  ]
  node [
    id 82
    label "samiec"
  ]
  node [
    id 83
    label "ch&#322;opina"
  ]
  node [
    id 84
    label "twardziel"
  ]
  node [
    id 85
    label "androlog"
  ]
  node [
    id 86
    label "dzie&#324;"
  ]
  node [
    id 87
    label "pokolenie"
  ]
  node [
    id 88
    label "zesp&#243;&#322;"
  ]
  node [
    id 89
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 90
    label "organ"
  ]
  node [
    id 91
    label "harcerstwo"
  ]
  node [
    id 92
    label "biedny"
  ]
  node [
    id 93
    label "&#322;&#243;dzki"
  ]
  node [
    id 94
    label "kapu&#347;niak"
  ]
  node [
    id 95
    label "nie&#322;upka"
  ]
  node [
    id 96
    label "istota_&#380;ywa"
  ]
  node [
    id 97
    label "dziad_kalwaryjski"
  ]
  node [
    id 98
    label "starzec"
  ]
  node [
    id 99
    label "geezer"
  ]
  node [
    id 100
    label "ozdabia&#263;"
  ]
  node [
    id 101
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 102
    label "pora_roku"
  ]
  node [
    id 103
    label "podj&#261;&#263;"
  ]
  node [
    id 104
    label "sta&#263;_si&#281;"
  ]
  node [
    id 105
    label "determine"
  ]
  node [
    id 106
    label "zrobi&#263;"
  ]
  node [
    id 107
    label "zareagowa&#263;"
  ]
  node [
    id 108
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 109
    label "draw"
  ]
  node [
    id 110
    label "allude"
  ]
  node [
    id 111
    label "zmieni&#263;"
  ]
  node [
    id 112
    label "zacz&#261;&#263;"
  ]
  node [
    id 113
    label "raise"
  ]
  node [
    id 114
    label "post&#261;pi&#263;"
  ]
  node [
    id 115
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 116
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 117
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 118
    label "zorganizowa&#263;"
  ]
  node [
    id 119
    label "appoint"
  ]
  node [
    id 120
    label "wystylizowa&#263;"
  ]
  node [
    id 121
    label "cause"
  ]
  node [
    id 122
    label "przerobi&#263;"
  ]
  node [
    id 123
    label "nabra&#263;"
  ]
  node [
    id 124
    label "make"
  ]
  node [
    id 125
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 126
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 127
    label "wydali&#263;"
  ]
  node [
    id 128
    label "czu&#263;"
  ]
  node [
    id 129
    label "desire"
  ]
  node [
    id 130
    label "kcie&#263;"
  ]
  node [
    id 131
    label "postrzega&#263;"
  ]
  node [
    id 132
    label "przewidywa&#263;"
  ]
  node [
    id 133
    label "by&#263;"
  ]
  node [
    id 134
    label "smell"
  ]
  node [
    id 135
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 136
    label "uczuwa&#263;"
  ]
  node [
    id 137
    label "spirit"
  ]
  node [
    id 138
    label "doznawa&#263;"
  ]
  node [
    id 139
    label "anticipate"
  ]
  node [
    id 140
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 141
    label "kom&#243;rka"
  ]
  node [
    id 142
    label "ekran_dotykowy"
  ]
  node [
    id 143
    label "cytoplazma"
  ]
  node [
    id 144
    label "b&#322;ona_kom&#243;rkowa"
  ]
  node [
    id 145
    label "pomieszczenie"
  ]
  node [
    id 146
    label "plaster"
  ]
  node [
    id 147
    label "burza"
  ]
  node [
    id 148
    label "akantoliza"
  ]
  node [
    id 149
    label "pole"
  ]
  node [
    id 150
    label "p&#281;cherzyk"
  ]
  node [
    id 151
    label "hipoderma"
  ]
  node [
    id 152
    label "struktura_anatomiczna"
  ]
  node [
    id 153
    label "telefon"
  ]
  node [
    id 154
    label "filia"
  ]
  node [
    id 155
    label "embrioblast"
  ]
  node [
    id 156
    label "wakuom"
  ]
  node [
    id 157
    label "tkanka"
  ]
  node [
    id 158
    label "osocze_krwi"
  ]
  node [
    id 159
    label "biomembrana"
  ]
  node [
    id 160
    label "tabela"
  ]
  node [
    id 161
    label "b&#322;ona_podstawna"
  ]
  node [
    id 162
    label "organellum"
  ]
  node [
    id 163
    label "oddychanie_kom&#243;rkowe"
  ]
  node [
    id 164
    label "cytochemia"
  ]
  node [
    id 165
    label "mikrosom"
  ]
  node [
    id 166
    label "wy&#347;wietlacz"
  ]
  node [
    id 167
    label "obszar"
  ]
  node [
    id 168
    label "cell"
  ]
  node [
    id 169
    label "genotyp"
  ]
  node [
    id 170
    label "urz&#261;dzenie"
  ]
  node [
    id 171
    label "urz&#261;dzenie_mobilne"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
]
