graph [
  node [
    id 0
    label "andrzej"
    origin "text"
  ]
  node [
    id 1
    label "ostoja"
    origin "text"
  ]
  node [
    id 2
    label "owsiany"
    origin "text"
  ]
  node [
    id 3
    label "rama"
  ]
  node [
    id 4
    label "podstawa"
  ]
  node [
    id 5
    label "pojazd_szynowy"
  ]
  node [
    id 6
    label "miejsce"
  ]
  node [
    id 7
    label "siedlisko"
  ]
  node [
    id 8
    label "podpora"
  ]
  node [
    id 9
    label "przestrze&#324;"
  ]
  node [
    id 10
    label "rz&#261;d"
  ]
  node [
    id 11
    label "uwaga"
  ]
  node [
    id 12
    label "cecha"
  ]
  node [
    id 13
    label "praca"
  ]
  node [
    id 14
    label "plac"
  ]
  node [
    id 15
    label "location"
  ]
  node [
    id 16
    label "warunek_lokalowy"
  ]
  node [
    id 17
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 18
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 19
    label "cia&#322;o"
  ]
  node [
    id 20
    label "status"
  ]
  node [
    id 21
    label "chwila"
  ]
  node [
    id 22
    label "za&#322;o&#380;enie"
  ]
  node [
    id 23
    label "struktura"
  ]
  node [
    id 24
    label "postawa"
  ]
  node [
    id 25
    label "zakres"
  ]
  node [
    id 26
    label "paczka"
  ]
  node [
    id 27
    label "dodatek"
  ]
  node [
    id 28
    label "stela&#380;"
  ]
  node [
    id 29
    label "human_body"
  ]
  node [
    id 30
    label "oprawa"
  ]
  node [
    id 31
    label "element_konstrukcyjny"
  ]
  node [
    id 32
    label "pojazd"
  ]
  node [
    id 33
    label "szablon"
  ]
  node [
    id 34
    label "obramowanie"
  ]
  node [
    id 35
    label "column"
  ]
  node [
    id 36
    label "nadzieja"
  ]
  node [
    id 37
    label "skupisko"
  ]
  node [
    id 38
    label "o&#347;rodek"
  ]
  node [
    id 39
    label "mikrosiedlisko"
  ]
  node [
    id 40
    label "sadowisko"
  ]
  node [
    id 41
    label "strategia"
  ]
  node [
    id 42
    label "background"
  ]
  node [
    id 43
    label "przedmiot"
  ]
  node [
    id 44
    label "punkt_odniesienia"
  ]
  node [
    id 45
    label "zasadzenie"
  ]
  node [
    id 46
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 47
    label "&#347;ciana"
  ]
  node [
    id 48
    label "podstawowy"
  ]
  node [
    id 49
    label "dzieci&#281;ctwo"
  ]
  node [
    id 50
    label "d&#243;&#322;"
  ]
  node [
    id 51
    label "documentation"
  ]
  node [
    id 52
    label "bok"
  ]
  node [
    id 53
    label "pomys&#322;"
  ]
  node [
    id 54
    label "zasadzi&#263;"
  ]
  node [
    id 55
    label "pot&#281;ga"
  ]
  node [
    id 56
    label "zbo&#380;owy"
  ]
  node [
    id 57
    label "m&#261;czny"
  ]
  node [
    id 58
    label "specjalny"
  ]
  node [
    id 59
    label "Andrzej"
  ]
  node [
    id 60
    label "armia"
  ]
  node [
    id 61
    label "krajowy"
  ]
  node [
    id 62
    label "konspiracyjny"
  ]
  node [
    id 63
    label "wojsko"
  ]
  node [
    id 64
    label "polskie"
  ]
  node [
    id 65
    label "Stanis&#322;awa"
  ]
  node [
    id 66
    label "Sojczy&#324;skiego"
  ]
  node [
    id 67
    label "wydzia&#322;"
  ]
  node [
    id 68
    label "prawy"
  ]
  node [
    id 69
    label "uniwersytet"
  ]
  node [
    id 70
    label "&#322;&#243;dzki"
  ]
  node [
    id 71
    label "zwi&#261;zek"
  ]
  node [
    id 72
    label "m&#322;odzi"
  ]
  node [
    id 73
    label "demokrata"
  ]
  node [
    id 74
    label "NSZZ"
  ]
  node [
    id 75
    label "solidarno&#347;&#263;"
  ]
  node [
    id 76
    label "rucho"
  ]
  node [
    id 77
    label "obrona"
  ]
  node [
    id 78
    label "cz&#322;owiek"
  ]
  node [
    id 79
    label "i"
  ]
  node [
    id 80
    label "obywatel"
  ]
  node [
    id 81
    label "rada"
  ]
  node [
    id 82
    label "ZINO"
  ]
  node [
    id 83
    label "polityczny"
  ]
  node [
    id 84
    label "konfederacja"
  ]
  node [
    id 85
    label "polski"
  ]
  node [
    id 86
    label "niepodleg&#322;y"
  ]
  node [
    id 87
    label "spo&#322;eczny"
  ]
  node [
    id 88
    label "komitet"
  ]
  node [
    id 89
    label "pami&#281;&#263;"
  ]
  node [
    id 90
    label "marsza&#322;ek"
  ]
  node [
    id 91
    label "J&#243;zefa"
  ]
  node [
    id 92
    label "Pi&#322;sudski"
  ]
  node [
    id 93
    label "zielony"
  ]
  node [
    id 94
    label "planeta"
  ]
  node [
    id 95
    label "stowarzyszy&#263;"
  ]
  node [
    id 96
    label "pisarz"
  ]
  node [
    id 97
    label "literat"
  ]
  node [
    id 98
    label "miejski"
  ]
  node [
    id 99
    label "Hanna"
  ]
  node [
    id 100
    label "Suchocka"
  ]
  node [
    id 101
    label "akcja"
  ]
  node [
    id 102
    label "wyborczy"
  ]
  node [
    id 103
    label "forum"
  ]
  node [
    id 104
    label "obywatelski"
  ]
  node [
    id 105
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 106
    label "demokracja"
  ]
  node [
    id 107
    label "Jerzy"
  ]
  node [
    id 108
    label "Kropiwnickiego"
  ]
  node [
    id 109
    label "lecha"
  ]
  node [
    id 110
    label "Kaczy&#324;ski"
  ]
  node [
    id 111
    label "order"
  ]
  node [
    id 112
    label "odrodzi&#263;"
  ]
  node [
    id 113
    label "starzy"
  ]
  node [
    id 114
    label "cmentarz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 64
    target 95
  ]
  edge [
    source 64
    target 96
  ]
  edge [
    source 64
    target 71
  ]
  edge [
    source 64
    target 97
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 68
    target 76
  ]
  edge [
    source 68
    target 77
  ]
  edge [
    source 68
    target 78
  ]
  edge [
    source 68
    target 79
  ]
  edge [
    source 68
    target 80
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 97
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 75
    target 101
  ]
  edge [
    source 75
    target 102
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 78
  ]
  edge [
    source 76
    target 79
  ]
  edge [
    source 76
    target 80
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 79
  ]
  edge [
    source 77
    target 80
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 81
    target 98
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 111
  ]
  edge [
    source 85
    target 112
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 89
  ]
  edge [
    source 87
    target 90
  ]
  edge [
    source 87
    target 91
  ]
  edge [
    source 87
    target 92
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 88
    target 91
  ]
  edge [
    source 88
    target 92
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 106
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 106
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 113
    target 114
  ]
]
