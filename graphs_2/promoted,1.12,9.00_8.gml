graph [
  node [
    id 0
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 1
    label "nic"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "zmieni&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ci&#261;g"
    origin "text"
  ]
  node [
    id 5
    label "bliski"
    origin "text"
  ]
  node [
    id 6
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "wis&#322;a"
    origin "text"
  ]
  node [
    id 8
    label "krak"
    origin "text"
  ]
  node [
    id 9
    label "przyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "rozgrywka"
    origin "text"
  ]
  node [
    id 11
    label "wiosna"
    origin "text"
  ]
  node [
    id 12
    label "obecna"
    origin "text"
  ]
  node [
    id 13
    label "dru&#380;yna"
    origin "text"
  ]
  node [
    id 14
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kolejny"
    origin "text"
  ]
  node [
    id 17
    label "sezon"
    origin "text"
  ]
  node [
    id 18
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "czwarta"
    origin "text"
  ]
  node [
    id 20
    label "liga"
    origin "text"
  ]
  node [
    id 21
    label "ilo&#347;&#263;"
  ]
  node [
    id 22
    label "ciura"
  ]
  node [
    id 23
    label "miernota"
  ]
  node [
    id 24
    label "g&#243;wno"
  ]
  node [
    id 25
    label "love"
  ]
  node [
    id 26
    label "brak"
  ]
  node [
    id 27
    label "nieistnienie"
  ]
  node [
    id 28
    label "odej&#347;cie"
  ]
  node [
    id 29
    label "defect"
  ]
  node [
    id 30
    label "gap"
  ]
  node [
    id 31
    label "odej&#347;&#263;"
  ]
  node [
    id 32
    label "kr&#243;tki"
  ]
  node [
    id 33
    label "wada"
  ]
  node [
    id 34
    label "odchodzi&#263;"
  ]
  node [
    id 35
    label "wyr&#243;b"
  ]
  node [
    id 36
    label "odchodzenie"
  ]
  node [
    id 37
    label "prywatywny"
  ]
  node [
    id 38
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 39
    label "rozmiar"
  ]
  node [
    id 40
    label "part"
  ]
  node [
    id 41
    label "jako&#347;&#263;"
  ]
  node [
    id 42
    label "cz&#322;owiek"
  ]
  node [
    id 43
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 44
    label "tandetno&#347;&#263;"
  ]
  node [
    id 45
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 46
    label "ka&#322;"
  ]
  node [
    id 47
    label "tandeta"
  ]
  node [
    id 48
    label "zero"
  ]
  node [
    id 49
    label "drobiazg"
  ]
  node [
    id 50
    label "chor&#261;&#380;y"
  ]
  node [
    id 51
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 52
    label "sprawi&#263;"
  ]
  node [
    id 53
    label "change"
  ]
  node [
    id 54
    label "zrobi&#263;"
  ]
  node [
    id 55
    label "zast&#261;pi&#263;"
  ]
  node [
    id 56
    label "come_up"
  ]
  node [
    id 57
    label "przej&#347;&#263;"
  ]
  node [
    id 58
    label "straci&#263;"
  ]
  node [
    id 59
    label "zyska&#263;"
  ]
  node [
    id 60
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 61
    label "bomber"
  ]
  node [
    id 62
    label "zdecydowa&#263;"
  ]
  node [
    id 63
    label "wyrobi&#263;"
  ]
  node [
    id 64
    label "wzi&#261;&#263;"
  ]
  node [
    id 65
    label "catch"
  ]
  node [
    id 66
    label "spowodowa&#263;"
  ]
  node [
    id 67
    label "frame"
  ]
  node [
    id 68
    label "przygotowa&#263;"
  ]
  node [
    id 69
    label "pozyska&#263;"
  ]
  node [
    id 70
    label "utilize"
  ]
  node [
    id 71
    label "naby&#263;"
  ]
  node [
    id 72
    label "uzyska&#263;"
  ]
  node [
    id 73
    label "poprawi&#263;_si&#281;"
  ]
  node [
    id 74
    label "receive"
  ]
  node [
    id 75
    label "stracenie"
  ]
  node [
    id 76
    label "leave_office"
  ]
  node [
    id 77
    label "zabi&#263;"
  ]
  node [
    id 78
    label "forfeit"
  ]
  node [
    id 79
    label "wytraci&#263;"
  ]
  node [
    id 80
    label "waste"
  ]
  node [
    id 81
    label "przegra&#263;"
  ]
  node [
    id 82
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 83
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 84
    label "execute"
  ]
  node [
    id 85
    label "omin&#261;&#263;"
  ]
  node [
    id 86
    label "ustawa"
  ]
  node [
    id 87
    label "podlec"
  ]
  node [
    id 88
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 89
    label "min&#261;&#263;"
  ]
  node [
    id 90
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 91
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 92
    label "zaliczy&#263;"
  ]
  node [
    id 93
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 94
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 95
    label "przeby&#263;"
  ]
  node [
    id 96
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 97
    label "die"
  ]
  node [
    id 98
    label "dozna&#263;"
  ]
  node [
    id 99
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 100
    label "zacz&#261;&#263;"
  ]
  node [
    id 101
    label "happen"
  ]
  node [
    id 102
    label "pass"
  ]
  node [
    id 103
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 104
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 105
    label "beat"
  ]
  node [
    id 106
    label "mienie"
  ]
  node [
    id 107
    label "absorb"
  ]
  node [
    id 108
    label "przerobi&#263;"
  ]
  node [
    id 109
    label "pique"
  ]
  node [
    id 110
    label "przesta&#263;"
  ]
  node [
    id 111
    label "post&#261;pi&#263;"
  ]
  node [
    id 112
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 113
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 114
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 115
    label "zorganizowa&#263;"
  ]
  node [
    id 116
    label "appoint"
  ]
  node [
    id 117
    label "wystylizowa&#263;"
  ]
  node [
    id 118
    label "cause"
  ]
  node [
    id 119
    label "nabra&#263;"
  ]
  node [
    id 120
    label "make"
  ]
  node [
    id 121
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 122
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 123
    label "wydali&#263;"
  ]
  node [
    id 124
    label "lot"
  ]
  node [
    id 125
    label "pr&#261;d"
  ]
  node [
    id 126
    label "przebieg"
  ]
  node [
    id 127
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 128
    label "k&#322;us"
  ]
  node [
    id 129
    label "zbi&#243;r"
  ]
  node [
    id 130
    label "si&#322;a"
  ]
  node [
    id 131
    label "cable"
  ]
  node [
    id 132
    label "wydarzenie"
  ]
  node [
    id 133
    label "lina"
  ]
  node [
    id 134
    label "way"
  ]
  node [
    id 135
    label "stan"
  ]
  node [
    id 136
    label "ch&#243;d"
  ]
  node [
    id 137
    label "current"
  ]
  node [
    id 138
    label "trasa"
  ]
  node [
    id 139
    label "progression"
  ]
  node [
    id 140
    label "rz&#261;d"
  ]
  node [
    id 141
    label "egzemplarz"
  ]
  node [
    id 142
    label "series"
  ]
  node [
    id 143
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 144
    label "uprawianie"
  ]
  node [
    id 145
    label "praca_rolnicza"
  ]
  node [
    id 146
    label "collection"
  ]
  node [
    id 147
    label "dane"
  ]
  node [
    id 148
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 149
    label "pakiet_klimatyczny"
  ]
  node [
    id 150
    label "poj&#281;cie"
  ]
  node [
    id 151
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 152
    label "sum"
  ]
  node [
    id 153
    label "gathering"
  ]
  node [
    id 154
    label "album"
  ]
  node [
    id 155
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 156
    label "energia"
  ]
  node [
    id 157
    label "przep&#322;yw"
  ]
  node [
    id 158
    label "ideologia"
  ]
  node [
    id 159
    label "apparent_motion"
  ]
  node [
    id 160
    label "przyp&#322;yw"
  ]
  node [
    id 161
    label "metoda"
  ]
  node [
    id 162
    label "electricity"
  ]
  node [
    id 163
    label "dreszcz"
  ]
  node [
    id 164
    label "ruch"
  ]
  node [
    id 165
    label "zjawisko"
  ]
  node [
    id 166
    label "praktyka"
  ]
  node [
    id 167
    label "system"
  ]
  node [
    id 168
    label "parametr"
  ]
  node [
    id 169
    label "rozwi&#261;zanie"
  ]
  node [
    id 170
    label "wojsko"
  ]
  node [
    id 171
    label "cecha"
  ]
  node [
    id 172
    label "wuchta"
  ]
  node [
    id 173
    label "zaleta"
  ]
  node [
    id 174
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 175
    label "moment_si&#322;y"
  ]
  node [
    id 176
    label "mn&#243;stwo"
  ]
  node [
    id 177
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 178
    label "zdolno&#347;&#263;"
  ]
  node [
    id 179
    label "capacity"
  ]
  node [
    id 180
    label "magnitude"
  ]
  node [
    id 181
    label "potencja"
  ]
  node [
    id 182
    label "przemoc"
  ]
  node [
    id 183
    label "podr&#243;&#380;"
  ]
  node [
    id 184
    label "migracja"
  ]
  node [
    id 185
    label "hike"
  ]
  node [
    id 186
    label "przedmiot"
  ]
  node [
    id 187
    label "wyluzowanie"
  ]
  node [
    id 188
    label "skr&#281;tka"
  ]
  node [
    id 189
    label "pika-pina"
  ]
  node [
    id 190
    label "bom"
  ]
  node [
    id 191
    label "abaka"
  ]
  node [
    id 192
    label "wyluzowa&#263;"
  ]
  node [
    id 193
    label "step"
  ]
  node [
    id 194
    label "lekkoatletyka"
  ]
  node [
    id 195
    label "konkurencja"
  ]
  node [
    id 196
    label "czerwona_kartka"
  ]
  node [
    id 197
    label "krok"
  ]
  node [
    id 198
    label "wy&#347;cig"
  ]
  node [
    id 199
    label "bieg"
  ]
  node [
    id 200
    label "trot"
  ]
  node [
    id 201
    label "Ohio"
  ]
  node [
    id 202
    label "wci&#281;cie"
  ]
  node [
    id 203
    label "Nowy_York"
  ]
  node [
    id 204
    label "warstwa"
  ]
  node [
    id 205
    label "samopoczucie"
  ]
  node [
    id 206
    label "Illinois"
  ]
  node [
    id 207
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 208
    label "state"
  ]
  node [
    id 209
    label "Jukatan"
  ]
  node [
    id 210
    label "Kalifornia"
  ]
  node [
    id 211
    label "Wirginia"
  ]
  node [
    id 212
    label "wektor"
  ]
  node [
    id 213
    label "by&#263;"
  ]
  node [
    id 214
    label "Goa"
  ]
  node [
    id 215
    label "Teksas"
  ]
  node [
    id 216
    label "Waszyngton"
  ]
  node [
    id 217
    label "miejsce"
  ]
  node [
    id 218
    label "Massachusetts"
  ]
  node [
    id 219
    label "Alaska"
  ]
  node [
    id 220
    label "Arakan"
  ]
  node [
    id 221
    label "Hawaje"
  ]
  node [
    id 222
    label "Maryland"
  ]
  node [
    id 223
    label "punkt"
  ]
  node [
    id 224
    label "Michigan"
  ]
  node [
    id 225
    label "Arizona"
  ]
  node [
    id 226
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 227
    label "Georgia"
  ]
  node [
    id 228
    label "poziom"
  ]
  node [
    id 229
    label "Pensylwania"
  ]
  node [
    id 230
    label "shape"
  ]
  node [
    id 231
    label "Luizjana"
  ]
  node [
    id 232
    label "Nowy_Meksyk"
  ]
  node [
    id 233
    label "Alabama"
  ]
  node [
    id 234
    label "Kansas"
  ]
  node [
    id 235
    label "Oregon"
  ]
  node [
    id 236
    label "Oklahoma"
  ]
  node [
    id 237
    label "Floryda"
  ]
  node [
    id 238
    label "jednostka_administracyjna"
  ]
  node [
    id 239
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 240
    label "droga"
  ]
  node [
    id 241
    label "infrastruktura"
  ]
  node [
    id 242
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 243
    label "w&#281;ze&#322;"
  ]
  node [
    id 244
    label "marszrutyzacja"
  ]
  node [
    id 245
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 246
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 247
    label "podbieg"
  ]
  node [
    id 248
    label "przybli&#380;enie"
  ]
  node [
    id 249
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 250
    label "kategoria"
  ]
  node [
    id 251
    label "szpaler"
  ]
  node [
    id 252
    label "lon&#380;a"
  ]
  node [
    id 253
    label "uporz&#261;dkowanie"
  ]
  node [
    id 254
    label "egzekutywa"
  ]
  node [
    id 255
    label "jednostka_systematyczna"
  ]
  node [
    id 256
    label "instytucja"
  ]
  node [
    id 257
    label "premier"
  ]
  node [
    id 258
    label "Londyn"
  ]
  node [
    id 259
    label "gabinet_cieni"
  ]
  node [
    id 260
    label "gromada"
  ]
  node [
    id 261
    label "number"
  ]
  node [
    id 262
    label "Konsulat"
  ]
  node [
    id 263
    label "tract"
  ]
  node [
    id 264
    label "klasa"
  ]
  node [
    id 265
    label "w&#322;adza"
  ]
  node [
    id 266
    label "chronometra&#380;ysta"
  ]
  node [
    id 267
    label "odlot"
  ]
  node [
    id 268
    label "l&#261;dowanie"
  ]
  node [
    id 269
    label "start"
  ]
  node [
    id 270
    label "flight"
  ]
  node [
    id 271
    label "przebiec"
  ]
  node [
    id 272
    label "charakter"
  ]
  node [
    id 273
    label "czynno&#347;&#263;"
  ]
  node [
    id 274
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 275
    label "motyw"
  ]
  node [
    id 276
    label "przebiegni&#281;cie"
  ]
  node [
    id 277
    label "fabu&#322;a"
  ]
  node [
    id 278
    label "linia"
  ]
  node [
    id 279
    label "procedura"
  ]
  node [
    id 280
    label "proces"
  ]
  node [
    id 281
    label "room"
  ]
  node [
    id 282
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 283
    label "sequence"
  ]
  node [
    id 284
    label "praca"
  ]
  node [
    id 285
    label "cycle"
  ]
  node [
    id 286
    label "blisko"
  ]
  node [
    id 287
    label "znajomy"
  ]
  node [
    id 288
    label "zwi&#261;zany"
  ]
  node [
    id 289
    label "przesz&#322;y"
  ]
  node [
    id 290
    label "silny"
  ]
  node [
    id 291
    label "zbli&#380;enie"
  ]
  node [
    id 292
    label "oddalony"
  ]
  node [
    id 293
    label "dok&#322;adny"
  ]
  node [
    id 294
    label "nieodleg&#322;y"
  ]
  node [
    id 295
    label "przysz&#322;y"
  ]
  node [
    id 296
    label "gotowy"
  ]
  node [
    id 297
    label "ma&#322;y"
  ]
  node [
    id 298
    label "szybki"
  ]
  node [
    id 299
    label "jednowyrazowy"
  ]
  node [
    id 300
    label "s&#322;aby"
  ]
  node [
    id 301
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 302
    label "kr&#243;tko"
  ]
  node [
    id 303
    label "drobny"
  ]
  node [
    id 304
    label "z&#322;y"
  ]
  node [
    id 305
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 306
    label "dok&#322;adnie"
  ]
  node [
    id 307
    label "silnie"
  ]
  node [
    id 308
    label "nietrze&#378;wy"
  ]
  node [
    id 309
    label "czekanie"
  ]
  node [
    id 310
    label "martwy"
  ]
  node [
    id 311
    label "m&#243;c"
  ]
  node [
    id 312
    label "gotowo"
  ]
  node [
    id 313
    label "przygotowywanie"
  ]
  node [
    id 314
    label "przygotowanie"
  ]
  node [
    id 315
    label "dyspozycyjny"
  ]
  node [
    id 316
    label "zalany"
  ]
  node [
    id 317
    label "nieuchronny"
  ]
  node [
    id 318
    label "doj&#347;cie"
  ]
  node [
    id 319
    label "zetkni&#281;cie"
  ]
  node [
    id 320
    label "plan"
  ]
  node [
    id 321
    label "po&#380;ycie"
  ]
  node [
    id 322
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 323
    label "podnieci&#263;"
  ]
  node [
    id 324
    label "numer"
  ]
  node [
    id 325
    label "closeup"
  ]
  node [
    id 326
    label "podniecenie"
  ]
  node [
    id 327
    label "po&#322;&#261;czenie"
  ]
  node [
    id 328
    label "konfidencja"
  ]
  node [
    id 329
    label "seks"
  ]
  node [
    id 330
    label "podniecanie"
  ]
  node [
    id 331
    label "imisja"
  ]
  node [
    id 332
    label "pobratymstwo"
  ]
  node [
    id 333
    label "rozmna&#380;anie"
  ]
  node [
    id 334
    label "proximity"
  ]
  node [
    id 335
    label "uj&#281;cie"
  ]
  node [
    id 336
    label "ruch_frykcyjny"
  ]
  node [
    id 337
    label "na_pieska"
  ]
  node [
    id 338
    label "pozycja_misjonarska"
  ]
  node [
    id 339
    label "przemieszczenie"
  ]
  node [
    id 340
    label "dru&#380;ba"
  ]
  node [
    id 341
    label "approach"
  ]
  node [
    id 342
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 343
    label "z&#322;&#261;czenie"
  ]
  node [
    id 344
    label "gra_wst&#281;pna"
  ]
  node [
    id 345
    label "znajomo&#347;&#263;"
  ]
  node [
    id 346
    label "erotyka"
  ]
  node [
    id 347
    label "baraszki"
  ]
  node [
    id 348
    label "po&#380;&#261;danie"
  ]
  node [
    id 349
    label "wzw&#243;d"
  ]
  node [
    id 350
    label "podnieca&#263;"
  ]
  node [
    id 351
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 352
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 353
    label "sprecyzowanie"
  ]
  node [
    id 354
    label "precyzyjny"
  ]
  node [
    id 355
    label "miliamperomierz"
  ]
  node [
    id 356
    label "precyzowanie"
  ]
  node [
    id 357
    label "rzetelny"
  ]
  node [
    id 358
    label "ludzko&#347;&#263;"
  ]
  node [
    id 359
    label "asymilowanie"
  ]
  node [
    id 360
    label "wapniak"
  ]
  node [
    id 361
    label "asymilowa&#263;"
  ]
  node [
    id 362
    label "os&#322;abia&#263;"
  ]
  node [
    id 363
    label "posta&#263;"
  ]
  node [
    id 364
    label "hominid"
  ]
  node [
    id 365
    label "podw&#322;adny"
  ]
  node [
    id 366
    label "os&#322;abianie"
  ]
  node [
    id 367
    label "g&#322;owa"
  ]
  node [
    id 368
    label "figura"
  ]
  node [
    id 369
    label "portrecista"
  ]
  node [
    id 370
    label "dwun&#243;g"
  ]
  node [
    id 371
    label "profanum"
  ]
  node [
    id 372
    label "mikrokosmos"
  ]
  node [
    id 373
    label "nasada"
  ]
  node [
    id 374
    label "duch"
  ]
  node [
    id 375
    label "antropochoria"
  ]
  node [
    id 376
    label "osoba"
  ]
  node [
    id 377
    label "wz&#243;r"
  ]
  node [
    id 378
    label "senior"
  ]
  node [
    id 379
    label "oddzia&#322;ywanie"
  ]
  node [
    id 380
    label "Adam"
  ]
  node [
    id 381
    label "homo_sapiens"
  ]
  node [
    id 382
    label "polifag"
  ]
  node [
    id 383
    label "znany"
  ]
  node [
    id 384
    label "zapoznanie"
  ]
  node [
    id 385
    label "sw&#243;j"
  ]
  node [
    id 386
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 387
    label "zapoznanie_si&#281;"
  ]
  node [
    id 388
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 389
    label "znajomek"
  ]
  node [
    id 390
    label "zapoznawanie"
  ]
  node [
    id 391
    label "przyj&#281;ty"
  ]
  node [
    id 392
    label "pewien"
  ]
  node [
    id 393
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 394
    label "znajomo"
  ]
  node [
    id 395
    label "za_pan_brat"
  ]
  node [
    id 396
    label "nieznaczny"
  ]
  node [
    id 397
    label "przeci&#281;tny"
  ]
  node [
    id 398
    label "wstydliwy"
  ]
  node [
    id 399
    label "niewa&#380;ny"
  ]
  node [
    id 400
    label "ch&#322;opiec"
  ]
  node [
    id 401
    label "m&#322;ody"
  ]
  node [
    id 402
    label "ma&#322;o"
  ]
  node [
    id 403
    label "marny"
  ]
  node [
    id 404
    label "nieliczny"
  ]
  node [
    id 405
    label "n&#281;dznie"
  ]
  node [
    id 406
    label "oderwany"
  ]
  node [
    id 407
    label "daleki"
  ]
  node [
    id 408
    label "daleko"
  ]
  node [
    id 409
    label "miniony"
  ]
  node [
    id 410
    label "ostatni"
  ]
  node [
    id 411
    label "intensywny"
  ]
  node [
    id 412
    label "krzepienie"
  ]
  node [
    id 413
    label "&#380;ywotny"
  ]
  node [
    id 414
    label "mocny"
  ]
  node [
    id 415
    label "pokrzepienie"
  ]
  node [
    id 416
    label "zdecydowany"
  ]
  node [
    id 417
    label "niepodwa&#380;alny"
  ]
  node [
    id 418
    label "du&#380;y"
  ]
  node [
    id 419
    label "mocno"
  ]
  node [
    id 420
    label "przekonuj&#261;cy"
  ]
  node [
    id 421
    label "wytrzyma&#322;y"
  ]
  node [
    id 422
    label "konkretny"
  ]
  node [
    id 423
    label "zdrowy"
  ]
  node [
    id 424
    label "meflochina"
  ]
  node [
    id 425
    label "zajebisty"
  ]
  node [
    id 426
    label "doba"
  ]
  node [
    id 427
    label "weekend"
  ]
  node [
    id 428
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 429
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 430
    label "czas"
  ]
  node [
    id 431
    label "miesi&#261;c"
  ]
  node [
    id 432
    label "poprzedzanie"
  ]
  node [
    id 433
    label "czasoprzestrze&#324;"
  ]
  node [
    id 434
    label "laba"
  ]
  node [
    id 435
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 436
    label "chronometria"
  ]
  node [
    id 437
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 438
    label "rachuba_czasu"
  ]
  node [
    id 439
    label "przep&#322;ywanie"
  ]
  node [
    id 440
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 441
    label "czasokres"
  ]
  node [
    id 442
    label "odczyt"
  ]
  node [
    id 443
    label "chwila"
  ]
  node [
    id 444
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 445
    label "dzieje"
  ]
  node [
    id 446
    label "kategoria_gramatyczna"
  ]
  node [
    id 447
    label "poprzedzenie"
  ]
  node [
    id 448
    label "trawienie"
  ]
  node [
    id 449
    label "pochodzi&#263;"
  ]
  node [
    id 450
    label "period"
  ]
  node [
    id 451
    label "okres_czasu"
  ]
  node [
    id 452
    label "poprzedza&#263;"
  ]
  node [
    id 453
    label "schy&#322;ek"
  ]
  node [
    id 454
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 455
    label "odwlekanie_si&#281;"
  ]
  node [
    id 456
    label "zegar"
  ]
  node [
    id 457
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 458
    label "czwarty_wymiar"
  ]
  node [
    id 459
    label "pochodzenie"
  ]
  node [
    id 460
    label "koniugacja"
  ]
  node [
    id 461
    label "Zeitgeist"
  ]
  node [
    id 462
    label "trawi&#263;"
  ]
  node [
    id 463
    label "pogoda"
  ]
  node [
    id 464
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 465
    label "poprzedzi&#263;"
  ]
  node [
    id 466
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 467
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 468
    label "time_period"
  ]
  node [
    id 469
    label "noc"
  ]
  node [
    id 470
    label "dzie&#324;"
  ]
  node [
    id 471
    label "godzina"
  ]
  node [
    id 472
    label "long_time"
  ]
  node [
    id 473
    label "jednostka_geologiczna"
  ]
  node [
    id 474
    label "niedziela"
  ]
  node [
    id 475
    label "sobota"
  ]
  node [
    id 476
    label "miech"
  ]
  node [
    id 477
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 478
    label "rok"
  ]
  node [
    id 479
    label "kalendy"
  ]
  node [
    id 480
    label "mount"
  ]
  node [
    id 481
    label "wej&#347;&#263;"
  ]
  node [
    id 482
    label "sta&#263;_si&#281;"
  ]
  node [
    id 483
    label "move"
  ]
  node [
    id 484
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 485
    label "zaistnie&#263;"
  ]
  node [
    id 486
    label "z&#322;oi&#263;"
  ]
  node [
    id 487
    label "ascend"
  ]
  node [
    id 488
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 489
    label "przekroczy&#263;"
  ]
  node [
    id 490
    label "nast&#261;pi&#263;"
  ]
  node [
    id 491
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 492
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 493
    label "intervene"
  ]
  node [
    id 494
    label "get"
  ]
  node [
    id 495
    label "pozna&#263;"
  ]
  node [
    id 496
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 497
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 498
    label "wnikn&#261;&#263;"
  ]
  node [
    id 499
    label "przenikn&#261;&#263;"
  ]
  node [
    id 500
    label "doj&#347;&#263;"
  ]
  node [
    id 501
    label "spotka&#263;"
  ]
  node [
    id 502
    label "submit"
  ]
  node [
    id 503
    label "become"
  ]
  node [
    id 504
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 505
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 506
    label "odj&#261;&#263;"
  ]
  node [
    id 507
    label "introduce"
  ]
  node [
    id 508
    label "begin"
  ]
  node [
    id 509
    label "do"
  ]
  node [
    id 510
    label "trafienie"
  ]
  node [
    id 511
    label "rewan&#380;owy"
  ]
  node [
    id 512
    label "zagrywka"
  ]
  node [
    id 513
    label "faza"
  ]
  node [
    id 514
    label "euroliga"
  ]
  node [
    id 515
    label "interliga"
  ]
  node [
    id 516
    label "contest"
  ]
  node [
    id 517
    label "runda"
  ]
  node [
    id 518
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 519
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 520
    label "koszyk&#243;wka"
  ]
  node [
    id 521
    label "cykl_astronomiczny"
  ]
  node [
    id 522
    label "coil"
  ]
  node [
    id 523
    label "fotoelement"
  ]
  node [
    id 524
    label "komutowanie"
  ]
  node [
    id 525
    label "stan_skupienia"
  ]
  node [
    id 526
    label "nastr&#243;j"
  ]
  node [
    id 527
    label "przerywacz"
  ]
  node [
    id 528
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 529
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 530
    label "kraw&#281;d&#378;"
  ]
  node [
    id 531
    label "obsesja"
  ]
  node [
    id 532
    label "dw&#243;jnik"
  ]
  node [
    id 533
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 534
    label "okres"
  ]
  node [
    id 535
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 536
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 537
    label "przew&#243;d"
  ]
  node [
    id 538
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 539
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 540
    label "obw&#243;d"
  ]
  node [
    id 541
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 542
    label "degree"
  ]
  node [
    id 543
    label "komutowa&#263;"
  ]
  node [
    id 544
    label "seria"
  ]
  node [
    id 545
    label "rhythm"
  ]
  node [
    id 546
    label "turniej"
  ]
  node [
    id 547
    label "okr&#261;&#380;enie"
  ]
  node [
    id 548
    label "gambit"
  ]
  node [
    id 549
    label "manewr"
  ]
  node [
    id 550
    label "uderzenie"
  ]
  node [
    id 551
    label "gra"
  ]
  node [
    id 552
    label "posuni&#281;cie"
  ]
  node [
    id 553
    label "myk"
  ]
  node [
    id 554
    label "gra_w_karty"
  ]
  node [
    id 555
    label "mecz"
  ]
  node [
    id 556
    label "travel"
  ]
  node [
    id 557
    label "zjawienie_si&#281;"
  ]
  node [
    id 558
    label "dolecenie"
  ]
  node [
    id 559
    label "strike"
  ]
  node [
    id 560
    label "dostanie_si&#281;"
  ]
  node [
    id 561
    label "wpadni&#281;cie"
  ]
  node [
    id 562
    label "spowodowanie"
  ]
  node [
    id 563
    label "pocisk"
  ]
  node [
    id 564
    label "hit"
  ]
  node [
    id 565
    label "zdarzenie_si&#281;"
  ]
  node [
    id 566
    label "sukces"
  ]
  node [
    id 567
    label "znalezienie_si&#281;"
  ]
  node [
    id 568
    label "znalezienie"
  ]
  node [
    id 569
    label "dopasowanie_si&#281;"
  ]
  node [
    id 570
    label "dotarcie"
  ]
  node [
    id 571
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 572
    label "gather"
  ]
  node [
    id 573
    label "dostanie"
  ]
  node [
    id 574
    label "zwiesna"
  ]
  node [
    id 575
    label "przedn&#243;wek"
  ]
  node [
    id 576
    label "pora_roku"
  ]
  node [
    id 577
    label "pocz&#261;tek"
  ]
  node [
    id 578
    label "nowalijka"
  ]
  node [
    id 579
    label "pierworodztwo"
  ]
  node [
    id 580
    label "upgrade"
  ]
  node [
    id 581
    label "nast&#281;pstwo"
  ]
  node [
    id 582
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 583
    label "p&#243;&#322;rocze"
  ]
  node [
    id 584
    label "martwy_sezon"
  ]
  node [
    id 585
    label "kalendarz"
  ]
  node [
    id 586
    label "lata"
  ]
  node [
    id 587
    label "stulecie"
  ]
  node [
    id 588
    label "kurs"
  ]
  node [
    id 589
    label "jubileusz"
  ]
  node [
    id 590
    label "grupa"
  ]
  node [
    id 591
    label "kwarta&#322;"
  ]
  node [
    id 592
    label "warzywo"
  ]
  node [
    id 593
    label "formacja"
  ]
  node [
    id 594
    label "dublet"
  ]
  node [
    id 595
    label "whole"
  ]
  node [
    id 596
    label "zesp&#243;&#322;"
  ]
  node [
    id 597
    label "szczep"
  ]
  node [
    id 598
    label "zast&#281;p"
  ]
  node [
    id 599
    label "pododdzia&#322;"
  ]
  node [
    id 600
    label "pluton"
  ]
  node [
    id 601
    label "force"
  ]
  node [
    id 602
    label "Bund"
  ]
  node [
    id 603
    label "Mazowsze"
  ]
  node [
    id 604
    label "PPR"
  ]
  node [
    id 605
    label "Jakobici"
  ]
  node [
    id 606
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 607
    label "leksem"
  ]
  node [
    id 608
    label "SLD"
  ]
  node [
    id 609
    label "zespolik"
  ]
  node [
    id 610
    label "Razem"
  ]
  node [
    id 611
    label "PiS"
  ]
  node [
    id 612
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 613
    label "partia"
  ]
  node [
    id 614
    label "Kuomintang"
  ]
  node [
    id 615
    label "ZSL"
  ]
  node [
    id 616
    label "szko&#322;a"
  ]
  node [
    id 617
    label "jednostka"
  ]
  node [
    id 618
    label "organizacja"
  ]
  node [
    id 619
    label "rugby"
  ]
  node [
    id 620
    label "AWS"
  ]
  node [
    id 621
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 622
    label "blok"
  ]
  node [
    id 623
    label "PO"
  ]
  node [
    id 624
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 625
    label "Federali&#347;ci"
  ]
  node [
    id 626
    label "PSL"
  ]
  node [
    id 627
    label "Wigowie"
  ]
  node [
    id 628
    label "ZChN"
  ]
  node [
    id 629
    label "rocznik"
  ]
  node [
    id 630
    label "The_Beatles"
  ]
  node [
    id 631
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 632
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 633
    label "unit"
  ]
  node [
    id 634
    label "Depeche_Mode"
  ]
  node [
    id 635
    label "forma"
  ]
  node [
    id 636
    label "odm&#322;adzanie"
  ]
  node [
    id 637
    label "&#346;wietliki"
  ]
  node [
    id 638
    label "skupienie"
  ]
  node [
    id 639
    label "odm&#322;adza&#263;"
  ]
  node [
    id 640
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 641
    label "zabudowania"
  ]
  node [
    id 642
    label "group"
  ]
  node [
    id 643
    label "schorzenie"
  ]
  node [
    id 644
    label "ro&#347;lina"
  ]
  node [
    id 645
    label "batch"
  ]
  node [
    id 646
    label "odm&#322;odzenie"
  ]
  node [
    id 647
    label "oddzia&#322;"
  ]
  node [
    id 648
    label "pu&#322;k"
  ]
  node [
    id 649
    label "dru&#380;yna_harcerska"
  ]
  node [
    id 650
    label "t&#322;um"
  ]
  node [
    id 651
    label "zoosocjologia"
  ]
  node [
    id 652
    label "Tagalowie"
  ]
  node [
    id 653
    label "Ugrowie"
  ]
  node [
    id 654
    label "Retowie"
  ]
  node [
    id 655
    label "podgromada"
  ]
  node [
    id 656
    label "Negryci"
  ]
  node [
    id 657
    label "Ladynowie"
  ]
  node [
    id 658
    label "Wizygoci"
  ]
  node [
    id 659
    label "Dogonowie"
  ]
  node [
    id 660
    label "strain"
  ]
  node [
    id 661
    label "mikrobiologia"
  ]
  node [
    id 662
    label "plemi&#281;"
  ]
  node [
    id 663
    label "linia_filogenetyczna"
  ]
  node [
    id 664
    label "grupa_organizm&#243;w"
  ]
  node [
    id 665
    label "gatunek"
  ]
  node [
    id 666
    label "Do&#322;ganie"
  ]
  node [
    id 667
    label "podrodzina"
  ]
  node [
    id 668
    label "Indoira&#324;czycy"
  ]
  node [
    id 669
    label "paleontologia"
  ]
  node [
    id 670
    label "Kozacy"
  ]
  node [
    id 671
    label "Indoariowie"
  ]
  node [
    id 672
    label "Maroni"
  ]
  node [
    id 673
    label "Kumbrowie"
  ]
  node [
    id 674
    label "Po&#322;owcy"
  ]
  node [
    id 675
    label "Nogajowie"
  ]
  node [
    id 676
    label "Nawahowie"
  ]
  node [
    id 677
    label "ornitologia"
  ]
  node [
    id 678
    label "podk&#322;ad"
  ]
  node [
    id 679
    label "Wenedowie"
  ]
  node [
    id 680
    label "Majowie"
  ]
  node [
    id 681
    label "Kipczacy"
  ]
  node [
    id 682
    label "odmiana"
  ]
  node [
    id 683
    label "Frygijczycy"
  ]
  node [
    id 684
    label "grupa_etniczna"
  ]
  node [
    id 685
    label "Paleoazjaci"
  ]
  node [
    id 686
    label "teriologia"
  ]
  node [
    id 687
    label "hufiec"
  ]
  node [
    id 688
    label "Tocharowie"
  ]
  node [
    id 689
    label "jednostka_organizacyjna"
  ]
  node [
    id 690
    label "transuranowiec"
  ]
  node [
    id 691
    label "dzia&#322;on"
  ]
  node [
    id 692
    label "aktynowiec"
  ]
  node [
    id 693
    label "bateria"
  ]
  node [
    id 694
    label "kompania"
  ]
  node [
    id 695
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 696
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 697
    label "kaftan"
  ]
  node [
    id 698
    label "zwyci&#281;stwo"
  ]
  node [
    id 699
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 700
    label "osta&#263;_si&#281;"
  ]
  node [
    id 701
    label "pozosta&#263;"
  ]
  node [
    id 702
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 703
    label "proceed"
  ]
  node [
    id 704
    label "support"
  ]
  node [
    id 705
    label "prze&#380;y&#263;"
  ]
  node [
    id 706
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 707
    label "unloose"
  ]
  node [
    id 708
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 709
    label "bring"
  ]
  node [
    id 710
    label "urzeczywistni&#263;"
  ]
  node [
    id 711
    label "undo"
  ]
  node [
    id 712
    label "usun&#261;&#263;"
  ]
  node [
    id 713
    label "wymy&#347;li&#263;"
  ]
  node [
    id 714
    label "concoct"
  ]
  node [
    id 715
    label "coating"
  ]
  node [
    id 716
    label "drop"
  ]
  node [
    id 717
    label "sko&#324;czy&#263;"
  ]
  node [
    id 718
    label "fail"
  ]
  node [
    id 719
    label "zerwa&#263;"
  ]
  node [
    id 720
    label "kill"
  ]
  node [
    id 721
    label "withdraw"
  ]
  node [
    id 722
    label "motivate"
  ]
  node [
    id 723
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 724
    label "wyrugowa&#263;"
  ]
  node [
    id 725
    label "go"
  ]
  node [
    id 726
    label "przenie&#347;&#263;"
  ]
  node [
    id 727
    label "przesun&#261;&#263;"
  ]
  node [
    id 728
    label "actualize"
  ]
  node [
    id 729
    label "nast&#281;pnie"
  ]
  node [
    id 730
    label "inny"
  ]
  node [
    id 731
    label "nastopny"
  ]
  node [
    id 732
    label "kolejno"
  ]
  node [
    id 733
    label "kt&#243;ry&#347;"
  ]
  node [
    id 734
    label "osobno"
  ]
  node [
    id 735
    label "r&#243;&#380;ny"
  ]
  node [
    id 736
    label "inszy"
  ]
  node [
    id 737
    label "inaczej"
  ]
  node [
    id 738
    label "season"
  ]
  node [
    id 739
    label "serial"
  ]
  node [
    id 740
    label "set"
  ]
  node [
    id 741
    label "stage_set"
  ]
  node [
    id 742
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 743
    label "d&#378;wi&#281;k"
  ]
  node [
    id 744
    label "komplet"
  ]
  node [
    id 745
    label "line"
  ]
  node [
    id 746
    label "sekwencja"
  ]
  node [
    id 747
    label "zestawienie"
  ]
  node [
    id 748
    label "produkcja"
  ]
  node [
    id 749
    label "program_telewizyjny"
  ]
  node [
    id 750
    label "film"
  ]
  node [
    id 751
    label "Klan"
  ]
  node [
    id 752
    label "Ranczo"
  ]
  node [
    id 753
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 754
    label "his"
  ]
  node [
    id 755
    label "ut"
  ]
  node [
    id 756
    label "C"
  ]
  node [
    id 757
    label "time"
  ]
  node [
    id 758
    label "p&#243;&#322;godzina"
  ]
  node [
    id 759
    label "jednostka_czasu"
  ]
  node [
    id 760
    label "minuta"
  ]
  node [
    id 761
    label "kwadrans"
  ]
  node [
    id 762
    label "mecz_mistrzowski"
  ]
  node [
    id 763
    label "&#347;rodowisko"
  ]
  node [
    id 764
    label "arrangement"
  ]
  node [
    id 765
    label "obrona"
  ]
  node [
    id 766
    label "pomoc"
  ]
  node [
    id 767
    label "rezerwa"
  ]
  node [
    id 768
    label "pr&#243;ba"
  ]
  node [
    id 769
    label "atak"
  ]
  node [
    id 770
    label "moneta"
  ]
  node [
    id 771
    label "union"
  ]
  node [
    id 772
    label "po&#322;o&#380;enie"
  ]
  node [
    id 773
    label "p&#322;aszczyzna"
  ]
  node [
    id 774
    label "punkt_widzenia"
  ]
  node [
    id 775
    label "kierunek"
  ]
  node [
    id 776
    label "wyk&#322;adnik"
  ]
  node [
    id 777
    label "szczebel"
  ]
  node [
    id 778
    label "budynek"
  ]
  node [
    id 779
    label "wysoko&#347;&#263;"
  ]
  node [
    id 780
    label "ranga"
  ]
  node [
    id 781
    label "class"
  ]
  node [
    id 782
    label "obiekt_naturalny"
  ]
  node [
    id 783
    label "otoczenie"
  ]
  node [
    id 784
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 785
    label "environment"
  ]
  node [
    id 786
    label "rzecz"
  ]
  node [
    id 787
    label "huczek"
  ]
  node [
    id 788
    label "ekosystem"
  ]
  node [
    id 789
    label "wszechstworzenie"
  ]
  node [
    id 790
    label "woda"
  ]
  node [
    id 791
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 792
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 793
    label "teren"
  ]
  node [
    id 794
    label "stw&#243;r"
  ]
  node [
    id 795
    label "warunki"
  ]
  node [
    id 796
    label "Ziemia"
  ]
  node [
    id 797
    label "fauna"
  ]
  node [
    id 798
    label "biota"
  ]
  node [
    id 799
    label "do&#347;wiadczenie"
  ]
  node [
    id 800
    label "spotkanie"
  ]
  node [
    id 801
    label "pobiera&#263;"
  ]
  node [
    id 802
    label "metal_szlachetny"
  ]
  node [
    id 803
    label "pobranie"
  ]
  node [
    id 804
    label "usi&#322;owanie"
  ]
  node [
    id 805
    label "pobra&#263;"
  ]
  node [
    id 806
    label "pobieranie"
  ]
  node [
    id 807
    label "znak"
  ]
  node [
    id 808
    label "rezultat"
  ]
  node [
    id 809
    label "effort"
  ]
  node [
    id 810
    label "analiza_chemiczna"
  ]
  node [
    id 811
    label "item"
  ]
  node [
    id 812
    label "sytuacja"
  ]
  node [
    id 813
    label "probiernictwo"
  ]
  node [
    id 814
    label "test"
  ]
  node [
    id 815
    label "podmiot"
  ]
  node [
    id 816
    label "struktura"
  ]
  node [
    id 817
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 818
    label "TOPR"
  ]
  node [
    id 819
    label "endecki"
  ]
  node [
    id 820
    label "przedstawicielstwo"
  ]
  node [
    id 821
    label "od&#322;am"
  ]
  node [
    id 822
    label "Cepelia"
  ]
  node [
    id 823
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 824
    label "ZBoWiD"
  ]
  node [
    id 825
    label "organization"
  ]
  node [
    id 826
    label "centrala"
  ]
  node [
    id 827
    label "GOPR"
  ]
  node [
    id 828
    label "ZOMO"
  ]
  node [
    id 829
    label "ZMP"
  ]
  node [
    id 830
    label "komitet_koordynacyjny"
  ]
  node [
    id 831
    label "przybud&#243;wka"
  ]
  node [
    id 832
    label "boj&#243;wka"
  ]
  node [
    id 833
    label "awers"
  ]
  node [
    id 834
    label "legenda"
  ]
  node [
    id 835
    label "rewers"
  ]
  node [
    id 836
    label "egzerga"
  ]
  node [
    id 837
    label "pieni&#261;dz"
  ]
  node [
    id 838
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 839
    label "otok"
  ]
  node [
    id 840
    label "balansjerka"
  ]
  node [
    id 841
    label "Entuzjastki"
  ]
  node [
    id 842
    label "kompozycja"
  ]
  node [
    id 843
    label "Terranie"
  ]
  node [
    id 844
    label "category"
  ]
  node [
    id 845
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 846
    label "cz&#261;steczka"
  ]
  node [
    id 847
    label "type"
  ]
  node [
    id 848
    label "specgrupa"
  ]
  node [
    id 849
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 850
    label "Eurogrupa"
  ]
  node [
    id 851
    label "formacja_geologiczna"
  ]
  node [
    id 852
    label "harcerze_starsi"
  ]
  node [
    id 853
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 854
    label "zas&#243;b"
  ]
  node [
    id 855
    label "nieufno&#347;&#263;"
  ]
  node [
    id 856
    label "zapasy"
  ]
  node [
    id 857
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 858
    label "resource"
  ]
  node [
    id 859
    label "&#347;rodek"
  ]
  node [
    id 860
    label "darowizna"
  ]
  node [
    id 861
    label "doch&#243;d"
  ]
  node [
    id 862
    label "telefon_zaufania"
  ]
  node [
    id 863
    label "pomocnik"
  ]
  node [
    id 864
    label "zgodzi&#263;"
  ]
  node [
    id 865
    label "property"
  ]
  node [
    id 866
    label "walka"
  ]
  node [
    id 867
    label "oznaka"
  ]
  node [
    id 868
    label "pogorszenie"
  ]
  node [
    id 869
    label "krytyka"
  ]
  node [
    id 870
    label "bat"
  ]
  node [
    id 871
    label "kaszel"
  ]
  node [
    id 872
    label "fit"
  ]
  node [
    id 873
    label "rzuci&#263;"
  ]
  node [
    id 874
    label "spasm"
  ]
  node [
    id 875
    label "wypowied&#378;"
  ]
  node [
    id 876
    label "&#380;&#261;danie"
  ]
  node [
    id 877
    label "ofensywa"
  ]
  node [
    id 878
    label "stroke"
  ]
  node [
    id 879
    label "pozycja"
  ]
  node [
    id 880
    label "rzucenie"
  ]
  node [
    id 881
    label "knock"
  ]
  node [
    id 882
    label "egzamin"
  ]
  node [
    id 883
    label "gracz"
  ]
  node [
    id 884
    label "protection"
  ]
  node [
    id 885
    label "poparcie"
  ]
  node [
    id 886
    label "reakcja"
  ]
  node [
    id 887
    label "defense"
  ]
  node [
    id 888
    label "s&#261;d"
  ]
  node [
    id 889
    label "auspices"
  ]
  node [
    id 890
    label "ochrona"
  ]
  node [
    id 891
    label "sp&#243;r"
  ]
  node [
    id 892
    label "post&#281;powanie"
  ]
  node [
    id 893
    label "defensive_structure"
  ]
  node [
    id 894
    label "guard_duty"
  ]
  node [
    id 895
    label "strona"
  ]
  node [
    id 896
    label "Wis&#322;a"
  ]
  node [
    id 897
    label "Krak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 53
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 482
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 430
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 432
  ]
  edge [
    source 17
    target 433
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 435
  ]
  edge [
    source 17
    target 436
  ]
  edge [
    source 17
    target 437
  ]
  edge [
    source 17
    target 438
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 440
  ]
  edge [
    source 17
    target 441
  ]
  edge [
    source 17
    target 442
  ]
  edge [
    source 17
    target 443
  ]
  edge [
    source 17
    target 444
  ]
  edge [
    source 17
    target 445
  ]
  edge [
    source 17
    target 446
  ]
  edge [
    source 17
    target 447
  ]
  edge [
    source 17
    target 448
  ]
  edge [
    source 17
    target 449
  ]
  edge [
    source 17
    target 450
  ]
  edge [
    source 17
    target 451
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 453
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 456
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 585
  ]
  edge [
    source 17
    target 521
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 576
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 431
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 509
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 757
  ]
  edge [
    source 19
    target 426
  ]
  edge [
    source 19
    target 758
  ]
  edge [
    source 19
    target 759
  ]
  edge [
    source 19
    target 430
  ]
  edge [
    source 19
    target 760
  ]
  edge [
    source 19
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 519
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 590
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 41
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 513
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 582
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 596
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 786
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 788
  ]
  edge [
    source 20
    target 789
  ]
  edge [
    source 20
    target 790
  ]
  edge [
    source 20
    target 791
  ]
  edge [
    source 20
    target 792
  ]
  edge [
    source 20
    target 793
  ]
  edge [
    source 20
    target 372
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 796
  ]
  edge [
    source 20
    target 797
  ]
  edge [
    source 20
    target 798
  ]
  edge [
    source 20
    target 799
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 803
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 810
  ]
  edge [
    source 20
    target 811
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 20
    target 813
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 814
  ]
  edge [
    source 20
    target 815
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 20
    target 818
  ]
  edge [
    source 20
    target 819
  ]
  edge [
    source 20
    target 820
  ]
  edge [
    source 20
    target 821
  ]
  edge [
    source 20
    target 822
  ]
  edge [
    source 20
    target 823
  ]
  edge [
    source 20
    target 824
  ]
  edge [
    source 20
    target 825
  ]
  edge [
    source 20
    target 826
  ]
  edge [
    source 20
    target 827
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 830
  ]
  edge [
    source 20
    target 831
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 38
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 20
    target 834
  ]
  edge [
    source 20
    target 835
  ]
  edge [
    source 20
    target 836
  ]
  edge [
    source 20
    target 837
  ]
  edge [
    source 20
    target 838
  ]
  edge [
    source 20
    target 839
  ]
  edge [
    source 20
    target 840
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 361
  ]
  edge [
    source 20
    target 841
  ]
  edge [
    source 20
    target 842
  ]
  edge [
    source 20
    target 843
  ]
  edge [
    source 20
    target 640
  ]
  edge [
    source 20
    target 844
  ]
  edge [
    source 20
    target 647
  ]
  edge [
    source 20
    target 845
  ]
  edge [
    source 20
    target 846
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 847
  ]
  edge [
    source 20
    target 848
  ]
  edge [
    source 20
    target 849
  ]
  edge [
    source 20
    target 637
  ]
  edge [
    source 20
    target 646
  ]
  edge [
    source 20
    target 850
  ]
  edge [
    source 20
    target 639
  ]
  edge [
    source 20
    target 851
  ]
  edge [
    source 20
    target 852
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 853
  ]
  edge [
    source 20
    target 854
  ]
  edge [
    source 20
    target 855
  ]
  edge [
    source 20
    target 856
  ]
  edge [
    source 20
    target 857
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 20
    target 859
  ]
  edge [
    source 20
    target 860
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 861
  ]
  edge [
    source 20
    target 862
  ]
  edge [
    source 20
    target 863
  ]
  edge [
    source 20
    target 864
  ]
  edge [
    source 20
    target 865
  ]
  edge [
    source 20
    target 866
  ]
  edge [
    source 20
    target 867
  ]
  edge [
    source 20
    target 868
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 869
  ]
  edge [
    source 20
    target 870
  ]
  edge [
    source 20
    target 871
  ]
  edge [
    source 20
    target 872
  ]
  edge [
    source 20
    target 873
  ]
  edge [
    source 20
    target 874
  ]
  edge [
    source 20
    target 512
  ]
  edge [
    source 20
    target 875
  ]
  edge [
    source 20
    target 876
  ]
  edge [
    source 20
    target 549
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 877
  ]
  edge [
    source 20
    target 463
  ]
  edge [
    source 20
    target 878
  ]
  edge [
    source 20
    target 879
  ]
  edge [
    source 20
    target 880
  ]
  edge [
    source 20
    target 881
  ]
  edge [
    source 20
    target 882
  ]
  edge [
    source 20
    target 883
  ]
  edge [
    source 20
    target 884
  ]
  edge [
    source 20
    target 885
  ]
  edge [
    source 20
    target 555
  ]
  edge [
    source 20
    target 886
  ]
  edge [
    source 20
    target 887
  ]
  edge [
    source 20
    target 888
  ]
  edge [
    source 20
    target 889
  ]
  edge [
    source 20
    target 551
  ]
  edge [
    source 20
    target 890
  ]
  edge [
    source 20
    target 891
  ]
  edge [
    source 20
    target 892
  ]
  edge [
    source 20
    target 893
  ]
  edge [
    source 20
    target 894
  ]
  edge [
    source 20
    target 895
  ]
  edge [
    source 896
    target 897
  ]
]
