graph [
  node [
    id 0
    label "wczoraj"
    origin "text"
  ]
  node [
    id 1
    label "apel"
    origin "text"
  ]
  node [
    id 2
    label "wykop"
    origin "text"
  ]
  node [
    id 3
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 4
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 5
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 6
    label "dawno"
  ]
  node [
    id 7
    label "doba"
  ]
  node [
    id 8
    label "niedawno"
  ]
  node [
    id 9
    label "aktualnie"
  ]
  node [
    id 10
    label "ostatni"
  ]
  node [
    id 11
    label "dawny"
  ]
  node [
    id 12
    label "d&#322;ugotrwale"
  ]
  node [
    id 13
    label "wcze&#347;niej"
  ]
  node [
    id 14
    label "ongi&#347;"
  ]
  node [
    id 15
    label "dawnie"
  ]
  node [
    id 16
    label "tydzie&#324;"
  ]
  node [
    id 17
    label "noc"
  ]
  node [
    id 18
    label "dzie&#324;"
  ]
  node [
    id 19
    label "czas"
  ]
  node [
    id 20
    label "godzina"
  ]
  node [
    id 21
    label "long_time"
  ]
  node [
    id 22
    label "jednostka_geologiczna"
  ]
  node [
    id 23
    label "pro&#347;ba"
  ]
  node [
    id 24
    label "znak"
  ]
  node [
    id 25
    label "zdyscyplinowanie"
  ]
  node [
    id 26
    label "recoil"
  ]
  node [
    id 27
    label "spotkanie"
  ]
  node [
    id 28
    label "uderzenie"
  ]
  node [
    id 29
    label "pies_my&#347;liwski"
  ]
  node [
    id 30
    label "bid"
  ]
  node [
    id 31
    label "sygna&#322;"
  ]
  node [
    id 32
    label "szermierka"
  ]
  node [
    id 33
    label "zbi&#243;rka"
  ]
  node [
    id 34
    label "doznanie"
  ]
  node [
    id 35
    label "gathering"
  ]
  node [
    id 36
    label "zawarcie"
  ]
  node [
    id 37
    label "wydarzenie"
  ]
  node [
    id 38
    label "znajomy"
  ]
  node [
    id 39
    label "powitanie"
  ]
  node [
    id 40
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 41
    label "spowodowanie"
  ]
  node [
    id 42
    label "zdarzenie_si&#281;"
  ]
  node [
    id 43
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 44
    label "znalezienie"
  ]
  node [
    id 45
    label "match"
  ]
  node [
    id 46
    label "employment"
  ]
  node [
    id 47
    label "po&#380;egnanie"
  ]
  node [
    id 48
    label "gather"
  ]
  node [
    id 49
    label "spotykanie"
  ]
  node [
    id 50
    label "spotkanie_si&#281;"
  ]
  node [
    id 51
    label "instrumentalizacja"
  ]
  node [
    id 52
    label "trafienie"
  ]
  node [
    id 53
    label "walka"
  ]
  node [
    id 54
    label "cios"
  ]
  node [
    id 55
    label "wdarcie_si&#281;"
  ]
  node [
    id 56
    label "pogorszenie"
  ]
  node [
    id 57
    label "d&#378;wi&#281;k"
  ]
  node [
    id 58
    label "poczucie"
  ]
  node [
    id 59
    label "coup"
  ]
  node [
    id 60
    label "reakcja"
  ]
  node [
    id 61
    label "contact"
  ]
  node [
    id 62
    label "stukni&#281;cie"
  ]
  node [
    id 63
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 64
    label "bat"
  ]
  node [
    id 65
    label "rush"
  ]
  node [
    id 66
    label "odbicie"
  ]
  node [
    id 67
    label "dawka"
  ]
  node [
    id 68
    label "zadanie"
  ]
  node [
    id 69
    label "&#347;ci&#281;cie"
  ]
  node [
    id 70
    label "st&#322;uczenie"
  ]
  node [
    id 71
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 72
    label "time"
  ]
  node [
    id 73
    label "odbicie_si&#281;"
  ]
  node [
    id 74
    label "dotkni&#281;cie"
  ]
  node [
    id 75
    label "charge"
  ]
  node [
    id 76
    label "dostanie"
  ]
  node [
    id 77
    label "skrytykowanie"
  ]
  node [
    id 78
    label "zagrywka"
  ]
  node [
    id 79
    label "manewr"
  ]
  node [
    id 80
    label "nast&#261;pienie"
  ]
  node [
    id 81
    label "uderzanie"
  ]
  node [
    id 82
    label "pogoda"
  ]
  node [
    id 83
    label "stroke"
  ]
  node [
    id 84
    label "pobicie"
  ]
  node [
    id 85
    label "ruch"
  ]
  node [
    id 86
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 87
    label "flap"
  ]
  node [
    id 88
    label "dotyk"
  ]
  node [
    id 89
    label "zrobienie"
  ]
  node [
    id 90
    label "podporz&#261;dkowanie"
  ]
  node [
    id 91
    label "zachowanie"
  ]
  node [
    id 92
    label "porz&#261;dek"
  ]
  node [
    id 93
    label "zachowywanie"
  ]
  node [
    id 94
    label "mores"
  ]
  node [
    id 95
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 96
    label "zachowa&#263;"
  ]
  node [
    id 97
    label "zachowywa&#263;"
  ]
  node [
    id 98
    label "nauczenie"
  ]
  node [
    id 99
    label "wypowied&#378;"
  ]
  node [
    id 100
    label "solicitation"
  ]
  node [
    id 101
    label "dow&#243;d"
  ]
  node [
    id 102
    label "oznakowanie"
  ]
  node [
    id 103
    label "fakt"
  ]
  node [
    id 104
    label "stawia&#263;"
  ]
  node [
    id 105
    label "wytw&#243;r"
  ]
  node [
    id 106
    label "point"
  ]
  node [
    id 107
    label "kodzik"
  ]
  node [
    id 108
    label "postawi&#263;"
  ]
  node [
    id 109
    label "mark"
  ]
  node [
    id 110
    label "herb"
  ]
  node [
    id 111
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 112
    label "attribute"
  ]
  node [
    id 113
    label "implikowa&#263;"
  ]
  node [
    id 114
    label "przekazywa&#263;"
  ]
  node [
    id 115
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 116
    label "pulsation"
  ]
  node [
    id 117
    label "przekazywanie"
  ]
  node [
    id 118
    label "przewodzenie"
  ]
  node [
    id 119
    label "po&#322;&#261;czenie"
  ]
  node [
    id 120
    label "fala"
  ]
  node [
    id 121
    label "doj&#347;cie"
  ]
  node [
    id 122
    label "przekazanie"
  ]
  node [
    id 123
    label "przewodzi&#263;"
  ]
  node [
    id 124
    label "zapowied&#378;"
  ]
  node [
    id 125
    label "medium_transmisyjne"
  ]
  node [
    id 126
    label "demodulacja"
  ]
  node [
    id 127
    label "doj&#347;&#263;"
  ]
  node [
    id 128
    label "przekaza&#263;"
  ]
  node [
    id 129
    label "czynnik"
  ]
  node [
    id 130
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 131
    label "aliasing"
  ]
  node [
    id 132
    label "wizja"
  ]
  node [
    id 133
    label "modulacja"
  ]
  node [
    id 134
    label "drift"
  ]
  node [
    id 135
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 136
    label "plansza"
  ]
  node [
    id 137
    label "wi&#261;zanie"
  ]
  node [
    id 138
    label "ripostowa&#263;"
  ]
  node [
    id 139
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 140
    label "czerwona_kartka"
  ]
  node [
    id 141
    label "pozycja"
  ]
  node [
    id 142
    label "czarna_kartka"
  ]
  node [
    id 143
    label "fight"
  ]
  node [
    id 144
    label "sztuka"
  ]
  node [
    id 145
    label "sport_walki"
  ]
  node [
    id 146
    label "manszeta"
  ]
  node [
    id 147
    label "ripostowanie"
  ]
  node [
    id 148
    label "tusz"
  ]
  node [
    id 149
    label "kwestarz"
  ]
  node [
    id 150
    label "kwestowanie"
  ]
  node [
    id 151
    label "collection"
  ]
  node [
    id 152
    label "koszyk&#243;wka"
  ]
  node [
    id 153
    label "chwyt"
  ]
  node [
    id 154
    label "czynno&#347;&#263;"
  ]
  node [
    id 155
    label "budowa"
  ]
  node [
    id 156
    label "zrzutowy"
  ]
  node [
    id 157
    label "odk&#322;ad"
  ]
  node [
    id 158
    label "chody"
  ]
  node [
    id 159
    label "szaniec"
  ]
  node [
    id 160
    label "wyrobisko"
  ]
  node [
    id 161
    label "kopniak"
  ]
  node [
    id 162
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 163
    label "odwa&#322;"
  ]
  node [
    id 164
    label "grodzisko"
  ]
  node [
    id 165
    label "kick"
  ]
  node [
    id 166
    label "kopni&#281;cie"
  ]
  node [
    id 167
    label "&#347;rodkowiec"
  ]
  node [
    id 168
    label "podsadzka"
  ]
  node [
    id 169
    label "obudowa"
  ]
  node [
    id 170
    label "sp&#261;g"
  ]
  node [
    id 171
    label "strop"
  ]
  node [
    id 172
    label "rabowarka"
  ]
  node [
    id 173
    label "opinka"
  ]
  node [
    id 174
    label "stojak_cierny"
  ]
  node [
    id 175
    label "kopalnia"
  ]
  node [
    id 176
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 177
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 178
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 179
    label "immersion"
  ]
  node [
    id 180
    label "umieszczenie"
  ]
  node [
    id 181
    label "zesp&#243;&#322;"
  ]
  node [
    id 182
    label "horodyszcze"
  ]
  node [
    id 183
    label "Wyszogr&#243;d"
  ]
  node [
    id 184
    label "las"
  ]
  node [
    id 185
    label "nora"
  ]
  node [
    id 186
    label "miejsce"
  ]
  node [
    id 187
    label "trasa"
  ]
  node [
    id 188
    label "usypisko"
  ]
  node [
    id 189
    label "r&#243;w"
  ]
  node [
    id 190
    label "wa&#322;"
  ]
  node [
    id 191
    label "redoubt"
  ]
  node [
    id 192
    label "fortyfikacja"
  ]
  node [
    id 193
    label "mechanika"
  ]
  node [
    id 194
    label "struktura"
  ]
  node [
    id 195
    label "figura"
  ]
  node [
    id 196
    label "miejsce_pracy"
  ]
  node [
    id 197
    label "cecha"
  ]
  node [
    id 198
    label "organ"
  ]
  node [
    id 199
    label "kreacja"
  ]
  node [
    id 200
    label "zwierz&#281;"
  ]
  node [
    id 201
    label "posesja"
  ]
  node [
    id 202
    label "konstrukcja"
  ]
  node [
    id 203
    label "wjazd"
  ]
  node [
    id 204
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 205
    label "praca"
  ]
  node [
    id 206
    label "constitution"
  ]
  node [
    id 207
    label "gleba"
  ]
  node [
    id 208
    label "p&#281;d"
  ]
  node [
    id 209
    label "zbi&#243;r"
  ]
  node [
    id 210
    label "ablegier"
  ]
  node [
    id 211
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 212
    label "layer"
  ]
  node [
    id 213
    label "r&#243;j"
  ]
  node [
    id 214
    label "mrowisko"
  ]
  node [
    id 215
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 216
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 217
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 218
    label "dzi&#347;"
  ]
  node [
    id 219
    label "teraz"
  ]
  node [
    id 220
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 221
    label "jednocze&#347;nie"
  ]
  node [
    id 222
    label "departament"
  ]
  node [
    id 223
    label "urz&#261;d"
  ]
  node [
    id 224
    label "NKWD"
  ]
  node [
    id 225
    label "ministerium"
  ]
  node [
    id 226
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 227
    label "MSW"
  ]
  node [
    id 228
    label "resort"
  ]
  node [
    id 229
    label "stanowisko"
  ]
  node [
    id 230
    label "position"
  ]
  node [
    id 231
    label "instytucja"
  ]
  node [
    id 232
    label "siedziba"
  ]
  node [
    id 233
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 234
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 235
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 236
    label "mianowaniec"
  ]
  node [
    id 237
    label "dzia&#322;"
  ]
  node [
    id 238
    label "okienko"
  ]
  node [
    id 239
    label "w&#322;adza"
  ]
  node [
    id 240
    label "jednostka_organizacyjna"
  ]
  node [
    id 241
    label "relation"
  ]
  node [
    id 242
    label "podsekcja"
  ]
  node [
    id 243
    label "ministry"
  ]
  node [
    id 244
    label "Martynika"
  ]
  node [
    id 245
    label "Gwadelupa"
  ]
  node [
    id 246
    label "Moza"
  ]
  node [
    id 247
    label "jednostka_administracyjna"
  ]
  node [
    id 248
    label "react"
  ]
  node [
    id 249
    label "dawa&#263;"
  ]
  node [
    id 250
    label "by&#263;"
  ]
  node [
    id 251
    label "ponosi&#263;"
  ]
  node [
    id 252
    label "report"
  ]
  node [
    id 253
    label "pytanie"
  ]
  node [
    id 254
    label "equate"
  ]
  node [
    id 255
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 256
    label "answer"
  ]
  node [
    id 257
    label "powodowa&#263;"
  ]
  node [
    id 258
    label "tone"
  ]
  node [
    id 259
    label "contend"
  ]
  node [
    id 260
    label "reagowa&#263;"
  ]
  node [
    id 261
    label "impart"
  ]
  node [
    id 262
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 263
    label "uczestniczy&#263;"
  ]
  node [
    id 264
    label "mie&#263;_miejsce"
  ]
  node [
    id 265
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 266
    label "motywowa&#263;"
  ]
  node [
    id 267
    label "act"
  ]
  node [
    id 268
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 269
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 270
    label "equal"
  ]
  node [
    id 271
    label "trwa&#263;"
  ]
  node [
    id 272
    label "chodzi&#263;"
  ]
  node [
    id 273
    label "si&#281;ga&#263;"
  ]
  node [
    id 274
    label "stan"
  ]
  node [
    id 275
    label "obecno&#347;&#263;"
  ]
  node [
    id 276
    label "stand"
  ]
  node [
    id 277
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 278
    label "dostarcza&#263;"
  ]
  node [
    id 279
    label "robi&#263;"
  ]
  node [
    id 280
    label "&#322;adowa&#263;"
  ]
  node [
    id 281
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 282
    label "give"
  ]
  node [
    id 283
    label "przeznacza&#263;"
  ]
  node [
    id 284
    label "surrender"
  ]
  node [
    id 285
    label "traktowa&#263;"
  ]
  node [
    id 286
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 287
    label "obiecywa&#263;"
  ]
  node [
    id 288
    label "odst&#281;powa&#263;"
  ]
  node [
    id 289
    label "tender"
  ]
  node [
    id 290
    label "rap"
  ]
  node [
    id 291
    label "umieszcza&#263;"
  ]
  node [
    id 292
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 293
    label "t&#322;uc"
  ]
  node [
    id 294
    label "powierza&#263;"
  ]
  node [
    id 295
    label "render"
  ]
  node [
    id 296
    label "wpiernicza&#263;"
  ]
  node [
    id 297
    label "exsert"
  ]
  node [
    id 298
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 299
    label "train"
  ]
  node [
    id 300
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 301
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 302
    label "p&#322;aci&#263;"
  ]
  node [
    id 303
    label "hold_out"
  ]
  node [
    id 304
    label "nalewa&#263;"
  ]
  node [
    id 305
    label "zezwala&#263;"
  ]
  node [
    id 306
    label "hold"
  ]
  node [
    id 307
    label "wst&#281;powa&#263;"
  ]
  node [
    id 308
    label "str&#243;j"
  ]
  node [
    id 309
    label "hurt"
  ]
  node [
    id 310
    label "digest"
  ]
  node [
    id 311
    label "make"
  ]
  node [
    id 312
    label "bolt"
  ]
  node [
    id 313
    label "odci&#261;ga&#263;"
  ]
  node [
    id 314
    label "doznawa&#263;"
  ]
  node [
    id 315
    label "umowa"
  ]
  node [
    id 316
    label "cover"
  ]
  node [
    id 317
    label "sprawa"
  ]
  node [
    id 318
    label "wypytanie"
  ]
  node [
    id 319
    label "egzaminowanie"
  ]
  node [
    id 320
    label "zwracanie_si&#281;"
  ]
  node [
    id 321
    label "wywo&#322;ywanie"
  ]
  node [
    id 322
    label "rozpytywanie"
  ]
  node [
    id 323
    label "wypowiedzenie"
  ]
  node [
    id 324
    label "problemat"
  ]
  node [
    id 325
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 326
    label "problematyka"
  ]
  node [
    id 327
    label "sprawdzian"
  ]
  node [
    id 328
    label "przes&#322;uchiwanie"
  ]
  node [
    id 329
    label "question"
  ]
  node [
    id 330
    label "sprawdzanie"
  ]
  node [
    id 331
    label "odpowiadanie"
  ]
  node [
    id 332
    label "survey"
  ]
  node [
    id 333
    label "puls"
  ]
  node [
    id 334
    label "biznes"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 333
    target 334
  ]
]
