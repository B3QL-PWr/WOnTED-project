graph [
  node [
    id 0
    label "proporcjonalny"
    origin "text"
  ]
  node [
    id 1
    label "podzia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "zysk"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "stosowa&#263;by"
    origin "text"
  ]
  node [
    id 5
    label "niezale&#380;ny"
    origin "text"
  ]
  node [
    id 6
    label "podmiot"
    origin "text"
  ]
  node [
    id 7
    label "uczestniczy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dana"
    origin "text"
  ]
  node [
    id 9
    label "transakcja"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "pomoc"
    origin "text"
  ]
  node [
    id 12
    label "prosty"
  ]
  node [
    id 13
    label "analogiczny"
  ]
  node [
    id 14
    label "harmonijny"
  ]
  node [
    id 15
    label "foremny"
  ]
  node [
    id 16
    label "proporcjonalnie"
  ]
  node [
    id 17
    label "jednakowy"
  ]
  node [
    id 18
    label "adekwatny"
  ]
  node [
    id 19
    label "nale&#380;yty"
  ]
  node [
    id 20
    label "adekwatnie"
  ]
  node [
    id 21
    label "dobry"
  ]
  node [
    id 22
    label "taki&#380;"
  ]
  node [
    id 23
    label "identyczny"
  ]
  node [
    id 24
    label "zr&#243;wnanie"
  ]
  node [
    id 25
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 26
    label "zr&#243;wnywanie"
  ]
  node [
    id 27
    label "mundurowanie"
  ]
  node [
    id 28
    label "mundurowa&#263;"
  ]
  node [
    id 29
    label "jednakowo"
  ]
  node [
    id 30
    label "po_prostu"
  ]
  node [
    id 31
    label "naiwny"
  ]
  node [
    id 32
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 33
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 34
    label "prostowanie"
  ]
  node [
    id 35
    label "skromny"
  ]
  node [
    id 36
    label "prostoduszny"
  ]
  node [
    id 37
    label "zwyk&#322;y"
  ]
  node [
    id 38
    label "&#322;atwy"
  ]
  node [
    id 39
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 40
    label "rozprostowanie"
  ]
  node [
    id 41
    label "prostowanie_si&#281;"
  ]
  node [
    id 42
    label "cios"
  ]
  node [
    id 43
    label "naturalny"
  ]
  node [
    id 44
    label "niepozorny"
  ]
  node [
    id 45
    label "prosto"
  ]
  node [
    id 46
    label "analogicznie"
  ]
  node [
    id 47
    label "podobny"
  ]
  node [
    id 48
    label "zgodny"
  ]
  node [
    id 49
    label "harmonijnie"
  ]
  node [
    id 50
    label "spokojny"
  ]
  node [
    id 51
    label "p&#322;ynny"
  ]
  node [
    id 52
    label "udany"
  ]
  node [
    id 53
    label "sp&#243;jny"
  ]
  node [
    id 54
    label "zharmonizowanie_si&#281;"
  ]
  node [
    id 55
    label "foremnie"
  ]
  node [
    id 56
    label "regularny"
  ]
  node [
    id 57
    label "pomiernie"
  ]
  node [
    id 58
    label "zale&#380;nie"
  ]
  node [
    id 59
    label "distribution"
  ]
  node [
    id 60
    label "wydarzenie"
  ]
  node [
    id 61
    label "stopie&#324;"
  ]
  node [
    id 62
    label "wytw&#243;r"
  ]
  node [
    id 63
    label "competence"
  ]
  node [
    id 64
    label "fission"
  ]
  node [
    id 65
    label "blastogeneza"
  ]
  node [
    id 66
    label "eksdywizja"
  ]
  node [
    id 67
    label "charakter"
  ]
  node [
    id 68
    label "przebiegni&#281;cie"
  ]
  node [
    id 69
    label "przebiec"
  ]
  node [
    id 70
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 71
    label "motyw"
  ]
  node [
    id 72
    label "fabu&#322;a"
  ]
  node [
    id 73
    label "czynno&#347;&#263;"
  ]
  node [
    id 74
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 75
    label "przedmiot"
  ]
  node [
    id 76
    label "rezultat"
  ]
  node [
    id 77
    label "p&#322;&#243;d"
  ]
  node [
    id 78
    label "work"
  ]
  node [
    id 79
    label "szczebel"
  ]
  node [
    id 80
    label "d&#378;wi&#281;k"
  ]
  node [
    id 81
    label "podstopie&#324;"
  ]
  node [
    id 82
    label "forma"
  ]
  node [
    id 83
    label "minuta"
  ]
  node [
    id 84
    label "podn&#243;&#380;ek"
  ]
  node [
    id 85
    label "przymiotnik"
  ]
  node [
    id 86
    label "gama"
  ]
  node [
    id 87
    label "element"
  ]
  node [
    id 88
    label "znaczenie"
  ]
  node [
    id 89
    label "miejsce"
  ]
  node [
    id 90
    label "kategoria_gramatyczna"
  ]
  node [
    id 91
    label "wschodek"
  ]
  node [
    id 92
    label "ocena"
  ]
  node [
    id 93
    label "degree"
  ]
  node [
    id 94
    label "poziom"
  ]
  node [
    id 95
    label "wielko&#347;&#263;"
  ]
  node [
    id 96
    label "jednostka"
  ]
  node [
    id 97
    label "rank"
  ]
  node [
    id 98
    label "przys&#322;&#243;wek"
  ]
  node [
    id 99
    label "schody"
  ]
  node [
    id 100
    label "kszta&#322;t"
  ]
  node [
    id 101
    label "dobro"
  ]
  node [
    id 102
    label "zaleta"
  ]
  node [
    id 103
    label "doch&#243;d"
  ]
  node [
    id 104
    label "krzywa_Engla"
  ]
  node [
    id 105
    label "wp&#322;yw"
  ]
  node [
    id 106
    label "korzy&#347;&#263;"
  ]
  node [
    id 107
    label "income"
  ]
  node [
    id 108
    label "stopa_procentowa"
  ]
  node [
    id 109
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 110
    label "rzecz"
  ]
  node [
    id 111
    label "kalokagatia"
  ]
  node [
    id 112
    label "dobra"
  ]
  node [
    id 113
    label "dobro&#263;"
  ]
  node [
    id 114
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 115
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 116
    label "cel"
  ]
  node [
    id 117
    label "go&#322;&#261;bek"
  ]
  node [
    id 118
    label "warto&#347;&#263;"
  ]
  node [
    id 119
    label "despond"
  ]
  node [
    id 120
    label "g&#322;agolica"
  ]
  node [
    id 121
    label "litera"
  ]
  node [
    id 122
    label "rewaluowanie"
  ]
  node [
    id 123
    label "zrewaluowa&#263;"
  ]
  node [
    id 124
    label "rewaluowa&#263;"
  ]
  node [
    id 125
    label "wabik"
  ]
  node [
    id 126
    label "strona"
  ]
  node [
    id 127
    label "zrewaluowanie"
  ]
  node [
    id 128
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 129
    label "niezale&#380;nie"
  ]
  node [
    id 130
    label "usamodzielnienie"
  ]
  node [
    id 131
    label "usamodzielnianie"
  ]
  node [
    id 132
    label "emancipation"
  ]
  node [
    id 133
    label "uwolnienie"
  ]
  node [
    id 134
    label "uwalnianie"
  ]
  node [
    id 135
    label "cz&#322;owiek"
  ]
  node [
    id 136
    label "byt"
  ]
  node [
    id 137
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 138
    label "nauka_prawa"
  ]
  node [
    id 139
    label "prawo"
  ]
  node [
    id 140
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 141
    label "organizacja"
  ]
  node [
    id 142
    label "osobowo&#347;&#263;"
  ]
  node [
    id 143
    label "asymilowa&#263;"
  ]
  node [
    id 144
    label "nasada"
  ]
  node [
    id 145
    label "profanum"
  ]
  node [
    id 146
    label "wz&#243;r"
  ]
  node [
    id 147
    label "senior"
  ]
  node [
    id 148
    label "asymilowanie"
  ]
  node [
    id 149
    label "os&#322;abia&#263;"
  ]
  node [
    id 150
    label "homo_sapiens"
  ]
  node [
    id 151
    label "osoba"
  ]
  node [
    id 152
    label "ludzko&#347;&#263;"
  ]
  node [
    id 153
    label "Adam"
  ]
  node [
    id 154
    label "hominid"
  ]
  node [
    id 155
    label "posta&#263;"
  ]
  node [
    id 156
    label "portrecista"
  ]
  node [
    id 157
    label "polifag"
  ]
  node [
    id 158
    label "podw&#322;adny"
  ]
  node [
    id 159
    label "dwun&#243;g"
  ]
  node [
    id 160
    label "wapniak"
  ]
  node [
    id 161
    label "duch"
  ]
  node [
    id 162
    label "os&#322;abianie"
  ]
  node [
    id 163
    label "antropochoria"
  ]
  node [
    id 164
    label "figura"
  ]
  node [
    id 165
    label "g&#322;owa"
  ]
  node [
    id 166
    label "mikrokosmos"
  ]
  node [
    id 167
    label "oddzia&#322;ywanie"
  ]
  node [
    id 168
    label "ontologicznie"
  ]
  node [
    id 169
    label "utrzyma&#263;"
  ]
  node [
    id 170
    label "bycie"
  ]
  node [
    id 171
    label "utrzymanie"
  ]
  node [
    id 172
    label "utrzymywanie"
  ]
  node [
    id 173
    label "entity"
  ]
  node [
    id 174
    label "utrzymywa&#263;"
  ]
  node [
    id 175
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 176
    label "egzystencja"
  ]
  node [
    id 177
    label "wy&#380;ywienie"
  ]
  node [
    id 178
    label "potencja"
  ]
  node [
    id 179
    label "subsystencja"
  ]
  node [
    id 180
    label "przybud&#243;wka"
  ]
  node [
    id 181
    label "struktura"
  ]
  node [
    id 182
    label "organization"
  ]
  node [
    id 183
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 184
    label "od&#322;am"
  ]
  node [
    id 185
    label "TOPR"
  ]
  node [
    id 186
    label "komitet_koordynacyjny"
  ]
  node [
    id 187
    label "przedstawicielstwo"
  ]
  node [
    id 188
    label "ZMP"
  ]
  node [
    id 189
    label "Cepelia"
  ]
  node [
    id 190
    label "GOPR"
  ]
  node [
    id 191
    label "endecki"
  ]
  node [
    id 192
    label "ZBoWiD"
  ]
  node [
    id 193
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 194
    label "boj&#243;wka"
  ]
  node [
    id 195
    label "ZOMO"
  ]
  node [
    id 196
    label "zesp&#243;&#322;"
  ]
  node [
    id 197
    label "jednostka_organizacyjna"
  ]
  node [
    id 198
    label "centrala"
  ]
  node [
    id 199
    label "mentalno&#347;&#263;"
  ]
  node [
    id 200
    label "superego"
  ]
  node [
    id 201
    label "cecha"
  ]
  node [
    id 202
    label "self"
  ]
  node [
    id 203
    label "wn&#281;trze"
  ]
  node [
    id 204
    label "psychika"
  ]
  node [
    id 205
    label "wyj&#261;tkowy"
  ]
  node [
    id 206
    label "status"
  ]
  node [
    id 207
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 208
    label "kanonistyka"
  ]
  node [
    id 209
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 210
    label "kazuistyka"
  ]
  node [
    id 211
    label "legislacyjnie"
  ]
  node [
    id 212
    label "zasada_d'Alemberta"
  ]
  node [
    id 213
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 214
    label "procesualistyka"
  ]
  node [
    id 215
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 216
    label "prawo_karne"
  ]
  node [
    id 217
    label "opis"
  ]
  node [
    id 218
    label "regu&#322;a_Allena"
  ]
  node [
    id 219
    label "kryminalistyka"
  ]
  node [
    id 220
    label "prawo_Mendla"
  ]
  node [
    id 221
    label "criterion"
  ]
  node [
    id 222
    label "standard"
  ]
  node [
    id 223
    label "obserwacja"
  ]
  node [
    id 224
    label "szko&#322;a"
  ]
  node [
    id 225
    label "kultura_duchowa"
  ]
  node [
    id 226
    label "normatywizm"
  ]
  node [
    id 227
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 228
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 229
    label "umocowa&#263;"
  ]
  node [
    id 230
    label "cywilistyka"
  ]
  node [
    id 231
    label "jurisprudence"
  ]
  node [
    id 232
    label "regu&#322;a_Glogera"
  ]
  node [
    id 233
    label "kryminologia"
  ]
  node [
    id 234
    label "zasada"
  ]
  node [
    id 235
    label "law"
  ]
  node [
    id 236
    label "qualification"
  ]
  node [
    id 237
    label "judykatura"
  ]
  node [
    id 238
    label "przepis"
  ]
  node [
    id 239
    label "prawo_karne_procesowe"
  ]
  node [
    id 240
    label "normalizacja"
  ]
  node [
    id 241
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 242
    label "wykonawczy"
  ]
  node [
    id 243
    label "dominion"
  ]
  node [
    id 244
    label "twierdzenie"
  ]
  node [
    id 245
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 246
    label "kierunek"
  ]
  node [
    id 247
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 248
    label "robi&#263;"
  ]
  node [
    id 249
    label "by&#263;"
  ]
  node [
    id 250
    label "participate"
  ]
  node [
    id 251
    label "oszukiwa&#263;"
  ]
  node [
    id 252
    label "tentegowa&#263;"
  ]
  node [
    id 253
    label "urz&#261;dza&#263;"
  ]
  node [
    id 254
    label "praca"
  ]
  node [
    id 255
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 256
    label "czyni&#263;"
  ]
  node [
    id 257
    label "przerabia&#263;"
  ]
  node [
    id 258
    label "act"
  ]
  node [
    id 259
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 260
    label "give"
  ]
  node [
    id 261
    label "post&#281;powa&#263;"
  ]
  node [
    id 262
    label "peddle"
  ]
  node [
    id 263
    label "organizowa&#263;"
  ]
  node [
    id 264
    label "falowa&#263;"
  ]
  node [
    id 265
    label "stylizowa&#263;"
  ]
  node [
    id 266
    label "wydala&#263;"
  ]
  node [
    id 267
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 268
    label "ukazywa&#263;"
  ]
  node [
    id 269
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 270
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 271
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 272
    label "stan"
  ]
  node [
    id 273
    label "stand"
  ]
  node [
    id 274
    label "trwa&#263;"
  ]
  node [
    id 275
    label "equal"
  ]
  node [
    id 276
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 277
    label "chodzi&#263;"
  ]
  node [
    id 278
    label "obecno&#347;&#263;"
  ]
  node [
    id 279
    label "si&#281;ga&#263;"
  ]
  node [
    id 280
    label "mie&#263;_miejsce"
  ]
  node [
    id 281
    label "dar"
  ]
  node [
    id 282
    label "cnota"
  ]
  node [
    id 283
    label "buddyzm"
  ]
  node [
    id 284
    label "da&#324;"
  ]
  node [
    id 285
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 286
    label "dyspozycja"
  ]
  node [
    id 287
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 288
    label "faculty"
  ]
  node [
    id 289
    label "stygmat"
  ]
  node [
    id 290
    label "honesty"
  ]
  node [
    id 291
    label "panie&#324;stwo"
  ]
  node [
    id 292
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 293
    label "aretologia"
  ]
  node [
    id 294
    label "Buddhism"
  ]
  node [
    id 295
    label "wad&#378;rajana"
  ]
  node [
    id 296
    label "asura"
  ]
  node [
    id 297
    label "tantryzm"
  ]
  node [
    id 298
    label "therawada"
  ]
  node [
    id 299
    label "mahajana"
  ]
  node [
    id 300
    label "kalpa"
  ]
  node [
    id 301
    label "li"
  ]
  node [
    id 302
    label "maja"
  ]
  node [
    id 303
    label "bardo"
  ]
  node [
    id 304
    label "ahinsa"
  ]
  node [
    id 305
    label "religia"
  ]
  node [
    id 306
    label "lampka_ma&#347;lana"
  ]
  node [
    id 307
    label "arahant"
  ]
  node [
    id 308
    label "hinajana"
  ]
  node [
    id 309
    label "bonzo"
  ]
  node [
    id 310
    label "zam&#243;wienie"
  ]
  node [
    id 311
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 312
    label "arbitra&#380;"
  ]
  node [
    id 313
    label "facjenda"
  ]
  node [
    id 314
    label "kontrakt_terminowy"
  ]
  node [
    id 315
    label "cena_transferowa"
  ]
  node [
    id 316
    label "bezproblemowy"
  ]
  node [
    id 317
    label "activity"
  ]
  node [
    id 318
    label "zarezerwowanie"
  ]
  node [
    id 319
    label "zg&#322;oszenie"
  ]
  node [
    id 320
    label "perpetration"
  ]
  node [
    id 321
    label "rozdysponowanie"
  ]
  node [
    id 322
    label "zlecenie"
  ]
  node [
    id 323
    label "order"
  ]
  node [
    id 324
    label "zamawianie"
  ]
  node [
    id 325
    label "indent"
  ]
  node [
    id 326
    label "zaczarowanie"
  ]
  node [
    id 327
    label "zamawia&#263;"
  ]
  node [
    id 328
    label "zam&#243;wi&#263;"
  ]
  node [
    id 329
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 330
    label "polecenie"
  ]
  node [
    id 331
    label "pojednawstwo"
  ]
  node [
    id 332
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 333
    label "kurs_walutowy"
  ]
  node [
    id 334
    label "telefon_zaufania"
  ]
  node [
    id 335
    label "property"
  ]
  node [
    id 336
    label "&#347;rodek"
  ]
  node [
    id 337
    label "liga"
  ]
  node [
    id 338
    label "zgodzi&#263;"
  ]
  node [
    id 339
    label "darowizna"
  ]
  node [
    id 340
    label "pomocnik"
  ]
  node [
    id 341
    label "grupa"
  ]
  node [
    id 342
    label "discipline"
  ]
  node [
    id 343
    label "zboczy&#263;"
  ]
  node [
    id 344
    label "w&#261;tek"
  ]
  node [
    id 345
    label "kultura"
  ]
  node [
    id 346
    label "sponiewiera&#263;"
  ]
  node [
    id 347
    label "zboczenie"
  ]
  node [
    id 348
    label "zbaczanie"
  ]
  node [
    id 349
    label "thing"
  ]
  node [
    id 350
    label "om&#243;wi&#263;"
  ]
  node [
    id 351
    label "tre&#347;&#263;"
  ]
  node [
    id 352
    label "kr&#261;&#380;enie"
  ]
  node [
    id 353
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 354
    label "istota"
  ]
  node [
    id 355
    label "zbacza&#263;"
  ]
  node [
    id 356
    label "om&#243;wienie"
  ]
  node [
    id 357
    label "tematyka"
  ]
  node [
    id 358
    label "omawianie"
  ]
  node [
    id 359
    label "omawia&#263;"
  ]
  node [
    id 360
    label "robienie"
  ]
  node [
    id 361
    label "program_nauczania"
  ]
  node [
    id 362
    label "sponiewieranie"
  ]
  node [
    id 363
    label "punkt"
  ]
  node [
    id 364
    label "spos&#243;b"
  ]
  node [
    id 365
    label "chemikalia"
  ]
  node [
    id 366
    label "abstrakcja"
  ]
  node [
    id 367
    label "czas"
  ]
  node [
    id 368
    label "substancja"
  ]
  node [
    id 369
    label "gracz"
  ]
  node [
    id 370
    label "wrzosowate"
  ]
  node [
    id 371
    label "pomagacz"
  ]
  node [
    id 372
    label "zawodnik"
  ]
  node [
    id 373
    label "bylina"
  ]
  node [
    id 374
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 375
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 376
    label "r&#281;ka"
  ]
  node [
    id 377
    label "kredens"
  ]
  node [
    id 378
    label "kompozycja"
  ]
  node [
    id 379
    label "pakiet_klimatyczny"
  ]
  node [
    id 380
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 381
    label "type"
  ]
  node [
    id 382
    label "cz&#261;steczka"
  ]
  node [
    id 383
    label "gromada"
  ]
  node [
    id 384
    label "specgrupa"
  ]
  node [
    id 385
    label "egzemplarz"
  ]
  node [
    id 386
    label "stage_set"
  ]
  node [
    id 387
    label "zbi&#243;r"
  ]
  node [
    id 388
    label "odm&#322;odzenie"
  ]
  node [
    id 389
    label "odm&#322;adza&#263;"
  ]
  node [
    id 390
    label "harcerze_starsi"
  ]
  node [
    id 391
    label "jednostka_systematyczna"
  ]
  node [
    id 392
    label "oddzia&#322;"
  ]
  node [
    id 393
    label "category"
  ]
  node [
    id 394
    label "&#346;wietliki"
  ]
  node [
    id 395
    label "formacja_geologiczna"
  ]
  node [
    id 396
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 397
    label "Eurogrupa"
  ]
  node [
    id 398
    label "Terranie"
  ]
  node [
    id 399
    label "odm&#322;adzanie"
  ]
  node [
    id 400
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 401
    label "Entuzjastki"
  ]
  node [
    id 402
    label "zapomoga"
  ]
  node [
    id 403
    label "przeniesienie_praw"
  ]
  node [
    id 404
    label "zgodzenie"
  ]
  node [
    id 405
    label "zatrudni&#263;"
  ]
  node [
    id 406
    label "zgadza&#263;"
  ]
  node [
    id 407
    label "&#347;rodowisko"
  ]
  node [
    id 408
    label "moneta"
  ]
  node [
    id 409
    label "rezerwa"
  ]
  node [
    id 410
    label "arrangement"
  ]
  node [
    id 411
    label "obrona"
  ]
  node [
    id 412
    label "pr&#243;ba"
  ]
  node [
    id 413
    label "atak"
  ]
  node [
    id 414
    label "mecz_mistrzowski"
  ]
  node [
    id 415
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 416
    label "union"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
]
