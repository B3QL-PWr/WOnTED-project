graph [
  node [
    id 0
    label "sukces"
    origin "text"
  ]
  node [
    id 1
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 2
    label "kamizelka"
    origin "text"
  ]
  node [
    id 3
    label "kobieta_sukcesu"
  ]
  node [
    id 4
    label "success"
  ]
  node [
    id 5
    label "rezultat"
  ]
  node [
    id 6
    label "passa"
  ]
  node [
    id 7
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 8
    label "uzyskanie"
  ]
  node [
    id 9
    label "dochrapanie_si&#281;"
  ]
  node [
    id 10
    label "skill"
  ]
  node [
    id 11
    label "accomplishment"
  ]
  node [
    id 12
    label "zdarzenie_si&#281;"
  ]
  node [
    id 13
    label "zaawansowanie"
  ]
  node [
    id 14
    label "dotarcie"
  ]
  node [
    id 15
    label "act"
  ]
  node [
    id 16
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 17
    label "dzia&#322;anie"
  ]
  node [
    id 18
    label "typ"
  ]
  node [
    id 19
    label "event"
  ]
  node [
    id 20
    label "przyczyna"
  ]
  node [
    id 21
    label "przebieg"
  ]
  node [
    id 22
    label "pora&#380;ka"
  ]
  node [
    id 23
    label "continuum"
  ]
  node [
    id 24
    label "ci&#261;g&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "ci&#261;g"
  ]
  node [
    id 26
    label "typ_mongoloidalny"
  ]
  node [
    id 27
    label "kolorowy"
  ]
  node [
    id 28
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 29
    label "ciep&#322;y"
  ]
  node [
    id 30
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 31
    label "jasny"
  ]
  node [
    id 32
    label "zabarwienie_si&#281;"
  ]
  node [
    id 33
    label "ciekawy"
  ]
  node [
    id 34
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 37
    label "weso&#322;y"
  ]
  node [
    id 38
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 39
    label "barwienie"
  ]
  node [
    id 40
    label "kolorowo"
  ]
  node [
    id 41
    label "barwnie"
  ]
  node [
    id 42
    label "kolorowanie"
  ]
  node [
    id 43
    label "barwisty"
  ]
  node [
    id 44
    label "przyjemny"
  ]
  node [
    id 45
    label "barwienie_si&#281;"
  ]
  node [
    id 46
    label "pi&#281;kny"
  ]
  node [
    id 47
    label "ubarwienie"
  ]
  node [
    id 48
    label "mi&#322;y"
  ]
  node [
    id 49
    label "ocieplanie_si&#281;"
  ]
  node [
    id 50
    label "ocieplanie"
  ]
  node [
    id 51
    label "grzanie"
  ]
  node [
    id 52
    label "ocieplenie_si&#281;"
  ]
  node [
    id 53
    label "zagrzanie"
  ]
  node [
    id 54
    label "ocieplenie"
  ]
  node [
    id 55
    label "korzystny"
  ]
  node [
    id 56
    label "ciep&#322;o"
  ]
  node [
    id 57
    label "dobry"
  ]
  node [
    id 58
    label "o&#347;wietlenie"
  ]
  node [
    id 59
    label "szczery"
  ]
  node [
    id 60
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 61
    label "jasno"
  ]
  node [
    id 62
    label "o&#347;wietlanie"
  ]
  node [
    id 63
    label "przytomny"
  ]
  node [
    id 64
    label "zrozumia&#322;y"
  ]
  node [
    id 65
    label "niezm&#261;cony"
  ]
  node [
    id 66
    label "bia&#322;y"
  ]
  node [
    id 67
    label "jednoznaczny"
  ]
  node [
    id 68
    label "klarowny"
  ]
  node [
    id 69
    label "pogodny"
  ]
  node [
    id 70
    label "odcinanie_si&#281;"
  ]
  node [
    id 71
    label "g&#243;ra"
  ]
  node [
    id 72
    label "westa"
  ]
  node [
    id 73
    label "przedmiot"
  ]
  node [
    id 74
    label "przelezienie"
  ]
  node [
    id 75
    label "&#347;piew"
  ]
  node [
    id 76
    label "Synaj"
  ]
  node [
    id 77
    label "Kreml"
  ]
  node [
    id 78
    label "d&#378;wi&#281;k"
  ]
  node [
    id 79
    label "kierunek"
  ]
  node [
    id 80
    label "wysoki"
  ]
  node [
    id 81
    label "element"
  ]
  node [
    id 82
    label "wzniesienie"
  ]
  node [
    id 83
    label "grupa"
  ]
  node [
    id 84
    label "pi&#281;tro"
  ]
  node [
    id 85
    label "Ropa"
  ]
  node [
    id 86
    label "kupa"
  ]
  node [
    id 87
    label "przele&#378;&#263;"
  ]
  node [
    id 88
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 89
    label "karczek"
  ]
  node [
    id 90
    label "rami&#261;czko"
  ]
  node [
    id 91
    label "Jaworze"
  ]
  node [
    id 92
    label "cz&#281;&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
]
