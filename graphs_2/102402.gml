graph [
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszyscy"
    origin "text"
  ]
  node [
    id 2
    label "lektura"
    origin "text"
  ]
  node [
    id 3
    label "nowy"
    origin "text"
  ]
  node [
    id 4
    label "numer"
    origin "text"
  ]
  node [
    id 5
    label "outro"
    origin "text"
  ]
  node [
    id 6
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 7
    label "invite"
  ]
  node [
    id 8
    label "ask"
  ]
  node [
    id 9
    label "oferowa&#263;"
  ]
  node [
    id 10
    label "zach&#281;ca&#263;"
  ]
  node [
    id 11
    label "volunteer"
  ]
  node [
    id 12
    label "recitation"
  ]
  node [
    id 13
    label "wczytywanie_si&#281;"
  ]
  node [
    id 14
    label "poczytanie"
  ]
  node [
    id 15
    label "doczytywanie"
  ]
  node [
    id 16
    label "zaczytanie_si&#281;"
  ]
  node [
    id 17
    label "czytywanie"
  ]
  node [
    id 18
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 19
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 20
    label "tekst"
  ]
  node [
    id 21
    label "poznawanie"
  ]
  node [
    id 22
    label "wyczytywanie"
  ]
  node [
    id 23
    label "oczytywanie_si&#281;"
  ]
  node [
    id 24
    label "egzemplarz"
  ]
  node [
    id 25
    label "rozdzia&#322;"
  ]
  node [
    id 26
    label "wk&#322;ad"
  ]
  node [
    id 27
    label "tytu&#322;"
  ]
  node [
    id 28
    label "zak&#322;adka"
  ]
  node [
    id 29
    label "nomina&#322;"
  ]
  node [
    id 30
    label "ok&#322;adka"
  ]
  node [
    id 31
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 32
    label "wydawnictwo"
  ]
  node [
    id 33
    label "ekslibris"
  ]
  node [
    id 34
    label "przek&#322;adacz"
  ]
  node [
    id 35
    label "bibliofilstwo"
  ]
  node [
    id 36
    label "falc"
  ]
  node [
    id 37
    label "pagina"
  ]
  node [
    id 38
    label "zw&#243;j"
  ]
  node [
    id 39
    label "uczenie_si&#281;"
  ]
  node [
    id 40
    label "cognition"
  ]
  node [
    id 41
    label "proces"
  ]
  node [
    id 42
    label "znajomy"
  ]
  node [
    id 43
    label "umo&#380;liwianie"
  ]
  node [
    id 44
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 45
    label "rozr&#243;&#380;nianie"
  ]
  node [
    id 46
    label "zapoznawanie"
  ]
  node [
    id 47
    label "robienie"
  ]
  node [
    id 48
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 49
    label "designation"
  ]
  node [
    id 50
    label "inclusion"
  ]
  node [
    id 51
    label "zjawisko"
  ]
  node [
    id 52
    label "czucie"
  ]
  node [
    id 53
    label "recognition"
  ]
  node [
    id 54
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 55
    label "spotykanie"
  ]
  node [
    id 56
    label "merging"
  ]
  node [
    id 57
    label "przep&#322;ywanie"
  ]
  node [
    id 58
    label "zawieranie"
  ]
  node [
    id 59
    label "ekscerpcja"
  ]
  node [
    id 60
    label "j&#281;zykowo"
  ]
  node [
    id 61
    label "wypowied&#378;"
  ]
  node [
    id 62
    label "redakcja"
  ]
  node [
    id 63
    label "wytw&#243;r"
  ]
  node [
    id 64
    label "pomini&#281;cie"
  ]
  node [
    id 65
    label "dzie&#322;o"
  ]
  node [
    id 66
    label "preparacja"
  ]
  node [
    id 67
    label "odmianka"
  ]
  node [
    id 68
    label "opu&#347;ci&#263;"
  ]
  node [
    id 69
    label "koniektura"
  ]
  node [
    id 70
    label "pisa&#263;"
  ]
  node [
    id 71
    label "obelga"
  ]
  node [
    id 72
    label "podawanie"
  ]
  node [
    id 73
    label "czytanie"
  ]
  node [
    id 74
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 75
    label "uznanie"
  ]
  node [
    id 76
    label "ko&#324;czenie"
  ]
  node [
    id 77
    label "kolejny"
  ]
  node [
    id 78
    label "nowo"
  ]
  node [
    id 79
    label "cz&#322;owiek"
  ]
  node [
    id 80
    label "bie&#380;&#261;cy"
  ]
  node [
    id 81
    label "drugi"
  ]
  node [
    id 82
    label "narybek"
  ]
  node [
    id 83
    label "obcy"
  ]
  node [
    id 84
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 85
    label "nowotny"
  ]
  node [
    id 86
    label "nadprzyrodzony"
  ]
  node [
    id 87
    label "nieznany"
  ]
  node [
    id 88
    label "pozaludzki"
  ]
  node [
    id 89
    label "obco"
  ]
  node [
    id 90
    label "tameczny"
  ]
  node [
    id 91
    label "osoba"
  ]
  node [
    id 92
    label "nieznajomo"
  ]
  node [
    id 93
    label "inny"
  ]
  node [
    id 94
    label "cudzy"
  ]
  node [
    id 95
    label "istota_&#380;ywa"
  ]
  node [
    id 96
    label "zaziemsko"
  ]
  node [
    id 97
    label "jednoczesny"
  ]
  node [
    id 98
    label "unowocze&#347;nianie"
  ]
  node [
    id 99
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 100
    label "tera&#378;niejszy"
  ]
  node [
    id 101
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 102
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 103
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 104
    label "nast&#281;pnie"
  ]
  node [
    id 105
    label "nastopny"
  ]
  node [
    id 106
    label "kolejno"
  ]
  node [
    id 107
    label "kt&#243;ry&#347;"
  ]
  node [
    id 108
    label "sw&#243;j"
  ]
  node [
    id 109
    label "przeciwny"
  ]
  node [
    id 110
    label "wt&#243;ry"
  ]
  node [
    id 111
    label "dzie&#324;"
  ]
  node [
    id 112
    label "odwrotnie"
  ]
  node [
    id 113
    label "podobny"
  ]
  node [
    id 114
    label "bie&#380;&#261;co"
  ]
  node [
    id 115
    label "ci&#261;g&#322;y"
  ]
  node [
    id 116
    label "aktualny"
  ]
  node [
    id 117
    label "ludzko&#347;&#263;"
  ]
  node [
    id 118
    label "asymilowanie"
  ]
  node [
    id 119
    label "wapniak"
  ]
  node [
    id 120
    label "asymilowa&#263;"
  ]
  node [
    id 121
    label "os&#322;abia&#263;"
  ]
  node [
    id 122
    label "posta&#263;"
  ]
  node [
    id 123
    label "hominid"
  ]
  node [
    id 124
    label "podw&#322;adny"
  ]
  node [
    id 125
    label "os&#322;abianie"
  ]
  node [
    id 126
    label "g&#322;owa"
  ]
  node [
    id 127
    label "figura"
  ]
  node [
    id 128
    label "portrecista"
  ]
  node [
    id 129
    label "dwun&#243;g"
  ]
  node [
    id 130
    label "profanum"
  ]
  node [
    id 131
    label "mikrokosmos"
  ]
  node [
    id 132
    label "nasada"
  ]
  node [
    id 133
    label "duch"
  ]
  node [
    id 134
    label "antropochoria"
  ]
  node [
    id 135
    label "wz&#243;r"
  ]
  node [
    id 136
    label "senior"
  ]
  node [
    id 137
    label "oddzia&#322;ywanie"
  ]
  node [
    id 138
    label "Adam"
  ]
  node [
    id 139
    label "homo_sapiens"
  ]
  node [
    id 140
    label "polifag"
  ]
  node [
    id 141
    label "dopiero_co"
  ]
  node [
    id 142
    label "formacja"
  ]
  node [
    id 143
    label "potomstwo"
  ]
  node [
    id 144
    label "punkt"
  ]
  node [
    id 145
    label "turn"
  ]
  node [
    id 146
    label "liczba"
  ]
  node [
    id 147
    label "&#380;art"
  ]
  node [
    id 148
    label "zi&#243;&#322;ko"
  ]
  node [
    id 149
    label "publikacja"
  ]
  node [
    id 150
    label "manewr"
  ]
  node [
    id 151
    label "impression"
  ]
  node [
    id 152
    label "wyst&#281;p"
  ]
  node [
    id 153
    label "sztos"
  ]
  node [
    id 154
    label "oznaczenie"
  ]
  node [
    id 155
    label "hotel"
  ]
  node [
    id 156
    label "pok&#243;j"
  ]
  node [
    id 157
    label "czasopismo"
  ]
  node [
    id 158
    label "akt_p&#322;ciowy"
  ]
  node [
    id 159
    label "orygina&#322;"
  ]
  node [
    id 160
    label "facet"
  ]
  node [
    id 161
    label "hip-hop"
  ]
  node [
    id 162
    label "bilard"
  ]
  node [
    id 163
    label "narkotyk"
  ]
  node [
    id 164
    label "uderzenie"
  ]
  node [
    id 165
    label "wypas"
  ]
  node [
    id 166
    label "oszustwo"
  ]
  node [
    id 167
    label "kraft"
  ]
  node [
    id 168
    label "nicpo&#324;"
  ]
  node [
    id 169
    label "agent"
  ]
  node [
    id 170
    label "kategoria"
  ]
  node [
    id 171
    label "pierwiastek"
  ]
  node [
    id 172
    label "rozmiar"
  ]
  node [
    id 173
    label "poj&#281;cie"
  ]
  node [
    id 174
    label "number"
  ]
  node [
    id 175
    label "cecha"
  ]
  node [
    id 176
    label "kategoria_gramatyczna"
  ]
  node [
    id 177
    label "grupa"
  ]
  node [
    id 178
    label "kwadrat_magiczny"
  ]
  node [
    id 179
    label "wyra&#380;enie"
  ]
  node [
    id 180
    label "koniugacja"
  ]
  node [
    id 181
    label "marking"
  ]
  node [
    id 182
    label "ustalenie"
  ]
  node [
    id 183
    label "symbol"
  ]
  node [
    id 184
    label "nazwanie"
  ]
  node [
    id 185
    label "wskazanie"
  ]
  node [
    id 186
    label "marker"
  ]
  node [
    id 187
    label "utrzymywanie"
  ]
  node [
    id 188
    label "move"
  ]
  node [
    id 189
    label "wydarzenie"
  ]
  node [
    id 190
    label "movement"
  ]
  node [
    id 191
    label "posuni&#281;cie"
  ]
  node [
    id 192
    label "myk"
  ]
  node [
    id 193
    label "taktyka"
  ]
  node [
    id 194
    label "utrzyma&#263;"
  ]
  node [
    id 195
    label "ruch"
  ]
  node [
    id 196
    label "maneuver"
  ]
  node [
    id 197
    label "utrzymanie"
  ]
  node [
    id 198
    label "utrzymywa&#263;"
  ]
  node [
    id 199
    label "czyn"
  ]
  node [
    id 200
    label "szczeg&#243;&#322;"
  ]
  node [
    id 201
    label "humor"
  ]
  node [
    id 202
    label "cyrk"
  ]
  node [
    id 203
    label "dokazywanie"
  ]
  node [
    id 204
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 205
    label "szpas"
  ]
  node [
    id 206
    label "spalenie"
  ]
  node [
    id 207
    label "opowiadanie"
  ]
  node [
    id 208
    label "furda"
  ]
  node [
    id 209
    label "banalny"
  ]
  node [
    id 210
    label "koncept"
  ]
  node [
    id 211
    label "gryps"
  ]
  node [
    id 212
    label "anecdote"
  ]
  node [
    id 213
    label "sofcik"
  ]
  node [
    id 214
    label "pomys&#322;"
  ]
  node [
    id 215
    label "raptularz"
  ]
  node [
    id 216
    label "g&#243;wno"
  ]
  node [
    id 217
    label "palenie"
  ]
  node [
    id 218
    label "finfa"
  ]
  node [
    id 219
    label "po&#322;o&#380;enie"
  ]
  node [
    id 220
    label "sprawa"
  ]
  node [
    id 221
    label "ust&#281;p"
  ]
  node [
    id 222
    label "plan"
  ]
  node [
    id 223
    label "obiekt_matematyczny"
  ]
  node [
    id 224
    label "problemat"
  ]
  node [
    id 225
    label "plamka"
  ]
  node [
    id 226
    label "stopie&#324;_pisma"
  ]
  node [
    id 227
    label "jednostka"
  ]
  node [
    id 228
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 229
    label "miejsce"
  ]
  node [
    id 230
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 231
    label "mark"
  ]
  node [
    id 232
    label "chwila"
  ]
  node [
    id 233
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 234
    label "prosta"
  ]
  node [
    id 235
    label "problematyka"
  ]
  node [
    id 236
    label "obiekt"
  ]
  node [
    id 237
    label "zapunktowa&#263;"
  ]
  node [
    id 238
    label "podpunkt"
  ]
  node [
    id 239
    label "wojsko"
  ]
  node [
    id 240
    label "kres"
  ]
  node [
    id 241
    label "przestrze&#324;"
  ]
  node [
    id 242
    label "point"
  ]
  node [
    id 243
    label "pozycja"
  ]
  node [
    id 244
    label "mir"
  ]
  node [
    id 245
    label "uk&#322;ad"
  ]
  node [
    id 246
    label "pacyfista"
  ]
  node [
    id 247
    label "preliminarium_pokojowe"
  ]
  node [
    id 248
    label "spok&#243;j"
  ]
  node [
    id 249
    label "pomieszczenie"
  ]
  node [
    id 250
    label "druk"
  ]
  node [
    id 251
    label "produkcja"
  ]
  node [
    id 252
    label "notification"
  ]
  node [
    id 253
    label "bratek"
  ]
  node [
    id 254
    label "model"
  ]
  node [
    id 255
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 256
    label "performance"
  ]
  node [
    id 257
    label "impreza"
  ]
  node [
    id 258
    label "tingel-tangel"
  ]
  node [
    id 259
    label "trema"
  ]
  node [
    id 260
    label "odtworzenie"
  ]
  node [
    id 261
    label "nocleg"
  ]
  node [
    id 262
    label "budynek_zamieszkania_zbiorowego"
  ]
  node [
    id 263
    label "restauracja"
  ]
  node [
    id 264
    label "go&#347;&#263;"
  ]
  node [
    id 265
    label "recepcja"
  ]
  node [
    id 266
    label "psychotest"
  ]
  node [
    id 267
    label "pismo"
  ]
  node [
    id 268
    label "communication"
  ]
  node [
    id 269
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 270
    label "zajawka"
  ]
  node [
    id 271
    label "Zwrotnica"
  ]
  node [
    id 272
    label "dzia&#322;"
  ]
  node [
    id 273
    label "prasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
]
