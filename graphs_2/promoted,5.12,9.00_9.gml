graph [
  node [
    id 0
    label "roczny"
    origin "text"
  ]
  node [
    id 1
    label "kacperek"
    origin "text"
  ]
  node [
    id 2
    label "gor&#261;czkowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kilkunastomiesi&#281;czny"
  ]
  node [
    id 4
    label "wielomiesi&#281;czny"
  ]
  node [
    id 5
    label "chorowa&#263;"
  ]
  node [
    id 6
    label "pain"
  ]
  node [
    id 7
    label "garlic"
  ]
  node [
    id 8
    label "pragn&#261;&#263;"
  ]
  node [
    id 9
    label "cierpie&#263;"
  ]
  node [
    id 10
    label "Kacperek"
  ]
  node [
    id 11
    label "grodziski"
  ]
  node [
    id 12
    label "Mazowiecki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 11
    target 12
  ]
]
