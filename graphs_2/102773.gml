graph [
  node [
    id 0
    label "opis"
    origin "text"
  ]
  node [
    id 1
    label "okoliczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wypadek"
    origin "text"
  ]
  node [
    id 3
    label "wypowied&#378;"
  ]
  node [
    id 4
    label "obja&#347;nienie"
  ]
  node [
    id 5
    label "exposition"
  ]
  node [
    id 6
    label "czynno&#347;&#263;"
  ]
  node [
    id 7
    label "bezproblemowy"
  ]
  node [
    id 8
    label "wydarzenie"
  ]
  node [
    id 9
    label "activity"
  ]
  node [
    id 10
    label "zrozumia&#322;y"
  ]
  node [
    id 11
    label "remark"
  ]
  node [
    id 12
    label "report"
  ]
  node [
    id 13
    label "przedstawienie"
  ]
  node [
    id 14
    label "poinformowanie"
  ]
  node [
    id 15
    label "informacja"
  ]
  node [
    id 16
    label "explanation"
  ]
  node [
    id 17
    label "parafrazowanie"
  ]
  node [
    id 18
    label "komunikat"
  ]
  node [
    id 19
    label "stylizacja"
  ]
  node [
    id 20
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 21
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 22
    label "strawestowanie"
  ]
  node [
    id 23
    label "sparafrazowanie"
  ]
  node [
    id 24
    label "sformu&#322;owanie"
  ]
  node [
    id 25
    label "pos&#322;uchanie"
  ]
  node [
    id 26
    label "strawestowa&#263;"
  ]
  node [
    id 27
    label "parafrazowa&#263;"
  ]
  node [
    id 28
    label "delimitacja"
  ]
  node [
    id 29
    label "rezultat"
  ]
  node [
    id 30
    label "ozdobnik"
  ]
  node [
    id 31
    label "trawestowa&#263;"
  ]
  node [
    id 32
    label "s&#261;d"
  ]
  node [
    id 33
    label "sparafrazowa&#263;"
  ]
  node [
    id 34
    label "trawestowanie"
  ]
  node [
    id 35
    label "sytuacja"
  ]
  node [
    id 36
    label "sk&#322;adnik"
  ]
  node [
    id 37
    label "warunki"
  ]
  node [
    id 38
    label "status"
  ]
  node [
    id 39
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 40
    label "fixture"
  ]
  node [
    id 41
    label "surowiec"
  ]
  node [
    id 42
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 43
    label "suma"
  ]
  node [
    id 44
    label "divisor"
  ]
  node [
    id 45
    label "sk&#322;ad"
  ]
  node [
    id 46
    label "charakter"
  ]
  node [
    id 47
    label "przebiegni&#281;cie"
  ]
  node [
    id 48
    label "przebiec"
  ]
  node [
    id 49
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 50
    label "motyw"
  ]
  node [
    id 51
    label "fabu&#322;a"
  ]
  node [
    id 52
    label "szczeg&#243;&#322;"
  ]
  node [
    id 53
    label "realia"
  ]
  node [
    id 54
    label "state"
  ]
  node [
    id 55
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 56
    label "happening"
  ]
  node [
    id 57
    label "event"
  ]
  node [
    id 58
    label "perypetia"
  ]
  node [
    id 59
    label "w&#281;ze&#322;"
  ]
  node [
    id 60
    label "opowiadanie"
  ]
  node [
    id 61
    label "w&#261;tek"
  ]
  node [
    id 62
    label "cecha"
  ]
  node [
    id 63
    label "fraza"
  ]
  node [
    id 64
    label "ozdoba"
  ]
  node [
    id 65
    label "przyczyna"
  ]
  node [
    id 66
    label "temat"
  ]
  node [
    id 67
    label "melodia"
  ]
  node [
    id 68
    label "cz&#322;owiek"
  ]
  node [
    id 69
    label "zbi&#243;r"
  ]
  node [
    id 70
    label "przedmiot"
  ]
  node [
    id 71
    label "fizjonomia"
  ]
  node [
    id 72
    label "kompleksja"
  ]
  node [
    id 73
    label "zjawisko"
  ]
  node [
    id 74
    label "entity"
  ]
  node [
    id 75
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 76
    label "psychika"
  ]
  node [
    id 77
    label "posta&#263;"
  ]
  node [
    id 78
    label "osobowo&#347;&#263;"
  ]
  node [
    id 79
    label "proceed"
  ]
  node [
    id 80
    label "fly"
  ]
  node [
    id 81
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 82
    label "przeby&#263;"
  ]
  node [
    id 83
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 84
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 85
    label "przesun&#261;&#263;"
  ]
  node [
    id 86
    label "przemierzy&#263;"
  ]
  node [
    id 87
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 88
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 89
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 90
    label "run"
  ]
  node [
    id 91
    label "przebycie"
  ]
  node [
    id 92
    label "przemkni&#281;cie"
  ]
  node [
    id 93
    label "zdarzenie_si&#281;"
  ]
  node [
    id 94
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 95
    label "zabrzmienie"
  ]
  node [
    id 96
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 13
  ]
]
