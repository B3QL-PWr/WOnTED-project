graph [
  node [
    id 0
    label "program"
    origin "text"
  ]
  node [
    id 1
    label "dni"
    origin "text"
  ]
  node [
    id 2
    label "parz&#281;czewa"
    origin "text"
  ]
  node [
    id 3
    label "czerwiec"
    origin "text"
  ]
  node [
    id 4
    label "instalowa&#263;"
  ]
  node [
    id 5
    label "oprogramowanie"
  ]
  node [
    id 6
    label "odinstalowywa&#263;"
  ]
  node [
    id 7
    label "spis"
  ]
  node [
    id 8
    label "zaprezentowanie"
  ]
  node [
    id 9
    label "podprogram"
  ]
  node [
    id 10
    label "ogranicznik_referencyjny"
  ]
  node [
    id 11
    label "course_of_study"
  ]
  node [
    id 12
    label "booklet"
  ]
  node [
    id 13
    label "dzia&#322;"
  ]
  node [
    id 14
    label "odinstalowanie"
  ]
  node [
    id 15
    label "broszura"
  ]
  node [
    id 16
    label "wytw&#243;r"
  ]
  node [
    id 17
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 18
    label "kana&#322;"
  ]
  node [
    id 19
    label "teleferie"
  ]
  node [
    id 20
    label "zainstalowanie"
  ]
  node [
    id 21
    label "struktura_organizacyjna"
  ]
  node [
    id 22
    label "pirat"
  ]
  node [
    id 23
    label "zaprezentowa&#263;"
  ]
  node [
    id 24
    label "prezentowanie"
  ]
  node [
    id 25
    label "prezentowa&#263;"
  ]
  node [
    id 26
    label "interfejs"
  ]
  node [
    id 27
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 28
    label "okno"
  ]
  node [
    id 29
    label "blok"
  ]
  node [
    id 30
    label "punkt"
  ]
  node [
    id 31
    label "folder"
  ]
  node [
    id 32
    label "zainstalowa&#263;"
  ]
  node [
    id 33
    label "za&#322;o&#380;enie"
  ]
  node [
    id 34
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 35
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 36
    label "ram&#243;wka"
  ]
  node [
    id 37
    label "tryb"
  ]
  node [
    id 38
    label "emitowa&#263;"
  ]
  node [
    id 39
    label "emitowanie"
  ]
  node [
    id 40
    label "odinstalowywanie"
  ]
  node [
    id 41
    label "instrukcja"
  ]
  node [
    id 42
    label "informatyka"
  ]
  node [
    id 43
    label "deklaracja"
  ]
  node [
    id 44
    label "menu"
  ]
  node [
    id 45
    label "sekcja_krytyczna"
  ]
  node [
    id 46
    label "furkacja"
  ]
  node [
    id 47
    label "podstawa"
  ]
  node [
    id 48
    label "instalowanie"
  ]
  node [
    id 49
    label "oferta"
  ]
  node [
    id 50
    label "odinstalowa&#263;"
  ]
  node [
    id 51
    label "druk_ulotny"
  ]
  node [
    id 52
    label "wydawnictwo"
  ]
  node [
    id 53
    label "rozmiar"
  ]
  node [
    id 54
    label "zakres"
  ]
  node [
    id 55
    label "zasi&#261;g"
  ]
  node [
    id 56
    label "izochronizm"
  ]
  node [
    id 57
    label "bridge"
  ]
  node [
    id 58
    label "distribution"
  ]
  node [
    id 59
    label "pot&#281;ga"
  ]
  node [
    id 60
    label "documentation"
  ]
  node [
    id 61
    label "przedmiot"
  ]
  node [
    id 62
    label "column"
  ]
  node [
    id 63
    label "zasadzenie"
  ]
  node [
    id 64
    label "punkt_odniesienia"
  ]
  node [
    id 65
    label "zasadzi&#263;"
  ]
  node [
    id 66
    label "bok"
  ]
  node [
    id 67
    label "d&#243;&#322;"
  ]
  node [
    id 68
    label "dzieci&#281;ctwo"
  ]
  node [
    id 69
    label "background"
  ]
  node [
    id 70
    label "podstawowy"
  ]
  node [
    id 71
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 72
    label "strategia"
  ]
  node [
    id 73
    label "pomys&#322;"
  ]
  node [
    id 74
    label "&#347;ciana"
  ]
  node [
    id 75
    label "p&#322;&#243;d"
  ]
  node [
    id 76
    label "work"
  ]
  node [
    id 77
    label "rezultat"
  ]
  node [
    id 78
    label "ko&#322;o"
  ]
  node [
    id 79
    label "spos&#243;b"
  ]
  node [
    id 80
    label "modalno&#347;&#263;"
  ]
  node [
    id 81
    label "z&#261;b"
  ]
  node [
    id 82
    label "cecha"
  ]
  node [
    id 83
    label "kategoria_gramatyczna"
  ]
  node [
    id 84
    label "skala"
  ]
  node [
    id 85
    label "funkcjonowa&#263;"
  ]
  node [
    id 86
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 87
    label "koniugacja"
  ]
  node [
    id 88
    label "offer"
  ]
  node [
    id 89
    label "propozycja"
  ]
  node [
    id 90
    label "o&#347;wiadczenie"
  ]
  node [
    id 91
    label "obietnica"
  ]
  node [
    id 92
    label "formularz"
  ]
  node [
    id 93
    label "statement"
  ]
  node [
    id 94
    label "announcement"
  ]
  node [
    id 95
    label "akt"
  ]
  node [
    id 96
    label "digest"
  ]
  node [
    id 97
    label "konstrukcja"
  ]
  node [
    id 98
    label "dokument"
  ]
  node [
    id 99
    label "o&#347;wiadczyny"
  ]
  node [
    id 100
    label "szaniec"
  ]
  node [
    id 101
    label "topologia_magistrali"
  ]
  node [
    id 102
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 103
    label "grodzisko"
  ]
  node [
    id 104
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 105
    label "tarapaty"
  ]
  node [
    id 106
    label "piaskownik"
  ]
  node [
    id 107
    label "struktura_anatomiczna"
  ]
  node [
    id 108
    label "miejsce"
  ]
  node [
    id 109
    label "bystrza"
  ]
  node [
    id 110
    label "pit"
  ]
  node [
    id 111
    label "odk&#322;ad"
  ]
  node [
    id 112
    label "chody"
  ]
  node [
    id 113
    label "klarownia"
  ]
  node [
    id 114
    label "kanalizacja"
  ]
  node [
    id 115
    label "przew&#243;d"
  ]
  node [
    id 116
    label "budowa"
  ]
  node [
    id 117
    label "ciek"
  ]
  node [
    id 118
    label "teatr"
  ]
  node [
    id 119
    label "gara&#380;"
  ]
  node [
    id 120
    label "zrzutowy"
  ]
  node [
    id 121
    label "warsztat"
  ]
  node [
    id 122
    label "syfon"
  ]
  node [
    id 123
    label "odwa&#322;"
  ]
  node [
    id 124
    label "urz&#261;dzenie"
  ]
  node [
    id 125
    label "zbi&#243;r"
  ]
  node [
    id 126
    label "catalog"
  ]
  node [
    id 127
    label "pozycja"
  ]
  node [
    id 128
    label "tekst"
  ]
  node [
    id 129
    label "sumariusz"
  ]
  node [
    id 130
    label "book"
  ]
  node [
    id 131
    label "stock"
  ]
  node [
    id 132
    label "figurowa&#263;"
  ]
  node [
    id 133
    label "czynno&#347;&#263;"
  ]
  node [
    id 134
    label "wyliczanka"
  ]
  node [
    id 135
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 136
    label "usuwanie"
  ]
  node [
    id 137
    label "usuni&#281;cie"
  ]
  node [
    id 138
    label "HP"
  ]
  node [
    id 139
    label "dost&#281;p"
  ]
  node [
    id 140
    label "infa"
  ]
  node [
    id 141
    label "kierunek"
  ]
  node [
    id 142
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 143
    label "kryptologia"
  ]
  node [
    id 144
    label "baza_danych"
  ]
  node [
    id 145
    label "przetwarzanie_informacji"
  ]
  node [
    id 146
    label "sztuczna_inteligencja"
  ]
  node [
    id 147
    label "gramatyka_formalna"
  ]
  node [
    id 148
    label "zamek"
  ]
  node [
    id 149
    label "dziedzina_informatyki"
  ]
  node [
    id 150
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 151
    label "artefakt"
  ]
  node [
    id 152
    label "dostosowa&#263;"
  ]
  node [
    id 153
    label "zrobi&#263;"
  ]
  node [
    id 154
    label "komputer"
  ]
  node [
    id 155
    label "install"
  ]
  node [
    id 156
    label "umie&#347;ci&#263;"
  ]
  node [
    id 157
    label "dostosowywa&#263;"
  ]
  node [
    id 158
    label "supply"
  ]
  node [
    id 159
    label "robi&#263;"
  ]
  node [
    id 160
    label "accommodate"
  ]
  node [
    id 161
    label "umieszcza&#263;"
  ]
  node [
    id 162
    label "fit"
  ]
  node [
    id 163
    label "usuwa&#263;"
  ]
  node [
    id 164
    label "zdolno&#347;&#263;"
  ]
  node [
    id 165
    label "usun&#261;&#263;"
  ]
  node [
    id 166
    label "dostosowanie"
  ]
  node [
    id 167
    label "installation"
  ]
  node [
    id 168
    label "pozak&#322;adanie"
  ]
  node [
    id 169
    label "proposition"
  ]
  node [
    id 170
    label "layout"
  ]
  node [
    id 171
    label "umieszczenie"
  ]
  node [
    id 172
    label "zrobienie"
  ]
  node [
    id 173
    label "parapet"
  ]
  node [
    id 174
    label "szyba"
  ]
  node [
    id 175
    label "okiennica"
  ]
  node [
    id 176
    label "prze&#347;wit"
  ]
  node [
    id 177
    label "pulpit"
  ]
  node [
    id 178
    label "transenna"
  ]
  node [
    id 179
    label "kwatera_okienna"
  ]
  node [
    id 180
    label "inspekt"
  ]
  node [
    id 181
    label "nora"
  ]
  node [
    id 182
    label "futryna"
  ]
  node [
    id 183
    label "nadokiennik"
  ]
  node [
    id 184
    label "skrzyd&#322;o"
  ]
  node [
    id 185
    label "lufcik"
  ]
  node [
    id 186
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 187
    label "casement"
  ]
  node [
    id 188
    label "menad&#380;er_okien"
  ]
  node [
    id 189
    label "otw&#243;r"
  ]
  node [
    id 190
    label "umieszczanie"
  ]
  node [
    id 191
    label "collection"
  ]
  node [
    id 192
    label "robienie"
  ]
  node [
    id 193
    label "wmontowanie"
  ]
  node [
    id 194
    label "wmontowywanie"
  ]
  node [
    id 195
    label "fitting"
  ]
  node [
    id 196
    label "dostosowywanie"
  ]
  node [
    id 197
    label "testify"
  ]
  node [
    id 198
    label "przedstawi&#263;"
  ]
  node [
    id 199
    label "zapozna&#263;"
  ]
  node [
    id 200
    label "pokaza&#263;"
  ]
  node [
    id 201
    label "represent"
  ]
  node [
    id 202
    label "typify"
  ]
  node [
    id 203
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 204
    label "uprzedzi&#263;"
  ]
  node [
    id 205
    label "attest"
  ]
  node [
    id 206
    label "wyra&#380;anie"
  ]
  node [
    id 207
    label "uprzedzanie"
  ]
  node [
    id 208
    label "representation"
  ]
  node [
    id 209
    label "zapoznawanie"
  ]
  node [
    id 210
    label "present"
  ]
  node [
    id 211
    label "przedstawianie"
  ]
  node [
    id 212
    label "display"
  ]
  node [
    id 213
    label "demonstrowanie"
  ]
  node [
    id 214
    label "presentation"
  ]
  node [
    id 215
    label "granie"
  ]
  node [
    id 216
    label "przest&#281;pca"
  ]
  node [
    id 217
    label "kopiowa&#263;"
  ]
  node [
    id 218
    label "podr&#243;bka"
  ]
  node [
    id 219
    label "kieruj&#261;cy"
  ]
  node [
    id 220
    label "&#380;agl&#243;wka"
  ]
  node [
    id 221
    label "rum"
  ]
  node [
    id 222
    label "rozb&#243;jnik"
  ]
  node [
    id 223
    label "postrzeleniec"
  ]
  node [
    id 224
    label "zapoznanie"
  ]
  node [
    id 225
    label "zapoznanie_si&#281;"
  ]
  node [
    id 226
    label "exhibit"
  ]
  node [
    id 227
    label "pokazanie"
  ]
  node [
    id 228
    label "wyst&#261;pienie"
  ]
  node [
    id 229
    label "uprzedzenie"
  ]
  node [
    id 230
    label "przedstawienie"
  ]
  node [
    id 231
    label "gra&#263;"
  ]
  node [
    id 232
    label "zapoznawa&#263;"
  ]
  node [
    id 233
    label "uprzedza&#263;"
  ]
  node [
    id 234
    label "wyra&#380;a&#263;"
  ]
  node [
    id 235
    label "przedstawia&#263;"
  ]
  node [
    id 236
    label "rynek"
  ]
  node [
    id 237
    label "energia"
  ]
  node [
    id 238
    label "wysy&#322;anie"
  ]
  node [
    id 239
    label "wys&#322;anie"
  ]
  node [
    id 240
    label "wydzielenie"
  ]
  node [
    id 241
    label "tembr"
  ]
  node [
    id 242
    label "wprowadzenie"
  ]
  node [
    id 243
    label "wydobycie"
  ]
  node [
    id 244
    label "wydzielanie"
  ]
  node [
    id 245
    label "wydobywanie"
  ]
  node [
    id 246
    label "nadawanie"
  ]
  node [
    id 247
    label "emission"
  ]
  node [
    id 248
    label "wprowadzanie"
  ]
  node [
    id 249
    label "nadanie"
  ]
  node [
    id 250
    label "issue"
  ]
  node [
    id 251
    label "nadawa&#263;"
  ]
  node [
    id 252
    label "wysy&#322;a&#263;"
  ]
  node [
    id 253
    label "nada&#263;"
  ]
  node [
    id 254
    label "air"
  ]
  node [
    id 255
    label "wydoby&#263;"
  ]
  node [
    id 256
    label "emit"
  ]
  node [
    id 257
    label "wys&#322;a&#263;"
  ]
  node [
    id 258
    label "wydzieli&#263;"
  ]
  node [
    id 259
    label "wydziela&#263;"
  ]
  node [
    id 260
    label "wprowadzi&#263;"
  ]
  node [
    id 261
    label "wydobywa&#263;"
  ]
  node [
    id 262
    label "wprowadza&#263;"
  ]
  node [
    id 263
    label "ulotka"
  ]
  node [
    id 264
    label "wskaz&#243;wka"
  ]
  node [
    id 265
    label "instruktarz"
  ]
  node [
    id 266
    label "routine"
  ]
  node [
    id 267
    label "proceduralnie"
  ]
  node [
    id 268
    label "danie"
  ]
  node [
    id 269
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 270
    label "restauracja"
  ]
  node [
    id 271
    label "cennik"
  ]
  node [
    id 272
    label "chart"
  ]
  node [
    id 273
    label "karta"
  ]
  node [
    id 274
    label "zestaw"
  ]
  node [
    id 275
    label "bajt"
  ]
  node [
    id 276
    label "bloking"
  ]
  node [
    id 277
    label "j&#261;kanie"
  ]
  node [
    id 278
    label "przeszkoda"
  ]
  node [
    id 279
    label "zesp&#243;&#322;"
  ]
  node [
    id 280
    label "blokada"
  ]
  node [
    id 281
    label "bry&#322;a"
  ]
  node [
    id 282
    label "kontynent"
  ]
  node [
    id 283
    label "nastawnia"
  ]
  node [
    id 284
    label "blockage"
  ]
  node [
    id 285
    label "block"
  ]
  node [
    id 286
    label "organizacja"
  ]
  node [
    id 287
    label "budynek"
  ]
  node [
    id 288
    label "start"
  ]
  node [
    id 289
    label "skorupa_ziemska"
  ]
  node [
    id 290
    label "zeszyt"
  ]
  node [
    id 291
    label "grupa"
  ]
  node [
    id 292
    label "blokowisko"
  ]
  node [
    id 293
    label "artyku&#322;"
  ]
  node [
    id 294
    label "barak"
  ]
  node [
    id 295
    label "stok_kontynentalny"
  ]
  node [
    id 296
    label "whole"
  ]
  node [
    id 297
    label "square"
  ]
  node [
    id 298
    label "siatk&#243;wka"
  ]
  node [
    id 299
    label "kr&#261;g"
  ]
  node [
    id 300
    label "obrona"
  ]
  node [
    id 301
    label "ok&#322;adka"
  ]
  node [
    id 302
    label "bie&#380;nia"
  ]
  node [
    id 303
    label "referat"
  ]
  node [
    id 304
    label "dom_wielorodzinny"
  ]
  node [
    id 305
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 306
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 307
    label "jednostka_organizacyjna"
  ]
  node [
    id 308
    label "urz&#261;d"
  ]
  node [
    id 309
    label "sfera"
  ]
  node [
    id 310
    label "miejsce_pracy"
  ]
  node [
    id 311
    label "insourcing"
  ]
  node [
    id 312
    label "stopie&#324;"
  ]
  node [
    id 313
    label "competence"
  ]
  node [
    id 314
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 315
    label "bezdro&#380;e"
  ]
  node [
    id 316
    label "poddzia&#322;"
  ]
  node [
    id 317
    label "podwini&#281;cie"
  ]
  node [
    id 318
    label "zap&#322;acenie"
  ]
  node [
    id 319
    label "przyodzianie"
  ]
  node [
    id 320
    label "budowla"
  ]
  node [
    id 321
    label "pokrycie"
  ]
  node [
    id 322
    label "rozebranie"
  ]
  node [
    id 323
    label "zak&#322;adka"
  ]
  node [
    id 324
    label "struktura"
  ]
  node [
    id 325
    label "poubieranie"
  ]
  node [
    id 326
    label "infliction"
  ]
  node [
    id 327
    label "spowodowanie"
  ]
  node [
    id 328
    label "przebranie"
  ]
  node [
    id 329
    label "przywdzianie"
  ]
  node [
    id 330
    label "obleczenie_si&#281;"
  ]
  node [
    id 331
    label "utworzenie"
  ]
  node [
    id 332
    label "str&#243;j"
  ]
  node [
    id 333
    label "twierdzenie"
  ]
  node [
    id 334
    label "obleczenie"
  ]
  node [
    id 335
    label "przygotowywanie"
  ]
  node [
    id 336
    label "przymierzenie"
  ]
  node [
    id 337
    label "wyko&#324;czenie"
  ]
  node [
    id 338
    label "point"
  ]
  node [
    id 339
    label "przygotowanie"
  ]
  node [
    id 340
    label "przewidzenie"
  ]
  node [
    id 341
    label "po&#322;o&#380;enie"
  ]
  node [
    id 342
    label "sprawa"
  ]
  node [
    id 343
    label "ust&#281;p"
  ]
  node [
    id 344
    label "plan"
  ]
  node [
    id 345
    label "obiekt_matematyczny"
  ]
  node [
    id 346
    label "problemat"
  ]
  node [
    id 347
    label "plamka"
  ]
  node [
    id 348
    label "stopie&#324;_pisma"
  ]
  node [
    id 349
    label "jednostka"
  ]
  node [
    id 350
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 351
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 352
    label "mark"
  ]
  node [
    id 353
    label "chwila"
  ]
  node [
    id 354
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 355
    label "prosta"
  ]
  node [
    id 356
    label "problematyka"
  ]
  node [
    id 357
    label "obiekt"
  ]
  node [
    id 358
    label "zapunktowa&#263;"
  ]
  node [
    id 359
    label "podpunkt"
  ]
  node [
    id 360
    label "wojsko"
  ]
  node [
    id 361
    label "kres"
  ]
  node [
    id 362
    label "przestrze&#324;"
  ]
  node [
    id 363
    label "reengineering"
  ]
  node [
    id 364
    label "scheduling"
  ]
  node [
    id 365
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 366
    label "okienko"
  ]
  node [
    id 367
    label "czas"
  ]
  node [
    id 368
    label "poprzedzanie"
  ]
  node [
    id 369
    label "czasoprzestrze&#324;"
  ]
  node [
    id 370
    label "laba"
  ]
  node [
    id 371
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 372
    label "chronometria"
  ]
  node [
    id 373
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 374
    label "rachuba_czasu"
  ]
  node [
    id 375
    label "przep&#322;ywanie"
  ]
  node [
    id 376
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 377
    label "czasokres"
  ]
  node [
    id 378
    label "odczyt"
  ]
  node [
    id 379
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 380
    label "dzieje"
  ]
  node [
    id 381
    label "poprzedzenie"
  ]
  node [
    id 382
    label "trawienie"
  ]
  node [
    id 383
    label "pochodzi&#263;"
  ]
  node [
    id 384
    label "period"
  ]
  node [
    id 385
    label "okres_czasu"
  ]
  node [
    id 386
    label "poprzedza&#263;"
  ]
  node [
    id 387
    label "schy&#322;ek"
  ]
  node [
    id 388
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 389
    label "odwlekanie_si&#281;"
  ]
  node [
    id 390
    label "zegar"
  ]
  node [
    id 391
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 392
    label "czwarty_wymiar"
  ]
  node [
    id 393
    label "pochodzenie"
  ]
  node [
    id 394
    label "Zeitgeist"
  ]
  node [
    id 395
    label "trawi&#263;"
  ]
  node [
    id 396
    label "pogoda"
  ]
  node [
    id 397
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 398
    label "poprzedzi&#263;"
  ]
  node [
    id 399
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 400
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 401
    label "time_period"
  ]
  node [
    id 402
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 403
    label "ro&#347;lina_zielna"
  ]
  node [
    id 404
    label "go&#378;dzikowate"
  ]
  node [
    id 405
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 406
    label "miesi&#261;c"
  ]
  node [
    id 407
    label "tydzie&#324;"
  ]
  node [
    id 408
    label "miech"
  ]
  node [
    id 409
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 410
    label "rok"
  ]
  node [
    id 411
    label "kalendy"
  ]
  node [
    id 412
    label "go&#378;dzikowce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
]
