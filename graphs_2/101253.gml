graph [
  node [
    id 0
    label "miros&#322;aw"
    origin "text"
  ]
  node [
    id 1
    label "czy&#380;ykiewicz"
    origin "text"
  ]
  node [
    id 2
    label "Miros&#322;awa"
  ]
  node [
    id 3
    label "Czy&#380;ykiewicz"
  ]
  node [
    id 4
    label "Andrzej"
  ]
  node [
    id 5
    label "ostatni"
  ]
  node [
    id 6
    label "wieczerza&#263;"
  ]
  node [
    id 7
    label "wyspa"
  ]
  node [
    id 8
    label "karczma"
  ]
  node [
    id 9
    label "przeznaczy&#263;"
  ]
  node [
    id 10
    label "do"
  ]
  node [
    id 11
    label "rozbi&#243;rka"
  ]
  node [
    id 12
    label "akademia"
  ]
  node [
    id 13
    label "sztuka"
  ]
  node [
    id 14
    label "pi&#281;kny"
  ]
  node [
    id 15
    label "iii"
  ]
  node [
    id 16
    label "program"
  ]
  node [
    id 17
    label "Maciej"
  ]
  node [
    id 18
    label "Zembatego"
  ]
  node [
    id 19
    label "kraina"
  ]
  node [
    id 20
    label "&#322;agodno&#347;&#263;"
  ]
  node [
    id 21
    label "Josifa"
  ]
  node [
    id 22
    label "Brodski"
  ]
  node [
    id 23
    label "W&#322;odzimierz"
  ]
  node [
    id 24
    label "Wysocki"
  ]
  node [
    id 25
    label "spotkanie"
  ]
  node [
    id 26
    label "zamkowy"
  ]
  node [
    id 27
    label "&#347;piewa&#263;"
  ]
  node [
    id 28
    label "poezja"
  ]
  node [
    id 29
    label "studencki"
  ]
  node [
    id 30
    label "festiwal"
  ]
  node [
    id 31
    label "piosenka"
  ]
  node [
    id 32
    label "Mateusz"
  ]
  node [
    id 33
    label "&#346;wi&#281;cicki"
  ]
  node [
    id 34
    label "J"
  ]
  node [
    id 35
    label "Kaczmarski"
  ]
  node [
    id 36
    label "autoportret"
  ]
  node [
    id 37
    label "i"
  ]
  node [
    id 38
    label "Jerzy"
  ]
  node [
    id 39
    label "Satanowskiego"
  ]
  node [
    id 40
    label "Hanna"
  ]
  node [
    id 41
    label "Banaszak"
  ]
  node [
    id 42
    label "zanim"
  ]
  node [
    id 43
    label "by&#263;"
  ]
  node [
    id 44
    label "u"
  ]
  node [
    id 45
    label "brzeg"
  ]
  node [
    id 46
    label "gdy"
  ]
  node [
    id 47
    label "wiatr"
  ]
  node [
    id 48
    label "deszczy&#263;"
  ]
  node [
    id 49
    label "bieg"
  ]
  node [
    id 50
    label "rok"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 46
  ]
  edge [
    source 37
    target 47
  ]
  edge [
    source 37
    target 48
  ]
  edge [
    source 37
    target 49
  ]
  edge [
    source 37
    target 50
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 49
    target 50
  ]
]
