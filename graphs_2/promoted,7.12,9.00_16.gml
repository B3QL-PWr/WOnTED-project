graph [
  node [
    id 0
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 1
    label "raport"
    origin "text"
  ]
  node [
    id 2
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przez"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "instytut"
    origin "text"
  ]
  node [
    id 6
    label "prawy"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 8
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "imigrant"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "przyby&#322;a"
    origin "text"
  ]
  node [
    id 12
    label "kraj"
    origin "text"
  ]
  node [
    id 13
    label "cel"
    origin "text"
  ]
  node [
    id 14
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 15
    label "praca"
    origin "text"
  ]
  node [
    id 16
    label "spotka&#263;"
    origin "text"
  ]
  node [
    id 17
    label "ci&#281;&#380;ki"
    origin "text"
  ]
  node [
    id 18
    label "wyzysk"
    origin "text"
  ]
  node [
    id 19
    label "statement"
  ]
  node [
    id 20
    label "raport_Beveridge'a"
  ]
  node [
    id 21
    label "relacja"
  ]
  node [
    id 22
    label "raport_Fischlera"
  ]
  node [
    id 23
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 24
    label "ustosunkowywa&#263;"
  ]
  node [
    id 25
    label "wi&#261;zanie"
  ]
  node [
    id 26
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 27
    label "sprawko"
  ]
  node [
    id 28
    label "bratnia_dusza"
  ]
  node [
    id 29
    label "trasa"
  ]
  node [
    id 30
    label "zwi&#261;zanie"
  ]
  node [
    id 31
    label "ustosunkowywanie"
  ]
  node [
    id 32
    label "marriage"
  ]
  node [
    id 33
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 34
    label "message"
  ]
  node [
    id 35
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 36
    label "ustosunkowa&#263;"
  ]
  node [
    id 37
    label "korespondent"
  ]
  node [
    id 38
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 39
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 40
    label "zwi&#261;za&#263;"
  ]
  node [
    id 41
    label "podzbi&#243;r"
  ]
  node [
    id 42
    label "ustosunkowanie"
  ]
  node [
    id 43
    label "wypowied&#378;"
  ]
  node [
    id 44
    label "zwi&#261;zek"
  ]
  node [
    id 45
    label "upubliczni&#263;"
  ]
  node [
    id 46
    label "picture"
  ]
  node [
    id 47
    label "wydawnictwo"
  ]
  node [
    id 48
    label "wprowadzi&#263;"
  ]
  node [
    id 49
    label "rynek"
  ]
  node [
    id 50
    label "doprowadzi&#263;"
  ]
  node [
    id 51
    label "testify"
  ]
  node [
    id 52
    label "insert"
  ]
  node [
    id 53
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 54
    label "wpisa&#263;"
  ]
  node [
    id 55
    label "zapozna&#263;"
  ]
  node [
    id 56
    label "zrobi&#263;"
  ]
  node [
    id 57
    label "wej&#347;&#263;"
  ]
  node [
    id 58
    label "spowodowa&#263;"
  ]
  node [
    id 59
    label "zej&#347;&#263;"
  ]
  node [
    id 60
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 61
    label "umie&#347;ci&#263;"
  ]
  node [
    id 62
    label "zacz&#261;&#263;"
  ]
  node [
    id 63
    label "indicate"
  ]
  node [
    id 64
    label "udost&#281;pni&#263;"
  ]
  node [
    id 65
    label "debit"
  ]
  node [
    id 66
    label "redaktor"
  ]
  node [
    id 67
    label "druk"
  ]
  node [
    id 68
    label "publikacja"
  ]
  node [
    id 69
    label "redakcja"
  ]
  node [
    id 70
    label "szata_graficzna"
  ]
  node [
    id 71
    label "firma"
  ]
  node [
    id 72
    label "wydawa&#263;"
  ]
  node [
    id 73
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 74
    label "wyda&#263;"
  ]
  node [
    id 75
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 76
    label "poster"
  ]
  node [
    id 77
    label "po_niemiecku"
  ]
  node [
    id 78
    label "German"
  ]
  node [
    id 79
    label "niemiecko"
  ]
  node [
    id 80
    label "cenar"
  ]
  node [
    id 81
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 82
    label "europejski"
  ]
  node [
    id 83
    label "strudel"
  ]
  node [
    id 84
    label "niemiec"
  ]
  node [
    id 85
    label "pionier"
  ]
  node [
    id 86
    label "zachodnioeuropejski"
  ]
  node [
    id 87
    label "j&#281;zyk"
  ]
  node [
    id 88
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 89
    label "junkers"
  ]
  node [
    id 90
    label "szwabski"
  ]
  node [
    id 91
    label "szwabsko"
  ]
  node [
    id 92
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 93
    label "po_szwabsku"
  ]
  node [
    id 94
    label "platt"
  ]
  node [
    id 95
    label "europejsko"
  ]
  node [
    id 96
    label "&#380;o&#322;nierz"
  ]
  node [
    id 97
    label "saper"
  ]
  node [
    id 98
    label "prekursor"
  ]
  node [
    id 99
    label "osadnik"
  ]
  node [
    id 100
    label "skaut"
  ]
  node [
    id 101
    label "g&#322;osiciel"
  ]
  node [
    id 102
    label "ciasto"
  ]
  node [
    id 103
    label "taniec_ludowy"
  ]
  node [
    id 104
    label "melodia"
  ]
  node [
    id 105
    label "taniec"
  ]
  node [
    id 106
    label "podgrzewacz"
  ]
  node [
    id 107
    label "samolot_wojskowy"
  ]
  node [
    id 108
    label "moreska"
  ]
  node [
    id 109
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 110
    label "zachodni"
  ]
  node [
    id 111
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 112
    label "langosz"
  ]
  node [
    id 113
    label "po_europejsku"
  ]
  node [
    id 114
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 115
    label "European"
  ]
  node [
    id 116
    label "typowy"
  ]
  node [
    id 117
    label "charakterystyczny"
  ]
  node [
    id 118
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 119
    label "artykulator"
  ]
  node [
    id 120
    label "kod"
  ]
  node [
    id 121
    label "kawa&#322;ek"
  ]
  node [
    id 122
    label "przedmiot"
  ]
  node [
    id 123
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 124
    label "gramatyka"
  ]
  node [
    id 125
    label "stylik"
  ]
  node [
    id 126
    label "przet&#322;umaczenie"
  ]
  node [
    id 127
    label "formalizowanie"
  ]
  node [
    id 128
    label "ssanie"
  ]
  node [
    id 129
    label "ssa&#263;"
  ]
  node [
    id 130
    label "language"
  ]
  node [
    id 131
    label "liza&#263;"
  ]
  node [
    id 132
    label "napisa&#263;"
  ]
  node [
    id 133
    label "konsonantyzm"
  ]
  node [
    id 134
    label "wokalizm"
  ]
  node [
    id 135
    label "pisa&#263;"
  ]
  node [
    id 136
    label "fonetyka"
  ]
  node [
    id 137
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 138
    label "jeniec"
  ]
  node [
    id 139
    label "but"
  ]
  node [
    id 140
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 141
    label "po_koroniarsku"
  ]
  node [
    id 142
    label "kultura_duchowa"
  ]
  node [
    id 143
    label "t&#322;umaczenie"
  ]
  node [
    id 144
    label "m&#243;wienie"
  ]
  node [
    id 145
    label "pype&#263;"
  ]
  node [
    id 146
    label "lizanie"
  ]
  node [
    id 147
    label "pismo"
  ]
  node [
    id 148
    label "formalizowa&#263;"
  ]
  node [
    id 149
    label "rozumie&#263;"
  ]
  node [
    id 150
    label "organ"
  ]
  node [
    id 151
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 152
    label "rozumienie"
  ]
  node [
    id 153
    label "spos&#243;b"
  ]
  node [
    id 154
    label "makroglosja"
  ]
  node [
    id 155
    label "m&#243;wi&#263;"
  ]
  node [
    id 156
    label "jama_ustna"
  ]
  node [
    id 157
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 158
    label "formacja_geologiczna"
  ]
  node [
    id 159
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 160
    label "natural_language"
  ]
  node [
    id 161
    label "s&#322;ownictwo"
  ]
  node [
    id 162
    label "urz&#261;dzenie"
  ]
  node [
    id 163
    label "instytucja"
  ]
  node [
    id 164
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 165
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 166
    label "Ossolineum"
  ]
  node [
    id 167
    label "plac&#243;wka"
  ]
  node [
    id 168
    label "institute"
  ]
  node [
    id 169
    label "agencja"
  ]
  node [
    id 170
    label "siedziba"
  ]
  node [
    id 171
    label "sie&#263;"
  ]
  node [
    id 172
    label "osoba_prawna"
  ]
  node [
    id 173
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 174
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 175
    label "poj&#281;cie"
  ]
  node [
    id 176
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 177
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 178
    label "biuro"
  ]
  node [
    id 179
    label "organizacja"
  ]
  node [
    id 180
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 181
    label "Fundusze_Unijne"
  ]
  node [
    id 182
    label "zamyka&#263;"
  ]
  node [
    id 183
    label "establishment"
  ]
  node [
    id 184
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 185
    label "urz&#261;d"
  ]
  node [
    id 186
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 187
    label "afiliowa&#263;"
  ]
  node [
    id 188
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 189
    label "standard"
  ]
  node [
    id 190
    label "zamykanie"
  ]
  node [
    id 191
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 192
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 193
    label "w_prawo"
  ]
  node [
    id 194
    label "s&#322;uszny"
  ]
  node [
    id 195
    label "naturalny"
  ]
  node [
    id 196
    label "chwalebny"
  ]
  node [
    id 197
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 198
    label "zgodnie_z_prawem"
  ]
  node [
    id 199
    label "zacny"
  ]
  node [
    id 200
    label "moralny"
  ]
  node [
    id 201
    label "prawicowy"
  ]
  node [
    id 202
    label "na_prawo"
  ]
  node [
    id 203
    label "cnotliwy"
  ]
  node [
    id 204
    label "legalny"
  ]
  node [
    id 205
    label "z_prawa"
  ]
  node [
    id 206
    label "gajny"
  ]
  node [
    id 207
    label "legalnie"
  ]
  node [
    id 208
    label "pochwalny"
  ]
  node [
    id 209
    label "wspania&#322;y"
  ]
  node [
    id 210
    label "szlachetny"
  ]
  node [
    id 211
    label "powa&#380;ny"
  ]
  node [
    id 212
    label "chwalebnie"
  ]
  node [
    id 213
    label "moralnie"
  ]
  node [
    id 214
    label "warto&#347;ciowy"
  ]
  node [
    id 215
    label "etycznie"
  ]
  node [
    id 216
    label "dobry"
  ]
  node [
    id 217
    label "s&#322;usznie"
  ]
  node [
    id 218
    label "zasadny"
  ]
  node [
    id 219
    label "nale&#380;yty"
  ]
  node [
    id 220
    label "prawdziwy"
  ]
  node [
    id 221
    label "solidny"
  ]
  node [
    id 222
    label "skromny"
  ]
  node [
    id 223
    label "niewinny"
  ]
  node [
    id 224
    label "cny"
  ]
  node [
    id 225
    label "cnotliwie"
  ]
  node [
    id 226
    label "prostolinijny"
  ]
  node [
    id 227
    label "zacnie"
  ]
  node [
    id 228
    label "szczery"
  ]
  node [
    id 229
    label "zrozumia&#322;y"
  ]
  node [
    id 230
    label "immanentny"
  ]
  node [
    id 231
    label "zwyczajny"
  ]
  node [
    id 232
    label "bezsporny"
  ]
  node [
    id 233
    label "organicznie"
  ]
  node [
    id 234
    label "pierwotny"
  ]
  node [
    id 235
    label "neutralny"
  ]
  node [
    id 236
    label "normalny"
  ]
  node [
    id 237
    label "rzeczywisty"
  ]
  node [
    id 238
    label "naturalnie"
  ]
  node [
    id 239
    label "prawicowo"
  ]
  node [
    id 240
    label "prawoskr&#281;tny"
  ]
  node [
    id 241
    label "konserwatywny"
  ]
  node [
    id 242
    label "ludzko&#347;&#263;"
  ]
  node [
    id 243
    label "asymilowanie"
  ]
  node [
    id 244
    label "wapniak"
  ]
  node [
    id 245
    label "asymilowa&#263;"
  ]
  node [
    id 246
    label "os&#322;abia&#263;"
  ]
  node [
    id 247
    label "posta&#263;"
  ]
  node [
    id 248
    label "hominid"
  ]
  node [
    id 249
    label "podw&#322;adny"
  ]
  node [
    id 250
    label "os&#322;abianie"
  ]
  node [
    id 251
    label "g&#322;owa"
  ]
  node [
    id 252
    label "figura"
  ]
  node [
    id 253
    label "portrecista"
  ]
  node [
    id 254
    label "dwun&#243;g"
  ]
  node [
    id 255
    label "profanum"
  ]
  node [
    id 256
    label "mikrokosmos"
  ]
  node [
    id 257
    label "nasada"
  ]
  node [
    id 258
    label "duch"
  ]
  node [
    id 259
    label "antropochoria"
  ]
  node [
    id 260
    label "osoba"
  ]
  node [
    id 261
    label "wz&#243;r"
  ]
  node [
    id 262
    label "senior"
  ]
  node [
    id 263
    label "oddzia&#322;ywanie"
  ]
  node [
    id 264
    label "Adam"
  ]
  node [
    id 265
    label "homo_sapiens"
  ]
  node [
    id 266
    label "polifag"
  ]
  node [
    id 267
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 268
    label "cz&#322;owiekowate"
  ]
  node [
    id 269
    label "konsument"
  ]
  node [
    id 270
    label "istota_&#380;ywa"
  ]
  node [
    id 271
    label "pracownik"
  ]
  node [
    id 272
    label "Chocho&#322;"
  ]
  node [
    id 273
    label "Herkules_Poirot"
  ]
  node [
    id 274
    label "Edyp"
  ]
  node [
    id 275
    label "parali&#380;owa&#263;"
  ]
  node [
    id 276
    label "Harry_Potter"
  ]
  node [
    id 277
    label "Casanova"
  ]
  node [
    id 278
    label "Zgredek"
  ]
  node [
    id 279
    label "Gargantua"
  ]
  node [
    id 280
    label "Winnetou"
  ]
  node [
    id 281
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 282
    label "Dulcynea"
  ]
  node [
    id 283
    label "kategoria_gramatyczna"
  ]
  node [
    id 284
    label "person"
  ]
  node [
    id 285
    label "Plastu&#347;"
  ]
  node [
    id 286
    label "Quasimodo"
  ]
  node [
    id 287
    label "Sherlock_Holmes"
  ]
  node [
    id 288
    label "Faust"
  ]
  node [
    id 289
    label "Wallenrod"
  ]
  node [
    id 290
    label "Dwukwiat"
  ]
  node [
    id 291
    label "Don_Juan"
  ]
  node [
    id 292
    label "koniugacja"
  ]
  node [
    id 293
    label "Don_Kiszot"
  ]
  node [
    id 294
    label "Hamlet"
  ]
  node [
    id 295
    label "Werter"
  ]
  node [
    id 296
    label "istota"
  ]
  node [
    id 297
    label "Szwejk"
  ]
  node [
    id 298
    label "doros&#322;y"
  ]
  node [
    id 299
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 300
    label "jajko"
  ]
  node [
    id 301
    label "rodzic"
  ]
  node [
    id 302
    label "wapniaki"
  ]
  node [
    id 303
    label "zwierzchnik"
  ]
  node [
    id 304
    label "feuda&#322;"
  ]
  node [
    id 305
    label "starzec"
  ]
  node [
    id 306
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 307
    label "zawodnik"
  ]
  node [
    id 308
    label "komendancja"
  ]
  node [
    id 309
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 310
    label "de-escalation"
  ]
  node [
    id 311
    label "powodowanie"
  ]
  node [
    id 312
    label "os&#322;abienie"
  ]
  node [
    id 313
    label "kondycja_fizyczna"
  ]
  node [
    id 314
    label "os&#322;abi&#263;"
  ]
  node [
    id 315
    label "debilitation"
  ]
  node [
    id 316
    label "zdrowie"
  ]
  node [
    id 317
    label "zmniejszanie"
  ]
  node [
    id 318
    label "s&#322;abszy"
  ]
  node [
    id 319
    label "pogarszanie"
  ]
  node [
    id 320
    label "suppress"
  ]
  node [
    id 321
    label "robi&#263;"
  ]
  node [
    id 322
    label "powodowa&#263;"
  ]
  node [
    id 323
    label "zmniejsza&#263;"
  ]
  node [
    id 324
    label "bate"
  ]
  node [
    id 325
    label "asymilowanie_si&#281;"
  ]
  node [
    id 326
    label "absorption"
  ]
  node [
    id 327
    label "pobieranie"
  ]
  node [
    id 328
    label "czerpanie"
  ]
  node [
    id 329
    label "acquisition"
  ]
  node [
    id 330
    label "zmienianie"
  ]
  node [
    id 331
    label "organizm"
  ]
  node [
    id 332
    label "assimilation"
  ]
  node [
    id 333
    label "upodabnianie"
  ]
  node [
    id 334
    label "g&#322;oska"
  ]
  node [
    id 335
    label "kultura"
  ]
  node [
    id 336
    label "podobny"
  ]
  node [
    id 337
    label "grupa"
  ]
  node [
    id 338
    label "assimilate"
  ]
  node [
    id 339
    label "dostosowywa&#263;"
  ]
  node [
    id 340
    label "dostosowa&#263;"
  ]
  node [
    id 341
    label "przejmowa&#263;"
  ]
  node [
    id 342
    label "upodobni&#263;"
  ]
  node [
    id 343
    label "przej&#261;&#263;"
  ]
  node [
    id 344
    label "upodabnia&#263;"
  ]
  node [
    id 345
    label "pobiera&#263;"
  ]
  node [
    id 346
    label "pobra&#263;"
  ]
  node [
    id 347
    label "charakterystyka"
  ]
  node [
    id 348
    label "zaistnie&#263;"
  ]
  node [
    id 349
    label "cecha"
  ]
  node [
    id 350
    label "Osjan"
  ]
  node [
    id 351
    label "kto&#347;"
  ]
  node [
    id 352
    label "wygl&#261;d"
  ]
  node [
    id 353
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 354
    label "osobowo&#347;&#263;"
  ]
  node [
    id 355
    label "wytw&#243;r"
  ]
  node [
    id 356
    label "trim"
  ]
  node [
    id 357
    label "poby&#263;"
  ]
  node [
    id 358
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 359
    label "Aspazja"
  ]
  node [
    id 360
    label "punkt_widzenia"
  ]
  node [
    id 361
    label "kompleksja"
  ]
  node [
    id 362
    label "wytrzyma&#263;"
  ]
  node [
    id 363
    label "budowa"
  ]
  node [
    id 364
    label "formacja"
  ]
  node [
    id 365
    label "pozosta&#263;"
  ]
  node [
    id 366
    label "point"
  ]
  node [
    id 367
    label "przedstawienie"
  ]
  node [
    id 368
    label "go&#347;&#263;"
  ]
  node [
    id 369
    label "zapis"
  ]
  node [
    id 370
    label "figure"
  ]
  node [
    id 371
    label "typ"
  ]
  node [
    id 372
    label "mildew"
  ]
  node [
    id 373
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 374
    label "ideal"
  ]
  node [
    id 375
    label "rule"
  ]
  node [
    id 376
    label "ruch"
  ]
  node [
    id 377
    label "dekal"
  ]
  node [
    id 378
    label "motyw"
  ]
  node [
    id 379
    label "projekt"
  ]
  node [
    id 380
    label "pryncypa&#322;"
  ]
  node [
    id 381
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 382
    label "kszta&#322;t"
  ]
  node [
    id 383
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 384
    label "wiedza"
  ]
  node [
    id 385
    label "kierowa&#263;"
  ]
  node [
    id 386
    label "alkohol"
  ]
  node [
    id 387
    label "zdolno&#347;&#263;"
  ]
  node [
    id 388
    label "&#380;ycie"
  ]
  node [
    id 389
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 390
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 391
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 392
    label "sztuka"
  ]
  node [
    id 393
    label "dekiel"
  ]
  node [
    id 394
    label "ro&#347;lina"
  ]
  node [
    id 395
    label "&#347;ci&#281;cie"
  ]
  node [
    id 396
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 397
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 398
    label "&#347;ci&#281;gno"
  ]
  node [
    id 399
    label "noosfera"
  ]
  node [
    id 400
    label "byd&#322;o"
  ]
  node [
    id 401
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 402
    label "makrocefalia"
  ]
  node [
    id 403
    label "obiekt"
  ]
  node [
    id 404
    label "ucho"
  ]
  node [
    id 405
    label "g&#243;ra"
  ]
  node [
    id 406
    label "m&#243;zg"
  ]
  node [
    id 407
    label "kierownictwo"
  ]
  node [
    id 408
    label "fryzura"
  ]
  node [
    id 409
    label "umys&#322;"
  ]
  node [
    id 410
    label "cia&#322;o"
  ]
  node [
    id 411
    label "cz&#322;onek"
  ]
  node [
    id 412
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 413
    label "czaszka"
  ]
  node [
    id 414
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 415
    label "dziedzina"
  ]
  node [
    id 416
    label "hipnotyzowanie"
  ]
  node [
    id 417
    label "&#347;lad"
  ]
  node [
    id 418
    label "docieranie"
  ]
  node [
    id 419
    label "natural_process"
  ]
  node [
    id 420
    label "reakcja_chemiczna"
  ]
  node [
    id 421
    label "wdzieranie_si&#281;"
  ]
  node [
    id 422
    label "zjawisko"
  ]
  node [
    id 423
    label "act"
  ]
  node [
    id 424
    label "rezultat"
  ]
  node [
    id 425
    label "lobbysta"
  ]
  node [
    id 426
    label "allochoria"
  ]
  node [
    id 427
    label "fotograf"
  ]
  node [
    id 428
    label "malarz"
  ]
  node [
    id 429
    label "artysta"
  ]
  node [
    id 430
    label "p&#322;aszczyzna"
  ]
  node [
    id 431
    label "bierka_szachowa"
  ]
  node [
    id 432
    label "obiekt_matematyczny"
  ]
  node [
    id 433
    label "gestaltyzm"
  ]
  node [
    id 434
    label "styl"
  ]
  node [
    id 435
    label "obraz"
  ]
  node [
    id 436
    label "rzecz"
  ]
  node [
    id 437
    label "d&#378;wi&#281;k"
  ]
  node [
    id 438
    label "character"
  ]
  node [
    id 439
    label "rze&#378;ba"
  ]
  node [
    id 440
    label "stylistyka"
  ]
  node [
    id 441
    label "miejsce"
  ]
  node [
    id 442
    label "antycypacja"
  ]
  node [
    id 443
    label "ornamentyka"
  ]
  node [
    id 444
    label "informacja"
  ]
  node [
    id 445
    label "facet"
  ]
  node [
    id 446
    label "popis"
  ]
  node [
    id 447
    label "wiersz"
  ]
  node [
    id 448
    label "symetria"
  ]
  node [
    id 449
    label "lingwistyka_kognitywna"
  ]
  node [
    id 450
    label "karta"
  ]
  node [
    id 451
    label "shape"
  ]
  node [
    id 452
    label "perspektywa"
  ]
  node [
    id 453
    label "nak&#322;adka"
  ]
  node [
    id 454
    label "li&#347;&#263;"
  ]
  node [
    id 455
    label "jama_gard&#322;owa"
  ]
  node [
    id 456
    label "rezonator"
  ]
  node [
    id 457
    label "podstawa"
  ]
  node [
    id 458
    label "base"
  ]
  node [
    id 459
    label "piek&#322;o"
  ]
  node [
    id 460
    label "human_body"
  ]
  node [
    id 461
    label "ofiarowywanie"
  ]
  node [
    id 462
    label "sfera_afektywna"
  ]
  node [
    id 463
    label "nekromancja"
  ]
  node [
    id 464
    label "Po&#347;wist"
  ]
  node [
    id 465
    label "podekscytowanie"
  ]
  node [
    id 466
    label "deformowanie"
  ]
  node [
    id 467
    label "sumienie"
  ]
  node [
    id 468
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 469
    label "deformowa&#263;"
  ]
  node [
    id 470
    label "psychika"
  ]
  node [
    id 471
    label "zjawa"
  ]
  node [
    id 472
    label "zmar&#322;y"
  ]
  node [
    id 473
    label "istota_nadprzyrodzona"
  ]
  node [
    id 474
    label "power"
  ]
  node [
    id 475
    label "entity"
  ]
  node [
    id 476
    label "ofiarowywa&#263;"
  ]
  node [
    id 477
    label "oddech"
  ]
  node [
    id 478
    label "seksualno&#347;&#263;"
  ]
  node [
    id 479
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 480
    label "byt"
  ]
  node [
    id 481
    label "si&#322;a"
  ]
  node [
    id 482
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 483
    label "ego"
  ]
  node [
    id 484
    label "ofiarowanie"
  ]
  node [
    id 485
    label "charakter"
  ]
  node [
    id 486
    label "fizjonomia"
  ]
  node [
    id 487
    label "kompleks"
  ]
  node [
    id 488
    label "zapalno&#347;&#263;"
  ]
  node [
    id 489
    label "T&#281;sknica"
  ]
  node [
    id 490
    label "ofiarowa&#263;"
  ]
  node [
    id 491
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 492
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 493
    label "passion"
  ]
  node [
    id 494
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 495
    label "odbicie"
  ]
  node [
    id 496
    label "atom"
  ]
  node [
    id 497
    label "przyroda"
  ]
  node [
    id 498
    label "Ziemia"
  ]
  node [
    id 499
    label "kosmos"
  ]
  node [
    id 500
    label "miniatura"
  ]
  node [
    id 501
    label "Rzym_Zachodni"
  ]
  node [
    id 502
    label "whole"
  ]
  node [
    id 503
    label "ilo&#347;&#263;"
  ]
  node [
    id 504
    label "element"
  ]
  node [
    id 505
    label "Rzym_Wschodni"
  ]
  node [
    id 506
    label "r&#243;&#380;niczka"
  ]
  node [
    id 507
    label "&#347;rodowisko"
  ]
  node [
    id 508
    label "materia"
  ]
  node [
    id 509
    label "szambo"
  ]
  node [
    id 510
    label "aspo&#322;eczny"
  ]
  node [
    id 511
    label "component"
  ]
  node [
    id 512
    label "szkodnik"
  ]
  node [
    id 513
    label "gangsterski"
  ]
  node [
    id 514
    label "underworld"
  ]
  node [
    id 515
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 516
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 517
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 518
    label "rozmiar"
  ]
  node [
    id 519
    label "part"
  ]
  node [
    id 520
    label "kom&#243;rka"
  ]
  node [
    id 521
    label "furnishing"
  ]
  node [
    id 522
    label "zabezpieczenie"
  ]
  node [
    id 523
    label "zrobienie"
  ]
  node [
    id 524
    label "wyrz&#261;dzenie"
  ]
  node [
    id 525
    label "zagospodarowanie"
  ]
  node [
    id 526
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 527
    label "ig&#322;a"
  ]
  node [
    id 528
    label "narz&#281;dzie"
  ]
  node [
    id 529
    label "wirnik"
  ]
  node [
    id 530
    label "aparatura"
  ]
  node [
    id 531
    label "system_energetyczny"
  ]
  node [
    id 532
    label "impulsator"
  ]
  node [
    id 533
    label "mechanizm"
  ]
  node [
    id 534
    label "sprz&#281;t"
  ]
  node [
    id 535
    label "czynno&#347;&#263;"
  ]
  node [
    id 536
    label "blokowanie"
  ]
  node [
    id 537
    label "set"
  ]
  node [
    id 538
    label "zablokowanie"
  ]
  node [
    id 539
    label "przygotowanie"
  ]
  node [
    id 540
    label "komora"
  ]
  node [
    id 541
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 542
    label "cudzoziemiec"
  ]
  node [
    id 543
    label "przybysz"
  ]
  node [
    id 544
    label "migrant"
  ]
  node [
    id 545
    label "imigracja"
  ]
  node [
    id 546
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 547
    label "nowy"
  ]
  node [
    id 548
    label "miesi&#261;c_ksi&#281;&#380;ycowy"
  ]
  node [
    id 549
    label "obcy"
  ]
  node [
    id 550
    label "obcokrajowy"
  ]
  node [
    id 551
    label "etran&#380;er"
  ]
  node [
    id 552
    label "mieszkaniec"
  ]
  node [
    id 553
    label "zagraniczny"
  ]
  node [
    id 554
    label "cudzoziemski"
  ]
  node [
    id 555
    label "nap&#322;yw"
  ]
  node [
    id 556
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 557
    label "immigration"
  ]
  node [
    id 558
    label "migracja"
  ]
  node [
    id 559
    label "Katar"
  ]
  node [
    id 560
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 561
    label "Mazowsze"
  ]
  node [
    id 562
    label "Libia"
  ]
  node [
    id 563
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 564
    label "Gwatemala"
  ]
  node [
    id 565
    label "Anglia"
  ]
  node [
    id 566
    label "Amazonia"
  ]
  node [
    id 567
    label "Afganistan"
  ]
  node [
    id 568
    label "Ekwador"
  ]
  node [
    id 569
    label "Bordeaux"
  ]
  node [
    id 570
    label "Tad&#380;ykistan"
  ]
  node [
    id 571
    label "Bhutan"
  ]
  node [
    id 572
    label "Argentyna"
  ]
  node [
    id 573
    label "D&#380;ibuti"
  ]
  node [
    id 574
    label "Wenezuela"
  ]
  node [
    id 575
    label "Ukraina"
  ]
  node [
    id 576
    label "Gabon"
  ]
  node [
    id 577
    label "Naddniestrze"
  ]
  node [
    id 578
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 579
    label "Europa_Zachodnia"
  ]
  node [
    id 580
    label "Armagnac"
  ]
  node [
    id 581
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 582
    label "Rwanda"
  ]
  node [
    id 583
    label "Liechtenstein"
  ]
  node [
    id 584
    label "Amhara"
  ]
  node [
    id 585
    label "Sri_Lanka"
  ]
  node [
    id 586
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 587
    label "Zamojszczyzna"
  ]
  node [
    id 588
    label "Madagaskar"
  ]
  node [
    id 589
    label "Tonga"
  ]
  node [
    id 590
    label "Kongo"
  ]
  node [
    id 591
    label "Bangladesz"
  ]
  node [
    id 592
    label "Kanada"
  ]
  node [
    id 593
    label "Ma&#322;opolska"
  ]
  node [
    id 594
    label "Wehrlen"
  ]
  node [
    id 595
    label "Turkiestan"
  ]
  node [
    id 596
    label "Algieria"
  ]
  node [
    id 597
    label "Noworosja"
  ]
  node [
    id 598
    label "Surinam"
  ]
  node [
    id 599
    label "Chile"
  ]
  node [
    id 600
    label "Sahara_Zachodnia"
  ]
  node [
    id 601
    label "Uganda"
  ]
  node [
    id 602
    label "Lubelszczyzna"
  ]
  node [
    id 603
    label "W&#281;gry"
  ]
  node [
    id 604
    label "Mezoameryka"
  ]
  node [
    id 605
    label "Birma"
  ]
  node [
    id 606
    label "Ba&#322;kany"
  ]
  node [
    id 607
    label "Kurdystan"
  ]
  node [
    id 608
    label "Kazachstan"
  ]
  node [
    id 609
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 610
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 611
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 612
    label "Armenia"
  ]
  node [
    id 613
    label "Tuwalu"
  ]
  node [
    id 614
    label "Timor_Wschodni"
  ]
  node [
    id 615
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 616
    label "Szkocja"
  ]
  node [
    id 617
    label "Baszkiria"
  ]
  node [
    id 618
    label "Tonkin"
  ]
  node [
    id 619
    label "Maghreb"
  ]
  node [
    id 620
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 621
    label "Izrael"
  ]
  node [
    id 622
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 623
    label "Nadrenia"
  ]
  node [
    id 624
    label "Estonia"
  ]
  node [
    id 625
    label "Komory"
  ]
  node [
    id 626
    label "Podhale"
  ]
  node [
    id 627
    label "Wielkopolska"
  ]
  node [
    id 628
    label "Zabajkale"
  ]
  node [
    id 629
    label "Kamerun"
  ]
  node [
    id 630
    label "Haiti"
  ]
  node [
    id 631
    label "Belize"
  ]
  node [
    id 632
    label "Sierra_Leone"
  ]
  node [
    id 633
    label "Apulia"
  ]
  node [
    id 634
    label "Luksemburg"
  ]
  node [
    id 635
    label "brzeg"
  ]
  node [
    id 636
    label "USA"
  ]
  node [
    id 637
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 638
    label "Barbados"
  ]
  node [
    id 639
    label "San_Marino"
  ]
  node [
    id 640
    label "Bu&#322;garia"
  ]
  node [
    id 641
    label "Wietnam"
  ]
  node [
    id 642
    label "Indonezja"
  ]
  node [
    id 643
    label "Bojkowszczyzna"
  ]
  node [
    id 644
    label "Malawi"
  ]
  node [
    id 645
    label "Francja"
  ]
  node [
    id 646
    label "Zambia"
  ]
  node [
    id 647
    label "Kujawy"
  ]
  node [
    id 648
    label "Angola"
  ]
  node [
    id 649
    label "Liguria"
  ]
  node [
    id 650
    label "Grenada"
  ]
  node [
    id 651
    label "Pamir"
  ]
  node [
    id 652
    label "Nepal"
  ]
  node [
    id 653
    label "Panama"
  ]
  node [
    id 654
    label "Rumunia"
  ]
  node [
    id 655
    label "Indochiny"
  ]
  node [
    id 656
    label "Podlasie"
  ]
  node [
    id 657
    label "Polinezja"
  ]
  node [
    id 658
    label "Kurpie"
  ]
  node [
    id 659
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 660
    label "S&#261;decczyzna"
  ]
  node [
    id 661
    label "Umbria"
  ]
  node [
    id 662
    label "Czarnog&#243;ra"
  ]
  node [
    id 663
    label "Malediwy"
  ]
  node [
    id 664
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 665
    label "S&#322;owacja"
  ]
  node [
    id 666
    label "Karaiby"
  ]
  node [
    id 667
    label "Ukraina_Zachodnia"
  ]
  node [
    id 668
    label "Kielecczyzna"
  ]
  node [
    id 669
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 670
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 671
    label "Egipt"
  ]
  node [
    id 672
    label "Kolumbia"
  ]
  node [
    id 673
    label "Mozambik"
  ]
  node [
    id 674
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 675
    label "Laos"
  ]
  node [
    id 676
    label "Burundi"
  ]
  node [
    id 677
    label "Suazi"
  ]
  node [
    id 678
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 679
    label "Czechy"
  ]
  node [
    id 680
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 681
    label "Wyspy_Marshalla"
  ]
  node [
    id 682
    label "Trynidad_i_Tobago"
  ]
  node [
    id 683
    label "Dominika"
  ]
  node [
    id 684
    label "Palau"
  ]
  node [
    id 685
    label "Syria"
  ]
  node [
    id 686
    label "Skandynawia"
  ]
  node [
    id 687
    label "Gwinea_Bissau"
  ]
  node [
    id 688
    label "Liberia"
  ]
  node [
    id 689
    label "Zimbabwe"
  ]
  node [
    id 690
    label "Polska"
  ]
  node [
    id 691
    label "Jamajka"
  ]
  node [
    id 692
    label "Tyrol"
  ]
  node [
    id 693
    label "Huculszczyzna"
  ]
  node [
    id 694
    label "Bory_Tucholskie"
  ]
  node [
    id 695
    label "Turyngia"
  ]
  node [
    id 696
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 697
    label "Dominikana"
  ]
  node [
    id 698
    label "Senegal"
  ]
  node [
    id 699
    label "Gruzja"
  ]
  node [
    id 700
    label "Chorwacja"
  ]
  node [
    id 701
    label "Togo"
  ]
  node [
    id 702
    label "Meksyk"
  ]
  node [
    id 703
    label "jednostka_administracyjna"
  ]
  node [
    id 704
    label "Macedonia"
  ]
  node [
    id 705
    label "Gujana"
  ]
  node [
    id 706
    label "Zair"
  ]
  node [
    id 707
    label "Kambod&#380;a"
  ]
  node [
    id 708
    label "Albania"
  ]
  node [
    id 709
    label "Mauritius"
  ]
  node [
    id 710
    label "Monako"
  ]
  node [
    id 711
    label "Gwinea"
  ]
  node [
    id 712
    label "Mali"
  ]
  node [
    id 713
    label "Nigeria"
  ]
  node [
    id 714
    label "Kalabria"
  ]
  node [
    id 715
    label "Hercegowina"
  ]
  node [
    id 716
    label "Kostaryka"
  ]
  node [
    id 717
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 718
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 719
    label "Lotaryngia"
  ]
  node [
    id 720
    label "Hanower"
  ]
  node [
    id 721
    label "Paragwaj"
  ]
  node [
    id 722
    label "W&#322;ochy"
  ]
  node [
    id 723
    label "Wyspy_Salomona"
  ]
  node [
    id 724
    label "Seszele"
  ]
  node [
    id 725
    label "Hiszpania"
  ]
  node [
    id 726
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 727
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 728
    label "Walia"
  ]
  node [
    id 729
    label "Boliwia"
  ]
  node [
    id 730
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 731
    label "Opolskie"
  ]
  node [
    id 732
    label "Kirgistan"
  ]
  node [
    id 733
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 734
    label "Irlandia"
  ]
  node [
    id 735
    label "Kampania"
  ]
  node [
    id 736
    label "Czad"
  ]
  node [
    id 737
    label "Irak"
  ]
  node [
    id 738
    label "Lesoto"
  ]
  node [
    id 739
    label "Malta"
  ]
  node [
    id 740
    label "Andora"
  ]
  node [
    id 741
    label "Sand&#380;ak"
  ]
  node [
    id 742
    label "Chiny"
  ]
  node [
    id 743
    label "Filipiny"
  ]
  node [
    id 744
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 745
    label "Syjon"
  ]
  node [
    id 746
    label "Niemcy"
  ]
  node [
    id 747
    label "Kabylia"
  ]
  node [
    id 748
    label "Lombardia"
  ]
  node [
    id 749
    label "Warmia"
  ]
  node [
    id 750
    label "Brazylia"
  ]
  node [
    id 751
    label "Nikaragua"
  ]
  node [
    id 752
    label "Pakistan"
  ]
  node [
    id 753
    label "&#321;emkowszczyzna"
  ]
  node [
    id 754
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 755
    label "Kaszmir"
  ]
  node [
    id 756
    label "Kenia"
  ]
  node [
    id 757
    label "Niger"
  ]
  node [
    id 758
    label "Tunezja"
  ]
  node [
    id 759
    label "Portugalia"
  ]
  node [
    id 760
    label "Fid&#380;i"
  ]
  node [
    id 761
    label "Maroko"
  ]
  node [
    id 762
    label "Botswana"
  ]
  node [
    id 763
    label "Tajlandia"
  ]
  node [
    id 764
    label "Australia"
  ]
  node [
    id 765
    label "&#321;&#243;dzkie"
  ]
  node [
    id 766
    label "Europa_Wschodnia"
  ]
  node [
    id 767
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 768
    label "Burkina_Faso"
  ]
  node [
    id 769
    label "Benin"
  ]
  node [
    id 770
    label "Tanzania"
  ]
  node [
    id 771
    label "interior"
  ]
  node [
    id 772
    label "Indie"
  ]
  node [
    id 773
    label "&#321;otwa"
  ]
  node [
    id 774
    label "Biskupizna"
  ]
  node [
    id 775
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 776
    label "Kiribati"
  ]
  node [
    id 777
    label "Kaukaz"
  ]
  node [
    id 778
    label "Antigua_i_Barbuda"
  ]
  node [
    id 779
    label "Rodezja"
  ]
  node [
    id 780
    label "Afryka_Wschodnia"
  ]
  node [
    id 781
    label "Cypr"
  ]
  node [
    id 782
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 783
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 784
    label "Podkarpacie"
  ]
  node [
    id 785
    label "obszar"
  ]
  node [
    id 786
    label "Peru"
  ]
  node [
    id 787
    label "Toskania"
  ]
  node [
    id 788
    label "Afryka_Zachodnia"
  ]
  node [
    id 789
    label "Austria"
  ]
  node [
    id 790
    label "Podbeskidzie"
  ]
  node [
    id 791
    label "Urugwaj"
  ]
  node [
    id 792
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 793
    label "Jordania"
  ]
  node [
    id 794
    label "Bo&#347;nia"
  ]
  node [
    id 795
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 796
    label "Grecja"
  ]
  node [
    id 797
    label "Azerbejd&#380;an"
  ]
  node [
    id 798
    label "Oceania"
  ]
  node [
    id 799
    label "Turcja"
  ]
  node [
    id 800
    label "Pomorze_Zachodnie"
  ]
  node [
    id 801
    label "Samoa"
  ]
  node [
    id 802
    label "Powi&#347;le"
  ]
  node [
    id 803
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 804
    label "ziemia"
  ]
  node [
    id 805
    label "Oman"
  ]
  node [
    id 806
    label "Sudan"
  ]
  node [
    id 807
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 808
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 809
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 810
    label "Uzbekistan"
  ]
  node [
    id 811
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 812
    label "Honduras"
  ]
  node [
    id 813
    label "Mongolia"
  ]
  node [
    id 814
    label "Portoryko"
  ]
  node [
    id 815
    label "Kaszuby"
  ]
  node [
    id 816
    label "Ko&#322;yma"
  ]
  node [
    id 817
    label "Szlezwik"
  ]
  node [
    id 818
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 819
    label "Serbia"
  ]
  node [
    id 820
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 821
    label "Tajwan"
  ]
  node [
    id 822
    label "Wielka_Brytania"
  ]
  node [
    id 823
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 824
    label "Liban"
  ]
  node [
    id 825
    label "Japonia"
  ]
  node [
    id 826
    label "Ghana"
  ]
  node [
    id 827
    label "Bahrajn"
  ]
  node [
    id 828
    label "Belgia"
  ]
  node [
    id 829
    label "Etiopia"
  ]
  node [
    id 830
    label "Mikronezja"
  ]
  node [
    id 831
    label "Polesie"
  ]
  node [
    id 832
    label "Kuwejt"
  ]
  node [
    id 833
    label "Kerala"
  ]
  node [
    id 834
    label "Mazury"
  ]
  node [
    id 835
    label "Bahamy"
  ]
  node [
    id 836
    label "Rosja"
  ]
  node [
    id 837
    label "Mo&#322;dawia"
  ]
  node [
    id 838
    label "Palestyna"
  ]
  node [
    id 839
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 840
    label "Lauda"
  ]
  node [
    id 841
    label "Azja_Wschodnia"
  ]
  node [
    id 842
    label "Litwa"
  ]
  node [
    id 843
    label "S&#322;owenia"
  ]
  node [
    id 844
    label "Szwajcaria"
  ]
  node [
    id 845
    label "Erytrea"
  ]
  node [
    id 846
    label "Lubuskie"
  ]
  node [
    id 847
    label "Kuba"
  ]
  node [
    id 848
    label "Arabia_Saudyjska"
  ]
  node [
    id 849
    label "Galicja"
  ]
  node [
    id 850
    label "Zakarpacie"
  ]
  node [
    id 851
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 852
    label "Laponia"
  ]
  node [
    id 853
    label "granica_pa&#324;stwa"
  ]
  node [
    id 854
    label "Malezja"
  ]
  node [
    id 855
    label "Korea"
  ]
  node [
    id 856
    label "Yorkshire"
  ]
  node [
    id 857
    label "Bawaria"
  ]
  node [
    id 858
    label "Zag&#243;rze"
  ]
  node [
    id 859
    label "Jemen"
  ]
  node [
    id 860
    label "Nowa_Zelandia"
  ]
  node [
    id 861
    label "Andaluzja"
  ]
  node [
    id 862
    label "Namibia"
  ]
  node [
    id 863
    label "Nauru"
  ]
  node [
    id 864
    label "&#379;ywiecczyzna"
  ]
  node [
    id 865
    label "Brunei"
  ]
  node [
    id 866
    label "Oksytania"
  ]
  node [
    id 867
    label "Opolszczyzna"
  ]
  node [
    id 868
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 869
    label "Kociewie"
  ]
  node [
    id 870
    label "Khitai"
  ]
  node [
    id 871
    label "Mauretania"
  ]
  node [
    id 872
    label "Iran"
  ]
  node [
    id 873
    label "Gambia"
  ]
  node [
    id 874
    label "Somalia"
  ]
  node [
    id 875
    label "Holandia"
  ]
  node [
    id 876
    label "Lasko"
  ]
  node [
    id 877
    label "Turkmenistan"
  ]
  node [
    id 878
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 879
    label "Salwador"
  ]
  node [
    id 880
    label "woda"
  ]
  node [
    id 881
    label "linia"
  ]
  node [
    id 882
    label "zbi&#243;r"
  ]
  node [
    id 883
    label "ekoton"
  ]
  node [
    id 884
    label "str&#261;d"
  ]
  node [
    id 885
    label "koniec"
  ]
  node [
    id 886
    label "plantowa&#263;"
  ]
  node [
    id 887
    label "zapadnia"
  ]
  node [
    id 888
    label "budynek"
  ]
  node [
    id 889
    label "skorupa_ziemska"
  ]
  node [
    id 890
    label "glinowanie"
  ]
  node [
    id 891
    label "martwica"
  ]
  node [
    id 892
    label "teren"
  ]
  node [
    id 893
    label "litosfera"
  ]
  node [
    id 894
    label "penetrator"
  ]
  node [
    id 895
    label "glinowa&#263;"
  ]
  node [
    id 896
    label "domain"
  ]
  node [
    id 897
    label "podglebie"
  ]
  node [
    id 898
    label "kompleks_sorpcyjny"
  ]
  node [
    id 899
    label "kort"
  ]
  node [
    id 900
    label "czynnik_produkcji"
  ]
  node [
    id 901
    label "pojazd"
  ]
  node [
    id 902
    label "powierzchnia"
  ]
  node [
    id 903
    label "pr&#243;chnica"
  ]
  node [
    id 904
    label "pomieszczenie"
  ]
  node [
    id 905
    label "ryzosfera"
  ]
  node [
    id 906
    label "dotleni&#263;"
  ]
  node [
    id 907
    label "glej"
  ]
  node [
    id 908
    label "pa&#324;stwo"
  ]
  node [
    id 909
    label "posadzka"
  ]
  node [
    id 910
    label "geosystem"
  ]
  node [
    id 911
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 912
    label "przestrze&#324;"
  ]
  node [
    id 913
    label "podmiot"
  ]
  node [
    id 914
    label "jednostka_organizacyjna"
  ]
  node [
    id 915
    label "struktura"
  ]
  node [
    id 916
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 917
    label "TOPR"
  ]
  node [
    id 918
    label "endecki"
  ]
  node [
    id 919
    label "zesp&#243;&#322;"
  ]
  node [
    id 920
    label "przedstawicielstwo"
  ]
  node [
    id 921
    label "od&#322;am"
  ]
  node [
    id 922
    label "Cepelia"
  ]
  node [
    id 923
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 924
    label "ZBoWiD"
  ]
  node [
    id 925
    label "organization"
  ]
  node [
    id 926
    label "centrala"
  ]
  node [
    id 927
    label "GOPR"
  ]
  node [
    id 928
    label "ZOMO"
  ]
  node [
    id 929
    label "ZMP"
  ]
  node [
    id 930
    label "komitet_koordynacyjny"
  ]
  node [
    id 931
    label "przybud&#243;wka"
  ]
  node [
    id 932
    label "boj&#243;wka"
  ]
  node [
    id 933
    label "p&#243;&#322;noc"
  ]
  node [
    id 934
    label "Kosowo"
  ]
  node [
    id 935
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 936
    label "Zab&#322;ocie"
  ]
  node [
    id 937
    label "zach&#243;d"
  ]
  node [
    id 938
    label "po&#322;udnie"
  ]
  node [
    id 939
    label "Pow&#261;zki"
  ]
  node [
    id 940
    label "Piotrowo"
  ]
  node [
    id 941
    label "Olszanica"
  ]
  node [
    id 942
    label "holarktyka"
  ]
  node [
    id 943
    label "Ruda_Pabianicka"
  ]
  node [
    id 944
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 945
    label "Ludwin&#243;w"
  ]
  node [
    id 946
    label "Arktyka"
  ]
  node [
    id 947
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 948
    label "Zabu&#380;e"
  ]
  node [
    id 949
    label "antroposfera"
  ]
  node [
    id 950
    label "terytorium"
  ]
  node [
    id 951
    label "Neogea"
  ]
  node [
    id 952
    label "Syberia_Zachodnia"
  ]
  node [
    id 953
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 954
    label "zakres"
  ]
  node [
    id 955
    label "pas_planetoid"
  ]
  node [
    id 956
    label "Syberia_Wschodnia"
  ]
  node [
    id 957
    label "Antarktyka"
  ]
  node [
    id 958
    label "Rakowice"
  ]
  node [
    id 959
    label "akrecja"
  ]
  node [
    id 960
    label "wymiar"
  ]
  node [
    id 961
    label "&#321;&#281;g"
  ]
  node [
    id 962
    label "Kresy_Zachodnie"
  ]
  node [
    id 963
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 964
    label "wsch&#243;d"
  ]
  node [
    id 965
    label "Notogea"
  ]
  node [
    id 966
    label "inti"
  ]
  node [
    id 967
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 968
    label "sol"
  ]
  node [
    id 969
    label "baht"
  ]
  node [
    id 970
    label "boliviano"
  ]
  node [
    id 971
    label "dong"
  ]
  node [
    id 972
    label "Annam"
  ]
  node [
    id 973
    label "colon"
  ]
  node [
    id 974
    label "Ameryka_Centralna"
  ]
  node [
    id 975
    label "Piemont"
  ]
  node [
    id 976
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 977
    label "NATO"
  ]
  node [
    id 978
    label "Sardynia"
  ]
  node [
    id 979
    label "Italia"
  ]
  node [
    id 980
    label "strefa_euro"
  ]
  node [
    id 981
    label "Ok&#281;cie"
  ]
  node [
    id 982
    label "Karyntia"
  ]
  node [
    id 983
    label "Romania"
  ]
  node [
    id 984
    label "Warszawa"
  ]
  node [
    id 985
    label "lir"
  ]
  node [
    id 986
    label "Sycylia"
  ]
  node [
    id 987
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 988
    label "Ad&#380;aria"
  ]
  node [
    id 989
    label "lari"
  ]
  node [
    id 990
    label "Jukatan"
  ]
  node [
    id 991
    label "dolar_Belize"
  ]
  node [
    id 992
    label "dolar"
  ]
  node [
    id 993
    label "Ohio"
  ]
  node [
    id 994
    label "P&#243;&#322;noc"
  ]
  node [
    id 995
    label "Nowy_York"
  ]
  node [
    id 996
    label "Illinois"
  ]
  node [
    id 997
    label "Po&#322;udnie"
  ]
  node [
    id 998
    label "Kalifornia"
  ]
  node [
    id 999
    label "Wirginia"
  ]
  node [
    id 1000
    label "Teksas"
  ]
  node [
    id 1001
    label "Waszyngton"
  ]
  node [
    id 1002
    label "zielona_karta"
  ]
  node [
    id 1003
    label "Alaska"
  ]
  node [
    id 1004
    label "Massachusetts"
  ]
  node [
    id 1005
    label "Hawaje"
  ]
  node [
    id 1006
    label "Maryland"
  ]
  node [
    id 1007
    label "Michigan"
  ]
  node [
    id 1008
    label "Arizona"
  ]
  node [
    id 1009
    label "Georgia"
  ]
  node [
    id 1010
    label "stan_wolny"
  ]
  node [
    id 1011
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1012
    label "Pensylwania"
  ]
  node [
    id 1013
    label "Luizjana"
  ]
  node [
    id 1014
    label "Nowy_Meksyk"
  ]
  node [
    id 1015
    label "Wuj_Sam"
  ]
  node [
    id 1016
    label "Alabama"
  ]
  node [
    id 1017
    label "Kansas"
  ]
  node [
    id 1018
    label "Oregon"
  ]
  node [
    id 1019
    label "Zach&#243;d"
  ]
  node [
    id 1020
    label "Oklahoma"
  ]
  node [
    id 1021
    label "Floryda"
  ]
  node [
    id 1022
    label "Hudson"
  ]
  node [
    id 1023
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1024
    label "somoni"
  ]
  node [
    id 1025
    label "euro"
  ]
  node [
    id 1026
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1027
    label "perper"
  ]
  node [
    id 1028
    label "Bengal"
  ]
  node [
    id 1029
    label "taka"
  ]
  node [
    id 1030
    label "Karelia"
  ]
  node [
    id 1031
    label "Mari_El"
  ]
  node [
    id 1032
    label "Inguszetia"
  ]
  node [
    id 1033
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1034
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1035
    label "Udmurcja"
  ]
  node [
    id 1036
    label "Newa"
  ]
  node [
    id 1037
    label "&#321;adoga"
  ]
  node [
    id 1038
    label "Czeczenia"
  ]
  node [
    id 1039
    label "Anadyr"
  ]
  node [
    id 1040
    label "Syberia"
  ]
  node [
    id 1041
    label "Tatarstan"
  ]
  node [
    id 1042
    label "Wszechrosja"
  ]
  node [
    id 1043
    label "Azja"
  ]
  node [
    id 1044
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1045
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1046
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1047
    label "Witim"
  ]
  node [
    id 1048
    label "Kamczatka"
  ]
  node [
    id 1049
    label "Jama&#322;"
  ]
  node [
    id 1050
    label "Dagestan"
  ]
  node [
    id 1051
    label "Tuwa"
  ]
  node [
    id 1052
    label "car"
  ]
  node [
    id 1053
    label "Komi"
  ]
  node [
    id 1054
    label "Czuwaszja"
  ]
  node [
    id 1055
    label "Chakasja"
  ]
  node [
    id 1056
    label "Perm"
  ]
  node [
    id 1057
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1058
    label "Ajon"
  ]
  node [
    id 1059
    label "Adygeja"
  ]
  node [
    id 1060
    label "Dniepr"
  ]
  node [
    id 1061
    label "rubel_rosyjski"
  ]
  node [
    id 1062
    label "Don"
  ]
  node [
    id 1063
    label "Mordowia"
  ]
  node [
    id 1064
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1065
    label "gourde"
  ]
  node [
    id 1066
    label "escudo_angolskie"
  ]
  node [
    id 1067
    label "kwanza"
  ]
  node [
    id 1068
    label "ariary"
  ]
  node [
    id 1069
    label "Ocean_Indyjski"
  ]
  node [
    id 1070
    label "frank_malgaski"
  ]
  node [
    id 1071
    label "Unia_Europejska"
  ]
  node [
    id 1072
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1073
    label "Windawa"
  ]
  node [
    id 1074
    label "&#379;mud&#378;"
  ]
  node [
    id 1075
    label "lit"
  ]
  node [
    id 1076
    label "Synaj"
  ]
  node [
    id 1077
    label "paraszyt"
  ]
  node [
    id 1078
    label "funt_egipski"
  ]
  node [
    id 1079
    label "birr"
  ]
  node [
    id 1080
    label "negus"
  ]
  node [
    id 1081
    label "peso_kolumbijskie"
  ]
  node [
    id 1082
    label "Orinoko"
  ]
  node [
    id 1083
    label "rial_katarski"
  ]
  node [
    id 1084
    label "dram"
  ]
  node [
    id 1085
    label "Limburgia"
  ]
  node [
    id 1086
    label "gulden"
  ]
  node [
    id 1087
    label "Zelandia"
  ]
  node [
    id 1088
    label "Niderlandy"
  ]
  node [
    id 1089
    label "Brabancja"
  ]
  node [
    id 1090
    label "cedi"
  ]
  node [
    id 1091
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1092
    label "milrejs"
  ]
  node [
    id 1093
    label "cruzado"
  ]
  node [
    id 1094
    label "real"
  ]
  node [
    id 1095
    label "frank_monakijski"
  ]
  node [
    id 1096
    label "Fryburg"
  ]
  node [
    id 1097
    label "Bazylea"
  ]
  node [
    id 1098
    label "Alpy"
  ]
  node [
    id 1099
    label "frank_szwajcarski"
  ]
  node [
    id 1100
    label "Helwecja"
  ]
  node [
    id 1101
    label "Berno"
  ]
  node [
    id 1102
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1103
    label "Dniestr"
  ]
  node [
    id 1104
    label "Gagauzja"
  ]
  node [
    id 1105
    label "Indie_Zachodnie"
  ]
  node [
    id 1106
    label "Sikkim"
  ]
  node [
    id 1107
    label "Asam"
  ]
  node [
    id 1108
    label "rupia_indyjska"
  ]
  node [
    id 1109
    label "Indie_Portugalskie"
  ]
  node [
    id 1110
    label "Indie_Wschodnie"
  ]
  node [
    id 1111
    label "Bollywood"
  ]
  node [
    id 1112
    label "Pend&#380;ab"
  ]
  node [
    id 1113
    label "boliwar"
  ]
  node [
    id 1114
    label "naira"
  ]
  node [
    id 1115
    label "frank_gwinejski"
  ]
  node [
    id 1116
    label "sum"
  ]
  node [
    id 1117
    label "Karaka&#322;pacja"
  ]
  node [
    id 1118
    label "dolar_liberyjski"
  ]
  node [
    id 1119
    label "Dacja"
  ]
  node [
    id 1120
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1121
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1122
    label "Dobrud&#380;a"
  ]
  node [
    id 1123
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1124
    label "dolar_namibijski"
  ]
  node [
    id 1125
    label "kuna"
  ]
  node [
    id 1126
    label "Rugia"
  ]
  node [
    id 1127
    label "Saksonia"
  ]
  node [
    id 1128
    label "Dolna_Saksonia"
  ]
  node [
    id 1129
    label "Anglosas"
  ]
  node [
    id 1130
    label "Hesja"
  ]
  node [
    id 1131
    label "Wirtembergia"
  ]
  node [
    id 1132
    label "Po&#322;abie"
  ]
  node [
    id 1133
    label "Germania"
  ]
  node [
    id 1134
    label "Frankonia"
  ]
  node [
    id 1135
    label "Badenia"
  ]
  node [
    id 1136
    label "Holsztyn"
  ]
  node [
    id 1137
    label "marka"
  ]
  node [
    id 1138
    label "Brandenburgia"
  ]
  node [
    id 1139
    label "Szwabia"
  ]
  node [
    id 1140
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1141
    label "Westfalia"
  ]
  node [
    id 1142
    label "Helgoland"
  ]
  node [
    id 1143
    label "Karlsbad"
  ]
  node [
    id 1144
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1145
    label "korona_w&#281;gierska"
  ]
  node [
    id 1146
    label "forint"
  ]
  node [
    id 1147
    label "Lipt&#243;w"
  ]
  node [
    id 1148
    label "tenge"
  ]
  node [
    id 1149
    label "szach"
  ]
  node [
    id 1150
    label "Baktria"
  ]
  node [
    id 1151
    label "afgani"
  ]
  node [
    id 1152
    label "kip"
  ]
  node [
    id 1153
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1154
    label "Salzburg"
  ]
  node [
    id 1155
    label "Rakuzy"
  ]
  node [
    id 1156
    label "Dyja"
  ]
  node [
    id 1157
    label "konsulent"
  ]
  node [
    id 1158
    label "szyling_austryjacki"
  ]
  node [
    id 1159
    label "peso_urugwajskie"
  ]
  node [
    id 1160
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1161
    label "korona_esto&#324;ska"
  ]
  node [
    id 1162
    label "Inflanty"
  ]
  node [
    id 1163
    label "marka_esto&#324;ska"
  ]
  node [
    id 1164
    label "tala"
  ]
  node [
    id 1165
    label "Podole"
  ]
  node [
    id 1166
    label "Wsch&#243;d"
  ]
  node [
    id 1167
    label "Naddnieprze"
  ]
  node [
    id 1168
    label "Ma&#322;orosja"
  ]
  node [
    id 1169
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1170
    label "Nadbu&#380;e"
  ]
  node [
    id 1171
    label "hrywna"
  ]
  node [
    id 1172
    label "Zaporo&#380;e"
  ]
  node [
    id 1173
    label "Krym"
  ]
  node [
    id 1174
    label "Przykarpacie"
  ]
  node [
    id 1175
    label "Kozaczyzna"
  ]
  node [
    id 1176
    label "karbowaniec"
  ]
  node [
    id 1177
    label "riel"
  ]
  node [
    id 1178
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1179
    label "kyat"
  ]
  node [
    id 1180
    label "Arakan"
  ]
  node [
    id 1181
    label "funt_liba&#324;ski"
  ]
  node [
    id 1182
    label "Mariany"
  ]
  node [
    id 1183
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1184
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1185
    label "dinar_algierski"
  ]
  node [
    id 1186
    label "ringgit"
  ]
  node [
    id 1187
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1188
    label "Borneo"
  ]
  node [
    id 1189
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1190
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1191
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1192
    label "lira_izraelska"
  ]
  node [
    id 1193
    label "szekel"
  ]
  node [
    id 1194
    label "Galilea"
  ]
  node [
    id 1195
    label "Judea"
  ]
  node [
    id 1196
    label "tolar"
  ]
  node [
    id 1197
    label "frank_luksemburski"
  ]
  node [
    id 1198
    label "lempira"
  ]
  node [
    id 1199
    label "Pozna&#324;"
  ]
  node [
    id 1200
    label "lira_malta&#324;ska"
  ]
  node [
    id 1201
    label "Gozo"
  ]
  node [
    id 1202
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1203
    label "Paros"
  ]
  node [
    id 1204
    label "Epir"
  ]
  node [
    id 1205
    label "panhellenizm"
  ]
  node [
    id 1206
    label "Eubea"
  ]
  node [
    id 1207
    label "Rodos"
  ]
  node [
    id 1208
    label "Achaja"
  ]
  node [
    id 1209
    label "Termopile"
  ]
  node [
    id 1210
    label "Attyka"
  ]
  node [
    id 1211
    label "Hellada"
  ]
  node [
    id 1212
    label "Etolia"
  ]
  node [
    id 1213
    label "palestra"
  ]
  node [
    id 1214
    label "Kreta"
  ]
  node [
    id 1215
    label "drachma"
  ]
  node [
    id 1216
    label "Olimp"
  ]
  node [
    id 1217
    label "Tesalia"
  ]
  node [
    id 1218
    label "Peloponez"
  ]
  node [
    id 1219
    label "Eolia"
  ]
  node [
    id 1220
    label "Beocja"
  ]
  node [
    id 1221
    label "Parnas"
  ]
  node [
    id 1222
    label "Lesbos"
  ]
  node [
    id 1223
    label "Atlantyk"
  ]
  node [
    id 1224
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1225
    label "Ulster"
  ]
  node [
    id 1226
    label "funt_irlandzki"
  ]
  node [
    id 1227
    label "Buriaci"
  ]
  node [
    id 1228
    label "tugrik"
  ]
  node [
    id 1229
    label "ajmak"
  ]
  node [
    id 1230
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1231
    label "Pikardia"
  ]
  node [
    id 1232
    label "Alzacja"
  ]
  node [
    id 1233
    label "Masyw_Centralny"
  ]
  node [
    id 1234
    label "Akwitania"
  ]
  node [
    id 1235
    label "Sekwana"
  ]
  node [
    id 1236
    label "Langwedocja"
  ]
  node [
    id 1237
    label "Martynika"
  ]
  node [
    id 1238
    label "Bretania"
  ]
  node [
    id 1239
    label "Sabaudia"
  ]
  node [
    id 1240
    label "Korsyka"
  ]
  node [
    id 1241
    label "Normandia"
  ]
  node [
    id 1242
    label "Gaskonia"
  ]
  node [
    id 1243
    label "Burgundia"
  ]
  node [
    id 1244
    label "frank_francuski"
  ]
  node [
    id 1245
    label "Wandea"
  ]
  node [
    id 1246
    label "Prowansja"
  ]
  node [
    id 1247
    label "Gwadelupa"
  ]
  node [
    id 1248
    label "lew"
  ]
  node [
    id 1249
    label "c&#243;rdoba"
  ]
  node [
    id 1250
    label "dolar_Zimbabwe"
  ]
  node [
    id 1251
    label "frank_rwandyjski"
  ]
  node [
    id 1252
    label "kwacha_zambijska"
  ]
  node [
    id 1253
    label "Kurlandia"
  ]
  node [
    id 1254
    label "&#322;at"
  ]
  node [
    id 1255
    label "Liwonia"
  ]
  node [
    id 1256
    label "rubel_&#322;otewski"
  ]
  node [
    id 1257
    label "Himalaje"
  ]
  node [
    id 1258
    label "rupia_nepalska"
  ]
  node [
    id 1259
    label "funt_suda&#324;ski"
  ]
  node [
    id 1260
    label "dolar_bahamski"
  ]
  node [
    id 1261
    label "Wielka_Bahama"
  ]
  node [
    id 1262
    label "Pa&#322;uki"
  ]
  node [
    id 1263
    label "Wolin"
  ]
  node [
    id 1264
    label "z&#322;oty"
  ]
  node [
    id 1265
    label "So&#322;a"
  ]
  node [
    id 1266
    label "Krajna"
  ]
  node [
    id 1267
    label "Suwalszczyzna"
  ]
  node [
    id 1268
    label "barwy_polskie"
  ]
  node [
    id 1269
    label "Izera"
  ]
  node [
    id 1270
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1271
    label "Kaczawa"
  ]
  node [
    id 1272
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1273
    label "Wis&#322;a"
  ]
  node [
    id 1274
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1275
    label "Antyle"
  ]
  node [
    id 1276
    label "dolar_Tuvalu"
  ]
  node [
    id 1277
    label "dinar_iracki"
  ]
  node [
    id 1278
    label "korona_s&#322;owacka"
  ]
  node [
    id 1279
    label "Turiec"
  ]
  node [
    id 1280
    label "jen"
  ]
  node [
    id 1281
    label "jinja"
  ]
  node [
    id 1282
    label "Okinawa"
  ]
  node [
    id 1283
    label "Japonica"
  ]
  node [
    id 1284
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1285
    label "szyling_kenijski"
  ]
  node [
    id 1286
    label "peso_chilijskie"
  ]
  node [
    id 1287
    label "Zanzibar"
  ]
  node [
    id 1288
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1289
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1290
    label "Cebu"
  ]
  node [
    id 1291
    label "Sahara"
  ]
  node [
    id 1292
    label "Tasmania"
  ]
  node [
    id 1293
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1294
    label "dolar_australijski"
  ]
  node [
    id 1295
    label "Quebec"
  ]
  node [
    id 1296
    label "dolar_kanadyjski"
  ]
  node [
    id 1297
    label "Nowa_Fundlandia"
  ]
  node [
    id 1298
    label "quetzal"
  ]
  node [
    id 1299
    label "Manica"
  ]
  node [
    id 1300
    label "escudo_mozambickie"
  ]
  node [
    id 1301
    label "Cabo_Delgado"
  ]
  node [
    id 1302
    label "Inhambane"
  ]
  node [
    id 1303
    label "Maputo"
  ]
  node [
    id 1304
    label "Gaza"
  ]
  node [
    id 1305
    label "Niasa"
  ]
  node [
    id 1306
    label "Nampula"
  ]
  node [
    id 1307
    label "metical"
  ]
  node [
    id 1308
    label "frank_tunezyjski"
  ]
  node [
    id 1309
    label "dinar_tunezyjski"
  ]
  node [
    id 1310
    label "lud"
  ]
  node [
    id 1311
    label "frank_kongijski"
  ]
  node [
    id 1312
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1313
    label "dinar_Bahrajnu"
  ]
  node [
    id 1314
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1315
    label "escudo_portugalskie"
  ]
  node [
    id 1316
    label "Melanezja"
  ]
  node [
    id 1317
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1318
    label "d&#380;amahirijja"
  ]
  node [
    id 1319
    label "dinar_libijski"
  ]
  node [
    id 1320
    label "balboa"
  ]
  node [
    id 1321
    label "dolar_surinamski"
  ]
  node [
    id 1322
    label "dolar_Brunei"
  ]
  node [
    id 1323
    label "Estremadura"
  ]
  node [
    id 1324
    label "Kastylia"
  ]
  node [
    id 1325
    label "Aragonia"
  ]
  node [
    id 1326
    label "hacjender"
  ]
  node [
    id 1327
    label "Asturia"
  ]
  node [
    id 1328
    label "Baskonia"
  ]
  node [
    id 1329
    label "Majorka"
  ]
  node [
    id 1330
    label "Walencja"
  ]
  node [
    id 1331
    label "peseta"
  ]
  node [
    id 1332
    label "Katalonia"
  ]
  node [
    id 1333
    label "Luksemburgia"
  ]
  node [
    id 1334
    label "frank_belgijski"
  ]
  node [
    id 1335
    label "Walonia"
  ]
  node [
    id 1336
    label "Flandria"
  ]
  node [
    id 1337
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1338
    label "dolar_Barbadosu"
  ]
  node [
    id 1339
    label "korona_czeska"
  ]
  node [
    id 1340
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1341
    label "Wojwodina"
  ]
  node [
    id 1342
    label "dinar_serbski"
  ]
  node [
    id 1343
    label "funt_syryjski"
  ]
  node [
    id 1344
    label "alawizm"
  ]
  node [
    id 1345
    label "Szantung"
  ]
  node [
    id 1346
    label "Chiny_Zachodnie"
  ]
  node [
    id 1347
    label "Kuantung"
  ]
  node [
    id 1348
    label "D&#380;ungaria"
  ]
  node [
    id 1349
    label "yuan"
  ]
  node [
    id 1350
    label "Hongkong"
  ]
  node [
    id 1351
    label "Chiny_Wschodnie"
  ]
  node [
    id 1352
    label "Guangdong"
  ]
  node [
    id 1353
    label "Junnan"
  ]
  node [
    id 1354
    label "Mand&#380;uria"
  ]
  node [
    id 1355
    label "Syczuan"
  ]
  node [
    id 1356
    label "zair"
  ]
  node [
    id 1357
    label "Katanga"
  ]
  node [
    id 1358
    label "ugija"
  ]
  node [
    id 1359
    label "dalasi"
  ]
  node [
    id 1360
    label "funt_cypryjski"
  ]
  node [
    id 1361
    label "Afrodyzje"
  ]
  node [
    id 1362
    label "para"
  ]
  node [
    id 1363
    label "lek"
  ]
  node [
    id 1364
    label "frank_alba&#324;ski"
  ]
  node [
    id 1365
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1366
    label "dolar_jamajski"
  ]
  node [
    id 1367
    label "kafar"
  ]
  node [
    id 1368
    label "Ocean_Spokojny"
  ]
  node [
    id 1369
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1370
    label "som"
  ]
  node [
    id 1371
    label "guarani"
  ]
  node [
    id 1372
    label "rial_ira&#324;ski"
  ]
  node [
    id 1373
    label "mu&#322;&#322;a"
  ]
  node [
    id 1374
    label "Persja"
  ]
  node [
    id 1375
    label "Jawa"
  ]
  node [
    id 1376
    label "Sumatra"
  ]
  node [
    id 1377
    label "rupia_indonezyjska"
  ]
  node [
    id 1378
    label "Nowa_Gwinea"
  ]
  node [
    id 1379
    label "Moluki"
  ]
  node [
    id 1380
    label "szyling_somalijski"
  ]
  node [
    id 1381
    label "szyling_ugandyjski"
  ]
  node [
    id 1382
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1383
    label "Ujgur"
  ]
  node [
    id 1384
    label "Azja_Mniejsza"
  ]
  node [
    id 1385
    label "lira_turecka"
  ]
  node [
    id 1386
    label "Pireneje"
  ]
  node [
    id 1387
    label "nakfa"
  ]
  node [
    id 1388
    label "won"
  ]
  node [
    id 1389
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1390
    label "&#346;wite&#378;"
  ]
  node [
    id 1391
    label "dinar_kuwejcki"
  ]
  node [
    id 1392
    label "Nachiczewan"
  ]
  node [
    id 1393
    label "manat_azerski"
  ]
  node [
    id 1394
    label "Karabach"
  ]
  node [
    id 1395
    label "dolar_Kiribati"
  ]
  node [
    id 1396
    label "moszaw"
  ]
  node [
    id 1397
    label "Kanaan"
  ]
  node [
    id 1398
    label "Aruba"
  ]
  node [
    id 1399
    label "Kajmany"
  ]
  node [
    id 1400
    label "Anguilla"
  ]
  node [
    id 1401
    label "Mogielnica"
  ]
  node [
    id 1402
    label "jezioro"
  ]
  node [
    id 1403
    label "Rumelia"
  ]
  node [
    id 1404
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1405
    label "Poprad"
  ]
  node [
    id 1406
    label "Tatry"
  ]
  node [
    id 1407
    label "Podtatrze"
  ]
  node [
    id 1408
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1409
    label "Austro-W&#281;gry"
  ]
  node [
    id 1410
    label "Biskupice"
  ]
  node [
    id 1411
    label "Iwanowice"
  ]
  node [
    id 1412
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1413
    label "Rogo&#378;nik"
  ]
  node [
    id 1414
    label "Ropa"
  ]
  node [
    id 1415
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1416
    label "Karpaty"
  ]
  node [
    id 1417
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1418
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1419
    label "Beskid_Niski"
  ]
  node [
    id 1420
    label "Etruria"
  ]
  node [
    id 1421
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1422
    label "Bojanowo"
  ]
  node [
    id 1423
    label "Obra"
  ]
  node [
    id 1424
    label "Wilkowo_Polskie"
  ]
  node [
    id 1425
    label "Dobra"
  ]
  node [
    id 1426
    label "Buriacja"
  ]
  node [
    id 1427
    label "Rozewie"
  ]
  node [
    id 1428
    label "&#346;l&#261;sk"
  ]
  node [
    id 1429
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1430
    label "Norwegia"
  ]
  node [
    id 1431
    label "Szwecja"
  ]
  node [
    id 1432
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1433
    label "Finlandia"
  ]
  node [
    id 1434
    label "Wiktoria"
  ]
  node [
    id 1435
    label "Guernsey"
  ]
  node [
    id 1436
    label "Conrad"
  ]
  node [
    id 1437
    label "funt_szterling"
  ]
  node [
    id 1438
    label "Portland"
  ]
  node [
    id 1439
    label "El&#380;bieta_I"
  ]
  node [
    id 1440
    label "Kornwalia"
  ]
  node [
    id 1441
    label "Amazonka"
  ]
  node [
    id 1442
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1443
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1444
    label "Moza"
  ]
  node [
    id 1445
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1446
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1447
    label "Paj&#281;czno"
  ]
  node [
    id 1448
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1449
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1450
    label "Gop&#322;o"
  ]
  node [
    id 1451
    label "Jerozolima"
  ]
  node [
    id 1452
    label "Dolna_Frankonia"
  ]
  node [
    id 1453
    label "funt_szkocki"
  ]
  node [
    id 1454
    label "Kaledonia"
  ]
  node [
    id 1455
    label "Abchazja"
  ]
  node [
    id 1456
    label "Sarmata"
  ]
  node [
    id 1457
    label "Eurazja"
  ]
  node [
    id 1458
    label "Mariensztat"
  ]
  node [
    id 1459
    label "punkt"
  ]
  node [
    id 1460
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 1461
    label "thing"
  ]
  node [
    id 1462
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1463
    label "by&#263;"
  ]
  node [
    id 1464
    label "trwa&#263;"
  ]
  node [
    id 1465
    label "use"
  ]
  node [
    id 1466
    label "suffice"
  ]
  node [
    id 1467
    label "pracowa&#263;"
  ]
  node [
    id 1468
    label "match"
  ]
  node [
    id 1469
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 1470
    label "pies"
  ]
  node [
    id 1471
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 1472
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1473
    label "wait"
  ]
  node [
    id 1474
    label "pomaga&#263;"
  ]
  node [
    id 1475
    label "object"
  ]
  node [
    id 1476
    label "temat"
  ]
  node [
    id 1477
    label "wpadni&#281;cie"
  ]
  node [
    id 1478
    label "mienie"
  ]
  node [
    id 1479
    label "wpa&#347;&#263;"
  ]
  node [
    id 1480
    label "wpadanie"
  ]
  node [
    id 1481
    label "wpada&#263;"
  ]
  node [
    id 1482
    label "jutro"
  ]
  node [
    id 1483
    label "czas"
  ]
  node [
    id 1484
    label "warunek_lokalowy"
  ]
  node [
    id 1485
    label "plac"
  ]
  node [
    id 1486
    label "location"
  ]
  node [
    id 1487
    label "uwaga"
  ]
  node [
    id 1488
    label "status"
  ]
  node [
    id 1489
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1490
    label "chwila"
  ]
  node [
    id 1491
    label "rz&#261;d"
  ]
  node [
    id 1492
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1493
    label "sprawa"
  ]
  node [
    id 1494
    label "ust&#281;p"
  ]
  node [
    id 1495
    label "plan"
  ]
  node [
    id 1496
    label "problemat"
  ]
  node [
    id 1497
    label "plamka"
  ]
  node [
    id 1498
    label "stopie&#324;_pisma"
  ]
  node [
    id 1499
    label "jednostka"
  ]
  node [
    id 1500
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1501
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1502
    label "mark"
  ]
  node [
    id 1503
    label "prosta"
  ]
  node [
    id 1504
    label "problematyka"
  ]
  node [
    id 1505
    label "zapunktowa&#263;"
  ]
  node [
    id 1506
    label "podpunkt"
  ]
  node [
    id 1507
    label "wojsko"
  ]
  node [
    id 1508
    label "kres"
  ]
  node [
    id 1509
    label "pozycja"
  ]
  node [
    id 1510
    label "dzia&#322;anie"
  ]
  node [
    id 1511
    label "event"
  ]
  node [
    id 1512
    label "przyczyna"
  ]
  node [
    id 1513
    label "entertainment"
  ]
  node [
    id 1514
    label "consumption"
  ]
  node [
    id 1515
    label "spowodowanie"
  ]
  node [
    id 1516
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1517
    label "erecting"
  ]
  node [
    id 1518
    label "movement"
  ]
  node [
    id 1519
    label "zacz&#281;cie"
  ]
  node [
    id 1520
    label "zareagowanie"
  ]
  node [
    id 1521
    label "narobienie"
  ]
  node [
    id 1522
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1523
    label "creation"
  ]
  node [
    id 1524
    label "porobienie"
  ]
  node [
    id 1525
    label "activity"
  ]
  node [
    id 1526
    label "bezproblemowy"
  ]
  node [
    id 1527
    label "wydarzenie"
  ]
  node [
    id 1528
    label "campaign"
  ]
  node [
    id 1529
    label "causing"
  ]
  node [
    id 1530
    label "discourtesy"
  ]
  node [
    id 1531
    label "odj&#281;cie"
  ]
  node [
    id 1532
    label "post&#261;pienie"
  ]
  node [
    id 1533
    label "opening"
  ]
  node [
    id 1534
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1535
    label "wyra&#380;enie"
  ]
  node [
    id 1536
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1537
    label "po&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1538
    label "poczucie"
  ]
  node [
    id 1539
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1540
    label "najem"
  ]
  node [
    id 1541
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1542
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1543
    label "zak&#322;ad"
  ]
  node [
    id 1544
    label "stosunek_pracy"
  ]
  node [
    id 1545
    label "benedykty&#324;ski"
  ]
  node [
    id 1546
    label "poda&#380;_pracy"
  ]
  node [
    id 1547
    label "pracowanie"
  ]
  node [
    id 1548
    label "tyrka"
  ]
  node [
    id 1549
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1550
    label "zaw&#243;d"
  ]
  node [
    id 1551
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1552
    label "tynkarski"
  ]
  node [
    id 1553
    label "zmiana"
  ]
  node [
    id 1554
    label "zobowi&#261;zanie"
  ]
  node [
    id 1555
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1556
    label "p&#322;&#243;d"
  ]
  node [
    id 1557
    label "work"
  ]
  node [
    id 1558
    label "stosunek_prawny"
  ]
  node [
    id 1559
    label "oblig"
  ]
  node [
    id 1560
    label "uregulowa&#263;"
  ]
  node [
    id 1561
    label "oddzia&#322;anie"
  ]
  node [
    id 1562
    label "occupation"
  ]
  node [
    id 1563
    label "duty"
  ]
  node [
    id 1564
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 1565
    label "zapowied&#378;"
  ]
  node [
    id 1566
    label "obowi&#261;zek"
  ]
  node [
    id 1567
    label "zapewnienie"
  ]
  node [
    id 1568
    label "miejsce_pracy"
  ]
  node [
    id 1569
    label "zak&#322;adka"
  ]
  node [
    id 1570
    label "wyko&#324;czenie"
  ]
  node [
    id 1571
    label "czyn"
  ]
  node [
    id 1572
    label "company"
  ]
  node [
    id 1573
    label "umowa"
  ]
  node [
    id 1574
    label "&#321;ubianka"
  ]
  node [
    id 1575
    label "dzia&#322;_personalny"
  ]
  node [
    id 1576
    label "Kreml"
  ]
  node [
    id 1577
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1578
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1579
    label "sadowisko"
  ]
  node [
    id 1580
    label "rewizja"
  ]
  node [
    id 1581
    label "passage"
  ]
  node [
    id 1582
    label "oznaka"
  ]
  node [
    id 1583
    label "change"
  ]
  node [
    id 1584
    label "ferment"
  ]
  node [
    id 1585
    label "komplet"
  ]
  node [
    id 1586
    label "anatomopatolog"
  ]
  node [
    id 1587
    label "zmianka"
  ]
  node [
    id 1588
    label "amendment"
  ]
  node [
    id 1589
    label "odmienianie"
  ]
  node [
    id 1590
    label "tura"
  ]
  node [
    id 1591
    label "cierpliwy"
  ]
  node [
    id 1592
    label "mozolny"
  ]
  node [
    id 1593
    label "wytrwa&#322;y"
  ]
  node [
    id 1594
    label "benedykty&#324;sko"
  ]
  node [
    id 1595
    label "po_benedykty&#324;sku"
  ]
  node [
    id 1596
    label "endeavor"
  ]
  node [
    id 1597
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1598
    label "mie&#263;_miejsce"
  ]
  node [
    id 1599
    label "podejmowa&#263;"
  ]
  node [
    id 1600
    label "dziama&#263;"
  ]
  node [
    id 1601
    label "do"
  ]
  node [
    id 1602
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1603
    label "bangla&#263;"
  ]
  node [
    id 1604
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 1605
    label "maszyna"
  ]
  node [
    id 1606
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1607
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1608
    label "tryb"
  ]
  node [
    id 1609
    label "funkcjonowa&#263;"
  ]
  node [
    id 1610
    label "zawodoznawstwo"
  ]
  node [
    id 1611
    label "emocja"
  ]
  node [
    id 1612
    label "office"
  ]
  node [
    id 1613
    label "kwalifikacje"
  ]
  node [
    id 1614
    label "craft"
  ]
  node [
    id 1615
    label "przepracowanie_si&#281;"
  ]
  node [
    id 1616
    label "zarz&#261;dzanie"
  ]
  node [
    id 1617
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 1618
    label "podlizanie_si&#281;"
  ]
  node [
    id 1619
    label "dopracowanie"
  ]
  node [
    id 1620
    label "podlizywanie_si&#281;"
  ]
  node [
    id 1621
    label "uruchamianie"
  ]
  node [
    id 1622
    label "d&#261;&#380;enie"
  ]
  node [
    id 1623
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 1624
    label "uruchomienie"
  ]
  node [
    id 1625
    label "nakr&#281;canie"
  ]
  node [
    id 1626
    label "funkcjonowanie"
  ]
  node [
    id 1627
    label "tr&#243;jstronny"
  ]
  node [
    id 1628
    label "postaranie_si&#281;"
  ]
  node [
    id 1629
    label "odpocz&#281;cie"
  ]
  node [
    id 1630
    label "nakr&#281;cenie"
  ]
  node [
    id 1631
    label "zatrzymanie"
  ]
  node [
    id 1632
    label "spracowanie_si&#281;"
  ]
  node [
    id 1633
    label "skakanie"
  ]
  node [
    id 1634
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 1635
    label "podtrzymywanie"
  ]
  node [
    id 1636
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1637
    label "zaprz&#281;ganie"
  ]
  node [
    id 1638
    label "podejmowanie"
  ]
  node [
    id 1639
    label "wyrabianie"
  ]
  node [
    id 1640
    label "dzianie_si&#281;"
  ]
  node [
    id 1641
    label "przepracowanie"
  ]
  node [
    id 1642
    label "poruszanie_si&#281;"
  ]
  node [
    id 1643
    label "funkcja"
  ]
  node [
    id 1644
    label "impact"
  ]
  node [
    id 1645
    label "przepracowywanie"
  ]
  node [
    id 1646
    label "awansowanie"
  ]
  node [
    id 1647
    label "courtship"
  ]
  node [
    id 1648
    label "zapracowanie"
  ]
  node [
    id 1649
    label "wyrobienie"
  ]
  node [
    id 1650
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 1651
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1652
    label "transakcja"
  ]
  node [
    id 1653
    label "lead"
  ]
  node [
    id 1654
    label "w&#322;adza"
  ]
  node [
    id 1655
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1656
    label "visualize"
  ]
  node [
    id 1657
    label "pozna&#263;"
  ]
  node [
    id 1658
    label "befall"
  ]
  node [
    id 1659
    label "go_steady"
  ]
  node [
    id 1660
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1661
    label "znale&#378;&#263;"
  ]
  node [
    id 1662
    label "pozyska&#263;"
  ]
  node [
    id 1663
    label "oceni&#263;"
  ]
  node [
    id 1664
    label "devise"
  ]
  node [
    id 1665
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 1666
    label "dozna&#263;"
  ]
  node [
    id 1667
    label "wykry&#263;"
  ]
  node [
    id 1668
    label "odzyska&#263;"
  ]
  node [
    id 1669
    label "znaj&#347;&#263;"
  ]
  node [
    id 1670
    label "invent"
  ]
  node [
    id 1671
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1672
    label "zrozumie&#263;"
  ]
  node [
    id 1673
    label "feel"
  ]
  node [
    id 1674
    label "topographic_point"
  ]
  node [
    id 1675
    label "przyswoi&#263;"
  ]
  node [
    id 1676
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 1677
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 1678
    label "teach"
  ]
  node [
    id 1679
    label "experience"
  ]
  node [
    id 1680
    label "monumentalny"
  ]
  node [
    id 1681
    label "mocny"
  ]
  node [
    id 1682
    label "trudny"
  ]
  node [
    id 1683
    label "kompletny"
  ]
  node [
    id 1684
    label "masywny"
  ]
  node [
    id 1685
    label "wielki"
  ]
  node [
    id 1686
    label "wymagaj&#261;cy"
  ]
  node [
    id 1687
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 1688
    label "przyswajalny"
  ]
  node [
    id 1689
    label "niezgrabny"
  ]
  node [
    id 1690
    label "liczny"
  ]
  node [
    id 1691
    label "nieprzejrzysty"
  ]
  node [
    id 1692
    label "niedelikatny"
  ]
  node [
    id 1693
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 1694
    label "intensywny"
  ]
  node [
    id 1695
    label "wolny"
  ]
  node [
    id 1696
    label "nieudany"
  ]
  node [
    id 1697
    label "zbrojny"
  ]
  node [
    id 1698
    label "dotkliwy"
  ]
  node [
    id 1699
    label "ci&#281;&#380;ko"
  ]
  node [
    id 1700
    label "bojowy"
  ]
  node [
    id 1701
    label "k&#322;opotliwy"
  ]
  node [
    id 1702
    label "ambitny"
  ]
  node [
    id 1703
    label "grubo"
  ]
  node [
    id 1704
    label "gro&#378;ny"
  ]
  node [
    id 1705
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 1706
    label "druzgoc&#261;cy"
  ]
  node [
    id 1707
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 1708
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1709
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 1710
    label "oci&#281;&#380;ale"
  ]
  node [
    id 1711
    label "obwis&#322;y"
  ]
  node [
    id 1712
    label "kompletnie"
  ]
  node [
    id 1713
    label "zupe&#322;ny"
  ]
  node [
    id 1714
    label "w_pizdu"
  ]
  node [
    id 1715
    label "pe&#322;ny"
  ]
  node [
    id 1716
    label "cz&#281;sty"
  ]
  node [
    id 1717
    label "licznie"
  ]
  node [
    id 1718
    label "rojenie_si&#281;"
  ]
  node [
    id 1719
    label "przykry"
  ]
  node [
    id 1720
    label "dotkliwie"
  ]
  node [
    id 1721
    label "znaczny"
  ]
  node [
    id 1722
    label "wyj&#261;tkowy"
  ]
  node [
    id 1723
    label "nieprzeci&#281;tny"
  ]
  node [
    id 1724
    label "wysoce"
  ]
  node [
    id 1725
    label "wa&#380;ny"
  ]
  node [
    id 1726
    label "wybitny"
  ]
  node [
    id 1727
    label "dupny"
  ]
  node [
    id 1728
    label "niestosowny"
  ]
  node [
    id 1729
    label "niekszta&#322;tny"
  ]
  node [
    id 1730
    label "niezgrabnie"
  ]
  node [
    id 1731
    label "du&#380;y"
  ]
  node [
    id 1732
    label "g&#322;o&#347;ny"
  ]
  node [
    id 1733
    label "masywnie"
  ]
  node [
    id 1734
    label "szybki"
  ]
  node [
    id 1735
    label "znacz&#261;cy"
  ]
  node [
    id 1736
    label "zwarty"
  ]
  node [
    id 1737
    label "efektywny"
  ]
  node [
    id 1738
    label "ogrodnictwo"
  ]
  node [
    id 1739
    label "dynamiczny"
  ]
  node [
    id 1740
    label "intensywnie"
  ]
  node [
    id 1741
    label "nieproporcjonalny"
  ]
  node [
    id 1742
    label "specjalny"
  ]
  node [
    id 1743
    label "mo&#380;liwy"
  ]
  node [
    id 1744
    label "rozrzedzenie"
  ]
  node [
    id 1745
    label "rzedni&#281;cie"
  ]
  node [
    id 1746
    label "niespieszny"
  ]
  node [
    id 1747
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1748
    label "wakowa&#263;"
  ]
  node [
    id 1749
    label "rozwadnianie"
  ]
  node [
    id 1750
    label "niezale&#380;ny"
  ]
  node [
    id 1751
    label "zrzedni&#281;cie"
  ]
  node [
    id 1752
    label "swobodnie"
  ]
  node [
    id 1753
    label "rozrzedzanie"
  ]
  node [
    id 1754
    label "rozwodnienie"
  ]
  node [
    id 1755
    label "strza&#322;"
  ]
  node [
    id 1756
    label "wolnie"
  ]
  node [
    id 1757
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1758
    label "wolno"
  ]
  node [
    id 1759
    label "lu&#378;no"
  ]
  node [
    id 1760
    label "skomplikowany"
  ]
  node [
    id 1761
    label "niebezpieczny"
  ]
  node [
    id 1762
    label "gro&#378;nie"
  ]
  node [
    id 1763
    label "nad&#261;sany"
  ]
  node [
    id 1764
    label "nieprzyjemny"
  ]
  node [
    id 1765
    label "niegrzeczny"
  ]
  node [
    id 1766
    label "nieoboj&#281;tny"
  ]
  node [
    id 1767
    label "niewra&#380;liwy"
  ]
  node [
    id 1768
    label "wytrzyma&#322;y"
  ]
  node [
    id 1769
    label "niedelikatnie"
  ]
  node [
    id 1770
    label "bojowo"
  ]
  node [
    id 1771
    label "pewny"
  ]
  node [
    id 1772
    label "&#347;mia&#322;y"
  ]
  node [
    id 1773
    label "zadziorny"
  ]
  node [
    id 1774
    label "bojowniczy"
  ]
  node [
    id 1775
    label "waleczny"
  ]
  node [
    id 1776
    label "zbrojnie"
  ]
  node [
    id 1777
    label "przyodziany"
  ]
  node [
    id 1778
    label "uzbrojony"
  ]
  node [
    id 1779
    label "ostry"
  ]
  node [
    id 1780
    label "k&#322;opotliwie"
  ]
  node [
    id 1781
    label "niewygodny"
  ]
  node [
    id 1782
    label "wymagaj&#261;co"
  ]
  node [
    id 1783
    label "monumentalnie"
  ]
  node [
    id 1784
    label "wznios&#322;y"
  ]
  node [
    id 1785
    label "nieudanie"
  ]
  node [
    id 1786
    label "nieciekawy"
  ]
  node [
    id 1787
    label "z&#322;y"
  ]
  node [
    id 1788
    label "niepodwa&#380;alny"
  ]
  node [
    id 1789
    label "zdecydowany"
  ]
  node [
    id 1790
    label "stabilny"
  ]
  node [
    id 1791
    label "krzepki"
  ]
  node [
    id 1792
    label "silny"
  ]
  node [
    id 1793
    label "wyrazisty"
  ]
  node [
    id 1794
    label "przekonuj&#261;cy"
  ]
  node [
    id 1795
    label "widoczny"
  ]
  node [
    id 1796
    label "mocno"
  ]
  node [
    id 1797
    label "wzmocni&#263;"
  ]
  node [
    id 1798
    label "wzmacnia&#263;"
  ]
  node [
    id 1799
    label "konkretny"
  ]
  node [
    id 1800
    label "silnie"
  ]
  node [
    id 1801
    label "meflochina"
  ]
  node [
    id 1802
    label "charakterystycznie"
  ]
  node [
    id 1803
    label "szczeg&#243;lny"
  ]
  node [
    id 1804
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1805
    label "hard"
  ]
  node [
    id 1806
    label "&#378;le"
  ]
  node [
    id 1807
    label "heavily"
  ]
  node [
    id 1808
    label "gruby"
  ]
  node [
    id 1809
    label "niegrzecznie"
  ]
  node [
    id 1810
    label "niema&#322;o"
  ]
  node [
    id 1811
    label "dono&#347;nie"
  ]
  node [
    id 1812
    label "grubia&#324;ski"
  ]
  node [
    id 1813
    label "fajnie"
  ]
  node [
    id 1814
    label "prostacko"
  ]
  node [
    id 1815
    label "ciep&#322;o"
  ]
  node [
    id 1816
    label "m&#261;cenie"
  ]
  node [
    id 1817
    label "ciecz"
  ]
  node [
    id 1818
    label "niejawny"
  ]
  node [
    id 1819
    label "zanieczyszczanie"
  ]
  node [
    id 1820
    label "ciemny"
  ]
  node [
    id 1821
    label "nieklarowny"
  ]
  node [
    id 1822
    label "niezrozumia&#322;y"
  ]
  node [
    id 1823
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 1824
    label "zanieczyszczenie"
  ]
  node [
    id 1825
    label "niepewny"
  ]
  node [
    id 1826
    label "samodzielny"
  ]
  node [
    id 1827
    label "ambitnie"
  ]
  node [
    id 1828
    label "zdeterminowany"
  ]
  node [
    id 1829
    label "wysokich_lot&#243;w"
  ]
  node [
    id 1830
    label "rozb&#243;j"
  ]
  node [
    id 1831
    label "exploitation"
  ]
  node [
    id 1832
    label "krzywda"
  ]
  node [
    id 1833
    label "strata"
  ]
  node [
    id 1834
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 1835
    label "obelga"
  ]
  node [
    id 1836
    label "bias"
  ]
  node [
    id 1837
    label "atak"
  ]
  node [
    id 1838
    label "Europa"
  ]
  node [
    id 1839
    label "wschodni"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 12
    target 1077
  ]
  edge [
    source 12
    target 1078
  ]
  edge [
    source 12
    target 1079
  ]
  edge [
    source 12
    target 1080
  ]
  edge [
    source 12
    target 1081
  ]
  edge [
    source 12
    target 1082
  ]
  edge [
    source 12
    target 1083
  ]
  edge [
    source 12
    target 1084
  ]
  edge [
    source 12
    target 1085
  ]
  edge [
    source 12
    target 1086
  ]
  edge [
    source 12
    target 1087
  ]
  edge [
    source 12
    target 1088
  ]
  edge [
    source 12
    target 1089
  ]
  edge [
    source 12
    target 1090
  ]
  edge [
    source 12
    target 1091
  ]
  edge [
    source 12
    target 1092
  ]
  edge [
    source 12
    target 1093
  ]
  edge [
    source 12
    target 1094
  ]
  edge [
    source 12
    target 1095
  ]
  edge [
    source 12
    target 1096
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 1098
  ]
  edge [
    source 12
    target 1099
  ]
  edge [
    source 12
    target 1100
  ]
  edge [
    source 12
    target 1101
  ]
  edge [
    source 12
    target 1102
  ]
  edge [
    source 12
    target 1103
  ]
  edge [
    source 12
    target 1104
  ]
  edge [
    source 12
    target 1105
  ]
  edge [
    source 12
    target 1106
  ]
  edge [
    source 12
    target 1107
  ]
  edge [
    source 12
    target 1108
  ]
  edge [
    source 12
    target 1109
  ]
  edge [
    source 12
    target 1110
  ]
  edge [
    source 12
    target 1111
  ]
  edge [
    source 12
    target 1112
  ]
  edge [
    source 12
    target 1113
  ]
  edge [
    source 12
    target 1114
  ]
  edge [
    source 12
    target 1115
  ]
  edge [
    source 12
    target 1116
  ]
  edge [
    source 12
    target 1117
  ]
  edge [
    source 12
    target 1118
  ]
  edge [
    source 12
    target 1119
  ]
  edge [
    source 12
    target 1120
  ]
  edge [
    source 12
    target 1121
  ]
  edge [
    source 12
    target 1122
  ]
  edge [
    source 12
    target 1123
  ]
  edge [
    source 12
    target 1124
  ]
  edge [
    source 12
    target 1125
  ]
  edge [
    source 12
    target 1126
  ]
  edge [
    source 12
    target 1127
  ]
  edge [
    source 12
    target 1128
  ]
  edge [
    source 12
    target 1129
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1131
  ]
  edge [
    source 12
    target 1132
  ]
  edge [
    source 12
    target 1133
  ]
  edge [
    source 12
    target 1134
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1153
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1155
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1165
  ]
  edge [
    source 12
    target 1166
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 1229
  ]
  edge [
    source 12
    target 1230
  ]
  edge [
    source 12
    target 1231
  ]
  edge [
    source 12
    target 1232
  ]
  edge [
    source 12
    target 1233
  ]
  edge [
    source 12
    target 1234
  ]
  edge [
    source 12
    target 1235
  ]
  edge [
    source 12
    target 1236
  ]
  edge [
    source 12
    target 1237
  ]
  edge [
    source 12
    target 1238
  ]
  edge [
    source 12
    target 1239
  ]
  edge [
    source 12
    target 1240
  ]
  edge [
    source 12
    target 1241
  ]
  edge [
    source 12
    target 1242
  ]
  edge [
    source 12
    target 1243
  ]
  edge [
    source 12
    target 1244
  ]
  edge [
    source 12
    target 1245
  ]
  edge [
    source 12
    target 1246
  ]
  edge [
    source 12
    target 1247
  ]
  edge [
    source 12
    target 1248
  ]
  edge [
    source 12
    target 1249
  ]
  edge [
    source 12
    target 1250
  ]
  edge [
    source 12
    target 1251
  ]
  edge [
    source 12
    target 1252
  ]
  edge [
    source 12
    target 1253
  ]
  edge [
    source 12
    target 1254
  ]
  edge [
    source 12
    target 1255
  ]
  edge [
    source 12
    target 1256
  ]
  edge [
    source 12
    target 1257
  ]
  edge [
    source 12
    target 1258
  ]
  edge [
    source 12
    target 1259
  ]
  edge [
    source 12
    target 1260
  ]
  edge [
    source 12
    target 1261
  ]
  edge [
    source 12
    target 1262
  ]
  edge [
    source 12
    target 1263
  ]
  edge [
    source 12
    target 1264
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 12
    target 1309
  ]
  edge [
    source 12
    target 1310
  ]
  edge [
    source 12
    target 1311
  ]
  edge [
    source 12
    target 1312
  ]
  edge [
    source 12
    target 1313
  ]
  edge [
    source 12
    target 1314
  ]
  edge [
    source 12
    target 1315
  ]
  edge [
    source 12
    target 1316
  ]
  edge [
    source 12
    target 1317
  ]
  edge [
    source 12
    target 1318
  ]
  edge [
    source 12
    target 1319
  ]
  edge [
    source 12
    target 1320
  ]
  edge [
    source 12
    target 1321
  ]
  edge [
    source 12
    target 1322
  ]
  edge [
    source 12
    target 1323
  ]
  edge [
    source 12
    target 1324
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 1325
  ]
  edge [
    source 12
    target 1326
  ]
  edge [
    source 12
    target 1327
  ]
  edge [
    source 12
    target 1328
  ]
  edge [
    source 12
    target 1329
  ]
  edge [
    source 12
    target 1330
  ]
  edge [
    source 12
    target 1331
  ]
  edge [
    source 12
    target 1332
  ]
  edge [
    source 12
    target 1333
  ]
  edge [
    source 12
    target 1334
  ]
  edge [
    source 12
    target 1335
  ]
  edge [
    source 12
    target 1336
  ]
  edge [
    source 12
    target 1337
  ]
  edge [
    source 12
    target 1338
  ]
  edge [
    source 12
    target 1339
  ]
  edge [
    source 12
    target 1340
  ]
  edge [
    source 12
    target 1341
  ]
  edge [
    source 12
    target 1342
  ]
  edge [
    source 12
    target 1343
  ]
  edge [
    source 12
    target 1344
  ]
  edge [
    source 12
    target 1345
  ]
  edge [
    source 12
    target 1346
  ]
  edge [
    source 12
    target 1347
  ]
  edge [
    source 12
    target 1348
  ]
  edge [
    source 12
    target 1349
  ]
  edge [
    source 12
    target 1350
  ]
  edge [
    source 12
    target 1351
  ]
  edge [
    source 12
    target 1352
  ]
  edge [
    source 12
    target 1353
  ]
  edge [
    source 12
    target 1354
  ]
  edge [
    source 12
    target 1355
  ]
  edge [
    source 12
    target 1356
  ]
  edge [
    source 12
    target 1357
  ]
  edge [
    source 12
    target 1358
  ]
  edge [
    source 12
    target 1359
  ]
  edge [
    source 12
    target 1360
  ]
  edge [
    source 12
    target 1361
  ]
  edge [
    source 12
    target 1362
  ]
  edge [
    source 12
    target 1363
  ]
  edge [
    source 12
    target 1364
  ]
  edge [
    source 12
    target 1365
  ]
  edge [
    source 12
    target 1366
  ]
  edge [
    source 12
    target 1367
  ]
  edge [
    source 12
    target 1368
  ]
  edge [
    source 12
    target 1369
  ]
  edge [
    source 12
    target 1370
  ]
  edge [
    source 12
    target 1371
  ]
  edge [
    source 12
    target 1372
  ]
  edge [
    source 12
    target 1373
  ]
  edge [
    source 12
    target 1374
  ]
  edge [
    source 12
    target 1375
  ]
  edge [
    source 12
    target 1376
  ]
  edge [
    source 12
    target 1377
  ]
  edge [
    source 12
    target 1378
  ]
  edge [
    source 12
    target 1379
  ]
  edge [
    source 12
    target 1380
  ]
  edge [
    source 12
    target 1381
  ]
  edge [
    source 12
    target 1382
  ]
  edge [
    source 12
    target 1383
  ]
  edge [
    source 12
    target 1384
  ]
  edge [
    source 12
    target 1385
  ]
  edge [
    source 12
    target 1386
  ]
  edge [
    source 12
    target 1387
  ]
  edge [
    source 12
    target 1388
  ]
  edge [
    source 12
    target 1389
  ]
  edge [
    source 12
    target 1390
  ]
  edge [
    source 12
    target 1391
  ]
  edge [
    source 12
    target 1392
  ]
  edge [
    source 12
    target 1393
  ]
  edge [
    source 12
    target 1394
  ]
  edge [
    source 12
    target 1395
  ]
  edge [
    source 12
    target 1396
  ]
  edge [
    source 12
    target 1397
  ]
  edge [
    source 12
    target 1398
  ]
  edge [
    source 12
    target 1399
  ]
  edge [
    source 12
    target 1400
  ]
  edge [
    source 12
    target 1401
  ]
  edge [
    source 12
    target 1402
  ]
  edge [
    source 12
    target 1403
  ]
  edge [
    source 12
    target 1404
  ]
  edge [
    source 12
    target 1405
  ]
  edge [
    source 12
    target 1406
  ]
  edge [
    source 12
    target 1407
  ]
  edge [
    source 12
    target 1408
  ]
  edge [
    source 12
    target 1409
  ]
  edge [
    source 12
    target 1410
  ]
  edge [
    source 12
    target 1411
  ]
  edge [
    source 12
    target 1412
  ]
  edge [
    source 12
    target 1413
  ]
  edge [
    source 12
    target 1414
  ]
  edge [
    source 12
    target 1415
  ]
  edge [
    source 12
    target 1416
  ]
  edge [
    source 12
    target 1417
  ]
  edge [
    source 12
    target 1418
  ]
  edge [
    source 12
    target 1419
  ]
  edge [
    source 12
    target 1420
  ]
  edge [
    source 12
    target 1421
  ]
  edge [
    source 12
    target 1422
  ]
  edge [
    source 12
    target 1423
  ]
  edge [
    source 12
    target 1424
  ]
  edge [
    source 12
    target 1425
  ]
  edge [
    source 12
    target 1426
  ]
  edge [
    source 12
    target 1427
  ]
  edge [
    source 12
    target 1428
  ]
  edge [
    source 12
    target 1429
  ]
  edge [
    source 12
    target 1430
  ]
  edge [
    source 12
    target 1431
  ]
  edge [
    source 12
    target 1432
  ]
  edge [
    source 12
    target 1433
  ]
  edge [
    source 12
    target 1434
  ]
  edge [
    source 12
    target 1435
  ]
  edge [
    source 12
    target 1436
  ]
  edge [
    source 12
    target 1437
  ]
  edge [
    source 12
    target 1438
  ]
  edge [
    source 12
    target 1439
  ]
  edge [
    source 12
    target 1440
  ]
  edge [
    source 12
    target 1441
  ]
  edge [
    source 12
    target 1442
  ]
  edge [
    source 12
    target 1443
  ]
  edge [
    source 12
    target 1444
  ]
  edge [
    source 12
    target 1445
  ]
  edge [
    source 12
    target 1446
  ]
  edge [
    source 12
    target 1447
  ]
  edge [
    source 12
    target 1448
  ]
  edge [
    source 12
    target 1449
  ]
  edge [
    source 12
    target 1450
  ]
  edge [
    source 12
    target 1451
  ]
  edge [
    source 12
    target 1452
  ]
  edge [
    source 12
    target 1453
  ]
  edge [
    source 12
    target 1454
  ]
  edge [
    source 12
    target 1455
  ]
  edge [
    source 12
    target 1456
  ]
  edge [
    source 12
    target 1457
  ]
  edge [
    source 12
    target 1458
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1459
  ]
  edge [
    source 13
    target 1460
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 1461
  ]
  edge [
    source 13
    target 1462
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 1463
  ]
  edge [
    source 13
    target 1464
  ]
  edge [
    source 13
    target 1465
  ]
  edge [
    source 13
    target 1466
  ]
  edge [
    source 13
    target 1467
  ]
  edge [
    source 13
    target 1468
  ]
  edge [
    source 13
    target 1469
  ]
  edge [
    source 13
    target 1470
  ]
  edge [
    source 13
    target 1471
  ]
  edge [
    source 13
    target 1472
  ]
  edge [
    source 13
    target 1473
  ]
  edge [
    source 13
    target 1474
  ]
  edge [
    source 13
    target 1475
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 1476
  ]
  edge [
    source 13
    target 1477
  ]
  edge [
    source 13
    target 1478
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 1479
  ]
  edge [
    source 13
    target 1480
  ]
  edge [
    source 13
    target 1481
  ]
  edge [
    source 13
    target 1482
  ]
  edge [
    source 13
    target 1483
  ]
  edge [
    source 13
    target 1484
  ]
  edge [
    source 13
    target 1485
  ]
  edge [
    source 13
    target 1486
  ]
  edge [
    source 13
    target 1487
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 1488
  ]
  edge [
    source 13
    target 1489
  ]
  edge [
    source 13
    target 1490
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 1491
  ]
  edge [
    source 13
    target 1492
  ]
  edge [
    source 13
    target 1493
  ]
  edge [
    source 13
    target 1494
  ]
  edge [
    source 13
    target 1495
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 1496
  ]
  edge [
    source 13
    target 1497
  ]
  edge [
    source 13
    target 1498
  ]
  edge [
    source 13
    target 1499
  ]
  edge [
    source 13
    target 1500
  ]
  edge [
    source 13
    target 1501
  ]
  edge [
    source 13
    target 1502
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 1503
  ]
  edge [
    source 13
    target 1504
  ]
  edge [
    source 13
    target 1505
  ]
  edge [
    source 13
    target 1506
  ]
  edge [
    source 13
    target 1507
  ]
  edge [
    source 13
    target 1508
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 1509
  ]
  edge [
    source 13
    target 1510
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 1511
  ]
  edge [
    source 13
    target 1512
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1513
  ]
  edge [
    source 14
    target 1514
  ]
  edge [
    source 14
    target 1515
  ]
  edge [
    source 14
    target 1516
  ]
  edge [
    source 14
    target 1517
  ]
  edge [
    source 14
    target 1518
  ]
  edge [
    source 14
    target 1519
  ]
  edge [
    source 14
    target 1520
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 1521
  ]
  edge [
    source 14
    target 1522
  ]
  edge [
    source 14
    target 1523
  ]
  edge [
    source 14
    target 1524
  ]
  edge [
    source 14
    target 1525
  ]
  edge [
    source 14
    target 1526
  ]
  edge [
    source 14
    target 1527
  ]
  edge [
    source 14
    target 1528
  ]
  edge [
    source 14
    target 1529
  ]
  edge [
    source 14
    target 1530
  ]
  edge [
    source 14
    target 1531
  ]
  edge [
    source 14
    target 1532
  ]
  edge [
    source 14
    target 1533
  ]
  edge [
    source 14
    target 1534
  ]
  edge [
    source 14
    target 1535
  ]
  edge [
    source 14
    target 1536
  ]
  edge [
    source 14
    target 1537
  ]
  edge [
    source 14
    target 1538
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1539
  ]
  edge [
    source 15
    target 1540
  ]
  edge [
    source 15
    target 1541
  ]
  edge [
    source 15
    target 1542
  ]
  edge [
    source 15
    target 1543
  ]
  edge [
    source 15
    target 1544
  ]
  edge [
    source 15
    target 1545
  ]
  edge [
    source 15
    target 1546
  ]
  edge [
    source 15
    target 1547
  ]
  edge [
    source 15
    target 1548
  ]
  edge [
    source 15
    target 1549
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 441
  ]
  edge [
    source 15
    target 1550
  ]
  edge [
    source 15
    target 1551
  ]
  edge [
    source 15
    target 1552
  ]
  edge [
    source 15
    target 1467
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 1553
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 1554
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 1555
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 1556
  ]
  edge [
    source 15
    target 1557
  ]
  edge [
    source 15
    target 424
  ]
  edge [
    source 15
    target 1525
  ]
  edge [
    source 15
    target 1526
  ]
  edge [
    source 15
    target 1527
  ]
  edge [
    source 15
    target 1484
  ]
  edge [
    source 15
    target 1485
  ]
  edge [
    source 15
    target 1486
  ]
  edge [
    source 15
    target 1487
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 1488
  ]
  edge [
    source 15
    target 1489
  ]
  edge [
    source 15
    target 1490
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 349
  ]
  edge [
    source 15
    target 1491
  ]
  edge [
    source 15
    target 1558
  ]
  edge [
    source 15
    target 1559
  ]
  edge [
    source 15
    target 1560
  ]
  edge [
    source 15
    target 1561
  ]
  edge [
    source 15
    target 1562
  ]
  edge [
    source 15
    target 1563
  ]
  edge [
    source 15
    target 1564
  ]
  edge [
    source 15
    target 1565
  ]
  edge [
    source 15
    target 1566
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 1567
  ]
  edge [
    source 15
    target 1568
  ]
  edge [
    source 15
    target 1569
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 1570
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 1571
  ]
  edge [
    source 15
    target 1572
  ]
  edge [
    source 15
    target 1573
  ]
  edge [
    source 15
    target 1574
  ]
  edge [
    source 15
    target 1575
  ]
  edge [
    source 15
    target 1576
  ]
  edge [
    source 15
    target 1577
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 1578
  ]
  edge [
    source 15
    target 1579
  ]
  edge [
    source 15
    target 1580
  ]
  edge [
    source 15
    target 1581
  ]
  edge [
    source 15
    target 1582
  ]
  edge [
    source 15
    target 1583
  ]
  edge [
    source 15
    target 1584
  ]
  edge [
    source 15
    target 1585
  ]
  edge [
    source 15
    target 1586
  ]
  edge [
    source 15
    target 1587
  ]
  edge [
    source 15
    target 1483
  ]
  edge [
    source 15
    target 422
  ]
  edge [
    source 15
    target 1588
  ]
  edge [
    source 15
    target 1589
  ]
  edge [
    source 15
    target 1590
  ]
  edge [
    source 15
    target 1591
  ]
  edge [
    source 15
    target 1592
  ]
  edge [
    source 15
    target 1593
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 1594
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 1595
  ]
  edge [
    source 15
    target 1596
  ]
  edge [
    source 15
    target 1597
  ]
  edge [
    source 15
    target 1598
  ]
  edge [
    source 15
    target 1599
  ]
  edge [
    source 15
    target 1600
  ]
  edge [
    source 15
    target 1601
  ]
  edge [
    source 15
    target 1602
  ]
  edge [
    source 15
    target 1603
  ]
  edge [
    source 15
    target 1604
  ]
  edge [
    source 15
    target 1605
  ]
  edge [
    source 15
    target 1606
  ]
  edge [
    source 15
    target 1607
  ]
  edge [
    source 15
    target 1608
  ]
  edge [
    source 15
    target 1609
  ]
  edge [
    source 15
    target 1610
  ]
  edge [
    source 15
    target 1611
  ]
  edge [
    source 15
    target 1612
  ]
  edge [
    source 15
    target 1613
  ]
  edge [
    source 15
    target 1614
  ]
  edge [
    source 15
    target 1615
  ]
  edge [
    source 15
    target 1616
  ]
  edge [
    source 15
    target 1617
  ]
  edge [
    source 15
    target 1618
  ]
  edge [
    source 15
    target 1619
  ]
  edge [
    source 15
    target 1620
  ]
  edge [
    source 15
    target 1621
  ]
  edge [
    source 15
    target 1510
  ]
  edge [
    source 15
    target 1622
  ]
  edge [
    source 15
    target 1623
  ]
  edge [
    source 15
    target 1624
  ]
  edge [
    source 15
    target 1625
  ]
  edge [
    source 15
    target 1626
  ]
  edge [
    source 15
    target 1627
  ]
  edge [
    source 15
    target 1628
  ]
  edge [
    source 15
    target 1629
  ]
  edge [
    source 15
    target 1630
  ]
  edge [
    source 15
    target 1631
  ]
  edge [
    source 15
    target 1632
  ]
  edge [
    source 15
    target 1633
  ]
  edge [
    source 15
    target 1634
  ]
  edge [
    source 15
    target 1635
  ]
  edge [
    source 15
    target 1636
  ]
  edge [
    source 15
    target 1637
  ]
  edge [
    source 15
    target 1638
  ]
  edge [
    source 15
    target 1639
  ]
  edge [
    source 15
    target 1640
  ]
  edge [
    source 15
    target 1465
  ]
  edge [
    source 15
    target 1641
  ]
  edge [
    source 15
    target 1642
  ]
  edge [
    source 15
    target 1643
  ]
  edge [
    source 15
    target 1644
  ]
  edge [
    source 15
    target 1645
  ]
  edge [
    source 15
    target 1646
  ]
  edge [
    source 15
    target 1647
  ]
  edge [
    source 15
    target 1648
  ]
  edge [
    source 15
    target 1649
  ]
  edge [
    source 15
    target 1650
  ]
  edge [
    source 15
    target 1651
  ]
  edge [
    source 15
    target 1652
  ]
  edge [
    source 15
    target 178
  ]
  edge [
    source 15
    target 1653
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 1654
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1655
  ]
  edge [
    source 16
    target 52
  ]
  edge [
    source 16
    target 1656
  ]
  edge [
    source 16
    target 1657
  ]
  edge [
    source 16
    target 1658
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 1659
  ]
  edge [
    source 16
    target 1660
  ]
  edge [
    source 16
    target 1661
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 1662
  ]
  edge [
    source 16
    target 1663
  ]
  edge [
    source 16
    target 1664
  ]
  edge [
    source 16
    target 1665
  ]
  edge [
    source 16
    target 1666
  ]
  edge [
    source 16
    target 1667
  ]
  edge [
    source 16
    target 1668
  ]
  edge [
    source 16
    target 1669
  ]
  edge [
    source 16
    target 1670
  ]
  edge [
    source 16
    target 1671
  ]
  edge [
    source 16
    target 1672
  ]
  edge [
    source 16
    target 1673
  ]
  edge [
    source 16
    target 1674
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 1675
  ]
  edge [
    source 16
    target 1676
  ]
  edge [
    source 16
    target 1677
  ]
  edge [
    source 16
    target 1678
  ]
  edge [
    source 16
    target 1679
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1680
  ]
  edge [
    source 17
    target 1681
  ]
  edge [
    source 17
    target 1682
  ]
  edge [
    source 17
    target 1683
  ]
  edge [
    source 17
    target 1684
  ]
  edge [
    source 17
    target 1685
  ]
  edge [
    source 17
    target 1686
  ]
  edge [
    source 17
    target 1687
  ]
  edge [
    source 17
    target 1688
  ]
  edge [
    source 17
    target 1689
  ]
  edge [
    source 17
    target 1690
  ]
  edge [
    source 17
    target 1691
  ]
  edge [
    source 17
    target 1692
  ]
  edge [
    source 17
    target 1693
  ]
  edge [
    source 17
    target 1694
  ]
  edge [
    source 17
    target 1695
  ]
  edge [
    source 17
    target 1696
  ]
  edge [
    source 17
    target 1697
  ]
  edge [
    source 17
    target 1698
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 1699
  ]
  edge [
    source 17
    target 1700
  ]
  edge [
    source 17
    target 1701
  ]
  edge [
    source 17
    target 1702
  ]
  edge [
    source 17
    target 1703
  ]
  edge [
    source 17
    target 1704
  ]
  edge [
    source 17
    target 1705
  ]
  edge [
    source 17
    target 1706
  ]
  edge [
    source 17
    target 1707
  ]
  edge [
    source 17
    target 1708
  ]
  edge [
    source 17
    target 1709
  ]
  edge [
    source 17
    target 1710
  ]
  edge [
    source 17
    target 1711
  ]
  edge [
    source 17
    target 1712
  ]
  edge [
    source 17
    target 1713
  ]
  edge [
    source 17
    target 1714
  ]
  edge [
    source 17
    target 1715
  ]
  edge [
    source 17
    target 1716
  ]
  edge [
    source 17
    target 1717
  ]
  edge [
    source 17
    target 1718
  ]
  edge [
    source 17
    target 1719
  ]
  edge [
    source 17
    target 1720
  ]
  edge [
    source 17
    target 1721
  ]
  edge [
    source 17
    target 1722
  ]
  edge [
    source 17
    target 1723
  ]
  edge [
    source 17
    target 1724
  ]
  edge [
    source 17
    target 1725
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 1726
  ]
  edge [
    source 17
    target 1727
  ]
  edge [
    source 17
    target 1728
  ]
  edge [
    source 17
    target 1729
  ]
  edge [
    source 17
    target 1730
  ]
  edge [
    source 17
    target 1731
  ]
  edge [
    source 17
    target 1732
  ]
  edge [
    source 17
    target 1733
  ]
  edge [
    source 17
    target 1734
  ]
  edge [
    source 17
    target 1735
  ]
  edge [
    source 17
    target 1736
  ]
  edge [
    source 17
    target 1737
  ]
  edge [
    source 17
    target 1738
  ]
  edge [
    source 17
    target 1739
  ]
  edge [
    source 17
    target 1740
  ]
  edge [
    source 17
    target 1741
  ]
  edge [
    source 17
    target 1742
  ]
  edge [
    source 17
    target 1743
  ]
  edge [
    source 17
    target 1744
  ]
  edge [
    source 17
    target 1745
  ]
  edge [
    source 17
    target 1746
  ]
  edge [
    source 17
    target 1747
  ]
  edge [
    source 17
    target 1748
  ]
  edge [
    source 17
    target 1749
  ]
  edge [
    source 17
    target 1750
  ]
  edge [
    source 17
    target 1751
  ]
  edge [
    source 17
    target 1752
  ]
  edge [
    source 17
    target 1753
  ]
  edge [
    source 17
    target 1754
  ]
  edge [
    source 17
    target 1755
  ]
  edge [
    source 17
    target 1756
  ]
  edge [
    source 17
    target 1757
  ]
  edge [
    source 17
    target 1758
  ]
  edge [
    source 17
    target 1759
  ]
  edge [
    source 17
    target 1760
  ]
  edge [
    source 17
    target 1761
  ]
  edge [
    source 17
    target 1762
  ]
  edge [
    source 17
    target 1763
  ]
  edge [
    source 17
    target 1764
  ]
  edge [
    source 17
    target 1765
  ]
  edge [
    source 17
    target 1766
  ]
  edge [
    source 17
    target 1767
  ]
  edge [
    source 17
    target 1768
  ]
  edge [
    source 17
    target 1769
  ]
  edge [
    source 17
    target 1770
  ]
  edge [
    source 17
    target 1771
  ]
  edge [
    source 17
    target 1772
  ]
  edge [
    source 17
    target 1773
  ]
  edge [
    source 17
    target 1774
  ]
  edge [
    source 17
    target 1775
  ]
  edge [
    source 17
    target 1776
  ]
  edge [
    source 17
    target 1777
  ]
  edge [
    source 17
    target 1778
  ]
  edge [
    source 17
    target 1779
  ]
  edge [
    source 17
    target 1780
  ]
  edge [
    source 17
    target 1781
  ]
  edge [
    source 17
    target 1782
  ]
  edge [
    source 17
    target 1783
  ]
  edge [
    source 17
    target 1784
  ]
  edge [
    source 17
    target 1785
  ]
  edge [
    source 17
    target 1786
  ]
  edge [
    source 17
    target 1787
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 1788
  ]
  edge [
    source 17
    target 1789
  ]
  edge [
    source 17
    target 1790
  ]
  edge [
    source 17
    target 1791
  ]
  edge [
    source 17
    target 1792
  ]
  edge [
    source 17
    target 1793
  ]
  edge [
    source 17
    target 1794
  ]
  edge [
    source 17
    target 1795
  ]
  edge [
    source 17
    target 1796
  ]
  edge [
    source 17
    target 1797
  ]
  edge [
    source 17
    target 1798
  ]
  edge [
    source 17
    target 1799
  ]
  edge [
    source 17
    target 1800
  ]
  edge [
    source 17
    target 1801
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 1802
  ]
  edge [
    source 17
    target 1803
  ]
  edge [
    source 17
    target 116
  ]
  edge [
    source 17
    target 1804
  ]
  edge [
    source 17
    target 336
  ]
  edge [
    source 17
    target 1805
  ]
  edge [
    source 17
    target 1806
  ]
  edge [
    source 17
    target 1807
  ]
  edge [
    source 17
    target 1808
  ]
  edge [
    source 17
    target 1809
  ]
  edge [
    source 17
    target 1810
  ]
  edge [
    source 17
    target 1811
  ]
  edge [
    source 17
    target 1812
  ]
  edge [
    source 17
    target 1813
  ]
  edge [
    source 17
    target 1814
  ]
  edge [
    source 17
    target 1815
  ]
  edge [
    source 17
    target 1816
  ]
  edge [
    source 17
    target 1817
  ]
  edge [
    source 17
    target 1818
  ]
  edge [
    source 17
    target 1819
  ]
  edge [
    source 17
    target 1820
  ]
  edge [
    source 17
    target 1821
  ]
  edge [
    source 17
    target 1822
  ]
  edge [
    source 17
    target 1823
  ]
  edge [
    source 17
    target 1824
  ]
  edge [
    source 17
    target 1825
  ]
  edge [
    source 17
    target 1826
  ]
  edge [
    source 17
    target 1827
  ]
  edge [
    source 17
    target 1828
  ]
  edge [
    source 17
    target 1829
  ]
  edge [
    source 18
    target 1830
  ]
  edge [
    source 18
    target 1831
  ]
  edge [
    source 18
    target 1832
  ]
  edge [
    source 18
    target 1833
  ]
  edge [
    source 18
    target 1834
  ]
  edge [
    source 18
    target 1835
  ]
  edge [
    source 18
    target 1836
  ]
  edge [
    source 18
    target 1837
  ]
  edge [
    source 1838
    target 1839
  ]
]
