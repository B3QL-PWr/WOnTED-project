graph [
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 2
    label "protestancki"
    origin "text"
  ]
  node [
    id 3
    label "haga"
    origin "text"
  ]
  node [
    id 4
    label "pa&#378;dziernik"
    origin "text"
  ]
  node [
    id 5
    label "duchowny"
    origin "text"
  ]
  node [
    id 6
    label "odprawia&#263;"
    origin "text"
  ]
  node [
    id 7
    label "nieprzerwany"
    origin "text"
  ]
  node [
    id 8
    label "nabo&#380;e&#324;stwo"
    origin "text"
  ]
  node [
    id 9
    label "shot"
  ]
  node [
    id 10
    label "jednakowy"
  ]
  node [
    id 11
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 12
    label "ujednolicenie"
  ]
  node [
    id 13
    label "jaki&#347;"
  ]
  node [
    id 14
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 15
    label "jednolicie"
  ]
  node [
    id 16
    label "kieliszek"
  ]
  node [
    id 17
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 18
    label "w&#243;dka"
  ]
  node [
    id 19
    label "ten"
  ]
  node [
    id 20
    label "szk&#322;o"
  ]
  node [
    id 21
    label "zawarto&#347;&#263;"
  ]
  node [
    id 22
    label "naczynie"
  ]
  node [
    id 23
    label "alkohol"
  ]
  node [
    id 24
    label "sznaps"
  ]
  node [
    id 25
    label "nap&#243;j"
  ]
  node [
    id 26
    label "gorza&#322;ka"
  ]
  node [
    id 27
    label "mohorycz"
  ]
  node [
    id 28
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 29
    label "zr&#243;wnanie"
  ]
  node [
    id 30
    label "mundurowanie"
  ]
  node [
    id 31
    label "taki&#380;"
  ]
  node [
    id 32
    label "jednakowo"
  ]
  node [
    id 33
    label "mundurowa&#263;"
  ]
  node [
    id 34
    label "zr&#243;wnywanie"
  ]
  node [
    id 35
    label "identyczny"
  ]
  node [
    id 36
    label "okre&#347;lony"
  ]
  node [
    id 37
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 38
    label "z&#322;o&#380;ony"
  ]
  node [
    id 39
    label "przyzwoity"
  ]
  node [
    id 40
    label "ciekawy"
  ]
  node [
    id 41
    label "jako&#347;"
  ]
  node [
    id 42
    label "jako_tako"
  ]
  node [
    id 43
    label "niez&#322;y"
  ]
  node [
    id 44
    label "dziwny"
  ]
  node [
    id 45
    label "charakterystyczny"
  ]
  node [
    id 46
    label "g&#322;&#281;bszy"
  ]
  node [
    id 47
    label "drink"
  ]
  node [
    id 48
    label "jednolity"
  ]
  node [
    id 49
    label "upodobnienie"
  ]
  node [
    id 50
    label "calibration"
  ]
  node [
    id 51
    label "kult"
  ]
  node [
    id 52
    label "ub&#322;agalnia"
  ]
  node [
    id 53
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 54
    label "nawa"
  ]
  node [
    id 55
    label "wsp&#243;lnota"
  ]
  node [
    id 56
    label "Ska&#322;ka"
  ]
  node [
    id 57
    label "zakrystia"
  ]
  node [
    id 58
    label "prezbiterium"
  ]
  node [
    id 59
    label "kropielnica"
  ]
  node [
    id 60
    label "organizacja_religijna"
  ]
  node [
    id 61
    label "nerwica_eklezjogenna"
  ]
  node [
    id 62
    label "church"
  ]
  node [
    id 63
    label "kruchta"
  ]
  node [
    id 64
    label "dom"
  ]
  node [
    id 65
    label "zwi&#261;zanie"
  ]
  node [
    id 66
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 67
    label "podobie&#324;stwo"
  ]
  node [
    id 68
    label "Skandynawia"
  ]
  node [
    id 69
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 70
    label "partnership"
  ]
  node [
    id 71
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 72
    label "wi&#261;zanie"
  ]
  node [
    id 73
    label "Ba&#322;kany"
  ]
  node [
    id 74
    label "society"
  ]
  node [
    id 75
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 76
    label "zwi&#261;za&#263;"
  ]
  node [
    id 77
    label "Walencja"
  ]
  node [
    id 78
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 79
    label "bratnia_dusza"
  ]
  node [
    id 80
    label "zwi&#261;zek"
  ]
  node [
    id 81
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 82
    label "marriage"
  ]
  node [
    id 83
    label "przybytek"
  ]
  node [
    id 84
    label "siedlisko"
  ]
  node [
    id 85
    label "budynek"
  ]
  node [
    id 86
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 87
    label "rodzina"
  ]
  node [
    id 88
    label "substancja_mieszkaniowa"
  ]
  node [
    id 89
    label "instytucja"
  ]
  node [
    id 90
    label "siedziba"
  ]
  node [
    id 91
    label "dom_rodzinny"
  ]
  node [
    id 92
    label "grupa"
  ]
  node [
    id 93
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 94
    label "poj&#281;cie"
  ]
  node [
    id 95
    label "stead"
  ]
  node [
    id 96
    label "garderoba"
  ]
  node [
    id 97
    label "wiecha"
  ]
  node [
    id 98
    label "fratria"
  ]
  node [
    id 99
    label "uwielbienie"
  ]
  node [
    id 100
    label "religia"
  ]
  node [
    id 101
    label "translacja"
  ]
  node [
    id 102
    label "postawa"
  ]
  node [
    id 103
    label "egzegeta"
  ]
  node [
    id 104
    label "worship"
  ]
  node [
    id 105
    label "obrz&#281;d"
  ]
  node [
    id 106
    label "babiniec"
  ]
  node [
    id 107
    label "przedsionek"
  ]
  node [
    id 108
    label "pomieszczenie"
  ]
  node [
    id 109
    label "zesp&#243;&#322;"
  ]
  node [
    id 110
    label "korpus"
  ]
  node [
    id 111
    label "&#347;rodowisko"
  ]
  node [
    id 112
    label "o&#322;tarz"
  ]
  node [
    id 113
    label "stalle"
  ]
  node [
    id 114
    label "lampka_wieczysta"
  ]
  node [
    id 115
    label "tabernakulum"
  ]
  node [
    id 116
    label "duchowie&#324;stwo"
  ]
  node [
    id 117
    label "naczynie_na_wod&#281;_&#347;wi&#281;con&#261;"
  ]
  node [
    id 118
    label "paramenty"
  ]
  node [
    id 119
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 120
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 121
    label "wyznaniowy"
  ]
  node [
    id 122
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 123
    label "nale&#380;ny"
  ]
  node [
    id 124
    label "nale&#380;yty"
  ]
  node [
    id 125
    label "typowy"
  ]
  node [
    id 126
    label "uprawniony"
  ]
  node [
    id 127
    label "zasadniczy"
  ]
  node [
    id 128
    label "stosownie"
  ]
  node [
    id 129
    label "taki"
  ]
  node [
    id 130
    label "prawdziwy"
  ]
  node [
    id 131
    label "dobry"
  ]
  node [
    id 132
    label "religijny"
  ]
  node [
    id 133
    label "chrze&#347;cija&#324;sko"
  ]
  node [
    id 134
    label "krze&#347;cija&#324;ski"
  ]
  node [
    id 135
    label "oktober"
  ]
  node [
    id 136
    label "miesi&#261;c"
  ]
  node [
    id 137
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 138
    label "tydzie&#324;"
  ]
  node [
    id 139
    label "miech"
  ]
  node [
    id 140
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 141
    label "czas"
  ]
  node [
    id 142
    label "rok"
  ]
  node [
    id 143
    label "kalendy"
  ]
  node [
    id 144
    label "Luter"
  ]
  node [
    id 145
    label "cz&#322;owiek"
  ]
  node [
    id 146
    label "eklezjasta"
  ]
  node [
    id 147
    label "Bayes"
  ]
  node [
    id 148
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 149
    label "sekularyzacja"
  ]
  node [
    id 150
    label "tonsura"
  ]
  node [
    id 151
    label "seminarzysta"
  ]
  node [
    id 152
    label "Hus"
  ]
  node [
    id 153
    label "wyznawca"
  ]
  node [
    id 154
    label "przedstawiciel"
  ]
  node [
    id 155
    label "&#347;w"
  ]
  node [
    id 156
    label "kongregacja"
  ]
  node [
    id 157
    label "nabo&#380;ny"
  ]
  node [
    id 158
    label "wierz&#261;cy"
  ]
  node [
    id 159
    label "religijnie"
  ]
  node [
    id 160
    label "czciciel"
  ]
  node [
    id 161
    label "zwolennik"
  ]
  node [
    id 162
    label "wierzenie"
  ]
  node [
    id 163
    label "ludzko&#347;&#263;"
  ]
  node [
    id 164
    label "asymilowanie"
  ]
  node [
    id 165
    label "wapniak"
  ]
  node [
    id 166
    label "asymilowa&#263;"
  ]
  node [
    id 167
    label "os&#322;abia&#263;"
  ]
  node [
    id 168
    label "posta&#263;"
  ]
  node [
    id 169
    label "hominid"
  ]
  node [
    id 170
    label "podw&#322;adny"
  ]
  node [
    id 171
    label "os&#322;abianie"
  ]
  node [
    id 172
    label "g&#322;owa"
  ]
  node [
    id 173
    label "figura"
  ]
  node [
    id 174
    label "portrecista"
  ]
  node [
    id 175
    label "dwun&#243;g"
  ]
  node [
    id 176
    label "profanum"
  ]
  node [
    id 177
    label "mikrokosmos"
  ]
  node [
    id 178
    label "nasada"
  ]
  node [
    id 179
    label "duch"
  ]
  node [
    id 180
    label "antropochoria"
  ]
  node [
    id 181
    label "osoba"
  ]
  node [
    id 182
    label "wz&#243;r"
  ]
  node [
    id 183
    label "senior"
  ]
  node [
    id 184
    label "oddzia&#322;ywanie"
  ]
  node [
    id 185
    label "Adam"
  ]
  node [
    id 186
    label "homo_sapiens"
  ]
  node [
    id 187
    label "polifag"
  ]
  node [
    id 188
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 189
    label "cz&#322;onek"
  ]
  node [
    id 190
    label "przyk&#322;ad"
  ]
  node [
    id 191
    label "substytuowa&#263;"
  ]
  node [
    id 192
    label "substytuowanie"
  ]
  node [
    id 193
    label "zast&#281;pca"
  ]
  node [
    id 194
    label "ksi&#261;dz"
  ]
  node [
    id 195
    label "Grek"
  ]
  node [
    id 196
    label "obywatel"
  ]
  node [
    id 197
    label "eklezja"
  ]
  node [
    id 198
    label "kaznodzieja"
  ]
  node [
    id 199
    label "reformacja"
  ]
  node [
    id 200
    label "stan"
  ]
  node [
    id 201
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 202
    label "mitologia"
  ]
  node [
    id 203
    label "wyznanie"
  ]
  node [
    id 204
    label "przedmiot"
  ]
  node [
    id 205
    label "ideologia"
  ]
  node [
    id 206
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 207
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 208
    label "nawracanie_si&#281;"
  ]
  node [
    id 209
    label "rela"
  ]
  node [
    id 210
    label "kultura_duchowa"
  ]
  node [
    id 211
    label "kosmologia"
  ]
  node [
    id 212
    label "kultura"
  ]
  node [
    id 213
    label "kosmogonia"
  ]
  node [
    id 214
    label "nawraca&#263;"
  ]
  node [
    id 215
    label "mistyka"
  ]
  node [
    id 216
    label "wyw&#322;aszczenie"
  ]
  node [
    id 217
    label "proces"
  ]
  node [
    id 218
    label "secularization"
  ]
  node [
    id 219
    label "zrezygnowanie"
  ]
  node [
    id 220
    label "zjawisko"
  ]
  node [
    id 221
    label "wp&#322;yw"
  ]
  node [
    id 222
    label "fryzura"
  ]
  node [
    id 223
    label "strzy&#380;enie"
  ]
  node [
    id 224
    label "tonsure"
  ]
  node [
    id 225
    label "praktyka"
  ]
  node [
    id 226
    label "&#322;ysina"
  ]
  node [
    id 227
    label "uczestnik"
  ]
  node [
    id 228
    label "ucze&#324;"
  ]
  node [
    id 229
    label "student"
  ]
  node [
    id 230
    label "ordynacja"
  ]
  node [
    id 231
    label "prezbiter"
  ]
  node [
    id 232
    label "prawo"
  ]
  node [
    id 233
    label "diakon"
  ]
  node [
    id 234
    label "zbi&#243;r"
  ]
  node [
    id 235
    label "komisja"
  ]
  node [
    id 236
    label "zarz&#261;d"
  ]
  node [
    id 237
    label "rada"
  ]
  node [
    id 238
    label "zakon"
  ]
  node [
    id 239
    label "klasztor"
  ]
  node [
    id 240
    label "jednostka_administracyjna"
  ]
  node [
    id 241
    label "instytut_&#347;wiecki"
  ]
  node [
    id 242
    label "zjazd"
  ]
  node [
    id 243
    label "wyprawia&#263;"
  ]
  node [
    id 244
    label "robi&#263;"
  ]
  node [
    id 245
    label "wylewa&#263;"
  ]
  node [
    id 246
    label "perform"
  ]
  node [
    id 247
    label "wymawia&#263;"
  ]
  node [
    id 248
    label "unbosom"
  ]
  node [
    id 249
    label "oddala&#263;"
  ]
  node [
    id 250
    label "pami&#281;ta&#263;"
  ]
  node [
    id 251
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 252
    label "express"
  ]
  node [
    id 253
    label "werbalizowa&#263;"
  ]
  node [
    id 254
    label "zastrzega&#263;"
  ]
  node [
    id 255
    label "oskar&#380;a&#263;"
  ]
  node [
    id 256
    label "denounce"
  ]
  node [
    id 257
    label "say"
  ]
  node [
    id 258
    label "typify"
  ]
  node [
    id 259
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 260
    label "wydobywa&#263;"
  ]
  node [
    id 261
    label "wysy&#322;a&#263;"
  ]
  node [
    id 262
    label "organize"
  ]
  node [
    id 263
    label "preparowa&#263;"
  ]
  node [
    id 264
    label "train"
  ]
  node [
    id 265
    label "forowa&#263;"
  ]
  node [
    id 266
    label "organizowa&#263;"
  ]
  node [
    id 267
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 268
    label "czyni&#263;"
  ]
  node [
    id 269
    label "give"
  ]
  node [
    id 270
    label "stylizowa&#263;"
  ]
  node [
    id 271
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 272
    label "falowa&#263;"
  ]
  node [
    id 273
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 274
    label "peddle"
  ]
  node [
    id 275
    label "praca"
  ]
  node [
    id 276
    label "wydala&#263;"
  ]
  node [
    id 277
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 278
    label "tentegowa&#263;"
  ]
  node [
    id 279
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 280
    label "urz&#261;dza&#263;"
  ]
  node [
    id 281
    label "oszukiwa&#263;"
  ]
  node [
    id 282
    label "work"
  ]
  node [
    id 283
    label "ukazywa&#263;"
  ]
  node [
    id 284
    label "przerabia&#263;"
  ]
  node [
    id 285
    label "act"
  ]
  node [
    id 286
    label "post&#281;powa&#263;"
  ]
  node [
    id 287
    label "oddalenie"
  ]
  node [
    id 288
    label "remove"
  ]
  node [
    id 289
    label "pokazywa&#263;"
  ]
  node [
    id 290
    label "nakazywa&#263;"
  ]
  node [
    id 291
    label "przemieszcza&#263;"
  ]
  node [
    id 292
    label "oddali&#263;"
  ]
  node [
    id 293
    label "dissolve"
  ]
  node [
    id 294
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 295
    label "oddalanie"
  ]
  node [
    id 296
    label "retard"
  ]
  node [
    id 297
    label "sprawia&#263;"
  ]
  node [
    id 298
    label "odrzuca&#263;"
  ]
  node [
    id 299
    label "zwalnia&#263;"
  ]
  node [
    id 300
    label "odk&#322;ada&#263;"
  ]
  node [
    id 301
    label "spill"
  ]
  node [
    id 302
    label "wyrzuca&#263;"
  ]
  node [
    id 303
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 304
    label "opr&#243;&#380;nia&#263;"
  ]
  node [
    id 305
    label "pokrywa&#263;"
  ]
  node [
    id 306
    label "wyra&#380;a&#263;"
  ]
  node [
    id 307
    label "la&#263;"
  ]
  node [
    id 308
    label "Tischner"
  ]
  node [
    id 309
    label "zwierzchnik"
  ]
  node [
    id 310
    label "msza"
  ]
  node [
    id 311
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 312
    label "Tadeusz_Rydzyk"
  ]
  node [
    id 313
    label "kap&#322;an"
  ]
  node [
    id 314
    label "niezmierzony"
  ]
  node [
    id 315
    label "ci&#261;g&#322;y"
  ]
  node [
    id 316
    label "nieprzerwanie"
  ]
  node [
    id 317
    label "nieustanny"
  ]
  node [
    id 318
    label "niesko&#324;czony"
  ]
  node [
    id 319
    label "bezprzestanny"
  ]
  node [
    id 320
    label "nieustannie"
  ]
  node [
    id 321
    label "sta&#322;y"
  ]
  node [
    id 322
    label "ci&#261;gle"
  ]
  node [
    id 323
    label "nieograniczenie"
  ]
  node [
    id 324
    label "przestrze&#324;"
  ]
  node [
    id 325
    label "rozleg&#322;y"
  ]
  node [
    id 326
    label "otwarty"
  ]
  node [
    id 327
    label "niesko&#324;czenie"
  ]
  node [
    id 328
    label "nadzwyczajny"
  ]
  node [
    id 329
    label "ogromny"
  ]
  node [
    id 330
    label "olbrzymi"
  ]
  node [
    id 331
    label "bezczasowy"
  ]
  node [
    id 332
    label "devotion"
  ]
  node [
    id 333
    label "powa&#380;anie"
  ]
  node [
    id 334
    label "dba&#322;o&#347;&#263;"
  ]
  node [
    id 335
    label "mod&#322;y"
  ]
  node [
    id 336
    label "czynno&#347;&#263;"
  ]
  node [
    id 337
    label "modlitwa"
  ]
  node [
    id 338
    label "ceremony"
  ]
  node [
    id 339
    label "honorowa&#263;"
  ]
  node [
    id 340
    label "uhonorowanie"
  ]
  node [
    id 341
    label "zaimponowanie"
  ]
  node [
    id 342
    label "honorowanie"
  ]
  node [
    id 343
    label "uszanowa&#263;"
  ]
  node [
    id 344
    label "chowanie"
  ]
  node [
    id 345
    label "respektowanie"
  ]
  node [
    id 346
    label "uszanowanie"
  ]
  node [
    id 347
    label "szacuneczek"
  ]
  node [
    id 348
    label "rewerencja"
  ]
  node [
    id 349
    label "uhonorowa&#263;"
  ]
  node [
    id 350
    label "czucie"
  ]
  node [
    id 351
    label "szanowa&#263;"
  ]
  node [
    id 352
    label "fame"
  ]
  node [
    id 353
    label "respect"
  ]
  node [
    id 354
    label "imponowanie"
  ]
  node [
    id 355
    label "accuracy"
  ]
  node [
    id 356
    label "cecha"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
]
