graph [
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 2
    label "geografia"
    origin "text"
  ]
  node [
    id 3
    label "wolontariusz"
    origin "text"
  ]
  node [
    id 4
    label "wszystek"
    origin "text"
  ]
  node [
    id 5
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 6
    label "osoba"
    origin "text"
  ]
  node [
    id 7
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 8
    label "przy"
    origin "text"
  ]
  node [
    id 9
    label "pisanie"
    origin "text"
  ]
  node [
    id 10
    label "podr&#281;cznik"
    origin "text"
  ]
  node [
    id 11
    label "dla"
    origin "text"
  ]
  node [
    id 12
    label "gimnazjum"
    origin "text"
  ]
  node [
    id 13
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "prowadzony"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "nasa"
    origin "text"
  ]
  node [
    id 18
    label "praca"
    origin "text"
  ]
  node [
    id 19
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 20
    label "invite"
  ]
  node [
    id 21
    label "ask"
  ]
  node [
    id 22
    label "oferowa&#263;"
  ]
  node [
    id 23
    label "zach&#281;ca&#263;"
  ]
  node [
    id 24
    label "volunteer"
  ]
  node [
    id 25
    label "belfer"
  ]
  node [
    id 26
    label "kszta&#322;ciciel"
  ]
  node [
    id 27
    label "preceptor"
  ]
  node [
    id 28
    label "pedagog"
  ]
  node [
    id 29
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 30
    label "szkolnik"
  ]
  node [
    id 31
    label "profesor"
  ]
  node [
    id 32
    label "popularyzator"
  ]
  node [
    id 33
    label "rozszerzyciel"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "autor"
  ]
  node [
    id 36
    label "wyprawka"
  ]
  node [
    id 37
    label "mundurek"
  ]
  node [
    id 38
    label "szko&#322;a"
  ]
  node [
    id 39
    label "tarcza"
  ]
  node [
    id 40
    label "elew"
  ]
  node [
    id 41
    label "absolwent"
  ]
  node [
    id 42
    label "klasa"
  ]
  node [
    id 43
    label "stopie&#324;_naukowy"
  ]
  node [
    id 44
    label "nauczyciel_akademicki"
  ]
  node [
    id 45
    label "tytu&#322;"
  ]
  node [
    id 46
    label "profesura"
  ]
  node [
    id 47
    label "konsulent"
  ]
  node [
    id 48
    label "wirtuoz"
  ]
  node [
    id 49
    label "zwierzchnik"
  ]
  node [
    id 50
    label "ekspert"
  ]
  node [
    id 51
    label "ochotnik"
  ]
  node [
    id 52
    label "pomocnik"
  ]
  node [
    id 53
    label "student"
  ]
  node [
    id 54
    label "nauczyciel_muzyki"
  ]
  node [
    id 55
    label "zakonnik"
  ]
  node [
    id 56
    label "J&#281;drzejewicz"
  ]
  node [
    id 57
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 58
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 59
    label "John_Dewey"
  ]
  node [
    id 60
    label "geography"
  ]
  node [
    id 61
    label "przedmiot"
  ]
  node [
    id 62
    label "sedymentologia"
  ]
  node [
    id 63
    label "oceanologia"
  ]
  node [
    id 64
    label "astrografia"
  ]
  node [
    id 65
    label "kierunek"
  ]
  node [
    id 66
    label "geografia_regionalna"
  ]
  node [
    id 67
    label "antropogeografia"
  ]
  node [
    id 68
    label "kartografia"
  ]
  node [
    id 69
    label "fitogeografia"
  ]
  node [
    id 70
    label "zoogeografia"
  ]
  node [
    id 71
    label "topografia"
  ]
  node [
    id 72
    label "turyzm"
  ]
  node [
    id 73
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 74
    label "krajoznawstwo"
  ]
  node [
    id 75
    label "gegra"
  ]
  node [
    id 76
    label "homologia"
  ]
  node [
    id 77
    label "geografia_fizyczna"
  ]
  node [
    id 78
    label "biogeografia"
  ]
  node [
    id 79
    label "hierotopografia"
  ]
  node [
    id 80
    label "geografia_ekonomiczna"
  ]
  node [
    id 81
    label "polarnictwo"
  ]
  node [
    id 82
    label "jeografia"
  ]
  node [
    id 83
    label "geografia_lingwistyczna"
  ]
  node [
    id 84
    label "warunki"
  ]
  node [
    id 85
    label "nauka_przyrodnicza"
  ]
  node [
    id 86
    label "nauki_o_Ziemi"
  ]
  node [
    id 87
    label "klimatologia"
  ]
  node [
    id 88
    label "przebieg"
  ]
  node [
    id 89
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 90
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 91
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 92
    label "praktyka"
  ]
  node [
    id 93
    label "system"
  ]
  node [
    id 94
    label "przeorientowywanie"
  ]
  node [
    id 95
    label "studia"
  ]
  node [
    id 96
    label "linia"
  ]
  node [
    id 97
    label "bok"
  ]
  node [
    id 98
    label "skr&#281;canie"
  ]
  node [
    id 99
    label "skr&#281;ca&#263;"
  ]
  node [
    id 100
    label "przeorientowywa&#263;"
  ]
  node [
    id 101
    label "orientowanie"
  ]
  node [
    id 102
    label "skr&#281;ci&#263;"
  ]
  node [
    id 103
    label "przeorientowanie"
  ]
  node [
    id 104
    label "zorientowanie"
  ]
  node [
    id 105
    label "przeorientowa&#263;"
  ]
  node [
    id 106
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 107
    label "metoda"
  ]
  node [
    id 108
    label "ty&#322;"
  ]
  node [
    id 109
    label "zorientowa&#263;"
  ]
  node [
    id 110
    label "g&#243;ra"
  ]
  node [
    id 111
    label "orientowa&#263;"
  ]
  node [
    id 112
    label "spos&#243;b"
  ]
  node [
    id 113
    label "ideologia"
  ]
  node [
    id 114
    label "orientacja"
  ]
  node [
    id 115
    label "prz&#243;d"
  ]
  node [
    id 116
    label "bearing"
  ]
  node [
    id 117
    label "skr&#281;cenie"
  ]
  node [
    id 118
    label "status"
  ]
  node [
    id 119
    label "sytuacja"
  ]
  node [
    id 120
    label "zboczenie"
  ]
  node [
    id 121
    label "om&#243;wienie"
  ]
  node [
    id 122
    label "sponiewieranie"
  ]
  node [
    id 123
    label "discipline"
  ]
  node [
    id 124
    label "rzecz"
  ]
  node [
    id 125
    label "omawia&#263;"
  ]
  node [
    id 126
    label "kr&#261;&#380;enie"
  ]
  node [
    id 127
    label "tre&#347;&#263;"
  ]
  node [
    id 128
    label "robienie"
  ]
  node [
    id 129
    label "sponiewiera&#263;"
  ]
  node [
    id 130
    label "element"
  ]
  node [
    id 131
    label "entity"
  ]
  node [
    id 132
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 133
    label "tematyka"
  ]
  node [
    id 134
    label "w&#261;tek"
  ]
  node [
    id 135
    label "charakter"
  ]
  node [
    id 136
    label "zbaczanie"
  ]
  node [
    id 137
    label "program_nauczania"
  ]
  node [
    id 138
    label "om&#243;wi&#263;"
  ]
  node [
    id 139
    label "omawianie"
  ]
  node [
    id 140
    label "thing"
  ]
  node [
    id 141
    label "kultura"
  ]
  node [
    id 142
    label "istota"
  ]
  node [
    id 143
    label "zbacza&#263;"
  ]
  node [
    id 144
    label "zboczy&#263;"
  ]
  node [
    id 145
    label "opis"
  ]
  node [
    id 146
    label "turystyka"
  ]
  node [
    id 147
    label "topography"
  ]
  node [
    id 148
    label "pochy&#322;omierz"
  ]
  node [
    id 149
    label "geodezja"
  ]
  node [
    id 150
    label "osnowa_geodezyjna"
  ]
  node [
    id 151
    label "ukszta&#322;towanie"
  ]
  node [
    id 152
    label "izolinia"
  ]
  node [
    id 153
    label "cecha"
  ]
  node [
    id 154
    label "kartometria"
  ]
  node [
    id 155
    label "geologia"
  ]
  node [
    id 156
    label "dendroklimatologia"
  ]
  node [
    id 157
    label "agroklimatologia"
  ]
  node [
    id 158
    label "meteorologia"
  ]
  node [
    id 159
    label "makroklimatologia"
  ]
  node [
    id 160
    label "zoologia"
  ]
  node [
    id 161
    label "wyprawa"
  ]
  node [
    id 162
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 163
    label "chorologia"
  ]
  node [
    id 164
    label "nauka"
  ]
  node [
    id 165
    label "botanika"
  ]
  node [
    id 166
    label "florystyka"
  ]
  node [
    id 167
    label "podobie&#324;stwo"
  ]
  node [
    id 168
    label "biologia"
  ]
  node [
    id 169
    label "zgodno&#347;&#263;"
  ]
  node [
    id 170
    label "homology"
  ]
  node [
    id 171
    label "funkcja"
  ]
  node [
    id 172
    label "fotografia"
  ]
  node [
    id 173
    label "spo&#322;ecznik"
  ]
  node [
    id 174
    label "praktykant"
  ]
  node [
    id 175
    label "Kuro&#324;"
  ]
  node [
    id 176
    label "dzia&#322;acz"
  ]
  node [
    id 177
    label "nowicjusz"
  ]
  node [
    id 178
    label "legia"
  ]
  node [
    id 179
    label "ch&#281;tny"
  ]
  node [
    id 180
    label "werbowanie_si&#281;"
  ]
  node [
    id 181
    label "stopie&#324;_harcerski"
  ]
  node [
    id 182
    label "rekrut"
  ]
  node [
    id 183
    label "ca&#322;y"
  ]
  node [
    id 184
    label "jedyny"
  ]
  node [
    id 185
    label "du&#380;y"
  ]
  node [
    id 186
    label "zdr&#243;w"
  ]
  node [
    id 187
    label "calu&#347;ko"
  ]
  node [
    id 188
    label "kompletny"
  ]
  node [
    id 189
    label "&#380;ywy"
  ]
  node [
    id 190
    label "pe&#322;ny"
  ]
  node [
    id 191
    label "podobny"
  ]
  node [
    id 192
    label "ca&#322;o"
  ]
  node [
    id 193
    label "Chocho&#322;"
  ]
  node [
    id 194
    label "Herkules_Poirot"
  ]
  node [
    id 195
    label "Edyp"
  ]
  node [
    id 196
    label "ludzko&#347;&#263;"
  ]
  node [
    id 197
    label "parali&#380;owa&#263;"
  ]
  node [
    id 198
    label "Harry_Potter"
  ]
  node [
    id 199
    label "Casanova"
  ]
  node [
    id 200
    label "Gargantua"
  ]
  node [
    id 201
    label "Zgredek"
  ]
  node [
    id 202
    label "Winnetou"
  ]
  node [
    id 203
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 204
    label "posta&#263;"
  ]
  node [
    id 205
    label "Dulcynea"
  ]
  node [
    id 206
    label "kategoria_gramatyczna"
  ]
  node [
    id 207
    label "g&#322;owa"
  ]
  node [
    id 208
    label "figura"
  ]
  node [
    id 209
    label "portrecista"
  ]
  node [
    id 210
    label "person"
  ]
  node [
    id 211
    label "Sherlock_Holmes"
  ]
  node [
    id 212
    label "Quasimodo"
  ]
  node [
    id 213
    label "Plastu&#347;"
  ]
  node [
    id 214
    label "Faust"
  ]
  node [
    id 215
    label "Wallenrod"
  ]
  node [
    id 216
    label "Dwukwiat"
  ]
  node [
    id 217
    label "koniugacja"
  ]
  node [
    id 218
    label "profanum"
  ]
  node [
    id 219
    label "Don_Juan"
  ]
  node [
    id 220
    label "Don_Kiszot"
  ]
  node [
    id 221
    label "mikrokosmos"
  ]
  node [
    id 222
    label "duch"
  ]
  node [
    id 223
    label "antropochoria"
  ]
  node [
    id 224
    label "oddzia&#322;ywanie"
  ]
  node [
    id 225
    label "Hamlet"
  ]
  node [
    id 226
    label "Werter"
  ]
  node [
    id 227
    label "Szwejk"
  ]
  node [
    id 228
    label "homo_sapiens"
  ]
  node [
    id 229
    label "mentalno&#347;&#263;"
  ]
  node [
    id 230
    label "superego"
  ]
  node [
    id 231
    label "psychika"
  ]
  node [
    id 232
    label "znaczenie"
  ]
  node [
    id 233
    label "wn&#281;trze"
  ]
  node [
    id 234
    label "charakterystyka"
  ]
  node [
    id 235
    label "zaistnie&#263;"
  ]
  node [
    id 236
    label "Osjan"
  ]
  node [
    id 237
    label "kto&#347;"
  ]
  node [
    id 238
    label "wygl&#261;d"
  ]
  node [
    id 239
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 240
    label "osobowo&#347;&#263;"
  ]
  node [
    id 241
    label "wytw&#243;r"
  ]
  node [
    id 242
    label "trim"
  ]
  node [
    id 243
    label "poby&#263;"
  ]
  node [
    id 244
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 245
    label "Aspazja"
  ]
  node [
    id 246
    label "punkt_widzenia"
  ]
  node [
    id 247
    label "kompleksja"
  ]
  node [
    id 248
    label "wytrzyma&#263;"
  ]
  node [
    id 249
    label "budowa"
  ]
  node [
    id 250
    label "formacja"
  ]
  node [
    id 251
    label "pozosta&#263;"
  ]
  node [
    id 252
    label "point"
  ]
  node [
    id 253
    label "przedstawienie"
  ]
  node [
    id 254
    label "go&#347;&#263;"
  ]
  node [
    id 255
    label "hamper"
  ]
  node [
    id 256
    label "spasm"
  ]
  node [
    id 257
    label "mrozi&#263;"
  ]
  node [
    id 258
    label "pora&#380;a&#263;"
  ]
  node [
    id 259
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 260
    label "fleksja"
  ]
  node [
    id 261
    label "liczba"
  ]
  node [
    id 262
    label "coupling"
  ]
  node [
    id 263
    label "tryb"
  ]
  node [
    id 264
    label "czas"
  ]
  node [
    id 265
    label "czasownik"
  ]
  node [
    id 266
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 267
    label "orz&#281;sek"
  ]
  node [
    id 268
    label "pryncypa&#322;"
  ]
  node [
    id 269
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 270
    label "kszta&#322;t"
  ]
  node [
    id 271
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 272
    label "wiedza"
  ]
  node [
    id 273
    label "kierowa&#263;"
  ]
  node [
    id 274
    label "alkohol"
  ]
  node [
    id 275
    label "zdolno&#347;&#263;"
  ]
  node [
    id 276
    label "&#380;ycie"
  ]
  node [
    id 277
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 278
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 279
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 280
    label "sztuka"
  ]
  node [
    id 281
    label "dekiel"
  ]
  node [
    id 282
    label "ro&#347;lina"
  ]
  node [
    id 283
    label "&#347;ci&#281;cie"
  ]
  node [
    id 284
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 285
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 286
    label "&#347;ci&#281;gno"
  ]
  node [
    id 287
    label "noosfera"
  ]
  node [
    id 288
    label "byd&#322;o"
  ]
  node [
    id 289
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 290
    label "makrocefalia"
  ]
  node [
    id 291
    label "obiekt"
  ]
  node [
    id 292
    label "ucho"
  ]
  node [
    id 293
    label "m&#243;zg"
  ]
  node [
    id 294
    label "kierownictwo"
  ]
  node [
    id 295
    label "fryzura"
  ]
  node [
    id 296
    label "umys&#322;"
  ]
  node [
    id 297
    label "cia&#322;o"
  ]
  node [
    id 298
    label "cz&#322;onek"
  ]
  node [
    id 299
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 300
    label "czaszka"
  ]
  node [
    id 301
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 302
    label "dziedzina"
  ]
  node [
    id 303
    label "powodowanie"
  ]
  node [
    id 304
    label "hipnotyzowanie"
  ]
  node [
    id 305
    label "&#347;lad"
  ]
  node [
    id 306
    label "docieranie"
  ]
  node [
    id 307
    label "natural_process"
  ]
  node [
    id 308
    label "reakcja_chemiczna"
  ]
  node [
    id 309
    label "wdzieranie_si&#281;"
  ]
  node [
    id 310
    label "zjawisko"
  ]
  node [
    id 311
    label "act"
  ]
  node [
    id 312
    label "rezultat"
  ]
  node [
    id 313
    label "lobbysta"
  ]
  node [
    id 314
    label "allochoria"
  ]
  node [
    id 315
    label "fotograf"
  ]
  node [
    id 316
    label "malarz"
  ]
  node [
    id 317
    label "artysta"
  ]
  node [
    id 318
    label "p&#322;aszczyzna"
  ]
  node [
    id 319
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 320
    label "bierka_szachowa"
  ]
  node [
    id 321
    label "obiekt_matematyczny"
  ]
  node [
    id 322
    label "gestaltyzm"
  ]
  node [
    id 323
    label "styl"
  ]
  node [
    id 324
    label "obraz"
  ]
  node [
    id 325
    label "d&#378;wi&#281;k"
  ]
  node [
    id 326
    label "character"
  ]
  node [
    id 327
    label "rze&#378;ba"
  ]
  node [
    id 328
    label "stylistyka"
  ]
  node [
    id 329
    label "figure"
  ]
  node [
    id 330
    label "miejsce"
  ]
  node [
    id 331
    label "antycypacja"
  ]
  node [
    id 332
    label "ornamentyka"
  ]
  node [
    id 333
    label "informacja"
  ]
  node [
    id 334
    label "facet"
  ]
  node [
    id 335
    label "popis"
  ]
  node [
    id 336
    label "wiersz"
  ]
  node [
    id 337
    label "symetria"
  ]
  node [
    id 338
    label "lingwistyka_kognitywna"
  ]
  node [
    id 339
    label "karta"
  ]
  node [
    id 340
    label "shape"
  ]
  node [
    id 341
    label "podzbi&#243;r"
  ]
  node [
    id 342
    label "perspektywa"
  ]
  node [
    id 343
    label "Szekspir"
  ]
  node [
    id 344
    label "Mickiewicz"
  ]
  node [
    id 345
    label "cierpienie"
  ]
  node [
    id 346
    label "piek&#322;o"
  ]
  node [
    id 347
    label "human_body"
  ]
  node [
    id 348
    label "ofiarowywanie"
  ]
  node [
    id 349
    label "sfera_afektywna"
  ]
  node [
    id 350
    label "nekromancja"
  ]
  node [
    id 351
    label "Po&#347;wist"
  ]
  node [
    id 352
    label "podekscytowanie"
  ]
  node [
    id 353
    label "deformowanie"
  ]
  node [
    id 354
    label "sumienie"
  ]
  node [
    id 355
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 356
    label "deformowa&#263;"
  ]
  node [
    id 357
    label "zjawa"
  ]
  node [
    id 358
    label "zmar&#322;y"
  ]
  node [
    id 359
    label "istota_nadprzyrodzona"
  ]
  node [
    id 360
    label "power"
  ]
  node [
    id 361
    label "ofiarowywa&#263;"
  ]
  node [
    id 362
    label "oddech"
  ]
  node [
    id 363
    label "seksualno&#347;&#263;"
  ]
  node [
    id 364
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 365
    label "byt"
  ]
  node [
    id 366
    label "si&#322;a"
  ]
  node [
    id 367
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 368
    label "ego"
  ]
  node [
    id 369
    label "ofiarowanie"
  ]
  node [
    id 370
    label "fizjonomia"
  ]
  node [
    id 371
    label "kompleks"
  ]
  node [
    id 372
    label "zapalno&#347;&#263;"
  ]
  node [
    id 373
    label "T&#281;sknica"
  ]
  node [
    id 374
    label "ofiarowa&#263;"
  ]
  node [
    id 375
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 376
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 377
    label "passion"
  ]
  node [
    id 378
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 379
    label "atom"
  ]
  node [
    id 380
    label "odbicie"
  ]
  node [
    id 381
    label "przyroda"
  ]
  node [
    id 382
    label "Ziemia"
  ]
  node [
    id 383
    label "kosmos"
  ]
  node [
    id 384
    label "miniatura"
  ]
  node [
    id 385
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 386
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 387
    label "enchantment"
  ]
  node [
    id 388
    label "formu&#322;owanie"
  ]
  node [
    id 389
    label "stawianie"
  ]
  node [
    id 390
    label "zamazywanie"
  ]
  node [
    id 391
    label "t&#322;uczenie"
  ]
  node [
    id 392
    label "pisywanie"
  ]
  node [
    id 393
    label "zamazanie"
  ]
  node [
    id 394
    label "tworzenie"
  ]
  node [
    id 395
    label "ozdabianie"
  ]
  node [
    id 396
    label "dysgrafia"
  ]
  node [
    id 397
    label "przepisanie"
  ]
  node [
    id 398
    label "popisanie"
  ]
  node [
    id 399
    label "donoszenie"
  ]
  node [
    id 400
    label "wci&#261;ganie"
  ]
  node [
    id 401
    label "odpisywanie"
  ]
  node [
    id 402
    label "dopisywanie"
  ]
  node [
    id 403
    label "dysortografia"
  ]
  node [
    id 404
    label "writing"
  ]
  node [
    id 405
    label "przypisywanie"
  ]
  node [
    id 406
    label "kre&#347;lenie"
  ]
  node [
    id 407
    label "przekazanie"
  ]
  node [
    id 408
    label "skopiowanie"
  ]
  node [
    id 409
    label "arrangement"
  ]
  node [
    id 410
    label "przeniesienie"
  ]
  node [
    id 411
    label "testament"
  ]
  node [
    id 412
    label "lekarstwo"
  ]
  node [
    id 413
    label "zadanie"
  ]
  node [
    id 414
    label "answer"
  ]
  node [
    id 415
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 416
    label "transcription"
  ]
  node [
    id 417
    label "zalecenie"
  ]
  node [
    id 418
    label "spowodowanie"
  ]
  node [
    id 419
    label "pokrycie"
  ]
  node [
    id 420
    label "niewidoczny"
  ]
  node [
    id 421
    label "cichy"
  ]
  node [
    id 422
    label "nieokre&#347;lony"
  ]
  node [
    id 423
    label "retraction"
  ]
  node [
    id 424
    label "w&#322;&#261;czanie"
  ]
  node [
    id 425
    label "pokrywanie"
  ]
  node [
    id 426
    label "kopiowanie"
  ]
  node [
    id 427
    label "przekazywanie"
  ]
  node [
    id 428
    label "zrzekanie_si&#281;"
  ]
  node [
    id 429
    label "odpowiadanie"
  ]
  node [
    id 430
    label "odliczanie"
  ]
  node [
    id 431
    label "popisywanie"
  ]
  node [
    id 432
    label "ko&#324;czenie"
  ]
  node [
    id 433
    label "dodawanie"
  ]
  node [
    id 434
    label "ziszczanie_si&#281;"
  ]
  node [
    id 435
    label "rozdrabnianie"
  ]
  node [
    id 436
    label "strike"
  ]
  node [
    id 437
    label "produkowanie"
  ]
  node [
    id 438
    label "fracture"
  ]
  node [
    id 439
    label "stukanie"
  ]
  node [
    id 440
    label "rozbijanie"
  ]
  node [
    id 441
    label "zestrzeliwanie"
  ]
  node [
    id 442
    label "zestrzelenie"
  ]
  node [
    id 443
    label "odstrzeliwanie"
  ]
  node [
    id 444
    label "mi&#281;so"
  ]
  node [
    id 445
    label "wystrzelanie"
  ]
  node [
    id 446
    label "wylatywanie"
  ]
  node [
    id 447
    label "chybianie"
  ]
  node [
    id 448
    label "plucie"
  ]
  node [
    id 449
    label "przypieprzanie"
  ]
  node [
    id 450
    label "przestrzeliwanie"
  ]
  node [
    id 451
    label "respite"
  ]
  node [
    id 452
    label "walczenie"
  ]
  node [
    id 453
    label "dorzynanie"
  ]
  node [
    id 454
    label "ostrzelanie"
  ]
  node [
    id 455
    label "kropni&#281;cie"
  ]
  node [
    id 456
    label "bicie"
  ]
  node [
    id 457
    label "ostrzeliwanie"
  ]
  node [
    id 458
    label "trafianie"
  ]
  node [
    id 459
    label "zat&#322;uczenie"
  ]
  node [
    id 460
    label "ut&#322;uczenie"
  ]
  node [
    id 461
    label "odpalanie"
  ]
  node [
    id 462
    label "odstrzelenie"
  ]
  node [
    id 463
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 464
    label "postrzelanie"
  ]
  node [
    id 465
    label "zabijanie"
  ]
  node [
    id 466
    label "powtarzanie"
  ]
  node [
    id 467
    label "uderzanie"
  ]
  node [
    id 468
    label "film_editing"
  ]
  node [
    id 469
    label "chybienie"
  ]
  node [
    id 470
    label "grzanie"
  ]
  node [
    id 471
    label "palenie"
  ]
  node [
    id 472
    label "fire"
  ]
  node [
    id 473
    label "mia&#380;d&#380;enie"
  ]
  node [
    id 474
    label "prze&#322;adowywanie"
  ]
  node [
    id 475
    label "granie"
  ]
  node [
    id 476
    label "zajmowanie_si&#281;"
  ]
  node [
    id 477
    label "attribute"
  ]
  node [
    id 478
    label "uznawanie"
  ]
  node [
    id 479
    label "property"
  ]
  node [
    id 480
    label "uniewa&#380;nianie"
  ]
  node [
    id 481
    label "skre&#347;lanie"
  ]
  node [
    id 482
    label "pokre&#347;lenie"
  ]
  node [
    id 483
    label "anointing"
  ]
  node [
    id 484
    label "usuwanie"
  ]
  node [
    id 485
    label "sporz&#261;dzanie"
  ]
  node [
    id 486
    label "czynno&#347;&#263;"
  ]
  node [
    id 487
    label "opowiadanie"
  ]
  node [
    id 488
    label "formation"
  ]
  node [
    id 489
    label "umieszczanie"
  ]
  node [
    id 490
    label "rozmieszczanie"
  ]
  node [
    id 491
    label "postawienie"
  ]
  node [
    id 492
    label "podstawianie"
  ]
  node [
    id 493
    label "spinanie"
  ]
  node [
    id 494
    label "kupowanie"
  ]
  node [
    id 495
    label "sponsorship"
  ]
  node [
    id 496
    label "zostawianie"
  ]
  node [
    id 497
    label "podstawienie"
  ]
  node [
    id 498
    label "zabudowywanie"
  ]
  node [
    id 499
    label "przebudowanie_si&#281;"
  ]
  node [
    id 500
    label "gotowanie_si&#281;"
  ]
  node [
    id 501
    label "position"
  ]
  node [
    id 502
    label "nastawianie_si&#281;"
  ]
  node [
    id 503
    label "upami&#281;tnianie"
  ]
  node [
    id 504
    label "spi&#281;cie"
  ]
  node [
    id 505
    label "nastawianie"
  ]
  node [
    id 506
    label "przebudowanie"
  ]
  node [
    id 507
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 508
    label "przestawianie"
  ]
  node [
    id 509
    label "typowanie"
  ]
  node [
    id 510
    label "przebudowywanie"
  ]
  node [
    id 511
    label "podbudowanie"
  ]
  node [
    id 512
    label "podbudowywanie"
  ]
  node [
    id 513
    label "dawanie"
  ]
  node [
    id 514
    label "fundator"
  ]
  node [
    id 515
    label "wyrastanie"
  ]
  node [
    id 516
    label "przebudowywanie_si&#281;"
  ]
  node [
    id 517
    label "przestawienie"
  ]
  node [
    id 518
    label "odbudowanie"
  ]
  node [
    id 519
    label "adornment"
  ]
  node [
    id 520
    label "upi&#281;kszanie"
  ]
  node [
    id 521
    label "pi&#281;kniejszy"
  ]
  node [
    id 522
    label "pope&#322;nianie"
  ]
  node [
    id 523
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 524
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 525
    label "stanowienie"
  ]
  node [
    id 526
    label "structure"
  ]
  node [
    id 527
    label "development"
  ]
  node [
    id 528
    label "exploitation"
  ]
  node [
    id 529
    label "do&#322;&#261;czanie"
  ]
  node [
    id 530
    label "zu&#380;ycie"
  ]
  node [
    id 531
    label "dosi&#281;ganie"
  ]
  node [
    id 532
    label "zanoszenie"
  ]
  node [
    id 533
    label "przebycie"
  ]
  node [
    id 534
    label "sk&#322;adanie"
  ]
  node [
    id 535
    label "informowanie"
  ]
  node [
    id 536
    label "ci&#261;&#380;a"
  ]
  node [
    id 537
    label "urodzenie"
  ]
  node [
    id 538
    label "conceptualization"
  ]
  node [
    id 539
    label "rozwlekanie"
  ]
  node [
    id 540
    label "zauwa&#380;anie"
  ]
  node [
    id 541
    label "komunikowanie"
  ]
  node [
    id 542
    label "formu&#322;owanie_si&#281;"
  ]
  node [
    id 543
    label "m&#243;wienie"
  ]
  node [
    id 544
    label "rzucanie"
  ]
  node [
    id 545
    label "dysgraphia"
  ]
  node [
    id 546
    label "dysleksja"
  ]
  node [
    id 547
    label "pisa&#263;"
  ]
  node [
    id 548
    label "pomoc_naukowa"
  ]
  node [
    id 549
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 550
    label "egzemplarz"
  ]
  node [
    id 551
    label "rozdzia&#322;"
  ]
  node [
    id 552
    label "wk&#322;ad"
  ]
  node [
    id 553
    label "zak&#322;adka"
  ]
  node [
    id 554
    label "nomina&#322;"
  ]
  node [
    id 555
    label "ok&#322;adka"
  ]
  node [
    id 556
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 557
    label "wydawnictwo"
  ]
  node [
    id 558
    label "ekslibris"
  ]
  node [
    id 559
    label "tekst"
  ]
  node [
    id 560
    label "przek&#322;adacz"
  ]
  node [
    id 561
    label "bibliofilstwo"
  ]
  node [
    id 562
    label "falc"
  ]
  node [
    id 563
    label "pagina"
  ]
  node [
    id 564
    label "zw&#243;j"
  ]
  node [
    id 565
    label "zapomoga"
  ]
  node [
    id 566
    label "komplet"
  ]
  node [
    id 567
    label "ucze&#324;"
  ]
  node [
    id 568
    label "layette"
  ]
  node [
    id 569
    label "niemowl&#281;"
  ]
  node [
    id 570
    label "wiano"
  ]
  node [
    id 571
    label "szko&#322;a_ponadpodstawowa"
  ]
  node [
    id 572
    label "warunek_lokalowy"
  ]
  node [
    id 573
    label "plac"
  ]
  node [
    id 574
    label "location"
  ]
  node [
    id 575
    label "uwaga"
  ]
  node [
    id 576
    label "przestrze&#324;"
  ]
  node [
    id 577
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 578
    label "chwila"
  ]
  node [
    id 579
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 580
    label "rz&#261;d"
  ]
  node [
    id 581
    label "do&#347;wiadczenie"
  ]
  node [
    id 582
    label "teren_szko&#322;y"
  ]
  node [
    id 583
    label "kwalifikacje"
  ]
  node [
    id 584
    label "school"
  ]
  node [
    id 585
    label "zda&#263;"
  ]
  node [
    id 586
    label "gabinet"
  ]
  node [
    id 587
    label "urszulanki"
  ]
  node [
    id 588
    label "sztuba"
  ]
  node [
    id 589
    label "&#322;awa_szkolna"
  ]
  node [
    id 590
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 591
    label "przepisa&#263;"
  ]
  node [
    id 592
    label "muzyka"
  ]
  node [
    id 593
    label "grupa"
  ]
  node [
    id 594
    label "form"
  ]
  node [
    id 595
    label "lekcja"
  ]
  node [
    id 596
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 597
    label "skolaryzacja"
  ]
  node [
    id 598
    label "zdanie"
  ]
  node [
    id 599
    label "stopek"
  ]
  node [
    id 600
    label "sekretariat"
  ]
  node [
    id 601
    label "lesson"
  ]
  node [
    id 602
    label "instytucja"
  ]
  node [
    id 603
    label "niepokalanki"
  ]
  node [
    id 604
    label "siedziba"
  ]
  node [
    id 605
    label "szkolenie"
  ]
  node [
    id 606
    label "kara"
  ]
  node [
    id 607
    label "tablica"
  ]
  node [
    id 608
    label "nastawi&#263;"
  ]
  node [
    id 609
    label "draw"
  ]
  node [
    id 610
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 611
    label "incorporate"
  ]
  node [
    id 612
    label "obejrze&#263;"
  ]
  node [
    id 613
    label "impersonate"
  ]
  node [
    id 614
    label "dokoptowa&#263;"
  ]
  node [
    id 615
    label "prosecute"
  ]
  node [
    id 616
    label "uruchomi&#263;"
  ]
  node [
    id 617
    label "umie&#347;ci&#263;"
  ]
  node [
    id 618
    label "zacz&#261;&#263;"
  ]
  node [
    id 619
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 620
    label "spowodowa&#263;"
  ]
  node [
    id 621
    label "begin"
  ]
  node [
    id 622
    label "trip"
  ]
  node [
    id 623
    label "post&#261;pi&#263;"
  ]
  node [
    id 624
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 625
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 626
    label "odj&#261;&#263;"
  ]
  node [
    id 627
    label "zrobi&#263;"
  ]
  node [
    id 628
    label "cause"
  ]
  node [
    id 629
    label "introduce"
  ]
  node [
    id 630
    label "do"
  ]
  node [
    id 631
    label "set"
  ]
  node [
    id 632
    label "put"
  ]
  node [
    id 633
    label "uplasowa&#263;"
  ]
  node [
    id 634
    label "wpierniczy&#263;"
  ]
  node [
    id 635
    label "okre&#347;li&#263;"
  ]
  node [
    id 636
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 637
    label "zmieni&#263;"
  ]
  node [
    id 638
    label "umieszcza&#263;"
  ]
  node [
    id 639
    label "z&#322;amanie"
  ]
  node [
    id 640
    label "plant"
  ]
  node [
    id 641
    label "ustawi&#263;"
  ]
  node [
    id 642
    label "direct"
  ]
  node [
    id 643
    label "aim"
  ]
  node [
    id 644
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 645
    label "poprawi&#263;"
  ]
  node [
    id 646
    label "przyrz&#261;dzi&#263;"
  ]
  node [
    id 647
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 648
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 649
    label "wyznaczy&#263;"
  ]
  node [
    id 650
    label "visualize"
  ]
  node [
    id 651
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 652
    label "dokooptowa&#263;"
  ]
  node [
    id 653
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 654
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 655
    label "najem"
  ]
  node [
    id 656
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 657
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 658
    label "zak&#322;ad"
  ]
  node [
    id 659
    label "stosunek_pracy"
  ]
  node [
    id 660
    label "benedykty&#324;ski"
  ]
  node [
    id 661
    label "poda&#380;_pracy"
  ]
  node [
    id 662
    label "pracowanie"
  ]
  node [
    id 663
    label "tyrka"
  ]
  node [
    id 664
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 665
    label "zaw&#243;d"
  ]
  node [
    id 666
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 667
    label "tynkarski"
  ]
  node [
    id 668
    label "pracowa&#263;"
  ]
  node [
    id 669
    label "zmiana"
  ]
  node [
    id 670
    label "czynnik_produkcji"
  ]
  node [
    id 671
    label "zobowi&#261;zanie"
  ]
  node [
    id 672
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 673
    label "p&#322;&#243;d"
  ]
  node [
    id 674
    label "work"
  ]
  node [
    id 675
    label "activity"
  ]
  node [
    id 676
    label "bezproblemowy"
  ]
  node [
    id 677
    label "wydarzenie"
  ]
  node [
    id 678
    label "stosunek_prawny"
  ]
  node [
    id 679
    label "oblig"
  ]
  node [
    id 680
    label "uregulowa&#263;"
  ]
  node [
    id 681
    label "oddzia&#322;anie"
  ]
  node [
    id 682
    label "occupation"
  ]
  node [
    id 683
    label "duty"
  ]
  node [
    id 684
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 685
    label "zapowied&#378;"
  ]
  node [
    id 686
    label "obowi&#261;zek"
  ]
  node [
    id 687
    label "statement"
  ]
  node [
    id 688
    label "zapewnienie"
  ]
  node [
    id 689
    label "miejsce_pracy"
  ]
  node [
    id 690
    label "jednostka_organizacyjna"
  ]
  node [
    id 691
    label "wyko&#324;czenie"
  ]
  node [
    id 692
    label "firma"
  ]
  node [
    id 693
    label "czyn"
  ]
  node [
    id 694
    label "company"
  ]
  node [
    id 695
    label "instytut"
  ]
  node [
    id 696
    label "umowa"
  ]
  node [
    id 697
    label "&#321;ubianka"
  ]
  node [
    id 698
    label "dzia&#322;_personalny"
  ]
  node [
    id 699
    label "Kreml"
  ]
  node [
    id 700
    label "Bia&#322;y_Dom"
  ]
  node [
    id 701
    label "budynek"
  ]
  node [
    id 702
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 703
    label "sadowisko"
  ]
  node [
    id 704
    label "rewizja"
  ]
  node [
    id 705
    label "passage"
  ]
  node [
    id 706
    label "oznaka"
  ]
  node [
    id 707
    label "change"
  ]
  node [
    id 708
    label "ferment"
  ]
  node [
    id 709
    label "anatomopatolog"
  ]
  node [
    id 710
    label "zmianka"
  ]
  node [
    id 711
    label "amendment"
  ]
  node [
    id 712
    label "odmienianie"
  ]
  node [
    id 713
    label "tura"
  ]
  node [
    id 714
    label "cierpliwy"
  ]
  node [
    id 715
    label "mozolny"
  ]
  node [
    id 716
    label "wytrwa&#322;y"
  ]
  node [
    id 717
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 718
    label "benedykty&#324;sko"
  ]
  node [
    id 719
    label "typowy"
  ]
  node [
    id 720
    label "po_benedykty&#324;sku"
  ]
  node [
    id 721
    label "endeavor"
  ]
  node [
    id 722
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 723
    label "mie&#263;_miejsce"
  ]
  node [
    id 724
    label "podejmowa&#263;"
  ]
  node [
    id 725
    label "dziama&#263;"
  ]
  node [
    id 726
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 727
    label "bangla&#263;"
  ]
  node [
    id 728
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 729
    label "maszyna"
  ]
  node [
    id 730
    label "dzia&#322;a&#263;"
  ]
  node [
    id 731
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 732
    label "funkcjonowa&#263;"
  ]
  node [
    id 733
    label "zawodoznawstwo"
  ]
  node [
    id 734
    label "emocja"
  ]
  node [
    id 735
    label "office"
  ]
  node [
    id 736
    label "craft"
  ]
  node [
    id 737
    label "przepracowanie_si&#281;"
  ]
  node [
    id 738
    label "zarz&#261;dzanie"
  ]
  node [
    id 739
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 740
    label "podlizanie_si&#281;"
  ]
  node [
    id 741
    label "dopracowanie"
  ]
  node [
    id 742
    label "podlizywanie_si&#281;"
  ]
  node [
    id 743
    label "uruchamianie"
  ]
  node [
    id 744
    label "dzia&#322;anie"
  ]
  node [
    id 745
    label "d&#261;&#380;enie"
  ]
  node [
    id 746
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 747
    label "uruchomienie"
  ]
  node [
    id 748
    label "nakr&#281;canie"
  ]
  node [
    id 749
    label "funkcjonowanie"
  ]
  node [
    id 750
    label "tr&#243;jstronny"
  ]
  node [
    id 751
    label "postaranie_si&#281;"
  ]
  node [
    id 752
    label "odpocz&#281;cie"
  ]
  node [
    id 753
    label "nakr&#281;cenie"
  ]
  node [
    id 754
    label "zatrzymanie"
  ]
  node [
    id 755
    label "spracowanie_si&#281;"
  ]
  node [
    id 756
    label "skakanie"
  ]
  node [
    id 757
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 758
    label "podtrzymywanie"
  ]
  node [
    id 759
    label "zaprz&#281;ganie"
  ]
  node [
    id 760
    label "podejmowanie"
  ]
  node [
    id 761
    label "wyrabianie"
  ]
  node [
    id 762
    label "dzianie_si&#281;"
  ]
  node [
    id 763
    label "use"
  ]
  node [
    id 764
    label "przepracowanie"
  ]
  node [
    id 765
    label "poruszanie_si&#281;"
  ]
  node [
    id 766
    label "impact"
  ]
  node [
    id 767
    label "przepracowywanie"
  ]
  node [
    id 768
    label "awansowanie"
  ]
  node [
    id 769
    label "courtship"
  ]
  node [
    id 770
    label "zapracowanie"
  ]
  node [
    id 771
    label "wyrobienie"
  ]
  node [
    id 772
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 773
    label "w&#322;&#261;czenie"
  ]
  node [
    id 774
    label "transakcja"
  ]
  node [
    id 775
    label "biuro"
  ]
  node [
    id 776
    label "lead"
  ]
  node [
    id 777
    label "zesp&#243;&#322;"
  ]
  node [
    id 778
    label "w&#322;adza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 330
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 294
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 312
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 297
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 264
  ]
  edge [
    source 18
    target 310
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 263
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 424
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
]
