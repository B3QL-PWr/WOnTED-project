graph [
  node [
    id 0
    label "toronto"
    origin "text"
  ]
  node [
    id 1
    label "dominion"
    origin "text"
  ]
  node [
    id 2
    label "centre"
    origin "text"
  ]
  node [
    id 3
    label "Toronto"
  ]
  node [
    id 4
    label "Dominion"
  ]
  node [
    id 5
    label "Centre"
  ]
  node [
    id 6
    label "Financial"
  ]
  node [
    id 7
    label "District"
  ]
  node [
    id 8
    label "Ludwig"
  ]
  node [
    id 9
    label "Mies"
  ]
  node [
    id 10
    label "van"
  ]
  node [
    id 11
    label "dera"
  ]
  node [
    id 12
    label "Rohe"
  ]
  node [
    id 13
    label "Joe"
  ]
  node [
    id 14
    label "Fafard"
  ]
  node [
    id 15
    label "gallery"
  ]
  node [
    id 16
    label "of"
  ]
  node [
    id 17
    label "Inuit"
  ]
  node [
    id 18
    label "Art"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
]
