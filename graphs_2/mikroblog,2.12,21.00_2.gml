graph [
  node [
    id 0
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "rozdajo"
    origin "text"
  ]
  node [
    id 2
    label "rozdajosteam"
    origin "text"
  ]
  node [
    id 3
    label "organizowa&#263;"
  ]
  node [
    id 4
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 5
    label "czyni&#263;"
  ]
  node [
    id 6
    label "give"
  ]
  node [
    id 7
    label "stylizowa&#263;"
  ]
  node [
    id 8
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 9
    label "falowa&#263;"
  ]
  node [
    id 10
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 11
    label "peddle"
  ]
  node [
    id 12
    label "praca"
  ]
  node [
    id 13
    label "wydala&#263;"
  ]
  node [
    id 14
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 15
    label "tentegowa&#263;"
  ]
  node [
    id 16
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 17
    label "urz&#261;dza&#263;"
  ]
  node [
    id 18
    label "oszukiwa&#263;"
  ]
  node [
    id 19
    label "work"
  ]
  node [
    id 20
    label "ukazywa&#263;"
  ]
  node [
    id 21
    label "przerabia&#263;"
  ]
  node [
    id 22
    label "act"
  ]
  node [
    id 23
    label "post&#281;powa&#263;"
  ]
  node [
    id 24
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 25
    label "billow"
  ]
  node [
    id 26
    label "clutter"
  ]
  node [
    id 27
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 28
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 29
    label "beckon"
  ]
  node [
    id 30
    label "powiewa&#263;"
  ]
  node [
    id 31
    label "planowa&#263;"
  ]
  node [
    id 32
    label "dostosowywa&#263;"
  ]
  node [
    id 33
    label "treat"
  ]
  node [
    id 34
    label "pozyskiwa&#263;"
  ]
  node [
    id 35
    label "ensnare"
  ]
  node [
    id 36
    label "skupia&#263;"
  ]
  node [
    id 37
    label "create"
  ]
  node [
    id 38
    label "przygotowywa&#263;"
  ]
  node [
    id 39
    label "tworzy&#263;"
  ]
  node [
    id 40
    label "standard"
  ]
  node [
    id 41
    label "wprowadza&#263;"
  ]
  node [
    id 42
    label "kopiowa&#263;"
  ]
  node [
    id 43
    label "czerpa&#263;"
  ]
  node [
    id 44
    label "dally"
  ]
  node [
    id 45
    label "mock"
  ]
  node [
    id 46
    label "decydowa&#263;"
  ]
  node [
    id 47
    label "cast"
  ]
  node [
    id 48
    label "podbija&#263;"
  ]
  node [
    id 49
    label "sprawia&#263;"
  ]
  node [
    id 50
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 51
    label "przechodzi&#263;"
  ]
  node [
    id 52
    label "wytwarza&#263;"
  ]
  node [
    id 53
    label "amend"
  ]
  node [
    id 54
    label "zalicza&#263;"
  ]
  node [
    id 55
    label "overwork"
  ]
  node [
    id 56
    label "convert"
  ]
  node [
    id 57
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 58
    label "zamienia&#263;"
  ]
  node [
    id 59
    label "zmienia&#263;"
  ]
  node [
    id 60
    label "modyfikowa&#263;"
  ]
  node [
    id 61
    label "radzi&#263;_sobie"
  ]
  node [
    id 62
    label "pracowa&#263;"
  ]
  node [
    id 63
    label "przetwarza&#263;"
  ]
  node [
    id 64
    label "sp&#281;dza&#263;"
  ]
  node [
    id 65
    label "stylize"
  ]
  node [
    id 66
    label "upodabnia&#263;"
  ]
  node [
    id 67
    label "nadawa&#263;"
  ]
  node [
    id 68
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 69
    label "go"
  ]
  node [
    id 70
    label "przybiera&#263;"
  ]
  node [
    id 71
    label "i&#347;&#263;"
  ]
  node [
    id 72
    label "use"
  ]
  node [
    id 73
    label "blurt_out"
  ]
  node [
    id 74
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 75
    label "usuwa&#263;"
  ]
  node [
    id 76
    label "unwrap"
  ]
  node [
    id 77
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 78
    label "pokazywa&#263;"
  ]
  node [
    id 79
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 80
    label "orzyna&#263;"
  ]
  node [
    id 81
    label "oszwabia&#263;"
  ]
  node [
    id 82
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 83
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 84
    label "cheat"
  ]
  node [
    id 85
    label "dispose"
  ]
  node [
    id 86
    label "aran&#380;owa&#263;"
  ]
  node [
    id 87
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 88
    label "odpowiada&#263;"
  ]
  node [
    id 89
    label "zabezpiecza&#263;"
  ]
  node [
    id 90
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 91
    label "doprowadza&#263;"
  ]
  node [
    id 92
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 93
    label "najem"
  ]
  node [
    id 94
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 95
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 96
    label "zak&#322;ad"
  ]
  node [
    id 97
    label "stosunek_pracy"
  ]
  node [
    id 98
    label "benedykty&#324;ski"
  ]
  node [
    id 99
    label "poda&#380;_pracy"
  ]
  node [
    id 100
    label "pracowanie"
  ]
  node [
    id 101
    label "tyrka"
  ]
  node [
    id 102
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 103
    label "wytw&#243;r"
  ]
  node [
    id 104
    label "miejsce"
  ]
  node [
    id 105
    label "zaw&#243;d"
  ]
  node [
    id 106
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 107
    label "tynkarski"
  ]
  node [
    id 108
    label "czynno&#347;&#263;"
  ]
  node [
    id 109
    label "zmiana"
  ]
  node [
    id 110
    label "czynnik_produkcji"
  ]
  node [
    id 111
    label "zobowi&#261;zanie"
  ]
  node [
    id 112
    label "kierownictwo"
  ]
  node [
    id 113
    label "siedziba"
  ]
  node [
    id 114
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 115
    label "CITIES"
  ]
  node [
    id 116
    label "SKYLINES"
  ]
  node [
    id 117
    label "AFTER"
  ]
  node [
    id 118
    label "DARK"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 117
  ]
  edge [
    source 115
    target 118
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 118
  ]
  edge [
    source 117
    target 118
  ]
]
