graph [
  node [
    id 0
    label "wykop"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "miejsce"
    origin "text"
  ]
  node [
    id 3
    label "gdzie"
    origin "text"
  ]
  node [
    id 4
    label "gromadzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ciekawy"
    origin "text"
  ]
  node [
    id 6
    label "informacja"
    origin "text"
  ]
  node [
    id 7
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 8
    label "news"
    origin "text"
  ]
  node [
    id 9
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 10
    label "linek"
    origin "text"
  ]
  node [
    id 11
    label "budowa"
  ]
  node [
    id 12
    label "zrzutowy"
  ]
  node [
    id 13
    label "odk&#322;ad"
  ]
  node [
    id 14
    label "chody"
  ]
  node [
    id 15
    label "szaniec"
  ]
  node [
    id 16
    label "wyrobisko"
  ]
  node [
    id 17
    label "kopniak"
  ]
  node [
    id 18
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 19
    label "odwa&#322;"
  ]
  node [
    id 20
    label "grodzisko"
  ]
  node [
    id 21
    label "cios"
  ]
  node [
    id 22
    label "kick"
  ]
  node [
    id 23
    label "kopni&#281;cie"
  ]
  node [
    id 24
    label "&#347;rodkowiec"
  ]
  node [
    id 25
    label "podsadzka"
  ]
  node [
    id 26
    label "obudowa"
  ]
  node [
    id 27
    label "sp&#261;g"
  ]
  node [
    id 28
    label "strop"
  ]
  node [
    id 29
    label "rabowarka"
  ]
  node [
    id 30
    label "opinka"
  ]
  node [
    id 31
    label "stojak_cierny"
  ]
  node [
    id 32
    label "kopalnia"
  ]
  node [
    id 33
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 34
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 35
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 36
    label "immersion"
  ]
  node [
    id 37
    label "umieszczenie"
  ]
  node [
    id 38
    label "las"
  ]
  node [
    id 39
    label "nora"
  ]
  node [
    id 40
    label "pies_my&#347;liwski"
  ]
  node [
    id 41
    label "trasa"
  ]
  node [
    id 42
    label "doj&#347;cie"
  ]
  node [
    id 43
    label "zesp&#243;&#322;"
  ]
  node [
    id 44
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 45
    label "horodyszcze"
  ]
  node [
    id 46
    label "Wyszogr&#243;d"
  ]
  node [
    id 47
    label "usypisko"
  ]
  node [
    id 48
    label "r&#243;w"
  ]
  node [
    id 49
    label "wa&#322;"
  ]
  node [
    id 50
    label "redoubt"
  ]
  node [
    id 51
    label "fortyfikacja"
  ]
  node [
    id 52
    label "mechanika"
  ]
  node [
    id 53
    label "struktura"
  ]
  node [
    id 54
    label "figura"
  ]
  node [
    id 55
    label "miejsce_pracy"
  ]
  node [
    id 56
    label "cecha"
  ]
  node [
    id 57
    label "organ"
  ]
  node [
    id 58
    label "kreacja"
  ]
  node [
    id 59
    label "zwierz&#281;"
  ]
  node [
    id 60
    label "posesja"
  ]
  node [
    id 61
    label "konstrukcja"
  ]
  node [
    id 62
    label "wjazd"
  ]
  node [
    id 63
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 64
    label "praca"
  ]
  node [
    id 65
    label "constitution"
  ]
  node [
    id 66
    label "gleba"
  ]
  node [
    id 67
    label "p&#281;d"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "ablegier"
  ]
  node [
    id 70
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 71
    label "layer"
  ]
  node [
    id 72
    label "r&#243;j"
  ]
  node [
    id 73
    label "mrowisko"
  ]
  node [
    id 74
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 75
    label "mie&#263;_miejsce"
  ]
  node [
    id 76
    label "equal"
  ]
  node [
    id 77
    label "trwa&#263;"
  ]
  node [
    id 78
    label "chodzi&#263;"
  ]
  node [
    id 79
    label "si&#281;ga&#263;"
  ]
  node [
    id 80
    label "stan"
  ]
  node [
    id 81
    label "obecno&#347;&#263;"
  ]
  node [
    id 82
    label "stand"
  ]
  node [
    id 83
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 84
    label "uczestniczy&#263;"
  ]
  node [
    id 85
    label "participate"
  ]
  node [
    id 86
    label "robi&#263;"
  ]
  node [
    id 87
    label "istnie&#263;"
  ]
  node [
    id 88
    label "pozostawa&#263;"
  ]
  node [
    id 89
    label "zostawa&#263;"
  ]
  node [
    id 90
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 91
    label "adhere"
  ]
  node [
    id 92
    label "compass"
  ]
  node [
    id 93
    label "korzysta&#263;"
  ]
  node [
    id 94
    label "appreciation"
  ]
  node [
    id 95
    label "osi&#261;ga&#263;"
  ]
  node [
    id 96
    label "dociera&#263;"
  ]
  node [
    id 97
    label "get"
  ]
  node [
    id 98
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 99
    label "mierzy&#263;"
  ]
  node [
    id 100
    label "u&#380;ywa&#263;"
  ]
  node [
    id 101
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 102
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 103
    label "exsert"
  ]
  node [
    id 104
    label "being"
  ]
  node [
    id 105
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 106
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 107
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 108
    label "p&#322;ywa&#263;"
  ]
  node [
    id 109
    label "run"
  ]
  node [
    id 110
    label "bangla&#263;"
  ]
  node [
    id 111
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 112
    label "przebiega&#263;"
  ]
  node [
    id 113
    label "wk&#322;ada&#263;"
  ]
  node [
    id 114
    label "proceed"
  ]
  node [
    id 115
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 116
    label "carry"
  ]
  node [
    id 117
    label "bywa&#263;"
  ]
  node [
    id 118
    label "dziama&#263;"
  ]
  node [
    id 119
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 120
    label "stara&#263;_si&#281;"
  ]
  node [
    id 121
    label "para"
  ]
  node [
    id 122
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 123
    label "str&#243;j"
  ]
  node [
    id 124
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 125
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 126
    label "krok"
  ]
  node [
    id 127
    label "tryb"
  ]
  node [
    id 128
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 129
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 130
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 131
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 132
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 133
    label "continue"
  ]
  node [
    id 134
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 135
    label "Ohio"
  ]
  node [
    id 136
    label "wci&#281;cie"
  ]
  node [
    id 137
    label "Nowy_York"
  ]
  node [
    id 138
    label "warstwa"
  ]
  node [
    id 139
    label "samopoczucie"
  ]
  node [
    id 140
    label "Illinois"
  ]
  node [
    id 141
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 142
    label "state"
  ]
  node [
    id 143
    label "Jukatan"
  ]
  node [
    id 144
    label "Kalifornia"
  ]
  node [
    id 145
    label "Wirginia"
  ]
  node [
    id 146
    label "wektor"
  ]
  node [
    id 147
    label "Teksas"
  ]
  node [
    id 148
    label "Goa"
  ]
  node [
    id 149
    label "Waszyngton"
  ]
  node [
    id 150
    label "Massachusetts"
  ]
  node [
    id 151
    label "Alaska"
  ]
  node [
    id 152
    label "Arakan"
  ]
  node [
    id 153
    label "Hawaje"
  ]
  node [
    id 154
    label "Maryland"
  ]
  node [
    id 155
    label "punkt"
  ]
  node [
    id 156
    label "Michigan"
  ]
  node [
    id 157
    label "Arizona"
  ]
  node [
    id 158
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 159
    label "Georgia"
  ]
  node [
    id 160
    label "poziom"
  ]
  node [
    id 161
    label "Pensylwania"
  ]
  node [
    id 162
    label "shape"
  ]
  node [
    id 163
    label "Luizjana"
  ]
  node [
    id 164
    label "Nowy_Meksyk"
  ]
  node [
    id 165
    label "Alabama"
  ]
  node [
    id 166
    label "ilo&#347;&#263;"
  ]
  node [
    id 167
    label "Kansas"
  ]
  node [
    id 168
    label "Oregon"
  ]
  node [
    id 169
    label "Floryda"
  ]
  node [
    id 170
    label "Oklahoma"
  ]
  node [
    id 171
    label "jednostka_administracyjna"
  ]
  node [
    id 172
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 173
    label "warunek_lokalowy"
  ]
  node [
    id 174
    label "plac"
  ]
  node [
    id 175
    label "location"
  ]
  node [
    id 176
    label "uwaga"
  ]
  node [
    id 177
    label "przestrze&#324;"
  ]
  node [
    id 178
    label "status"
  ]
  node [
    id 179
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 180
    label "chwila"
  ]
  node [
    id 181
    label "cia&#322;o"
  ]
  node [
    id 182
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 183
    label "rz&#261;d"
  ]
  node [
    id 184
    label "charakterystyka"
  ]
  node [
    id 185
    label "m&#322;ot"
  ]
  node [
    id 186
    label "znak"
  ]
  node [
    id 187
    label "drzewo"
  ]
  node [
    id 188
    label "pr&#243;ba"
  ]
  node [
    id 189
    label "attribute"
  ]
  node [
    id 190
    label "marka"
  ]
  node [
    id 191
    label "Rzym_Zachodni"
  ]
  node [
    id 192
    label "whole"
  ]
  node [
    id 193
    label "element"
  ]
  node [
    id 194
    label "Rzym_Wschodni"
  ]
  node [
    id 195
    label "urz&#261;dzenie"
  ]
  node [
    id 196
    label "wypowied&#378;"
  ]
  node [
    id 197
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 198
    label "nagana"
  ]
  node [
    id 199
    label "tekst"
  ]
  node [
    id 200
    label "upomnienie"
  ]
  node [
    id 201
    label "dzienniczek"
  ]
  node [
    id 202
    label "wzgl&#261;d"
  ]
  node [
    id 203
    label "gossip"
  ]
  node [
    id 204
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 205
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 206
    label "najem"
  ]
  node [
    id 207
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 208
    label "zak&#322;ad"
  ]
  node [
    id 209
    label "stosunek_pracy"
  ]
  node [
    id 210
    label "benedykty&#324;ski"
  ]
  node [
    id 211
    label "poda&#380;_pracy"
  ]
  node [
    id 212
    label "pracowanie"
  ]
  node [
    id 213
    label "tyrka"
  ]
  node [
    id 214
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 215
    label "wytw&#243;r"
  ]
  node [
    id 216
    label "zaw&#243;d"
  ]
  node [
    id 217
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 218
    label "tynkarski"
  ]
  node [
    id 219
    label "pracowa&#263;"
  ]
  node [
    id 220
    label "czynno&#347;&#263;"
  ]
  node [
    id 221
    label "zmiana"
  ]
  node [
    id 222
    label "czynnik_produkcji"
  ]
  node [
    id 223
    label "zobowi&#261;zanie"
  ]
  node [
    id 224
    label "kierownictwo"
  ]
  node [
    id 225
    label "siedziba"
  ]
  node [
    id 226
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 227
    label "rozdzielanie"
  ]
  node [
    id 228
    label "bezbrze&#380;e"
  ]
  node [
    id 229
    label "czasoprzestrze&#324;"
  ]
  node [
    id 230
    label "niezmierzony"
  ]
  node [
    id 231
    label "przedzielenie"
  ]
  node [
    id 232
    label "nielito&#347;ciwy"
  ]
  node [
    id 233
    label "rozdziela&#263;"
  ]
  node [
    id 234
    label "oktant"
  ]
  node [
    id 235
    label "przedzieli&#263;"
  ]
  node [
    id 236
    label "przestw&#243;r"
  ]
  node [
    id 237
    label "condition"
  ]
  node [
    id 238
    label "awansowa&#263;"
  ]
  node [
    id 239
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 240
    label "znaczenie"
  ]
  node [
    id 241
    label "awans"
  ]
  node [
    id 242
    label "podmiotowo"
  ]
  node [
    id 243
    label "awansowanie"
  ]
  node [
    id 244
    label "sytuacja"
  ]
  node [
    id 245
    label "time"
  ]
  node [
    id 246
    label "czas"
  ]
  node [
    id 247
    label "rozmiar"
  ]
  node [
    id 248
    label "liczba"
  ]
  node [
    id 249
    label "circumference"
  ]
  node [
    id 250
    label "leksem"
  ]
  node [
    id 251
    label "cyrkumferencja"
  ]
  node [
    id 252
    label "strona"
  ]
  node [
    id 253
    label "ekshumowanie"
  ]
  node [
    id 254
    label "jednostka_organizacyjna"
  ]
  node [
    id 255
    label "p&#322;aszczyzna"
  ]
  node [
    id 256
    label "odwadnia&#263;"
  ]
  node [
    id 257
    label "zabalsamowanie"
  ]
  node [
    id 258
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 259
    label "odwodni&#263;"
  ]
  node [
    id 260
    label "sk&#243;ra"
  ]
  node [
    id 261
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 262
    label "staw"
  ]
  node [
    id 263
    label "ow&#322;osienie"
  ]
  node [
    id 264
    label "mi&#281;so"
  ]
  node [
    id 265
    label "zabalsamowa&#263;"
  ]
  node [
    id 266
    label "Izba_Konsyliarska"
  ]
  node [
    id 267
    label "unerwienie"
  ]
  node [
    id 268
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 269
    label "kremacja"
  ]
  node [
    id 270
    label "biorytm"
  ]
  node [
    id 271
    label "sekcja"
  ]
  node [
    id 272
    label "istota_&#380;ywa"
  ]
  node [
    id 273
    label "otworzy&#263;"
  ]
  node [
    id 274
    label "otwiera&#263;"
  ]
  node [
    id 275
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 276
    label "otworzenie"
  ]
  node [
    id 277
    label "materia"
  ]
  node [
    id 278
    label "pochowanie"
  ]
  node [
    id 279
    label "otwieranie"
  ]
  node [
    id 280
    label "ty&#322;"
  ]
  node [
    id 281
    label "szkielet"
  ]
  node [
    id 282
    label "tanatoplastyk"
  ]
  node [
    id 283
    label "odwadnianie"
  ]
  node [
    id 284
    label "Komitet_Region&#243;w"
  ]
  node [
    id 285
    label "odwodnienie"
  ]
  node [
    id 286
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 287
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 288
    label "nieumar&#322;y"
  ]
  node [
    id 289
    label "pochowa&#263;"
  ]
  node [
    id 290
    label "balsamowa&#263;"
  ]
  node [
    id 291
    label "tanatoplastyka"
  ]
  node [
    id 292
    label "temperatura"
  ]
  node [
    id 293
    label "ekshumowa&#263;"
  ]
  node [
    id 294
    label "balsamowanie"
  ]
  node [
    id 295
    label "uk&#322;ad"
  ]
  node [
    id 296
    label "prz&#243;d"
  ]
  node [
    id 297
    label "l&#281;d&#378;wie"
  ]
  node [
    id 298
    label "cz&#322;onek"
  ]
  node [
    id 299
    label "pogrzeb"
  ]
  node [
    id 300
    label "&#321;ubianka"
  ]
  node [
    id 301
    label "area"
  ]
  node [
    id 302
    label "Majdan"
  ]
  node [
    id 303
    label "pole_bitwy"
  ]
  node [
    id 304
    label "stoisko"
  ]
  node [
    id 305
    label "obszar"
  ]
  node [
    id 306
    label "pierzeja"
  ]
  node [
    id 307
    label "obiekt_handlowy"
  ]
  node [
    id 308
    label "zgromadzenie"
  ]
  node [
    id 309
    label "miasto"
  ]
  node [
    id 310
    label "targowica"
  ]
  node [
    id 311
    label "kram"
  ]
  node [
    id 312
    label "przybli&#380;enie"
  ]
  node [
    id 313
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 314
    label "kategoria"
  ]
  node [
    id 315
    label "szpaler"
  ]
  node [
    id 316
    label "lon&#380;a"
  ]
  node [
    id 317
    label "uporz&#261;dkowanie"
  ]
  node [
    id 318
    label "instytucja"
  ]
  node [
    id 319
    label "jednostka_systematyczna"
  ]
  node [
    id 320
    label "egzekutywa"
  ]
  node [
    id 321
    label "premier"
  ]
  node [
    id 322
    label "Londyn"
  ]
  node [
    id 323
    label "gabinet_cieni"
  ]
  node [
    id 324
    label "gromada"
  ]
  node [
    id 325
    label "number"
  ]
  node [
    id 326
    label "Konsulat"
  ]
  node [
    id 327
    label "tract"
  ]
  node [
    id 328
    label "klasa"
  ]
  node [
    id 329
    label "w&#322;adza"
  ]
  node [
    id 330
    label "posiada&#263;"
  ]
  node [
    id 331
    label "poci&#261;ga&#263;"
  ]
  node [
    id 332
    label "zbiera&#263;"
  ]
  node [
    id 333
    label "powodowa&#263;"
  ]
  node [
    id 334
    label "congregate"
  ]
  node [
    id 335
    label "organizowa&#263;"
  ]
  node [
    id 336
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 337
    label "czyni&#263;"
  ]
  node [
    id 338
    label "give"
  ]
  node [
    id 339
    label "stylizowa&#263;"
  ]
  node [
    id 340
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 341
    label "falowa&#263;"
  ]
  node [
    id 342
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 343
    label "peddle"
  ]
  node [
    id 344
    label "wydala&#263;"
  ]
  node [
    id 345
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 346
    label "tentegowa&#263;"
  ]
  node [
    id 347
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 348
    label "urz&#261;dza&#263;"
  ]
  node [
    id 349
    label "oszukiwa&#263;"
  ]
  node [
    id 350
    label "work"
  ]
  node [
    id 351
    label "ukazywa&#263;"
  ]
  node [
    id 352
    label "przerabia&#263;"
  ]
  node [
    id 353
    label "act"
  ]
  node [
    id 354
    label "post&#281;powa&#263;"
  ]
  node [
    id 355
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 356
    label "motywowa&#263;"
  ]
  node [
    id 357
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 358
    label "przejmowa&#263;"
  ]
  node [
    id 359
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 360
    label "bra&#263;"
  ]
  node [
    id 361
    label "pozyskiwa&#263;"
  ]
  node [
    id 362
    label "wzbiera&#263;"
  ]
  node [
    id 363
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 364
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 365
    label "meet"
  ]
  node [
    id 366
    label "dostawa&#263;"
  ]
  node [
    id 367
    label "consolidate"
  ]
  node [
    id 368
    label "umieszcza&#263;"
  ]
  node [
    id 369
    label "uk&#322;ada&#263;"
  ]
  node [
    id 370
    label "wiedzie&#263;"
  ]
  node [
    id 371
    label "zawiera&#263;"
  ]
  node [
    id 372
    label "mie&#263;"
  ]
  node [
    id 373
    label "support"
  ]
  node [
    id 374
    label "zdolno&#347;&#263;"
  ]
  node [
    id 375
    label "keep_open"
  ]
  node [
    id 376
    label "pull"
  ]
  node [
    id 377
    label "upija&#263;"
  ]
  node [
    id 378
    label "wsysa&#263;"
  ]
  node [
    id 379
    label "przechyla&#263;"
  ]
  node [
    id 380
    label "pokrywa&#263;"
  ]
  node [
    id 381
    label "rusza&#263;"
  ]
  node [
    id 382
    label "trail"
  ]
  node [
    id 383
    label "przesuwa&#263;"
  ]
  node [
    id 384
    label "skutkowa&#263;"
  ]
  node [
    id 385
    label "nos"
  ]
  node [
    id 386
    label "powiewa&#263;"
  ]
  node [
    id 387
    label "katar"
  ]
  node [
    id 388
    label "mani&#263;"
  ]
  node [
    id 389
    label "force"
  ]
  node [
    id 390
    label "nietuzinkowy"
  ]
  node [
    id 391
    label "cz&#322;owiek"
  ]
  node [
    id 392
    label "intryguj&#261;cy"
  ]
  node [
    id 393
    label "ch&#281;tny"
  ]
  node [
    id 394
    label "swoisty"
  ]
  node [
    id 395
    label "interesowanie"
  ]
  node [
    id 396
    label "dziwny"
  ]
  node [
    id 397
    label "interesuj&#261;cy"
  ]
  node [
    id 398
    label "ciekawie"
  ]
  node [
    id 399
    label "indagator"
  ]
  node [
    id 400
    label "interesuj&#261;co"
  ]
  node [
    id 401
    label "atrakcyjny"
  ]
  node [
    id 402
    label "intryguj&#261;co"
  ]
  node [
    id 403
    label "niespotykany"
  ]
  node [
    id 404
    label "nietuzinkowo"
  ]
  node [
    id 405
    label "ch&#281;tliwy"
  ]
  node [
    id 406
    label "ch&#281;tnie"
  ]
  node [
    id 407
    label "napalony"
  ]
  node [
    id 408
    label "chy&#380;y"
  ]
  node [
    id 409
    label "&#380;yczliwy"
  ]
  node [
    id 410
    label "przychylny"
  ]
  node [
    id 411
    label "gotowy"
  ]
  node [
    id 412
    label "ludzko&#347;&#263;"
  ]
  node [
    id 413
    label "asymilowanie"
  ]
  node [
    id 414
    label "wapniak"
  ]
  node [
    id 415
    label "asymilowa&#263;"
  ]
  node [
    id 416
    label "os&#322;abia&#263;"
  ]
  node [
    id 417
    label "posta&#263;"
  ]
  node [
    id 418
    label "hominid"
  ]
  node [
    id 419
    label "podw&#322;adny"
  ]
  node [
    id 420
    label "os&#322;abianie"
  ]
  node [
    id 421
    label "g&#322;owa"
  ]
  node [
    id 422
    label "portrecista"
  ]
  node [
    id 423
    label "dwun&#243;g"
  ]
  node [
    id 424
    label "profanum"
  ]
  node [
    id 425
    label "mikrokosmos"
  ]
  node [
    id 426
    label "nasada"
  ]
  node [
    id 427
    label "duch"
  ]
  node [
    id 428
    label "antropochoria"
  ]
  node [
    id 429
    label "osoba"
  ]
  node [
    id 430
    label "wz&#243;r"
  ]
  node [
    id 431
    label "senior"
  ]
  node [
    id 432
    label "oddzia&#322;ywanie"
  ]
  node [
    id 433
    label "Adam"
  ]
  node [
    id 434
    label "homo_sapiens"
  ]
  node [
    id 435
    label "polifag"
  ]
  node [
    id 436
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 437
    label "odr&#281;bny"
  ]
  node [
    id 438
    label "swoi&#347;cie"
  ]
  node [
    id 439
    label "dziwnie"
  ]
  node [
    id 440
    label "dziwy"
  ]
  node [
    id 441
    label "inny"
  ]
  node [
    id 442
    label "dobrze"
  ]
  node [
    id 443
    label "occupation"
  ]
  node [
    id 444
    label "bycie"
  ]
  node [
    id 445
    label "ciekawski"
  ]
  node [
    id 446
    label "publikacja"
  ]
  node [
    id 447
    label "wiedza"
  ]
  node [
    id 448
    label "obiega&#263;"
  ]
  node [
    id 449
    label "powzi&#281;cie"
  ]
  node [
    id 450
    label "dane"
  ]
  node [
    id 451
    label "obiegni&#281;cie"
  ]
  node [
    id 452
    label "sygna&#322;"
  ]
  node [
    id 453
    label "obieganie"
  ]
  node [
    id 454
    label "powzi&#261;&#263;"
  ]
  node [
    id 455
    label "obiec"
  ]
  node [
    id 456
    label "doj&#347;&#263;"
  ]
  node [
    id 457
    label "po&#322;o&#380;enie"
  ]
  node [
    id 458
    label "sprawa"
  ]
  node [
    id 459
    label "ust&#281;p"
  ]
  node [
    id 460
    label "plan"
  ]
  node [
    id 461
    label "obiekt_matematyczny"
  ]
  node [
    id 462
    label "problemat"
  ]
  node [
    id 463
    label "plamka"
  ]
  node [
    id 464
    label "stopie&#324;_pisma"
  ]
  node [
    id 465
    label "jednostka"
  ]
  node [
    id 466
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 467
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 468
    label "mark"
  ]
  node [
    id 469
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 470
    label "prosta"
  ]
  node [
    id 471
    label "problematyka"
  ]
  node [
    id 472
    label "obiekt"
  ]
  node [
    id 473
    label "zapunktowa&#263;"
  ]
  node [
    id 474
    label "podpunkt"
  ]
  node [
    id 475
    label "wojsko"
  ]
  node [
    id 476
    label "kres"
  ]
  node [
    id 477
    label "point"
  ]
  node [
    id 478
    label "pozycja"
  ]
  node [
    id 479
    label "cognition"
  ]
  node [
    id 480
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 481
    label "intelekt"
  ]
  node [
    id 482
    label "pozwolenie"
  ]
  node [
    id 483
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 484
    label "zaawansowanie"
  ]
  node [
    id 485
    label "wykszta&#322;cenie"
  ]
  node [
    id 486
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 487
    label "przekazywa&#263;"
  ]
  node [
    id 488
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 489
    label "pulsation"
  ]
  node [
    id 490
    label "przekazywanie"
  ]
  node [
    id 491
    label "przewodzenie"
  ]
  node [
    id 492
    label "d&#378;wi&#281;k"
  ]
  node [
    id 493
    label "po&#322;&#261;czenie"
  ]
  node [
    id 494
    label "fala"
  ]
  node [
    id 495
    label "przekazanie"
  ]
  node [
    id 496
    label "przewodzi&#263;"
  ]
  node [
    id 497
    label "zapowied&#378;"
  ]
  node [
    id 498
    label "medium_transmisyjne"
  ]
  node [
    id 499
    label "demodulacja"
  ]
  node [
    id 500
    label "przekaza&#263;"
  ]
  node [
    id 501
    label "czynnik"
  ]
  node [
    id 502
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 503
    label "aliasing"
  ]
  node [
    id 504
    label "wizja"
  ]
  node [
    id 505
    label "modulacja"
  ]
  node [
    id 506
    label "drift"
  ]
  node [
    id 507
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 508
    label "druk"
  ]
  node [
    id 509
    label "produkcja"
  ]
  node [
    id 510
    label "notification"
  ]
  node [
    id 511
    label "edytowa&#263;"
  ]
  node [
    id 512
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 513
    label "spakowanie"
  ]
  node [
    id 514
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 515
    label "pakowa&#263;"
  ]
  node [
    id 516
    label "rekord"
  ]
  node [
    id 517
    label "korelator"
  ]
  node [
    id 518
    label "wyci&#261;ganie"
  ]
  node [
    id 519
    label "pakowanie"
  ]
  node [
    id 520
    label "sekwencjonowa&#263;"
  ]
  node [
    id 521
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 522
    label "jednostka_informacji"
  ]
  node [
    id 523
    label "evidence"
  ]
  node [
    id 524
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 525
    label "rozpakowywanie"
  ]
  node [
    id 526
    label "rozpakowanie"
  ]
  node [
    id 527
    label "rozpakowywa&#263;"
  ]
  node [
    id 528
    label "konwersja"
  ]
  node [
    id 529
    label "nap&#322;ywanie"
  ]
  node [
    id 530
    label "rozpakowa&#263;"
  ]
  node [
    id 531
    label "spakowa&#263;"
  ]
  node [
    id 532
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 533
    label "edytowanie"
  ]
  node [
    id 534
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 535
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 536
    label "sekwencjonowanie"
  ]
  node [
    id 537
    label "flow"
  ]
  node [
    id 538
    label "odwiedza&#263;"
  ]
  node [
    id 539
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 540
    label "rotate"
  ]
  node [
    id 541
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 542
    label "authorize"
  ]
  node [
    id 543
    label "podj&#261;&#263;"
  ]
  node [
    id 544
    label "zacz&#261;&#263;"
  ]
  node [
    id 545
    label "otrzyma&#263;"
  ]
  node [
    id 546
    label "sta&#263;_si&#281;"
  ]
  node [
    id 547
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 548
    label "supervene"
  ]
  node [
    id 549
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 550
    label "zaj&#347;&#263;"
  ]
  node [
    id 551
    label "catch"
  ]
  node [
    id 552
    label "bodziec"
  ]
  node [
    id 553
    label "przesy&#322;ka"
  ]
  node [
    id 554
    label "dodatek"
  ]
  node [
    id 555
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 556
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 557
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 558
    label "heed"
  ]
  node [
    id 559
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 560
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 561
    label "spowodowa&#263;"
  ]
  node [
    id 562
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 563
    label "dozna&#263;"
  ]
  node [
    id 564
    label "dokoptowa&#263;"
  ]
  node [
    id 565
    label "postrzega&#263;"
  ]
  node [
    id 566
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 567
    label "orgazm"
  ]
  node [
    id 568
    label "dolecie&#263;"
  ]
  node [
    id 569
    label "drive"
  ]
  node [
    id 570
    label "dotrze&#263;"
  ]
  node [
    id 571
    label "uzyska&#263;"
  ]
  node [
    id 572
    label "dop&#322;ata"
  ]
  node [
    id 573
    label "become"
  ]
  node [
    id 574
    label "odwiedzi&#263;"
  ]
  node [
    id 575
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 576
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 577
    label "orb"
  ]
  node [
    id 578
    label "podj&#281;cie"
  ]
  node [
    id 579
    label "otrzymanie"
  ]
  node [
    id 580
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 581
    label "dochodzenie"
  ]
  node [
    id 582
    label "uzyskanie"
  ]
  node [
    id 583
    label "skill"
  ]
  node [
    id 584
    label "dochrapanie_si&#281;"
  ]
  node [
    id 585
    label "znajomo&#347;ci"
  ]
  node [
    id 586
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 587
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 588
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 589
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 590
    label "powi&#261;zanie"
  ]
  node [
    id 591
    label "entrance"
  ]
  node [
    id 592
    label "affiliation"
  ]
  node [
    id 593
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 594
    label "dor&#281;czenie"
  ]
  node [
    id 595
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 596
    label "spowodowanie"
  ]
  node [
    id 597
    label "dost&#281;p"
  ]
  node [
    id 598
    label "avenue"
  ]
  node [
    id 599
    label "postrzeganie"
  ]
  node [
    id 600
    label "doznanie"
  ]
  node [
    id 601
    label "dojrza&#322;y"
  ]
  node [
    id 602
    label "dojechanie"
  ]
  node [
    id 603
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 604
    label "ingress"
  ]
  node [
    id 605
    label "strzelenie"
  ]
  node [
    id 606
    label "orzekni&#281;cie"
  ]
  node [
    id 607
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 608
    label "dolecenie"
  ]
  node [
    id 609
    label "rozpowszechnienie"
  ]
  node [
    id 610
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 611
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 612
    label "stanie_si&#281;"
  ]
  node [
    id 613
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 614
    label "zrobienie"
  ]
  node [
    id 615
    label "odwiedzanie"
  ]
  node [
    id 616
    label "biegni&#281;cie"
  ]
  node [
    id 617
    label "zakre&#347;lanie"
  ]
  node [
    id 618
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 619
    label "okr&#261;&#380;anie"
  ]
  node [
    id 620
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 621
    label "zakre&#347;lenie"
  ]
  node [
    id 622
    label "odwiedzenie"
  ]
  node [
    id 623
    label "okr&#261;&#380;enie"
  ]
  node [
    id 624
    label "kszta&#322;t"
  ]
  node [
    id 625
    label "provider"
  ]
  node [
    id 626
    label "biznes_elektroniczny"
  ]
  node [
    id 627
    label "zasadzka"
  ]
  node [
    id 628
    label "mesh"
  ]
  node [
    id 629
    label "plecionka"
  ]
  node [
    id 630
    label "gauze"
  ]
  node [
    id 631
    label "web"
  ]
  node [
    id 632
    label "organizacja"
  ]
  node [
    id 633
    label "gra_sieciowa"
  ]
  node [
    id 634
    label "net"
  ]
  node [
    id 635
    label "media"
  ]
  node [
    id 636
    label "sie&#263;_komputerowa"
  ]
  node [
    id 637
    label "nitka"
  ]
  node [
    id 638
    label "snu&#263;"
  ]
  node [
    id 639
    label "vane"
  ]
  node [
    id 640
    label "instalacja"
  ]
  node [
    id 641
    label "wysnu&#263;"
  ]
  node [
    id 642
    label "organization"
  ]
  node [
    id 643
    label "us&#322;uga_internetowa"
  ]
  node [
    id 644
    label "rozmieszczenie"
  ]
  node [
    id 645
    label "podcast"
  ]
  node [
    id 646
    label "hipertekst"
  ]
  node [
    id 647
    label "cyberprzestrze&#324;"
  ]
  node [
    id 648
    label "mem"
  ]
  node [
    id 649
    label "grooming"
  ]
  node [
    id 650
    label "punkt_dost&#281;pu"
  ]
  node [
    id 651
    label "netbook"
  ]
  node [
    id 652
    label "e-hazard"
  ]
  node [
    id 653
    label "zastawia&#263;"
  ]
  node [
    id 654
    label "zastawi&#263;"
  ]
  node [
    id 655
    label "ambush"
  ]
  node [
    id 656
    label "atak"
  ]
  node [
    id 657
    label "podst&#281;p"
  ]
  node [
    id 658
    label "formacja"
  ]
  node [
    id 659
    label "punkt_widzenia"
  ]
  node [
    id 660
    label "wygl&#261;d"
  ]
  node [
    id 661
    label "spirala"
  ]
  node [
    id 662
    label "p&#322;at"
  ]
  node [
    id 663
    label "comeliness"
  ]
  node [
    id 664
    label "kielich"
  ]
  node [
    id 665
    label "face"
  ]
  node [
    id 666
    label "blaszka"
  ]
  node [
    id 667
    label "charakter"
  ]
  node [
    id 668
    label "p&#281;tla"
  ]
  node [
    id 669
    label "pasmo"
  ]
  node [
    id 670
    label "linearno&#347;&#263;"
  ]
  node [
    id 671
    label "gwiazda"
  ]
  node [
    id 672
    label "miniatura"
  ]
  node [
    id 673
    label "integer"
  ]
  node [
    id 674
    label "zlewanie_si&#281;"
  ]
  node [
    id 675
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 676
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 677
    label "pe&#322;ny"
  ]
  node [
    id 678
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 679
    label "u&#322;o&#380;enie"
  ]
  node [
    id 680
    label "porozmieszczanie"
  ]
  node [
    id 681
    label "wyst&#281;powanie"
  ]
  node [
    id 682
    label "layout"
  ]
  node [
    id 683
    label "o&#347;"
  ]
  node [
    id 684
    label "usenet"
  ]
  node [
    id 685
    label "rozprz&#261;c"
  ]
  node [
    id 686
    label "zachowanie"
  ]
  node [
    id 687
    label "cybernetyk"
  ]
  node [
    id 688
    label "podsystem"
  ]
  node [
    id 689
    label "system"
  ]
  node [
    id 690
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 691
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 692
    label "sk&#322;ad"
  ]
  node [
    id 693
    label "systemat"
  ]
  node [
    id 694
    label "konstelacja"
  ]
  node [
    id 695
    label "podmiot"
  ]
  node [
    id 696
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 697
    label "TOPR"
  ]
  node [
    id 698
    label "endecki"
  ]
  node [
    id 699
    label "przedstawicielstwo"
  ]
  node [
    id 700
    label "od&#322;am"
  ]
  node [
    id 701
    label "Cepelia"
  ]
  node [
    id 702
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 703
    label "ZBoWiD"
  ]
  node [
    id 704
    label "centrala"
  ]
  node [
    id 705
    label "GOPR"
  ]
  node [
    id 706
    label "ZOMO"
  ]
  node [
    id 707
    label "ZMP"
  ]
  node [
    id 708
    label "komitet_koordynacyjny"
  ]
  node [
    id 709
    label "przybud&#243;wka"
  ]
  node [
    id 710
    label "boj&#243;wka"
  ]
  node [
    id 711
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 712
    label "proces"
  ]
  node [
    id 713
    label "kompozycja"
  ]
  node [
    id 714
    label "uzbrajanie"
  ]
  node [
    id 715
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 716
    label "co&#347;"
  ]
  node [
    id 717
    label "budynek"
  ]
  node [
    id 718
    label "thing"
  ]
  node [
    id 719
    label "poj&#281;cie"
  ]
  node [
    id 720
    label "program"
  ]
  node [
    id 721
    label "rzecz"
  ]
  node [
    id 722
    label "mass-media"
  ]
  node [
    id 723
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 724
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 725
    label "przekazior"
  ]
  node [
    id 726
    label "medium"
  ]
  node [
    id 727
    label "ornament"
  ]
  node [
    id 728
    label "przedmiot"
  ]
  node [
    id 729
    label "splot"
  ]
  node [
    id 730
    label "braid"
  ]
  node [
    id 731
    label "szachulec"
  ]
  node [
    id 732
    label "b&#322;&#261;d"
  ]
  node [
    id 733
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 734
    label "nawijad&#322;o"
  ]
  node [
    id 735
    label "sznur"
  ]
  node [
    id 736
    label "motowid&#322;o"
  ]
  node [
    id 737
    label "makaron"
  ]
  node [
    id 738
    label "internet"
  ]
  node [
    id 739
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 740
    label "kartka"
  ]
  node [
    id 741
    label "logowanie"
  ]
  node [
    id 742
    label "plik"
  ]
  node [
    id 743
    label "s&#261;d"
  ]
  node [
    id 744
    label "adres_internetowy"
  ]
  node [
    id 745
    label "linia"
  ]
  node [
    id 746
    label "serwis_internetowy"
  ]
  node [
    id 747
    label "bok"
  ]
  node [
    id 748
    label "skr&#281;canie"
  ]
  node [
    id 749
    label "skr&#281;ca&#263;"
  ]
  node [
    id 750
    label "orientowanie"
  ]
  node [
    id 751
    label "skr&#281;ci&#263;"
  ]
  node [
    id 752
    label "uj&#281;cie"
  ]
  node [
    id 753
    label "zorientowanie"
  ]
  node [
    id 754
    label "fragment"
  ]
  node [
    id 755
    label "zorientowa&#263;"
  ]
  node [
    id 756
    label "pagina"
  ]
  node [
    id 757
    label "g&#243;ra"
  ]
  node [
    id 758
    label "orientowa&#263;"
  ]
  node [
    id 759
    label "voice"
  ]
  node [
    id 760
    label "orientacja"
  ]
  node [
    id 761
    label "powierzchnia"
  ]
  node [
    id 762
    label "forma"
  ]
  node [
    id 763
    label "skr&#281;cenie"
  ]
  node [
    id 764
    label "wyj&#261;&#263;"
  ]
  node [
    id 765
    label "stworzy&#263;"
  ]
  node [
    id 766
    label "zasadzi&#263;"
  ]
  node [
    id 767
    label "paj&#261;k"
  ]
  node [
    id 768
    label "devise"
  ]
  node [
    id 769
    label "wyjmowa&#263;"
  ]
  node [
    id 770
    label "project"
  ]
  node [
    id 771
    label "my&#347;le&#263;"
  ]
  node [
    id 772
    label "produkowa&#263;"
  ]
  node [
    id 773
    label "tworzy&#263;"
  ]
  node [
    id 774
    label "meme"
  ]
  node [
    id 775
    label "wydawnictwo"
  ]
  node [
    id 776
    label "molestowanie_seksualne"
  ]
  node [
    id 777
    label "piel&#281;gnacja"
  ]
  node [
    id 778
    label "zwierz&#281;_domowe"
  ]
  node [
    id 779
    label "hazard"
  ]
  node [
    id 780
    label "dostawca"
  ]
  node [
    id 781
    label "telefonia"
  ]
  node [
    id 782
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 783
    label "ma&#322;y"
  ]
  node [
    id 784
    label "message"
  ]
  node [
    id 785
    label "nius"
  ]
  node [
    id 786
    label "nowostka"
  ]
  node [
    id 787
    label "doniesienie"
  ]
  node [
    id 788
    label "do&#322;&#261;czenie"
  ]
  node [
    id 789
    label "naznoszenie"
  ]
  node [
    id 790
    label "zawiadomienie"
  ]
  node [
    id 791
    label "zniesienie"
  ]
  node [
    id 792
    label "zaniesienie"
  ]
  node [
    id 793
    label "announcement"
  ]
  node [
    id 794
    label "fetch"
  ]
  node [
    id 795
    label "poinformowanie"
  ]
  node [
    id 796
    label "znoszenie"
  ]
  node [
    id 797
    label "communication"
  ]
  node [
    id 798
    label "signal"
  ]
  node [
    id 799
    label "znosi&#263;"
  ]
  node [
    id 800
    label "znie&#347;&#263;"
  ]
  node [
    id 801
    label "zarys"
  ]
  node [
    id 802
    label "komunikat"
  ]
  node [
    id 803
    label "depesza_emska"
  ]
  node [
    id 804
    label "j&#261;dro"
  ]
  node [
    id 805
    label "systemik"
  ]
  node [
    id 806
    label "oprogramowanie"
  ]
  node [
    id 807
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 808
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 809
    label "model"
  ]
  node [
    id 810
    label "porz&#261;dek"
  ]
  node [
    id 811
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 812
    label "przyn&#281;ta"
  ]
  node [
    id 813
    label "p&#322;&#243;d"
  ]
  node [
    id 814
    label "w&#281;dkarstwo"
  ]
  node [
    id 815
    label "eratem"
  ]
  node [
    id 816
    label "oddzia&#322;"
  ]
  node [
    id 817
    label "doktryna"
  ]
  node [
    id 818
    label "pulpit"
  ]
  node [
    id 819
    label "jednostka_geologiczna"
  ]
  node [
    id 820
    label "metoda"
  ]
  node [
    id 821
    label "ryba"
  ]
  node [
    id 822
    label "Leopard"
  ]
  node [
    id 823
    label "spos&#243;b"
  ]
  node [
    id 824
    label "Android"
  ]
  node [
    id 825
    label "method"
  ]
  node [
    id 826
    label "podstawa"
  ]
  node [
    id 827
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 828
    label "nowina"
  ]
  node [
    id 829
    label "blok"
  ]
  node [
    id 830
    label "prawda"
  ]
  node [
    id 831
    label "znak_j&#281;zykowy"
  ]
  node [
    id 832
    label "nag&#322;&#243;wek"
  ]
  node [
    id 833
    label "szkic"
  ]
  node [
    id 834
    label "line"
  ]
  node [
    id 835
    label "wyr&#243;b"
  ]
  node [
    id 836
    label "rodzajnik"
  ]
  node [
    id 837
    label "dokument"
  ]
  node [
    id 838
    label "towar"
  ]
  node [
    id 839
    label "paragraf"
  ]
  node [
    id 840
    label "ekscerpcja"
  ]
  node [
    id 841
    label "j&#281;zykowo"
  ]
  node [
    id 842
    label "redakcja"
  ]
  node [
    id 843
    label "pomini&#281;cie"
  ]
  node [
    id 844
    label "dzie&#322;o"
  ]
  node [
    id 845
    label "preparacja"
  ]
  node [
    id 846
    label "odmianka"
  ]
  node [
    id 847
    label "opu&#347;ci&#263;"
  ]
  node [
    id 848
    label "koniektura"
  ]
  node [
    id 849
    label "pisa&#263;"
  ]
  node [
    id 850
    label "obelga"
  ]
  node [
    id 851
    label "utw&#243;r"
  ]
  node [
    id 852
    label "za&#322;o&#380;enie"
  ]
  node [
    id 853
    label "nieprawdziwy"
  ]
  node [
    id 854
    label "prawdziwy"
  ]
  node [
    id 855
    label "truth"
  ]
  node [
    id 856
    label "realia"
  ]
  node [
    id 857
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 858
    label "produkt"
  ]
  node [
    id 859
    label "creation"
  ]
  node [
    id 860
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 861
    label "p&#322;uczkarnia"
  ]
  node [
    id 862
    label "znakowarka"
  ]
  node [
    id 863
    label "tytu&#322;"
  ]
  node [
    id 864
    label "head"
  ]
  node [
    id 865
    label "znak_pisarski"
  ]
  node [
    id 866
    label "przepis"
  ]
  node [
    id 867
    label "bajt"
  ]
  node [
    id 868
    label "bloking"
  ]
  node [
    id 869
    label "j&#261;kanie"
  ]
  node [
    id 870
    label "przeszkoda"
  ]
  node [
    id 871
    label "blokada"
  ]
  node [
    id 872
    label "bry&#322;a"
  ]
  node [
    id 873
    label "dzia&#322;"
  ]
  node [
    id 874
    label "kontynent"
  ]
  node [
    id 875
    label "nastawnia"
  ]
  node [
    id 876
    label "blockage"
  ]
  node [
    id 877
    label "block"
  ]
  node [
    id 878
    label "start"
  ]
  node [
    id 879
    label "skorupa_ziemska"
  ]
  node [
    id 880
    label "zeszyt"
  ]
  node [
    id 881
    label "grupa"
  ]
  node [
    id 882
    label "blokowisko"
  ]
  node [
    id 883
    label "barak"
  ]
  node [
    id 884
    label "stok_kontynentalny"
  ]
  node [
    id 885
    label "square"
  ]
  node [
    id 886
    label "siatk&#243;wka"
  ]
  node [
    id 887
    label "kr&#261;g"
  ]
  node [
    id 888
    label "ram&#243;wka"
  ]
  node [
    id 889
    label "zamek"
  ]
  node [
    id 890
    label "obrona"
  ]
  node [
    id 891
    label "ok&#322;adka"
  ]
  node [
    id 892
    label "bie&#380;nia"
  ]
  node [
    id 893
    label "referat"
  ]
  node [
    id 894
    label "dom_wielorodzinny"
  ]
  node [
    id 895
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 896
    label "zapis"
  ]
  node [
    id 897
    label "&#347;wiadectwo"
  ]
  node [
    id 898
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 899
    label "parafa"
  ]
  node [
    id 900
    label "raport&#243;wka"
  ]
  node [
    id 901
    label "record"
  ]
  node [
    id 902
    label "fascyku&#322;"
  ]
  node [
    id 903
    label "dokumentacja"
  ]
  node [
    id 904
    label "registratura"
  ]
  node [
    id 905
    label "writing"
  ]
  node [
    id 906
    label "sygnatariusz"
  ]
  node [
    id 907
    label "rysunek"
  ]
  node [
    id 908
    label "szkicownik"
  ]
  node [
    id 909
    label "opracowanie"
  ]
  node [
    id 910
    label "sketch"
  ]
  node [
    id 911
    label "plot"
  ]
  node [
    id 912
    label "pomys&#322;"
  ]
  node [
    id 913
    label "opowiadanie"
  ]
  node [
    id 914
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 915
    label "metka"
  ]
  node [
    id 916
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 917
    label "szprycowa&#263;"
  ]
  node [
    id 918
    label "naszprycowa&#263;"
  ]
  node [
    id 919
    label "rzuca&#263;"
  ]
  node [
    id 920
    label "tandeta"
  ]
  node [
    id 921
    label "obr&#243;t_handlowy"
  ]
  node [
    id 922
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 923
    label "rzuci&#263;"
  ]
  node [
    id 924
    label "naszprycowanie"
  ]
  node [
    id 925
    label "tkanina"
  ]
  node [
    id 926
    label "szprycowanie"
  ]
  node [
    id 927
    label "za&#322;adownia"
  ]
  node [
    id 928
    label "asortyment"
  ]
  node [
    id 929
    label "&#322;&#243;dzki"
  ]
  node [
    id 930
    label "narkobiznes"
  ]
  node [
    id 931
    label "rzucenie"
  ]
  node [
    id 932
    label "rzucanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 9
    target 896
  ]
  edge [
    source 9
    target 897
  ]
  edge [
    source 9
    target 898
  ]
  edge [
    source 9
    target 899
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 900
  ]
  edge [
    source 9
    target 901
  ]
  edge [
    source 9
    target 902
  ]
  edge [
    source 9
    target 903
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 9
    target 906
  ]
  edge [
    source 9
    target 907
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 909
  ]
  edge [
    source 9
    target 910
  ]
  edge [
    source 9
    target 911
  ]
  edge [
    source 9
    target 912
  ]
  edge [
    source 9
    target 913
  ]
  edge [
    source 9
    target 914
  ]
  edge [
    source 9
    target 915
  ]
  edge [
    source 9
    target 916
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 932
  ]
]
