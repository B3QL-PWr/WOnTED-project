graph [
  node [
    id 0
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 1
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 2
    label "s&#261;d"
  ]
  node [
    id 3
    label "szko&#322;a"
  ]
  node [
    id 4
    label "wytw&#243;r"
  ]
  node [
    id 5
    label "p&#322;&#243;d"
  ]
  node [
    id 6
    label "thinking"
  ]
  node [
    id 7
    label "umys&#322;"
  ]
  node [
    id 8
    label "political_orientation"
  ]
  node [
    id 9
    label "istota"
  ]
  node [
    id 10
    label "pomys&#322;"
  ]
  node [
    id 11
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 12
    label "idea"
  ]
  node [
    id 13
    label "system"
  ]
  node [
    id 14
    label "fantomatyka"
  ]
  node [
    id 15
    label "pami&#281;&#263;"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "intelekt"
  ]
  node [
    id 18
    label "pomieszanie_si&#281;"
  ]
  node [
    id 19
    label "wn&#281;trze"
  ]
  node [
    id 20
    label "wyobra&#378;nia"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "work"
  ]
  node [
    id 23
    label "rezultat"
  ]
  node [
    id 24
    label "mentalno&#347;&#263;"
  ]
  node [
    id 25
    label "superego"
  ]
  node [
    id 26
    label "psychika"
  ]
  node [
    id 27
    label "znaczenie"
  ]
  node [
    id 28
    label "charakter"
  ]
  node [
    id 29
    label "cecha"
  ]
  node [
    id 30
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 31
    label "moczownik"
  ]
  node [
    id 32
    label "embryo"
  ]
  node [
    id 33
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 34
    label "zarodek"
  ]
  node [
    id 35
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 36
    label "latawiec"
  ]
  node [
    id 37
    label "j&#261;dro"
  ]
  node [
    id 38
    label "systemik"
  ]
  node [
    id 39
    label "rozprz&#261;c"
  ]
  node [
    id 40
    label "oprogramowanie"
  ]
  node [
    id 41
    label "poj&#281;cie"
  ]
  node [
    id 42
    label "systemat"
  ]
  node [
    id 43
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 44
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 45
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 46
    label "model"
  ]
  node [
    id 47
    label "struktura"
  ]
  node [
    id 48
    label "usenet"
  ]
  node [
    id 49
    label "zbi&#243;r"
  ]
  node [
    id 50
    label "porz&#261;dek"
  ]
  node [
    id 51
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 52
    label "przyn&#281;ta"
  ]
  node [
    id 53
    label "net"
  ]
  node [
    id 54
    label "w&#281;dkarstwo"
  ]
  node [
    id 55
    label "eratem"
  ]
  node [
    id 56
    label "oddzia&#322;"
  ]
  node [
    id 57
    label "doktryna"
  ]
  node [
    id 58
    label "pulpit"
  ]
  node [
    id 59
    label "konstelacja"
  ]
  node [
    id 60
    label "jednostka_geologiczna"
  ]
  node [
    id 61
    label "o&#347;"
  ]
  node [
    id 62
    label "podsystem"
  ]
  node [
    id 63
    label "metoda"
  ]
  node [
    id 64
    label "ryba"
  ]
  node [
    id 65
    label "Leopard"
  ]
  node [
    id 66
    label "spos&#243;b"
  ]
  node [
    id 67
    label "Android"
  ]
  node [
    id 68
    label "zachowanie"
  ]
  node [
    id 69
    label "cybernetyk"
  ]
  node [
    id 70
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 71
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 72
    label "method"
  ]
  node [
    id 73
    label "sk&#322;ad"
  ]
  node [
    id 74
    label "podstawa"
  ]
  node [
    id 75
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 76
    label "zesp&#243;&#322;"
  ]
  node [
    id 77
    label "podejrzany"
  ]
  node [
    id 78
    label "s&#261;downictwo"
  ]
  node [
    id 79
    label "biuro"
  ]
  node [
    id 80
    label "court"
  ]
  node [
    id 81
    label "forum"
  ]
  node [
    id 82
    label "bronienie"
  ]
  node [
    id 83
    label "urz&#261;d"
  ]
  node [
    id 84
    label "wydarzenie"
  ]
  node [
    id 85
    label "oskar&#380;yciel"
  ]
  node [
    id 86
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 87
    label "skazany"
  ]
  node [
    id 88
    label "post&#281;powanie"
  ]
  node [
    id 89
    label "broni&#263;"
  ]
  node [
    id 90
    label "pods&#261;dny"
  ]
  node [
    id 91
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 92
    label "obrona"
  ]
  node [
    id 93
    label "wypowied&#378;"
  ]
  node [
    id 94
    label "instytucja"
  ]
  node [
    id 95
    label "antylogizm"
  ]
  node [
    id 96
    label "konektyw"
  ]
  node [
    id 97
    label "&#347;wiadek"
  ]
  node [
    id 98
    label "procesowicz"
  ]
  node [
    id 99
    label "strona"
  ]
  node [
    id 100
    label "technika"
  ]
  node [
    id 101
    label "pocz&#261;tki"
  ]
  node [
    id 102
    label "ukradzenie"
  ]
  node [
    id 103
    label "ukra&#347;&#263;"
  ]
  node [
    id 104
    label "do&#347;wiadczenie"
  ]
  node [
    id 105
    label "teren_szko&#322;y"
  ]
  node [
    id 106
    label "wiedza"
  ]
  node [
    id 107
    label "Mickiewicz"
  ]
  node [
    id 108
    label "kwalifikacje"
  ]
  node [
    id 109
    label "podr&#281;cznik"
  ]
  node [
    id 110
    label "absolwent"
  ]
  node [
    id 111
    label "praktyka"
  ]
  node [
    id 112
    label "school"
  ]
  node [
    id 113
    label "zda&#263;"
  ]
  node [
    id 114
    label "gabinet"
  ]
  node [
    id 115
    label "urszulanki"
  ]
  node [
    id 116
    label "sztuba"
  ]
  node [
    id 117
    label "&#322;awa_szkolna"
  ]
  node [
    id 118
    label "nauka"
  ]
  node [
    id 119
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 120
    label "przepisa&#263;"
  ]
  node [
    id 121
    label "muzyka"
  ]
  node [
    id 122
    label "grupa"
  ]
  node [
    id 123
    label "form"
  ]
  node [
    id 124
    label "klasa"
  ]
  node [
    id 125
    label "lekcja"
  ]
  node [
    id 126
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 127
    label "przepisanie"
  ]
  node [
    id 128
    label "czas"
  ]
  node [
    id 129
    label "skolaryzacja"
  ]
  node [
    id 130
    label "zdanie"
  ]
  node [
    id 131
    label "stopek"
  ]
  node [
    id 132
    label "sekretariat"
  ]
  node [
    id 133
    label "ideologia"
  ]
  node [
    id 134
    label "lesson"
  ]
  node [
    id 135
    label "niepokalanki"
  ]
  node [
    id 136
    label "siedziba"
  ]
  node [
    id 137
    label "szkolenie"
  ]
  node [
    id 138
    label "kara"
  ]
  node [
    id 139
    label "tablica"
  ]
  node [
    id 140
    label "byt"
  ]
  node [
    id 141
    label "Kant"
  ]
  node [
    id 142
    label "cel"
  ]
  node [
    id 143
    label "ideacja"
  ]
  node [
    id 144
    label "Irokezi"
  ]
  node [
    id 145
    label "ludno&#347;&#263;"
  ]
  node [
    id 146
    label "Syngalezi"
  ]
  node [
    id 147
    label "Apacze"
  ]
  node [
    id 148
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 149
    label "t&#322;um"
  ]
  node [
    id 150
    label "Mohikanie"
  ]
  node [
    id 151
    label "Komancze"
  ]
  node [
    id 152
    label "lud"
  ]
  node [
    id 153
    label "Samojedzi"
  ]
  node [
    id 154
    label "Siuksowie"
  ]
  node [
    id 155
    label "Buriaci"
  ]
  node [
    id 156
    label "Czejenowie"
  ]
  node [
    id 157
    label "Wotiacy"
  ]
  node [
    id 158
    label "Aztekowie"
  ]
  node [
    id 159
    label "nacja"
  ]
  node [
    id 160
    label "Baszkirzy"
  ]
  node [
    id 161
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 162
    label "innowierstwo"
  ]
  node [
    id 163
    label "ch&#322;opstwo"
  ]
  node [
    id 164
    label "demofobia"
  ]
  node [
    id 165
    label "najazd"
  ]
  node [
    id 166
    label "Tagalowie"
  ]
  node [
    id 167
    label "Ugrowie"
  ]
  node [
    id 168
    label "Retowie"
  ]
  node [
    id 169
    label "Negryci"
  ]
  node [
    id 170
    label "Ladynowie"
  ]
  node [
    id 171
    label "Wizygoci"
  ]
  node [
    id 172
    label "Dogonowie"
  ]
  node [
    id 173
    label "chamstwo"
  ]
  node [
    id 174
    label "Do&#322;ganie"
  ]
  node [
    id 175
    label "Indoira&#324;czycy"
  ]
  node [
    id 176
    label "gmin"
  ]
  node [
    id 177
    label "Kozacy"
  ]
  node [
    id 178
    label "Indoariowie"
  ]
  node [
    id 179
    label "Maroni"
  ]
  node [
    id 180
    label "Po&#322;owcy"
  ]
  node [
    id 181
    label "Kumbrowie"
  ]
  node [
    id 182
    label "Nogajowie"
  ]
  node [
    id 183
    label "Nawahowie"
  ]
  node [
    id 184
    label "Wenedowie"
  ]
  node [
    id 185
    label "Majowie"
  ]
  node [
    id 186
    label "Kipczacy"
  ]
  node [
    id 187
    label "Frygijczycy"
  ]
  node [
    id 188
    label "Paleoazjaci"
  ]
  node [
    id 189
    label "Tocharowie"
  ]
  node [
    id 190
    label "etnogeneza"
  ]
  node [
    id 191
    label "nationality"
  ]
  node [
    id 192
    label "Sri_Lanka"
  ]
  node [
    id 193
    label "Mongolia"
  ]
  node [
    id 194
    label "XD"
  ]
  node [
    id 195
    label "ojeba&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 194
    target 195
  ]
]
