graph [
  node [
    id 0
    label "kleje&#324;"
    origin "text"
  ]
  node [
    id 1
    label "opona"
    origin "text"
  ]
  node [
    id 2
    label "ciecz"
  ]
  node [
    id 3
    label "tkanka_nowotworowa"
  ]
  node [
    id 4
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 5
    label "ciek&#322;y"
  ]
  node [
    id 6
    label "podbiec"
  ]
  node [
    id 7
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 8
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 9
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 10
    label "baniak"
  ]
  node [
    id 11
    label "podbiega&#263;"
  ]
  node [
    id 12
    label "nieprzejrzysty"
  ]
  node [
    id 13
    label "wpadanie"
  ]
  node [
    id 14
    label "chlupa&#263;"
  ]
  node [
    id 15
    label "p&#322;ywa&#263;"
  ]
  node [
    id 16
    label "stan_skupienia"
  ]
  node [
    id 17
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 18
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 19
    label "odp&#322;ywanie"
  ]
  node [
    id 20
    label "zachlupa&#263;"
  ]
  node [
    id 21
    label "wpadni&#281;cie"
  ]
  node [
    id 22
    label "cia&#322;o"
  ]
  node [
    id 23
    label "wytoczenie"
  ]
  node [
    id 24
    label "substancja"
  ]
  node [
    id 25
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 26
    label "b&#322;ona"
  ]
  node [
    id 27
    label "wa&#322;ek"
  ]
  node [
    id 28
    label "bie&#380;nik"
  ]
  node [
    id 29
    label "ogumienie"
  ]
  node [
    id 30
    label "meninx"
  ]
  node [
    id 31
    label "ko&#322;o"
  ]
  node [
    id 32
    label "m&#243;zgoczaszka"
  ]
  node [
    id 33
    label "wytw&#243;r"
  ]
  node [
    id 34
    label "tkanka"
  ]
  node [
    id 35
    label "fa&#322;da"
  ]
  node [
    id 36
    label "post&#281;pek"
  ]
  node [
    id 37
    label "walec"
  ]
  node [
    id 38
    label "p&#243;&#322;wa&#322;ek"
  ]
  node [
    id 39
    label "wa&#322;kowanie"
  ]
  node [
    id 40
    label "wy&#380;ymaczka"
  ]
  node [
    id 41
    label "chutzpa"
  ]
  node [
    id 42
    label "cylinder"
  ]
  node [
    id 43
    label "poduszka"
  ]
  node [
    id 44
    label "maszyna"
  ]
  node [
    id 45
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 46
    label "narz&#281;dzie"
  ]
  node [
    id 47
    label "przewa&#322;"
  ]
  node [
    id 48
    label "blok"
  ]
  node [
    id 49
    label "serweta"
  ]
  node [
    id 50
    label "lina"
  ]
  node [
    id 51
    label "powierzchnia"
  ]
  node [
    id 52
    label "urz&#261;dzenie"
  ]
  node [
    id 53
    label "chodnik"
  ]
  node [
    id 54
    label "d&#281;tka"
  ]
  node [
    id 55
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 56
    label "odcinek_ko&#322;a"
  ]
  node [
    id 57
    label "zabawa"
  ]
  node [
    id 58
    label "&#322;ama&#263;"
  ]
  node [
    id 59
    label "sphere"
  ]
  node [
    id 60
    label "gang"
  ]
  node [
    id 61
    label "p&#243;&#322;kole"
  ]
  node [
    id 62
    label "stowarzyszenie"
  ]
  node [
    id 63
    label "zwolnica"
  ]
  node [
    id 64
    label "przedmiot"
  ]
  node [
    id 65
    label "figura_ograniczona"
  ]
  node [
    id 66
    label "podwozie"
  ]
  node [
    id 67
    label "sejmik"
  ]
  node [
    id 68
    label "obr&#281;cz"
  ]
  node [
    id 69
    label "lap"
  ]
  node [
    id 70
    label "pi"
  ]
  node [
    id 71
    label "kolokwium"
  ]
  node [
    id 72
    label "okr&#261;g"
  ]
  node [
    id 73
    label "figura_geometryczna"
  ]
  node [
    id 74
    label "grupa"
  ]
  node [
    id 75
    label "o&#347;"
  ]
  node [
    id 76
    label "&#322;amanie"
  ]
  node [
    id 77
    label "piasta"
  ]
  node [
    id 78
    label "whip"
  ]
  node [
    id 79
    label "pojazd"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
]
