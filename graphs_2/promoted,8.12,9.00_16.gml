graph [
  node [
    id 0
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 1
    label "usa"
    origin "text"
  ]
  node [
    id 2
    label "today"
    origin "text"
  ]
  node [
    id 3
    label "temat"
    origin "text"
  ]
  node [
    id 4
    label "film"
    origin "text"
  ]
  node [
    id 5
    label "lista"
    origin "text"
  ]
  node [
    id 6
    label "schindler"
    origin "text"
  ]
  node [
    id 7
    label "nim"
    origin "text"
  ]
  node [
    id 8
    label "polski"
    origin "text"
  ]
  node [
    id 9
    label "ob&#243;z"
    origin "text"
  ]
  node [
    id 10
    label "zag&#322;ada"
    origin "text"
  ]
  node [
    id 11
    label "bohater"
    origin "text"
  ]
  node [
    id 12
    label "niemcy"
    origin "text"
  ]
  node [
    id 13
    label "blok"
  ]
  node [
    id 14
    label "prawda"
  ]
  node [
    id 15
    label "znak_j&#281;zykowy"
  ]
  node [
    id 16
    label "nag&#322;&#243;wek"
  ]
  node [
    id 17
    label "szkic"
  ]
  node [
    id 18
    label "line"
  ]
  node [
    id 19
    label "fragment"
  ]
  node [
    id 20
    label "tekst"
  ]
  node [
    id 21
    label "wyr&#243;b"
  ]
  node [
    id 22
    label "rodzajnik"
  ]
  node [
    id 23
    label "dokument"
  ]
  node [
    id 24
    label "towar"
  ]
  node [
    id 25
    label "paragraf"
  ]
  node [
    id 26
    label "ekscerpcja"
  ]
  node [
    id 27
    label "j&#281;zykowo"
  ]
  node [
    id 28
    label "wypowied&#378;"
  ]
  node [
    id 29
    label "redakcja"
  ]
  node [
    id 30
    label "wytw&#243;r"
  ]
  node [
    id 31
    label "pomini&#281;cie"
  ]
  node [
    id 32
    label "dzie&#322;o"
  ]
  node [
    id 33
    label "preparacja"
  ]
  node [
    id 34
    label "odmianka"
  ]
  node [
    id 35
    label "opu&#347;ci&#263;"
  ]
  node [
    id 36
    label "koniektura"
  ]
  node [
    id 37
    label "pisa&#263;"
  ]
  node [
    id 38
    label "obelga"
  ]
  node [
    id 39
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 40
    label "utw&#243;r"
  ]
  node [
    id 41
    label "s&#261;d"
  ]
  node [
    id 42
    label "za&#322;o&#380;enie"
  ]
  node [
    id 43
    label "nieprawdziwy"
  ]
  node [
    id 44
    label "prawdziwy"
  ]
  node [
    id 45
    label "truth"
  ]
  node [
    id 46
    label "realia"
  ]
  node [
    id 47
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 48
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 49
    label "produkt"
  ]
  node [
    id 50
    label "creation"
  ]
  node [
    id 51
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 52
    label "p&#322;uczkarnia"
  ]
  node [
    id 53
    label "znakowarka"
  ]
  node [
    id 54
    label "produkcja"
  ]
  node [
    id 55
    label "tytu&#322;"
  ]
  node [
    id 56
    label "head"
  ]
  node [
    id 57
    label "znak_pisarski"
  ]
  node [
    id 58
    label "przepis"
  ]
  node [
    id 59
    label "bajt"
  ]
  node [
    id 60
    label "bloking"
  ]
  node [
    id 61
    label "j&#261;kanie"
  ]
  node [
    id 62
    label "przeszkoda"
  ]
  node [
    id 63
    label "zesp&#243;&#322;"
  ]
  node [
    id 64
    label "blokada"
  ]
  node [
    id 65
    label "bry&#322;a"
  ]
  node [
    id 66
    label "dzia&#322;"
  ]
  node [
    id 67
    label "kontynent"
  ]
  node [
    id 68
    label "nastawnia"
  ]
  node [
    id 69
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 70
    label "blockage"
  ]
  node [
    id 71
    label "zbi&#243;r"
  ]
  node [
    id 72
    label "block"
  ]
  node [
    id 73
    label "organizacja"
  ]
  node [
    id 74
    label "budynek"
  ]
  node [
    id 75
    label "start"
  ]
  node [
    id 76
    label "skorupa_ziemska"
  ]
  node [
    id 77
    label "program"
  ]
  node [
    id 78
    label "zeszyt"
  ]
  node [
    id 79
    label "grupa"
  ]
  node [
    id 80
    label "blokowisko"
  ]
  node [
    id 81
    label "barak"
  ]
  node [
    id 82
    label "stok_kontynentalny"
  ]
  node [
    id 83
    label "whole"
  ]
  node [
    id 84
    label "square"
  ]
  node [
    id 85
    label "siatk&#243;wka"
  ]
  node [
    id 86
    label "kr&#261;g"
  ]
  node [
    id 87
    label "ram&#243;wka"
  ]
  node [
    id 88
    label "zamek"
  ]
  node [
    id 89
    label "obrona"
  ]
  node [
    id 90
    label "ok&#322;adka"
  ]
  node [
    id 91
    label "bie&#380;nia"
  ]
  node [
    id 92
    label "referat"
  ]
  node [
    id 93
    label "dom_wielorodzinny"
  ]
  node [
    id 94
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 95
    label "zapis"
  ]
  node [
    id 96
    label "&#347;wiadectwo"
  ]
  node [
    id 97
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 98
    label "parafa"
  ]
  node [
    id 99
    label "plik"
  ]
  node [
    id 100
    label "raport&#243;wka"
  ]
  node [
    id 101
    label "record"
  ]
  node [
    id 102
    label "registratura"
  ]
  node [
    id 103
    label "dokumentacja"
  ]
  node [
    id 104
    label "fascyku&#322;"
  ]
  node [
    id 105
    label "writing"
  ]
  node [
    id 106
    label "sygnatariusz"
  ]
  node [
    id 107
    label "rysunek"
  ]
  node [
    id 108
    label "szkicownik"
  ]
  node [
    id 109
    label "opracowanie"
  ]
  node [
    id 110
    label "sketch"
  ]
  node [
    id 111
    label "plot"
  ]
  node [
    id 112
    label "pomys&#322;"
  ]
  node [
    id 113
    label "opowiadanie"
  ]
  node [
    id 114
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 115
    label "metka"
  ]
  node [
    id 116
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 117
    label "cz&#322;owiek"
  ]
  node [
    id 118
    label "szprycowa&#263;"
  ]
  node [
    id 119
    label "naszprycowa&#263;"
  ]
  node [
    id 120
    label "rzuca&#263;"
  ]
  node [
    id 121
    label "tandeta"
  ]
  node [
    id 122
    label "obr&#243;t_handlowy"
  ]
  node [
    id 123
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 124
    label "rzuci&#263;"
  ]
  node [
    id 125
    label "naszprycowanie"
  ]
  node [
    id 126
    label "tkanina"
  ]
  node [
    id 127
    label "szprycowanie"
  ]
  node [
    id 128
    label "za&#322;adownia"
  ]
  node [
    id 129
    label "asortyment"
  ]
  node [
    id 130
    label "&#322;&#243;dzki"
  ]
  node [
    id 131
    label "narkobiznes"
  ]
  node [
    id 132
    label "rzucenie"
  ]
  node [
    id 133
    label "rzucanie"
  ]
  node [
    id 134
    label "sprawa"
  ]
  node [
    id 135
    label "wyraz_pochodny"
  ]
  node [
    id 136
    label "zboczenie"
  ]
  node [
    id 137
    label "om&#243;wienie"
  ]
  node [
    id 138
    label "cecha"
  ]
  node [
    id 139
    label "rzecz"
  ]
  node [
    id 140
    label "omawia&#263;"
  ]
  node [
    id 141
    label "fraza"
  ]
  node [
    id 142
    label "tre&#347;&#263;"
  ]
  node [
    id 143
    label "entity"
  ]
  node [
    id 144
    label "forum"
  ]
  node [
    id 145
    label "topik"
  ]
  node [
    id 146
    label "tematyka"
  ]
  node [
    id 147
    label "w&#261;tek"
  ]
  node [
    id 148
    label "zbaczanie"
  ]
  node [
    id 149
    label "forma"
  ]
  node [
    id 150
    label "om&#243;wi&#263;"
  ]
  node [
    id 151
    label "omawianie"
  ]
  node [
    id 152
    label "melodia"
  ]
  node [
    id 153
    label "otoczka"
  ]
  node [
    id 154
    label "istota"
  ]
  node [
    id 155
    label "zbacza&#263;"
  ]
  node [
    id 156
    label "zboczy&#263;"
  ]
  node [
    id 157
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 158
    label "wypowiedzenie"
  ]
  node [
    id 159
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 160
    label "zdanie"
  ]
  node [
    id 161
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 162
    label "motyw"
  ]
  node [
    id 163
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 164
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 165
    label "informacja"
  ]
  node [
    id 166
    label "zawarto&#347;&#263;"
  ]
  node [
    id 167
    label "kszta&#322;t"
  ]
  node [
    id 168
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 169
    label "jednostka_systematyczna"
  ]
  node [
    id 170
    label "poznanie"
  ]
  node [
    id 171
    label "leksem"
  ]
  node [
    id 172
    label "stan"
  ]
  node [
    id 173
    label "blaszka"
  ]
  node [
    id 174
    label "poj&#281;cie"
  ]
  node [
    id 175
    label "kantyzm"
  ]
  node [
    id 176
    label "zdolno&#347;&#263;"
  ]
  node [
    id 177
    label "do&#322;ek"
  ]
  node [
    id 178
    label "gwiazda"
  ]
  node [
    id 179
    label "formality"
  ]
  node [
    id 180
    label "struktura"
  ]
  node [
    id 181
    label "wygl&#261;d"
  ]
  node [
    id 182
    label "mode"
  ]
  node [
    id 183
    label "morfem"
  ]
  node [
    id 184
    label "rdze&#324;"
  ]
  node [
    id 185
    label "posta&#263;"
  ]
  node [
    id 186
    label "kielich"
  ]
  node [
    id 187
    label "ornamentyka"
  ]
  node [
    id 188
    label "pasmo"
  ]
  node [
    id 189
    label "zwyczaj"
  ]
  node [
    id 190
    label "punkt_widzenia"
  ]
  node [
    id 191
    label "g&#322;owa"
  ]
  node [
    id 192
    label "naczynie"
  ]
  node [
    id 193
    label "p&#322;at"
  ]
  node [
    id 194
    label "maszyna_drukarska"
  ]
  node [
    id 195
    label "obiekt"
  ]
  node [
    id 196
    label "style"
  ]
  node [
    id 197
    label "linearno&#347;&#263;"
  ]
  node [
    id 198
    label "wyra&#380;enie"
  ]
  node [
    id 199
    label "formacja"
  ]
  node [
    id 200
    label "spirala"
  ]
  node [
    id 201
    label "dyspozycja"
  ]
  node [
    id 202
    label "odmiana"
  ]
  node [
    id 203
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 204
    label "wz&#243;r"
  ]
  node [
    id 205
    label "October"
  ]
  node [
    id 206
    label "p&#281;tla"
  ]
  node [
    id 207
    label "arystotelizm"
  ]
  node [
    id 208
    label "szablon"
  ]
  node [
    id 209
    label "miniatura"
  ]
  node [
    id 210
    label "zanucenie"
  ]
  node [
    id 211
    label "nuta"
  ]
  node [
    id 212
    label "zakosztowa&#263;"
  ]
  node [
    id 213
    label "zajawka"
  ]
  node [
    id 214
    label "zanuci&#263;"
  ]
  node [
    id 215
    label "emocja"
  ]
  node [
    id 216
    label "oskoma"
  ]
  node [
    id 217
    label "melika"
  ]
  node [
    id 218
    label "nucenie"
  ]
  node [
    id 219
    label "nuci&#263;"
  ]
  node [
    id 220
    label "brzmienie"
  ]
  node [
    id 221
    label "zjawisko"
  ]
  node [
    id 222
    label "taste"
  ]
  node [
    id 223
    label "muzyka"
  ]
  node [
    id 224
    label "inclination"
  ]
  node [
    id 225
    label "charakterystyka"
  ]
  node [
    id 226
    label "m&#322;ot"
  ]
  node [
    id 227
    label "znak"
  ]
  node [
    id 228
    label "drzewo"
  ]
  node [
    id 229
    label "pr&#243;ba"
  ]
  node [
    id 230
    label "attribute"
  ]
  node [
    id 231
    label "marka"
  ]
  node [
    id 232
    label "matter"
  ]
  node [
    id 233
    label "splot"
  ]
  node [
    id 234
    label "ceg&#322;a"
  ]
  node [
    id 235
    label "socket"
  ]
  node [
    id 236
    label "rozmieszczenie"
  ]
  node [
    id 237
    label "fabu&#322;a"
  ]
  node [
    id 238
    label "mentalno&#347;&#263;"
  ]
  node [
    id 239
    label "superego"
  ]
  node [
    id 240
    label "psychika"
  ]
  node [
    id 241
    label "znaczenie"
  ]
  node [
    id 242
    label "wn&#281;trze"
  ]
  node [
    id 243
    label "charakter"
  ]
  node [
    id 244
    label "okrywa"
  ]
  node [
    id 245
    label "kontekst"
  ]
  node [
    id 246
    label "object"
  ]
  node [
    id 247
    label "przedmiot"
  ]
  node [
    id 248
    label "wpadni&#281;cie"
  ]
  node [
    id 249
    label "mienie"
  ]
  node [
    id 250
    label "przyroda"
  ]
  node [
    id 251
    label "kultura"
  ]
  node [
    id 252
    label "wpa&#347;&#263;"
  ]
  node [
    id 253
    label "wpadanie"
  ]
  node [
    id 254
    label "wpada&#263;"
  ]
  node [
    id 255
    label "discussion"
  ]
  node [
    id 256
    label "rozpatrywanie"
  ]
  node [
    id 257
    label "dyskutowanie"
  ]
  node [
    id 258
    label "swerve"
  ]
  node [
    id 259
    label "kierunek"
  ]
  node [
    id 260
    label "digress"
  ]
  node [
    id 261
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 262
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 263
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 264
    label "odchodzi&#263;"
  ]
  node [
    id 265
    label "twist"
  ]
  node [
    id 266
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 267
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 268
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 269
    label "omowny"
  ]
  node [
    id 270
    label "figura_stylistyczna"
  ]
  node [
    id 271
    label "sformu&#322;owanie"
  ]
  node [
    id 272
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 273
    label "odchodzenie"
  ]
  node [
    id 274
    label "aberrance"
  ]
  node [
    id 275
    label "dyskutowa&#263;"
  ]
  node [
    id 276
    label "formu&#322;owa&#263;"
  ]
  node [
    id 277
    label "discourse"
  ]
  node [
    id 278
    label "perversion"
  ]
  node [
    id 279
    label "death"
  ]
  node [
    id 280
    label "odej&#347;cie"
  ]
  node [
    id 281
    label "turn"
  ]
  node [
    id 282
    label "k&#261;t"
  ]
  node [
    id 283
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 284
    label "odchylenie_si&#281;"
  ]
  node [
    id 285
    label "deviation"
  ]
  node [
    id 286
    label "patologia"
  ]
  node [
    id 287
    label "przedyskutowa&#263;"
  ]
  node [
    id 288
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 289
    label "publicize"
  ]
  node [
    id 290
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 291
    label "distract"
  ]
  node [
    id 292
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 293
    label "odej&#347;&#263;"
  ]
  node [
    id 294
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 295
    label "zmieni&#263;"
  ]
  node [
    id 296
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 297
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 298
    label "kognicja"
  ]
  node [
    id 299
    label "rozprawa"
  ]
  node [
    id 300
    label "wydarzenie"
  ]
  node [
    id 301
    label "szczeg&#243;&#322;"
  ]
  node [
    id 302
    label "proposition"
  ]
  node [
    id 303
    label "przes&#322;anka"
  ]
  node [
    id 304
    label "idea"
  ]
  node [
    id 305
    label "paj&#261;k"
  ]
  node [
    id 306
    label "przewodnik"
  ]
  node [
    id 307
    label "odcinek"
  ]
  node [
    id 308
    label "topikowate"
  ]
  node [
    id 309
    label "grupa_dyskusyjna"
  ]
  node [
    id 310
    label "plac"
  ]
  node [
    id 311
    label "bazylika"
  ]
  node [
    id 312
    label "przestrze&#324;"
  ]
  node [
    id 313
    label "miejsce"
  ]
  node [
    id 314
    label "portal"
  ]
  node [
    id 315
    label "konferencja"
  ]
  node [
    id 316
    label "agora"
  ]
  node [
    id 317
    label "strona"
  ]
  node [
    id 318
    label "animatronika"
  ]
  node [
    id 319
    label "odczulenie"
  ]
  node [
    id 320
    label "odczula&#263;"
  ]
  node [
    id 321
    label "blik"
  ]
  node [
    id 322
    label "odczuli&#263;"
  ]
  node [
    id 323
    label "scena"
  ]
  node [
    id 324
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 325
    label "muza"
  ]
  node [
    id 326
    label "postprodukcja"
  ]
  node [
    id 327
    label "trawiarnia"
  ]
  node [
    id 328
    label "sklejarka"
  ]
  node [
    id 329
    label "sztuka"
  ]
  node [
    id 330
    label "uj&#281;cie"
  ]
  node [
    id 331
    label "filmoteka"
  ]
  node [
    id 332
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 333
    label "klatka"
  ]
  node [
    id 334
    label "rozbieg&#243;wka"
  ]
  node [
    id 335
    label "napisy"
  ]
  node [
    id 336
    label "ta&#347;ma"
  ]
  node [
    id 337
    label "odczulanie"
  ]
  node [
    id 338
    label "anamorfoza"
  ]
  node [
    id 339
    label "dorobek"
  ]
  node [
    id 340
    label "ty&#322;&#243;wka"
  ]
  node [
    id 341
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 342
    label "b&#322;ona"
  ]
  node [
    id 343
    label "emulsja_fotograficzna"
  ]
  node [
    id 344
    label "photograph"
  ]
  node [
    id 345
    label "czo&#322;&#243;wka"
  ]
  node [
    id 346
    label "rola"
  ]
  node [
    id 347
    label "&#347;cie&#380;ka"
  ]
  node [
    id 348
    label "wodorost"
  ]
  node [
    id 349
    label "webbing"
  ]
  node [
    id 350
    label "p&#243;&#322;produkt"
  ]
  node [
    id 351
    label "nagranie"
  ]
  node [
    id 352
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 353
    label "kula"
  ]
  node [
    id 354
    label "pas"
  ]
  node [
    id 355
    label "watkowce"
  ]
  node [
    id 356
    label "zielenica"
  ]
  node [
    id 357
    label "ta&#347;moteka"
  ]
  node [
    id 358
    label "no&#347;nik_danych"
  ]
  node [
    id 359
    label "transporter"
  ]
  node [
    id 360
    label "hutnictwo"
  ]
  node [
    id 361
    label "klaps"
  ]
  node [
    id 362
    label "pasek"
  ]
  node [
    id 363
    label "przewijanie_si&#281;"
  ]
  node [
    id 364
    label "blacha"
  ]
  node [
    id 365
    label "tkanka"
  ]
  node [
    id 366
    label "m&#243;zgoczaszka"
  ]
  node [
    id 367
    label "konto"
  ]
  node [
    id 368
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 369
    label "wypracowa&#263;"
  ]
  node [
    id 370
    label "pr&#243;bowanie"
  ]
  node [
    id 371
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 372
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 373
    label "realizacja"
  ]
  node [
    id 374
    label "didaskalia"
  ]
  node [
    id 375
    label "czyn"
  ]
  node [
    id 376
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 377
    label "environment"
  ]
  node [
    id 378
    label "scenariusz"
  ]
  node [
    id 379
    label "egzemplarz"
  ]
  node [
    id 380
    label "jednostka"
  ]
  node [
    id 381
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 382
    label "kultura_duchowa"
  ]
  node [
    id 383
    label "fortel"
  ]
  node [
    id 384
    label "theatrical_performance"
  ]
  node [
    id 385
    label "ambala&#380;"
  ]
  node [
    id 386
    label "sprawno&#347;&#263;"
  ]
  node [
    id 387
    label "kobieta"
  ]
  node [
    id 388
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 389
    label "Faust"
  ]
  node [
    id 390
    label "scenografia"
  ]
  node [
    id 391
    label "ods&#322;ona"
  ]
  node [
    id 392
    label "pokaz"
  ]
  node [
    id 393
    label "ilo&#347;&#263;"
  ]
  node [
    id 394
    label "przedstawienie"
  ]
  node [
    id 395
    label "przedstawi&#263;"
  ]
  node [
    id 396
    label "Apollo"
  ]
  node [
    id 397
    label "przedstawianie"
  ]
  node [
    id 398
    label "przedstawia&#263;"
  ]
  node [
    id 399
    label "inspiratorka"
  ]
  node [
    id 400
    label "banan"
  ]
  node [
    id 401
    label "talent"
  ]
  node [
    id 402
    label "Melpomena"
  ]
  node [
    id 403
    label "natchnienie"
  ]
  node [
    id 404
    label "bogini"
  ]
  node [
    id 405
    label "ro&#347;lina"
  ]
  node [
    id 406
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 407
    label "palma"
  ]
  node [
    id 408
    label "pocz&#261;tek"
  ]
  node [
    id 409
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 410
    label "kle&#263;"
  ]
  node [
    id 411
    label "hodowla"
  ]
  node [
    id 412
    label "human_body"
  ]
  node [
    id 413
    label "pr&#281;t"
  ]
  node [
    id 414
    label "kopalnia"
  ]
  node [
    id 415
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 416
    label "pomieszczenie"
  ]
  node [
    id 417
    label "konstrukcja"
  ]
  node [
    id 418
    label "ogranicza&#263;"
  ]
  node [
    id 419
    label "sytuacja"
  ]
  node [
    id 420
    label "akwarium"
  ]
  node [
    id 421
    label "d&#378;wig"
  ]
  node [
    id 422
    label "technika"
  ]
  node [
    id 423
    label "kinematografia"
  ]
  node [
    id 424
    label "podwy&#380;szenie"
  ]
  node [
    id 425
    label "kurtyna"
  ]
  node [
    id 426
    label "akt"
  ]
  node [
    id 427
    label "widzownia"
  ]
  node [
    id 428
    label "sznurownia"
  ]
  node [
    id 429
    label "dramaturgy"
  ]
  node [
    id 430
    label "sphere"
  ]
  node [
    id 431
    label "budka_suflera"
  ]
  node [
    id 432
    label "epizod"
  ]
  node [
    id 433
    label "k&#322;&#243;tnia"
  ]
  node [
    id 434
    label "kiesze&#324;"
  ]
  node [
    id 435
    label "stadium"
  ]
  node [
    id 436
    label "podest"
  ]
  node [
    id 437
    label "horyzont"
  ]
  node [
    id 438
    label "teren"
  ]
  node [
    id 439
    label "instytucja"
  ]
  node [
    id 440
    label "proscenium"
  ]
  node [
    id 441
    label "nadscenie"
  ]
  node [
    id 442
    label "antyteatr"
  ]
  node [
    id 443
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 444
    label "pochwytanie"
  ]
  node [
    id 445
    label "wording"
  ]
  node [
    id 446
    label "wzbudzenie"
  ]
  node [
    id 447
    label "withdrawal"
  ]
  node [
    id 448
    label "capture"
  ]
  node [
    id 449
    label "podniesienie"
  ]
  node [
    id 450
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 451
    label "zapisanie"
  ]
  node [
    id 452
    label "prezentacja"
  ]
  node [
    id 453
    label "zamkni&#281;cie"
  ]
  node [
    id 454
    label "zabranie"
  ]
  node [
    id 455
    label "poinformowanie"
  ]
  node [
    id 456
    label "zaaresztowanie"
  ]
  node [
    id 457
    label "wzi&#281;cie"
  ]
  node [
    id 458
    label "materia&#322;"
  ]
  node [
    id 459
    label "rz&#261;d"
  ]
  node [
    id 460
    label "alpinizm"
  ]
  node [
    id 461
    label "wst&#281;p"
  ]
  node [
    id 462
    label "bieg"
  ]
  node [
    id 463
    label "elita"
  ]
  node [
    id 464
    label "rajd"
  ]
  node [
    id 465
    label "poligrafia"
  ]
  node [
    id 466
    label "pododdzia&#322;"
  ]
  node [
    id 467
    label "latarka_czo&#322;owa"
  ]
  node [
    id 468
    label "&#347;ciana"
  ]
  node [
    id 469
    label "zderzenie"
  ]
  node [
    id 470
    label "front"
  ]
  node [
    id 471
    label "fina&#322;"
  ]
  node [
    id 472
    label "uprawienie"
  ]
  node [
    id 473
    label "dialog"
  ]
  node [
    id 474
    label "p&#322;osa"
  ]
  node [
    id 475
    label "wykonywanie"
  ]
  node [
    id 476
    label "ziemia"
  ]
  node [
    id 477
    label "wykonywa&#263;"
  ]
  node [
    id 478
    label "ustawienie"
  ]
  node [
    id 479
    label "pole"
  ]
  node [
    id 480
    label "gospodarstwo"
  ]
  node [
    id 481
    label "uprawi&#263;"
  ]
  node [
    id 482
    label "function"
  ]
  node [
    id 483
    label "zreinterpretowa&#263;"
  ]
  node [
    id 484
    label "zastosowanie"
  ]
  node [
    id 485
    label "reinterpretowa&#263;"
  ]
  node [
    id 486
    label "wrench"
  ]
  node [
    id 487
    label "irygowanie"
  ]
  node [
    id 488
    label "ustawi&#263;"
  ]
  node [
    id 489
    label "irygowa&#263;"
  ]
  node [
    id 490
    label "zreinterpretowanie"
  ]
  node [
    id 491
    label "cel"
  ]
  node [
    id 492
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 493
    label "gra&#263;"
  ]
  node [
    id 494
    label "aktorstwo"
  ]
  node [
    id 495
    label "kostium"
  ]
  node [
    id 496
    label "zagon"
  ]
  node [
    id 497
    label "zagra&#263;"
  ]
  node [
    id 498
    label "reinterpretowanie"
  ]
  node [
    id 499
    label "sk&#322;ad"
  ]
  node [
    id 500
    label "zagranie"
  ]
  node [
    id 501
    label "radlina"
  ]
  node [
    id 502
    label "granie"
  ]
  node [
    id 503
    label "farba"
  ]
  node [
    id 504
    label "odblask"
  ]
  node [
    id 505
    label "plama"
  ]
  node [
    id 506
    label "urz&#261;dzenie"
  ]
  node [
    id 507
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 508
    label "alergia"
  ]
  node [
    id 509
    label "usuni&#281;cie"
  ]
  node [
    id 510
    label "zmniejszenie"
  ]
  node [
    id 511
    label "wyleczenie"
  ]
  node [
    id 512
    label "desensitization"
  ]
  node [
    id 513
    label "zmniejszanie"
  ]
  node [
    id 514
    label "usuwanie"
  ]
  node [
    id 515
    label "terapia"
  ]
  node [
    id 516
    label "usun&#261;&#263;"
  ]
  node [
    id 517
    label "wyleczy&#263;"
  ]
  node [
    id 518
    label "zmniejszy&#263;"
  ]
  node [
    id 519
    label "leczy&#263;"
  ]
  node [
    id 520
    label "usuwa&#263;"
  ]
  node [
    id 521
    label "zmniejsza&#263;"
  ]
  node [
    id 522
    label "przek&#322;ad"
  ]
  node [
    id 523
    label "dialogista"
  ]
  node [
    id 524
    label "proces_biologiczny"
  ]
  node [
    id 525
    label "zamiana"
  ]
  node [
    id 526
    label "deformacja"
  ]
  node [
    id 527
    label "faza"
  ]
  node [
    id 528
    label "archiwum"
  ]
  node [
    id 529
    label "catalog"
  ]
  node [
    id 530
    label "pozycja"
  ]
  node [
    id 531
    label "sumariusz"
  ]
  node [
    id 532
    label "book"
  ]
  node [
    id 533
    label "stock"
  ]
  node [
    id 534
    label "figurowa&#263;"
  ]
  node [
    id 535
    label "wyliczanka"
  ]
  node [
    id 536
    label "series"
  ]
  node [
    id 537
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 538
    label "uprawianie"
  ]
  node [
    id 539
    label "praca_rolnicza"
  ]
  node [
    id 540
    label "collection"
  ]
  node [
    id 541
    label "dane"
  ]
  node [
    id 542
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 543
    label "pakiet_klimatyczny"
  ]
  node [
    id 544
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 545
    label "sum"
  ]
  node [
    id 546
    label "gathering"
  ]
  node [
    id 547
    label "album"
  ]
  node [
    id 548
    label "po&#322;o&#380;enie"
  ]
  node [
    id 549
    label "debit"
  ]
  node [
    id 550
    label "druk"
  ]
  node [
    id 551
    label "szata_graficzna"
  ]
  node [
    id 552
    label "wydawa&#263;"
  ]
  node [
    id 553
    label "szermierka"
  ]
  node [
    id 554
    label "spis"
  ]
  node [
    id 555
    label "wyda&#263;"
  ]
  node [
    id 556
    label "publikacja"
  ]
  node [
    id 557
    label "status"
  ]
  node [
    id 558
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 559
    label "adres"
  ]
  node [
    id 560
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 561
    label "redaktor"
  ]
  node [
    id 562
    label "awansowa&#263;"
  ]
  node [
    id 563
    label "wojsko"
  ]
  node [
    id 564
    label "bearing"
  ]
  node [
    id 565
    label "awans"
  ]
  node [
    id 566
    label "awansowanie"
  ]
  node [
    id 567
    label "poster"
  ]
  node [
    id 568
    label "le&#380;e&#263;"
  ]
  node [
    id 569
    label "entliczek"
  ]
  node [
    id 570
    label "zabawa"
  ]
  node [
    id 571
    label "wiersz"
  ]
  node [
    id 572
    label "pentliczek"
  ]
  node [
    id 573
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 574
    label "gra_planszowa"
  ]
  node [
    id 575
    label "Polish"
  ]
  node [
    id 576
    label "goniony"
  ]
  node [
    id 577
    label "oberek"
  ]
  node [
    id 578
    label "ryba_po_grecku"
  ]
  node [
    id 579
    label "sztajer"
  ]
  node [
    id 580
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 581
    label "krakowiak"
  ]
  node [
    id 582
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 583
    label "pierogi_ruskie"
  ]
  node [
    id 584
    label "lacki"
  ]
  node [
    id 585
    label "polak"
  ]
  node [
    id 586
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 587
    label "chodzony"
  ]
  node [
    id 588
    label "po_polsku"
  ]
  node [
    id 589
    label "mazur"
  ]
  node [
    id 590
    label "polsko"
  ]
  node [
    id 591
    label "skoczny"
  ]
  node [
    id 592
    label "drabant"
  ]
  node [
    id 593
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 594
    label "j&#281;zyk"
  ]
  node [
    id 595
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 596
    label "artykulator"
  ]
  node [
    id 597
    label "kod"
  ]
  node [
    id 598
    label "kawa&#322;ek"
  ]
  node [
    id 599
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 600
    label "gramatyka"
  ]
  node [
    id 601
    label "stylik"
  ]
  node [
    id 602
    label "przet&#322;umaczenie"
  ]
  node [
    id 603
    label "formalizowanie"
  ]
  node [
    id 604
    label "ssa&#263;"
  ]
  node [
    id 605
    label "ssanie"
  ]
  node [
    id 606
    label "language"
  ]
  node [
    id 607
    label "liza&#263;"
  ]
  node [
    id 608
    label "napisa&#263;"
  ]
  node [
    id 609
    label "konsonantyzm"
  ]
  node [
    id 610
    label "wokalizm"
  ]
  node [
    id 611
    label "fonetyka"
  ]
  node [
    id 612
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 613
    label "jeniec"
  ]
  node [
    id 614
    label "but"
  ]
  node [
    id 615
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 616
    label "po_koroniarsku"
  ]
  node [
    id 617
    label "t&#322;umaczenie"
  ]
  node [
    id 618
    label "m&#243;wienie"
  ]
  node [
    id 619
    label "pype&#263;"
  ]
  node [
    id 620
    label "lizanie"
  ]
  node [
    id 621
    label "pismo"
  ]
  node [
    id 622
    label "formalizowa&#263;"
  ]
  node [
    id 623
    label "rozumie&#263;"
  ]
  node [
    id 624
    label "organ"
  ]
  node [
    id 625
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 626
    label "rozumienie"
  ]
  node [
    id 627
    label "spos&#243;b"
  ]
  node [
    id 628
    label "makroglosja"
  ]
  node [
    id 629
    label "m&#243;wi&#263;"
  ]
  node [
    id 630
    label "jama_ustna"
  ]
  node [
    id 631
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 632
    label "formacja_geologiczna"
  ]
  node [
    id 633
    label "natural_language"
  ]
  node [
    id 634
    label "s&#322;ownictwo"
  ]
  node [
    id 635
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 636
    label "wschodnioeuropejski"
  ]
  node [
    id 637
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 638
    label "poga&#324;ski"
  ]
  node [
    id 639
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 640
    label "topielec"
  ]
  node [
    id 641
    label "europejski"
  ]
  node [
    id 642
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 643
    label "langosz"
  ]
  node [
    id 644
    label "sponiewieranie"
  ]
  node [
    id 645
    label "discipline"
  ]
  node [
    id 646
    label "kr&#261;&#380;enie"
  ]
  node [
    id 647
    label "robienie"
  ]
  node [
    id 648
    label "sponiewiera&#263;"
  ]
  node [
    id 649
    label "element"
  ]
  node [
    id 650
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 651
    label "program_nauczania"
  ]
  node [
    id 652
    label "thing"
  ]
  node [
    id 653
    label "gwardzista"
  ]
  node [
    id 654
    label "taniec"
  ]
  node [
    id 655
    label "taniec_ludowy"
  ]
  node [
    id 656
    label "&#347;redniowieczny"
  ]
  node [
    id 657
    label "europejsko"
  ]
  node [
    id 658
    label "specjalny"
  ]
  node [
    id 659
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 660
    label "weso&#322;y"
  ]
  node [
    id 661
    label "sprawny"
  ]
  node [
    id 662
    label "rytmiczny"
  ]
  node [
    id 663
    label "skocznie"
  ]
  node [
    id 664
    label "energiczny"
  ]
  node [
    id 665
    label "przytup"
  ]
  node [
    id 666
    label "ho&#322;ubiec"
  ]
  node [
    id 667
    label "wodzi&#263;"
  ]
  node [
    id 668
    label "lendler"
  ]
  node [
    id 669
    label "austriacki"
  ]
  node [
    id 670
    label "polka"
  ]
  node [
    id 671
    label "ludowy"
  ]
  node [
    id 672
    label "pie&#347;&#324;"
  ]
  node [
    id 673
    label "mieszkaniec"
  ]
  node [
    id 674
    label "centu&#347;"
  ]
  node [
    id 675
    label "lalka"
  ]
  node [
    id 676
    label "Ma&#322;opolanin"
  ]
  node [
    id 677
    label "krakauer"
  ]
  node [
    id 678
    label "Paneuropa"
  ]
  node [
    id 679
    label "confederation"
  ]
  node [
    id 680
    label "podob&#243;z"
  ]
  node [
    id 681
    label "namiot"
  ]
  node [
    id 682
    label "obozowisko"
  ]
  node [
    id 683
    label "schronienie"
  ]
  node [
    id 684
    label "odpoczynek"
  ]
  node [
    id 685
    label "ONZ"
  ]
  node [
    id 686
    label "zwi&#261;zek"
  ]
  node [
    id 687
    label "NATO"
  ]
  node [
    id 688
    label "alianci"
  ]
  node [
    id 689
    label "miejsce_odosobnienia"
  ]
  node [
    id 690
    label "osada"
  ]
  node [
    id 691
    label "rozrywka"
  ]
  node [
    id 692
    label "wyraj"
  ]
  node [
    id 693
    label "wczas"
  ]
  node [
    id 694
    label "diversion"
  ]
  node [
    id 695
    label "odwadnia&#263;"
  ]
  node [
    id 696
    label "wi&#261;zanie"
  ]
  node [
    id 697
    label "odwodni&#263;"
  ]
  node [
    id 698
    label "bratnia_dusza"
  ]
  node [
    id 699
    label "powi&#261;zanie"
  ]
  node [
    id 700
    label "zwi&#261;zanie"
  ]
  node [
    id 701
    label "konstytucja"
  ]
  node [
    id 702
    label "marriage"
  ]
  node [
    id 703
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 704
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 705
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 706
    label "zwi&#261;za&#263;"
  ]
  node [
    id 707
    label "odwadnianie"
  ]
  node [
    id 708
    label "odwodnienie"
  ]
  node [
    id 709
    label "marketing_afiliacyjny"
  ]
  node [
    id 710
    label "substancja_chemiczna"
  ]
  node [
    id 711
    label "koligacja"
  ]
  node [
    id 712
    label "lokant"
  ]
  node [
    id 713
    label "azeotrop"
  ]
  node [
    id 714
    label "bezpieczny"
  ]
  node [
    id 715
    label "ukryty"
  ]
  node [
    id 716
    label "cover"
  ]
  node [
    id 717
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 718
    label "ukrycie"
  ]
  node [
    id 719
    label "odm&#322;adzanie"
  ]
  node [
    id 720
    label "liga"
  ]
  node [
    id 721
    label "asymilowanie"
  ]
  node [
    id 722
    label "gromada"
  ]
  node [
    id 723
    label "asymilowa&#263;"
  ]
  node [
    id 724
    label "Entuzjastki"
  ]
  node [
    id 725
    label "kompozycja"
  ]
  node [
    id 726
    label "Terranie"
  ]
  node [
    id 727
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 728
    label "category"
  ]
  node [
    id 729
    label "oddzia&#322;"
  ]
  node [
    id 730
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 731
    label "cz&#261;steczka"
  ]
  node [
    id 732
    label "stage_set"
  ]
  node [
    id 733
    label "type"
  ]
  node [
    id 734
    label "specgrupa"
  ]
  node [
    id 735
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 736
    label "&#346;wietliki"
  ]
  node [
    id 737
    label "odm&#322;odzenie"
  ]
  node [
    id 738
    label "Eurogrupa"
  ]
  node [
    id 739
    label "odm&#322;adza&#263;"
  ]
  node [
    id 740
    label "harcerze_starsi"
  ]
  node [
    id 741
    label "North_Atlantic_Treaty_Organization"
  ]
  node [
    id 742
    label "uk&#322;ad"
  ]
  node [
    id 743
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 744
    label "b&#322;&#281;kitne_he&#322;my"
  ]
  node [
    id 745
    label "misja_weryfikacyjna"
  ]
  node [
    id 746
    label "WIPO"
  ]
  node [
    id 747
    label "United_Nations"
  ]
  node [
    id 748
    label "crash"
  ]
  node [
    id 749
    label "zniszczenie"
  ]
  node [
    id 750
    label "pogr&#261;&#380;enie"
  ]
  node [
    id 751
    label "ludob&#243;jstwo"
  ]
  node [
    id 752
    label "kl&#281;ska_&#380;ywio&#322;owa"
  ]
  node [
    id 753
    label "szmalcownik"
  ]
  node [
    id 754
    label "pogr&#261;&#380;y&#263;"
  ]
  node [
    id 755
    label "holocaust"
  ]
  node [
    id 756
    label "apokalipsa"
  ]
  node [
    id 757
    label "negacjonizm"
  ]
  node [
    id 758
    label "apocalypse"
  ]
  node [
    id 759
    label "s&#261;d_ostateczny"
  ]
  node [
    id 760
    label "proroctwo"
  ]
  node [
    id 761
    label "Apokalipsa"
  ]
  node [
    id 762
    label "dzie&#324;_ostateczny"
  ]
  node [
    id 763
    label "wear"
  ]
  node [
    id 764
    label "destruction"
  ]
  node [
    id 765
    label "zu&#380;ycie"
  ]
  node [
    id 766
    label "attrition"
  ]
  node [
    id 767
    label "zaszkodzenie"
  ]
  node [
    id 768
    label "os&#322;abienie"
  ]
  node [
    id 769
    label "podpalenie"
  ]
  node [
    id 770
    label "strata"
  ]
  node [
    id 771
    label "kondycja_fizyczna"
  ]
  node [
    id 772
    label "spowodowanie"
  ]
  node [
    id 773
    label "spl&#261;drowanie"
  ]
  node [
    id 774
    label "zdrowie"
  ]
  node [
    id 775
    label "poniszczenie"
  ]
  node [
    id 776
    label "ruin"
  ]
  node [
    id 777
    label "stanie_si&#281;"
  ]
  node [
    id 778
    label "rezultat"
  ]
  node [
    id 779
    label "czynno&#347;&#263;"
  ]
  node [
    id 780
    label "poniszczenie_si&#281;"
  ]
  node [
    id 781
    label "mord"
  ]
  node [
    id 782
    label "genocide"
  ]
  node [
    id 783
    label "doprowadzi&#263;"
  ]
  node [
    id 784
    label "zanurzy&#263;"
  ]
  node [
    id 785
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 786
    label "dip"
  ]
  node [
    id 787
    label "wprowadzi&#263;"
  ]
  node [
    id 788
    label "zanurzenie"
  ]
  node [
    id 789
    label "loss"
  ]
  node [
    id 790
    label "doprowadzenie"
  ]
  node [
    id 791
    label "pogl&#261;d"
  ]
  node [
    id 792
    label "szanta&#380;ysta"
  ]
  node [
    id 793
    label "talerz_perkusyjny"
  ]
  node [
    id 794
    label "Messi"
  ]
  node [
    id 795
    label "Herkules_Poirot"
  ]
  node [
    id 796
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 797
    label "Achilles"
  ]
  node [
    id 798
    label "Harry_Potter"
  ]
  node [
    id 799
    label "Casanova"
  ]
  node [
    id 800
    label "Zgredek"
  ]
  node [
    id 801
    label "Asterix"
  ]
  node [
    id 802
    label "Winnetou"
  ]
  node [
    id 803
    label "uczestnik"
  ]
  node [
    id 804
    label "&#347;mia&#322;ek"
  ]
  node [
    id 805
    label "Mario"
  ]
  node [
    id 806
    label "Borewicz"
  ]
  node [
    id 807
    label "Quasimodo"
  ]
  node [
    id 808
    label "Sherlock_Holmes"
  ]
  node [
    id 809
    label "Wallenrod"
  ]
  node [
    id 810
    label "Herkules"
  ]
  node [
    id 811
    label "bohaterski"
  ]
  node [
    id 812
    label "Don_Juan"
  ]
  node [
    id 813
    label "podmiot"
  ]
  node [
    id 814
    label "Don_Kiszot"
  ]
  node [
    id 815
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 816
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 817
    label "Hamlet"
  ]
  node [
    id 818
    label "Werter"
  ]
  node [
    id 819
    label "Szwejk"
  ]
  node [
    id 820
    label "zaistnie&#263;"
  ]
  node [
    id 821
    label "Osjan"
  ]
  node [
    id 822
    label "kto&#347;"
  ]
  node [
    id 823
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 824
    label "osobowo&#347;&#263;"
  ]
  node [
    id 825
    label "trim"
  ]
  node [
    id 826
    label "poby&#263;"
  ]
  node [
    id 827
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 828
    label "Aspazja"
  ]
  node [
    id 829
    label "kompleksja"
  ]
  node [
    id 830
    label "wytrzyma&#263;"
  ]
  node [
    id 831
    label "budowa"
  ]
  node [
    id 832
    label "pozosta&#263;"
  ]
  node [
    id 833
    label "point"
  ]
  node [
    id 834
    label "go&#347;&#263;"
  ]
  node [
    id 835
    label "zuch"
  ]
  node [
    id 836
    label "rycerzyk"
  ]
  node [
    id 837
    label "odwa&#380;ny"
  ]
  node [
    id 838
    label "ryzykant"
  ]
  node [
    id 839
    label "morowiec"
  ]
  node [
    id 840
    label "trawa"
  ]
  node [
    id 841
    label "twardziel"
  ]
  node [
    id 842
    label "owsowe"
  ]
  node [
    id 843
    label "ludzko&#347;&#263;"
  ]
  node [
    id 844
    label "wapniak"
  ]
  node [
    id 845
    label "os&#322;abia&#263;"
  ]
  node [
    id 846
    label "hominid"
  ]
  node [
    id 847
    label "podw&#322;adny"
  ]
  node [
    id 848
    label "os&#322;abianie"
  ]
  node [
    id 849
    label "figura"
  ]
  node [
    id 850
    label "portrecista"
  ]
  node [
    id 851
    label "dwun&#243;g"
  ]
  node [
    id 852
    label "profanum"
  ]
  node [
    id 853
    label "mikrokosmos"
  ]
  node [
    id 854
    label "nasada"
  ]
  node [
    id 855
    label "duch"
  ]
  node [
    id 856
    label "antropochoria"
  ]
  node [
    id 857
    label "osoba"
  ]
  node [
    id 858
    label "senior"
  ]
  node [
    id 859
    label "oddzia&#322;ywanie"
  ]
  node [
    id 860
    label "Adam"
  ]
  node [
    id 861
    label "homo_sapiens"
  ]
  node [
    id 862
    label "polifag"
  ]
  node [
    id 863
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 864
    label "byt"
  ]
  node [
    id 865
    label "prawo"
  ]
  node [
    id 866
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 867
    label "nauka_prawa"
  ]
  node [
    id 868
    label "Szekspir"
  ]
  node [
    id 869
    label "Mickiewicz"
  ]
  node [
    id 870
    label "cierpienie"
  ]
  node [
    id 871
    label "Atomowa_Pch&#322;a"
  ]
  node [
    id 872
    label "bohatersko"
  ]
  node [
    id 873
    label "dzielny"
  ]
  node [
    id 874
    label "silny"
  ]
  node [
    id 875
    label "waleczny"
  ]
  node [
    id 876
    label "Schindler"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 876
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 199
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 191
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 204
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
]
