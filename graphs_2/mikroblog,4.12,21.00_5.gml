graph [
  node [
    id 0
    label "s&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "g&#243;wno"
    origin "text"
  ]
  node [
    id 2
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 4
    label "robi&#263;"
  ]
  node [
    id 5
    label "my&#347;le&#263;"
  ]
  node [
    id 6
    label "deliver"
  ]
  node [
    id 7
    label "powodowa&#263;"
  ]
  node [
    id 8
    label "hold"
  ]
  node [
    id 9
    label "sprawowa&#263;"
  ]
  node [
    id 10
    label "os&#261;dza&#263;"
  ]
  node [
    id 11
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 12
    label "zas&#261;dza&#263;"
  ]
  node [
    id 13
    label "take_care"
  ]
  node [
    id 14
    label "troska&#263;_si&#281;"
  ]
  node [
    id 15
    label "rozpatrywa&#263;"
  ]
  node [
    id 16
    label "zamierza&#263;"
  ]
  node [
    id 17
    label "argue"
  ]
  node [
    id 18
    label "organizowa&#263;"
  ]
  node [
    id 19
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 20
    label "czyni&#263;"
  ]
  node [
    id 21
    label "give"
  ]
  node [
    id 22
    label "stylizowa&#263;"
  ]
  node [
    id 23
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 24
    label "falowa&#263;"
  ]
  node [
    id 25
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 26
    label "peddle"
  ]
  node [
    id 27
    label "praca"
  ]
  node [
    id 28
    label "wydala&#263;"
  ]
  node [
    id 29
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 30
    label "tentegowa&#263;"
  ]
  node [
    id 31
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 32
    label "urz&#261;dza&#263;"
  ]
  node [
    id 33
    label "oszukiwa&#263;"
  ]
  node [
    id 34
    label "work"
  ]
  node [
    id 35
    label "ukazywa&#263;"
  ]
  node [
    id 36
    label "przerabia&#263;"
  ]
  node [
    id 37
    label "act"
  ]
  node [
    id 38
    label "post&#281;powa&#263;"
  ]
  node [
    id 39
    label "mie&#263;_miejsce"
  ]
  node [
    id 40
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 41
    label "motywowa&#263;"
  ]
  node [
    id 42
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 43
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 44
    label "prosecute"
  ]
  node [
    id 45
    label "by&#263;"
  ]
  node [
    id 46
    label "strike"
  ]
  node [
    id 47
    label "znajdowa&#263;"
  ]
  node [
    id 48
    label "allow"
  ]
  node [
    id 49
    label "przyznawa&#263;"
  ]
  node [
    id 50
    label "wydawa&#263;_wyrok"
  ]
  node [
    id 51
    label "condemn"
  ]
  node [
    id 52
    label "ka&#322;"
  ]
  node [
    id 53
    label "tandeta"
  ]
  node [
    id 54
    label "zero"
  ]
  node [
    id 55
    label "drobiazg"
  ]
  node [
    id 56
    label "punkt"
  ]
  node [
    id 57
    label "liczba"
  ]
  node [
    id 58
    label "ilo&#347;&#263;"
  ]
  node [
    id 59
    label "podzia&#322;ka"
  ]
  node [
    id 60
    label "ciura"
  ]
  node [
    id 61
    label "cyfra"
  ]
  node [
    id 62
    label "miernota"
  ]
  node [
    id 63
    label "love"
  ]
  node [
    id 64
    label "brak"
  ]
  node [
    id 65
    label "wydalina"
  ]
  node [
    id 66
    label "koprofilia"
  ]
  node [
    id 67
    label "stool"
  ]
  node [
    id 68
    label "odchody"
  ]
  node [
    id 69
    label "balas"
  ]
  node [
    id 70
    label "fekalia"
  ]
  node [
    id 71
    label "marno&#347;&#263;"
  ]
  node [
    id 72
    label "lichota"
  ]
  node [
    id 73
    label "mierno&#347;&#263;"
  ]
  node [
    id 74
    label "ta&#322;atajstwo"
  ]
  node [
    id 75
    label "produkt"
  ]
  node [
    id 76
    label "pisanina"
  ]
  node [
    id 77
    label "utw&#243;r"
  ]
  node [
    id 78
    label "lipa"
  ]
  node [
    id 79
    label "towar"
  ]
  node [
    id 80
    label "plewa"
  ]
  node [
    id 81
    label "bagatelle"
  ]
  node [
    id 82
    label "bangle"
  ]
  node [
    id 83
    label "triviality"
  ]
  node [
    id 84
    label "zbi&#243;r"
  ]
  node [
    id 85
    label "fraszka"
  ]
  node [
    id 86
    label "banalny"
  ]
  node [
    id 87
    label "sofcik"
  ]
  node [
    id 88
    label "fidryga&#322;ki"
  ]
  node [
    id 89
    label "kr&#243;tki"
  ]
  node [
    id 90
    label "szczeg&#243;&#322;"
  ]
  node [
    id 91
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 92
    label "nowela"
  ]
  node [
    id 93
    label "furda"
  ]
  node [
    id 94
    label "react"
  ]
  node [
    id 95
    label "dawa&#263;"
  ]
  node [
    id 96
    label "ponosi&#263;"
  ]
  node [
    id 97
    label "report"
  ]
  node [
    id 98
    label "pytanie"
  ]
  node [
    id 99
    label "equate"
  ]
  node [
    id 100
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 101
    label "answer"
  ]
  node [
    id 102
    label "tone"
  ]
  node [
    id 103
    label "contend"
  ]
  node [
    id 104
    label "reagowa&#263;"
  ]
  node [
    id 105
    label "impart"
  ]
  node [
    id 106
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 107
    label "uczestniczy&#263;"
  ]
  node [
    id 108
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 109
    label "equal"
  ]
  node [
    id 110
    label "trwa&#263;"
  ]
  node [
    id 111
    label "chodzi&#263;"
  ]
  node [
    id 112
    label "si&#281;ga&#263;"
  ]
  node [
    id 113
    label "stan"
  ]
  node [
    id 114
    label "obecno&#347;&#263;"
  ]
  node [
    id 115
    label "stand"
  ]
  node [
    id 116
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 117
    label "przekazywa&#263;"
  ]
  node [
    id 118
    label "dostarcza&#263;"
  ]
  node [
    id 119
    label "&#322;adowa&#263;"
  ]
  node [
    id 120
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 121
    label "przeznacza&#263;"
  ]
  node [
    id 122
    label "surrender"
  ]
  node [
    id 123
    label "traktowa&#263;"
  ]
  node [
    id 124
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 125
    label "obiecywa&#263;"
  ]
  node [
    id 126
    label "odst&#281;powa&#263;"
  ]
  node [
    id 127
    label "tender"
  ]
  node [
    id 128
    label "rap"
  ]
  node [
    id 129
    label "umieszcza&#263;"
  ]
  node [
    id 130
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 131
    label "t&#322;uc"
  ]
  node [
    id 132
    label "powierza&#263;"
  ]
  node [
    id 133
    label "render"
  ]
  node [
    id 134
    label "wpiernicza&#263;"
  ]
  node [
    id 135
    label "exsert"
  ]
  node [
    id 136
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 137
    label "train"
  ]
  node [
    id 138
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 139
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 140
    label "p&#322;aci&#263;"
  ]
  node [
    id 141
    label "hold_out"
  ]
  node [
    id 142
    label "nalewa&#263;"
  ]
  node [
    id 143
    label "zezwala&#263;"
  ]
  node [
    id 144
    label "wst&#281;powa&#263;"
  ]
  node [
    id 145
    label "str&#243;j"
  ]
  node [
    id 146
    label "hurt"
  ]
  node [
    id 147
    label "digest"
  ]
  node [
    id 148
    label "make"
  ]
  node [
    id 149
    label "bolt"
  ]
  node [
    id 150
    label "odci&#261;ga&#263;"
  ]
  node [
    id 151
    label "doznawa&#263;"
  ]
  node [
    id 152
    label "umowa"
  ]
  node [
    id 153
    label "cover"
  ]
  node [
    id 154
    label "sprawa"
  ]
  node [
    id 155
    label "wypytanie"
  ]
  node [
    id 156
    label "egzaminowanie"
  ]
  node [
    id 157
    label "zwracanie_si&#281;"
  ]
  node [
    id 158
    label "wywo&#322;ywanie"
  ]
  node [
    id 159
    label "rozpytywanie"
  ]
  node [
    id 160
    label "wypowiedzenie"
  ]
  node [
    id 161
    label "wypowied&#378;"
  ]
  node [
    id 162
    label "problemat"
  ]
  node [
    id 163
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 164
    label "problematyka"
  ]
  node [
    id 165
    label "sprawdzian"
  ]
  node [
    id 166
    label "zadanie"
  ]
  node [
    id 167
    label "przes&#322;uchiwanie"
  ]
  node [
    id 168
    label "question"
  ]
  node [
    id 169
    label "sprawdzanie"
  ]
  node [
    id 170
    label "odpowiadanie"
  ]
  node [
    id 171
    label "survey"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
]
