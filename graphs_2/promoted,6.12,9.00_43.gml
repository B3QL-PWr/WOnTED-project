graph [
  node [
    id 0
    label "rom"
    origin "text"
  ]
  node [
    id 1
    label "podkrakowski"
    origin "text"
  ]
  node [
    id 2
    label "maszkowic"
    origin "text"
  ]
  node [
    id 3
    label "mieszka&#263;"
    origin "text"
  ]
  node [
    id 4
    label "skandaliczny"
    origin "text"
  ]
  node [
    id 5
    label "warunek"
    origin "text"
  ]
  node [
    id 6
    label "osada"
    origin "text"
  ]
  node [
    id 7
    label "przypomina&#263;"
    origin "text"
  ]
  node [
    id 8
    label "slums"
    origin "text"
  ]
  node [
    id 9
    label "panowa&#263;"
  ]
  node [
    id 10
    label "przebywa&#263;"
  ]
  node [
    id 11
    label "sta&#263;"
  ]
  node [
    id 12
    label "fall"
  ]
  node [
    id 13
    label "zajmowa&#263;"
  ]
  node [
    id 14
    label "room"
  ]
  node [
    id 15
    label "dominowa&#263;"
  ]
  node [
    id 16
    label "kontrolowa&#263;"
  ]
  node [
    id 17
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 18
    label "kierowa&#263;"
  ]
  node [
    id 19
    label "control"
  ]
  node [
    id 20
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 21
    label "istnie&#263;"
  ]
  node [
    id 22
    label "przewa&#380;a&#263;"
  ]
  node [
    id 23
    label "manipulate"
  ]
  node [
    id 24
    label "fill"
  ]
  node [
    id 25
    label "pali&#263;_si&#281;"
  ]
  node [
    id 26
    label "obejmowa&#263;"
  ]
  node [
    id 27
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 28
    label "zadawa&#263;"
  ]
  node [
    id 29
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 30
    label "rozciekawia&#263;"
  ]
  node [
    id 31
    label "topographic_point"
  ]
  node [
    id 32
    label "prosecute"
  ]
  node [
    id 33
    label "zabiera&#263;"
  ]
  node [
    id 34
    label "korzysta&#263;"
  ]
  node [
    id 35
    label "return"
  ]
  node [
    id 36
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 37
    label "bra&#263;"
  ]
  node [
    id 38
    label "schorzenie"
  ]
  node [
    id 39
    label "klasyfikacja"
  ]
  node [
    id 40
    label "aim"
  ]
  node [
    id 41
    label "komornik"
  ]
  node [
    id 42
    label "trwa&#263;"
  ]
  node [
    id 43
    label "powodowa&#263;"
  ]
  node [
    id 44
    label "sake"
  ]
  node [
    id 45
    label "do"
  ]
  node [
    id 46
    label "robi&#263;"
  ]
  node [
    id 47
    label "anektowa&#263;"
  ]
  node [
    id 48
    label "dostarcza&#263;"
  ]
  node [
    id 49
    label "hesitate"
  ]
  node [
    id 50
    label "pause"
  ]
  node [
    id 51
    label "przestawa&#263;"
  ]
  node [
    id 52
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 53
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 54
    label "tkwi&#263;"
  ]
  node [
    id 55
    label "undertaking"
  ]
  node [
    id 56
    label "wystarcza&#263;"
  ]
  node [
    id 57
    label "kosztowa&#263;"
  ]
  node [
    id 58
    label "pozostawa&#263;"
  ]
  node [
    id 59
    label "czeka&#263;"
  ]
  node [
    id 60
    label "sprawowa&#263;"
  ]
  node [
    id 61
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 62
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 63
    label "wystarczy&#263;"
  ]
  node [
    id 64
    label "wystawa&#263;"
  ]
  node [
    id 65
    label "by&#263;"
  ]
  node [
    id 66
    label "base"
  ]
  node [
    id 67
    label "digest"
  ]
  node [
    id 68
    label "stand"
  ]
  node [
    id 69
    label "straszny"
  ]
  node [
    id 70
    label "skandalicznie"
  ]
  node [
    id 71
    label "sensacyjny"
  ]
  node [
    id 72
    label "gorsz&#261;cy"
  ]
  node [
    id 73
    label "fabularny"
  ]
  node [
    id 74
    label "sensacyjnie"
  ]
  node [
    id 75
    label "gor&#261;cy"
  ]
  node [
    id 76
    label "nieoczekiwany"
  ]
  node [
    id 77
    label "niegrzeczny"
  ]
  node [
    id 78
    label "kurewski"
  ]
  node [
    id 79
    label "niemoralny"
  ]
  node [
    id 80
    label "strasznie"
  ]
  node [
    id 81
    label "olbrzymi"
  ]
  node [
    id 82
    label "gorsz&#261;co"
  ]
  node [
    id 83
    label "nie&#322;adny"
  ]
  node [
    id 84
    label "z&#322;y"
  ]
  node [
    id 85
    label "za&#322;o&#380;enie"
  ]
  node [
    id 86
    label "faktor"
  ]
  node [
    id 87
    label "umowa"
  ]
  node [
    id 88
    label "ekspozycja"
  ]
  node [
    id 89
    label "condition"
  ]
  node [
    id 90
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 91
    label "agent"
  ]
  node [
    id 92
    label "infliction"
  ]
  node [
    id 93
    label "spowodowanie"
  ]
  node [
    id 94
    label "proposition"
  ]
  node [
    id 95
    label "przygotowanie"
  ]
  node [
    id 96
    label "pozak&#322;adanie"
  ]
  node [
    id 97
    label "point"
  ]
  node [
    id 98
    label "program"
  ]
  node [
    id 99
    label "poubieranie"
  ]
  node [
    id 100
    label "rozebranie"
  ]
  node [
    id 101
    label "str&#243;j"
  ]
  node [
    id 102
    label "budowla"
  ]
  node [
    id 103
    label "przewidzenie"
  ]
  node [
    id 104
    label "zak&#322;adka"
  ]
  node [
    id 105
    label "czynno&#347;&#263;"
  ]
  node [
    id 106
    label "twierdzenie"
  ]
  node [
    id 107
    label "przygotowywanie"
  ]
  node [
    id 108
    label "podwini&#281;cie"
  ]
  node [
    id 109
    label "zap&#322;acenie"
  ]
  node [
    id 110
    label "wyko&#324;czenie"
  ]
  node [
    id 111
    label "struktura"
  ]
  node [
    id 112
    label "utworzenie"
  ]
  node [
    id 113
    label "przebranie"
  ]
  node [
    id 114
    label "obleczenie"
  ]
  node [
    id 115
    label "przymierzenie"
  ]
  node [
    id 116
    label "obleczenie_si&#281;"
  ]
  node [
    id 117
    label "przywdzianie"
  ]
  node [
    id 118
    label "umieszczenie"
  ]
  node [
    id 119
    label "zrobienie"
  ]
  node [
    id 120
    label "przyodzianie"
  ]
  node [
    id 121
    label "pokrycie"
  ]
  node [
    id 122
    label "sytuacja"
  ]
  node [
    id 123
    label "sk&#322;adnik"
  ]
  node [
    id 124
    label "wydarzenie"
  ]
  node [
    id 125
    label "warunki"
  ]
  node [
    id 126
    label "czyn"
  ]
  node [
    id 127
    label "zawarcie"
  ]
  node [
    id 128
    label "zawrze&#263;"
  ]
  node [
    id 129
    label "contract"
  ]
  node [
    id 130
    label "porozumienie"
  ]
  node [
    id 131
    label "gestia_transportowa"
  ]
  node [
    id 132
    label "klauzula"
  ]
  node [
    id 133
    label "programowanie_agentowe"
  ]
  node [
    id 134
    label "facet"
  ]
  node [
    id 135
    label "wojsko"
  ]
  node [
    id 136
    label "system_wieloagentowy"
  ]
  node [
    id 137
    label "kontrakt"
  ]
  node [
    id 138
    label "wywiad"
  ]
  node [
    id 139
    label "orygina&#322;"
  ]
  node [
    id 140
    label "dzier&#380;awca"
  ]
  node [
    id 141
    label "wytw&#243;r"
  ]
  node [
    id 142
    label "rep"
  ]
  node [
    id 143
    label "&#347;ledziciel"
  ]
  node [
    id 144
    label "informator"
  ]
  node [
    id 145
    label "przedstawiciel"
  ]
  node [
    id 146
    label "zi&#243;&#322;ko"
  ]
  node [
    id 147
    label "agentura"
  ]
  node [
    id 148
    label "funkcjonariusz"
  ]
  node [
    id 149
    label "detektyw"
  ]
  node [
    id 150
    label "po&#347;rednik"
  ]
  node [
    id 151
    label "czynnik"
  ]
  node [
    id 152
    label "filtr"
  ]
  node [
    id 153
    label "kustosz"
  ]
  node [
    id 154
    label "galeria"
  ]
  node [
    id 155
    label "fotografia"
  ]
  node [
    id 156
    label "spot"
  ]
  node [
    id 157
    label "wprowadzenie"
  ]
  node [
    id 158
    label "po&#322;o&#380;enie"
  ]
  node [
    id 159
    label "parametr"
  ]
  node [
    id 160
    label "miejsce"
  ]
  node [
    id 161
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 162
    label "scena"
  ]
  node [
    id 163
    label "muzeum"
  ]
  node [
    id 164
    label "impreza"
  ]
  node [
    id 165
    label "Arsena&#322;"
  ]
  node [
    id 166
    label "akcja"
  ]
  node [
    id 167
    label "wystawienie"
  ]
  node [
    id 168
    label "wst&#281;p"
  ]
  node [
    id 169
    label "kurator"
  ]
  node [
    id 170
    label "operacja"
  ]
  node [
    id 171
    label "strona_&#347;wiata"
  ]
  node [
    id 172
    label "kolekcja"
  ]
  node [
    id 173
    label "wernisa&#380;"
  ]
  node [
    id 174
    label "Agropromocja"
  ]
  node [
    id 175
    label "wystawa"
  ]
  node [
    id 176
    label "wspinaczka"
  ]
  node [
    id 177
    label "Pokrowskoje"
  ]
  node [
    id 178
    label "Grzybowo"
  ]
  node [
    id 179
    label "nasada"
  ]
  node [
    id 180
    label "Go&#322;&#261;bek"
  ]
  node [
    id 181
    label "Rog&#243;w"
  ]
  node [
    id 182
    label "Gr&#243;dek"
  ]
  node [
    id 183
    label "Turlej"
  ]
  node [
    id 184
    label "Nowy_Korczyn"
  ]
  node [
    id 185
    label "G&#243;rczyn"
  ]
  node [
    id 186
    label "kwiatostan"
  ]
  node [
    id 187
    label "zbi&#243;r"
  ]
  node [
    id 188
    label "Rejowiec"
  ]
  node [
    id 189
    label "ochrona"
  ]
  node [
    id 190
    label "Falenty"
  ]
  node [
    id 191
    label "Sobk&#243;w"
  ]
  node [
    id 192
    label "strza&#322;a"
  ]
  node [
    id 193
    label "&#346;mie&#322;&#243;w"
  ]
  node [
    id 194
    label "wojownik"
  ]
  node [
    id 195
    label "Wieniec-Zdr&#243;j"
  ]
  node [
    id 196
    label "Izbica"
  ]
  node [
    id 197
    label "Babin"
  ]
  node [
    id 198
    label "skupienie"
  ]
  node [
    id 199
    label "grupa"
  ]
  node [
    id 200
    label "Antoniewo"
  ]
  node [
    id 201
    label "crew"
  ]
  node [
    id 202
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 203
    label "dru&#380;yna"
  ]
  node [
    id 204
    label "W&#243;lka"
  ]
  node [
    id 205
    label "Paszk&#243;w"
  ]
  node [
    id 206
    label "Gwiazdowo"
  ]
  node [
    id 207
    label "&#379;ebrowo"
  ]
  node [
    id 208
    label "Skotniki"
  ]
  node [
    id 209
    label "Grabowiec"
  ]
  node [
    id 210
    label "tarcza"
  ]
  node [
    id 211
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 212
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 213
    label "borowiec"
  ]
  node [
    id 214
    label "obstawienie"
  ]
  node [
    id 215
    label "chemical_bond"
  ]
  node [
    id 216
    label "obiekt"
  ]
  node [
    id 217
    label "formacja"
  ]
  node [
    id 218
    label "obstawianie"
  ]
  node [
    id 219
    label "transportacja"
  ]
  node [
    id 220
    label "obstawia&#263;"
  ]
  node [
    id 221
    label "ubezpieczenie"
  ]
  node [
    id 222
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 223
    label "congestion"
  ]
  node [
    id 224
    label "uwaga"
  ]
  node [
    id 225
    label "z&#322;&#261;czenie"
  ]
  node [
    id 226
    label "agglomeration"
  ]
  node [
    id 227
    label "przegrupowanie"
  ]
  node [
    id 228
    label "concentration"
  ]
  node [
    id 229
    label "po&#322;&#261;czenie"
  ]
  node [
    id 230
    label "zgromadzenie"
  ]
  node [
    id 231
    label "kupienie"
  ]
  node [
    id 232
    label "poj&#281;cie"
  ]
  node [
    id 233
    label "pakiet_klimatyczny"
  ]
  node [
    id 234
    label "uprawianie"
  ]
  node [
    id 235
    label "collection"
  ]
  node [
    id 236
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 237
    label "gathering"
  ]
  node [
    id 238
    label "album"
  ]
  node [
    id 239
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 240
    label "praca_rolnicza"
  ]
  node [
    id 241
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 242
    label "sum"
  ]
  node [
    id 243
    label "egzemplarz"
  ]
  node [
    id 244
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 245
    label "series"
  ]
  node [
    id 246
    label "dane"
  ]
  node [
    id 247
    label "asymilowa&#263;"
  ]
  node [
    id 248
    label "kompozycja"
  ]
  node [
    id 249
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 250
    label "type"
  ]
  node [
    id 251
    label "cz&#261;steczka"
  ]
  node [
    id 252
    label "gromada"
  ]
  node [
    id 253
    label "specgrupa"
  ]
  node [
    id 254
    label "stage_set"
  ]
  node [
    id 255
    label "asymilowanie"
  ]
  node [
    id 256
    label "odm&#322;odzenie"
  ]
  node [
    id 257
    label "odm&#322;adza&#263;"
  ]
  node [
    id 258
    label "harcerze_starsi"
  ]
  node [
    id 259
    label "jednostka_systematyczna"
  ]
  node [
    id 260
    label "oddzia&#322;"
  ]
  node [
    id 261
    label "category"
  ]
  node [
    id 262
    label "liga"
  ]
  node [
    id 263
    label "&#346;wietliki"
  ]
  node [
    id 264
    label "formacja_geologiczna"
  ]
  node [
    id 265
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 266
    label "Eurogrupa"
  ]
  node [
    id 267
    label "Terranie"
  ]
  node [
    id 268
    label "odm&#322;adzanie"
  ]
  node [
    id 269
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 270
    label "Entuzjastki"
  ]
  node [
    id 271
    label "dublet"
  ]
  node [
    id 272
    label "pododdzia&#322;"
  ]
  node [
    id 273
    label "zast&#281;p"
  ]
  node [
    id 274
    label "szczep"
  ]
  node [
    id 275
    label "whole"
  ]
  node [
    id 276
    label "pluton"
  ]
  node [
    id 277
    label "force"
  ]
  node [
    id 278
    label "zesp&#243;&#322;"
  ]
  node [
    id 279
    label "Aurignac"
  ]
  node [
    id 280
    label "Opat&#243;wek"
  ]
  node [
    id 281
    label "Cecora"
  ]
  node [
    id 282
    label "osiedle"
  ]
  node [
    id 283
    label "Saint-Acheul"
  ]
  node [
    id 284
    label "Levallois-Perret"
  ]
  node [
    id 285
    label "Boulogne"
  ]
  node [
    id 286
    label "Sabaudia"
  ]
  node [
    id 287
    label "cz&#322;owiek"
  ]
  node [
    id 288
    label "nak&#322;adka"
  ]
  node [
    id 289
    label "jama_gard&#322;owa"
  ]
  node [
    id 290
    label "podstawa"
  ]
  node [
    id 291
    label "li&#347;&#263;"
  ]
  node [
    id 292
    label "rezonator"
  ]
  node [
    id 293
    label "&#380;o&#322;nierz"
  ]
  node [
    id 294
    label "walcz&#261;cy"
  ]
  node [
    id 295
    label "Pozna&#324;"
  ]
  node [
    id 296
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 297
    label "inflorescence"
  ]
  node [
    id 298
    label "o&#347;_kwiatowa"
  ]
  node [
    id 299
    label "ko&#322;czan"
  ]
  node [
    id 300
    label "bolt"
  ]
  node [
    id 301
    label "pie&#324;"
  ]
  node [
    id 302
    label "statecznik"
  ]
  node [
    id 303
    label "pocisk"
  ]
  node [
    id 304
    label "grot"
  ]
  node [
    id 305
    label "pojazd"
  ]
  node [
    id 306
    label "szybki"
  ]
  node [
    id 307
    label "informowa&#263;"
  ]
  node [
    id 308
    label "recall"
  ]
  node [
    id 309
    label "prompt"
  ]
  node [
    id 310
    label "u&#347;wiadamia&#263;"
  ]
  node [
    id 311
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 312
    label "explain"
  ]
  node [
    id 313
    label "powiada&#263;"
  ]
  node [
    id 314
    label "komunikowa&#263;"
  ]
  node [
    id 315
    label "inform"
  ]
  node [
    id 316
    label "dom"
  ]
  node [
    id 317
    label "substancja_mieszkaniowa"
  ]
  node [
    id 318
    label "rodzina"
  ]
  node [
    id 319
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 320
    label "siedziba"
  ]
  node [
    id 321
    label "dom_rodzinny"
  ]
  node [
    id 322
    label "budynek"
  ]
  node [
    id 323
    label "garderoba"
  ]
  node [
    id 324
    label "fratria"
  ]
  node [
    id 325
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 326
    label "stead"
  ]
  node [
    id 327
    label "instytucja"
  ]
  node [
    id 328
    label "wiecha"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 328
  ]
]
