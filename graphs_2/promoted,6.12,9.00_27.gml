graph [
  node [
    id 0
    label "wybuch&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "epidemia"
    origin "text"
  ]
  node [
    id 2
    label "odra"
    origin "text"
  ]
  node [
    id 3
    label "spo&#322;eczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#380;ydowski"
    origin "text"
  ]
  node [
    id 5
    label "londyn"
    origin "text"
  ]
  node [
    id 6
    label "atakowanie"
  ]
  node [
    id 7
    label "plaga"
  ]
  node [
    id 8
    label "atakowa&#263;"
  ]
  node [
    id 9
    label "trouble"
  ]
  node [
    id 10
    label "wydarzenie"
  ]
  node [
    id 11
    label "strike"
  ]
  node [
    id 12
    label "robi&#263;"
  ]
  node [
    id 13
    label "schorzenie"
  ]
  node [
    id 14
    label "dzia&#322;a&#263;"
  ]
  node [
    id 15
    label "ofensywny"
  ]
  node [
    id 16
    label "przewaga"
  ]
  node [
    id 17
    label "sport"
  ]
  node [
    id 18
    label "attack"
  ]
  node [
    id 19
    label "rozgrywa&#263;"
  ]
  node [
    id 20
    label "krytykowa&#263;"
  ]
  node [
    id 21
    label "walczy&#263;"
  ]
  node [
    id 22
    label "aim"
  ]
  node [
    id 23
    label "trouble_oneself"
  ]
  node [
    id 24
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 25
    label "napada&#263;"
  ]
  node [
    id 26
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 27
    label "m&#243;wi&#263;"
  ]
  node [
    id 28
    label "nast&#281;powa&#263;"
  ]
  node [
    id 29
    label "usi&#322;owa&#263;"
  ]
  node [
    id 30
    label "grasowanie"
  ]
  node [
    id 31
    label "napadanie"
  ]
  node [
    id 32
    label "rozgrywanie"
  ]
  node [
    id 33
    label "robienie"
  ]
  node [
    id 34
    label "k&#322;&#243;cenie_si&#281;"
  ]
  node [
    id 35
    label "polowanie"
  ]
  node [
    id 36
    label "walczenie"
  ]
  node [
    id 37
    label "usi&#322;owanie"
  ]
  node [
    id 38
    label "m&#243;wienie"
  ]
  node [
    id 39
    label "wyskakiwanie_z_g&#281;b&#261;"
  ]
  node [
    id 40
    label "pojawianie_si&#281;"
  ]
  node [
    id 41
    label "czynno&#347;&#263;"
  ]
  node [
    id 42
    label "krytykowanie"
  ]
  node [
    id 43
    label "torpedowanie"
  ]
  node [
    id 44
    label "szczucie"
  ]
  node [
    id 45
    label "przebywanie"
  ]
  node [
    id 46
    label "oddzia&#322;ywanie"
  ]
  node [
    id 47
    label "friction"
  ]
  node [
    id 48
    label "nast&#281;powanie"
  ]
  node [
    id 49
    label "granie"
  ]
  node [
    id 50
    label "wirus_odry"
  ]
  node [
    id 51
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 52
    label "choroba_somatyczna"
  ]
  node [
    id 53
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 54
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 55
    label "Fremeni"
  ]
  node [
    id 56
    label "facylitacja"
  ]
  node [
    id 57
    label "zbi&#243;r"
  ]
  node [
    id 58
    label "cywilizacja"
  ]
  node [
    id 59
    label "pole"
  ]
  node [
    id 60
    label "elita"
  ]
  node [
    id 61
    label "status"
  ]
  node [
    id 62
    label "aspo&#322;eczny"
  ]
  node [
    id 63
    label "ludzie_pracy"
  ]
  node [
    id 64
    label "pozaklasowy"
  ]
  node [
    id 65
    label "uwarstwienie"
  ]
  node [
    id 66
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 67
    label "community"
  ]
  node [
    id 68
    label "klasa"
  ]
  node [
    id 69
    label "kastowo&#347;&#263;"
  ]
  node [
    id 70
    label "pejsaty"
  ]
  node [
    id 71
    label "&#380;ydowsko"
  ]
  node [
    id 72
    label "semicki"
  ]
  node [
    id 73
    label "charakterystyczny"
  ]
  node [
    id 74
    label "parchaty"
  ]
  node [
    id 75
    label "moszaw"
  ]
  node [
    id 76
    label "charakterystycznie"
  ]
  node [
    id 77
    label "szczeg&#243;lny"
  ]
  node [
    id 78
    label "wyj&#261;tkowy"
  ]
  node [
    id 79
    label "typowy"
  ]
  node [
    id 80
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 81
    label "podobny"
  ]
  node [
    id 82
    label "egzotyczny"
  ]
  node [
    id 83
    label "semicko"
  ]
  node [
    id 84
    label "po_&#380;ydowsku"
  ]
  node [
    id 85
    label "Palestyna"
  ]
  node [
    id 86
    label "izraelski"
  ]
  node [
    id 87
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 88
    label "chory"
  ]
  node [
    id 89
    label "paskudny"
  ]
  node [
    id 90
    label "ow&#322;osiony"
  ]
  node [
    id 91
    label "&#347;wiatowy"
  ]
  node [
    id 92
    label "organizacja"
  ]
  node [
    id 93
    label "zdrowie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 92
    target 93
  ]
]
