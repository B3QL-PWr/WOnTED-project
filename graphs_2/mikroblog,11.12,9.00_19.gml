graph [
  node [
    id 0
    label "teza"
    origin "text"
  ]
  node [
    id 1
    label "czu&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dawny"
    origin "text"
  ]
  node [
    id 3
    label "klimat"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wi&#281;to"
    origin "text"
  ]
  node [
    id 5
    label "s&#261;d"
  ]
  node [
    id 6
    label "twierdzenie"
  ]
  node [
    id 7
    label "znaczenie"
  ]
  node [
    id 8
    label "poj&#281;cie"
  ]
  node [
    id 9
    label "dialektyka"
  ]
  node [
    id 10
    label "argument"
  ]
  node [
    id 11
    label "idea"
  ]
  node [
    id 12
    label "zesp&#243;&#322;"
  ]
  node [
    id 13
    label "podejrzany"
  ]
  node [
    id 14
    label "s&#261;downictwo"
  ]
  node [
    id 15
    label "system"
  ]
  node [
    id 16
    label "biuro"
  ]
  node [
    id 17
    label "wytw&#243;r"
  ]
  node [
    id 18
    label "court"
  ]
  node [
    id 19
    label "forum"
  ]
  node [
    id 20
    label "bronienie"
  ]
  node [
    id 21
    label "urz&#261;d"
  ]
  node [
    id 22
    label "wydarzenie"
  ]
  node [
    id 23
    label "oskar&#380;yciel"
  ]
  node [
    id 24
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 25
    label "skazany"
  ]
  node [
    id 26
    label "post&#281;powanie"
  ]
  node [
    id 27
    label "broni&#263;"
  ]
  node [
    id 28
    label "my&#347;l"
  ]
  node [
    id 29
    label "pods&#261;dny"
  ]
  node [
    id 30
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 31
    label "obrona"
  ]
  node [
    id 32
    label "wypowied&#378;"
  ]
  node [
    id 33
    label "instytucja"
  ]
  node [
    id 34
    label "antylogizm"
  ]
  node [
    id 35
    label "konektyw"
  ]
  node [
    id 36
    label "&#347;wiadek"
  ]
  node [
    id 37
    label "procesowicz"
  ]
  node [
    id 38
    label "strona"
  ]
  node [
    id 39
    label "pos&#322;uchanie"
  ]
  node [
    id 40
    label "skumanie"
  ]
  node [
    id 41
    label "orientacja"
  ]
  node [
    id 42
    label "teoria"
  ]
  node [
    id 43
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 44
    label "clasp"
  ]
  node [
    id 45
    label "przem&#243;wienie"
  ]
  node [
    id 46
    label "forma"
  ]
  node [
    id 47
    label "zorientowanie"
  ]
  node [
    id 48
    label "ideologia"
  ]
  node [
    id 49
    label "byt"
  ]
  node [
    id 50
    label "intelekt"
  ]
  node [
    id 51
    label "Kant"
  ]
  node [
    id 52
    label "p&#322;&#243;d"
  ]
  node [
    id 53
    label "cel"
  ]
  node [
    id 54
    label "istota"
  ]
  node [
    id 55
    label "pomys&#322;"
  ]
  node [
    id 56
    label "ideacja"
  ]
  node [
    id 57
    label "odk&#322;adanie"
  ]
  node [
    id 58
    label "condition"
  ]
  node [
    id 59
    label "liczenie"
  ]
  node [
    id 60
    label "stawianie"
  ]
  node [
    id 61
    label "bycie"
  ]
  node [
    id 62
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 63
    label "assay"
  ]
  node [
    id 64
    label "wskazywanie"
  ]
  node [
    id 65
    label "wyraz"
  ]
  node [
    id 66
    label "gravity"
  ]
  node [
    id 67
    label "weight"
  ]
  node [
    id 68
    label "command"
  ]
  node [
    id 69
    label "odgrywanie_roli"
  ]
  node [
    id 70
    label "informacja"
  ]
  node [
    id 71
    label "cecha"
  ]
  node [
    id 72
    label "okre&#347;lanie"
  ]
  node [
    id 73
    label "kto&#347;"
  ]
  node [
    id 74
    label "wyra&#380;enie"
  ]
  node [
    id 75
    label "koncepcja"
  ]
  node [
    id 76
    label "marksizm"
  ]
  node [
    id 77
    label "metoda"
  ]
  node [
    id 78
    label "logika"
  ]
  node [
    id 79
    label "uzasadnianie"
  ]
  node [
    id 80
    label "dialectics"
  ]
  node [
    id 81
    label "heglizm"
  ]
  node [
    id 82
    label "parametr"
  ]
  node [
    id 83
    label "operand"
  ]
  node [
    id 84
    label "dow&#243;d"
  ]
  node [
    id 85
    label "zmienna"
  ]
  node [
    id 86
    label "argumentacja"
  ]
  node [
    id 87
    label "rzecz"
  ]
  node [
    id 88
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 89
    label "alternatywa_Fredholma"
  ]
  node [
    id 90
    label "oznajmianie"
  ]
  node [
    id 91
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 92
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 93
    label "paradoks_Leontiefa"
  ]
  node [
    id 94
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 95
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 96
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 97
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 98
    label "twierdzenie_Pettisa"
  ]
  node [
    id 99
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 100
    label "twierdzenie_Maya"
  ]
  node [
    id 101
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 102
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 103
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 104
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 105
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 106
    label "zapewnianie"
  ]
  node [
    id 107
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 108
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 109
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 110
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 111
    label "twierdzenie_Stokesa"
  ]
  node [
    id 112
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 113
    label "twierdzenie_Cevy"
  ]
  node [
    id 114
    label "twierdzenie_Pascala"
  ]
  node [
    id 115
    label "proposition"
  ]
  node [
    id 116
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 117
    label "komunikowanie"
  ]
  node [
    id 118
    label "zasada"
  ]
  node [
    id 119
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 120
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 121
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 122
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 123
    label "postrzega&#263;"
  ]
  node [
    id 124
    label "przewidywa&#263;"
  ]
  node [
    id 125
    label "by&#263;"
  ]
  node [
    id 126
    label "smell"
  ]
  node [
    id 127
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 128
    label "uczuwa&#263;"
  ]
  node [
    id 129
    label "spirit"
  ]
  node [
    id 130
    label "doznawa&#263;"
  ]
  node [
    id 131
    label "anticipate"
  ]
  node [
    id 132
    label "perceive"
  ]
  node [
    id 133
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 134
    label "punkt_widzenia"
  ]
  node [
    id 135
    label "obacza&#263;"
  ]
  node [
    id 136
    label "widzie&#263;"
  ]
  node [
    id 137
    label "dochodzi&#263;"
  ]
  node [
    id 138
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 139
    label "notice"
  ]
  node [
    id 140
    label "doj&#347;&#263;"
  ]
  node [
    id 141
    label "os&#261;dza&#263;"
  ]
  node [
    id 142
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 143
    label "mie&#263;_miejsce"
  ]
  node [
    id 144
    label "equal"
  ]
  node [
    id 145
    label "trwa&#263;"
  ]
  node [
    id 146
    label "chodzi&#263;"
  ]
  node [
    id 147
    label "si&#281;ga&#263;"
  ]
  node [
    id 148
    label "stan"
  ]
  node [
    id 149
    label "obecno&#347;&#263;"
  ]
  node [
    id 150
    label "stand"
  ]
  node [
    id 151
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 152
    label "uczestniczy&#263;"
  ]
  node [
    id 153
    label "zamierza&#263;"
  ]
  node [
    id 154
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 155
    label "hurt"
  ]
  node [
    id 156
    label "wali&#263;"
  ]
  node [
    id 157
    label "jeba&#263;"
  ]
  node [
    id 158
    label "pachnie&#263;"
  ]
  node [
    id 159
    label "proceed"
  ]
  node [
    id 160
    label "przestarza&#322;y"
  ]
  node [
    id 161
    label "odleg&#322;y"
  ]
  node [
    id 162
    label "przesz&#322;y"
  ]
  node [
    id 163
    label "od_dawna"
  ]
  node [
    id 164
    label "poprzedni"
  ]
  node [
    id 165
    label "dawno"
  ]
  node [
    id 166
    label "d&#322;ugoletni"
  ]
  node [
    id 167
    label "anachroniczny"
  ]
  node [
    id 168
    label "dawniej"
  ]
  node [
    id 169
    label "niegdysiejszy"
  ]
  node [
    id 170
    label "wcze&#347;niejszy"
  ]
  node [
    id 171
    label "kombatant"
  ]
  node [
    id 172
    label "stary"
  ]
  node [
    id 173
    label "poprzednio"
  ]
  node [
    id 174
    label "zestarzenie_si&#281;"
  ]
  node [
    id 175
    label "starzenie_si&#281;"
  ]
  node [
    id 176
    label "archaicznie"
  ]
  node [
    id 177
    label "zgrzybienie"
  ]
  node [
    id 178
    label "niedzisiejszy"
  ]
  node [
    id 179
    label "staro&#347;wiecki"
  ]
  node [
    id 180
    label "przestarzale"
  ]
  node [
    id 181
    label "anachronicznie"
  ]
  node [
    id 182
    label "niezgodny"
  ]
  node [
    id 183
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 184
    label "ongi&#347;"
  ]
  node [
    id 185
    label "odlegle"
  ]
  node [
    id 186
    label "delikatny"
  ]
  node [
    id 187
    label "r&#243;&#380;ny"
  ]
  node [
    id 188
    label "daleko"
  ]
  node [
    id 189
    label "s&#322;aby"
  ]
  node [
    id 190
    label "daleki"
  ]
  node [
    id 191
    label "oddalony"
  ]
  node [
    id 192
    label "obcy"
  ]
  node [
    id 193
    label "nieobecny"
  ]
  node [
    id 194
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 195
    label "ojciec"
  ]
  node [
    id 196
    label "nienowoczesny"
  ]
  node [
    id 197
    label "gruba_ryba"
  ]
  node [
    id 198
    label "staro"
  ]
  node [
    id 199
    label "m&#261;&#380;"
  ]
  node [
    id 200
    label "starzy"
  ]
  node [
    id 201
    label "dotychczasowy"
  ]
  node [
    id 202
    label "p&#243;&#378;ny"
  ]
  node [
    id 203
    label "charakterystyczny"
  ]
  node [
    id 204
    label "brat"
  ]
  node [
    id 205
    label "po_staro&#347;wiecku"
  ]
  node [
    id 206
    label "zwierzchnik"
  ]
  node [
    id 207
    label "znajomy"
  ]
  node [
    id 208
    label "starczo"
  ]
  node [
    id 209
    label "dojrza&#322;y"
  ]
  node [
    id 210
    label "wcze&#347;niej"
  ]
  node [
    id 211
    label "miniony"
  ]
  node [
    id 212
    label "ostatni"
  ]
  node [
    id 213
    label "d&#322;ugi"
  ]
  node [
    id 214
    label "wieloletni"
  ]
  node [
    id 215
    label "kiedy&#347;"
  ]
  node [
    id 216
    label "d&#322;ugotrwale"
  ]
  node [
    id 217
    label "dawnie"
  ]
  node [
    id 218
    label "wyjadacz"
  ]
  node [
    id 219
    label "weteran"
  ]
  node [
    id 220
    label "atmosfera"
  ]
  node [
    id 221
    label "styl"
  ]
  node [
    id 222
    label "trzonek"
  ]
  node [
    id 223
    label "reakcja"
  ]
  node [
    id 224
    label "narz&#281;dzie"
  ]
  node [
    id 225
    label "spos&#243;b"
  ]
  node [
    id 226
    label "zbi&#243;r"
  ]
  node [
    id 227
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 228
    label "zachowanie"
  ]
  node [
    id 229
    label "stylik"
  ]
  node [
    id 230
    label "dyscyplina_sportowa"
  ]
  node [
    id 231
    label "handle"
  ]
  node [
    id 232
    label "stroke"
  ]
  node [
    id 233
    label "line"
  ]
  node [
    id 234
    label "napisa&#263;"
  ]
  node [
    id 235
    label "charakter"
  ]
  node [
    id 236
    label "natural_language"
  ]
  node [
    id 237
    label "pisa&#263;"
  ]
  node [
    id 238
    label "kanon"
  ]
  node [
    id 239
    label "behawior"
  ]
  node [
    id 240
    label "Mazowsze"
  ]
  node [
    id 241
    label "odm&#322;adzanie"
  ]
  node [
    id 242
    label "&#346;wietliki"
  ]
  node [
    id 243
    label "whole"
  ]
  node [
    id 244
    label "skupienie"
  ]
  node [
    id 245
    label "The_Beatles"
  ]
  node [
    id 246
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 247
    label "odm&#322;adza&#263;"
  ]
  node [
    id 248
    label "zabudowania"
  ]
  node [
    id 249
    label "group"
  ]
  node [
    id 250
    label "zespolik"
  ]
  node [
    id 251
    label "schorzenie"
  ]
  node [
    id 252
    label "ro&#347;lina"
  ]
  node [
    id 253
    label "grupa"
  ]
  node [
    id 254
    label "Depeche_Mode"
  ]
  node [
    id 255
    label "batch"
  ]
  node [
    id 256
    label "odm&#322;odzenie"
  ]
  node [
    id 257
    label "troposfera"
  ]
  node [
    id 258
    label "obiekt_naturalny"
  ]
  node [
    id 259
    label "metasfera"
  ]
  node [
    id 260
    label "atmosferyki"
  ]
  node [
    id 261
    label "homosfera"
  ]
  node [
    id 262
    label "powietrznia"
  ]
  node [
    id 263
    label "jonosfera"
  ]
  node [
    id 264
    label "planeta"
  ]
  node [
    id 265
    label "termosfera"
  ]
  node [
    id 266
    label "egzosfera"
  ]
  node [
    id 267
    label "heterosfera"
  ]
  node [
    id 268
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 269
    label "tropopauza"
  ]
  node [
    id 270
    label "kwas"
  ]
  node [
    id 271
    label "powietrze"
  ]
  node [
    id 272
    label "stratosfera"
  ]
  node [
    id 273
    label "pow&#322;oka"
  ]
  node [
    id 274
    label "mezosfera"
  ]
  node [
    id 275
    label "Ziemia"
  ]
  node [
    id 276
    label "mezopauza"
  ]
  node [
    id 277
    label "atmosphere"
  ]
  node [
    id 278
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 279
    label "ramadan"
  ]
  node [
    id 280
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 281
    label "Nowy_Rok"
  ]
  node [
    id 282
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 283
    label "czas"
  ]
  node [
    id 284
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 285
    label "Barb&#243;rka"
  ]
  node [
    id 286
    label "poprzedzanie"
  ]
  node [
    id 287
    label "czasoprzestrze&#324;"
  ]
  node [
    id 288
    label "laba"
  ]
  node [
    id 289
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 290
    label "chronometria"
  ]
  node [
    id 291
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 292
    label "rachuba_czasu"
  ]
  node [
    id 293
    label "przep&#322;ywanie"
  ]
  node [
    id 294
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 295
    label "czasokres"
  ]
  node [
    id 296
    label "odczyt"
  ]
  node [
    id 297
    label "chwila"
  ]
  node [
    id 298
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 299
    label "dzieje"
  ]
  node [
    id 300
    label "kategoria_gramatyczna"
  ]
  node [
    id 301
    label "poprzedzenie"
  ]
  node [
    id 302
    label "trawienie"
  ]
  node [
    id 303
    label "pochodzi&#263;"
  ]
  node [
    id 304
    label "period"
  ]
  node [
    id 305
    label "okres_czasu"
  ]
  node [
    id 306
    label "poprzedza&#263;"
  ]
  node [
    id 307
    label "schy&#322;ek"
  ]
  node [
    id 308
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 309
    label "odwlekanie_si&#281;"
  ]
  node [
    id 310
    label "zegar"
  ]
  node [
    id 311
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 312
    label "czwarty_wymiar"
  ]
  node [
    id 313
    label "pochodzenie"
  ]
  node [
    id 314
    label "koniugacja"
  ]
  node [
    id 315
    label "Zeitgeist"
  ]
  node [
    id 316
    label "trawi&#263;"
  ]
  node [
    id 317
    label "pogoda"
  ]
  node [
    id 318
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 319
    label "poprzedzi&#263;"
  ]
  node [
    id 320
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 321
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 322
    label "time_period"
  ]
  node [
    id 323
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 324
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 325
    label "patos"
  ]
  node [
    id 326
    label "egzaltacja"
  ]
  node [
    id 327
    label "grudzie&#324;"
  ]
  node [
    id 328
    label "g&#243;rnik"
  ]
  node [
    id 329
    label "comber"
  ]
  node [
    id 330
    label "saum"
  ]
  node [
    id 331
    label "&#347;cis&#322;y_post"
  ]
  node [
    id 332
    label "miesi&#261;c"
  ]
  node [
    id 333
    label "ma&#322;y_bajram"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
]
