graph [
  node [
    id 0
    label "zgadza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "rzadko"
    origin "text"
  ]
  node [
    id 3
    label "zgodzi&#263;"
  ]
  node [
    id 4
    label "zgadzanie"
  ]
  node [
    id 5
    label "assent"
  ]
  node [
    id 6
    label "zatrudnia&#263;"
  ]
  node [
    id 7
    label "bra&#263;"
  ]
  node [
    id 8
    label "zatrudni&#263;"
  ]
  node [
    id 9
    label "zatrudnianie"
  ]
  node [
    id 10
    label "undertake"
  ]
  node [
    id 11
    label "pomoc"
  ]
  node [
    id 12
    label "zgodzenie"
  ]
  node [
    id 13
    label "thinly"
  ]
  node [
    id 14
    label "czasami"
  ]
  node [
    id 15
    label "rzadki"
  ]
  node [
    id 16
    label "niezwykle"
  ]
  node [
    id 17
    label "lu&#378;ny"
  ]
  node [
    id 18
    label "rzedni&#281;cie"
  ]
  node [
    id 19
    label "zrzedni&#281;cie"
  ]
  node [
    id 20
    label "rozrzedzanie"
  ]
  node [
    id 21
    label "rozwodnienie"
  ]
  node [
    id 22
    label "rozrzedzenie"
  ]
  node [
    id 23
    label "niezwyk&#322;y"
  ]
  node [
    id 24
    label "rozwadnianie"
  ]
  node [
    id 25
    label "lu&#378;no"
  ]
  node [
    id 26
    label "osobny"
  ]
  node [
    id 27
    label "rozdeptanie"
  ]
  node [
    id 28
    label "daleki"
  ]
  node [
    id 29
    label "rozdeptywanie"
  ]
  node [
    id 30
    label "&#322;atwy"
  ]
  node [
    id 31
    label "swobodny"
  ]
  node [
    id 32
    label "nieformalny"
  ]
  node [
    id 33
    label "dodatkowy"
  ]
  node [
    id 34
    label "przyjemny"
  ]
  node [
    id 35
    label "beztroski"
  ]
  node [
    id 36
    label "nieokre&#347;lony"
  ]
  node [
    id 37
    label "nie&#347;cis&#322;y"
  ]
  node [
    id 38
    label "nieregularny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
]
