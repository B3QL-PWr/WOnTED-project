graph [
  node [
    id 0
    label "normalny"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "tokijski"
    origin "text"
  ]
  node [
    id 3
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 4
    label "dla"
    origin "text"
  ]
  node [
    id 5
    label "student"
    origin "text"
  ]
  node [
    id 6
    label "polonistyka"
    origin "text"
  ]
  node [
    id 7
    label "cz&#281;sty"
  ]
  node [
    id 8
    label "przeci&#281;tny"
  ]
  node [
    id 9
    label "oczywisty"
  ]
  node [
    id 10
    label "zdr&#243;w"
  ]
  node [
    id 11
    label "zwykle"
  ]
  node [
    id 12
    label "zwyczajny"
  ]
  node [
    id 13
    label "zwyczajnie"
  ]
  node [
    id 14
    label "prawid&#322;owy"
  ]
  node [
    id 15
    label "normalnie"
  ]
  node [
    id 16
    label "okre&#347;lony"
  ]
  node [
    id 17
    label "wiadomy"
  ]
  node [
    id 18
    label "cz&#281;sto"
  ]
  node [
    id 19
    label "s&#322;uszny"
  ]
  node [
    id 20
    label "symetryczny"
  ]
  node [
    id 21
    label "prawid&#322;owo"
  ]
  node [
    id 22
    label "po&#380;&#261;dany"
  ]
  node [
    id 23
    label "dobry"
  ]
  node [
    id 24
    label "oczewisty"
  ]
  node [
    id 25
    label "oswojony"
  ]
  node [
    id 26
    label "na&#322;o&#380;ny"
  ]
  node [
    id 27
    label "zdrowy"
  ]
  node [
    id 28
    label "orientacyjny"
  ]
  node [
    id 29
    label "przeci&#281;tnie"
  ]
  node [
    id 30
    label "&#347;rednio"
  ]
  node [
    id 31
    label "taki_sobie"
  ]
  node [
    id 32
    label "zwyk&#322;y"
  ]
  node [
    id 33
    label "poprostu"
  ]
  node [
    id 34
    label "Stanford"
  ]
  node [
    id 35
    label "academy"
  ]
  node [
    id 36
    label "Harvard"
  ]
  node [
    id 37
    label "uczelnia"
  ]
  node [
    id 38
    label "wydzia&#322;"
  ]
  node [
    id 39
    label "Sorbona"
  ]
  node [
    id 40
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 41
    label "Princeton"
  ]
  node [
    id 42
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 43
    label "ku&#378;nia"
  ]
  node [
    id 44
    label "warsztat"
  ]
  node [
    id 45
    label "instytucja"
  ]
  node [
    id 46
    label "fabryka"
  ]
  node [
    id 47
    label "kanclerz"
  ]
  node [
    id 48
    label "szko&#322;a"
  ]
  node [
    id 49
    label "podkanclerz"
  ]
  node [
    id 50
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 51
    label "miasteczko_studenckie"
  ]
  node [
    id 52
    label "kwestura"
  ]
  node [
    id 53
    label "wyk&#322;adanie"
  ]
  node [
    id 54
    label "rektorat"
  ]
  node [
    id 55
    label "school"
  ]
  node [
    id 56
    label "senat"
  ]
  node [
    id 57
    label "promotorstwo"
  ]
  node [
    id 58
    label "jednostka_organizacyjna"
  ]
  node [
    id 59
    label "relation"
  ]
  node [
    id 60
    label "urz&#261;d"
  ]
  node [
    id 61
    label "whole"
  ]
  node [
    id 62
    label "miejsce_pracy"
  ]
  node [
    id 63
    label "podsekcja"
  ]
  node [
    id 64
    label "insourcing"
  ]
  node [
    id 65
    label "politechnika"
  ]
  node [
    id 66
    label "katedra"
  ]
  node [
    id 67
    label "ministerstwo"
  ]
  node [
    id 68
    label "dzia&#322;"
  ]
  node [
    id 69
    label "paryski"
  ]
  node [
    id 70
    label "ameryka&#324;ski"
  ]
  node [
    id 71
    label "indeks"
  ]
  node [
    id 72
    label "s&#322;uchacz"
  ]
  node [
    id 73
    label "immatrykulowanie"
  ]
  node [
    id 74
    label "absolwent"
  ]
  node [
    id 75
    label "immatrykulowa&#263;"
  ]
  node [
    id 76
    label "akademik"
  ]
  node [
    id 77
    label "tutor"
  ]
  node [
    id 78
    label "odbiorca"
  ]
  node [
    id 79
    label "cz&#322;owiek"
  ]
  node [
    id 80
    label "pracownik_naukowy"
  ]
  node [
    id 81
    label "akademia"
  ]
  node [
    id 82
    label "cz&#322;onek"
  ]
  node [
    id 83
    label "przedstawiciel"
  ]
  node [
    id 84
    label "artysta"
  ]
  node [
    id 85
    label "dom"
  ]
  node [
    id 86
    label "reprezentant"
  ]
  node [
    id 87
    label "ucze&#324;"
  ]
  node [
    id 88
    label "nauczyciel"
  ]
  node [
    id 89
    label "nauczyciel_akademicki"
  ]
  node [
    id 90
    label "opiekun"
  ]
  node [
    id 91
    label "wychowawca"
  ]
  node [
    id 92
    label "mentor"
  ]
  node [
    id 93
    label "znak_pisarski"
  ]
  node [
    id 94
    label "directory"
  ]
  node [
    id 95
    label "wska&#378;nik"
  ]
  node [
    id 96
    label "za&#347;wiadczenie"
  ]
  node [
    id 97
    label "indeks_Lernera"
  ]
  node [
    id 98
    label "spis"
  ]
  node [
    id 99
    label "album"
  ]
  node [
    id 100
    label "zapisanie"
  ]
  node [
    id 101
    label "zapisywanie"
  ]
  node [
    id 102
    label "zapisywa&#263;"
  ]
  node [
    id 103
    label "zapisa&#263;"
  ]
  node [
    id 104
    label "filologia"
  ]
  node [
    id 105
    label "slawistyka"
  ]
  node [
    id 106
    label "nauka_humanistyczna"
  ]
  node [
    id 107
    label "sorabistyka"
  ]
  node [
    id 108
    label "rutenistyka"
  ]
  node [
    id 109
    label "serbochorwatystyka"
  ]
  node [
    id 110
    label "neofilologia"
  ]
  node [
    id 111
    label "s&#322;owacystyka"
  ]
  node [
    id 112
    label "rusycystyka"
  ]
  node [
    id 113
    label "bu&#322;garystyka"
  ]
  node [
    id 114
    label "nauka"
  ]
  node [
    id 115
    label "bohemistyka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
]
