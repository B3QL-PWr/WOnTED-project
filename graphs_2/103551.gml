graph [
  node [
    id 0
    label "scena"
    origin "text"
  ]
  node [
    id 1
    label "sznurownia"
  ]
  node [
    id 2
    label "nadscenie"
  ]
  node [
    id 3
    label "fragment"
  ]
  node [
    id 4
    label "film"
  ]
  node [
    id 5
    label "k&#322;&#243;tnia"
  ]
  node [
    id 6
    label "horyzont"
  ]
  node [
    id 7
    label "sztuka"
  ]
  node [
    id 8
    label "epizod"
  ]
  node [
    id 9
    label "sphere"
  ]
  node [
    id 10
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 11
    label "kiesze&#324;"
  ]
  node [
    id 12
    label "podest"
  ]
  node [
    id 13
    label "antyteatr"
  ]
  node [
    id 14
    label "wydarzenie"
  ]
  node [
    id 15
    label "przedstawia&#263;"
  ]
  node [
    id 16
    label "instytucja"
  ]
  node [
    id 17
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 18
    label "przedstawienie"
  ]
  node [
    id 19
    label "budka_suflera"
  ]
  node [
    id 20
    label "akt"
  ]
  node [
    id 21
    label "proscenium"
  ]
  node [
    id 22
    label "teren"
  ]
  node [
    id 23
    label "przedstawianie"
  ]
  node [
    id 24
    label "podwy&#380;szenie"
  ]
  node [
    id 25
    label "stadium"
  ]
  node [
    id 26
    label "dramaturgy"
  ]
  node [
    id 27
    label "widzownia"
  ]
  node [
    id 28
    label "kurtyna"
  ]
  node [
    id 29
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 30
    label "ods&#322;ona"
  ]
  node [
    id 31
    label "scenariusz"
  ]
  node [
    id 32
    label "fortel"
  ]
  node [
    id 33
    label "kultura"
  ]
  node [
    id 34
    label "utw&#243;r"
  ]
  node [
    id 35
    label "kobieta"
  ]
  node [
    id 36
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 37
    label "ambala&#380;"
  ]
  node [
    id 38
    label "Apollo"
  ]
  node [
    id 39
    label "egzemplarz"
  ]
  node [
    id 40
    label "didaskalia"
  ]
  node [
    id 41
    label "czyn"
  ]
  node [
    id 42
    label "przedmiot"
  ]
  node [
    id 43
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 44
    label "turn"
  ]
  node [
    id 45
    label "towar"
  ]
  node [
    id 46
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 47
    label "head"
  ]
  node [
    id 48
    label "cz&#322;owiek"
  ]
  node [
    id 49
    label "kultura_duchowa"
  ]
  node [
    id 50
    label "theatrical_performance"
  ]
  node [
    id 51
    label "pokaz"
  ]
  node [
    id 52
    label "pr&#243;bowanie"
  ]
  node [
    id 53
    label "sprawno&#347;&#263;"
  ]
  node [
    id 54
    label "jednostka"
  ]
  node [
    id 55
    label "ilo&#347;&#263;"
  ]
  node [
    id 56
    label "environment"
  ]
  node [
    id 57
    label "scenografia"
  ]
  node [
    id 58
    label "realizacja"
  ]
  node [
    id 59
    label "rola"
  ]
  node [
    id 60
    label "Faust"
  ]
  node [
    id 61
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 62
    label "przedstawi&#263;"
  ]
  node [
    id 63
    label "poj&#281;cie"
  ]
  node [
    id 64
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 65
    label "afiliowa&#263;"
  ]
  node [
    id 66
    label "establishment"
  ]
  node [
    id 67
    label "zamyka&#263;"
  ]
  node [
    id 68
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 69
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 70
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 71
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 72
    label "standard"
  ]
  node [
    id 73
    label "Fundusze_Unijne"
  ]
  node [
    id 74
    label "biuro"
  ]
  node [
    id 75
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 76
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 77
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 78
    label "zamykanie"
  ]
  node [
    id 79
    label "organizacja"
  ]
  node [
    id 80
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 81
    label "osoba_prawna"
  ]
  node [
    id 82
    label "urz&#261;d"
  ]
  node [
    id 83
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 84
    label "moment"
  ]
  node [
    id 85
    label "odcinek"
  ]
  node [
    id 86
    label "w&#261;tek"
  ]
  node [
    id 87
    label "episode"
  ]
  node [
    id 88
    label "szczeg&#243;&#322;"
  ]
  node [
    id 89
    label "sequence"
  ]
  node [
    id 90
    label "motyw"
  ]
  node [
    id 91
    label "proces_ekonomiczny"
  ]
  node [
    id 92
    label "miejsce"
  ]
  node [
    id 93
    label "erecting"
  ]
  node [
    id 94
    label "powi&#281;kszenie"
  ]
  node [
    id 95
    label "activity"
  ]
  node [
    id 96
    label "absolutorium"
  ]
  node [
    id 97
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 98
    label "dzia&#322;anie"
  ]
  node [
    id 99
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 100
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 101
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 102
    label "zakres"
  ]
  node [
    id 103
    label "miejsce_pracy"
  ]
  node [
    id 104
    label "przyroda"
  ]
  node [
    id 105
    label "obszar"
  ]
  node [
    id 106
    label "wymiar"
  ]
  node [
    id 107
    label "nation"
  ]
  node [
    id 108
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 109
    label "w&#322;adza"
  ]
  node [
    id 110
    label "kontekst"
  ]
  node [
    id 111
    label "krajobraz"
  ]
  node [
    id 112
    label "rozognienie_si&#281;"
  ]
  node [
    id 113
    label "spina"
  ]
  node [
    id 114
    label "rozognia&#263;_si&#281;"
  ]
  node [
    id 115
    label "swar"
  ]
  node [
    id 116
    label "rozognianie_si&#281;"
  ]
  node [
    id 117
    label "gniew"
  ]
  node [
    id 118
    label "sp&#243;r"
  ]
  node [
    id 119
    label "row"
  ]
  node [
    id 120
    label "rozogni&#263;_si&#281;"
  ]
  node [
    id 121
    label "zadra"
  ]
  node [
    id 122
    label "charakter"
  ]
  node [
    id 123
    label "przebiegni&#281;cie"
  ]
  node [
    id 124
    label "przebiec"
  ]
  node [
    id 125
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 126
    label "fabu&#322;a"
  ]
  node [
    id 127
    label "czynno&#347;&#263;"
  ]
  node [
    id 128
    label "szczelina"
  ]
  node [
    id 129
    label "kiesa"
  ]
  node [
    id 130
    label "dzi&#261;s&#322;o"
  ]
  node [
    id 131
    label "kiejda"
  ]
  node [
    id 132
    label "element"
  ]
  node [
    id 133
    label "stomatologia"
  ]
  node [
    id 134
    label "schorzenie"
  ]
  node [
    id 135
    label "wn&#281;ka"
  ]
  node [
    id 136
    label "kapita&#322;"
  ]
  node [
    id 137
    label "bag"
  ]
  node [
    id 138
    label "zas&#322;ona"
  ]
  node [
    id 139
    label "bariera"
  ]
  node [
    id 140
    label "mur"
  ]
  node [
    id 141
    label "fortyfikacja"
  ]
  node [
    id 142
    label "kortyna"
  ]
  node [
    id 143
    label "teatr"
  ]
  node [
    id 144
    label "bastion"
  ]
  node [
    id 145
    label "dekoracja"
  ]
  node [
    id 146
    label "d&#378;wignia"
  ]
  node [
    id 147
    label "class"
  ]
  node [
    id 148
    label "scope"
  ]
  node [
    id 149
    label "&#347;ciana"
  ]
  node [
    id 150
    label "granica"
  ]
  node [
    id 151
    label "huczek"
  ]
  node [
    id 152
    label "ty&#322;"
  ]
  node [
    id 153
    label "grupa"
  ]
  node [
    id 154
    label "podium"
  ]
  node [
    id 155
    label "widownia"
  ]
  node [
    id 156
    label "podstawa"
  ]
  node [
    id 157
    label "schody"
  ]
  node [
    id 158
    label "element_konstrukcyjny"
  ]
  node [
    id 159
    label "platform"
  ]
  node [
    id 160
    label "bycie"
  ]
  node [
    id 161
    label "pokazywanie"
  ]
  node [
    id 162
    label "wyst&#281;powanie"
  ]
  node [
    id 163
    label "opisywanie"
  ]
  node [
    id 164
    label "presentation"
  ]
  node [
    id 165
    label "ukazywanie"
  ]
  node [
    id 166
    label "zapoznawanie"
  ]
  node [
    id 167
    label "demonstrowanie"
  ]
  node [
    id 168
    label "podawanie"
  ]
  node [
    id 169
    label "obgadywanie"
  ]
  node [
    id 170
    label "display"
  ]
  node [
    id 171
    label "representation"
  ]
  node [
    id 172
    label "podanie"
  ]
  node [
    id 173
    label "opisanie"
  ]
  node [
    id 174
    label "pokazanie"
  ]
  node [
    id 175
    label "wyst&#261;pienie"
  ]
  node [
    id 176
    label "ukazanie"
  ]
  node [
    id 177
    label "zapoznanie"
  ]
  node [
    id 178
    label "posta&#263;"
  ]
  node [
    id 179
    label "malarstwo"
  ]
  node [
    id 180
    label "obgadanie"
  ]
  node [
    id 181
    label "spos&#243;b"
  ]
  node [
    id 182
    label "report"
  ]
  node [
    id 183
    label "exhibit"
  ]
  node [
    id 184
    label "narration"
  ]
  node [
    id 185
    label "cyrk"
  ]
  node [
    id 186
    label "wytw&#243;r"
  ]
  node [
    id 187
    label "zademonstrowanie"
  ]
  node [
    id 188
    label "szko&#322;a"
  ]
  node [
    id 189
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 190
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 191
    label "podawa&#263;"
  ]
  node [
    id 192
    label "zg&#322;asza&#263;"
  ]
  node [
    id 193
    label "ukazywa&#263;"
  ]
  node [
    id 194
    label "stanowi&#263;"
  ]
  node [
    id 195
    label "demonstrowa&#263;"
  ]
  node [
    id 196
    label "typify"
  ]
  node [
    id 197
    label "attest"
  ]
  node [
    id 198
    label "represent"
  ]
  node [
    id 199
    label "pokazywa&#263;"
  ]
  node [
    id 200
    label "opisywa&#263;"
  ]
  node [
    id 201
    label "zapoznawa&#263;"
  ]
  node [
    id 202
    label "b&#322;ona"
  ]
  node [
    id 203
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 204
    label "trawiarnia"
  ]
  node [
    id 205
    label "emulsja_fotograficzna"
  ]
  node [
    id 206
    label "rozbieg&#243;wka"
  ]
  node [
    id 207
    label "sklejarka"
  ]
  node [
    id 208
    label "odczuli&#263;"
  ]
  node [
    id 209
    label "filmoteka"
  ]
  node [
    id 210
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 211
    label "dorobek"
  ]
  node [
    id 212
    label "animatronika"
  ]
  node [
    id 213
    label "napisy"
  ]
  node [
    id 214
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 215
    label "blik"
  ]
  node [
    id 216
    label "odczula&#263;"
  ]
  node [
    id 217
    label "odczulenie"
  ]
  node [
    id 218
    label "photograph"
  ]
  node [
    id 219
    label "klatka"
  ]
  node [
    id 220
    label "ta&#347;ma"
  ]
  node [
    id 221
    label "uj&#281;cie"
  ]
  node [
    id 222
    label "anamorfoza"
  ]
  node [
    id 223
    label "czo&#322;&#243;wka"
  ]
  node [
    id 224
    label "odczulanie"
  ]
  node [
    id 225
    label "postprodukcja"
  ]
  node [
    id 226
    label "muza"
  ]
  node [
    id 227
    label "block"
  ]
  node [
    id 228
    label "ty&#322;&#243;wka"
  ]
  node [
    id 229
    label "erotyka"
  ]
  node [
    id 230
    label "podniecanie"
  ]
  node [
    id 231
    label "po&#380;ycie"
  ]
  node [
    id 232
    label "dokument"
  ]
  node [
    id 233
    label "baraszki"
  ]
  node [
    id 234
    label "numer"
  ]
  node [
    id 235
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 236
    label "certificate"
  ]
  node [
    id 237
    label "ruch_frykcyjny"
  ]
  node [
    id 238
    label "ontologia"
  ]
  node [
    id 239
    label "wzw&#243;d"
  ]
  node [
    id 240
    label "seks"
  ]
  node [
    id 241
    label "pozycja_misjonarska"
  ]
  node [
    id 242
    label "rozmna&#380;anie"
  ]
  node [
    id 243
    label "arystotelizm"
  ]
  node [
    id 244
    label "zwyczaj"
  ]
  node [
    id 245
    label "urzeczywistnienie"
  ]
  node [
    id 246
    label "z&#322;&#261;czenie"
  ]
  node [
    id 247
    label "funkcja"
  ]
  node [
    id 248
    label "act"
  ]
  node [
    id 249
    label "imisja"
  ]
  node [
    id 250
    label "podniecenie"
  ]
  node [
    id 251
    label "podnieca&#263;"
  ]
  node [
    id 252
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 253
    label "fascyku&#322;"
  ]
  node [
    id 254
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 255
    label "nago&#347;&#263;"
  ]
  node [
    id 256
    label "gra_wst&#281;pna"
  ]
  node [
    id 257
    label "po&#380;&#261;danie"
  ]
  node [
    id 258
    label "podnieci&#263;"
  ]
  node [
    id 259
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 260
    label "na_pieska"
  ]
  node [
    id 261
    label "jednostka_miary_u&#380;ywana_w_&#380;egludze"
  ]
  node [
    id 262
    label "czas"
  ]
  node [
    id 263
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 264
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 265
    label "publiczno&#347;&#263;"
  ]
  node [
    id 266
    label "arena"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
]
