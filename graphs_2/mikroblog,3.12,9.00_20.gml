graph [
  node [
    id 0
    label "racja"
    origin "text"
  ]
  node [
    id 1
    label "s&#261;d"
  ]
  node [
    id 2
    label "prawda"
  ]
  node [
    id 3
    label "porcja"
  ]
  node [
    id 4
    label "argument"
  ]
  node [
    id 5
    label "przyczyna"
  ]
  node [
    id 6
    label "parametr"
  ]
  node [
    id 7
    label "operand"
  ]
  node [
    id 8
    label "dow&#243;d"
  ]
  node [
    id 9
    label "zmienna"
  ]
  node [
    id 10
    label "argumentacja"
  ]
  node [
    id 11
    label "rzecz"
  ]
  node [
    id 12
    label "zas&#243;b"
  ]
  node [
    id 13
    label "ilo&#347;&#263;"
  ]
  node [
    id 14
    label "&#380;o&#322;d"
  ]
  node [
    id 15
    label "za&#322;o&#380;enie"
  ]
  node [
    id 16
    label "nieprawdziwy"
  ]
  node [
    id 17
    label "prawdziwy"
  ]
  node [
    id 18
    label "truth"
  ]
  node [
    id 19
    label "realia"
  ]
  node [
    id 20
    label "zesp&#243;&#322;"
  ]
  node [
    id 21
    label "podejrzany"
  ]
  node [
    id 22
    label "s&#261;downictwo"
  ]
  node [
    id 23
    label "system"
  ]
  node [
    id 24
    label "biuro"
  ]
  node [
    id 25
    label "wytw&#243;r"
  ]
  node [
    id 26
    label "court"
  ]
  node [
    id 27
    label "forum"
  ]
  node [
    id 28
    label "bronienie"
  ]
  node [
    id 29
    label "urz&#261;d"
  ]
  node [
    id 30
    label "wydarzenie"
  ]
  node [
    id 31
    label "oskar&#380;yciel"
  ]
  node [
    id 32
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 33
    label "skazany"
  ]
  node [
    id 34
    label "post&#281;powanie"
  ]
  node [
    id 35
    label "broni&#263;"
  ]
  node [
    id 36
    label "my&#347;l"
  ]
  node [
    id 37
    label "pods&#261;dny"
  ]
  node [
    id 38
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 39
    label "obrona"
  ]
  node [
    id 40
    label "wypowied&#378;"
  ]
  node [
    id 41
    label "instytucja"
  ]
  node [
    id 42
    label "antylogizm"
  ]
  node [
    id 43
    label "konektyw"
  ]
  node [
    id 44
    label "&#347;wiadek"
  ]
  node [
    id 45
    label "procesowicz"
  ]
  node [
    id 46
    label "strona"
  ]
  node [
    id 47
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 48
    label "subject"
  ]
  node [
    id 49
    label "czynnik"
  ]
  node [
    id 50
    label "matuszka"
  ]
  node [
    id 51
    label "rezultat"
  ]
  node [
    id 52
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 53
    label "geneza"
  ]
  node [
    id 54
    label "poci&#261;ganie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
]
