graph [
  node [
    id 0
    label "art"
    origin "text"
  ]
  node [
    id 1
    label "decyzja"
    origin "text"
  ]
  node [
    id 2
    label "sprawa"
    origin "text"
  ]
  node [
    id 3
    label "ekwiwalent"
    origin "text"
  ]
  node [
    id 4
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ten"
    origin "text"
  ]
  node [
    id 6
    label "wyp&#322;aca&#263;"
    origin "text"
  ]
  node [
    id 7
    label "jednostka"
    origin "text"
  ]
  node [
    id 8
    label "organizacyjny"
    origin "text"
  ]
  node [
    id 9
    label "zus"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 12
    label "prawo"
    origin "text"
  ]
  node [
    id 13
    label "emerytura"
    origin "text"
  ]
  node [
    id 14
    label "lub"
    origin "text"
  ]
  node [
    id 15
    label "renta"
    origin "text"
  ]
  node [
    id 16
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 17
    label "management"
  ]
  node [
    id 18
    label "resolution"
  ]
  node [
    id 19
    label "wytw&#243;r"
  ]
  node [
    id 20
    label "zdecydowanie"
  ]
  node [
    id 21
    label "dokument"
  ]
  node [
    id 22
    label "zapis"
  ]
  node [
    id 23
    label "&#347;wiadectwo"
  ]
  node [
    id 24
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 25
    label "parafa"
  ]
  node [
    id 26
    label "plik"
  ]
  node [
    id 27
    label "raport&#243;wka"
  ]
  node [
    id 28
    label "utw&#243;r"
  ]
  node [
    id 29
    label "record"
  ]
  node [
    id 30
    label "registratura"
  ]
  node [
    id 31
    label "dokumentacja"
  ]
  node [
    id 32
    label "fascyku&#322;"
  ]
  node [
    id 33
    label "artyku&#322;"
  ]
  node [
    id 34
    label "writing"
  ]
  node [
    id 35
    label "sygnatariusz"
  ]
  node [
    id 36
    label "przedmiot"
  ]
  node [
    id 37
    label "p&#322;&#243;d"
  ]
  node [
    id 38
    label "work"
  ]
  node [
    id 39
    label "rezultat"
  ]
  node [
    id 40
    label "zesp&#243;&#322;"
  ]
  node [
    id 41
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 42
    label "pewnie"
  ]
  node [
    id 43
    label "zdecydowany"
  ]
  node [
    id 44
    label "zauwa&#380;alnie"
  ]
  node [
    id 45
    label "oddzia&#322;anie"
  ]
  node [
    id 46
    label "podj&#281;cie"
  ]
  node [
    id 47
    label "cecha"
  ]
  node [
    id 48
    label "resoluteness"
  ]
  node [
    id 49
    label "judgment"
  ]
  node [
    id 50
    label "zrobienie"
  ]
  node [
    id 51
    label "kognicja"
  ]
  node [
    id 52
    label "object"
  ]
  node [
    id 53
    label "rozprawa"
  ]
  node [
    id 54
    label "temat"
  ]
  node [
    id 55
    label "wydarzenie"
  ]
  node [
    id 56
    label "szczeg&#243;&#322;"
  ]
  node [
    id 57
    label "proposition"
  ]
  node [
    id 58
    label "przes&#322;anka"
  ]
  node [
    id 59
    label "rzecz"
  ]
  node [
    id 60
    label "idea"
  ]
  node [
    id 61
    label "przebiec"
  ]
  node [
    id 62
    label "charakter"
  ]
  node [
    id 63
    label "czynno&#347;&#263;"
  ]
  node [
    id 64
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 65
    label "motyw"
  ]
  node [
    id 66
    label "przebiegni&#281;cie"
  ]
  node [
    id 67
    label "fabu&#322;a"
  ]
  node [
    id 68
    label "ideologia"
  ]
  node [
    id 69
    label "byt"
  ]
  node [
    id 70
    label "intelekt"
  ]
  node [
    id 71
    label "Kant"
  ]
  node [
    id 72
    label "cel"
  ]
  node [
    id 73
    label "poj&#281;cie"
  ]
  node [
    id 74
    label "istota"
  ]
  node [
    id 75
    label "pomys&#322;"
  ]
  node [
    id 76
    label "ideacja"
  ]
  node [
    id 77
    label "wpadni&#281;cie"
  ]
  node [
    id 78
    label "mienie"
  ]
  node [
    id 79
    label "przyroda"
  ]
  node [
    id 80
    label "obiekt"
  ]
  node [
    id 81
    label "kultura"
  ]
  node [
    id 82
    label "wpa&#347;&#263;"
  ]
  node [
    id 83
    label "wpadanie"
  ]
  node [
    id 84
    label "wpada&#263;"
  ]
  node [
    id 85
    label "s&#261;d"
  ]
  node [
    id 86
    label "rozumowanie"
  ]
  node [
    id 87
    label "opracowanie"
  ]
  node [
    id 88
    label "proces"
  ]
  node [
    id 89
    label "obrady"
  ]
  node [
    id 90
    label "cytat"
  ]
  node [
    id 91
    label "tekst"
  ]
  node [
    id 92
    label "obja&#347;nienie"
  ]
  node [
    id 93
    label "s&#261;dzenie"
  ]
  node [
    id 94
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 95
    label "niuansowa&#263;"
  ]
  node [
    id 96
    label "element"
  ]
  node [
    id 97
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "sk&#322;adnik"
  ]
  node [
    id 99
    label "zniuansowa&#263;"
  ]
  node [
    id 100
    label "fakt"
  ]
  node [
    id 101
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 102
    label "przyczyna"
  ]
  node [
    id 103
    label "wnioskowanie"
  ]
  node [
    id 104
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 105
    label "wyraz_pochodny"
  ]
  node [
    id 106
    label "zboczenie"
  ]
  node [
    id 107
    label "om&#243;wienie"
  ]
  node [
    id 108
    label "omawia&#263;"
  ]
  node [
    id 109
    label "fraza"
  ]
  node [
    id 110
    label "tre&#347;&#263;"
  ]
  node [
    id 111
    label "entity"
  ]
  node [
    id 112
    label "forum"
  ]
  node [
    id 113
    label "topik"
  ]
  node [
    id 114
    label "tematyka"
  ]
  node [
    id 115
    label "w&#261;tek"
  ]
  node [
    id 116
    label "zbaczanie"
  ]
  node [
    id 117
    label "forma"
  ]
  node [
    id 118
    label "om&#243;wi&#263;"
  ]
  node [
    id 119
    label "omawianie"
  ]
  node [
    id 120
    label "melodia"
  ]
  node [
    id 121
    label "otoczka"
  ]
  node [
    id 122
    label "zbacza&#263;"
  ]
  node [
    id 123
    label "zboczy&#263;"
  ]
  node [
    id 124
    label "odpowiednik"
  ]
  node [
    id 125
    label "odmiana"
  ]
  node [
    id 126
    label "robi&#263;"
  ]
  node [
    id 127
    label "mie&#263;_miejsce"
  ]
  node [
    id 128
    label "plon"
  ]
  node [
    id 129
    label "give"
  ]
  node [
    id 130
    label "surrender"
  ]
  node [
    id 131
    label "kojarzy&#263;"
  ]
  node [
    id 132
    label "d&#378;wi&#281;k"
  ]
  node [
    id 133
    label "impart"
  ]
  node [
    id 134
    label "dawa&#263;"
  ]
  node [
    id 135
    label "reszta"
  ]
  node [
    id 136
    label "zapach"
  ]
  node [
    id 137
    label "wydawnictwo"
  ]
  node [
    id 138
    label "wiano"
  ]
  node [
    id 139
    label "produkcja"
  ]
  node [
    id 140
    label "wprowadza&#263;"
  ]
  node [
    id 141
    label "podawa&#263;"
  ]
  node [
    id 142
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 143
    label "ujawnia&#263;"
  ]
  node [
    id 144
    label "placard"
  ]
  node [
    id 145
    label "powierza&#263;"
  ]
  node [
    id 146
    label "denuncjowa&#263;"
  ]
  node [
    id 147
    label "tajemnica"
  ]
  node [
    id 148
    label "panna_na_wydaniu"
  ]
  node [
    id 149
    label "wytwarza&#263;"
  ]
  node [
    id 150
    label "train"
  ]
  node [
    id 151
    label "przekazywa&#263;"
  ]
  node [
    id 152
    label "dostarcza&#263;"
  ]
  node [
    id 153
    label "&#322;adowa&#263;"
  ]
  node [
    id 154
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 155
    label "przeznacza&#263;"
  ]
  node [
    id 156
    label "traktowa&#263;"
  ]
  node [
    id 157
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 158
    label "obiecywa&#263;"
  ]
  node [
    id 159
    label "odst&#281;powa&#263;"
  ]
  node [
    id 160
    label "tender"
  ]
  node [
    id 161
    label "rap"
  ]
  node [
    id 162
    label "umieszcza&#263;"
  ]
  node [
    id 163
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 164
    label "t&#322;uc"
  ]
  node [
    id 165
    label "render"
  ]
  node [
    id 166
    label "wpiernicza&#263;"
  ]
  node [
    id 167
    label "exsert"
  ]
  node [
    id 168
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 169
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 170
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 171
    label "p&#322;aci&#263;"
  ]
  node [
    id 172
    label "hold_out"
  ]
  node [
    id 173
    label "nalewa&#263;"
  ]
  node [
    id 174
    label "zezwala&#263;"
  ]
  node [
    id 175
    label "hold"
  ]
  node [
    id 176
    label "organizowa&#263;"
  ]
  node [
    id 177
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 178
    label "czyni&#263;"
  ]
  node [
    id 179
    label "stylizowa&#263;"
  ]
  node [
    id 180
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 181
    label "falowa&#263;"
  ]
  node [
    id 182
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 183
    label "peddle"
  ]
  node [
    id 184
    label "praca"
  ]
  node [
    id 185
    label "wydala&#263;"
  ]
  node [
    id 186
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 187
    label "tentegowa&#263;"
  ]
  node [
    id 188
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 189
    label "urz&#261;dza&#263;"
  ]
  node [
    id 190
    label "oszukiwa&#263;"
  ]
  node [
    id 191
    label "ukazywa&#263;"
  ]
  node [
    id 192
    label "przerabia&#263;"
  ]
  node [
    id 193
    label "act"
  ]
  node [
    id 194
    label "post&#281;powa&#263;"
  ]
  node [
    id 195
    label "rynek"
  ]
  node [
    id 196
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 197
    label "wprawia&#263;"
  ]
  node [
    id 198
    label "zaczyna&#263;"
  ]
  node [
    id 199
    label "wpisywa&#263;"
  ]
  node [
    id 200
    label "wchodzi&#263;"
  ]
  node [
    id 201
    label "take"
  ]
  node [
    id 202
    label "zapoznawa&#263;"
  ]
  node [
    id 203
    label "powodowa&#263;"
  ]
  node [
    id 204
    label "inflict"
  ]
  node [
    id 205
    label "schodzi&#263;"
  ]
  node [
    id 206
    label "induct"
  ]
  node [
    id 207
    label "begin"
  ]
  node [
    id 208
    label "doprowadza&#263;"
  ]
  node [
    id 209
    label "create"
  ]
  node [
    id 210
    label "donosi&#263;"
  ]
  node [
    id 211
    label "inform"
  ]
  node [
    id 212
    label "demaskator"
  ]
  node [
    id 213
    label "dostrzega&#263;"
  ]
  node [
    id 214
    label "objawia&#263;"
  ]
  node [
    id 215
    label "unwrap"
  ]
  node [
    id 216
    label "informowa&#263;"
  ]
  node [
    id 217
    label "indicate"
  ]
  node [
    id 218
    label "zaskakiwa&#263;"
  ]
  node [
    id 219
    label "cover"
  ]
  node [
    id 220
    label "rozumie&#263;"
  ]
  node [
    id 221
    label "swat"
  ]
  node [
    id 222
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 223
    label "relate"
  ]
  node [
    id 224
    label "wyznawa&#263;"
  ]
  node [
    id 225
    label "oddawa&#263;"
  ]
  node [
    id 226
    label "confide"
  ]
  node [
    id 227
    label "zleca&#263;"
  ]
  node [
    id 228
    label "ufa&#263;"
  ]
  node [
    id 229
    label "command"
  ]
  node [
    id 230
    label "grant"
  ]
  node [
    id 231
    label "tenis"
  ]
  node [
    id 232
    label "deal"
  ]
  node [
    id 233
    label "stawia&#263;"
  ]
  node [
    id 234
    label "rozgrywa&#263;"
  ]
  node [
    id 235
    label "kelner"
  ]
  node [
    id 236
    label "siatk&#243;wka"
  ]
  node [
    id 237
    label "jedzenie"
  ]
  node [
    id 238
    label "faszerowa&#263;"
  ]
  node [
    id 239
    label "introduce"
  ]
  node [
    id 240
    label "serwowa&#263;"
  ]
  node [
    id 241
    label "kwota"
  ]
  node [
    id 242
    label "wydanie"
  ]
  node [
    id 243
    label "remainder"
  ]
  node [
    id 244
    label "pozosta&#322;y"
  ]
  node [
    id 245
    label "wyda&#263;"
  ]
  node [
    id 246
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 247
    label "impreza"
  ]
  node [
    id 248
    label "realizacja"
  ]
  node [
    id 249
    label "tingel-tangel"
  ]
  node [
    id 250
    label "numer"
  ]
  node [
    id 251
    label "monta&#380;"
  ]
  node [
    id 252
    label "postprodukcja"
  ]
  node [
    id 253
    label "performance"
  ]
  node [
    id 254
    label "fabrication"
  ]
  node [
    id 255
    label "zbi&#243;r"
  ]
  node [
    id 256
    label "product"
  ]
  node [
    id 257
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 258
    label "uzysk"
  ]
  node [
    id 259
    label "rozw&#243;j"
  ]
  node [
    id 260
    label "odtworzenie"
  ]
  node [
    id 261
    label "dorobek"
  ]
  node [
    id 262
    label "kreacja"
  ]
  node [
    id 263
    label "trema"
  ]
  node [
    id 264
    label "creation"
  ]
  node [
    id 265
    label "kooperowa&#263;"
  ]
  node [
    id 266
    label "return"
  ]
  node [
    id 267
    label "metr"
  ]
  node [
    id 268
    label "naturalia"
  ]
  node [
    id 269
    label "wypaplanie"
  ]
  node [
    id 270
    label "enigmat"
  ]
  node [
    id 271
    label "spos&#243;b"
  ]
  node [
    id 272
    label "wiedza"
  ]
  node [
    id 273
    label "zachowanie"
  ]
  node [
    id 274
    label "zachowywanie"
  ]
  node [
    id 275
    label "secret"
  ]
  node [
    id 276
    label "obowi&#261;zek"
  ]
  node [
    id 277
    label "dyskrecja"
  ]
  node [
    id 278
    label "informacja"
  ]
  node [
    id 279
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 280
    label "taj&#324;"
  ]
  node [
    id 281
    label "zachowa&#263;"
  ]
  node [
    id 282
    label "zachowywa&#263;"
  ]
  node [
    id 283
    label "posa&#380;ek"
  ]
  node [
    id 284
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 285
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 286
    label "debit"
  ]
  node [
    id 287
    label "redaktor"
  ]
  node [
    id 288
    label "druk"
  ]
  node [
    id 289
    label "publikacja"
  ]
  node [
    id 290
    label "redakcja"
  ]
  node [
    id 291
    label "szata_graficzna"
  ]
  node [
    id 292
    label "firma"
  ]
  node [
    id 293
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 294
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 295
    label "poster"
  ]
  node [
    id 296
    label "phone"
  ]
  node [
    id 297
    label "zjawisko"
  ]
  node [
    id 298
    label "intonacja"
  ]
  node [
    id 299
    label "note"
  ]
  node [
    id 300
    label "onomatopeja"
  ]
  node [
    id 301
    label "modalizm"
  ]
  node [
    id 302
    label "nadlecenie"
  ]
  node [
    id 303
    label "sound"
  ]
  node [
    id 304
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 305
    label "solmizacja"
  ]
  node [
    id 306
    label "seria"
  ]
  node [
    id 307
    label "dobiec"
  ]
  node [
    id 308
    label "transmiter"
  ]
  node [
    id 309
    label "heksachord"
  ]
  node [
    id 310
    label "akcent"
  ]
  node [
    id 311
    label "repetycja"
  ]
  node [
    id 312
    label "brzmienie"
  ]
  node [
    id 313
    label "liczba_kwantowa"
  ]
  node [
    id 314
    label "kosmetyk"
  ]
  node [
    id 315
    label "ciasto"
  ]
  node [
    id 316
    label "aromat"
  ]
  node [
    id 317
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 318
    label "puff"
  ]
  node [
    id 319
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 320
    label "przyprawa"
  ]
  node [
    id 321
    label "upojno&#347;&#263;"
  ]
  node [
    id 322
    label "owiewanie"
  ]
  node [
    id 323
    label "smak"
  ]
  node [
    id 324
    label "okre&#347;lony"
  ]
  node [
    id 325
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 326
    label "wiadomy"
  ]
  node [
    id 327
    label "refund"
  ]
  node [
    id 328
    label "odwzajemnia&#263;_si&#281;"
  ]
  node [
    id 329
    label "pay"
  ]
  node [
    id 330
    label "osi&#261;ga&#263;"
  ]
  node [
    id 331
    label "buli&#263;"
  ]
  node [
    id 332
    label "przyswoi&#263;"
  ]
  node [
    id 333
    label "ludzko&#347;&#263;"
  ]
  node [
    id 334
    label "one"
  ]
  node [
    id 335
    label "ewoluowanie"
  ]
  node [
    id 336
    label "supremum"
  ]
  node [
    id 337
    label "skala"
  ]
  node [
    id 338
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 339
    label "przyswajanie"
  ]
  node [
    id 340
    label "wyewoluowanie"
  ]
  node [
    id 341
    label "reakcja"
  ]
  node [
    id 342
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 343
    label "przeliczy&#263;"
  ]
  node [
    id 344
    label "wyewoluowa&#263;"
  ]
  node [
    id 345
    label "ewoluowa&#263;"
  ]
  node [
    id 346
    label "matematyka"
  ]
  node [
    id 347
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 348
    label "rzut"
  ]
  node [
    id 349
    label "liczba_naturalna"
  ]
  node [
    id 350
    label "czynnik_biotyczny"
  ]
  node [
    id 351
    label "g&#322;owa"
  ]
  node [
    id 352
    label "figura"
  ]
  node [
    id 353
    label "individual"
  ]
  node [
    id 354
    label "portrecista"
  ]
  node [
    id 355
    label "przyswaja&#263;"
  ]
  node [
    id 356
    label "przyswojenie"
  ]
  node [
    id 357
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 358
    label "profanum"
  ]
  node [
    id 359
    label "mikrokosmos"
  ]
  node [
    id 360
    label "starzenie_si&#281;"
  ]
  node [
    id 361
    label "duch"
  ]
  node [
    id 362
    label "przeliczanie"
  ]
  node [
    id 363
    label "osoba"
  ]
  node [
    id 364
    label "oddzia&#322;ywanie"
  ]
  node [
    id 365
    label "antropochoria"
  ]
  node [
    id 366
    label "funkcja"
  ]
  node [
    id 367
    label "homo_sapiens"
  ]
  node [
    id 368
    label "przelicza&#263;"
  ]
  node [
    id 369
    label "infimum"
  ]
  node [
    id 370
    label "przeliczenie"
  ]
  node [
    id 371
    label "pos&#322;uchanie"
  ]
  node [
    id 372
    label "skumanie"
  ]
  node [
    id 373
    label "orientacja"
  ]
  node [
    id 374
    label "teoria"
  ]
  node [
    id 375
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 376
    label "clasp"
  ]
  node [
    id 377
    label "przem&#243;wienie"
  ]
  node [
    id 378
    label "zorientowanie"
  ]
  node [
    id 379
    label "co&#347;"
  ]
  node [
    id 380
    label "budynek"
  ]
  node [
    id 381
    label "thing"
  ]
  node [
    id 382
    label "program"
  ]
  node [
    id 383
    label "strona"
  ]
  node [
    id 384
    label "Chocho&#322;"
  ]
  node [
    id 385
    label "Herkules_Poirot"
  ]
  node [
    id 386
    label "Edyp"
  ]
  node [
    id 387
    label "parali&#380;owa&#263;"
  ]
  node [
    id 388
    label "Harry_Potter"
  ]
  node [
    id 389
    label "Casanova"
  ]
  node [
    id 390
    label "Gargantua"
  ]
  node [
    id 391
    label "Zgredek"
  ]
  node [
    id 392
    label "Winnetou"
  ]
  node [
    id 393
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 394
    label "posta&#263;"
  ]
  node [
    id 395
    label "Dulcynea"
  ]
  node [
    id 396
    label "kategoria_gramatyczna"
  ]
  node [
    id 397
    label "person"
  ]
  node [
    id 398
    label "Sherlock_Holmes"
  ]
  node [
    id 399
    label "Quasimodo"
  ]
  node [
    id 400
    label "Plastu&#347;"
  ]
  node [
    id 401
    label "Faust"
  ]
  node [
    id 402
    label "Wallenrod"
  ]
  node [
    id 403
    label "Dwukwiat"
  ]
  node [
    id 404
    label "koniugacja"
  ]
  node [
    id 405
    label "Don_Juan"
  ]
  node [
    id 406
    label "Don_Kiszot"
  ]
  node [
    id 407
    label "Hamlet"
  ]
  node [
    id 408
    label "Werter"
  ]
  node [
    id 409
    label "Szwejk"
  ]
  node [
    id 410
    label "integer"
  ]
  node [
    id 411
    label "liczba"
  ]
  node [
    id 412
    label "zlewanie_si&#281;"
  ]
  node [
    id 413
    label "ilo&#347;&#263;"
  ]
  node [
    id 414
    label "uk&#322;ad"
  ]
  node [
    id 415
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 416
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 417
    label "pe&#322;ny"
  ]
  node [
    id 418
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 419
    label "Rzym_Zachodni"
  ]
  node [
    id 420
    label "whole"
  ]
  node [
    id 421
    label "Rzym_Wschodni"
  ]
  node [
    id 422
    label "urz&#261;dzenie"
  ]
  node [
    id 423
    label "masztab"
  ]
  node [
    id 424
    label "kreska"
  ]
  node [
    id 425
    label "podzia&#322;ka"
  ]
  node [
    id 426
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 427
    label "wielko&#347;&#263;"
  ]
  node [
    id 428
    label "zero"
  ]
  node [
    id 429
    label "interwa&#322;"
  ]
  node [
    id 430
    label "przymiar"
  ]
  node [
    id 431
    label "struktura"
  ]
  node [
    id 432
    label "sfera"
  ]
  node [
    id 433
    label "dominanta"
  ]
  node [
    id 434
    label "tetrachord"
  ]
  node [
    id 435
    label "scale"
  ]
  node [
    id 436
    label "przedzia&#322;"
  ]
  node [
    id 437
    label "podzakres"
  ]
  node [
    id 438
    label "proporcja"
  ]
  node [
    id 439
    label "dziedzina"
  ]
  node [
    id 440
    label "part"
  ]
  node [
    id 441
    label "rejestr"
  ]
  node [
    id 442
    label "subdominanta"
  ]
  node [
    id 443
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 444
    label "cz&#322;owiek"
  ]
  node [
    id 445
    label "atom"
  ]
  node [
    id 446
    label "odbicie"
  ]
  node [
    id 447
    label "Ziemia"
  ]
  node [
    id 448
    label "kosmos"
  ]
  node [
    id 449
    label "miniatura"
  ]
  node [
    id 450
    label "czyn"
  ]
  node [
    id 451
    label "addytywno&#347;&#263;"
  ]
  node [
    id 452
    label "function"
  ]
  node [
    id 453
    label "zastosowanie"
  ]
  node [
    id 454
    label "funkcjonowanie"
  ]
  node [
    id 455
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 456
    label "powierzanie"
  ]
  node [
    id 457
    label "przeciwdziedzina"
  ]
  node [
    id 458
    label "awansowa&#263;"
  ]
  node [
    id 459
    label "wakowa&#263;"
  ]
  node [
    id 460
    label "znaczenie"
  ]
  node [
    id 461
    label "postawi&#263;"
  ]
  node [
    id 462
    label "awansowanie"
  ]
  node [
    id 463
    label "wymienienie"
  ]
  node [
    id 464
    label "przerachowanie"
  ]
  node [
    id 465
    label "skontrolowanie"
  ]
  node [
    id 466
    label "count"
  ]
  node [
    id 467
    label "sprawdza&#263;"
  ]
  node [
    id 468
    label "zmienia&#263;"
  ]
  node [
    id 469
    label "przerachowywa&#263;"
  ]
  node [
    id 470
    label "ograniczenie"
  ]
  node [
    id 471
    label "armia"
  ]
  node [
    id 472
    label "nawr&#243;t_choroby"
  ]
  node [
    id 473
    label "potomstwo"
  ]
  node [
    id 474
    label "odwzorowanie"
  ]
  node [
    id 475
    label "rysunek"
  ]
  node [
    id 476
    label "scene"
  ]
  node [
    id 477
    label "throw"
  ]
  node [
    id 478
    label "float"
  ]
  node [
    id 479
    label "punkt"
  ]
  node [
    id 480
    label "projection"
  ]
  node [
    id 481
    label "injection"
  ]
  node [
    id 482
    label "blow"
  ]
  node [
    id 483
    label "ruch"
  ]
  node [
    id 484
    label "k&#322;ad"
  ]
  node [
    id 485
    label "mold"
  ]
  node [
    id 486
    label "sprawdzanie"
  ]
  node [
    id 487
    label "zast&#281;powanie"
  ]
  node [
    id 488
    label "przerachowywanie"
  ]
  node [
    id 489
    label "rachunek_operatorowy"
  ]
  node [
    id 490
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 491
    label "kryptologia"
  ]
  node [
    id 492
    label "logicyzm"
  ]
  node [
    id 493
    label "logika"
  ]
  node [
    id 494
    label "matematyka_czysta"
  ]
  node [
    id 495
    label "forsing"
  ]
  node [
    id 496
    label "modelowanie_matematyczne"
  ]
  node [
    id 497
    label "matma"
  ]
  node [
    id 498
    label "teoria_katastrof"
  ]
  node [
    id 499
    label "kierunek"
  ]
  node [
    id 500
    label "fizyka_matematyczna"
  ]
  node [
    id 501
    label "teoria_graf&#243;w"
  ]
  node [
    id 502
    label "rachunki"
  ]
  node [
    id 503
    label "topologia_algebraiczna"
  ]
  node [
    id 504
    label "matematyka_stosowana"
  ]
  node [
    id 505
    label "sprawdzi&#263;"
  ]
  node [
    id 506
    label "change"
  ]
  node [
    id 507
    label "przerachowa&#263;"
  ]
  node [
    id 508
    label "zmieni&#263;"
  ]
  node [
    id 509
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 510
    label "organizm"
  ]
  node [
    id 511
    label "translate"
  ]
  node [
    id 512
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 513
    label "pobra&#263;"
  ]
  node [
    id 514
    label "thrill"
  ]
  node [
    id 515
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 516
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 517
    label "pobranie"
  ]
  node [
    id 518
    label "wyniesienie"
  ]
  node [
    id 519
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 520
    label "assimilation"
  ]
  node [
    id 521
    label "emotion"
  ]
  node [
    id 522
    label "nauczenie_si&#281;"
  ]
  node [
    id 523
    label "zaczerpni&#281;cie"
  ]
  node [
    id 524
    label "mechanizm_obronny"
  ]
  node [
    id 525
    label "convention"
  ]
  node [
    id 526
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 527
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 528
    label "czerpanie"
  ]
  node [
    id 529
    label "uczenie_si&#281;"
  ]
  node [
    id 530
    label "pobieranie"
  ]
  node [
    id 531
    label "acquisition"
  ]
  node [
    id 532
    label "od&#380;ywianie"
  ]
  node [
    id 533
    label "wynoszenie"
  ]
  node [
    id 534
    label "absorption"
  ]
  node [
    id 535
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 536
    label "react"
  ]
  node [
    id 537
    label "reaction"
  ]
  node [
    id 538
    label "rozmowa"
  ]
  node [
    id 539
    label "response"
  ]
  node [
    id 540
    label "respondent"
  ]
  node [
    id 541
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 542
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 543
    label "treat"
  ]
  node [
    id 544
    label "czerpa&#263;"
  ]
  node [
    id 545
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 546
    label "pobiera&#263;"
  ]
  node [
    id 547
    label "rede"
  ]
  node [
    id 548
    label "pryncypa&#322;"
  ]
  node [
    id 549
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 550
    label "kszta&#322;t"
  ]
  node [
    id 551
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 552
    label "kierowa&#263;"
  ]
  node [
    id 553
    label "alkohol"
  ]
  node [
    id 554
    label "zdolno&#347;&#263;"
  ]
  node [
    id 555
    label "&#380;ycie"
  ]
  node [
    id 556
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 557
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 558
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 559
    label "sztuka"
  ]
  node [
    id 560
    label "dekiel"
  ]
  node [
    id 561
    label "ro&#347;lina"
  ]
  node [
    id 562
    label "&#347;ci&#281;cie"
  ]
  node [
    id 563
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 564
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 565
    label "&#347;ci&#281;gno"
  ]
  node [
    id 566
    label "noosfera"
  ]
  node [
    id 567
    label "byd&#322;o"
  ]
  node [
    id 568
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 569
    label "makrocefalia"
  ]
  node [
    id 570
    label "ucho"
  ]
  node [
    id 571
    label "g&#243;ra"
  ]
  node [
    id 572
    label "m&#243;zg"
  ]
  node [
    id 573
    label "kierownictwo"
  ]
  node [
    id 574
    label "fryzura"
  ]
  node [
    id 575
    label "umys&#322;"
  ]
  node [
    id 576
    label "cia&#322;o"
  ]
  node [
    id 577
    label "cz&#322;onek"
  ]
  node [
    id 578
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 579
    label "czaszka"
  ]
  node [
    id 580
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 581
    label "powodowanie"
  ]
  node [
    id 582
    label "hipnotyzowanie"
  ]
  node [
    id 583
    label "&#347;lad"
  ]
  node [
    id 584
    label "docieranie"
  ]
  node [
    id 585
    label "natural_process"
  ]
  node [
    id 586
    label "reakcja_chemiczna"
  ]
  node [
    id 587
    label "wdzieranie_si&#281;"
  ]
  node [
    id 588
    label "lobbysta"
  ]
  node [
    id 589
    label "allochoria"
  ]
  node [
    id 590
    label "wygl&#261;d"
  ]
  node [
    id 591
    label "fotograf"
  ]
  node [
    id 592
    label "malarz"
  ]
  node [
    id 593
    label "artysta"
  ]
  node [
    id 594
    label "charakterystyka"
  ]
  node [
    id 595
    label "p&#322;aszczyzna"
  ]
  node [
    id 596
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 597
    label "bierka_szachowa"
  ]
  node [
    id 598
    label "obiekt_matematyczny"
  ]
  node [
    id 599
    label "gestaltyzm"
  ]
  node [
    id 600
    label "styl"
  ]
  node [
    id 601
    label "obraz"
  ]
  node [
    id 602
    label "Osjan"
  ]
  node [
    id 603
    label "character"
  ]
  node [
    id 604
    label "kto&#347;"
  ]
  node [
    id 605
    label "rze&#378;ba"
  ]
  node [
    id 606
    label "stylistyka"
  ]
  node [
    id 607
    label "figure"
  ]
  node [
    id 608
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 609
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 610
    label "miejsce"
  ]
  node [
    id 611
    label "antycypacja"
  ]
  node [
    id 612
    label "ornamentyka"
  ]
  node [
    id 613
    label "Aspazja"
  ]
  node [
    id 614
    label "facet"
  ]
  node [
    id 615
    label "popis"
  ]
  node [
    id 616
    label "wiersz"
  ]
  node [
    id 617
    label "kompleksja"
  ]
  node [
    id 618
    label "budowa"
  ]
  node [
    id 619
    label "symetria"
  ]
  node [
    id 620
    label "lingwistyka_kognitywna"
  ]
  node [
    id 621
    label "karta"
  ]
  node [
    id 622
    label "shape"
  ]
  node [
    id 623
    label "podzbi&#243;r"
  ]
  node [
    id 624
    label "przedstawienie"
  ]
  node [
    id 625
    label "point"
  ]
  node [
    id 626
    label "perspektywa"
  ]
  node [
    id 627
    label "piek&#322;o"
  ]
  node [
    id 628
    label "human_body"
  ]
  node [
    id 629
    label "ofiarowywanie"
  ]
  node [
    id 630
    label "sfera_afektywna"
  ]
  node [
    id 631
    label "nekromancja"
  ]
  node [
    id 632
    label "Po&#347;wist"
  ]
  node [
    id 633
    label "podekscytowanie"
  ]
  node [
    id 634
    label "deformowanie"
  ]
  node [
    id 635
    label "sumienie"
  ]
  node [
    id 636
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 637
    label "deformowa&#263;"
  ]
  node [
    id 638
    label "osobowo&#347;&#263;"
  ]
  node [
    id 639
    label "psychika"
  ]
  node [
    id 640
    label "zjawa"
  ]
  node [
    id 641
    label "zmar&#322;y"
  ]
  node [
    id 642
    label "istota_nadprzyrodzona"
  ]
  node [
    id 643
    label "power"
  ]
  node [
    id 644
    label "ofiarowywa&#263;"
  ]
  node [
    id 645
    label "oddech"
  ]
  node [
    id 646
    label "seksualno&#347;&#263;"
  ]
  node [
    id 647
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 648
    label "si&#322;a"
  ]
  node [
    id 649
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 650
    label "ego"
  ]
  node [
    id 651
    label "ofiarowanie"
  ]
  node [
    id 652
    label "fizjonomia"
  ]
  node [
    id 653
    label "kompleks"
  ]
  node [
    id 654
    label "zapalno&#347;&#263;"
  ]
  node [
    id 655
    label "T&#281;sknica"
  ]
  node [
    id 656
    label "ofiarowa&#263;"
  ]
  node [
    id 657
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 658
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 659
    label "passion"
  ]
  node [
    id 660
    label "sk&#322;adka"
  ]
  node [
    id 661
    label "&#243;semka"
  ]
  node [
    id 662
    label "czw&#243;rka"
  ]
  node [
    id 663
    label "sk&#322;ada&#263;_si&#281;"
  ]
  node [
    id 664
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 665
    label "zbi&#243;rka"
  ]
  node [
    id 666
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 667
    label "arkusz"
  ]
  node [
    id 668
    label "put"
  ]
  node [
    id 669
    label "zdecydowa&#263;"
  ]
  node [
    id 670
    label "zrobi&#263;"
  ]
  node [
    id 671
    label "bind"
  ]
  node [
    id 672
    label "umocni&#263;"
  ]
  node [
    id 673
    label "spowodowa&#263;"
  ]
  node [
    id 674
    label "podnie&#347;&#263;"
  ]
  node [
    id 675
    label "umocnienie"
  ]
  node [
    id 676
    label "utrwali&#263;"
  ]
  node [
    id 677
    label "fixate"
  ]
  node [
    id 678
    label "wzmocni&#263;"
  ]
  node [
    id 679
    label "ustabilizowa&#263;"
  ]
  node [
    id 680
    label "zabezpieczy&#263;"
  ]
  node [
    id 681
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 682
    label "sta&#263;_si&#281;"
  ]
  node [
    id 683
    label "podj&#261;&#263;"
  ]
  node [
    id 684
    label "decide"
  ]
  node [
    id 685
    label "determine"
  ]
  node [
    id 686
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 687
    label "post&#261;pi&#263;"
  ]
  node [
    id 688
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 689
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 690
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 691
    label "zorganizowa&#263;"
  ]
  node [
    id 692
    label "appoint"
  ]
  node [
    id 693
    label "wystylizowa&#263;"
  ]
  node [
    id 694
    label "cause"
  ]
  node [
    id 695
    label "przerobi&#263;"
  ]
  node [
    id 696
    label "nabra&#263;"
  ]
  node [
    id 697
    label "make"
  ]
  node [
    id 698
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 699
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 700
    label "wydali&#263;"
  ]
  node [
    id 701
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 702
    label "umocowa&#263;"
  ]
  node [
    id 703
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 704
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 705
    label "procesualistyka"
  ]
  node [
    id 706
    label "regu&#322;a_Allena"
  ]
  node [
    id 707
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 708
    label "kryminalistyka"
  ]
  node [
    id 709
    label "szko&#322;a"
  ]
  node [
    id 710
    label "zasada_d'Alemberta"
  ]
  node [
    id 711
    label "obserwacja"
  ]
  node [
    id 712
    label "normatywizm"
  ]
  node [
    id 713
    label "jurisprudence"
  ]
  node [
    id 714
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 715
    label "kultura_duchowa"
  ]
  node [
    id 716
    label "przepis"
  ]
  node [
    id 717
    label "prawo_karne_procesowe"
  ]
  node [
    id 718
    label "criterion"
  ]
  node [
    id 719
    label "kazuistyka"
  ]
  node [
    id 720
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 721
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 722
    label "kryminologia"
  ]
  node [
    id 723
    label "opis"
  ]
  node [
    id 724
    label "regu&#322;a_Glogera"
  ]
  node [
    id 725
    label "prawo_Mendla"
  ]
  node [
    id 726
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 727
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 728
    label "prawo_karne"
  ]
  node [
    id 729
    label "legislacyjnie"
  ]
  node [
    id 730
    label "twierdzenie"
  ]
  node [
    id 731
    label "cywilistyka"
  ]
  node [
    id 732
    label "judykatura"
  ]
  node [
    id 733
    label "kanonistyka"
  ]
  node [
    id 734
    label "standard"
  ]
  node [
    id 735
    label "nauka_prawa"
  ]
  node [
    id 736
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 737
    label "podmiot"
  ]
  node [
    id 738
    label "law"
  ]
  node [
    id 739
    label "qualification"
  ]
  node [
    id 740
    label "dominion"
  ]
  node [
    id 741
    label "wykonawczy"
  ]
  node [
    id 742
    label "zasada"
  ]
  node [
    id 743
    label "normalizacja"
  ]
  node [
    id 744
    label "wypowied&#378;"
  ]
  node [
    id 745
    label "exposition"
  ]
  node [
    id 746
    label "model"
  ]
  node [
    id 747
    label "ordinariness"
  ]
  node [
    id 748
    label "instytucja"
  ]
  node [
    id 749
    label "taniec_towarzyski"
  ]
  node [
    id 750
    label "organizowanie"
  ]
  node [
    id 751
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 752
    label "zorganizowanie"
  ]
  node [
    id 753
    label "mechanika"
  ]
  node [
    id 754
    label "o&#347;"
  ]
  node [
    id 755
    label "usenet"
  ]
  node [
    id 756
    label "rozprz&#261;c"
  ]
  node [
    id 757
    label "cybernetyk"
  ]
  node [
    id 758
    label "podsystem"
  ]
  node [
    id 759
    label "system"
  ]
  node [
    id 760
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 761
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 762
    label "sk&#322;ad"
  ]
  node [
    id 763
    label "systemat"
  ]
  node [
    id 764
    label "konstrukcja"
  ]
  node [
    id 765
    label "konstelacja"
  ]
  node [
    id 766
    label "przebieg"
  ]
  node [
    id 767
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 768
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 769
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 770
    label "praktyka"
  ]
  node [
    id 771
    label "przeorientowywanie"
  ]
  node [
    id 772
    label "studia"
  ]
  node [
    id 773
    label "linia"
  ]
  node [
    id 774
    label "bok"
  ]
  node [
    id 775
    label "skr&#281;canie"
  ]
  node [
    id 776
    label "skr&#281;ca&#263;"
  ]
  node [
    id 777
    label "przeorientowywa&#263;"
  ]
  node [
    id 778
    label "orientowanie"
  ]
  node [
    id 779
    label "skr&#281;ci&#263;"
  ]
  node [
    id 780
    label "przeorientowanie"
  ]
  node [
    id 781
    label "przeorientowa&#263;"
  ]
  node [
    id 782
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 783
    label "metoda"
  ]
  node [
    id 784
    label "ty&#322;"
  ]
  node [
    id 785
    label "zorientowa&#263;"
  ]
  node [
    id 786
    label "orientowa&#263;"
  ]
  node [
    id 787
    label "prz&#243;d"
  ]
  node [
    id 788
    label "bearing"
  ]
  node [
    id 789
    label "skr&#281;cenie"
  ]
  node [
    id 790
    label "do&#347;wiadczenie"
  ]
  node [
    id 791
    label "teren_szko&#322;y"
  ]
  node [
    id 792
    label "Mickiewicz"
  ]
  node [
    id 793
    label "kwalifikacje"
  ]
  node [
    id 794
    label "podr&#281;cznik"
  ]
  node [
    id 795
    label "absolwent"
  ]
  node [
    id 796
    label "school"
  ]
  node [
    id 797
    label "zda&#263;"
  ]
  node [
    id 798
    label "gabinet"
  ]
  node [
    id 799
    label "urszulanki"
  ]
  node [
    id 800
    label "sztuba"
  ]
  node [
    id 801
    label "&#322;awa_szkolna"
  ]
  node [
    id 802
    label "nauka"
  ]
  node [
    id 803
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 804
    label "przepisa&#263;"
  ]
  node [
    id 805
    label "muzyka"
  ]
  node [
    id 806
    label "grupa"
  ]
  node [
    id 807
    label "form"
  ]
  node [
    id 808
    label "klasa"
  ]
  node [
    id 809
    label "lekcja"
  ]
  node [
    id 810
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 811
    label "przepisanie"
  ]
  node [
    id 812
    label "czas"
  ]
  node [
    id 813
    label "skolaryzacja"
  ]
  node [
    id 814
    label "zdanie"
  ]
  node [
    id 815
    label "stopek"
  ]
  node [
    id 816
    label "sekretariat"
  ]
  node [
    id 817
    label "lesson"
  ]
  node [
    id 818
    label "niepokalanki"
  ]
  node [
    id 819
    label "siedziba"
  ]
  node [
    id 820
    label "szkolenie"
  ]
  node [
    id 821
    label "kara"
  ]
  node [
    id 822
    label "tablica"
  ]
  node [
    id 823
    label "posiada&#263;"
  ]
  node [
    id 824
    label "egzekutywa"
  ]
  node [
    id 825
    label "potencja&#322;"
  ]
  node [
    id 826
    label "wyb&#243;r"
  ]
  node [
    id 827
    label "prospect"
  ]
  node [
    id 828
    label "ability"
  ]
  node [
    id 829
    label "obliczeniowo"
  ]
  node [
    id 830
    label "alternatywa"
  ]
  node [
    id 831
    label "operator_modalny"
  ]
  node [
    id 832
    label "badanie"
  ]
  node [
    id 833
    label "proces_my&#347;lowy"
  ]
  node [
    id 834
    label "remark"
  ]
  node [
    id 835
    label "stwierdzenie"
  ]
  node [
    id 836
    label "observation"
  ]
  node [
    id 837
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 838
    label "alternatywa_Fredholma"
  ]
  node [
    id 839
    label "oznajmianie"
  ]
  node [
    id 840
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 841
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 842
    label "paradoks_Leontiefa"
  ]
  node [
    id 843
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 844
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 845
    label "teza"
  ]
  node [
    id 846
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 847
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 848
    label "twierdzenie_Pettisa"
  ]
  node [
    id 849
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 850
    label "twierdzenie_Maya"
  ]
  node [
    id 851
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 852
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 853
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 854
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 855
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 856
    label "zapewnianie"
  ]
  node [
    id 857
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 858
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 859
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 860
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 861
    label "twierdzenie_Stokesa"
  ]
  node [
    id 862
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 863
    label "twierdzenie_Cevy"
  ]
  node [
    id 864
    label "twierdzenie_Pascala"
  ]
  node [
    id 865
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 866
    label "komunikowanie"
  ]
  node [
    id 867
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 868
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 869
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 870
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 871
    label "relacja"
  ]
  node [
    id 872
    label "calibration"
  ]
  node [
    id 873
    label "operacja"
  ]
  node [
    id 874
    label "porz&#261;dek"
  ]
  node [
    id 875
    label "dominance"
  ]
  node [
    id 876
    label "zabieg"
  ]
  node [
    id 877
    label "standardization"
  ]
  node [
    id 878
    label "zmiana"
  ]
  node [
    id 879
    label "orzecznictwo"
  ]
  node [
    id 880
    label "wykonawczo"
  ]
  node [
    id 881
    label "organizacja"
  ]
  node [
    id 882
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 883
    label "set"
  ]
  node [
    id 884
    label "nada&#263;"
  ]
  node [
    id 885
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 886
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 887
    label "cook"
  ]
  node [
    id 888
    label "status"
  ]
  node [
    id 889
    label "procedura"
  ]
  node [
    id 890
    label "norma_prawna"
  ]
  node [
    id 891
    label "przedawnienie_si&#281;"
  ]
  node [
    id 892
    label "przedawnianie_si&#281;"
  ]
  node [
    id 893
    label "porada"
  ]
  node [
    id 894
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 895
    label "regulation"
  ]
  node [
    id 896
    label "recepta"
  ]
  node [
    id 897
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 898
    label "kodeks"
  ]
  node [
    id 899
    label "base"
  ]
  node [
    id 900
    label "umowa"
  ]
  node [
    id 901
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 902
    label "moralno&#347;&#263;"
  ]
  node [
    id 903
    label "occupation"
  ]
  node [
    id 904
    label "podstawa"
  ]
  node [
    id 905
    label "substancja"
  ]
  node [
    id 906
    label "prawid&#322;o"
  ]
  node [
    id 907
    label "casuistry"
  ]
  node [
    id 908
    label "manipulacja"
  ]
  node [
    id 909
    label "probabilizm"
  ]
  node [
    id 910
    label "dermatoglifika"
  ]
  node [
    id 911
    label "mikro&#347;lad"
  ]
  node [
    id 912
    label "technika_&#347;ledcza"
  ]
  node [
    id 913
    label "dzia&#322;"
  ]
  node [
    id 914
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 915
    label "egzystencja"
  ]
  node [
    id 916
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 917
    label "retirement"
  ]
  node [
    id 918
    label "poprzedzanie"
  ]
  node [
    id 919
    label "czasoprzestrze&#324;"
  ]
  node [
    id 920
    label "laba"
  ]
  node [
    id 921
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 922
    label "chronometria"
  ]
  node [
    id 923
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 924
    label "rachuba_czasu"
  ]
  node [
    id 925
    label "przep&#322;ywanie"
  ]
  node [
    id 926
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 927
    label "czasokres"
  ]
  node [
    id 928
    label "odczyt"
  ]
  node [
    id 929
    label "chwila"
  ]
  node [
    id 930
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 931
    label "dzieje"
  ]
  node [
    id 932
    label "poprzedzenie"
  ]
  node [
    id 933
    label "trawienie"
  ]
  node [
    id 934
    label "pochodzi&#263;"
  ]
  node [
    id 935
    label "period"
  ]
  node [
    id 936
    label "okres_czasu"
  ]
  node [
    id 937
    label "poprzedza&#263;"
  ]
  node [
    id 938
    label "schy&#322;ek"
  ]
  node [
    id 939
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 940
    label "odwlekanie_si&#281;"
  ]
  node [
    id 941
    label "zegar"
  ]
  node [
    id 942
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 943
    label "czwarty_wymiar"
  ]
  node [
    id 944
    label "pochodzenie"
  ]
  node [
    id 945
    label "Zeitgeist"
  ]
  node [
    id 946
    label "trawi&#263;"
  ]
  node [
    id 947
    label "pogoda"
  ]
  node [
    id 948
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 949
    label "poprzedzi&#263;"
  ]
  node [
    id 950
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 951
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 952
    label "time_period"
  ]
  node [
    id 953
    label "utrzymywanie"
  ]
  node [
    id 954
    label "bycie"
  ]
  node [
    id 955
    label "utrzymanie"
  ]
  node [
    id 956
    label "warunki"
  ]
  node [
    id 957
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 958
    label "utrzyma&#263;"
  ]
  node [
    id 959
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 960
    label "wiek_matuzalemowy"
  ]
  node [
    id 961
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 962
    label "utrzymywa&#263;"
  ]
  node [
    id 963
    label "doch&#243;d"
  ]
  node [
    id 964
    label "economic_rent"
  ]
  node [
    id 965
    label "income"
  ]
  node [
    id 966
    label "stopa_procentowa"
  ]
  node [
    id 967
    label "krzywa_Engla"
  ]
  node [
    id 968
    label "korzy&#347;&#263;"
  ]
  node [
    id 969
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 970
    label "wp&#322;yw"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 47
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
]
