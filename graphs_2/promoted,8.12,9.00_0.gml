graph [
  node [
    id 0
    label "totolotek"
    origin "text"
  ]
  node [
    id 1
    label "polska"
    origin "text"
  ]
  node [
    id 2
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 3
    label "g&#243;wno"
    origin "text"
  ]
  node [
    id 4
    label "histori"
    origin "text"
  ]
  node [
    id 5
    label "polski"
    origin "text"
  ]
  node [
    id 6
    label "bukmacherka"
    origin "text"
  ]
  node [
    id 7
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kupon"
    origin "text"
  ]
  node [
    id 9
    label "wygrana"
    origin "text"
  ]
  node [
    id 10
    label "stwierdza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "byl"
    origin "text"
  ]
  node [
    id 12
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 13
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 14
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 15
    label "gdzie"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "potwierdzenie"
    origin "text"
  ]
  node [
    id 18
    label "zaakceptowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "czw&#243;rka"
  ]
  node [
    id 20
    label "sz&#243;stka"
  ]
  node [
    id 21
    label "pi&#261;tka"
  ]
  node [
    id 22
    label "totalizator"
  ]
  node [
    id 23
    label "tr&#243;jka"
  ]
  node [
    id 24
    label "gra_hazardowa"
  ]
  node [
    id 25
    label "punkt"
  ]
  node [
    id 26
    label "zak&#322;ad_wzajemny"
  ]
  node [
    id 27
    label "zak&#322;ad"
  ]
  node [
    id 28
    label "pool"
  ]
  node [
    id 29
    label "toto-lotek"
  ]
  node [
    id 30
    label "trafienie"
  ]
  node [
    id 31
    label "zbi&#243;r"
  ]
  node [
    id 32
    label "pieni&#261;dz"
  ]
  node [
    id 33
    label "hotel"
  ]
  node [
    id 34
    label "cyfra"
  ]
  node [
    id 35
    label "pok&#243;j"
  ]
  node [
    id 36
    label "stopie&#324;"
  ]
  node [
    id 37
    label "five"
  ]
  node [
    id 38
    label "obiekt"
  ]
  node [
    id 39
    label "blotka"
  ]
  node [
    id 40
    label "pi&#261;tak"
  ]
  node [
    id 41
    label "gest"
  ]
  node [
    id 42
    label "przedtrzonowiec"
  ]
  node [
    id 43
    label "bilard"
  ]
  node [
    id 44
    label "kie&#322;"
  ]
  node [
    id 45
    label "three"
  ]
  node [
    id 46
    label "zaprz&#281;g"
  ]
  node [
    id 47
    label "krok_taneczny"
  ]
  node [
    id 48
    label "arkusz_drukarski"
  ]
  node [
    id 49
    label "&#322;&#243;dka"
  ]
  node [
    id 50
    label "four"
  ]
  node [
    id 51
    label "&#263;wiartka"
  ]
  node [
    id 52
    label "minialbum"
  ]
  node [
    id 53
    label "osada"
  ]
  node [
    id 54
    label "p&#322;yta_winylowa"
  ]
  node [
    id 55
    label "six"
  ]
  node [
    id 56
    label "doros&#322;y"
  ]
  node [
    id 57
    label "znaczny"
  ]
  node [
    id 58
    label "niema&#322;o"
  ]
  node [
    id 59
    label "wiele"
  ]
  node [
    id 60
    label "rozwini&#281;ty"
  ]
  node [
    id 61
    label "dorodny"
  ]
  node [
    id 62
    label "wa&#380;ny"
  ]
  node [
    id 63
    label "prawdziwy"
  ]
  node [
    id 64
    label "du&#380;o"
  ]
  node [
    id 65
    label "&#380;ywny"
  ]
  node [
    id 66
    label "szczery"
  ]
  node [
    id 67
    label "naturalny"
  ]
  node [
    id 68
    label "naprawd&#281;"
  ]
  node [
    id 69
    label "realnie"
  ]
  node [
    id 70
    label "podobny"
  ]
  node [
    id 71
    label "zgodny"
  ]
  node [
    id 72
    label "m&#261;dry"
  ]
  node [
    id 73
    label "prawdziwie"
  ]
  node [
    id 74
    label "znacznie"
  ]
  node [
    id 75
    label "zauwa&#380;alny"
  ]
  node [
    id 76
    label "wynios&#322;y"
  ]
  node [
    id 77
    label "dono&#347;ny"
  ]
  node [
    id 78
    label "silny"
  ]
  node [
    id 79
    label "wa&#380;nie"
  ]
  node [
    id 80
    label "istotnie"
  ]
  node [
    id 81
    label "eksponowany"
  ]
  node [
    id 82
    label "dobry"
  ]
  node [
    id 83
    label "ukszta&#322;towany"
  ]
  node [
    id 84
    label "do&#347;cig&#322;y"
  ]
  node [
    id 85
    label "&#378;ra&#322;y"
  ]
  node [
    id 86
    label "zdr&#243;w"
  ]
  node [
    id 87
    label "dorodnie"
  ]
  node [
    id 88
    label "okaza&#322;y"
  ]
  node [
    id 89
    label "mocno"
  ]
  node [
    id 90
    label "wiela"
  ]
  node [
    id 91
    label "bardzo"
  ]
  node [
    id 92
    label "cz&#281;sto"
  ]
  node [
    id 93
    label "wydoro&#347;lenie"
  ]
  node [
    id 94
    label "cz&#322;owiek"
  ]
  node [
    id 95
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 96
    label "doro&#347;lenie"
  ]
  node [
    id 97
    label "doro&#347;le"
  ]
  node [
    id 98
    label "senior"
  ]
  node [
    id 99
    label "dojrzale"
  ]
  node [
    id 100
    label "wapniak"
  ]
  node [
    id 101
    label "dojrza&#322;y"
  ]
  node [
    id 102
    label "doletni"
  ]
  node [
    id 103
    label "ka&#322;"
  ]
  node [
    id 104
    label "tandeta"
  ]
  node [
    id 105
    label "zero"
  ]
  node [
    id 106
    label "drobiazg"
  ]
  node [
    id 107
    label "liczba"
  ]
  node [
    id 108
    label "ilo&#347;&#263;"
  ]
  node [
    id 109
    label "podzia&#322;ka"
  ]
  node [
    id 110
    label "ciura"
  ]
  node [
    id 111
    label "miernota"
  ]
  node [
    id 112
    label "love"
  ]
  node [
    id 113
    label "brak"
  ]
  node [
    id 114
    label "wydalina"
  ]
  node [
    id 115
    label "koprofilia"
  ]
  node [
    id 116
    label "stool"
  ]
  node [
    id 117
    label "odchody"
  ]
  node [
    id 118
    label "balas"
  ]
  node [
    id 119
    label "fekalia"
  ]
  node [
    id 120
    label "marno&#347;&#263;"
  ]
  node [
    id 121
    label "lichota"
  ]
  node [
    id 122
    label "mierno&#347;&#263;"
  ]
  node [
    id 123
    label "ta&#322;atajstwo"
  ]
  node [
    id 124
    label "produkt"
  ]
  node [
    id 125
    label "pisanina"
  ]
  node [
    id 126
    label "utw&#243;r"
  ]
  node [
    id 127
    label "lipa"
  ]
  node [
    id 128
    label "towar"
  ]
  node [
    id 129
    label "plewa"
  ]
  node [
    id 130
    label "bagatelle"
  ]
  node [
    id 131
    label "bangle"
  ]
  node [
    id 132
    label "triviality"
  ]
  node [
    id 133
    label "fraszka"
  ]
  node [
    id 134
    label "banalny"
  ]
  node [
    id 135
    label "sofcik"
  ]
  node [
    id 136
    label "fidryga&#322;ki"
  ]
  node [
    id 137
    label "kr&#243;tki"
  ]
  node [
    id 138
    label "szczeg&#243;&#322;"
  ]
  node [
    id 139
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 140
    label "nowela"
  ]
  node [
    id 141
    label "furda"
  ]
  node [
    id 142
    label "przedmiot"
  ]
  node [
    id 143
    label "Polish"
  ]
  node [
    id 144
    label "goniony"
  ]
  node [
    id 145
    label "oberek"
  ]
  node [
    id 146
    label "ryba_po_grecku"
  ]
  node [
    id 147
    label "sztajer"
  ]
  node [
    id 148
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 149
    label "krakowiak"
  ]
  node [
    id 150
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 151
    label "pierogi_ruskie"
  ]
  node [
    id 152
    label "lacki"
  ]
  node [
    id 153
    label "polak"
  ]
  node [
    id 154
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 155
    label "chodzony"
  ]
  node [
    id 156
    label "po_polsku"
  ]
  node [
    id 157
    label "mazur"
  ]
  node [
    id 158
    label "polsko"
  ]
  node [
    id 159
    label "skoczny"
  ]
  node [
    id 160
    label "drabant"
  ]
  node [
    id 161
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 162
    label "j&#281;zyk"
  ]
  node [
    id 163
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 164
    label "artykulator"
  ]
  node [
    id 165
    label "kod"
  ]
  node [
    id 166
    label "kawa&#322;ek"
  ]
  node [
    id 167
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 168
    label "gramatyka"
  ]
  node [
    id 169
    label "stylik"
  ]
  node [
    id 170
    label "przet&#322;umaczenie"
  ]
  node [
    id 171
    label "formalizowanie"
  ]
  node [
    id 172
    label "ssanie"
  ]
  node [
    id 173
    label "ssa&#263;"
  ]
  node [
    id 174
    label "language"
  ]
  node [
    id 175
    label "liza&#263;"
  ]
  node [
    id 176
    label "napisa&#263;"
  ]
  node [
    id 177
    label "konsonantyzm"
  ]
  node [
    id 178
    label "wokalizm"
  ]
  node [
    id 179
    label "pisa&#263;"
  ]
  node [
    id 180
    label "fonetyka"
  ]
  node [
    id 181
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 182
    label "jeniec"
  ]
  node [
    id 183
    label "but"
  ]
  node [
    id 184
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 185
    label "po_koroniarsku"
  ]
  node [
    id 186
    label "kultura_duchowa"
  ]
  node [
    id 187
    label "t&#322;umaczenie"
  ]
  node [
    id 188
    label "m&#243;wienie"
  ]
  node [
    id 189
    label "pype&#263;"
  ]
  node [
    id 190
    label "lizanie"
  ]
  node [
    id 191
    label "pismo"
  ]
  node [
    id 192
    label "formalizowa&#263;"
  ]
  node [
    id 193
    label "rozumie&#263;"
  ]
  node [
    id 194
    label "organ"
  ]
  node [
    id 195
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 196
    label "rozumienie"
  ]
  node [
    id 197
    label "spos&#243;b"
  ]
  node [
    id 198
    label "makroglosja"
  ]
  node [
    id 199
    label "m&#243;wi&#263;"
  ]
  node [
    id 200
    label "jama_ustna"
  ]
  node [
    id 201
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 202
    label "formacja_geologiczna"
  ]
  node [
    id 203
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 204
    label "natural_language"
  ]
  node [
    id 205
    label "s&#322;ownictwo"
  ]
  node [
    id 206
    label "urz&#261;dzenie"
  ]
  node [
    id 207
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 208
    label "wschodnioeuropejski"
  ]
  node [
    id 209
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 210
    label "poga&#324;ski"
  ]
  node [
    id 211
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 212
    label "topielec"
  ]
  node [
    id 213
    label "europejski"
  ]
  node [
    id 214
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 215
    label "langosz"
  ]
  node [
    id 216
    label "zboczenie"
  ]
  node [
    id 217
    label "om&#243;wienie"
  ]
  node [
    id 218
    label "sponiewieranie"
  ]
  node [
    id 219
    label "discipline"
  ]
  node [
    id 220
    label "rzecz"
  ]
  node [
    id 221
    label "omawia&#263;"
  ]
  node [
    id 222
    label "kr&#261;&#380;enie"
  ]
  node [
    id 223
    label "tre&#347;&#263;"
  ]
  node [
    id 224
    label "robienie"
  ]
  node [
    id 225
    label "sponiewiera&#263;"
  ]
  node [
    id 226
    label "element"
  ]
  node [
    id 227
    label "entity"
  ]
  node [
    id 228
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 229
    label "tematyka"
  ]
  node [
    id 230
    label "w&#261;tek"
  ]
  node [
    id 231
    label "charakter"
  ]
  node [
    id 232
    label "zbaczanie"
  ]
  node [
    id 233
    label "program_nauczania"
  ]
  node [
    id 234
    label "om&#243;wi&#263;"
  ]
  node [
    id 235
    label "omawianie"
  ]
  node [
    id 236
    label "thing"
  ]
  node [
    id 237
    label "kultura"
  ]
  node [
    id 238
    label "istota"
  ]
  node [
    id 239
    label "zbacza&#263;"
  ]
  node [
    id 240
    label "zboczy&#263;"
  ]
  node [
    id 241
    label "gwardzista"
  ]
  node [
    id 242
    label "melodia"
  ]
  node [
    id 243
    label "taniec"
  ]
  node [
    id 244
    label "taniec_ludowy"
  ]
  node [
    id 245
    label "&#347;redniowieczny"
  ]
  node [
    id 246
    label "europejsko"
  ]
  node [
    id 247
    label "specjalny"
  ]
  node [
    id 248
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 249
    label "weso&#322;y"
  ]
  node [
    id 250
    label "sprawny"
  ]
  node [
    id 251
    label "rytmiczny"
  ]
  node [
    id 252
    label "skocznie"
  ]
  node [
    id 253
    label "energiczny"
  ]
  node [
    id 254
    label "przytup"
  ]
  node [
    id 255
    label "ho&#322;ubiec"
  ]
  node [
    id 256
    label "wodzi&#263;"
  ]
  node [
    id 257
    label "lendler"
  ]
  node [
    id 258
    label "austriacki"
  ]
  node [
    id 259
    label "polka"
  ]
  node [
    id 260
    label "ludowy"
  ]
  node [
    id 261
    label "pie&#347;&#324;"
  ]
  node [
    id 262
    label "mieszkaniec"
  ]
  node [
    id 263
    label "centu&#347;"
  ]
  node [
    id 264
    label "lalka"
  ]
  node [
    id 265
    label "Ma&#322;opolanin"
  ]
  node [
    id 266
    label "krakauer"
  ]
  node [
    id 267
    label "po&#347;rednictwo"
  ]
  node [
    id 268
    label "metier"
  ]
  node [
    id 269
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 270
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 271
    label "zaziera&#263;"
  ]
  node [
    id 272
    label "move"
  ]
  node [
    id 273
    label "zaczyna&#263;"
  ]
  node [
    id 274
    label "spotyka&#263;"
  ]
  node [
    id 275
    label "przenika&#263;"
  ]
  node [
    id 276
    label "osi&#261;ga&#263;"
  ]
  node [
    id 277
    label "nast&#281;powa&#263;"
  ]
  node [
    id 278
    label "mount"
  ]
  node [
    id 279
    label "bra&#263;"
  ]
  node [
    id 280
    label "go"
  ]
  node [
    id 281
    label "&#322;oi&#263;"
  ]
  node [
    id 282
    label "intervene"
  ]
  node [
    id 283
    label "scale"
  ]
  node [
    id 284
    label "poznawa&#263;"
  ]
  node [
    id 285
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 286
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 287
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 288
    label "dochodzi&#263;"
  ]
  node [
    id 289
    label "przekracza&#263;"
  ]
  node [
    id 290
    label "wnika&#263;"
  ]
  node [
    id 291
    label "atakowa&#263;"
  ]
  node [
    id 292
    label "invade"
  ]
  node [
    id 293
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 294
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 295
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 296
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 297
    label "strike"
  ]
  node [
    id 298
    label "robi&#263;"
  ]
  node [
    id 299
    label "schorzenie"
  ]
  node [
    id 300
    label "dzia&#322;a&#263;"
  ]
  node [
    id 301
    label "ofensywny"
  ]
  node [
    id 302
    label "przewaga"
  ]
  node [
    id 303
    label "sport"
  ]
  node [
    id 304
    label "epidemia"
  ]
  node [
    id 305
    label "attack"
  ]
  node [
    id 306
    label "rozgrywa&#263;"
  ]
  node [
    id 307
    label "krytykowa&#263;"
  ]
  node [
    id 308
    label "walczy&#263;"
  ]
  node [
    id 309
    label "aim"
  ]
  node [
    id 310
    label "trouble_oneself"
  ]
  node [
    id 311
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 312
    label "napada&#263;"
  ]
  node [
    id 313
    label "usi&#322;owa&#263;"
  ]
  node [
    id 314
    label "ograniczenie"
  ]
  node [
    id 315
    label "przebywa&#263;"
  ]
  node [
    id 316
    label "conflict"
  ]
  node [
    id 317
    label "transgress"
  ]
  node [
    id 318
    label "appear"
  ]
  node [
    id 319
    label "mija&#263;"
  ]
  node [
    id 320
    label "zawiera&#263;"
  ]
  node [
    id 321
    label "cognizance"
  ]
  node [
    id 322
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 323
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 324
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 325
    label "go_steady"
  ]
  node [
    id 326
    label "detect"
  ]
  node [
    id 327
    label "make"
  ]
  node [
    id 328
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 329
    label "hurt"
  ]
  node [
    id 330
    label "styka&#263;_si&#281;"
  ]
  node [
    id 331
    label "trespass"
  ]
  node [
    id 332
    label "transpire"
  ]
  node [
    id 333
    label "naciska&#263;"
  ]
  node [
    id 334
    label "mie&#263;_miejsce"
  ]
  node [
    id 335
    label "alternate"
  ]
  node [
    id 336
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 337
    label "chance"
  ]
  node [
    id 338
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 339
    label "uzyskiwa&#263;"
  ]
  node [
    id 340
    label "dociera&#263;"
  ]
  node [
    id 341
    label "mark"
  ]
  node [
    id 342
    label "get"
  ]
  node [
    id 343
    label "claim"
  ]
  node [
    id 344
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 345
    label "ripen"
  ]
  node [
    id 346
    label "supervene"
  ]
  node [
    id 347
    label "doczeka&#263;"
  ]
  node [
    id 348
    label "przesy&#322;ka"
  ]
  node [
    id 349
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 350
    label "doznawa&#263;"
  ]
  node [
    id 351
    label "reach"
  ]
  node [
    id 352
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 353
    label "zachodzi&#263;"
  ]
  node [
    id 354
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 355
    label "postrzega&#263;"
  ]
  node [
    id 356
    label "orgazm"
  ]
  node [
    id 357
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 358
    label "dokoptowywa&#263;"
  ]
  node [
    id 359
    label "dolatywa&#263;"
  ]
  node [
    id 360
    label "powodowa&#263;"
  ]
  node [
    id 361
    label "submit"
  ]
  node [
    id 362
    label "odejmowa&#263;"
  ]
  node [
    id 363
    label "bankrupt"
  ]
  node [
    id 364
    label "open"
  ]
  node [
    id 365
    label "set_about"
  ]
  node [
    id 366
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 367
    label "begin"
  ]
  node [
    id 368
    label "post&#281;powa&#263;"
  ]
  node [
    id 369
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 370
    label "fall"
  ]
  node [
    id 371
    label "znajdowa&#263;"
  ]
  node [
    id 372
    label "happen"
  ]
  node [
    id 373
    label "goban"
  ]
  node [
    id 374
    label "gra_planszowa"
  ]
  node [
    id 375
    label "sport_umys&#322;owy"
  ]
  node [
    id 376
    label "chi&#324;ski"
  ]
  node [
    id 377
    label "zagl&#261;da&#263;"
  ]
  node [
    id 378
    label "wpada&#263;"
  ]
  node [
    id 379
    label "pokonywa&#263;"
  ]
  node [
    id 380
    label "nat&#322;uszcza&#263;"
  ]
  node [
    id 381
    label "je&#378;dzi&#263;"
  ]
  node [
    id 382
    label "peddle"
  ]
  node [
    id 383
    label "obgadywa&#263;"
  ]
  node [
    id 384
    label "bi&#263;"
  ]
  node [
    id 385
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 386
    label "naci&#261;ga&#263;"
  ]
  node [
    id 387
    label "gra&#263;"
  ]
  node [
    id 388
    label "tankowa&#263;"
  ]
  node [
    id 389
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 390
    label "bang"
  ]
  node [
    id 391
    label "domy&#347;la&#263;_si&#281;"
  ]
  node [
    id 392
    label "drench"
  ]
  node [
    id 393
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 394
    label "meet"
  ]
  node [
    id 395
    label "substancja"
  ]
  node [
    id 396
    label "saturate"
  ]
  node [
    id 397
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 398
    label "tworzy&#263;"
  ]
  node [
    id 399
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 400
    label "porywa&#263;"
  ]
  node [
    id 401
    label "korzysta&#263;"
  ]
  node [
    id 402
    label "take"
  ]
  node [
    id 403
    label "poczytywa&#263;"
  ]
  node [
    id 404
    label "levy"
  ]
  node [
    id 405
    label "wk&#322;ada&#263;"
  ]
  node [
    id 406
    label "raise"
  ]
  node [
    id 407
    label "przyjmowa&#263;"
  ]
  node [
    id 408
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 409
    label "rucha&#263;"
  ]
  node [
    id 410
    label "prowadzi&#263;"
  ]
  node [
    id 411
    label "za&#380;ywa&#263;"
  ]
  node [
    id 412
    label "otrzymywa&#263;"
  ]
  node [
    id 413
    label "&#263;pa&#263;"
  ]
  node [
    id 414
    label "interpretowa&#263;"
  ]
  node [
    id 415
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 416
    label "dostawa&#263;"
  ]
  node [
    id 417
    label "rusza&#263;"
  ]
  node [
    id 418
    label "chwyta&#263;"
  ]
  node [
    id 419
    label "grza&#263;"
  ]
  node [
    id 420
    label "wch&#322;ania&#263;"
  ]
  node [
    id 421
    label "wygrywa&#263;"
  ]
  node [
    id 422
    label "u&#380;ywa&#263;"
  ]
  node [
    id 423
    label "ucieka&#263;"
  ]
  node [
    id 424
    label "arise"
  ]
  node [
    id 425
    label "uprawia&#263;_seks"
  ]
  node [
    id 426
    label "abstract"
  ]
  node [
    id 427
    label "towarzystwo"
  ]
  node [
    id 428
    label "branie"
  ]
  node [
    id 429
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 430
    label "zalicza&#263;"
  ]
  node [
    id 431
    label "wzi&#261;&#263;"
  ]
  node [
    id 432
    label "&#322;apa&#263;"
  ]
  node [
    id 433
    label "przewa&#380;a&#263;"
  ]
  node [
    id 434
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 435
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 436
    label "formularz"
  ]
  node [
    id 437
    label "odcinek"
  ]
  node [
    id 438
    label "wytw&#243;r"
  ]
  node [
    id 439
    label "dokument"
  ]
  node [
    id 440
    label "zestaw"
  ]
  node [
    id 441
    label "formu&#322;a"
  ]
  node [
    id 442
    label "teren"
  ]
  node [
    id 443
    label "pole"
  ]
  node [
    id 444
    label "part"
  ]
  node [
    id 445
    label "line"
  ]
  node [
    id 446
    label "coupon"
  ]
  node [
    id 447
    label "fragment"
  ]
  node [
    id 448
    label "pokwitowanie"
  ]
  node [
    id 449
    label "moneta"
  ]
  node [
    id 450
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 451
    label "epizod"
  ]
  node [
    id 452
    label "puchar"
  ]
  node [
    id 453
    label "korzy&#347;&#263;"
  ]
  node [
    id 454
    label "sukces"
  ]
  node [
    id 455
    label "conquest"
  ]
  node [
    id 456
    label "kobieta_sukcesu"
  ]
  node [
    id 457
    label "success"
  ]
  node [
    id 458
    label "rezultat"
  ]
  node [
    id 459
    label "passa"
  ]
  node [
    id 460
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 461
    label "zaleta"
  ]
  node [
    id 462
    label "dobro"
  ]
  node [
    id 463
    label "naczynie"
  ]
  node [
    id 464
    label "nagroda"
  ]
  node [
    id 465
    label "zwyci&#281;stwo"
  ]
  node [
    id 466
    label "zawody"
  ]
  node [
    id 467
    label "zawarto&#347;&#263;"
  ]
  node [
    id 468
    label "attest"
  ]
  node [
    id 469
    label "uznawa&#263;"
  ]
  node [
    id 470
    label "oznajmia&#263;"
  ]
  node [
    id 471
    label "os&#261;dza&#263;"
  ]
  node [
    id 472
    label "consider"
  ]
  node [
    id 473
    label "notice"
  ]
  node [
    id 474
    label "przyznawa&#263;"
  ]
  node [
    id 475
    label "inform"
  ]
  node [
    id 476
    label "informowa&#263;"
  ]
  node [
    id 477
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 478
    label "error"
  ]
  node [
    id 479
    label "pomylenie_si&#281;"
  ]
  node [
    id 480
    label "czyn"
  ]
  node [
    id 481
    label "baseball"
  ]
  node [
    id 482
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 483
    label "mniemanie"
  ]
  node [
    id 484
    label "byk"
  ]
  node [
    id 485
    label "treatment"
  ]
  node [
    id 486
    label "pogl&#261;d"
  ]
  node [
    id 487
    label "my&#347;lenie"
  ]
  node [
    id 488
    label "dzia&#322;anie"
  ]
  node [
    id 489
    label "typ"
  ]
  node [
    id 490
    label "event"
  ]
  node [
    id 491
    label "przyczyna"
  ]
  node [
    id 492
    label "nies&#322;uszno&#347;&#263;"
  ]
  node [
    id 493
    label "sytuacja"
  ]
  node [
    id 494
    label "niedopasowanie"
  ]
  node [
    id 495
    label "funkcja"
  ]
  node [
    id 496
    label "act"
  ]
  node [
    id 497
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 498
    label "bydl&#281;"
  ]
  node [
    id 499
    label "strategia_byka"
  ]
  node [
    id 500
    label "si&#322;acz"
  ]
  node [
    id 501
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 502
    label "brat"
  ]
  node [
    id 503
    label "cios"
  ]
  node [
    id 504
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 505
    label "symbol"
  ]
  node [
    id 506
    label "bull"
  ]
  node [
    id 507
    label "gie&#322;da"
  ]
  node [
    id 508
    label "inwestor"
  ]
  node [
    id 509
    label "samiec"
  ]
  node [
    id 510
    label "optymista"
  ]
  node [
    id 511
    label "olbrzym"
  ]
  node [
    id 512
    label "kij_baseballowy"
  ]
  node [
    id 513
    label "r&#281;kawica_baseballowa"
  ]
  node [
    id 514
    label "gra"
  ]
  node [
    id 515
    label "sport_zespo&#322;owy"
  ]
  node [
    id 516
    label "&#322;apacz"
  ]
  node [
    id 517
    label "baza"
  ]
  node [
    id 518
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 519
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 520
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 521
    label "osta&#263;_si&#281;"
  ]
  node [
    id 522
    label "change"
  ]
  node [
    id 523
    label "pozosta&#263;"
  ]
  node [
    id 524
    label "catch"
  ]
  node [
    id 525
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 526
    label "proceed"
  ]
  node [
    id 527
    label "support"
  ]
  node [
    id 528
    label "prze&#380;y&#263;"
  ]
  node [
    id 529
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 530
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 531
    label "sta&#263;_si&#281;"
  ]
  node [
    id 532
    label "raptowny"
  ]
  node [
    id 533
    label "insert"
  ]
  node [
    id 534
    label "incorporate"
  ]
  node [
    id 535
    label "pozna&#263;"
  ]
  node [
    id 536
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 537
    label "boil"
  ]
  node [
    id 538
    label "uk&#322;ad"
  ]
  node [
    id 539
    label "umowa"
  ]
  node [
    id 540
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 541
    label "zamkn&#261;&#263;"
  ]
  node [
    id 542
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 543
    label "ustali&#263;"
  ]
  node [
    id 544
    label "admit"
  ]
  node [
    id 545
    label "wezbra&#263;"
  ]
  node [
    id 546
    label "embrace"
  ]
  node [
    id 547
    label "zako&#324;czy&#263;"
  ]
  node [
    id 548
    label "put"
  ]
  node [
    id 549
    label "ukry&#263;"
  ]
  node [
    id 550
    label "zablokowa&#263;"
  ]
  node [
    id 551
    label "sko&#324;czy&#263;"
  ]
  node [
    id 552
    label "uj&#261;&#263;"
  ]
  node [
    id 553
    label "zatrzyma&#263;"
  ]
  node [
    id 554
    label "close"
  ]
  node [
    id 555
    label "lock"
  ]
  node [
    id 556
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 557
    label "spowodowa&#263;"
  ]
  node [
    id 558
    label "kill"
  ]
  node [
    id 559
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 560
    label "umie&#347;ci&#263;"
  ]
  node [
    id 561
    label "udost&#281;pni&#263;"
  ]
  node [
    id 562
    label "obj&#261;&#263;"
  ]
  node [
    id 563
    label "clasp"
  ]
  node [
    id 564
    label "hold"
  ]
  node [
    id 565
    label "zdecydowa&#263;"
  ]
  node [
    id 566
    label "zrobi&#263;"
  ]
  node [
    id 567
    label "bind"
  ]
  node [
    id 568
    label "umocni&#263;"
  ]
  node [
    id 569
    label "unwrap"
  ]
  node [
    id 570
    label "zrozumie&#263;"
  ]
  node [
    id 571
    label "feel"
  ]
  node [
    id 572
    label "topographic_point"
  ]
  node [
    id 573
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 574
    label "visualize"
  ]
  node [
    id 575
    label "przyswoi&#263;"
  ]
  node [
    id 576
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 577
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 578
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 579
    label "teach"
  ]
  node [
    id 580
    label "experience"
  ]
  node [
    id 581
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 582
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 583
    label "wzm&#243;c_si&#281;"
  ]
  node [
    id 584
    label "nape&#322;ni&#263;_si&#281;"
  ]
  node [
    id 585
    label "zebra&#263;"
  ]
  node [
    id 586
    label "podda&#263;_si&#281;"
  ]
  node [
    id 587
    label "rise"
  ]
  node [
    id 588
    label "cover"
  ]
  node [
    id 589
    label "zawarcie"
  ]
  node [
    id 590
    label "warunek"
  ]
  node [
    id 591
    label "gestia_transportowa"
  ]
  node [
    id 592
    label "contract"
  ]
  node [
    id 593
    label "porozumienie"
  ]
  node [
    id 594
    label "klauzula"
  ]
  node [
    id 595
    label "rozprz&#261;c"
  ]
  node [
    id 596
    label "treaty"
  ]
  node [
    id 597
    label "systemat"
  ]
  node [
    id 598
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 599
    label "system"
  ]
  node [
    id 600
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 601
    label "struktura"
  ]
  node [
    id 602
    label "usenet"
  ]
  node [
    id 603
    label "przestawi&#263;"
  ]
  node [
    id 604
    label "alliance"
  ]
  node [
    id 605
    label "ONZ"
  ]
  node [
    id 606
    label "NATO"
  ]
  node [
    id 607
    label "konstelacja"
  ]
  node [
    id 608
    label "o&#347;"
  ]
  node [
    id 609
    label "podsystem"
  ]
  node [
    id 610
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 611
    label "wi&#281;&#378;"
  ]
  node [
    id 612
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 613
    label "zachowanie"
  ]
  node [
    id 614
    label "cybernetyk"
  ]
  node [
    id 615
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 616
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 617
    label "sk&#322;ad"
  ]
  node [
    id 618
    label "traktat_wersalski"
  ]
  node [
    id 619
    label "cia&#322;o"
  ]
  node [
    id 620
    label "szybki"
  ]
  node [
    id 621
    label "gwa&#322;towny"
  ]
  node [
    id 622
    label "zawrzenie"
  ]
  node [
    id 623
    label "nieoczekiwany"
  ]
  node [
    id 624
    label "raptownie"
  ]
  node [
    id 625
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 626
    label "equal"
  ]
  node [
    id 627
    label "trwa&#263;"
  ]
  node [
    id 628
    label "chodzi&#263;"
  ]
  node [
    id 629
    label "si&#281;ga&#263;"
  ]
  node [
    id 630
    label "stan"
  ]
  node [
    id 631
    label "obecno&#347;&#263;"
  ]
  node [
    id 632
    label "stand"
  ]
  node [
    id 633
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 634
    label "uczestniczy&#263;"
  ]
  node [
    id 635
    label "participate"
  ]
  node [
    id 636
    label "istnie&#263;"
  ]
  node [
    id 637
    label "pozostawa&#263;"
  ]
  node [
    id 638
    label "zostawa&#263;"
  ]
  node [
    id 639
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 640
    label "adhere"
  ]
  node [
    id 641
    label "compass"
  ]
  node [
    id 642
    label "appreciation"
  ]
  node [
    id 643
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 644
    label "mierzy&#263;"
  ]
  node [
    id 645
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 646
    label "exsert"
  ]
  node [
    id 647
    label "being"
  ]
  node [
    id 648
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 649
    label "cecha"
  ]
  node [
    id 650
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 651
    label "p&#322;ywa&#263;"
  ]
  node [
    id 652
    label "run"
  ]
  node [
    id 653
    label "bangla&#263;"
  ]
  node [
    id 654
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 655
    label "przebiega&#263;"
  ]
  node [
    id 656
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 657
    label "carry"
  ]
  node [
    id 658
    label "bywa&#263;"
  ]
  node [
    id 659
    label "dziama&#263;"
  ]
  node [
    id 660
    label "stara&#263;_si&#281;"
  ]
  node [
    id 661
    label "para"
  ]
  node [
    id 662
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 663
    label "str&#243;j"
  ]
  node [
    id 664
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 665
    label "krok"
  ]
  node [
    id 666
    label "tryb"
  ]
  node [
    id 667
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 668
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 669
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 670
    label "continue"
  ]
  node [
    id 671
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 672
    label "Ohio"
  ]
  node [
    id 673
    label "wci&#281;cie"
  ]
  node [
    id 674
    label "Nowy_York"
  ]
  node [
    id 675
    label "warstwa"
  ]
  node [
    id 676
    label "samopoczucie"
  ]
  node [
    id 677
    label "Illinois"
  ]
  node [
    id 678
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 679
    label "state"
  ]
  node [
    id 680
    label "Jukatan"
  ]
  node [
    id 681
    label "Kalifornia"
  ]
  node [
    id 682
    label "Wirginia"
  ]
  node [
    id 683
    label "wektor"
  ]
  node [
    id 684
    label "Goa"
  ]
  node [
    id 685
    label "Teksas"
  ]
  node [
    id 686
    label "Waszyngton"
  ]
  node [
    id 687
    label "miejsce"
  ]
  node [
    id 688
    label "Massachusetts"
  ]
  node [
    id 689
    label "Alaska"
  ]
  node [
    id 690
    label "Arakan"
  ]
  node [
    id 691
    label "Hawaje"
  ]
  node [
    id 692
    label "Maryland"
  ]
  node [
    id 693
    label "Michigan"
  ]
  node [
    id 694
    label "Arizona"
  ]
  node [
    id 695
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 696
    label "Georgia"
  ]
  node [
    id 697
    label "poziom"
  ]
  node [
    id 698
    label "Pensylwania"
  ]
  node [
    id 699
    label "shape"
  ]
  node [
    id 700
    label "Luizjana"
  ]
  node [
    id 701
    label "Nowy_Meksyk"
  ]
  node [
    id 702
    label "Alabama"
  ]
  node [
    id 703
    label "Kansas"
  ]
  node [
    id 704
    label "Oregon"
  ]
  node [
    id 705
    label "Oklahoma"
  ]
  node [
    id 706
    label "Floryda"
  ]
  node [
    id 707
    label "jednostka_administracyjna"
  ]
  node [
    id 708
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 709
    label "o&#347;wiadczenie"
  ]
  node [
    id 710
    label "certificate"
  ]
  node [
    id 711
    label "zgodzenie_si&#281;"
  ]
  node [
    id 712
    label "stwierdzenie"
  ]
  node [
    id 713
    label "sanction"
  ]
  node [
    id 714
    label "przy&#347;wiadczenie"
  ]
  node [
    id 715
    label "kontrasygnowanie"
  ]
  node [
    id 716
    label "wypowied&#378;"
  ]
  node [
    id 717
    label "resolution"
  ]
  node [
    id 718
    label "zwiastowanie"
  ]
  node [
    id 719
    label "statement"
  ]
  node [
    id 720
    label "announcement"
  ]
  node [
    id 721
    label "komunikat"
  ]
  node [
    id 722
    label "poinformowanie"
  ]
  node [
    id 723
    label "zapis"
  ]
  node [
    id 724
    label "&#347;wiadectwo"
  ]
  node [
    id 725
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 726
    label "parafa"
  ]
  node [
    id 727
    label "plik"
  ]
  node [
    id 728
    label "raport&#243;wka"
  ]
  node [
    id 729
    label "record"
  ]
  node [
    id 730
    label "fascyku&#322;"
  ]
  node [
    id 731
    label "dokumentacja"
  ]
  node [
    id 732
    label "registratura"
  ]
  node [
    id 733
    label "artyku&#322;"
  ]
  node [
    id 734
    label "writing"
  ]
  node [
    id 735
    label "sygnatariusz"
  ]
  node [
    id 736
    label "ustalenie"
  ]
  node [
    id 737
    label "oznajmienie"
  ]
  node [
    id 738
    label "podpisanie"
  ]
  node [
    id 739
    label "countersignature"
  ]
  node [
    id 740
    label "podpisywanie"
  ]
  node [
    id 741
    label "dramatize"
  ]
  node [
    id 742
    label "pogodzi&#263;_si&#281;"
  ]
  node [
    id 743
    label "pozwoli&#263;"
  ]
  node [
    id 744
    label "pofolgowa&#263;"
  ]
  node [
    id 745
    label "uzna&#263;"
  ]
  node [
    id 746
    label "leave"
  ]
  node [
    id 747
    label "receive"
  ]
  node [
    id 748
    label "oceni&#263;"
  ]
  node [
    id 749
    label "przyzna&#263;"
  ]
  node [
    id 750
    label "stwierdzi&#263;"
  ]
  node [
    id 751
    label "assent"
  ]
  node [
    id 752
    label "rede"
  ]
  node [
    id 753
    label "see"
  ]
  node [
    id 754
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 754
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 625
  ]
  edge [
    source 16
    target 334
  ]
  edge [
    source 16
    target 626
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 16
    target 628
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 630
  ]
  edge [
    source 16
    target 631
  ]
  edge [
    source 16
    target 632
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 298
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 276
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 643
  ]
  edge [
    source 16
    target 644
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 294
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 438
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 343
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 573
  ]
]
