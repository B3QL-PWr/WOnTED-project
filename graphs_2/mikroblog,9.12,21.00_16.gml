graph [
  node [
    id 0
    label "tera&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 2
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "hehe"
    origin "text"
  ]
  node [
    id 4
    label "heheszki"
    origin "text"
  ]
  node [
    id 5
    label "gownowpis"
    origin "text"
  ]
  node [
    id 6
    label "marnowa&#263;"
  ]
  node [
    id 7
    label "szasta&#263;"
  ]
  node [
    id 8
    label "mie&#263;_miejsce"
  ]
  node [
    id 9
    label "traci&#263;"
  ]
  node [
    id 10
    label "tyra&#263;"
  ]
  node [
    id 11
    label "waste"
  ]
  node [
    id 12
    label "traktowa&#263;"
  ]
  node [
    id 13
    label "appear"
  ]
  node [
    id 14
    label "kult"
  ]
  node [
    id 15
    label "nawa"
  ]
  node [
    id 16
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 17
    label "ub&#322;agalnia"
  ]
  node [
    id 18
    label "wsp&#243;lnota"
  ]
  node [
    id 19
    label "Ska&#322;ka"
  ]
  node [
    id 20
    label "zakrystia"
  ]
  node [
    id 21
    label "kropielnica"
  ]
  node [
    id 22
    label "prezbiterium"
  ]
  node [
    id 23
    label "organizacja_religijna"
  ]
  node [
    id 24
    label "nerwica_eklezjogenna"
  ]
  node [
    id 25
    label "church"
  ]
  node [
    id 26
    label "kruchta"
  ]
  node [
    id 27
    label "dom"
  ]
  node [
    id 28
    label "zwi&#261;zanie"
  ]
  node [
    id 29
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 30
    label "podobie&#324;stwo"
  ]
  node [
    id 31
    label "Skandynawia"
  ]
  node [
    id 32
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 33
    label "partnership"
  ]
  node [
    id 34
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 35
    label "wi&#261;zanie"
  ]
  node [
    id 36
    label "Ba&#322;kany"
  ]
  node [
    id 37
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 38
    label "society"
  ]
  node [
    id 39
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 40
    label "Walencja"
  ]
  node [
    id 41
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 42
    label "bratnia_dusza"
  ]
  node [
    id 43
    label "zwi&#261;zek"
  ]
  node [
    id 44
    label "zwi&#261;za&#263;"
  ]
  node [
    id 45
    label "marriage"
  ]
  node [
    id 46
    label "przybytek"
  ]
  node [
    id 47
    label "siedlisko"
  ]
  node [
    id 48
    label "budynek"
  ]
  node [
    id 49
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 50
    label "rodzina"
  ]
  node [
    id 51
    label "substancja_mieszkaniowa"
  ]
  node [
    id 52
    label "instytucja"
  ]
  node [
    id 53
    label "siedziba"
  ]
  node [
    id 54
    label "dom_rodzinny"
  ]
  node [
    id 55
    label "grupa"
  ]
  node [
    id 56
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 57
    label "poj&#281;cie"
  ]
  node [
    id 58
    label "stead"
  ]
  node [
    id 59
    label "garderoba"
  ]
  node [
    id 60
    label "wiecha"
  ]
  node [
    id 61
    label "fratria"
  ]
  node [
    id 62
    label "uwielbienie"
  ]
  node [
    id 63
    label "religia"
  ]
  node [
    id 64
    label "translacja"
  ]
  node [
    id 65
    label "postawa"
  ]
  node [
    id 66
    label "egzegeta"
  ]
  node [
    id 67
    label "worship"
  ]
  node [
    id 68
    label "obrz&#281;d"
  ]
  node [
    id 69
    label "babiniec"
  ]
  node [
    id 70
    label "przedsionek"
  ]
  node [
    id 71
    label "zesp&#243;&#322;"
  ]
  node [
    id 72
    label "korpus"
  ]
  node [
    id 73
    label "naczynie_na_wod&#281;_&#347;wi&#281;con&#261;"
  ]
  node [
    id 74
    label "paramenty"
  ]
  node [
    id 75
    label "pomieszczenie"
  ]
  node [
    id 76
    label "&#347;rodowisko"
  ]
  node [
    id 77
    label "o&#322;tarz"
  ]
  node [
    id 78
    label "stalle"
  ]
  node [
    id 79
    label "lampka_wieczysta"
  ]
  node [
    id 80
    label "tabernakulum"
  ]
  node [
    id 81
    label "duchowie&#324;stwo"
  ]
  node [
    id 82
    label "sail"
  ]
  node [
    id 83
    label "leave"
  ]
  node [
    id 84
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 85
    label "travel"
  ]
  node [
    id 86
    label "proceed"
  ]
  node [
    id 87
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 88
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 89
    label "zrobi&#263;"
  ]
  node [
    id 90
    label "zmieni&#263;"
  ]
  node [
    id 91
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 92
    label "zosta&#263;"
  ]
  node [
    id 93
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 94
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 95
    label "przyj&#261;&#263;"
  ]
  node [
    id 96
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 97
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 98
    label "uda&#263;_si&#281;"
  ]
  node [
    id 99
    label "zacz&#261;&#263;"
  ]
  node [
    id 100
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 101
    label "play_along"
  ]
  node [
    id 102
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 103
    label "opu&#347;ci&#263;"
  ]
  node [
    id 104
    label "become"
  ]
  node [
    id 105
    label "post&#261;pi&#263;"
  ]
  node [
    id 106
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 107
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 108
    label "odj&#261;&#263;"
  ]
  node [
    id 109
    label "cause"
  ]
  node [
    id 110
    label "introduce"
  ]
  node [
    id 111
    label "begin"
  ]
  node [
    id 112
    label "do"
  ]
  node [
    id 113
    label "sprawi&#263;"
  ]
  node [
    id 114
    label "change"
  ]
  node [
    id 115
    label "zast&#261;pi&#263;"
  ]
  node [
    id 116
    label "come_up"
  ]
  node [
    id 117
    label "przej&#347;&#263;"
  ]
  node [
    id 118
    label "straci&#263;"
  ]
  node [
    id 119
    label "zyska&#263;"
  ]
  node [
    id 120
    label "przybra&#263;"
  ]
  node [
    id 121
    label "strike"
  ]
  node [
    id 122
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 123
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 124
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 125
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 126
    label "receive"
  ]
  node [
    id 127
    label "obra&#263;"
  ]
  node [
    id 128
    label "uzna&#263;"
  ]
  node [
    id 129
    label "draw"
  ]
  node [
    id 130
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 131
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 132
    label "przyj&#281;cie"
  ]
  node [
    id 133
    label "fall"
  ]
  node [
    id 134
    label "swallow"
  ]
  node [
    id 135
    label "odebra&#263;"
  ]
  node [
    id 136
    label "dostarczy&#263;"
  ]
  node [
    id 137
    label "umie&#347;ci&#263;"
  ]
  node [
    id 138
    label "wzi&#261;&#263;"
  ]
  node [
    id 139
    label "absorb"
  ]
  node [
    id 140
    label "undertake"
  ]
  node [
    id 141
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 142
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 143
    label "osta&#263;_si&#281;"
  ]
  node [
    id 144
    label "pozosta&#263;"
  ]
  node [
    id 145
    label "catch"
  ]
  node [
    id 146
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 147
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 148
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 149
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 150
    label "zorganizowa&#263;"
  ]
  node [
    id 151
    label "appoint"
  ]
  node [
    id 152
    label "wystylizowa&#263;"
  ]
  node [
    id 153
    label "przerobi&#263;"
  ]
  node [
    id 154
    label "nabra&#263;"
  ]
  node [
    id 155
    label "make"
  ]
  node [
    id 156
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 157
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 158
    label "wydali&#263;"
  ]
  node [
    id 159
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 160
    label "pozostawi&#263;"
  ]
  node [
    id 161
    label "obni&#380;y&#263;"
  ]
  node [
    id 162
    label "zostawi&#263;"
  ]
  node [
    id 163
    label "przesta&#263;"
  ]
  node [
    id 164
    label "potani&#263;"
  ]
  node [
    id 165
    label "drop"
  ]
  node [
    id 166
    label "evacuate"
  ]
  node [
    id 167
    label "humiliate"
  ]
  node [
    id 168
    label "tekst"
  ]
  node [
    id 169
    label "authorize"
  ]
  node [
    id 170
    label "omin&#261;&#263;"
  ]
  node [
    id 171
    label "loom"
  ]
  node [
    id 172
    label "result"
  ]
  node [
    id 173
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 174
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 175
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 176
    label "zgin&#261;&#263;"
  ]
  node [
    id 177
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 178
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 179
    label "rise"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
