graph [
  node [
    id 0
    label "komentator"
    origin "text"
  ]
  node [
    id 1
    label "obserwowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "poletko"
    origin "text"
  ]
  node [
    id 4
    label "web"
    origin "text"
  ]
  node [
    id 5
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ca&#322;kowicie"
    origin "text"
  ]
  node [
    id 8
    label "wt&#243;rny"
    origin "text"
  ]
  node [
    id 9
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 10
    label "kopiowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 12
    label "serwis"
    origin "text"
  ]
  node [
    id 13
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 14
    label "pogo&#324;"
    origin "text"
  ]
  node [
    id 15
    label "nowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "te&#380;"
    origin "text"
  ]
  node [
    id 17
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 18
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "&#347;lepa"
    origin "text"
  ]
  node [
    id 20
    label "uliczka"
    origin "text"
  ]
  node [
    id 21
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 22
    label "donosi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "nowa"
    origin "text"
  ]
  node [
    id 24
    label "dziennikarz"
  ]
  node [
    id 25
    label "uczony"
  ]
  node [
    id 26
    label "autor"
  ]
  node [
    id 27
    label "publicysta"
  ]
  node [
    id 28
    label "nowiniarz"
  ]
  node [
    id 29
    label "bran&#380;owiec"
  ]
  node [
    id 30
    label "akredytowanie"
  ]
  node [
    id 31
    label "akredytowa&#263;"
  ]
  node [
    id 32
    label "wykszta&#322;cony"
  ]
  node [
    id 33
    label "inteligent"
  ]
  node [
    id 34
    label "cz&#322;owiek"
  ]
  node [
    id 35
    label "intelektualista"
  ]
  node [
    id 36
    label "Awerroes"
  ]
  node [
    id 37
    label "uczenie"
  ]
  node [
    id 38
    label "nauczny"
  ]
  node [
    id 39
    label "m&#261;dry"
  ]
  node [
    id 40
    label "kszta&#322;ciciel"
  ]
  node [
    id 41
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 42
    label "tworzyciel"
  ]
  node [
    id 43
    label "wykonawca"
  ]
  node [
    id 44
    label "pomys&#322;odawca"
  ]
  node [
    id 45
    label "&#347;w"
  ]
  node [
    id 46
    label "dostrzega&#263;"
  ]
  node [
    id 47
    label "patrze&#263;"
  ]
  node [
    id 48
    label "look"
  ]
  node [
    id 49
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 50
    label "punkt_widzenia"
  ]
  node [
    id 51
    label "koso"
  ]
  node [
    id 52
    label "robi&#263;"
  ]
  node [
    id 53
    label "pogl&#261;da&#263;"
  ]
  node [
    id 54
    label "dba&#263;"
  ]
  node [
    id 55
    label "szuka&#263;"
  ]
  node [
    id 56
    label "traktowa&#263;"
  ]
  node [
    id 57
    label "go_steady"
  ]
  node [
    id 58
    label "os&#261;dza&#263;"
  ]
  node [
    id 59
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 60
    label "obacza&#263;"
  ]
  node [
    id 61
    label "widzie&#263;"
  ]
  node [
    id 62
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 63
    label "notice"
  ]
  node [
    id 64
    label "perceive"
  ]
  node [
    id 65
    label "stylizacja"
  ]
  node [
    id 66
    label "wygl&#261;d"
  ]
  node [
    id 67
    label "przedmiot"
  ]
  node [
    id 68
    label "Polish"
  ]
  node [
    id 69
    label "goniony"
  ]
  node [
    id 70
    label "oberek"
  ]
  node [
    id 71
    label "ryba_po_grecku"
  ]
  node [
    id 72
    label "sztajer"
  ]
  node [
    id 73
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 74
    label "krakowiak"
  ]
  node [
    id 75
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 76
    label "pierogi_ruskie"
  ]
  node [
    id 77
    label "lacki"
  ]
  node [
    id 78
    label "polak"
  ]
  node [
    id 79
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 80
    label "chodzony"
  ]
  node [
    id 81
    label "po_polsku"
  ]
  node [
    id 82
    label "mazur"
  ]
  node [
    id 83
    label "polsko"
  ]
  node [
    id 84
    label "skoczny"
  ]
  node [
    id 85
    label "drabant"
  ]
  node [
    id 86
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 87
    label "j&#281;zyk"
  ]
  node [
    id 88
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 89
    label "artykulator"
  ]
  node [
    id 90
    label "kod"
  ]
  node [
    id 91
    label "kawa&#322;ek"
  ]
  node [
    id 92
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 93
    label "gramatyka"
  ]
  node [
    id 94
    label "stylik"
  ]
  node [
    id 95
    label "przet&#322;umaczenie"
  ]
  node [
    id 96
    label "formalizowanie"
  ]
  node [
    id 97
    label "ssanie"
  ]
  node [
    id 98
    label "ssa&#263;"
  ]
  node [
    id 99
    label "language"
  ]
  node [
    id 100
    label "liza&#263;"
  ]
  node [
    id 101
    label "napisa&#263;"
  ]
  node [
    id 102
    label "konsonantyzm"
  ]
  node [
    id 103
    label "wokalizm"
  ]
  node [
    id 104
    label "pisa&#263;"
  ]
  node [
    id 105
    label "fonetyka"
  ]
  node [
    id 106
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 107
    label "jeniec"
  ]
  node [
    id 108
    label "but"
  ]
  node [
    id 109
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 110
    label "po_koroniarsku"
  ]
  node [
    id 111
    label "kultura_duchowa"
  ]
  node [
    id 112
    label "t&#322;umaczenie"
  ]
  node [
    id 113
    label "m&#243;wienie"
  ]
  node [
    id 114
    label "pype&#263;"
  ]
  node [
    id 115
    label "lizanie"
  ]
  node [
    id 116
    label "pismo"
  ]
  node [
    id 117
    label "formalizowa&#263;"
  ]
  node [
    id 118
    label "rozumie&#263;"
  ]
  node [
    id 119
    label "organ"
  ]
  node [
    id 120
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 121
    label "rozumienie"
  ]
  node [
    id 122
    label "spos&#243;b"
  ]
  node [
    id 123
    label "makroglosja"
  ]
  node [
    id 124
    label "m&#243;wi&#263;"
  ]
  node [
    id 125
    label "jama_ustna"
  ]
  node [
    id 126
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 127
    label "formacja_geologiczna"
  ]
  node [
    id 128
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 129
    label "natural_language"
  ]
  node [
    id 130
    label "s&#322;ownictwo"
  ]
  node [
    id 131
    label "urz&#261;dzenie"
  ]
  node [
    id 132
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 133
    label "wschodnioeuropejski"
  ]
  node [
    id 134
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 135
    label "poga&#324;ski"
  ]
  node [
    id 136
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 137
    label "topielec"
  ]
  node [
    id 138
    label "europejski"
  ]
  node [
    id 139
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 140
    label "langosz"
  ]
  node [
    id 141
    label "zboczenie"
  ]
  node [
    id 142
    label "om&#243;wienie"
  ]
  node [
    id 143
    label "sponiewieranie"
  ]
  node [
    id 144
    label "discipline"
  ]
  node [
    id 145
    label "rzecz"
  ]
  node [
    id 146
    label "omawia&#263;"
  ]
  node [
    id 147
    label "kr&#261;&#380;enie"
  ]
  node [
    id 148
    label "tre&#347;&#263;"
  ]
  node [
    id 149
    label "robienie"
  ]
  node [
    id 150
    label "sponiewiera&#263;"
  ]
  node [
    id 151
    label "element"
  ]
  node [
    id 152
    label "entity"
  ]
  node [
    id 153
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 154
    label "tematyka"
  ]
  node [
    id 155
    label "w&#261;tek"
  ]
  node [
    id 156
    label "charakter"
  ]
  node [
    id 157
    label "zbaczanie"
  ]
  node [
    id 158
    label "program_nauczania"
  ]
  node [
    id 159
    label "om&#243;wi&#263;"
  ]
  node [
    id 160
    label "omawianie"
  ]
  node [
    id 161
    label "thing"
  ]
  node [
    id 162
    label "kultura"
  ]
  node [
    id 163
    label "istota"
  ]
  node [
    id 164
    label "zbacza&#263;"
  ]
  node [
    id 165
    label "zboczy&#263;"
  ]
  node [
    id 166
    label "gwardzista"
  ]
  node [
    id 167
    label "melodia"
  ]
  node [
    id 168
    label "taniec"
  ]
  node [
    id 169
    label "taniec_ludowy"
  ]
  node [
    id 170
    label "&#347;redniowieczny"
  ]
  node [
    id 171
    label "europejsko"
  ]
  node [
    id 172
    label "specjalny"
  ]
  node [
    id 173
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 174
    label "weso&#322;y"
  ]
  node [
    id 175
    label "sprawny"
  ]
  node [
    id 176
    label "rytmiczny"
  ]
  node [
    id 177
    label "skocznie"
  ]
  node [
    id 178
    label "energiczny"
  ]
  node [
    id 179
    label "przytup"
  ]
  node [
    id 180
    label "ho&#322;ubiec"
  ]
  node [
    id 181
    label "wodzi&#263;"
  ]
  node [
    id 182
    label "lendler"
  ]
  node [
    id 183
    label "austriacki"
  ]
  node [
    id 184
    label "polka"
  ]
  node [
    id 185
    label "ludowy"
  ]
  node [
    id 186
    label "pie&#347;&#324;"
  ]
  node [
    id 187
    label "mieszkaniec"
  ]
  node [
    id 188
    label "centu&#347;"
  ]
  node [
    id 189
    label "lalka"
  ]
  node [
    id 190
    label "Ma&#322;opolanin"
  ]
  node [
    id 191
    label "krakauer"
  ]
  node [
    id 192
    label "grz&#261;dka"
  ]
  node [
    id 193
    label "posiad&#322;o&#347;&#263;"
  ]
  node [
    id 194
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 195
    label "pa&#324;stwo"
  ]
  node [
    id 196
    label "frymark"
  ]
  node [
    id 197
    label "Wilko"
  ]
  node [
    id 198
    label "ogr&#243;d"
  ]
  node [
    id 199
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 200
    label "pilnowa&#263;"
  ]
  node [
    id 201
    label "my&#347;le&#263;"
  ]
  node [
    id 202
    label "continue"
  ]
  node [
    id 203
    label "consider"
  ]
  node [
    id 204
    label "deliver"
  ]
  node [
    id 205
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 206
    label "uznawa&#263;"
  ]
  node [
    id 207
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 208
    label "organizowa&#263;"
  ]
  node [
    id 209
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 210
    label "czyni&#263;"
  ]
  node [
    id 211
    label "give"
  ]
  node [
    id 212
    label "stylizowa&#263;"
  ]
  node [
    id 213
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 214
    label "falowa&#263;"
  ]
  node [
    id 215
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 216
    label "peddle"
  ]
  node [
    id 217
    label "praca"
  ]
  node [
    id 218
    label "wydala&#263;"
  ]
  node [
    id 219
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 220
    label "tentegowa&#263;"
  ]
  node [
    id 221
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 222
    label "urz&#261;dza&#263;"
  ]
  node [
    id 223
    label "oszukiwa&#263;"
  ]
  node [
    id 224
    label "work"
  ]
  node [
    id 225
    label "ukazywa&#263;"
  ]
  node [
    id 226
    label "przerabia&#263;"
  ]
  node [
    id 227
    label "act"
  ]
  node [
    id 228
    label "post&#281;powa&#263;"
  ]
  node [
    id 229
    label "take_care"
  ]
  node [
    id 230
    label "troska&#263;_si&#281;"
  ]
  node [
    id 231
    label "rozpatrywa&#263;"
  ]
  node [
    id 232
    label "zamierza&#263;"
  ]
  node [
    id 233
    label "argue"
  ]
  node [
    id 234
    label "stwierdza&#263;"
  ]
  node [
    id 235
    label "przyznawa&#263;"
  ]
  node [
    id 236
    label "zachowywa&#263;"
  ]
  node [
    id 237
    label "cover"
  ]
  node [
    id 238
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 239
    label "mie&#263;_miejsce"
  ]
  node [
    id 240
    label "equal"
  ]
  node [
    id 241
    label "trwa&#263;"
  ]
  node [
    id 242
    label "chodzi&#263;"
  ]
  node [
    id 243
    label "si&#281;ga&#263;"
  ]
  node [
    id 244
    label "stan"
  ]
  node [
    id 245
    label "obecno&#347;&#263;"
  ]
  node [
    id 246
    label "stand"
  ]
  node [
    id 247
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 248
    label "uczestniczy&#263;"
  ]
  node [
    id 249
    label "participate"
  ]
  node [
    id 250
    label "pozostawa&#263;"
  ]
  node [
    id 251
    label "zostawa&#263;"
  ]
  node [
    id 252
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 253
    label "adhere"
  ]
  node [
    id 254
    label "compass"
  ]
  node [
    id 255
    label "korzysta&#263;"
  ]
  node [
    id 256
    label "appreciation"
  ]
  node [
    id 257
    label "osi&#261;ga&#263;"
  ]
  node [
    id 258
    label "dociera&#263;"
  ]
  node [
    id 259
    label "get"
  ]
  node [
    id 260
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 261
    label "mierzy&#263;"
  ]
  node [
    id 262
    label "u&#380;ywa&#263;"
  ]
  node [
    id 263
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 264
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 265
    label "exsert"
  ]
  node [
    id 266
    label "being"
  ]
  node [
    id 267
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 268
    label "cecha"
  ]
  node [
    id 269
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 270
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 271
    label "p&#322;ywa&#263;"
  ]
  node [
    id 272
    label "run"
  ]
  node [
    id 273
    label "bangla&#263;"
  ]
  node [
    id 274
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 275
    label "przebiega&#263;"
  ]
  node [
    id 276
    label "wk&#322;ada&#263;"
  ]
  node [
    id 277
    label "proceed"
  ]
  node [
    id 278
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 279
    label "carry"
  ]
  node [
    id 280
    label "bywa&#263;"
  ]
  node [
    id 281
    label "dziama&#263;"
  ]
  node [
    id 282
    label "stara&#263;_si&#281;"
  ]
  node [
    id 283
    label "para"
  ]
  node [
    id 284
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 285
    label "str&#243;j"
  ]
  node [
    id 286
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 287
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 288
    label "krok"
  ]
  node [
    id 289
    label "tryb"
  ]
  node [
    id 290
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 291
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 292
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 293
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 294
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 295
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 296
    label "Ohio"
  ]
  node [
    id 297
    label "wci&#281;cie"
  ]
  node [
    id 298
    label "Nowy_York"
  ]
  node [
    id 299
    label "warstwa"
  ]
  node [
    id 300
    label "samopoczucie"
  ]
  node [
    id 301
    label "Illinois"
  ]
  node [
    id 302
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 303
    label "state"
  ]
  node [
    id 304
    label "Jukatan"
  ]
  node [
    id 305
    label "Kalifornia"
  ]
  node [
    id 306
    label "Wirginia"
  ]
  node [
    id 307
    label "wektor"
  ]
  node [
    id 308
    label "Teksas"
  ]
  node [
    id 309
    label "Goa"
  ]
  node [
    id 310
    label "Waszyngton"
  ]
  node [
    id 311
    label "miejsce"
  ]
  node [
    id 312
    label "Massachusetts"
  ]
  node [
    id 313
    label "Alaska"
  ]
  node [
    id 314
    label "Arakan"
  ]
  node [
    id 315
    label "Hawaje"
  ]
  node [
    id 316
    label "Maryland"
  ]
  node [
    id 317
    label "punkt"
  ]
  node [
    id 318
    label "Michigan"
  ]
  node [
    id 319
    label "Arizona"
  ]
  node [
    id 320
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 321
    label "Georgia"
  ]
  node [
    id 322
    label "poziom"
  ]
  node [
    id 323
    label "Pensylwania"
  ]
  node [
    id 324
    label "shape"
  ]
  node [
    id 325
    label "Luizjana"
  ]
  node [
    id 326
    label "Nowy_Meksyk"
  ]
  node [
    id 327
    label "Alabama"
  ]
  node [
    id 328
    label "ilo&#347;&#263;"
  ]
  node [
    id 329
    label "Kansas"
  ]
  node [
    id 330
    label "Oregon"
  ]
  node [
    id 331
    label "Floryda"
  ]
  node [
    id 332
    label "Oklahoma"
  ]
  node [
    id 333
    label "jednostka_administracyjna"
  ]
  node [
    id 334
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 335
    label "kompletny"
  ]
  node [
    id 336
    label "zupe&#322;ny"
  ]
  node [
    id 337
    label "wniwecz"
  ]
  node [
    id 338
    label "kompletnie"
  ]
  node [
    id 339
    label "w_pizdu"
  ]
  node [
    id 340
    label "pe&#322;ny"
  ]
  node [
    id 341
    label "og&#243;lnie"
  ]
  node [
    id 342
    label "ca&#322;y"
  ]
  node [
    id 343
    label "&#322;&#261;czny"
  ]
  node [
    id 344
    label "zupe&#322;nie"
  ]
  node [
    id 345
    label "wt&#243;rnie"
  ]
  node [
    id 346
    label "nieoryginalny"
  ]
  node [
    id 347
    label "uboczny"
  ]
  node [
    id 348
    label "nieoryginalnie"
  ]
  node [
    id 349
    label "&#347;redni"
  ]
  node [
    id 350
    label "fa&#322;szywy"
  ]
  node [
    id 351
    label "zwyczajny"
  ]
  node [
    id 352
    label "nieciekawy"
  ]
  node [
    id 353
    label "unoriginal"
  ]
  node [
    id 354
    label "dodatkowy"
  ]
  node [
    id 355
    label "ubocznie"
  ]
  node [
    id 356
    label "ustawi&#263;"
  ]
  node [
    id 357
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 358
    label "establish"
  ]
  node [
    id 359
    label "podstawa"
  ]
  node [
    id 360
    label "recline"
  ]
  node [
    id 361
    label "osnowa&#263;"
  ]
  node [
    id 362
    label "woda"
  ]
  node [
    id 363
    label "hoax"
  ]
  node [
    id 364
    label "return"
  ]
  node [
    id 365
    label "wzi&#261;&#263;"
  ]
  node [
    id 366
    label "seize"
  ]
  node [
    id 367
    label "skorzysta&#263;"
  ]
  node [
    id 368
    label "poprawi&#263;"
  ]
  node [
    id 369
    label "nada&#263;"
  ]
  node [
    id 370
    label "marshal"
  ]
  node [
    id 371
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 372
    label "wyznaczy&#263;"
  ]
  node [
    id 373
    label "stanowisko"
  ]
  node [
    id 374
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 375
    label "spowodowa&#263;"
  ]
  node [
    id 376
    label "zabezpieczy&#263;"
  ]
  node [
    id 377
    label "umie&#347;ci&#263;"
  ]
  node [
    id 378
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 379
    label "zinterpretowa&#263;"
  ]
  node [
    id 380
    label "wskaza&#263;"
  ]
  node [
    id 381
    label "set"
  ]
  node [
    id 382
    label "przyzna&#263;"
  ]
  node [
    id 383
    label "sk&#322;oni&#263;"
  ]
  node [
    id 384
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 385
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 386
    label "zdecydowa&#263;"
  ]
  node [
    id 387
    label "accommodate"
  ]
  node [
    id 388
    label "ustali&#263;"
  ]
  node [
    id 389
    label "situate"
  ]
  node [
    id 390
    label "rola"
  ]
  node [
    id 391
    label "osnu&#263;"
  ]
  node [
    id 392
    label "zasadzi&#263;"
  ]
  node [
    id 393
    label "pot&#281;ga"
  ]
  node [
    id 394
    label "documentation"
  ]
  node [
    id 395
    label "column"
  ]
  node [
    id 396
    label "za&#322;o&#380;enie"
  ]
  node [
    id 397
    label "punkt_odniesienia"
  ]
  node [
    id 398
    label "zasadzenie"
  ]
  node [
    id 399
    label "bok"
  ]
  node [
    id 400
    label "d&#243;&#322;"
  ]
  node [
    id 401
    label "dzieci&#281;ctwo"
  ]
  node [
    id 402
    label "background"
  ]
  node [
    id 403
    label "podstawowy"
  ]
  node [
    id 404
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 405
    label "strategia"
  ]
  node [
    id 406
    label "pomys&#322;"
  ]
  node [
    id 407
    label "&#347;ciana"
  ]
  node [
    id 408
    label "wytwarza&#263;"
  ]
  node [
    id 409
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 410
    label "transcribe"
  ]
  node [
    id 411
    label "pirat"
  ]
  node [
    id 412
    label "mock"
  ]
  node [
    id 413
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 414
    label "dostosowywa&#263;"
  ]
  node [
    id 415
    label "nadawa&#263;"
  ]
  node [
    id 416
    label "create"
  ]
  node [
    id 417
    label "przest&#281;pca"
  ]
  node [
    id 418
    label "podr&#243;bka"
  ]
  node [
    id 419
    label "kieruj&#261;cy"
  ]
  node [
    id 420
    label "&#380;agl&#243;wka"
  ]
  node [
    id 421
    label "rum"
  ]
  node [
    id 422
    label "program"
  ]
  node [
    id 423
    label "rozb&#243;jnik"
  ]
  node [
    id 424
    label "postrzeleniec"
  ]
  node [
    id 425
    label "czerpa&#263;"
  ]
  node [
    id 426
    label "dally"
  ]
  node [
    id 427
    label "YouTube"
  ]
  node [
    id 428
    label "wytw&#243;r"
  ]
  node [
    id 429
    label "zak&#322;ad"
  ]
  node [
    id 430
    label "uderzenie"
  ]
  node [
    id 431
    label "service"
  ]
  node [
    id 432
    label "us&#322;uga"
  ]
  node [
    id 433
    label "porcja"
  ]
  node [
    id 434
    label "zastawa"
  ]
  node [
    id 435
    label "mecz"
  ]
  node [
    id 436
    label "strona"
  ]
  node [
    id 437
    label "doniesienie"
  ]
  node [
    id 438
    label "instrumentalizacja"
  ]
  node [
    id 439
    label "trafienie"
  ]
  node [
    id 440
    label "walka"
  ]
  node [
    id 441
    label "cios"
  ]
  node [
    id 442
    label "zdarzenie_si&#281;"
  ]
  node [
    id 443
    label "wdarcie_si&#281;"
  ]
  node [
    id 444
    label "pogorszenie"
  ]
  node [
    id 445
    label "d&#378;wi&#281;k"
  ]
  node [
    id 446
    label "poczucie"
  ]
  node [
    id 447
    label "coup"
  ]
  node [
    id 448
    label "reakcja"
  ]
  node [
    id 449
    label "contact"
  ]
  node [
    id 450
    label "stukni&#281;cie"
  ]
  node [
    id 451
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 452
    label "bat"
  ]
  node [
    id 453
    label "spowodowanie"
  ]
  node [
    id 454
    label "rush"
  ]
  node [
    id 455
    label "odbicie"
  ]
  node [
    id 456
    label "dawka"
  ]
  node [
    id 457
    label "zadanie"
  ]
  node [
    id 458
    label "&#347;ci&#281;cie"
  ]
  node [
    id 459
    label "st&#322;uczenie"
  ]
  node [
    id 460
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 461
    label "time"
  ]
  node [
    id 462
    label "odbicie_si&#281;"
  ]
  node [
    id 463
    label "dotkni&#281;cie"
  ]
  node [
    id 464
    label "charge"
  ]
  node [
    id 465
    label "dostanie"
  ]
  node [
    id 466
    label "skrytykowanie"
  ]
  node [
    id 467
    label "zagrywka"
  ]
  node [
    id 468
    label "manewr"
  ]
  node [
    id 469
    label "nast&#261;pienie"
  ]
  node [
    id 470
    label "uderzanie"
  ]
  node [
    id 471
    label "pogoda"
  ]
  node [
    id 472
    label "stroke"
  ]
  node [
    id 473
    label "pobicie"
  ]
  node [
    id 474
    label "ruch"
  ]
  node [
    id 475
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 476
    label "flap"
  ]
  node [
    id 477
    label "dotyk"
  ]
  node [
    id 478
    label "zrobienie"
  ]
  node [
    id 479
    label "produkt_gotowy"
  ]
  node [
    id 480
    label "asortyment"
  ]
  node [
    id 481
    label "czynno&#347;&#263;"
  ]
  node [
    id 482
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 483
    label "&#347;wiadczenie"
  ]
  node [
    id 484
    label "element_wyposa&#380;enia"
  ]
  node [
    id 485
    label "sto&#322;owizna"
  ]
  node [
    id 486
    label "p&#322;&#243;d"
  ]
  node [
    id 487
    label "rezultat"
  ]
  node [
    id 488
    label "zas&#243;b"
  ]
  node [
    id 489
    label "&#380;o&#322;d"
  ]
  node [
    id 490
    label "zak&#322;adka"
  ]
  node [
    id 491
    label "jednostka_organizacyjna"
  ]
  node [
    id 492
    label "miejsce_pracy"
  ]
  node [
    id 493
    label "instytucja"
  ]
  node [
    id 494
    label "wyko&#324;czenie"
  ]
  node [
    id 495
    label "firma"
  ]
  node [
    id 496
    label "czyn"
  ]
  node [
    id 497
    label "company"
  ]
  node [
    id 498
    label "instytut"
  ]
  node [
    id 499
    label "umowa"
  ]
  node [
    id 500
    label "po&#322;o&#380;enie"
  ]
  node [
    id 501
    label "sprawa"
  ]
  node [
    id 502
    label "ust&#281;p"
  ]
  node [
    id 503
    label "plan"
  ]
  node [
    id 504
    label "obiekt_matematyczny"
  ]
  node [
    id 505
    label "problemat"
  ]
  node [
    id 506
    label "plamka"
  ]
  node [
    id 507
    label "stopie&#324;_pisma"
  ]
  node [
    id 508
    label "jednostka"
  ]
  node [
    id 509
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 510
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 511
    label "mark"
  ]
  node [
    id 512
    label "chwila"
  ]
  node [
    id 513
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 514
    label "prosta"
  ]
  node [
    id 515
    label "problematyka"
  ]
  node [
    id 516
    label "obiekt"
  ]
  node [
    id 517
    label "zapunktowa&#263;"
  ]
  node [
    id 518
    label "podpunkt"
  ]
  node [
    id 519
    label "wojsko"
  ]
  node [
    id 520
    label "kres"
  ]
  node [
    id 521
    label "przestrze&#324;"
  ]
  node [
    id 522
    label "point"
  ]
  node [
    id 523
    label "pozycja"
  ]
  node [
    id 524
    label "obrona"
  ]
  node [
    id 525
    label "gra"
  ]
  node [
    id 526
    label "game"
  ]
  node [
    id 527
    label "serw"
  ]
  node [
    id 528
    label "dwumecz"
  ]
  node [
    id 529
    label "kartka"
  ]
  node [
    id 530
    label "logowanie"
  ]
  node [
    id 531
    label "plik"
  ]
  node [
    id 532
    label "s&#261;d"
  ]
  node [
    id 533
    label "adres_internetowy"
  ]
  node [
    id 534
    label "linia"
  ]
  node [
    id 535
    label "serwis_internetowy"
  ]
  node [
    id 536
    label "posta&#263;"
  ]
  node [
    id 537
    label "skr&#281;canie"
  ]
  node [
    id 538
    label "skr&#281;ca&#263;"
  ]
  node [
    id 539
    label "orientowanie"
  ]
  node [
    id 540
    label "skr&#281;ci&#263;"
  ]
  node [
    id 541
    label "uj&#281;cie"
  ]
  node [
    id 542
    label "zorientowanie"
  ]
  node [
    id 543
    label "ty&#322;"
  ]
  node [
    id 544
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 545
    label "fragment"
  ]
  node [
    id 546
    label "layout"
  ]
  node [
    id 547
    label "zorientowa&#263;"
  ]
  node [
    id 548
    label "pagina"
  ]
  node [
    id 549
    label "podmiot"
  ]
  node [
    id 550
    label "g&#243;ra"
  ]
  node [
    id 551
    label "orientowa&#263;"
  ]
  node [
    id 552
    label "voice"
  ]
  node [
    id 553
    label "orientacja"
  ]
  node [
    id 554
    label "prz&#243;d"
  ]
  node [
    id 555
    label "internet"
  ]
  node [
    id 556
    label "powierzchnia"
  ]
  node [
    id 557
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 558
    label "forma"
  ]
  node [
    id 559
    label "skr&#281;cenie"
  ]
  node [
    id 560
    label "do&#322;&#261;czenie"
  ]
  node [
    id 561
    label "message"
  ]
  node [
    id 562
    label "naznoszenie"
  ]
  node [
    id 563
    label "zawiadomienie"
  ]
  node [
    id 564
    label "zniesienie"
  ]
  node [
    id 565
    label "zaniesienie"
  ]
  node [
    id 566
    label "announcement"
  ]
  node [
    id 567
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 568
    label "fetch"
  ]
  node [
    id 569
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 570
    label "poinformowanie"
  ]
  node [
    id 571
    label "j&#281;zyk_angielski"
  ]
  node [
    id 572
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 573
    label "fajny"
  ]
  node [
    id 574
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 575
    label "po_ameryka&#324;sku"
  ]
  node [
    id 576
    label "typowy"
  ]
  node [
    id 577
    label "anglosaski"
  ]
  node [
    id 578
    label "boston"
  ]
  node [
    id 579
    label "pepperoni"
  ]
  node [
    id 580
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 581
    label "nowoczesny"
  ]
  node [
    id 582
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 583
    label "zachodni"
  ]
  node [
    id 584
    label "Princeton"
  ]
  node [
    id 585
    label "charakterystyczny"
  ]
  node [
    id 586
    label "cake-walk"
  ]
  node [
    id 587
    label "zachodny"
  ]
  node [
    id 588
    label "po_anglosasku"
  ]
  node [
    id 589
    label "anglosasko"
  ]
  node [
    id 590
    label "nowy"
  ]
  node [
    id 591
    label "nowo&#380;ytny"
  ]
  node [
    id 592
    label "otwarty"
  ]
  node [
    id 593
    label "nowocze&#347;nie"
  ]
  node [
    id 594
    label "byczy"
  ]
  node [
    id 595
    label "fajnie"
  ]
  node [
    id 596
    label "klawy"
  ]
  node [
    id 597
    label "dobry"
  ]
  node [
    id 598
    label "typowo"
  ]
  node [
    id 599
    label "cz&#281;sty"
  ]
  node [
    id 600
    label "zwyk&#322;y"
  ]
  node [
    id 601
    label "charakterystycznie"
  ]
  node [
    id 602
    label "szczeg&#243;lny"
  ]
  node [
    id 603
    label "wyj&#261;tkowy"
  ]
  node [
    id 604
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 605
    label "podobny"
  ]
  node [
    id 606
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 607
    label "nale&#380;ny"
  ]
  node [
    id 608
    label "nale&#380;yty"
  ]
  node [
    id 609
    label "uprawniony"
  ]
  node [
    id 610
    label "zasadniczy"
  ]
  node [
    id 611
    label "stosownie"
  ]
  node [
    id 612
    label "taki"
  ]
  node [
    id 613
    label "prawdziwy"
  ]
  node [
    id 614
    label "ten"
  ]
  node [
    id 615
    label "tkanina_we&#322;niana"
  ]
  node [
    id 616
    label "walc"
  ]
  node [
    id 617
    label "ubrani&#243;wka"
  ]
  node [
    id 618
    label "salami"
  ]
  node [
    id 619
    label "taniec_towarzyski"
  ]
  node [
    id 620
    label "grupa"
  ]
  node [
    id 621
    label "bieg"
  ]
  node [
    id 622
    label "bystrzyca"
  ]
  node [
    id 623
    label "wy&#347;cig"
  ]
  node [
    id 624
    label "cycle"
  ]
  node [
    id 625
    label "parametr"
  ]
  node [
    id 626
    label "roll"
  ]
  node [
    id 627
    label "procedura"
  ]
  node [
    id 628
    label "kierunek"
  ]
  node [
    id 629
    label "proces"
  ]
  node [
    id 630
    label "d&#261;&#380;enie"
  ]
  node [
    id 631
    label "przedbieg"
  ]
  node [
    id 632
    label "konkurencja"
  ]
  node [
    id 633
    label "pr&#261;d"
  ]
  node [
    id 634
    label "ciek_wodny"
  ]
  node [
    id 635
    label "kurs"
  ]
  node [
    id 636
    label "syfon"
  ]
  node [
    id 637
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 638
    label "czo&#322;&#243;wka"
  ]
  node [
    id 639
    label "odm&#322;adzanie"
  ]
  node [
    id 640
    label "liga"
  ]
  node [
    id 641
    label "jednostka_systematyczna"
  ]
  node [
    id 642
    label "asymilowanie"
  ]
  node [
    id 643
    label "gromada"
  ]
  node [
    id 644
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 645
    label "asymilowa&#263;"
  ]
  node [
    id 646
    label "egzemplarz"
  ]
  node [
    id 647
    label "Entuzjastki"
  ]
  node [
    id 648
    label "zbi&#243;r"
  ]
  node [
    id 649
    label "kompozycja"
  ]
  node [
    id 650
    label "Terranie"
  ]
  node [
    id 651
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 652
    label "category"
  ]
  node [
    id 653
    label "pakiet_klimatyczny"
  ]
  node [
    id 654
    label "oddzia&#322;"
  ]
  node [
    id 655
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 656
    label "cz&#261;steczka"
  ]
  node [
    id 657
    label "stage_set"
  ]
  node [
    id 658
    label "type"
  ]
  node [
    id 659
    label "specgrupa"
  ]
  node [
    id 660
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 661
    label "&#346;wietliki"
  ]
  node [
    id 662
    label "odm&#322;odzenie"
  ]
  node [
    id 663
    label "Eurogrupa"
  ]
  node [
    id 664
    label "odm&#322;adza&#263;"
  ]
  node [
    id 665
    label "harcerze_starsi"
  ]
  node [
    id 666
    label "urozmaicenie"
  ]
  node [
    id 667
    label "wiek"
  ]
  node [
    id 668
    label "newness"
  ]
  node [
    id 669
    label "co&#347;"
  ]
  node [
    id 670
    label "podzielenie"
  ]
  node [
    id 671
    label "nadanie"
  ]
  node [
    id 672
    label "differentiation"
  ]
  node [
    id 673
    label "period"
  ]
  node [
    id 674
    label "choroba_wieku"
  ]
  node [
    id 675
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 676
    label "chron"
  ]
  node [
    id 677
    label "czas"
  ]
  node [
    id 678
    label "rok"
  ]
  node [
    id 679
    label "long_time"
  ]
  node [
    id 680
    label "jednostka_geologiczna"
  ]
  node [
    id 681
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 682
    label "&#380;y&#263;"
  ]
  node [
    id 683
    label "kierowa&#263;"
  ]
  node [
    id 684
    label "g&#243;rowa&#263;"
  ]
  node [
    id 685
    label "tworzy&#263;"
  ]
  node [
    id 686
    label "krzywa"
  ]
  node [
    id 687
    label "linia_melodyczna"
  ]
  node [
    id 688
    label "control"
  ]
  node [
    id 689
    label "string"
  ]
  node [
    id 690
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 691
    label "ukierunkowywa&#263;"
  ]
  node [
    id 692
    label "sterowa&#263;"
  ]
  node [
    id 693
    label "kre&#347;li&#263;"
  ]
  node [
    id 694
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 695
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 696
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 697
    label "eksponowa&#263;"
  ]
  node [
    id 698
    label "navigate"
  ]
  node [
    id 699
    label "manipulate"
  ]
  node [
    id 700
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 701
    label "przesuwa&#263;"
  ]
  node [
    id 702
    label "partner"
  ]
  node [
    id 703
    label "prowadzenie"
  ]
  node [
    id 704
    label "powodowa&#263;"
  ]
  node [
    id 705
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 706
    label "motywowa&#263;"
  ]
  node [
    id 707
    label "draw"
  ]
  node [
    id 708
    label "clear"
  ]
  node [
    id 709
    label "usuwa&#263;"
  ]
  node [
    id 710
    label "report"
  ]
  node [
    id 711
    label "rysowa&#263;"
  ]
  node [
    id 712
    label "describe"
  ]
  node [
    id 713
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 714
    label "przedstawia&#263;"
  ]
  node [
    id 715
    label "delineate"
  ]
  node [
    id 716
    label "pope&#322;nia&#263;"
  ]
  node [
    id 717
    label "consist"
  ]
  node [
    id 718
    label "stanowi&#263;"
  ]
  node [
    id 719
    label "raise"
  ]
  node [
    id 720
    label "podkre&#347;la&#263;"
  ]
  node [
    id 721
    label "demonstrowa&#263;"
  ]
  node [
    id 722
    label "unwrap"
  ]
  node [
    id 723
    label "napromieniowywa&#263;"
  ]
  node [
    id 724
    label "trzyma&#263;"
  ]
  node [
    id 725
    label "manipulowa&#263;"
  ]
  node [
    id 726
    label "wysy&#322;a&#263;"
  ]
  node [
    id 727
    label "zwierzchnik"
  ]
  node [
    id 728
    label "ustawia&#263;"
  ]
  node [
    id 729
    label "przeznacza&#263;"
  ]
  node [
    id 730
    label "match"
  ]
  node [
    id 731
    label "administrowa&#263;"
  ]
  node [
    id 732
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 733
    label "order"
  ]
  node [
    id 734
    label "indicate"
  ]
  node [
    id 735
    label "undertaking"
  ]
  node [
    id 736
    label "base_on_balls"
  ]
  node [
    id 737
    label "wyprzedza&#263;"
  ]
  node [
    id 738
    label "wygrywa&#263;"
  ]
  node [
    id 739
    label "chop"
  ]
  node [
    id 740
    label "przekracza&#263;"
  ]
  node [
    id 741
    label "treat"
  ]
  node [
    id 742
    label "zaspokaja&#263;"
  ]
  node [
    id 743
    label "suffice"
  ]
  node [
    id 744
    label "zaspakaja&#263;"
  ]
  node [
    id 745
    label "uprawia&#263;_seks"
  ]
  node [
    id 746
    label "serve"
  ]
  node [
    id 747
    label "estrange"
  ]
  node [
    id 748
    label "transfer"
  ]
  node [
    id 749
    label "translate"
  ]
  node [
    id 750
    label "go"
  ]
  node [
    id 751
    label "zmienia&#263;"
  ]
  node [
    id 752
    label "postpone"
  ]
  node [
    id 753
    label "przestawia&#263;"
  ]
  node [
    id 754
    label "rusza&#263;"
  ]
  node [
    id 755
    label "przenosi&#263;"
  ]
  node [
    id 756
    label "wyznacza&#263;"
  ]
  node [
    id 757
    label "pause"
  ]
  node [
    id 758
    label "stay"
  ]
  node [
    id 759
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 760
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 761
    label "dane"
  ]
  node [
    id 762
    label "klawisz"
  ]
  node [
    id 763
    label "figura_geometryczna"
  ]
  node [
    id 764
    label "poprowadzi&#263;"
  ]
  node [
    id 765
    label "curvature"
  ]
  node [
    id 766
    label "curve"
  ]
  node [
    id 767
    label "wystawa&#263;"
  ]
  node [
    id 768
    label "sprout"
  ]
  node [
    id 769
    label "dysponowanie"
  ]
  node [
    id 770
    label "sterowanie"
  ]
  node [
    id 771
    label "powodowanie"
  ]
  node [
    id 772
    label "management"
  ]
  node [
    id 773
    label "kierowanie"
  ]
  node [
    id 774
    label "ukierunkowywanie"
  ]
  node [
    id 775
    label "przywodzenie"
  ]
  node [
    id 776
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 777
    label "doprowadzanie"
  ]
  node [
    id 778
    label "kre&#347;lenie"
  ]
  node [
    id 779
    label "lead"
  ]
  node [
    id 780
    label "eksponowanie"
  ]
  node [
    id 781
    label "prowadzanie"
  ]
  node [
    id 782
    label "wprowadzanie"
  ]
  node [
    id 783
    label "doprowadzenie"
  ]
  node [
    id 784
    label "poprowadzenie"
  ]
  node [
    id 785
    label "kszta&#322;towanie"
  ]
  node [
    id 786
    label "aim"
  ]
  node [
    id 787
    label "zwracanie"
  ]
  node [
    id 788
    label "przecinanie"
  ]
  node [
    id 789
    label "ta&#324;czenie"
  ]
  node [
    id 790
    label "przewy&#380;szanie"
  ]
  node [
    id 791
    label "g&#243;rowanie"
  ]
  node [
    id 792
    label "zaprowadzanie"
  ]
  node [
    id 793
    label "dawanie"
  ]
  node [
    id 794
    label "trzymanie"
  ]
  node [
    id 795
    label "oprowadzanie"
  ]
  node [
    id 796
    label "wprowadzenie"
  ]
  node [
    id 797
    label "drive"
  ]
  node [
    id 798
    label "oprowadzenie"
  ]
  node [
    id 799
    label "przeci&#281;cie"
  ]
  node [
    id 800
    label "przeci&#261;ganie"
  ]
  node [
    id 801
    label "pozarz&#261;dzanie"
  ]
  node [
    id 802
    label "granie"
  ]
  node [
    id 803
    label "pracownik"
  ]
  node [
    id 804
    label "przedsi&#281;biorca"
  ]
  node [
    id 805
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 806
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 807
    label "kolaborator"
  ]
  node [
    id 808
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 809
    label "sp&#243;lnik"
  ]
  node [
    id 810
    label "aktor"
  ]
  node [
    id 811
    label "uczestniczenie"
  ]
  node [
    id 812
    label "ulica"
  ]
  node [
    id 813
    label "droga"
  ]
  node [
    id 814
    label "korona_drogi"
  ]
  node [
    id 815
    label "pas_rozdzielczy"
  ]
  node [
    id 816
    label "&#347;rodowisko"
  ]
  node [
    id 817
    label "streetball"
  ]
  node [
    id 818
    label "miasteczko"
  ]
  node [
    id 819
    label "pas_ruchu"
  ]
  node [
    id 820
    label "chodnik"
  ]
  node [
    id 821
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 822
    label "pierzeja"
  ]
  node [
    id 823
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 824
    label "wysepka"
  ]
  node [
    id 825
    label "arteria"
  ]
  node [
    id 826
    label "Broadway"
  ]
  node [
    id 827
    label "autostrada"
  ]
  node [
    id 828
    label "jezdnia"
  ]
  node [
    id 829
    label "Stary_&#346;wiat"
  ]
  node [
    id 830
    label "asymilowanie_si&#281;"
  ]
  node [
    id 831
    label "p&#243;&#322;noc"
  ]
  node [
    id 832
    label "Wsch&#243;d"
  ]
  node [
    id 833
    label "class"
  ]
  node [
    id 834
    label "geosfera"
  ]
  node [
    id 835
    label "obiekt_naturalny"
  ]
  node [
    id 836
    label "przejmowanie"
  ]
  node [
    id 837
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 838
    label "przyroda"
  ]
  node [
    id 839
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 840
    label "po&#322;udnie"
  ]
  node [
    id 841
    label "zjawisko"
  ]
  node [
    id 842
    label "makrokosmos"
  ]
  node [
    id 843
    label "huczek"
  ]
  node [
    id 844
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 845
    label "environment"
  ]
  node [
    id 846
    label "morze"
  ]
  node [
    id 847
    label "rze&#378;ba"
  ]
  node [
    id 848
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 849
    label "przejmowa&#263;"
  ]
  node [
    id 850
    label "hydrosfera"
  ]
  node [
    id 851
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 852
    label "ciemna_materia"
  ]
  node [
    id 853
    label "ekosystem"
  ]
  node [
    id 854
    label "biota"
  ]
  node [
    id 855
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 856
    label "ekosfera"
  ]
  node [
    id 857
    label "geotermia"
  ]
  node [
    id 858
    label "planeta"
  ]
  node [
    id 859
    label "ozonosfera"
  ]
  node [
    id 860
    label "wszechstworzenie"
  ]
  node [
    id 861
    label "kuchnia"
  ]
  node [
    id 862
    label "biosfera"
  ]
  node [
    id 863
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 864
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 865
    label "populace"
  ]
  node [
    id 866
    label "magnetosfera"
  ]
  node [
    id 867
    label "Nowy_&#346;wiat"
  ]
  node [
    id 868
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 869
    label "universe"
  ]
  node [
    id 870
    label "biegun"
  ]
  node [
    id 871
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 872
    label "litosfera"
  ]
  node [
    id 873
    label "teren"
  ]
  node [
    id 874
    label "mikrokosmos"
  ]
  node [
    id 875
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 876
    label "stw&#243;r"
  ]
  node [
    id 877
    label "p&#243;&#322;kula"
  ]
  node [
    id 878
    label "przej&#281;cie"
  ]
  node [
    id 879
    label "barysfera"
  ]
  node [
    id 880
    label "obszar"
  ]
  node [
    id 881
    label "czarna_dziura"
  ]
  node [
    id 882
    label "atmosfera"
  ]
  node [
    id 883
    label "przej&#261;&#263;"
  ]
  node [
    id 884
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 885
    label "Ziemia"
  ]
  node [
    id 886
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 887
    label "geoida"
  ]
  node [
    id 888
    label "zagranica"
  ]
  node [
    id 889
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 890
    label "fauna"
  ]
  node [
    id 891
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 892
    label "Kosowo"
  ]
  node [
    id 893
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 894
    label "Zab&#322;ocie"
  ]
  node [
    id 895
    label "zach&#243;d"
  ]
  node [
    id 896
    label "Pow&#261;zki"
  ]
  node [
    id 897
    label "Piotrowo"
  ]
  node [
    id 898
    label "Olszanica"
  ]
  node [
    id 899
    label "Ruda_Pabianicka"
  ]
  node [
    id 900
    label "holarktyka"
  ]
  node [
    id 901
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 902
    label "Ludwin&#243;w"
  ]
  node [
    id 903
    label "Arktyka"
  ]
  node [
    id 904
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 905
    label "Zabu&#380;e"
  ]
  node [
    id 906
    label "antroposfera"
  ]
  node [
    id 907
    label "Neogea"
  ]
  node [
    id 908
    label "terytorium"
  ]
  node [
    id 909
    label "Syberia_Zachodnia"
  ]
  node [
    id 910
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 911
    label "zakres"
  ]
  node [
    id 912
    label "pas_planetoid"
  ]
  node [
    id 913
    label "Syberia_Wschodnia"
  ]
  node [
    id 914
    label "Antarktyka"
  ]
  node [
    id 915
    label "Rakowice"
  ]
  node [
    id 916
    label "akrecja"
  ]
  node [
    id 917
    label "wymiar"
  ]
  node [
    id 918
    label "&#321;&#281;g"
  ]
  node [
    id 919
    label "Kresy_Zachodnie"
  ]
  node [
    id 920
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 921
    label "wsch&#243;d"
  ]
  node [
    id 922
    label "Notogea"
  ]
  node [
    id 923
    label "integer"
  ]
  node [
    id 924
    label "liczba"
  ]
  node [
    id 925
    label "zlewanie_si&#281;"
  ]
  node [
    id 926
    label "uk&#322;ad"
  ]
  node [
    id 927
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 928
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 929
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 930
    label "boski"
  ]
  node [
    id 931
    label "krajobraz"
  ]
  node [
    id 932
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 933
    label "przywidzenie"
  ]
  node [
    id 934
    label "presence"
  ]
  node [
    id 935
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 936
    label "rozdzielanie"
  ]
  node [
    id 937
    label "bezbrze&#380;e"
  ]
  node [
    id 938
    label "czasoprzestrze&#324;"
  ]
  node [
    id 939
    label "niezmierzony"
  ]
  node [
    id 940
    label "przedzielenie"
  ]
  node [
    id 941
    label "nielito&#347;ciwy"
  ]
  node [
    id 942
    label "rozdziela&#263;"
  ]
  node [
    id 943
    label "oktant"
  ]
  node [
    id 944
    label "przedzieli&#263;"
  ]
  node [
    id 945
    label "przestw&#243;r"
  ]
  node [
    id 946
    label "rura"
  ]
  node [
    id 947
    label "grzebiuszka"
  ]
  node [
    id 948
    label "atom"
  ]
  node [
    id 949
    label "kosmos"
  ]
  node [
    id 950
    label "miniatura"
  ]
  node [
    id 951
    label "smok_wawelski"
  ]
  node [
    id 952
    label "niecz&#322;owiek"
  ]
  node [
    id 953
    label "monster"
  ]
  node [
    id 954
    label "istota_&#380;ywa"
  ]
  node [
    id 955
    label "potw&#243;r"
  ]
  node [
    id 956
    label "istota_fantastyczna"
  ]
  node [
    id 957
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 958
    label "ciep&#322;o"
  ]
  node [
    id 959
    label "energia_termiczna"
  ]
  node [
    id 960
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 961
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 962
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 963
    label "aspekt"
  ]
  node [
    id 964
    label "troposfera"
  ]
  node [
    id 965
    label "klimat"
  ]
  node [
    id 966
    label "metasfera"
  ]
  node [
    id 967
    label "atmosferyki"
  ]
  node [
    id 968
    label "homosfera"
  ]
  node [
    id 969
    label "powietrznia"
  ]
  node [
    id 970
    label "jonosfera"
  ]
  node [
    id 971
    label "termosfera"
  ]
  node [
    id 972
    label "egzosfera"
  ]
  node [
    id 973
    label "heterosfera"
  ]
  node [
    id 974
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 975
    label "tropopauza"
  ]
  node [
    id 976
    label "kwas"
  ]
  node [
    id 977
    label "powietrze"
  ]
  node [
    id 978
    label "stratosfera"
  ]
  node [
    id 979
    label "pow&#322;oka"
  ]
  node [
    id 980
    label "mezosfera"
  ]
  node [
    id 981
    label "mezopauza"
  ]
  node [
    id 982
    label "atmosphere"
  ]
  node [
    id 983
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 984
    label "sferoida"
  ]
  node [
    id 985
    label "object"
  ]
  node [
    id 986
    label "temat"
  ]
  node [
    id 987
    label "wpadni&#281;cie"
  ]
  node [
    id 988
    label "mienie"
  ]
  node [
    id 989
    label "wpa&#347;&#263;"
  ]
  node [
    id 990
    label "wpadanie"
  ]
  node [
    id 991
    label "wpada&#263;"
  ]
  node [
    id 992
    label "bra&#263;"
  ]
  node [
    id 993
    label "handle"
  ]
  node [
    id 994
    label "wzbudza&#263;"
  ]
  node [
    id 995
    label "ogarnia&#263;"
  ]
  node [
    id 996
    label "bang"
  ]
  node [
    id 997
    label "stimulate"
  ]
  node [
    id 998
    label "ogarn&#261;&#263;"
  ]
  node [
    id 999
    label "wzbudzi&#263;"
  ]
  node [
    id 1000
    label "thrill"
  ]
  node [
    id 1001
    label "czerpanie"
  ]
  node [
    id 1002
    label "acquisition"
  ]
  node [
    id 1003
    label "branie"
  ]
  node [
    id 1004
    label "caparison"
  ]
  node [
    id 1005
    label "movement"
  ]
  node [
    id 1006
    label "wzbudzanie"
  ]
  node [
    id 1007
    label "ogarnianie"
  ]
  node [
    id 1008
    label "wra&#380;enie"
  ]
  node [
    id 1009
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1010
    label "interception"
  ]
  node [
    id 1011
    label "wzbudzenie"
  ]
  node [
    id 1012
    label "emotion"
  ]
  node [
    id 1013
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1014
    label "wzi&#281;cie"
  ]
  node [
    id 1015
    label "performance"
  ]
  node [
    id 1016
    label "sztuka"
  ]
  node [
    id 1017
    label "Boreasz"
  ]
  node [
    id 1018
    label "noc"
  ]
  node [
    id 1019
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1020
    label "strona_&#347;wiata"
  ]
  node [
    id 1021
    label "godzina"
  ]
  node [
    id 1022
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1023
    label "kriosfera"
  ]
  node [
    id 1024
    label "lej_polarny"
  ]
  node [
    id 1025
    label "sfera"
  ]
  node [
    id 1026
    label "brzeg"
  ]
  node [
    id 1027
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 1028
    label "p&#322;oza"
  ]
  node [
    id 1029
    label "zawiasy"
  ]
  node [
    id 1030
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1031
    label "element_anatomiczny"
  ]
  node [
    id 1032
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 1033
    label "reda"
  ]
  node [
    id 1034
    label "zbiornik_wodny"
  ]
  node [
    id 1035
    label "przymorze"
  ]
  node [
    id 1036
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 1037
    label "bezmiar"
  ]
  node [
    id 1038
    label "pe&#322;ne_morze"
  ]
  node [
    id 1039
    label "latarnia_morska"
  ]
  node [
    id 1040
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1041
    label "nereida"
  ]
  node [
    id 1042
    label "okeanida"
  ]
  node [
    id 1043
    label "marina"
  ]
  node [
    id 1044
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 1045
    label "Morze_Czerwone"
  ]
  node [
    id 1046
    label "talasoterapia"
  ]
  node [
    id 1047
    label "Morze_Bia&#322;e"
  ]
  node [
    id 1048
    label "paliszcze"
  ]
  node [
    id 1049
    label "Neptun"
  ]
  node [
    id 1050
    label "Morze_Czarne"
  ]
  node [
    id 1051
    label "laguna"
  ]
  node [
    id 1052
    label "Morze_Egejskie"
  ]
  node [
    id 1053
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 1054
    label "Morze_Adriatyckie"
  ]
  node [
    id 1055
    label "rze&#378;biarstwo"
  ]
  node [
    id 1056
    label "planacja"
  ]
  node [
    id 1057
    label "relief"
  ]
  node [
    id 1058
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1059
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1060
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 1061
    label "bozzetto"
  ]
  node [
    id 1062
    label "plastyka"
  ]
  node [
    id 1063
    label "j&#261;dro"
  ]
  node [
    id 1064
    label "&#347;rodek"
  ]
  node [
    id 1065
    label "dzie&#324;"
  ]
  node [
    id 1066
    label "dwunasta"
  ]
  node [
    id 1067
    label "pora"
  ]
  node [
    id 1068
    label "ozon"
  ]
  node [
    id 1069
    label "gleba"
  ]
  node [
    id 1070
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 1071
    label "sialma"
  ]
  node [
    id 1072
    label "skorupa_ziemska"
  ]
  node [
    id 1073
    label "warstwa_perydotytowa"
  ]
  node [
    id 1074
    label "warstwa_granitowa"
  ]
  node [
    id 1075
    label "kula"
  ]
  node [
    id 1076
    label "kresom&#243;zgowie"
  ]
  node [
    id 1077
    label "przyra"
  ]
  node [
    id 1078
    label "biom"
  ]
  node [
    id 1079
    label "awifauna"
  ]
  node [
    id 1080
    label "ichtiofauna"
  ]
  node [
    id 1081
    label "geosystem"
  ]
  node [
    id 1082
    label "dotleni&#263;"
  ]
  node [
    id 1083
    label "spi&#281;trza&#263;"
  ]
  node [
    id 1084
    label "spi&#281;trzenie"
  ]
  node [
    id 1085
    label "utylizator"
  ]
  node [
    id 1086
    label "p&#322;ycizna"
  ]
  node [
    id 1087
    label "nabranie"
  ]
  node [
    id 1088
    label "Waruna"
  ]
  node [
    id 1089
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1090
    label "przybieranie"
  ]
  node [
    id 1091
    label "uci&#261;g"
  ]
  node [
    id 1092
    label "bombast"
  ]
  node [
    id 1093
    label "fala"
  ]
  node [
    id 1094
    label "kryptodepresja"
  ]
  node [
    id 1095
    label "water"
  ]
  node [
    id 1096
    label "wysi&#281;k"
  ]
  node [
    id 1097
    label "pustka"
  ]
  node [
    id 1098
    label "ciecz"
  ]
  node [
    id 1099
    label "przybrze&#380;e"
  ]
  node [
    id 1100
    label "nap&#243;j"
  ]
  node [
    id 1101
    label "spi&#281;trzanie"
  ]
  node [
    id 1102
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1103
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1104
    label "bicie"
  ]
  node [
    id 1105
    label "klarownik"
  ]
  node [
    id 1106
    label "chlastanie"
  ]
  node [
    id 1107
    label "woda_s&#322;odka"
  ]
  node [
    id 1108
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1109
    label "nabra&#263;"
  ]
  node [
    id 1110
    label "chlasta&#263;"
  ]
  node [
    id 1111
    label "uj&#281;cie_wody"
  ]
  node [
    id 1112
    label "zrzut"
  ]
  node [
    id 1113
    label "wypowied&#378;"
  ]
  node [
    id 1114
    label "wodnik"
  ]
  node [
    id 1115
    label "pojazd"
  ]
  node [
    id 1116
    label "l&#243;d"
  ]
  node [
    id 1117
    label "wybrze&#380;e"
  ]
  node [
    id 1118
    label "deklamacja"
  ]
  node [
    id 1119
    label "tlenek"
  ]
  node [
    id 1120
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1121
    label "biotop"
  ]
  node [
    id 1122
    label "biocenoza"
  ]
  node [
    id 1123
    label "kontekst"
  ]
  node [
    id 1124
    label "nation"
  ]
  node [
    id 1125
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1126
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1127
    label "w&#322;adza"
  ]
  node [
    id 1128
    label "szata_ro&#347;linna"
  ]
  node [
    id 1129
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1130
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1131
    label "zielono&#347;&#263;"
  ]
  node [
    id 1132
    label "pi&#281;tro"
  ]
  node [
    id 1133
    label "plant"
  ]
  node [
    id 1134
    label "ro&#347;lina"
  ]
  node [
    id 1135
    label "iglak"
  ]
  node [
    id 1136
    label "cyprysowate"
  ]
  node [
    id 1137
    label "zaj&#281;cie"
  ]
  node [
    id 1138
    label "tajniki"
  ]
  node [
    id 1139
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 1140
    label "jedzenie"
  ]
  node [
    id 1141
    label "zaplecze"
  ]
  node [
    id 1142
    label "pomieszczenie"
  ]
  node [
    id 1143
    label "zlewozmywak"
  ]
  node [
    id 1144
    label "gotowa&#263;"
  ]
  node [
    id 1145
    label "strefa"
  ]
  node [
    id 1146
    label "Jowisz"
  ]
  node [
    id 1147
    label "syzygia"
  ]
  node [
    id 1148
    label "Saturn"
  ]
  node [
    id 1149
    label "Uran"
  ]
  node [
    id 1150
    label "dar"
  ]
  node [
    id 1151
    label "real"
  ]
  node [
    id 1152
    label "Ukraina"
  ]
  node [
    id 1153
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1154
    label "blok_wschodni"
  ]
  node [
    id 1155
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1156
    label "Europa_Wschodnia"
  ]
  node [
    id 1157
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 1158
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1159
    label "spill_the_beans"
  ]
  node [
    id 1160
    label "przeby&#263;"
  ]
  node [
    id 1161
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 1162
    label "zanosi&#263;"
  ]
  node [
    id 1163
    label "inform"
  ]
  node [
    id 1164
    label "zu&#380;y&#263;"
  ]
  node [
    id 1165
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 1166
    label "introduce"
  ]
  node [
    id 1167
    label "render"
  ]
  node [
    id 1168
    label "ci&#261;&#380;a"
  ]
  node [
    id 1169
    label "informowa&#263;"
  ]
  node [
    id 1170
    label "odby&#263;"
  ]
  node [
    id 1171
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1172
    label "traversal"
  ]
  node [
    id 1173
    label "zaatakowa&#263;"
  ]
  node [
    id 1174
    label "overwhelm"
  ]
  node [
    id 1175
    label "prze&#380;y&#263;"
  ]
  node [
    id 1176
    label "powiada&#263;"
  ]
  node [
    id 1177
    label "komunikowa&#263;"
  ]
  node [
    id 1178
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1179
    label "bind"
  ]
  node [
    id 1180
    label "dodawa&#263;"
  ]
  node [
    id 1181
    label "dokoptowywa&#263;"
  ]
  node [
    id 1182
    label "submit"
  ]
  node [
    id 1183
    label "spotyka&#263;"
  ]
  node [
    id 1184
    label "fall"
  ]
  node [
    id 1185
    label "winnings"
  ]
  node [
    id 1186
    label "consume"
  ]
  node [
    id 1187
    label "zrobi&#263;"
  ]
  node [
    id 1188
    label "dostarcza&#263;"
  ]
  node [
    id 1189
    label "kry&#263;"
  ]
  node [
    id 1190
    label "usi&#322;owa&#263;"
  ]
  node [
    id 1191
    label "gestoza"
  ]
  node [
    id 1192
    label "kuwada"
  ]
  node [
    id 1193
    label "teleangiektazja"
  ]
  node [
    id 1194
    label "guzek_Montgomery'ego"
  ]
  node [
    id 1195
    label "proces_fizjologiczny"
  ]
  node [
    id 1196
    label "donoszenie"
  ]
  node [
    id 1197
    label "rozmna&#380;anie"
  ]
  node [
    id 1198
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1199
    label "gwiazda"
  ]
  node [
    id 1200
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1201
    label "Arktur"
  ]
  node [
    id 1202
    label "kszta&#322;t"
  ]
  node [
    id 1203
    label "Gwiazda_Polarna"
  ]
  node [
    id 1204
    label "agregatka"
  ]
  node [
    id 1205
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 1206
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1207
    label "Nibiru"
  ]
  node [
    id 1208
    label "konstelacja"
  ]
  node [
    id 1209
    label "ornament"
  ]
  node [
    id 1210
    label "delta_Scuti"
  ]
  node [
    id 1211
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1212
    label "s&#322;awa"
  ]
  node [
    id 1213
    label "promie&#324;"
  ]
  node [
    id 1214
    label "star"
  ]
  node [
    id 1215
    label "gwiazdosz"
  ]
  node [
    id 1216
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 1217
    label "asocjacja_gwiazd"
  ]
  node [
    id 1218
    label "supergrupa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 224
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 14
    target 635
  ]
  edge [
    source 14
    target 289
  ]
  edge [
    source 14
    target 636
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 640
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 268
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 52
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 219
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 409
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 239
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 227
  ]
  edge [
    source 18
    target 413
  ]
  edge [
    source 18
    target 208
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 18
    target 221
  ]
  edge [
    source 18
    target 222
  ]
  edge [
    source 18
    target 223
  ]
  edge [
    source 18
    target 224
  ]
  edge [
    source 18
    target 225
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 228
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 408
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 370
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 415
  ]
  edge [
    source 18
    target 324
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 20
    target 813
  ]
  edge [
    source 20
    target 814
  ]
  edge [
    source 20
    target 815
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 20
    target 818
  ]
  edge [
    source 20
    target 819
  ]
  edge [
    source 20
    target 820
  ]
  edge [
    source 20
    target 821
  ]
  edge [
    source 20
    target 822
  ]
  edge [
    source 20
    target 823
  ]
  edge [
    source 20
    target 824
  ]
  edge [
    source 20
    target 825
  ]
  edge [
    source 20
    target 826
  ]
  edge [
    source 20
    target 827
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 21
    target 829
  ]
  edge [
    source 21
    target 830
  ]
  edge [
    source 21
    target 831
  ]
  edge [
    source 21
    target 67
  ]
  edge [
    source 21
    target 832
  ]
  edge [
    source 21
    target 833
  ]
  edge [
    source 21
    target 834
  ]
  edge [
    source 21
    target 835
  ]
  edge [
    source 21
    target 836
  ]
  edge [
    source 21
    target 837
  ]
  edge [
    source 21
    target 838
  ]
  edge [
    source 21
    target 839
  ]
  edge [
    source 21
    target 840
  ]
  edge [
    source 21
    target 841
  ]
  edge [
    source 21
    target 145
  ]
  edge [
    source 21
    target 842
  ]
  edge [
    source 21
    target 843
  ]
  edge [
    source 21
    target 644
  ]
  edge [
    source 21
    target 844
  ]
  edge [
    source 21
    target 845
  ]
  edge [
    source 21
    target 846
  ]
  edge [
    source 21
    target 847
  ]
  edge [
    source 21
    target 848
  ]
  edge [
    source 21
    target 849
  ]
  edge [
    source 21
    target 850
  ]
  edge [
    source 21
    target 851
  ]
  edge [
    source 21
    target 852
  ]
  edge [
    source 21
    target 853
  ]
  edge [
    source 21
    target 854
  ]
  edge [
    source 21
    target 855
  ]
  edge [
    source 21
    target 856
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 858
  ]
  edge [
    source 21
    target 859
  ]
  edge [
    source 21
    target 860
  ]
  edge [
    source 21
    target 620
  ]
  edge [
    source 21
    target 362
  ]
  edge [
    source 21
    target 861
  ]
  edge [
    source 21
    target 862
  ]
  edge [
    source 21
    target 863
  ]
  edge [
    source 21
    target 864
  ]
  edge [
    source 21
    target 865
  ]
  edge [
    source 21
    target 866
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 871
  ]
  edge [
    source 21
    target 872
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 875
  ]
  edge [
    source 21
    target 521
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 639
  ]
  edge [
    source 21
    target 640
  ]
  edge [
    source 21
    target 641
  ]
  edge [
    source 21
    target 642
  ]
  edge [
    source 21
    target 643
  ]
  edge [
    source 21
    target 645
  ]
  edge [
    source 21
    target 646
  ]
  edge [
    source 21
    target 647
  ]
  edge [
    source 21
    target 648
  ]
  edge [
    source 21
    target 649
  ]
  edge [
    source 21
    target 650
  ]
  edge [
    source 21
    target 651
  ]
  edge [
    source 21
    target 652
  ]
  edge [
    source 21
    target 653
  ]
  edge [
    source 21
    target 654
  ]
  edge [
    source 21
    target 655
  ]
  edge [
    source 21
    target 656
  ]
  edge [
    source 21
    target 657
  ]
  edge [
    source 21
    target 658
  ]
  edge [
    source 21
    target 659
  ]
  edge [
    source 21
    target 660
  ]
  edge [
    source 21
    target 661
  ]
  edge [
    source 21
    target 662
  ]
  edge [
    source 21
    target 663
  ]
  edge [
    source 21
    target 664
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 665
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 328
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 340
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 629
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 317
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 816
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 34
  ]
  edge [
    source 21
    target 455
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 162
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 268
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 163
  ]
  edge [
    source 21
    target 516
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 741
  ]
  edge [
    source 21
    target 425
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 750
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 21
    target 365
  ]
  edge [
    source 21
    target 357
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 998
  ]
  edge [
    source 21
    target 999
  ]
  edge [
    source 21
    target 1000
  ]
  edge [
    source 21
    target 1001
  ]
  edge [
    source 21
    target 1002
  ]
  edge [
    source 21
    target 1003
  ]
  edge [
    source 21
    target 1004
  ]
  edge [
    source 21
    target 1005
  ]
  edge [
    source 21
    target 1006
  ]
  edge [
    source 21
    target 481
  ]
  edge [
    source 21
    target 1007
  ]
  edge [
    source 21
    target 1008
  ]
  edge [
    source 21
    target 1009
  ]
  edge [
    source 21
    target 1010
  ]
  edge [
    source 21
    target 1011
  ]
  edge [
    source 21
    target 1012
  ]
  edge [
    source 21
    target 1013
  ]
  edge [
    source 21
    target 1014
  ]
  edge [
    source 21
    target 141
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 143
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 150
  ]
  edge [
    source 21
    target 151
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 154
  ]
  edge [
    source 21
    target 155
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 1015
  ]
  edge [
    source 21
    target 1016
  ]
  edge [
    source 21
    target 1017
  ]
  edge [
    source 21
    target 1018
  ]
  edge [
    source 21
    target 1019
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 1021
  ]
  edge [
    source 21
    target 557
  ]
  edge [
    source 21
    target 1022
  ]
  edge [
    source 21
    target 299
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 119
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 1052
  ]
  edge [
    source 21
    target 1053
  ]
  edge [
    source 21
    target 1054
  ]
  edge [
    source 21
    target 1055
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1120
  ]
  edge [
    source 21
    target 1121
  ]
  edge [
    source 21
    target 1122
  ]
  edge [
    source 21
    target 1123
  ]
  edge [
    source 21
    target 492
  ]
  edge [
    source 21
    target 1124
  ]
  edge [
    source 21
    target 1125
  ]
  edge [
    source 21
    target 1126
  ]
  edge [
    source 21
    target 1127
  ]
  edge [
    source 21
    target 1128
  ]
  edge [
    source 21
    target 1129
  ]
  edge [
    source 21
    target 1130
  ]
  edge [
    source 21
    target 1131
  ]
  edge [
    source 21
    target 1132
  ]
  edge [
    source 21
    target 1133
  ]
  edge [
    source 21
    target 1134
  ]
  edge [
    source 21
    target 1135
  ]
  edge [
    source 21
    target 1136
  ]
  edge [
    source 21
    target 1137
  ]
  edge [
    source 21
    target 493
  ]
  edge [
    source 21
    target 1138
  ]
  edge [
    source 21
    target 1139
  ]
  edge [
    source 21
    target 1140
  ]
  edge [
    source 21
    target 1141
  ]
  edge [
    source 21
    target 1142
  ]
  edge [
    source 21
    target 1143
  ]
  edge [
    source 21
    target 1144
  ]
  edge [
    source 21
    target 1145
  ]
  edge [
    source 21
    target 1146
  ]
  edge [
    source 21
    target 1147
  ]
  edge [
    source 21
    target 1148
  ]
  edge [
    source 21
    target 1149
  ]
  edge [
    source 21
    target 561
  ]
  edge [
    source 21
    target 1150
  ]
  edge [
    source 21
    target 1151
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1165
  ]
  edge [
    source 22
    target 259
  ]
  edge [
    source 22
    target 1166
  ]
  edge [
    source 22
    target 1167
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 258
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 257
  ]
  edge [
    source 22
    target 265
  ]
  edge [
    source 22
    target 375
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 755
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 23
    target 1199
  ]
  edge [
    source 23
    target 1200
  ]
  edge [
    source 23
    target 1201
  ]
  edge [
    source 23
    target 1202
  ]
  edge [
    source 23
    target 1203
  ]
  edge [
    source 23
    target 1204
  ]
  edge [
    source 23
    target 643
  ]
  edge [
    source 23
    target 1205
  ]
  edge [
    source 23
    target 1206
  ]
  edge [
    source 23
    target 1207
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 1209
  ]
  edge [
    source 23
    target 1210
  ]
  edge [
    source 23
    target 1211
  ]
  edge [
    source 23
    target 863
  ]
  edge [
    source 23
    target 516
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 23
    target 1217
  ]
  edge [
    source 23
    target 1218
  ]
]
