graph [
  node [
    id 0
    label "przeprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "co&#347;"
    origin "text"
  ]
  node [
    id 4
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ten"
    origin "text"
  ]
  node [
    id 6
    label "muza"
    origin "text"
  ]
  node [
    id 7
    label "adam"
    origin "text"
  ]
  node [
    id 8
    label "us&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 9
    label "ludzki"
    origin "text"
  ]
  node [
    id 10
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 11
    label "otworzy&#263;"
    origin "text"
  ]
  node [
    id 12
    label "oko"
    origin "text"
  ]
  node [
    id 13
    label "pomara&#324;czowo"
    origin "text"
  ]
  node [
    id 14
    label "czarna"
    origin "text"
  ]
  node [
    id 15
    label "ok&#322;adka"
    origin "text"
  ]
  node [
    id 16
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 17
    label "krew"
    origin "text"
  ]
  node [
    id 18
    label "albo"
    origin "text"
  ]
  node [
    id 19
    label "ropa"
    origin "text"
  ]
  node [
    id 20
    label "zamieni&#263;"
    origin "text"
  ]
  node [
    id 21
    label "si&#281;"
    origin "text"
  ]
  node [
    id 22
    label "blady"
    origin "text"
  ]
  node [
    id 23
    label "twarz"
    origin "text"
  ]
  node [
    id 24
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 25
    label "&#347;lad"
    origin "text"
  ]
  node [
    id 26
    label "czerwona"
    origin "text"
  ]
  node [
    id 27
    label "szminka"
    origin "text"
  ]
  node [
    id 28
    label "szyja"
    origin "text"
  ]
  node [
    id 29
    label "sk&#322;ada&#263;"
  ]
  node [
    id 30
    label "przekazywa&#263;"
  ]
  node [
    id 31
    label "zbiera&#263;"
  ]
  node [
    id 32
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 33
    label "przywraca&#263;"
  ]
  node [
    id 34
    label "dawa&#263;"
  ]
  node [
    id 35
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 36
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 37
    label "convey"
  ]
  node [
    id 38
    label "publicize"
  ]
  node [
    id 39
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 40
    label "render"
  ]
  node [
    id 41
    label "uk&#322;ada&#263;"
  ]
  node [
    id 42
    label "opracowywa&#263;"
  ]
  node [
    id 43
    label "set"
  ]
  node [
    id 44
    label "oddawa&#263;"
  ]
  node [
    id 45
    label "train"
  ]
  node [
    id 46
    label "zmienia&#263;"
  ]
  node [
    id 47
    label "dzieli&#263;"
  ]
  node [
    id 48
    label "scala&#263;"
  ]
  node [
    id 49
    label "zestaw"
  ]
  node [
    id 50
    label "belfer"
  ]
  node [
    id 51
    label "murza"
  ]
  node [
    id 52
    label "cz&#322;owiek"
  ]
  node [
    id 53
    label "ojciec"
  ]
  node [
    id 54
    label "samiec"
  ]
  node [
    id 55
    label "androlog"
  ]
  node [
    id 56
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 57
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 58
    label "efendi"
  ]
  node [
    id 59
    label "opiekun"
  ]
  node [
    id 60
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 61
    label "pa&#324;stwo"
  ]
  node [
    id 62
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 63
    label "bratek"
  ]
  node [
    id 64
    label "Mieszko_I"
  ]
  node [
    id 65
    label "Midas"
  ]
  node [
    id 66
    label "m&#261;&#380;"
  ]
  node [
    id 67
    label "bogaty"
  ]
  node [
    id 68
    label "popularyzator"
  ]
  node [
    id 69
    label "pracodawca"
  ]
  node [
    id 70
    label "kszta&#322;ciciel"
  ]
  node [
    id 71
    label "preceptor"
  ]
  node [
    id 72
    label "nabab"
  ]
  node [
    id 73
    label "pupil"
  ]
  node [
    id 74
    label "andropauza"
  ]
  node [
    id 75
    label "zwrot"
  ]
  node [
    id 76
    label "przyw&#243;dca"
  ]
  node [
    id 77
    label "doros&#322;y"
  ]
  node [
    id 78
    label "pedagog"
  ]
  node [
    id 79
    label "rz&#261;dzenie"
  ]
  node [
    id 80
    label "jegomo&#347;&#263;"
  ]
  node [
    id 81
    label "szkolnik"
  ]
  node [
    id 82
    label "ch&#322;opina"
  ]
  node [
    id 83
    label "w&#322;odarz"
  ]
  node [
    id 84
    label "profesor"
  ]
  node [
    id 85
    label "gra_w_karty"
  ]
  node [
    id 86
    label "w&#322;adza"
  ]
  node [
    id 87
    label "Fidel_Castro"
  ]
  node [
    id 88
    label "Anders"
  ]
  node [
    id 89
    label "Ko&#347;ciuszko"
  ]
  node [
    id 90
    label "Tito"
  ]
  node [
    id 91
    label "Miko&#322;ajczyk"
  ]
  node [
    id 92
    label "Sabataj_Cwi"
  ]
  node [
    id 93
    label "lider"
  ]
  node [
    id 94
    label "Mao"
  ]
  node [
    id 95
    label "p&#322;atnik"
  ]
  node [
    id 96
    label "zwierzchnik"
  ]
  node [
    id 97
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 98
    label "nadzorca"
  ]
  node [
    id 99
    label "funkcjonariusz"
  ]
  node [
    id 100
    label "podmiot"
  ]
  node [
    id 101
    label "wykupienie"
  ]
  node [
    id 102
    label "bycie_w_posiadaniu"
  ]
  node [
    id 103
    label "wykupywanie"
  ]
  node [
    id 104
    label "rozszerzyciel"
  ]
  node [
    id 105
    label "ludzko&#347;&#263;"
  ]
  node [
    id 106
    label "asymilowanie"
  ]
  node [
    id 107
    label "wapniak"
  ]
  node [
    id 108
    label "asymilowa&#263;"
  ]
  node [
    id 109
    label "os&#322;abia&#263;"
  ]
  node [
    id 110
    label "posta&#263;"
  ]
  node [
    id 111
    label "hominid"
  ]
  node [
    id 112
    label "podw&#322;adny"
  ]
  node [
    id 113
    label "os&#322;abianie"
  ]
  node [
    id 114
    label "g&#322;owa"
  ]
  node [
    id 115
    label "figura"
  ]
  node [
    id 116
    label "portrecista"
  ]
  node [
    id 117
    label "dwun&#243;g"
  ]
  node [
    id 118
    label "profanum"
  ]
  node [
    id 119
    label "mikrokosmos"
  ]
  node [
    id 120
    label "nasada"
  ]
  node [
    id 121
    label "duch"
  ]
  node [
    id 122
    label "antropochoria"
  ]
  node [
    id 123
    label "osoba"
  ]
  node [
    id 124
    label "wz&#243;r"
  ]
  node [
    id 125
    label "senior"
  ]
  node [
    id 126
    label "oddzia&#322;ywanie"
  ]
  node [
    id 127
    label "Adam"
  ]
  node [
    id 128
    label "homo_sapiens"
  ]
  node [
    id 129
    label "polifag"
  ]
  node [
    id 130
    label "wydoro&#347;lenie"
  ]
  node [
    id 131
    label "du&#380;y"
  ]
  node [
    id 132
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 133
    label "doro&#347;lenie"
  ]
  node [
    id 134
    label "&#378;ra&#322;y"
  ]
  node [
    id 135
    label "doro&#347;le"
  ]
  node [
    id 136
    label "dojrzale"
  ]
  node [
    id 137
    label "dojrza&#322;y"
  ]
  node [
    id 138
    label "m&#261;dry"
  ]
  node [
    id 139
    label "doletni"
  ]
  node [
    id 140
    label "punkt"
  ]
  node [
    id 141
    label "turn"
  ]
  node [
    id 142
    label "turning"
  ]
  node [
    id 143
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 144
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 145
    label "skr&#281;t"
  ]
  node [
    id 146
    label "obr&#243;t"
  ]
  node [
    id 147
    label "fraza_czasownikowa"
  ]
  node [
    id 148
    label "jednostka_leksykalna"
  ]
  node [
    id 149
    label "zmiana"
  ]
  node [
    id 150
    label "wyra&#380;enie"
  ]
  node [
    id 151
    label "starosta"
  ]
  node [
    id 152
    label "zarz&#261;dca"
  ]
  node [
    id 153
    label "w&#322;adca"
  ]
  node [
    id 154
    label "nauczyciel"
  ]
  node [
    id 155
    label "stopie&#324;_naukowy"
  ]
  node [
    id 156
    label "nauczyciel_akademicki"
  ]
  node [
    id 157
    label "tytu&#322;"
  ]
  node [
    id 158
    label "profesura"
  ]
  node [
    id 159
    label "konsulent"
  ]
  node [
    id 160
    label "wirtuoz"
  ]
  node [
    id 161
    label "autor"
  ]
  node [
    id 162
    label "wyprawka"
  ]
  node [
    id 163
    label "mundurek"
  ]
  node [
    id 164
    label "szko&#322;a"
  ]
  node [
    id 165
    label "tarcza"
  ]
  node [
    id 166
    label "elew"
  ]
  node [
    id 167
    label "absolwent"
  ]
  node [
    id 168
    label "klasa"
  ]
  node [
    id 169
    label "ekspert"
  ]
  node [
    id 170
    label "ochotnik"
  ]
  node [
    id 171
    label "pomocnik"
  ]
  node [
    id 172
    label "student"
  ]
  node [
    id 173
    label "nauczyciel_muzyki"
  ]
  node [
    id 174
    label "zakonnik"
  ]
  node [
    id 175
    label "urz&#281;dnik"
  ]
  node [
    id 176
    label "bogacz"
  ]
  node [
    id 177
    label "dostojnik"
  ]
  node [
    id 178
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 179
    label "kuwada"
  ]
  node [
    id 180
    label "tworzyciel"
  ]
  node [
    id 181
    label "rodzice"
  ]
  node [
    id 182
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 183
    label "&#347;w"
  ]
  node [
    id 184
    label "pomys&#322;odawca"
  ]
  node [
    id 185
    label "rodzic"
  ]
  node [
    id 186
    label "wykonawca"
  ]
  node [
    id 187
    label "ojczym"
  ]
  node [
    id 188
    label "przodek"
  ]
  node [
    id 189
    label "papa"
  ]
  node [
    id 190
    label "stary"
  ]
  node [
    id 191
    label "kochanek"
  ]
  node [
    id 192
    label "fio&#322;ek"
  ]
  node [
    id 193
    label "facet"
  ]
  node [
    id 194
    label "brat"
  ]
  node [
    id 195
    label "zwierz&#281;"
  ]
  node [
    id 196
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 197
    label "ma&#322;&#380;onek"
  ]
  node [
    id 198
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 199
    label "m&#243;j"
  ]
  node [
    id 200
    label "ch&#322;op"
  ]
  node [
    id 201
    label "pan_m&#322;ody"
  ]
  node [
    id 202
    label "&#347;lubny"
  ]
  node [
    id 203
    label "pan_domu"
  ]
  node [
    id 204
    label "pan_i_w&#322;adca"
  ]
  node [
    id 205
    label "mo&#347;&#263;"
  ]
  node [
    id 206
    label "Frygia"
  ]
  node [
    id 207
    label "sprawowanie"
  ]
  node [
    id 208
    label "dominion"
  ]
  node [
    id 209
    label "dominowanie"
  ]
  node [
    id 210
    label "reign"
  ]
  node [
    id 211
    label "rule"
  ]
  node [
    id 212
    label "zwierz&#281;_domowe"
  ]
  node [
    id 213
    label "J&#281;drzejewicz"
  ]
  node [
    id 214
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 215
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 216
    label "John_Dewey"
  ]
  node [
    id 217
    label "specjalista"
  ]
  node [
    id 218
    label "&#380;ycie"
  ]
  node [
    id 219
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 220
    label "Turek"
  ]
  node [
    id 221
    label "effendi"
  ]
  node [
    id 222
    label "obfituj&#261;cy"
  ]
  node [
    id 223
    label "r&#243;&#380;norodny"
  ]
  node [
    id 224
    label "spania&#322;y"
  ]
  node [
    id 225
    label "obficie"
  ]
  node [
    id 226
    label "sytuowany"
  ]
  node [
    id 227
    label "och&#281;do&#380;ny"
  ]
  node [
    id 228
    label "forsiasty"
  ]
  node [
    id 229
    label "zapa&#347;ny"
  ]
  node [
    id 230
    label "bogato"
  ]
  node [
    id 231
    label "Katar"
  ]
  node [
    id 232
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 233
    label "Libia"
  ]
  node [
    id 234
    label "Gwatemala"
  ]
  node [
    id 235
    label "Afganistan"
  ]
  node [
    id 236
    label "Ekwador"
  ]
  node [
    id 237
    label "Tad&#380;ykistan"
  ]
  node [
    id 238
    label "Bhutan"
  ]
  node [
    id 239
    label "Argentyna"
  ]
  node [
    id 240
    label "D&#380;ibuti"
  ]
  node [
    id 241
    label "Wenezuela"
  ]
  node [
    id 242
    label "Ukraina"
  ]
  node [
    id 243
    label "Gabon"
  ]
  node [
    id 244
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 245
    label "Rwanda"
  ]
  node [
    id 246
    label "Liechtenstein"
  ]
  node [
    id 247
    label "organizacja"
  ]
  node [
    id 248
    label "Sri_Lanka"
  ]
  node [
    id 249
    label "Madagaskar"
  ]
  node [
    id 250
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 251
    label "Tonga"
  ]
  node [
    id 252
    label "Kongo"
  ]
  node [
    id 253
    label "Bangladesz"
  ]
  node [
    id 254
    label "Kanada"
  ]
  node [
    id 255
    label "Wehrlen"
  ]
  node [
    id 256
    label "Algieria"
  ]
  node [
    id 257
    label "Surinam"
  ]
  node [
    id 258
    label "Chile"
  ]
  node [
    id 259
    label "Sahara_Zachodnia"
  ]
  node [
    id 260
    label "Uganda"
  ]
  node [
    id 261
    label "W&#281;gry"
  ]
  node [
    id 262
    label "Birma"
  ]
  node [
    id 263
    label "Kazachstan"
  ]
  node [
    id 264
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 265
    label "Armenia"
  ]
  node [
    id 266
    label "Tuwalu"
  ]
  node [
    id 267
    label "Timor_Wschodni"
  ]
  node [
    id 268
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 269
    label "Izrael"
  ]
  node [
    id 270
    label "Estonia"
  ]
  node [
    id 271
    label "Komory"
  ]
  node [
    id 272
    label "Kamerun"
  ]
  node [
    id 273
    label "Haiti"
  ]
  node [
    id 274
    label "Belize"
  ]
  node [
    id 275
    label "Sierra_Leone"
  ]
  node [
    id 276
    label "Luksemburg"
  ]
  node [
    id 277
    label "USA"
  ]
  node [
    id 278
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 279
    label "Barbados"
  ]
  node [
    id 280
    label "San_Marino"
  ]
  node [
    id 281
    label "Bu&#322;garia"
  ]
  node [
    id 282
    label "Wietnam"
  ]
  node [
    id 283
    label "Indonezja"
  ]
  node [
    id 284
    label "Malawi"
  ]
  node [
    id 285
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 286
    label "Francja"
  ]
  node [
    id 287
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 288
    label "partia"
  ]
  node [
    id 289
    label "Zambia"
  ]
  node [
    id 290
    label "Angola"
  ]
  node [
    id 291
    label "Grenada"
  ]
  node [
    id 292
    label "Nepal"
  ]
  node [
    id 293
    label "Panama"
  ]
  node [
    id 294
    label "Rumunia"
  ]
  node [
    id 295
    label "Czarnog&#243;ra"
  ]
  node [
    id 296
    label "Malediwy"
  ]
  node [
    id 297
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 298
    label "S&#322;owacja"
  ]
  node [
    id 299
    label "para"
  ]
  node [
    id 300
    label "Egipt"
  ]
  node [
    id 301
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 302
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 303
    label "Kolumbia"
  ]
  node [
    id 304
    label "Mozambik"
  ]
  node [
    id 305
    label "Laos"
  ]
  node [
    id 306
    label "Burundi"
  ]
  node [
    id 307
    label "Suazi"
  ]
  node [
    id 308
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 309
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 310
    label "Czechy"
  ]
  node [
    id 311
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 312
    label "Wyspy_Marshalla"
  ]
  node [
    id 313
    label "Trynidad_i_Tobago"
  ]
  node [
    id 314
    label "Dominika"
  ]
  node [
    id 315
    label "Palau"
  ]
  node [
    id 316
    label "Syria"
  ]
  node [
    id 317
    label "Gwinea_Bissau"
  ]
  node [
    id 318
    label "Liberia"
  ]
  node [
    id 319
    label "Zimbabwe"
  ]
  node [
    id 320
    label "Polska"
  ]
  node [
    id 321
    label "Jamajka"
  ]
  node [
    id 322
    label "Dominikana"
  ]
  node [
    id 323
    label "Senegal"
  ]
  node [
    id 324
    label "Gruzja"
  ]
  node [
    id 325
    label "Togo"
  ]
  node [
    id 326
    label "Chorwacja"
  ]
  node [
    id 327
    label "Meksyk"
  ]
  node [
    id 328
    label "Macedonia"
  ]
  node [
    id 329
    label "Gujana"
  ]
  node [
    id 330
    label "Zair"
  ]
  node [
    id 331
    label "Albania"
  ]
  node [
    id 332
    label "Kambod&#380;a"
  ]
  node [
    id 333
    label "Mauritius"
  ]
  node [
    id 334
    label "Monako"
  ]
  node [
    id 335
    label "Gwinea"
  ]
  node [
    id 336
    label "Mali"
  ]
  node [
    id 337
    label "Nigeria"
  ]
  node [
    id 338
    label "Kostaryka"
  ]
  node [
    id 339
    label "Hanower"
  ]
  node [
    id 340
    label "Paragwaj"
  ]
  node [
    id 341
    label "W&#322;ochy"
  ]
  node [
    id 342
    label "Wyspy_Salomona"
  ]
  node [
    id 343
    label "Seszele"
  ]
  node [
    id 344
    label "Hiszpania"
  ]
  node [
    id 345
    label "Boliwia"
  ]
  node [
    id 346
    label "Kirgistan"
  ]
  node [
    id 347
    label "Irlandia"
  ]
  node [
    id 348
    label "Czad"
  ]
  node [
    id 349
    label "Irak"
  ]
  node [
    id 350
    label "Lesoto"
  ]
  node [
    id 351
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 352
    label "Malta"
  ]
  node [
    id 353
    label "Andora"
  ]
  node [
    id 354
    label "Chiny"
  ]
  node [
    id 355
    label "Filipiny"
  ]
  node [
    id 356
    label "Antarktis"
  ]
  node [
    id 357
    label "Niemcy"
  ]
  node [
    id 358
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 359
    label "Brazylia"
  ]
  node [
    id 360
    label "terytorium"
  ]
  node [
    id 361
    label "Nikaragua"
  ]
  node [
    id 362
    label "Pakistan"
  ]
  node [
    id 363
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 364
    label "Kenia"
  ]
  node [
    id 365
    label "Niger"
  ]
  node [
    id 366
    label "Tunezja"
  ]
  node [
    id 367
    label "Portugalia"
  ]
  node [
    id 368
    label "Fid&#380;i"
  ]
  node [
    id 369
    label "Maroko"
  ]
  node [
    id 370
    label "Botswana"
  ]
  node [
    id 371
    label "Tajlandia"
  ]
  node [
    id 372
    label "Australia"
  ]
  node [
    id 373
    label "Burkina_Faso"
  ]
  node [
    id 374
    label "interior"
  ]
  node [
    id 375
    label "Benin"
  ]
  node [
    id 376
    label "Tanzania"
  ]
  node [
    id 377
    label "Indie"
  ]
  node [
    id 378
    label "&#321;otwa"
  ]
  node [
    id 379
    label "Kiribati"
  ]
  node [
    id 380
    label "Antigua_i_Barbuda"
  ]
  node [
    id 381
    label "Rodezja"
  ]
  node [
    id 382
    label "Cypr"
  ]
  node [
    id 383
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 384
    label "Peru"
  ]
  node [
    id 385
    label "Austria"
  ]
  node [
    id 386
    label "Urugwaj"
  ]
  node [
    id 387
    label "Jordania"
  ]
  node [
    id 388
    label "Grecja"
  ]
  node [
    id 389
    label "Azerbejd&#380;an"
  ]
  node [
    id 390
    label "Turcja"
  ]
  node [
    id 391
    label "Samoa"
  ]
  node [
    id 392
    label "Sudan"
  ]
  node [
    id 393
    label "Oman"
  ]
  node [
    id 394
    label "ziemia"
  ]
  node [
    id 395
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 396
    label "Uzbekistan"
  ]
  node [
    id 397
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 398
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 399
    label "Honduras"
  ]
  node [
    id 400
    label "Mongolia"
  ]
  node [
    id 401
    label "Portoryko"
  ]
  node [
    id 402
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 403
    label "Serbia"
  ]
  node [
    id 404
    label "Tajwan"
  ]
  node [
    id 405
    label "Wielka_Brytania"
  ]
  node [
    id 406
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 407
    label "Liban"
  ]
  node [
    id 408
    label "Japonia"
  ]
  node [
    id 409
    label "Ghana"
  ]
  node [
    id 410
    label "Bahrajn"
  ]
  node [
    id 411
    label "Belgia"
  ]
  node [
    id 412
    label "Etiopia"
  ]
  node [
    id 413
    label "Mikronezja"
  ]
  node [
    id 414
    label "Kuwejt"
  ]
  node [
    id 415
    label "grupa"
  ]
  node [
    id 416
    label "Bahamy"
  ]
  node [
    id 417
    label "Rosja"
  ]
  node [
    id 418
    label "Mo&#322;dawia"
  ]
  node [
    id 419
    label "Litwa"
  ]
  node [
    id 420
    label "S&#322;owenia"
  ]
  node [
    id 421
    label "Szwajcaria"
  ]
  node [
    id 422
    label "Erytrea"
  ]
  node [
    id 423
    label "Kuba"
  ]
  node [
    id 424
    label "Arabia_Saudyjska"
  ]
  node [
    id 425
    label "granica_pa&#324;stwa"
  ]
  node [
    id 426
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 427
    label "Malezja"
  ]
  node [
    id 428
    label "Korea"
  ]
  node [
    id 429
    label "Jemen"
  ]
  node [
    id 430
    label "Nowa_Zelandia"
  ]
  node [
    id 431
    label "Namibia"
  ]
  node [
    id 432
    label "Nauru"
  ]
  node [
    id 433
    label "holoarktyka"
  ]
  node [
    id 434
    label "Brunei"
  ]
  node [
    id 435
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 436
    label "Khitai"
  ]
  node [
    id 437
    label "Mauretania"
  ]
  node [
    id 438
    label "Iran"
  ]
  node [
    id 439
    label "Gambia"
  ]
  node [
    id 440
    label "Somalia"
  ]
  node [
    id 441
    label "Holandia"
  ]
  node [
    id 442
    label "Turkmenistan"
  ]
  node [
    id 443
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 444
    label "Salwador"
  ]
  node [
    id 445
    label "thing"
  ]
  node [
    id 446
    label "cosik"
  ]
  node [
    id 447
    label "post&#261;pi&#263;"
  ]
  node [
    id 448
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 449
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 450
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 451
    label "zorganizowa&#263;"
  ]
  node [
    id 452
    label "appoint"
  ]
  node [
    id 453
    label "wystylizowa&#263;"
  ]
  node [
    id 454
    label "cause"
  ]
  node [
    id 455
    label "przerobi&#263;"
  ]
  node [
    id 456
    label "nabra&#263;"
  ]
  node [
    id 457
    label "make"
  ]
  node [
    id 458
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 459
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 460
    label "wydali&#263;"
  ]
  node [
    id 461
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 462
    label "advance"
  ]
  node [
    id 463
    label "act"
  ]
  node [
    id 464
    label "see"
  ]
  node [
    id 465
    label "usun&#261;&#263;"
  ]
  node [
    id 466
    label "sack"
  ]
  node [
    id 467
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 468
    label "restore"
  ]
  node [
    id 469
    label "dostosowa&#263;"
  ]
  node [
    id 470
    label "pozyska&#263;"
  ]
  node [
    id 471
    label "stworzy&#263;"
  ]
  node [
    id 472
    label "plan"
  ]
  node [
    id 473
    label "stage"
  ]
  node [
    id 474
    label "urobi&#263;"
  ]
  node [
    id 475
    label "ensnare"
  ]
  node [
    id 476
    label "wprowadzi&#263;"
  ]
  node [
    id 477
    label "zaplanowa&#263;"
  ]
  node [
    id 478
    label "przygotowa&#263;"
  ]
  node [
    id 479
    label "standard"
  ]
  node [
    id 480
    label "skupi&#263;"
  ]
  node [
    id 481
    label "podbi&#263;"
  ]
  node [
    id 482
    label "umocni&#263;"
  ]
  node [
    id 483
    label "doprowadzi&#263;"
  ]
  node [
    id 484
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 485
    label "zadowoli&#263;"
  ]
  node [
    id 486
    label "accommodate"
  ]
  node [
    id 487
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 488
    label "zabezpieczy&#263;"
  ]
  node [
    id 489
    label "wytworzy&#263;"
  ]
  node [
    id 490
    label "pomy&#347;le&#263;"
  ]
  node [
    id 491
    label "woda"
  ]
  node [
    id 492
    label "hoax"
  ]
  node [
    id 493
    label "deceive"
  ]
  node [
    id 494
    label "oszwabi&#263;"
  ]
  node [
    id 495
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 496
    label "objecha&#263;"
  ]
  node [
    id 497
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 498
    label "gull"
  ]
  node [
    id 499
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 500
    label "wzi&#261;&#263;"
  ]
  node [
    id 501
    label "naby&#263;"
  ]
  node [
    id 502
    label "fraud"
  ]
  node [
    id 503
    label "kupi&#263;"
  ]
  node [
    id 504
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 505
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 506
    label "zaliczy&#263;"
  ]
  node [
    id 507
    label "overwork"
  ]
  node [
    id 508
    label "zmodyfikowa&#263;"
  ]
  node [
    id 509
    label "change"
  ]
  node [
    id 510
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 511
    label "przej&#347;&#263;"
  ]
  node [
    id 512
    label "zmieni&#263;"
  ]
  node [
    id 513
    label "convert"
  ]
  node [
    id 514
    label "prze&#380;y&#263;"
  ]
  node [
    id 515
    label "przetworzy&#263;"
  ]
  node [
    id 516
    label "upora&#263;_si&#281;"
  ]
  node [
    id 517
    label "stylize"
  ]
  node [
    id 518
    label "nada&#263;"
  ]
  node [
    id 519
    label "upodobni&#263;"
  ]
  node [
    id 520
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 521
    label "sprawi&#263;"
  ]
  node [
    id 522
    label "okre&#347;lony"
  ]
  node [
    id 523
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 524
    label "wiadomy"
  ]
  node [
    id 525
    label "inspiratorka"
  ]
  node [
    id 526
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 527
    label "banan"
  ]
  node [
    id 528
    label "talent"
  ]
  node [
    id 529
    label "kobieta"
  ]
  node [
    id 530
    label "Melpomena"
  ]
  node [
    id 531
    label "natchnienie"
  ]
  node [
    id 532
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 533
    label "bogini"
  ]
  node [
    id 534
    label "ro&#347;lina"
  ]
  node [
    id 535
    label "muzyka"
  ]
  node [
    id 536
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 537
    label "palma"
  ]
  node [
    id 538
    label "&#380;ona"
  ]
  node [
    id 539
    label "samica"
  ]
  node [
    id 540
    label "uleganie"
  ]
  node [
    id 541
    label "ulec"
  ]
  node [
    id 542
    label "m&#281;&#380;yna"
  ]
  node [
    id 543
    label "partnerka"
  ]
  node [
    id 544
    label "ulegni&#281;cie"
  ]
  node [
    id 545
    label "&#322;ono"
  ]
  node [
    id 546
    label "menopauza"
  ]
  node [
    id 547
    label "przekwitanie"
  ]
  node [
    id 548
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 549
    label "babka"
  ]
  node [
    id 550
    label "ulega&#263;"
  ]
  node [
    id 551
    label "enchantment"
  ]
  node [
    id 552
    label "zach&#281;ta"
  ]
  node [
    id 553
    label "inspiration"
  ]
  node [
    id 554
    label "podekscytowanie"
  ]
  node [
    id 555
    label "brylant"
  ]
  node [
    id 556
    label "dyspozycja"
  ]
  node [
    id 557
    label "gigant"
  ]
  node [
    id 558
    label "faculty"
  ]
  node [
    id 559
    label "stygmat"
  ]
  node [
    id 560
    label "moneta"
  ]
  node [
    id 561
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 562
    label "zbi&#243;r"
  ]
  node [
    id 563
    label "dorobek"
  ]
  node [
    id 564
    label "tworzenie"
  ]
  node [
    id 565
    label "kreacja"
  ]
  node [
    id 566
    label "creation"
  ]
  node [
    id 567
    label "kultura"
  ]
  node [
    id 568
    label "bananowate"
  ]
  node [
    id 569
    label "egzotyk"
  ]
  node [
    id 570
    label "dorobkiewicz"
  ]
  node [
    id 571
    label "snob"
  ]
  node [
    id 572
    label "bylina"
  ]
  node [
    id 573
    label "u&#347;miech"
  ]
  node [
    id 574
    label "bananowa_m&#322;odzie&#380;"
  ]
  node [
    id 575
    label "bananowiec"
  ]
  node [
    id 576
    label "darmozjad"
  ]
  node [
    id 577
    label "torebka"
  ]
  node [
    id 578
    label "owoc_egzotyczny"
  ]
  node [
    id 579
    label "jagoda"
  ]
  node [
    id 580
    label "arekowate"
  ]
  node [
    id 581
    label "przedmiot"
  ]
  node [
    id 582
    label "znak"
  ]
  node [
    id 583
    label "ga&#322;&#261;zka"
  ]
  node [
    id 584
    label "drzewo"
  ]
  node [
    id 585
    label "k&#322;odzina"
  ]
  node [
    id 586
    label "palm"
  ]
  node [
    id 587
    label "zbiorowisko"
  ]
  node [
    id 588
    label "ro&#347;liny"
  ]
  node [
    id 589
    label "p&#281;d"
  ]
  node [
    id 590
    label "wegetowanie"
  ]
  node [
    id 591
    label "zadziorek"
  ]
  node [
    id 592
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 593
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 594
    label "do&#322;owa&#263;"
  ]
  node [
    id 595
    label "wegetacja"
  ]
  node [
    id 596
    label "owoc"
  ]
  node [
    id 597
    label "strzyc"
  ]
  node [
    id 598
    label "w&#322;&#243;kno"
  ]
  node [
    id 599
    label "g&#322;uszenie"
  ]
  node [
    id 600
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 601
    label "fitotron"
  ]
  node [
    id 602
    label "bulwka"
  ]
  node [
    id 603
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 604
    label "odn&#243;&#380;ka"
  ]
  node [
    id 605
    label "epiderma"
  ]
  node [
    id 606
    label "gumoza"
  ]
  node [
    id 607
    label "strzy&#380;enie"
  ]
  node [
    id 608
    label "wypotnik"
  ]
  node [
    id 609
    label "flawonoid"
  ]
  node [
    id 610
    label "wyro&#347;le"
  ]
  node [
    id 611
    label "do&#322;owanie"
  ]
  node [
    id 612
    label "g&#322;uszy&#263;"
  ]
  node [
    id 613
    label "pora&#380;a&#263;"
  ]
  node [
    id 614
    label "fitocenoza"
  ]
  node [
    id 615
    label "hodowla"
  ]
  node [
    id 616
    label "fotoautotrof"
  ]
  node [
    id 617
    label "nieuleczalnie_chory"
  ]
  node [
    id 618
    label "wegetowa&#263;"
  ]
  node [
    id 619
    label "pochewka"
  ]
  node [
    id 620
    label "sok"
  ]
  node [
    id 621
    label "system_korzeniowy"
  ]
  node [
    id 622
    label "zawi&#261;zek"
  ]
  node [
    id 623
    label "boski"
  ]
  node [
    id 624
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 625
    label "Tanit"
  ]
  node [
    id 626
    label "Nemezis"
  ]
  node [
    id 627
    label "tragedia"
  ]
  node [
    id 628
    label "wokalistyka"
  ]
  node [
    id 629
    label "wykonywanie"
  ]
  node [
    id 630
    label "wykonywa&#263;"
  ]
  node [
    id 631
    label "zjawisko"
  ]
  node [
    id 632
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 633
    label "beatbox"
  ]
  node [
    id 634
    label "komponowa&#263;"
  ]
  node [
    id 635
    label "komponowanie"
  ]
  node [
    id 636
    label "wytw&#243;r"
  ]
  node [
    id 637
    label "pasa&#380;"
  ]
  node [
    id 638
    label "notacja_muzyczna"
  ]
  node [
    id 639
    label "kontrapunkt"
  ]
  node [
    id 640
    label "nauka"
  ]
  node [
    id 641
    label "sztuka"
  ]
  node [
    id 642
    label "instrumentalistyka"
  ]
  node [
    id 643
    label "harmonia"
  ]
  node [
    id 644
    label "wys&#322;uchanie"
  ]
  node [
    id 645
    label "kapela"
  ]
  node [
    id 646
    label "britpop"
  ]
  node [
    id 647
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 648
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 649
    label "postrzec"
  ]
  node [
    id 650
    label "cognizance"
  ]
  node [
    id 651
    label "zobaczy&#263;"
  ]
  node [
    id 652
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 653
    label "notice"
  ]
  node [
    id 654
    label "oceni&#263;"
  ]
  node [
    id 655
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 656
    label "respect"
  ]
  node [
    id 657
    label "przyzwoity"
  ]
  node [
    id 658
    label "naturalny"
  ]
  node [
    id 659
    label "ludzko"
  ]
  node [
    id 660
    label "prawdziwy"
  ]
  node [
    id 661
    label "normalny"
  ]
  node [
    id 662
    label "po_ludzku"
  ]
  node [
    id 663
    label "empatyczny"
  ]
  node [
    id 664
    label "skromny"
  ]
  node [
    id 665
    label "kulturalny"
  ]
  node [
    id 666
    label "grzeczny"
  ]
  node [
    id 667
    label "stosowny"
  ]
  node [
    id 668
    label "przystojny"
  ]
  node [
    id 669
    label "niez&#322;y"
  ]
  node [
    id 670
    label "nale&#380;yty"
  ]
  node [
    id 671
    label "moralny"
  ]
  node [
    id 672
    label "przyzwoicie"
  ]
  node [
    id 673
    label "wystarczaj&#261;cy"
  ]
  node [
    id 674
    label "&#380;ywny"
  ]
  node [
    id 675
    label "szczery"
  ]
  node [
    id 676
    label "naprawd&#281;"
  ]
  node [
    id 677
    label "realnie"
  ]
  node [
    id 678
    label "podobny"
  ]
  node [
    id 679
    label "zgodny"
  ]
  node [
    id 680
    label "prawdziwie"
  ]
  node [
    id 681
    label "przeci&#281;tny"
  ]
  node [
    id 682
    label "oczywisty"
  ]
  node [
    id 683
    label "zdr&#243;w"
  ]
  node [
    id 684
    label "zwykle"
  ]
  node [
    id 685
    label "zwyczajny"
  ]
  node [
    id 686
    label "zwyczajnie"
  ]
  node [
    id 687
    label "prawid&#322;owy"
  ]
  node [
    id 688
    label "normalnie"
  ]
  node [
    id 689
    label "cz&#281;sty"
  ]
  node [
    id 690
    label "empatycznie"
  ]
  node [
    id 691
    label "wra&#380;liwy"
  ]
  node [
    id 692
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 693
    label "nale&#380;ny"
  ]
  node [
    id 694
    label "typowy"
  ]
  node [
    id 695
    label "uprawniony"
  ]
  node [
    id 696
    label "zasadniczy"
  ]
  node [
    id 697
    label "stosownie"
  ]
  node [
    id 698
    label "taki"
  ]
  node [
    id 699
    label "charakterystyczny"
  ]
  node [
    id 700
    label "dobry"
  ]
  node [
    id 701
    label "prawy"
  ]
  node [
    id 702
    label "zrozumia&#322;y"
  ]
  node [
    id 703
    label "immanentny"
  ]
  node [
    id 704
    label "bezsporny"
  ]
  node [
    id 705
    label "organicznie"
  ]
  node [
    id 706
    label "pierwotny"
  ]
  node [
    id 707
    label "neutralny"
  ]
  node [
    id 708
    label "rzeczywisty"
  ]
  node [
    id 709
    label "naturalnie"
  ]
  node [
    id 710
    label "po_prostu"
  ]
  node [
    id 711
    label "podobnie"
  ]
  node [
    id 712
    label "zesp&#243;&#322;"
  ]
  node [
    id 713
    label "wpadni&#281;cie"
  ]
  node [
    id 714
    label "wydawa&#263;"
  ]
  node [
    id 715
    label "regestr"
  ]
  node [
    id 716
    label "zdolno&#347;&#263;"
  ]
  node [
    id 717
    label "wyda&#263;"
  ]
  node [
    id 718
    label "wpa&#347;&#263;"
  ]
  node [
    id 719
    label "d&#378;wi&#281;k"
  ]
  node [
    id 720
    label "note"
  ]
  node [
    id 721
    label "&#347;piewak_operowy"
  ]
  node [
    id 722
    label "onomatopeja"
  ]
  node [
    id 723
    label "decyzja"
  ]
  node [
    id 724
    label "linia_melodyczna"
  ]
  node [
    id 725
    label "sound"
  ]
  node [
    id 726
    label "opinion"
  ]
  node [
    id 727
    label "wpada&#263;"
  ]
  node [
    id 728
    label "nakaz"
  ]
  node [
    id 729
    label "matowie&#263;"
  ]
  node [
    id 730
    label "foniatra"
  ]
  node [
    id 731
    label "stanowisko"
  ]
  node [
    id 732
    label "ch&#243;rzysta"
  ]
  node [
    id 733
    label "mutacja"
  ]
  node [
    id 734
    label "&#347;piewaczka"
  ]
  node [
    id 735
    label "zmatowienie"
  ]
  node [
    id 736
    label "wydanie"
  ]
  node [
    id 737
    label "wokal"
  ]
  node [
    id 738
    label "wypowied&#378;"
  ]
  node [
    id 739
    label "emisja"
  ]
  node [
    id 740
    label "zmatowie&#263;"
  ]
  node [
    id 741
    label "&#347;piewak"
  ]
  node [
    id 742
    label "matowienie"
  ]
  node [
    id 743
    label "brzmienie"
  ]
  node [
    id 744
    label "wpadanie"
  ]
  node [
    id 745
    label "posiada&#263;"
  ]
  node [
    id 746
    label "cecha"
  ]
  node [
    id 747
    label "potencja&#322;"
  ]
  node [
    id 748
    label "zapomina&#263;"
  ]
  node [
    id 749
    label "zapomnienie"
  ]
  node [
    id 750
    label "zapominanie"
  ]
  node [
    id 751
    label "ability"
  ]
  node [
    id 752
    label "obliczeniowo"
  ]
  node [
    id 753
    label "zapomnie&#263;"
  ]
  node [
    id 754
    label "odm&#322;adzanie"
  ]
  node [
    id 755
    label "liga"
  ]
  node [
    id 756
    label "jednostka_systematyczna"
  ]
  node [
    id 757
    label "gromada"
  ]
  node [
    id 758
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 759
    label "egzemplarz"
  ]
  node [
    id 760
    label "Entuzjastki"
  ]
  node [
    id 761
    label "kompozycja"
  ]
  node [
    id 762
    label "Terranie"
  ]
  node [
    id 763
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 764
    label "category"
  ]
  node [
    id 765
    label "pakiet_klimatyczny"
  ]
  node [
    id 766
    label "oddzia&#322;"
  ]
  node [
    id 767
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 768
    label "cz&#261;steczka"
  ]
  node [
    id 769
    label "stage_set"
  ]
  node [
    id 770
    label "type"
  ]
  node [
    id 771
    label "specgrupa"
  ]
  node [
    id 772
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 773
    label "&#346;wietliki"
  ]
  node [
    id 774
    label "odm&#322;odzenie"
  ]
  node [
    id 775
    label "Eurogrupa"
  ]
  node [
    id 776
    label "odm&#322;adza&#263;"
  ]
  node [
    id 777
    label "formacja_geologiczna"
  ]
  node [
    id 778
    label "harcerze_starsi"
  ]
  node [
    id 779
    label "statement"
  ]
  node [
    id 780
    label "polecenie"
  ]
  node [
    id 781
    label "bodziec"
  ]
  node [
    id 782
    label "proces"
  ]
  node [
    id 783
    label "krajobraz"
  ]
  node [
    id 784
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 785
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 786
    label "przywidzenie"
  ]
  node [
    id 787
    label "presence"
  ]
  node [
    id 788
    label "charakter"
  ]
  node [
    id 789
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 790
    label "management"
  ]
  node [
    id 791
    label "resolution"
  ]
  node [
    id 792
    label "zdecydowanie"
  ]
  node [
    id 793
    label "dokument"
  ]
  node [
    id 794
    label "Bund"
  ]
  node [
    id 795
    label "PPR"
  ]
  node [
    id 796
    label "wybranek"
  ]
  node [
    id 797
    label "Jakobici"
  ]
  node [
    id 798
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 799
    label "SLD"
  ]
  node [
    id 800
    label "Razem"
  ]
  node [
    id 801
    label "PiS"
  ]
  node [
    id 802
    label "package"
  ]
  node [
    id 803
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 804
    label "Kuomintang"
  ]
  node [
    id 805
    label "ZSL"
  ]
  node [
    id 806
    label "AWS"
  ]
  node [
    id 807
    label "gra"
  ]
  node [
    id 808
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 809
    label "game"
  ]
  node [
    id 810
    label "blok"
  ]
  node [
    id 811
    label "materia&#322;"
  ]
  node [
    id 812
    label "PO"
  ]
  node [
    id 813
    label "si&#322;a"
  ]
  node [
    id 814
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 815
    label "niedoczas"
  ]
  node [
    id 816
    label "Federali&#347;ci"
  ]
  node [
    id 817
    label "PSL"
  ]
  node [
    id 818
    label "Wigowie"
  ]
  node [
    id 819
    label "ZChN"
  ]
  node [
    id 820
    label "egzekutywa"
  ]
  node [
    id 821
    label "aktyw"
  ]
  node [
    id 822
    label "wybranka"
  ]
  node [
    id 823
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 824
    label "unit"
  ]
  node [
    id 825
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 826
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 827
    label "muzyk"
  ]
  node [
    id 828
    label "phone"
  ]
  node [
    id 829
    label "intonacja"
  ]
  node [
    id 830
    label "modalizm"
  ]
  node [
    id 831
    label "nadlecenie"
  ]
  node [
    id 832
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 833
    label "solmizacja"
  ]
  node [
    id 834
    label "seria"
  ]
  node [
    id 835
    label "dobiec"
  ]
  node [
    id 836
    label "transmiter"
  ]
  node [
    id 837
    label "heksachord"
  ]
  node [
    id 838
    label "akcent"
  ]
  node [
    id 839
    label "repetycja"
  ]
  node [
    id 840
    label "po&#322;o&#380;enie"
  ]
  node [
    id 841
    label "pogl&#261;d"
  ]
  node [
    id 842
    label "wojsko"
  ]
  node [
    id 843
    label "awansowa&#263;"
  ]
  node [
    id 844
    label "stawia&#263;"
  ]
  node [
    id 845
    label "uprawianie"
  ]
  node [
    id 846
    label "wakowa&#263;"
  ]
  node [
    id 847
    label "powierzanie"
  ]
  node [
    id 848
    label "postawi&#263;"
  ]
  node [
    id 849
    label "miejsce"
  ]
  node [
    id 850
    label "awansowanie"
  ]
  node [
    id 851
    label "praca"
  ]
  node [
    id 852
    label "pos&#322;uchanie"
  ]
  node [
    id 853
    label "s&#261;d"
  ]
  node [
    id 854
    label "sparafrazowanie"
  ]
  node [
    id 855
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 856
    label "strawestowa&#263;"
  ]
  node [
    id 857
    label "sparafrazowa&#263;"
  ]
  node [
    id 858
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 859
    label "trawestowa&#263;"
  ]
  node [
    id 860
    label "sformu&#322;owanie"
  ]
  node [
    id 861
    label "parafrazowanie"
  ]
  node [
    id 862
    label "ozdobnik"
  ]
  node [
    id 863
    label "delimitacja"
  ]
  node [
    id 864
    label "parafrazowa&#263;"
  ]
  node [
    id 865
    label "stylizacja"
  ]
  node [
    id 866
    label "komunikat"
  ]
  node [
    id 867
    label "trawestowanie"
  ]
  node [
    id 868
    label "strawestowanie"
  ]
  node [
    id 869
    label "rezultat"
  ]
  node [
    id 870
    label "Mazowsze"
  ]
  node [
    id 871
    label "whole"
  ]
  node [
    id 872
    label "skupienie"
  ]
  node [
    id 873
    label "The_Beatles"
  ]
  node [
    id 874
    label "zabudowania"
  ]
  node [
    id 875
    label "group"
  ]
  node [
    id 876
    label "zespolik"
  ]
  node [
    id 877
    label "schorzenie"
  ]
  node [
    id 878
    label "Depeche_Mode"
  ]
  node [
    id 879
    label "batch"
  ]
  node [
    id 880
    label "decline"
  ]
  node [
    id 881
    label "kolor"
  ]
  node [
    id 882
    label "niszczy&#263;_si&#281;"
  ]
  node [
    id 883
    label "dematerializowa&#263;_si&#281;"
  ]
  node [
    id 884
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 885
    label "tarnish"
  ]
  node [
    id 886
    label "przype&#322;za&#263;"
  ]
  node [
    id 887
    label "bledn&#261;&#263;"
  ]
  node [
    id 888
    label "burze&#263;"
  ]
  node [
    id 889
    label "publikacja"
  ]
  node [
    id 890
    label "expense"
  ]
  node [
    id 891
    label "introdukcja"
  ]
  node [
    id 892
    label "wydobywanie"
  ]
  node [
    id 893
    label "przesy&#322;"
  ]
  node [
    id 894
    label "consequence"
  ]
  node [
    id 895
    label "wydzielanie"
  ]
  node [
    id 896
    label "ga&#347;ni&#281;cie"
  ]
  node [
    id 897
    label "stawanie_si&#281;"
  ]
  node [
    id 898
    label "przyt&#322;umiony"
  ]
  node [
    id 899
    label "burzenie"
  ]
  node [
    id 900
    label "matowy"
  ]
  node [
    id 901
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 902
    label "odbarwianie_si&#281;"
  ]
  node [
    id 903
    label "przype&#322;zanie"
  ]
  node [
    id 904
    label "ja&#347;nienie"
  ]
  node [
    id 905
    label "wyblak&#322;y"
  ]
  node [
    id 906
    label "niszczenie_si&#281;"
  ]
  node [
    id 907
    label "laryngolog"
  ]
  node [
    id 908
    label "zniszczenie_si&#281;"
  ]
  node [
    id 909
    label "przype&#322;&#378;ni&#281;cie"
  ]
  node [
    id 910
    label "zja&#347;nienie"
  ]
  node [
    id 911
    label "odbarwienie_si&#281;"
  ]
  node [
    id 912
    label "spowodowanie"
  ]
  node [
    id 913
    label "zmienienie"
  ]
  node [
    id 914
    label "stanie_si&#281;"
  ]
  node [
    id 915
    label "czynno&#347;&#263;"
  ]
  node [
    id 916
    label "zga&#347;ni&#281;cie"
  ]
  node [
    id 917
    label "wyraz_pochodny"
  ]
  node [
    id 918
    label "variation"
  ]
  node [
    id 919
    label "zaburzenie"
  ]
  node [
    id 920
    label "operator"
  ]
  node [
    id 921
    label "odmiana"
  ]
  node [
    id 922
    label "variety"
  ]
  node [
    id 923
    label "proces_fizjologiczny"
  ]
  node [
    id 924
    label "zamiana"
  ]
  node [
    id 925
    label "mutagenny"
  ]
  node [
    id 926
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 927
    label "gen"
  ]
  node [
    id 928
    label "sta&#263;_si&#281;"
  ]
  node [
    id 929
    label "zbledn&#261;&#263;"
  ]
  node [
    id 930
    label "pale"
  ]
  node [
    id 931
    label "zniszczy&#263;_si&#281;"
  ]
  node [
    id 932
    label "przype&#322;zn&#261;&#263;"
  ]
  node [
    id 933
    label "choreuta"
  ]
  node [
    id 934
    label "ch&#243;r"
  ]
  node [
    id 935
    label "wymy&#347;lenie"
  ]
  node [
    id 936
    label "spotkanie"
  ]
  node [
    id 937
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 938
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 939
    label "collapse"
  ]
  node [
    id 940
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 941
    label "rzecz"
  ]
  node [
    id 942
    label "poniesienie"
  ]
  node [
    id 943
    label "zapach"
  ]
  node [
    id 944
    label "ciecz"
  ]
  node [
    id 945
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 946
    label "odwiedzenie"
  ]
  node [
    id 947
    label "uderzenie"
  ]
  node [
    id 948
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 949
    label "rzeka"
  ]
  node [
    id 950
    label "postrzeganie"
  ]
  node [
    id 951
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 952
    label "dostanie_si&#281;"
  ]
  node [
    id 953
    label "&#347;wiat&#322;o"
  ]
  node [
    id 954
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 955
    label "release"
  ]
  node [
    id 956
    label "rozbicie_si&#281;"
  ]
  node [
    id 957
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 958
    label "dostawanie_si&#281;"
  ]
  node [
    id 959
    label "odwiedzanie"
  ]
  node [
    id 960
    label "spotykanie"
  ]
  node [
    id 961
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 962
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 963
    label "wymy&#347;lanie"
  ]
  node [
    id 964
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 965
    label "ingress"
  ]
  node [
    id 966
    label "dzianie_si&#281;"
  ]
  node [
    id 967
    label "wp&#322;ywanie"
  ]
  node [
    id 968
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 969
    label "overlap"
  ]
  node [
    id 970
    label "wkl&#281;sanie"
  ]
  node [
    id 971
    label "powierzy&#263;"
  ]
  node [
    id 972
    label "pieni&#261;dze"
  ]
  node [
    id 973
    label "plon"
  ]
  node [
    id 974
    label "give"
  ]
  node [
    id 975
    label "skojarzy&#263;"
  ]
  node [
    id 976
    label "zadenuncjowa&#263;"
  ]
  node [
    id 977
    label "impart"
  ]
  node [
    id 978
    label "da&#263;"
  ]
  node [
    id 979
    label "reszta"
  ]
  node [
    id 980
    label "wydawnictwo"
  ]
  node [
    id 981
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 982
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 983
    label "wiano"
  ]
  node [
    id 984
    label "produkcja"
  ]
  node [
    id 985
    label "translate"
  ]
  node [
    id 986
    label "picture"
  ]
  node [
    id 987
    label "poda&#263;"
  ]
  node [
    id 988
    label "dress"
  ]
  node [
    id 989
    label "tajemnica"
  ]
  node [
    id 990
    label "panna_na_wydaniu"
  ]
  node [
    id 991
    label "supply"
  ]
  node [
    id 992
    label "ujawni&#263;"
  ]
  node [
    id 993
    label "delivery"
  ]
  node [
    id 994
    label "zdarzenie_si&#281;"
  ]
  node [
    id 995
    label "rendition"
  ]
  node [
    id 996
    label "impression"
  ]
  node [
    id 997
    label "zadenuncjowanie"
  ]
  node [
    id 998
    label "wytworzenie"
  ]
  node [
    id 999
    label "issue"
  ]
  node [
    id 1000
    label "danie"
  ]
  node [
    id 1001
    label "czasopismo"
  ]
  node [
    id 1002
    label "podanie"
  ]
  node [
    id 1003
    label "wprowadzenie"
  ]
  node [
    id 1004
    label "ujawnienie"
  ]
  node [
    id 1005
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 1006
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1007
    label "urz&#261;dzenie"
  ]
  node [
    id 1008
    label "zrobienie"
  ]
  node [
    id 1009
    label "robi&#263;"
  ]
  node [
    id 1010
    label "mie&#263;_miejsce"
  ]
  node [
    id 1011
    label "surrender"
  ]
  node [
    id 1012
    label "kojarzy&#263;"
  ]
  node [
    id 1013
    label "wprowadza&#263;"
  ]
  node [
    id 1014
    label "podawa&#263;"
  ]
  node [
    id 1015
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1016
    label "ujawnia&#263;"
  ]
  node [
    id 1017
    label "placard"
  ]
  node [
    id 1018
    label "powierza&#263;"
  ]
  node [
    id 1019
    label "denuncjowa&#263;"
  ]
  node [
    id 1020
    label "wytwarza&#263;"
  ]
  node [
    id 1021
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 1022
    label "strike"
  ]
  node [
    id 1023
    label "zaziera&#263;"
  ]
  node [
    id 1024
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1025
    label "czu&#263;"
  ]
  node [
    id 1026
    label "spotyka&#263;"
  ]
  node [
    id 1027
    label "drop"
  ]
  node [
    id 1028
    label "pogo"
  ]
  node [
    id 1029
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1030
    label "ogrom"
  ]
  node [
    id 1031
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1032
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 1033
    label "popada&#263;"
  ]
  node [
    id 1034
    label "odwiedza&#263;"
  ]
  node [
    id 1035
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1036
    label "przypomina&#263;"
  ]
  node [
    id 1037
    label "ujmowa&#263;"
  ]
  node [
    id 1038
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 1039
    label "fall"
  ]
  node [
    id 1040
    label "chowa&#263;"
  ]
  node [
    id 1041
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 1042
    label "demaskowa&#263;"
  ]
  node [
    id 1043
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 1044
    label "emocja"
  ]
  node [
    id 1045
    label "flatten"
  ]
  node [
    id 1046
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 1047
    label "fall_upon"
  ]
  node [
    id 1048
    label "ponie&#347;&#263;"
  ]
  node [
    id 1049
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 1050
    label "uderzy&#263;"
  ]
  node [
    id 1051
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1052
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 1053
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1054
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 1055
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1056
    label "spotka&#263;"
  ]
  node [
    id 1057
    label "odwiedzi&#263;"
  ]
  node [
    id 1058
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 1059
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 1060
    label "catalog"
  ]
  node [
    id 1061
    label "stock"
  ]
  node [
    id 1062
    label "pozycja"
  ]
  node [
    id 1063
    label "sumariusz"
  ]
  node [
    id 1064
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 1065
    label "spis"
  ]
  node [
    id 1066
    label "tekst"
  ]
  node [
    id 1067
    label "check"
  ]
  node [
    id 1068
    label "book"
  ]
  node [
    id 1069
    label "figurowa&#263;"
  ]
  node [
    id 1070
    label "rejestr"
  ]
  node [
    id 1071
    label "wyliczanka"
  ]
  node [
    id 1072
    label "instrumentacja_g&#322;oskowa"
  ]
  node [
    id 1073
    label "leksem"
  ]
  node [
    id 1074
    label "figura_s&#322;&#243;w"
  ]
  node [
    id 1075
    label "&#347;piew"
  ]
  node [
    id 1076
    label "wyra&#380;anie"
  ]
  node [
    id 1077
    label "tone"
  ]
  node [
    id 1078
    label "wydawanie"
  ]
  node [
    id 1079
    label "spirit"
  ]
  node [
    id 1080
    label "kolorystyka"
  ]
  node [
    id 1081
    label "przeci&#261;&#263;"
  ]
  node [
    id 1082
    label "udost&#281;pni&#263;"
  ]
  node [
    id 1083
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 1084
    label "establish"
  ]
  node [
    id 1085
    label "cia&#322;o"
  ]
  node [
    id 1086
    label "spowodowa&#263;"
  ]
  node [
    id 1087
    label "uruchomi&#263;"
  ]
  node [
    id 1088
    label "begin"
  ]
  node [
    id 1089
    label "zacz&#261;&#263;"
  ]
  node [
    id 1090
    label "traverse"
  ]
  node [
    id 1091
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1092
    label "zablokowa&#263;"
  ]
  node [
    id 1093
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1094
    label "uci&#261;&#263;"
  ]
  node [
    id 1095
    label "traversal"
  ]
  node [
    id 1096
    label "naruszy&#263;"
  ]
  node [
    id 1097
    label "przebi&#263;"
  ]
  node [
    id 1098
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 1099
    label "przedzieli&#263;"
  ]
  node [
    id 1100
    label "przerwa&#263;"
  ]
  node [
    id 1101
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1102
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1103
    label "odj&#261;&#263;"
  ]
  node [
    id 1104
    label "introduce"
  ]
  node [
    id 1105
    label "do"
  ]
  node [
    id 1106
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1107
    label "trip"
  ]
  node [
    id 1108
    label "wear"
  ]
  node [
    id 1109
    label "rozmie&#347;ci&#263;"
  ]
  node [
    id 1110
    label "peddle"
  ]
  node [
    id 1111
    label "os&#322;abi&#263;"
  ]
  node [
    id 1112
    label "zepsu&#263;"
  ]
  node [
    id 1113
    label "podzieli&#263;"
  ]
  node [
    id 1114
    label "range"
  ]
  node [
    id 1115
    label "oddali&#263;"
  ]
  node [
    id 1116
    label "stagger"
  ]
  node [
    id 1117
    label "raise"
  ]
  node [
    id 1118
    label "wygra&#263;"
  ]
  node [
    id 1119
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1120
    label "open"
  ]
  node [
    id 1121
    label "ekshumowanie"
  ]
  node [
    id 1122
    label "uk&#322;ad"
  ]
  node [
    id 1123
    label "jednostka_organizacyjna"
  ]
  node [
    id 1124
    label "p&#322;aszczyzna"
  ]
  node [
    id 1125
    label "odwadnia&#263;"
  ]
  node [
    id 1126
    label "zabalsamowanie"
  ]
  node [
    id 1127
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1128
    label "odwodni&#263;"
  ]
  node [
    id 1129
    label "sk&#243;ra"
  ]
  node [
    id 1130
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1131
    label "staw"
  ]
  node [
    id 1132
    label "ow&#322;osienie"
  ]
  node [
    id 1133
    label "mi&#281;so"
  ]
  node [
    id 1134
    label "zabalsamowa&#263;"
  ]
  node [
    id 1135
    label "Izba_Konsyliarska"
  ]
  node [
    id 1136
    label "unerwienie"
  ]
  node [
    id 1137
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1138
    label "kremacja"
  ]
  node [
    id 1139
    label "biorytm"
  ]
  node [
    id 1140
    label "sekcja"
  ]
  node [
    id 1141
    label "istota_&#380;ywa"
  ]
  node [
    id 1142
    label "otwiera&#263;"
  ]
  node [
    id 1143
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1144
    label "otworzenie"
  ]
  node [
    id 1145
    label "materia"
  ]
  node [
    id 1146
    label "pochowanie"
  ]
  node [
    id 1147
    label "otwieranie"
  ]
  node [
    id 1148
    label "szkielet"
  ]
  node [
    id 1149
    label "ty&#322;"
  ]
  node [
    id 1150
    label "tanatoplastyk"
  ]
  node [
    id 1151
    label "odwadnianie"
  ]
  node [
    id 1152
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1153
    label "odwodnienie"
  ]
  node [
    id 1154
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1155
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1156
    label "pochowa&#263;"
  ]
  node [
    id 1157
    label "tanatoplastyka"
  ]
  node [
    id 1158
    label "balsamowa&#263;"
  ]
  node [
    id 1159
    label "nieumar&#322;y"
  ]
  node [
    id 1160
    label "temperatura"
  ]
  node [
    id 1161
    label "balsamowanie"
  ]
  node [
    id 1162
    label "ekshumowa&#263;"
  ]
  node [
    id 1163
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1164
    label "prz&#243;d"
  ]
  node [
    id 1165
    label "cz&#322;onek"
  ]
  node [
    id 1166
    label "pogrzeb"
  ]
  node [
    id 1167
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 1168
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 1169
    label "oczy"
  ]
  node [
    id 1170
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 1171
    label "&#378;renica"
  ]
  node [
    id 1172
    label "uwaga"
  ]
  node [
    id 1173
    label "spojrzenie"
  ]
  node [
    id 1174
    label "&#347;lepko"
  ]
  node [
    id 1175
    label "net"
  ]
  node [
    id 1176
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 1177
    label "siniec"
  ]
  node [
    id 1178
    label "wzrok"
  ]
  node [
    id 1179
    label "powieka"
  ]
  node [
    id 1180
    label "spoj&#243;wka"
  ]
  node [
    id 1181
    label "organ"
  ]
  node [
    id 1182
    label "ga&#322;ka_oczna"
  ]
  node [
    id 1183
    label "kaprawienie"
  ]
  node [
    id 1184
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 1185
    label "coloboma"
  ]
  node [
    id 1186
    label "ros&#243;&#322;"
  ]
  node [
    id 1187
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 1188
    label "&#347;lepie"
  ]
  node [
    id 1189
    label "nerw_wzrokowy"
  ]
  node [
    id 1190
    label "kaprawie&#263;"
  ]
  node [
    id 1191
    label "tkanka"
  ]
  node [
    id 1192
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1193
    label "tw&#243;r"
  ]
  node [
    id 1194
    label "organogeneza"
  ]
  node [
    id 1195
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1196
    label "struktura_anatomiczna"
  ]
  node [
    id 1197
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1198
    label "dekortykacja"
  ]
  node [
    id 1199
    label "stomia"
  ]
  node [
    id 1200
    label "budowa"
  ]
  node [
    id 1201
    label "okolica"
  ]
  node [
    id 1202
    label "m&#281;tnienie"
  ]
  node [
    id 1203
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 1204
    label "widzenie"
  ]
  node [
    id 1205
    label "okulista"
  ]
  node [
    id 1206
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 1207
    label "zmys&#322;"
  ]
  node [
    id 1208
    label "expression"
  ]
  node [
    id 1209
    label "widzie&#263;"
  ]
  node [
    id 1210
    label "m&#281;tnie&#263;"
  ]
  node [
    id 1211
    label "kontakt"
  ]
  node [
    id 1212
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1213
    label "stan"
  ]
  node [
    id 1214
    label "nagana"
  ]
  node [
    id 1215
    label "upomnienie"
  ]
  node [
    id 1216
    label "dzienniczek"
  ]
  node [
    id 1217
    label "wzgl&#261;d"
  ]
  node [
    id 1218
    label "gossip"
  ]
  node [
    id 1219
    label "patrzenie"
  ]
  node [
    id 1220
    label "patrze&#263;"
  ]
  node [
    id 1221
    label "expectation"
  ]
  node [
    id 1222
    label "popatrzenie"
  ]
  node [
    id 1223
    label "pojmowanie"
  ]
  node [
    id 1224
    label "stare"
  ]
  node [
    id 1225
    label "zinterpretowanie"
  ]
  node [
    id 1226
    label "decentracja"
  ]
  node [
    id 1227
    label "object"
  ]
  node [
    id 1228
    label "temat"
  ]
  node [
    id 1229
    label "mienie"
  ]
  node [
    id 1230
    label "przyroda"
  ]
  node [
    id 1231
    label "istota"
  ]
  node [
    id 1232
    label "obiekt"
  ]
  node [
    id 1233
    label "cera"
  ]
  node [
    id 1234
    label "wielko&#347;&#263;"
  ]
  node [
    id 1235
    label "rys"
  ]
  node [
    id 1236
    label "przedstawiciel"
  ]
  node [
    id 1237
    label "profil"
  ]
  node [
    id 1238
    label "p&#322;e&#263;"
  ]
  node [
    id 1239
    label "zas&#322;ona"
  ]
  node [
    id 1240
    label "p&#243;&#322;profil"
  ]
  node [
    id 1241
    label "policzek"
  ]
  node [
    id 1242
    label "brew"
  ]
  node [
    id 1243
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1244
    label "uj&#281;cie"
  ]
  node [
    id 1245
    label "micha"
  ]
  node [
    id 1246
    label "reputacja"
  ]
  node [
    id 1247
    label "wyraz_twarzy"
  ]
  node [
    id 1248
    label "czo&#322;o"
  ]
  node [
    id 1249
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 1250
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 1251
    label "twarzyczka"
  ]
  node [
    id 1252
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 1253
    label "ucho"
  ]
  node [
    id 1254
    label "usta"
  ]
  node [
    id 1255
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 1256
    label "dzi&#243;b"
  ]
  node [
    id 1257
    label "nos"
  ]
  node [
    id 1258
    label "podbr&#243;dek"
  ]
  node [
    id 1259
    label "liczko"
  ]
  node [
    id 1260
    label "pysk"
  ]
  node [
    id 1261
    label "maskowato&#347;&#263;"
  ]
  node [
    id 1262
    label "eyeliner"
  ]
  node [
    id 1263
    label "ga&#322;y"
  ]
  node [
    id 1264
    label "zupa"
  ]
  node [
    id 1265
    label "consomme"
  ]
  node [
    id 1266
    label "sk&#243;rzak"
  ]
  node [
    id 1267
    label "tarczka"
  ]
  node [
    id 1268
    label "mruganie"
  ]
  node [
    id 1269
    label "mruga&#263;"
  ]
  node [
    id 1270
    label "entropion"
  ]
  node [
    id 1271
    label "ptoza"
  ]
  node [
    id 1272
    label "mrugni&#281;cie"
  ]
  node [
    id 1273
    label "mrugn&#261;&#263;"
  ]
  node [
    id 1274
    label "wrodzone_zw&#281;&#380;enie_szpary_powiekowej"
  ]
  node [
    id 1275
    label "grad&#243;wka"
  ]
  node [
    id 1276
    label "j&#281;czmie&#324;"
  ]
  node [
    id 1277
    label "rz&#281;sa"
  ]
  node [
    id 1278
    label "ektropion"
  ]
  node [
    id 1279
    label "&#347;luz&#243;wka"
  ]
  node [
    id 1280
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 1281
    label "st&#322;uczenie"
  ]
  node [
    id 1282
    label "effusion"
  ]
  node [
    id 1283
    label "oznaka"
  ]
  node [
    id 1284
    label "karpiowate"
  ]
  node [
    id 1285
    label "obw&#243;dka"
  ]
  node [
    id 1286
    label "ryba"
  ]
  node [
    id 1287
    label "przebarwienie"
  ]
  node [
    id 1288
    label "zm&#281;czenie"
  ]
  node [
    id 1289
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1290
    label "szczelina"
  ]
  node [
    id 1291
    label "wada_wrodzona"
  ]
  node [
    id 1292
    label "ropie&#263;"
  ]
  node [
    id 1293
    label "ropienie"
  ]
  node [
    id 1294
    label "provider"
  ]
  node [
    id 1295
    label "b&#322;&#261;d"
  ]
  node [
    id 1296
    label "hipertekst"
  ]
  node [
    id 1297
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1298
    label "mem"
  ]
  node [
    id 1299
    label "gra_sieciowa"
  ]
  node [
    id 1300
    label "grooming"
  ]
  node [
    id 1301
    label "media"
  ]
  node [
    id 1302
    label "biznes_elektroniczny"
  ]
  node [
    id 1303
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1304
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1305
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1306
    label "netbook"
  ]
  node [
    id 1307
    label "e-hazard"
  ]
  node [
    id 1308
    label "podcast"
  ]
  node [
    id 1309
    label "strona"
  ]
  node [
    id 1310
    label "kwaskowato"
  ]
  node [
    id 1311
    label "&#347;wie&#380;o"
  ]
  node [
    id 1312
    label "pomara&#324;czowy"
  ]
  node [
    id 1313
    label "owocowo"
  ]
  node [
    id 1314
    label "ciep&#322;o"
  ]
  node [
    id 1315
    label "geotermia"
  ]
  node [
    id 1316
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1317
    label "przyjemnie"
  ]
  node [
    id 1318
    label "pogoda"
  ]
  node [
    id 1319
    label "emisyjno&#347;&#263;"
  ]
  node [
    id 1320
    label "mi&#322;o"
  ]
  node [
    id 1321
    label "ciep&#322;y"
  ]
  node [
    id 1322
    label "heat"
  ]
  node [
    id 1323
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 1324
    label "o&#380;ywczo"
  ]
  node [
    id 1325
    label "m&#322;odo"
  ]
  node [
    id 1326
    label "jasno"
  ]
  node [
    id 1327
    label "odmiennie"
  ]
  node [
    id 1328
    label "dobrze"
  ]
  node [
    id 1329
    label "niestandardowo"
  ]
  node [
    id 1330
    label "czysto"
  ]
  node [
    id 1331
    label "&#347;wie&#380;y"
  ]
  node [
    id 1332
    label "soczy&#347;cie"
  ]
  node [
    id 1333
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 1334
    label "oryginalny"
  ]
  node [
    id 1335
    label "nowo"
  ]
  node [
    id 1336
    label "refreshingly"
  ]
  node [
    id 1337
    label "zdrowo"
  ]
  node [
    id 1338
    label "kwaskowy"
  ]
  node [
    id 1339
    label "rze&#347;ko"
  ]
  node [
    id 1340
    label "s&#322;odko"
  ]
  node [
    id 1341
    label "owocowy"
  ]
  node [
    id 1342
    label "cytrusowy"
  ]
  node [
    id 1343
    label "czarny"
  ]
  node [
    id 1344
    label "kawa"
  ]
  node [
    id 1345
    label "murzynek"
  ]
  node [
    id 1346
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 1347
    label "dripper"
  ]
  node [
    id 1348
    label "ziarno"
  ]
  node [
    id 1349
    label "u&#380;ywka"
  ]
  node [
    id 1350
    label "marzanowate"
  ]
  node [
    id 1351
    label "nap&#243;j"
  ]
  node [
    id 1352
    label "jedzenie"
  ]
  node [
    id 1353
    label "produkt"
  ]
  node [
    id 1354
    label "pestkowiec"
  ]
  node [
    id 1355
    label "porcja"
  ]
  node [
    id 1356
    label "kofeina"
  ]
  node [
    id 1357
    label "chemex"
  ]
  node [
    id 1358
    label "czarna_kawa"
  ]
  node [
    id 1359
    label "ma&#347;lak_pstry"
  ]
  node [
    id 1360
    label "ciasto"
  ]
  node [
    id 1361
    label "czarne"
  ]
  node [
    id 1362
    label "kolorowy"
  ]
  node [
    id 1363
    label "bierka_szachowa"
  ]
  node [
    id 1364
    label "gorzki"
  ]
  node [
    id 1365
    label "murzy&#324;ski"
  ]
  node [
    id 1366
    label "ksi&#261;dz"
  ]
  node [
    id 1367
    label "kompletny"
  ]
  node [
    id 1368
    label "przewrotny"
  ]
  node [
    id 1369
    label "ponury"
  ]
  node [
    id 1370
    label "beznadziejny"
  ]
  node [
    id 1371
    label "z&#322;y"
  ]
  node [
    id 1372
    label "wedel"
  ]
  node [
    id 1373
    label "czarnuch"
  ]
  node [
    id 1374
    label "granatowo"
  ]
  node [
    id 1375
    label "ciemny"
  ]
  node [
    id 1376
    label "negatywny"
  ]
  node [
    id 1377
    label "ciemnienie"
  ]
  node [
    id 1378
    label "czernienie"
  ]
  node [
    id 1379
    label "zaczernienie"
  ]
  node [
    id 1380
    label "pesymistycznie"
  ]
  node [
    id 1381
    label "abolicjonista"
  ]
  node [
    id 1382
    label "brudny"
  ]
  node [
    id 1383
    label "zaczernianie_si&#281;"
  ]
  node [
    id 1384
    label "kafar"
  ]
  node [
    id 1385
    label "czarnuchowaty"
  ]
  node [
    id 1386
    label "pessimistic"
  ]
  node [
    id 1387
    label "czarniawy"
  ]
  node [
    id 1388
    label "ciemnosk&#243;ry"
  ]
  node [
    id 1389
    label "okrutny"
  ]
  node [
    id 1390
    label "czarno"
  ]
  node [
    id 1391
    label "zaczernienie_si&#281;"
  ]
  node [
    id 1392
    label "niepomy&#347;lny"
  ]
  node [
    id 1393
    label "wk&#322;ad"
  ]
  node [
    id 1394
    label "oprawa"
  ]
  node [
    id 1395
    label "boarding"
  ]
  node [
    id 1396
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1397
    label "oprawianie"
  ]
  node [
    id 1398
    label "os&#322;ona"
  ]
  node [
    id 1399
    label "oprawia&#263;"
  ]
  node [
    id 1400
    label "zeszyt"
  ]
  node [
    id 1401
    label "binda"
  ]
  node [
    id 1402
    label "warunki"
  ]
  node [
    id 1403
    label "filet"
  ]
  node [
    id 1404
    label "obramowanie"
  ]
  node [
    id 1405
    label "ochrona"
  ]
  node [
    id 1406
    label "operacja"
  ]
  node [
    id 1407
    label "farmaceutyk"
  ]
  node [
    id 1408
    label "psychotest"
  ]
  node [
    id 1409
    label "pismo"
  ]
  node [
    id 1410
    label "communication"
  ]
  node [
    id 1411
    label "zajawka"
  ]
  node [
    id 1412
    label "Zwrotnica"
  ]
  node [
    id 1413
    label "dzia&#322;"
  ]
  node [
    id 1414
    label "prasa"
  ]
  node [
    id 1415
    label "rozdzia&#322;"
  ]
  node [
    id 1416
    label "zak&#322;adka"
  ]
  node [
    id 1417
    label "nomina&#322;"
  ]
  node [
    id 1418
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 1419
    label "ekslibris"
  ]
  node [
    id 1420
    label "przek&#322;adacz"
  ]
  node [
    id 1421
    label "bibliofilstwo"
  ]
  node [
    id 1422
    label "falc"
  ]
  node [
    id 1423
    label "pagina"
  ]
  node [
    id 1424
    label "zw&#243;j"
  ]
  node [
    id 1425
    label "bajt"
  ]
  node [
    id 1426
    label "bloking"
  ]
  node [
    id 1427
    label "j&#261;kanie"
  ]
  node [
    id 1428
    label "przeszkoda"
  ]
  node [
    id 1429
    label "blokada"
  ]
  node [
    id 1430
    label "bry&#322;a"
  ]
  node [
    id 1431
    label "kontynent"
  ]
  node [
    id 1432
    label "nastawnia"
  ]
  node [
    id 1433
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1434
    label "blockage"
  ]
  node [
    id 1435
    label "block"
  ]
  node [
    id 1436
    label "budynek"
  ]
  node [
    id 1437
    label "start"
  ]
  node [
    id 1438
    label "skorupa_ziemska"
  ]
  node [
    id 1439
    label "program"
  ]
  node [
    id 1440
    label "blokowisko"
  ]
  node [
    id 1441
    label "artyku&#322;"
  ]
  node [
    id 1442
    label "barak"
  ]
  node [
    id 1443
    label "stok_kontynentalny"
  ]
  node [
    id 1444
    label "square"
  ]
  node [
    id 1445
    label "siatk&#243;wka"
  ]
  node [
    id 1446
    label "kr&#261;g"
  ]
  node [
    id 1447
    label "ram&#243;wka"
  ]
  node [
    id 1448
    label "zamek"
  ]
  node [
    id 1449
    label "obrona"
  ]
  node [
    id 1450
    label "bie&#380;nia"
  ]
  node [
    id 1451
    label "referat"
  ]
  node [
    id 1452
    label "dom_wielorodzinny"
  ]
  node [
    id 1453
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 1454
    label "kartka"
  ]
  node [
    id 1455
    label "kajet"
  ]
  node [
    id 1456
    label "bind"
  ]
  node [
    id 1457
    label "umieszcza&#263;"
  ]
  node [
    id 1458
    label "obraz"
  ]
  node [
    id 1459
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 1460
    label "przygotowywa&#263;"
  ]
  node [
    id 1461
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 1462
    label "obsadza&#263;"
  ]
  node [
    id 1463
    label "umieszczanie"
  ]
  node [
    id 1464
    label "uatrakcyjnianie"
  ]
  node [
    id 1465
    label "obsadzanie"
  ]
  node [
    id 1466
    label "dressing"
  ]
  node [
    id 1467
    label "wyposa&#380;anie"
  ]
  node [
    id 1468
    label "binding"
  ]
  node [
    id 1469
    label "przygotowywanie"
  ]
  node [
    id 1470
    label "kwota"
  ]
  node [
    id 1471
    label "uczestnictwo"
  ]
  node [
    id 1472
    label "element"
  ]
  node [
    id 1473
    label "input"
  ]
  node [
    id 1474
    label "lokata"
  ]
  node [
    id 1475
    label "teatr"
  ]
  node [
    id 1476
    label "exhibit"
  ]
  node [
    id 1477
    label "display"
  ]
  node [
    id 1478
    label "pokazywa&#263;"
  ]
  node [
    id 1479
    label "demonstrowa&#263;"
  ]
  node [
    id 1480
    label "przedstawienie"
  ]
  node [
    id 1481
    label "zapoznawa&#263;"
  ]
  node [
    id 1482
    label "opisywa&#263;"
  ]
  node [
    id 1483
    label "ukazywa&#263;"
  ]
  node [
    id 1484
    label "represent"
  ]
  node [
    id 1485
    label "zg&#322;asza&#263;"
  ]
  node [
    id 1486
    label "typify"
  ]
  node [
    id 1487
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1488
    label "attest"
  ]
  node [
    id 1489
    label "stanowi&#263;"
  ]
  node [
    id 1490
    label "warto&#347;&#263;"
  ]
  node [
    id 1491
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1492
    label "by&#263;"
  ]
  node [
    id 1493
    label "wyraz"
  ]
  node [
    id 1494
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1495
    label "przeszkala&#263;"
  ]
  node [
    id 1496
    label "powodowa&#263;"
  ]
  node [
    id 1497
    label "exsert"
  ]
  node [
    id 1498
    label "bespeak"
  ]
  node [
    id 1499
    label "informowa&#263;"
  ]
  node [
    id 1500
    label "indicate"
  ]
  node [
    id 1501
    label "report"
  ]
  node [
    id 1502
    label "write"
  ]
  node [
    id 1503
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1504
    label "odst&#281;powa&#263;"
  ]
  node [
    id 1505
    label "perform"
  ]
  node [
    id 1506
    label "wychodzi&#263;"
  ]
  node [
    id 1507
    label "seclude"
  ]
  node [
    id 1508
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1509
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 1510
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1511
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 1512
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1513
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1514
    label "appear"
  ]
  node [
    id 1515
    label "unwrap"
  ]
  node [
    id 1516
    label "rezygnowa&#263;"
  ]
  node [
    id 1517
    label "overture"
  ]
  node [
    id 1518
    label "uczestniczy&#263;"
  ]
  node [
    id 1519
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1520
    label "decide"
  ]
  node [
    id 1521
    label "pies_my&#347;liwski"
  ]
  node [
    id 1522
    label "decydowa&#263;"
  ]
  node [
    id 1523
    label "zatrzymywa&#263;"
  ]
  node [
    id 1524
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1525
    label "tenis"
  ]
  node [
    id 1526
    label "deal"
  ]
  node [
    id 1527
    label "rozgrywa&#263;"
  ]
  node [
    id 1528
    label "kelner"
  ]
  node [
    id 1529
    label "cover"
  ]
  node [
    id 1530
    label "tender"
  ]
  node [
    id 1531
    label "faszerowa&#263;"
  ]
  node [
    id 1532
    label "serwowa&#263;"
  ]
  node [
    id 1533
    label "zawiera&#263;"
  ]
  node [
    id 1534
    label "poznawa&#263;"
  ]
  node [
    id 1535
    label "obznajamia&#263;"
  ]
  node [
    id 1536
    label "go_steady"
  ]
  node [
    id 1537
    label "pr&#243;bowanie"
  ]
  node [
    id 1538
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1539
    label "zademonstrowanie"
  ]
  node [
    id 1540
    label "obgadanie"
  ]
  node [
    id 1541
    label "realizacja"
  ]
  node [
    id 1542
    label "scena"
  ]
  node [
    id 1543
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1544
    label "narration"
  ]
  node [
    id 1545
    label "cyrk"
  ]
  node [
    id 1546
    label "theatrical_performance"
  ]
  node [
    id 1547
    label "opisanie"
  ]
  node [
    id 1548
    label "malarstwo"
  ]
  node [
    id 1549
    label "scenografia"
  ]
  node [
    id 1550
    label "ukazanie"
  ]
  node [
    id 1551
    label "zapoznanie"
  ]
  node [
    id 1552
    label "pokaz"
  ]
  node [
    id 1553
    label "spos&#243;b"
  ]
  node [
    id 1554
    label "ods&#322;ona"
  ]
  node [
    id 1555
    label "pokazanie"
  ]
  node [
    id 1556
    label "wyst&#261;pienie"
  ]
  node [
    id 1557
    label "przedstawi&#263;"
  ]
  node [
    id 1558
    label "przedstawianie"
  ]
  node [
    id 1559
    label "rola"
  ]
  node [
    id 1560
    label "teren"
  ]
  node [
    id 1561
    label "play"
  ]
  node [
    id 1562
    label "antyteatr"
  ]
  node [
    id 1563
    label "instytucja"
  ]
  node [
    id 1564
    label "deski"
  ]
  node [
    id 1565
    label "sala"
  ]
  node [
    id 1566
    label "literatura"
  ]
  node [
    id 1567
    label "dekoratornia"
  ]
  node [
    id 1568
    label "modelatornia"
  ]
  node [
    id 1569
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 1570
    label "widzownia"
  ]
  node [
    id 1571
    label "wykrwawia&#263;_si&#281;"
  ]
  node [
    id 1572
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1573
    label "wykrwawi&#263;"
  ]
  node [
    id 1574
    label "hematokryt"
  ]
  node [
    id 1575
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1576
    label "wykrwawienie_si&#281;"
  ]
  node [
    id 1577
    label "farba"
  ]
  node [
    id 1578
    label "wykrwawianie_si&#281;"
  ]
  node [
    id 1579
    label "wykrwawianie"
  ]
  node [
    id 1580
    label "pokrewie&#324;stwo"
  ]
  node [
    id 1581
    label "krwinka"
  ]
  node [
    id 1582
    label "lifeblood"
  ]
  node [
    id 1583
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1584
    label "osocze_krwi"
  ]
  node [
    id 1585
    label "tkanka_&#322;&#261;czna"
  ]
  node [
    id 1586
    label "wykrwawia&#263;"
  ]
  node [
    id 1587
    label "wykrwawienie"
  ]
  node [
    id 1588
    label "dializowa&#263;"
  ]
  node [
    id 1589
    label "&#347;mier&#263;"
  ]
  node [
    id 1590
    label "wykrwawi&#263;_si&#281;"
  ]
  node [
    id 1591
    label "dializowanie"
  ]
  node [
    id 1592
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 1593
    label "krwioplucie"
  ]
  node [
    id 1594
    label "marker_nowotworowy"
  ]
  node [
    id 1595
    label "charakterystyka"
  ]
  node [
    id 1596
    label "m&#322;ot"
  ]
  node [
    id 1597
    label "pr&#243;ba"
  ]
  node [
    id 1598
    label "attribute"
  ]
  node [
    id 1599
    label "marka"
  ]
  node [
    id 1600
    label "koligacja"
  ]
  node [
    id 1601
    label "alliance"
  ]
  node [
    id 1602
    label "podobie&#324;stwo"
  ]
  node [
    id 1603
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1604
    label "wydarzenie"
  ]
  node [
    id 1605
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1606
    label "psychika"
  ]
  node [
    id 1607
    label "kompleksja"
  ]
  node [
    id 1608
    label "fizjonomia"
  ]
  node [
    id 1609
    label "entity"
  ]
  node [
    id 1610
    label "defenestracja"
  ]
  node [
    id 1611
    label "agonia"
  ]
  node [
    id 1612
    label "kres"
  ]
  node [
    id 1613
    label "mogi&#322;a"
  ]
  node [
    id 1614
    label "kres_&#380;ycia"
  ]
  node [
    id 1615
    label "upadek"
  ]
  node [
    id 1616
    label "szeol"
  ]
  node [
    id 1617
    label "pogrzebanie"
  ]
  node [
    id 1618
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1619
    label "&#380;a&#322;oba"
  ]
  node [
    id 1620
    label "zabicie"
  ]
  node [
    id 1621
    label "blood_cell"
  ]
  node [
    id 1622
    label "kom&#243;rka_zwierz&#281;ca"
  ]
  node [
    id 1623
    label "pr&#243;szy&#263;"
  ]
  node [
    id 1624
    label "kry&#263;"
  ]
  node [
    id 1625
    label "pr&#243;szenie"
  ]
  node [
    id 1626
    label "podk&#322;ad"
  ]
  node [
    id 1627
    label "blik"
  ]
  node [
    id 1628
    label "krycie"
  ]
  node [
    id 1629
    label "wypunktowa&#263;"
  ]
  node [
    id 1630
    label "substancja"
  ]
  node [
    id 1631
    label "punktowa&#263;"
  ]
  node [
    id 1632
    label "odkrztuszanie"
  ]
  node [
    id 1633
    label "zesp&#243;&#322;_chorobowy"
  ]
  node [
    id 1634
    label "odpluwanie"
  ]
  node [
    id 1635
    label "hemoptysis"
  ]
  node [
    id 1636
    label "flegma"
  ]
  node [
    id 1637
    label "leczy&#263;"
  ]
  node [
    id 1638
    label "spu&#347;ci&#263;"
  ]
  node [
    id 1639
    label "spuszczenie"
  ]
  node [
    id 1640
    label "spuszczanie"
  ]
  node [
    id 1641
    label "proporcja"
  ]
  node [
    id 1642
    label "hematocrit"
  ]
  node [
    id 1643
    label "leczenie"
  ]
  node [
    id 1644
    label "rotation"
  ]
  node [
    id 1645
    label "obieg"
  ]
  node [
    id 1646
    label "kontrolowanie"
  ]
  node [
    id 1647
    label "patrol"
  ]
  node [
    id 1648
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 1649
    label "lap"
  ]
  node [
    id 1650
    label "spuszcza&#263;"
  ]
  node [
    id 1651
    label "kontrolowa&#263;"
  ]
  node [
    id 1652
    label "carry"
  ]
  node [
    id 1653
    label "wheel"
  ]
  node [
    id 1654
    label "matter"
  ]
  node [
    id 1655
    label "surowiec_energetyczny"
  ]
  node [
    id 1656
    label "bitum"
  ]
  node [
    id 1657
    label "oktan"
  ]
  node [
    id 1658
    label "wydzielina"
  ]
  node [
    id 1659
    label "kopalina_podstawowa"
  ]
  node [
    id 1660
    label "Orlen"
  ]
  node [
    id 1661
    label "petrodolar"
  ]
  node [
    id 1662
    label "mineraloid"
  ]
  node [
    id 1663
    label "secretion"
  ]
  node [
    id 1664
    label "mieszanina"
  ]
  node [
    id 1665
    label "bitumen"
  ]
  node [
    id 1666
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 1667
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1668
    label "ciek&#322;y"
  ]
  node [
    id 1669
    label "chlupa&#263;"
  ]
  node [
    id 1670
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 1671
    label "wytoczenie"
  ]
  node [
    id 1672
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 1673
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 1674
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 1675
    label "stan_skupienia"
  ]
  node [
    id 1676
    label "nieprzejrzysty"
  ]
  node [
    id 1677
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1678
    label "podbiega&#263;"
  ]
  node [
    id 1679
    label "baniak"
  ]
  node [
    id 1680
    label "zachlupa&#263;"
  ]
  node [
    id 1681
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 1682
    label "odp&#322;ywanie"
  ]
  node [
    id 1683
    label "podbiec"
  ]
  node [
    id 1684
    label "byt"
  ]
  node [
    id 1685
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1686
    label "informacja"
  ]
  node [
    id 1687
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 1688
    label "ropa_naftowa"
  ]
  node [
    id 1689
    label "octane"
  ]
  node [
    id 1690
    label "w&#281;giel"
  ]
  node [
    id 1691
    label "alkan"
  ]
  node [
    id 1692
    label "paliwo"
  ]
  node [
    id 1693
    label "dolar"
  ]
  node [
    id 1694
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1695
    label "komunikowa&#263;"
  ]
  node [
    id 1696
    label "bomber"
  ]
  node [
    id 1697
    label "zdecydowa&#263;"
  ]
  node [
    id 1698
    label "come_up"
  ]
  node [
    id 1699
    label "straci&#263;"
  ]
  node [
    id 1700
    label "zyska&#263;"
  ]
  node [
    id 1701
    label "communicate"
  ]
  node [
    id 1702
    label "nieatrakcyjny"
  ]
  node [
    id 1703
    label "blado"
  ]
  node [
    id 1704
    label "mizerny"
  ]
  node [
    id 1705
    label "niezabawny"
  ]
  node [
    id 1706
    label "nienasycony"
  ]
  node [
    id 1707
    label "s&#322;aby"
  ]
  node [
    id 1708
    label "niewa&#380;ny"
  ]
  node [
    id 1709
    label "oboj&#281;tny"
  ]
  node [
    id 1710
    label "poszarzenie"
  ]
  node [
    id 1711
    label "ch&#322;odny"
  ]
  node [
    id 1712
    label "szarzenie"
  ]
  node [
    id 1713
    label "bezbarwnie"
  ]
  node [
    id 1714
    label "nieciekawy"
  ]
  node [
    id 1715
    label "jasny"
  ]
  node [
    id 1716
    label "nieatrakcyjnie"
  ]
  node [
    id 1717
    label "nietrwa&#322;y"
  ]
  node [
    id 1718
    label "marnie"
  ]
  node [
    id 1719
    label "delikatny"
  ]
  node [
    id 1720
    label "po&#347;ledni"
  ]
  node [
    id 1721
    label "niezdrowy"
  ]
  node [
    id 1722
    label "nieumiej&#281;tny"
  ]
  node [
    id 1723
    label "s&#322;abo"
  ]
  node [
    id 1724
    label "nieznaczny"
  ]
  node [
    id 1725
    label "lura"
  ]
  node [
    id 1726
    label "nieudany"
  ]
  node [
    id 1727
    label "s&#322;abowity"
  ]
  node [
    id 1728
    label "zawodny"
  ]
  node [
    id 1729
    label "&#322;agodny"
  ]
  node [
    id 1730
    label "md&#322;y"
  ]
  node [
    id 1731
    label "niedoskona&#322;y"
  ]
  node [
    id 1732
    label "przemijaj&#261;cy"
  ]
  node [
    id 1733
    label "niemocny"
  ]
  node [
    id 1734
    label "niefajny"
  ]
  node [
    id 1735
    label "kiepsko"
  ]
  node [
    id 1736
    label "uniewa&#380;nianie_si&#281;"
  ]
  node [
    id 1737
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 1738
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 1739
    label "nieistotnie"
  ]
  node [
    id 1740
    label "zi&#281;bienie"
  ]
  node [
    id 1741
    label "niesympatyczny"
  ]
  node [
    id 1742
    label "och&#322;odzenie"
  ]
  node [
    id 1743
    label "opanowany"
  ]
  node [
    id 1744
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 1745
    label "rozs&#261;dny"
  ]
  node [
    id 1746
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 1747
    label "sch&#322;adzanie"
  ]
  node [
    id 1748
    label "ch&#322;odno"
  ]
  node [
    id 1749
    label "nieumiarkowany"
  ]
  node [
    id 1750
    label "silny"
  ]
  node [
    id 1751
    label "nienasycenie"
  ]
  node [
    id 1752
    label "nieintensywny"
  ]
  node [
    id 1753
    label "o&#347;wietlenie"
  ]
  node [
    id 1754
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 1755
    label "o&#347;wietlanie"
  ]
  node [
    id 1756
    label "przytomny"
  ]
  node [
    id 1757
    label "niezm&#261;cony"
  ]
  node [
    id 1758
    label "bia&#322;y"
  ]
  node [
    id 1759
    label "klarowny"
  ]
  node [
    id 1760
    label "jednoznaczny"
  ]
  node [
    id 1761
    label "pogodny"
  ]
  node [
    id 1762
    label "nieciekawie"
  ]
  node [
    id 1763
    label "oswojony"
  ]
  node [
    id 1764
    label "na&#322;o&#380;ny"
  ]
  node [
    id 1765
    label "zoboj&#281;tnienie"
  ]
  node [
    id 1766
    label "nieszkodliwy"
  ]
  node [
    id 1767
    label "&#347;ni&#281;ty"
  ]
  node [
    id 1768
    label "oboj&#281;tnie"
  ]
  node [
    id 1769
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 1770
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 1771
    label "neutralizowanie"
  ]
  node [
    id 1772
    label "bierny"
  ]
  node [
    id 1773
    label "zneutralizowanie"
  ]
  node [
    id 1774
    label "palely"
  ]
  node [
    id 1775
    label "blandly"
  ]
  node [
    id 1776
    label "nieefektownie"
  ]
  node [
    id 1777
    label "nijaki"
  ]
  node [
    id 1778
    label "biedny"
  ]
  node [
    id 1779
    label "sm&#281;tny"
  ]
  node [
    id 1780
    label "marny"
  ]
  node [
    id 1781
    label "mizernie"
  ]
  node [
    id 1782
    label "n&#281;dznie"
  ]
  node [
    id 1783
    label "niedobrze"
  ]
  node [
    id 1784
    label "bezbarwny"
  ]
  node [
    id 1785
    label "rozwidnianie_si&#281;"
  ]
  node [
    id 1786
    label "odcinanie_si&#281;"
  ]
  node [
    id 1787
    label "szary"
  ]
  node [
    id 1788
    label "zmierzchanie_si&#281;"
  ]
  node [
    id 1789
    label "barwienie_si&#281;"
  ]
  node [
    id 1790
    label "zabarwienie_si&#281;"
  ]
  node [
    id 1791
    label "rozwidnienie_si&#281;"
  ]
  node [
    id 1792
    label "zmierzchni&#281;cie_si&#281;"
  ]
  node [
    id 1793
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 1794
    label "przyk&#322;ad"
  ]
  node [
    id 1795
    label "substytuowa&#263;"
  ]
  node [
    id 1796
    label "substytuowanie"
  ]
  node [
    id 1797
    label "zast&#281;pca"
  ]
  node [
    id 1798
    label "zaistnie&#263;"
  ]
  node [
    id 1799
    label "Osjan"
  ]
  node [
    id 1800
    label "kto&#347;"
  ]
  node [
    id 1801
    label "wygl&#261;d"
  ]
  node [
    id 1802
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1803
    label "trim"
  ]
  node [
    id 1804
    label "poby&#263;"
  ]
  node [
    id 1805
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1806
    label "Aspazja"
  ]
  node [
    id 1807
    label "punkt_widzenia"
  ]
  node [
    id 1808
    label "wytrzyma&#263;"
  ]
  node [
    id 1809
    label "formacja"
  ]
  node [
    id 1810
    label "pozosta&#263;"
  ]
  node [
    id 1811
    label "point"
  ]
  node [
    id 1812
    label "go&#347;&#263;"
  ]
  node [
    id 1813
    label "ptak"
  ]
  node [
    id 1814
    label "grzebie&#324;"
  ]
  node [
    id 1815
    label "bow"
  ]
  node [
    id 1816
    label "statek"
  ]
  node [
    id 1817
    label "ustnik"
  ]
  node [
    id 1818
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 1819
    label "samolot"
  ]
  node [
    id 1820
    label "zako&#324;czenie"
  ]
  node [
    id 1821
    label "ostry"
  ]
  node [
    id 1822
    label "blizna"
  ]
  node [
    id 1823
    label "dziob&#243;wka"
  ]
  node [
    id 1824
    label "kierunek"
  ]
  node [
    id 1825
    label "przestrze&#324;"
  ]
  node [
    id 1826
    label "znaczenie"
  ]
  node [
    id 1827
    label "opinia"
  ]
  node [
    id 1828
    label "pochwytanie"
  ]
  node [
    id 1829
    label "wording"
  ]
  node [
    id 1830
    label "wzbudzenie"
  ]
  node [
    id 1831
    label "withdrawal"
  ]
  node [
    id 1832
    label "capture"
  ]
  node [
    id 1833
    label "podniesienie"
  ]
  node [
    id 1834
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1835
    label "film"
  ]
  node [
    id 1836
    label "zapisanie"
  ]
  node [
    id 1837
    label "prezentacja"
  ]
  node [
    id 1838
    label "rzucenie"
  ]
  node [
    id 1839
    label "zamkni&#281;cie"
  ]
  node [
    id 1840
    label "zabranie"
  ]
  node [
    id 1841
    label "poinformowanie"
  ]
  node [
    id 1842
    label "zaaresztowanie"
  ]
  node [
    id 1843
    label "wzi&#281;cie"
  ]
  node [
    id 1844
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 1845
    label "listwa"
  ]
  node [
    id 1846
    label "profile"
  ]
  node [
    id 1847
    label "konto"
  ]
  node [
    id 1848
    label "przekr&#243;j"
  ]
  node [
    id 1849
    label "podgl&#261;d"
  ]
  node [
    id 1850
    label "sylwetka"
  ]
  node [
    id 1851
    label "dominanta"
  ]
  node [
    id 1852
    label "section"
  ]
  node [
    id 1853
    label "rzeczywisto&#347;&#263;_wirtualna"
  ]
  node [
    id 1854
    label "kontur"
  ]
  node [
    id 1855
    label "faseta"
  ]
  node [
    id 1856
    label "awatar"
  ]
  node [
    id 1857
    label "element_konstrukcyjny"
  ]
  node [
    id 1858
    label "ozdoba"
  ]
  node [
    id 1859
    label "sex"
  ]
  node [
    id 1860
    label "transseksualizm"
  ]
  node [
    id 1861
    label "przegroda"
  ]
  node [
    id 1862
    label "przy&#322;bica"
  ]
  node [
    id 1863
    label "obronienie"
  ]
  node [
    id 1864
    label "dekoracja_okna"
  ]
  node [
    id 1865
    label "warunek_lokalowy"
  ]
  node [
    id 1866
    label "rozmiar"
  ]
  node [
    id 1867
    label "liczba"
  ]
  node [
    id 1868
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1869
    label "zaleta"
  ]
  node [
    id 1870
    label "ilo&#347;&#263;"
  ]
  node [
    id 1871
    label "measure"
  ]
  node [
    id 1872
    label "dymensja"
  ]
  node [
    id 1873
    label "poj&#281;cie"
  ]
  node [
    id 1874
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1875
    label "potencja"
  ]
  node [
    id 1876
    label "property"
  ]
  node [
    id 1877
    label "skro&#324;"
  ]
  node [
    id 1878
    label "do&#322;eczek"
  ]
  node [
    id 1879
    label "ubliga"
  ]
  node [
    id 1880
    label "niedorobek"
  ]
  node [
    id 1881
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 1882
    label "lico"
  ]
  node [
    id 1883
    label "element_anatomiczny"
  ]
  node [
    id 1884
    label "wyzwisko"
  ]
  node [
    id 1885
    label "wrzuta"
  ]
  node [
    id 1886
    label "krzywda"
  ]
  node [
    id 1887
    label "indignation"
  ]
  node [
    id 1888
    label "oparcie"
  ]
  node [
    id 1889
    label "zbroja_p&#322;ytowa"
  ]
  node [
    id 1890
    label "skrzypce"
  ]
  node [
    id 1891
    label "he&#322;m"
  ]
  node [
    id 1892
    label "tkanina"
  ]
  node [
    id 1893
    label "przet&#322;uszcza&#263;_si&#281;"
  ]
  node [
    id 1894
    label "przet&#322;uszczanie_si&#281;"
  ]
  node [
    id 1895
    label "podstawy"
  ]
  node [
    id 1896
    label "opracowanie"
  ]
  node [
    id 1897
    label "napinacz"
  ]
  node [
    id 1898
    label "czapka"
  ]
  node [
    id 1899
    label "ucho_zewn&#281;trzne"
  ]
  node [
    id 1900
    label "elektronystagmografia"
  ]
  node [
    id 1901
    label "handle"
  ]
  node [
    id 1902
    label "ochraniacz"
  ]
  node [
    id 1903
    label "ma&#322;&#380;owina"
  ]
  node [
    id 1904
    label "ucho_wewn&#281;trzne"
  ]
  node [
    id 1905
    label "uchwyt"
  ]
  node [
    id 1906
    label "ucho_&#347;rodkowe"
  ]
  node [
    id 1907
    label "przew&#243;d_s&#322;uchowy"
  ]
  node [
    id 1908
    label "otw&#243;r"
  ]
  node [
    id 1909
    label "otw&#243;r_nosowy"
  ]
  node [
    id 1910
    label "ma&#322;&#380;owina_nosowa"
  ]
  node [
    id 1911
    label "si&#261;kanie"
  ]
  node [
    id 1912
    label "si&#261;kn&#261;&#263;"
  ]
  node [
    id 1913
    label "eskimoski"
  ]
  node [
    id 1914
    label "nozdrze"
  ]
  node [
    id 1915
    label "przegroda_nosowa"
  ]
  node [
    id 1916
    label "si&#261;ka&#263;"
  ]
  node [
    id 1917
    label "arhinia"
  ]
  node [
    id 1918
    label "rezonator"
  ]
  node [
    id 1919
    label "eskimosek"
  ]
  node [
    id 1920
    label "ozena"
  ]
  node [
    id 1921
    label "si&#261;kni&#281;cie"
  ]
  node [
    id 1922
    label "katar"
  ]
  node [
    id 1923
    label "jama_nosowa"
  ]
  node [
    id 1924
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 1925
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 1926
    label "zacinanie"
  ]
  node [
    id 1927
    label "ssa&#263;"
  ]
  node [
    id 1928
    label "zacina&#263;"
  ]
  node [
    id 1929
    label "zaci&#261;&#263;"
  ]
  node [
    id 1930
    label "ssanie"
  ]
  node [
    id 1931
    label "jama_ustna"
  ]
  node [
    id 1932
    label "jadaczka"
  ]
  node [
    id 1933
    label "zaci&#281;cie"
  ]
  node [
    id 1934
    label "warga_dolna"
  ]
  node [
    id 1935
    label "warga_g&#243;rna"
  ]
  node [
    id 1936
    label "ryjek"
  ]
  node [
    id 1937
    label "&#322;eb"
  ]
  node [
    id 1938
    label "morda"
  ]
  node [
    id 1939
    label "g&#243;wniarz"
  ]
  node [
    id 1940
    label "synek"
  ]
  node [
    id 1941
    label "boyfriend"
  ]
  node [
    id 1942
    label "okrzos"
  ]
  node [
    id 1943
    label "dziecko"
  ]
  node [
    id 1944
    label "sympatia"
  ]
  node [
    id 1945
    label "usynowienie"
  ]
  node [
    id 1946
    label "kawaler"
  ]
  node [
    id 1947
    label "pederasta"
  ]
  node [
    id 1948
    label "m&#322;odzieniec"
  ]
  node [
    id 1949
    label "kajtek"
  ]
  node [
    id 1950
    label "&#347;l&#261;ski"
  ]
  node [
    id 1951
    label "usynawianie"
  ]
  node [
    id 1952
    label "utulenie"
  ]
  node [
    id 1953
    label "pediatra"
  ]
  node [
    id 1954
    label "dzieciak"
  ]
  node [
    id 1955
    label "utulanie"
  ]
  node [
    id 1956
    label "dzieciarnia"
  ]
  node [
    id 1957
    label "niepe&#322;noletni"
  ]
  node [
    id 1958
    label "organizm"
  ]
  node [
    id 1959
    label "utula&#263;"
  ]
  node [
    id 1960
    label "cz&#322;owieczek"
  ]
  node [
    id 1961
    label "fledgling"
  ]
  node [
    id 1962
    label "utuli&#263;"
  ]
  node [
    id 1963
    label "m&#322;odzik"
  ]
  node [
    id 1964
    label "pedofil"
  ]
  node [
    id 1965
    label "m&#322;odziak"
  ]
  node [
    id 1966
    label "potomek"
  ]
  node [
    id 1967
    label "entliczek-pentliczek"
  ]
  node [
    id 1968
    label "potomstwo"
  ]
  node [
    id 1969
    label "sraluch"
  ]
  node [
    id 1970
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1971
    label "partner"
  ]
  node [
    id 1972
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1973
    label "love"
  ]
  node [
    id 1974
    label "kredens"
  ]
  node [
    id 1975
    label "zawodnik"
  ]
  node [
    id 1976
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 1977
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 1978
    label "gracz"
  ]
  node [
    id 1979
    label "r&#281;ka"
  ]
  node [
    id 1980
    label "pomoc"
  ]
  node [
    id 1981
    label "wrzosowate"
  ]
  node [
    id 1982
    label "pomagacz"
  ]
  node [
    id 1983
    label "junior"
  ]
  node [
    id 1984
    label "junak"
  ]
  node [
    id 1985
    label "m&#322;odzie&#380;"
  ]
  node [
    id 1986
    label "mo&#322;ojec"
  ]
  node [
    id 1987
    label "m&#322;okos"
  ]
  node [
    id 1988
    label "smarkateria"
  ]
  node [
    id 1989
    label "ch&#322;opiec"
  ]
  node [
    id 1990
    label "kawa&#322;ek"
  ]
  node [
    id 1991
    label "gej"
  ]
  node [
    id 1992
    label "cug"
  ]
  node [
    id 1993
    label "krepel"
  ]
  node [
    id 1994
    label "francuz"
  ]
  node [
    id 1995
    label "mietlorz"
  ]
  node [
    id 1996
    label "etnolekt"
  ]
  node [
    id 1997
    label "sza&#322;ot"
  ]
  node [
    id 1998
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 1999
    label "regionalny"
  ]
  node [
    id 2000
    label "polski"
  ]
  node [
    id 2001
    label "halba"
  ]
  node [
    id 2002
    label "buchta"
  ]
  node [
    id 2003
    label "czarne_kluski"
  ]
  node [
    id 2004
    label "szpajza"
  ]
  node [
    id 2005
    label "szl&#261;ski"
  ]
  node [
    id 2006
    label "&#347;lonski"
  ]
  node [
    id 2007
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 2008
    label "waloszek"
  ]
  node [
    id 2009
    label "order"
  ]
  node [
    id 2010
    label "zalotnik"
  ]
  node [
    id 2011
    label "kawalerka"
  ]
  node [
    id 2012
    label "rycerz"
  ]
  node [
    id 2013
    label "odznaczenie"
  ]
  node [
    id 2014
    label "nie&#380;onaty"
  ]
  node [
    id 2015
    label "zakon_rycerski"
  ]
  node [
    id 2016
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 2017
    label "przysposobienie"
  ]
  node [
    id 2018
    label "syn"
  ]
  node [
    id 2019
    label "adoption"
  ]
  node [
    id 2020
    label "przysposabianie"
  ]
  node [
    id 2021
    label "sznurowanie"
  ]
  node [
    id 2022
    label "odrobina"
  ]
  node [
    id 2023
    label "skutek"
  ]
  node [
    id 2024
    label "sznurowa&#263;"
  ]
  node [
    id 2025
    label "odcisk"
  ]
  node [
    id 2026
    label "wp&#322;yw"
  ]
  node [
    id 2027
    label "dash"
  ]
  node [
    id 2028
    label "grain"
  ]
  node [
    id 2029
    label "intensywno&#347;&#263;"
  ]
  node [
    id 2030
    label "trace"
  ]
  node [
    id 2031
    label "&#347;wiadectwo"
  ]
  node [
    id 2032
    label "zgrubienie"
  ]
  node [
    id 2033
    label "odbicie"
  ]
  node [
    id 2034
    label "rozrost"
  ]
  node [
    id 2035
    label "lobbysta"
  ]
  node [
    id 2036
    label "doch&#243;d_narodowy"
  ]
  node [
    id 2037
    label "biegni&#281;cie"
  ]
  node [
    id 2038
    label "wi&#261;zanie"
  ]
  node [
    id 2039
    label "zawi&#261;zywanie"
  ]
  node [
    id 2040
    label "sk&#322;adanie"
  ]
  node [
    id 2041
    label "lace"
  ]
  node [
    id 2042
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 2043
    label "biec"
  ]
  node [
    id 2044
    label "wi&#261;za&#263;"
  ]
  node [
    id 2045
    label "kosmetyk"
  ]
  node [
    id 2046
    label "kosmetyk_kolorowy"
  ]
  node [
    id 2047
    label "sztyft"
  ]
  node [
    id 2048
    label "o&#322;&#243;wek"
  ]
  node [
    id 2049
    label "szewc"
  ]
  node [
    id 2050
    label "mazak"
  ]
  node [
    id 2051
    label "pi&#243;ro"
  ]
  node [
    id 2052
    label "d&#322;ugopis"
  ]
  node [
    id 2053
    label "&#380;elopis"
  ]
  node [
    id 2054
    label "fang"
  ]
  node [
    id 2055
    label "pr&#281;cik"
  ]
  node [
    id 2056
    label "szpilka"
  ]
  node [
    id 2057
    label "pin"
  ]
  node [
    id 2058
    label "preparat"
  ]
  node [
    id 2059
    label "cosmetic"
  ]
  node [
    id 2060
    label "olejek"
  ]
  node [
    id 2061
    label "gruczo&#322;_tarczycowy"
  ]
  node [
    id 2062
    label "&#380;y&#322;a_szyjna_wewn&#281;trzna"
  ]
  node [
    id 2063
    label "d&#243;&#322;_nadobojczykowy"
  ]
  node [
    id 2064
    label "gardziel"
  ]
  node [
    id 2065
    label "&#380;y&#322;a_szyjna_przednia"
  ]
  node [
    id 2066
    label "podgardle"
  ]
  node [
    id 2067
    label "nerw_przeponowy"
  ]
  node [
    id 2068
    label "mi&#281;sie&#324;_pochy&#322;y"
  ]
  node [
    id 2069
    label "przedbramie"
  ]
  node [
    id 2070
    label "grdyka"
  ]
  node [
    id 2071
    label "mi&#281;sie&#324;_mostkowo-obojczykowo-sutkowy"
  ]
  node [
    id 2072
    label "&#380;y&#322;a_szyjna_zewn&#281;trzna"
  ]
  node [
    id 2073
    label "kark"
  ]
  node [
    id 2074
    label "neck"
  ]
  node [
    id 2075
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 2076
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 2077
    label "ptaszek"
  ]
  node [
    id 2078
    label "przyrodzenie"
  ]
  node [
    id 2079
    label "fiut"
  ]
  node [
    id 2080
    label "shaft"
  ]
  node [
    id 2081
    label "wchodzenie"
  ]
  node [
    id 2082
    label "wej&#347;cie"
  ]
  node [
    id 2083
    label "fortyfikacja"
  ]
  node [
    id 2084
    label "dobud&#243;wka"
  ]
  node [
    id 2085
    label "biblizm"
  ]
  node [
    id 2086
    label "uzda"
  ]
  node [
    id 2087
    label "dewlap"
  ]
  node [
    id 2088
    label "wieprzowina"
  ]
  node [
    id 2089
    label "tusza"
  ]
  node [
    id 2090
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 2091
    label "sze&#347;ciopak"
  ]
  node [
    id 2092
    label "kulturysta"
  ]
  node [
    id 2093
    label "mu&#322;y"
  ]
  node [
    id 2094
    label "przej&#347;cie"
  ]
  node [
    id 2095
    label "przew&#243;d_pokarmowy"
  ]
  node [
    id 2096
    label "strunowiec"
  ]
  node [
    id 2097
    label "cie&#347;&#324;_gardzieli"
  ]
  node [
    id 2098
    label "hutnictwo"
  ]
  node [
    id 2099
    label "ga&#378;nik"
  ]
  node [
    id 2100
    label "cylinder"
  ]
  node [
    id 2101
    label "piec"
  ]
  node [
    id 2102
    label "throat"
  ]
  node [
    id 2103
    label "w&#281;zina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 881
  ]
  edge [
    source 10
    target 882
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 884
  ]
  edge [
    source 10
    target 885
  ]
  edge [
    source 10
    target 886
  ]
  edge [
    source 10
    target 887
  ]
  edge [
    source 10
    target 888
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 10
    target 891
  ]
  edge [
    source 10
    target 892
  ]
  edge [
    source 10
    target 893
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 10
    target 895
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 10
    target 904
  ]
  edge [
    source 10
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 930
  ]
  edge [
    source 10
    target 931
  ]
  edge [
    source 10
    target 932
  ]
  edge [
    source 10
    target 933
  ]
  edge [
    source 10
    target 934
  ]
  edge [
    source 10
    target 935
  ]
  edge [
    source 10
    target 936
  ]
  edge [
    source 10
    target 937
  ]
  edge [
    source 10
    target 938
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 939
  ]
  edge [
    source 10
    target 940
  ]
  edge [
    source 10
    target 941
  ]
  edge [
    source 10
    target 942
  ]
  edge [
    source 10
    target 943
  ]
  edge [
    source 10
    target 944
  ]
  edge [
    source 10
    target 945
  ]
  edge [
    source 10
    target 946
  ]
  edge [
    source 10
    target 947
  ]
  edge [
    source 10
    target 948
  ]
  edge [
    source 10
    target 949
  ]
  edge [
    source 10
    target 950
  ]
  edge [
    source 10
    target 951
  ]
  edge [
    source 10
    target 952
  ]
  edge [
    source 10
    target 953
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 955
  ]
  edge [
    source 10
    target 956
  ]
  edge [
    source 10
    target 957
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 961
  ]
  edge [
    source 10
    target 962
  ]
  edge [
    source 10
    target 963
  ]
  edge [
    source 10
    target 964
  ]
  edge [
    source 10
    target 965
  ]
  edge [
    source 10
    target 966
  ]
  edge [
    source 10
    target 967
  ]
  edge [
    source 10
    target 968
  ]
  edge [
    source 10
    target 969
  ]
  edge [
    source 10
    target 970
  ]
  edge [
    source 10
    target 971
  ]
  edge [
    source 10
    target 972
  ]
  edge [
    source 10
    target 973
  ]
  edge [
    source 10
    target 974
  ]
  edge [
    source 10
    target 975
  ]
  edge [
    source 10
    target 976
  ]
  edge [
    source 10
    target 977
  ]
  edge [
    source 10
    target 978
  ]
  edge [
    source 10
    target 979
  ]
  edge [
    source 10
    target 980
  ]
  edge [
    source 10
    target 981
  ]
  edge [
    source 10
    target 982
  ]
  edge [
    source 10
    target 983
  ]
  edge [
    source 10
    target 984
  ]
  edge [
    source 10
    target 985
  ]
  edge [
    source 10
    target 986
  ]
  edge [
    source 10
    target 987
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 988
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 989
  ]
  edge [
    source 10
    target 990
  ]
  edge [
    source 10
    target 991
  ]
  edge [
    source 10
    target 992
  ]
  edge [
    source 10
    target 993
  ]
  edge [
    source 10
    target 994
  ]
  edge [
    source 10
    target 995
  ]
  edge [
    source 10
    target 996
  ]
  edge [
    source 10
    target 997
  ]
  edge [
    source 10
    target 998
  ]
  edge [
    source 10
    target 999
  ]
  edge [
    source 10
    target 1000
  ]
  edge [
    source 10
    target 1001
  ]
  edge [
    source 10
    target 1002
  ]
  edge [
    source 10
    target 1003
  ]
  edge [
    source 10
    target 1004
  ]
  edge [
    source 10
    target 1005
  ]
  edge [
    source 10
    target 1006
  ]
  edge [
    source 10
    target 1007
  ]
  edge [
    source 10
    target 1008
  ]
  edge [
    source 10
    target 1009
  ]
  edge [
    source 10
    target 1010
  ]
  edge [
    source 10
    target 1011
  ]
  edge [
    source 10
    target 1012
  ]
  edge [
    source 10
    target 34
  ]
  edge [
    source 10
    target 1013
  ]
  edge [
    source 10
    target 1014
  ]
  edge [
    source 10
    target 1015
  ]
  edge [
    source 10
    target 1016
  ]
  edge [
    source 10
    target 1017
  ]
  edge [
    source 10
    target 1018
  ]
  edge [
    source 10
    target 1019
  ]
  edge [
    source 10
    target 1020
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 1021
  ]
  edge [
    source 10
    target 1022
  ]
  edge [
    source 10
    target 1023
  ]
  edge [
    source 10
    target 1024
  ]
  edge [
    source 10
    target 1025
  ]
  edge [
    source 10
    target 1026
  ]
  edge [
    source 10
    target 1027
  ]
  edge [
    source 10
    target 1028
  ]
  edge [
    source 10
    target 1029
  ]
  edge [
    source 10
    target 1030
  ]
  edge [
    source 10
    target 1031
  ]
  edge [
    source 10
    target 1032
  ]
  edge [
    source 10
    target 1033
  ]
  edge [
    source 10
    target 1034
  ]
  edge [
    source 10
    target 1035
  ]
  edge [
    source 10
    target 1036
  ]
  edge [
    source 10
    target 1037
  ]
  edge [
    source 10
    target 1038
  ]
  edge [
    source 10
    target 1039
  ]
  edge [
    source 10
    target 1040
  ]
  edge [
    source 10
    target 1041
  ]
  edge [
    source 10
    target 1042
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 1043
  ]
  edge [
    source 10
    target 1044
  ]
  edge [
    source 10
    target 1045
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 1046
  ]
  edge [
    source 10
    target 1047
  ]
  edge [
    source 10
    target 1048
  ]
  edge [
    source 10
    target 1049
  ]
  edge [
    source 10
    target 1050
  ]
  edge [
    source 10
    target 1051
  ]
  edge [
    source 10
    target 1052
  ]
  edge [
    source 10
    target 1053
  ]
  edge [
    source 10
    target 1054
  ]
  edge [
    source 10
    target 1055
  ]
  edge [
    source 10
    target 1056
  ]
  edge [
    source 10
    target 1057
  ]
  edge [
    source 10
    target 1058
  ]
  edge [
    source 10
    target 1059
  ]
  edge [
    source 10
    target 1060
  ]
  edge [
    source 10
    target 1061
  ]
  edge [
    source 10
    target 1062
  ]
  edge [
    source 10
    target 1063
  ]
  edge [
    source 10
    target 1064
  ]
  edge [
    source 10
    target 1065
  ]
  edge [
    source 10
    target 1066
  ]
  edge [
    source 10
    target 1067
  ]
  edge [
    source 10
    target 1068
  ]
  edge [
    source 10
    target 1069
  ]
  edge [
    source 10
    target 1070
  ]
  edge [
    source 10
    target 1071
  ]
  edge [
    source 10
    target 1072
  ]
  edge [
    source 10
    target 1073
  ]
  edge [
    source 10
    target 1074
  ]
  edge [
    source 10
    target 1075
  ]
  edge [
    source 10
    target 1076
  ]
  edge [
    source 10
    target 1077
  ]
  edge [
    source 10
    target 1078
  ]
  edge [
    source 10
    target 1079
  ]
  edge [
    source 10
    target 1080
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1081
  ]
  edge [
    source 11
    target 1082
  ]
  edge [
    source 11
    target 1083
  ]
  edge [
    source 11
    target 1084
  ]
  edge [
    source 11
    target 1085
  ]
  edge [
    source 11
    target 1086
  ]
  edge [
    source 11
    target 1087
  ]
  edge [
    source 11
    target 1088
  ]
  edge [
    source 11
    target 1089
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 1090
  ]
  edge [
    source 11
    target 1091
  ]
  edge [
    source 11
    target 1092
  ]
  edge [
    source 11
    target 1093
  ]
  edge [
    source 11
    target 1094
  ]
  edge [
    source 11
    target 1095
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 1096
  ]
  edge [
    source 11
    target 1097
  ]
  edge [
    source 11
    target 1098
  ]
  edge [
    source 11
    target 1099
  ]
  edge [
    source 11
    target 1100
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 1101
  ]
  edge [
    source 11
    target 1102
  ]
  edge [
    source 11
    target 1103
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 1104
  ]
  edge [
    source 11
    target 1105
  ]
  edge [
    source 11
    target 1106
  ]
  edge [
    source 11
    target 1107
  ]
  edge [
    source 11
    target 1108
  ]
  edge [
    source 11
    target 1109
  ]
  edge [
    source 11
    target 1110
  ]
  edge [
    source 11
    target 1111
  ]
  edge [
    source 11
    target 1112
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 1113
  ]
  edge [
    source 11
    target 1114
  ]
  edge [
    source 11
    target 1115
  ]
  edge [
    source 11
    target 1116
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 1117
  ]
  edge [
    source 11
    target 1118
  ]
  edge [
    source 11
    target 1119
  ]
  edge [
    source 11
    target 1120
  ]
  edge [
    source 11
    target 1121
  ]
  edge [
    source 11
    target 1122
  ]
  edge [
    source 11
    target 1123
  ]
  edge [
    source 11
    target 1124
  ]
  edge [
    source 11
    target 1125
  ]
  edge [
    source 11
    target 1126
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 1127
  ]
  edge [
    source 11
    target 1128
  ]
  edge [
    source 11
    target 1129
  ]
  edge [
    source 11
    target 1130
  ]
  edge [
    source 11
    target 1131
  ]
  edge [
    source 11
    target 1132
  ]
  edge [
    source 11
    target 1133
  ]
  edge [
    source 11
    target 1134
  ]
  edge [
    source 11
    target 1135
  ]
  edge [
    source 11
    target 1136
  ]
  edge [
    source 11
    target 1137
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 1138
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 1139
  ]
  edge [
    source 11
    target 1140
  ]
  edge [
    source 11
    target 1141
  ]
  edge [
    source 11
    target 1142
  ]
  edge [
    source 11
    target 1143
  ]
  edge [
    source 11
    target 1144
  ]
  edge [
    source 11
    target 1145
  ]
  edge [
    source 11
    target 1146
  ]
  edge [
    source 11
    target 1147
  ]
  edge [
    source 11
    target 1148
  ]
  edge [
    source 11
    target 1149
  ]
  edge [
    source 11
    target 1150
  ]
  edge [
    source 11
    target 1151
  ]
  edge [
    source 11
    target 1152
  ]
  edge [
    source 11
    target 1153
  ]
  edge [
    source 11
    target 1154
  ]
  edge [
    source 11
    target 1155
  ]
  edge [
    source 11
    target 1156
  ]
  edge [
    source 11
    target 1157
  ]
  edge [
    source 11
    target 1158
  ]
  edge [
    source 11
    target 1159
  ]
  edge [
    source 11
    target 1160
  ]
  edge [
    source 11
    target 1161
  ]
  edge [
    source 11
    target 1162
  ]
  edge [
    source 11
    target 1163
  ]
  edge [
    source 11
    target 1164
  ]
  edge [
    source 11
    target 1165
  ]
  edge [
    source 11
    target 1166
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1123
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1122
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1127
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 1229
  ]
  edge [
    source 12
    target 1230
  ]
  edge [
    source 12
    target 1231
  ]
  edge [
    source 12
    target 1232
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 1233
  ]
  edge [
    source 12
    target 1234
  ]
  edge [
    source 12
    target 1235
  ]
  edge [
    source 12
    target 1236
  ]
  edge [
    source 12
    target 1237
  ]
  edge [
    source 12
    target 1238
  ]
  edge [
    source 12
    target 1239
  ]
  edge [
    source 12
    target 1240
  ]
  edge [
    source 12
    target 1241
  ]
  edge [
    source 12
    target 1242
  ]
  edge [
    source 12
    target 1243
  ]
  edge [
    source 12
    target 1244
  ]
  edge [
    source 12
    target 1245
  ]
  edge [
    source 12
    target 1246
  ]
  edge [
    source 12
    target 1247
  ]
  edge [
    source 12
    target 1248
  ]
  edge [
    source 12
    target 1249
  ]
  edge [
    source 12
    target 1250
  ]
  edge [
    source 12
    target 1251
  ]
  edge [
    source 12
    target 1252
  ]
  edge [
    source 12
    target 1253
  ]
  edge [
    source 12
    target 1254
  ]
  edge [
    source 12
    target 1255
  ]
  edge [
    source 12
    target 1256
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1257
  ]
  edge [
    source 12
    target 1258
  ]
  edge [
    source 12
    target 1259
  ]
  edge [
    source 12
    target 1260
  ]
  edge [
    source 12
    target 1261
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 1262
  ]
  edge [
    source 12
    target 1263
  ]
  edge [
    source 12
    target 1264
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 12
    target 1309
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1310
  ]
  edge [
    source 13
    target 1311
  ]
  edge [
    source 13
    target 1312
  ]
  edge [
    source 13
    target 1313
  ]
  edge [
    source 13
    target 1314
  ]
  edge [
    source 13
    target 1044
  ]
  edge [
    source 13
    target 1315
  ]
  edge [
    source 13
    target 1316
  ]
  edge [
    source 13
    target 1317
  ]
  edge [
    source 13
    target 1318
  ]
  edge [
    source 13
    target 1319
  ]
  edge [
    source 13
    target 1160
  ]
  edge [
    source 13
    target 1320
  ]
  edge [
    source 13
    target 1321
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 1322
  ]
  edge [
    source 13
    target 1323
  ]
  edge [
    source 13
    target 1324
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 1334
  ]
  edge [
    source 13
    target 1335
  ]
  edge [
    source 13
    target 1336
  ]
  edge [
    source 13
    target 1337
  ]
  edge [
    source 13
    target 1338
  ]
  edge [
    source 13
    target 1339
  ]
  edge [
    source 13
    target 1340
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 1341
  ]
  edge [
    source 13
    target 1342
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1343
  ]
  edge [
    source 14
    target 1344
  ]
  edge [
    source 14
    target 1345
  ]
  edge [
    source 14
    target 1346
  ]
  edge [
    source 14
    target 1347
  ]
  edge [
    source 14
    target 1348
  ]
  edge [
    source 14
    target 1349
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 1350
  ]
  edge [
    source 14
    target 1351
  ]
  edge [
    source 14
    target 1352
  ]
  edge [
    source 14
    target 1353
  ]
  edge [
    source 14
    target 1354
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 1355
  ]
  edge [
    source 14
    target 1356
  ]
  edge [
    source 14
    target 1357
  ]
  edge [
    source 14
    target 1358
  ]
  edge [
    source 14
    target 1359
  ]
  edge [
    source 14
    target 1360
  ]
  edge [
    source 14
    target 1361
  ]
  edge [
    source 14
    target 1362
  ]
  edge [
    source 14
    target 1363
  ]
  edge [
    source 14
    target 1364
  ]
  edge [
    source 14
    target 1365
  ]
  edge [
    source 14
    target 1366
  ]
  edge [
    source 14
    target 1367
  ]
  edge [
    source 14
    target 1368
  ]
  edge [
    source 14
    target 1369
  ]
  edge [
    source 14
    target 1370
  ]
  edge [
    source 14
    target 1371
  ]
  edge [
    source 14
    target 1372
  ]
  edge [
    source 14
    target 1373
  ]
  edge [
    source 14
    target 1374
  ]
  edge [
    source 14
    target 1375
  ]
  edge [
    source 14
    target 1376
  ]
  edge [
    source 14
    target 1377
  ]
  edge [
    source 14
    target 1378
  ]
  edge [
    source 14
    target 1379
  ]
  edge [
    source 14
    target 1380
  ]
  edge [
    source 14
    target 1381
  ]
  edge [
    source 14
    target 1382
  ]
  edge [
    source 14
    target 1383
  ]
  edge [
    source 14
    target 1384
  ]
  edge [
    source 14
    target 1385
  ]
  edge [
    source 14
    target 1386
  ]
  edge [
    source 14
    target 1387
  ]
  edge [
    source 14
    target 1388
  ]
  edge [
    source 14
    target 1389
  ]
  edge [
    source 14
    target 1390
  ]
  edge [
    source 14
    target 1391
  ]
  edge [
    source 14
    target 1392
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 1393
  ]
  edge [
    source 15
    target 1394
  ]
  edge [
    source 15
    target 1395
  ]
  edge [
    source 15
    target 1396
  ]
  edge [
    source 15
    target 1397
  ]
  edge [
    source 15
    target 1398
  ]
  edge [
    source 15
    target 1399
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1400
  ]
  edge [
    source 15
    target 1401
  ]
  edge [
    source 15
    target 1402
  ]
  edge [
    source 15
    target 1403
  ]
  edge [
    source 15
    target 1404
  ]
  edge [
    source 15
    target 1405
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 1406
  ]
  edge [
    source 15
    target 1407
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 1408
  ]
  edge [
    source 15
    target 1409
  ]
  edge [
    source 15
    target 1410
  ]
  edge [
    source 15
    target 1192
  ]
  edge [
    source 15
    target 1411
  ]
  edge [
    source 15
    target 1412
  ]
  edge [
    source 15
    target 1413
  ]
  edge [
    source 15
    target 1414
  ]
  edge [
    source 15
    target 1415
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 1416
  ]
  edge [
    source 15
    target 1417
  ]
  edge [
    source 15
    target 1418
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 1419
  ]
  edge [
    source 15
    target 1066
  ]
  edge [
    source 15
    target 1420
  ]
  edge [
    source 15
    target 1421
  ]
  edge [
    source 15
    target 1422
  ]
  edge [
    source 15
    target 1423
  ]
  edge [
    source 15
    target 1424
  ]
  edge [
    source 15
    target 1425
  ]
  edge [
    source 15
    target 1426
  ]
  edge [
    source 15
    target 1427
  ]
  edge [
    source 15
    target 1428
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 1429
  ]
  edge [
    source 15
    target 1430
  ]
  edge [
    source 15
    target 1431
  ]
  edge [
    source 15
    target 1432
  ]
  edge [
    source 15
    target 1433
  ]
  edge [
    source 15
    target 1434
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 1435
  ]
  edge [
    source 15
    target 247
  ]
  edge [
    source 15
    target 1436
  ]
  edge [
    source 15
    target 1437
  ]
  edge [
    source 15
    target 1438
  ]
  edge [
    source 15
    target 1439
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 15
    target 1440
  ]
  edge [
    source 15
    target 1441
  ]
  edge [
    source 15
    target 1442
  ]
  edge [
    source 15
    target 1443
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 1444
  ]
  edge [
    source 15
    target 1445
  ]
  edge [
    source 15
    target 1446
  ]
  edge [
    source 15
    target 1447
  ]
  edge [
    source 15
    target 1448
  ]
  edge [
    source 15
    target 1449
  ]
  edge [
    source 15
    target 1450
  ]
  edge [
    source 15
    target 1451
  ]
  edge [
    source 15
    target 1452
  ]
  edge [
    source 15
    target 1453
  ]
  edge [
    source 15
    target 1454
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 1455
  ]
  edge [
    source 15
    target 1456
  ]
  edge [
    source 15
    target 1457
  ]
  edge [
    source 15
    target 1458
  ]
  edge [
    source 15
    target 1459
  ]
  edge [
    source 15
    target 1460
  ]
  edge [
    source 15
    target 1461
  ]
  edge [
    source 15
    target 1462
  ]
  edge [
    source 15
    target 1463
  ]
  edge [
    source 15
    target 1464
  ]
  edge [
    source 15
    target 1465
  ]
  edge [
    source 15
    target 1466
  ]
  edge [
    source 15
    target 1467
  ]
  edge [
    source 15
    target 1468
  ]
  edge [
    source 15
    target 1469
  ]
  edge [
    source 15
    target 1470
  ]
  edge [
    source 15
    target 1471
  ]
  edge [
    source 15
    target 1472
  ]
  edge [
    source 15
    target 1473
  ]
  edge [
    source 15
    target 1474
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1475
  ]
  edge [
    source 16
    target 1476
  ]
  edge [
    source 16
    target 1014
  ]
  edge [
    source 16
    target 1477
  ]
  edge [
    source 16
    target 1478
  ]
  edge [
    source 16
    target 1479
  ]
  edge [
    source 16
    target 1480
  ]
  edge [
    source 16
    target 1481
  ]
  edge [
    source 16
    target 1482
  ]
  edge [
    source 16
    target 1483
  ]
  edge [
    source 16
    target 1484
  ]
  edge [
    source 16
    target 1485
  ]
  edge [
    source 16
    target 1486
  ]
  edge [
    source 16
    target 1487
  ]
  edge [
    source 16
    target 1488
  ]
  edge [
    source 16
    target 1489
  ]
  edge [
    source 16
    target 1490
  ]
  edge [
    source 16
    target 1491
  ]
  edge [
    source 16
    target 1492
  ]
  edge [
    source 16
    target 1493
  ]
  edge [
    source 16
    target 1494
  ]
  edge [
    source 16
    target 1495
  ]
  edge [
    source 16
    target 1496
  ]
  edge [
    source 16
    target 1104
  ]
  edge [
    source 16
    target 1497
  ]
  edge [
    source 16
    target 1498
  ]
  edge [
    source 16
    target 1499
  ]
  edge [
    source 16
    target 1500
  ]
  edge [
    source 16
    target 1501
  ]
  edge [
    source 16
    target 1502
  ]
  edge [
    source 16
    target 1503
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 1504
  ]
  edge [
    source 16
    target 1505
  ]
  edge [
    source 16
    target 1506
  ]
  edge [
    source 16
    target 1507
  ]
  edge [
    source 16
    target 1508
  ]
  edge [
    source 16
    target 1509
  ]
  edge [
    source 16
    target 1510
  ]
  edge [
    source 16
    target 1511
  ]
  edge [
    source 16
    target 1512
  ]
  edge [
    source 16
    target 1029
  ]
  edge [
    source 16
    target 1513
  ]
  edge [
    source 16
    target 463
  ]
  edge [
    source 16
    target 1514
  ]
  edge [
    source 16
    target 1515
  ]
  edge [
    source 16
    target 1516
  ]
  edge [
    source 16
    target 1517
  ]
  edge [
    source 16
    target 1518
  ]
  edge [
    source 16
    target 1519
  ]
  edge [
    source 16
    target 1520
  ]
  edge [
    source 16
    target 1521
  ]
  edge [
    source 16
    target 1522
  ]
  edge [
    source 16
    target 1523
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 1524
  ]
  edge [
    source 16
    target 1525
  ]
  edge [
    source 16
    target 1526
  ]
  edge [
    source 16
    target 34
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 1527
  ]
  edge [
    source 16
    target 1528
  ]
  edge [
    source 16
    target 1445
  ]
  edge [
    source 16
    target 1529
  ]
  edge [
    source 16
    target 1530
  ]
  edge [
    source 16
    target 1352
  ]
  edge [
    source 16
    target 1531
  ]
  edge [
    source 16
    target 1532
  ]
  edge [
    source 16
    target 1533
  ]
  edge [
    source 16
    target 1534
  ]
  edge [
    source 16
    target 1535
  ]
  edge [
    source 16
    target 1536
  ]
  edge [
    source 16
    target 1537
  ]
  edge [
    source 16
    target 1538
  ]
  edge [
    source 16
    target 1539
  ]
  edge [
    source 16
    target 1540
  ]
  edge [
    source 16
    target 1541
  ]
  edge [
    source 16
    target 1542
  ]
  edge [
    source 16
    target 1543
  ]
  edge [
    source 16
    target 1544
  ]
  edge [
    source 16
    target 1545
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 1546
  ]
  edge [
    source 16
    target 1547
  ]
  edge [
    source 16
    target 1548
  ]
  edge [
    source 16
    target 1549
  ]
  edge [
    source 16
    target 1550
  ]
  edge [
    source 16
    target 1551
  ]
  edge [
    source 16
    target 1552
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1553
  ]
  edge [
    source 16
    target 1554
  ]
  edge [
    source 16
    target 1555
  ]
  edge [
    source 16
    target 1556
  ]
  edge [
    source 16
    target 1557
  ]
  edge [
    source 16
    target 1558
  ]
  edge [
    source 16
    target 1559
  ]
  edge [
    source 16
    target 1560
  ]
  edge [
    source 16
    target 1561
  ]
  edge [
    source 16
    target 1562
  ]
  edge [
    source 16
    target 1563
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 1436
  ]
  edge [
    source 16
    target 1564
  ]
  edge [
    source 16
    target 1565
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 1566
  ]
  edge [
    source 16
    target 1567
  ]
  edge [
    source 16
    target 1568
  ]
  edge [
    source 16
    target 1569
  ]
  edge [
    source 16
    target 1570
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1571
  ]
  edge [
    source 17
    target 1572
  ]
  edge [
    source 17
    target 1573
  ]
  edge [
    source 17
    target 1574
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 1575
  ]
  edge [
    source 17
    target 1576
  ]
  edge [
    source 17
    target 1577
  ]
  edge [
    source 17
    target 1578
  ]
  edge [
    source 17
    target 1579
  ]
  edge [
    source 17
    target 1580
  ]
  edge [
    source 17
    target 1581
  ]
  edge [
    source 17
    target 1582
  ]
  edge [
    source 17
    target 1583
  ]
  edge [
    source 17
    target 1584
  ]
  edge [
    source 17
    target 1585
  ]
  edge [
    source 17
    target 1586
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 1587
  ]
  edge [
    source 17
    target 1588
  ]
  edge [
    source 17
    target 1589
  ]
  edge [
    source 17
    target 1590
  ]
  edge [
    source 17
    target 1591
  ]
  edge [
    source 17
    target 1592
  ]
  edge [
    source 17
    target 1593
  ]
  edge [
    source 17
    target 1594
  ]
  edge [
    source 17
    target 1595
  ]
  edge [
    source 17
    target 1596
  ]
  edge [
    source 17
    target 582
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 1597
  ]
  edge [
    source 17
    target 1598
  ]
  edge [
    source 17
    target 1599
  ]
  edge [
    source 17
    target 1600
  ]
  edge [
    source 17
    target 1601
  ]
  edge [
    source 17
    target 1602
  ]
  edge [
    source 17
    target 581
  ]
  edge [
    source 17
    target 1603
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 1604
  ]
  edge [
    source 17
    target 1605
  ]
  edge [
    source 17
    target 1606
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 1607
  ]
  edge [
    source 17
    target 1608
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 1609
  ]
  edge [
    source 17
    target 1610
  ]
  edge [
    source 17
    target 1611
  ]
  edge [
    source 17
    target 1612
  ]
  edge [
    source 17
    target 1613
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 1614
  ]
  edge [
    source 17
    target 1615
  ]
  edge [
    source 17
    target 1616
  ]
  edge [
    source 17
    target 1617
  ]
  edge [
    source 17
    target 1618
  ]
  edge [
    source 17
    target 1619
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 1620
  ]
  edge [
    source 17
    target 1621
  ]
  edge [
    source 17
    target 1622
  ]
  edge [
    source 17
    target 1623
  ]
  edge [
    source 17
    target 1624
  ]
  edge [
    source 17
    target 1625
  ]
  edge [
    source 17
    target 1626
  ]
  edge [
    source 17
    target 1627
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 1628
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 1629
  ]
  edge [
    source 17
    target 1630
  ]
  edge [
    source 17
    target 1631
  ]
  edge [
    source 17
    target 1632
  ]
  edge [
    source 17
    target 1633
  ]
  edge [
    source 17
    target 1634
  ]
  edge [
    source 17
    target 1635
  ]
  edge [
    source 17
    target 1636
  ]
  edge [
    source 17
    target 1637
  ]
  edge [
    source 17
    target 1638
  ]
  edge [
    source 17
    target 1639
  ]
  edge [
    source 17
    target 1640
  ]
  edge [
    source 17
    target 1641
  ]
  edge [
    source 17
    target 1642
  ]
  edge [
    source 17
    target 1643
  ]
  edge [
    source 17
    target 1644
  ]
  edge [
    source 17
    target 1645
  ]
  edge [
    source 17
    target 1646
  ]
  edge [
    source 17
    target 1647
  ]
  edge [
    source 17
    target 1648
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 1649
  ]
  edge [
    source 17
    target 1650
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1651
  ]
  edge [
    source 17
    target 1652
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 1653
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1654
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 1655
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 1656
  ]
  edge [
    source 19
    target 1657
  ]
  edge [
    source 19
    target 1658
  ]
  edge [
    source 19
    target 1659
  ]
  edge [
    source 19
    target 1660
  ]
  edge [
    source 19
    target 1661
  ]
  edge [
    source 19
    target 1662
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 1663
  ]
  edge [
    source 19
    target 1630
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 1664
  ]
  edge [
    source 19
    target 1665
  ]
  edge [
    source 19
    target 1666
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 1667
  ]
  edge [
    source 19
    target 1668
  ]
  edge [
    source 19
    target 1669
  ]
  edge [
    source 19
    target 1670
  ]
  edge [
    source 19
    target 1671
  ]
  edge [
    source 19
    target 1672
  ]
  edge [
    source 19
    target 1673
  ]
  edge [
    source 19
    target 1674
  ]
  edge [
    source 19
    target 1675
  ]
  edge [
    source 19
    target 1676
  ]
  edge [
    source 19
    target 1677
  ]
  edge [
    source 19
    target 1678
  ]
  edge [
    source 19
    target 1679
  ]
  edge [
    source 19
    target 1680
  ]
  edge [
    source 19
    target 1681
  ]
  edge [
    source 19
    target 1682
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1683
  ]
  edge [
    source 19
    target 744
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1684
  ]
  edge [
    source 19
    target 1685
  ]
  edge [
    source 19
    target 1686
  ]
  edge [
    source 19
    target 1687
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 1688
  ]
  edge [
    source 19
    target 1689
  ]
  edge [
    source 19
    target 1690
  ]
  edge [
    source 19
    target 1691
  ]
  edge [
    source 19
    target 1692
  ]
  edge [
    source 19
    target 1693
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1694
  ]
  edge [
    source 20
    target 1695
  ]
  edge [
    source 20
    target 512
  ]
  edge [
    source 20
    target 652
  ]
  edge [
    source 20
    target 1696
  ]
  edge [
    source 20
    target 1697
  ]
  edge [
    source 20
    target 521
  ]
  edge [
    source 20
    target 509
  ]
  edge [
    source 20
    target 1698
  ]
  edge [
    source 20
    target 511
  ]
  edge [
    source 20
    target 1699
  ]
  edge [
    source 20
    target 1700
  ]
  edge [
    source 20
    target 1701
  ]
  edge [
    source 20
    target 1496
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1702
  ]
  edge [
    source 22
    target 1703
  ]
  edge [
    source 22
    target 1704
  ]
  edge [
    source 22
    target 1705
  ]
  edge [
    source 22
    target 1706
  ]
  edge [
    source 22
    target 1707
  ]
  edge [
    source 22
    target 1708
  ]
  edge [
    source 22
    target 685
  ]
  edge [
    source 22
    target 1709
  ]
  edge [
    source 22
    target 1710
  ]
  edge [
    source 22
    target 1711
  ]
  edge [
    source 22
    target 1712
  ]
  edge [
    source 22
    target 1713
  ]
  edge [
    source 22
    target 1714
  ]
  edge [
    source 22
    target 1715
  ]
  edge [
    source 22
    target 1716
  ]
  edge [
    source 22
    target 1717
  ]
  edge [
    source 22
    target 1718
  ]
  edge [
    source 22
    target 1719
  ]
  edge [
    source 22
    target 1720
  ]
  edge [
    source 22
    target 1721
  ]
  edge [
    source 22
    target 1371
  ]
  edge [
    source 22
    target 1722
  ]
  edge [
    source 22
    target 1723
  ]
  edge [
    source 22
    target 1724
  ]
  edge [
    source 22
    target 1725
  ]
  edge [
    source 22
    target 1726
  ]
  edge [
    source 22
    target 1727
  ]
  edge [
    source 22
    target 1728
  ]
  edge [
    source 22
    target 1729
  ]
  edge [
    source 22
    target 1730
  ]
  edge [
    source 22
    target 1731
  ]
  edge [
    source 22
    target 1732
  ]
  edge [
    source 22
    target 1733
  ]
  edge [
    source 22
    target 1734
  ]
  edge [
    source 22
    target 1735
  ]
  edge [
    source 22
    target 1736
  ]
  edge [
    source 22
    target 1737
  ]
  edge [
    source 22
    target 1738
  ]
  edge [
    source 22
    target 1739
  ]
  edge [
    source 22
    target 1740
  ]
  edge [
    source 22
    target 1741
  ]
  edge [
    source 22
    target 1742
  ]
  edge [
    source 22
    target 1743
  ]
  edge [
    source 22
    target 1744
  ]
  edge [
    source 22
    target 1745
  ]
  edge [
    source 22
    target 1746
  ]
  edge [
    source 22
    target 1747
  ]
  edge [
    source 22
    target 1748
  ]
  edge [
    source 22
    target 1749
  ]
  edge [
    source 22
    target 1750
  ]
  edge [
    source 22
    target 1751
  ]
  edge [
    source 22
    target 1752
  ]
  edge [
    source 22
    target 1753
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 1754
  ]
  edge [
    source 22
    target 1326
  ]
  edge [
    source 22
    target 1755
  ]
  edge [
    source 22
    target 1756
  ]
  edge [
    source 22
    target 702
  ]
  edge [
    source 22
    target 1757
  ]
  edge [
    source 22
    target 1758
  ]
  edge [
    source 22
    target 1759
  ]
  edge [
    source 22
    target 1760
  ]
  edge [
    source 22
    target 700
  ]
  edge [
    source 22
    target 1761
  ]
  edge [
    source 22
    target 1762
  ]
  edge [
    source 22
    target 681
  ]
  edge [
    source 22
    target 1763
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 684
  ]
  edge [
    source 22
    target 1764
  ]
  edge [
    source 22
    target 522
  ]
  edge [
    source 22
    target 1765
  ]
  edge [
    source 22
    target 1766
  ]
  edge [
    source 22
    target 1767
  ]
  edge [
    source 22
    target 1768
  ]
  edge [
    source 22
    target 1769
  ]
  edge [
    source 22
    target 1770
  ]
  edge [
    source 22
    target 1771
  ]
  edge [
    source 22
    target 1772
  ]
  edge [
    source 22
    target 1773
  ]
  edge [
    source 22
    target 707
  ]
  edge [
    source 22
    target 1774
  ]
  edge [
    source 22
    target 1775
  ]
  edge [
    source 22
    target 1776
  ]
  edge [
    source 22
    target 1777
  ]
  edge [
    source 22
    target 1778
  ]
  edge [
    source 22
    target 1779
  ]
  edge [
    source 22
    target 1780
  ]
  edge [
    source 22
    target 1781
  ]
  edge [
    source 22
    target 1782
  ]
  edge [
    source 22
    target 1783
  ]
  edge [
    source 22
    target 905
  ]
  edge [
    source 22
    target 1784
  ]
  edge [
    source 22
    target 1785
  ]
  edge [
    source 22
    target 1786
  ]
  edge [
    source 22
    target 1787
  ]
  edge [
    source 22
    target 1788
  ]
  edge [
    source 22
    target 1789
  ]
  edge [
    source 22
    target 897
  ]
  edge [
    source 22
    target 1790
  ]
  edge [
    source 22
    target 1791
  ]
  edge [
    source 22
    target 1792
  ]
  edge [
    source 22
    target 914
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 52
  ]
  edge [
    source 23
    target 1233
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 1235
  ]
  edge [
    source 23
    target 1236
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1238
  ]
  edge [
    source 23
    target 110
  ]
  edge [
    source 23
    target 1239
  ]
  edge [
    source 23
    target 1240
  ]
  edge [
    source 23
    target 1241
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 1243
  ]
  edge [
    source 23
    target 1244
  ]
  edge [
    source 23
    target 1245
  ]
  edge [
    source 23
    target 1246
  ]
  edge [
    source 23
    target 1247
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1248
  ]
  edge [
    source 23
    target 1249
  ]
  edge [
    source 23
    target 1250
  ]
  edge [
    source 23
    target 1251
  ]
  edge [
    source 23
    target 1252
  ]
  edge [
    source 23
    target 1253
  ]
  edge [
    source 23
    target 1254
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 1256
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1793
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 1794
  ]
  edge [
    source 23
    target 1795
  ]
  edge [
    source 23
    target 1796
  ]
  edge [
    source 23
    target 1797
  ]
  edge [
    source 23
    target 1595
  ]
  edge [
    source 23
    target 1798
  ]
  edge [
    source 23
    target 1799
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 1800
  ]
  edge [
    source 23
    target 1801
  ]
  edge [
    source 23
    target 1802
  ]
  edge [
    source 23
    target 1605
  ]
  edge [
    source 23
    target 636
  ]
  edge [
    source 23
    target 1803
  ]
  edge [
    source 23
    target 1804
  ]
  edge [
    source 23
    target 1805
  ]
  edge [
    source 23
    target 1806
  ]
  edge [
    source 23
    target 1807
  ]
  edge [
    source 23
    target 1607
  ]
  edge [
    source 23
    target 1808
  ]
  edge [
    source 23
    target 1200
  ]
  edge [
    source 23
    target 1809
  ]
  edge [
    source 23
    target 1810
  ]
  edge [
    source 23
    target 1811
  ]
  edge [
    source 23
    target 1480
  ]
  edge [
    source 23
    target 1812
  ]
  edge [
    source 23
    target 1813
  ]
  edge [
    source 23
    target 1814
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 23
    target 1196
  ]
  edge [
    source 23
    target 1815
  ]
  edge [
    source 23
    target 1816
  ]
  edge [
    source 23
    target 1817
  ]
  edge [
    source 23
    target 1818
  ]
  edge [
    source 23
    target 1819
  ]
  edge [
    source 23
    target 1820
  ]
  edge [
    source 23
    target 1821
  ]
  edge [
    source 23
    target 825
  ]
  edge [
    source 23
    target 1822
  ]
  edge [
    source 23
    target 1823
  ]
  edge [
    source 23
    target 1824
  ]
  edge [
    source 23
    target 1825
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 1309
  ]
  edge [
    source 23
    target 1826
  ]
  edge [
    source 23
    target 1827
  ]
  edge [
    source 23
    target 105
  ]
  edge [
    source 23
    target 106
  ]
  edge [
    source 23
    target 107
  ]
  edge [
    source 23
    target 108
  ]
  edge [
    source 23
    target 109
  ]
  edge [
    source 23
    target 111
  ]
  edge [
    source 23
    target 112
  ]
  edge [
    source 23
    target 113
  ]
  edge [
    source 23
    target 114
  ]
  edge [
    source 23
    target 115
  ]
  edge [
    source 23
    target 116
  ]
  edge [
    source 23
    target 117
  ]
  edge [
    source 23
    target 118
  ]
  edge [
    source 23
    target 119
  ]
  edge [
    source 23
    target 120
  ]
  edge [
    source 23
    target 121
  ]
  edge [
    source 23
    target 122
  ]
  edge [
    source 23
    target 123
  ]
  edge [
    source 23
    target 124
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 23
    target 129
  ]
  edge [
    source 23
    target 1828
  ]
  edge [
    source 23
    target 1829
  ]
  edge [
    source 23
    target 1830
  ]
  edge [
    source 23
    target 1831
  ]
  edge [
    source 23
    target 1832
  ]
  edge [
    source 23
    target 1833
  ]
  edge [
    source 23
    target 1834
  ]
  edge [
    source 23
    target 1835
  ]
  edge [
    source 23
    target 1542
  ]
  edge [
    source 23
    target 1836
  ]
  edge [
    source 23
    target 1837
  ]
  edge [
    source 23
    target 1838
  ]
  edge [
    source 23
    target 1839
  ]
  edge [
    source 23
    target 1840
  ]
  edge [
    source 23
    target 1841
  ]
  edge [
    source 23
    target 1842
  ]
  edge [
    source 23
    target 1843
  ]
  edge [
    source 23
    target 1844
  ]
  edge [
    source 23
    target 1845
  ]
  edge [
    source 23
    target 1846
  ]
  edge [
    source 23
    target 1847
  ]
  edge [
    source 23
    target 1848
  ]
  edge [
    source 23
    target 1849
  ]
  edge [
    source 23
    target 1850
  ]
  edge [
    source 23
    target 1285
  ]
  edge [
    source 23
    target 1851
  ]
  edge [
    source 23
    target 1852
  ]
  edge [
    source 23
    target 834
  ]
  edge [
    source 23
    target 1853
  ]
  edge [
    source 23
    target 1854
  ]
  edge [
    source 23
    target 788
  ]
  edge [
    source 23
    target 1855
  ]
  edge [
    source 23
    target 1856
  ]
  edge [
    source 23
    target 1857
  ]
  edge [
    source 23
    target 1858
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 1859
  ]
  edge [
    source 23
    target 1860
  ]
  edge [
    source 23
    target 562
  ]
  edge [
    source 23
    target 1861
  ]
  edge [
    source 23
    target 1862
  ]
  edge [
    source 23
    target 1863
  ]
  edge [
    source 23
    target 1864
  ]
  edge [
    source 23
    target 1398
  ]
  edge [
    source 23
    target 1405
  ]
  edge [
    source 23
    target 1865
  ]
  edge [
    source 23
    target 1866
  ]
  edge [
    source 23
    target 1867
  ]
  edge [
    source 23
    target 1868
  ]
  edge [
    source 23
    target 1869
  ]
  edge [
    source 23
    target 1870
  ]
  edge [
    source 23
    target 1871
  ]
  edge [
    source 23
    target 1872
  ]
  edge [
    source 23
    target 1873
  ]
  edge [
    source 23
    target 1874
  ]
  edge [
    source 23
    target 716
  ]
  edge [
    source 23
    target 1875
  ]
  edge [
    source 23
    target 1876
  ]
  edge [
    source 23
    target 1877
  ]
  edge [
    source 23
    target 1878
  ]
  edge [
    source 23
    target 1879
  ]
  edge [
    source 23
    target 1880
  ]
  edge [
    source 23
    target 1881
  ]
  edge [
    source 23
    target 947
  ]
  edge [
    source 23
    target 1882
  ]
  edge [
    source 23
    target 1883
  ]
  edge [
    source 23
    target 1884
  ]
  edge [
    source 23
    target 1885
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 1886
  ]
  edge [
    source 23
    target 1887
  ]
  edge [
    source 23
    target 1888
  ]
  edge [
    source 23
    target 1889
  ]
  edge [
    source 23
    target 1890
  ]
  edge [
    source 23
    target 1891
  ]
  edge [
    source 23
    target 1268
  ]
  edge [
    source 23
    target 1267
  ]
  edge [
    source 23
    target 1266
  ]
  edge [
    source 23
    target 1269
  ]
  edge [
    source 23
    target 1270
  ]
  edge [
    source 23
    target 1271
  ]
  edge [
    source 23
    target 1272
  ]
  edge [
    source 23
    target 1273
  ]
  edge [
    source 23
    target 1274
  ]
  edge [
    source 23
    target 1275
  ]
  edge [
    source 23
    target 1276
  ]
  edge [
    source 23
    target 1277
  ]
  edge [
    source 23
    target 1278
  ]
  edge [
    source 23
    target 1892
  ]
  edge [
    source 23
    target 1893
  ]
  edge [
    source 23
    target 1894
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 941
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1190
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1182
  ]
  edge [
    source 23
    target 1183
  ]
  edge [
    source 23
    target 1184
  ]
  edge [
    source 23
    target 1186
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 738
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1185
  ]
  edge [
    source 23
    target 1895
  ]
  edge [
    source 23
    target 1896
  ]
  edge [
    source 23
    target 1897
  ]
  edge [
    source 23
    target 1898
  ]
  edge [
    source 23
    target 1899
  ]
  edge [
    source 23
    target 1900
  ]
  edge [
    source 23
    target 1901
  ]
  edge [
    source 23
    target 1902
  ]
  edge [
    source 23
    target 1903
  ]
  edge [
    source 23
    target 1904
  ]
  edge [
    source 23
    target 1905
  ]
  edge [
    source 23
    target 1906
  ]
  edge [
    source 23
    target 1907
  ]
  edge [
    source 23
    target 1908
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 23
    target 1909
  ]
  edge [
    source 23
    target 1910
  ]
  edge [
    source 23
    target 1911
  ]
  edge [
    source 23
    target 1912
  ]
  edge [
    source 23
    target 1913
  ]
  edge [
    source 23
    target 1914
  ]
  edge [
    source 23
    target 1915
  ]
  edge [
    source 23
    target 1916
  ]
  edge [
    source 23
    target 1917
  ]
  edge [
    source 23
    target 1918
  ]
  edge [
    source 23
    target 1919
  ]
  edge [
    source 23
    target 1920
  ]
  edge [
    source 23
    target 1921
  ]
  edge [
    source 23
    target 1922
  ]
  edge [
    source 23
    target 1923
  ]
  edge [
    source 23
    target 1924
  ]
  edge [
    source 23
    target 1925
  ]
  edge [
    source 23
    target 1926
  ]
  edge [
    source 23
    target 1927
  ]
  edge [
    source 23
    target 1928
  ]
  edge [
    source 23
    target 1929
  ]
  edge [
    source 23
    target 1930
  ]
  edge [
    source 23
    target 1931
  ]
  edge [
    source 23
    target 1932
  ]
  edge [
    source 23
    target 1933
  ]
  edge [
    source 23
    target 1934
  ]
  edge [
    source 23
    target 1935
  ]
  edge [
    source 23
    target 1936
  ]
  edge [
    source 23
    target 1937
  ]
  edge [
    source 23
    target 1938
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1939
  ]
  edge [
    source 24
    target 1940
  ]
  edge [
    source 24
    target 52
  ]
  edge [
    source 24
    target 1941
  ]
  edge [
    source 24
    target 1942
  ]
  edge [
    source 24
    target 1943
  ]
  edge [
    source 24
    target 1944
  ]
  edge [
    source 24
    target 1945
  ]
  edge [
    source 24
    target 171
  ]
  edge [
    source 24
    target 1946
  ]
  edge [
    source 24
    target 1947
  ]
  edge [
    source 24
    target 1948
  ]
  edge [
    source 24
    target 1949
  ]
  edge [
    source 24
    target 1950
  ]
  edge [
    source 24
    target 1951
  ]
  edge [
    source 24
    target 1952
  ]
  edge [
    source 24
    target 1953
  ]
  edge [
    source 24
    target 1954
  ]
  edge [
    source 24
    target 1955
  ]
  edge [
    source 24
    target 1956
  ]
  edge [
    source 24
    target 1957
  ]
  edge [
    source 24
    target 1958
  ]
  edge [
    source 24
    target 1959
  ]
  edge [
    source 24
    target 1960
  ]
  edge [
    source 24
    target 1961
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 1962
  ]
  edge [
    source 24
    target 1963
  ]
  edge [
    source 24
    target 1964
  ]
  edge [
    source 24
    target 1965
  ]
  edge [
    source 24
    target 1966
  ]
  edge [
    source 24
    target 1967
  ]
  edge [
    source 24
    target 1968
  ]
  edge [
    source 24
    target 1969
  ]
  edge [
    source 24
    target 105
  ]
  edge [
    source 24
    target 106
  ]
  edge [
    source 24
    target 107
  ]
  edge [
    source 24
    target 108
  ]
  edge [
    source 24
    target 109
  ]
  edge [
    source 24
    target 110
  ]
  edge [
    source 24
    target 111
  ]
  edge [
    source 24
    target 112
  ]
  edge [
    source 24
    target 113
  ]
  edge [
    source 24
    target 114
  ]
  edge [
    source 24
    target 115
  ]
  edge [
    source 24
    target 116
  ]
  edge [
    source 24
    target 117
  ]
  edge [
    source 24
    target 118
  ]
  edge [
    source 24
    target 119
  ]
  edge [
    source 24
    target 120
  ]
  edge [
    source 24
    target 121
  ]
  edge [
    source 24
    target 122
  ]
  edge [
    source 24
    target 123
  ]
  edge [
    source 24
    target 124
  ]
  edge [
    source 24
    target 125
  ]
  edge [
    source 24
    target 126
  ]
  edge [
    source 24
    target 127
  ]
  edge [
    source 24
    target 128
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 1044
  ]
  edge [
    source 24
    target 1970
  ]
  edge [
    source 24
    target 1971
  ]
  edge [
    source 24
    target 1972
  ]
  edge [
    source 24
    target 1973
  ]
  edge [
    source 24
    target 1974
  ]
  edge [
    source 24
    target 1975
  ]
  edge [
    source 24
    target 1976
  ]
  edge [
    source 24
    target 572
  ]
  edge [
    source 24
    target 1977
  ]
  edge [
    source 24
    target 1978
  ]
  edge [
    source 24
    target 1979
  ]
  edge [
    source 24
    target 1980
  ]
  edge [
    source 24
    target 1981
  ]
  edge [
    source 24
    target 1982
  ]
  edge [
    source 24
    target 1983
  ]
  edge [
    source 24
    target 1984
  ]
  edge [
    source 24
    target 1985
  ]
  edge [
    source 24
    target 1986
  ]
  edge [
    source 24
    target 1987
  ]
  edge [
    source 24
    target 1988
  ]
  edge [
    source 24
    target 1989
  ]
  edge [
    source 24
    target 1990
  ]
  edge [
    source 24
    target 1991
  ]
  edge [
    source 24
    target 1992
  ]
  edge [
    source 24
    target 1993
  ]
  edge [
    source 24
    target 1994
  ]
  edge [
    source 24
    target 1995
  ]
  edge [
    source 24
    target 1996
  ]
  edge [
    source 24
    target 1997
  ]
  edge [
    source 24
    target 132
  ]
  edge [
    source 24
    target 1998
  ]
  edge [
    source 24
    target 1999
  ]
  edge [
    source 24
    target 2000
  ]
  edge [
    source 24
    target 2001
  ]
  edge [
    source 24
    target 2002
  ]
  edge [
    source 24
    target 2003
  ]
  edge [
    source 24
    target 2004
  ]
  edge [
    source 24
    target 2005
  ]
  edge [
    source 24
    target 2006
  ]
  edge [
    source 24
    target 2007
  ]
  edge [
    source 24
    target 2008
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 2009
  ]
  edge [
    source 24
    target 2010
  ]
  edge [
    source 24
    target 2011
  ]
  edge [
    source 24
    target 2012
  ]
  edge [
    source 24
    target 2013
  ]
  edge [
    source 24
    target 2014
  ]
  edge [
    source 24
    target 2015
  ]
  edge [
    source 24
    target 2016
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 2017
  ]
  edge [
    source 24
    target 2018
  ]
  edge [
    source 24
    target 2019
  ]
  edge [
    source 24
    target 2020
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 2021
  ]
  edge [
    source 25
    target 2022
  ]
  edge [
    source 25
    target 2023
  ]
  edge [
    source 25
    target 2024
  ]
  edge [
    source 25
    target 1289
  ]
  edge [
    source 25
    target 1598
  ]
  edge [
    source 25
    target 2025
  ]
  edge [
    source 25
    target 2026
  ]
  edge [
    source 25
    target 2027
  ]
  edge [
    source 25
    target 1870
  ]
  edge [
    source 25
    target 2028
  ]
  edge [
    source 25
    target 2029
  ]
  edge [
    source 25
    target 979
  ]
  edge [
    source 25
    target 2030
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 2031
  ]
  edge [
    source 25
    target 869
  ]
  edge [
    source 25
    target 2032
  ]
  edge [
    source 25
    target 149
  ]
  edge [
    source 25
    target 2033
  ]
  edge [
    source 25
    target 2034
  ]
  edge [
    source 25
    target 1470
  ]
  edge [
    source 25
    target 631
  ]
  edge [
    source 25
    target 2035
  ]
  edge [
    source 25
    target 2036
  ]
  edge [
    source 25
    target 2037
  ]
  edge [
    source 25
    target 2038
  ]
  edge [
    source 25
    target 2039
  ]
  edge [
    source 25
    target 2040
  ]
  edge [
    source 25
    target 2041
  ]
  edge [
    source 25
    target 2042
  ]
  edge [
    source 25
    target 2043
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 1456
  ]
  edge [
    source 25
    target 2044
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 2045
  ]
  edge [
    source 27
    target 2046
  ]
  edge [
    source 27
    target 2047
  ]
  edge [
    source 27
    target 2048
  ]
  edge [
    source 27
    target 1393
  ]
  edge [
    source 27
    target 2049
  ]
  edge [
    source 27
    target 2050
  ]
  edge [
    source 27
    target 2051
  ]
  edge [
    source 27
    target 2052
  ]
  edge [
    source 27
    target 1353
  ]
  edge [
    source 27
    target 2053
  ]
  edge [
    source 27
    target 2054
  ]
  edge [
    source 27
    target 2055
  ]
  edge [
    source 27
    target 2056
  ]
  edge [
    source 27
    target 2057
  ]
  edge [
    source 27
    target 2058
  ]
  edge [
    source 27
    target 2059
  ]
  edge [
    source 27
    target 2060
  ]
  edge [
    source 28
    target 2061
  ]
  edge [
    source 28
    target 2062
  ]
  edge [
    source 28
    target 2063
  ]
  edge [
    source 28
    target 2064
  ]
  edge [
    source 28
    target 2065
  ]
  edge [
    source 28
    target 2066
  ]
  edge [
    source 28
    target 2067
  ]
  edge [
    source 28
    target 2068
  ]
  edge [
    source 28
    target 2069
  ]
  edge [
    source 28
    target 2070
  ]
  edge [
    source 28
    target 2071
  ]
  edge [
    source 28
    target 2072
  ]
  edge [
    source 28
    target 1165
  ]
  edge [
    source 28
    target 2073
  ]
  edge [
    source 28
    target 2074
  ]
  edge [
    source 28
    target 2075
  ]
  edge [
    source 28
    target 100
  ]
  edge [
    source 28
    target 2076
  ]
  edge [
    source 28
    target 52
  ]
  edge [
    source 28
    target 1181
  ]
  edge [
    source 28
    target 2077
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 1883
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 2078
  ]
  edge [
    source 28
    target 2079
  ]
  edge [
    source 28
    target 2080
  ]
  edge [
    source 28
    target 2081
  ]
  edge [
    source 28
    target 415
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 2082
  ]
  edge [
    source 28
    target 2083
  ]
  edge [
    source 28
    target 2084
  ]
  edge [
    source 28
    target 2085
  ]
  edge [
    source 28
    target 2086
  ]
  edge [
    source 28
    target 2087
  ]
  edge [
    source 28
    target 1133
  ]
  edge [
    source 28
    target 2088
  ]
  edge [
    source 28
    target 2089
  ]
  edge [
    source 28
    target 2090
  ]
  edge [
    source 28
    target 2091
  ]
  edge [
    source 28
    target 2092
  ]
  edge [
    source 28
    target 2093
  ]
  edge [
    source 28
    target 2094
  ]
  edge [
    source 28
    target 2095
  ]
  edge [
    source 28
    target 2096
  ]
  edge [
    source 28
    target 2097
  ]
  edge [
    source 28
    target 2098
  ]
  edge [
    source 28
    target 2099
  ]
  edge [
    source 28
    target 2100
  ]
  edge [
    source 28
    target 825
  ]
  edge [
    source 28
    target 2101
  ]
  edge [
    source 28
    target 2102
  ]
  edge [
    source 28
    target 2103
  ]
]
