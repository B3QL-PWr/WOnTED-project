graph [
  node [
    id 0
    label "pomimo"
    origin "text"
  ]
  node [
    id 1
    label "grozi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "niebezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "inicjator"
    origin "text"
  ]
  node [
    id 6
    label "obozowy"
    origin "text"
  ]
  node [
    id 7
    label "ruch"
    origin "text"
  ]
  node [
    id 8
    label "op&#243;r"
    origin "text"
  ]
  node [
    id 9
    label "potajemnie"
    origin "text"
  ]
  node [
    id 10
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "wieczor"
    origin "text"
  ]
  node [
    id 12
    label "patriotyczny"
    origin "text"
  ]
  node [
    id 13
    label "pomaga&#263;"
    origin "text"
  ]
  node [
    id 14
    label "prze&#380;y&#263;"
    origin "text"
  ]
  node [
    id 15
    label "s&#322;aby"
    origin "text"
  ]
  node [
    id 16
    label "wi&#281;zie&#324;"
    origin "text"
  ]
  node [
    id 17
    label "hazard"
  ]
  node [
    id 18
    label "zapowiada&#263;"
  ]
  node [
    id 19
    label "boast"
  ]
  node [
    id 20
    label "ostrzega&#263;"
  ]
  node [
    id 21
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 22
    label "harbinger"
  ]
  node [
    id 23
    label "og&#322;asza&#263;"
  ]
  node [
    id 24
    label "bode"
  ]
  node [
    id 25
    label "post"
  ]
  node [
    id 26
    label "informowa&#263;"
  ]
  node [
    id 27
    label "play"
  ]
  node [
    id 28
    label "prawdopodobie&#324;stwo"
  ]
  node [
    id 29
    label "rozrywka"
  ]
  node [
    id 30
    label "wideoloteria"
  ]
  node [
    id 31
    label "poj&#281;cie"
  ]
  node [
    id 32
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 33
    label "zawisa&#263;"
  ]
  node [
    id 34
    label "cecha"
  ]
  node [
    id 35
    label "zagrozi&#263;"
  ]
  node [
    id 36
    label "zawisanie"
  ]
  node [
    id 37
    label "czarny_punkt"
  ]
  node [
    id 38
    label "charakterystyka"
  ]
  node [
    id 39
    label "m&#322;ot"
  ]
  node [
    id 40
    label "znak"
  ]
  node [
    id 41
    label "drzewo"
  ]
  node [
    id 42
    label "pr&#243;ba"
  ]
  node [
    id 43
    label "attribute"
  ]
  node [
    id 44
    label "marka"
  ]
  node [
    id 45
    label "zaszachowa&#263;"
  ]
  node [
    id 46
    label "threaten"
  ]
  node [
    id 47
    label "szachowa&#263;"
  ]
  node [
    id 48
    label "zaistnie&#263;"
  ]
  node [
    id 49
    label "uprzedzi&#263;"
  ]
  node [
    id 50
    label "menace"
  ]
  node [
    id 51
    label "pojawianie_si&#281;"
  ]
  node [
    id 52
    label "umieranie"
  ]
  node [
    id 53
    label "nieruchomienie"
  ]
  node [
    id 54
    label "dzianie_si&#281;"
  ]
  node [
    id 55
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 56
    label "mie&#263;_miejsce"
  ]
  node [
    id 57
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 58
    label "nieruchomie&#263;"
  ]
  node [
    id 59
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 60
    label "ko&#324;czy&#263;"
  ]
  node [
    id 61
    label "wystarczy&#263;"
  ]
  node [
    id 62
    label "trwa&#263;"
  ]
  node [
    id 63
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 64
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 65
    label "przebywa&#263;"
  ]
  node [
    id 66
    label "by&#263;"
  ]
  node [
    id 67
    label "pozostawa&#263;"
  ]
  node [
    id 68
    label "kosztowa&#263;"
  ]
  node [
    id 69
    label "undertaking"
  ]
  node [
    id 70
    label "digest"
  ]
  node [
    id 71
    label "wystawa&#263;"
  ]
  node [
    id 72
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 73
    label "wystarcza&#263;"
  ]
  node [
    id 74
    label "base"
  ]
  node [
    id 75
    label "mieszka&#263;"
  ]
  node [
    id 76
    label "stand"
  ]
  node [
    id 77
    label "sprawowa&#263;"
  ]
  node [
    id 78
    label "czeka&#263;"
  ]
  node [
    id 79
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 80
    label "istnie&#263;"
  ]
  node [
    id 81
    label "zostawa&#263;"
  ]
  node [
    id 82
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 83
    label "adhere"
  ]
  node [
    id 84
    label "function"
  ]
  node [
    id 85
    label "bind"
  ]
  node [
    id 86
    label "panowa&#263;"
  ]
  node [
    id 87
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 88
    label "zjednywa&#263;"
  ]
  node [
    id 89
    label "tkwi&#263;"
  ]
  node [
    id 90
    label "pause"
  ]
  node [
    id 91
    label "przestawa&#263;"
  ]
  node [
    id 92
    label "hesitate"
  ]
  node [
    id 93
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 94
    label "equal"
  ]
  node [
    id 95
    label "chodzi&#263;"
  ]
  node [
    id 96
    label "si&#281;ga&#263;"
  ]
  node [
    id 97
    label "stan"
  ]
  node [
    id 98
    label "obecno&#347;&#263;"
  ]
  node [
    id 99
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 100
    label "uczestniczy&#263;"
  ]
  node [
    id 101
    label "try"
  ]
  node [
    id 102
    label "savor"
  ]
  node [
    id 103
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 104
    label "cena"
  ]
  node [
    id 105
    label "doznawa&#263;"
  ]
  node [
    id 106
    label "essay"
  ]
  node [
    id 107
    label "suffice"
  ]
  node [
    id 108
    label "spowodowa&#263;"
  ]
  node [
    id 109
    label "stan&#261;&#263;"
  ]
  node [
    id 110
    label "zaspokoi&#263;"
  ]
  node [
    id 111
    label "dosta&#263;"
  ]
  node [
    id 112
    label "zaspokaja&#263;"
  ]
  node [
    id 113
    label "dostawa&#263;"
  ]
  node [
    id 114
    label "stawa&#263;"
  ]
  node [
    id 115
    label "pauzowa&#263;"
  ]
  node [
    id 116
    label "oczekiwa&#263;"
  ]
  node [
    id 117
    label "decydowa&#263;"
  ]
  node [
    id 118
    label "sp&#281;dza&#263;"
  ]
  node [
    id 119
    label "look"
  ]
  node [
    id 120
    label "hold"
  ]
  node [
    id 121
    label "anticipate"
  ]
  node [
    id 122
    label "blend"
  ]
  node [
    id 123
    label "stop"
  ]
  node [
    id 124
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 125
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 126
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 127
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 128
    label "support"
  ]
  node [
    id 129
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 130
    label "prosecute"
  ]
  node [
    id 131
    label "zajmowa&#263;"
  ]
  node [
    id 132
    label "room"
  ]
  node [
    id 133
    label "fall"
  ]
  node [
    id 134
    label "czynnik"
  ]
  node [
    id 135
    label "motor"
  ]
  node [
    id 136
    label "cz&#322;owiek"
  ]
  node [
    id 137
    label "biblioteka"
  ]
  node [
    id 138
    label "pojazd_drogowy"
  ]
  node [
    id 139
    label "wyci&#261;garka"
  ]
  node [
    id 140
    label "gondola_silnikowa"
  ]
  node [
    id 141
    label "aerosanie"
  ]
  node [
    id 142
    label "dwuko&#322;owiec"
  ]
  node [
    id 143
    label "wiatrochron"
  ]
  node [
    id 144
    label "rz&#281;&#380;enie"
  ]
  node [
    id 145
    label "podgrzewacz"
  ]
  node [
    id 146
    label "wirnik"
  ]
  node [
    id 147
    label "kosz"
  ]
  node [
    id 148
    label "motogodzina"
  ]
  node [
    id 149
    label "&#322;a&#324;cuch"
  ]
  node [
    id 150
    label "motoszybowiec"
  ]
  node [
    id 151
    label "program"
  ]
  node [
    id 152
    label "gniazdo_zaworowe"
  ]
  node [
    id 153
    label "mechanizm"
  ]
  node [
    id 154
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 155
    label "engine"
  ]
  node [
    id 156
    label "dociera&#263;"
  ]
  node [
    id 157
    label "samoch&#243;d"
  ]
  node [
    id 158
    label "dotarcie"
  ]
  node [
    id 159
    label "nap&#281;d"
  ]
  node [
    id 160
    label "motor&#243;wka"
  ]
  node [
    id 161
    label "rz&#281;zi&#263;"
  ]
  node [
    id 162
    label "perpetuum_mobile"
  ]
  node [
    id 163
    label "kierownica"
  ]
  node [
    id 164
    label "docieranie"
  ]
  node [
    id 165
    label "bombowiec"
  ]
  node [
    id 166
    label "dotrze&#263;"
  ]
  node [
    id 167
    label "radiator"
  ]
  node [
    id 168
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 169
    label "divisor"
  ]
  node [
    id 170
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 171
    label "faktor"
  ]
  node [
    id 172
    label "agent"
  ]
  node [
    id 173
    label "ekspozycja"
  ]
  node [
    id 174
    label "iloczyn"
  ]
  node [
    id 175
    label "mechanika"
  ]
  node [
    id 176
    label "utrzymywanie"
  ]
  node [
    id 177
    label "move"
  ]
  node [
    id 178
    label "poruszenie"
  ]
  node [
    id 179
    label "movement"
  ]
  node [
    id 180
    label "myk"
  ]
  node [
    id 181
    label "utrzyma&#263;"
  ]
  node [
    id 182
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 183
    label "zjawisko"
  ]
  node [
    id 184
    label "utrzymanie"
  ]
  node [
    id 185
    label "travel"
  ]
  node [
    id 186
    label "kanciasty"
  ]
  node [
    id 187
    label "commercial_enterprise"
  ]
  node [
    id 188
    label "model"
  ]
  node [
    id 189
    label "strumie&#324;"
  ]
  node [
    id 190
    label "proces"
  ]
  node [
    id 191
    label "aktywno&#347;&#263;"
  ]
  node [
    id 192
    label "kr&#243;tki"
  ]
  node [
    id 193
    label "taktyka"
  ]
  node [
    id 194
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 195
    label "apraksja"
  ]
  node [
    id 196
    label "natural_process"
  ]
  node [
    id 197
    label "utrzymywa&#263;"
  ]
  node [
    id 198
    label "d&#322;ugi"
  ]
  node [
    id 199
    label "wydarzenie"
  ]
  node [
    id 200
    label "dyssypacja_energii"
  ]
  node [
    id 201
    label "tumult"
  ]
  node [
    id 202
    label "stopek"
  ]
  node [
    id 203
    label "czynno&#347;&#263;"
  ]
  node [
    id 204
    label "zmiana"
  ]
  node [
    id 205
    label "manewr"
  ]
  node [
    id 206
    label "lokomocja"
  ]
  node [
    id 207
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 208
    label "komunikacja"
  ]
  node [
    id 209
    label "drift"
  ]
  node [
    id 210
    label "kognicja"
  ]
  node [
    id 211
    label "przebieg"
  ]
  node [
    id 212
    label "rozprawa"
  ]
  node [
    id 213
    label "legislacyjnie"
  ]
  node [
    id 214
    label "przes&#322;anka"
  ]
  node [
    id 215
    label "nast&#281;pstwo"
  ]
  node [
    id 216
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 217
    label "przebiec"
  ]
  node [
    id 218
    label "charakter"
  ]
  node [
    id 219
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 220
    label "motyw"
  ]
  node [
    id 221
    label "przebiegni&#281;cie"
  ]
  node [
    id 222
    label "fabu&#322;a"
  ]
  node [
    id 223
    label "action"
  ]
  node [
    id 224
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 225
    label "postawa"
  ]
  node [
    id 226
    label "posuni&#281;cie"
  ]
  node [
    id 227
    label "maneuver"
  ]
  node [
    id 228
    label "absolutorium"
  ]
  node [
    id 229
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 230
    label "dzia&#322;anie"
  ]
  node [
    id 231
    label "activity"
  ]
  node [
    id 232
    label "boski"
  ]
  node [
    id 233
    label "krajobraz"
  ]
  node [
    id 234
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 235
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 236
    label "przywidzenie"
  ]
  node [
    id 237
    label "presence"
  ]
  node [
    id 238
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 239
    label "transportation_system"
  ]
  node [
    id 240
    label "explicite"
  ]
  node [
    id 241
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 242
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 243
    label "wydeptywanie"
  ]
  node [
    id 244
    label "miejsce"
  ]
  node [
    id 245
    label "wydeptanie"
  ]
  node [
    id 246
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 247
    label "implicite"
  ]
  node [
    id 248
    label "ekspedytor"
  ]
  node [
    id 249
    label "bezproblemowy"
  ]
  node [
    id 250
    label "rewizja"
  ]
  node [
    id 251
    label "passage"
  ]
  node [
    id 252
    label "oznaka"
  ]
  node [
    id 253
    label "change"
  ]
  node [
    id 254
    label "ferment"
  ]
  node [
    id 255
    label "komplet"
  ]
  node [
    id 256
    label "anatomopatolog"
  ]
  node [
    id 257
    label "zmianka"
  ]
  node [
    id 258
    label "czas"
  ]
  node [
    id 259
    label "amendment"
  ]
  node [
    id 260
    label "praca"
  ]
  node [
    id 261
    label "odmienianie"
  ]
  node [
    id 262
    label "tura"
  ]
  node [
    id 263
    label "woda_powierzchniowa"
  ]
  node [
    id 264
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 265
    label "ciek_wodny"
  ]
  node [
    id 266
    label "mn&#243;stwo"
  ]
  node [
    id 267
    label "Ajgospotamoj"
  ]
  node [
    id 268
    label "fala"
  ]
  node [
    id 269
    label "disquiet"
  ]
  node [
    id 270
    label "ha&#322;as"
  ]
  node [
    id 271
    label "struktura"
  ]
  node [
    id 272
    label "mechanika_teoretyczna"
  ]
  node [
    id 273
    label "mechanika_gruntu"
  ]
  node [
    id 274
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 275
    label "mechanika_klasyczna"
  ]
  node [
    id 276
    label "elektromechanika"
  ]
  node [
    id 277
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 278
    label "nauka"
  ]
  node [
    id 279
    label "fizyka"
  ]
  node [
    id 280
    label "aeromechanika"
  ]
  node [
    id 281
    label "telemechanika"
  ]
  node [
    id 282
    label "hydromechanika"
  ]
  node [
    id 283
    label "daleki"
  ]
  node [
    id 284
    label "d&#322;ugo"
  ]
  node [
    id 285
    label "szybki"
  ]
  node [
    id 286
    label "jednowyrazowy"
  ]
  node [
    id 287
    label "bliski"
  ]
  node [
    id 288
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 289
    label "kr&#243;tko"
  ]
  node [
    id 290
    label "drobny"
  ]
  node [
    id 291
    label "brak"
  ]
  node [
    id 292
    label "z&#322;y"
  ]
  node [
    id 293
    label "byt"
  ]
  node [
    id 294
    label "argue"
  ]
  node [
    id 295
    label "podtrzymywa&#263;"
  ]
  node [
    id 296
    label "s&#261;dzi&#263;"
  ]
  node [
    id 297
    label "twierdzi&#263;"
  ]
  node [
    id 298
    label "zapewnia&#263;"
  ]
  node [
    id 299
    label "corroborate"
  ]
  node [
    id 300
    label "trzyma&#263;"
  ]
  node [
    id 301
    label "defy"
  ]
  node [
    id 302
    label "cope"
  ]
  node [
    id 303
    label "broni&#263;"
  ]
  node [
    id 304
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 305
    label "zachowywa&#263;"
  ]
  node [
    id 306
    label "obroni&#263;"
  ]
  node [
    id 307
    label "potrzyma&#263;"
  ]
  node [
    id 308
    label "op&#322;aci&#263;"
  ]
  node [
    id 309
    label "zdo&#322;a&#263;"
  ]
  node [
    id 310
    label "podtrzyma&#263;"
  ]
  node [
    id 311
    label "feed"
  ]
  node [
    id 312
    label "zrobi&#263;"
  ]
  node [
    id 313
    label "przetrzyma&#263;"
  ]
  node [
    id 314
    label "foster"
  ]
  node [
    id 315
    label "preserve"
  ]
  node [
    id 316
    label "zapewni&#263;"
  ]
  node [
    id 317
    label "zachowa&#263;"
  ]
  node [
    id 318
    label "unie&#347;&#263;"
  ]
  node [
    id 319
    label "bronienie"
  ]
  node [
    id 320
    label "trzymanie"
  ]
  node [
    id 321
    label "podtrzymywanie"
  ]
  node [
    id 322
    label "bycie"
  ]
  node [
    id 323
    label "potrzymanie"
  ]
  node [
    id 324
    label "wychowywanie"
  ]
  node [
    id 325
    label "panowanie"
  ]
  node [
    id 326
    label "zachowywanie"
  ]
  node [
    id 327
    label "twierdzenie"
  ]
  node [
    id 328
    label "preservation"
  ]
  node [
    id 329
    label "chowanie"
  ]
  node [
    id 330
    label "retention"
  ]
  node [
    id 331
    label "op&#322;acanie"
  ]
  node [
    id 332
    label "zapewnienie"
  ]
  node [
    id 333
    label "s&#261;dzenie"
  ]
  node [
    id 334
    label "zapewnianie"
  ]
  node [
    id 335
    label "obronienie"
  ]
  node [
    id 336
    label "zap&#322;acenie"
  ]
  node [
    id 337
    label "zachowanie"
  ]
  node [
    id 338
    label "przetrzymanie"
  ]
  node [
    id 339
    label "bearing"
  ]
  node [
    id 340
    label "zdo&#322;anie"
  ]
  node [
    id 341
    label "subsystencja"
  ]
  node [
    id 342
    label "uniesienie"
  ]
  node [
    id 343
    label "wy&#380;ywienie"
  ]
  node [
    id 344
    label "podtrzymanie"
  ]
  node [
    id 345
    label "wychowanie"
  ]
  node [
    id 346
    label "zrobienie"
  ]
  node [
    id 347
    label "wzbudzenie"
  ]
  node [
    id 348
    label "gesture"
  ]
  node [
    id 349
    label "spowodowanie"
  ]
  node [
    id 350
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 351
    label "poruszanie_si&#281;"
  ]
  node [
    id 352
    label "zdarzenie_si&#281;"
  ]
  node [
    id 353
    label "nietaktowny"
  ]
  node [
    id 354
    label "kanciasto"
  ]
  node [
    id 355
    label "niezgrabny"
  ]
  node [
    id 356
    label "kanciaty"
  ]
  node [
    id 357
    label "szorstki"
  ]
  node [
    id 358
    label "niesk&#322;adny"
  ]
  node [
    id 359
    label "stra&#380;nik"
  ]
  node [
    id 360
    label "szko&#322;a"
  ]
  node [
    id 361
    label "przedszkole"
  ]
  node [
    id 362
    label "opiekun"
  ]
  node [
    id 363
    label "spos&#243;b"
  ]
  node [
    id 364
    label "prezenter"
  ]
  node [
    id 365
    label "typ"
  ]
  node [
    id 366
    label "mildew"
  ]
  node [
    id 367
    label "zi&#243;&#322;ko"
  ]
  node [
    id 368
    label "motif"
  ]
  node [
    id 369
    label "pozowanie"
  ]
  node [
    id 370
    label "ideal"
  ]
  node [
    id 371
    label "wz&#243;r"
  ]
  node [
    id 372
    label "matryca"
  ]
  node [
    id 373
    label "adaptation"
  ]
  node [
    id 374
    label "pozowa&#263;"
  ]
  node [
    id 375
    label "imitacja"
  ]
  node [
    id 376
    label "orygina&#322;"
  ]
  node [
    id 377
    label "facet"
  ]
  node [
    id 378
    label "miniatura"
  ]
  node [
    id 379
    label "apraxia"
  ]
  node [
    id 380
    label "zaburzenie"
  ]
  node [
    id 381
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 382
    label "sport_motorowy"
  ]
  node [
    id 383
    label "jazda"
  ]
  node [
    id 384
    label "zwiad"
  ]
  node [
    id 385
    label "metoda"
  ]
  node [
    id 386
    label "pocz&#261;tki"
  ]
  node [
    id 387
    label "wrinkle"
  ]
  node [
    id 388
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 389
    label "Sierpie&#324;"
  ]
  node [
    id 390
    label "Michnik"
  ]
  node [
    id 391
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 392
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 393
    label "reluctance"
  ]
  node [
    id 394
    label "niech&#281;&#263;"
  ]
  node [
    id 395
    label "w&#322;a&#347;ciwo&#347;&#263;_fizyczna"
  ]
  node [
    id 396
    label "impedancja"
  ]
  node [
    id 397
    label "si&#322;a"
  ]
  node [
    id 398
    label "resistance"
  ]
  node [
    id 399
    label "protestacja"
  ]
  node [
    id 400
    label "czerwona_kartka"
  ]
  node [
    id 401
    label "opory_ruchu"
  ]
  node [
    id 402
    label "immunity"
  ]
  node [
    id 403
    label "reakcja"
  ]
  node [
    id 404
    label "react"
  ]
  node [
    id 405
    label "reaction"
  ]
  node [
    id 406
    label "organizm"
  ]
  node [
    id 407
    label "rozmowa"
  ]
  node [
    id 408
    label "response"
  ]
  node [
    id 409
    label "rezultat"
  ]
  node [
    id 410
    label "respondent"
  ]
  node [
    id 411
    label "energia"
  ]
  node [
    id 412
    label "parametr"
  ]
  node [
    id 413
    label "rozwi&#261;zanie"
  ]
  node [
    id 414
    label "wojsko"
  ]
  node [
    id 415
    label "wuchta"
  ]
  node [
    id 416
    label "zaleta"
  ]
  node [
    id 417
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 418
    label "moment_si&#322;y"
  ]
  node [
    id 419
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 420
    label "zdolno&#347;&#263;"
  ]
  node [
    id 421
    label "capacity"
  ]
  node [
    id 422
    label "magnitude"
  ]
  node [
    id 423
    label "potencja"
  ]
  node [
    id 424
    label "przemoc"
  ]
  node [
    id 425
    label "nastawienie"
  ]
  node [
    id 426
    label "emocja"
  ]
  node [
    id 427
    label "dysgust"
  ]
  node [
    id 428
    label "op&#243;r_elektryczny"
  ]
  node [
    id 429
    label "uog&#243;lnienie"
  ]
  node [
    id 430
    label "sprzeciw"
  ]
  node [
    id 431
    label "sekretnie"
  ]
  node [
    id 432
    label "kryjomy"
  ]
  node [
    id 433
    label "potajemny"
  ]
  node [
    id 434
    label "kryjomo"
  ]
  node [
    id 435
    label "sekretny"
  ]
  node [
    id 436
    label "niejawnie"
  ]
  node [
    id 437
    label "skryty"
  ]
  node [
    id 438
    label "planowa&#263;"
  ]
  node [
    id 439
    label "dostosowywa&#263;"
  ]
  node [
    id 440
    label "treat"
  ]
  node [
    id 441
    label "pozyskiwa&#263;"
  ]
  node [
    id 442
    label "ensnare"
  ]
  node [
    id 443
    label "skupia&#263;"
  ]
  node [
    id 444
    label "create"
  ]
  node [
    id 445
    label "przygotowywa&#263;"
  ]
  node [
    id 446
    label "tworzy&#263;"
  ]
  node [
    id 447
    label "standard"
  ]
  node [
    id 448
    label "wprowadza&#263;"
  ]
  node [
    id 449
    label "rynek"
  ]
  node [
    id 450
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 451
    label "robi&#263;"
  ]
  node [
    id 452
    label "wprawia&#263;"
  ]
  node [
    id 453
    label "zaczyna&#263;"
  ]
  node [
    id 454
    label "wpisywa&#263;"
  ]
  node [
    id 455
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 456
    label "wchodzi&#263;"
  ]
  node [
    id 457
    label "take"
  ]
  node [
    id 458
    label "zapoznawa&#263;"
  ]
  node [
    id 459
    label "powodowa&#263;"
  ]
  node [
    id 460
    label "inflict"
  ]
  node [
    id 461
    label "umieszcza&#263;"
  ]
  node [
    id 462
    label "schodzi&#263;"
  ]
  node [
    id 463
    label "induct"
  ]
  node [
    id 464
    label "begin"
  ]
  node [
    id 465
    label "doprowadza&#263;"
  ]
  node [
    id 466
    label "ognisko"
  ]
  node [
    id 467
    label "huddle"
  ]
  node [
    id 468
    label "zbiera&#263;"
  ]
  node [
    id 469
    label "masowa&#263;"
  ]
  node [
    id 470
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 471
    label "uzyskiwa&#263;"
  ]
  node [
    id 472
    label "wytwarza&#263;"
  ]
  node [
    id 473
    label "tease"
  ]
  node [
    id 474
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 475
    label "pope&#322;nia&#263;"
  ]
  node [
    id 476
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 477
    label "get"
  ]
  node [
    id 478
    label "consist"
  ]
  node [
    id 479
    label "stanowi&#263;"
  ]
  node [
    id 480
    label "raise"
  ]
  node [
    id 481
    label "sposobi&#263;"
  ]
  node [
    id 482
    label "usposabia&#263;"
  ]
  node [
    id 483
    label "train"
  ]
  node [
    id 484
    label "arrange"
  ]
  node [
    id 485
    label "szkoli&#263;"
  ]
  node [
    id 486
    label "wykonywa&#263;"
  ]
  node [
    id 487
    label "pryczy&#263;"
  ]
  node [
    id 488
    label "mean"
  ]
  node [
    id 489
    label "lot_&#347;lizgowy"
  ]
  node [
    id 490
    label "organize"
  ]
  node [
    id 491
    label "project"
  ]
  node [
    id 492
    label "my&#347;le&#263;"
  ]
  node [
    id 493
    label "volunteer"
  ]
  node [
    id 494
    label "opracowywa&#263;"
  ]
  node [
    id 495
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 496
    label "zmienia&#263;"
  ]
  node [
    id 497
    label "ordinariness"
  ]
  node [
    id 498
    label "instytucja"
  ]
  node [
    id 499
    label "zorganizowa&#263;"
  ]
  node [
    id 500
    label "taniec_towarzyski"
  ]
  node [
    id 501
    label "organizowanie"
  ]
  node [
    id 502
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 503
    label "criterion"
  ]
  node [
    id 504
    label "zorganizowanie"
  ]
  node [
    id 505
    label "cover"
  ]
  node [
    id 506
    label "patriotycznie"
  ]
  node [
    id 507
    label "bogoojczy&#378;niany"
  ]
  node [
    id 508
    label "wierny"
  ]
  node [
    id 509
    label "patriotic"
  ]
  node [
    id 510
    label "wiernie"
  ]
  node [
    id 511
    label "sta&#322;y"
  ]
  node [
    id 512
    label "lojalny"
  ]
  node [
    id 513
    label "dok&#322;adny"
  ]
  node [
    id 514
    label "wyznawca"
  ]
  node [
    id 515
    label "pozorny"
  ]
  node [
    id 516
    label "nadmierny"
  ]
  node [
    id 517
    label "aid"
  ]
  node [
    id 518
    label "u&#322;atwia&#263;"
  ]
  node [
    id 519
    label "concur"
  ]
  node [
    id 520
    label "sprzyja&#263;"
  ]
  node [
    id 521
    label "skutkowa&#263;"
  ]
  node [
    id 522
    label "Warszawa"
  ]
  node [
    id 523
    label "back"
  ]
  node [
    id 524
    label "&#322;atwi&#263;"
  ]
  node [
    id 525
    label "ease"
  ]
  node [
    id 526
    label "czu&#263;"
  ]
  node [
    id 527
    label "chowa&#263;"
  ]
  node [
    id 528
    label "sprawdza&#263;_si&#281;"
  ]
  node [
    id 529
    label "poci&#261;ga&#263;"
  ]
  node [
    id 530
    label "przynosi&#263;"
  ]
  node [
    id 531
    label "i&#347;&#263;_w_parze"
  ]
  node [
    id 532
    label "work"
  ]
  node [
    id 533
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 534
    label "czyni&#263;"
  ]
  node [
    id 535
    label "give"
  ]
  node [
    id 536
    label "stylizowa&#263;"
  ]
  node [
    id 537
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 538
    label "falowa&#263;"
  ]
  node [
    id 539
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 540
    label "peddle"
  ]
  node [
    id 541
    label "wydala&#263;"
  ]
  node [
    id 542
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 543
    label "tentegowa&#263;"
  ]
  node [
    id 544
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 545
    label "urz&#261;dza&#263;"
  ]
  node [
    id 546
    label "oszukiwa&#263;"
  ]
  node [
    id 547
    label "ukazywa&#263;"
  ]
  node [
    id 548
    label "przerabia&#263;"
  ]
  node [
    id 549
    label "act"
  ]
  node [
    id 550
    label "post&#281;powa&#263;"
  ]
  node [
    id 551
    label "warszawa"
  ]
  node [
    id 552
    label "Powi&#347;le"
  ]
  node [
    id 553
    label "Wawa"
  ]
  node [
    id 554
    label "syreni_gr&#243;d"
  ]
  node [
    id 555
    label "Wawer"
  ]
  node [
    id 556
    label "W&#322;ochy"
  ]
  node [
    id 557
    label "Ursyn&#243;w"
  ]
  node [
    id 558
    label "Bielany"
  ]
  node [
    id 559
    label "Weso&#322;a"
  ]
  node [
    id 560
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 561
    label "Targ&#243;wek"
  ]
  node [
    id 562
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 563
    label "Muran&#243;w"
  ]
  node [
    id 564
    label "Warsiawa"
  ]
  node [
    id 565
    label "Ursus"
  ]
  node [
    id 566
    label "Ochota"
  ]
  node [
    id 567
    label "Marymont"
  ]
  node [
    id 568
    label "Ujazd&#243;w"
  ]
  node [
    id 569
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 570
    label "Solec"
  ]
  node [
    id 571
    label "Bemowo"
  ]
  node [
    id 572
    label "Mokot&#243;w"
  ]
  node [
    id 573
    label "Wilan&#243;w"
  ]
  node [
    id 574
    label "warszawka"
  ]
  node [
    id 575
    label "varsaviana"
  ]
  node [
    id 576
    label "Wola"
  ]
  node [
    id 577
    label "Rembert&#243;w"
  ]
  node [
    id 578
    label "Praga"
  ]
  node [
    id 579
    label "&#379;oliborz"
  ]
  node [
    id 580
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 581
    label "motywowa&#263;"
  ]
  node [
    id 582
    label "poradzi&#263;_sobie"
  ]
  node [
    id 583
    label "visualize"
  ]
  node [
    id 584
    label "dozna&#263;"
  ]
  node [
    id 585
    label "wytrzyma&#263;"
  ]
  node [
    id 586
    label "przej&#347;&#263;"
  ]
  node [
    id 587
    label "see"
  ]
  node [
    id 588
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 589
    label "feel"
  ]
  node [
    id 590
    label "zmusi&#263;"
  ]
  node [
    id 591
    label "pozosta&#263;"
  ]
  node [
    id 592
    label "proceed"
  ]
  node [
    id 593
    label "ustawa"
  ]
  node [
    id 594
    label "podlec"
  ]
  node [
    id 595
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 596
    label "min&#261;&#263;"
  ]
  node [
    id 597
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 598
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 599
    label "zaliczy&#263;"
  ]
  node [
    id 600
    label "zmieni&#263;"
  ]
  node [
    id 601
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 602
    label "przeby&#263;"
  ]
  node [
    id 603
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 604
    label "die"
  ]
  node [
    id 605
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 606
    label "zacz&#261;&#263;"
  ]
  node [
    id 607
    label "happen"
  ]
  node [
    id 608
    label "pass"
  ]
  node [
    id 609
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 610
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 611
    label "beat"
  ]
  node [
    id 612
    label "mienie"
  ]
  node [
    id 613
    label "absorb"
  ]
  node [
    id 614
    label "przerobi&#263;"
  ]
  node [
    id 615
    label "pique"
  ]
  node [
    id 616
    label "przesta&#263;"
  ]
  node [
    id 617
    label "nietrwa&#322;y"
  ]
  node [
    id 618
    label "mizerny"
  ]
  node [
    id 619
    label "marnie"
  ]
  node [
    id 620
    label "delikatny"
  ]
  node [
    id 621
    label "po&#347;ledni"
  ]
  node [
    id 622
    label "niezdrowy"
  ]
  node [
    id 623
    label "nieumiej&#281;tny"
  ]
  node [
    id 624
    label "s&#322;abo"
  ]
  node [
    id 625
    label "nieznaczny"
  ]
  node [
    id 626
    label "lura"
  ]
  node [
    id 627
    label "nieudany"
  ]
  node [
    id 628
    label "s&#322;abowity"
  ]
  node [
    id 629
    label "zawodny"
  ]
  node [
    id 630
    label "&#322;agodny"
  ]
  node [
    id 631
    label "md&#322;y"
  ]
  node [
    id 632
    label "niedoskona&#322;y"
  ]
  node [
    id 633
    label "przemijaj&#261;cy"
  ]
  node [
    id 634
    label "niemocny"
  ]
  node [
    id 635
    label "niefajny"
  ]
  node [
    id 636
    label "kiepsko"
  ]
  node [
    id 637
    label "pieski"
  ]
  node [
    id 638
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 639
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 640
    label "niekorzystny"
  ]
  node [
    id 641
    label "z&#322;oszczenie"
  ]
  node [
    id 642
    label "sierdzisty"
  ]
  node [
    id 643
    label "niegrzeczny"
  ]
  node [
    id 644
    label "zez&#322;oszczenie"
  ]
  node [
    id 645
    label "zdenerwowany"
  ]
  node [
    id 646
    label "negatywny"
  ]
  node [
    id 647
    label "rozgniewanie"
  ]
  node [
    id 648
    label "gniewanie"
  ]
  node [
    id 649
    label "niemoralny"
  ]
  node [
    id 650
    label "&#378;le"
  ]
  node [
    id 651
    label "niepomy&#347;lny"
  ]
  node [
    id 652
    label "syf"
  ]
  node [
    id 653
    label "domek_z_kart"
  ]
  node [
    id 654
    label "zmienny"
  ]
  node [
    id 655
    label "nietrwale"
  ]
  node [
    id 656
    label "przemijaj&#261;co"
  ]
  node [
    id 657
    label "zawodnie"
  ]
  node [
    id 658
    label "niepewny"
  ]
  node [
    id 659
    label "chorowicie"
  ]
  node [
    id 660
    label "niezdrowo"
  ]
  node [
    id 661
    label "dziwaczny"
  ]
  node [
    id 662
    label "chorobliwy"
  ]
  node [
    id 663
    label "szkodliwy"
  ]
  node [
    id 664
    label "chory"
  ]
  node [
    id 665
    label "nieudanie"
  ]
  node [
    id 666
    label "nieciekawy"
  ]
  node [
    id 667
    label "niemi&#322;y"
  ]
  node [
    id 668
    label "nieprzyjemny"
  ]
  node [
    id 669
    label "niefajnie"
  ]
  node [
    id 670
    label "nieznacznie"
  ]
  node [
    id 671
    label "drobnostkowy"
  ]
  node [
    id 672
    label "niewa&#380;ny"
  ]
  node [
    id 673
    label "ma&#322;y"
  ]
  node [
    id 674
    label "nieumiej&#281;tnie"
  ]
  node [
    id 675
    label "niedoskonale"
  ]
  node [
    id 676
    label "ma&#322;o"
  ]
  node [
    id 677
    label "kiepski"
  ]
  node [
    id 678
    label "marny"
  ]
  node [
    id 679
    label "nadaremnie"
  ]
  node [
    id 680
    label "przeci&#281;tny"
  ]
  node [
    id 681
    label "po&#347;lednio"
  ]
  node [
    id 682
    label "nieswojo"
  ]
  node [
    id 683
    label "feebly"
  ]
  node [
    id 684
    label "w&#261;t&#322;y"
  ]
  node [
    id 685
    label "delikatnienie"
  ]
  node [
    id 686
    label "subtelny"
  ]
  node [
    id 687
    label "spokojny"
  ]
  node [
    id 688
    label "mi&#322;y"
  ]
  node [
    id 689
    label "prosty"
  ]
  node [
    id 690
    label "lekki"
  ]
  node [
    id 691
    label "zdelikatnienie"
  ]
  node [
    id 692
    label "&#322;agodnie"
  ]
  node [
    id 693
    label "nieszkodliwy"
  ]
  node [
    id 694
    label "delikatnie"
  ]
  node [
    id 695
    label "letki"
  ]
  node [
    id 696
    label "typowy"
  ]
  node [
    id 697
    label "zwyczajny"
  ]
  node [
    id 698
    label "harmonijny"
  ]
  node [
    id 699
    label "niesurowy"
  ]
  node [
    id 700
    label "przyjemny"
  ]
  node [
    id 701
    label "nieostry"
  ]
  node [
    id 702
    label "biedny"
  ]
  node [
    id 703
    label "blady"
  ]
  node [
    id 704
    label "sm&#281;tny"
  ]
  node [
    id 705
    label "mizernie"
  ]
  node [
    id 706
    label "n&#281;dznie"
  ]
  node [
    id 707
    label "szczyny"
  ]
  node [
    id 708
    label "nap&#243;j"
  ]
  node [
    id 709
    label "wydelikacanie"
  ]
  node [
    id 710
    label "k&#322;opotliwy"
  ]
  node [
    id 711
    label "dra&#380;liwy"
  ]
  node [
    id 712
    label "ostro&#380;ny"
  ]
  node [
    id 713
    label "wra&#380;liwy"
  ]
  node [
    id 714
    label "wydelikacenie"
  ]
  node [
    id 715
    label "taktowny"
  ]
  node [
    id 716
    label "choro"
  ]
  node [
    id 717
    label "przykry"
  ]
  node [
    id 718
    label "md&#322;o"
  ]
  node [
    id 719
    label "ckliwy"
  ]
  node [
    id 720
    label "nik&#322;y"
  ]
  node [
    id 721
    label "nijaki"
  ]
  node [
    id 722
    label "pierdel"
  ]
  node [
    id 723
    label "&#321;ubianka"
  ]
  node [
    id 724
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 725
    label "kiciarz"
  ]
  node [
    id 726
    label "ciupa"
  ]
  node [
    id 727
    label "reedukator"
  ]
  node [
    id 728
    label "pasiak"
  ]
  node [
    id 729
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 730
    label "Butyrki"
  ]
  node [
    id 731
    label "miejsce_odosobnienia"
  ]
  node [
    id 732
    label "ludzko&#347;&#263;"
  ]
  node [
    id 733
    label "asymilowanie"
  ]
  node [
    id 734
    label "wapniak"
  ]
  node [
    id 735
    label "asymilowa&#263;"
  ]
  node [
    id 736
    label "os&#322;abia&#263;"
  ]
  node [
    id 737
    label "posta&#263;"
  ]
  node [
    id 738
    label "hominid"
  ]
  node [
    id 739
    label "podw&#322;adny"
  ]
  node [
    id 740
    label "os&#322;abianie"
  ]
  node [
    id 741
    label "g&#322;owa"
  ]
  node [
    id 742
    label "figura"
  ]
  node [
    id 743
    label "portrecista"
  ]
  node [
    id 744
    label "dwun&#243;g"
  ]
  node [
    id 745
    label "profanum"
  ]
  node [
    id 746
    label "mikrokosmos"
  ]
  node [
    id 747
    label "nasada"
  ]
  node [
    id 748
    label "duch"
  ]
  node [
    id 749
    label "antropochoria"
  ]
  node [
    id 750
    label "osoba"
  ]
  node [
    id 751
    label "senior"
  ]
  node [
    id 752
    label "oddzia&#322;ywanie"
  ]
  node [
    id 753
    label "Adam"
  ]
  node [
    id 754
    label "homo_sapiens"
  ]
  node [
    id 755
    label "polifag"
  ]
  node [
    id 756
    label "alfabet_wi&#281;zienny"
  ]
  node [
    id 757
    label "odsiadka"
  ]
  node [
    id 758
    label "wi&#281;zienie"
  ]
  node [
    id 759
    label "tkanina"
  ]
  node [
    id 760
    label "pasiasty"
  ]
  node [
    id 761
    label "uniform"
  ]
  node [
    id 762
    label "nauczyciel"
  ]
  node [
    id 763
    label "pracownik"
  ]
  node [
    id 764
    label "dom_poprawczy"
  ]
  node [
    id 765
    label "Monar"
  ]
  node [
    id 766
    label "rehabilitant"
  ]
  node [
    id 767
    label "francuski"
  ]
  node [
    id 768
    label "czerwony"
  ]
  node [
    id 769
    label "krzy&#380;"
  ]
  node [
    id 770
    label "Wac&#322;awa"
  ]
  node [
    id 771
    label "Milkego"
  ]
  node [
    id 772
    label "zwi&#261;zek"
  ]
  node [
    id 773
    label "m&#322;odzie&#380;"
  ]
  node [
    id 774
    label "&#8222;"
  ]
  node [
    id 775
    label "Grunwald"
  ]
  node [
    id 776
    label "&#8221;"
  ]
  node [
    id 777
    label "liceum"
  ]
  node [
    id 778
    label "og&#243;lnokszta&#322;c&#261;cy"
  ]
  node [
    id 779
    label "on"
  ]
  node [
    id 780
    label "marsza&#322;ek"
  ]
  node [
    id 781
    label "Stanis&#322;awa"
  ]
  node [
    id 782
    label "Ma&#322;achowski"
  ]
  node [
    id 783
    label "harcerski"
  ]
  node [
    id 784
    label "dru&#380;yna"
  ]
  node [
    id 785
    label "artystyczny"
  ]
  node [
    id 786
    label "zesp&#243;&#322;"
  ]
  node [
    id 787
    label "pie&#347;&#324;"
  ]
  node [
    id 788
    label "i"
  ]
  node [
    id 789
    label "taniec"
  ]
  node [
    id 790
    label "dziecko"
  ]
  node [
    id 791
    label "p&#322;ocki"
  ]
  node [
    id 792
    label "&#347;wiatowy"
  ]
  node [
    id 793
    label "festiwal"
  ]
  node [
    id 794
    label "student"
  ]
  node [
    id 795
    label "Milke"
  ]
  node [
    id 796
    label "wielki"
  ]
  node [
    id 797
    label "order"
  ]
  node [
    id 798
    label "odrodzi&#263;"
  ]
  node [
    id 799
    label "polski"
  ]
  node [
    id 800
    label "zas&#322;u&#380;y&#263;"
  ]
  node [
    id 801
    label "dla"
  ]
  node [
    id 802
    label "kultura"
  ]
  node [
    id 803
    label "za"
  ]
  node [
    id 804
    label "zas&#322;uga"
  ]
  node [
    id 805
    label "o&#347;wiata"
  ]
  node [
    id 806
    label "wychowa&#263;"
  ]
  node [
    id 807
    label "komandorski"
  ]
  node [
    id 808
    label "zeszyt"
  ]
  node [
    id 809
    label "gwiazda"
  ]
  node [
    id 810
    label "obronno&#347;&#263;"
  ]
  node [
    id 811
    label "kraj"
  ]
  node [
    id 812
    label "gloria"
  ]
  node [
    id 813
    label "Artis"
  ]
  node [
    id 814
    label "medal"
  ]
  node [
    id 815
    label "komisja"
  ]
  node [
    id 816
    label "edukacja"
  ]
  node [
    id 817
    label "narodowy"
  ]
  node [
    id 818
    label "z&#322;oty"
  ]
  node [
    id 819
    label "towarzystwo"
  ]
  node [
    id 820
    label "przyjaciel"
  ]
  node [
    id 821
    label "u&#347;miech"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 381
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 292
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 767
    target 768
  ]
  edge [
    source 767
    target 769
  ]
  edge [
    source 768
    target 769
  ]
  edge [
    source 769
    target 796
  ]
  edge [
    source 769
    target 797
  ]
  edge [
    source 769
    target 798
  ]
  edge [
    source 769
    target 799
  ]
  edge [
    source 769
    target 807
  ]
  edge [
    source 769
    target 808
  ]
  edge [
    source 769
    target 809
  ]
  edge [
    source 769
    target 818
  ]
  edge [
    source 769
    target 804
  ]
  edge [
    source 770
    target 771
  ]
  edge [
    source 770
    target 795
  ]
  edge [
    source 772
    target 773
  ]
  edge [
    source 772
    target 774
  ]
  edge [
    source 772
    target 775
  ]
  edge [
    source 772
    target 776
  ]
  edge [
    source 773
    target 774
  ]
  edge [
    source 773
    target 775
  ]
  edge [
    source 773
    target 776
  ]
  edge [
    source 773
    target 792
  ]
  edge [
    source 773
    target 793
  ]
  edge [
    source 773
    target 788
  ]
  edge [
    source 773
    target 794
  ]
  edge [
    source 774
    target 775
  ]
  edge [
    source 774
    target 776
  ]
  edge [
    source 774
    target 783
  ]
  edge [
    source 774
    target 786
  ]
  edge [
    source 774
    target 787
  ]
  edge [
    source 774
    target 788
  ]
  edge [
    source 774
    target 789
  ]
  edge [
    source 774
    target 790
  ]
  edge [
    source 774
    target 791
  ]
  edge [
    source 775
    target 776
  ]
  edge [
    source 776
    target 783
  ]
  edge [
    source 776
    target 786
  ]
  edge [
    source 776
    target 787
  ]
  edge [
    source 776
    target 788
  ]
  edge [
    source 776
    target 789
  ]
  edge [
    source 776
    target 790
  ]
  edge [
    source 776
    target 791
  ]
  edge [
    source 777
    target 778
  ]
  edge [
    source 777
    target 779
  ]
  edge [
    source 777
    target 780
  ]
  edge [
    source 777
    target 781
  ]
  edge [
    source 777
    target 782
  ]
  edge [
    source 778
    target 779
  ]
  edge [
    source 778
    target 780
  ]
  edge [
    source 778
    target 781
  ]
  edge [
    source 778
    target 782
  ]
  edge [
    source 779
    target 780
  ]
  edge [
    source 779
    target 781
  ]
  edge [
    source 779
    target 782
  ]
  edge [
    source 780
    target 781
  ]
  edge [
    source 780
    target 782
  ]
  edge [
    source 781
    target 782
  ]
  edge [
    source 783
    target 784
  ]
  edge [
    source 783
    target 785
  ]
  edge [
    source 783
    target 786
  ]
  edge [
    source 783
    target 787
  ]
  edge [
    source 783
    target 788
  ]
  edge [
    source 783
    target 789
  ]
  edge [
    source 783
    target 790
  ]
  edge [
    source 783
    target 791
  ]
  edge [
    source 784
    target 785
  ]
  edge [
    source 786
    target 787
  ]
  edge [
    source 786
    target 788
  ]
  edge [
    source 786
    target 789
  ]
  edge [
    source 786
    target 790
  ]
  edge [
    source 786
    target 791
  ]
  edge [
    source 787
    target 788
  ]
  edge [
    source 787
    target 789
  ]
  edge [
    source 787
    target 790
  ]
  edge [
    source 787
    target 791
  ]
  edge [
    source 788
    target 789
  ]
  edge [
    source 788
    target 790
  ]
  edge [
    source 788
    target 791
  ]
  edge [
    source 788
    target 792
  ]
  edge [
    source 788
    target 793
  ]
  edge [
    source 788
    target 794
  ]
  edge [
    source 788
    target 803
  ]
  edge [
    source 788
    target 804
  ]
  edge [
    source 788
    target 801
  ]
  edge [
    source 788
    target 805
  ]
  edge [
    source 788
    target 806
  ]
  edge [
    source 789
    target 790
  ]
  edge [
    source 789
    target 791
  ]
  edge [
    source 790
    target 791
  ]
  edge [
    source 790
    target 819
  ]
  edge [
    source 790
    target 820
  ]
  edge [
    source 792
    target 793
  ]
  edge [
    source 792
    target 794
  ]
  edge [
    source 793
    target 794
  ]
  edge [
    source 796
    target 797
  ]
  edge [
    source 796
    target 798
  ]
  edge [
    source 796
    target 799
  ]
  edge [
    source 797
    target 798
  ]
  edge [
    source 797
    target 799
  ]
  edge [
    source 797
    target 807
  ]
  edge [
    source 797
    target 808
  ]
  edge [
    source 797
    target 809
  ]
  edge [
    source 797
    target 821
  ]
  edge [
    source 798
    target 799
  ]
  edge [
    source 798
    target 807
  ]
  edge [
    source 798
    target 808
  ]
  edge [
    source 798
    target 809
  ]
  edge [
    source 799
    target 800
  ]
  edge [
    source 799
    target 801
  ]
  edge [
    source 799
    target 802
  ]
  edge [
    source 799
    target 807
  ]
  edge [
    source 799
    target 808
  ]
  edge [
    source 799
    target 809
  ]
  edge [
    source 800
    target 801
  ]
  edge [
    source 800
    target 802
  ]
  edge [
    source 801
    target 802
  ]
  edge [
    source 801
    target 803
  ]
  edge [
    source 801
    target 804
  ]
  edge [
    source 801
    target 805
  ]
  edge [
    source 801
    target 806
  ]
  edge [
    source 801
    target 810
  ]
  edge [
    source 801
    target 811
  ]
  edge [
    source 803
    target 804
  ]
  edge [
    source 803
    target 805
  ]
  edge [
    source 803
    target 806
  ]
  edge [
    source 803
    target 810
  ]
  edge [
    source 803
    target 811
  ]
  edge [
    source 804
    target 805
  ]
  edge [
    source 804
    target 806
  ]
  edge [
    source 804
    target 810
  ]
  edge [
    source 804
    target 811
  ]
  edge [
    source 804
    target 818
  ]
  edge [
    source 805
    target 806
  ]
  edge [
    source 807
    target 808
  ]
  edge [
    source 807
    target 809
  ]
  edge [
    source 808
    target 809
  ]
  edge [
    source 810
    target 811
  ]
  edge [
    source 812
    target 813
  ]
  edge [
    source 814
    target 815
  ]
  edge [
    source 814
    target 816
  ]
  edge [
    source 814
    target 817
  ]
  edge [
    source 815
    target 816
  ]
  edge [
    source 815
    target 817
  ]
  edge [
    source 816
    target 817
  ]
  edge [
    source 819
    target 820
  ]
]
