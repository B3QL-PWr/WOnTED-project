graph [
  node [
    id 0
    label "morderstwo"
    origin "text"
  ]
  node [
    id 1
    label "arlis"
    origin "text"
  ]
  node [
    id 2
    label "perry"
    origin "text"
  ]
  node [
    id 3
    label "przest&#281;pstwo"
  ]
  node [
    id 4
    label "zabicie"
  ]
  node [
    id 5
    label "brudny"
  ]
  node [
    id 6
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 7
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 8
    label "crime"
  ]
  node [
    id 9
    label "sprawstwo"
  ]
  node [
    id 10
    label "&#347;mier&#263;"
  ]
  node [
    id 11
    label "destruction"
  ]
  node [
    id 12
    label "zabrzmienie"
  ]
  node [
    id 13
    label "skrzywdzenie"
  ]
  node [
    id 14
    label "pozabijanie"
  ]
  node [
    id 15
    label "zniszczenie"
  ]
  node [
    id 16
    label "zaszkodzenie"
  ]
  node [
    id 17
    label "usuni&#281;cie"
  ]
  node [
    id 18
    label "spowodowanie"
  ]
  node [
    id 19
    label "killing"
  ]
  node [
    id 20
    label "zdarzenie_si&#281;"
  ]
  node [
    id 21
    label "czyn"
  ]
  node [
    id 22
    label "umarcie"
  ]
  node [
    id 23
    label "granie"
  ]
  node [
    id 24
    label "zamkni&#281;cie"
  ]
  node [
    id 25
    label "compaction"
  ]
  node [
    id 26
    label "Arlis"
  ]
  node [
    id 27
    label "Perry"
  ]
  node [
    id 28
    label "hej"
  ]
  node [
    id 29
    label "Mirka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
]
