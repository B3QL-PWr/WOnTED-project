graph [
  node [
    id 0
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 1
    label "wprawdzie"
    origin "text"
  ]
  node [
    id 2
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zawsze"
    origin "text"
  ]
  node [
    id 4
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 5
    label "dwa"
    origin "text"
  ]
  node [
    id 6
    label "kraj"
    origin "text"
  ]
  node [
    id 7
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 9
    label "problem"
    origin "text"
  ]
  node [
    id 10
    label "niepor&#243;wnywalny"
    origin "text"
  ]
  node [
    id 11
    label "element"
    origin "text"
  ]
  node [
    id 12
    label "pewnie"
    origin "text"
  ]
  node [
    id 13
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 14
    label "siebie"
    origin "text"
  ]
  node [
    id 15
    label "dobrze"
    origin "text"
  ]
  node [
    id 16
    label "albo"
    origin "text"
  ]
  node [
    id 17
    label "&#378;le"
    origin "text"
  ]
  node [
    id 18
    label "free"
  ]
  node [
    id 19
    label "discover"
  ]
  node [
    id 20
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 21
    label "wydoby&#263;"
  ]
  node [
    id 22
    label "okre&#347;li&#263;"
  ]
  node [
    id 23
    label "poda&#263;"
  ]
  node [
    id 24
    label "express"
  ]
  node [
    id 25
    label "wyrazi&#263;"
  ]
  node [
    id 26
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 27
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 28
    label "rzekn&#261;&#263;"
  ]
  node [
    id 29
    label "unwrap"
  ]
  node [
    id 30
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 31
    label "convey"
  ]
  node [
    id 32
    label "tenis"
  ]
  node [
    id 33
    label "supply"
  ]
  node [
    id 34
    label "da&#263;"
  ]
  node [
    id 35
    label "ustawi&#263;"
  ]
  node [
    id 36
    label "siatk&#243;wka"
  ]
  node [
    id 37
    label "give"
  ]
  node [
    id 38
    label "zagra&#263;"
  ]
  node [
    id 39
    label "jedzenie"
  ]
  node [
    id 40
    label "poinformowa&#263;"
  ]
  node [
    id 41
    label "introduce"
  ]
  node [
    id 42
    label "nafaszerowa&#263;"
  ]
  node [
    id 43
    label "zaserwowa&#263;"
  ]
  node [
    id 44
    label "draw"
  ]
  node [
    id 45
    label "doby&#263;"
  ]
  node [
    id 46
    label "g&#243;rnictwo"
  ]
  node [
    id 47
    label "wyeksploatowa&#263;"
  ]
  node [
    id 48
    label "extract"
  ]
  node [
    id 49
    label "obtain"
  ]
  node [
    id 50
    label "wyj&#261;&#263;"
  ]
  node [
    id 51
    label "ocali&#263;"
  ]
  node [
    id 52
    label "uzyska&#263;"
  ]
  node [
    id 53
    label "wyda&#263;"
  ]
  node [
    id 54
    label "wydosta&#263;"
  ]
  node [
    id 55
    label "uwydatni&#263;"
  ]
  node [
    id 56
    label "distill"
  ]
  node [
    id 57
    label "raise"
  ]
  node [
    id 58
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 59
    label "testify"
  ]
  node [
    id 60
    label "zakomunikowa&#263;"
  ]
  node [
    id 61
    label "oznaczy&#263;"
  ]
  node [
    id 62
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 63
    label "vent"
  ]
  node [
    id 64
    label "zdecydowa&#263;"
  ]
  node [
    id 65
    label "zrobi&#263;"
  ]
  node [
    id 66
    label "spowodowa&#263;"
  ]
  node [
    id 67
    label "situate"
  ]
  node [
    id 68
    label "nominate"
  ]
  node [
    id 69
    label "cz&#281;sto"
  ]
  node [
    id 70
    label "ci&#261;gle"
  ]
  node [
    id 71
    label "zaw&#380;dy"
  ]
  node [
    id 72
    label "na_zawsze"
  ]
  node [
    id 73
    label "cz&#281;sty"
  ]
  node [
    id 74
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 75
    label "stale"
  ]
  node [
    id 76
    label "ci&#261;g&#322;y"
  ]
  node [
    id 77
    label "nieprzerwanie"
  ]
  node [
    id 78
    label "figura_stylistyczna"
  ]
  node [
    id 79
    label "simile"
  ]
  node [
    id 80
    label "comparison"
  ]
  node [
    id 81
    label "zanalizowanie"
  ]
  node [
    id 82
    label "zestawienie"
  ]
  node [
    id 83
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 84
    label "rozwa&#380;enie"
  ]
  node [
    id 85
    label "udowodnienie"
  ]
  node [
    id 86
    label "przebadanie"
  ]
  node [
    id 87
    label "sumariusz"
  ]
  node [
    id 88
    label "ustawienie"
  ]
  node [
    id 89
    label "z&#322;amanie"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "kompozycja"
  ]
  node [
    id 92
    label "strata"
  ]
  node [
    id 93
    label "composition"
  ]
  node [
    id 94
    label "book"
  ]
  node [
    id 95
    label "informacja"
  ]
  node [
    id 96
    label "stock"
  ]
  node [
    id 97
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 98
    label "catalog"
  ]
  node [
    id 99
    label "z&#322;o&#380;enie"
  ]
  node [
    id 100
    label "sprawozdanie_finansowe"
  ]
  node [
    id 101
    label "figurowa&#263;"
  ]
  node [
    id 102
    label "z&#322;&#261;czenie"
  ]
  node [
    id 103
    label "count"
  ]
  node [
    id 104
    label "wyra&#380;enie"
  ]
  node [
    id 105
    label "wyliczanka"
  ]
  node [
    id 106
    label "set"
  ]
  node [
    id 107
    label "analiza"
  ]
  node [
    id 108
    label "deficyt"
  ]
  node [
    id 109
    label "obrot&#243;wka"
  ]
  node [
    id 110
    label "przedstawienie"
  ]
  node [
    id 111
    label "pozycja"
  ]
  node [
    id 112
    label "tekst"
  ]
  node [
    id 113
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 114
    label "discrimination"
  ]
  node [
    id 115
    label "diverseness"
  ]
  node [
    id 116
    label "eklektyk"
  ]
  node [
    id 117
    label "rozproszenie_si&#281;"
  ]
  node [
    id 118
    label "differentiation"
  ]
  node [
    id 119
    label "bogactwo"
  ]
  node [
    id 120
    label "cecha"
  ]
  node [
    id 121
    label "multikulturalizm"
  ]
  node [
    id 122
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 123
    label "rozdzielenie"
  ]
  node [
    id 124
    label "nadanie"
  ]
  node [
    id 125
    label "podzielenie"
  ]
  node [
    id 126
    label "zrobienie"
  ]
  node [
    id 127
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 128
    label "Katar"
  ]
  node [
    id 129
    label "Mazowsze"
  ]
  node [
    id 130
    label "Libia"
  ]
  node [
    id 131
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 132
    label "Gwatemala"
  ]
  node [
    id 133
    label "Anglia"
  ]
  node [
    id 134
    label "Amazonia"
  ]
  node [
    id 135
    label "Ekwador"
  ]
  node [
    id 136
    label "Afganistan"
  ]
  node [
    id 137
    label "Bordeaux"
  ]
  node [
    id 138
    label "Tad&#380;ykistan"
  ]
  node [
    id 139
    label "Bhutan"
  ]
  node [
    id 140
    label "Argentyna"
  ]
  node [
    id 141
    label "D&#380;ibuti"
  ]
  node [
    id 142
    label "Wenezuela"
  ]
  node [
    id 143
    label "Gabon"
  ]
  node [
    id 144
    label "Ukraina"
  ]
  node [
    id 145
    label "Naddniestrze"
  ]
  node [
    id 146
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 147
    label "Europa_Zachodnia"
  ]
  node [
    id 148
    label "Armagnac"
  ]
  node [
    id 149
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 150
    label "Rwanda"
  ]
  node [
    id 151
    label "Liechtenstein"
  ]
  node [
    id 152
    label "Amhara"
  ]
  node [
    id 153
    label "organizacja"
  ]
  node [
    id 154
    label "Sri_Lanka"
  ]
  node [
    id 155
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 156
    label "Zamojszczyzna"
  ]
  node [
    id 157
    label "Madagaskar"
  ]
  node [
    id 158
    label "Kongo"
  ]
  node [
    id 159
    label "Tonga"
  ]
  node [
    id 160
    label "Bangladesz"
  ]
  node [
    id 161
    label "Kanada"
  ]
  node [
    id 162
    label "Turkiestan"
  ]
  node [
    id 163
    label "Wehrlen"
  ]
  node [
    id 164
    label "Ma&#322;opolska"
  ]
  node [
    id 165
    label "Algieria"
  ]
  node [
    id 166
    label "Noworosja"
  ]
  node [
    id 167
    label "Uganda"
  ]
  node [
    id 168
    label "Surinam"
  ]
  node [
    id 169
    label "Sahara_Zachodnia"
  ]
  node [
    id 170
    label "Chile"
  ]
  node [
    id 171
    label "Lubelszczyzna"
  ]
  node [
    id 172
    label "W&#281;gry"
  ]
  node [
    id 173
    label "Mezoameryka"
  ]
  node [
    id 174
    label "Birma"
  ]
  node [
    id 175
    label "Ba&#322;kany"
  ]
  node [
    id 176
    label "Kurdystan"
  ]
  node [
    id 177
    label "Kazachstan"
  ]
  node [
    id 178
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 179
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 180
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 181
    label "Armenia"
  ]
  node [
    id 182
    label "Tuwalu"
  ]
  node [
    id 183
    label "Timor_Wschodni"
  ]
  node [
    id 184
    label "Baszkiria"
  ]
  node [
    id 185
    label "Szkocja"
  ]
  node [
    id 186
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 187
    label "Tonkin"
  ]
  node [
    id 188
    label "Maghreb"
  ]
  node [
    id 189
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 190
    label "Izrael"
  ]
  node [
    id 191
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 192
    label "Nadrenia"
  ]
  node [
    id 193
    label "Estonia"
  ]
  node [
    id 194
    label "Komory"
  ]
  node [
    id 195
    label "Podhale"
  ]
  node [
    id 196
    label "Wielkopolska"
  ]
  node [
    id 197
    label "Zabajkale"
  ]
  node [
    id 198
    label "Kamerun"
  ]
  node [
    id 199
    label "Haiti"
  ]
  node [
    id 200
    label "Belize"
  ]
  node [
    id 201
    label "Sierra_Leone"
  ]
  node [
    id 202
    label "Apulia"
  ]
  node [
    id 203
    label "Luksemburg"
  ]
  node [
    id 204
    label "brzeg"
  ]
  node [
    id 205
    label "USA"
  ]
  node [
    id 206
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 207
    label "Barbados"
  ]
  node [
    id 208
    label "San_Marino"
  ]
  node [
    id 209
    label "Bu&#322;garia"
  ]
  node [
    id 210
    label "Indonezja"
  ]
  node [
    id 211
    label "Wietnam"
  ]
  node [
    id 212
    label "Bojkowszczyzna"
  ]
  node [
    id 213
    label "Malawi"
  ]
  node [
    id 214
    label "Francja"
  ]
  node [
    id 215
    label "Zambia"
  ]
  node [
    id 216
    label "Kujawy"
  ]
  node [
    id 217
    label "Angola"
  ]
  node [
    id 218
    label "Liguria"
  ]
  node [
    id 219
    label "Grenada"
  ]
  node [
    id 220
    label "Pamir"
  ]
  node [
    id 221
    label "Nepal"
  ]
  node [
    id 222
    label "Panama"
  ]
  node [
    id 223
    label "Rumunia"
  ]
  node [
    id 224
    label "Indochiny"
  ]
  node [
    id 225
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 226
    label "Polinezja"
  ]
  node [
    id 227
    label "Kurpie"
  ]
  node [
    id 228
    label "Podlasie"
  ]
  node [
    id 229
    label "S&#261;decczyzna"
  ]
  node [
    id 230
    label "Umbria"
  ]
  node [
    id 231
    label "Czarnog&#243;ra"
  ]
  node [
    id 232
    label "Malediwy"
  ]
  node [
    id 233
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 234
    label "S&#322;owacja"
  ]
  node [
    id 235
    label "Karaiby"
  ]
  node [
    id 236
    label "Ukraina_Zachodnia"
  ]
  node [
    id 237
    label "Kielecczyzna"
  ]
  node [
    id 238
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 239
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 240
    label "Egipt"
  ]
  node [
    id 241
    label "Kalabria"
  ]
  node [
    id 242
    label "Kolumbia"
  ]
  node [
    id 243
    label "Mozambik"
  ]
  node [
    id 244
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 245
    label "Laos"
  ]
  node [
    id 246
    label "Burundi"
  ]
  node [
    id 247
    label "Suazi"
  ]
  node [
    id 248
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 249
    label "Czechy"
  ]
  node [
    id 250
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 251
    label "Wyspy_Marshalla"
  ]
  node [
    id 252
    label "Dominika"
  ]
  node [
    id 253
    label "Trynidad_i_Tobago"
  ]
  node [
    id 254
    label "Syria"
  ]
  node [
    id 255
    label "Palau"
  ]
  node [
    id 256
    label "Skandynawia"
  ]
  node [
    id 257
    label "Gwinea_Bissau"
  ]
  node [
    id 258
    label "Liberia"
  ]
  node [
    id 259
    label "Jamajka"
  ]
  node [
    id 260
    label "Zimbabwe"
  ]
  node [
    id 261
    label "Polska"
  ]
  node [
    id 262
    label "Bory_Tucholskie"
  ]
  node [
    id 263
    label "Huculszczyzna"
  ]
  node [
    id 264
    label "Tyrol"
  ]
  node [
    id 265
    label "Turyngia"
  ]
  node [
    id 266
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 267
    label "Dominikana"
  ]
  node [
    id 268
    label "Senegal"
  ]
  node [
    id 269
    label "Togo"
  ]
  node [
    id 270
    label "Gujana"
  ]
  node [
    id 271
    label "jednostka_administracyjna"
  ]
  node [
    id 272
    label "Albania"
  ]
  node [
    id 273
    label "Zair"
  ]
  node [
    id 274
    label "Meksyk"
  ]
  node [
    id 275
    label "Gruzja"
  ]
  node [
    id 276
    label "Macedonia"
  ]
  node [
    id 277
    label "Kambod&#380;a"
  ]
  node [
    id 278
    label "Chorwacja"
  ]
  node [
    id 279
    label "Monako"
  ]
  node [
    id 280
    label "Mauritius"
  ]
  node [
    id 281
    label "Gwinea"
  ]
  node [
    id 282
    label "Mali"
  ]
  node [
    id 283
    label "Nigeria"
  ]
  node [
    id 284
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 285
    label "Hercegowina"
  ]
  node [
    id 286
    label "Kostaryka"
  ]
  node [
    id 287
    label "Lotaryngia"
  ]
  node [
    id 288
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 289
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 290
    label "Hanower"
  ]
  node [
    id 291
    label "Paragwaj"
  ]
  node [
    id 292
    label "W&#322;ochy"
  ]
  node [
    id 293
    label "Seszele"
  ]
  node [
    id 294
    label "Wyspy_Salomona"
  ]
  node [
    id 295
    label "Hiszpania"
  ]
  node [
    id 296
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 297
    label "Walia"
  ]
  node [
    id 298
    label "Boliwia"
  ]
  node [
    id 299
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 300
    label "Opolskie"
  ]
  node [
    id 301
    label "Kirgistan"
  ]
  node [
    id 302
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 303
    label "Irlandia"
  ]
  node [
    id 304
    label "Kampania"
  ]
  node [
    id 305
    label "Czad"
  ]
  node [
    id 306
    label "Irak"
  ]
  node [
    id 307
    label "Lesoto"
  ]
  node [
    id 308
    label "Malta"
  ]
  node [
    id 309
    label "Andora"
  ]
  node [
    id 310
    label "Sand&#380;ak"
  ]
  node [
    id 311
    label "Chiny"
  ]
  node [
    id 312
    label "Filipiny"
  ]
  node [
    id 313
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 314
    label "Syjon"
  ]
  node [
    id 315
    label "Niemcy"
  ]
  node [
    id 316
    label "Kabylia"
  ]
  node [
    id 317
    label "Lombardia"
  ]
  node [
    id 318
    label "Warmia"
  ]
  node [
    id 319
    label "Nikaragua"
  ]
  node [
    id 320
    label "Pakistan"
  ]
  node [
    id 321
    label "Brazylia"
  ]
  node [
    id 322
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 323
    label "Kaszmir"
  ]
  node [
    id 324
    label "Maroko"
  ]
  node [
    id 325
    label "Portugalia"
  ]
  node [
    id 326
    label "Niger"
  ]
  node [
    id 327
    label "Kenia"
  ]
  node [
    id 328
    label "Botswana"
  ]
  node [
    id 329
    label "Fid&#380;i"
  ]
  node [
    id 330
    label "Tunezja"
  ]
  node [
    id 331
    label "Australia"
  ]
  node [
    id 332
    label "Tajlandia"
  ]
  node [
    id 333
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 334
    label "&#321;&#243;dzkie"
  ]
  node [
    id 335
    label "Kaukaz"
  ]
  node [
    id 336
    label "Burkina_Faso"
  ]
  node [
    id 337
    label "Tanzania"
  ]
  node [
    id 338
    label "Benin"
  ]
  node [
    id 339
    label "Europa_Wschodnia"
  ]
  node [
    id 340
    label "interior"
  ]
  node [
    id 341
    label "Indie"
  ]
  node [
    id 342
    label "&#321;otwa"
  ]
  node [
    id 343
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 344
    label "Biskupizna"
  ]
  node [
    id 345
    label "Kiribati"
  ]
  node [
    id 346
    label "Antigua_i_Barbuda"
  ]
  node [
    id 347
    label "Rodezja"
  ]
  node [
    id 348
    label "Afryka_Wschodnia"
  ]
  node [
    id 349
    label "Cypr"
  ]
  node [
    id 350
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 351
    label "Podkarpacie"
  ]
  node [
    id 352
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 353
    label "obszar"
  ]
  node [
    id 354
    label "Peru"
  ]
  node [
    id 355
    label "Afryka_Zachodnia"
  ]
  node [
    id 356
    label "Toskania"
  ]
  node [
    id 357
    label "Austria"
  ]
  node [
    id 358
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 359
    label "Urugwaj"
  ]
  node [
    id 360
    label "Podbeskidzie"
  ]
  node [
    id 361
    label "Jordania"
  ]
  node [
    id 362
    label "Bo&#347;nia"
  ]
  node [
    id 363
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 364
    label "Grecja"
  ]
  node [
    id 365
    label "Azerbejd&#380;an"
  ]
  node [
    id 366
    label "Oceania"
  ]
  node [
    id 367
    label "Turcja"
  ]
  node [
    id 368
    label "Pomorze_Zachodnie"
  ]
  node [
    id 369
    label "Samoa"
  ]
  node [
    id 370
    label "Powi&#347;le"
  ]
  node [
    id 371
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 372
    label "ziemia"
  ]
  node [
    id 373
    label "Sudan"
  ]
  node [
    id 374
    label "Oman"
  ]
  node [
    id 375
    label "&#321;emkowszczyzna"
  ]
  node [
    id 376
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 377
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 378
    label "Uzbekistan"
  ]
  node [
    id 379
    label "Portoryko"
  ]
  node [
    id 380
    label "Honduras"
  ]
  node [
    id 381
    label "Mongolia"
  ]
  node [
    id 382
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 383
    label "Kaszuby"
  ]
  node [
    id 384
    label "Ko&#322;yma"
  ]
  node [
    id 385
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 386
    label "Szlezwik"
  ]
  node [
    id 387
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 388
    label "Serbia"
  ]
  node [
    id 389
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 390
    label "Tajwan"
  ]
  node [
    id 391
    label "Wielka_Brytania"
  ]
  node [
    id 392
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 393
    label "Liban"
  ]
  node [
    id 394
    label "Japonia"
  ]
  node [
    id 395
    label "Ghana"
  ]
  node [
    id 396
    label "Belgia"
  ]
  node [
    id 397
    label "Bahrajn"
  ]
  node [
    id 398
    label "Mikronezja"
  ]
  node [
    id 399
    label "Etiopia"
  ]
  node [
    id 400
    label "Polesie"
  ]
  node [
    id 401
    label "Kuwejt"
  ]
  node [
    id 402
    label "Kerala"
  ]
  node [
    id 403
    label "Mazury"
  ]
  node [
    id 404
    label "Bahamy"
  ]
  node [
    id 405
    label "Rosja"
  ]
  node [
    id 406
    label "Mo&#322;dawia"
  ]
  node [
    id 407
    label "Palestyna"
  ]
  node [
    id 408
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 409
    label "Lauda"
  ]
  node [
    id 410
    label "Azja_Wschodnia"
  ]
  node [
    id 411
    label "Litwa"
  ]
  node [
    id 412
    label "S&#322;owenia"
  ]
  node [
    id 413
    label "Szwajcaria"
  ]
  node [
    id 414
    label "Erytrea"
  ]
  node [
    id 415
    label "Zakarpacie"
  ]
  node [
    id 416
    label "Arabia_Saudyjska"
  ]
  node [
    id 417
    label "Kuba"
  ]
  node [
    id 418
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 419
    label "Galicja"
  ]
  node [
    id 420
    label "Lubuskie"
  ]
  node [
    id 421
    label "Laponia"
  ]
  node [
    id 422
    label "granica_pa&#324;stwa"
  ]
  node [
    id 423
    label "Malezja"
  ]
  node [
    id 424
    label "Korea"
  ]
  node [
    id 425
    label "Yorkshire"
  ]
  node [
    id 426
    label "Bawaria"
  ]
  node [
    id 427
    label "Zag&#243;rze"
  ]
  node [
    id 428
    label "Jemen"
  ]
  node [
    id 429
    label "Nowa_Zelandia"
  ]
  node [
    id 430
    label "Andaluzja"
  ]
  node [
    id 431
    label "Namibia"
  ]
  node [
    id 432
    label "Nauru"
  ]
  node [
    id 433
    label "&#379;ywiecczyzna"
  ]
  node [
    id 434
    label "Brunei"
  ]
  node [
    id 435
    label "Oksytania"
  ]
  node [
    id 436
    label "Opolszczyzna"
  ]
  node [
    id 437
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 438
    label "Kociewie"
  ]
  node [
    id 439
    label "Khitai"
  ]
  node [
    id 440
    label "Mauretania"
  ]
  node [
    id 441
    label "Iran"
  ]
  node [
    id 442
    label "Gambia"
  ]
  node [
    id 443
    label "Somalia"
  ]
  node [
    id 444
    label "Holandia"
  ]
  node [
    id 445
    label "Lasko"
  ]
  node [
    id 446
    label "Turkmenistan"
  ]
  node [
    id 447
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 448
    label "Salwador"
  ]
  node [
    id 449
    label "woda"
  ]
  node [
    id 450
    label "linia"
  ]
  node [
    id 451
    label "ekoton"
  ]
  node [
    id 452
    label "str&#261;d"
  ]
  node [
    id 453
    label "koniec"
  ]
  node [
    id 454
    label "plantowa&#263;"
  ]
  node [
    id 455
    label "zapadnia"
  ]
  node [
    id 456
    label "budynek"
  ]
  node [
    id 457
    label "skorupa_ziemska"
  ]
  node [
    id 458
    label "glinowanie"
  ]
  node [
    id 459
    label "martwica"
  ]
  node [
    id 460
    label "teren"
  ]
  node [
    id 461
    label "litosfera"
  ]
  node [
    id 462
    label "penetrator"
  ]
  node [
    id 463
    label "glinowa&#263;"
  ]
  node [
    id 464
    label "domain"
  ]
  node [
    id 465
    label "podglebie"
  ]
  node [
    id 466
    label "kompleks_sorpcyjny"
  ]
  node [
    id 467
    label "miejsce"
  ]
  node [
    id 468
    label "kort"
  ]
  node [
    id 469
    label "czynnik_produkcji"
  ]
  node [
    id 470
    label "pojazd"
  ]
  node [
    id 471
    label "powierzchnia"
  ]
  node [
    id 472
    label "pr&#243;chnica"
  ]
  node [
    id 473
    label "pomieszczenie"
  ]
  node [
    id 474
    label "ryzosfera"
  ]
  node [
    id 475
    label "p&#322;aszczyzna"
  ]
  node [
    id 476
    label "dotleni&#263;"
  ]
  node [
    id 477
    label "glej"
  ]
  node [
    id 478
    label "pa&#324;stwo"
  ]
  node [
    id 479
    label "posadzka"
  ]
  node [
    id 480
    label "geosystem"
  ]
  node [
    id 481
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 482
    label "przestrze&#324;"
  ]
  node [
    id 483
    label "podmiot"
  ]
  node [
    id 484
    label "jednostka_organizacyjna"
  ]
  node [
    id 485
    label "struktura"
  ]
  node [
    id 486
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 487
    label "TOPR"
  ]
  node [
    id 488
    label "endecki"
  ]
  node [
    id 489
    label "zesp&#243;&#322;"
  ]
  node [
    id 490
    label "od&#322;am"
  ]
  node [
    id 491
    label "przedstawicielstwo"
  ]
  node [
    id 492
    label "Cepelia"
  ]
  node [
    id 493
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 494
    label "ZBoWiD"
  ]
  node [
    id 495
    label "organization"
  ]
  node [
    id 496
    label "centrala"
  ]
  node [
    id 497
    label "GOPR"
  ]
  node [
    id 498
    label "ZOMO"
  ]
  node [
    id 499
    label "ZMP"
  ]
  node [
    id 500
    label "komitet_koordynacyjny"
  ]
  node [
    id 501
    label "przybud&#243;wka"
  ]
  node [
    id 502
    label "boj&#243;wka"
  ]
  node [
    id 503
    label "p&#243;&#322;noc"
  ]
  node [
    id 504
    label "Kosowo"
  ]
  node [
    id 505
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 506
    label "Zab&#322;ocie"
  ]
  node [
    id 507
    label "zach&#243;d"
  ]
  node [
    id 508
    label "po&#322;udnie"
  ]
  node [
    id 509
    label "Pow&#261;zki"
  ]
  node [
    id 510
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 511
    label "Piotrowo"
  ]
  node [
    id 512
    label "Olszanica"
  ]
  node [
    id 513
    label "Ruda_Pabianicka"
  ]
  node [
    id 514
    label "holarktyka"
  ]
  node [
    id 515
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 516
    label "Ludwin&#243;w"
  ]
  node [
    id 517
    label "Arktyka"
  ]
  node [
    id 518
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 519
    label "Zabu&#380;e"
  ]
  node [
    id 520
    label "antroposfera"
  ]
  node [
    id 521
    label "Neogea"
  ]
  node [
    id 522
    label "terytorium"
  ]
  node [
    id 523
    label "Syberia_Zachodnia"
  ]
  node [
    id 524
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 525
    label "zakres"
  ]
  node [
    id 526
    label "pas_planetoid"
  ]
  node [
    id 527
    label "Syberia_Wschodnia"
  ]
  node [
    id 528
    label "Antarktyka"
  ]
  node [
    id 529
    label "Rakowice"
  ]
  node [
    id 530
    label "akrecja"
  ]
  node [
    id 531
    label "wymiar"
  ]
  node [
    id 532
    label "&#321;&#281;g"
  ]
  node [
    id 533
    label "Kresy_Zachodnie"
  ]
  node [
    id 534
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 535
    label "wsch&#243;d"
  ]
  node [
    id 536
    label "Notogea"
  ]
  node [
    id 537
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 538
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 539
    label "Pend&#380;ab"
  ]
  node [
    id 540
    label "funt_liba&#324;ski"
  ]
  node [
    id 541
    label "strefa_euro"
  ]
  node [
    id 542
    label "Pozna&#324;"
  ]
  node [
    id 543
    label "lira_malta&#324;ska"
  ]
  node [
    id 544
    label "Gozo"
  ]
  node [
    id 545
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 546
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 547
    label "dolar_namibijski"
  ]
  node [
    id 548
    label "milrejs"
  ]
  node [
    id 549
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 550
    label "NATO"
  ]
  node [
    id 551
    label "escudo_portugalskie"
  ]
  node [
    id 552
    label "dolar_bahamski"
  ]
  node [
    id 553
    label "Wielka_Bahama"
  ]
  node [
    id 554
    label "dolar_liberyjski"
  ]
  node [
    id 555
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 556
    label "riel"
  ]
  node [
    id 557
    label "Karelia"
  ]
  node [
    id 558
    label "Mari_El"
  ]
  node [
    id 559
    label "Inguszetia"
  ]
  node [
    id 560
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 561
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 562
    label "Udmurcja"
  ]
  node [
    id 563
    label "Newa"
  ]
  node [
    id 564
    label "&#321;adoga"
  ]
  node [
    id 565
    label "Czeczenia"
  ]
  node [
    id 566
    label "Anadyr"
  ]
  node [
    id 567
    label "Syberia"
  ]
  node [
    id 568
    label "Tatarstan"
  ]
  node [
    id 569
    label "Wszechrosja"
  ]
  node [
    id 570
    label "Azja"
  ]
  node [
    id 571
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 572
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 573
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 574
    label "Witim"
  ]
  node [
    id 575
    label "Kamczatka"
  ]
  node [
    id 576
    label "Jama&#322;"
  ]
  node [
    id 577
    label "Dagestan"
  ]
  node [
    id 578
    label "Tuwa"
  ]
  node [
    id 579
    label "car"
  ]
  node [
    id 580
    label "Komi"
  ]
  node [
    id 581
    label "Czuwaszja"
  ]
  node [
    id 582
    label "Chakasja"
  ]
  node [
    id 583
    label "Perm"
  ]
  node [
    id 584
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 585
    label "Ajon"
  ]
  node [
    id 586
    label "Adygeja"
  ]
  node [
    id 587
    label "Dniepr"
  ]
  node [
    id 588
    label "rubel_rosyjski"
  ]
  node [
    id 589
    label "Don"
  ]
  node [
    id 590
    label "Mordowia"
  ]
  node [
    id 591
    label "s&#322;owianofilstwo"
  ]
  node [
    id 592
    label "lew"
  ]
  node [
    id 593
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 594
    label "Dobrud&#380;a"
  ]
  node [
    id 595
    label "Unia_Europejska"
  ]
  node [
    id 596
    label "lira_izraelska"
  ]
  node [
    id 597
    label "szekel"
  ]
  node [
    id 598
    label "Galilea"
  ]
  node [
    id 599
    label "Judea"
  ]
  node [
    id 600
    label "Luksemburgia"
  ]
  node [
    id 601
    label "frank_belgijski"
  ]
  node [
    id 602
    label "Limburgia"
  ]
  node [
    id 603
    label "Walonia"
  ]
  node [
    id 604
    label "Brabancja"
  ]
  node [
    id 605
    label "Flandria"
  ]
  node [
    id 606
    label "Niderlandy"
  ]
  node [
    id 607
    label "dinar_iracki"
  ]
  node [
    id 608
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 609
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 610
    label "szyling_ugandyjski"
  ]
  node [
    id 611
    label "dolar_jamajski"
  ]
  node [
    id 612
    label "kafar"
  ]
  node [
    id 613
    label "ringgit"
  ]
  node [
    id 614
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 615
    label "Borneo"
  ]
  node [
    id 616
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 617
    label "dolar_surinamski"
  ]
  node [
    id 618
    label "funt_suda&#324;ski"
  ]
  node [
    id 619
    label "dolar_guja&#324;ski"
  ]
  node [
    id 620
    label "Manica"
  ]
  node [
    id 621
    label "escudo_mozambickie"
  ]
  node [
    id 622
    label "Cabo_Delgado"
  ]
  node [
    id 623
    label "Inhambane"
  ]
  node [
    id 624
    label "Maputo"
  ]
  node [
    id 625
    label "Gaza"
  ]
  node [
    id 626
    label "Niasa"
  ]
  node [
    id 627
    label "Nampula"
  ]
  node [
    id 628
    label "metical"
  ]
  node [
    id 629
    label "Sahara"
  ]
  node [
    id 630
    label "inti"
  ]
  node [
    id 631
    label "sol"
  ]
  node [
    id 632
    label "kip"
  ]
  node [
    id 633
    label "Pireneje"
  ]
  node [
    id 634
    label "euro"
  ]
  node [
    id 635
    label "kwacha_zambijska"
  ]
  node [
    id 636
    label "Buriaci"
  ]
  node [
    id 637
    label "tugrik"
  ]
  node [
    id 638
    label "ajmak"
  ]
  node [
    id 639
    label "balboa"
  ]
  node [
    id 640
    label "Ameryka_Centralna"
  ]
  node [
    id 641
    label "dolar"
  ]
  node [
    id 642
    label "gulden"
  ]
  node [
    id 643
    label "Zelandia"
  ]
  node [
    id 644
    label "manat_turkme&#324;ski"
  ]
  node [
    id 645
    label "dolar_Tuvalu"
  ]
  node [
    id 646
    label "zair"
  ]
  node [
    id 647
    label "Katanga"
  ]
  node [
    id 648
    label "frank_szwajcarski"
  ]
  node [
    id 649
    label "Jukatan"
  ]
  node [
    id 650
    label "dolar_Belize"
  ]
  node [
    id 651
    label "colon"
  ]
  node [
    id 652
    label "Dyja"
  ]
  node [
    id 653
    label "korona_czeska"
  ]
  node [
    id 654
    label "Izera"
  ]
  node [
    id 655
    label "ugija"
  ]
  node [
    id 656
    label "szyling_kenijski"
  ]
  node [
    id 657
    label "Nachiczewan"
  ]
  node [
    id 658
    label "manat_azerski"
  ]
  node [
    id 659
    label "Karabach"
  ]
  node [
    id 660
    label "Bengal"
  ]
  node [
    id 661
    label "taka"
  ]
  node [
    id 662
    label "Ocean_Spokojny"
  ]
  node [
    id 663
    label "dolar_Kiribati"
  ]
  node [
    id 664
    label "peso_filipi&#324;skie"
  ]
  node [
    id 665
    label "Cebu"
  ]
  node [
    id 666
    label "Atlantyk"
  ]
  node [
    id 667
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 668
    label "Ulster"
  ]
  node [
    id 669
    label "funt_irlandzki"
  ]
  node [
    id 670
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 671
    label "cedi"
  ]
  node [
    id 672
    label "ariary"
  ]
  node [
    id 673
    label "Ocean_Indyjski"
  ]
  node [
    id 674
    label "frank_malgaski"
  ]
  node [
    id 675
    label "Estremadura"
  ]
  node [
    id 676
    label "Kastylia"
  ]
  node [
    id 677
    label "Rzym_Zachodni"
  ]
  node [
    id 678
    label "Aragonia"
  ]
  node [
    id 679
    label "hacjender"
  ]
  node [
    id 680
    label "Asturia"
  ]
  node [
    id 681
    label "Baskonia"
  ]
  node [
    id 682
    label "Majorka"
  ]
  node [
    id 683
    label "Walencja"
  ]
  node [
    id 684
    label "peseta"
  ]
  node [
    id 685
    label "Katalonia"
  ]
  node [
    id 686
    label "peso_chilijskie"
  ]
  node [
    id 687
    label "Indie_Zachodnie"
  ]
  node [
    id 688
    label "Sikkim"
  ]
  node [
    id 689
    label "Asam"
  ]
  node [
    id 690
    label "rupia_indyjska"
  ]
  node [
    id 691
    label "Indie_Portugalskie"
  ]
  node [
    id 692
    label "Indie_Wschodnie"
  ]
  node [
    id 693
    label "Bollywood"
  ]
  node [
    id 694
    label "jen"
  ]
  node [
    id 695
    label "jinja"
  ]
  node [
    id 696
    label "Okinawa"
  ]
  node [
    id 697
    label "Japonica"
  ]
  node [
    id 698
    label "Rugia"
  ]
  node [
    id 699
    label "Saksonia"
  ]
  node [
    id 700
    label "Dolna_Saksonia"
  ]
  node [
    id 701
    label "Anglosas"
  ]
  node [
    id 702
    label "Hesja"
  ]
  node [
    id 703
    label "Wirtembergia"
  ]
  node [
    id 704
    label "Po&#322;abie"
  ]
  node [
    id 705
    label "Germania"
  ]
  node [
    id 706
    label "Frankonia"
  ]
  node [
    id 707
    label "Badenia"
  ]
  node [
    id 708
    label "Holsztyn"
  ]
  node [
    id 709
    label "marka"
  ]
  node [
    id 710
    label "Szwabia"
  ]
  node [
    id 711
    label "Brandenburgia"
  ]
  node [
    id 712
    label "Niemcy_Zachodnie"
  ]
  node [
    id 713
    label "Westfalia"
  ]
  node [
    id 714
    label "Helgoland"
  ]
  node [
    id 715
    label "Karlsbad"
  ]
  node [
    id 716
    label "Niemcy_Wschodnie"
  ]
  node [
    id 717
    label "Piemont"
  ]
  node [
    id 718
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 719
    label "Sardynia"
  ]
  node [
    id 720
    label "Italia"
  ]
  node [
    id 721
    label "Ok&#281;cie"
  ]
  node [
    id 722
    label "Karyntia"
  ]
  node [
    id 723
    label "Romania"
  ]
  node [
    id 724
    label "Warszawa"
  ]
  node [
    id 725
    label "lir"
  ]
  node [
    id 726
    label "Sycylia"
  ]
  node [
    id 727
    label "Dacja"
  ]
  node [
    id 728
    label "lej_rumu&#324;ski"
  ]
  node [
    id 729
    label "Siedmiogr&#243;d"
  ]
  node [
    id 730
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 731
    label "funt_syryjski"
  ]
  node [
    id 732
    label "alawizm"
  ]
  node [
    id 733
    label "frank_rwandyjski"
  ]
  node [
    id 734
    label "dinar_Bahrajnu"
  ]
  node [
    id 735
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 736
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 737
    label "frank_luksemburski"
  ]
  node [
    id 738
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 739
    label "peso_kuba&#324;skie"
  ]
  node [
    id 740
    label "frank_monakijski"
  ]
  node [
    id 741
    label "dinar_algierski"
  ]
  node [
    id 742
    label "Wojwodina"
  ]
  node [
    id 743
    label "dinar_serbski"
  ]
  node [
    id 744
    label "boliwar"
  ]
  node [
    id 745
    label "Orinoko"
  ]
  node [
    id 746
    label "tenge"
  ]
  node [
    id 747
    label "para"
  ]
  node [
    id 748
    label "lek"
  ]
  node [
    id 749
    label "frank_alba&#324;ski"
  ]
  node [
    id 750
    label "dolar_Barbadosu"
  ]
  node [
    id 751
    label "Antyle"
  ]
  node [
    id 752
    label "kyat"
  ]
  node [
    id 753
    label "Arakan"
  ]
  node [
    id 754
    label "c&#243;rdoba"
  ]
  node [
    id 755
    label "Paros"
  ]
  node [
    id 756
    label "Epir"
  ]
  node [
    id 757
    label "panhellenizm"
  ]
  node [
    id 758
    label "Eubea"
  ]
  node [
    id 759
    label "Rodos"
  ]
  node [
    id 760
    label "Achaja"
  ]
  node [
    id 761
    label "Termopile"
  ]
  node [
    id 762
    label "Attyka"
  ]
  node [
    id 763
    label "Hellada"
  ]
  node [
    id 764
    label "Etolia"
  ]
  node [
    id 765
    label "palestra"
  ]
  node [
    id 766
    label "Kreta"
  ]
  node [
    id 767
    label "drachma"
  ]
  node [
    id 768
    label "Olimp"
  ]
  node [
    id 769
    label "Tesalia"
  ]
  node [
    id 770
    label "Peloponez"
  ]
  node [
    id 771
    label "Eolia"
  ]
  node [
    id 772
    label "Beocja"
  ]
  node [
    id 773
    label "Parnas"
  ]
  node [
    id 774
    label "Lesbos"
  ]
  node [
    id 775
    label "Mariany"
  ]
  node [
    id 776
    label "Salzburg"
  ]
  node [
    id 777
    label "Rakuzy"
  ]
  node [
    id 778
    label "konsulent"
  ]
  node [
    id 779
    label "szyling_austryjacki"
  ]
  node [
    id 780
    label "birr"
  ]
  node [
    id 781
    label "negus"
  ]
  node [
    id 782
    label "Jawa"
  ]
  node [
    id 783
    label "Sumatra"
  ]
  node [
    id 784
    label "rupia_indonezyjska"
  ]
  node [
    id 785
    label "Nowa_Gwinea"
  ]
  node [
    id 786
    label "Moluki"
  ]
  node [
    id 787
    label "boliviano"
  ]
  node [
    id 788
    label "Pikardia"
  ]
  node [
    id 789
    label "Masyw_Centralny"
  ]
  node [
    id 790
    label "Akwitania"
  ]
  node [
    id 791
    label "Alzacja"
  ]
  node [
    id 792
    label "Sekwana"
  ]
  node [
    id 793
    label "Langwedocja"
  ]
  node [
    id 794
    label "Martynika"
  ]
  node [
    id 795
    label "Bretania"
  ]
  node [
    id 796
    label "Sabaudia"
  ]
  node [
    id 797
    label "Korsyka"
  ]
  node [
    id 798
    label "Normandia"
  ]
  node [
    id 799
    label "Gaskonia"
  ]
  node [
    id 800
    label "Burgundia"
  ]
  node [
    id 801
    label "frank_francuski"
  ]
  node [
    id 802
    label "Wandea"
  ]
  node [
    id 803
    label "Prowansja"
  ]
  node [
    id 804
    label "Gwadelupa"
  ]
  node [
    id 805
    label "somoni"
  ]
  node [
    id 806
    label "Melanezja"
  ]
  node [
    id 807
    label "dolar_Fid&#380;i"
  ]
  node [
    id 808
    label "funt_cypryjski"
  ]
  node [
    id 809
    label "Afrodyzje"
  ]
  node [
    id 810
    label "peso_dominika&#324;skie"
  ]
  node [
    id 811
    label "Fryburg"
  ]
  node [
    id 812
    label "Bazylea"
  ]
  node [
    id 813
    label "Alpy"
  ]
  node [
    id 814
    label "Helwecja"
  ]
  node [
    id 815
    label "Berno"
  ]
  node [
    id 816
    label "sum"
  ]
  node [
    id 817
    label "Karaka&#322;pacja"
  ]
  node [
    id 818
    label "Kurlandia"
  ]
  node [
    id 819
    label "Windawa"
  ]
  node [
    id 820
    label "&#322;at"
  ]
  node [
    id 821
    label "Liwonia"
  ]
  node [
    id 822
    label "rubel_&#322;otewski"
  ]
  node [
    id 823
    label "Inflanty"
  ]
  node [
    id 824
    label "Wile&#324;szczyzna"
  ]
  node [
    id 825
    label "&#379;mud&#378;"
  ]
  node [
    id 826
    label "lit"
  ]
  node [
    id 827
    label "frank_tunezyjski"
  ]
  node [
    id 828
    label "dinar_tunezyjski"
  ]
  node [
    id 829
    label "lempira"
  ]
  node [
    id 830
    label "korona_w&#281;gierska"
  ]
  node [
    id 831
    label "forint"
  ]
  node [
    id 832
    label "Lipt&#243;w"
  ]
  node [
    id 833
    label "dong"
  ]
  node [
    id 834
    label "Annam"
  ]
  node [
    id 835
    label "lud"
  ]
  node [
    id 836
    label "frank_kongijski"
  ]
  node [
    id 837
    label "szyling_somalijski"
  ]
  node [
    id 838
    label "cruzado"
  ]
  node [
    id 839
    label "real"
  ]
  node [
    id 840
    label "Podole"
  ]
  node [
    id 841
    label "Wsch&#243;d"
  ]
  node [
    id 842
    label "Naddnieprze"
  ]
  node [
    id 843
    label "Ma&#322;orosja"
  ]
  node [
    id 844
    label "Wo&#322;y&#324;"
  ]
  node [
    id 845
    label "Nadbu&#380;e"
  ]
  node [
    id 846
    label "hrywna"
  ]
  node [
    id 847
    label "Zaporo&#380;e"
  ]
  node [
    id 848
    label "Krym"
  ]
  node [
    id 849
    label "Dniestr"
  ]
  node [
    id 850
    label "Przykarpacie"
  ]
  node [
    id 851
    label "Kozaczyzna"
  ]
  node [
    id 852
    label "karbowaniec"
  ]
  node [
    id 853
    label "Tasmania"
  ]
  node [
    id 854
    label "Nowy_&#346;wiat"
  ]
  node [
    id 855
    label "dolar_australijski"
  ]
  node [
    id 856
    label "gourde"
  ]
  node [
    id 857
    label "escudo_angolskie"
  ]
  node [
    id 858
    label "kwanza"
  ]
  node [
    id 859
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 860
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 861
    label "Ad&#380;aria"
  ]
  node [
    id 862
    label "lari"
  ]
  node [
    id 863
    label "naira"
  ]
  node [
    id 864
    label "Ohio"
  ]
  node [
    id 865
    label "P&#243;&#322;noc"
  ]
  node [
    id 866
    label "Nowy_York"
  ]
  node [
    id 867
    label "Illinois"
  ]
  node [
    id 868
    label "Po&#322;udnie"
  ]
  node [
    id 869
    label "Kalifornia"
  ]
  node [
    id 870
    label "Wirginia"
  ]
  node [
    id 871
    label "Teksas"
  ]
  node [
    id 872
    label "Waszyngton"
  ]
  node [
    id 873
    label "zielona_karta"
  ]
  node [
    id 874
    label "Alaska"
  ]
  node [
    id 875
    label "Massachusetts"
  ]
  node [
    id 876
    label "Hawaje"
  ]
  node [
    id 877
    label "Maryland"
  ]
  node [
    id 878
    label "Michigan"
  ]
  node [
    id 879
    label "Arizona"
  ]
  node [
    id 880
    label "Georgia"
  ]
  node [
    id 881
    label "stan_wolny"
  ]
  node [
    id 882
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 883
    label "Pensylwania"
  ]
  node [
    id 884
    label "Luizjana"
  ]
  node [
    id 885
    label "Nowy_Meksyk"
  ]
  node [
    id 886
    label "Wuj_Sam"
  ]
  node [
    id 887
    label "Alabama"
  ]
  node [
    id 888
    label "Kansas"
  ]
  node [
    id 889
    label "Oregon"
  ]
  node [
    id 890
    label "Zach&#243;d"
  ]
  node [
    id 891
    label "Oklahoma"
  ]
  node [
    id 892
    label "Floryda"
  ]
  node [
    id 893
    label "Hudson"
  ]
  node [
    id 894
    label "som"
  ]
  node [
    id 895
    label "peso_urugwajskie"
  ]
  node [
    id 896
    label "denar_macedo&#324;ski"
  ]
  node [
    id 897
    label "dolar_Brunei"
  ]
  node [
    id 898
    label "rial_ira&#324;ski"
  ]
  node [
    id 899
    label "mu&#322;&#322;a"
  ]
  node [
    id 900
    label "Persja"
  ]
  node [
    id 901
    label "d&#380;amahirijja"
  ]
  node [
    id 902
    label "dinar_libijski"
  ]
  node [
    id 903
    label "nakfa"
  ]
  node [
    id 904
    label "rial_katarski"
  ]
  node [
    id 905
    label "quetzal"
  ]
  node [
    id 906
    label "won"
  ]
  node [
    id 907
    label "rial_jeme&#324;ski"
  ]
  node [
    id 908
    label "peso_argenty&#324;skie"
  ]
  node [
    id 909
    label "guarani"
  ]
  node [
    id 910
    label "perper"
  ]
  node [
    id 911
    label "dinar_kuwejcki"
  ]
  node [
    id 912
    label "dalasi"
  ]
  node [
    id 913
    label "dolar_Zimbabwe"
  ]
  node [
    id 914
    label "Szantung"
  ]
  node [
    id 915
    label "Chiny_Zachodnie"
  ]
  node [
    id 916
    label "Kuantung"
  ]
  node [
    id 917
    label "D&#380;ungaria"
  ]
  node [
    id 918
    label "yuan"
  ]
  node [
    id 919
    label "Hongkong"
  ]
  node [
    id 920
    label "Chiny_Wschodnie"
  ]
  node [
    id 921
    label "Guangdong"
  ]
  node [
    id 922
    label "Junnan"
  ]
  node [
    id 923
    label "Mand&#380;uria"
  ]
  node [
    id 924
    label "Syczuan"
  ]
  node [
    id 925
    label "Pa&#322;uki"
  ]
  node [
    id 926
    label "Wolin"
  ]
  node [
    id 927
    label "z&#322;oty"
  ]
  node [
    id 928
    label "So&#322;a"
  ]
  node [
    id 929
    label "Krajna"
  ]
  node [
    id 930
    label "Suwalszczyzna"
  ]
  node [
    id 931
    label "barwy_polskie"
  ]
  node [
    id 932
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 933
    label "Kaczawa"
  ]
  node [
    id 934
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 935
    label "Wis&#322;a"
  ]
  node [
    id 936
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 937
    label "Ujgur"
  ]
  node [
    id 938
    label "Azja_Mniejsza"
  ]
  node [
    id 939
    label "lira_turecka"
  ]
  node [
    id 940
    label "kuna"
  ]
  node [
    id 941
    label "dram"
  ]
  node [
    id 942
    label "tala"
  ]
  node [
    id 943
    label "korona_s&#322;owacka"
  ]
  node [
    id 944
    label "Turiec"
  ]
  node [
    id 945
    label "Himalaje"
  ]
  node [
    id 946
    label "rupia_nepalska"
  ]
  node [
    id 947
    label "frank_gwinejski"
  ]
  node [
    id 948
    label "korona_esto&#324;ska"
  ]
  node [
    id 949
    label "marka_esto&#324;ska"
  ]
  node [
    id 950
    label "Quebec"
  ]
  node [
    id 951
    label "dolar_kanadyjski"
  ]
  node [
    id 952
    label "Nowa_Fundlandia"
  ]
  node [
    id 953
    label "Zanzibar"
  ]
  node [
    id 954
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 955
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 956
    label "&#346;wite&#378;"
  ]
  node [
    id 957
    label "peso_kolumbijskie"
  ]
  node [
    id 958
    label "Synaj"
  ]
  node [
    id 959
    label "paraszyt"
  ]
  node [
    id 960
    label "funt_egipski"
  ]
  node [
    id 961
    label "szach"
  ]
  node [
    id 962
    label "Baktria"
  ]
  node [
    id 963
    label "afgani"
  ]
  node [
    id 964
    label "baht"
  ]
  node [
    id 965
    label "tolar"
  ]
  node [
    id 966
    label "lej_mo&#322;dawski"
  ]
  node [
    id 967
    label "Gagauzja"
  ]
  node [
    id 968
    label "moszaw"
  ]
  node [
    id 969
    label "Kanaan"
  ]
  node [
    id 970
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 971
    label "Jerozolima"
  ]
  node [
    id 972
    label "Beskidy_Zachodnie"
  ]
  node [
    id 973
    label "Wiktoria"
  ]
  node [
    id 974
    label "Guernsey"
  ]
  node [
    id 975
    label "Conrad"
  ]
  node [
    id 976
    label "funt_szterling"
  ]
  node [
    id 977
    label "Portland"
  ]
  node [
    id 978
    label "El&#380;bieta_I"
  ]
  node [
    id 979
    label "Kornwalia"
  ]
  node [
    id 980
    label "Dolna_Frankonia"
  ]
  node [
    id 981
    label "Karpaty"
  ]
  node [
    id 982
    label "Beskid_Niski"
  ]
  node [
    id 983
    label "Mariensztat"
  ]
  node [
    id 984
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 985
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 986
    label "Paj&#281;czno"
  ]
  node [
    id 987
    label "Mogielnica"
  ]
  node [
    id 988
    label "Gop&#322;o"
  ]
  node [
    id 989
    label "Moza"
  ]
  node [
    id 990
    label "Poprad"
  ]
  node [
    id 991
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 992
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 993
    label "Bojanowo"
  ]
  node [
    id 994
    label "Obra"
  ]
  node [
    id 995
    label "Wilkowo_Polskie"
  ]
  node [
    id 996
    label "Dobra"
  ]
  node [
    id 997
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 998
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 999
    label "Etruria"
  ]
  node [
    id 1000
    label "Rumelia"
  ]
  node [
    id 1001
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1002
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1003
    label "Abchazja"
  ]
  node [
    id 1004
    label "Sarmata"
  ]
  node [
    id 1005
    label "Eurazja"
  ]
  node [
    id 1006
    label "Tatry"
  ]
  node [
    id 1007
    label "Podtatrze"
  ]
  node [
    id 1008
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1009
    label "jezioro"
  ]
  node [
    id 1010
    label "&#346;l&#261;sk"
  ]
  node [
    id 1011
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1012
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1013
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1014
    label "Austro-W&#281;gry"
  ]
  node [
    id 1015
    label "funt_szkocki"
  ]
  node [
    id 1016
    label "Kaledonia"
  ]
  node [
    id 1017
    label "Biskupice"
  ]
  node [
    id 1018
    label "Iwanowice"
  ]
  node [
    id 1019
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1020
    label "Rogo&#378;nik"
  ]
  node [
    id 1021
    label "Ropa"
  ]
  node [
    id 1022
    label "Buriacja"
  ]
  node [
    id 1023
    label "Rozewie"
  ]
  node [
    id 1024
    label "Norwegia"
  ]
  node [
    id 1025
    label "Szwecja"
  ]
  node [
    id 1026
    label "Finlandia"
  ]
  node [
    id 1027
    label "Aruba"
  ]
  node [
    id 1028
    label "Kajmany"
  ]
  node [
    id 1029
    label "Anguilla"
  ]
  node [
    id 1030
    label "Amazonka"
  ]
  node [
    id 1031
    label "dolecie&#263;"
  ]
  node [
    id 1032
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 1033
    label "spotka&#263;"
  ]
  node [
    id 1034
    label "przypasowa&#263;"
  ]
  node [
    id 1035
    label "hit"
  ]
  node [
    id 1036
    label "pocisk"
  ]
  node [
    id 1037
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 1038
    label "stumble"
  ]
  node [
    id 1039
    label "dotrze&#263;"
  ]
  node [
    id 1040
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1041
    label "wpa&#347;&#263;"
  ]
  node [
    id 1042
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 1043
    label "znale&#378;&#263;"
  ]
  node [
    id 1044
    label "happen"
  ]
  node [
    id 1045
    label "insert"
  ]
  node [
    id 1046
    label "visualize"
  ]
  node [
    id 1047
    label "pozna&#263;"
  ]
  node [
    id 1048
    label "befall"
  ]
  node [
    id 1049
    label "go_steady"
  ]
  node [
    id 1050
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1051
    label "strike"
  ]
  node [
    id 1052
    label "ulec"
  ]
  node [
    id 1053
    label "collapse"
  ]
  node [
    id 1054
    label "rzecz"
  ]
  node [
    id 1055
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1056
    label "fall_upon"
  ]
  node [
    id 1057
    label "ponie&#347;&#263;"
  ]
  node [
    id 1058
    label "ogrom"
  ]
  node [
    id 1059
    label "zapach"
  ]
  node [
    id 1060
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 1061
    label "uderzy&#263;"
  ]
  node [
    id 1062
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1063
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 1064
    label "wpada&#263;"
  ]
  node [
    id 1065
    label "decline"
  ]
  node [
    id 1066
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1067
    label "fall"
  ]
  node [
    id 1068
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 1069
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1070
    label "emocja"
  ]
  node [
    id 1071
    label "odwiedzi&#263;"
  ]
  node [
    id 1072
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 1073
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 1074
    label "pozyska&#263;"
  ]
  node [
    id 1075
    label "oceni&#263;"
  ]
  node [
    id 1076
    label "devise"
  ]
  node [
    id 1077
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 1078
    label "dozna&#263;"
  ]
  node [
    id 1079
    label "wykry&#263;"
  ]
  node [
    id 1080
    label "odzyska&#263;"
  ]
  node [
    id 1081
    label "znaj&#347;&#263;"
  ]
  node [
    id 1082
    label "invent"
  ]
  node [
    id 1083
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 1084
    label "utrze&#263;"
  ]
  node [
    id 1085
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 1086
    label "silnik"
  ]
  node [
    id 1087
    label "catch"
  ]
  node [
    id 1088
    label "dopasowa&#263;"
  ]
  node [
    id 1089
    label "advance"
  ]
  node [
    id 1090
    label "get"
  ]
  node [
    id 1091
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 1092
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 1093
    label "dorobi&#263;"
  ]
  node [
    id 1094
    label "become"
  ]
  node [
    id 1095
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 1096
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1097
    label "range"
  ]
  node [
    id 1098
    label "flow"
  ]
  node [
    id 1099
    label "doj&#347;&#263;"
  ]
  node [
    id 1100
    label "moda"
  ]
  node [
    id 1101
    label "popularny"
  ]
  node [
    id 1102
    label "utw&#243;r"
  ]
  node [
    id 1103
    label "sensacja"
  ]
  node [
    id 1104
    label "nowina"
  ]
  node [
    id 1105
    label "odkrycie"
  ]
  node [
    id 1106
    label "amunicja"
  ]
  node [
    id 1107
    label "g&#322;owica"
  ]
  node [
    id 1108
    label "trafienie"
  ]
  node [
    id 1109
    label "trafianie"
  ]
  node [
    id 1110
    label "kulka"
  ]
  node [
    id 1111
    label "rdze&#324;"
  ]
  node [
    id 1112
    label "prochownia"
  ]
  node [
    id 1113
    label "przeniesienie"
  ]
  node [
    id 1114
    label "&#322;adunek_bojowy"
  ]
  node [
    id 1115
    label "przenoszenie"
  ]
  node [
    id 1116
    label "przenie&#347;&#263;"
  ]
  node [
    id 1117
    label "trafia&#263;"
  ]
  node [
    id 1118
    label "przenosi&#263;"
  ]
  node [
    id 1119
    label "bro&#324;"
  ]
  node [
    id 1120
    label "inny"
  ]
  node [
    id 1121
    label "jaki&#347;"
  ]
  node [
    id 1122
    label "r&#243;&#380;nie"
  ]
  node [
    id 1123
    label "przyzwoity"
  ]
  node [
    id 1124
    label "ciekawy"
  ]
  node [
    id 1125
    label "jako&#347;"
  ]
  node [
    id 1126
    label "jako_tako"
  ]
  node [
    id 1127
    label "niez&#322;y"
  ]
  node [
    id 1128
    label "dziwny"
  ]
  node [
    id 1129
    label "charakterystyczny"
  ]
  node [
    id 1130
    label "kolejny"
  ]
  node [
    id 1131
    label "osobno"
  ]
  node [
    id 1132
    label "inszy"
  ]
  node [
    id 1133
    label "inaczej"
  ]
  node [
    id 1134
    label "osobnie"
  ]
  node [
    id 1135
    label "sprawa"
  ]
  node [
    id 1136
    label "subiekcja"
  ]
  node [
    id 1137
    label "problemat"
  ]
  node [
    id 1138
    label "jajko_Kolumba"
  ]
  node [
    id 1139
    label "obstruction"
  ]
  node [
    id 1140
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1141
    label "problematyka"
  ]
  node [
    id 1142
    label "trudno&#347;&#263;"
  ]
  node [
    id 1143
    label "pierepa&#322;ka"
  ]
  node [
    id 1144
    label "ambaras"
  ]
  node [
    id 1145
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1146
    label "napotka&#263;"
  ]
  node [
    id 1147
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 1148
    label "k&#322;opotliwy"
  ]
  node [
    id 1149
    label "napotkanie"
  ]
  node [
    id 1150
    label "poziom"
  ]
  node [
    id 1151
    label "difficulty"
  ]
  node [
    id 1152
    label "obstacle"
  ]
  node [
    id 1153
    label "sytuacja"
  ]
  node [
    id 1154
    label "kognicja"
  ]
  node [
    id 1155
    label "object"
  ]
  node [
    id 1156
    label "rozprawa"
  ]
  node [
    id 1157
    label "temat"
  ]
  node [
    id 1158
    label "wydarzenie"
  ]
  node [
    id 1159
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1160
    label "proposition"
  ]
  node [
    id 1161
    label "przes&#322;anka"
  ]
  node [
    id 1162
    label "idea"
  ]
  node [
    id 1163
    label "k&#322;opot"
  ]
  node [
    id 1164
    label "wyj&#261;tkowy"
  ]
  node [
    id 1165
    label "niepor&#243;wnywalnie"
  ]
  node [
    id 1166
    label "wyj&#261;tkowo"
  ]
  node [
    id 1167
    label "doskonale"
  ]
  node [
    id 1168
    label "niezr&#243;wnany"
  ]
  node [
    id 1169
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1170
    label "&#347;rodowisko"
  ]
  node [
    id 1171
    label "przedmiot"
  ]
  node [
    id 1172
    label "materia"
  ]
  node [
    id 1173
    label "szambo"
  ]
  node [
    id 1174
    label "aspo&#322;eczny"
  ]
  node [
    id 1175
    label "component"
  ]
  node [
    id 1176
    label "szkodnik"
  ]
  node [
    id 1177
    label "gangsterski"
  ]
  node [
    id 1178
    label "poj&#281;cie"
  ]
  node [
    id 1179
    label "underworld"
  ]
  node [
    id 1180
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1181
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1182
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1183
    label "materia&#322;"
  ]
  node [
    id 1184
    label "byt"
  ]
  node [
    id 1185
    label "ropa"
  ]
  node [
    id 1186
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 1187
    label "whole"
  ]
  node [
    id 1188
    label "ilo&#347;&#263;"
  ]
  node [
    id 1189
    label "Rzym_Wschodni"
  ]
  node [
    id 1190
    label "urz&#261;dzenie"
  ]
  node [
    id 1191
    label "pos&#322;uchanie"
  ]
  node [
    id 1192
    label "skumanie"
  ]
  node [
    id 1193
    label "orientacja"
  ]
  node [
    id 1194
    label "wytw&#243;r"
  ]
  node [
    id 1195
    label "teoria"
  ]
  node [
    id 1196
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1197
    label "clasp"
  ]
  node [
    id 1198
    label "przem&#243;wienie"
  ]
  node [
    id 1199
    label "forma"
  ]
  node [
    id 1200
    label "zorientowanie"
  ]
  node [
    id 1201
    label "cz&#322;owiek"
  ]
  node [
    id 1202
    label "fumigacja"
  ]
  node [
    id 1203
    label "zwierz&#281;"
  ]
  node [
    id 1204
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 1205
    label "niszczyciel"
  ]
  node [
    id 1206
    label "zwierz&#281;_domowe"
  ]
  node [
    id 1207
    label "vermin"
  ]
  node [
    id 1208
    label "class"
  ]
  node [
    id 1209
    label "obiekt_naturalny"
  ]
  node [
    id 1210
    label "otoczenie"
  ]
  node [
    id 1211
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1212
    label "environment"
  ]
  node [
    id 1213
    label "huczek"
  ]
  node [
    id 1214
    label "ekosystem"
  ]
  node [
    id 1215
    label "wszechstworzenie"
  ]
  node [
    id 1216
    label "grupa"
  ]
  node [
    id 1217
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1218
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1219
    label "mikrokosmos"
  ]
  node [
    id 1220
    label "stw&#243;r"
  ]
  node [
    id 1221
    label "warunki"
  ]
  node [
    id 1222
    label "Ziemia"
  ]
  node [
    id 1223
    label "fauna"
  ]
  node [
    id 1224
    label "biota"
  ]
  node [
    id 1225
    label "integer"
  ]
  node [
    id 1226
    label "liczba"
  ]
  node [
    id 1227
    label "zlewanie_si&#281;"
  ]
  node [
    id 1228
    label "uk&#322;ad"
  ]
  node [
    id 1229
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1230
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1231
    label "pe&#322;ny"
  ]
  node [
    id 1232
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1233
    label "zboczenie"
  ]
  node [
    id 1234
    label "om&#243;wienie"
  ]
  node [
    id 1235
    label "sponiewieranie"
  ]
  node [
    id 1236
    label "discipline"
  ]
  node [
    id 1237
    label "omawia&#263;"
  ]
  node [
    id 1238
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1239
    label "tre&#347;&#263;"
  ]
  node [
    id 1240
    label "robienie"
  ]
  node [
    id 1241
    label "sponiewiera&#263;"
  ]
  node [
    id 1242
    label "entity"
  ]
  node [
    id 1243
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1244
    label "tematyka"
  ]
  node [
    id 1245
    label "w&#261;tek"
  ]
  node [
    id 1246
    label "charakter"
  ]
  node [
    id 1247
    label "zbaczanie"
  ]
  node [
    id 1248
    label "program_nauczania"
  ]
  node [
    id 1249
    label "om&#243;wi&#263;"
  ]
  node [
    id 1250
    label "omawianie"
  ]
  node [
    id 1251
    label "thing"
  ]
  node [
    id 1252
    label "kultura"
  ]
  node [
    id 1253
    label "istota"
  ]
  node [
    id 1254
    label "zbacza&#263;"
  ]
  node [
    id 1255
    label "zboczy&#263;"
  ]
  node [
    id 1256
    label "po_gangstersku"
  ]
  node [
    id 1257
    label "przest&#281;pczy"
  ]
  node [
    id 1258
    label "smr&#243;d"
  ]
  node [
    id 1259
    label "gips"
  ]
  node [
    id 1260
    label "koszmar"
  ]
  node [
    id 1261
    label "pasztet"
  ]
  node [
    id 1262
    label "kanalizacja"
  ]
  node [
    id 1263
    label "mire"
  ]
  node [
    id 1264
    label "budowla"
  ]
  node [
    id 1265
    label "zbiornik"
  ]
  node [
    id 1266
    label "kloaka"
  ]
  node [
    id 1267
    label "niekorzystny"
  ]
  node [
    id 1268
    label "aspo&#322;ecznie"
  ]
  node [
    id 1269
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1270
    label "typowy"
  ]
  node [
    id 1271
    label "niech&#281;tny"
  ]
  node [
    id 1272
    label "najpewniej"
  ]
  node [
    id 1273
    label "pewny"
  ]
  node [
    id 1274
    label "wiarygodnie"
  ]
  node [
    id 1275
    label "mocno"
  ]
  node [
    id 1276
    label "pewniej"
  ]
  node [
    id 1277
    label "bezpiecznie"
  ]
  node [
    id 1278
    label "zwinnie"
  ]
  node [
    id 1279
    label "bezpieczny"
  ]
  node [
    id 1280
    label "&#322;atwo"
  ]
  node [
    id 1281
    label "bezpieczno"
  ]
  node [
    id 1282
    label "credibly"
  ]
  node [
    id 1283
    label "wiarygodny"
  ]
  node [
    id 1284
    label "believably"
  ]
  node [
    id 1285
    label "intensywny"
  ]
  node [
    id 1286
    label "mocny"
  ]
  node [
    id 1287
    label "silny"
  ]
  node [
    id 1288
    label "przekonuj&#261;co"
  ]
  node [
    id 1289
    label "niema&#322;o"
  ]
  node [
    id 1290
    label "powerfully"
  ]
  node [
    id 1291
    label "widocznie"
  ]
  node [
    id 1292
    label "szczerze"
  ]
  node [
    id 1293
    label "konkretnie"
  ]
  node [
    id 1294
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1295
    label "stabilnie"
  ]
  node [
    id 1296
    label "silnie"
  ]
  node [
    id 1297
    label "zdecydowanie"
  ]
  node [
    id 1298
    label "strongly"
  ]
  node [
    id 1299
    label "mo&#380;liwy"
  ]
  node [
    id 1300
    label "spokojny"
  ]
  node [
    id 1301
    label "upewnianie_si&#281;"
  ]
  node [
    id 1302
    label "ufanie"
  ]
  node [
    id 1303
    label "wierzenie"
  ]
  node [
    id 1304
    label "upewnienie_si&#281;"
  ]
  node [
    id 1305
    label "zwinny"
  ]
  node [
    id 1306
    label "polotnie"
  ]
  node [
    id 1307
    label "p&#322;ynnie"
  ]
  node [
    id 1308
    label "sprawnie"
  ]
  node [
    id 1309
    label "powo&#322;a&#263;"
  ]
  node [
    id 1310
    label "sie&#263;_rybacka"
  ]
  node [
    id 1311
    label "zu&#380;y&#263;"
  ]
  node [
    id 1312
    label "ustali&#263;"
  ]
  node [
    id 1313
    label "pick"
  ]
  node [
    id 1314
    label "kotwica"
  ]
  node [
    id 1315
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1316
    label "wydzieli&#263;"
  ]
  node [
    id 1317
    label "wy&#322;&#261;czy&#263;"
  ]
  node [
    id 1318
    label "remove"
  ]
  node [
    id 1319
    label "consume"
  ]
  node [
    id 1320
    label "appoint"
  ]
  node [
    id 1321
    label "poborowy"
  ]
  node [
    id 1322
    label "wskaza&#263;"
  ]
  node [
    id 1323
    label "put"
  ]
  node [
    id 1324
    label "bind"
  ]
  node [
    id 1325
    label "umocni&#263;"
  ]
  node [
    id 1326
    label "narz&#281;dzie"
  ]
  node [
    id 1327
    label "kotwiczy&#263;"
  ]
  node [
    id 1328
    label "zakotwiczenie"
  ]
  node [
    id 1329
    label "wybieranie"
  ]
  node [
    id 1330
    label "wybiera&#263;"
  ]
  node [
    id 1331
    label "statek"
  ]
  node [
    id 1332
    label "zegar"
  ]
  node [
    id 1333
    label "zakotwiczy&#263;"
  ]
  node [
    id 1334
    label "wybranie"
  ]
  node [
    id 1335
    label "kotwiczenie"
  ]
  node [
    id 1336
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1337
    label "odpowiednio"
  ]
  node [
    id 1338
    label "dobroczynnie"
  ]
  node [
    id 1339
    label "moralnie"
  ]
  node [
    id 1340
    label "korzystnie"
  ]
  node [
    id 1341
    label "pozytywnie"
  ]
  node [
    id 1342
    label "lepiej"
  ]
  node [
    id 1343
    label "wiele"
  ]
  node [
    id 1344
    label "skutecznie"
  ]
  node [
    id 1345
    label "pomy&#347;lnie"
  ]
  node [
    id 1346
    label "dobry"
  ]
  node [
    id 1347
    label "charakterystycznie"
  ]
  node [
    id 1348
    label "nale&#380;nie"
  ]
  node [
    id 1349
    label "stosowny"
  ]
  node [
    id 1350
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1351
    label "nale&#380;ycie"
  ]
  node [
    id 1352
    label "prawdziwie"
  ]
  node [
    id 1353
    label "auspiciously"
  ]
  node [
    id 1354
    label "pomy&#347;lny"
  ]
  node [
    id 1355
    label "moralny"
  ]
  node [
    id 1356
    label "etyczny"
  ]
  node [
    id 1357
    label "skuteczny"
  ]
  node [
    id 1358
    label "wiela"
  ]
  node [
    id 1359
    label "du&#380;y"
  ]
  node [
    id 1360
    label "utylitarnie"
  ]
  node [
    id 1361
    label "korzystny"
  ]
  node [
    id 1362
    label "beneficially"
  ]
  node [
    id 1363
    label "przyjemnie"
  ]
  node [
    id 1364
    label "pozytywny"
  ]
  node [
    id 1365
    label "ontologicznie"
  ]
  node [
    id 1366
    label "dodatni"
  ]
  node [
    id 1367
    label "odpowiedni"
  ]
  node [
    id 1368
    label "wiersz"
  ]
  node [
    id 1369
    label "dobroczynny"
  ]
  node [
    id 1370
    label "czw&#243;rka"
  ]
  node [
    id 1371
    label "&#347;mieszny"
  ]
  node [
    id 1372
    label "mi&#322;y"
  ]
  node [
    id 1373
    label "grzeczny"
  ]
  node [
    id 1374
    label "powitanie"
  ]
  node [
    id 1375
    label "ca&#322;y"
  ]
  node [
    id 1376
    label "zwrot"
  ]
  node [
    id 1377
    label "drogi"
  ]
  node [
    id 1378
    label "pos&#322;uszny"
  ]
  node [
    id 1379
    label "philanthropically"
  ]
  node [
    id 1380
    label "spo&#322;ecznie"
  ]
  node [
    id 1381
    label "negatywnie"
  ]
  node [
    id 1382
    label "niepomy&#347;lnie"
  ]
  node [
    id 1383
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 1384
    label "piesko"
  ]
  node [
    id 1385
    label "niezgodnie"
  ]
  node [
    id 1386
    label "gorzej"
  ]
  node [
    id 1387
    label "niekorzystnie"
  ]
  node [
    id 1388
    label "z&#322;y"
  ]
  node [
    id 1389
    label "pieski"
  ]
  node [
    id 1390
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 1391
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 1392
    label "z&#322;oszczenie"
  ]
  node [
    id 1393
    label "sierdzisty"
  ]
  node [
    id 1394
    label "niegrzeczny"
  ]
  node [
    id 1395
    label "zez&#322;oszczenie"
  ]
  node [
    id 1396
    label "zdenerwowany"
  ]
  node [
    id 1397
    label "negatywny"
  ]
  node [
    id 1398
    label "rozgniewanie"
  ]
  node [
    id 1399
    label "gniewanie"
  ]
  node [
    id 1400
    label "niemoralny"
  ]
  node [
    id 1401
    label "niepomy&#347;lny"
  ]
  node [
    id 1402
    label "syf"
  ]
  node [
    id 1403
    label "niezgodny"
  ]
  node [
    id 1404
    label "odmiennie"
  ]
  node [
    id 1405
    label "niepo&#380;&#261;danie"
  ]
  node [
    id 1406
    label "ujemny"
  ]
  node [
    id 1407
    label "Arabia"
  ]
  node [
    id 1408
    label "saudyjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 812
  ]
  edge [
    source 6
    target 813
  ]
  edge [
    source 6
    target 814
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 816
  ]
  edge [
    source 6
    target 817
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 6
    target 824
  ]
  edge [
    source 6
    target 825
  ]
  edge [
    source 6
    target 826
  ]
  edge [
    source 6
    target 827
  ]
  edge [
    source 6
    target 828
  ]
  edge [
    source 6
    target 829
  ]
  edge [
    source 6
    target 830
  ]
  edge [
    source 6
    target 831
  ]
  edge [
    source 6
    target 832
  ]
  edge [
    source 6
    target 833
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 835
  ]
  edge [
    source 6
    target 836
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 6
    target 838
  ]
  edge [
    source 6
    target 839
  ]
  edge [
    source 6
    target 840
  ]
  edge [
    source 6
    target 841
  ]
  edge [
    source 6
    target 842
  ]
  edge [
    source 6
    target 843
  ]
  edge [
    source 6
    target 844
  ]
  edge [
    source 6
    target 845
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 865
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 867
  ]
  edge [
    source 6
    target 868
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 946
  ]
  edge [
    source 6
    target 947
  ]
  edge [
    source 6
    target 948
  ]
  edge [
    source 6
    target 949
  ]
  edge [
    source 6
    target 950
  ]
  edge [
    source 6
    target 951
  ]
  edge [
    source 6
    target 952
  ]
  edge [
    source 6
    target 953
  ]
  edge [
    source 6
    target 954
  ]
  edge [
    source 6
    target 955
  ]
  edge [
    source 6
    target 956
  ]
  edge [
    source 6
    target 957
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 959
  ]
  edge [
    source 6
    target 960
  ]
  edge [
    source 6
    target 961
  ]
  edge [
    source 6
    target 962
  ]
  edge [
    source 6
    target 963
  ]
  edge [
    source 6
    target 964
  ]
  edge [
    source 6
    target 965
  ]
  edge [
    source 6
    target 966
  ]
  edge [
    source 6
    target 967
  ]
  edge [
    source 6
    target 968
  ]
  edge [
    source 6
    target 969
  ]
  edge [
    source 6
    target 970
  ]
  edge [
    source 6
    target 971
  ]
  edge [
    source 6
    target 972
  ]
  edge [
    source 6
    target 973
  ]
  edge [
    source 6
    target 974
  ]
  edge [
    source 6
    target 975
  ]
  edge [
    source 6
    target 976
  ]
  edge [
    source 6
    target 977
  ]
  edge [
    source 6
    target 978
  ]
  edge [
    source 6
    target 979
  ]
  edge [
    source 6
    target 980
  ]
  edge [
    source 6
    target 981
  ]
  edge [
    source 6
    target 982
  ]
  edge [
    source 6
    target 983
  ]
  edge [
    source 6
    target 984
  ]
  edge [
    source 6
    target 985
  ]
  edge [
    source 6
    target 986
  ]
  edge [
    source 6
    target 987
  ]
  edge [
    source 6
    target 988
  ]
  edge [
    source 6
    target 989
  ]
  edge [
    source 6
    target 990
  ]
  edge [
    source 6
    target 991
  ]
  edge [
    source 6
    target 992
  ]
  edge [
    source 6
    target 993
  ]
  edge [
    source 6
    target 994
  ]
  edge [
    source 6
    target 995
  ]
  edge [
    source 6
    target 996
  ]
  edge [
    source 6
    target 997
  ]
  edge [
    source 6
    target 998
  ]
  edge [
    source 6
    target 999
  ]
  edge [
    source 6
    target 1000
  ]
  edge [
    source 6
    target 1001
  ]
  edge [
    source 6
    target 1002
  ]
  edge [
    source 6
    target 1003
  ]
  edge [
    source 6
    target 1004
  ]
  edge [
    source 6
    target 1005
  ]
  edge [
    source 6
    target 1006
  ]
  edge [
    source 6
    target 1007
  ]
  edge [
    source 6
    target 1008
  ]
  edge [
    source 6
    target 1009
  ]
  edge [
    source 6
    target 1010
  ]
  edge [
    source 6
    target 1011
  ]
  edge [
    source 6
    target 1012
  ]
  edge [
    source 6
    target 1013
  ]
  edge [
    source 6
    target 1014
  ]
  edge [
    source 6
    target 1015
  ]
  edge [
    source 6
    target 1016
  ]
  edge [
    source 6
    target 1017
  ]
  edge [
    source 6
    target 1018
  ]
  edge [
    source 6
    target 1019
  ]
  edge [
    source 6
    target 1020
  ]
  edge [
    source 6
    target 1021
  ]
  edge [
    source 6
    target 1022
  ]
  edge [
    source 6
    target 1023
  ]
  edge [
    source 6
    target 1024
  ]
  edge [
    source 6
    target 1025
  ]
  edge [
    source 6
    target 1026
  ]
  edge [
    source 6
    target 1027
  ]
  edge [
    source 6
    target 1028
  ]
  edge [
    source 6
    target 1029
  ]
  edge [
    source 6
    target 1030
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1031
  ]
  edge [
    source 7
    target 1032
  ]
  edge [
    source 7
    target 1033
  ]
  edge [
    source 7
    target 1034
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 7
    target 1053
  ]
  edge [
    source 7
    target 1054
  ]
  edge [
    source 7
    target 1055
  ]
  edge [
    source 7
    target 1056
  ]
  edge [
    source 7
    target 1057
  ]
  edge [
    source 7
    target 1058
  ]
  edge [
    source 7
    target 1059
  ]
  edge [
    source 7
    target 1060
  ]
  edge [
    source 7
    target 1061
  ]
  edge [
    source 7
    target 1062
  ]
  edge [
    source 7
    target 1063
  ]
  edge [
    source 7
    target 1064
  ]
  edge [
    source 7
    target 1065
  ]
  edge [
    source 7
    target 1066
  ]
  edge [
    source 7
    target 1067
  ]
  edge [
    source 7
    target 1068
  ]
  edge [
    source 7
    target 1069
  ]
  edge [
    source 7
    target 1070
  ]
  edge [
    source 7
    target 1071
  ]
  edge [
    source 7
    target 1072
  ]
  edge [
    source 7
    target 1073
  ]
  edge [
    source 7
    target 1074
  ]
  edge [
    source 7
    target 1075
  ]
  edge [
    source 7
    target 1076
  ]
  edge [
    source 7
    target 1077
  ]
  edge [
    source 7
    target 1078
  ]
  edge [
    source 7
    target 1079
  ]
  edge [
    source 7
    target 1080
  ]
  edge [
    source 7
    target 1081
  ]
  edge [
    source 7
    target 1082
  ]
  edge [
    source 7
    target 1083
  ]
  edge [
    source 7
    target 1084
  ]
  edge [
    source 7
    target 1085
  ]
  edge [
    source 7
    target 1086
  ]
  edge [
    source 7
    target 1087
  ]
  edge [
    source 7
    target 1088
  ]
  edge [
    source 7
    target 1089
  ]
  edge [
    source 7
    target 1090
  ]
  edge [
    source 7
    target 1091
  ]
  edge [
    source 7
    target 1092
  ]
  edge [
    source 7
    target 1093
  ]
  edge [
    source 7
    target 1094
  ]
  edge [
    source 7
    target 1095
  ]
  edge [
    source 7
    target 1096
  ]
  edge [
    source 7
    target 1097
  ]
  edge [
    source 7
    target 1098
  ]
  edge [
    source 7
    target 1099
  ]
  edge [
    source 7
    target 1100
  ]
  edge [
    source 7
    target 1101
  ]
  edge [
    source 7
    target 1102
  ]
  edge [
    source 7
    target 1103
  ]
  edge [
    source 7
    target 1104
  ]
  edge [
    source 7
    target 1105
  ]
  edge [
    source 7
    target 1106
  ]
  edge [
    source 7
    target 1107
  ]
  edge [
    source 7
    target 1108
  ]
  edge [
    source 7
    target 1109
  ]
  edge [
    source 7
    target 1110
  ]
  edge [
    source 7
    target 1111
  ]
  edge [
    source 7
    target 1112
  ]
  edge [
    source 7
    target 1113
  ]
  edge [
    source 7
    target 1114
  ]
  edge [
    source 7
    target 1115
  ]
  edge [
    source 7
    target 1116
  ]
  edge [
    source 7
    target 1117
  ]
  edge [
    source 7
    target 1118
  ]
  edge [
    source 7
    target 1119
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1120
  ]
  edge [
    source 8
    target 1121
  ]
  edge [
    source 8
    target 1122
  ]
  edge [
    source 8
    target 1123
  ]
  edge [
    source 8
    target 1124
  ]
  edge [
    source 8
    target 1125
  ]
  edge [
    source 8
    target 1126
  ]
  edge [
    source 8
    target 1127
  ]
  edge [
    source 8
    target 1128
  ]
  edge [
    source 8
    target 1129
  ]
  edge [
    source 8
    target 1130
  ]
  edge [
    source 8
    target 1131
  ]
  edge [
    source 8
    target 1132
  ]
  edge [
    source 8
    target 1133
  ]
  edge [
    source 8
    target 1134
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1135
  ]
  edge [
    source 9
    target 1136
  ]
  edge [
    source 9
    target 1137
  ]
  edge [
    source 9
    target 1138
  ]
  edge [
    source 9
    target 1139
  ]
  edge [
    source 9
    target 1140
  ]
  edge [
    source 9
    target 1141
  ]
  edge [
    source 9
    target 1142
  ]
  edge [
    source 9
    target 1143
  ]
  edge [
    source 9
    target 1144
  ]
  edge [
    source 9
    target 1145
  ]
  edge [
    source 9
    target 1146
  ]
  edge [
    source 9
    target 1147
  ]
  edge [
    source 9
    target 1148
  ]
  edge [
    source 9
    target 1149
  ]
  edge [
    source 9
    target 1150
  ]
  edge [
    source 9
    target 1151
  ]
  edge [
    source 9
    target 1152
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 1153
  ]
  edge [
    source 9
    target 1154
  ]
  edge [
    source 9
    target 1155
  ]
  edge [
    source 9
    target 1156
  ]
  edge [
    source 9
    target 1157
  ]
  edge [
    source 9
    target 1158
  ]
  edge [
    source 9
    target 1159
  ]
  edge [
    source 9
    target 1160
  ]
  edge [
    source 9
    target 1161
  ]
  edge [
    source 9
    target 1054
  ]
  edge [
    source 9
    target 1162
  ]
  edge [
    source 9
    target 1163
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1164
  ]
  edge [
    source 10
    target 1165
  ]
  edge [
    source 10
    target 1166
  ]
  edge [
    source 10
    target 1120
  ]
  edge [
    source 10
    target 1121
  ]
  edge [
    source 10
    target 1122
  ]
  edge [
    source 10
    target 1167
  ]
  edge [
    source 10
    target 1168
  ]
  edge [
    source 11
    target 1169
  ]
  edge [
    source 11
    target 1170
  ]
  edge [
    source 11
    target 1171
  ]
  edge [
    source 11
    target 1172
  ]
  edge [
    source 11
    target 1173
  ]
  edge [
    source 11
    target 1174
  ]
  edge [
    source 11
    target 1175
  ]
  edge [
    source 11
    target 1176
  ]
  edge [
    source 11
    target 1177
  ]
  edge [
    source 11
    target 1178
  ]
  edge [
    source 11
    target 1179
  ]
  edge [
    source 11
    target 1180
  ]
  edge [
    source 11
    target 1181
  ]
  edge [
    source 11
    target 1182
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 1183
  ]
  edge [
    source 11
    target 1157
  ]
  edge [
    source 11
    target 1184
  ]
  edge [
    source 11
    target 1159
  ]
  edge [
    source 11
    target 1185
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 1186
  ]
  edge [
    source 11
    target 1054
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 1187
  ]
  edge [
    source 11
    target 1188
  ]
  edge [
    source 11
    target 1189
  ]
  edge [
    source 11
    target 1190
  ]
  edge [
    source 11
    target 1191
  ]
  edge [
    source 11
    target 1192
  ]
  edge [
    source 11
    target 1193
  ]
  edge [
    source 11
    target 1194
  ]
  edge [
    source 11
    target 1195
  ]
  edge [
    source 11
    target 1196
  ]
  edge [
    source 11
    target 1197
  ]
  edge [
    source 11
    target 1198
  ]
  edge [
    source 11
    target 1199
  ]
  edge [
    source 11
    target 1200
  ]
  edge [
    source 11
    target 1201
  ]
  edge [
    source 11
    target 1202
  ]
  edge [
    source 11
    target 1203
  ]
  edge [
    source 11
    target 1204
  ]
  edge [
    source 11
    target 1205
  ]
  edge [
    source 11
    target 1206
  ]
  edge [
    source 11
    target 1207
  ]
  edge [
    source 11
    target 1208
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 1209
  ]
  edge [
    source 11
    target 1210
  ]
  edge [
    source 11
    target 1211
  ]
  edge [
    source 11
    target 1212
  ]
  edge [
    source 11
    target 1213
  ]
  edge [
    source 11
    target 1214
  ]
  edge [
    source 11
    target 1215
  ]
  edge [
    source 11
    target 1216
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 1217
  ]
  edge [
    source 11
    target 1218
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 1219
  ]
  edge [
    source 11
    target 1220
  ]
  edge [
    source 11
    target 1221
  ]
  edge [
    source 11
    target 1222
  ]
  edge [
    source 11
    target 1223
  ]
  edge [
    source 11
    target 1224
  ]
  edge [
    source 11
    target 1225
  ]
  edge [
    source 11
    target 1226
  ]
  edge [
    source 11
    target 1227
  ]
  edge [
    source 11
    target 1228
  ]
  edge [
    source 11
    target 1229
  ]
  edge [
    source 11
    target 1230
  ]
  edge [
    source 11
    target 1231
  ]
  edge [
    source 11
    target 1232
  ]
  edge [
    source 11
    target 1233
  ]
  edge [
    source 11
    target 1234
  ]
  edge [
    source 11
    target 1235
  ]
  edge [
    source 11
    target 1236
  ]
  edge [
    source 11
    target 1237
  ]
  edge [
    source 11
    target 1238
  ]
  edge [
    source 11
    target 1239
  ]
  edge [
    source 11
    target 1240
  ]
  edge [
    source 11
    target 1241
  ]
  edge [
    source 11
    target 1242
  ]
  edge [
    source 11
    target 1243
  ]
  edge [
    source 11
    target 1244
  ]
  edge [
    source 11
    target 1245
  ]
  edge [
    source 11
    target 1246
  ]
  edge [
    source 11
    target 1247
  ]
  edge [
    source 11
    target 1248
  ]
  edge [
    source 11
    target 1249
  ]
  edge [
    source 11
    target 1250
  ]
  edge [
    source 11
    target 1251
  ]
  edge [
    source 11
    target 1252
  ]
  edge [
    source 11
    target 1253
  ]
  edge [
    source 11
    target 1254
  ]
  edge [
    source 11
    target 1255
  ]
  edge [
    source 11
    target 1256
  ]
  edge [
    source 11
    target 1257
  ]
  edge [
    source 11
    target 1258
  ]
  edge [
    source 11
    target 1259
  ]
  edge [
    source 11
    target 1260
  ]
  edge [
    source 11
    target 1261
  ]
  edge [
    source 11
    target 1262
  ]
  edge [
    source 11
    target 1263
  ]
  edge [
    source 11
    target 1264
  ]
  edge [
    source 11
    target 1265
  ]
  edge [
    source 11
    target 1266
  ]
  edge [
    source 11
    target 1267
  ]
  edge [
    source 11
    target 1268
  ]
  edge [
    source 11
    target 1269
  ]
  edge [
    source 11
    target 1270
  ]
  edge [
    source 11
    target 1271
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1309
  ]
  edge [
    source 13
    target 1310
  ]
  edge [
    source 13
    target 1311
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 1312
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 13
    target 1313
  ]
  edge [
    source 13
    target 1314
  ]
  edge [
    source 13
    target 1315
  ]
  edge [
    source 13
    target 1316
  ]
  edge [
    source 13
    target 1317
  ]
  edge [
    source 13
    target 1318
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 1319
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 1320
  ]
  edge [
    source 13
    target 1321
  ]
  edge [
    source 13
    target 1322
  ]
  edge [
    source 13
    target 1323
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 1324
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1070
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 1334
  ]
  edge [
    source 13
    target 1335
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1336
  ]
  edge [
    source 15
    target 1337
  ]
  edge [
    source 15
    target 1338
  ]
  edge [
    source 15
    target 1339
  ]
  edge [
    source 15
    target 1340
  ]
  edge [
    source 15
    target 1341
  ]
  edge [
    source 15
    target 1342
  ]
  edge [
    source 15
    target 1343
  ]
  edge [
    source 15
    target 1344
  ]
  edge [
    source 15
    target 1345
  ]
  edge [
    source 15
    target 1346
  ]
  edge [
    source 15
    target 1347
  ]
  edge [
    source 15
    target 1348
  ]
  edge [
    source 15
    target 1349
  ]
  edge [
    source 15
    target 1350
  ]
  edge [
    source 15
    target 1351
  ]
  edge [
    source 15
    target 1352
  ]
  edge [
    source 15
    target 1353
  ]
  edge [
    source 15
    target 1354
  ]
  edge [
    source 15
    target 1355
  ]
  edge [
    source 15
    target 1356
  ]
  edge [
    source 15
    target 1357
  ]
  edge [
    source 15
    target 1358
  ]
  edge [
    source 15
    target 1359
  ]
  edge [
    source 15
    target 1360
  ]
  edge [
    source 15
    target 1361
  ]
  edge [
    source 15
    target 1362
  ]
  edge [
    source 15
    target 1363
  ]
  edge [
    source 15
    target 1364
  ]
  edge [
    source 15
    target 1365
  ]
  edge [
    source 15
    target 1366
  ]
  edge [
    source 15
    target 1367
  ]
  edge [
    source 15
    target 1368
  ]
  edge [
    source 15
    target 1369
  ]
  edge [
    source 15
    target 1370
  ]
  edge [
    source 15
    target 1300
  ]
  edge [
    source 15
    target 1371
  ]
  edge [
    source 15
    target 1372
  ]
  edge [
    source 15
    target 1373
  ]
  edge [
    source 15
    target 1374
  ]
  edge [
    source 15
    target 1375
  ]
  edge [
    source 15
    target 1376
  ]
  edge [
    source 15
    target 1377
  ]
  edge [
    source 15
    target 1378
  ]
  edge [
    source 15
    target 1379
  ]
  edge [
    source 15
    target 1380
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 1381
  ]
  edge [
    source 17
    target 1382
  ]
  edge [
    source 17
    target 1383
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 1385
  ]
  edge [
    source 17
    target 1386
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1388
  ]
  edge [
    source 17
    target 1389
  ]
  edge [
    source 17
    target 1390
  ]
  edge [
    source 17
    target 1391
  ]
  edge [
    source 17
    target 1267
  ]
  edge [
    source 17
    target 1392
  ]
  edge [
    source 17
    target 1393
  ]
  edge [
    source 17
    target 1394
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 1398
  ]
  edge [
    source 17
    target 1399
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 1407
    target 1408
  ]
]
