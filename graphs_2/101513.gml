graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "porucznik"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 4
    label "byby&#263;"
    origin "text"
  ]
  node [
    id 5
    label "tylko"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "rozb&#243;jnik"
    origin "text"
  ]
  node [
    id 8
    label "wojenny"
    origin "text"
  ]
  node [
    id 9
    label "wielki"
    origin "text"
  ]
  node [
    id 10
    label "sprawca"
    origin "text"
  ]
  node [
    id 11
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 13
    label "polszcze"
    origin "text"
  ]
  node [
    id 14
    label "przy"
    origin "text"
  ]
  node [
    id 15
    label "jeden"
    origin "text"
  ]
  node [
    id 16
    label "jurysdykcja"
    origin "text"
  ]
  node [
    id 17
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 18
    label "dziedzic"
    origin "text"
  ]
  node [
    id 19
    label "ch&#322;op"
    origin "text"
  ]
  node [
    id 20
    label "jako"
    origin "text"
  ]
  node [
    id 21
    label "lub"
    origin "text"
  ]
  node [
    id 22
    label "ekonom"
    origin "text"
  ]
  node [
    id 23
    label "ukrzywdzi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "ten"
    origin "text"
  ]
  node [
    id 25
    label "si&#281;"
    origin "text"
  ]
  node [
    id 26
    label "udo"
    origin "text"
  ]
  node [
    id 27
    label "skarga"
    origin "text"
  ]
  node [
    id 28
    label "zaraz"
    origin "text"
  ]
  node [
    id 29
    label "posy&#322;a&#263;"
    origin "text"
  ]
  node [
    id 30
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 31
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 32
    label "gro&#378;ba"
    origin "text"
  ]
  node [
    id 33
    label "inaczej"
    origin "text"
  ]
  node [
    id 34
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 35
    label "min"
    origin "text"
  ]
  node [
    id 36
    label "tok"
    origin "text"
  ]
  node [
    id 37
    label "dym"
    origin "text"
  ]
  node [
    id 38
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 39
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 40
    label "dwa"
    origin "text"
  ]
  node [
    id 41
    label "raz"
    origin "text"
  ]
  node [
    id 42
    label "dla"
    origin "text"
  ]
  node [
    id 43
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 44
    label "com"
    origin "text"
  ]
  node [
    id 45
    label "przyrzec"
    origin "text"
  ]
  node [
    id 46
    label "tom"
    origin "text"
  ]
  node [
    id 47
    label "dotrzyma&#263;"
    origin "text"
  ]
  node [
    id 48
    label "ale"
    origin "text"
  ]
  node [
    id 49
    label "chwa&#322;a"
    origin "text"
  ]
  node [
    id 50
    label "b&#243;g"
    origin "text"
  ]
  node [
    id 51
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 52
    label "teraz"
    origin "text"
  ]
  node [
    id 53
    label "potrzeba"
    origin "text"
  ]
  node [
    id 54
    label "ponawia&#263;"
    origin "text"
  ]
  node [
    id 55
    label "okolica"
    origin "text"
  ]
  node [
    id 56
    label "nikt"
    origin "text"
  ]
  node [
    id 57
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 58
    label "nad"
    origin "text"
  ]
  node [
    id 59
    label "inwentarz"
    origin "text"
  ]
  node [
    id 60
    label "za&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 61
    label "belfer"
  ]
  node [
    id 62
    label "murza"
  ]
  node [
    id 63
    label "cz&#322;owiek"
  ]
  node [
    id 64
    label "ojciec"
  ]
  node [
    id 65
    label "samiec"
  ]
  node [
    id 66
    label "androlog"
  ]
  node [
    id 67
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 68
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 69
    label "efendi"
  ]
  node [
    id 70
    label "opiekun"
  ]
  node [
    id 71
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 72
    label "pa&#324;stwo"
  ]
  node [
    id 73
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 74
    label "bratek"
  ]
  node [
    id 75
    label "Mieszko_I"
  ]
  node [
    id 76
    label "Midas"
  ]
  node [
    id 77
    label "m&#261;&#380;"
  ]
  node [
    id 78
    label "bogaty"
  ]
  node [
    id 79
    label "popularyzator"
  ]
  node [
    id 80
    label "pracodawca"
  ]
  node [
    id 81
    label "kszta&#322;ciciel"
  ]
  node [
    id 82
    label "preceptor"
  ]
  node [
    id 83
    label "nabab"
  ]
  node [
    id 84
    label "pupil"
  ]
  node [
    id 85
    label "andropauza"
  ]
  node [
    id 86
    label "zwrot"
  ]
  node [
    id 87
    label "przyw&#243;dca"
  ]
  node [
    id 88
    label "doros&#322;y"
  ]
  node [
    id 89
    label "pedagog"
  ]
  node [
    id 90
    label "rz&#261;dzenie"
  ]
  node [
    id 91
    label "jegomo&#347;&#263;"
  ]
  node [
    id 92
    label "szkolnik"
  ]
  node [
    id 93
    label "ch&#322;opina"
  ]
  node [
    id 94
    label "w&#322;odarz"
  ]
  node [
    id 95
    label "profesor"
  ]
  node [
    id 96
    label "gra_w_karty"
  ]
  node [
    id 97
    label "w&#322;adza"
  ]
  node [
    id 98
    label "Fidel_Castro"
  ]
  node [
    id 99
    label "Anders"
  ]
  node [
    id 100
    label "Ko&#347;ciuszko"
  ]
  node [
    id 101
    label "Tito"
  ]
  node [
    id 102
    label "Miko&#322;ajczyk"
  ]
  node [
    id 103
    label "lider"
  ]
  node [
    id 104
    label "Mao"
  ]
  node [
    id 105
    label "Sabataj_Cwi"
  ]
  node [
    id 106
    label "p&#322;atnik"
  ]
  node [
    id 107
    label "zwierzchnik"
  ]
  node [
    id 108
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 109
    label "nadzorca"
  ]
  node [
    id 110
    label "funkcjonariusz"
  ]
  node [
    id 111
    label "podmiot"
  ]
  node [
    id 112
    label "wykupienie"
  ]
  node [
    id 113
    label "bycie_w_posiadaniu"
  ]
  node [
    id 114
    label "wykupywanie"
  ]
  node [
    id 115
    label "rozszerzyciel"
  ]
  node [
    id 116
    label "ludzko&#347;&#263;"
  ]
  node [
    id 117
    label "asymilowanie"
  ]
  node [
    id 118
    label "wapniak"
  ]
  node [
    id 119
    label "asymilowa&#263;"
  ]
  node [
    id 120
    label "os&#322;abia&#263;"
  ]
  node [
    id 121
    label "posta&#263;"
  ]
  node [
    id 122
    label "hominid"
  ]
  node [
    id 123
    label "podw&#322;adny"
  ]
  node [
    id 124
    label "os&#322;abianie"
  ]
  node [
    id 125
    label "g&#322;owa"
  ]
  node [
    id 126
    label "figura"
  ]
  node [
    id 127
    label "portrecista"
  ]
  node [
    id 128
    label "dwun&#243;g"
  ]
  node [
    id 129
    label "profanum"
  ]
  node [
    id 130
    label "mikrokosmos"
  ]
  node [
    id 131
    label "nasada"
  ]
  node [
    id 132
    label "duch"
  ]
  node [
    id 133
    label "antropochoria"
  ]
  node [
    id 134
    label "osoba"
  ]
  node [
    id 135
    label "wz&#243;r"
  ]
  node [
    id 136
    label "senior"
  ]
  node [
    id 137
    label "oddzia&#322;ywanie"
  ]
  node [
    id 138
    label "Adam"
  ]
  node [
    id 139
    label "homo_sapiens"
  ]
  node [
    id 140
    label "polifag"
  ]
  node [
    id 141
    label "wydoro&#347;lenie"
  ]
  node [
    id 142
    label "du&#380;y"
  ]
  node [
    id 143
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 144
    label "doro&#347;lenie"
  ]
  node [
    id 145
    label "&#378;ra&#322;y"
  ]
  node [
    id 146
    label "doro&#347;le"
  ]
  node [
    id 147
    label "dojrzale"
  ]
  node [
    id 148
    label "dojrza&#322;y"
  ]
  node [
    id 149
    label "m&#261;dry"
  ]
  node [
    id 150
    label "doletni"
  ]
  node [
    id 151
    label "punkt"
  ]
  node [
    id 152
    label "turn"
  ]
  node [
    id 153
    label "turning"
  ]
  node [
    id 154
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 155
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 156
    label "skr&#281;t"
  ]
  node [
    id 157
    label "obr&#243;t"
  ]
  node [
    id 158
    label "fraza_czasownikowa"
  ]
  node [
    id 159
    label "jednostka_leksykalna"
  ]
  node [
    id 160
    label "zmiana"
  ]
  node [
    id 161
    label "wyra&#380;enie"
  ]
  node [
    id 162
    label "starosta"
  ]
  node [
    id 163
    label "zarz&#261;dca"
  ]
  node [
    id 164
    label "w&#322;adca"
  ]
  node [
    id 165
    label "nauczyciel"
  ]
  node [
    id 166
    label "stopie&#324;_naukowy"
  ]
  node [
    id 167
    label "nauczyciel_akademicki"
  ]
  node [
    id 168
    label "tytu&#322;"
  ]
  node [
    id 169
    label "profesura"
  ]
  node [
    id 170
    label "konsulent"
  ]
  node [
    id 171
    label "wirtuoz"
  ]
  node [
    id 172
    label "autor"
  ]
  node [
    id 173
    label "wyprawka"
  ]
  node [
    id 174
    label "mundurek"
  ]
  node [
    id 175
    label "szko&#322;a"
  ]
  node [
    id 176
    label "tarcza"
  ]
  node [
    id 177
    label "elew"
  ]
  node [
    id 178
    label "absolwent"
  ]
  node [
    id 179
    label "klasa"
  ]
  node [
    id 180
    label "ekspert"
  ]
  node [
    id 181
    label "ochotnik"
  ]
  node [
    id 182
    label "pomocnik"
  ]
  node [
    id 183
    label "student"
  ]
  node [
    id 184
    label "nauczyciel_muzyki"
  ]
  node [
    id 185
    label "zakonnik"
  ]
  node [
    id 186
    label "urz&#281;dnik"
  ]
  node [
    id 187
    label "bogacz"
  ]
  node [
    id 188
    label "dostojnik"
  ]
  node [
    id 189
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 190
    label "kuwada"
  ]
  node [
    id 191
    label "tworzyciel"
  ]
  node [
    id 192
    label "rodzice"
  ]
  node [
    id 193
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 194
    label "&#347;w"
  ]
  node [
    id 195
    label "pomys&#322;odawca"
  ]
  node [
    id 196
    label "rodzic"
  ]
  node [
    id 197
    label "wykonawca"
  ]
  node [
    id 198
    label "ojczym"
  ]
  node [
    id 199
    label "przodek"
  ]
  node [
    id 200
    label "papa"
  ]
  node [
    id 201
    label "stary"
  ]
  node [
    id 202
    label "kochanek"
  ]
  node [
    id 203
    label "fio&#322;ek"
  ]
  node [
    id 204
    label "facet"
  ]
  node [
    id 205
    label "brat"
  ]
  node [
    id 206
    label "zwierz&#281;"
  ]
  node [
    id 207
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 208
    label "ma&#322;&#380;onek"
  ]
  node [
    id 209
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 210
    label "m&#243;j"
  ]
  node [
    id 211
    label "pan_m&#322;ody"
  ]
  node [
    id 212
    label "&#347;lubny"
  ]
  node [
    id 213
    label "pan_domu"
  ]
  node [
    id 214
    label "pan_i_w&#322;adca"
  ]
  node [
    id 215
    label "mo&#347;&#263;"
  ]
  node [
    id 216
    label "Frygia"
  ]
  node [
    id 217
    label "sprawowanie"
  ]
  node [
    id 218
    label "dominion"
  ]
  node [
    id 219
    label "dominowanie"
  ]
  node [
    id 220
    label "reign"
  ]
  node [
    id 221
    label "rule"
  ]
  node [
    id 222
    label "zwierz&#281;_domowe"
  ]
  node [
    id 223
    label "J&#281;drzejewicz"
  ]
  node [
    id 224
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 225
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 226
    label "John_Dewey"
  ]
  node [
    id 227
    label "specjalista"
  ]
  node [
    id 228
    label "&#380;ycie"
  ]
  node [
    id 229
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 230
    label "Turek"
  ]
  node [
    id 231
    label "effendi"
  ]
  node [
    id 232
    label "obfituj&#261;cy"
  ]
  node [
    id 233
    label "r&#243;&#380;norodny"
  ]
  node [
    id 234
    label "spania&#322;y"
  ]
  node [
    id 235
    label "obficie"
  ]
  node [
    id 236
    label "sytuowany"
  ]
  node [
    id 237
    label "och&#281;do&#380;ny"
  ]
  node [
    id 238
    label "forsiasty"
  ]
  node [
    id 239
    label "zapa&#347;ny"
  ]
  node [
    id 240
    label "bogato"
  ]
  node [
    id 241
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 242
    label "Katar"
  ]
  node [
    id 243
    label "Libia"
  ]
  node [
    id 244
    label "Gwatemala"
  ]
  node [
    id 245
    label "Ekwador"
  ]
  node [
    id 246
    label "Afganistan"
  ]
  node [
    id 247
    label "Tad&#380;ykistan"
  ]
  node [
    id 248
    label "Bhutan"
  ]
  node [
    id 249
    label "Argentyna"
  ]
  node [
    id 250
    label "D&#380;ibuti"
  ]
  node [
    id 251
    label "Wenezuela"
  ]
  node [
    id 252
    label "Gabon"
  ]
  node [
    id 253
    label "Ukraina"
  ]
  node [
    id 254
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 255
    label "Rwanda"
  ]
  node [
    id 256
    label "Liechtenstein"
  ]
  node [
    id 257
    label "organizacja"
  ]
  node [
    id 258
    label "Sri_Lanka"
  ]
  node [
    id 259
    label "Madagaskar"
  ]
  node [
    id 260
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 261
    label "Kongo"
  ]
  node [
    id 262
    label "Tonga"
  ]
  node [
    id 263
    label "Bangladesz"
  ]
  node [
    id 264
    label "Kanada"
  ]
  node [
    id 265
    label "Wehrlen"
  ]
  node [
    id 266
    label "Algieria"
  ]
  node [
    id 267
    label "Uganda"
  ]
  node [
    id 268
    label "Surinam"
  ]
  node [
    id 269
    label "Sahara_Zachodnia"
  ]
  node [
    id 270
    label "Chile"
  ]
  node [
    id 271
    label "W&#281;gry"
  ]
  node [
    id 272
    label "Birma"
  ]
  node [
    id 273
    label "Kazachstan"
  ]
  node [
    id 274
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 275
    label "Armenia"
  ]
  node [
    id 276
    label "Tuwalu"
  ]
  node [
    id 277
    label "Timor_Wschodni"
  ]
  node [
    id 278
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 279
    label "Izrael"
  ]
  node [
    id 280
    label "Estonia"
  ]
  node [
    id 281
    label "Komory"
  ]
  node [
    id 282
    label "Kamerun"
  ]
  node [
    id 283
    label "Haiti"
  ]
  node [
    id 284
    label "Belize"
  ]
  node [
    id 285
    label "Sierra_Leone"
  ]
  node [
    id 286
    label "Luksemburg"
  ]
  node [
    id 287
    label "USA"
  ]
  node [
    id 288
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 289
    label "Barbados"
  ]
  node [
    id 290
    label "San_Marino"
  ]
  node [
    id 291
    label "Bu&#322;garia"
  ]
  node [
    id 292
    label "Indonezja"
  ]
  node [
    id 293
    label "Wietnam"
  ]
  node [
    id 294
    label "Malawi"
  ]
  node [
    id 295
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 296
    label "Francja"
  ]
  node [
    id 297
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 298
    label "partia"
  ]
  node [
    id 299
    label "Zambia"
  ]
  node [
    id 300
    label "Angola"
  ]
  node [
    id 301
    label "Grenada"
  ]
  node [
    id 302
    label "Nepal"
  ]
  node [
    id 303
    label "Panama"
  ]
  node [
    id 304
    label "Rumunia"
  ]
  node [
    id 305
    label "Czarnog&#243;ra"
  ]
  node [
    id 306
    label "Malediwy"
  ]
  node [
    id 307
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 308
    label "S&#322;owacja"
  ]
  node [
    id 309
    label "para"
  ]
  node [
    id 310
    label "Egipt"
  ]
  node [
    id 311
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 312
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 313
    label "Mozambik"
  ]
  node [
    id 314
    label "Kolumbia"
  ]
  node [
    id 315
    label "Laos"
  ]
  node [
    id 316
    label "Burundi"
  ]
  node [
    id 317
    label "Suazi"
  ]
  node [
    id 318
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 319
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 320
    label "Czechy"
  ]
  node [
    id 321
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 322
    label "Wyspy_Marshalla"
  ]
  node [
    id 323
    label "Dominika"
  ]
  node [
    id 324
    label "Trynidad_i_Tobago"
  ]
  node [
    id 325
    label "Syria"
  ]
  node [
    id 326
    label "Palau"
  ]
  node [
    id 327
    label "Gwinea_Bissau"
  ]
  node [
    id 328
    label "Liberia"
  ]
  node [
    id 329
    label "Jamajka"
  ]
  node [
    id 330
    label "Zimbabwe"
  ]
  node [
    id 331
    label "Polska"
  ]
  node [
    id 332
    label "Dominikana"
  ]
  node [
    id 333
    label "Senegal"
  ]
  node [
    id 334
    label "Togo"
  ]
  node [
    id 335
    label "Gujana"
  ]
  node [
    id 336
    label "Gruzja"
  ]
  node [
    id 337
    label "Albania"
  ]
  node [
    id 338
    label "Zair"
  ]
  node [
    id 339
    label "Meksyk"
  ]
  node [
    id 340
    label "Macedonia"
  ]
  node [
    id 341
    label "Chorwacja"
  ]
  node [
    id 342
    label "Kambod&#380;a"
  ]
  node [
    id 343
    label "Monako"
  ]
  node [
    id 344
    label "Mauritius"
  ]
  node [
    id 345
    label "Gwinea"
  ]
  node [
    id 346
    label "Mali"
  ]
  node [
    id 347
    label "Nigeria"
  ]
  node [
    id 348
    label "Kostaryka"
  ]
  node [
    id 349
    label "Hanower"
  ]
  node [
    id 350
    label "Paragwaj"
  ]
  node [
    id 351
    label "W&#322;ochy"
  ]
  node [
    id 352
    label "Seszele"
  ]
  node [
    id 353
    label "Wyspy_Salomona"
  ]
  node [
    id 354
    label "Hiszpania"
  ]
  node [
    id 355
    label "Boliwia"
  ]
  node [
    id 356
    label "Kirgistan"
  ]
  node [
    id 357
    label "Irlandia"
  ]
  node [
    id 358
    label "Czad"
  ]
  node [
    id 359
    label "Irak"
  ]
  node [
    id 360
    label "Lesoto"
  ]
  node [
    id 361
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 362
    label "Malta"
  ]
  node [
    id 363
    label "Andora"
  ]
  node [
    id 364
    label "Chiny"
  ]
  node [
    id 365
    label "Filipiny"
  ]
  node [
    id 366
    label "Antarktis"
  ]
  node [
    id 367
    label "Niemcy"
  ]
  node [
    id 368
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 369
    label "Pakistan"
  ]
  node [
    id 370
    label "terytorium"
  ]
  node [
    id 371
    label "Nikaragua"
  ]
  node [
    id 372
    label "Brazylia"
  ]
  node [
    id 373
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 374
    label "Maroko"
  ]
  node [
    id 375
    label "Portugalia"
  ]
  node [
    id 376
    label "Niger"
  ]
  node [
    id 377
    label "Kenia"
  ]
  node [
    id 378
    label "Botswana"
  ]
  node [
    id 379
    label "Fid&#380;i"
  ]
  node [
    id 380
    label "Tunezja"
  ]
  node [
    id 381
    label "Australia"
  ]
  node [
    id 382
    label "Tajlandia"
  ]
  node [
    id 383
    label "Burkina_Faso"
  ]
  node [
    id 384
    label "interior"
  ]
  node [
    id 385
    label "Tanzania"
  ]
  node [
    id 386
    label "Benin"
  ]
  node [
    id 387
    label "Indie"
  ]
  node [
    id 388
    label "&#321;otwa"
  ]
  node [
    id 389
    label "Kiribati"
  ]
  node [
    id 390
    label "Antigua_i_Barbuda"
  ]
  node [
    id 391
    label "Rodezja"
  ]
  node [
    id 392
    label "Cypr"
  ]
  node [
    id 393
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 394
    label "Peru"
  ]
  node [
    id 395
    label "Austria"
  ]
  node [
    id 396
    label "Urugwaj"
  ]
  node [
    id 397
    label "Jordania"
  ]
  node [
    id 398
    label "Grecja"
  ]
  node [
    id 399
    label "Azerbejd&#380;an"
  ]
  node [
    id 400
    label "Turcja"
  ]
  node [
    id 401
    label "Samoa"
  ]
  node [
    id 402
    label "Sudan"
  ]
  node [
    id 403
    label "Oman"
  ]
  node [
    id 404
    label "ziemia"
  ]
  node [
    id 405
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 406
    label "Uzbekistan"
  ]
  node [
    id 407
    label "Portoryko"
  ]
  node [
    id 408
    label "Honduras"
  ]
  node [
    id 409
    label "Mongolia"
  ]
  node [
    id 410
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 411
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 412
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 413
    label "Serbia"
  ]
  node [
    id 414
    label "Tajwan"
  ]
  node [
    id 415
    label "Wielka_Brytania"
  ]
  node [
    id 416
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 417
    label "Liban"
  ]
  node [
    id 418
    label "Japonia"
  ]
  node [
    id 419
    label "Ghana"
  ]
  node [
    id 420
    label "Belgia"
  ]
  node [
    id 421
    label "Bahrajn"
  ]
  node [
    id 422
    label "Mikronezja"
  ]
  node [
    id 423
    label "Etiopia"
  ]
  node [
    id 424
    label "Kuwejt"
  ]
  node [
    id 425
    label "grupa"
  ]
  node [
    id 426
    label "Bahamy"
  ]
  node [
    id 427
    label "Rosja"
  ]
  node [
    id 428
    label "Mo&#322;dawia"
  ]
  node [
    id 429
    label "Litwa"
  ]
  node [
    id 430
    label "S&#322;owenia"
  ]
  node [
    id 431
    label "Szwajcaria"
  ]
  node [
    id 432
    label "Erytrea"
  ]
  node [
    id 433
    label "Arabia_Saudyjska"
  ]
  node [
    id 434
    label "Kuba"
  ]
  node [
    id 435
    label "granica_pa&#324;stwa"
  ]
  node [
    id 436
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 437
    label "Malezja"
  ]
  node [
    id 438
    label "Korea"
  ]
  node [
    id 439
    label "Jemen"
  ]
  node [
    id 440
    label "Nowa_Zelandia"
  ]
  node [
    id 441
    label "Namibia"
  ]
  node [
    id 442
    label "Nauru"
  ]
  node [
    id 443
    label "holoarktyka"
  ]
  node [
    id 444
    label "Brunei"
  ]
  node [
    id 445
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 446
    label "Khitai"
  ]
  node [
    id 447
    label "Mauretania"
  ]
  node [
    id 448
    label "Iran"
  ]
  node [
    id 449
    label "Gambia"
  ]
  node [
    id 450
    label "Somalia"
  ]
  node [
    id 451
    label "Holandia"
  ]
  node [
    id 452
    label "Turkmenistan"
  ]
  node [
    id 453
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 454
    label "Salwador"
  ]
  node [
    id 455
    label "Borewicz"
  ]
  node [
    id 456
    label "lejtnant"
  ]
  node [
    id 457
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 458
    label "oficer"
  ]
  node [
    id 459
    label "podchor&#261;&#380;y"
  ]
  node [
    id 460
    label "podoficer"
  ]
  node [
    id 461
    label "mundurowy"
  ]
  node [
    id 462
    label "gaworzy&#263;"
  ]
  node [
    id 463
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 464
    label "remark"
  ]
  node [
    id 465
    label "rozmawia&#263;"
  ]
  node [
    id 466
    label "wyra&#380;a&#263;"
  ]
  node [
    id 467
    label "umie&#263;"
  ]
  node [
    id 468
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 469
    label "dziama&#263;"
  ]
  node [
    id 470
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 471
    label "formu&#322;owa&#263;"
  ]
  node [
    id 472
    label "dysfonia"
  ]
  node [
    id 473
    label "express"
  ]
  node [
    id 474
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 475
    label "talk"
  ]
  node [
    id 476
    label "u&#380;ywa&#263;"
  ]
  node [
    id 477
    label "prawi&#263;"
  ]
  node [
    id 478
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 479
    label "powiada&#263;"
  ]
  node [
    id 480
    label "tell"
  ]
  node [
    id 481
    label "chew_the_fat"
  ]
  node [
    id 482
    label "say"
  ]
  node [
    id 483
    label "j&#281;zyk"
  ]
  node [
    id 484
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 485
    label "informowa&#263;"
  ]
  node [
    id 486
    label "wydobywa&#263;"
  ]
  node [
    id 487
    label "okre&#347;la&#263;"
  ]
  node [
    id 488
    label "korzysta&#263;"
  ]
  node [
    id 489
    label "distribute"
  ]
  node [
    id 490
    label "give"
  ]
  node [
    id 491
    label "bash"
  ]
  node [
    id 492
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 493
    label "doznawa&#263;"
  ]
  node [
    id 494
    label "decydowa&#263;"
  ]
  node [
    id 495
    label "signify"
  ]
  node [
    id 496
    label "style"
  ]
  node [
    id 497
    label "powodowa&#263;"
  ]
  node [
    id 498
    label "komunikowa&#263;"
  ]
  node [
    id 499
    label "inform"
  ]
  node [
    id 500
    label "znaczy&#263;"
  ]
  node [
    id 501
    label "give_voice"
  ]
  node [
    id 502
    label "oznacza&#263;"
  ]
  node [
    id 503
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 504
    label "represent"
  ]
  node [
    id 505
    label "convey"
  ]
  node [
    id 506
    label "arouse"
  ]
  node [
    id 507
    label "robi&#263;"
  ]
  node [
    id 508
    label "determine"
  ]
  node [
    id 509
    label "work"
  ]
  node [
    id 510
    label "reakcja_chemiczna"
  ]
  node [
    id 511
    label "uwydatnia&#263;"
  ]
  node [
    id 512
    label "eksploatowa&#263;"
  ]
  node [
    id 513
    label "uzyskiwa&#263;"
  ]
  node [
    id 514
    label "wydostawa&#263;"
  ]
  node [
    id 515
    label "wyjmowa&#263;"
  ]
  node [
    id 516
    label "train"
  ]
  node [
    id 517
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 518
    label "wydawa&#263;"
  ]
  node [
    id 519
    label "dobywa&#263;"
  ]
  node [
    id 520
    label "ocala&#263;"
  ]
  node [
    id 521
    label "excavate"
  ]
  node [
    id 522
    label "g&#243;rnictwo"
  ]
  node [
    id 523
    label "raise"
  ]
  node [
    id 524
    label "wiedzie&#263;"
  ]
  node [
    id 525
    label "can"
  ]
  node [
    id 526
    label "m&#243;c"
  ]
  node [
    id 527
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 528
    label "rozumie&#263;"
  ]
  node [
    id 529
    label "szczeka&#263;"
  ]
  node [
    id 530
    label "funkcjonowa&#263;"
  ]
  node [
    id 531
    label "mawia&#263;"
  ]
  node [
    id 532
    label "opowiada&#263;"
  ]
  node [
    id 533
    label "chatter"
  ]
  node [
    id 534
    label "niemowl&#281;"
  ]
  node [
    id 535
    label "kosmetyk"
  ]
  node [
    id 536
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 537
    label "stanowisko_archeologiczne"
  ]
  node [
    id 538
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 539
    label "artykulator"
  ]
  node [
    id 540
    label "kod"
  ]
  node [
    id 541
    label "kawa&#322;ek"
  ]
  node [
    id 542
    label "przedmiot"
  ]
  node [
    id 543
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 544
    label "gramatyka"
  ]
  node [
    id 545
    label "stylik"
  ]
  node [
    id 546
    label "przet&#322;umaczenie"
  ]
  node [
    id 547
    label "formalizowanie"
  ]
  node [
    id 548
    label "ssa&#263;"
  ]
  node [
    id 549
    label "ssanie"
  ]
  node [
    id 550
    label "language"
  ]
  node [
    id 551
    label "liza&#263;"
  ]
  node [
    id 552
    label "napisa&#263;"
  ]
  node [
    id 553
    label "konsonantyzm"
  ]
  node [
    id 554
    label "wokalizm"
  ]
  node [
    id 555
    label "pisa&#263;"
  ]
  node [
    id 556
    label "fonetyka"
  ]
  node [
    id 557
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 558
    label "jeniec"
  ]
  node [
    id 559
    label "but"
  ]
  node [
    id 560
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 561
    label "po_koroniarsku"
  ]
  node [
    id 562
    label "kultura_duchowa"
  ]
  node [
    id 563
    label "t&#322;umaczenie"
  ]
  node [
    id 564
    label "m&#243;wienie"
  ]
  node [
    id 565
    label "pype&#263;"
  ]
  node [
    id 566
    label "lizanie"
  ]
  node [
    id 567
    label "pismo"
  ]
  node [
    id 568
    label "formalizowa&#263;"
  ]
  node [
    id 569
    label "organ"
  ]
  node [
    id 570
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 571
    label "rozumienie"
  ]
  node [
    id 572
    label "spos&#243;b"
  ]
  node [
    id 573
    label "makroglosja"
  ]
  node [
    id 574
    label "jama_ustna"
  ]
  node [
    id 575
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 576
    label "formacja_geologiczna"
  ]
  node [
    id 577
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 578
    label "natural_language"
  ]
  node [
    id 579
    label "s&#322;ownictwo"
  ]
  node [
    id 580
    label "urz&#261;dzenie"
  ]
  node [
    id 581
    label "dysphonia"
  ]
  node [
    id 582
    label "dysleksja"
  ]
  node [
    id 583
    label "s&#261;d"
  ]
  node [
    id 584
    label "wytw&#243;r"
  ]
  node [
    id 585
    label "p&#322;&#243;d"
  ]
  node [
    id 586
    label "thinking"
  ]
  node [
    id 587
    label "umys&#322;"
  ]
  node [
    id 588
    label "political_orientation"
  ]
  node [
    id 589
    label "istota"
  ]
  node [
    id 590
    label "pomys&#322;"
  ]
  node [
    id 591
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 592
    label "idea"
  ]
  node [
    id 593
    label "system"
  ]
  node [
    id 594
    label "fantomatyka"
  ]
  node [
    id 595
    label "pami&#281;&#263;"
  ]
  node [
    id 596
    label "intelekt"
  ]
  node [
    id 597
    label "pomieszanie_si&#281;"
  ]
  node [
    id 598
    label "wn&#281;trze"
  ]
  node [
    id 599
    label "wyobra&#378;nia"
  ]
  node [
    id 600
    label "rezultat"
  ]
  node [
    id 601
    label "mentalno&#347;&#263;"
  ]
  node [
    id 602
    label "superego"
  ]
  node [
    id 603
    label "psychika"
  ]
  node [
    id 604
    label "znaczenie"
  ]
  node [
    id 605
    label "charakter"
  ]
  node [
    id 606
    label "cecha"
  ]
  node [
    id 607
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 608
    label "moczownik"
  ]
  node [
    id 609
    label "embryo"
  ]
  node [
    id 610
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 611
    label "zarodek"
  ]
  node [
    id 612
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 613
    label "latawiec"
  ]
  node [
    id 614
    label "j&#261;dro"
  ]
  node [
    id 615
    label "systemik"
  ]
  node [
    id 616
    label "rozprz&#261;c"
  ]
  node [
    id 617
    label "oprogramowanie"
  ]
  node [
    id 618
    label "poj&#281;cie"
  ]
  node [
    id 619
    label "systemat"
  ]
  node [
    id 620
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 621
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 622
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 623
    label "model"
  ]
  node [
    id 624
    label "struktura"
  ]
  node [
    id 625
    label "usenet"
  ]
  node [
    id 626
    label "zbi&#243;r"
  ]
  node [
    id 627
    label "porz&#261;dek"
  ]
  node [
    id 628
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 629
    label "przyn&#281;ta"
  ]
  node [
    id 630
    label "net"
  ]
  node [
    id 631
    label "w&#281;dkarstwo"
  ]
  node [
    id 632
    label "eratem"
  ]
  node [
    id 633
    label "oddzia&#322;"
  ]
  node [
    id 634
    label "doktryna"
  ]
  node [
    id 635
    label "pulpit"
  ]
  node [
    id 636
    label "konstelacja"
  ]
  node [
    id 637
    label "jednostka_geologiczna"
  ]
  node [
    id 638
    label "o&#347;"
  ]
  node [
    id 639
    label "podsystem"
  ]
  node [
    id 640
    label "metoda"
  ]
  node [
    id 641
    label "ryba"
  ]
  node [
    id 642
    label "Leopard"
  ]
  node [
    id 643
    label "Android"
  ]
  node [
    id 644
    label "zachowanie"
  ]
  node [
    id 645
    label "cybernetyk"
  ]
  node [
    id 646
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 647
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 648
    label "method"
  ]
  node [
    id 649
    label "sk&#322;ad"
  ]
  node [
    id 650
    label "podstawa"
  ]
  node [
    id 651
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 652
    label "zesp&#243;&#322;"
  ]
  node [
    id 653
    label "podejrzany"
  ]
  node [
    id 654
    label "s&#261;downictwo"
  ]
  node [
    id 655
    label "biuro"
  ]
  node [
    id 656
    label "court"
  ]
  node [
    id 657
    label "forum"
  ]
  node [
    id 658
    label "bronienie"
  ]
  node [
    id 659
    label "urz&#261;d"
  ]
  node [
    id 660
    label "wydarzenie"
  ]
  node [
    id 661
    label "oskar&#380;yciel"
  ]
  node [
    id 662
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 663
    label "skazany"
  ]
  node [
    id 664
    label "post&#281;powanie"
  ]
  node [
    id 665
    label "broni&#263;"
  ]
  node [
    id 666
    label "pods&#261;dny"
  ]
  node [
    id 667
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 668
    label "obrona"
  ]
  node [
    id 669
    label "wypowied&#378;"
  ]
  node [
    id 670
    label "instytucja"
  ]
  node [
    id 671
    label "antylogizm"
  ]
  node [
    id 672
    label "konektyw"
  ]
  node [
    id 673
    label "&#347;wiadek"
  ]
  node [
    id 674
    label "procesowicz"
  ]
  node [
    id 675
    label "strona"
  ]
  node [
    id 676
    label "technika"
  ]
  node [
    id 677
    label "pocz&#261;tki"
  ]
  node [
    id 678
    label "ukra&#347;&#263;"
  ]
  node [
    id 679
    label "ukradzenie"
  ]
  node [
    id 680
    label "do&#347;wiadczenie"
  ]
  node [
    id 681
    label "teren_szko&#322;y"
  ]
  node [
    id 682
    label "wiedza"
  ]
  node [
    id 683
    label "Mickiewicz"
  ]
  node [
    id 684
    label "kwalifikacje"
  ]
  node [
    id 685
    label "podr&#281;cznik"
  ]
  node [
    id 686
    label "praktyka"
  ]
  node [
    id 687
    label "school"
  ]
  node [
    id 688
    label "zda&#263;"
  ]
  node [
    id 689
    label "gabinet"
  ]
  node [
    id 690
    label "urszulanki"
  ]
  node [
    id 691
    label "sztuba"
  ]
  node [
    id 692
    label "&#322;awa_szkolna"
  ]
  node [
    id 693
    label "nauka"
  ]
  node [
    id 694
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 695
    label "przepisa&#263;"
  ]
  node [
    id 696
    label "muzyka"
  ]
  node [
    id 697
    label "form"
  ]
  node [
    id 698
    label "lekcja"
  ]
  node [
    id 699
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 700
    label "przepisanie"
  ]
  node [
    id 701
    label "czas"
  ]
  node [
    id 702
    label "skolaryzacja"
  ]
  node [
    id 703
    label "zdanie"
  ]
  node [
    id 704
    label "stopek"
  ]
  node [
    id 705
    label "sekretariat"
  ]
  node [
    id 706
    label "ideologia"
  ]
  node [
    id 707
    label "lesson"
  ]
  node [
    id 708
    label "niepokalanki"
  ]
  node [
    id 709
    label "siedziba"
  ]
  node [
    id 710
    label "szkolenie"
  ]
  node [
    id 711
    label "kara"
  ]
  node [
    id 712
    label "tablica"
  ]
  node [
    id 713
    label "byt"
  ]
  node [
    id 714
    label "Kant"
  ]
  node [
    id 715
    label "cel"
  ]
  node [
    id 716
    label "ideacja"
  ]
  node [
    id 717
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 718
    label "mie&#263;_miejsce"
  ]
  node [
    id 719
    label "equal"
  ]
  node [
    id 720
    label "trwa&#263;"
  ]
  node [
    id 721
    label "chodzi&#263;"
  ]
  node [
    id 722
    label "si&#281;ga&#263;"
  ]
  node [
    id 723
    label "stan"
  ]
  node [
    id 724
    label "obecno&#347;&#263;"
  ]
  node [
    id 725
    label "stand"
  ]
  node [
    id 726
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 727
    label "uczestniczy&#263;"
  ]
  node [
    id 728
    label "participate"
  ]
  node [
    id 729
    label "istnie&#263;"
  ]
  node [
    id 730
    label "pozostawa&#263;"
  ]
  node [
    id 731
    label "zostawa&#263;"
  ]
  node [
    id 732
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 733
    label "adhere"
  ]
  node [
    id 734
    label "compass"
  ]
  node [
    id 735
    label "appreciation"
  ]
  node [
    id 736
    label "osi&#261;ga&#263;"
  ]
  node [
    id 737
    label "dociera&#263;"
  ]
  node [
    id 738
    label "get"
  ]
  node [
    id 739
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 740
    label "mierzy&#263;"
  ]
  node [
    id 741
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 742
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 743
    label "exsert"
  ]
  node [
    id 744
    label "being"
  ]
  node [
    id 745
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 746
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 747
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 748
    label "p&#322;ywa&#263;"
  ]
  node [
    id 749
    label "run"
  ]
  node [
    id 750
    label "bangla&#263;"
  ]
  node [
    id 751
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 752
    label "przebiega&#263;"
  ]
  node [
    id 753
    label "wk&#322;ada&#263;"
  ]
  node [
    id 754
    label "proceed"
  ]
  node [
    id 755
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 756
    label "carry"
  ]
  node [
    id 757
    label "bywa&#263;"
  ]
  node [
    id 758
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 759
    label "stara&#263;_si&#281;"
  ]
  node [
    id 760
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 761
    label "str&#243;j"
  ]
  node [
    id 762
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 763
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 764
    label "krok"
  ]
  node [
    id 765
    label "tryb"
  ]
  node [
    id 766
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 767
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 768
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 769
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 770
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 771
    label "continue"
  ]
  node [
    id 772
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 773
    label "Ohio"
  ]
  node [
    id 774
    label "wci&#281;cie"
  ]
  node [
    id 775
    label "Nowy_York"
  ]
  node [
    id 776
    label "warstwa"
  ]
  node [
    id 777
    label "samopoczucie"
  ]
  node [
    id 778
    label "Illinois"
  ]
  node [
    id 779
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 780
    label "state"
  ]
  node [
    id 781
    label "Jukatan"
  ]
  node [
    id 782
    label "Kalifornia"
  ]
  node [
    id 783
    label "Wirginia"
  ]
  node [
    id 784
    label "wektor"
  ]
  node [
    id 785
    label "Goa"
  ]
  node [
    id 786
    label "Teksas"
  ]
  node [
    id 787
    label "Waszyngton"
  ]
  node [
    id 788
    label "miejsce"
  ]
  node [
    id 789
    label "Massachusetts"
  ]
  node [
    id 790
    label "Alaska"
  ]
  node [
    id 791
    label "Arakan"
  ]
  node [
    id 792
    label "Hawaje"
  ]
  node [
    id 793
    label "Maryland"
  ]
  node [
    id 794
    label "Michigan"
  ]
  node [
    id 795
    label "Arizona"
  ]
  node [
    id 796
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 797
    label "Georgia"
  ]
  node [
    id 798
    label "poziom"
  ]
  node [
    id 799
    label "Pensylwania"
  ]
  node [
    id 800
    label "shape"
  ]
  node [
    id 801
    label "Luizjana"
  ]
  node [
    id 802
    label "Nowy_Meksyk"
  ]
  node [
    id 803
    label "Alabama"
  ]
  node [
    id 804
    label "ilo&#347;&#263;"
  ]
  node [
    id 805
    label "Kansas"
  ]
  node [
    id 806
    label "Oregon"
  ]
  node [
    id 807
    label "Oklahoma"
  ]
  node [
    id 808
    label "Floryda"
  ]
  node [
    id 809
    label "jednostka_administracyjna"
  ]
  node [
    id 810
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 811
    label "brygant"
  ]
  node [
    id 812
    label "bandyta"
  ]
  node [
    id 813
    label "przest&#281;pca"
  ]
  node [
    id 814
    label "apasz"
  ]
  node [
    id 815
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 816
    label "rewolucjonista"
  ]
  node [
    id 817
    label "bojowo"
  ]
  node [
    id 818
    label "typowy"
  ]
  node [
    id 819
    label "zwyczajny"
  ]
  node [
    id 820
    label "typowo"
  ]
  node [
    id 821
    label "cz&#281;sty"
  ]
  node [
    id 822
    label "zwyk&#322;y"
  ]
  node [
    id 823
    label "&#347;mia&#322;o"
  ]
  node [
    id 824
    label "wojennie"
  ]
  node [
    id 825
    label "walecznie"
  ]
  node [
    id 826
    label "pewnie"
  ]
  node [
    id 827
    label "zadziornie"
  ]
  node [
    id 828
    label "bojowy"
  ]
  node [
    id 829
    label "znaczny"
  ]
  node [
    id 830
    label "wyj&#261;tkowy"
  ]
  node [
    id 831
    label "nieprzeci&#281;tny"
  ]
  node [
    id 832
    label "wysoce"
  ]
  node [
    id 833
    label "wa&#380;ny"
  ]
  node [
    id 834
    label "prawdziwy"
  ]
  node [
    id 835
    label "wybitny"
  ]
  node [
    id 836
    label "dupny"
  ]
  node [
    id 837
    label "wysoki"
  ]
  node [
    id 838
    label "intensywnie"
  ]
  node [
    id 839
    label "nieprzeci&#281;tnie"
  ]
  node [
    id 840
    label "niespotykany"
  ]
  node [
    id 841
    label "wydatny"
  ]
  node [
    id 842
    label "wspania&#322;y"
  ]
  node [
    id 843
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 844
    label "&#347;wietny"
  ]
  node [
    id 845
    label "imponuj&#261;cy"
  ]
  node [
    id 846
    label "wybitnie"
  ]
  node [
    id 847
    label "celny"
  ]
  node [
    id 848
    label "&#380;ywny"
  ]
  node [
    id 849
    label "szczery"
  ]
  node [
    id 850
    label "naturalny"
  ]
  node [
    id 851
    label "naprawd&#281;"
  ]
  node [
    id 852
    label "realnie"
  ]
  node [
    id 853
    label "podobny"
  ]
  node [
    id 854
    label "zgodny"
  ]
  node [
    id 855
    label "prawdziwie"
  ]
  node [
    id 856
    label "wyj&#261;tkowo"
  ]
  node [
    id 857
    label "inny"
  ]
  node [
    id 858
    label "znacznie"
  ]
  node [
    id 859
    label "zauwa&#380;alny"
  ]
  node [
    id 860
    label "wynios&#322;y"
  ]
  node [
    id 861
    label "dono&#347;ny"
  ]
  node [
    id 862
    label "silny"
  ]
  node [
    id 863
    label "wa&#380;nie"
  ]
  node [
    id 864
    label "istotnie"
  ]
  node [
    id 865
    label "eksponowany"
  ]
  node [
    id 866
    label "dobry"
  ]
  node [
    id 867
    label "do_dupy"
  ]
  node [
    id 868
    label "z&#322;y"
  ]
  node [
    id 869
    label "sprawiciel"
  ]
  node [
    id 870
    label "nemezis"
  ]
  node [
    id 871
    label "konsekwencja"
  ]
  node [
    id 872
    label "punishment"
  ]
  node [
    id 873
    label "righteousness"
  ]
  node [
    id 874
    label "roboty_przymusowe"
  ]
  node [
    id 875
    label "odczuwa&#263;"
  ]
  node [
    id 876
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 877
    label "skrupienie_si&#281;"
  ]
  node [
    id 878
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 879
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 880
    label "odczucie"
  ]
  node [
    id 881
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 882
    label "koszula_Dejaniry"
  ]
  node [
    id 883
    label "odczuwanie"
  ]
  node [
    id 884
    label "event"
  ]
  node [
    id 885
    label "skrupianie_si&#281;"
  ]
  node [
    id 886
    label "odczu&#263;"
  ]
  node [
    id 887
    label "charakterystyka"
  ]
  node [
    id 888
    label "m&#322;ot"
  ]
  node [
    id 889
    label "znak"
  ]
  node [
    id 890
    label "drzewo"
  ]
  node [
    id 891
    label "pr&#243;ba"
  ]
  node [
    id 892
    label "attribute"
  ]
  node [
    id 893
    label "marka"
  ]
  node [
    id 894
    label "jedyny"
  ]
  node [
    id 895
    label "zdr&#243;w"
  ]
  node [
    id 896
    label "calu&#347;ko"
  ]
  node [
    id 897
    label "kompletny"
  ]
  node [
    id 898
    label "&#380;ywy"
  ]
  node [
    id 899
    label "pe&#322;ny"
  ]
  node [
    id 900
    label "ca&#322;o"
  ]
  node [
    id 901
    label "kompletnie"
  ]
  node [
    id 902
    label "zupe&#322;ny"
  ]
  node [
    id 903
    label "w_pizdu"
  ]
  node [
    id 904
    label "przypominanie"
  ]
  node [
    id 905
    label "podobnie"
  ]
  node [
    id 906
    label "upodabnianie_si&#281;"
  ]
  node [
    id 907
    label "upodobnienie"
  ]
  node [
    id 908
    label "drugi"
  ]
  node [
    id 909
    label "taki"
  ]
  node [
    id 910
    label "charakterystyczny"
  ]
  node [
    id 911
    label "upodobnienie_si&#281;"
  ]
  node [
    id 912
    label "zasymilowanie"
  ]
  node [
    id 913
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 914
    label "ukochany"
  ]
  node [
    id 915
    label "najlepszy"
  ]
  node [
    id 916
    label "optymalnie"
  ]
  node [
    id 917
    label "niema&#322;o"
  ]
  node [
    id 918
    label "wiele"
  ]
  node [
    id 919
    label "rozwini&#281;ty"
  ]
  node [
    id 920
    label "dorodny"
  ]
  node [
    id 921
    label "du&#380;o"
  ]
  node [
    id 922
    label "zdrowy"
  ]
  node [
    id 923
    label "ciekawy"
  ]
  node [
    id 924
    label "szybki"
  ]
  node [
    id 925
    label "&#380;ywotny"
  ]
  node [
    id 926
    label "&#380;ywo"
  ]
  node [
    id 927
    label "o&#380;ywianie"
  ]
  node [
    id 928
    label "g&#322;&#281;boki"
  ]
  node [
    id 929
    label "wyra&#378;ny"
  ]
  node [
    id 930
    label "czynny"
  ]
  node [
    id 931
    label "aktualny"
  ]
  node [
    id 932
    label "zgrabny"
  ]
  node [
    id 933
    label "realistyczny"
  ]
  node [
    id 934
    label "energiczny"
  ]
  node [
    id 935
    label "nieograniczony"
  ]
  node [
    id 936
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 937
    label "satysfakcja"
  ]
  node [
    id 938
    label "bezwzgl&#281;dny"
  ]
  node [
    id 939
    label "otwarty"
  ]
  node [
    id 940
    label "wype&#322;nienie"
  ]
  node [
    id 941
    label "pe&#322;no"
  ]
  node [
    id 942
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 943
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 944
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 945
    label "r&#243;wny"
  ]
  node [
    id 946
    label "nieuszkodzony"
  ]
  node [
    id 947
    label "odpowiednio"
  ]
  node [
    id 948
    label "shot"
  ]
  node [
    id 949
    label "jednakowy"
  ]
  node [
    id 950
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 951
    label "ujednolicenie"
  ]
  node [
    id 952
    label "jaki&#347;"
  ]
  node [
    id 953
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 954
    label "jednolicie"
  ]
  node [
    id 955
    label "kieliszek"
  ]
  node [
    id 956
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 957
    label "w&#243;dka"
  ]
  node [
    id 958
    label "szk&#322;o"
  ]
  node [
    id 959
    label "zawarto&#347;&#263;"
  ]
  node [
    id 960
    label "naczynie"
  ]
  node [
    id 961
    label "alkohol"
  ]
  node [
    id 962
    label "sznaps"
  ]
  node [
    id 963
    label "nap&#243;j"
  ]
  node [
    id 964
    label "gorza&#322;ka"
  ]
  node [
    id 965
    label "mohorycz"
  ]
  node [
    id 966
    label "okre&#347;lony"
  ]
  node [
    id 967
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 968
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 969
    label "zr&#243;wnanie"
  ]
  node [
    id 970
    label "mundurowanie"
  ]
  node [
    id 971
    label "taki&#380;"
  ]
  node [
    id 972
    label "jednakowo"
  ]
  node [
    id 973
    label "mundurowa&#263;"
  ]
  node [
    id 974
    label "zr&#243;wnywanie"
  ]
  node [
    id 975
    label "identyczny"
  ]
  node [
    id 976
    label "z&#322;o&#380;ony"
  ]
  node [
    id 977
    label "przyzwoity"
  ]
  node [
    id 978
    label "jako&#347;"
  ]
  node [
    id 979
    label "jako_tako"
  ]
  node [
    id 980
    label "niez&#322;y"
  ]
  node [
    id 981
    label "dziwny"
  ]
  node [
    id 982
    label "g&#322;&#281;bszy"
  ]
  node [
    id 983
    label "drink"
  ]
  node [
    id 984
    label "jednolity"
  ]
  node [
    id 985
    label "calibration"
  ]
  node [
    id 986
    label "judiciary"
  ]
  node [
    id 987
    label "prawo"
  ]
  node [
    id 988
    label "panowanie"
  ]
  node [
    id 989
    label "Kreml"
  ]
  node [
    id 990
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 991
    label "wydolno&#347;&#263;"
  ]
  node [
    id 992
    label "rz&#261;d"
  ]
  node [
    id 993
    label "spadkobierca"
  ]
  node [
    id 994
    label "ziemianin"
  ]
  node [
    id 995
    label "legitymowa&#263;"
  ]
  node [
    id 996
    label "legitymowanie"
  ]
  node [
    id 997
    label "ziemia&#324;stwo"
  ]
  node [
    id 998
    label "chor&#261;&#380;y"
  ]
  node [
    id 999
    label "dw&#243;r"
  ]
  node [
    id 1000
    label "szlachcic"
  ]
  node [
    id 1001
    label "uw&#322;aszczy&#263;"
  ]
  node [
    id 1002
    label "uw&#322;aszcza&#263;"
  ]
  node [
    id 1003
    label "rolnik"
  ]
  node [
    id 1004
    label "ch&#322;opstwo"
  ]
  node [
    id 1005
    label "cham"
  ]
  node [
    id 1006
    label "bamber"
  ]
  node [
    id 1007
    label "partner"
  ]
  node [
    id 1008
    label "uw&#322;aszczanie"
  ]
  node [
    id 1009
    label "prawo_wychodu"
  ]
  node [
    id 1010
    label "w&#322;o&#347;cianin"
  ]
  node [
    id 1011
    label "przedstawiciel"
  ]
  node [
    id 1012
    label "twardziel"
  ]
  node [
    id 1013
    label "pracownik"
  ]
  node [
    id 1014
    label "przedsi&#281;biorca"
  ]
  node [
    id 1015
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 1016
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 1017
    label "kolaborator"
  ]
  node [
    id 1018
    label "prowadzi&#263;"
  ]
  node [
    id 1019
    label "sp&#243;lnik"
  ]
  node [
    id 1020
    label "aktor"
  ]
  node [
    id 1021
    label "uczestniczenie"
  ]
  node [
    id 1022
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 1023
    label "cz&#322;onek"
  ]
  node [
    id 1024
    label "substytuowa&#263;"
  ]
  node [
    id 1025
    label "substytuowanie"
  ]
  node [
    id 1026
    label "zast&#281;pca"
  ]
  node [
    id 1027
    label "wie&#347;niak"
  ]
  node [
    id 1028
    label "nadawanie"
  ]
  node [
    id 1029
    label "enfranchise"
  ]
  node [
    id 1030
    label "nadawa&#263;"
  ]
  node [
    id 1031
    label "nada&#263;"
  ]
  node [
    id 1032
    label "ludno&#347;&#263;"
  ]
  node [
    id 1033
    label "chamstwo"
  ]
  node [
    id 1034
    label "gmin"
  ]
  node [
    id 1035
    label "osadnik"
  ]
  node [
    id 1036
    label "Niemiec"
  ]
  node [
    id 1037
    label "prostak"
  ]
  node [
    id 1038
    label "chamski"
  ]
  node [
    id 1039
    label "oficjalista"
  ]
  node [
    id 1040
    label "urz&#281;dnik_s&#261;dowy"
  ]
  node [
    id 1041
    label "urz&#281;dnik_nadworny"
  ]
  node [
    id 1042
    label "skrzywdzi&#263;"
  ]
  node [
    id 1043
    label "zaszkodzi&#263;"
  ]
  node [
    id 1044
    label "niesprawiedliwy"
  ]
  node [
    id 1045
    label "spowodowa&#263;"
  ]
  node [
    id 1046
    label "hurt"
  ]
  node [
    id 1047
    label "wrong"
  ]
  node [
    id 1048
    label "wiadomy"
  ]
  node [
    id 1049
    label "mi&#281;sie&#324;_dwug&#322;owy_uda"
  ]
  node [
    id 1050
    label "mi&#281;sie&#324;_czworog&#322;owy"
  ]
  node [
    id 1051
    label "noga"
  ]
  node [
    id 1052
    label "t&#281;tnica_udowa"
  ]
  node [
    id 1053
    label "struktura_anatomiczna"
  ]
  node [
    id 1054
    label "kr&#281;tarz"
  ]
  node [
    id 1055
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1056
    label "Rzym_Zachodni"
  ]
  node [
    id 1057
    label "whole"
  ]
  node [
    id 1058
    label "element"
  ]
  node [
    id 1059
    label "Rzym_Wschodni"
  ]
  node [
    id 1060
    label "odn&#243;&#380;e"
  ]
  node [
    id 1061
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 1062
    label "ko&#347;&#263;"
  ]
  node [
    id 1063
    label "dogrywa&#263;"
  ]
  node [
    id 1064
    label "s&#322;abeusz"
  ]
  node [
    id 1065
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 1066
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 1067
    label "czpas"
  ]
  node [
    id 1068
    label "nerw_udowy"
  ]
  node [
    id 1069
    label "bezbramkowy"
  ]
  node [
    id 1070
    label "podpora"
  ]
  node [
    id 1071
    label "faulowa&#263;"
  ]
  node [
    id 1072
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 1073
    label "zamurowanie"
  ]
  node [
    id 1074
    label "depta&#263;"
  ]
  node [
    id 1075
    label "mi&#281;czak"
  ]
  node [
    id 1076
    label "stopa"
  ]
  node [
    id 1077
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 1078
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 1079
    label "mato&#322;"
  ]
  node [
    id 1080
    label "ekstraklasa"
  ]
  node [
    id 1081
    label "sfaulowa&#263;"
  ]
  node [
    id 1082
    label "&#322;&#261;czyna"
  ]
  node [
    id 1083
    label "lobowanie"
  ]
  node [
    id 1084
    label "dogrywanie"
  ]
  node [
    id 1085
    label "napinacz"
  ]
  node [
    id 1086
    label "dublet"
  ]
  node [
    id 1087
    label "sfaulowanie"
  ]
  node [
    id 1088
    label "lobowa&#263;"
  ]
  node [
    id 1089
    label "gira"
  ]
  node [
    id 1090
    label "bramkarz"
  ]
  node [
    id 1091
    label "zamurowywanie"
  ]
  node [
    id 1092
    label "kopni&#281;cie"
  ]
  node [
    id 1093
    label "faulowanie"
  ]
  node [
    id 1094
    label "&#322;amaga"
  ]
  node [
    id 1095
    label "kopn&#261;&#263;"
  ]
  node [
    id 1096
    label "kopanie"
  ]
  node [
    id 1097
    label "dogranie"
  ]
  node [
    id 1098
    label "pi&#322;ka"
  ]
  node [
    id 1099
    label "przelobowa&#263;"
  ]
  node [
    id 1100
    label "mundial"
  ]
  node [
    id 1101
    label "catenaccio"
  ]
  node [
    id 1102
    label "r&#281;ka"
  ]
  node [
    id 1103
    label "kopa&#263;"
  ]
  node [
    id 1104
    label "dogra&#263;"
  ]
  node [
    id 1105
    label "ko&#324;czyna"
  ]
  node [
    id 1106
    label "tackle"
  ]
  node [
    id 1107
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 1108
    label "narz&#261;d_ruchu"
  ]
  node [
    id 1109
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 1110
    label "interliga"
  ]
  node [
    id 1111
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 1112
    label "zamurowywa&#263;"
  ]
  node [
    id 1113
    label "przelobowanie"
  ]
  node [
    id 1114
    label "czerwona_kartka"
  ]
  node [
    id 1115
    label "Wis&#322;a"
  ]
  node [
    id 1116
    label "zamurowa&#263;"
  ]
  node [
    id 1117
    label "jedenastka"
  ]
  node [
    id 1118
    label "request"
  ]
  node [
    id 1119
    label "akt_oskar&#380;enia"
  ]
  node [
    id 1120
    label "skwierk"
  ]
  node [
    id 1121
    label "zawiadomienie"
  ]
  node [
    id 1122
    label "odwo&#322;anie"
  ]
  node [
    id 1123
    label "pos&#322;uchanie"
  ]
  node [
    id 1124
    label "sparafrazowanie"
  ]
  node [
    id 1125
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1126
    label "strawestowa&#263;"
  ]
  node [
    id 1127
    label "sparafrazowa&#263;"
  ]
  node [
    id 1128
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1129
    label "trawestowa&#263;"
  ]
  node [
    id 1130
    label "sformu&#322;owanie"
  ]
  node [
    id 1131
    label "parafrazowanie"
  ]
  node [
    id 1132
    label "ozdobnik"
  ]
  node [
    id 1133
    label "delimitacja"
  ]
  node [
    id 1134
    label "parafrazowa&#263;"
  ]
  node [
    id 1135
    label "stylizacja"
  ]
  node [
    id 1136
    label "komunikat"
  ]
  node [
    id 1137
    label "trawestowanie"
  ]
  node [
    id 1138
    label "strawestowanie"
  ]
  node [
    id 1139
    label "announcement"
  ]
  node [
    id 1140
    label "poinformowanie"
  ]
  node [
    id 1141
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 1142
    label "reference"
  ]
  node [
    id 1143
    label "uniewa&#380;nienie"
  ]
  node [
    id 1144
    label "retraction"
  ]
  node [
    id 1145
    label "mention"
  ]
  node [
    id 1146
    label "odprawienie"
  ]
  node [
    id 1147
    label "wniosek"
  ]
  node [
    id 1148
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1149
    label "&#380;ale"
  ]
  node [
    id 1150
    label "&#347;wiergot"
  ]
  node [
    id 1151
    label "zara"
  ]
  node [
    id 1152
    label "blisko"
  ]
  node [
    id 1153
    label "nied&#322;ugo"
  ]
  node [
    id 1154
    label "kr&#243;tki"
  ]
  node [
    id 1155
    label "nied&#322;ugi"
  ]
  node [
    id 1156
    label "wpr&#281;dce"
  ]
  node [
    id 1157
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 1158
    label "bliski"
  ]
  node [
    id 1159
    label "dok&#322;adnie"
  ]
  node [
    id 1160
    label "silnie"
  ]
  node [
    id 1161
    label "przekazywa&#263;"
  ]
  node [
    id 1162
    label "grant"
  ]
  node [
    id 1163
    label "order"
  ]
  node [
    id 1164
    label "nakazywa&#263;"
  ]
  node [
    id 1165
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1166
    label "podawa&#263;"
  ]
  node [
    id 1167
    label "wp&#322;aca&#263;"
  ]
  node [
    id 1168
    label "sygna&#322;"
  ]
  node [
    id 1169
    label "impart"
  ]
  node [
    id 1170
    label "poleca&#263;"
  ]
  node [
    id 1171
    label "wymaga&#263;"
  ]
  node [
    id 1172
    label "pakowa&#263;"
  ]
  node [
    id 1173
    label "inflict"
  ]
  node [
    id 1174
    label "command"
  ]
  node [
    id 1175
    label "dotacja"
  ]
  node [
    id 1176
    label "odznaka"
  ]
  node [
    id 1177
    label "kawaler"
  ]
  node [
    id 1178
    label "post&#261;pi&#263;"
  ]
  node [
    id 1179
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1180
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1181
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1182
    label "zorganizowa&#263;"
  ]
  node [
    id 1183
    label "appoint"
  ]
  node [
    id 1184
    label "wystylizowa&#263;"
  ]
  node [
    id 1185
    label "cause"
  ]
  node [
    id 1186
    label "przerobi&#263;"
  ]
  node [
    id 1187
    label "nabra&#263;"
  ]
  node [
    id 1188
    label "make"
  ]
  node [
    id 1189
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1190
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1191
    label "wydali&#263;"
  ]
  node [
    id 1192
    label "advance"
  ]
  node [
    id 1193
    label "act"
  ]
  node [
    id 1194
    label "see"
  ]
  node [
    id 1195
    label "usun&#261;&#263;"
  ]
  node [
    id 1196
    label "sack"
  ]
  node [
    id 1197
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 1198
    label "restore"
  ]
  node [
    id 1199
    label "dostosowa&#263;"
  ]
  node [
    id 1200
    label "pozyska&#263;"
  ]
  node [
    id 1201
    label "stworzy&#263;"
  ]
  node [
    id 1202
    label "plan"
  ]
  node [
    id 1203
    label "stage"
  ]
  node [
    id 1204
    label "urobi&#263;"
  ]
  node [
    id 1205
    label "ensnare"
  ]
  node [
    id 1206
    label "wprowadzi&#263;"
  ]
  node [
    id 1207
    label "zaplanowa&#263;"
  ]
  node [
    id 1208
    label "przygotowa&#263;"
  ]
  node [
    id 1209
    label "standard"
  ]
  node [
    id 1210
    label "skupi&#263;"
  ]
  node [
    id 1211
    label "podbi&#263;"
  ]
  node [
    id 1212
    label "umocni&#263;"
  ]
  node [
    id 1213
    label "doprowadzi&#263;"
  ]
  node [
    id 1214
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 1215
    label "zadowoli&#263;"
  ]
  node [
    id 1216
    label "accommodate"
  ]
  node [
    id 1217
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 1218
    label "zabezpieczy&#263;"
  ]
  node [
    id 1219
    label "wytworzy&#263;"
  ]
  node [
    id 1220
    label "pomy&#347;le&#263;"
  ]
  node [
    id 1221
    label "woda"
  ]
  node [
    id 1222
    label "hoax"
  ]
  node [
    id 1223
    label "deceive"
  ]
  node [
    id 1224
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 1225
    label "oszwabi&#263;"
  ]
  node [
    id 1226
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 1227
    label "gull"
  ]
  node [
    id 1228
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 1229
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 1230
    label "wzi&#261;&#263;"
  ]
  node [
    id 1231
    label "naby&#263;"
  ]
  node [
    id 1232
    label "fraud"
  ]
  node [
    id 1233
    label "kupi&#263;"
  ]
  node [
    id 1234
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1235
    label "objecha&#263;"
  ]
  node [
    id 1236
    label "stylize"
  ]
  node [
    id 1237
    label "upodobni&#263;"
  ]
  node [
    id 1238
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1239
    label "zaliczy&#263;"
  ]
  node [
    id 1240
    label "overwork"
  ]
  node [
    id 1241
    label "zamieni&#263;"
  ]
  node [
    id 1242
    label "zmodyfikowa&#263;"
  ]
  node [
    id 1243
    label "change"
  ]
  node [
    id 1244
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1245
    label "przej&#347;&#263;"
  ]
  node [
    id 1246
    label "zmieni&#263;"
  ]
  node [
    id 1247
    label "convert"
  ]
  node [
    id 1248
    label "prze&#380;y&#263;"
  ]
  node [
    id 1249
    label "przetworzy&#263;"
  ]
  node [
    id 1250
    label "upora&#263;_si&#281;"
  ]
  node [
    id 1251
    label "sprawi&#263;"
  ]
  node [
    id 1252
    label "threat"
  ]
  node [
    id 1253
    label "zawisa&#263;"
  ]
  node [
    id 1254
    label "zagrozi&#263;"
  ]
  node [
    id 1255
    label "postrach"
  ]
  node [
    id 1256
    label "pogroza"
  ]
  node [
    id 1257
    label "zawisanie"
  ]
  node [
    id 1258
    label "czarny_punkt"
  ]
  node [
    id 1259
    label "niebezpiecze&#324;stwo"
  ]
  node [
    id 1260
    label "nieruchomie&#263;"
  ]
  node [
    id 1261
    label "ko&#324;czy&#263;"
  ]
  node [
    id 1262
    label "pojawianie_si&#281;"
  ]
  node [
    id 1263
    label "umieranie"
  ]
  node [
    id 1264
    label "nieruchomienie"
  ]
  node [
    id 1265
    label "dzianie_si&#281;"
  ]
  node [
    id 1266
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1267
    label "zaszachowa&#263;"
  ]
  node [
    id 1268
    label "threaten"
  ]
  node [
    id 1269
    label "szachowa&#263;"
  ]
  node [
    id 1270
    label "zaistnie&#263;"
  ]
  node [
    id 1271
    label "uprzedzi&#263;"
  ]
  node [
    id 1272
    label "menace"
  ]
  node [
    id 1273
    label "przyczyna"
  ]
  node [
    id 1274
    label "przera&#380;enie"
  ]
  node [
    id 1275
    label "reakcja"
  ]
  node [
    id 1276
    label "niestandardowo"
  ]
  node [
    id 1277
    label "kolejny"
  ]
  node [
    id 1278
    label "osobno"
  ]
  node [
    id 1279
    label "r&#243;&#380;ny"
  ]
  node [
    id 1280
    label "inszy"
  ]
  node [
    id 1281
    label "niestandardowy"
  ]
  node [
    id 1282
    label "niekonwencjonalnie"
  ]
  node [
    id 1283
    label "nietypowo"
  ]
  node [
    id 1284
    label "doba"
  ]
  node [
    id 1285
    label "weekend"
  ]
  node [
    id 1286
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 1287
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 1288
    label "miesi&#261;c"
  ]
  node [
    id 1289
    label "poprzedzanie"
  ]
  node [
    id 1290
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1291
    label "laba"
  ]
  node [
    id 1292
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1293
    label "chronometria"
  ]
  node [
    id 1294
    label "rachuba_czasu"
  ]
  node [
    id 1295
    label "przep&#322;ywanie"
  ]
  node [
    id 1296
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1297
    label "czasokres"
  ]
  node [
    id 1298
    label "odczyt"
  ]
  node [
    id 1299
    label "chwila"
  ]
  node [
    id 1300
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1301
    label "dzieje"
  ]
  node [
    id 1302
    label "kategoria_gramatyczna"
  ]
  node [
    id 1303
    label "poprzedzenie"
  ]
  node [
    id 1304
    label "trawienie"
  ]
  node [
    id 1305
    label "pochodzi&#263;"
  ]
  node [
    id 1306
    label "period"
  ]
  node [
    id 1307
    label "okres_czasu"
  ]
  node [
    id 1308
    label "poprzedza&#263;"
  ]
  node [
    id 1309
    label "schy&#322;ek"
  ]
  node [
    id 1310
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1311
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1312
    label "zegar"
  ]
  node [
    id 1313
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1314
    label "czwarty_wymiar"
  ]
  node [
    id 1315
    label "pochodzenie"
  ]
  node [
    id 1316
    label "koniugacja"
  ]
  node [
    id 1317
    label "Zeitgeist"
  ]
  node [
    id 1318
    label "trawi&#263;"
  ]
  node [
    id 1319
    label "pogoda"
  ]
  node [
    id 1320
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1321
    label "poprzedzi&#263;"
  ]
  node [
    id 1322
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1323
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1324
    label "time_period"
  ]
  node [
    id 1325
    label "noc"
  ]
  node [
    id 1326
    label "dzie&#324;"
  ]
  node [
    id 1327
    label "godzina"
  ]
  node [
    id 1328
    label "long_time"
  ]
  node [
    id 1329
    label "niedziela"
  ]
  node [
    id 1330
    label "sobota"
  ]
  node [
    id 1331
    label "miech"
  ]
  node [
    id 1332
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1333
    label "rok"
  ]
  node [
    id 1334
    label "kalendy"
  ]
  node [
    id 1335
    label "procedura"
  ]
  node [
    id 1336
    label "apparent_motion"
  ]
  node [
    id 1337
    label "proces"
  ]
  node [
    id 1338
    label "toczek"
  ]
  node [
    id 1339
    label "odg&#322;os"
  ]
  node [
    id 1340
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1341
    label "rytm"
  ]
  node [
    id 1342
    label "cycle"
  ]
  node [
    id 1343
    label "resonance"
  ]
  node [
    id 1344
    label "wydanie"
  ]
  node [
    id 1345
    label "wpadni&#281;cie"
  ]
  node [
    id 1346
    label "wpadanie"
  ]
  node [
    id 1347
    label "sound"
  ]
  node [
    id 1348
    label "brzmienie"
  ]
  node [
    id 1349
    label "zjawisko"
  ]
  node [
    id 1350
    label "wyda&#263;"
  ]
  node [
    id 1351
    label "wpa&#347;&#263;"
  ]
  node [
    id 1352
    label "note"
  ]
  node [
    id 1353
    label "onomatopeja"
  ]
  node [
    id 1354
    label "wpada&#263;"
  ]
  node [
    id 1355
    label "time"
  ]
  node [
    id 1356
    label "rytmika"
  ]
  node [
    id 1357
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 1358
    label "wybicie"
  ]
  node [
    id 1359
    label "tempo"
  ]
  node [
    id 1360
    label "metrum"
  ]
  node [
    id 1361
    label "wybijanie"
  ]
  node [
    id 1362
    label "rozmieszczenie"
  ]
  node [
    id 1363
    label "miarowo&#347;&#263;"
  ]
  node [
    id 1364
    label "eurytmia"
  ]
  node [
    id 1365
    label "kognicja"
  ]
  node [
    id 1366
    label "przebieg"
  ]
  node [
    id 1367
    label "rozprawa"
  ]
  node [
    id 1368
    label "legislacyjnie"
  ]
  node [
    id 1369
    label "przes&#322;anka"
  ]
  node [
    id 1370
    label "nast&#281;pstwo"
  ]
  node [
    id 1371
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1372
    label "organizowa&#263;"
  ]
  node [
    id 1373
    label "ordinariness"
  ]
  node [
    id 1374
    label "taniec_towarzyski"
  ]
  node [
    id 1375
    label "organizowanie"
  ]
  node [
    id 1376
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1377
    label "criterion"
  ]
  node [
    id 1378
    label "zorganizowanie"
  ]
  node [
    id 1379
    label "zielenica"
  ]
  node [
    id 1380
    label "kapelusz"
  ]
  node [
    id 1381
    label "toczkowate"
  ]
  node [
    id 1382
    label "facylitator"
  ]
  node [
    id 1383
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1384
    label "metodyka"
  ]
  node [
    id 1385
    label "brak"
  ]
  node [
    id 1386
    label "gry&#378;&#263;"
  ]
  node [
    id 1387
    label "mieszanina"
  ]
  node [
    id 1388
    label "aggro"
  ]
  node [
    id 1389
    label "zamieszki"
  ]
  node [
    id 1390
    label "gaz"
  ]
  node [
    id 1391
    label "frakcja"
  ]
  node [
    id 1392
    label "substancja"
  ]
  node [
    id 1393
    label "synteza"
  ]
  node [
    id 1394
    label "gas"
  ]
  node [
    id 1395
    label "instalacja"
  ]
  node [
    id 1396
    label "peda&#322;"
  ]
  node [
    id 1397
    label "p&#322;omie&#324;"
  ]
  node [
    id 1398
    label "paliwo"
  ]
  node [
    id 1399
    label "accelerator"
  ]
  node [
    id 1400
    label "cia&#322;o"
  ]
  node [
    id 1401
    label "termojonizacja"
  ]
  node [
    id 1402
    label "stan_skupienia"
  ]
  node [
    id 1403
    label "rozpr&#281;&#380;liwo&#347;&#263;"
  ]
  node [
    id 1404
    label "przy&#347;piesznik"
  ]
  node [
    id 1405
    label "bro&#324;"
  ]
  node [
    id 1406
    label "disquiet"
  ]
  node [
    id 1407
    label "vex"
  ]
  node [
    id 1408
    label "kaganiec"
  ]
  node [
    id 1409
    label "niepokoi&#263;"
  ]
  node [
    id 1410
    label "snap"
  ]
  node [
    id 1411
    label "rozdrabnia&#263;"
  ]
  node [
    id 1412
    label "kaleczy&#263;"
  ]
  node [
    id 1413
    label "dra&#380;ni&#263;"
  ]
  node [
    id 1414
    label "sail"
  ]
  node [
    id 1415
    label "leave"
  ]
  node [
    id 1416
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 1417
    label "travel"
  ]
  node [
    id 1418
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1419
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 1420
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 1421
    label "zosta&#263;"
  ]
  node [
    id 1422
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1423
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 1424
    label "przyj&#261;&#263;"
  ]
  node [
    id 1425
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 1426
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 1427
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1428
    label "zacz&#261;&#263;"
  ]
  node [
    id 1429
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1430
    label "play_along"
  ]
  node [
    id 1431
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 1432
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1433
    label "become"
  ]
  node [
    id 1434
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1435
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1436
    label "odj&#261;&#263;"
  ]
  node [
    id 1437
    label "introduce"
  ]
  node [
    id 1438
    label "begin"
  ]
  node [
    id 1439
    label "do"
  ]
  node [
    id 1440
    label "przybra&#263;"
  ]
  node [
    id 1441
    label "strike"
  ]
  node [
    id 1442
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1443
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 1444
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 1445
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 1446
    label "receive"
  ]
  node [
    id 1447
    label "obra&#263;"
  ]
  node [
    id 1448
    label "uzna&#263;"
  ]
  node [
    id 1449
    label "draw"
  ]
  node [
    id 1450
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1451
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 1452
    label "przyj&#281;cie"
  ]
  node [
    id 1453
    label "fall"
  ]
  node [
    id 1454
    label "swallow"
  ]
  node [
    id 1455
    label "odebra&#263;"
  ]
  node [
    id 1456
    label "dostarczy&#263;"
  ]
  node [
    id 1457
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1458
    label "absorb"
  ]
  node [
    id 1459
    label "undertake"
  ]
  node [
    id 1460
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1461
    label "come_up"
  ]
  node [
    id 1462
    label "straci&#263;"
  ]
  node [
    id 1463
    label "zyska&#263;"
  ]
  node [
    id 1464
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1465
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1466
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1467
    label "pozosta&#263;"
  ]
  node [
    id 1468
    label "catch"
  ]
  node [
    id 1469
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1470
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1471
    label "pozostawi&#263;"
  ]
  node [
    id 1472
    label "obni&#380;y&#263;"
  ]
  node [
    id 1473
    label "zostawi&#263;"
  ]
  node [
    id 1474
    label "przesta&#263;"
  ]
  node [
    id 1475
    label "potani&#263;"
  ]
  node [
    id 1476
    label "drop"
  ]
  node [
    id 1477
    label "evacuate"
  ]
  node [
    id 1478
    label "humiliate"
  ]
  node [
    id 1479
    label "tekst"
  ]
  node [
    id 1480
    label "authorize"
  ]
  node [
    id 1481
    label "omin&#261;&#263;"
  ]
  node [
    id 1482
    label "loom"
  ]
  node [
    id 1483
    label "result"
  ]
  node [
    id 1484
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 1485
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 1486
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 1487
    label "appear"
  ]
  node [
    id 1488
    label "zgin&#261;&#263;"
  ]
  node [
    id 1489
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 1490
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 1491
    label "rise"
  ]
  node [
    id 1492
    label "pierworodztwo"
  ]
  node [
    id 1493
    label "faza"
  ]
  node [
    id 1494
    label "upgrade"
  ]
  node [
    id 1495
    label "warunek_lokalowy"
  ]
  node [
    id 1496
    label "plac"
  ]
  node [
    id 1497
    label "location"
  ]
  node [
    id 1498
    label "uwaga"
  ]
  node [
    id 1499
    label "przestrze&#324;"
  ]
  node [
    id 1500
    label "status"
  ]
  node [
    id 1501
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1502
    label "praca"
  ]
  node [
    id 1503
    label "cykl_astronomiczny"
  ]
  node [
    id 1504
    label "coil"
  ]
  node [
    id 1505
    label "fotoelement"
  ]
  node [
    id 1506
    label "komutowanie"
  ]
  node [
    id 1507
    label "nastr&#243;j"
  ]
  node [
    id 1508
    label "przerywacz"
  ]
  node [
    id 1509
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1510
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1511
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1512
    label "obsesja"
  ]
  node [
    id 1513
    label "dw&#243;jnik"
  ]
  node [
    id 1514
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1515
    label "okres"
  ]
  node [
    id 1516
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1517
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1518
    label "przew&#243;d"
  ]
  node [
    id 1519
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1520
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1521
    label "obw&#243;d"
  ]
  node [
    id 1522
    label "degree"
  ]
  node [
    id 1523
    label "komutowa&#263;"
  ]
  node [
    id 1524
    label "pierwor&#243;dztwo"
  ]
  node [
    id 1525
    label "wydziedziczy&#263;"
  ]
  node [
    id 1526
    label "wydziedziczenie"
  ]
  node [
    id 1527
    label "kolejno&#347;&#263;"
  ]
  node [
    id 1528
    label "ulepszenie"
  ]
  node [
    id 1529
    label "cios"
  ]
  node [
    id 1530
    label "uderzenie"
  ]
  node [
    id 1531
    label "blok"
  ]
  node [
    id 1532
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 1533
    label "struktura_geologiczna"
  ]
  node [
    id 1534
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 1535
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 1536
    label "coup"
  ]
  node [
    id 1537
    label "siekacz"
  ]
  node [
    id 1538
    label "instrumentalizacja"
  ]
  node [
    id 1539
    label "trafienie"
  ]
  node [
    id 1540
    label "walka"
  ]
  node [
    id 1541
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1542
    label "wdarcie_si&#281;"
  ]
  node [
    id 1543
    label "pogorszenie"
  ]
  node [
    id 1544
    label "poczucie"
  ]
  node [
    id 1545
    label "contact"
  ]
  node [
    id 1546
    label "stukni&#281;cie"
  ]
  node [
    id 1547
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 1548
    label "bat"
  ]
  node [
    id 1549
    label "spowodowanie"
  ]
  node [
    id 1550
    label "rush"
  ]
  node [
    id 1551
    label "odbicie"
  ]
  node [
    id 1552
    label "dawka"
  ]
  node [
    id 1553
    label "zadanie"
  ]
  node [
    id 1554
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1555
    label "st&#322;uczenie"
  ]
  node [
    id 1556
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 1557
    label "odbicie_si&#281;"
  ]
  node [
    id 1558
    label "dotkni&#281;cie"
  ]
  node [
    id 1559
    label "charge"
  ]
  node [
    id 1560
    label "dostanie"
  ]
  node [
    id 1561
    label "skrytykowanie"
  ]
  node [
    id 1562
    label "zagrywka"
  ]
  node [
    id 1563
    label "manewr"
  ]
  node [
    id 1564
    label "nast&#261;pienie"
  ]
  node [
    id 1565
    label "uderzanie"
  ]
  node [
    id 1566
    label "stroke"
  ]
  node [
    id 1567
    label "pobicie"
  ]
  node [
    id 1568
    label "ruch"
  ]
  node [
    id 1569
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1570
    label "flap"
  ]
  node [
    id 1571
    label "dotyk"
  ]
  node [
    id 1572
    label "zrobienie"
  ]
  node [
    id 1573
    label "fakt"
  ]
  node [
    id 1574
    label "czyn"
  ]
  node [
    id 1575
    label "ilustracja"
  ]
  node [
    id 1576
    label "materia&#322;"
  ]
  node [
    id 1577
    label "szata_graficzna"
  ]
  node [
    id 1578
    label "photograph"
  ]
  node [
    id 1579
    label "obrazek"
  ]
  node [
    id 1580
    label "bia&#322;e_plamy"
  ]
  node [
    id 1581
    label "funkcja"
  ]
  node [
    id 1582
    label "vow"
  ]
  node [
    id 1583
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1584
    label "b&#281;ben"
  ]
  node [
    id 1585
    label "pi&#281;cioksi&#261;g"
  ]
  node [
    id 1586
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1587
    label "cylinder"
  ]
  node [
    id 1588
    label "brzuszysko"
  ]
  node [
    id 1589
    label "dziecko"
  ]
  node [
    id 1590
    label "membranofon"
  ]
  node [
    id 1591
    label "maszyna"
  ]
  node [
    id 1592
    label "kopu&#322;a"
  ]
  node [
    id 1593
    label "naci&#261;g_perkusyjny"
  ]
  node [
    id 1594
    label "szpulka"
  ]
  node [
    id 1595
    label "d&#378;wig"
  ]
  node [
    id 1596
    label "egzemplarz"
  ]
  node [
    id 1597
    label "rozdzia&#322;"
  ]
  node [
    id 1598
    label "wk&#322;ad"
  ]
  node [
    id 1599
    label "zak&#322;adka"
  ]
  node [
    id 1600
    label "nomina&#322;"
  ]
  node [
    id 1601
    label "ok&#322;adka"
  ]
  node [
    id 1602
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 1603
    label "wydawnictwo"
  ]
  node [
    id 1604
    label "ekslibris"
  ]
  node [
    id 1605
    label "przek&#322;adacz"
  ]
  node [
    id 1606
    label "bibliofilstwo"
  ]
  node [
    id 1607
    label "falc"
  ]
  node [
    id 1608
    label "pagina"
  ]
  node [
    id 1609
    label "zw&#243;j"
  ]
  node [
    id 1610
    label "cykl"
  ]
  node [
    id 1611
    label "spe&#322;ni&#263;"
  ]
  node [
    id 1612
    label "perform"
  ]
  node [
    id 1613
    label "wywi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1614
    label "przechowa&#263;"
  ]
  node [
    id 1615
    label "preserve"
  ]
  node [
    id 1616
    label "urzeczywistni&#263;"
  ]
  node [
    id 1617
    label "uchroni&#263;"
  ]
  node [
    id 1618
    label "ukry&#263;"
  ]
  node [
    id 1619
    label "zachowa&#263;"
  ]
  node [
    id 1620
    label "podtrzyma&#263;"
  ]
  node [
    id 1621
    label "piwo"
  ]
  node [
    id 1622
    label "uwarzenie"
  ]
  node [
    id 1623
    label "warzenie"
  ]
  node [
    id 1624
    label "bacik"
  ]
  node [
    id 1625
    label "wyj&#347;cie"
  ]
  node [
    id 1626
    label "uwarzy&#263;"
  ]
  node [
    id 1627
    label "birofilia"
  ]
  node [
    id 1628
    label "warzy&#263;"
  ]
  node [
    id 1629
    label "nawarzy&#263;"
  ]
  node [
    id 1630
    label "browarnia"
  ]
  node [
    id 1631
    label "nawarzenie"
  ]
  node [
    id 1632
    label "anta&#322;"
  ]
  node [
    id 1633
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 1634
    label "honours"
  ]
  node [
    id 1635
    label "pride"
  ]
  node [
    id 1636
    label "hyr"
  ]
  node [
    id 1637
    label "renoma"
  ]
  node [
    id 1638
    label "rzecz"
  ]
  node [
    id 1639
    label "object"
  ]
  node [
    id 1640
    label "temat"
  ]
  node [
    id 1641
    label "mienie"
  ]
  node [
    id 1642
    label "przyroda"
  ]
  node [
    id 1643
    label "obiekt"
  ]
  node [
    id 1644
    label "kultura"
  ]
  node [
    id 1645
    label "prize"
  ]
  node [
    id 1646
    label "trophy"
  ]
  node [
    id 1647
    label "oznaczenie"
  ]
  node [
    id 1648
    label "potraktowanie"
  ]
  node [
    id 1649
    label "nagrodzenie"
  ]
  node [
    id 1650
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1651
    label "opinia"
  ]
  node [
    id 1652
    label "Ereb"
  ]
  node [
    id 1653
    label "Dionizos"
  ]
  node [
    id 1654
    label "s&#261;d_ostateczny"
  ]
  node [
    id 1655
    label "Waruna"
  ]
  node [
    id 1656
    label "ofiarowywanie"
  ]
  node [
    id 1657
    label "ba&#322;wan"
  ]
  node [
    id 1658
    label "Hesperos"
  ]
  node [
    id 1659
    label "Posejdon"
  ]
  node [
    id 1660
    label "Sylen"
  ]
  node [
    id 1661
    label "Janus"
  ]
  node [
    id 1662
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1663
    label "niebiosa"
  ]
  node [
    id 1664
    label "Boreasz"
  ]
  node [
    id 1665
    label "ofiarowywa&#263;"
  ]
  node [
    id 1666
    label "uwielbienie"
  ]
  node [
    id 1667
    label "Bachus"
  ]
  node [
    id 1668
    label "ofiarowanie"
  ]
  node [
    id 1669
    label "Neptun"
  ]
  node [
    id 1670
    label "tr&#243;jca"
  ]
  node [
    id 1671
    label "Kupidyn"
  ]
  node [
    id 1672
    label "igrzyska_greckie"
  ]
  node [
    id 1673
    label "politeizm"
  ]
  node [
    id 1674
    label "ofiarowa&#263;"
  ]
  node [
    id 1675
    label "gigant"
  ]
  node [
    id 1676
    label "idol"
  ]
  node [
    id 1677
    label "kombinacja_alpejska"
  ]
  node [
    id 1678
    label "firma"
  ]
  node [
    id 1679
    label "slalom"
  ]
  node [
    id 1680
    label "ucieczka"
  ]
  node [
    id 1681
    label "zdobienie"
  ]
  node [
    id 1682
    label "bestia"
  ]
  node [
    id 1683
    label "istota_fantastyczna"
  ]
  node [
    id 1684
    label "olbrzym"
  ]
  node [
    id 1685
    label "za&#347;wiaty"
  ]
  node [
    id 1686
    label "bezchmurno&#347;&#263;"
  ]
  node [
    id 1687
    label "znak_zodiaku"
  ]
  node [
    id 1688
    label "Tr&#243;jca_&#346;wi&#281;ta"
  ]
  node [
    id 1689
    label "opaczno&#347;&#263;"
  ]
  node [
    id 1690
    label "si&#322;a"
  ]
  node [
    id 1691
    label "absolut"
  ]
  node [
    id 1692
    label "zodiak"
  ]
  node [
    id 1693
    label "Kr&#243;lestwo_Niebieskie"
  ]
  node [
    id 1694
    label "czczenie"
  ]
  node [
    id 1695
    label "oko_opatrzno&#347;ci"
  ]
  node [
    id 1696
    label "Chocho&#322;"
  ]
  node [
    id 1697
    label "Herkules_Poirot"
  ]
  node [
    id 1698
    label "Edyp"
  ]
  node [
    id 1699
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1700
    label "Harry_Potter"
  ]
  node [
    id 1701
    label "Casanova"
  ]
  node [
    id 1702
    label "Gargantua"
  ]
  node [
    id 1703
    label "Zgredek"
  ]
  node [
    id 1704
    label "Winnetou"
  ]
  node [
    id 1705
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1706
    label "Dulcynea"
  ]
  node [
    id 1707
    label "person"
  ]
  node [
    id 1708
    label "Sherlock_Holmes"
  ]
  node [
    id 1709
    label "Quasimodo"
  ]
  node [
    id 1710
    label "Plastu&#347;"
  ]
  node [
    id 1711
    label "Faust"
  ]
  node [
    id 1712
    label "Wallenrod"
  ]
  node [
    id 1713
    label "Dwukwiat"
  ]
  node [
    id 1714
    label "Don_Juan"
  ]
  node [
    id 1715
    label "Don_Kiszot"
  ]
  node [
    id 1716
    label "Hamlet"
  ]
  node [
    id 1717
    label "Werter"
  ]
  node [
    id 1718
    label "Szwejk"
  ]
  node [
    id 1719
    label "w&#281;gielek"
  ]
  node [
    id 1720
    label "kula_&#347;niegowa"
  ]
  node [
    id 1721
    label "fala_morska"
  ]
  node [
    id 1722
    label "snowman"
  ]
  node [
    id 1723
    label "wave"
  ]
  node [
    id 1724
    label "marchewka"
  ]
  node [
    id 1725
    label "g&#322;upek"
  ]
  node [
    id 1726
    label "patyk"
  ]
  node [
    id 1727
    label "Eastwood"
  ]
  node [
    id 1728
    label "gwiazda"
  ]
  node [
    id 1729
    label "tr&#243;jka"
  ]
  node [
    id 1730
    label "Logan"
  ]
  node [
    id 1731
    label "winoro&#347;l"
  ]
  node [
    id 1732
    label "orfik"
  ]
  node [
    id 1733
    label "wino"
  ]
  node [
    id 1734
    label "satyr"
  ]
  node [
    id 1735
    label "orfizm"
  ]
  node [
    id 1736
    label "strza&#322;a_Amora"
  ]
  node [
    id 1737
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1738
    label "tr&#243;jz&#261;b"
  ]
  node [
    id 1739
    label "morze"
  ]
  node [
    id 1740
    label "p&#243;&#322;noc"
  ]
  node [
    id 1741
    label "Prokrust"
  ]
  node [
    id 1742
    label "ciemno&#347;&#263;"
  ]
  node [
    id 1743
    label "hinduizm"
  ]
  node [
    id 1744
    label "niebo"
  ]
  node [
    id 1745
    label "po&#347;wi&#281;ci&#263;"
  ]
  node [
    id 1746
    label "deklarowa&#263;"
  ]
  node [
    id 1747
    label "zdeklarowa&#263;"
  ]
  node [
    id 1748
    label "volunteer"
  ]
  node [
    id 1749
    label "zaproponowa&#263;"
  ]
  node [
    id 1750
    label "podarowa&#263;"
  ]
  node [
    id 1751
    label "afford"
  ]
  node [
    id 1752
    label "B&#243;g"
  ]
  node [
    id 1753
    label "oferowa&#263;"
  ]
  node [
    id 1754
    label "wierzenie"
  ]
  node [
    id 1755
    label "sacrifice"
  ]
  node [
    id 1756
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 1757
    label "podarowanie"
  ]
  node [
    id 1758
    label "zaproponowanie"
  ]
  node [
    id 1759
    label "oferowanie"
  ]
  node [
    id 1760
    label "forfeit"
  ]
  node [
    id 1761
    label "msza"
  ]
  node [
    id 1762
    label "crack"
  ]
  node [
    id 1763
    label "deklarowanie"
  ]
  node [
    id 1764
    label "zdeklarowanie"
  ]
  node [
    id 1765
    label "bo&#380;ek"
  ]
  node [
    id 1766
    label "powa&#380;anie"
  ]
  node [
    id 1767
    label "zachwyt"
  ]
  node [
    id 1768
    label "admiracja"
  ]
  node [
    id 1769
    label "tender"
  ]
  node [
    id 1770
    label "darowywa&#263;"
  ]
  node [
    id 1771
    label "zapewnia&#263;"
  ]
  node [
    id 1772
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 1773
    label "po&#347;wi&#281;canie"
  ]
  node [
    id 1774
    label "darowywanie"
  ]
  node [
    id 1775
    label "zapewnianie"
  ]
  node [
    id 1776
    label "hide"
  ]
  node [
    id 1777
    label "czu&#263;"
  ]
  node [
    id 1778
    label "support"
  ]
  node [
    id 1779
    label "need"
  ]
  node [
    id 1780
    label "interpretator"
  ]
  node [
    id 1781
    label "cover"
  ]
  node [
    id 1782
    label "postrzega&#263;"
  ]
  node [
    id 1783
    label "przewidywa&#263;"
  ]
  node [
    id 1784
    label "smell"
  ]
  node [
    id 1785
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 1786
    label "uczuwa&#263;"
  ]
  node [
    id 1787
    label "spirit"
  ]
  node [
    id 1788
    label "anticipate"
  ]
  node [
    id 1789
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 1790
    label "necessity"
  ]
  node [
    id 1791
    label "wym&#243;g"
  ]
  node [
    id 1792
    label "pragnienie"
  ]
  node [
    id 1793
    label "sytuacja"
  ]
  node [
    id 1794
    label "warunki"
  ]
  node [
    id 1795
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1796
    label "motyw"
  ]
  node [
    id 1797
    label "realia"
  ]
  node [
    id 1798
    label "uzyskanie"
  ]
  node [
    id 1799
    label "chcenie"
  ]
  node [
    id 1800
    label "ch&#281;&#263;"
  ]
  node [
    id 1801
    label "upragnienie"
  ]
  node [
    id 1802
    label "reflektowanie"
  ]
  node [
    id 1803
    label "potrzeba_fizjologiczna"
  ]
  node [
    id 1804
    label "eagerness"
  ]
  node [
    id 1805
    label "wyci&#261;ganie_r&#281;ki"
  ]
  node [
    id 1806
    label "condition"
  ]
  node [
    id 1807
    label "umowa"
  ]
  node [
    id 1808
    label "repeat"
  ]
  node [
    id 1809
    label "powtarza&#263;"
  ]
  node [
    id 1810
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 1811
    label "krajobraz"
  ]
  node [
    id 1812
    label "obszar"
  ]
  node [
    id 1813
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1814
    label "po_s&#261;siedzku"
  ]
  node [
    id 1815
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1816
    label "odm&#322;adzanie"
  ]
  node [
    id 1817
    label "liga"
  ]
  node [
    id 1818
    label "jednostka_systematyczna"
  ]
  node [
    id 1819
    label "gromada"
  ]
  node [
    id 1820
    label "Entuzjastki"
  ]
  node [
    id 1821
    label "kompozycja"
  ]
  node [
    id 1822
    label "Terranie"
  ]
  node [
    id 1823
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1824
    label "category"
  ]
  node [
    id 1825
    label "pakiet_klimatyczny"
  ]
  node [
    id 1826
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1827
    label "cz&#261;steczka"
  ]
  node [
    id 1828
    label "stage_set"
  ]
  node [
    id 1829
    label "type"
  ]
  node [
    id 1830
    label "specgrupa"
  ]
  node [
    id 1831
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1832
    label "&#346;wietliki"
  ]
  node [
    id 1833
    label "odm&#322;odzenie"
  ]
  node [
    id 1834
    label "Eurogrupa"
  ]
  node [
    id 1835
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1836
    label "harcerze_starsi"
  ]
  node [
    id 1837
    label "Kosowo"
  ]
  node [
    id 1838
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1839
    label "Zab&#322;ocie"
  ]
  node [
    id 1840
    label "zach&#243;d"
  ]
  node [
    id 1841
    label "po&#322;udnie"
  ]
  node [
    id 1842
    label "Pow&#261;zki"
  ]
  node [
    id 1843
    label "Piotrowo"
  ]
  node [
    id 1844
    label "Olszanica"
  ]
  node [
    id 1845
    label "holarktyka"
  ]
  node [
    id 1846
    label "Ruda_Pabianicka"
  ]
  node [
    id 1847
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1848
    label "Ludwin&#243;w"
  ]
  node [
    id 1849
    label "Arktyka"
  ]
  node [
    id 1850
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1851
    label "Zabu&#380;e"
  ]
  node [
    id 1852
    label "antroposfera"
  ]
  node [
    id 1853
    label "Neogea"
  ]
  node [
    id 1854
    label "Syberia_Zachodnia"
  ]
  node [
    id 1855
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1856
    label "zakres"
  ]
  node [
    id 1857
    label "pas_planetoid"
  ]
  node [
    id 1858
    label "Syberia_Wschodnia"
  ]
  node [
    id 1859
    label "Antarktyka"
  ]
  node [
    id 1860
    label "Rakowice"
  ]
  node [
    id 1861
    label "akrecja"
  ]
  node [
    id 1862
    label "wymiar"
  ]
  node [
    id 1863
    label "&#321;&#281;g"
  ]
  node [
    id 1864
    label "Kresy_Zachodnie"
  ]
  node [
    id 1865
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1866
    label "wsch&#243;d"
  ]
  node [
    id 1867
    label "Notogea"
  ]
  node [
    id 1868
    label "tkanka"
  ]
  node [
    id 1869
    label "jednostka_organizacyjna"
  ]
  node [
    id 1870
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1871
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1872
    label "tw&#243;r"
  ]
  node [
    id 1873
    label "organogeneza"
  ]
  node [
    id 1874
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1875
    label "uk&#322;ad"
  ]
  node [
    id 1876
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1877
    label "dekortykacja"
  ]
  node [
    id 1878
    label "Izba_Konsyliarska"
  ]
  node [
    id 1879
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1880
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1881
    label "stomia"
  ]
  node [
    id 1882
    label "budowa"
  ]
  node [
    id 1883
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1884
    label "teren"
  ]
  node [
    id 1885
    label "human_body"
  ]
  node [
    id 1886
    label "dzie&#322;o"
  ]
  node [
    id 1887
    label "obraz"
  ]
  node [
    id 1888
    label "widok"
  ]
  node [
    id 1889
    label "zaj&#347;cie"
  ]
  node [
    id 1890
    label "ekosystem"
  ]
  node [
    id 1891
    label "stw&#243;r"
  ]
  node [
    id 1892
    label "obiekt_naturalny"
  ]
  node [
    id 1893
    label "environment"
  ]
  node [
    id 1894
    label "Ziemia"
  ]
  node [
    id 1895
    label "przyra"
  ]
  node [
    id 1896
    label "wszechstworzenie"
  ]
  node [
    id 1897
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1898
    label "fauna"
  ]
  node [
    id 1899
    label "biota"
  ]
  node [
    id 1900
    label "miernota"
  ]
  node [
    id 1901
    label "ciura"
  ]
  node [
    id 1902
    label "jako&#347;&#263;"
  ]
  node [
    id 1903
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 1904
    label "tandetno&#347;&#263;"
  ]
  node [
    id 1905
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 1906
    label "zero"
  ]
  node [
    id 1907
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 1908
    label "spis"
  ]
  node [
    id 1909
    label "gospodarstwo"
  ]
  node [
    id 1910
    label "stock"
  ]
  node [
    id 1911
    label "catalog"
  ]
  node [
    id 1912
    label "pozycja"
  ]
  node [
    id 1913
    label "akt"
  ]
  node [
    id 1914
    label "sumariusz"
  ]
  node [
    id 1915
    label "book"
  ]
  node [
    id 1916
    label "figurowa&#263;"
  ]
  node [
    id 1917
    label "czynno&#347;&#263;"
  ]
  node [
    id 1918
    label "wyliczanka"
  ]
  node [
    id 1919
    label "przej&#347;cie"
  ]
  node [
    id 1920
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1921
    label "rodowo&#347;&#263;"
  ]
  node [
    id 1922
    label "patent"
  ]
  node [
    id 1923
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1924
    label "dobra"
  ]
  node [
    id 1925
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 1926
    label "possession"
  ]
  node [
    id 1927
    label "pole"
  ]
  node [
    id 1928
    label "miejsce_pracy"
  ]
  node [
    id 1929
    label "dom"
  ]
  node [
    id 1930
    label "stodo&#322;a"
  ]
  node [
    id 1931
    label "gospodarowanie"
  ]
  node [
    id 1932
    label "obora"
  ]
  node [
    id 1933
    label "gospodarowa&#263;"
  ]
  node [
    id 1934
    label "spichlerz"
  ]
  node [
    id 1935
    label "dom_rodzinny"
  ]
  node [
    id 1936
    label "zaskakiwa&#263;"
  ]
  node [
    id 1937
    label "przyjmowa&#263;"
  ]
  node [
    id 1938
    label "bra&#263;"
  ]
  node [
    id 1939
    label "use"
  ]
  node [
    id 1940
    label "dostarcza&#263;"
  ]
  node [
    id 1941
    label "close"
  ]
  node [
    id 1942
    label "wpuszcza&#263;"
  ]
  node [
    id 1943
    label "odbiera&#263;"
  ]
  node [
    id 1944
    label "wyprawia&#263;"
  ]
  node [
    id 1945
    label "przyjmowanie"
  ]
  node [
    id 1946
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1947
    label "umieszcza&#263;"
  ]
  node [
    id 1948
    label "poch&#322;ania&#263;"
  ]
  node [
    id 1949
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1950
    label "pracowa&#263;"
  ]
  node [
    id 1951
    label "uznawa&#263;"
  ]
  node [
    id 1952
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1953
    label "dopuszcza&#263;"
  ]
  node [
    id 1954
    label "zgadza&#263;_si&#281;"
  ]
  node [
    id 1955
    label "obiera&#263;"
  ]
  node [
    id 1956
    label "admit"
  ]
  node [
    id 1957
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 1958
    label "porywa&#263;"
  ]
  node [
    id 1959
    label "take"
  ]
  node [
    id 1960
    label "wchodzi&#263;"
  ]
  node [
    id 1961
    label "poczytywa&#263;"
  ]
  node [
    id 1962
    label "levy"
  ]
  node [
    id 1963
    label "pokonywa&#263;"
  ]
  node [
    id 1964
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 1965
    label "rucha&#263;"
  ]
  node [
    id 1966
    label "otrzymywa&#263;"
  ]
  node [
    id 1967
    label "&#263;pa&#263;"
  ]
  node [
    id 1968
    label "interpretowa&#263;"
  ]
  node [
    id 1969
    label "dostawa&#263;"
  ]
  node [
    id 1970
    label "rusza&#263;"
  ]
  node [
    id 1971
    label "chwyta&#263;"
  ]
  node [
    id 1972
    label "grza&#263;"
  ]
  node [
    id 1973
    label "wch&#322;ania&#263;"
  ]
  node [
    id 1974
    label "wygrywa&#263;"
  ]
  node [
    id 1975
    label "ucieka&#263;"
  ]
  node [
    id 1976
    label "arise"
  ]
  node [
    id 1977
    label "uprawia&#263;_seks"
  ]
  node [
    id 1978
    label "abstract"
  ]
  node [
    id 1979
    label "towarzystwo"
  ]
  node [
    id 1980
    label "atakowa&#263;"
  ]
  node [
    id 1981
    label "branie"
  ]
  node [
    id 1982
    label "zalicza&#263;"
  ]
  node [
    id 1983
    label "open"
  ]
  node [
    id 1984
    label "&#322;apa&#263;"
  ]
  node [
    id 1985
    label "przewa&#380;a&#263;"
  ]
  node [
    id 1986
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 1987
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 1988
    label "dziwi&#263;"
  ]
  node [
    id 1989
    label "obejmowa&#263;"
  ]
  node [
    id 1990
    label "surprise"
  ]
  node [
    id 1991
    label "Fox"
  ]
  node [
    id 1992
    label "&#347;wi&#281;ty"
  ]
  node [
    id 1993
    label "Szyjeckiej"
  ]
  node [
    id 1994
    label "buda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 0
    target 429
  ]
  edge [
    source 0
    target 430
  ]
  edge [
    source 0
    target 431
  ]
  edge [
    source 0
    target 432
  ]
  edge [
    source 0
    target 433
  ]
  edge [
    source 0
    target 434
  ]
  edge [
    source 0
    target 435
  ]
  edge [
    source 0
    target 436
  ]
  edge [
    source 0
    target 437
  ]
  edge [
    source 0
    target 438
  ]
  edge [
    source 0
    target 439
  ]
  edge [
    source 0
    target 440
  ]
  edge [
    source 0
    target 441
  ]
  edge [
    source 0
    target 442
  ]
  edge [
    source 0
    target 443
  ]
  edge [
    source 0
    target 444
  ]
  edge [
    source 0
    target 445
  ]
  edge [
    source 0
    target 446
  ]
  edge [
    source 0
    target 447
  ]
  edge [
    source 0
    target 448
  ]
  edge [
    source 0
    target 449
  ]
  edge [
    source 0
    target 450
  ]
  edge [
    source 0
    target 451
  ]
  edge [
    source 0
    target 452
  ]
  edge [
    source 0
    target 453
  ]
  edge [
    source 0
    target 454
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 3
    target 627
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 3
    target 629
  ]
  edge [
    source 3
    target 630
  ]
  edge [
    source 3
    target 631
  ]
  edge [
    source 3
    target 632
  ]
  edge [
    source 3
    target 633
  ]
  edge [
    source 3
    target 634
  ]
  edge [
    source 3
    target 635
  ]
  edge [
    source 3
    target 636
  ]
  edge [
    source 3
    target 637
  ]
  edge [
    source 3
    target 638
  ]
  edge [
    source 3
    target 639
  ]
  edge [
    source 3
    target 640
  ]
  edge [
    source 3
    target 641
  ]
  edge [
    source 3
    target 642
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 643
  ]
  edge [
    source 3
    target 644
  ]
  edge [
    source 3
    target 645
  ]
  edge [
    source 3
    target 646
  ]
  edge [
    source 3
    target 647
  ]
  edge [
    source 3
    target 648
  ]
  edge [
    source 3
    target 649
  ]
  edge [
    source 3
    target 650
  ]
  edge [
    source 3
    target 651
  ]
  edge [
    source 3
    target 652
  ]
  edge [
    source 3
    target 653
  ]
  edge [
    source 3
    target 654
  ]
  edge [
    source 3
    target 655
  ]
  edge [
    source 3
    target 656
  ]
  edge [
    source 3
    target 657
  ]
  edge [
    source 3
    target 658
  ]
  edge [
    source 3
    target 659
  ]
  edge [
    source 3
    target 660
  ]
  edge [
    source 3
    target 661
  ]
  edge [
    source 3
    target 662
  ]
  edge [
    source 3
    target 663
  ]
  edge [
    source 3
    target 664
  ]
  edge [
    source 3
    target 665
  ]
  edge [
    source 3
    target 666
  ]
  edge [
    source 3
    target 667
  ]
  edge [
    source 3
    target 668
  ]
  edge [
    source 3
    target 669
  ]
  edge [
    source 3
    target 670
  ]
  edge [
    source 3
    target 671
  ]
  edge [
    source 3
    target 672
  ]
  edge [
    source 3
    target 673
  ]
  edge [
    source 3
    target 674
  ]
  edge [
    source 3
    target 675
  ]
  edge [
    source 3
    target 676
  ]
  edge [
    source 3
    target 677
  ]
  edge [
    source 3
    target 678
  ]
  edge [
    source 3
    target 679
  ]
  edge [
    source 3
    target 680
  ]
  edge [
    source 3
    target 681
  ]
  edge [
    source 3
    target 682
  ]
  edge [
    source 3
    target 683
  ]
  edge [
    source 3
    target 684
  ]
  edge [
    source 3
    target 685
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 686
  ]
  edge [
    source 3
    target 687
  ]
  edge [
    source 3
    target 688
  ]
  edge [
    source 3
    target 689
  ]
  edge [
    source 3
    target 690
  ]
  edge [
    source 3
    target 691
  ]
  edge [
    source 3
    target 692
  ]
  edge [
    source 3
    target 693
  ]
  edge [
    source 3
    target 694
  ]
  edge [
    source 3
    target 695
  ]
  edge [
    source 3
    target 696
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 697
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 698
  ]
  edge [
    source 3
    target 699
  ]
  edge [
    source 3
    target 700
  ]
  edge [
    source 3
    target 701
  ]
  edge [
    source 3
    target 702
  ]
  edge [
    source 3
    target 703
  ]
  edge [
    source 3
    target 704
  ]
  edge [
    source 3
    target 705
  ]
  edge [
    source 3
    target 706
  ]
  edge [
    source 3
    target 707
  ]
  edge [
    source 3
    target 708
  ]
  edge [
    source 3
    target 709
  ]
  edge [
    source 3
    target 710
  ]
  edge [
    source 3
    target 711
  ]
  edge [
    source 3
    target 712
  ]
  edge [
    source 3
    target 713
  ]
  edge [
    source 3
    target 714
  ]
  edge [
    source 3
    target 715
  ]
  edge [
    source 3
    target 716
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 193
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 24
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 910
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 624
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 63
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 54
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 73
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 57
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 63
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 77
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 88
  ]
  edge [
    source 19
    target 68
  ]
  edge [
    source 19
    target 64
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 85
  ]
  edge [
    source 19
    target 72
  ]
  edge [
    source 19
    target 74
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 65
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 66
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 73
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 132
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 134
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 137
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 78
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1039
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 109
  ]
  edge [
    source 22
    target 1040
  ]
  edge [
    source 22
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 967
  ]
  edge [
    source 24
    target 1048
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1049
  ]
  edge [
    source 26
    target 1050
  ]
  edge [
    source 26
    target 1051
  ]
  edge [
    source 26
    target 1052
  ]
  edge [
    source 26
    target 1053
  ]
  edge [
    source 26
    target 1054
  ]
  edge [
    source 26
    target 1055
  ]
  edge [
    source 26
    target 1056
  ]
  edge [
    source 26
    target 1057
  ]
  edge [
    source 26
    target 804
  ]
  edge [
    source 26
    target 1058
  ]
  edge [
    source 26
    target 1059
  ]
  edge [
    source 26
    target 580
  ]
  edge [
    source 26
    target 1060
  ]
  edge [
    source 26
    target 1061
  ]
  edge [
    source 26
    target 1062
  ]
  edge [
    source 26
    target 1063
  ]
  edge [
    source 26
    target 1064
  ]
  edge [
    source 26
    target 1065
  ]
  edge [
    source 26
    target 1066
  ]
  edge [
    source 26
    target 1067
  ]
  edge [
    source 26
    target 1068
  ]
  edge [
    source 26
    target 1069
  ]
  edge [
    source 26
    target 1070
  ]
  edge [
    source 26
    target 1071
  ]
  edge [
    source 26
    target 1072
  ]
  edge [
    source 26
    target 1073
  ]
  edge [
    source 26
    target 1074
  ]
  edge [
    source 26
    target 1075
  ]
  edge [
    source 26
    target 1076
  ]
  edge [
    source 26
    target 1077
  ]
  edge [
    source 26
    target 1078
  ]
  edge [
    source 26
    target 1079
  ]
  edge [
    source 26
    target 1080
  ]
  edge [
    source 26
    target 1081
  ]
  edge [
    source 26
    target 1082
  ]
  edge [
    source 26
    target 1083
  ]
  edge [
    source 26
    target 1084
  ]
  edge [
    source 26
    target 1085
  ]
  edge [
    source 26
    target 1086
  ]
  edge [
    source 26
    target 1087
  ]
  edge [
    source 26
    target 1088
  ]
  edge [
    source 26
    target 1089
  ]
  edge [
    source 26
    target 1090
  ]
  edge [
    source 26
    target 1091
  ]
  edge [
    source 26
    target 1092
  ]
  edge [
    source 26
    target 1093
  ]
  edge [
    source 26
    target 1094
  ]
  edge [
    source 26
    target 1095
  ]
  edge [
    source 26
    target 1096
  ]
  edge [
    source 26
    target 1097
  ]
  edge [
    source 26
    target 1098
  ]
  edge [
    source 26
    target 1099
  ]
  edge [
    source 26
    target 1100
  ]
  edge [
    source 26
    target 1101
  ]
  edge [
    source 26
    target 1102
  ]
  edge [
    source 26
    target 1103
  ]
  edge [
    source 26
    target 1104
  ]
  edge [
    source 26
    target 1105
  ]
  edge [
    source 26
    target 1106
  ]
  edge [
    source 26
    target 1107
  ]
  edge [
    source 26
    target 1108
  ]
  edge [
    source 26
    target 1109
  ]
  edge [
    source 26
    target 1110
  ]
  edge [
    source 26
    target 1111
  ]
  edge [
    source 26
    target 1112
  ]
  edge [
    source 26
    target 1113
  ]
  edge [
    source 26
    target 1114
  ]
  edge [
    source 26
    target 1115
  ]
  edge [
    source 26
    target 1116
  ]
  edge [
    source 26
    target 1117
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1118
  ]
  edge [
    source 27
    target 669
  ]
  edge [
    source 27
    target 1119
  ]
  edge [
    source 27
    target 1120
  ]
  edge [
    source 27
    target 1121
  ]
  edge [
    source 27
    target 1122
  ]
  edge [
    source 27
    target 1123
  ]
  edge [
    source 27
    target 583
  ]
  edge [
    source 27
    target 1124
  ]
  edge [
    source 27
    target 1125
  ]
  edge [
    source 27
    target 1126
  ]
  edge [
    source 27
    target 1127
  ]
  edge [
    source 27
    target 1128
  ]
  edge [
    source 27
    target 1129
  ]
  edge [
    source 27
    target 1130
  ]
  edge [
    source 27
    target 1131
  ]
  edge [
    source 27
    target 1132
  ]
  edge [
    source 27
    target 1133
  ]
  edge [
    source 27
    target 1134
  ]
  edge [
    source 27
    target 1135
  ]
  edge [
    source 27
    target 1136
  ]
  edge [
    source 27
    target 1137
  ]
  edge [
    source 27
    target 1138
  ]
  edge [
    source 27
    target 600
  ]
  edge [
    source 27
    target 1139
  ]
  edge [
    source 27
    target 1140
  ]
  edge [
    source 27
    target 1141
  ]
  edge [
    source 27
    target 1142
  ]
  edge [
    source 27
    target 1143
  ]
  edge [
    source 27
    target 1144
  ]
  edge [
    source 27
    target 1145
  ]
  edge [
    source 27
    target 1146
  ]
  edge [
    source 27
    target 1147
  ]
  edge [
    source 27
    target 1148
  ]
  edge [
    source 27
    target 1149
  ]
  edge [
    source 27
    target 1150
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1151
  ]
  edge [
    source 28
    target 1152
  ]
  edge [
    source 28
    target 1153
  ]
  edge [
    source 28
    target 1154
  ]
  edge [
    source 28
    target 1155
  ]
  edge [
    source 28
    target 1156
  ]
  edge [
    source 28
    target 1157
  ]
  edge [
    source 28
    target 1158
  ]
  edge [
    source 28
    target 1159
  ]
  edge [
    source 28
    target 1160
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1161
  ]
  edge [
    source 29
    target 1162
  ]
  edge [
    source 29
    target 1163
  ]
  edge [
    source 29
    target 1164
  ]
  edge [
    source 29
    target 1165
  ]
  edge [
    source 29
    target 1166
  ]
  edge [
    source 29
    target 490
  ]
  edge [
    source 29
    target 1167
  ]
  edge [
    source 29
    target 1168
  ]
  edge [
    source 29
    target 497
  ]
  edge [
    source 29
    target 1169
  ]
  edge [
    source 29
    target 1170
  ]
  edge [
    source 29
    target 1171
  ]
  edge [
    source 29
    target 1172
  ]
  edge [
    source 29
    target 1173
  ]
  edge [
    source 29
    target 1174
  ]
  edge [
    source 29
    target 1175
  ]
  edge [
    source 29
    target 1176
  ]
  edge [
    source 29
    target 1177
  ]
  edge [
    source 29
    target 38
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1178
  ]
  edge [
    source 31
    target 1179
  ]
  edge [
    source 31
    target 1180
  ]
  edge [
    source 31
    target 1181
  ]
  edge [
    source 31
    target 1182
  ]
  edge [
    source 31
    target 1183
  ]
  edge [
    source 31
    target 1184
  ]
  edge [
    source 31
    target 1185
  ]
  edge [
    source 31
    target 1186
  ]
  edge [
    source 31
    target 1187
  ]
  edge [
    source 31
    target 1188
  ]
  edge [
    source 31
    target 1189
  ]
  edge [
    source 31
    target 1190
  ]
  edge [
    source 31
    target 1191
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 31
    target 1192
  ]
  edge [
    source 31
    target 1193
  ]
  edge [
    source 31
    target 1194
  ]
  edge [
    source 31
    target 1195
  ]
  edge [
    source 31
    target 1196
  ]
  edge [
    source 31
    target 1197
  ]
  edge [
    source 31
    target 1198
  ]
  edge [
    source 31
    target 1199
  ]
  edge [
    source 31
    target 1200
  ]
  edge [
    source 31
    target 1201
  ]
  edge [
    source 31
    target 1202
  ]
  edge [
    source 31
    target 1203
  ]
  edge [
    source 31
    target 1204
  ]
  edge [
    source 31
    target 1205
  ]
  edge [
    source 31
    target 1206
  ]
  edge [
    source 31
    target 1207
  ]
  edge [
    source 31
    target 1208
  ]
  edge [
    source 31
    target 1209
  ]
  edge [
    source 31
    target 1210
  ]
  edge [
    source 31
    target 1211
  ]
  edge [
    source 31
    target 1212
  ]
  edge [
    source 31
    target 1213
  ]
  edge [
    source 31
    target 1214
  ]
  edge [
    source 31
    target 1215
  ]
  edge [
    source 31
    target 1216
  ]
  edge [
    source 31
    target 1217
  ]
  edge [
    source 31
    target 1218
  ]
  edge [
    source 31
    target 1219
  ]
  edge [
    source 31
    target 1220
  ]
  edge [
    source 31
    target 1221
  ]
  edge [
    source 31
    target 1222
  ]
  edge [
    source 31
    target 1223
  ]
  edge [
    source 31
    target 1224
  ]
  edge [
    source 31
    target 1225
  ]
  edge [
    source 31
    target 1226
  ]
  edge [
    source 31
    target 1227
  ]
  edge [
    source 31
    target 1228
  ]
  edge [
    source 31
    target 1229
  ]
  edge [
    source 31
    target 1230
  ]
  edge [
    source 31
    target 1231
  ]
  edge [
    source 31
    target 1232
  ]
  edge [
    source 31
    target 1233
  ]
  edge [
    source 31
    target 1234
  ]
  edge [
    source 31
    target 1235
  ]
  edge [
    source 31
    target 1236
  ]
  edge [
    source 31
    target 1031
  ]
  edge [
    source 31
    target 1237
  ]
  edge [
    source 31
    target 1238
  ]
  edge [
    source 31
    target 1239
  ]
  edge [
    source 31
    target 1240
  ]
  edge [
    source 31
    target 1241
  ]
  edge [
    source 31
    target 1242
  ]
  edge [
    source 31
    target 1243
  ]
  edge [
    source 31
    target 1244
  ]
  edge [
    source 31
    target 1245
  ]
  edge [
    source 31
    target 1246
  ]
  edge [
    source 31
    target 1247
  ]
  edge [
    source 31
    target 1248
  ]
  edge [
    source 31
    target 1249
  ]
  edge [
    source 31
    target 1250
  ]
  edge [
    source 31
    target 1251
  ]
  edge [
    source 31
    target 47
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1252
  ]
  edge [
    source 32
    target 1253
  ]
  edge [
    source 32
    target 669
  ]
  edge [
    source 32
    target 606
  ]
  edge [
    source 32
    target 1254
  ]
  edge [
    source 32
    target 1255
  ]
  edge [
    source 32
    target 1256
  ]
  edge [
    source 32
    target 1257
  ]
  edge [
    source 32
    target 1258
  ]
  edge [
    source 32
    target 887
  ]
  edge [
    source 32
    target 888
  ]
  edge [
    source 32
    target 889
  ]
  edge [
    source 32
    target 890
  ]
  edge [
    source 32
    target 891
  ]
  edge [
    source 32
    target 892
  ]
  edge [
    source 32
    target 893
  ]
  edge [
    source 32
    target 1123
  ]
  edge [
    source 32
    target 583
  ]
  edge [
    source 32
    target 1124
  ]
  edge [
    source 32
    target 1126
  ]
  edge [
    source 32
    target 1125
  ]
  edge [
    source 32
    target 1129
  ]
  edge [
    source 32
    target 1127
  ]
  edge [
    source 32
    target 1128
  ]
  edge [
    source 32
    target 1130
  ]
  edge [
    source 32
    target 1131
  ]
  edge [
    source 32
    target 1132
  ]
  edge [
    source 32
    target 1133
  ]
  edge [
    source 32
    target 1134
  ]
  edge [
    source 32
    target 1135
  ]
  edge [
    source 32
    target 1136
  ]
  edge [
    source 32
    target 1137
  ]
  edge [
    source 32
    target 1138
  ]
  edge [
    source 32
    target 600
  ]
  edge [
    source 32
    target 1259
  ]
  edge [
    source 32
    target 718
  ]
  edge [
    source 32
    target 755
  ]
  edge [
    source 32
    target 1260
  ]
  edge [
    source 32
    target 769
  ]
  edge [
    source 32
    target 1261
  ]
  edge [
    source 32
    target 1262
  ]
  edge [
    source 32
    target 1263
  ]
  edge [
    source 32
    target 1264
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1276
  ]
  edge [
    source 33
    target 857
  ]
  edge [
    source 33
    target 1277
  ]
  edge [
    source 33
    target 1278
  ]
  edge [
    source 33
    target 1279
  ]
  edge [
    source 33
    target 1280
  ]
  edge [
    source 33
    target 1281
  ]
  edge [
    source 33
    target 1282
  ]
  edge [
    source 33
    target 1283
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1284
  ]
  edge [
    source 34
    target 1285
  ]
  edge [
    source 34
    target 1286
  ]
  edge [
    source 34
    target 1287
  ]
  edge [
    source 34
    target 701
  ]
  edge [
    source 34
    target 1288
  ]
  edge [
    source 34
    target 1289
  ]
  edge [
    source 34
    target 1290
  ]
  edge [
    source 34
    target 1291
  ]
  edge [
    source 34
    target 1292
  ]
  edge [
    source 34
    target 1293
  ]
  edge [
    source 34
    target 741
  ]
  edge [
    source 34
    target 1294
  ]
  edge [
    source 34
    target 1295
  ]
  edge [
    source 34
    target 1296
  ]
  edge [
    source 34
    target 1297
  ]
  edge [
    source 34
    target 1298
  ]
  edge [
    source 34
    target 1299
  ]
  edge [
    source 34
    target 1300
  ]
  edge [
    source 34
    target 1301
  ]
  edge [
    source 34
    target 1302
  ]
  edge [
    source 34
    target 1303
  ]
  edge [
    source 34
    target 1304
  ]
  edge [
    source 34
    target 1305
  ]
  edge [
    source 34
    target 1306
  ]
  edge [
    source 34
    target 1307
  ]
  edge [
    source 34
    target 1308
  ]
  edge [
    source 34
    target 1309
  ]
  edge [
    source 34
    target 1310
  ]
  edge [
    source 34
    target 1311
  ]
  edge [
    source 34
    target 1312
  ]
  edge [
    source 34
    target 1313
  ]
  edge [
    source 34
    target 1314
  ]
  edge [
    source 34
    target 1315
  ]
  edge [
    source 34
    target 1316
  ]
  edge [
    source 34
    target 1317
  ]
  edge [
    source 34
    target 1318
  ]
  edge [
    source 34
    target 1319
  ]
  edge [
    source 34
    target 1320
  ]
  edge [
    source 34
    target 1321
  ]
  edge [
    source 34
    target 1322
  ]
  edge [
    source 34
    target 1323
  ]
  edge [
    source 34
    target 1324
  ]
  edge [
    source 34
    target 1325
  ]
  edge [
    source 34
    target 1326
  ]
  edge [
    source 34
    target 1327
  ]
  edge [
    source 34
    target 1328
  ]
  edge [
    source 34
    target 637
  ]
  edge [
    source 34
    target 1329
  ]
  edge [
    source 34
    target 1330
  ]
  edge [
    source 34
    target 1331
  ]
  edge [
    source 34
    target 1332
  ]
  edge [
    source 34
    target 1333
  ]
  edge [
    source 34
    target 1334
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1335
  ]
  edge [
    source 36
    target 1336
  ]
  edge [
    source 36
    target 1337
  ]
  edge [
    source 36
    target 1338
  ]
  edge [
    source 36
    target 1339
  ]
  edge [
    source 36
    target 1340
  ]
  edge [
    source 36
    target 1209
  ]
  edge [
    source 36
    target 1341
  ]
  edge [
    source 36
    target 1342
  ]
  edge [
    source 36
    target 1343
  ]
  edge [
    source 36
    target 1344
  ]
  edge [
    source 36
    target 1345
  ]
  edge [
    source 36
    target 1148
  ]
  edge [
    source 36
    target 1346
  ]
  edge [
    source 36
    target 518
  ]
  edge [
    source 36
    target 1347
  ]
  edge [
    source 36
    target 1348
  ]
  edge [
    source 36
    target 1349
  ]
  edge [
    source 36
    target 1350
  ]
  edge [
    source 36
    target 1351
  ]
  edge [
    source 36
    target 1352
  ]
  edge [
    source 36
    target 1353
  ]
  edge [
    source 36
    target 1354
  ]
  edge [
    source 36
    target 1355
  ]
  edge [
    source 36
    target 1356
  ]
  edge [
    source 36
    target 626
  ]
  edge [
    source 36
    target 1357
  ]
  edge [
    source 36
    target 1358
  ]
  edge [
    source 36
    target 1359
  ]
  edge [
    source 36
    target 1360
  ]
  edge [
    source 36
    target 1361
  ]
  edge [
    source 36
    target 1362
  ]
  edge [
    source 36
    target 1363
  ]
  edge [
    source 36
    target 1364
  ]
  edge [
    source 36
    target 1365
  ]
  edge [
    source 36
    target 1366
  ]
  edge [
    source 36
    target 1367
  ]
  edge [
    source 36
    target 660
  ]
  edge [
    source 36
    target 1368
  ]
  edge [
    source 36
    target 1369
  ]
  edge [
    source 36
    target 1370
  ]
  edge [
    source 36
    target 1371
  ]
  edge [
    source 36
    target 623
  ]
  edge [
    source 36
    target 1372
  ]
  edge [
    source 36
    target 1373
  ]
  edge [
    source 36
    target 670
  ]
  edge [
    source 36
    target 1182
  ]
  edge [
    source 36
    target 1374
  ]
  edge [
    source 36
    target 1375
  ]
  edge [
    source 36
    target 1376
  ]
  edge [
    source 36
    target 1377
  ]
  edge [
    source 36
    target 1378
  ]
  edge [
    source 36
    target 1379
  ]
  edge [
    source 36
    target 1380
  ]
  edge [
    source 36
    target 1381
  ]
  edge [
    source 36
    target 583
  ]
  edge [
    source 36
    target 1382
  ]
  edge [
    source 36
    target 1383
  ]
  edge [
    source 36
    target 765
  ]
  edge [
    source 36
    target 1384
  ]
  edge [
    source 36
    target 1385
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1386
  ]
  edge [
    source 37
    target 1387
  ]
  edge [
    source 37
    target 1388
  ]
  edge [
    source 37
    target 1389
  ]
  edge [
    source 37
    target 1390
  ]
  edge [
    source 37
    target 626
  ]
  edge [
    source 37
    target 1391
  ]
  edge [
    source 37
    target 1392
  ]
  edge [
    source 37
    target 1393
  ]
  edge [
    source 37
    target 1394
  ]
  edge [
    source 37
    target 1395
  ]
  edge [
    source 37
    target 1396
  ]
  edge [
    source 37
    target 1397
  ]
  edge [
    source 37
    target 1398
  ]
  edge [
    source 37
    target 1399
  ]
  edge [
    source 37
    target 1400
  ]
  edge [
    source 37
    target 1401
  ]
  edge [
    source 37
    target 1402
  ]
  edge [
    source 37
    target 1403
  ]
  edge [
    source 37
    target 1404
  ]
  edge [
    source 37
    target 1405
  ]
  edge [
    source 37
    target 1406
  ]
  edge [
    source 37
    target 660
  ]
  edge [
    source 37
    target 1407
  ]
  edge [
    source 37
    target 1408
  ]
  edge [
    source 37
    target 1409
  ]
  edge [
    source 37
    target 1410
  ]
  edge [
    source 37
    target 1411
  ]
  edge [
    source 37
    target 1412
  ]
  edge [
    source 37
    target 1413
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1414
  ]
  edge [
    source 38
    target 1415
  ]
  edge [
    source 38
    target 1416
  ]
  edge [
    source 38
    target 1417
  ]
  edge [
    source 38
    target 754
  ]
  edge [
    source 38
    target 1418
  ]
  edge [
    source 38
    target 1419
  ]
  edge [
    source 38
    target 1246
  ]
  edge [
    source 38
    target 1420
  ]
  edge [
    source 38
    target 1421
  ]
  edge [
    source 38
    target 1422
  ]
  edge [
    source 38
    target 1423
  ]
  edge [
    source 38
    target 1424
  ]
  edge [
    source 38
    target 1425
  ]
  edge [
    source 38
    target 1426
  ]
  edge [
    source 38
    target 1427
  ]
  edge [
    source 38
    target 1428
  ]
  edge [
    source 38
    target 1429
  ]
  edge [
    source 38
    target 1430
  ]
  edge [
    source 38
    target 1431
  ]
  edge [
    source 38
    target 1432
  ]
  edge [
    source 38
    target 1433
  ]
  edge [
    source 38
    target 1178
  ]
  edge [
    source 38
    target 1434
  ]
  edge [
    source 38
    target 1435
  ]
  edge [
    source 38
    target 1436
  ]
  edge [
    source 38
    target 1185
  ]
  edge [
    source 38
    target 1437
  ]
  edge [
    source 38
    target 1438
  ]
  edge [
    source 38
    target 1439
  ]
  edge [
    source 38
    target 1440
  ]
  edge [
    source 38
    target 1441
  ]
  edge [
    source 38
    target 1442
  ]
  edge [
    source 38
    target 1443
  ]
  edge [
    source 38
    target 1444
  ]
  edge [
    source 38
    target 1445
  ]
  edge [
    source 38
    target 1446
  ]
  edge [
    source 38
    target 1447
  ]
  edge [
    source 38
    target 1448
  ]
  edge [
    source 38
    target 1449
  ]
  edge [
    source 38
    target 1450
  ]
  edge [
    source 38
    target 1451
  ]
  edge [
    source 38
    target 1452
  ]
  edge [
    source 38
    target 1453
  ]
  edge [
    source 38
    target 1454
  ]
  edge [
    source 38
    target 1455
  ]
  edge [
    source 38
    target 1456
  ]
  edge [
    source 38
    target 1457
  ]
  edge [
    source 38
    target 1230
  ]
  edge [
    source 38
    target 1458
  ]
  edge [
    source 38
    target 1459
  ]
  edge [
    source 38
    target 1251
  ]
  edge [
    source 38
    target 1243
  ]
  edge [
    source 38
    target 1460
  ]
  edge [
    source 38
    target 1461
  ]
  edge [
    source 38
    target 1245
  ]
  edge [
    source 38
    target 1462
  ]
  edge [
    source 38
    target 1463
  ]
  edge [
    source 38
    target 1464
  ]
  edge [
    source 38
    target 1465
  ]
  edge [
    source 38
    target 1466
  ]
  edge [
    source 38
    target 1467
  ]
  edge [
    source 38
    target 1468
  ]
  edge [
    source 38
    target 1469
  ]
  edge [
    source 38
    target 1179
  ]
  edge [
    source 38
    target 1180
  ]
  edge [
    source 38
    target 1181
  ]
  edge [
    source 38
    target 1182
  ]
  edge [
    source 38
    target 1183
  ]
  edge [
    source 38
    target 1184
  ]
  edge [
    source 38
    target 1186
  ]
  edge [
    source 38
    target 1187
  ]
  edge [
    source 38
    target 1188
  ]
  edge [
    source 38
    target 1189
  ]
  edge [
    source 38
    target 1190
  ]
  edge [
    source 38
    target 1191
  ]
  edge [
    source 38
    target 1470
  ]
  edge [
    source 38
    target 1471
  ]
  edge [
    source 38
    target 1472
  ]
  edge [
    source 38
    target 1473
  ]
  edge [
    source 38
    target 1474
  ]
  edge [
    source 38
    target 1475
  ]
  edge [
    source 38
    target 1476
  ]
  edge [
    source 38
    target 1477
  ]
  edge [
    source 38
    target 1478
  ]
  edge [
    source 38
    target 1479
  ]
  edge [
    source 38
    target 1480
  ]
  edge [
    source 38
    target 1481
  ]
  edge [
    source 38
    target 1482
  ]
  edge [
    source 38
    target 1483
  ]
  edge [
    source 38
    target 1484
  ]
  edge [
    source 38
    target 1485
  ]
  edge [
    source 38
    target 1486
  ]
  edge [
    source 38
    target 1487
  ]
  edge [
    source 38
    target 1488
  ]
  edge [
    source 38
    target 1489
  ]
  edge [
    source 38
    target 1490
  ]
  edge [
    source 38
    target 1491
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1492
  ]
  edge [
    source 39
    target 1493
  ]
  edge [
    source 39
    target 788
  ]
  edge [
    source 39
    target 1494
  ]
  edge [
    source 39
    target 1370
  ]
  edge [
    source 39
    target 1055
  ]
  edge [
    source 39
    target 1495
  ]
  edge [
    source 39
    target 1496
  ]
  edge [
    source 39
    target 1497
  ]
  edge [
    source 39
    target 1498
  ]
  edge [
    source 39
    target 1499
  ]
  edge [
    source 39
    target 1500
  ]
  edge [
    source 39
    target 1501
  ]
  edge [
    source 39
    target 1299
  ]
  edge [
    source 39
    target 1400
  ]
  edge [
    source 39
    target 606
  ]
  edge [
    source 39
    target 1502
  ]
  edge [
    source 39
    target 992
  ]
  edge [
    source 39
    target 1056
  ]
  edge [
    source 39
    target 1057
  ]
  edge [
    source 39
    target 804
  ]
  edge [
    source 39
    target 1058
  ]
  edge [
    source 39
    target 1059
  ]
  edge [
    source 39
    target 580
  ]
  edge [
    source 39
    target 1503
  ]
  edge [
    source 39
    target 1504
  ]
  edge [
    source 39
    target 1349
  ]
  edge [
    source 39
    target 1505
  ]
  edge [
    source 39
    target 1506
  ]
  edge [
    source 39
    target 1402
  ]
  edge [
    source 39
    target 1507
  ]
  edge [
    source 39
    target 1508
  ]
  edge [
    source 39
    target 1509
  ]
  edge [
    source 39
    target 1510
  ]
  edge [
    source 39
    target 1511
  ]
  edge [
    source 39
    target 1512
  ]
  edge [
    source 39
    target 1513
  ]
  edge [
    source 39
    target 1514
  ]
  edge [
    source 39
    target 1515
  ]
  edge [
    source 39
    target 1516
  ]
  edge [
    source 39
    target 1517
  ]
  edge [
    source 39
    target 1518
  ]
  edge [
    source 39
    target 1519
  ]
  edge [
    source 39
    target 701
  ]
  edge [
    source 39
    target 1520
  ]
  edge [
    source 39
    target 1521
  ]
  edge [
    source 39
    target 1371
  ]
  edge [
    source 39
    target 1522
  ]
  edge [
    source 39
    target 1523
  ]
  edge [
    source 39
    target 1524
  ]
  edge [
    source 39
    target 875
  ]
  edge [
    source 39
    target 876
  ]
  edge [
    source 39
    target 1525
  ]
  edge [
    source 39
    target 877
  ]
  edge [
    source 39
    target 1337
  ]
  edge [
    source 39
    target 879
  ]
  edge [
    source 39
    target 1526
  ]
  edge [
    source 39
    target 880
  ]
  edge [
    source 39
    target 881
  ]
  edge [
    source 39
    target 882
  ]
  edge [
    source 39
    target 1527
  ]
  edge [
    source 39
    target 883
  ]
  edge [
    source 39
    target 884
  ]
  edge [
    source 39
    target 600
  ]
  edge [
    source 39
    target 987
  ]
  edge [
    source 39
    target 885
  ]
  edge [
    source 39
    target 886
  ]
  edge [
    source 39
    target 1528
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1355
  ]
  edge [
    source 41
    target 1529
  ]
  edge [
    source 41
    target 1299
  ]
  edge [
    source 41
    target 1530
  ]
  edge [
    source 41
    target 1531
  ]
  edge [
    source 41
    target 948
  ]
  edge [
    source 41
    target 1532
  ]
  edge [
    source 41
    target 1533
  ]
  edge [
    source 41
    target 1534
  ]
  edge [
    source 41
    target 891
  ]
  edge [
    source 41
    target 1535
  ]
  edge [
    source 41
    target 1536
  ]
  edge [
    source 41
    target 1537
  ]
  edge [
    source 41
    target 1538
  ]
  edge [
    source 41
    target 1539
  ]
  edge [
    source 41
    target 1540
  ]
  edge [
    source 41
    target 1541
  ]
  edge [
    source 41
    target 1542
  ]
  edge [
    source 41
    target 1543
  ]
  edge [
    source 41
    target 1148
  ]
  edge [
    source 41
    target 1544
  ]
  edge [
    source 41
    target 1275
  ]
  edge [
    source 41
    target 1545
  ]
  edge [
    source 41
    target 1546
  ]
  edge [
    source 41
    target 1547
  ]
  edge [
    source 41
    target 1548
  ]
  edge [
    source 41
    target 1549
  ]
  edge [
    source 41
    target 1550
  ]
  edge [
    source 41
    target 1551
  ]
  edge [
    source 41
    target 1552
  ]
  edge [
    source 41
    target 1553
  ]
  edge [
    source 41
    target 1554
  ]
  edge [
    source 41
    target 1555
  ]
  edge [
    source 41
    target 1556
  ]
  edge [
    source 41
    target 1557
  ]
  edge [
    source 41
    target 1558
  ]
  edge [
    source 41
    target 1559
  ]
  edge [
    source 41
    target 1560
  ]
  edge [
    source 41
    target 1561
  ]
  edge [
    source 41
    target 1562
  ]
  edge [
    source 41
    target 1563
  ]
  edge [
    source 41
    target 1564
  ]
  edge [
    source 41
    target 1565
  ]
  edge [
    source 41
    target 1319
  ]
  edge [
    source 41
    target 1566
  ]
  edge [
    source 41
    target 1567
  ]
  edge [
    source 41
    target 1568
  ]
  edge [
    source 41
    target 1569
  ]
  edge [
    source 41
    target 1570
  ]
  edge [
    source 41
    target 1571
  ]
  edge [
    source 41
    target 1572
  ]
  edge [
    source 41
    target 701
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1573
  ]
  edge [
    source 43
    target 63
  ]
  edge [
    source 43
    target 1574
  ]
  edge [
    source 43
    target 1575
  ]
  edge [
    source 43
    target 1011
  ]
  edge [
    source 43
    target 1576
  ]
  edge [
    source 43
    target 1577
  ]
  edge [
    source 43
    target 1578
  ]
  edge [
    source 43
    target 1579
  ]
  edge [
    source 43
    target 1580
  ]
  edge [
    source 43
    target 660
  ]
  edge [
    source 43
    target 116
  ]
  edge [
    source 43
    target 117
  ]
  edge [
    source 43
    target 118
  ]
  edge [
    source 43
    target 119
  ]
  edge [
    source 43
    target 120
  ]
  edge [
    source 43
    target 121
  ]
  edge [
    source 43
    target 122
  ]
  edge [
    source 43
    target 123
  ]
  edge [
    source 43
    target 124
  ]
  edge [
    source 43
    target 125
  ]
  edge [
    source 43
    target 126
  ]
  edge [
    source 43
    target 127
  ]
  edge [
    source 43
    target 128
  ]
  edge [
    source 43
    target 129
  ]
  edge [
    source 43
    target 130
  ]
  edge [
    source 43
    target 131
  ]
  edge [
    source 43
    target 132
  ]
  edge [
    source 43
    target 133
  ]
  edge [
    source 43
    target 134
  ]
  edge [
    source 43
    target 135
  ]
  edge [
    source 43
    target 136
  ]
  edge [
    source 43
    target 137
  ]
  edge [
    source 43
    target 138
  ]
  edge [
    source 43
    target 139
  ]
  edge [
    source 43
    target 140
  ]
  edge [
    source 43
    target 1581
  ]
  edge [
    source 43
    target 1193
  ]
  edge [
    source 43
    target 1022
  ]
  edge [
    source 43
    target 1023
  ]
  edge [
    source 43
    target 1024
  ]
  edge [
    source 43
    target 1025
  ]
  edge [
    source 43
    target 1026
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1582
  ]
  edge [
    source 45
    target 1583
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1584
  ]
  edge [
    source 46
    target 1585
  ]
  edge [
    source 46
    target 1586
  ]
  edge [
    source 46
    target 1587
  ]
  edge [
    source 46
    target 1588
  ]
  edge [
    source 46
    target 1589
  ]
  edge [
    source 46
    target 1590
  ]
  edge [
    source 46
    target 1591
  ]
  edge [
    source 46
    target 650
  ]
  edge [
    source 46
    target 1592
  ]
  edge [
    source 46
    target 1593
  ]
  edge [
    source 46
    target 1594
  ]
  edge [
    source 46
    target 1595
  ]
  edge [
    source 46
    target 1596
  ]
  edge [
    source 46
    target 1597
  ]
  edge [
    source 46
    target 1598
  ]
  edge [
    source 46
    target 168
  ]
  edge [
    source 46
    target 1599
  ]
  edge [
    source 46
    target 1600
  ]
  edge [
    source 46
    target 1601
  ]
  edge [
    source 46
    target 1602
  ]
  edge [
    source 46
    target 1603
  ]
  edge [
    source 46
    target 1604
  ]
  edge [
    source 46
    target 1479
  ]
  edge [
    source 46
    target 1605
  ]
  edge [
    source 46
    target 1606
  ]
  edge [
    source 46
    target 1607
  ]
  edge [
    source 46
    target 1608
  ]
  edge [
    source 46
    target 1609
  ]
  edge [
    source 46
    target 1610
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1611
  ]
  edge [
    source 47
    target 1612
  ]
  edge [
    source 47
    target 1613
  ]
  edge [
    source 47
    target 1614
  ]
  edge [
    source 47
    target 1615
  ]
  edge [
    source 47
    target 1616
  ]
  edge [
    source 47
    target 1430
  ]
  edge [
    source 47
    target 595
  ]
  edge [
    source 47
    target 1617
  ]
  edge [
    source 47
    target 1618
  ]
  edge [
    source 47
    target 771
  ]
  edge [
    source 47
    target 1619
  ]
  edge [
    source 47
    target 1620
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1621
  ]
  edge [
    source 48
    target 1622
  ]
  edge [
    source 48
    target 1623
  ]
  edge [
    source 48
    target 961
  ]
  edge [
    source 48
    target 963
  ]
  edge [
    source 48
    target 1624
  ]
  edge [
    source 48
    target 1625
  ]
  edge [
    source 48
    target 1626
  ]
  edge [
    source 48
    target 1627
  ]
  edge [
    source 48
    target 1628
  ]
  edge [
    source 48
    target 1629
  ]
  edge [
    source 48
    target 1630
  ]
  edge [
    source 48
    target 1631
  ]
  edge [
    source 48
    target 1632
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1633
  ]
  edge [
    source 49
    target 1634
  ]
  edge [
    source 49
    target 1635
  ]
  edge [
    source 49
    target 1636
  ]
  edge [
    source 49
    target 1637
  ]
  edge [
    source 49
    target 1638
  ]
  edge [
    source 49
    target 1639
  ]
  edge [
    source 49
    target 542
  ]
  edge [
    source 49
    target 1640
  ]
  edge [
    source 49
    target 1345
  ]
  edge [
    source 49
    target 1641
  ]
  edge [
    source 49
    target 1642
  ]
  edge [
    source 49
    target 589
  ]
  edge [
    source 49
    target 1643
  ]
  edge [
    source 49
    target 1644
  ]
  edge [
    source 49
    target 1351
  ]
  edge [
    source 49
    target 1346
  ]
  edge [
    source 49
    target 1354
  ]
  edge [
    source 49
    target 1645
  ]
  edge [
    source 49
    target 1646
  ]
  edge [
    source 49
    target 1647
  ]
  edge [
    source 49
    target 1648
  ]
  edge [
    source 49
    target 1649
  ]
  edge [
    source 49
    target 1650
  ]
  edge [
    source 49
    target 1572
  ]
  edge [
    source 49
    target 1651
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1652
  ]
  edge [
    source 50
    target 1653
  ]
  edge [
    source 50
    target 1654
  ]
  edge [
    source 50
    target 1655
  ]
  edge [
    source 50
    target 1656
  ]
  edge [
    source 50
    target 1657
  ]
  edge [
    source 50
    target 1658
  ]
  edge [
    source 50
    target 1659
  ]
  edge [
    source 50
    target 1660
  ]
  edge [
    source 50
    target 1661
  ]
  edge [
    source 50
    target 1662
  ]
  edge [
    source 50
    target 1663
  ]
  edge [
    source 50
    target 1664
  ]
  edge [
    source 50
    target 1665
  ]
  edge [
    source 50
    target 1666
  ]
  edge [
    source 50
    target 1667
  ]
  edge [
    source 50
    target 1668
  ]
  edge [
    source 50
    target 1669
  ]
  edge [
    source 50
    target 1670
  ]
  edge [
    source 50
    target 1671
  ]
  edge [
    source 50
    target 1672
  ]
  edge [
    source 50
    target 1673
  ]
  edge [
    source 50
    target 1674
  ]
  edge [
    source 50
    target 1675
  ]
  edge [
    source 50
    target 1676
  ]
  edge [
    source 50
    target 134
  ]
  edge [
    source 50
    target 1677
  ]
  edge [
    source 50
    target 542
  ]
  edge [
    source 50
    target 126
  ]
  edge [
    source 50
    target 1070
  ]
  edge [
    source 50
    target 1678
  ]
  edge [
    source 50
    target 1679
  ]
  edge [
    source 50
    target 206
  ]
  edge [
    source 50
    target 1058
  ]
  edge [
    source 50
    target 1680
  ]
  edge [
    source 50
    target 1681
  ]
  edge [
    source 50
    target 1682
  ]
  edge [
    source 50
    target 1683
  ]
  edge [
    source 50
    target 1684
  ]
  edge [
    source 50
    target 1685
  ]
  edge [
    source 50
    target 1686
  ]
  edge [
    source 50
    target 621
  ]
  edge [
    source 50
    target 1687
  ]
  edge [
    source 50
    target 1688
  ]
  edge [
    source 50
    target 1689
  ]
  edge [
    source 50
    target 1690
  ]
  edge [
    source 50
    target 1691
  ]
  edge [
    source 50
    target 1692
  ]
  edge [
    source 50
    target 1693
  ]
  edge [
    source 50
    target 1499
  ]
  edge [
    source 50
    target 1694
  ]
  edge [
    source 50
    target 1695
  ]
  edge [
    source 50
    target 1696
  ]
  edge [
    source 50
    target 1697
  ]
  edge [
    source 50
    target 1698
  ]
  edge [
    source 50
    target 116
  ]
  edge [
    source 50
    target 1699
  ]
  edge [
    source 50
    target 1700
  ]
  edge [
    source 50
    target 1701
  ]
  edge [
    source 50
    target 1702
  ]
  edge [
    source 50
    target 1703
  ]
  edge [
    source 50
    target 1704
  ]
  edge [
    source 50
    target 1705
  ]
  edge [
    source 50
    target 121
  ]
  edge [
    source 50
    target 1706
  ]
  edge [
    source 50
    target 1302
  ]
  edge [
    source 50
    target 125
  ]
  edge [
    source 50
    target 127
  ]
  edge [
    source 50
    target 1707
  ]
  edge [
    source 50
    target 1708
  ]
  edge [
    source 50
    target 1709
  ]
  edge [
    source 50
    target 1710
  ]
  edge [
    source 50
    target 1711
  ]
  edge [
    source 50
    target 1712
  ]
  edge [
    source 50
    target 1713
  ]
  edge [
    source 50
    target 1316
  ]
  edge [
    source 50
    target 129
  ]
  edge [
    source 50
    target 1714
  ]
  edge [
    source 50
    target 1715
  ]
  edge [
    source 50
    target 130
  ]
  edge [
    source 50
    target 132
  ]
  edge [
    source 50
    target 133
  ]
  edge [
    source 50
    target 137
  ]
  edge [
    source 50
    target 1716
  ]
  edge [
    source 50
    target 1717
  ]
  edge [
    source 50
    target 589
  ]
  edge [
    source 50
    target 1718
  ]
  edge [
    source 50
    target 139
  ]
  edge [
    source 50
    target 1719
  ]
  edge [
    source 50
    target 1720
  ]
  edge [
    source 50
    target 1721
  ]
  edge [
    source 50
    target 1722
  ]
  edge [
    source 50
    target 1723
  ]
  edge [
    source 50
    target 1724
  ]
  edge [
    source 50
    target 1725
  ]
  edge [
    source 50
    target 1726
  ]
  edge [
    source 50
    target 1727
  ]
  edge [
    source 50
    target 135
  ]
  edge [
    source 50
    target 1728
  ]
  edge [
    source 50
    target 626
  ]
  edge [
    source 50
    target 1729
  ]
  edge [
    source 50
    target 1730
  ]
  edge [
    source 50
    target 1731
  ]
  edge [
    source 50
    target 1732
  ]
  edge [
    source 50
    target 1733
  ]
  edge [
    source 50
    target 1734
  ]
  edge [
    source 50
    target 1735
  ]
  edge [
    source 50
    target 913
  ]
  edge [
    source 50
    target 1736
  ]
  edge [
    source 50
    target 1737
  ]
  edge [
    source 50
    target 1738
  ]
  edge [
    source 50
    target 1739
  ]
  edge [
    source 50
    target 1740
  ]
  edge [
    source 50
    target 1741
  ]
  edge [
    source 50
    target 1742
  ]
  edge [
    source 50
    target 1221
  ]
  edge [
    source 50
    target 1743
  ]
  edge [
    source 50
    target 1744
  ]
  edge [
    source 50
    target 1745
  ]
  edge [
    source 50
    target 1169
  ]
  edge [
    source 50
    target 1746
  ]
  edge [
    source 50
    target 1747
  ]
  edge [
    source 50
    target 1748
  ]
  edge [
    source 50
    target 490
  ]
  edge [
    source 50
    target 1749
  ]
  edge [
    source 50
    target 1750
  ]
  edge [
    source 50
    target 1751
  ]
  edge [
    source 50
    target 1752
  ]
  edge [
    source 50
    target 1753
  ]
  edge [
    source 50
    target 1754
  ]
  edge [
    source 50
    target 1755
  ]
  edge [
    source 50
    target 1756
  ]
  edge [
    source 50
    target 1757
  ]
  edge [
    source 50
    target 1758
  ]
  edge [
    source 50
    target 1759
  ]
  edge [
    source 50
    target 1760
  ]
  edge [
    source 50
    target 1761
  ]
  edge [
    source 50
    target 1762
  ]
  edge [
    source 50
    target 1763
  ]
  edge [
    source 50
    target 1764
  ]
  edge [
    source 50
    target 1765
  ]
  edge [
    source 50
    target 1766
  ]
  edge [
    source 50
    target 1767
  ]
  edge [
    source 50
    target 1768
  ]
  edge [
    source 50
    target 1769
  ]
  edge [
    source 50
    target 1770
  ]
  edge [
    source 50
    target 1771
  ]
  edge [
    source 50
    target 1772
  ]
  edge [
    source 50
    target 1773
  ]
  edge [
    source 50
    target 1774
  ]
  edge [
    source 50
    target 1775
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1776
  ]
  edge [
    source 51
    target 1777
  ]
  edge [
    source 51
    target 1778
  ]
  edge [
    source 51
    target 1779
  ]
  edge [
    source 51
    target 726
  ]
  edge [
    source 51
    target 197
  ]
  edge [
    source 51
    target 1780
  ]
  edge [
    source 51
    target 1781
  ]
  edge [
    source 51
    target 1782
  ]
  edge [
    source 51
    target 1783
  ]
  edge [
    source 51
    target 1784
  ]
  edge [
    source 51
    target 1785
  ]
  edge [
    source 51
    target 1786
  ]
  edge [
    source 51
    target 1787
  ]
  edge [
    source 51
    target 493
  ]
  edge [
    source 51
    target 1788
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1789
  ]
  edge [
    source 52
    target 1299
  ]
  edge [
    source 52
    target 1355
  ]
  edge [
    source 52
    target 701
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 1790
  ]
  edge [
    source 53
    target 1791
  ]
  edge [
    source 53
    target 1792
  ]
  edge [
    source 53
    target 1779
  ]
  edge [
    source 53
    target 1793
  ]
  edge [
    source 53
    target 796
  ]
  edge [
    source 53
    target 1794
  ]
  edge [
    source 53
    target 1795
  ]
  edge [
    source 53
    target 780
  ]
  edge [
    source 53
    target 1796
  ]
  edge [
    source 53
    target 1797
  ]
  edge [
    source 53
    target 1798
  ]
  edge [
    source 53
    target 1799
  ]
  edge [
    source 53
    target 1800
  ]
  edge [
    source 53
    target 1801
  ]
  edge [
    source 53
    target 1802
  ]
  edge [
    source 53
    target 1803
  ]
  edge [
    source 53
    target 1804
  ]
  edge [
    source 53
    target 1805
  ]
  edge [
    source 53
    target 1806
  ]
  edge [
    source 53
    target 723
  ]
  edge [
    source 53
    target 1807
  ]
  edge [
    source 54
    target 1808
  ]
  edge [
    source 54
    target 1809
  ]
  edge [
    source 54
    target 507
  ]
  edge [
    source 54
    target 1166
  ]
  edge [
    source 54
    target 1810
  ]
  edge [
    source 54
    target 1169
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 1811
  ]
  edge [
    source 55
    target 569
  ]
  edge [
    source 55
    target 1812
  ]
  edge [
    source 55
    target 1813
  ]
  edge [
    source 55
    target 788
  ]
  edge [
    source 55
    target 1642
  ]
  edge [
    source 55
    target 425
  ]
  edge [
    source 55
    target 1814
  ]
  edge [
    source 55
    target 1815
  ]
  edge [
    source 55
    target 1495
  ]
  edge [
    source 55
    target 1496
  ]
  edge [
    source 55
    target 1497
  ]
  edge [
    source 55
    target 1498
  ]
  edge [
    source 55
    target 1499
  ]
  edge [
    source 55
    target 1500
  ]
  edge [
    source 55
    target 1501
  ]
  edge [
    source 55
    target 1299
  ]
  edge [
    source 55
    target 1400
  ]
  edge [
    source 55
    target 606
  ]
  edge [
    source 55
    target 1055
  ]
  edge [
    source 55
    target 1502
  ]
  edge [
    source 55
    target 992
  ]
  edge [
    source 55
    target 1816
  ]
  edge [
    source 55
    target 1817
  ]
  edge [
    source 55
    target 1818
  ]
  edge [
    source 55
    target 117
  ]
  edge [
    source 55
    target 1819
  ]
  edge [
    source 55
    target 621
  ]
  edge [
    source 55
    target 119
  ]
  edge [
    source 55
    target 1596
  ]
  edge [
    source 55
    target 1820
  ]
  edge [
    source 55
    target 626
  ]
  edge [
    source 55
    target 1821
  ]
  edge [
    source 55
    target 1822
  ]
  edge [
    source 55
    target 1823
  ]
  edge [
    source 55
    target 1824
  ]
  edge [
    source 55
    target 1825
  ]
  edge [
    source 55
    target 633
  ]
  edge [
    source 55
    target 1826
  ]
  edge [
    source 55
    target 1827
  ]
  edge [
    source 55
    target 1828
  ]
  edge [
    source 55
    target 1829
  ]
  edge [
    source 55
    target 1830
  ]
  edge [
    source 55
    target 1831
  ]
  edge [
    source 55
    target 1832
  ]
  edge [
    source 55
    target 1833
  ]
  edge [
    source 55
    target 1834
  ]
  edge [
    source 55
    target 1835
  ]
  edge [
    source 55
    target 576
  ]
  edge [
    source 55
    target 1836
  ]
  edge [
    source 55
    target 1740
  ]
  edge [
    source 55
    target 1837
  ]
  edge [
    source 55
    target 1838
  ]
  edge [
    source 55
    target 1839
  ]
  edge [
    source 55
    target 1840
  ]
  edge [
    source 55
    target 1841
  ]
  edge [
    source 55
    target 1842
  ]
  edge [
    source 55
    target 1843
  ]
  edge [
    source 55
    target 1844
  ]
  edge [
    source 55
    target 1845
  ]
  edge [
    source 55
    target 1846
  ]
  edge [
    source 55
    target 1847
  ]
  edge [
    source 55
    target 1848
  ]
  edge [
    source 55
    target 1849
  ]
  edge [
    source 55
    target 1850
  ]
  edge [
    source 55
    target 1851
  ]
  edge [
    source 55
    target 1852
  ]
  edge [
    source 55
    target 370
  ]
  edge [
    source 55
    target 1853
  ]
  edge [
    source 55
    target 1854
  ]
  edge [
    source 55
    target 1855
  ]
  edge [
    source 55
    target 1856
  ]
  edge [
    source 55
    target 1857
  ]
  edge [
    source 55
    target 1858
  ]
  edge [
    source 55
    target 1859
  ]
  edge [
    source 55
    target 1860
  ]
  edge [
    source 55
    target 1861
  ]
  edge [
    source 55
    target 1862
  ]
  edge [
    source 55
    target 1863
  ]
  edge [
    source 55
    target 1864
  ]
  edge [
    source 55
    target 1865
  ]
  edge [
    source 55
    target 1866
  ]
  edge [
    source 55
    target 1867
  ]
  edge [
    source 55
    target 1868
  ]
  edge [
    source 55
    target 1869
  ]
  edge [
    source 55
    target 1870
  ]
  edge [
    source 55
    target 1871
  ]
  edge [
    source 55
    target 1872
  ]
  edge [
    source 55
    target 1873
  ]
  edge [
    source 55
    target 652
  ]
  edge [
    source 55
    target 1874
  ]
  edge [
    source 55
    target 1053
  ]
  edge [
    source 55
    target 1875
  ]
  edge [
    source 55
    target 1876
  ]
  edge [
    source 55
    target 1877
  ]
  edge [
    source 55
    target 1878
  ]
  edge [
    source 55
    target 1879
  ]
  edge [
    source 55
    target 1880
  ]
  edge [
    source 55
    target 1881
  ]
  edge [
    source 55
    target 1882
  ]
  edge [
    source 55
    target 1883
  ]
  edge [
    source 55
    target 1884
  ]
  edge [
    source 55
    target 796
  ]
  edge [
    source 55
    target 1885
  ]
  edge [
    source 55
    target 1886
  ]
  edge [
    source 55
    target 1887
  ]
  edge [
    source 55
    target 1349
  ]
  edge [
    source 55
    target 1888
  ]
  edge [
    source 55
    target 1889
  ]
  edge [
    source 55
    target 1221
  ]
  edge [
    source 55
    target 297
  ]
  edge [
    source 55
    target 542
  ]
  edge [
    source 55
    target 130
  ]
  edge [
    source 55
    target 1890
  ]
  edge [
    source 55
    target 1638
  ]
  edge [
    source 55
    target 1891
  ]
  edge [
    source 55
    target 1892
  ]
  edge [
    source 55
    target 1893
  ]
  edge [
    source 55
    target 1894
  ]
  edge [
    source 55
    target 1895
  ]
  edge [
    source 55
    target 1896
  ]
  edge [
    source 55
    target 1897
  ]
  edge [
    source 55
    target 1898
  ]
  edge [
    source 55
    target 1899
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 1900
  ]
  edge [
    source 56
    target 1901
  ]
  edge [
    source 56
    target 1902
  ]
  edge [
    source 56
    target 63
  ]
  edge [
    source 56
    target 1903
  ]
  edge [
    source 56
    target 1904
  ]
  edge [
    source 56
    target 1905
  ]
  edge [
    source 56
    target 998
  ]
  edge [
    source 56
    target 1906
  ]
  edge [
    source 56
    target 1907
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 1641
  ]
  edge [
    source 59
    target 1908
  ]
  edge [
    source 59
    target 1909
  ]
  edge [
    source 59
    target 1910
  ]
  edge [
    source 59
    target 626
  ]
  edge [
    source 59
    target 1911
  ]
  edge [
    source 59
    target 1912
  ]
  edge [
    source 59
    target 1913
  ]
  edge [
    source 59
    target 1479
  ]
  edge [
    source 59
    target 1914
  ]
  edge [
    source 59
    target 1915
  ]
  edge [
    source 59
    target 1916
  ]
  edge [
    source 59
    target 1917
  ]
  edge [
    source 59
    target 1918
  ]
  edge [
    source 59
    target 1919
  ]
  edge [
    source 59
    target 1920
  ]
  edge [
    source 59
    target 1921
  ]
  edge [
    source 59
    target 1922
  ]
  edge [
    source 59
    target 1923
  ]
  edge [
    source 59
    target 1924
  ]
  edge [
    source 59
    target 723
  ]
  edge [
    source 59
    target 1925
  ]
  edge [
    source 59
    target 1245
  ]
  edge [
    source 59
    target 1926
  ]
  edge [
    source 59
    target 1927
  ]
  edge [
    source 59
    target 1928
  ]
  edge [
    source 59
    target 1929
  ]
  edge [
    source 59
    target 1930
  ]
  edge [
    source 59
    target 1931
  ]
  edge [
    source 59
    target 1932
  ]
  edge [
    source 59
    target 1933
  ]
  edge [
    source 59
    target 1934
  ]
  edge [
    source 59
    target 425
  ]
  edge [
    source 59
    target 1935
  ]
  edge [
    source 60
    target 1936
  ]
  edge [
    source 60
    target 1937
  ]
  edge [
    source 60
    target 1938
  ]
  edge [
    source 60
    target 1939
  ]
  edge [
    source 60
    target 493
  ]
  edge [
    source 60
    target 1940
  ]
  edge [
    source 60
    target 507
  ]
  edge [
    source 60
    target 1941
  ]
  edge [
    source 60
    target 1942
  ]
  edge [
    source 60
    target 1943
  ]
  edge [
    source 60
    target 1944
  ]
  edge [
    source 60
    target 1945
  ]
  edge [
    source 60
    target 1946
  ]
  edge [
    source 60
    target 1947
  ]
  edge [
    source 60
    target 1453
  ]
  edge [
    source 60
    target 1948
  ]
  edge [
    source 60
    target 1454
  ]
  edge [
    source 60
    target 1949
  ]
  edge [
    source 60
    target 1950
  ]
  edge [
    source 60
    target 1951
  ]
  edge [
    source 60
    target 1952
  ]
  edge [
    source 60
    target 1953
  ]
  edge [
    source 60
    target 1954
  ]
  edge [
    source 60
    target 1955
  ]
  edge [
    source 60
    target 1956
  ]
  edge [
    source 60
    target 1459
  ]
  edge [
    source 60
    target 1957
  ]
  edge [
    source 60
    target 1958
  ]
  edge [
    source 60
    target 488
  ]
  edge [
    source 60
    target 1959
  ]
  edge [
    source 60
    target 1960
  ]
  edge [
    source 60
    target 1961
  ]
  edge [
    source 60
    target 1962
  ]
  edge [
    source 60
    target 753
  ]
  edge [
    source 60
    target 523
  ]
  edge [
    source 60
    target 1963
  ]
  edge [
    source 60
    target 1964
  ]
  edge [
    source 60
    target 1965
  ]
  edge [
    source 60
    target 1018
  ]
  edge [
    source 60
    target 738
  ]
  edge [
    source 60
    target 1966
  ]
  edge [
    source 60
    target 1967
  ]
  edge [
    source 60
    target 1968
  ]
  edge [
    source 60
    target 1969
  ]
  edge [
    source 60
    target 1970
  ]
  edge [
    source 60
    target 1971
  ]
  edge [
    source 60
    target 1972
  ]
  edge [
    source 60
    target 1973
  ]
  edge [
    source 60
    target 1974
  ]
  edge [
    source 60
    target 476
  ]
  edge [
    source 60
    target 1975
  ]
  edge [
    source 60
    target 1976
  ]
  edge [
    source 60
    target 1977
  ]
  edge [
    source 60
    target 1978
  ]
  edge [
    source 60
    target 1979
  ]
  edge [
    source 60
    target 1980
  ]
  edge [
    source 60
    target 1981
  ]
  edge [
    source 60
    target 768
  ]
  edge [
    source 60
    target 1982
  ]
  edge [
    source 60
    target 1983
  ]
  edge [
    source 60
    target 1230
  ]
  edge [
    source 60
    target 1984
  ]
  edge [
    source 60
    target 1985
  ]
  edge [
    source 60
    target 1986
  ]
  edge [
    source 60
    target 1987
  ]
  edge [
    source 60
    target 1046
  ]
  edge [
    source 60
    target 1988
  ]
  edge [
    source 60
    target 1989
  ]
  edge [
    source 60
    target 497
  ]
  edge [
    source 60
    target 1990
  ]
  edge [
    source 60
    target 1991
  ]
  edge [
    source 60
    target 1354
  ]
  edge [
    source 132
    target 1992
  ]
  edge [
    source 1993
    target 1994
  ]
]
