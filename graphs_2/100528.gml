graph [
  node [
    id 0
    label "biberach"
    origin "text"
  ]
  node [
    id 1
    label "ri&#223;"
    origin "text"
  ]
  node [
    id 2
    label "stacja"
    origin "text"
  ]
  node [
    id 3
    label "kolejowy"
    origin "text"
  ]
  node [
    id 4
    label "punkt"
  ]
  node [
    id 5
    label "instytucja"
  ]
  node [
    id 6
    label "siedziba"
  ]
  node [
    id 7
    label "miejsce"
  ]
  node [
    id 8
    label "droga_krzy&#380;owa"
  ]
  node [
    id 9
    label "urz&#261;dzenie"
  ]
  node [
    id 10
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 11
    label "przedmiot"
  ]
  node [
    id 12
    label "kom&#243;rka"
  ]
  node [
    id 13
    label "furnishing"
  ]
  node [
    id 14
    label "zabezpieczenie"
  ]
  node [
    id 15
    label "zrobienie"
  ]
  node [
    id 16
    label "wyrz&#261;dzenie"
  ]
  node [
    id 17
    label "zagospodarowanie"
  ]
  node [
    id 18
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 19
    label "ig&#322;a"
  ]
  node [
    id 20
    label "narz&#281;dzie"
  ]
  node [
    id 21
    label "wirnik"
  ]
  node [
    id 22
    label "aparatura"
  ]
  node [
    id 23
    label "system_energetyczny"
  ]
  node [
    id 24
    label "impulsator"
  ]
  node [
    id 25
    label "mechanizm"
  ]
  node [
    id 26
    label "sprz&#281;t"
  ]
  node [
    id 27
    label "czynno&#347;&#263;"
  ]
  node [
    id 28
    label "blokowanie"
  ]
  node [
    id 29
    label "set"
  ]
  node [
    id 30
    label "zablokowanie"
  ]
  node [
    id 31
    label "przygotowanie"
  ]
  node [
    id 32
    label "komora"
  ]
  node [
    id 33
    label "j&#281;zyk"
  ]
  node [
    id 34
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 35
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 36
    label "&#321;ubianka"
  ]
  node [
    id 37
    label "miejsce_pracy"
  ]
  node [
    id 38
    label "dzia&#322;_personalny"
  ]
  node [
    id 39
    label "Kreml"
  ]
  node [
    id 40
    label "Bia&#322;y_Dom"
  ]
  node [
    id 41
    label "budynek"
  ]
  node [
    id 42
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 43
    label "sadowisko"
  ]
  node [
    id 44
    label "po&#322;o&#380;enie"
  ]
  node [
    id 45
    label "sprawa"
  ]
  node [
    id 46
    label "ust&#281;p"
  ]
  node [
    id 47
    label "plan"
  ]
  node [
    id 48
    label "obiekt_matematyczny"
  ]
  node [
    id 49
    label "problemat"
  ]
  node [
    id 50
    label "plamka"
  ]
  node [
    id 51
    label "stopie&#324;_pisma"
  ]
  node [
    id 52
    label "jednostka"
  ]
  node [
    id 53
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 54
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 55
    label "mark"
  ]
  node [
    id 56
    label "chwila"
  ]
  node [
    id 57
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 58
    label "prosta"
  ]
  node [
    id 59
    label "problematyka"
  ]
  node [
    id 60
    label "obiekt"
  ]
  node [
    id 61
    label "zapunktowa&#263;"
  ]
  node [
    id 62
    label "podpunkt"
  ]
  node [
    id 63
    label "wojsko"
  ]
  node [
    id 64
    label "kres"
  ]
  node [
    id 65
    label "przestrze&#324;"
  ]
  node [
    id 66
    label "point"
  ]
  node [
    id 67
    label "pozycja"
  ]
  node [
    id 68
    label "warunek_lokalowy"
  ]
  node [
    id 69
    label "plac"
  ]
  node [
    id 70
    label "location"
  ]
  node [
    id 71
    label "uwaga"
  ]
  node [
    id 72
    label "status"
  ]
  node [
    id 73
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 74
    label "cia&#322;o"
  ]
  node [
    id 75
    label "cecha"
  ]
  node [
    id 76
    label "praca"
  ]
  node [
    id 77
    label "rz&#261;d"
  ]
  node [
    id 78
    label "osoba_prawna"
  ]
  node [
    id 79
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 80
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 81
    label "poj&#281;cie"
  ]
  node [
    id 82
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 83
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 84
    label "biuro"
  ]
  node [
    id 85
    label "organizacja"
  ]
  node [
    id 86
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 87
    label "Fundusze_Unijne"
  ]
  node [
    id 88
    label "zamyka&#263;"
  ]
  node [
    id 89
    label "establishment"
  ]
  node [
    id 90
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 91
    label "urz&#261;d"
  ]
  node [
    id 92
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 93
    label "afiliowa&#263;"
  ]
  node [
    id 94
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 95
    label "standard"
  ]
  node [
    id 96
    label "zamykanie"
  ]
  node [
    id 97
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 98
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 99
    label "komunikacyjny"
  ]
  node [
    id 100
    label "dogodny"
  ]
  node [
    id 101
    label "Biberach"
  ]
  node [
    id 102
    label "Ri&#223;"
  ]
  node [
    id 103
    label "Badenia"
  ]
  node [
    id 104
    label "Wirtembergia"
  ]
  node [
    id 105
    label "an"
  ]
  node [
    id 106
    label "dera"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 105
  ]
  edge [
    source 101
    target 106
  ]
  edge [
    source 102
    target 105
  ]
  edge [
    source 102
    target 106
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 106
  ]
]
