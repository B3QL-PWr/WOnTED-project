graph [
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "ostry"
    origin "text"
  ]
  node [
    id 2
    label "moment"
    origin "text"
  ]
  node [
    id 3
    label "w_chuj"
  ]
  node [
    id 4
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 5
    label "mocny"
  ]
  node [
    id 6
    label "trudny"
  ]
  node [
    id 7
    label "nieneutralny"
  ]
  node [
    id 8
    label "porywczy"
  ]
  node [
    id 9
    label "dynamiczny"
  ]
  node [
    id 10
    label "nieprzyjazny"
  ]
  node [
    id 11
    label "skuteczny"
  ]
  node [
    id 12
    label "kategoryczny"
  ]
  node [
    id 13
    label "surowy"
  ]
  node [
    id 14
    label "silny"
  ]
  node [
    id 15
    label "bystro"
  ]
  node [
    id 16
    label "wyra&#378;ny"
  ]
  node [
    id 17
    label "raptowny"
  ]
  node [
    id 18
    label "szorstki"
  ]
  node [
    id 19
    label "energiczny"
  ]
  node [
    id 20
    label "intensywny"
  ]
  node [
    id 21
    label "dramatyczny"
  ]
  node [
    id 22
    label "zdecydowany"
  ]
  node [
    id 23
    label "nieoboj&#281;tny"
  ]
  node [
    id 24
    label "widoczny"
  ]
  node [
    id 25
    label "ostrzenie"
  ]
  node [
    id 26
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 27
    label "ci&#281;&#380;ki"
  ]
  node [
    id 28
    label "naostrzenie"
  ]
  node [
    id 29
    label "gryz&#261;cy"
  ]
  node [
    id 30
    label "dokuczliwy"
  ]
  node [
    id 31
    label "dotkliwy"
  ]
  node [
    id 32
    label "ostro"
  ]
  node [
    id 33
    label "jednoznaczny"
  ]
  node [
    id 34
    label "za&#380;arcie"
  ]
  node [
    id 35
    label "nieobyczajny"
  ]
  node [
    id 36
    label "niebezpieczny"
  ]
  node [
    id 37
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 38
    label "podniecaj&#261;cy"
  ]
  node [
    id 39
    label "osch&#322;y"
  ]
  node [
    id 40
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 41
    label "powa&#380;ny"
  ]
  node [
    id 42
    label "agresywny"
  ]
  node [
    id 43
    label "gro&#378;ny"
  ]
  node [
    id 44
    label "dziki"
  ]
  node [
    id 45
    label "czynno&#347;&#263;"
  ]
  node [
    id 46
    label "spowodowanie"
  ]
  node [
    id 47
    label "powodowanie"
  ]
  node [
    id 48
    label "zdecydowanie"
  ]
  node [
    id 49
    label "zapami&#281;tale"
  ]
  node [
    id 50
    label "za&#380;arty"
  ]
  node [
    id 51
    label "jednoznacznie"
  ]
  node [
    id 52
    label "gryz&#261;co"
  ]
  node [
    id 53
    label "energicznie"
  ]
  node [
    id 54
    label "nieneutralnie"
  ]
  node [
    id 55
    label "dziko"
  ]
  node [
    id 56
    label "widocznie"
  ]
  node [
    id 57
    label "wyra&#378;nie"
  ]
  node [
    id 58
    label "szybko"
  ]
  node [
    id 59
    label "ci&#281;&#380;ko"
  ]
  node [
    id 60
    label "podniecaj&#261;co"
  ]
  node [
    id 61
    label "intensywnie"
  ]
  node [
    id 62
    label "niemile"
  ]
  node [
    id 63
    label "raptownie"
  ]
  node [
    id 64
    label "ukwapliwy"
  ]
  node [
    id 65
    label "pochopny"
  ]
  node [
    id 66
    label "porywczo"
  ]
  node [
    id 67
    label "impulsywny"
  ]
  node [
    id 68
    label "nerwowy"
  ]
  node [
    id 69
    label "pop&#281;dliwy"
  ]
  node [
    id 70
    label "&#380;ywy"
  ]
  node [
    id 71
    label "jary"
  ]
  node [
    id 72
    label "straszny"
  ]
  node [
    id 73
    label "podejrzliwy"
  ]
  node [
    id 74
    label "szalony"
  ]
  node [
    id 75
    label "naturalny"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "dziczenie"
  ]
  node [
    id 78
    label "nielegalny"
  ]
  node [
    id 79
    label "nieucywilizowany"
  ]
  node [
    id 80
    label "nieopanowany"
  ]
  node [
    id 81
    label "nietowarzyski"
  ]
  node [
    id 82
    label "wrogi"
  ]
  node [
    id 83
    label "nieobliczalny"
  ]
  node [
    id 84
    label "nieobyty"
  ]
  node [
    id 85
    label "zdziczenie"
  ]
  node [
    id 86
    label "&#380;ywo"
  ]
  node [
    id 87
    label "bystry"
  ]
  node [
    id 88
    label "jasno"
  ]
  node [
    id 89
    label "inteligentnie"
  ]
  node [
    id 90
    label "zapalczywy"
  ]
  node [
    id 91
    label "zapalony"
  ]
  node [
    id 92
    label "oddany"
  ]
  node [
    id 93
    label "emocjonuj&#261;cy"
  ]
  node [
    id 94
    label "tragicznie"
  ]
  node [
    id 95
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 96
    label "przej&#281;ty"
  ]
  node [
    id 97
    label "dramatycznie"
  ]
  node [
    id 98
    label "wstrz&#261;saj&#261;cy"
  ]
  node [
    id 99
    label "krzepienie"
  ]
  node [
    id 100
    label "&#380;ywotny"
  ]
  node [
    id 101
    label "pokrzepienie"
  ]
  node [
    id 102
    label "niepodwa&#380;alny"
  ]
  node [
    id 103
    label "du&#380;y"
  ]
  node [
    id 104
    label "mocno"
  ]
  node [
    id 105
    label "przekonuj&#261;cy"
  ]
  node [
    id 106
    label "wytrzyma&#322;y"
  ]
  node [
    id 107
    label "konkretny"
  ]
  node [
    id 108
    label "zdrowy"
  ]
  node [
    id 109
    label "silnie"
  ]
  node [
    id 110
    label "meflochina"
  ]
  node [
    id 111
    label "zajebisty"
  ]
  node [
    id 112
    label "k&#322;opotliwy"
  ]
  node [
    id 113
    label "skomplikowany"
  ]
  node [
    id 114
    label "wymagaj&#261;cy"
  ]
  node [
    id 115
    label "pewny"
  ]
  node [
    id 116
    label "zauwa&#380;alny"
  ]
  node [
    id 117
    label "gotowy"
  ]
  node [
    id 118
    label "szybki"
  ]
  node [
    id 119
    label "gwa&#322;towny"
  ]
  node [
    id 120
    label "zawrzenie"
  ]
  node [
    id 121
    label "zawrze&#263;"
  ]
  node [
    id 122
    label "nieoczekiwany"
  ]
  node [
    id 123
    label "szczery"
  ]
  node [
    id 124
    label "stabilny"
  ]
  node [
    id 125
    label "krzepki"
  ]
  node [
    id 126
    label "wyrazisty"
  ]
  node [
    id 127
    label "wzmocni&#263;"
  ]
  node [
    id 128
    label "wzmacnia&#263;"
  ]
  node [
    id 129
    label "dobry"
  ]
  node [
    id 130
    label "okre&#347;lony"
  ]
  node [
    id 131
    label "identyczny"
  ]
  node [
    id 132
    label "aktywny"
  ]
  node [
    id 133
    label "szkodliwy"
  ]
  node [
    id 134
    label "poskutkowanie"
  ]
  node [
    id 135
    label "sprawny"
  ]
  node [
    id 136
    label "skutecznie"
  ]
  node [
    id 137
    label "skutkowanie"
  ]
  node [
    id 138
    label "znacz&#261;cy"
  ]
  node [
    id 139
    label "zwarty"
  ]
  node [
    id 140
    label "efektywny"
  ]
  node [
    id 141
    label "ogrodnictwo"
  ]
  node [
    id 142
    label "pe&#322;ny"
  ]
  node [
    id 143
    label "nieproporcjonalny"
  ]
  node [
    id 144
    label "specjalny"
  ]
  node [
    id 145
    label "wyjrzenie"
  ]
  node [
    id 146
    label "wygl&#261;danie"
  ]
  node [
    id 147
    label "widny"
  ]
  node [
    id 148
    label "widomy"
  ]
  node [
    id 149
    label "pojawianie_si&#281;"
  ]
  node [
    id 150
    label "widzialny"
  ]
  node [
    id 151
    label "wystawienie_si&#281;"
  ]
  node [
    id 152
    label "fizyczny"
  ]
  node [
    id 153
    label "widnienie"
  ]
  node [
    id 154
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 155
    label "ods&#322;anianie"
  ]
  node [
    id 156
    label "zarysowanie_si&#281;"
  ]
  node [
    id 157
    label "dostrzegalny"
  ]
  node [
    id 158
    label "wystawianie_si&#281;"
  ]
  node [
    id 159
    label "monumentalny"
  ]
  node [
    id 160
    label "kompletny"
  ]
  node [
    id 161
    label "masywny"
  ]
  node [
    id 162
    label "wielki"
  ]
  node [
    id 163
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 164
    label "przyswajalny"
  ]
  node [
    id 165
    label "niezgrabny"
  ]
  node [
    id 166
    label "liczny"
  ]
  node [
    id 167
    label "nieprzejrzysty"
  ]
  node [
    id 168
    label "niedelikatny"
  ]
  node [
    id 169
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 170
    label "wolny"
  ]
  node [
    id 171
    label "nieudany"
  ]
  node [
    id 172
    label "zbrojny"
  ]
  node [
    id 173
    label "charakterystyczny"
  ]
  node [
    id 174
    label "bojowy"
  ]
  node [
    id 175
    label "ambitny"
  ]
  node [
    id 176
    label "grubo"
  ]
  node [
    id 177
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 178
    label "gro&#378;nie"
  ]
  node [
    id 179
    label "nad&#261;sany"
  ]
  node [
    id 180
    label "oschle"
  ]
  node [
    id 181
    label "niech&#281;tny"
  ]
  node [
    id 182
    label "twardy"
  ]
  node [
    id 183
    label "srogi"
  ]
  node [
    id 184
    label "surowo"
  ]
  node [
    id 185
    label "oszcz&#281;dny"
  ]
  node [
    id 186
    label "&#347;wie&#380;y"
  ]
  node [
    id 187
    label "nieprzyjemny"
  ]
  node [
    id 188
    label "brzydki"
  ]
  node [
    id 189
    label "dojmuj&#261;cy"
  ]
  node [
    id 190
    label "uszczypliwy"
  ]
  node [
    id 191
    label "niemi&#322;y"
  ]
  node [
    id 192
    label "nie&#380;yczliwie"
  ]
  node [
    id 193
    label "niekorzystny"
  ]
  node [
    id 194
    label "nieprzyja&#378;nie"
  ]
  node [
    id 195
    label "negatywny"
  ]
  node [
    id 196
    label "wstr&#281;tliwy"
  ]
  node [
    id 197
    label "dokuczliwie"
  ]
  node [
    id 198
    label "zdolny"
  ]
  node [
    id 199
    label "w&#347;ciekle"
  ]
  node [
    id 200
    label "czynny"
  ]
  node [
    id 201
    label "agresywnie"
  ]
  node [
    id 202
    label "ofensywny"
  ]
  node [
    id 203
    label "przemoc"
  ]
  node [
    id 204
    label "drastycznie"
  ]
  node [
    id 205
    label "przykry"
  ]
  node [
    id 206
    label "dotkliwie"
  ]
  node [
    id 207
    label "kategorycznie"
  ]
  node [
    id 208
    label "stanowczy"
  ]
  node [
    id 209
    label "dynamizowanie"
  ]
  node [
    id 210
    label "zmienny"
  ]
  node [
    id 211
    label "zdynamizowanie"
  ]
  node [
    id 212
    label "dynamicznie"
  ]
  node [
    id 213
    label "Tuesday"
  ]
  node [
    id 214
    label "nieobyczajnie"
  ]
  node [
    id 215
    label "nieprzyzwoity"
  ]
  node [
    id 216
    label "gorsz&#261;cy"
  ]
  node [
    id 217
    label "naganny"
  ]
  node [
    id 218
    label "niebezpiecznie"
  ]
  node [
    id 219
    label "prawdziwy"
  ]
  node [
    id 220
    label "spowa&#380;nienie"
  ]
  node [
    id 221
    label "powa&#380;nie"
  ]
  node [
    id 222
    label "powa&#380;nienie"
  ]
  node [
    id 223
    label "niejednolity"
  ]
  node [
    id 224
    label "szorstko"
  ]
  node [
    id 225
    label "niski"
  ]
  node [
    id 226
    label "szurpaty"
  ]
  node [
    id 227
    label "chropawo"
  ]
  node [
    id 228
    label "nieg&#322;adki"
  ]
  node [
    id 229
    label "time"
  ]
  node [
    id 230
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 231
    label "okres_czasu"
  ]
  node [
    id 232
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 233
    label "fragment"
  ]
  node [
    id 234
    label "chron"
  ]
  node [
    id 235
    label "minute"
  ]
  node [
    id 236
    label "jednostka_geologiczna"
  ]
  node [
    id 237
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 238
    label "utw&#243;r"
  ]
  node [
    id 239
    label "wiek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
]
