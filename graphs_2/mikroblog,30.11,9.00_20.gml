graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "boldupy"
    origin "text"
  ]
  node [
    id 2
    label "rozumie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "gdy"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 5
    label "potrzebowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 7
    label "leczenie"
    origin "text"
  ]
  node [
    id 8
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 11
    label "rodzaj"
    origin "text"
  ]
  node [
    id 12
    label "zbi&#243;rka"
    origin "text"
  ]
  node [
    id 13
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kurwa"
    origin "text"
  ]
  node [
    id 15
    label "umiar"
    origin "text"
  ]
  node [
    id 16
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 17
    label "przodkini"
  ]
  node [
    id 18
    label "matka_zast&#281;pcza"
  ]
  node [
    id 19
    label "matczysko"
  ]
  node [
    id 20
    label "rodzice"
  ]
  node [
    id 21
    label "stara"
  ]
  node [
    id 22
    label "macierz"
  ]
  node [
    id 23
    label "rodzic"
  ]
  node [
    id 24
    label "Matka_Boska"
  ]
  node [
    id 25
    label "macocha"
  ]
  node [
    id 26
    label "starzy"
  ]
  node [
    id 27
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 28
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 29
    label "pokolenie"
  ]
  node [
    id 30
    label "wapniaki"
  ]
  node [
    id 31
    label "opiekun"
  ]
  node [
    id 32
    label "wapniak"
  ]
  node [
    id 33
    label "rodzic_chrzestny"
  ]
  node [
    id 34
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 35
    label "krewna"
  ]
  node [
    id 36
    label "matka"
  ]
  node [
    id 37
    label "&#380;ona"
  ]
  node [
    id 38
    label "kobieta"
  ]
  node [
    id 39
    label "partnerka"
  ]
  node [
    id 40
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 41
    label "matuszka"
  ]
  node [
    id 42
    label "parametryzacja"
  ]
  node [
    id 43
    label "pa&#324;stwo"
  ]
  node [
    id 44
    label "poj&#281;cie"
  ]
  node [
    id 45
    label "mod"
  ]
  node [
    id 46
    label "patriota"
  ]
  node [
    id 47
    label "m&#281;&#380;atka"
  ]
  node [
    id 48
    label "wiedzie&#263;"
  ]
  node [
    id 49
    label "kuma&#263;"
  ]
  node [
    id 50
    label "czu&#263;"
  ]
  node [
    id 51
    label "give"
  ]
  node [
    id 52
    label "dziama&#263;"
  ]
  node [
    id 53
    label "match"
  ]
  node [
    id 54
    label "empatia"
  ]
  node [
    id 55
    label "j&#281;zyk"
  ]
  node [
    id 56
    label "odbiera&#263;"
  ]
  node [
    id 57
    label "see"
  ]
  node [
    id 58
    label "zna&#263;"
  ]
  node [
    id 59
    label "cognizance"
  ]
  node [
    id 60
    label "postrzega&#263;"
  ]
  node [
    id 61
    label "przewidywa&#263;"
  ]
  node [
    id 62
    label "by&#263;"
  ]
  node [
    id 63
    label "smell"
  ]
  node [
    id 64
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 65
    label "uczuwa&#263;"
  ]
  node [
    id 66
    label "spirit"
  ]
  node [
    id 67
    label "doznawa&#263;"
  ]
  node [
    id 68
    label "anticipate"
  ]
  node [
    id 69
    label "zabiera&#263;"
  ]
  node [
    id 70
    label "zlecenie"
  ]
  node [
    id 71
    label "odzyskiwa&#263;"
  ]
  node [
    id 72
    label "radio"
  ]
  node [
    id 73
    label "przyjmowa&#263;"
  ]
  node [
    id 74
    label "bra&#263;"
  ]
  node [
    id 75
    label "antena"
  ]
  node [
    id 76
    label "fall"
  ]
  node [
    id 77
    label "liszy&#263;"
  ]
  node [
    id 78
    label "pozbawia&#263;"
  ]
  node [
    id 79
    label "telewizor"
  ]
  node [
    id 80
    label "konfiskowa&#263;"
  ]
  node [
    id 81
    label "deprive"
  ]
  node [
    id 82
    label "accept"
  ]
  node [
    id 83
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 84
    label "artykulator"
  ]
  node [
    id 85
    label "kod"
  ]
  node [
    id 86
    label "kawa&#322;ek"
  ]
  node [
    id 87
    label "przedmiot"
  ]
  node [
    id 88
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 89
    label "gramatyka"
  ]
  node [
    id 90
    label "stylik"
  ]
  node [
    id 91
    label "przet&#322;umaczenie"
  ]
  node [
    id 92
    label "formalizowanie"
  ]
  node [
    id 93
    label "ssa&#263;"
  ]
  node [
    id 94
    label "ssanie"
  ]
  node [
    id 95
    label "language"
  ]
  node [
    id 96
    label "liza&#263;"
  ]
  node [
    id 97
    label "napisa&#263;"
  ]
  node [
    id 98
    label "konsonantyzm"
  ]
  node [
    id 99
    label "wokalizm"
  ]
  node [
    id 100
    label "pisa&#263;"
  ]
  node [
    id 101
    label "fonetyka"
  ]
  node [
    id 102
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 103
    label "jeniec"
  ]
  node [
    id 104
    label "but"
  ]
  node [
    id 105
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 106
    label "po_koroniarsku"
  ]
  node [
    id 107
    label "kultura_duchowa"
  ]
  node [
    id 108
    label "t&#322;umaczenie"
  ]
  node [
    id 109
    label "m&#243;wienie"
  ]
  node [
    id 110
    label "pype&#263;"
  ]
  node [
    id 111
    label "lizanie"
  ]
  node [
    id 112
    label "pismo"
  ]
  node [
    id 113
    label "formalizowa&#263;"
  ]
  node [
    id 114
    label "organ"
  ]
  node [
    id 115
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 116
    label "rozumienie"
  ]
  node [
    id 117
    label "spos&#243;b"
  ]
  node [
    id 118
    label "makroglosja"
  ]
  node [
    id 119
    label "m&#243;wi&#263;"
  ]
  node [
    id 120
    label "jama_ustna"
  ]
  node [
    id 121
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 122
    label "formacja_geologiczna"
  ]
  node [
    id 123
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 124
    label "natural_language"
  ]
  node [
    id 125
    label "s&#322;ownictwo"
  ]
  node [
    id 126
    label "urz&#261;dzenie"
  ]
  node [
    id 127
    label "zrozumienie"
  ]
  node [
    id 128
    label "empathy"
  ]
  node [
    id 129
    label "lito&#347;&#263;"
  ]
  node [
    id 130
    label "wczuwa&#263;_si&#281;"
  ]
  node [
    id 131
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 132
    label "szczeka&#263;"
  ]
  node [
    id 133
    label "rozmawia&#263;"
  ]
  node [
    id 134
    label "funkcjonowa&#263;"
  ]
  node [
    id 135
    label "ludzko&#347;&#263;"
  ]
  node [
    id 136
    label "asymilowanie"
  ]
  node [
    id 137
    label "asymilowa&#263;"
  ]
  node [
    id 138
    label "os&#322;abia&#263;"
  ]
  node [
    id 139
    label "posta&#263;"
  ]
  node [
    id 140
    label "hominid"
  ]
  node [
    id 141
    label "podw&#322;adny"
  ]
  node [
    id 142
    label "os&#322;abianie"
  ]
  node [
    id 143
    label "g&#322;owa"
  ]
  node [
    id 144
    label "figura"
  ]
  node [
    id 145
    label "portrecista"
  ]
  node [
    id 146
    label "dwun&#243;g"
  ]
  node [
    id 147
    label "profanum"
  ]
  node [
    id 148
    label "mikrokosmos"
  ]
  node [
    id 149
    label "nasada"
  ]
  node [
    id 150
    label "duch"
  ]
  node [
    id 151
    label "antropochoria"
  ]
  node [
    id 152
    label "osoba"
  ]
  node [
    id 153
    label "wz&#243;r"
  ]
  node [
    id 154
    label "senior"
  ]
  node [
    id 155
    label "oddzia&#322;ywanie"
  ]
  node [
    id 156
    label "Adam"
  ]
  node [
    id 157
    label "homo_sapiens"
  ]
  node [
    id 158
    label "polifag"
  ]
  node [
    id 159
    label "konsument"
  ]
  node [
    id 160
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 161
    label "cz&#322;owiekowate"
  ]
  node [
    id 162
    label "istota_&#380;ywa"
  ]
  node [
    id 163
    label "pracownik"
  ]
  node [
    id 164
    label "Chocho&#322;"
  ]
  node [
    id 165
    label "Herkules_Poirot"
  ]
  node [
    id 166
    label "Edyp"
  ]
  node [
    id 167
    label "parali&#380;owa&#263;"
  ]
  node [
    id 168
    label "Harry_Potter"
  ]
  node [
    id 169
    label "Casanova"
  ]
  node [
    id 170
    label "Zgredek"
  ]
  node [
    id 171
    label "Gargantua"
  ]
  node [
    id 172
    label "Winnetou"
  ]
  node [
    id 173
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 174
    label "Dulcynea"
  ]
  node [
    id 175
    label "kategoria_gramatyczna"
  ]
  node [
    id 176
    label "person"
  ]
  node [
    id 177
    label "Plastu&#347;"
  ]
  node [
    id 178
    label "Quasimodo"
  ]
  node [
    id 179
    label "Sherlock_Holmes"
  ]
  node [
    id 180
    label "Faust"
  ]
  node [
    id 181
    label "Wallenrod"
  ]
  node [
    id 182
    label "Dwukwiat"
  ]
  node [
    id 183
    label "Don_Juan"
  ]
  node [
    id 184
    label "koniugacja"
  ]
  node [
    id 185
    label "Don_Kiszot"
  ]
  node [
    id 186
    label "Hamlet"
  ]
  node [
    id 187
    label "Werter"
  ]
  node [
    id 188
    label "istota"
  ]
  node [
    id 189
    label "Szwejk"
  ]
  node [
    id 190
    label "doros&#322;y"
  ]
  node [
    id 191
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 192
    label "jajko"
  ]
  node [
    id 193
    label "zwierzchnik"
  ]
  node [
    id 194
    label "feuda&#322;"
  ]
  node [
    id 195
    label "starzec"
  ]
  node [
    id 196
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 197
    label "zawodnik"
  ]
  node [
    id 198
    label "komendancja"
  ]
  node [
    id 199
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 200
    label "asymilowanie_si&#281;"
  ]
  node [
    id 201
    label "absorption"
  ]
  node [
    id 202
    label "pobieranie"
  ]
  node [
    id 203
    label "czerpanie"
  ]
  node [
    id 204
    label "acquisition"
  ]
  node [
    id 205
    label "zmienianie"
  ]
  node [
    id 206
    label "organizm"
  ]
  node [
    id 207
    label "assimilation"
  ]
  node [
    id 208
    label "upodabnianie"
  ]
  node [
    id 209
    label "g&#322;oska"
  ]
  node [
    id 210
    label "kultura"
  ]
  node [
    id 211
    label "podobny"
  ]
  node [
    id 212
    label "grupa"
  ]
  node [
    id 213
    label "suppress"
  ]
  node [
    id 214
    label "robi&#263;"
  ]
  node [
    id 215
    label "os&#322;abienie"
  ]
  node [
    id 216
    label "kondycja_fizyczna"
  ]
  node [
    id 217
    label "os&#322;abi&#263;"
  ]
  node [
    id 218
    label "zdrowie"
  ]
  node [
    id 219
    label "powodowa&#263;"
  ]
  node [
    id 220
    label "zmniejsza&#263;"
  ]
  node [
    id 221
    label "bate"
  ]
  node [
    id 222
    label "de-escalation"
  ]
  node [
    id 223
    label "powodowanie"
  ]
  node [
    id 224
    label "debilitation"
  ]
  node [
    id 225
    label "zmniejszanie"
  ]
  node [
    id 226
    label "s&#322;abszy"
  ]
  node [
    id 227
    label "pogarszanie"
  ]
  node [
    id 228
    label "assimilate"
  ]
  node [
    id 229
    label "dostosowywa&#263;"
  ]
  node [
    id 230
    label "dostosowa&#263;"
  ]
  node [
    id 231
    label "przejmowa&#263;"
  ]
  node [
    id 232
    label "upodobni&#263;"
  ]
  node [
    id 233
    label "przej&#261;&#263;"
  ]
  node [
    id 234
    label "upodabnia&#263;"
  ]
  node [
    id 235
    label "pobiera&#263;"
  ]
  node [
    id 236
    label "pobra&#263;"
  ]
  node [
    id 237
    label "zapis"
  ]
  node [
    id 238
    label "figure"
  ]
  node [
    id 239
    label "typ"
  ]
  node [
    id 240
    label "mildew"
  ]
  node [
    id 241
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 242
    label "ideal"
  ]
  node [
    id 243
    label "rule"
  ]
  node [
    id 244
    label "ruch"
  ]
  node [
    id 245
    label "dekal"
  ]
  node [
    id 246
    label "motyw"
  ]
  node [
    id 247
    label "projekt"
  ]
  node [
    id 248
    label "charakterystyka"
  ]
  node [
    id 249
    label "zaistnie&#263;"
  ]
  node [
    id 250
    label "Osjan"
  ]
  node [
    id 251
    label "cecha"
  ]
  node [
    id 252
    label "kto&#347;"
  ]
  node [
    id 253
    label "wygl&#261;d"
  ]
  node [
    id 254
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 255
    label "osobowo&#347;&#263;"
  ]
  node [
    id 256
    label "wytw&#243;r"
  ]
  node [
    id 257
    label "trim"
  ]
  node [
    id 258
    label "poby&#263;"
  ]
  node [
    id 259
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 260
    label "Aspazja"
  ]
  node [
    id 261
    label "punkt_widzenia"
  ]
  node [
    id 262
    label "kompleksja"
  ]
  node [
    id 263
    label "wytrzyma&#263;"
  ]
  node [
    id 264
    label "budowa"
  ]
  node [
    id 265
    label "formacja"
  ]
  node [
    id 266
    label "pozosta&#263;"
  ]
  node [
    id 267
    label "point"
  ]
  node [
    id 268
    label "przedstawienie"
  ]
  node [
    id 269
    label "go&#347;&#263;"
  ]
  node [
    id 270
    label "fotograf"
  ]
  node [
    id 271
    label "malarz"
  ]
  node [
    id 272
    label "artysta"
  ]
  node [
    id 273
    label "hipnotyzowanie"
  ]
  node [
    id 274
    label "&#347;lad"
  ]
  node [
    id 275
    label "docieranie"
  ]
  node [
    id 276
    label "natural_process"
  ]
  node [
    id 277
    label "reakcja_chemiczna"
  ]
  node [
    id 278
    label "wdzieranie_si&#281;"
  ]
  node [
    id 279
    label "zjawisko"
  ]
  node [
    id 280
    label "act"
  ]
  node [
    id 281
    label "rezultat"
  ]
  node [
    id 282
    label "lobbysta"
  ]
  node [
    id 283
    label "pryncypa&#322;"
  ]
  node [
    id 284
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 285
    label "kszta&#322;t"
  ]
  node [
    id 286
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 287
    label "wiedza"
  ]
  node [
    id 288
    label "kierowa&#263;"
  ]
  node [
    id 289
    label "alkohol"
  ]
  node [
    id 290
    label "zdolno&#347;&#263;"
  ]
  node [
    id 291
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 292
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 293
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 294
    label "sztuka"
  ]
  node [
    id 295
    label "dekiel"
  ]
  node [
    id 296
    label "ro&#347;lina"
  ]
  node [
    id 297
    label "&#347;ci&#281;cie"
  ]
  node [
    id 298
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 299
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 300
    label "&#347;ci&#281;gno"
  ]
  node [
    id 301
    label "noosfera"
  ]
  node [
    id 302
    label "byd&#322;o"
  ]
  node [
    id 303
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 304
    label "makrocefalia"
  ]
  node [
    id 305
    label "obiekt"
  ]
  node [
    id 306
    label "ucho"
  ]
  node [
    id 307
    label "g&#243;ra"
  ]
  node [
    id 308
    label "m&#243;zg"
  ]
  node [
    id 309
    label "kierownictwo"
  ]
  node [
    id 310
    label "fryzura"
  ]
  node [
    id 311
    label "umys&#322;"
  ]
  node [
    id 312
    label "cia&#322;o"
  ]
  node [
    id 313
    label "cz&#322;onek"
  ]
  node [
    id 314
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 315
    label "czaszka"
  ]
  node [
    id 316
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 317
    label "allochoria"
  ]
  node [
    id 318
    label "p&#322;aszczyzna"
  ]
  node [
    id 319
    label "bierka_szachowa"
  ]
  node [
    id 320
    label "obiekt_matematyczny"
  ]
  node [
    id 321
    label "gestaltyzm"
  ]
  node [
    id 322
    label "styl"
  ]
  node [
    id 323
    label "obraz"
  ]
  node [
    id 324
    label "rzecz"
  ]
  node [
    id 325
    label "d&#378;wi&#281;k"
  ]
  node [
    id 326
    label "character"
  ]
  node [
    id 327
    label "rze&#378;ba"
  ]
  node [
    id 328
    label "stylistyka"
  ]
  node [
    id 329
    label "miejsce"
  ]
  node [
    id 330
    label "antycypacja"
  ]
  node [
    id 331
    label "ornamentyka"
  ]
  node [
    id 332
    label "informacja"
  ]
  node [
    id 333
    label "facet"
  ]
  node [
    id 334
    label "popis"
  ]
  node [
    id 335
    label "wiersz"
  ]
  node [
    id 336
    label "symetria"
  ]
  node [
    id 337
    label "lingwistyka_kognitywna"
  ]
  node [
    id 338
    label "karta"
  ]
  node [
    id 339
    label "shape"
  ]
  node [
    id 340
    label "podzbi&#243;r"
  ]
  node [
    id 341
    label "perspektywa"
  ]
  node [
    id 342
    label "dziedzina"
  ]
  node [
    id 343
    label "nak&#322;adka"
  ]
  node [
    id 344
    label "li&#347;&#263;"
  ]
  node [
    id 345
    label "jama_gard&#322;owa"
  ]
  node [
    id 346
    label "rezonator"
  ]
  node [
    id 347
    label "podstawa"
  ]
  node [
    id 348
    label "base"
  ]
  node [
    id 349
    label "piek&#322;o"
  ]
  node [
    id 350
    label "human_body"
  ]
  node [
    id 351
    label "ofiarowywanie"
  ]
  node [
    id 352
    label "sfera_afektywna"
  ]
  node [
    id 353
    label "nekromancja"
  ]
  node [
    id 354
    label "Po&#347;wist"
  ]
  node [
    id 355
    label "podekscytowanie"
  ]
  node [
    id 356
    label "deformowanie"
  ]
  node [
    id 357
    label "sumienie"
  ]
  node [
    id 358
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 359
    label "deformowa&#263;"
  ]
  node [
    id 360
    label "psychika"
  ]
  node [
    id 361
    label "zjawa"
  ]
  node [
    id 362
    label "zmar&#322;y"
  ]
  node [
    id 363
    label "istota_nadprzyrodzona"
  ]
  node [
    id 364
    label "power"
  ]
  node [
    id 365
    label "entity"
  ]
  node [
    id 366
    label "ofiarowywa&#263;"
  ]
  node [
    id 367
    label "oddech"
  ]
  node [
    id 368
    label "seksualno&#347;&#263;"
  ]
  node [
    id 369
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 370
    label "byt"
  ]
  node [
    id 371
    label "si&#322;a"
  ]
  node [
    id 372
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 373
    label "ego"
  ]
  node [
    id 374
    label "ofiarowanie"
  ]
  node [
    id 375
    label "charakter"
  ]
  node [
    id 376
    label "fizjonomia"
  ]
  node [
    id 377
    label "kompleks"
  ]
  node [
    id 378
    label "zapalno&#347;&#263;"
  ]
  node [
    id 379
    label "T&#281;sknica"
  ]
  node [
    id 380
    label "ofiarowa&#263;"
  ]
  node [
    id 381
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 382
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 383
    label "passion"
  ]
  node [
    id 384
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 385
    label "odbicie"
  ]
  node [
    id 386
    label "atom"
  ]
  node [
    id 387
    label "przyroda"
  ]
  node [
    id 388
    label "Ziemia"
  ]
  node [
    id 389
    label "kosmos"
  ]
  node [
    id 390
    label "miniatura"
  ]
  node [
    id 391
    label "claim"
  ]
  node [
    id 392
    label "zapotrzebowa&#263;"
  ]
  node [
    id 393
    label "need"
  ]
  node [
    id 394
    label "chcie&#263;"
  ]
  node [
    id 395
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 396
    label "mie&#263;_miejsce"
  ]
  node [
    id 397
    label "equal"
  ]
  node [
    id 398
    label "trwa&#263;"
  ]
  node [
    id 399
    label "chodzi&#263;"
  ]
  node [
    id 400
    label "si&#281;ga&#263;"
  ]
  node [
    id 401
    label "stan"
  ]
  node [
    id 402
    label "obecno&#347;&#263;"
  ]
  node [
    id 403
    label "stand"
  ]
  node [
    id 404
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 405
    label "uczestniczy&#263;"
  ]
  node [
    id 406
    label "desire"
  ]
  node [
    id 407
    label "kcie&#263;"
  ]
  node [
    id 408
    label "zam&#243;wi&#263;"
  ]
  node [
    id 409
    label "rozmienia&#263;"
  ]
  node [
    id 410
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 411
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 412
    label "jednostka_monetarna"
  ]
  node [
    id 413
    label "moniak"
  ]
  node [
    id 414
    label "nomina&#322;"
  ]
  node [
    id 415
    label "zdewaluowa&#263;"
  ]
  node [
    id 416
    label "dewaluowanie"
  ]
  node [
    id 417
    label "pieni&#261;dze"
  ]
  node [
    id 418
    label "numizmat"
  ]
  node [
    id 419
    label "rozmienianie"
  ]
  node [
    id 420
    label "rozmieni&#263;"
  ]
  node [
    id 421
    label "zdewaluowanie"
  ]
  node [
    id 422
    label "rozmienienie"
  ]
  node [
    id 423
    label "dewaluowa&#263;"
  ]
  node [
    id 424
    label "coin"
  ]
  node [
    id 425
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 426
    label "p&#322;&#243;d"
  ]
  node [
    id 427
    label "work"
  ]
  node [
    id 428
    label "moneta"
  ]
  node [
    id 429
    label "drobne"
  ]
  node [
    id 430
    label "medal"
  ]
  node [
    id 431
    label "numismatics"
  ]
  node [
    id 432
    label "okaz"
  ]
  node [
    id 433
    label "warto&#347;&#263;"
  ]
  node [
    id 434
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 435
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 436
    label "par_value"
  ]
  node [
    id 437
    label "cena"
  ]
  node [
    id 438
    label "znaczek"
  ]
  node [
    id 439
    label "zast&#281;powanie"
  ]
  node [
    id 440
    label "wymienienie"
  ]
  node [
    id 441
    label "change"
  ]
  node [
    id 442
    label "zmieni&#263;"
  ]
  node [
    id 443
    label "zmienia&#263;"
  ]
  node [
    id 444
    label "alternate"
  ]
  node [
    id 445
    label "obni&#380;anie"
  ]
  node [
    id 446
    label "umniejszanie"
  ]
  node [
    id 447
    label "devaluation"
  ]
  node [
    id 448
    label "obni&#380;a&#263;"
  ]
  node [
    id 449
    label "umniejsza&#263;"
  ]
  node [
    id 450
    label "knock"
  ]
  node [
    id 451
    label "devalue"
  ]
  node [
    id 452
    label "depreciate"
  ]
  node [
    id 453
    label "umniejszy&#263;"
  ]
  node [
    id 454
    label "obni&#380;y&#263;"
  ]
  node [
    id 455
    label "adulteration"
  ]
  node [
    id 456
    label "obni&#380;enie"
  ]
  node [
    id 457
    label "umniejszenie"
  ]
  node [
    id 458
    label "portfel"
  ]
  node [
    id 459
    label "kwota"
  ]
  node [
    id 460
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 461
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 462
    label "forsa"
  ]
  node [
    id 463
    label "kapa&#263;"
  ]
  node [
    id 464
    label "kapn&#261;&#263;"
  ]
  node [
    id 465
    label "kapanie"
  ]
  node [
    id 466
    label "kapita&#322;"
  ]
  node [
    id 467
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 468
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 469
    label "kapni&#281;cie"
  ]
  node [
    id 470
    label "wyda&#263;"
  ]
  node [
    id 471
    label "hajs"
  ]
  node [
    id 472
    label "dydki"
  ]
  node [
    id 473
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 474
    label "nauka_medyczna"
  ]
  node [
    id 475
    label "wizytowanie"
  ]
  node [
    id 476
    label "alopata"
  ]
  node [
    id 477
    label "pomaganie"
  ]
  node [
    id 478
    label "&#322;agodzenie"
  ]
  node [
    id 479
    label "opatrzenie"
  ]
  node [
    id 480
    label "medication"
  ]
  node [
    id 481
    label "odwykowy"
  ]
  node [
    id 482
    label "zabieg"
  ]
  node [
    id 483
    label "opatrywanie"
  ]
  node [
    id 484
    label "sanatoryjny"
  ]
  node [
    id 485
    label "opieka_medyczna"
  ]
  node [
    id 486
    label "zdiagnozowanie"
  ]
  node [
    id 487
    label "plombowanie"
  ]
  node [
    id 488
    label "homeopata"
  ]
  node [
    id 489
    label "uzdrawianie"
  ]
  node [
    id 490
    label "wizyta"
  ]
  node [
    id 491
    label "przywracanie"
  ]
  node [
    id 492
    label "ulepszanie"
  ]
  node [
    id 493
    label "zdrowy"
  ]
  node [
    id 494
    label "aid"
  ]
  node [
    id 495
    label "helping"
  ]
  node [
    id 496
    label "wyci&#261;ganie_pomocnej_d&#322;oni"
  ]
  node [
    id 497
    label "robienie"
  ]
  node [
    id 498
    label "sprowadzanie"
  ]
  node [
    id 499
    label "care"
  ]
  node [
    id 500
    label "u&#322;atwianie"
  ]
  node [
    id 501
    label "czynno&#347;&#263;"
  ]
  node [
    id 502
    label "dzianie_si&#281;"
  ]
  node [
    id 503
    label "skutkowanie"
  ]
  node [
    id 504
    label "spokojny"
  ]
  node [
    id 505
    label "&#322;agodzenie_si&#281;"
  ]
  node [
    id 506
    label "attenuation"
  ]
  node [
    id 507
    label "opanowywanie"
  ]
  node [
    id 508
    label "buforowanie"
  ]
  node [
    id 509
    label "extenuation"
  ]
  node [
    id 510
    label "gaszenie"
  ]
  node [
    id 511
    label "odwiedziny"
  ]
  node [
    id 512
    label "pobyt"
  ]
  node [
    id 513
    label "odwiedzalno&#347;&#263;"
  ]
  node [
    id 514
    label "czyn"
  ]
  node [
    id 515
    label "operation"
  ]
  node [
    id 516
    label "uzupe&#322;nienie"
  ]
  node [
    id 517
    label "reform"
  ]
  node [
    id 518
    label "przywr&#243;cenie"
  ]
  node [
    id 519
    label "oznaczenie"
  ]
  node [
    id 520
    label "wyposa&#380;enie"
  ]
  node [
    id 521
    label "zabezpieczenie"
  ]
  node [
    id 522
    label "ponaprawianie"
  ]
  node [
    id 523
    label "waterproofing"
  ]
  node [
    id 524
    label "zabezpieczanie"
  ]
  node [
    id 525
    label "przymocowywanie"
  ]
  node [
    id 526
    label "wype&#322;nianie"
  ]
  node [
    id 527
    label "z&#261;b"
  ]
  node [
    id 528
    label "pie&#324;"
  ]
  node [
    id 529
    label "uzupe&#322;nianie"
  ]
  node [
    id 530
    label "wyposa&#380;anie"
  ]
  node [
    id 531
    label "provision"
  ]
  node [
    id 532
    label "oznaczanie"
  ]
  node [
    id 533
    label "repair"
  ]
  node [
    id 534
    label "terapia"
  ]
  node [
    id 535
    label "lekarz"
  ]
  node [
    id 536
    label "rozpoznanie"
  ]
  node [
    id 537
    label "ocenienie"
  ]
  node [
    id 538
    label "odwiedzanie"
  ]
  node [
    id 539
    label "kontrolowanie"
  ]
  node [
    id 540
    label "trial"
  ]
  node [
    id 541
    label "planowa&#263;"
  ]
  node [
    id 542
    label "treat"
  ]
  node [
    id 543
    label "pozyskiwa&#263;"
  ]
  node [
    id 544
    label "ensnare"
  ]
  node [
    id 545
    label "skupia&#263;"
  ]
  node [
    id 546
    label "create"
  ]
  node [
    id 547
    label "przygotowywa&#263;"
  ]
  node [
    id 548
    label "tworzy&#263;"
  ]
  node [
    id 549
    label "standard"
  ]
  node [
    id 550
    label "wprowadza&#263;"
  ]
  node [
    id 551
    label "rynek"
  ]
  node [
    id 552
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 553
    label "wprawia&#263;"
  ]
  node [
    id 554
    label "zaczyna&#263;"
  ]
  node [
    id 555
    label "wpisywa&#263;"
  ]
  node [
    id 556
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 557
    label "wchodzi&#263;"
  ]
  node [
    id 558
    label "take"
  ]
  node [
    id 559
    label "zapoznawa&#263;"
  ]
  node [
    id 560
    label "inflict"
  ]
  node [
    id 561
    label "umieszcza&#263;"
  ]
  node [
    id 562
    label "schodzi&#263;"
  ]
  node [
    id 563
    label "induct"
  ]
  node [
    id 564
    label "begin"
  ]
  node [
    id 565
    label "doprowadza&#263;"
  ]
  node [
    id 566
    label "ognisko"
  ]
  node [
    id 567
    label "huddle"
  ]
  node [
    id 568
    label "zbiera&#263;"
  ]
  node [
    id 569
    label "masowa&#263;"
  ]
  node [
    id 570
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 571
    label "uzyskiwa&#263;"
  ]
  node [
    id 572
    label "wytwarza&#263;"
  ]
  node [
    id 573
    label "tease"
  ]
  node [
    id 574
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 575
    label "pope&#322;nia&#263;"
  ]
  node [
    id 576
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 577
    label "get"
  ]
  node [
    id 578
    label "consist"
  ]
  node [
    id 579
    label "stanowi&#263;"
  ]
  node [
    id 580
    label "raise"
  ]
  node [
    id 581
    label "sposobi&#263;"
  ]
  node [
    id 582
    label "usposabia&#263;"
  ]
  node [
    id 583
    label "train"
  ]
  node [
    id 584
    label "arrange"
  ]
  node [
    id 585
    label "szkoli&#263;"
  ]
  node [
    id 586
    label "wykonywa&#263;"
  ]
  node [
    id 587
    label "pryczy&#263;"
  ]
  node [
    id 588
    label "mean"
  ]
  node [
    id 589
    label "lot_&#347;lizgowy"
  ]
  node [
    id 590
    label "organize"
  ]
  node [
    id 591
    label "project"
  ]
  node [
    id 592
    label "my&#347;le&#263;"
  ]
  node [
    id 593
    label "volunteer"
  ]
  node [
    id 594
    label "opracowywa&#263;"
  ]
  node [
    id 595
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 596
    label "model"
  ]
  node [
    id 597
    label "ordinariness"
  ]
  node [
    id 598
    label "instytucja"
  ]
  node [
    id 599
    label "zorganizowa&#263;"
  ]
  node [
    id 600
    label "taniec_towarzyski"
  ]
  node [
    id 601
    label "organizowanie"
  ]
  node [
    id 602
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 603
    label "criterion"
  ]
  node [
    id 604
    label "zorganizowanie"
  ]
  node [
    id 605
    label "cover"
  ]
  node [
    id 606
    label "inny"
  ]
  node [
    id 607
    label "jaki&#347;"
  ]
  node [
    id 608
    label "r&#243;&#380;nie"
  ]
  node [
    id 609
    label "przyzwoity"
  ]
  node [
    id 610
    label "ciekawy"
  ]
  node [
    id 611
    label "jako&#347;"
  ]
  node [
    id 612
    label "jako_tako"
  ]
  node [
    id 613
    label "niez&#322;y"
  ]
  node [
    id 614
    label "dziwny"
  ]
  node [
    id 615
    label "charakterystyczny"
  ]
  node [
    id 616
    label "kolejny"
  ]
  node [
    id 617
    label "osobno"
  ]
  node [
    id 618
    label "inszy"
  ]
  node [
    id 619
    label "inaczej"
  ]
  node [
    id 620
    label "osobnie"
  ]
  node [
    id 621
    label "rodzina"
  ]
  node [
    id 622
    label "fashion"
  ]
  node [
    id 623
    label "jednostka_systematyczna"
  ]
  node [
    id 624
    label "autorament"
  ]
  node [
    id 625
    label "variety"
  ]
  node [
    id 626
    label "pob&#243;r"
  ]
  node [
    id 627
    label "wojsko"
  ]
  node [
    id 628
    label "type"
  ]
  node [
    id 629
    label "powinowaci"
  ]
  node [
    id 630
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 631
    label "rodze&#324;stwo"
  ]
  node [
    id 632
    label "krewni"
  ]
  node [
    id 633
    label "Ossoli&#324;scy"
  ]
  node [
    id 634
    label "potomstwo"
  ]
  node [
    id 635
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 636
    label "theater"
  ]
  node [
    id 637
    label "zbi&#243;r"
  ]
  node [
    id 638
    label "Soplicowie"
  ]
  node [
    id 639
    label "kin"
  ]
  node [
    id 640
    label "family"
  ]
  node [
    id 641
    label "ordynacja"
  ]
  node [
    id 642
    label "dom_rodzinny"
  ]
  node [
    id 643
    label "Ostrogscy"
  ]
  node [
    id 644
    label "bliscy"
  ]
  node [
    id 645
    label "przyjaciel_domu"
  ]
  node [
    id 646
    label "dom"
  ]
  node [
    id 647
    label "rz&#261;d"
  ]
  node [
    id 648
    label "Firlejowie"
  ]
  node [
    id 649
    label "Kossakowie"
  ]
  node [
    id 650
    label "Czartoryscy"
  ]
  node [
    id 651
    label "Sapiehowie"
  ]
  node [
    id 652
    label "kwestarz"
  ]
  node [
    id 653
    label "kwestowanie"
  ]
  node [
    id 654
    label "apel"
  ]
  node [
    id 655
    label "recoil"
  ]
  node [
    id 656
    label "collection"
  ]
  node [
    id 657
    label "spotkanie"
  ]
  node [
    id 658
    label "koszyk&#243;wka"
  ]
  node [
    id 659
    label "chwyt"
  ]
  node [
    id 660
    label "doznanie"
  ]
  node [
    id 661
    label "gathering"
  ]
  node [
    id 662
    label "zawarcie"
  ]
  node [
    id 663
    label "wydarzenie"
  ]
  node [
    id 664
    label "znajomy"
  ]
  node [
    id 665
    label "powitanie"
  ]
  node [
    id 666
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 667
    label "spowodowanie"
  ]
  node [
    id 668
    label "zdarzenie_si&#281;"
  ]
  node [
    id 669
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 670
    label "znalezienie"
  ]
  node [
    id 671
    label "employment"
  ]
  node [
    id 672
    label "po&#380;egnanie"
  ]
  node [
    id 673
    label "gather"
  ]
  node [
    id 674
    label "spotykanie"
  ]
  node [
    id 675
    label "spotkanie_si&#281;"
  ]
  node [
    id 676
    label "activity"
  ]
  node [
    id 677
    label "bezproblemowy"
  ]
  node [
    id 678
    label "kompozycja"
  ]
  node [
    id 679
    label "zacisk"
  ]
  node [
    id 680
    label "strategia"
  ]
  node [
    id 681
    label "uchwyt"
  ]
  node [
    id 682
    label "uj&#281;cie"
  ]
  node [
    id 683
    label "uczestnik"
  ]
  node [
    id 684
    label "zakon_&#380;ebraczy"
  ]
  node [
    id 685
    label "wolontariusz"
  ]
  node [
    id 686
    label "zakonnik"
  ]
  node [
    id 687
    label "uczestniczenie"
  ]
  node [
    id 688
    label "pro&#347;ba"
  ]
  node [
    id 689
    label "znak"
  ]
  node [
    id 690
    label "zdyscyplinowanie"
  ]
  node [
    id 691
    label "uderzenie"
  ]
  node [
    id 692
    label "pies_my&#347;liwski"
  ]
  node [
    id 693
    label "bid"
  ]
  node [
    id 694
    label "sygna&#322;"
  ]
  node [
    id 695
    label "szermierka"
  ]
  node [
    id 696
    label "koz&#322;owa&#263;"
  ]
  node [
    id 697
    label "kroki"
  ]
  node [
    id 698
    label "kosz"
  ]
  node [
    id 699
    label "pi&#322;ka"
  ]
  node [
    id 700
    label "przelobowa&#263;"
  ]
  node [
    id 701
    label "strefa_podkoszowa"
  ]
  node [
    id 702
    label "przelobowanie"
  ]
  node [
    id 703
    label "wsad"
  ]
  node [
    id 704
    label "dwutakt"
  ]
  node [
    id 705
    label "koz&#322;owanie"
  ]
  node [
    id 706
    label "hide"
  ]
  node [
    id 707
    label "support"
  ]
  node [
    id 708
    label "wykonawca"
  ]
  node [
    id 709
    label "interpretator"
  ]
  node [
    id 710
    label "skurwienie_si&#281;"
  ]
  node [
    id 711
    label "zo&#322;za"
  ]
  node [
    id 712
    label "karze&#322;"
  ]
  node [
    id 713
    label "wyzwisko"
  ]
  node [
    id 714
    label "prostytutka"
  ]
  node [
    id 715
    label "przekle&#324;stwo"
  ]
  node [
    id 716
    label "szmaciarz"
  ]
  node [
    id 717
    label "kurcz&#281;"
  ]
  node [
    id 718
    label "kurwienie_si&#281;"
  ]
  node [
    id 719
    label "&#322;achmyta"
  ]
  node [
    id 720
    label "&#347;mieciarz"
  ]
  node [
    id 721
    label "&#322;aciarz"
  ]
  node [
    id 722
    label "abnegat"
  ]
  node [
    id 723
    label "futbolista"
  ]
  node [
    id 724
    label "pata&#322;ach"
  ]
  node [
    id 725
    label "zeszmacenie_si&#281;"
  ]
  node [
    id 726
    label "domokr&#261;&#380;ca"
  ]
  node [
    id 727
    label "zeszmacanie_si&#281;"
  ]
  node [
    id 728
    label "n&#281;dzarz"
  ]
  node [
    id 729
    label "kar&#322;owacenie"
  ]
  node [
    id 730
    label "skar&#322;owacenie"
  ]
  node [
    id 731
    label "p&#243;&#322;_dupy_zza_krzaka"
  ]
  node [
    id 732
    label "karakan"
  ]
  node [
    id 733
    label "humanoid"
  ]
  node [
    id 734
    label "kar&#322;owaty"
  ]
  node [
    id 735
    label "istota_fantastyczna"
  ]
  node [
    id 736
    label "dwarf"
  ]
  node [
    id 737
    label "gwiazda"
  ]
  node [
    id 738
    label "franca"
  ]
  node [
    id 739
    label "wstr&#281;ciucha"
  ]
  node [
    id 740
    label "fakt"
  ]
  node [
    id 741
    label "strapienie"
  ]
  node [
    id 742
    label "wykrzyknik"
  ]
  node [
    id 743
    label "wulgaryzm"
  ]
  node [
    id 744
    label "bluzg"
  ]
  node [
    id 745
    label "four-letter_word"
  ]
  node [
    id 746
    label "figura_my&#347;li"
  ]
  node [
    id 747
    label "cholera"
  ]
  node [
    id 748
    label "wypowied&#378;"
  ]
  node [
    id 749
    label "chuj"
  ]
  node [
    id 750
    label "pies"
  ]
  node [
    id 751
    label "chujowy"
  ]
  node [
    id 752
    label "obelga"
  ]
  node [
    id 753
    label "szmata"
  ]
  node [
    id 754
    label "pigalak"
  ]
  node [
    id 755
    label "ma&#322;pa"
  ]
  node [
    id 756
    label "dziewczyna_lekkich_obyczaj&#243;w"
  ]
  node [
    id 757
    label "rozpustnica"
  ]
  node [
    id 758
    label "diva"
  ]
  node [
    id 759
    label "jawnogrzesznica"
  ]
  node [
    id 760
    label "kura"
  ]
  node [
    id 761
    label "kurczak"
  ]
  node [
    id 762
    label "m&#322;ode"
  ]
  node [
    id 763
    label "continence"
  ]
  node [
    id 764
    label "oszcz&#281;dno&#347;&#263;"
  ]
  node [
    id 765
    label "rozs&#261;dek"
  ]
  node [
    id 766
    label "rozumno&#347;&#263;"
  ]
  node [
    id 767
    label "ekonomia"
  ]
  node [
    id 768
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 769
    label "racjonalno&#347;&#263;"
  ]
  node [
    id 770
    label "meanness"
  ]
  node [
    id 771
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 772
    label "ci&#281;cia"
  ]
  node [
    id 773
    label "prostota"
  ]
  node [
    id 774
    label "rozwa&#380;no&#347;&#263;"
  ]
  node [
    id 775
    label "raj_utracony"
  ]
  node [
    id 776
    label "umieranie"
  ]
  node [
    id 777
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 778
    label "prze&#380;ywanie"
  ]
  node [
    id 779
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 780
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 781
    label "po&#322;&#243;g"
  ]
  node [
    id 782
    label "umarcie"
  ]
  node [
    id 783
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 784
    label "subsistence"
  ]
  node [
    id 785
    label "okres_noworodkowy"
  ]
  node [
    id 786
    label "prze&#380;ycie"
  ]
  node [
    id 787
    label "wiek_matuzalemowy"
  ]
  node [
    id 788
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 789
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 790
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 791
    label "do&#380;ywanie"
  ]
  node [
    id 792
    label "dzieci&#324;stwo"
  ]
  node [
    id 793
    label "andropauza"
  ]
  node [
    id 794
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 795
    label "rozw&#243;j"
  ]
  node [
    id 796
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 797
    label "czas"
  ]
  node [
    id 798
    label "menopauza"
  ]
  node [
    id 799
    label "&#347;mier&#263;"
  ]
  node [
    id 800
    label "koleje_losu"
  ]
  node [
    id 801
    label "bycie"
  ]
  node [
    id 802
    label "zegar_biologiczny"
  ]
  node [
    id 803
    label "szwung"
  ]
  node [
    id 804
    label "przebywanie"
  ]
  node [
    id 805
    label "warunki"
  ]
  node [
    id 806
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 807
    label "niemowl&#281;ctwo"
  ]
  node [
    id 808
    label "&#380;ywy"
  ]
  node [
    id 809
    label "life"
  ]
  node [
    id 810
    label "staro&#347;&#263;"
  ]
  node [
    id 811
    label "energy"
  ]
  node [
    id 812
    label "trwanie"
  ]
  node [
    id 813
    label "wra&#380;enie"
  ]
  node [
    id 814
    label "przej&#347;cie"
  ]
  node [
    id 815
    label "poradzenie_sobie"
  ]
  node [
    id 816
    label "przetrwanie"
  ]
  node [
    id 817
    label "survival"
  ]
  node [
    id 818
    label "przechodzenie"
  ]
  node [
    id 819
    label "wytrzymywanie"
  ]
  node [
    id 820
    label "zaznawanie"
  ]
  node [
    id 821
    label "obejrzenie"
  ]
  node [
    id 822
    label "widzenie"
  ]
  node [
    id 823
    label "urzeczywistnianie"
  ]
  node [
    id 824
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 825
    label "przeszkodzenie"
  ]
  node [
    id 826
    label "produkowanie"
  ]
  node [
    id 827
    label "being"
  ]
  node [
    id 828
    label "znikni&#281;cie"
  ]
  node [
    id 829
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 830
    label "przeszkadzanie"
  ]
  node [
    id 831
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 832
    label "wyprodukowanie"
  ]
  node [
    id 833
    label "utrzymywanie"
  ]
  node [
    id 834
    label "subsystencja"
  ]
  node [
    id 835
    label "utrzyma&#263;"
  ]
  node [
    id 836
    label "egzystencja"
  ]
  node [
    id 837
    label "wy&#380;ywienie"
  ]
  node [
    id 838
    label "ontologicznie"
  ]
  node [
    id 839
    label "utrzymanie"
  ]
  node [
    id 840
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 841
    label "potencja"
  ]
  node [
    id 842
    label "utrzymywa&#263;"
  ]
  node [
    id 843
    label "status"
  ]
  node [
    id 844
    label "sytuacja"
  ]
  node [
    id 845
    label "poprzedzanie"
  ]
  node [
    id 846
    label "czasoprzestrze&#324;"
  ]
  node [
    id 847
    label "laba"
  ]
  node [
    id 848
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 849
    label "chronometria"
  ]
  node [
    id 850
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 851
    label "rachuba_czasu"
  ]
  node [
    id 852
    label "przep&#322;ywanie"
  ]
  node [
    id 853
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 854
    label "czasokres"
  ]
  node [
    id 855
    label "odczyt"
  ]
  node [
    id 856
    label "chwila"
  ]
  node [
    id 857
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 858
    label "dzieje"
  ]
  node [
    id 859
    label "poprzedzenie"
  ]
  node [
    id 860
    label "trawienie"
  ]
  node [
    id 861
    label "pochodzi&#263;"
  ]
  node [
    id 862
    label "period"
  ]
  node [
    id 863
    label "okres_czasu"
  ]
  node [
    id 864
    label "poprzedza&#263;"
  ]
  node [
    id 865
    label "schy&#322;ek"
  ]
  node [
    id 866
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 867
    label "odwlekanie_si&#281;"
  ]
  node [
    id 868
    label "zegar"
  ]
  node [
    id 869
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 870
    label "czwarty_wymiar"
  ]
  node [
    id 871
    label "pochodzenie"
  ]
  node [
    id 872
    label "Zeitgeist"
  ]
  node [
    id 873
    label "trawi&#263;"
  ]
  node [
    id 874
    label "pogoda"
  ]
  node [
    id 875
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 876
    label "poprzedzi&#263;"
  ]
  node [
    id 877
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 878
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 879
    label "time_period"
  ]
  node [
    id 880
    label "ocieranie_si&#281;"
  ]
  node [
    id 881
    label "otoczenie_si&#281;"
  ]
  node [
    id 882
    label "posiedzenie"
  ]
  node [
    id 883
    label "otarcie_si&#281;"
  ]
  node [
    id 884
    label "atakowanie"
  ]
  node [
    id 885
    label "otaczanie_si&#281;"
  ]
  node [
    id 886
    label "wyj&#347;cie"
  ]
  node [
    id 887
    label "zmierzanie"
  ]
  node [
    id 888
    label "residency"
  ]
  node [
    id 889
    label "sojourn"
  ]
  node [
    id 890
    label "wychodzenie"
  ]
  node [
    id 891
    label "tkwienie"
  ]
  node [
    id 892
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 893
    label "absolutorium"
  ]
  node [
    id 894
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 895
    label "dzia&#322;anie"
  ]
  node [
    id 896
    label "ton"
  ]
  node [
    id 897
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 898
    label "korkowanie"
  ]
  node [
    id 899
    label "death"
  ]
  node [
    id 900
    label "zabijanie"
  ]
  node [
    id 901
    label "martwy"
  ]
  node [
    id 902
    label "przestawanie"
  ]
  node [
    id 903
    label "odumieranie"
  ]
  node [
    id 904
    label "zdychanie"
  ]
  node [
    id 905
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 906
    label "zanikanie"
  ]
  node [
    id 907
    label "ko&#324;czenie"
  ]
  node [
    id 908
    label "nieuleczalnie_chory"
  ]
  node [
    id 909
    label "szybki"
  ]
  node [
    id 910
    label "&#380;ywotny"
  ]
  node [
    id 911
    label "naturalny"
  ]
  node [
    id 912
    label "&#380;ywo"
  ]
  node [
    id 913
    label "o&#380;ywianie"
  ]
  node [
    id 914
    label "silny"
  ]
  node [
    id 915
    label "g&#322;&#281;boki"
  ]
  node [
    id 916
    label "wyra&#378;ny"
  ]
  node [
    id 917
    label "czynny"
  ]
  node [
    id 918
    label "aktualny"
  ]
  node [
    id 919
    label "zgrabny"
  ]
  node [
    id 920
    label "prawdziwy"
  ]
  node [
    id 921
    label "realistyczny"
  ]
  node [
    id 922
    label "energiczny"
  ]
  node [
    id 923
    label "odumarcie"
  ]
  node [
    id 924
    label "przestanie"
  ]
  node [
    id 925
    label "dysponowanie_si&#281;"
  ]
  node [
    id 926
    label "pomarcie"
  ]
  node [
    id 927
    label "die"
  ]
  node [
    id 928
    label "sko&#324;czenie"
  ]
  node [
    id 929
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 930
    label "zdechni&#281;cie"
  ]
  node [
    id 931
    label "zabicie"
  ]
  node [
    id 932
    label "procedura"
  ]
  node [
    id 933
    label "proces"
  ]
  node [
    id 934
    label "proces_biologiczny"
  ]
  node [
    id 935
    label "z&#322;ote_czasy"
  ]
  node [
    id 936
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 937
    label "process"
  ]
  node [
    id 938
    label "cycle"
  ]
  node [
    id 939
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 940
    label "adolescence"
  ]
  node [
    id 941
    label "wiek"
  ]
  node [
    id 942
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 943
    label "zielone_lata"
  ]
  node [
    id 944
    label "rozwi&#261;zanie"
  ]
  node [
    id 945
    label "zlec"
  ]
  node [
    id 946
    label "zlegni&#281;cie"
  ]
  node [
    id 947
    label "defenestracja"
  ]
  node [
    id 948
    label "agonia"
  ]
  node [
    id 949
    label "kres"
  ]
  node [
    id 950
    label "mogi&#322;a"
  ]
  node [
    id 951
    label "kres_&#380;ycia"
  ]
  node [
    id 952
    label "upadek"
  ]
  node [
    id 953
    label "szeol"
  ]
  node [
    id 954
    label "pogrzebanie"
  ]
  node [
    id 955
    label "&#380;a&#322;oba"
  ]
  node [
    id 956
    label "pogrzeb"
  ]
  node [
    id 957
    label "majority"
  ]
  node [
    id 958
    label "osiemnastoletni"
  ]
  node [
    id 959
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 960
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 961
    label "age"
  ]
  node [
    id 962
    label "przekwitanie"
  ]
  node [
    id 963
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 964
    label "dzieci&#281;ctwo"
  ]
  node [
    id 965
    label "energia"
  ]
  node [
    id 966
    label "zapa&#322;"
  ]
  node [
    id 967
    label "Anna"
  ]
  node [
    id 968
    label "Dymna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 244
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 358
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 251
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 497
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 251
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 967
    target 968
  ]
]
