graph [
  node [
    id 0
    label "szanowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "plusujesz"
    origin "text"
  ]
  node [
    id 2
    label "nostalgia"
    origin "text"
  ]
  node [
    id 3
    label "staregry"
    origin "text"
  ]
  node [
    id 4
    label "gta"
    origin "text"
  ]
  node [
    id 5
    label "gimbynieznajo"
    origin "text"
  ]
  node [
    id 6
    label "powa&#380;anie"
  ]
  node [
    id 7
    label "treasure"
  ]
  node [
    id 8
    label "czu&#263;"
  ]
  node [
    id 9
    label "respektowa&#263;"
  ]
  node [
    id 10
    label "wyra&#380;a&#263;"
  ]
  node [
    id 11
    label "chowa&#263;"
  ]
  node [
    id 12
    label "postrzega&#263;"
  ]
  node [
    id 13
    label "przewidywa&#263;"
  ]
  node [
    id 14
    label "by&#263;"
  ]
  node [
    id 15
    label "smell"
  ]
  node [
    id 16
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 17
    label "uczuwa&#263;"
  ]
  node [
    id 18
    label "spirit"
  ]
  node [
    id 19
    label "doznawa&#263;"
  ]
  node [
    id 20
    label "anticipate"
  ]
  node [
    id 21
    label "report"
  ]
  node [
    id 22
    label "hide"
  ]
  node [
    id 23
    label "znosi&#263;"
  ]
  node [
    id 24
    label "train"
  ]
  node [
    id 25
    label "przetrzymywa&#263;"
  ]
  node [
    id 26
    label "hodowa&#263;"
  ]
  node [
    id 27
    label "meliniarz"
  ]
  node [
    id 28
    label "umieszcza&#263;"
  ]
  node [
    id 29
    label "ukrywa&#263;"
  ]
  node [
    id 30
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 31
    label "continue"
  ]
  node [
    id 32
    label "wk&#322;ada&#263;"
  ]
  node [
    id 33
    label "znaczy&#263;"
  ]
  node [
    id 34
    label "give_voice"
  ]
  node [
    id 35
    label "oznacza&#263;"
  ]
  node [
    id 36
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 37
    label "represent"
  ]
  node [
    id 38
    label "komunikowa&#263;"
  ]
  node [
    id 39
    label "convey"
  ]
  node [
    id 40
    label "arouse"
  ]
  node [
    id 41
    label "appreciate"
  ]
  node [
    id 42
    label "zachowywa&#263;"
  ]
  node [
    id 43
    label "tennis"
  ]
  node [
    id 44
    label "honorowa&#263;"
  ]
  node [
    id 45
    label "uhonorowanie"
  ]
  node [
    id 46
    label "zaimponowanie"
  ]
  node [
    id 47
    label "honorowanie"
  ]
  node [
    id 48
    label "uszanowa&#263;"
  ]
  node [
    id 49
    label "chowanie"
  ]
  node [
    id 50
    label "respektowanie"
  ]
  node [
    id 51
    label "uszanowanie"
  ]
  node [
    id 52
    label "szacuneczek"
  ]
  node [
    id 53
    label "rewerencja"
  ]
  node [
    id 54
    label "uhonorowa&#263;"
  ]
  node [
    id 55
    label "czucie"
  ]
  node [
    id 56
    label "fame"
  ]
  node [
    id 57
    label "respect"
  ]
  node [
    id 58
    label "postawa"
  ]
  node [
    id 59
    label "imponowanie"
  ]
  node [
    id 60
    label "smutek"
  ]
  node [
    id 61
    label "homesickness"
  ]
  node [
    id 62
    label "wspominki"
  ]
  node [
    id 63
    label "t&#281;sknota"
  ]
  node [
    id 64
    label "sm&#281;tek"
  ]
  node [
    id 65
    label "emocja"
  ]
  node [
    id 66
    label "przep&#322;akiwanie"
  ]
  node [
    id 67
    label "przep&#322;akanie"
  ]
  node [
    id 68
    label "przep&#322;akiwa&#263;"
  ]
  node [
    id 69
    label "nastr&#243;j"
  ]
  node [
    id 70
    label "przep&#322;aka&#263;"
  ]
  node [
    id 71
    label "&#380;al"
  ]
  node [
    id 72
    label "t&#281;skno&#347;&#263;"
  ]
  node [
    id 73
    label "pragnienie"
  ]
  node [
    id 74
    label "t&#281;sknica"
  ]
  node [
    id 75
    label "ut&#281;sknienie"
  ]
  node [
    id 76
    label "desire"
  ]
  node [
    id 77
    label "pami&#281;tnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
