graph [
  node [
    id 0
    label "tym"
    origin "text"
  ]
  node [
    id 1
    label "sam"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "jeszcze"
    origin "text"
  ]
  node [
    id 4
    label "przed"
    origin "text"
  ]
  node [
    id 5
    label "przem&#243;wienie"
    origin "text"
  ]
  node [
    id 6
    label "larry"
    origin "text"
  ]
  node [
    id 7
    label "lessig"
    origin "text"
  ]
  node [
    id 8
    label "benjamin"
    origin "text"
  ]
  node [
    id 9
    label "mako"
    origin "text"
  ]
  node [
    id 10
    label "hill"
    origin "text"
  ]
  node [
    id 11
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 13
    label "warsztatowy"
    origin "text"
  ]
  node [
    id 14
    label "dyskusja"
    origin "text"
  ]
  node [
    id 15
    label "problem"
    origin "text"
  ]
  node [
    id 16
    label "licencjonowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "praca"
    origin "text"
  ]
  node [
    id 18
    label "by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "autor"
    origin "text"
  ]
  node [
    id 20
    label "tekst"
    origin "text"
  ]
  node [
    id 21
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 22
    label "freedom"
    origin "text"
  ]
  node [
    id 23
    label "commons"
    origin "text"
  ]
  node [
    id 24
    label "anda"
    origin "text"
  ]
  node [
    id 25
    label "the"
    origin "text"
  ]
  node [
    id 26
    label "free"
    origin "text"
  ]
  node [
    id 27
    label "software"
    origin "text"
  ]
  node [
    id 28
    label "movement"
    origin "text"
  ]
  node [
    id 29
    label "standard"
    origin "text"
  ]
  node [
    id 30
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 31
    label "creative"
    origin "text"
  ]
  node [
    id 32
    label "ruch"
    origin "text"
  ]
  node [
    id 33
    label "oprogramowanie"
    origin "text"
  ]
  node [
    id 34
    label "oskar&#380;y&#263;"
    origin "text"
  ]
  node [
    id 35
    label "brak"
    origin "text"
  ]
  node [
    id 36
    label "definicja"
    origin "text"
  ]
  node [
    id 37
    label "propagowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 39
    label "jasno"
    origin "text"
  ]
  node [
    id 40
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 41
    label "czy"
    origin "text"
  ]
  node [
    id 42
    label "dan"
    origin "text"
  ]
  node [
    id 43
    label "dla"
    origin "text"
  ]
  node [
    id 44
    label "wystarczaj&#261;cy"
    origin "text"
  ]
  node [
    id 45
    label "otwarty"
    origin "text"
  ]
  node [
    id 46
    label "nie"
    origin "text"
  ]
  node [
    id 47
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 48
    label "wyra&#378;ny"
    origin "text"
  ]
  node [
    id 49
    label "granica"
    origin "text"
  ]
  node [
    id 50
    label "etyczny"
    origin "text"
  ]
  node [
    id 51
    label "stoa"
    origin "text"
  ]
  node [
    id 52
    label "przegrana"
    origin "text"
  ]
  node [
    id 53
    label "pozycja"
    origin "text"
  ]
  node [
    id 54
    label "pisz"
    origin "text"
  ]
  node [
    id 55
    label "sklep"
  ]
  node [
    id 56
    label "p&#243;&#322;ka"
  ]
  node [
    id 57
    label "firma"
  ]
  node [
    id 58
    label "stoisko"
  ]
  node [
    id 59
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 60
    label "sk&#322;ad"
  ]
  node [
    id 61
    label "obiekt_handlowy"
  ]
  node [
    id 62
    label "zaplecze"
  ]
  node [
    id 63
    label "witryna"
  ]
  node [
    id 64
    label "ranek"
  ]
  node [
    id 65
    label "doba"
  ]
  node [
    id 66
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 67
    label "noc"
  ]
  node [
    id 68
    label "podwiecz&#243;r"
  ]
  node [
    id 69
    label "po&#322;udnie"
  ]
  node [
    id 70
    label "godzina"
  ]
  node [
    id 71
    label "przedpo&#322;udnie"
  ]
  node [
    id 72
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 73
    label "long_time"
  ]
  node [
    id 74
    label "wiecz&#243;r"
  ]
  node [
    id 75
    label "t&#322;usty_czwartek"
  ]
  node [
    id 76
    label "popo&#322;udnie"
  ]
  node [
    id 77
    label "walentynki"
  ]
  node [
    id 78
    label "czynienie_si&#281;"
  ]
  node [
    id 79
    label "s&#322;o&#324;ce"
  ]
  node [
    id 80
    label "rano"
  ]
  node [
    id 81
    label "tydzie&#324;"
  ]
  node [
    id 82
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 83
    label "wzej&#347;cie"
  ]
  node [
    id 84
    label "czas"
  ]
  node [
    id 85
    label "wsta&#263;"
  ]
  node [
    id 86
    label "day"
  ]
  node [
    id 87
    label "termin"
  ]
  node [
    id 88
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 89
    label "wstanie"
  ]
  node [
    id 90
    label "przedwiecz&#243;r"
  ]
  node [
    id 91
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 92
    label "Sylwester"
  ]
  node [
    id 93
    label "poprzedzanie"
  ]
  node [
    id 94
    label "czasoprzestrze&#324;"
  ]
  node [
    id 95
    label "laba"
  ]
  node [
    id 96
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 97
    label "chronometria"
  ]
  node [
    id 98
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 99
    label "rachuba_czasu"
  ]
  node [
    id 100
    label "przep&#322;ywanie"
  ]
  node [
    id 101
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 102
    label "czasokres"
  ]
  node [
    id 103
    label "odczyt"
  ]
  node [
    id 104
    label "chwila"
  ]
  node [
    id 105
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 106
    label "dzieje"
  ]
  node [
    id 107
    label "kategoria_gramatyczna"
  ]
  node [
    id 108
    label "poprzedzenie"
  ]
  node [
    id 109
    label "trawienie"
  ]
  node [
    id 110
    label "pochodzi&#263;"
  ]
  node [
    id 111
    label "period"
  ]
  node [
    id 112
    label "okres_czasu"
  ]
  node [
    id 113
    label "poprzedza&#263;"
  ]
  node [
    id 114
    label "schy&#322;ek"
  ]
  node [
    id 115
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 116
    label "odwlekanie_si&#281;"
  ]
  node [
    id 117
    label "zegar"
  ]
  node [
    id 118
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 119
    label "czwarty_wymiar"
  ]
  node [
    id 120
    label "pochodzenie"
  ]
  node [
    id 121
    label "koniugacja"
  ]
  node [
    id 122
    label "Zeitgeist"
  ]
  node [
    id 123
    label "trawi&#263;"
  ]
  node [
    id 124
    label "pogoda"
  ]
  node [
    id 125
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 126
    label "poprzedzi&#263;"
  ]
  node [
    id 127
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 128
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 129
    label "time_period"
  ]
  node [
    id 130
    label "nazewnictwo"
  ]
  node [
    id 131
    label "term"
  ]
  node [
    id 132
    label "przypadni&#281;cie"
  ]
  node [
    id 133
    label "ekspiracja"
  ]
  node [
    id 134
    label "przypa&#347;&#263;"
  ]
  node [
    id 135
    label "chronogram"
  ]
  node [
    id 136
    label "praktyka"
  ]
  node [
    id 137
    label "nazwa"
  ]
  node [
    id 138
    label "odwieczerz"
  ]
  node [
    id 139
    label "pora"
  ]
  node [
    id 140
    label "przyj&#281;cie"
  ]
  node [
    id 141
    label "spotkanie"
  ]
  node [
    id 142
    label "night"
  ]
  node [
    id 143
    label "zach&#243;d"
  ]
  node [
    id 144
    label "vesper"
  ]
  node [
    id 145
    label "aurora"
  ]
  node [
    id 146
    label "wsch&#243;d"
  ]
  node [
    id 147
    label "zjawisko"
  ]
  node [
    id 148
    label "&#347;rodek"
  ]
  node [
    id 149
    label "obszar"
  ]
  node [
    id 150
    label "Ziemia"
  ]
  node [
    id 151
    label "dwunasta"
  ]
  node [
    id 152
    label "strona_&#347;wiata"
  ]
  node [
    id 153
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 154
    label "dopo&#322;udnie"
  ]
  node [
    id 155
    label "blady_&#347;wit"
  ]
  node [
    id 156
    label "podkurek"
  ]
  node [
    id 157
    label "time"
  ]
  node [
    id 158
    label "p&#243;&#322;godzina"
  ]
  node [
    id 159
    label "jednostka_czasu"
  ]
  node [
    id 160
    label "minuta"
  ]
  node [
    id 161
    label "kwadrans"
  ]
  node [
    id 162
    label "p&#243;&#322;noc"
  ]
  node [
    id 163
    label "nokturn"
  ]
  node [
    id 164
    label "jednostka_geologiczna"
  ]
  node [
    id 165
    label "weekend"
  ]
  node [
    id 166
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 167
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 168
    label "miesi&#261;c"
  ]
  node [
    id 169
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 170
    label "mount"
  ]
  node [
    id 171
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 172
    label "wzej&#347;&#263;"
  ]
  node [
    id 173
    label "ascend"
  ]
  node [
    id 174
    label "kuca&#263;"
  ]
  node [
    id 175
    label "wyzdrowie&#263;"
  ]
  node [
    id 176
    label "opu&#347;ci&#263;"
  ]
  node [
    id 177
    label "rise"
  ]
  node [
    id 178
    label "arise"
  ]
  node [
    id 179
    label "stan&#261;&#263;"
  ]
  node [
    id 180
    label "przesta&#263;"
  ]
  node [
    id 181
    label "wyzdrowienie"
  ]
  node [
    id 182
    label "le&#380;enie"
  ]
  node [
    id 183
    label "kl&#281;czenie"
  ]
  node [
    id 184
    label "opuszczenie"
  ]
  node [
    id 185
    label "uniesienie_si&#281;"
  ]
  node [
    id 186
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 187
    label "siedzenie"
  ]
  node [
    id 188
    label "beginning"
  ]
  node [
    id 189
    label "przestanie"
  ]
  node [
    id 190
    label "S&#322;o&#324;ce"
  ]
  node [
    id 191
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 192
    label "&#347;wiat&#322;o"
  ]
  node [
    id 193
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 194
    label "kochanie"
  ]
  node [
    id 195
    label "sunlight"
  ]
  node [
    id 196
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 197
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 198
    label "grudzie&#324;"
  ]
  node [
    id 199
    label "luty"
  ]
  node [
    id 200
    label "ci&#261;gle"
  ]
  node [
    id 201
    label "stale"
  ]
  node [
    id 202
    label "ci&#261;g&#322;y"
  ]
  node [
    id 203
    label "nieprzerwanie"
  ]
  node [
    id 204
    label "zrozumienie"
  ]
  node [
    id 205
    label "obronienie"
  ]
  node [
    id 206
    label "wydanie"
  ]
  node [
    id 207
    label "wyg&#322;oszenie"
  ]
  node [
    id 208
    label "wypowied&#378;"
  ]
  node [
    id 209
    label "oddzia&#322;anie"
  ]
  node [
    id 210
    label "address"
  ]
  node [
    id 211
    label "wydobycie"
  ]
  node [
    id 212
    label "wyst&#261;pienie"
  ]
  node [
    id 213
    label "talk"
  ]
  node [
    id 214
    label "odzyskanie"
  ]
  node [
    id 215
    label "sermon"
  ]
  node [
    id 216
    label "defense"
  ]
  node [
    id 217
    label "preservation"
  ]
  node [
    id 218
    label "usprawiedliwienie"
  ]
  node [
    id 219
    label "refutation"
  ]
  node [
    id 220
    label "zagranie"
  ]
  node [
    id 221
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 222
    label "zrobienie"
  ]
  node [
    id 223
    label "convalescence"
  ]
  node [
    id 224
    label "reclamation"
  ]
  node [
    id 225
    label "wr&#243;cenie"
  ]
  node [
    id 226
    label "odnowienie_si&#281;"
  ]
  node [
    id 227
    label "reply"
  ]
  node [
    id 228
    label "zahipnotyzowanie"
  ]
  node [
    id 229
    label "spowodowanie"
  ]
  node [
    id 230
    label "zdarzenie_si&#281;"
  ]
  node [
    id 231
    label "chemia"
  ]
  node [
    id 232
    label "wdarcie_si&#281;"
  ]
  node [
    id 233
    label "dotarcie"
  ]
  node [
    id 234
    label "reakcja_chemiczna"
  ]
  node [
    id 235
    label "czynno&#347;&#263;"
  ]
  node [
    id 236
    label "wyeksploatowanie"
  ]
  node [
    id 237
    label "draw"
  ]
  node [
    id 238
    label "uwydatnienie"
  ]
  node [
    id 239
    label "uzyskanie"
  ]
  node [
    id 240
    label "fusillade"
  ]
  node [
    id 241
    label "wyratowanie"
  ]
  node [
    id 242
    label "wyj&#281;cie"
  ]
  node [
    id 243
    label "powyci&#261;ganie"
  ]
  node [
    id 244
    label "wydostanie"
  ]
  node [
    id 245
    label "dobycie"
  ]
  node [
    id 246
    label "explosion"
  ]
  node [
    id 247
    label "g&#243;rnictwo"
  ]
  node [
    id 248
    label "produkcja"
  ]
  node [
    id 249
    label "delivery"
  ]
  node [
    id 250
    label "rendition"
  ]
  node [
    id 251
    label "d&#378;wi&#281;k"
  ]
  node [
    id 252
    label "egzemplarz"
  ]
  node [
    id 253
    label "impression"
  ]
  node [
    id 254
    label "publikacja"
  ]
  node [
    id 255
    label "zadenuncjowanie"
  ]
  node [
    id 256
    label "zapach"
  ]
  node [
    id 257
    label "reszta"
  ]
  node [
    id 258
    label "wytworzenie"
  ]
  node [
    id 259
    label "issue"
  ]
  node [
    id 260
    label "danie"
  ]
  node [
    id 261
    label "czasopismo"
  ]
  node [
    id 262
    label "podanie"
  ]
  node [
    id 263
    label "wprowadzenie"
  ]
  node [
    id 264
    label "odmiana"
  ]
  node [
    id 265
    label "ujawnienie"
  ]
  node [
    id 266
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 267
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 268
    label "urz&#261;dzenie"
  ]
  node [
    id 269
    label "wypowiedzenie"
  ]
  node [
    id 270
    label "pos&#322;uchanie"
  ]
  node [
    id 271
    label "s&#261;d"
  ]
  node [
    id 272
    label "sparafrazowanie"
  ]
  node [
    id 273
    label "strawestowa&#263;"
  ]
  node [
    id 274
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 275
    label "trawestowa&#263;"
  ]
  node [
    id 276
    label "sparafrazowa&#263;"
  ]
  node [
    id 277
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 278
    label "sformu&#322;owanie"
  ]
  node [
    id 279
    label "parafrazowanie"
  ]
  node [
    id 280
    label "ozdobnik"
  ]
  node [
    id 281
    label "delimitacja"
  ]
  node [
    id 282
    label "parafrazowa&#263;"
  ]
  node [
    id 283
    label "stylizacja"
  ]
  node [
    id 284
    label "komunikat"
  ]
  node [
    id 285
    label "trawestowanie"
  ]
  node [
    id 286
    label "strawestowanie"
  ]
  node [
    id 287
    label "rezultat"
  ]
  node [
    id 288
    label "kosmetyk"
  ]
  node [
    id 289
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 290
    label "cover"
  ]
  node [
    id 291
    label "skumanie"
  ]
  node [
    id 292
    label "appreciation"
  ]
  node [
    id 293
    label "creation"
  ]
  node [
    id 294
    label "zorientowanie"
  ]
  node [
    id 295
    label "ocenienie"
  ]
  node [
    id 296
    label "clasp"
  ]
  node [
    id 297
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 298
    label "poczucie"
  ]
  node [
    id 299
    label "sympathy"
  ]
  node [
    id 300
    label "nak&#322;onienie"
  ]
  node [
    id 301
    label "case"
  ]
  node [
    id 302
    label "odst&#261;pienie"
  ]
  node [
    id 303
    label "naznaczenie"
  ]
  node [
    id 304
    label "wyst&#281;p"
  ]
  node [
    id 305
    label "happening"
  ]
  node [
    id 306
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 307
    label "porobienie_si&#281;"
  ]
  node [
    id 308
    label "wyj&#347;cie"
  ]
  node [
    id 309
    label "zrezygnowanie"
  ]
  node [
    id 310
    label "pojawienie_si&#281;"
  ]
  node [
    id 311
    label "appearance"
  ]
  node [
    id 312
    label "egress"
  ]
  node [
    id 313
    label "zacz&#281;cie"
  ]
  node [
    id 314
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 315
    label "event"
  ]
  node [
    id 316
    label "performance"
  ]
  node [
    id 317
    label "exit"
  ]
  node [
    id 318
    label "przepisanie_si&#281;"
  ]
  node [
    id 319
    label "odziedziczy&#263;"
  ]
  node [
    id 320
    label "ruszy&#263;"
  ]
  node [
    id 321
    label "take"
  ]
  node [
    id 322
    label "zaatakowa&#263;"
  ]
  node [
    id 323
    label "skorzysta&#263;"
  ]
  node [
    id 324
    label "uciec"
  ]
  node [
    id 325
    label "receive"
  ]
  node [
    id 326
    label "nakaza&#263;"
  ]
  node [
    id 327
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 328
    label "obskoczy&#263;"
  ]
  node [
    id 329
    label "bra&#263;"
  ]
  node [
    id 330
    label "u&#380;y&#263;"
  ]
  node [
    id 331
    label "zrobi&#263;"
  ]
  node [
    id 332
    label "get"
  ]
  node [
    id 333
    label "wyrucha&#263;"
  ]
  node [
    id 334
    label "World_Health_Organization"
  ]
  node [
    id 335
    label "wyciupcia&#263;"
  ]
  node [
    id 336
    label "wygra&#263;"
  ]
  node [
    id 337
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 338
    label "withdraw"
  ]
  node [
    id 339
    label "wzi&#281;cie"
  ]
  node [
    id 340
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 341
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 342
    label "poczyta&#263;"
  ]
  node [
    id 343
    label "obj&#261;&#263;"
  ]
  node [
    id 344
    label "seize"
  ]
  node [
    id 345
    label "aim"
  ]
  node [
    id 346
    label "chwyci&#263;"
  ]
  node [
    id 347
    label "przyj&#261;&#263;"
  ]
  node [
    id 348
    label "pokona&#263;"
  ]
  node [
    id 349
    label "uda&#263;_si&#281;"
  ]
  node [
    id 350
    label "zacz&#261;&#263;"
  ]
  node [
    id 351
    label "otrzyma&#263;"
  ]
  node [
    id 352
    label "wej&#347;&#263;"
  ]
  node [
    id 353
    label "poruszy&#263;"
  ]
  node [
    id 354
    label "dosta&#263;"
  ]
  node [
    id 355
    label "post&#261;pi&#263;"
  ]
  node [
    id 356
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 357
    label "odj&#261;&#263;"
  ]
  node [
    id 358
    label "cause"
  ]
  node [
    id 359
    label "introduce"
  ]
  node [
    id 360
    label "begin"
  ]
  node [
    id 361
    label "do"
  ]
  node [
    id 362
    label "przybra&#263;"
  ]
  node [
    id 363
    label "strike"
  ]
  node [
    id 364
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 365
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 366
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 367
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 368
    label "obra&#263;"
  ]
  node [
    id 369
    label "uzna&#263;"
  ]
  node [
    id 370
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 371
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 372
    label "fall"
  ]
  node [
    id 373
    label "swallow"
  ]
  node [
    id 374
    label "odebra&#263;"
  ]
  node [
    id 375
    label "dostarczy&#263;"
  ]
  node [
    id 376
    label "umie&#347;ci&#263;"
  ]
  node [
    id 377
    label "absorb"
  ]
  node [
    id 378
    label "undertake"
  ]
  node [
    id 379
    label "ubra&#263;"
  ]
  node [
    id 380
    label "oblec_si&#281;"
  ]
  node [
    id 381
    label "przekaza&#263;"
  ]
  node [
    id 382
    label "str&#243;j"
  ]
  node [
    id 383
    label "insert"
  ]
  node [
    id 384
    label "wpoi&#263;"
  ]
  node [
    id 385
    label "pour"
  ]
  node [
    id 386
    label "przyodzia&#263;"
  ]
  node [
    id 387
    label "natchn&#261;&#263;"
  ]
  node [
    id 388
    label "load"
  ]
  node [
    id 389
    label "wzbudzi&#263;"
  ]
  node [
    id 390
    label "deposit"
  ]
  node [
    id 391
    label "oblec"
  ]
  node [
    id 392
    label "motivate"
  ]
  node [
    id 393
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 394
    label "zabra&#263;"
  ]
  node [
    id 395
    label "go"
  ]
  node [
    id 396
    label "allude"
  ]
  node [
    id 397
    label "cut"
  ]
  node [
    id 398
    label "spowodowa&#263;"
  ]
  node [
    id 399
    label "stimulate"
  ]
  node [
    id 400
    label "nast&#261;pi&#263;"
  ]
  node [
    id 401
    label "attack"
  ]
  node [
    id 402
    label "przeby&#263;"
  ]
  node [
    id 403
    label "spell"
  ]
  node [
    id 404
    label "postara&#263;_si&#281;"
  ]
  node [
    id 405
    label "rozegra&#263;"
  ]
  node [
    id 406
    label "powiedzie&#263;"
  ]
  node [
    id 407
    label "anoint"
  ]
  node [
    id 408
    label "sport"
  ]
  node [
    id 409
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 410
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 411
    label "skrytykowa&#263;"
  ]
  node [
    id 412
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 413
    label "zrozumie&#263;"
  ]
  node [
    id 414
    label "fascinate"
  ]
  node [
    id 415
    label "notice"
  ]
  node [
    id 416
    label "ogarn&#261;&#263;"
  ]
  node [
    id 417
    label "deem"
  ]
  node [
    id 418
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 419
    label "gen"
  ]
  node [
    id 420
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 421
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 422
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 423
    label "zorganizowa&#263;"
  ]
  node [
    id 424
    label "appoint"
  ]
  node [
    id 425
    label "wystylizowa&#263;"
  ]
  node [
    id 426
    label "przerobi&#263;"
  ]
  node [
    id 427
    label "nabra&#263;"
  ]
  node [
    id 428
    label "make"
  ]
  node [
    id 429
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 430
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 431
    label "wydali&#263;"
  ]
  node [
    id 432
    label "wytworzy&#263;"
  ]
  node [
    id 433
    label "give_birth"
  ]
  node [
    id 434
    label "return"
  ]
  node [
    id 435
    label "poleci&#263;"
  ]
  node [
    id 436
    label "order"
  ]
  node [
    id 437
    label "zapakowa&#263;"
  ]
  node [
    id 438
    label "utilize"
  ]
  node [
    id 439
    label "uzyska&#263;"
  ]
  node [
    id 440
    label "dozna&#263;"
  ]
  node [
    id 441
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 442
    label "employment"
  ]
  node [
    id 443
    label "wykorzysta&#263;"
  ]
  node [
    id 444
    label "przyswoi&#263;"
  ]
  node [
    id 445
    label "wykupi&#263;"
  ]
  node [
    id 446
    label "sponge"
  ]
  node [
    id 447
    label "zagwarantowa&#263;"
  ]
  node [
    id 448
    label "znie&#347;&#263;"
  ]
  node [
    id 449
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 450
    label "zagra&#263;"
  ]
  node [
    id 451
    label "score"
  ]
  node [
    id 452
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 453
    label "zwojowa&#263;"
  ]
  node [
    id 454
    label "leave"
  ]
  node [
    id 455
    label "net_income"
  ]
  node [
    id 456
    label "instrument_muzyczny"
  ]
  node [
    id 457
    label "poradzi&#263;_sobie"
  ]
  node [
    id 458
    label "zapobiec"
  ]
  node [
    id 459
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 460
    label "z&#322;oi&#263;"
  ]
  node [
    id 461
    label "overwhelm"
  ]
  node [
    id 462
    label "embrace"
  ]
  node [
    id 463
    label "manipulate"
  ]
  node [
    id 464
    label "assume"
  ]
  node [
    id 465
    label "podj&#261;&#263;"
  ]
  node [
    id 466
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 467
    label "skuma&#263;"
  ]
  node [
    id 468
    label "obejmowa&#263;"
  ]
  node [
    id 469
    label "zagarn&#261;&#263;"
  ]
  node [
    id 470
    label "obj&#281;cie"
  ]
  node [
    id 471
    label "involve"
  ]
  node [
    id 472
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 473
    label "dotkn&#261;&#263;"
  ]
  node [
    id 474
    label "robi&#263;"
  ]
  node [
    id 475
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 476
    label "porywa&#263;"
  ]
  node [
    id 477
    label "korzysta&#263;"
  ]
  node [
    id 478
    label "wchodzi&#263;"
  ]
  node [
    id 479
    label "poczytywa&#263;"
  ]
  node [
    id 480
    label "levy"
  ]
  node [
    id 481
    label "wk&#322;ada&#263;"
  ]
  node [
    id 482
    label "raise"
  ]
  node [
    id 483
    label "pokonywa&#263;"
  ]
  node [
    id 484
    label "przyjmowa&#263;"
  ]
  node [
    id 485
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 486
    label "rucha&#263;"
  ]
  node [
    id 487
    label "prowadzi&#263;"
  ]
  node [
    id 488
    label "za&#380;ywa&#263;"
  ]
  node [
    id 489
    label "otrzymywa&#263;"
  ]
  node [
    id 490
    label "&#263;pa&#263;"
  ]
  node [
    id 491
    label "interpretowa&#263;"
  ]
  node [
    id 492
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 493
    label "dostawa&#263;"
  ]
  node [
    id 494
    label "rusza&#263;"
  ]
  node [
    id 495
    label "chwyta&#263;"
  ]
  node [
    id 496
    label "grza&#263;"
  ]
  node [
    id 497
    label "wch&#322;ania&#263;"
  ]
  node [
    id 498
    label "wygrywa&#263;"
  ]
  node [
    id 499
    label "u&#380;ywa&#263;"
  ]
  node [
    id 500
    label "ucieka&#263;"
  ]
  node [
    id 501
    label "uprawia&#263;_seks"
  ]
  node [
    id 502
    label "abstract"
  ]
  node [
    id 503
    label "towarzystwo"
  ]
  node [
    id 504
    label "atakowa&#263;"
  ]
  node [
    id 505
    label "branie"
  ]
  node [
    id 506
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 507
    label "zalicza&#263;"
  ]
  node [
    id 508
    label "open"
  ]
  node [
    id 509
    label "&#322;apa&#263;"
  ]
  node [
    id 510
    label "przewa&#380;a&#263;"
  ]
  node [
    id 511
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 512
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 513
    label "dmuchni&#281;cie"
  ]
  node [
    id 514
    label "niesienie"
  ]
  node [
    id 515
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 516
    label "nakazanie"
  ]
  node [
    id 517
    label "ruszenie"
  ]
  node [
    id 518
    label "pokonanie"
  ]
  node [
    id 519
    label "wywiezienie"
  ]
  node [
    id 520
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 521
    label "wymienienie_si&#281;"
  ]
  node [
    id 522
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 523
    label "uciekni&#281;cie"
  ]
  node [
    id 524
    label "pobranie"
  ]
  node [
    id 525
    label "poczytanie"
  ]
  node [
    id 526
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 527
    label "pozabieranie"
  ]
  node [
    id 528
    label "u&#380;ycie"
  ]
  node [
    id 529
    label "powodzenie"
  ]
  node [
    id 530
    label "wej&#347;cie"
  ]
  node [
    id 531
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 532
    label "pickings"
  ]
  node [
    id 533
    label "zniesienie"
  ]
  node [
    id 534
    label "kupienie"
  ]
  node [
    id 535
    label "bite"
  ]
  node [
    id 536
    label "dostanie"
  ]
  node [
    id 537
    label "wyruchanie"
  ]
  node [
    id 538
    label "odziedziczenie"
  ]
  node [
    id 539
    label "capture"
  ]
  node [
    id 540
    label "otrzymanie"
  ]
  node [
    id 541
    label "wygranie"
  ]
  node [
    id 542
    label "w&#322;o&#380;enie"
  ]
  node [
    id 543
    label "udanie_si&#281;"
  ]
  node [
    id 544
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 545
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 546
    label "zwia&#263;"
  ]
  node [
    id 547
    label "wypierdoli&#263;"
  ]
  node [
    id 548
    label "fly"
  ]
  node [
    id 549
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 550
    label "spieprzy&#263;"
  ]
  node [
    id 551
    label "pass"
  ]
  node [
    id 552
    label "sta&#263;_si&#281;"
  ]
  node [
    id 553
    label "move"
  ]
  node [
    id 554
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 555
    label "zaistnie&#263;"
  ]
  node [
    id 556
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 557
    label "przekroczy&#263;"
  ]
  node [
    id 558
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 559
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 560
    label "catch"
  ]
  node [
    id 561
    label "intervene"
  ]
  node [
    id 562
    label "pozna&#263;"
  ]
  node [
    id 563
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 564
    label "wnikn&#261;&#263;"
  ]
  node [
    id 565
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 566
    label "przenikn&#261;&#263;"
  ]
  node [
    id 567
    label "doj&#347;&#263;"
  ]
  node [
    id 568
    label "spotka&#263;"
  ]
  node [
    id 569
    label "submit"
  ]
  node [
    id 570
    label "become"
  ]
  node [
    id 571
    label "zapanowa&#263;"
  ]
  node [
    id 572
    label "develop"
  ]
  node [
    id 573
    label "schorzenie"
  ]
  node [
    id 574
    label "nabawienie_si&#281;"
  ]
  node [
    id 575
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 576
    label "zwiastun"
  ]
  node [
    id 577
    label "doczeka&#263;"
  ]
  node [
    id 578
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 579
    label "kupi&#263;"
  ]
  node [
    id 580
    label "wysta&#263;"
  ]
  node [
    id 581
    label "wystarczy&#263;"
  ]
  node [
    id 582
    label "naby&#263;"
  ]
  node [
    id 583
    label "nabawianie_si&#281;"
  ]
  node [
    id 584
    label "range"
  ]
  node [
    id 585
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 586
    label "osaczy&#263;"
  ]
  node [
    id 587
    label "okra&#347;&#263;"
  ]
  node [
    id 588
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 589
    label "obiec"
  ]
  node [
    id 590
    label "ilo&#347;&#263;"
  ]
  node [
    id 591
    label "obecno&#347;&#263;"
  ]
  node [
    id 592
    label "kwota"
  ]
  node [
    id 593
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 594
    label "stan"
  ]
  node [
    id 595
    label "being"
  ]
  node [
    id 596
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 597
    label "cecha"
  ]
  node [
    id 598
    label "wynie&#347;&#263;"
  ]
  node [
    id 599
    label "pieni&#261;dze"
  ]
  node [
    id 600
    label "limit"
  ]
  node [
    id 601
    label "wynosi&#263;"
  ]
  node [
    id 602
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 603
    label "rozmiar"
  ]
  node [
    id 604
    label "part"
  ]
  node [
    id 605
    label "technicznie"
  ]
  node [
    id 606
    label "sucho"
  ]
  node [
    id 607
    label "technically"
  ]
  node [
    id 608
    label "techniczny"
  ]
  node [
    id 609
    label "rozmowa"
  ]
  node [
    id 610
    label "sympozjon"
  ]
  node [
    id 611
    label "conference"
  ]
  node [
    id 612
    label "cisza"
  ]
  node [
    id 613
    label "odpowied&#378;"
  ]
  node [
    id 614
    label "rozhowor"
  ]
  node [
    id 615
    label "discussion"
  ]
  node [
    id 616
    label "esej"
  ]
  node [
    id 617
    label "sympozjarcha"
  ]
  node [
    id 618
    label "zbi&#243;r"
  ]
  node [
    id 619
    label "faza"
  ]
  node [
    id 620
    label "rozrywka"
  ]
  node [
    id 621
    label "symposium"
  ]
  node [
    id 622
    label "utw&#243;r"
  ]
  node [
    id 623
    label "konferencja"
  ]
  node [
    id 624
    label "sprawa"
  ]
  node [
    id 625
    label "subiekcja"
  ]
  node [
    id 626
    label "problemat"
  ]
  node [
    id 627
    label "jajko_Kolumba"
  ]
  node [
    id 628
    label "obstruction"
  ]
  node [
    id 629
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 630
    label "problematyka"
  ]
  node [
    id 631
    label "trudno&#347;&#263;"
  ]
  node [
    id 632
    label "pierepa&#322;ka"
  ]
  node [
    id 633
    label "ambaras"
  ]
  node [
    id 634
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 635
    label "napotka&#263;"
  ]
  node [
    id 636
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 637
    label "k&#322;opotliwy"
  ]
  node [
    id 638
    label "napotkanie"
  ]
  node [
    id 639
    label "poziom"
  ]
  node [
    id 640
    label "difficulty"
  ]
  node [
    id 641
    label "obstacle"
  ]
  node [
    id 642
    label "sytuacja"
  ]
  node [
    id 643
    label "kognicja"
  ]
  node [
    id 644
    label "object"
  ]
  node [
    id 645
    label "rozprawa"
  ]
  node [
    id 646
    label "temat"
  ]
  node [
    id 647
    label "wydarzenie"
  ]
  node [
    id 648
    label "szczeg&#243;&#322;"
  ]
  node [
    id 649
    label "proposition"
  ]
  node [
    id 650
    label "przes&#322;anka"
  ]
  node [
    id 651
    label "rzecz"
  ]
  node [
    id 652
    label "idea"
  ]
  node [
    id 653
    label "k&#322;opot"
  ]
  node [
    id 654
    label "licencja"
  ]
  node [
    id 655
    label "akceptowa&#263;"
  ]
  node [
    id 656
    label "rasowy"
  ]
  node [
    id 657
    label "udzieli&#263;"
  ]
  node [
    id 658
    label "udziela&#263;"
  ]
  node [
    id 659
    label "hodowla"
  ]
  node [
    id 660
    label "license"
  ]
  node [
    id 661
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 662
    label "odst&#281;powa&#263;"
  ]
  node [
    id 663
    label "dawa&#263;"
  ]
  node [
    id 664
    label "assign"
  ]
  node [
    id 665
    label "render"
  ]
  node [
    id 666
    label "accord"
  ]
  node [
    id 667
    label "zezwala&#263;"
  ]
  node [
    id 668
    label "przyznawa&#263;"
  ]
  node [
    id 669
    label "da&#263;"
  ]
  node [
    id 670
    label "udost&#281;pni&#263;"
  ]
  node [
    id 671
    label "przyzna&#263;"
  ]
  node [
    id 672
    label "picture"
  ]
  node [
    id 673
    label "give"
  ]
  node [
    id 674
    label "promocja"
  ]
  node [
    id 675
    label "odst&#261;pi&#263;"
  ]
  node [
    id 676
    label "assent"
  ]
  node [
    id 677
    label "godzi&#263;_si&#281;"
  ]
  node [
    id 678
    label "uznawa&#263;"
  ]
  node [
    id 679
    label "authorize"
  ]
  node [
    id 680
    label "zezwolenie"
  ]
  node [
    id 681
    label "za&#347;wiadczenie"
  ]
  node [
    id 682
    label "pozwolenie"
  ]
  node [
    id 683
    label "prawo"
  ]
  node [
    id 684
    label "wspania&#322;y"
  ]
  node [
    id 685
    label "wytrawny"
  ]
  node [
    id 686
    label "szlachetny"
  ]
  node [
    id 687
    label "typowy"
  ]
  node [
    id 688
    label "arystokratycznie"
  ]
  node [
    id 689
    label "rasowo"
  ]
  node [
    id 690
    label "licencjonowanie"
  ]
  node [
    id 691
    label "prawdziwy"
  ]
  node [
    id 692
    label "markowy"
  ]
  node [
    id 693
    label "potrzymanie"
  ]
  node [
    id 694
    label "praca_rolnicza"
  ]
  node [
    id 695
    label "rolnictwo"
  ]
  node [
    id 696
    label "pod&#243;j"
  ]
  node [
    id 697
    label "filiacja"
  ]
  node [
    id 698
    label "opasa&#263;"
  ]
  node [
    id 699
    label "ch&#243;w"
  ]
  node [
    id 700
    label "sokolarnia"
  ]
  node [
    id 701
    label "potrzyma&#263;"
  ]
  node [
    id 702
    label "rozp&#322;&#243;d"
  ]
  node [
    id 703
    label "grupa_organizm&#243;w"
  ]
  node [
    id 704
    label "wypas"
  ]
  node [
    id 705
    label "wychowalnia"
  ]
  node [
    id 706
    label "pstr&#261;garnia"
  ]
  node [
    id 707
    label "krzy&#380;owanie"
  ]
  node [
    id 708
    label "odch&#243;w"
  ]
  node [
    id 709
    label "tucz"
  ]
  node [
    id 710
    label "ud&#243;j"
  ]
  node [
    id 711
    label "klatka"
  ]
  node [
    id 712
    label "opasienie"
  ]
  node [
    id 713
    label "wych&#243;w"
  ]
  node [
    id 714
    label "obrz&#261;dek"
  ]
  node [
    id 715
    label "opasanie"
  ]
  node [
    id 716
    label "polish"
  ]
  node [
    id 717
    label "akwarium"
  ]
  node [
    id 718
    label "biotechnika"
  ]
  node [
    id 719
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 720
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 721
    label "najem"
  ]
  node [
    id 722
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 723
    label "zak&#322;ad"
  ]
  node [
    id 724
    label "stosunek_pracy"
  ]
  node [
    id 725
    label "benedykty&#324;ski"
  ]
  node [
    id 726
    label "poda&#380;_pracy"
  ]
  node [
    id 727
    label "pracowanie"
  ]
  node [
    id 728
    label "tyrka"
  ]
  node [
    id 729
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 730
    label "wytw&#243;r"
  ]
  node [
    id 731
    label "miejsce"
  ]
  node [
    id 732
    label "zaw&#243;d"
  ]
  node [
    id 733
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 734
    label "tynkarski"
  ]
  node [
    id 735
    label "pracowa&#263;"
  ]
  node [
    id 736
    label "zmiana"
  ]
  node [
    id 737
    label "czynnik_produkcji"
  ]
  node [
    id 738
    label "zobowi&#261;zanie"
  ]
  node [
    id 739
    label "kierownictwo"
  ]
  node [
    id 740
    label "siedziba"
  ]
  node [
    id 741
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 742
    label "przedmiot"
  ]
  node [
    id 743
    label "p&#322;&#243;d"
  ]
  node [
    id 744
    label "work"
  ]
  node [
    id 745
    label "activity"
  ]
  node [
    id 746
    label "bezproblemowy"
  ]
  node [
    id 747
    label "warunek_lokalowy"
  ]
  node [
    id 748
    label "plac"
  ]
  node [
    id 749
    label "location"
  ]
  node [
    id 750
    label "uwaga"
  ]
  node [
    id 751
    label "przestrze&#324;"
  ]
  node [
    id 752
    label "status"
  ]
  node [
    id 753
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 754
    label "cia&#322;o"
  ]
  node [
    id 755
    label "rz&#261;d"
  ]
  node [
    id 756
    label "stosunek_prawny"
  ]
  node [
    id 757
    label "oblig"
  ]
  node [
    id 758
    label "uregulowa&#263;"
  ]
  node [
    id 759
    label "occupation"
  ]
  node [
    id 760
    label "duty"
  ]
  node [
    id 761
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 762
    label "zapowied&#378;"
  ]
  node [
    id 763
    label "obowi&#261;zek"
  ]
  node [
    id 764
    label "statement"
  ]
  node [
    id 765
    label "zapewnienie"
  ]
  node [
    id 766
    label "miejsce_pracy"
  ]
  node [
    id 767
    label "&#321;ubianka"
  ]
  node [
    id 768
    label "dzia&#322;_personalny"
  ]
  node [
    id 769
    label "Kreml"
  ]
  node [
    id 770
    label "Bia&#322;y_Dom"
  ]
  node [
    id 771
    label "budynek"
  ]
  node [
    id 772
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 773
    label "sadowisko"
  ]
  node [
    id 774
    label "zak&#322;adka"
  ]
  node [
    id 775
    label "jednostka_organizacyjna"
  ]
  node [
    id 776
    label "instytucja"
  ]
  node [
    id 777
    label "wyko&#324;czenie"
  ]
  node [
    id 778
    label "czyn"
  ]
  node [
    id 779
    label "company"
  ]
  node [
    id 780
    label "instytut"
  ]
  node [
    id 781
    label "umowa"
  ]
  node [
    id 782
    label "cierpliwy"
  ]
  node [
    id 783
    label "mozolny"
  ]
  node [
    id 784
    label "wytrwa&#322;y"
  ]
  node [
    id 785
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 786
    label "benedykty&#324;sko"
  ]
  node [
    id 787
    label "po_benedykty&#324;sku"
  ]
  node [
    id 788
    label "rewizja"
  ]
  node [
    id 789
    label "passage"
  ]
  node [
    id 790
    label "oznaka"
  ]
  node [
    id 791
    label "change"
  ]
  node [
    id 792
    label "ferment"
  ]
  node [
    id 793
    label "komplet"
  ]
  node [
    id 794
    label "anatomopatolog"
  ]
  node [
    id 795
    label "zmianka"
  ]
  node [
    id 796
    label "amendment"
  ]
  node [
    id 797
    label "odmienianie"
  ]
  node [
    id 798
    label "tura"
  ]
  node [
    id 799
    label "przepracowanie_si&#281;"
  ]
  node [
    id 800
    label "zarz&#261;dzanie"
  ]
  node [
    id 801
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 802
    label "podlizanie_si&#281;"
  ]
  node [
    id 803
    label "dopracowanie"
  ]
  node [
    id 804
    label "podlizywanie_si&#281;"
  ]
  node [
    id 805
    label "uruchamianie"
  ]
  node [
    id 806
    label "dzia&#322;anie"
  ]
  node [
    id 807
    label "d&#261;&#380;enie"
  ]
  node [
    id 808
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 809
    label "uruchomienie"
  ]
  node [
    id 810
    label "nakr&#281;canie"
  ]
  node [
    id 811
    label "funkcjonowanie"
  ]
  node [
    id 812
    label "tr&#243;jstronny"
  ]
  node [
    id 813
    label "postaranie_si&#281;"
  ]
  node [
    id 814
    label "odpocz&#281;cie"
  ]
  node [
    id 815
    label "nakr&#281;cenie"
  ]
  node [
    id 816
    label "zatrzymanie"
  ]
  node [
    id 817
    label "spracowanie_si&#281;"
  ]
  node [
    id 818
    label "skakanie"
  ]
  node [
    id 819
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 820
    label "podtrzymywanie"
  ]
  node [
    id 821
    label "w&#322;&#261;czanie"
  ]
  node [
    id 822
    label "zaprz&#281;ganie"
  ]
  node [
    id 823
    label "podejmowanie"
  ]
  node [
    id 824
    label "maszyna"
  ]
  node [
    id 825
    label "wyrabianie"
  ]
  node [
    id 826
    label "dzianie_si&#281;"
  ]
  node [
    id 827
    label "use"
  ]
  node [
    id 828
    label "przepracowanie"
  ]
  node [
    id 829
    label "poruszanie_si&#281;"
  ]
  node [
    id 830
    label "funkcja"
  ]
  node [
    id 831
    label "impact"
  ]
  node [
    id 832
    label "przepracowywanie"
  ]
  node [
    id 833
    label "awansowanie"
  ]
  node [
    id 834
    label "courtship"
  ]
  node [
    id 835
    label "zapracowanie"
  ]
  node [
    id 836
    label "wyrobienie"
  ]
  node [
    id 837
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 838
    label "w&#322;&#261;czenie"
  ]
  node [
    id 839
    label "zawodoznawstwo"
  ]
  node [
    id 840
    label "emocja"
  ]
  node [
    id 841
    label "office"
  ]
  node [
    id 842
    label "kwalifikacje"
  ]
  node [
    id 843
    label "craft"
  ]
  node [
    id 844
    label "transakcja"
  ]
  node [
    id 845
    label "endeavor"
  ]
  node [
    id 846
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 847
    label "mie&#263;_miejsce"
  ]
  node [
    id 848
    label "podejmowa&#263;"
  ]
  node [
    id 849
    label "dziama&#263;"
  ]
  node [
    id 850
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 851
    label "bangla&#263;"
  ]
  node [
    id 852
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 853
    label "dzia&#322;a&#263;"
  ]
  node [
    id 854
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 855
    label "tryb"
  ]
  node [
    id 856
    label "funkcjonowa&#263;"
  ]
  node [
    id 857
    label "biuro"
  ]
  node [
    id 858
    label "lead"
  ]
  node [
    id 859
    label "zesp&#243;&#322;"
  ]
  node [
    id 860
    label "w&#322;adza"
  ]
  node [
    id 861
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 862
    label "equal"
  ]
  node [
    id 863
    label "trwa&#263;"
  ]
  node [
    id 864
    label "chodzi&#263;"
  ]
  node [
    id 865
    label "si&#281;ga&#263;"
  ]
  node [
    id 866
    label "stand"
  ]
  node [
    id 867
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 868
    label "uczestniczy&#263;"
  ]
  node [
    id 869
    label "participate"
  ]
  node [
    id 870
    label "istnie&#263;"
  ]
  node [
    id 871
    label "pozostawa&#263;"
  ]
  node [
    id 872
    label "zostawa&#263;"
  ]
  node [
    id 873
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 874
    label "adhere"
  ]
  node [
    id 875
    label "compass"
  ]
  node [
    id 876
    label "osi&#261;ga&#263;"
  ]
  node [
    id 877
    label "dociera&#263;"
  ]
  node [
    id 878
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 879
    label "mierzy&#263;"
  ]
  node [
    id 880
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 881
    label "exsert"
  ]
  node [
    id 882
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 883
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 884
    label "p&#322;ywa&#263;"
  ]
  node [
    id 885
    label "run"
  ]
  node [
    id 886
    label "przebiega&#263;"
  ]
  node [
    id 887
    label "proceed"
  ]
  node [
    id 888
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 889
    label "carry"
  ]
  node [
    id 890
    label "bywa&#263;"
  ]
  node [
    id 891
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 892
    label "stara&#263;_si&#281;"
  ]
  node [
    id 893
    label "para"
  ]
  node [
    id 894
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 895
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 896
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 897
    label "krok"
  ]
  node [
    id 898
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 899
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 900
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 901
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 902
    label "continue"
  ]
  node [
    id 903
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 904
    label "Ohio"
  ]
  node [
    id 905
    label "wci&#281;cie"
  ]
  node [
    id 906
    label "Nowy_York"
  ]
  node [
    id 907
    label "warstwa"
  ]
  node [
    id 908
    label "samopoczucie"
  ]
  node [
    id 909
    label "Illinois"
  ]
  node [
    id 910
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 911
    label "state"
  ]
  node [
    id 912
    label "Jukatan"
  ]
  node [
    id 913
    label "Kalifornia"
  ]
  node [
    id 914
    label "Wirginia"
  ]
  node [
    id 915
    label "wektor"
  ]
  node [
    id 916
    label "Goa"
  ]
  node [
    id 917
    label "Teksas"
  ]
  node [
    id 918
    label "Waszyngton"
  ]
  node [
    id 919
    label "Massachusetts"
  ]
  node [
    id 920
    label "Alaska"
  ]
  node [
    id 921
    label "Arakan"
  ]
  node [
    id 922
    label "Hawaje"
  ]
  node [
    id 923
    label "Maryland"
  ]
  node [
    id 924
    label "punkt"
  ]
  node [
    id 925
    label "Michigan"
  ]
  node [
    id 926
    label "Arizona"
  ]
  node [
    id 927
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 928
    label "Georgia"
  ]
  node [
    id 929
    label "Pensylwania"
  ]
  node [
    id 930
    label "shape"
  ]
  node [
    id 931
    label "Luizjana"
  ]
  node [
    id 932
    label "Nowy_Meksyk"
  ]
  node [
    id 933
    label "Alabama"
  ]
  node [
    id 934
    label "Kansas"
  ]
  node [
    id 935
    label "Oregon"
  ]
  node [
    id 936
    label "Oklahoma"
  ]
  node [
    id 937
    label "Floryda"
  ]
  node [
    id 938
    label "jednostka_administracyjna"
  ]
  node [
    id 939
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 940
    label "kszta&#322;ciciel"
  ]
  node [
    id 941
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 942
    label "tworzyciel"
  ]
  node [
    id 943
    label "wykonawca"
  ]
  node [
    id 944
    label "pomys&#322;odawca"
  ]
  node [
    id 945
    label "&#347;w"
  ]
  node [
    id 946
    label "inicjator"
  ]
  node [
    id 947
    label "podmiot_gospodarczy"
  ]
  node [
    id 948
    label "artysta"
  ]
  node [
    id 949
    label "cz&#322;owiek"
  ]
  node [
    id 950
    label "muzyk"
  ]
  node [
    id 951
    label "nauczyciel"
  ]
  node [
    id 952
    label "ekscerpcja"
  ]
  node [
    id 953
    label "j&#281;zykowo"
  ]
  node [
    id 954
    label "redakcja"
  ]
  node [
    id 955
    label "pomini&#281;cie"
  ]
  node [
    id 956
    label "dzie&#322;o"
  ]
  node [
    id 957
    label "preparacja"
  ]
  node [
    id 958
    label "odmianka"
  ]
  node [
    id 959
    label "koniektura"
  ]
  node [
    id 960
    label "pisa&#263;"
  ]
  node [
    id 961
    label "obelga"
  ]
  node [
    id 962
    label "obrazowanie"
  ]
  node [
    id 963
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 964
    label "dorobek"
  ]
  node [
    id 965
    label "forma"
  ]
  node [
    id 966
    label "tre&#347;&#263;"
  ]
  node [
    id 967
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 968
    label "retrospektywa"
  ]
  node [
    id 969
    label "works"
  ]
  node [
    id 970
    label "tetralogia"
  ]
  node [
    id 971
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 972
    label "cholera"
  ]
  node [
    id 973
    label "ubliga"
  ]
  node [
    id 974
    label "niedorobek"
  ]
  node [
    id 975
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 976
    label "chuj"
  ]
  node [
    id 977
    label "bluzg"
  ]
  node [
    id 978
    label "wyzwisko"
  ]
  node [
    id 979
    label "indignation"
  ]
  node [
    id 980
    label "pies"
  ]
  node [
    id 981
    label "wrzuta"
  ]
  node [
    id 982
    label "chujowy"
  ]
  node [
    id 983
    label "krzywda"
  ]
  node [
    id 984
    label "szmata"
  ]
  node [
    id 985
    label "formu&#322;owa&#263;"
  ]
  node [
    id 986
    label "ozdabia&#263;"
  ]
  node [
    id 987
    label "stawia&#263;"
  ]
  node [
    id 988
    label "styl"
  ]
  node [
    id 989
    label "skryba"
  ]
  node [
    id 990
    label "read"
  ]
  node [
    id 991
    label "donosi&#263;"
  ]
  node [
    id 992
    label "code"
  ]
  node [
    id 993
    label "dysgrafia"
  ]
  node [
    id 994
    label "dysortografia"
  ]
  node [
    id 995
    label "tworzy&#263;"
  ]
  node [
    id 996
    label "prasa"
  ]
  node [
    id 997
    label "preparation"
  ]
  node [
    id 998
    label "proces_technologiczny"
  ]
  node [
    id 999
    label "uj&#281;cie"
  ]
  node [
    id 1000
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1001
    label "pozostawi&#263;"
  ]
  node [
    id 1002
    label "obni&#380;y&#263;"
  ]
  node [
    id 1003
    label "zostawi&#263;"
  ]
  node [
    id 1004
    label "potani&#263;"
  ]
  node [
    id 1005
    label "drop"
  ]
  node [
    id 1006
    label "evacuate"
  ]
  node [
    id 1007
    label "humiliate"
  ]
  node [
    id 1008
    label "straci&#263;"
  ]
  node [
    id 1009
    label "omin&#261;&#263;"
  ]
  node [
    id 1010
    label "u&#380;ytkownik"
  ]
  node [
    id 1011
    label "komunikacyjnie"
  ]
  node [
    id 1012
    label "redaktor"
  ]
  node [
    id 1013
    label "radio"
  ]
  node [
    id 1014
    label "composition"
  ]
  node [
    id 1015
    label "wydawnictwo"
  ]
  node [
    id 1016
    label "redaction"
  ]
  node [
    id 1017
    label "telewizja"
  ]
  node [
    id 1018
    label "obr&#243;bka"
  ]
  node [
    id 1019
    label "przypuszczenie"
  ]
  node [
    id 1020
    label "conjecture"
  ]
  node [
    id 1021
    label "wniosek"
  ]
  node [
    id 1022
    label "wyb&#243;r"
  ]
  node [
    id 1023
    label "dokumentacja"
  ]
  node [
    id 1024
    label "ellipsis"
  ]
  node [
    id 1025
    label "wykluczenie"
  ]
  node [
    id 1026
    label "figura_my&#347;li"
  ]
  node [
    id 1027
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 1028
    label "dzie&#324;_powszedni"
  ]
  node [
    id 1029
    label "reengineering"
  ]
  node [
    id 1030
    label "program"
  ]
  node [
    id 1031
    label "series"
  ]
  node [
    id 1032
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1033
    label "uprawianie"
  ]
  node [
    id 1034
    label "collection"
  ]
  node [
    id 1035
    label "dane"
  ]
  node [
    id 1036
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1037
    label "pakiet_klimatyczny"
  ]
  node [
    id 1038
    label "poj&#281;cie"
  ]
  node [
    id 1039
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1040
    label "sum"
  ]
  node [
    id 1041
    label "gathering"
  ]
  node [
    id 1042
    label "album"
  ]
  node [
    id 1043
    label "instalowa&#263;"
  ]
  node [
    id 1044
    label "odinstalowywa&#263;"
  ]
  node [
    id 1045
    label "spis"
  ]
  node [
    id 1046
    label "zaprezentowanie"
  ]
  node [
    id 1047
    label "podprogram"
  ]
  node [
    id 1048
    label "ogranicznik_referencyjny"
  ]
  node [
    id 1049
    label "course_of_study"
  ]
  node [
    id 1050
    label "booklet"
  ]
  node [
    id 1051
    label "dzia&#322;"
  ]
  node [
    id 1052
    label "odinstalowanie"
  ]
  node [
    id 1053
    label "broszura"
  ]
  node [
    id 1054
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 1055
    label "kana&#322;"
  ]
  node [
    id 1056
    label "teleferie"
  ]
  node [
    id 1057
    label "zainstalowanie"
  ]
  node [
    id 1058
    label "struktura_organizacyjna"
  ]
  node [
    id 1059
    label "pirat"
  ]
  node [
    id 1060
    label "zaprezentowa&#263;"
  ]
  node [
    id 1061
    label "prezentowanie"
  ]
  node [
    id 1062
    label "prezentowa&#263;"
  ]
  node [
    id 1063
    label "interfejs"
  ]
  node [
    id 1064
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1065
    label "okno"
  ]
  node [
    id 1066
    label "blok"
  ]
  node [
    id 1067
    label "folder"
  ]
  node [
    id 1068
    label "zainstalowa&#263;"
  ]
  node [
    id 1069
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1070
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1071
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1072
    label "ram&#243;wka"
  ]
  node [
    id 1073
    label "emitowa&#263;"
  ]
  node [
    id 1074
    label "emitowanie"
  ]
  node [
    id 1075
    label "odinstalowywanie"
  ]
  node [
    id 1076
    label "instrukcja"
  ]
  node [
    id 1077
    label "informatyka"
  ]
  node [
    id 1078
    label "deklaracja"
  ]
  node [
    id 1079
    label "menu"
  ]
  node [
    id 1080
    label "sekcja_krytyczna"
  ]
  node [
    id 1081
    label "furkacja"
  ]
  node [
    id 1082
    label "podstawa"
  ]
  node [
    id 1083
    label "instalowanie"
  ]
  node [
    id 1084
    label "oferta"
  ]
  node [
    id 1085
    label "odinstalowa&#263;"
  ]
  node [
    id 1086
    label "przer&#243;bka"
  ]
  node [
    id 1087
    label "odmienienie"
  ]
  node [
    id 1088
    label "strategia"
  ]
  node [
    id 1089
    label "zmienia&#263;"
  ]
  node [
    id 1090
    label "model"
  ]
  node [
    id 1091
    label "organizowa&#263;"
  ]
  node [
    id 1092
    label "ordinariness"
  ]
  node [
    id 1093
    label "taniec_towarzyski"
  ]
  node [
    id 1094
    label "organizowanie"
  ]
  node [
    id 1095
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1096
    label "criterion"
  ]
  node [
    id 1097
    label "zorganizowanie"
  ]
  node [
    id 1098
    label "spos&#243;b"
  ]
  node [
    id 1099
    label "prezenter"
  ]
  node [
    id 1100
    label "typ"
  ]
  node [
    id 1101
    label "mildew"
  ]
  node [
    id 1102
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1103
    label "motif"
  ]
  node [
    id 1104
    label "pozowanie"
  ]
  node [
    id 1105
    label "ideal"
  ]
  node [
    id 1106
    label "wz&#243;r"
  ]
  node [
    id 1107
    label "matryca"
  ]
  node [
    id 1108
    label "adaptation"
  ]
  node [
    id 1109
    label "pozowa&#263;"
  ]
  node [
    id 1110
    label "imitacja"
  ]
  node [
    id 1111
    label "orygina&#322;"
  ]
  node [
    id 1112
    label "facet"
  ]
  node [
    id 1113
    label "miniatura"
  ]
  node [
    id 1114
    label "charakter"
  ]
  node [
    id 1115
    label "osoba_prawna"
  ]
  node [
    id 1116
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1117
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1118
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1119
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1120
    label "organizacja"
  ]
  node [
    id 1121
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1122
    label "Fundusze_Unijne"
  ]
  node [
    id 1123
    label "zamyka&#263;"
  ]
  node [
    id 1124
    label "establishment"
  ]
  node [
    id 1125
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1126
    label "urz&#261;d"
  ]
  node [
    id 1127
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1128
    label "afiliowa&#263;"
  ]
  node [
    id 1129
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1130
    label "zamykanie"
  ]
  node [
    id 1131
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1132
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1133
    label "planowa&#263;"
  ]
  node [
    id 1134
    label "dostosowywa&#263;"
  ]
  node [
    id 1135
    label "treat"
  ]
  node [
    id 1136
    label "pozyskiwa&#263;"
  ]
  node [
    id 1137
    label "ensnare"
  ]
  node [
    id 1138
    label "skupia&#263;"
  ]
  node [
    id 1139
    label "create"
  ]
  node [
    id 1140
    label "przygotowywa&#263;"
  ]
  node [
    id 1141
    label "wprowadza&#263;"
  ]
  node [
    id 1142
    label "tworzenie"
  ]
  node [
    id 1143
    label "organizowanie_si&#281;"
  ]
  node [
    id 1144
    label "dyscyplinowanie"
  ]
  node [
    id 1145
    label "organization"
  ]
  node [
    id 1146
    label "uregulowanie"
  ]
  node [
    id 1147
    label "handling"
  ]
  node [
    id 1148
    label "pozyskiwanie"
  ]
  node [
    id 1149
    label "szykowanie"
  ]
  node [
    id 1150
    label "wprowadzanie"
  ]
  node [
    id 1151
    label "skupianie"
  ]
  node [
    id 1152
    label "dostosowa&#263;"
  ]
  node [
    id 1153
    label "pozyska&#263;"
  ]
  node [
    id 1154
    label "stworzy&#263;"
  ]
  node [
    id 1155
    label "plan"
  ]
  node [
    id 1156
    label "stage"
  ]
  node [
    id 1157
    label "urobi&#263;"
  ]
  node [
    id 1158
    label "wprowadzi&#263;"
  ]
  node [
    id 1159
    label "zaplanowa&#263;"
  ]
  node [
    id 1160
    label "przygotowa&#263;"
  ]
  node [
    id 1161
    label "skupi&#263;"
  ]
  node [
    id 1162
    label "zorganizowanie_si&#281;"
  ]
  node [
    id 1163
    label "stworzenie"
  ]
  node [
    id 1164
    label "zdyscyplinowanie"
  ]
  node [
    id 1165
    label "skupienie"
  ]
  node [
    id 1166
    label "bargain"
  ]
  node [
    id 1167
    label "pozyskanie"
  ]
  node [
    id 1168
    label "constitution"
  ]
  node [
    id 1169
    label "absolutno&#347;&#263;"
  ]
  node [
    id 1170
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 1171
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 1172
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 1173
    label "uwi&#281;zienie"
  ]
  node [
    id 1174
    label "charakterystyka"
  ]
  node [
    id 1175
    label "m&#322;ot"
  ]
  node [
    id 1176
    label "znak"
  ]
  node [
    id 1177
    label "drzewo"
  ]
  node [
    id 1178
    label "pr&#243;ba"
  ]
  node [
    id 1179
    label "attribute"
  ]
  node [
    id 1180
    label "marka"
  ]
  node [
    id 1181
    label "relacja"
  ]
  node [
    id 1182
    label "independence"
  ]
  node [
    id 1183
    label "monarchiczno&#347;&#263;"
  ]
  node [
    id 1184
    label "perpetrate"
  ]
  node [
    id 1185
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 1186
    label "zabranie"
  ]
  node [
    id 1187
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1188
    label "captivity"
  ]
  node [
    id 1189
    label "mechanika"
  ]
  node [
    id 1190
    label "utrzymywanie"
  ]
  node [
    id 1191
    label "poruszenie"
  ]
  node [
    id 1192
    label "myk"
  ]
  node [
    id 1193
    label "utrzyma&#263;"
  ]
  node [
    id 1194
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1195
    label "utrzymanie"
  ]
  node [
    id 1196
    label "travel"
  ]
  node [
    id 1197
    label "kanciasty"
  ]
  node [
    id 1198
    label "commercial_enterprise"
  ]
  node [
    id 1199
    label "strumie&#324;"
  ]
  node [
    id 1200
    label "proces"
  ]
  node [
    id 1201
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1202
    label "kr&#243;tki"
  ]
  node [
    id 1203
    label "taktyka"
  ]
  node [
    id 1204
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1205
    label "apraksja"
  ]
  node [
    id 1206
    label "natural_process"
  ]
  node [
    id 1207
    label "utrzymywa&#263;"
  ]
  node [
    id 1208
    label "d&#322;ugi"
  ]
  node [
    id 1209
    label "dyssypacja_energii"
  ]
  node [
    id 1210
    label "tumult"
  ]
  node [
    id 1211
    label "stopek"
  ]
  node [
    id 1212
    label "manewr"
  ]
  node [
    id 1213
    label "lokomocja"
  ]
  node [
    id 1214
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1215
    label "komunikacja"
  ]
  node [
    id 1216
    label "drift"
  ]
  node [
    id 1217
    label "przebieg"
  ]
  node [
    id 1218
    label "legislacyjnie"
  ]
  node [
    id 1219
    label "nast&#281;pstwo"
  ]
  node [
    id 1220
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1221
    label "przebiec"
  ]
  node [
    id 1222
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1223
    label "motyw"
  ]
  node [
    id 1224
    label "przebiegni&#281;cie"
  ]
  node [
    id 1225
    label "fabu&#322;a"
  ]
  node [
    id 1226
    label "action"
  ]
  node [
    id 1227
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1228
    label "postawa"
  ]
  node [
    id 1229
    label "posuni&#281;cie"
  ]
  node [
    id 1230
    label "maneuver"
  ]
  node [
    id 1231
    label "absolutorium"
  ]
  node [
    id 1232
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1233
    label "boski"
  ]
  node [
    id 1234
    label "krajobraz"
  ]
  node [
    id 1235
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1236
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1237
    label "przywidzenie"
  ]
  node [
    id 1238
    label "presence"
  ]
  node [
    id 1239
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1240
    label "transportation_system"
  ]
  node [
    id 1241
    label "explicite"
  ]
  node [
    id 1242
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1243
    label "wydeptywanie"
  ]
  node [
    id 1244
    label "wydeptanie"
  ]
  node [
    id 1245
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 1246
    label "implicite"
  ]
  node [
    id 1247
    label "ekspedytor"
  ]
  node [
    id 1248
    label "woda_powierzchniowa"
  ]
  node [
    id 1249
    label "ciek_wodny"
  ]
  node [
    id 1250
    label "mn&#243;stwo"
  ]
  node [
    id 1251
    label "Ajgospotamoj"
  ]
  node [
    id 1252
    label "fala"
  ]
  node [
    id 1253
    label "disquiet"
  ]
  node [
    id 1254
    label "ha&#322;as"
  ]
  node [
    id 1255
    label "struktura"
  ]
  node [
    id 1256
    label "mechanika_teoretyczna"
  ]
  node [
    id 1257
    label "mechanika_gruntu"
  ]
  node [
    id 1258
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 1259
    label "mechanika_klasyczna"
  ]
  node [
    id 1260
    label "elektromechanika"
  ]
  node [
    id 1261
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 1262
    label "nauka"
  ]
  node [
    id 1263
    label "fizyka"
  ]
  node [
    id 1264
    label "aeromechanika"
  ]
  node [
    id 1265
    label "telemechanika"
  ]
  node [
    id 1266
    label "hydromechanika"
  ]
  node [
    id 1267
    label "daleki"
  ]
  node [
    id 1268
    label "d&#322;ugo"
  ]
  node [
    id 1269
    label "szybki"
  ]
  node [
    id 1270
    label "jednowyrazowy"
  ]
  node [
    id 1271
    label "bliski"
  ]
  node [
    id 1272
    label "s&#322;aby"
  ]
  node [
    id 1273
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1274
    label "kr&#243;tko"
  ]
  node [
    id 1275
    label "drobny"
  ]
  node [
    id 1276
    label "z&#322;y"
  ]
  node [
    id 1277
    label "byt"
  ]
  node [
    id 1278
    label "argue"
  ]
  node [
    id 1279
    label "podtrzymywa&#263;"
  ]
  node [
    id 1280
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1281
    label "twierdzi&#263;"
  ]
  node [
    id 1282
    label "zapewnia&#263;"
  ]
  node [
    id 1283
    label "corroborate"
  ]
  node [
    id 1284
    label "trzyma&#263;"
  ]
  node [
    id 1285
    label "panowa&#263;"
  ]
  node [
    id 1286
    label "defy"
  ]
  node [
    id 1287
    label "cope"
  ]
  node [
    id 1288
    label "broni&#263;"
  ]
  node [
    id 1289
    label "sprawowa&#263;"
  ]
  node [
    id 1290
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 1291
    label "zachowywa&#263;"
  ]
  node [
    id 1292
    label "obroni&#263;"
  ]
  node [
    id 1293
    label "op&#322;aci&#263;"
  ]
  node [
    id 1294
    label "zdo&#322;a&#263;"
  ]
  node [
    id 1295
    label "podtrzyma&#263;"
  ]
  node [
    id 1296
    label "feed"
  ]
  node [
    id 1297
    label "przetrzyma&#263;"
  ]
  node [
    id 1298
    label "foster"
  ]
  node [
    id 1299
    label "preserve"
  ]
  node [
    id 1300
    label "zapewni&#263;"
  ]
  node [
    id 1301
    label "zachowa&#263;"
  ]
  node [
    id 1302
    label "unie&#347;&#263;"
  ]
  node [
    id 1303
    label "bronienie"
  ]
  node [
    id 1304
    label "trzymanie"
  ]
  node [
    id 1305
    label "bycie"
  ]
  node [
    id 1306
    label "wychowywanie"
  ]
  node [
    id 1307
    label "panowanie"
  ]
  node [
    id 1308
    label "zachowywanie"
  ]
  node [
    id 1309
    label "twierdzenie"
  ]
  node [
    id 1310
    label "chowanie"
  ]
  node [
    id 1311
    label "retention"
  ]
  node [
    id 1312
    label "op&#322;acanie"
  ]
  node [
    id 1313
    label "s&#261;dzenie"
  ]
  node [
    id 1314
    label "zapewnianie"
  ]
  node [
    id 1315
    label "zap&#322;acenie"
  ]
  node [
    id 1316
    label "zachowanie"
  ]
  node [
    id 1317
    label "przetrzymanie"
  ]
  node [
    id 1318
    label "bearing"
  ]
  node [
    id 1319
    label "zdo&#322;anie"
  ]
  node [
    id 1320
    label "subsystencja"
  ]
  node [
    id 1321
    label "uniesienie"
  ]
  node [
    id 1322
    label "wy&#380;ywienie"
  ]
  node [
    id 1323
    label "podtrzymanie"
  ]
  node [
    id 1324
    label "wychowanie"
  ]
  node [
    id 1325
    label "wzbudzenie"
  ]
  node [
    id 1326
    label "gesture"
  ]
  node [
    id 1327
    label "nietaktowny"
  ]
  node [
    id 1328
    label "kanciasto"
  ]
  node [
    id 1329
    label "niezgrabny"
  ]
  node [
    id 1330
    label "kanciaty"
  ]
  node [
    id 1331
    label "szorstki"
  ]
  node [
    id 1332
    label "niesk&#322;adny"
  ]
  node [
    id 1333
    label "stra&#380;nik"
  ]
  node [
    id 1334
    label "szko&#322;a"
  ]
  node [
    id 1335
    label "przedszkole"
  ]
  node [
    id 1336
    label "opiekun"
  ]
  node [
    id 1337
    label "apraxia"
  ]
  node [
    id 1338
    label "zaburzenie"
  ]
  node [
    id 1339
    label "sport_motorowy"
  ]
  node [
    id 1340
    label "jazda"
  ]
  node [
    id 1341
    label "zwiad"
  ]
  node [
    id 1342
    label "metoda"
  ]
  node [
    id 1343
    label "pocz&#261;tki"
  ]
  node [
    id 1344
    label "wrinkle"
  ]
  node [
    id 1345
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1346
    label "Sierpie&#324;"
  ]
  node [
    id 1347
    label "Michnik"
  ]
  node [
    id 1348
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 1349
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 1350
    label "disapprove"
  ]
  node [
    id 1351
    label "zakomunikowa&#263;"
  ]
  node [
    id 1352
    label "nieistnienie"
  ]
  node [
    id 1353
    label "odej&#347;cie"
  ]
  node [
    id 1354
    label "defect"
  ]
  node [
    id 1355
    label "gap"
  ]
  node [
    id 1356
    label "odej&#347;&#263;"
  ]
  node [
    id 1357
    label "wada"
  ]
  node [
    id 1358
    label "odchodzi&#263;"
  ]
  node [
    id 1359
    label "wyr&#243;b"
  ]
  node [
    id 1360
    label "odchodzenie"
  ]
  node [
    id 1361
    label "prywatywny"
  ]
  node [
    id 1362
    label "niebyt"
  ]
  node [
    id 1363
    label "nonexistence"
  ]
  node [
    id 1364
    label "faintness"
  ]
  node [
    id 1365
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1366
    label "strona"
  ]
  node [
    id 1367
    label "imperfection"
  ]
  node [
    id 1368
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 1369
    label "produkt"
  ]
  node [
    id 1370
    label "p&#322;uczkarnia"
  ]
  node [
    id 1371
    label "znakowarka"
  ]
  node [
    id 1372
    label "odrzut"
  ]
  node [
    id 1373
    label "zrezygnowa&#263;"
  ]
  node [
    id 1374
    label "min&#261;&#263;"
  ]
  node [
    id 1375
    label "leave_office"
  ]
  node [
    id 1376
    label "die"
  ]
  node [
    id 1377
    label "retract"
  ]
  node [
    id 1378
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 1379
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 1380
    label "korkowanie"
  ]
  node [
    id 1381
    label "death"
  ]
  node [
    id 1382
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 1383
    label "przestawanie"
  ]
  node [
    id 1384
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 1385
    label "zb&#281;dny"
  ]
  node [
    id 1386
    label "zdychanie"
  ]
  node [
    id 1387
    label "spisywanie_"
  ]
  node [
    id 1388
    label "usuwanie"
  ]
  node [
    id 1389
    label "tracenie"
  ]
  node [
    id 1390
    label "ko&#324;czenie"
  ]
  node [
    id 1391
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1392
    label "&#380;ycie"
  ]
  node [
    id 1393
    label "robienie"
  ]
  node [
    id 1394
    label "opuszczanie"
  ]
  node [
    id 1395
    label "wydalanie"
  ]
  node [
    id 1396
    label "odrzucanie"
  ]
  node [
    id 1397
    label "odstawianie"
  ]
  node [
    id 1398
    label "martwy"
  ]
  node [
    id 1399
    label "ust&#281;powanie"
  ]
  node [
    id 1400
    label "zrzekanie_si&#281;"
  ]
  node [
    id 1401
    label "oddzielanie_si&#281;"
  ]
  node [
    id 1402
    label "wyruszanie"
  ]
  node [
    id 1403
    label "odumieranie"
  ]
  node [
    id 1404
    label "odstawanie"
  ]
  node [
    id 1405
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1406
    label "mijanie"
  ]
  node [
    id 1407
    label "wracanie"
  ]
  node [
    id 1408
    label "oddalanie_si&#281;"
  ]
  node [
    id 1409
    label "kursowanie"
  ]
  node [
    id 1410
    label "blend"
  ]
  node [
    id 1411
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 1412
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 1413
    label "opuszcza&#263;"
  ]
  node [
    id 1414
    label "impart"
  ]
  node [
    id 1415
    label "wyrusza&#263;"
  ]
  node [
    id 1416
    label "seclude"
  ]
  node [
    id 1417
    label "gasn&#261;&#263;"
  ]
  node [
    id 1418
    label "przestawa&#263;"
  ]
  node [
    id 1419
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1420
    label "odstawa&#263;"
  ]
  node [
    id 1421
    label "rezygnowa&#263;"
  ]
  node [
    id 1422
    label "i&#347;&#263;"
  ]
  node [
    id 1423
    label "mija&#263;"
  ]
  node [
    id 1424
    label "mini&#281;cie"
  ]
  node [
    id 1425
    label "odumarcie"
  ]
  node [
    id 1426
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1427
    label "ust&#261;pienie"
  ]
  node [
    id 1428
    label "mogi&#322;a"
  ]
  node [
    id 1429
    label "pomarcie"
  ]
  node [
    id 1430
    label "spisanie_"
  ]
  node [
    id 1431
    label "oddalenie_si&#281;"
  ]
  node [
    id 1432
    label "defenestracja"
  ]
  node [
    id 1433
    label "danie_sobie_spokoju"
  ]
  node [
    id 1434
    label "kres_&#380;ycia"
  ]
  node [
    id 1435
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1436
    label "zdechni&#281;cie"
  ]
  node [
    id 1437
    label "stracenie"
  ]
  node [
    id 1438
    label "szeol"
  ]
  node [
    id 1439
    label "oddzielenie_si&#281;"
  ]
  node [
    id 1440
    label "deviation"
  ]
  node [
    id 1441
    label "wydalenie"
  ]
  node [
    id 1442
    label "&#380;a&#322;oba"
  ]
  node [
    id 1443
    label "pogrzebanie"
  ]
  node [
    id 1444
    label "sko&#324;czenie"
  ]
  node [
    id 1445
    label "withdrawal"
  ]
  node [
    id 1446
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 1447
    label "zabicie"
  ]
  node [
    id 1448
    label "agonia"
  ]
  node [
    id 1449
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 1450
    label "kres"
  ]
  node [
    id 1451
    label "usuni&#281;cie"
  ]
  node [
    id 1452
    label "relinquishment"
  ]
  node [
    id 1453
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1454
    label "poniechanie"
  ]
  node [
    id 1455
    label "zako&#324;czenie"
  ]
  node [
    id 1456
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1457
    label "wypisanie_si&#281;"
  ]
  node [
    id 1458
    label "ciekawski"
  ]
  node [
    id 1459
    label "statysta"
  ]
  node [
    id 1460
    label "definiendum"
  ]
  node [
    id 1461
    label "definiens"
  ]
  node [
    id 1462
    label "obja&#347;nienie"
  ]
  node [
    id 1463
    label "definition"
  ]
  node [
    id 1464
    label "explanation"
  ]
  node [
    id 1465
    label "remark"
  ]
  node [
    id 1466
    label "report"
  ]
  node [
    id 1467
    label "zrozumia&#322;y"
  ]
  node [
    id 1468
    label "przedstawienie"
  ]
  node [
    id 1469
    label "informacja"
  ]
  node [
    id 1470
    label "poinformowanie"
  ]
  node [
    id 1471
    label "rumor"
  ]
  node [
    id 1472
    label "rozpowszechnia&#263;"
  ]
  node [
    id 1473
    label "generalize"
  ]
  node [
    id 1474
    label "sprawia&#263;"
  ]
  node [
    id 1475
    label "noise"
  ]
  node [
    id 1476
    label "jednoznacznie"
  ]
  node [
    id 1477
    label "jasny"
  ]
  node [
    id 1478
    label "ja&#347;niej"
  ]
  node [
    id 1479
    label "dobrze"
  ]
  node [
    id 1480
    label "ja&#347;nie"
  ]
  node [
    id 1481
    label "zrozumiale"
  ]
  node [
    id 1482
    label "szczerze"
  ]
  node [
    id 1483
    label "pogodnie"
  ]
  node [
    id 1484
    label "klarownie"
  ]
  node [
    id 1485
    label "skutecznie"
  ]
  node [
    id 1486
    label "sprawnie"
  ]
  node [
    id 1487
    label "jednoznaczny"
  ]
  node [
    id 1488
    label "prosto"
  ]
  node [
    id 1489
    label "przezroczysty"
  ]
  node [
    id 1490
    label "transparently"
  ]
  node [
    id 1491
    label "przezroczo"
  ]
  node [
    id 1492
    label "klarowny"
  ]
  node [
    id 1493
    label "skuteczny"
  ]
  node [
    id 1494
    label "umiej&#281;tnie"
  ]
  node [
    id 1495
    label "kompetentnie"
  ]
  node [
    id 1496
    label "funkcjonalnie"
  ]
  node [
    id 1497
    label "szybko"
  ]
  node [
    id 1498
    label "sprawny"
  ]
  node [
    id 1499
    label "udanie"
  ]
  node [
    id 1500
    label "zdrowo"
  ]
  node [
    id 1501
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1502
    label "odpowiednio"
  ]
  node [
    id 1503
    label "dobroczynnie"
  ]
  node [
    id 1504
    label "moralnie"
  ]
  node [
    id 1505
    label "korzystnie"
  ]
  node [
    id 1506
    label "pozytywnie"
  ]
  node [
    id 1507
    label "lepiej"
  ]
  node [
    id 1508
    label "wiele"
  ]
  node [
    id 1509
    label "pomy&#347;lnie"
  ]
  node [
    id 1510
    label "dobry"
  ]
  node [
    id 1511
    label "szczery"
  ]
  node [
    id 1512
    label "s&#322;usznie"
  ]
  node [
    id 1513
    label "szczero"
  ]
  node [
    id 1514
    label "hojnie"
  ]
  node [
    id 1515
    label "honestly"
  ]
  node [
    id 1516
    label "przekonuj&#261;co"
  ]
  node [
    id 1517
    label "outspokenly"
  ]
  node [
    id 1518
    label "artlessly"
  ]
  node [
    id 1519
    label "uczciwie"
  ]
  node [
    id 1520
    label "bluffly"
  ]
  node [
    id 1521
    label "przyjemnie"
  ]
  node [
    id 1522
    label "spokojnie"
  ]
  node [
    id 1523
    label "pogodny"
  ]
  node [
    id 1524
    label "&#322;adnie"
  ]
  node [
    id 1525
    label "o&#347;wietlenie"
  ]
  node [
    id 1526
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 1527
    label "o&#347;wietlanie"
  ]
  node [
    id 1528
    label "przytomny"
  ]
  node [
    id 1529
    label "niezm&#261;cony"
  ]
  node [
    id 1530
    label "bia&#322;y"
  ]
  node [
    id 1531
    label "testify"
  ]
  node [
    id 1532
    label "oznajmi&#263;"
  ]
  node [
    id 1533
    label "declare"
  ]
  node [
    id 1534
    label "oceni&#263;"
  ]
  node [
    id 1535
    label "rede"
  ]
  node [
    id 1536
    label "see"
  ]
  node [
    id 1537
    label "poinformowa&#263;"
  ]
  node [
    id 1538
    label "advise"
  ]
  node [
    id 1539
    label "discover"
  ]
  node [
    id 1540
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 1541
    label "wydoby&#263;"
  ]
  node [
    id 1542
    label "okre&#347;li&#263;"
  ]
  node [
    id 1543
    label "poda&#263;"
  ]
  node [
    id 1544
    label "express"
  ]
  node [
    id 1545
    label "wyrazi&#263;"
  ]
  node [
    id 1546
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1547
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 1548
    label "rzekn&#261;&#263;"
  ]
  node [
    id 1549
    label "unwrap"
  ]
  node [
    id 1550
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1551
    label "convey"
  ]
  node [
    id 1552
    label "paleocen"
  ]
  node [
    id 1553
    label "wiek"
  ]
  node [
    id 1554
    label "choroba_wieku"
  ]
  node [
    id 1555
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 1556
    label "chron"
  ]
  node [
    id 1557
    label "rok"
  ]
  node [
    id 1558
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 1559
    label "paleogen"
  ]
  node [
    id 1560
    label "tanet"
  ]
  node [
    id 1561
    label "formacja_geologiczna"
  ]
  node [
    id 1562
    label "zeland"
  ]
  node [
    id 1563
    label "niez&#322;y"
  ]
  node [
    id 1564
    label "wystarczaj&#261;co"
  ]
  node [
    id 1565
    label "odpowiedni"
  ]
  node [
    id 1566
    label "zdarzony"
  ]
  node [
    id 1567
    label "nale&#380;ny"
  ]
  node [
    id 1568
    label "nale&#380;yty"
  ]
  node [
    id 1569
    label "stosownie"
  ]
  node [
    id 1570
    label "odpowiadanie"
  ]
  node [
    id 1571
    label "specjalny"
  ]
  node [
    id 1572
    label "intensywny"
  ]
  node [
    id 1573
    label "udolny"
  ]
  node [
    id 1574
    label "&#347;mieszny"
  ]
  node [
    id 1575
    label "niczegowaty"
  ]
  node [
    id 1576
    label "nieszpetny"
  ]
  node [
    id 1577
    label "spory"
  ]
  node [
    id 1578
    label "pozytywny"
  ]
  node [
    id 1579
    label "korzystny"
  ]
  node [
    id 1580
    label "nie&#378;le"
  ]
  node [
    id 1581
    label "otworzysty"
  ]
  node [
    id 1582
    label "aktywny"
  ]
  node [
    id 1583
    label "nieograniczony"
  ]
  node [
    id 1584
    label "publiczny"
  ]
  node [
    id 1585
    label "zdecydowany"
  ]
  node [
    id 1586
    label "prostoduszny"
  ]
  node [
    id 1587
    label "jawnie"
  ]
  node [
    id 1588
    label "bezpo&#347;redni"
  ]
  node [
    id 1589
    label "aktualny"
  ]
  node [
    id 1590
    label "kontaktowy"
  ]
  node [
    id 1591
    label "otwarcie"
  ]
  node [
    id 1592
    label "ewidentny"
  ]
  node [
    id 1593
    label "dost&#281;pny"
  ]
  node [
    id 1594
    label "gotowy"
  ]
  node [
    id 1595
    label "upublicznianie"
  ]
  node [
    id 1596
    label "jawny"
  ]
  node [
    id 1597
    label "upublicznienie"
  ]
  node [
    id 1598
    label "publicznie"
  ]
  node [
    id 1599
    label "oczywisty"
  ]
  node [
    id 1600
    label "pewny"
  ]
  node [
    id 1601
    label "ewidentnie"
  ]
  node [
    id 1602
    label "zdecydowanie"
  ]
  node [
    id 1603
    label "zauwa&#380;alny"
  ]
  node [
    id 1604
    label "dowolny"
  ]
  node [
    id 1605
    label "rozleg&#322;y"
  ]
  node [
    id 1606
    label "nieograniczenie"
  ]
  node [
    id 1607
    label "mo&#380;liwy"
  ]
  node [
    id 1608
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 1609
    label "odblokowanie_si&#281;"
  ]
  node [
    id 1610
    label "dost&#281;pnie"
  ]
  node [
    id 1611
    label "&#322;atwy"
  ]
  node [
    id 1612
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 1613
    label "przyst&#281;pnie"
  ]
  node [
    id 1614
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1615
    label "bezpo&#347;rednio"
  ]
  node [
    id 1616
    label "nietrze&#378;wy"
  ]
  node [
    id 1617
    label "czekanie"
  ]
  node [
    id 1618
    label "m&#243;c"
  ]
  node [
    id 1619
    label "gotowo"
  ]
  node [
    id 1620
    label "przygotowanie"
  ]
  node [
    id 1621
    label "przygotowywanie"
  ]
  node [
    id 1622
    label "dyspozycyjny"
  ]
  node [
    id 1623
    label "zalany"
  ]
  node [
    id 1624
    label "nieuchronny"
  ]
  node [
    id 1625
    label "doj&#347;cie"
  ]
  node [
    id 1626
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 1627
    label "aktualnie"
  ]
  node [
    id 1628
    label "wa&#380;ny"
  ]
  node [
    id 1629
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1630
    label "aktualizowanie"
  ]
  node [
    id 1631
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 1632
    label "uaktualnienie"
  ]
  node [
    id 1633
    label "ciekawy"
  ]
  node [
    id 1634
    label "realny"
  ]
  node [
    id 1635
    label "zdolny"
  ]
  node [
    id 1636
    label "czynnie"
  ]
  node [
    id 1637
    label "czynny"
  ]
  node [
    id 1638
    label "uczynnianie"
  ]
  node [
    id 1639
    label "aktywnie"
  ]
  node [
    id 1640
    label "istotny"
  ]
  node [
    id 1641
    label "faktyczny"
  ]
  node [
    id 1642
    label "uczynnienie"
  ]
  node [
    id 1643
    label "prostodusznie"
  ]
  node [
    id 1644
    label "przyst&#281;pny"
  ]
  node [
    id 1645
    label "kontaktowo"
  ]
  node [
    id 1646
    label "jawno"
  ]
  node [
    id 1647
    label "rozpocz&#281;cie"
  ]
  node [
    id 1648
    label "udost&#281;pnienie"
  ]
  node [
    id 1649
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1650
    label "opening"
  ]
  node [
    id 1651
    label "gra&#263;"
  ]
  node [
    id 1652
    label "granie"
  ]
  node [
    id 1653
    label "sprzeciw"
  ]
  node [
    id 1654
    label "czerwona_kartka"
  ]
  node [
    id 1655
    label "protestacja"
  ]
  node [
    id 1656
    label "reakcja"
  ]
  node [
    id 1657
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1658
    label "subject"
  ]
  node [
    id 1659
    label "czynnik"
  ]
  node [
    id 1660
    label "matuszka"
  ]
  node [
    id 1661
    label "poci&#261;ganie"
  ]
  node [
    id 1662
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1663
    label "przyczyna"
  ]
  node [
    id 1664
    label "geneza"
  ]
  node [
    id 1665
    label "uprz&#261;&#380;"
  ]
  node [
    id 1666
    label "kartka"
  ]
  node [
    id 1667
    label "logowanie"
  ]
  node [
    id 1668
    label "plik"
  ]
  node [
    id 1669
    label "adres_internetowy"
  ]
  node [
    id 1670
    label "linia"
  ]
  node [
    id 1671
    label "serwis_internetowy"
  ]
  node [
    id 1672
    label "posta&#263;"
  ]
  node [
    id 1673
    label "bok"
  ]
  node [
    id 1674
    label "skr&#281;canie"
  ]
  node [
    id 1675
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1676
    label "orientowanie"
  ]
  node [
    id 1677
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1678
    label "ty&#322;"
  ]
  node [
    id 1679
    label "fragment"
  ]
  node [
    id 1680
    label "layout"
  ]
  node [
    id 1681
    label "obiekt"
  ]
  node [
    id 1682
    label "zorientowa&#263;"
  ]
  node [
    id 1683
    label "pagina"
  ]
  node [
    id 1684
    label "podmiot"
  ]
  node [
    id 1685
    label "g&#243;ra"
  ]
  node [
    id 1686
    label "orientowa&#263;"
  ]
  node [
    id 1687
    label "voice"
  ]
  node [
    id 1688
    label "orientacja"
  ]
  node [
    id 1689
    label "prz&#243;d"
  ]
  node [
    id 1690
    label "internet"
  ]
  node [
    id 1691
    label "powierzchnia"
  ]
  node [
    id 1692
    label "skr&#281;cenie"
  ]
  node [
    id 1693
    label "u&#378;dzienica"
  ]
  node [
    id 1694
    label "postronek"
  ]
  node [
    id 1695
    label "uzda"
  ]
  node [
    id 1696
    label "chom&#261;to"
  ]
  node [
    id 1697
    label "naszelnik"
  ]
  node [
    id 1698
    label "nakarcznik"
  ]
  node [
    id 1699
    label "janczary"
  ]
  node [
    id 1700
    label "moderunek"
  ]
  node [
    id 1701
    label "podogonie"
  ]
  node [
    id 1702
    label "divisor"
  ]
  node [
    id 1703
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1704
    label "faktor"
  ]
  node [
    id 1705
    label "agent"
  ]
  node [
    id 1706
    label "ekspozycja"
  ]
  node [
    id 1707
    label "iloczyn"
  ]
  node [
    id 1708
    label "rodny"
  ]
  node [
    id 1709
    label "powstanie"
  ]
  node [
    id 1710
    label "monogeneza"
  ]
  node [
    id 1711
    label "zaistnienie"
  ]
  node [
    id 1712
    label "pocz&#261;tek"
  ]
  node [
    id 1713
    label "popadia"
  ]
  node [
    id 1714
    label "ojczyzna"
  ]
  node [
    id 1715
    label "implikacja"
  ]
  node [
    id 1716
    label "powodowanie"
  ]
  node [
    id 1717
    label "powiewanie"
  ]
  node [
    id 1718
    label "powleczenie"
  ]
  node [
    id 1719
    label "interesowanie"
  ]
  node [
    id 1720
    label "manienie"
  ]
  node [
    id 1721
    label "upijanie"
  ]
  node [
    id 1722
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1723
    label "przechylanie"
  ]
  node [
    id 1724
    label "temptation"
  ]
  node [
    id 1725
    label "pokrywanie"
  ]
  node [
    id 1726
    label "oddzieranie"
  ]
  node [
    id 1727
    label "urwanie"
  ]
  node [
    id 1728
    label "oddarcie"
  ]
  node [
    id 1729
    label "przesuwanie"
  ]
  node [
    id 1730
    label "zerwanie"
  ]
  node [
    id 1731
    label "ruszanie"
  ]
  node [
    id 1732
    label "traction"
  ]
  node [
    id 1733
    label "urywanie"
  ]
  node [
    id 1734
    label "nos"
  ]
  node [
    id 1735
    label "powlekanie"
  ]
  node [
    id 1736
    label "wsysanie"
  ]
  node [
    id 1737
    label "upicie"
  ]
  node [
    id 1738
    label "pull"
  ]
  node [
    id 1739
    label "wyszarpanie"
  ]
  node [
    id 1740
    label "pokrycie"
  ]
  node [
    id 1741
    label "wywo&#322;anie"
  ]
  node [
    id 1742
    label "si&#261;kanie"
  ]
  node [
    id 1743
    label "przechylenie"
  ]
  node [
    id 1744
    label "przesuni&#281;cie"
  ]
  node [
    id 1745
    label "zaci&#261;ganie"
  ]
  node [
    id 1746
    label "wessanie"
  ]
  node [
    id 1747
    label "powianie"
  ]
  node [
    id 1748
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1749
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 1750
    label "wyra&#378;nie"
  ]
  node [
    id 1751
    label "nieoboj&#281;tny"
  ]
  node [
    id 1752
    label "szkodliwy"
  ]
  node [
    id 1753
    label "nieneutralny"
  ]
  node [
    id 1754
    label "zauwa&#380;alnie"
  ]
  node [
    id 1755
    label "postrzegalny"
  ]
  node [
    id 1756
    label "nieneutralnie"
  ]
  node [
    id 1757
    label "distinctly"
  ]
  node [
    id 1758
    label "przej&#347;cie"
  ]
  node [
    id 1759
    label "zakres"
  ]
  node [
    id 1760
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1761
    label "Ural"
  ]
  node [
    id 1762
    label "miara"
  ]
  node [
    id 1763
    label "end"
  ]
  node [
    id 1764
    label "pu&#322;ap"
  ]
  node [
    id 1765
    label "koniec"
  ]
  node [
    id 1766
    label "granice"
  ]
  node [
    id 1767
    label "frontier"
  ]
  node [
    id 1768
    label "ustawa"
  ]
  node [
    id 1769
    label "wymienienie"
  ]
  node [
    id 1770
    label "zaliczenie"
  ]
  node [
    id 1771
    label "traversal"
  ]
  node [
    id 1772
    label "przewy&#380;szenie"
  ]
  node [
    id 1773
    label "experience"
  ]
  node [
    id 1774
    label "przepuszczenie"
  ]
  node [
    id 1775
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1776
    label "strain"
  ]
  node [
    id 1777
    label "przerobienie"
  ]
  node [
    id 1778
    label "crack"
  ]
  node [
    id 1779
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 1780
    label "wstawka"
  ]
  node [
    id 1781
    label "prze&#380;ycie"
  ]
  node [
    id 1782
    label "uznanie"
  ]
  node [
    id 1783
    label "doznanie"
  ]
  node [
    id 1784
    label "dostanie_si&#281;"
  ]
  node [
    id 1785
    label "trwanie"
  ]
  node [
    id 1786
    label "przebycie"
  ]
  node [
    id 1787
    label "wytyczenie"
  ]
  node [
    id 1788
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1789
    label "przepojenie"
  ]
  node [
    id 1790
    label "nas&#261;czenie"
  ]
  node [
    id 1791
    label "nale&#380;enie"
  ]
  node [
    id 1792
    label "mienie"
  ]
  node [
    id 1793
    label "przedostanie_si&#281;"
  ]
  node [
    id 1794
    label "przemokni&#281;cie"
  ]
  node [
    id 1795
    label "nasycenie_si&#281;"
  ]
  node [
    id 1796
    label "stanie_si&#281;"
  ]
  node [
    id 1797
    label "offense"
  ]
  node [
    id 1798
    label "teoria"
  ]
  node [
    id 1799
    label "strop"
  ]
  node [
    id 1800
    label "powa&#322;a"
  ]
  node [
    id 1801
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1802
    label "ostatnie_podrygi"
  ]
  node [
    id 1803
    label "visitation"
  ]
  node [
    id 1804
    label "szereg"
  ]
  node [
    id 1805
    label "proportion"
  ]
  node [
    id 1806
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 1807
    label "wielko&#347;&#263;"
  ]
  node [
    id 1808
    label "continence"
  ]
  node [
    id 1809
    label "supremum"
  ]
  node [
    id 1810
    label "skala"
  ]
  node [
    id 1811
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1812
    label "jednostka"
  ]
  node [
    id 1813
    label "przeliczy&#263;"
  ]
  node [
    id 1814
    label "matematyka"
  ]
  node [
    id 1815
    label "rzut"
  ]
  node [
    id 1816
    label "odwiedziny"
  ]
  node [
    id 1817
    label "liczba"
  ]
  node [
    id 1818
    label "przeliczanie"
  ]
  node [
    id 1819
    label "dymensja"
  ]
  node [
    id 1820
    label "przelicza&#263;"
  ]
  node [
    id 1821
    label "infimum"
  ]
  node [
    id 1822
    label "przeliczenie"
  ]
  node [
    id 1823
    label "sfera"
  ]
  node [
    id 1824
    label "podzakres"
  ]
  node [
    id 1825
    label "dziedzina"
  ]
  node [
    id 1826
    label "desygnat"
  ]
  node [
    id 1827
    label "circle"
  ]
  node [
    id 1828
    label "Eurazja"
  ]
  node [
    id 1829
    label "przyzwoity"
  ]
  node [
    id 1830
    label "etycznie"
  ]
  node [
    id 1831
    label "dobroczynny"
  ]
  node [
    id 1832
    label "czw&#243;rka"
  ]
  node [
    id 1833
    label "spokojny"
  ]
  node [
    id 1834
    label "mi&#322;y"
  ]
  node [
    id 1835
    label "grzeczny"
  ]
  node [
    id 1836
    label "powitanie"
  ]
  node [
    id 1837
    label "ca&#322;y"
  ]
  node [
    id 1838
    label "zwrot"
  ]
  node [
    id 1839
    label "pomy&#347;lny"
  ]
  node [
    id 1840
    label "moralny"
  ]
  node [
    id 1841
    label "drogi"
  ]
  node [
    id 1842
    label "pos&#322;uszny"
  ]
  node [
    id 1843
    label "kulturalny"
  ]
  node [
    id 1844
    label "skromny"
  ]
  node [
    id 1845
    label "stosowny"
  ]
  node [
    id 1846
    label "przystojny"
  ]
  node [
    id 1847
    label "przyzwoicie"
  ]
  node [
    id 1848
    label "kolumna"
  ]
  node [
    id 1849
    label "budowla"
  ]
  node [
    id 1850
    label "obudowanie"
  ]
  node [
    id 1851
    label "obudowywa&#263;"
  ]
  node [
    id 1852
    label "zbudowa&#263;"
  ]
  node [
    id 1853
    label "obudowa&#263;"
  ]
  node [
    id 1854
    label "kolumnada"
  ]
  node [
    id 1855
    label "korpus"
  ]
  node [
    id 1856
    label "Sukiennice"
  ]
  node [
    id 1857
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 1858
    label "fundament"
  ]
  node [
    id 1859
    label "obudowywanie"
  ]
  node [
    id 1860
    label "postanie"
  ]
  node [
    id 1861
    label "zbudowanie"
  ]
  node [
    id 1862
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 1863
    label "stan_surowy"
  ]
  node [
    id 1864
    label "konstrukcja"
  ]
  node [
    id 1865
    label "kszta&#322;t"
  ]
  node [
    id 1866
    label "column"
  ]
  node [
    id 1867
    label "podpora"
  ]
  node [
    id 1868
    label "s&#322;up"
  ]
  node [
    id 1869
    label "awangarda"
  ]
  node [
    id 1870
    label "heading"
  ]
  node [
    id 1871
    label "wykres"
  ]
  node [
    id 1872
    label "ogniwo_galwaniczne"
  ]
  node [
    id 1873
    label "element"
  ]
  node [
    id 1874
    label "pomnik"
  ]
  node [
    id 1875
    label "artyku&#322;"
  ]
  node [
    id 1876
    label "g&#322;o&#347;nik"
  ]
  node [
    id 1877
    label "plinta"
  ]
  node [
    id 1878
    label "tabela"
  ]
  node [
    id 1879
    label "trzon"
  ]
  node [
    id 1880
    label "szyk"
  ]
  node [
    id 1881
    label "megaron"
  ]
  node [
    id 1882
    label "macierz"
  ]
  node [
    id 1883
    label "reprezentacja"
  ]
  node [
    id 1884
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 1885
    label "baza"
  ]
  node [
    id 1886
    label "ariergarda"
  ]
  node [
    id 1887
    label "&#322;am"
  ]
  node [
    id 1888
    label "kierownica"
  ]
  node [
    id 1889
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1890
    label "wysiadka"
  ]
  node [
    id 1891
    label "reverse"
  ]
  node [
    id 1892
    label "strata"
  ]
  node [
    id 1893
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 1894
    label "przegra"
  ]
  node [
    id 1895
    label "k&#322;adzenie"
  ]
  node [
    id 1896
    label "niepowodzenie"
  ]
  node [
    id 1897
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 1898
    label "passa"
  ]
  node [
    id 1899
    label "bilans"
  ]
  node [
    id 1900
    label "zniszczenie"
  ]
  node [
    id 1901
    label "ubytek"
  ]
  node [
    id 1902
    label "szwank"
  ]
  node [
    id 1903
    label "pora&#380;ka"
  ]
  node [
    id 1904
    label "lot"
  ]
  node [
    id 1905
    label "przegraniec"
  ]
  node [
    id 1906
    label "tragedia"
  ]
  node [
    id 1907
    label "nieudacznik"
  ]
  node [
    id 1908
    label "pszczo&#322;a"
  ]
  node [
    id 1909
    label "continuum"
  ]
  node [
    id 1910
    label "ci&#261;g&#322;o&#347;&#263;"
  ]
  node [
    id 1911
    label "sukces"
  ]
  node [
    id 1912
    label "ci&#261;g"
  ]
  node [
    id 1913
    label "przenocowanie"
  ]
  node [
    id 1914
    label "nak&#322;adzenie"
  ]
  node [
    id 1915
    label "pouk&#322;adanie"
  ]
  node [
    id 1916
    label "zepsucie"
  ]
  node [
    id 1917
    label "ustawienie"
  ]
  node [
    id 1918
    label "trim"
  ]
  node [
    id 1919
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 1920
    label "ugoszczenie"
  ]
  node [
    id 1921
    label "adres"
  ]
  node [
    id 1922
    label "umieszczenie"
  ]
  node [
    id 1923
    label "reading"
  ]
  node [
    id 1924
    label "presentation"
  ]
  node [
    id 1925
    label "le&#380;e&#263;"
  ]
  node [
    id 1926
    label "umieszczanie"
  ]
  node [
    id 1927
    label "poszywanie"
  ]
  node [
    id 1928
    label "przyk&#322;adanie"
  ]
  node [
    id 1929
    label "ubieranie"
  ]
  node [
    id 1930
    label "disposal"
  ]
  node [
    id 1931
    label "sk&#322;adanie"
  ]
  node [
    id 1932
    label "psucie"
  ]
  node [
    id 1933
    label "obk&#322;adanie"
  ]
  node [
    id 1934
    label "zak&#322;adanie"
  ]
  node [
    id 1935
    label "montowanie"
  ]
  node [
    id 1936
    label "debit"
  ]
  node [
    id 1937
    label "druk"
  ]
  node [
    id 1938
    label "szata_graficzna"
  ]
  node [
    id 1939
    label "wydawa&#263;"
  ]
  node [
    id 1940
    label "szermierka"
  ]
  node [
    id 1941
    label "wyda&#263;"
  ]
  node [
    id 1942
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1943
    label "rozmieszczenie"
  ]
  node [
    id 1944
    label "awansowa&#263;"
  ]
  node [
    id 1945
    label "wojsko"
  ]
  node [
    id 1946
    label "znaczenie"
  ]
  node [
    id 1947
    label "awans"
  ]
  node [
    id 1948
    label "poster"
  ]
  node [
    id 1949
    label "odk&#322;adanie"
  ]
  node [
    id 1950
    label "condition"
  ]
  node [
    id 1951
    label "liczenie"
  ]
  node [
    id 1952
    label "stawianie"
  ]
  node [
    id 1953
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1954
    label "assay"
  ]
  node [
    id 1955
    label "wskazywanie"
  ]
  node [
    id 1956
    label "wyraz"
  ]
  node [
    id 1957
    label "gravity"
  ]
  node [
    id 1958
    label "weight"
  ]
  node [
    id 1959
    label "command"
  ]
  node [
    id 1960
    label "odgrywanie_roli"
  ]
  node [
    id 1961
    label "istota"
  ]
  node [
    id 1962
    label "okre&#347;lanie"
  ]
  node [
    id 1963
    label "kto&#347;"
  ]
  node [
    id 1964
    label "wyra&#380;enie"
  ]
  node [
    id 1965
    label "warunki"
  ]
  node [
    id 1966
    label "realia"
  ]
  node [
    id 1967
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1968
    label "ustalenie"
  ]
  node [
    id 1969
    label "erection"
  ]
  node [
    id 1970
    label "setup"
  ]
  node [
    id 1971
    label "erecting"
  ]
  node [
    id 1972
    label "poustawianie"
  ]
  node [
    id 1973
    label "zinterpretowanie"
  ]
  node [
    id 1974
    label "porozstawianie"
  ]
  node [
    id 1975
    label "rola"
  ]
  node [
    id 1976
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 1977
    label "technika"
  ]
  node [
    id 1978
    label "pismo"
  ]
  node [
    id 1979
    label "glif"
  ]
  node [
    id 1980
    label "dese&#324;"
  ]
  node [
    id 1981
    label "prohibita"
  ]
  node [
    id 1982
    label "cymelium"
  ]
  node [
    id 1983
    label "tkanina"
  ]
  node [
    id 1984
    label "zaproszenie"
  ]
  node [
    id 1985
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1986
    label "formatowa&#263;"
  ]
  node [
    id 1987
    label "formatowanie"
  ]
  node [
    id 1988
    label "zdobnik"
  ]
  node [
    id 1989
    label "character"
  ]
  node [
    id 1990
    label "printing"
  ]
  node [
    id 1991
    label "notification"
  ]
  node [
    id 1992
    label "porozmieszczanie"
  ]
  node [
    id 1993
    label "wyst&#281;powanie"
  ]
  node [
    id 1994
    label "uk&#322;ad"
  ]
  node [
    id 1995
    label "podmiotowo"
  ]
  node [
    id 1996
    label "spoczywa&#263;"
  ]
  node [
    id 1997
    label "lie"
  ]
  node [
    id 1998
    label "pokrywa&#263;"
  ]
  node [
    id 1999
    label "zwierz&#281;"
  ]
  node [
    id 2000
    label "equate"
  ]
  node [
    id 2001
    label "gr&#243;b"
  ]
  node [
    id 2002
    label "personalia"
  ]
  node [
    id 2003
    label "domena"
  ]
  node [
    id 2004
    label "kod_pocztowy"
  ]
  node [
    id 2005
    label "adres_elektroniczny"
  ]
  node [
    id 2006
    label "przesy&#322;ka"
  ]
  node [
    id 2007
    label "cywilizacja"
  ]
  node [
    id 2008
    label "pole"
  ]
  node [
    id 2009
    label "elita"
  ]
  node [
    id 2010
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 2011
    label "aspo&#322;eczny"
  ]
  node [
    id 2012
    label "ludzie_pracy"
  ]
  node [
    id 2013
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 2014
    label "pozaklasowy"
  ]
  node [
    id 2015
    label "uwarstwienie"
  ]
  node [
    id 2016
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 2017
    label "community"
  ]
  node [
    id 2018
    label "klasa"
  ]
  node [
    id 2019
    label "kastowo&#347;&#263;"
  ]
  node [
    id 2020
    label "position"
  ]
  node [
    id 2021
    label "preferment"
  ]
  node [
    id 2022
    label "wzrost"
  ]
  node [
    id 2023
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 2024
    label "korzy&#347;&#263;"
  ]
  node [
    id 2025
    label "kariera"
  ]
  node [
    id 2026
    label "nagroda"
  ]
  node [
    id 2027
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 2028
    label "zaliczka"
  ]
  node [
    id 2029
    label "stanowisko"
  ]
  node [
    id 2030
    label "przechodzenie"
  ]
  node [
    id 2031
    label "przeniesienie"
  ]
  node [
    id 2032
    label "promowanie"
  ]
  node [
    id 2033
    label "habilitowanie_si&#281;"
  ]
  node [
    id 2034
    label "obejmowanie"
  ]
  node [
    id 2035
    label "przenoszenie"
  ]
  node [
    id 2036
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 2037
    label "dawa&#263;_awans"
  ]
  node [
    id 2038
    label "przej&#347;&#263;"
  ]
  node [
    id 2039
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 2040
    label "da&#263;_awans"
  ]
  node [
    id 2041
    label "przechodzi&#263;"
  ]
  node [
    id 2042
    label "zrejterowanie"
  ]
  node [
    id 2043
    label "zmobilizowa&#263;"
  ]
  node [
    id 2044
    label "dezerter"
  ]
  node [
    id 2045
    label "oddzia&#322;_karny"
  ]
  node [
    id 2046
    label "rezerwa"
  ]
  node [
    id 2047
    label "tabor"
  ]
  node [
    id 2048
    label "wermacht"
  ]
  node [
    id 2049
    label "cofni&#281;cie"
  ]
  node [
    id 2050
    label "potencja"
  ]
  node [
    id 2051
    label "soldateska"
  ]
  node [
    id 2052
    label "ods&#322;ugiwanie"
  ]
  node [
    id 2053
    label "werbowanie_si&#281;"
  ]
  node [
    id 2054
    label "zdemobilizowanie"
  ]
  node [
    id 2055
    label "oddzia&#322;"
  ]
  node [
    id 2056
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 2057
    label "s&#322;u&#380;ba"
  ]
  node [
    id 2058
    label "or&#281;&#380;"
  ]
  node [
    id 2059
    label "Legia_Cudzoziemska"
  ]
  node [
    id 2060
    label "Armia_Czerwona"
  ]
  node [
    id 2061
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 2062
    label "rejterowanie"
  ]
  node [
    id 2063
    label "Czerwona_Gwardia"
  ]
  node [
    id 2064
    label "si&#322;a"
  ]
  node [
    id 2065
    label "zrejterowa&#263;"
  ]
  node [
    id 2066
    label "sztabslekarz"
  ]
  node [
    id 2067
    label "zmobilizowanie"
  ]
  node [
    id 2068
    label "wojo"
  ]
  node [
    id 2069
    label "pospolite_ruszenie"
  ]
  node [
    id 2070
    label "Eurokorpus"
  ]
  node [
    id 2071
    label "mobilizowanie"
  ]
  node [
    id 2072
    label "rejterowa&#263;"
  ]
  node [
    id 2073
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 2074
    label "mobilizowa&#263;"
  ]
  node [
    id 2075
    label "Armia_Krajowa"
  ]
  node [
    id 2076
    label "obrona"
  ]
  node [
    id 2077
    label "dryl"
  ]
  node [
    id 2078
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 2079
    label "petarda"
  ]
  node [
    id 2080
    label "zdemobilizowa&#263;"
  ]
  node [
    id 2081
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 2082
    label "plon"
  ]
  node [
    id 2083
    label "surrender"
  ]
  node [
    id 2084
    label "kojarzy&#263;"
  ]
  node [
    id 2085
    label "wiano"
  ]
  node [
    id 2086
    label "podawa&#263;"
  ]
  node [
    id 2087
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 2088
    label "ujawnia&#263;"
  ]
  node [
    id 2089
    label "placard"
  ]
  node [
    id 2090
    label "powierza&#263;"
  ]
  node [
    id 2091
    label "denuncjowa&#263;"
  ]
  node [
    id 2092
    label "tajemnica"
  ]
  node [
    id 2093
    label "panna_na_wydaniu"
  ]
  node [
    id 2094
    label "wytwarza&#263;"
  ]
  node [
    id 2095
    label "train"
  ]
  node [
    id 2096
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 2097
    label "bran&#380;owiec"
  ]
  node [
    id 2098
    label "edytor"
  ]
  node [
    id 2099
    label "powierzy&#263;"
  ]
  node [
    id 2100
    label "skojarzy&#263;"
  ]
  node [
    id 2101
    label "zadenuncjowa&#263;"
  ]
  node [
    id 2102
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 2103
    label "translate"
  ]
  node [
    id 2104
    label "dress"
  ]
  node [
    id 2105
    label "supply"
  ]
  node [
    id 2106
    label "ujawni&#263;"
  ]
  node [
    id 2107
    label "plansza"
  ]
  node [
    id 2108
    label "wi&#261;zanie"
  ]
  node [
    id 2109
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 2110
    label "ripostowa&#263;"
  ]
  node [
    id 2111
    label "czarna_kartka"
  ]
  node [
    id 2112
    label "fight"
  ]
  node [
    id 2113
    label "sztuka"
  ]
  node [
    id 2114
    label "sport_walki"
  ]
  node [
    id 2115
    label "apel"
  ]
  node [
    id 2116
    label "manszeta"
  ]
  node [
    id 2117
    label "ripostowanie"
  ]
  node [
    id 2118
    label "tusz"
  ]
  node [
    id 2119
    label "catalog"
  ]
  node [
    id 2120
    label "akt"
  ]
  node [
    id 2121
    label "sumariusz"
  ]
  node [
    id 2122
    label "book"
  ]
  node [
    id 2123
    label "stock"
  ]
  node [
    id 2124
    label "figurowa&#263;"
  ]
  node [
    id 2125
    label "wyliczanka"
  ]
  node [
    id 2126
    label "afisz"
  ]
  node [
    id 2127
    label "przybli&#380;enie"
  ]
  node [
    id 2128
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 2129
    label "kategoria"
  ]
  node [
    id 2130
    label "szpaler"
  ]
  node [
    id 2131
    label "lon&#380;a"
  ]
  node [
    id 2132
    label "uporz&#261;dkowanie"
  ]
  node [
    id 2133
    label "egzekutywa"
  ]
  node [
    id 2134
    label "jednostka_systematyczna"
  ]
  node [
    id 2135
    label "premier"
  ]
  node [
    id 2136
    label "Londyn"
  ]
  node [
    id 2137
    label "gabinet_cieni"
  ]
  node [
    id 2138
    label "gromada"
  ]
  node [
    id 2139
    label "number"
  ]
  node [
    id 2140
    label "Konsulat"
  ]
  node [
    id 2141
    label "tract"
  ]
  node [
    id 2142
    label "Larry"
  ]
  node [
    id 2143
    label "Lessig"
  ]
  node [
    id 2144
    label "Benjamin"
  ]
  node [
    id 2145
    label "Mako"
  ]
  node [
    id 2146
    label "Hill"
  ]
  node [
    id 2147
    label "Towards"
  ]
  node [
    id 2148
    label "albo"
  ]
  node [
    id 2149
    label "of"
  ]
  node [
    id 2150
    label "Freedom"
  ]
  node [
    id 2151
    label "Creative"
  ]
  node [
    id 2152
    label "Commons"
  ]
  node [
    id 2153
    label "Anda"
  ]
  node [
    id 2154
    label "Free"
  ]
  node [
    id 2155
    label "Movement"
  ]
  node [
    id 2156
    label "wyspa"
  ]
  node [
    id 2157
    label "kierunek"
  ]
  node [
    id 2158
    label "i"
  ]
  node [
    id 2159
    label "rucho"
  ]
  node [
    id 2160
    label "wolny"
  ]
  node [
    id 2161
    label "oprogramowa&#263;"
  ]
  node [
    id 2162
    label "WOS"
  ]
  node [
    id 2163
    label "4"
  ]
  node [
    id 2164
    label "Lessiga"
  ]
  node [
    id 2165
    label "Benjamina"
  ]
  node [
    id 2166
    label "Richard"
  ]
  node [
    id 2167
    label "Stallmanem"
  ]
  node [
    id 2168
    label "Jaros&#322;awa"
  ]
  node [
    id 2169
    label "Lipszyc"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 22
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 235
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 287
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 209
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 361
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 17
    target 53
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 292
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 332
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 499
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 382
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 43
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 730
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 287
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 20
    target 967
  ]
  edge [
    source 20
    target 968
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 293
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 20
    target 971
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 274
  ]
  edge [
    source 20
    target 275
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 285
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 20
    target 972
  ]
  edge [
    source 20
    target 973
  ]
  edge [
    source 20
    target 974
  ]
  edge [
    source 20
    target 975
  ]
  edge [
    source 20
    target 976
  ]
  edge [
    source 20
    target 977
  ]
  edge [
    source 20
    target 978
  ]
  edge [
    source 20
    target 979
  ]
  edge [
    source 20
    target 980
  ]
  edge [
    source 20
    target 981
  ]
  edge [
    source 20
    target 982
  ]
  edge [
    source 20
    target 983
  ]
  edge [
    source 20
    target 984
  ]
  edge [
    source 20
    target 985
  ]
  edge [
    source 20
    target 986
  ]
  edge [
    source 20
    target 987
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 988
  ]
  edge [
    source 20
    target 989
  ]
  edge [
    source 20
    target 990
  ]
  edge [
    source 20
    target 991
  ]
  edge [
    source 20
    target 992
  ]
  edge [
    source 20
    target 993
  ]
  edge [
    source 20
    target 994
  ]
  edge [
    source 20
    target 995
  ]
  edge [
    source 20
    target 996
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 20
    target 997
  ]
  edge [
    source 20
    target 998
  ]
  edge [
    source 20
    target 999
  ]
  edge [
    source 20
    target 1000
  ]
  edge [
    source 20
    target 1001
  ]
  edge [
    source 20
    target 1002
  ]
  edge [
    source 20
    target 1003
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 1004
  ]
  edge [
    source 20
    target 1005
  ]
  edge [
    source 20
    target 1006
  ]
  edge [
    source 20
    target 1007
  ]
  edge [
    source 20
    target 454
  ]
  edge [
    source 20
    target 1008
  ]
  edge [
    source 20
    target 549
  ]
  edge [
    source 20
    target 679
  ]
  edge [
    source 20
    target 1009
  ]
  edge [
    source 20
    target 1010
  ]
  edge [
    source 20
    target 1011
  ]
  edge [
    source 20
    target 1012
  ]
  edge [
    source 20
    target 1013
  ]
  edge [
    source 20
    target 859
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 1014
  ]
  edge [
    source 20
    target 1015
  ]
  edge [
    source 20
    target 1016
  ]
  edge [
    source 20
    target 1017
  ]
  edge [
    source 20
    target 1018
  ]
  edge [
    source 20
    target 1019
  ]
  edge [
    source 20
    target 1020
  ]
  edge [
    source 20
    target 1021
  ]
  edge [
    source 20
    target 1022
  ]
  edge [
    source 20
    target 1023
  ]
  edge [
    source 20
    target 1024
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 81
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 65
  ]
  edge [
    source 21
    target 165
  ]
  edge [
    source 21
    target 166
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 84
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 2147
  ]
  edge [
    source 25
    target 2148
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 2149
  ]
  edge [
    source 25
    target 2150
  ]
  edge [
    source 25
    target 2151
  ]
  edge [
    source 25
    target 2152
  ]
  edge [
    source 25
    target 2153
  ]
  edge [
    source 25
    target 2154
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 2155
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1029
  ]
  edge [
    source 27
    target 1030
  ]
  edge [
    source 27
    target 618
  ]
  edge [
    source 27
    target 252
  ]
  edge [
    source 27
    target 1031
  ]
  edge [
    source 27
    target 1032
  ]
  edge [
    source 27
    target 1033
  ]
  edge [
    source 27
    target 694
  ]
  edge [
    source 27
    target 1034
  ]
  edge [
    source 27
    target 1035
  ]
  edge [
    source 27
    target 1036
  ]
  edge [
    source 27
    target 1037
  ]
  edge [
    source 27
    target 1038
  ]
  edge [
    source 27
    target 1039
  ]
  edge [
    source 27
    target 1040
  ]
  edge [
    source 27
    target 1041
  ]
  edge [
    source 27
    target 602
  ]
  edge [
    source 27
    target 1042
  ]
  edge [
    source 27
    target 1043
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 1044
  ]
  edge [
    source 27
    target 1045
  ]
  edge [
    source 27
    target 1046
  ]
  edge [
    source 27
    target 1047
  ]
  edge [
    source 27
    target 1048
  ]
  edge [
    source 27
    target 1049
  ]
  edge [
    source 27
    target 1050
  ]
  edge [
    source 27
    target 1051
  ]
  edge [
    source 27
    target 1052
  ]
  edge [
    source 27
    target 1053
  ]
  edge [
    source 27
    target 730
  ]
  edge [
    source 27
    target 1054
  ]
  edge [
    source 27
    target 1055
  ]
  edge [
    source 27
    target 1056
  ]
  edge [
    source 27
    target 1057
  ]
  edge [
    source 27
    target 1058
  ]
  edge [
    source 27
    target 1059
  ]
  edge [
    source 27
    target 1060
  ]
  edge [
    source 27
    target 1061
  ]
  edge [
    source 27
    target 1062
  ]
  edge [
    source 27
    target 1063
  ]
  edge [
    source 27
    target 1064
  ]
  edge [
    source 27
    target 1065
  ]
  edge [
    source 27
    target 1066
  ]
  edge [
    source 27
    target 924
  ]
  edge [
    source 27
    target 1067
  ]
  edge [
    source 27
    target 1068
  ]
  edge [
    source 27
    target 1069
  ]
  edge [
    source 27
    target 1070
  ]
  edge [
    source 27
    target 1071
  ]
  edge [
    source 27
    target 1072
  ]
  edge [
    source 27
    target 855
  ]
  edge [
    source 27
    target 1073
  ]
  edge [
    source 27
    target 1074
  ]
  edge [
    source 27
    target 1075
  ]
  edge [
    source 27
    target 1076
  ]
  edge [
    source 27
    target 1077
  ]
  edge [
    source 27
    target 1078
  ]
  edge [
    source 27
    target 1079
  ]
  edge [
    source 27
    target 1080
  ]
  edge [
    source 27
    target 1081
  ]
  edge [
    source 27
    target 1082
  ]
  edge [
    source 27
    target 1083
  ]
  edge [
    source 27
    target 1084
  ]
  edge [
    source 27
    target 1085
  ]
  edge [
    source 27
    target 1086
  ]
  edge [
    source 27
    target 57
  ]
  edge [
    source 27
    target 1087
  ]
  edge [
    source 27
    target 1088
  ]
  edge [
    source 27
    target 1089
  ]
  edge [
    source 27
    target 2147
  ]
  edge [
    source 27
    target 2148
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 2149
  ]
  edge [
    source 27
    target 2150
  ]
  edge [
    source 27
    target 2151
  ]
  edge [
    source 27
    target 2152
  ]
  edge [
    source 27
    target 2153
  ]
  edge [
    source 27
    target 2154
  ]
  edge [
    source 27
    target 2155
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1090
  ]
  edge [
    source 29
    target 1091
  ]
  edge [
    source 29
    target 1092
  ]
  edge [
    source 29
    target 776
  ]
  edge [
    source 29
    target 423
  ]
  edge [
    source 29
    target 1093
  ]
  edge [
    source 29
    target 1094
  ]
  edge [
    source 29
    target 1095
  ]
  edge [
    source 29
    target 1096
  ]
  edge [
    source 29
    target 1097
  ]
  edge [
    source 29
    target 1098
  ]
  edge [
    source 29
    target 949
  ]
  edge [
    source 29
    target 1099
  ]
  edge [
    source 29
    target 1100
  ]
  edge [
    source 29
    target 1101
  ]
  edge [
    source 29
    target 1102
  ]
  edge [
    source 29
    target 1103
  ]
  edge [
    source 29
    target 1104
  ]
  edge [
    source 29
    target 1105
  ]
  edge [
    source 29
    target 1106
  ]
  edge [
    source 29
    target 1107
  ]
  edge [
    source 29
    target 1108
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 1109
  ]
  edge [
    source 29
    target 1110
  ]
  edge [
    source 29
    target 1111
  ]
  edge [
    source 29
    target 1112
  ]
  edge [
    source 29
    target 1113
  ]
  edge [
    source 29
    target 1114
  ]
  edge [
    source 29
    target 1115
  ]
  edge [
    source 29
    target 1116
  ]
  edge [
    source 29
    target 1117
  ]
  edge [
    source 29
    target 1038
  ]
  edge [
    source 29
    target 1118
  ]
  edge [
    source 29
    target 1119
  ]
  edge [
    source 29
    target 857
  ]
  edge [
    source 29
    target 1120
  ]
  edge [
    source 29
    target 1121
  ]
  edge [
    source 29
    target 1122
  ]
  edge [
    source 29
    target 1123
  ]
  edge [
    source 29
    target 1124
  ]
  edge [
    source 29
    target 1125
  ]
  edge [
    source 29
    target 1126
  ]
  edge [
    source 29
    target 1127
  ]
  edge [
    source 29
    target 1128
  ]
  edge [
    source 29
    target 1129
  ]
  edge [
    source 29
    target 1130
  ]
  edge [
    source 29
    target 1131
  ]
  edge [
    source 29
    target 1132
  ]
  edge [
    source 29
    target 1133
  ]
  edge [
    source 29
    target 1134
  ]
  edge [
    source 29
    target 1135
  ]
  edge [
    source 29
    target 1136
  ]
  edge [
    source 29
    target 1137
  ]
  edge [
    source 29
    target 1138
  ]
  edge [
    source 29
    target 1139
  ]
  edge [
    source 29
    target 1140
  ]
  edge [
    source 29
    target 995
  ]
  edge [
    source 29
    target 1141
  ]
  edge [
    source 29
    target 1142
  ]
  edge [
    source 29
    target 1143
  ]
  edge [
    source 29
    target 1144
  ]
  edge [
    source 29
    target 1145
  ]
  edge [
    source 29
    target 1146
  ]
  edge [
    source 29
    target 1147
  ]
  edge [
    source 29
    target 1148
  ]
  edge [
    source 29
    target 1149
  ]
  edge [
    source 29
    target 1150
  ]
  edge [
    source 29
    target 1151
  ]
  edge [
    source 29
    target 1152
  ]
  edge [
    source 29
    target 1153
  ]
  edge [
    source 29
    target 1154
  ]
  edge [
    source 29
    target 1155
  ]
  edge [
    source 29
    target 1156
  ]
  edge [
    source 29
    target 1157
  ]
  edge [
    source 29
    target 1158
  ]
  edge [
    source 29
    target 1159
  ]
  edge [
    source 29
    target 1160
  ]
  edge [
    source 29
    target 1161
  ]
  edge [
    source 29
    target 1162
  ]
  edge [
    source 29
    target 1163
  ]
  edge [
    source 29
    target 1164
  ]
  edge [
    source 29
    target 1165
  ]
  edge [
    source 29
    target 263
  ]
  edge [
    source 29
    target 1166
  ]
  edge [
    source 29
    target 1167
  ]
  edge [
    source 29
    target 1168
  ]
  edge [
    source 29
    target 2147
  ]
  edge [
    source 29
    target 2148
  ]
  edge [
    source 29
    target 2149
  ]
  edge [
    source 29
    target 2150
  ]
  edge [
    source 29
    target 2151
  ]
  edge [
    source 29
    target 2152
  ]
  edge [
    source 29
    target 2153
  ]
  edge [
    source 29
    target 2154
  ]
  edge [
    source 29
    target 2155
  ]
  edge [
    source 29
    target 2156
  ]
  edge [
    source 29
    target 2157
  ]
  edge [
    source 29
    target 2158
  ]
  edge [
    source 29
    target 2159
  ]
  edge [
    source 29
    target 2160
  ]
  edge [
    source 29
    target 2161
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 1169
  ]
  edge [
    source 30
    target 1170
  ]
  edge [
    source 30
    target 1171
  ]
  edge [
    source 30
    target 591
  ]
  edge [
    source 30
    target 1172
  ]
  edge [
    source 30
    target 597
  ]
  edge [
    source 30
    target 1173
  ]
  edge [
    source 30
    target 1174
  ]
  edge [
    source 30
    target 1175
  ]
  edge [
    source 30
    target 1176
  ]
  edge [
    source 30
    target 1177
  ]
  edge [
    source 30
    target 1178
  ]
  edge [
    source 30
    target 1179
  ]
  edge [
    source 30
    target 1180
  ]
  edge [
    source 30
    target 752
  ]
  edge [
    source 30
    target 1181
  ]
  edge [
    source 30
    target 1182
  ]
  edge [
    source 30
    target 1114
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 594
  ]
  edge [
    source 30
    target 595
  ]
  edge [
    source 30
    target 596
  ]
  edge [
    source 30
    target 1183
  ]
  edge [
    source 30
    target 1184
  ]
  edge [
    source 30
    target 394
  ]
  edge [
    source 30
    target 1185
  ]
  edge [
    source 30
    target 1186
  ]
  edge [
    source 30
    target 1187
  ]
  edge [
    source 30
    target 1188
  ]
  edge [
    source 30
    target 2156
  ]
  edge [
    source 30
    target 2157
  ]
  edge [
    source 30
    target 2151
  ]
  edge [
    source 30
    target 2152
  ]
  edge [
    source 30
    target 2158
  ]
  edge [
    source 30
    target 2159
  ]
  edge [
    source 30
    target 2160
  ]
  edge [
    source 30
    target 2161
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 1189
  ]
  edge [
    source 32
    target 1190
  ]
  edge [
    source 32
    target 553
  ]
  edge [
    source 32
    target 1191
  ]
  edge [
    source 32
    target 1192
  ]
  edge [
    source 32
    target 1193
  ]
  edge [
    source 32
    target 1194
  ]
  edge [
    source 32
    target 147
  ]
  edge [
    source 32
    target 1195
  ]
  edge [
    source 32
    target 1196
  ]
  edge [
    source 32
    target 1197
  ]
  edge [
    source 32
    target 1198
  ]
  edge [
    source 32
    target 1090
  ]
  edge [
    source 32
    target 1199
  ]
  edge [
    source 32
    target 1200
  ]
  edge [
    source 32
    target 1201
  ]
  edge [
    source 32
    target 1202
  ]
  edge [
    source 32
    target 1203
  ]
  edge [
    source 32
    target 1204
  ]
  edge [
    source 32
    target 1205
  ]
  edge [
    source 32
    target 1206
  ]
  edge [
    source 32
    target 1207
  ]
  edge [
    source 32
    target 1208
  ]
  edge [
    source 32
    target 647
  ]
  edge [
    source 32
    target 1209
  ]
  edge [
    source 32
    target 1210
  ]
  edge [
    source 32
    target 1211
  ]
  edge [
    source 32
    target 235
  ]
  edge [
    source 32
    target 736
  ]
  edge [
    source 32
    target 1212
  ]
  edge [
    source 32
    target 1213
  ]
  edge [
    source 32
    target 1214
  ]
  edge [
    source 32
    target 1215
  ]
  edge [
    source 32
    target 1216
  ]
  edge [
    source 32
    target 643
  ]
  edge [
    source 32
    target 1217
  ]
  edge [
    source 32
    target 645
  ]
  edge [
    source 32
    target 1218
  ]
  edge [
    source 32
    target 650
  ]
  edge [
    source 32
    target 1219
  ]
  edge [
    source 32
    target 1220
  ]
  edge [
    source 32
    target 1221
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 1222
  ]
  edge [
    source 32
    target 1223
  ]
  edge [
    source 32
    target 1224
  ]
  edge [
    source 32
    target 1225
  ]
  edge [
    source 32
    target 1226
  ]
  edge [
    source 32
    target 594
  ]
  edge [
    source 32
    target 1227
  ]
  edge [
    source 32
    target 1228
  ]
  edge [
    source 32
    target 1229
  ]
  edge [
    source 32
    target 1230
  ]
  edge [
    source 32
    target 1231
  ]
  edge [
    source 32
    target 1232
  ]
  edge [
    source 32
    target 806
  ]
  edge [
    source 32
    target 745
  ]
  edge [
    source 32
    target 1233
  ]
  edge [
    source 32
    target 1234
  ]
  edge [
    source 32
    target 1235
  ]
  edge [
    source 32
    target 1236
  ]
  edge [
    source 32
    target 1237
  ]
  edge [
    source 32
    target 1238
  ]
  edge [
    source 32
    target 1239
  ]
  edge [
    source 32
    target 1240
  ]
  edge [
    source 32
    target 1241
  ]
  edge [
    source 32
    target 1242
  ]
  edge [
    source 32
    target 593
  ]
  edge [
    source 32
    target 1243
  ]
  edge [
    source 32
    target 731
  ]
  edge [
    source 32
    target 1244
  ]
  edge [
    source 32
    target 1245
  ]
  edge [
    source 32
    target 1246
  ]
  edge [
    source 32
    target 1247
  ]
  edge [
    source 32
    target 746
  ]
  edge [
    source 32
    target 788
  ]
  edge [
    source 32
    target 789
  ]
  edge [
    source 32
    target 790
  ]
  edge [
    source 32
    target 791
  ]
  edge [
    source 32
    target 792
  ]
  edge [
    source 32
    target 793
  ]
  edge [
    source 32
    target 794
  ]
  edge [
    source 32
    target 795
  ]
  edge [
    source 32
    target 84
  ]
  edge [
    source 32
    target 796
  ]
  edge [
    source 32
    target 797
  ]
  edge [
    source 32
    target 798
  ]
  edge [
    source 32
    target 1248
  ]
  edge [
    source 32
    target 733
  ]
  edge [
    source 32
    target 1249
  ]
  edge [
    source 32
    target 1250
  ]
  edge [
    source 32
    target 1251
  ]
  edge [
    source 32
    target 1252
  ]
  edge [
    source 32
    target 1253
  ]
  edge [
    source 32
    target 1254
  ]
  edge [
    source 32
    target 1255
  ]
  edge [
    source 32
    target 1256
  ]
  edge [
    source 32
    target 1257
  ]
  edge [
    source 32
    target 1258
  ]
  edge [
    source 32
    target 1259
  ]
  edge [
    source 32
    target 1260
  ]
  edge [
    source 32
    target 1261
  ]
  edge [
    source 32
    target 1262
  ]
  edge [
    source 32
    target 597
  ]
  edge [
    source 32
    target 1263
  ]
  edge [
    source 32
    target 1264
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 701
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 32
    target 331
  ]
  edge [
    source 32
    target 1297
  ]
  edge [
    source 32
    target 1298
  ]
  edge [
    source 32
    target 1299
  ]
  edge [
    source 32
    target 1300
  ]
  edge [
    source 32
    target 1301
  ]
  edge [
    source 32
    target 1302
  ]
  edge [
    source 32
    target 1303
  ]
  edge [
    source 32
    target 1304
  ]
  edge [
    source 32
    target 820
  ]
  edge [
    source 32
    target 1305
  ]
  edge [
    source 32
    target 693
  ]
  edge [
    source 32
    target 1306
  ]
  edge [
    source 32
    target 1307
  ]
  edge [
    source 32
    target 1308
  ]
  edge [
    source 32
    target 1309
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 1310
  ]
  edge [
    source 32
    target 1311
  ]
  edge [
    source 32
    target 1312
  ]
  edge [
    source 32
    target 765
  ]
  edge [
    source 32
    target 1313
  ]
  edge [
    source 32
    target 1314
  ]
  edge [
    source 32
    target 205
  ]
  edge [
    source 32
    target 1315
  ]
  edge [
    source 32
    target 1316
  ]
  edge [
    source 32
    target 1317
  ]
  edge [
    source 32
    target 1318
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 1324
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 1325
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 32
    target 306
  ]
  edge [
    source 32
    target 829
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1328
  ]
  edge [
    source 32
    target 1329
  ]
  edge [
    source 32
    target 1330
  ]
  edge [
    source 32
    target 1331
  ]
  edge [
    source 32
    target 1332
  ]
  edge [
    source 32
    target 1333
  ]
  edge [
    source 32
    target 1334
  ]
  edge [
    source 32
    target 1335
  ]
  edge [
    source 32
    target 1336
  ]
  edge [
    source 32
    target 1098
  ]
  edge [
    source 32
    target 949
  ]
  edge [
    source 32
    target 1099
  ]
  edge [
    source 32
    target 1100
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 1102
  ]
  edge [
    source 32
    target 1103
  ]
  edge [
    source 32
    target 1104
  ]
  edge [
    source 32
    target 1105
  ]
  edge [
    source 32
    target 1106
  ]
  edge [
    source 32
    target 1107
  ]
  edge [
    source 32
    target 1108
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 1337
  ]
  edge [
    source 32
    target 1338
  ]
  edge [
    source 32
    target 459
  ]
  edge [
    source 32
    target 1339
  ]
  edge [
    source 32
    target 1340
  ]
  edge [
    source 32
    target 1341
  ]
  edge [
    source 32
    target 1342
  ]
  edge [
    source 32
    target 1343
  ]
  edge [
    source 32
    target 1344
  ]
  edge [
    source 32
    target 1345
  ]
  edge [
    source 32
    target 1346
  ]
  edge [
    source 32
    target 1347
  ]
  edge [
    source 32
    target 1348
  ]
  edge [
    source 32
    target 1349
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1029
  ]
  edge [
    source 33
    target 1030
  ]
  edge [
    source 33
    target 618
  ]
  edge [
    source 33
    target 252
  ]
  edge [
    source 33
    target 1031
  ]
  edge [
    source 33
    target 1032
  ]
  edge [
    source 33
    target 1033
  ]
  edge [
    source 33
    target 694
  ]
  edge [
    source 33
    target 1034
  ]
  edge [
    source 33
    target 1035
  ]
  edge [
    source 33
    target 1036
  ]
  edge [
    source 33
    target 1037
  ]
  edge [
    source 33
    target 1038
  ]
  edge [
    source 33
    target 1039
  ]
  edge [
    source 33
    target 1040
  ]
  edge [
    source 33
    target 1041
  ]
  edge [
    source 33
    target 602
  ]
  edge [
    source 33
    target 1042
  ]
  edge [
    source 33
    target 1043
  ]
  edge [
    source 33
    target 1044
  ]
  edge [
    source 33
    target 1045
  ]
  edge [
    source 33
    target 1046
  ]
  edge [
    source 33
    target 1047
  ]
  edge [
    source 33
    target 1048
  ]
  edge [
    source 33
    target 1049
  ]
  edge [
    source 33
    target 1050
  ]
  edge [
    source 33
    target 1051
  ]
  edge [
    source 33
    target 1052
  ]
  edge [
    source 33
    target 1053
  ]
  edge [
    source 33
    target 730
  ]
  edge [
    source 33
    target 1054
  ]
  edge [
    source 33
    target 1055
  ]
  edge [
    source 33
    target 1056
  ]
  edge [
    source 33
    target 1057
  ]
  edge [
    source 33
    target 1058
  ]
  edge [
    source 33
    target 1059
  ]
  edge [
    source 33
    target 1060
  ]
  edge [
    source 33
    target 1061
  ]
  edge [
    source 33
    target 1062
  ]
  edge [
    source 33
    target 1063
  ]
  edge [
    source 33
    target 1064
  ]
  edge [
    source 33
    target 1065
  ]
  edge [
    source 33
    target 1066
  ]
  edge [
    source 33
    target 924
  ]
  edge [
    source 33
    target 1067
  ]
  edge [
    source 33
    target 1068
  ]
  edge [
    source 33
    target 1069
  ]
  edge [
    source 33
    target 1070
  ]
  edge [
    source 33
    target 1071
  ]
  edge [
    source 33
    target 1072
  ]
  edge [
    source 33
    target 855
  ]
  edge [
    source 33
    target 1073
  ]
  edge [
    source 33
    target 1074
  ]
  edge [
    source 33
    target 1075
  ]
  edge [
    source 33
    target 1076
  ]
  edge [
    source 33
    target 1077
  ]
  edge [
    source 33
    target 1078
  ]
  edge [
    source 33
    target 1079
  ]
  edge [
    source 33
    target 1080
  ]
  edge [
    source 33
    target 1081
  ]
  edge [
    source 33
    target 1082
  ]
  edge [
    source 33
    target 1083
  ]
  edge [
    source 33
    target 1084
  ]
  edge [
    source 33
    target 1085
  ]
  edge [
    source 33
    target 1086
  ]
  edge [
    source 33
    target 57
  ]
  edge [
    source 33
    target 1087
  ]
  edge [
    source 33
    target 1088
  ]
  edge [
    source 33
    target 1089
  ]
  edge [
    source 34
    target 1350
  ]
  edge [
    source 34
    target 1351
  ]
  edge [
    source 34
    target 398
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 47
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 35
    target 1352
  ]
  edge [
    source 35
    target 1353
  ]
  edge [
    source 35
    target 1354
  ]
  edge [
    source 35
    target 1355
  ]
  edge [
    source 35
    target 1356
  ]
  edge [
    source 35
    target 1202
  ]
  edge [
    source 35
    target 1357
  ]
  edge [
    source 35
    target 1358
  ]
  edge [
    source 35
    target 1359
  ]
  edge [
    source 35
    target 1360
  ]
  edge [
    source 35
    target 1361
  ]
  edge [
    source 35
    target 594
  ]
  edge [
    source 35
    target 1362
  ]
  edge [
    source 35
    target 1363
  ]
  edge [
    source 35
    target 597
  ]
  edge [
    source 35
    target 1364
  ]
  edge [
    source 35
    target 1365
  ]
  edge [
    source 35
    target 573
  ]
  edge [
    source 35
    target 1366
  ]
  edge [
    source 35
    target 1367
  ]
  edge [
    source 35
    target 898
  ]
  edge [
    source 35
    target 730
  ]
  edge [
    source 35
    target 1368
  ]
  edge [
    source 35
    target 1369
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 1204
  ]
  edge [
    source 35
    target 1370
  ]
  edge [
    source 35
    target 1371
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 1269
  ]
  edge [
    source 35
    target 1270
  ]
  edge [
    source 35
    target 1271
  ]
  edge [
    source 35
    target 1272
  ]
  edge [
    source 35
    target 1273
  ]
  edge [
    source 35
    target 1274
  ]
  edge [
    source 35
    target 1275
  ]
  edge [
    source 35
    target 1276
  ]
  edge [
    source 35
    target 1372
  ]
  edge [
    source 35
    target 1005
  ]
  edge [
    source 35
    target 887
  ]
  edge [
    source 35
    target 1373
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 1374
  ]
  edge [
    source 35
    target 331
  ]
  edge [
    source 35
    target 1375
  ]
  edge [
    source 35
    target 1376
  ]
  edge [
    source 35
    target 1377
  ]
  edge [
    source 35
    target 176
  ]
  edge [
    source 35
    target 1378
  ]
  edge [
    source 35
    target 549
  ]
  edge [
    source 35
    target 1379
  ]
  edge [
    source 35
    target 180
  ]
  edge [
    source 35
    target 1380
  ]
  edge [
    source 35
    target 1381
  ]
  edge [
    source 35
    target 1382
  ]
  edge [
    source 35
    target 1383
  ]
  edge [
    source 35
    target 1384
  ]
  edge [
    source 35
    target 1385
  ]
  edge [
    source 35
    target 1386
  ]
  edge [
    source 35
    target 1387
  ]
  edge [
    source 35
    target 1388
  ]
  edge [
    source 35
    target 1389
  ]
  edge [
    source 35
    target 1390
  ]
  edge [
    source 35
    target 1391
  ]
  edge [
    source 35
    target 1392
  ]
  edge [
    source 35
    target 1393
  ]
  edge [
    source 35
    target 1394
  ]
  edge [
    source 35
    target 1395
  ]
  edge [
    source 35
    target 1396
  ]
  edge [
    source 35
    target 1397
  ]
  edge [
    source 35
    target 1398
  ]
  edge [
    source 35
    target 1399
  ]
  edge [
    source 35
    target 312
  ]
  edge [
    source 35
    target 1400
  ]
  edge [
    source 35
    target 826
  ]
  edge [
    source 35
    target 1401
  ]
  edge [
    source 35
    target 1305
  ]
  edge [
    source 35
    target 1402
  ]
  edge [
    source 35
    target 1403
  ]
  edge [
    source 35
    target 1404
  ]
  edge [
    source 35
    target 1405
  ]
  edge [
    source 35
    target 1406
  ]
  edge [
    source 35
    target 1407
  ]
  edge [
    source 35
    target 1408
  ]
  edge [
    source 35
    target 1409
  ]
  edge [
    source 35
    target 1410
  ]
  edge [
    source 35
    target 1411
  ]
  edge [
    source 35
    target 1412
  ]
  edge [
    source 35
    target 1413
  ]
  edge [
    source 35
    target 1414
  ]
  edge [
    source 35
    target 1415
  ]
  edge [
    source 35
    target 395
  ]
  edge [
    source 35
    target 1416
  ]
  edge [
    source 35
    target 1417
  ]
  edge [
    source 35
    target 1418
  ]
  edge [
    source 35
    target 1419
  ]
  edge [
    source 35
    target 1420
  ]
  edge [
    source 35
    target 1421
  ]
  edge [
    source 35
    target 1422
  ]
  edge [
    source 35
    target 1423
  ]
  edge [
    source 35
    target 1424
  ]
  edge [
    source 35
    target 1425
  ]
  edge [
    source 35
    target 1426
  ]
  edge [
    source 35
    target 517
  ]
  edge [
    source 35
    target 1427
  ]
  edge [
    source 35
    target 1428
  ]
  edge [
    source 35
    target 1429
  ]
  edge [
    source 35
    target 184
  ]
  edge [
    source 35
    target 1430
  ]
  edge [
    source 35
    target 1431
  ]
  edge [
    source 35
    target 1432
  ]
  edge [
    source 35
    target 1433
  ]
  edge [
    source 35
    target 1434
  ]
  edge [
    source 35
    target 1435
  ]
  edge [
    source 35
    target 1436
  ]
  edge [
    source 35
    target 317
  ]
  edge [
    source 35
    target 1437
  ]
  edge [
    source 35
    target 189
  ]
  edge [
    source 35
    target 225
  ]
  edge [
    source 35
    target 1438
  ]
  edge [
    source 35
    target 1439
  ]
  edge [
    source 35
    target 1440
  ]
  edge [
    source 35
    target 1441
  ]
  edge [
    source 35
    target 1442
  ]
  edge [
    source 35
    target 1443
  ]
  edge [
    source 35
    target 1444
  ]
  edge [
    source 35
    target 1445
  ]
  edge [
    source 35
    target 1446
  ]
  edge [
    source 35
    target 1447
  ]
  edge [
    source 35
    target 1448
  ]
  edge [
    source 35
    target 1449
  ]
  edge [
    source 35
    target 1450
  ]
  edge [
    source 35
    target 1451
  ]
  edge [
    source 35
    target 1452
  ]
  edge [
    source 35
    target 1453
  ]
  edge [
    source 35
    target 1454
  ]
  edge [
    source 35
    target 1455
  ]
  edge [
    source 35
    target 1456
  ]
  edge [
    source 35
    target 1457
  ]
  edge [
    source 35
    target 222
  ]
  edge [
    source 35
    target 1458
  ]
  edge [
    source 35
    target 1459
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1460
  ]
  edge [
    source 36
    target 1461
  ]
  edge [
    source 36
    target 1462
  ]
  edge [
    source 36
    target 1463
  ]
  edge [
    source 36
    target 1464
  ]
  edge [
    source 36
    target 1465
  ]
  edge [
    source 36
    target 1466
  ]
  edge [
    source 36
    target 1467
  ]
  edge [
    source 36
    target 1468
  ]
  edge [
    source 36
    target 1469
  ]
  edge [
    source 36
    target 1470
  ]
  edge [
    source 37
    target 1471
  ]
  edge [
    source 37
    target 1472
  ]
  edge [
    source 37
    target 1473
  ]
  edge [
    source 37
    target 1474
  ]
  edge [
    source 37
    target 1475
  ]
  edge [
    source 37
    target 1254
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1476
  ]
  edge [
    source 39
    target 1477
  ]
  edge [
    source 39
    target 1478
  ]
  edge [
    source 39
    target 1479
  ]
  edge [
    source 39
    target 1480
  ]
  edge [
    source 39
    target 1481
  ]
  edge [
    source 39
    target 1482
  ]
  edge [
    source 39
    target 1483
  ]
  edge [
    source 39
    target 1484
  ]
  edge [
    source 39
    target 1485
  ]
  edge [
    source 39
    target 1486
  ]
  edge [
    source 39
    target 1487
  ]
  edge [
    source 39
    target 1488
  ]
  edge [
    source 39
    target 1467
  ]
  edge [
    source 39
    target 1489
  ]
  edge [
    source 39
    target 1490
  ]
  edge [
    source 39
    target 1491
  ]
  edge [
    source 39
    target 1492
  ]
  edge [
    source 39
    target 1493
  ]
  edge [
    source 39
    target 1494
  ]
  edge [
    source 39
    target 1495
  ]
  edge [
    source 39
    target 1496
  ]
  edge [
    source 39
    target 1497
  ]
  edge [
    source 39
    target 1498
  ]
  edge [
    source 39
    target 1499
  ]
  edge [
    source 39
    target 1500
  ]
  edge [
    source 39
    target 1501
  ]
  edge [
    source 39
    target 1502
  ]
  edge [
    source 39
    target 1503
  ]
  edge [
    source 39
    target 1504
  ]
  edge [
    source 39
    target 1505
  ]
  edge [
    source 39
    target 1506
  ]
  edge [
    source 39
    target 1507
  ]
  edge [
    source 39
    target 1508
  ]
  edge [
    source 39
    target 1509
  ]
  edge [
    source 39
    target 1510
  ]
  edge [
    source 39
    target 1511
  ]
  edge [
    source 39
    target 1512
  ]
  edge [
    source 39
    target 1513
  ]
  edge [
    source 39
    target 1514
  ]
  edge [
    source 39
    target 1515
  ]
  edge [
    source 39
    target 1516
  ]
  edge [
    source 39
    target 1517
  ]
  edge [
    source 39
    target 1518
  ]
  edge [
    source 39
    target 1519
  ]
  edge [
    source 39
    target 1520
  ]
  edge [
    source 39
    target 1521
  ]
  edge [
    source 39
    target 1522
  ]
  edge [
    source 39
    target 1523
  ]
  edge [
    source 39
    target 1524
  ]
  edge [
    source 39
    target 1525
  ]
  edge [
    source 39
    target 1526
  ]
  edge [
    source 39
    target 1527
  ]
  edge [
    source 39
    target 1528
  ]
  edge [
    source 39
    target 1529
  ]
  edge [
    source 39
    target 1530
  ]
  edge [
    source 39
    target 52
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1531
  ]
  edge [
    source 40
    target 406
  ]
  edge [
    source 40
    target 369
  ]
  edge [
    source 40
    target 1532
  ]
  edge [
    source 40
    target 1533
  ]
  edge [
    source 40
    target 1534
  ]
  edge [
    source 40
    target 671
  ]
  edge [
    source 40
    target 676
  ]
  edge [
    source 40
    target 1535
  ]
  edge [
    source 40
    target 1536
  ]
  edge [
    source 40
    target 1537
  ]
  edge [
    source 40
    target 1538
  ]
  edge [
    source 40
    target 1539
  ]
  edge [
    source 40
    target 1540
  ]
  edge [
    source 40
    target 1541
  ]
  edge [
    source 40
    target 1542
  ]
  edge [
    source 40
    target 1543
  ]
  edge [
    source 40
    target 1544
  ]
  edge [
    source 40
    target 1545
  ]
  edge [
    source 40
    target 1546
  ]
  edge [
    source 40
    target 1547
  ]
  edge [
    source 40
    target 1548
  ]
  edge [
    source 40
    target 1549
  ]
  edge [
    source 40
    target 1550
  ]
  edge [
    source 40
    target 1551
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1552
  ]
  edge [
    source 42
    target 1553
  ]
  edge [
    source 42
    target 111
  ]
  edge [
    source 42
    target 1554
  ]
  edge [
    source 42
    target 1555
  ]
  edge [
    source 42
    target 1556
  ]
  edge [
    source 42
    target 84
  ]
  edge [
    source 42
    target 597
  ]
  edge [
    source 42
    target 1557
  ]
  edge [
    source 42
    target 73
  ]
  edge [
    source 42
    target 164
  ]
  edge [
    source 42
    target 1558
  ]
  edge [
    source 42
    target 1559
  ]
  edge [
    source 42
    target 1560
  ]
  edge [
    source 42
    target 1561
  ]
  edge [
    source 42
    target 1562
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1563
  ]
  edge [
    source 44
    target 1564
  ]
  edge [
    source 44
    target 1565
  ]
  edge [
    source 44
    target 1566
  ]
  edge [
    source 44
    target 1502
  ]
  edge [
    source 44
    target 1501
  ]
  edge [
    source 44
    target 785
  ]
  edge [
    source 44
    target 1567
  ]
  edge [
    source 44
    target 1568
  ]
  edge [
    source 44
    target 1569
  ]
  edge [
    source 44
    target 1570
  ]
  edge [
    source 44
    target 1571
  ]
  edge [
    source 44
    target 1572
  ]
  edge [
    source 44
    target 1573
  ]
  edge [
    source 44
    target 1493
  ]
  edge [
    source 44
    target 1574
  ]
  edge [
    source 44
    target 1575
  ]
  edge [
    source 44
    target 1479
  ]
  edge [
    source 44
    target 1576
  ]
  edge [
    source 44
    target 1577
  ]
  edge [
    source 44
    target 1578
  ]
  edge [
    source 44
    target 1579
  ]
  edge [
    source 44
    target 1580
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 45
    target 1581
  ]
  edge [
    source 45
    target 1582
  ]
  edge [
    source 45
    target 1583
  ]
  edge [
    source 45
    target 1584
  ]
  edge [
    source 45
    target 1585
  ]
  edge [
    source 45
    target 1586
  ]
  edge [
    source 45
    target 1587
  ]
  edge [
    source 45
    target 1588
  ]
  edge [
    source 45
    target 1589
  ]
  edge [
    source 45
    target 1590
  ]
  edge [
    source 45
    target 1591
  ]
  edge [
    source 45
    target 1592
  ]
  edge [
    source 45
    target 1593
  ]
  edge [
    source 45
    target 1594
  ]
  edge [
    source 45
    target 1595
  ]
  edge [
    source 45
    target 1596
  ]
  edge [
    source 45
    target 1597
  ]
  edge [
    source 45
    target 1598
  ]
  edge [
    source 45
    target 1599
  ]
  edge [
    source 45
    target 1600
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 1601
  ]
  edge [
    source 45
    target 1487
  ]
  edge [
    source 45
    target 1602
  ]
  edge [
    source 45
    target 1603
  ]
  edge [
    source 45
    target 1604
  ]
  edge [
    source 45
    target 751
  ]
  edge [
    source 45
    target 1605
  ]
  edge [
    source 45
    target 1606
  ]
  edge [
    source 45
    target 1607
  ]
  edge [
    source 45
    target 1608
  ]
  edge [
    source 45
    target 1609
  ]
  edge [
    source 45
    target 1467
  ]
  edge [
    source 45
    target 1610
  ]
  edge [
    source 45
    target 1611
  ]
  edge [
    source 45
    target 1612
  ]
  edge [
    source 45
    target 1613
  ]
  edge [
    source 45
    target 1614
  ]
  edge [
    source 45
    target 1271
  ]
  edge [
    source 45
    target 1615
  ]
  edge [
    source 45
    target 1511
  ]
  edge [
    source 45
    target 1616
  ]
  edge [
    source 45
    target 1617
  ]
  edge [
    source 45
    target 1398
  ]
  edge [
    source 45
    target 1618
  ]
  edge [
    source 45
    target 1619
  ]
  edge [
    source 45
    target 1620
  ]
  edge [
    source 45
    target 1621
  ]
  edge [
    source 45
    target 1622
  ]
  edge [
    source 45
    target 1623
  ]
  edge [
    source 45
    target 1624
  ]
  edge [
    source 45
    target 1625
  ]
  edge [
    source 45
    target 1626
  ]
  edge [
    source 45
    target 1627
  ]
  edge [
    source 45
    target 1628
  ]
  edge [
    source 45
    target 1629
  ]
  edge [
    source 45
    target 1630
  ]
  edge [
    source 45
    target 1631
  ]
  edge [
    source 45
    target 1632
  ]
  edge [
    source 45
    target 1572
  ]
  edge [
    source 45
    target 1633
  ]
  edge [
    source 45
    target 1634
  ]
  edge [
    source 45
    target 806
  ]
  edge [
    source 45
    target 1635
  ]
  edge [
    source 45
    target 1636
  ]
  edge [
    source 45
    target 1637
  ]
  edge [
    source 45
    target 1638
  ]
  edge [
    source 45
    target 1639
  ]
  edge [
    source 45
    target 1640
  ]
  edge [
    source 45
    target 1641
  ]
  edge [
    source 45
    target 1642
  ]
  edge [
    source 45
    target 1643
  ]
  edge [
    source 45
    target 1644
  ]
  edge [
    source 45
    target 1645
  ]
  edge [
    source 45
    target 1646
  ]
  edge [
    source 45
    target 1647
  ]
  edge [
    source 45
    target 1648
  ]
  edge [
    source 45
    target 1649
  ]
  edge [
    source 45
    target 1650
  ]
  edge [
    source 45
    target 1651
  ]
  edge [
    source 45
    target 235
  ]
  edge [
    source 45
    target 1652
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1653
  ]
  edge [
    source 46
    target 1654
  ]
  edge [
    source 46
    target 1655
  ]
  edge [
    source 46
    target 1656
  ]
  edge [
    source 47
    target 1657
  ]
  edge [
    source 47
    target 1658
  ]
  edge [
    source 47
    target 1659
  ]
  edge [
    source 47
    target 1660
  ]
  edge [
    source 47
    target 1661
  ]
  edge [
    source 47
    target 287
  ]
  edge [
    source 47
    target 1662
  ]
  edge [
    source 47
    target 1663
  ]
  edge [
    source 47
    target 1664
  ]
  edge [
    source 47
    target 1366
  ]
  edge [
    source 47
    target 1665
  ]
  edge [
    source 47
    target 1666
  ]
  edge [
    source 47
    target 882
  ]
  edge [
    source 47
    target 1667
  ]
  edge [
    source 47
    target 1668
  ]
  edge [
    source 47
    target 271
  ]
  edge [
    source 47
    target 1669
  ]
  edge [
    source 47
    target 1670
  ]
  edge [
    source 47
    target 1671
  ]
  edge [
    source 47
    target 1672
  ]
  edge [
    source 47
    target 1673
  ]
  edge [
    source 47
    target 1674
  ]
  edge [
    source 47
    target 1675
  ]
  edge [
    source 47
    target 1676
  ]
  edge [
    source 47
    target 1677
  ]
  edge [
    source 47
    target 999
  ]
  edge [
    source 47
    target 294
  ]
  edge [
    source 47
    target 1678
  ]
  edge [
    source 47
    target 753
  ]
  edge [
    source 47
    target 1679
  ]
  edge [
    source 47
    target 1680
  ]
  edge [
    source 47
    target 1681
  ]
  edge [
    source 47
    target 1682
  ]
  edge [
    source 47
    target 1683
  ]
  edge [
    source 47
    target 1684
  ]
  edge [
    source 47
    target 1685
  ]
  edge [
    source 47
    target 1686
  ]
  edge [
    source 47
    target 1687
  ]
  edge [
    source 47
    target 1688
  ]
  edge [
    source 47
    target 1689
  ]
  edge [
    source 47
    target 1690
  ]
  edge [
    source 47
    target 1691
  ]
  edge [
    source 47
    target 153
  ]
  edge [
    source 47
    target 965
  ]
  edge [
    source 47
    target 1692
  ]
  edge [
    source 47
    target 1693
  ]
  edge [
    source 47
    target 1694
  ]
  edge [
    source 47
    target 1695
  ]
  edge [
    source 47
    target 1696
  ]
  edge [
    source 47
    target 1697
  ]
  edge [
    source 47
    target 1698
  ]
  edge [
    source 47
    target 1699
  ]
  edge [
    source 47
    target 1700
  ]
  edge [
    source 47
    target 1701
  ]
  edge [
    source 47
    target 927
  ]
  edge [
    source 47
    target 1702
  ]
  edge [
    source 47
    target 1703
  ]
  edge [
    source 47
    target 1704
  ]
  edge [
    source 47
    target 1705
  ]
  edge [
    source 47
    target 1706
  ]
  edge [
    source 47
    target 1707
  ]
  edge [
    source 47
    target 1200
  ]
  edge [
    source 47
    target 859
  ]
  edge [
    source 47
    target 1708
  ]
  edge [
    source 47
    target 1709
  ]
  edge [
    source 47
    target 1710
  ]
  edge [
    source 47
    target 1711
  ]
  edge [
    source 47
    target 673
  ]
  edge [
    source 47
    target 1712
  ]
  edge [
    source 47
    target 186
  ]
  edge [
    source 47
    target 1713
  ]
  edge [
    source 47
    target 1714
  ]
  edge [
    source 47
    target 1715
  ]
  edge [
    source 47
    target 1716
  ]
  edge [
    source 47
    target 1717
  ]
  edge [
    source 47
    target 1718
  ]
  edge [
    source 47
    target 1719
  ]
  edge [
    source 47
    target 1720
  ]
  edge [
    source 47
    target 1721
  ]
  edge [
    source 47
    target 1722
  ]
  edge [
    source 47
    target 1723
  ]
  edge [
    source 47
    target 1724
  ]
  edge [
    source 47
    target 1725
  ]
  edge [
    source 47
    target 1726
  ]
  edge [
    source 47
    target 826
  ]
  edge [
    source 47
    target 1727
  ]
  edge [
    source 47
    target 1728
  ]
  edge [
    source 47
    target 1729
  ]
  edge [
    source 47
    target 1730
  ]
  edge [
    source 47
    target 1731
  ]
  edge [
    source 47
    target 1732
  ]
  edge [
    source 47
    target 1733
  ]
  edge [
    source 47
    target 1734
  ]
  edge [
    source 47
    target 1735
  ]
  edge [
    source 47
    target 1736
  ]
  edge [
    source 47
    target 1737
  ]
  edge [
    source 47
    target 1738
  ]
  edge [
    source 47
    target 553
  ]
  edge [
    source 47
    target 517
  ]
  edge [
    source 47
    target 1739
  ]
  edge [
    source 47
    target 1740
  ]
  edge [
    source 47
    target 1192
  ]
  edge [
    source 47
    target 1741
  ]
  edge [
    source 47
    target 1742
  ]
  edge [
    source 47
    target 1057
  ]
  edge [
    source 47
    target 1743
  ]
  edge [
    source 47
    target 1744
  ]
  edge [
    source 47
    target 1745
  ]
  edge [
    source 47
    target 1746
  ]
  edge [
    source 47
    target 1747
  ]
  edge [
    source 47
    target 1229
  ]
  edge [
    source 47
    target 1453
  ]
  edge [
    source 47
    target 1748
  ]
  edge [
    source 47
    target 1749
  ]
  edge [
    source 47
    target 806
  ]
  edge [
    source 47
    target 1100
  ]
  edge [
    source 47
    target 315
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1750
  ]
  edge [
    source 48
    target 1603
  ]
  edge [
    source 48
    target 1751
  ]
  edge [
    source 48
    target 1585
  ]
  edge [
    source 48
    target 1582
  ]
  edge [
    source 48
    target 1752
  ]
  edge [
    source 48
    target 1753
  ]
  edge [
    source 48
    target 1602
  ]
  edge [
    source 48
    target 1600
  ]
  edge [
    source 48
    target 1594
  ]
  edge [
    source 48
    target 1754
  ]
  edge [
    source 48
    target 1755
  ]
  edge [
    source 48
    target 1756
  ]
  edge [
    source 48
    target 1757
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1758
  ]
  edge [
    source 49
    target 1759
  ]
  edge [
    source 49
    target 1450
  ]
  edge [
    source 49
    target 1760
  ]
  edge [
    source 49
    target 1761
  ]
  edge [
    source 49
    target 1762
  ]
  edge [
    source 49
    target 1038
  ]
  edge [
    source 49
    target 1763
  ]
  edge [
    source 49
    target 1764
  ]
  edge [
    source 49
    target 1765
  ]
  edge [
    source 49
    target 1766
  ]
  edge [
    source 49
    target 1767
  ]
  edge [
    source 49
    target 1424
  ]
  edge [
    source 49
    target 1768
  ]
  edge [
    source 49
    target 1769
  ]
  edge [
    source 49
    target 1770
  ]
  edge [
    source 49
    target 1771
  ]
  edge [
    source 49
    target 230
  ]
  edge [
    source 49
    target 1772
  ]
  edge [
    source 49
    target 1773
  ]
  edge [
    source 49
    target 1774
  ]
  edge [
    source 49
    target 101
  ]
  edge [
    source 49
    target 1775
  ]
  edge [
    source 49
    target 1776
  ]
  edge [
    source 49
    target 619
  ]
  edge [
    source 49
    target 1777
  ]
  edge [
    source 49
    target 1243
  ]
  edge [
    source 49
    target 731
  ]
  edge [
    source 49
    target 1778
  ]
  edge [
    source 49
    target 1244
  ]
  edge [
    source 49
    target 1779
  ]
  edge [
    source 49
    target 1780
  ]
  edge [
    source 49
    target 1781
  ]
  edge [
    source 49
    target 1782
  ]
  edge [
    source 49
    target 1783
  ]
  edge [
    source 49
    target 1784
  ]
  edge [
    source 49
    target 1785
  ]
  edge [
    source 49
    target 1786
  ]
  edge [
    source 49
    target 1787
  ]
  edge [
    source 49
    target 1788
  ]
  edge [
    source 49
    target 1789
  ]
  edge [
    source 49
    target 1790
  ]
  edge [
    source 49
    target 1791
  ]
  edge [
    source 49
    target 1792
  ]
  edge [
    source 49
    target 1087
  ]
  edge [
    source 49
    target 1793
  ]
  edge [
    source 49
    target 1794
  ]
  edge [
    source 49
    target 1795
  ]
  edge [
    source 49
    target 313
  ]
  edge [
    source 49
    target 1796
  ]
  edge [
    source 49
    target 1797
  ]
  edge [
    source 49
    target 189
  ]
  edge [
    source 49
    target 270
  ]
  edge [
    source 49
    target 291
  ]
  edge [
    source 49
    target 1688
  ]
  edge [
    source 49
    target 730
  ]
  edge [
    source 49
    target 1798
  ]
  edge [
    source 49
    target 186
  ]
  edge [
    source 49
    target 296
  ]
  edge [
    source 49
    target 965
  ]
  edge [
    source 49
    target 294
  ]
  edge [
    source 49
    target 1799
  ]
  edge [
    source 49
    target 639
  ]
  edge [
    source 49
    target 1800
  ]
  edge [
    source 49
    target 1801
  ]
  edge [
    source 49
    target 1802
  ]
  edge [
    source 49
    target 924
  ]
  edge [
    source 49
    target 806
  ]
  edge [
    source 49
    target 104
  ]
  edge [
    source 49
    target 1803
  ]
  edge [
    source 49
    target 1448
  ]
  edge [
    source 49
    target 1432
  ]
  edge [
    source 49
    target 647
  ]
  edge [
    source 49
    target 1428
  ]
  edge [
    source 49
    target 1434
  ]
  edge [
    source 49
    target 1804
  ]
  edge [
    source 49
    target 1438
  ]
  edge [
    source 49
    target 1443
  ]
  edge [
    source 49
    target 1442
  ]
  edge [
    source 49
    target 153
  ]
  edge [
    source 49
    target 1447
  ]
  edge [
    source 49
    target 149
  ]
  edge [
    source 49
    target 1805
  ]
  edge [
    source 49
    target 1806
  ]
  edge [
    source 49
    target 1807
  ]
  edge [
    source 49
    target 1808
  ]
  edge [
    source 49
    target 1809
  ]
  edge [
    source 49
    target 597
  ]
  edge [
    source 49
    target 1810
  ]
  edge [
    source 49
    target 1811
  ]
  edge [
    source 49
    target 1365
  ]
  edge [
    source 49
    target 1812
  ]
  edge [
    source 49
    target 1813
  ]
  edge [
    source 49
    target 1814
  ]
  edge [
    source 49
    target 1815
  ]
  edge [
    source 49
    target 1816
  ]
  edge [
    source 49
    target 1817
  ]
  edge [
    source 49
    target 747
  ]
  edge [
    source 49
    target 590
  ]
  edge [
    source 49
    target 1818
  ]
  edge [
    source 49
    target 1819
  ]
  edge [
    source 49
    target 830
  ]
  edge [
    source 49
    target 1820
  ]
  edge [
    source 49
    target 1821
  ]
  edge [
    source 49
    target 1822
  ]
  edge [
    source 49
    target 618
  ]
  edge [
    source 49
    target 1823
  ]
  edge [
    source 49
    target 1070
  ]
  edge [
    source 49
    target 1824
  ]
  edge [
    source 49
    target 1825
  ]
  edge [
    source 49
    target 1826
  ]
  edge [
    source 49
    target 1827
  ]
  edge [
    source 49
    target 1828
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1829
  ]
  edge [
    source 50
    target 1830
  ]
  edge [
    source 50
    target 1510
  ]
  edge [
    source 50
    target 1504
  ]
  edge [
    source 50
    target 1831
  ]
  edge [
    source 50
    target 1832
  ]
  edge [
    source 50
    target 1833
  ]
  edge [
    source 50
    target 1493
  ]
  edge [
    source 50
    target 1574
  ]
  edge [
    source 50
    target 1834
  ]
  edge [
    source 50
    target 1835
  ]
  edge [
    source 50
    target 785
  ]
  edge [
    source 50
    target 1836
  ]
  edge [
    source 50
    target 1479
  ]
  edge [
    source 50
    target 1837
  ]
  edge [
    source 50
    target 1838
  ]
  edge [
    source 50
    target 1839
  ]
  edge [
    source 50
    target 1840
  ]
  edge [
    source 50
    target 1841
  ]
  edge [
    source 50
    target 1578
  ]
  edge [
    source 50
    target 1565
  ]
  edge [
    source 50
    target 1579
  ]
  edge [
    source 50
    target 1842
  ]
  edge [
    source 50
    target 1843
  ]
  edge [
    source 50
    target 1844
  ]
  edge [
    source 50
    target 1845
  ]
  edge [
    source 50
    target 1846
  ]
  edge [
    source 50
    target 1563
  ]
  edge [
    source 50
    target 1568
  ]
  edge [
    source 50
    target 1847
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1848
  ]
  edge [
    source 51
    target 1849
  ]
  edge [
    source 51
    target 1850
  ]
  edge [
    source 51
    target 1851
  ]
  edge [
    source 51
    target 1852
  ]
  edge [
    source 51
    target 1853
  ]
  edge [
    source 51
    target 1854
  ]
  edge [
    source 51
    target 1855
  ]
  edge [
    source 51
    target 1856
  ]
  edge [
    source 51
    target 1857
  ]
  edge [
    source 51
    target 1858
  ]
  edge [
    source 51
    target 1859
  ]
  edge [
    source 51
    target 1860
  ]
  edge [
    source 51
    target 1861
  ]
  edge [
    source 51
    target 1862
  ]
  edge [
    source 51
    target 1863
  ]
  edge [
    source 51
    target 1864
  ]
  edge [
    source 51
    target 651
  ]
  edge [
    source 51
    target 1865
  ]
  edge [
    source 51
    target 1866
  ]
  edge [
    source 51
    target 1867
  ]
  edge [
    source 51
    target 859
  ]
  edge [
    source 51
    target 1868
  ]
  edge [
    source 51
    target 1869
  ]
  edge [
    source 51
    target 1870
  ]
  edge [
    source 51
    target 1051
  ]
  edge [
    source 51
    target 1871
  ]
  edge [
    source 51
    target 1872
  ]
  edge [
    source 51
    target 731
  ]
  edge [
    source 51
    target 1873
  ]
  edge [
    source 51
    target 1874
  ]
  edge [
    source 51
    target 1875
  ]
  edge [
    source 51
    target 1876
  ]
  edge [
    source 51
    target 1877
  ]
  edge [
    source 51
    target 1878
  ]
  edge [
    source 51
    target 1879
  ]
  edge [
    source 51
    target 824
  ]
  edge [
    source 51
    target 1880
  ]
  edge [
    source 51
    target 1881
  ]
  edge [
    source 51
    target 1882
  ]
  edge [
    source 51
    target 1883
  ]
  edge [
    source 51
    target 1884
  ]
  edge [
    source 51
    target 1885
  ]
  edge [
    source 51
    target 755
  ]
  edge [
    source 51
    target 1886
  ]
  edge [
    source 51
    target 1887
  ]
  edge [
    source 51
    target 1888
  ]
  edge [
    source 51
    target 153
  ]
  edge [
    source 51
    target 268
  ]
  edge [
    source 51
    target 1366
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1889
  ]
  edge [
    source 52
    target 1890
  ]
  edge [
    source 52
    target 592
  ]
  edge [
    source 52
    target 1891
  ]
  edge [
    source 52
    target 1892
  ]
  edge [
    source 52
    target 1893
  ]
  edge [
    source 52
    target 1894
  ]
  edge [
    source 52
    target 1895
  ]
  edge [
    source 52
    target 1896
  ]
  edge [
    source 52
    target 1897
  ]
  edge [
    source 52
    target 287
  ]
  edge [
    source 52
    target 1898
  ]
  edge [
    source 52
    target 1803
  ]
  edge [
    source 52
    target 647
  ]
  edge [
    source 52
    target 806
  ]
  edge [
    source 52
    target 1100
  ]
  edge [
    source 52
    target 315
  ]
  edge [
    source 52
    target 1663
  ]
  edge [
    source 52
    target 1899
  ]
  edge [
    source 52
    target 1900
  ]
  edge [
    source 52
    target 1901
  ]
  edge [
    source 52
    target 1902
  ]
  edge [
    source 52
    target 598
  ]
  edge [
    source 52
    target 599
  ]
  edge [
    source 52
    target 590
  ]
  edge [
    source 52
    target 600
  ]
  edge [
    source 52
    target 601
  ]
  edge [
    source 52
    target 1903
  ]
  edge [
    source 52
    target 597
  ]
  edge [
    source 52
    target 736
  ]
  edge [
    source 52
    target 1450
  ]
  edge [
    source 52
    target 1904
  ]
  edge [
    source 52
    target 1905
  ]
  edge [
    source 52
    target 1906
  ]
  edge [
    source 52
    target 1907
  ]
  edge [
    source 52
    target 1908
  ]
  edge [
    source 52
    target 1217
  ]
  edge [
    source 52
    target 1909
  ]
  edge [
    source 52
    target 1910
  ]
  edge [
    source 52
    target 1911
  ]
  edge [
    source 52
    target 1912
  ]
  edge [
    source 52
    target 1913
  ]
  edge [
    source 52
    target 1914
  ]
  edge [
    source 52
    target 1915
  ]
  edge [
    source 52
    target 1740
  ]
  edge [
    source 52
    target 1916
  ]
  edge [
    source 52
    target 1917
  ]
  edge [
    source 52
    target 229
  ]
  edge [
    source 52
    target 1918
  ]
  edge [
    source 52
    target 731
  ]
  edge [
    source 52
    target 1919
  ]
  edge [
    source 52
    target 1920
  ]
  edge [
    source 52
    target 182
  ]
  edge [
    source 52
    target 1921
  ]
  edge [
    source 52
    target 1861
  ]
  edge [
    source 52
    target 1922
  ]
  edge [
    source 52
    target 1923
  ]
  edge [
    source 52
    target 235
  ]
  edge [
    source 52
    target 642
  ]
  edge [
    source 52
    target 1447
  ]
  edge [
    source 52
    target 541
  ]
  edge [
    source 52
    target 1924
  ]
  edge [
    source 52
    target 1925
  ]
  edge [
    source 52
    target 1926
  ]
  edge [
    source 52
    target 1927
  ]
  edge [
    source 52
    target 1716
  ]
  edge [
    source 52
    target 1928
  ]
  edge [
    source 52
    target 1929
  ]
  edge [
    source 52
    target 1930
  ]
  edge [
    source 52
    target 1931
  ]
  edge [
    source 52
    target 1932
  ]
  edge [
    source 52
    target 1933
  ]
  edge [
    source 52
    target 1934
  ]
  edge [
    source 52
    target 1935
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 1889
  ]
  edge [
    source 53
    target 1936
  ]
  edge [
    source 53
    target 1937
  ]
  edge [
    source 53
    target 882
  ]
  edge [
    source 53
    target 1938
  ]
  edge [
    source 53
    target 1939
  ]
  edge [
    source 53
    target 1940
  ]
  edge [
    source 53
    target 1045
  ]
  edge [
    source 53
    target 1941
  ]
  edge [
    source 53
    target 1917
  ]
  edge [
    source 53
    target 254
  ]
  edge [
    source 53
    target 752
  ]
  edge [
    source 53
    target 731
  ]
  edge [
    source 53
    target 1919
  ]
  edge [
    source 53
    target 1921
  ]
  edge [
    source 53
    target 1942
  ]
  edge [
    source 53
    target 1943
  ]
  edge [
    source 53
    target 642
  ]
  edge [
    source 53
    target 755
  ]
  edge [
    source 53
    target 1012
  ]
  edge [
    source 53
    target 1944
  ]
  edge [
    source 53
    target 1945
  ]
  edge [
    source 53
    target 1318
  ]
  edge [
    source 53
    target 1946
  ]
  edge [
    source 53
    target 1947
  ]
  edge [
    source 53
    target 833
  ]
  edge [
    source 53
    target 1948
  ]
  edge [
    source 53
    target 1925
  ]
  edge [
    source 53
    target 747
  ]
  edge [
    source 53
    target 748
  ]
  edge [
    source 53
    target 749
  ]
  edge [
    source 53
    target 750
  ]
  edge [
    source 53
    target 751
  ]
  edge [
    source 53
    target 753
  ]
  edge [
    source 53
    target 104
  ]
  edge [
    source 53
    target 754
  ]
  edge [
    source 53
    target 597
  ]
  edge [
    source 53
    target 153
  ]
  edge [
    source 53
    target 1949
  ]
  edge [
    source 53
    target 1950
  ]
  edge [
    source 53
    target 1951
  ]
  edge [
    source 53
    target 1952
  ]
  edge [
    source 53
    target 1305
  ]
  edge [
    source 53
    target 1953
  ]
  edge [
    source 53
    target 1954
  ]
  edge [
    source 53
    target 1955
  ]
  edge [
    source 53
    target 1956
  ]
  edge [
    source 53
    target 1957
  ]
  edge [
    source 53
    target 1958
  ]
  edge [
    source 53
    target 1959
  ]
  edge [
    source 53
    target 1960
  ]
  edge [
    source 53
    target 1961
  ]
  edge [
    source 53
    target 1469
  ]
  edge [
    source 53
    target 1962
  ]
  edge [
    source 53
    target 1963
  ]
  edge [
    source 53
    target 1964
  ]
  edge [
    source 53
    target 927
  ]
  edge [
    source 53
    target 1965
  ]
  edge [
    source 53
    target 648
  ]
  edge [
    source 53
    target 911
  ]
  edge [
    source 53
    target 1223
  ]
  edge [
    source 53
    target 1966
  ]
  edge [
    source 53
    target 1967
  ]
  edge [
    source 53
    target 1968
  ]
  edge [
    source 53
    target 1969
  ]
  edge [
    source 53
    target 1970
  ]
  edge [
    source 53
    target 229
  ]
  edge [
    source 53
    target 1971
  ]
  edge [
    source 53
    target 1972
  ]
  edge [
    source 53
    target 1973
  ]
  edge [
    source 53
    target 1974
  ]
  edge [
    source 53
    target 235
  ]
  edge [
    source 53
    target 1975
  ]
  edge [
    source 53
    target 1976
  ]
  edge [
    source 53
    target 1913
  ]
  edge [
    source 53
    target 1903
  ]
  edge [
    source 53
    target 1914
  ]
  edge [
    source 53
    target 1915
  ]
  edge [
    source 53
    target 1740
  ]
  edge [
    source 53
    target 1916
  ]
  edge [
    source 53
    target 1918
  ]
  edge [
    source 53
    target 1920
  ]
  edge [
    source 53
    target 182
  ]
  edge [
    source 53
    target 1861
  ]
  edge [
    source 53
    target 1922
  ]
  edge [
    source 53
    target 1923
  ]
  edge [
    source 53
    target 1447
  ]
  edge [
    source 53
    target 541
  ]
  edge [
    source 53
    target 1924
  ]
  edge [
    source 53
    target 1977
  ]
  edge [
    source 53
    target 253
  ]
  edge [
    source 53
    target 1978
  ]
  edge [
    source 53
    target 1979
  ]
  edge [
    source 53
    target 1980
  ]
  edge [
    source 53
    target 1981
  ]
  edge [
    source 53
    target 1982
  ]
  edge [
    source 53
    target 730
  ]
  edge [
    source 53
    target 1983
  ]
  edge [
    source 53
    target 1984
  ]
  edge [
    source 53
    target 1985
  ]
  edge [
    source 53
    target 1986
  ]
  edge [
    source 53
    target 1987
  ]
  edge [
    source 53
    target 1988
  ]
  edge [
    source 53
    target 1989
  ]
  edge [
    source 53
    target 1990
  ]
  edge [
    source 53
    target 248
  ]
  edge [
    source 53
    target 1991
  ]
  edge [
    source 53
    target 1992
  ]
  edge [
    source 53
    target 1993
  ]
  edge [
    source 53
    target 1994
  ]
  edge [
    source 53
    target 1680
  ]
  edge [
    source 53
    target 594
  ]
  edge [
    source 53
    target 1995
  ]
  edge [
    source 53
    target 863
  ]
  edge [
    source 53
    target 1996
  ]
  edge [
    source 53
    target 1997
  ]
  edge [
    source 53
    target 1998
  ]
  edge [
    source 53
    target 1999
  ]
  edge [
    source 53
    target 2000
  ]
  edge [
    source 53
    target 596
  ]
  edge [
    source 53
    target 2001
  ]
  edge [
    source 53
    target 2002
  ]
  edge [
    source 53
    target 2003
  ]
  edge [
    source 53
    target 1035
  ]
  edge [
    source 53
    target 740
  ]
  edge [
    source 53
    target 2004
  ]
  edge [
    source 53
    target 2005
  ]
  edge [
    source 53
    target 1825
  ]
  edge [
    source 53
    target 2006
  ]
  edge [
    source 53
    target 1366
  ]
  edge [
    source 53
    target 2007
  ]
  edge [
    source 53
    target 2008
  ]
  edge [
    source 53
    target 2009
  ]
  edge [
    source 53
    target 2010
  ]
  edge [
    source 53
    target 2011
  ]
  edge [
    source 53
    target 2012
  ]
  edge [
    source 53
    target 2013
  ]
  edge [
    source 53
    target 2014
  ]
  edge [
    source 53
    target 2015
  ]
  edge [
    source 53
    target 2016
  ]
  edge [
    source 53
    target 2017
  ]
  edge [
    source 53
    target 2018
  ]
  edge [
    source 53
    target 2019
  ]
  edge [
    source 53
    target 2020
  ]
  edge [
    source 53
    target 2021
  ]
  edge [
    source 53
    target 2022
  ]
  edge [
    source 53
    target 2023
  ]
  edge [
    source 53
    target 2024
  ]
  edge [
    source 53
    target 2025
  ]
  edge [
    source 53
    target 2026
  ]
  edge [
    source 53
    target 2027
  ]
  edge [
    source 53
    target 2028
  ]
  edge [
    source 53
    target 1758
  ]
  edge [
    source 53
    target 2029
  ]
  edge [
    source 53
    target 2030
  ]
  edge [
    source 53
    target 2031
  ]
  edge [
    source 53
    target 2032
  ]
  edge [
    source 53
    target 2033
  ]
  edge [
    source 53
    target 470
  ]
  edge [
    source 53
    target 2034
  ]
  edge [
    source 53
    target 2035
  ]
  edge [
    source 53
    target 1148
  ]
  edge [
    source 53
    target 1167
  ]
  edge [
    source 53
    target 2036
  ]
  edge [
    source 53
    target 482
  ]
  edge [
    source 53
    target 1153
  ]
  edge [
    source 53
    target 468
  ]
  edge [
    source 53
    target 1136
  ]
  edge [
    source 53
    target 2037
  ]
  edge [
    source 53
    target 343
  ]
  edge [
    source 53
    target 2038
  ]
  edge [
    source 53
    target 2039
  ]
  edge [
    source 53
    target 2040
  ]
  edge [
    source 53
    target 2041
  ]
  edge [
    source 53
    target 2042
  ]
  edge [
    source 53
    target 2043
  ]
  edge [
    source 53
    target 742
  ]
  edge [
    source 53
    target 2044
  ]
  edge [
    source 53
    target 2045
  ]
  edge [
    source 53
    target 2046
  ]
  edge [
    source 53
    target 2047
  ]
  edge [
    source 53
    target 2048
  ]
  edge [
    source 53
    target 2049
  ]
  edge [
    source 53
    target 2050
  ]
  edge [
    source 53
    target 1252
  ]
  edge [
    source 53
    target 1255
  ]
  edge [
    source 53
    target 1334
  ]
  edge [
    source 53
    target 1855
  ]
  edge [
    source 53
    target 2051
  ]
  edge [
    source 53
    target 2052
  ]
  edge [
    source 53
    target 2053
  ]
  edge [
    source 53
    target 2054
  ]
  edge [
    source 53
    target 2055
  ]
  edge [
    source 53
    target 2056
  ]
  edge [
    source 53
    target 2057
  ]
  edge [
    source 53
    target 2058
  ]
  edge [
    source 53
    target 2059
  ]
  edge [
    source 53
    target 2060
  ]
  edge [
    source 53
    target 2061
  ]
  edge [
    source 53
    target 2062
  ]
  edge [
    source 53
    target 2063
  ]
  edge [
    source 53
    target 2064
  ]
  edge [
    source 53
    target 2065
  ]
  edge [
    source 53
    target 2066
  ]
  edge [
    source 53
    target 2067
  ]
  edge [
    source 53
    target 2068
  ]
  edge [
    source 53
    target 2069
  ]
  edge [
    source 53
    target 2070
  ]
  edge [
    source 53
    target 2071
  ]
  edge [
    source 53
    target 2072
  ]
  edge [
    source 53
    target 2073
  ]
  edge [
    source 53
    target 2074
  ]
  edge [
    source 53
    target 2075
  ]
  edge [
    source 53
    target 2076
  ]
  edge [
    source 53
    target 2077
  ]
  edge [
    source 53
    target 2078
  ]
  edge [
    source 53
    target 2079
  ]
  edge [
    source 53
    target 2080
  ]
  edge [
    source 53
    target 2081
  ]
  edge [
    source 53
    target 683
  ]
  edge [
    source 53
    target 1015
  ]
  edge [
    source 53
    target 474
  ]
  edge [
    source 53
    target 847
  ]
  edge [
    source 53
    target 2082
  ]
  edge [
    source 53
    target 673
  ]
  edge [
    source 53
    target 2083
  ]
  edge [
    source 53
    target 2084
  ]
  edge [
    source 53
    target 251
  ]
  edge [
    source 53
    target 1414
  ]
  edge [
    source 53
    target 663
  ]
  edge [
    source 53
    target 257
  ]
  edge [
    source 53
    target 256
  ]
  edge [
    source 53
    target 2085
  ]
  edge [
    source 53
    target 1141
  ]
  edge [
    source 53
    target 2086
  ]
  edge [
    source 53
    target 2087
  ]
  edge [
    source 53
    target 2088
  ]
  edge [
    source 53
    target 2089
  ]
  edge [
    source 53
    target 2090
  ]
  edge [
    source 53
    target 2091
  ]
  edge [
    source 53
    target 2092
  ]
  edge [
    source 53
    target 2093
  ]
  edge [
    source 53
    target 2094
  ]
  edge [
    source 53
    target 2095
  ]
  edge [
    source 53
    target 2096
  ]
  edge [
    source 53
    target 949
  ]
  edge [
    source 53
    target 954
  ]
  edge [
    source 53
    target 2097
  ]
  edge [
    source 53
    target 2098
  ]
  edge [
    source 53
    target 2099
  ]
  edge [
    source 53
    target 599
  ]
  edge [
    source 53
    target 2100
  ]
  edge [
    source 53
    target 2101
  ]
  edge [
    source 53
    target 669
  ]
  edge [
    source 53
    target 331
  ]
  edge [
    source 53
    target 2102
  ]
  edge [
    source 53
    target 1546
  ]
  edge [
    source 53
    target 2103
  ]
  edge [
    source 53
    target 672
  ]
  edge [
    source 53
    target 1543
  ]
  edge [
    source 53
    target 1158
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 53
    target 2104
  ]
  edge [
    source 53
    target 412
  ]
  edge [
    source 53
    target 2105
  ]
  edge [
    source 53
    target 2106
  ]
  edge [
    source 53
    target 2107
  ]
  edge [
    source 53
    target 2108
  ]
  edge [
    source 53
    target 2109
  ]
  edge [
    source 53
    target 2110
  ]
  edge [
    source 53
    target 1654
  ]
  edge [
    source 53
    target 2111
  ]
  edge [
    source 53
    target 2112
  ]
  edge [
    source 53
    target 2113
  ]
  edge [
    source 53
    target 2114
  ]
  edge [
    source 53
    target 2115
  ]
  edge [
    source 53
    target 2116
  ]
  edge [
    source 53
    target 2117
  ]
  edge [
    source 53
    target 2118
  ]
  edge [
    source 53
    target 618
  ]
  edge [
    source 53
    target 2119
  ]
  edge [
    source 53
    target 2120
  ]
  edge [
    source 53
    target 2121
  ]
  edge [
    source 53
    target 2122
  ]
  edge [
    source 53
    target 2123
  ]
  edge [
    source 53
    target 2124
  ]
  edge [
    source 53
    target 2125
  ]
  edge [
    source 53
    target 2126
  ]
  edge [
    source 53
    target 2127
  ]
  edge [
    source 53
    target 2128
  ]
  edge [
    source 53
    target 2129
  ]
  edge [
    source 53
    target 2130
  ]
  edge [
    source 53
    target 2131
  ]
  edge [
    source 53
    target 2132
  ]
  edge [
    source 53
    target 2133
  ]
  edge [
    source 53
    target 2134
  ]
  edge [
    source 53
    target 776
  ]
  edge [
    source 53
    target 2135
  ]
  edge [
    source 53
    target 2136
  ]
  edge [
    source 53
    target 2137
  ]
  edge [
    source 53
    target 2138
  ]
  edge [
    source 53
    target 2139
  ]
  edge [
    source 53
    target 2140
  ]
  edge [
    source 53
    target 2141
  ]
  edge [
    source 53
    target 860
  ]
  edge [
    source 2142
    target 2143
  ]
  edge [
    source 2142
    target 2164
  ]
  edge [
    source 2144
    target 2145
  ]
  edge [
    source 2144
    target 2146
  ]
  edge [
    source 2145
    target 2146
  ]
  edge [
    source 2145
    target 2165
  ]
  edge [
    source 2146
    target 2165
  ]
  edge [
    source 2147
    target 2148
  ]
  edge [
    source 2147
    target 2149
  ]
  edge [
    source 2147
    target 2150
  ]
  edge [
    source 2147
    target 2151
  ]
  edge [
    source 2147
    target 2152
  ]
  edge [
    source 2147
    target 2153
  ]
  edge [
    source 2147
    target 2154
  ]
  edge [
    source 2147
    target 2155
  ]
  edge [
    source 2148
    target 2149
  ]
  edge [
    source 2148
    target 2150
  ]
  edge [
    source 2148
    target 2151
  ]
  edge [
    source 2148
    target 2152
  ]
  edge [
    source 2148
    target 2153
  ]
  edge [
    source 2148
    target 2154
  ]
  edge [
    source 2148
    target 2155
  ]
  edge [
    source 2149
    target 2150
  ]
  edge [
    source 2149
    target 2151
  ]
  edge [
    source 2149
    target 2152
  ]
  edge [
    source 2149
    target 2153
  ]
  edge [
    source 2149
    target 2154
  ]
  edge [
    source 2149
    target 2155
  ]
  edge [
    source 2150
    target 2151
  ]
  edge [
    source 2150
    target 2152
  ]
  edge [
    source 2150
    target 2153
  ]
  edge [
    source 2150
    target 2154
  ]
  edge [
    source 2150
    target 2155
  ]
  edge [
    source 2151
    target 2152
  ]
  edge [
    source 2151
    target 2153
  ]
  edge [
    source 2151
    target 2154
  ]
  edge [
    source 2151
    target 2155
  ]
  edge [
    source 2151
    target 2156
  ]
  edge [
    source 2151
    target 2157
  ]
  edge [
    source 2151
    target 2158
  ]
  edge [
    source 2151
    target 2159
  ]
  edge [
    source 2151
    target 2160
  ]
  edge [
    source 2151
    target 2161
  ]
  edge [
    source 2152
    target 2153
  ]
  edge [
    source 2152
    target 2154
  ]
  edge [
    source 2152
    target 2155
  ]
  edge [
    source 2152
    target 2156
  ]
  edge [
    source 2152
    target 2157
  ]
  edge [
    source 2152
    target 2158
  ]
  edge [
    source 2152
    target 2159
  ]
  edge [
    source 2152
    target 2160
  ]
  edge [
    source 2152
    target 2161
  ]
  edge [
    source 2153
    target 2154
  ]
  edge [
    source 2153
    target 2155
  ]
  edge [
    source 2154
    target 2155
  ]
  edge [
    source 2156
    target 2157
  ]
  edge [
    source 2156
    target 2158
  ]
  edge [
    source 2156
    target 2159
  ]
  edge [
    source 2156
    target 2160
  ]
  edge [
    source 2156
    target 2161
  ]
  edge [
    source 2157
    target 2158
  ]
  edge [
    source 2157
    target 2159
  ]
  edge [
    source 2157
    target 2160
  ]
  edge [
    source 2157
    target 2161
  ]
  edge [
    source 2158
    target 2159
  ]
  edge [
    source 2158
    target 2160
  ]
  edge [
    source 2158
    target 2161
  ]
  edge [
    source 2159
    target 2160
  ]
  edge [
    source 2159
    target 2161
  ]
  edge [
    source 2160
    target 2161
  ]
  edge [
    source 2162
    target 2163
  ]
  edge [
    source 2166
    target 2167
  ]
  edge [
    source 2168
    target 2169
  ]
]
