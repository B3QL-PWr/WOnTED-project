graph [
  node [
    id 0
    label "fizyk"
    origin "text"
  ]
  node [
    id 1
    label "raz"
    origin "text"
  ]
  node [
    id 2
    label "pierwszy"
    origin "text"
  ]
  node [
    id 3
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "postulat"
    origin "text"
  ]
  node [
    id 5
    label "fizyka"
    origin "text"
  ]
  node [
    id 6
    label "klasyczny"
    origin "text"
  ]
  node [
    id 7
    label "zastosowanie"
    origin "text"
  ]
  node [
    id 8
    label "cia&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "kilkana&#347;cie"
    origin "text"
  ]
  node [
    id 12
    label "miliard"
    origin "text"
  ]
  node [
    id 13
    label "atom"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "nauczyciel"
  ]
  node [
    id 17
    label "Kartezjusz"
  ]
  node [
    id 18
    label "Biot"
  ]
  node [
    id 19
    label "Einstein"
  ]
  node [
    id 20
    label "Galvani"
  ]
  node [
    id 21
    label "Culomb"
  ]
  node [
    id 22
    label "Doppler"
  ]
  node [
    id 23
    label "Lorentz"
  ]
  node [
    id 24
    label "naukowiec"
  ]
  node [
    id 25
    label "William_Nicol"
  ]
  node [
    id 26
    label "Faraday"
  ]
  node [
    id 27
    label "Weber"
  ]
  node [
    id 28
    label "Gilbert"
  ]
  node [
    id 29
    label "Newton"
  ]
  node [
    id 30
    label "Pascal"
  ]
  node [
    id 31
    label "Maxwell"
  ]
  node [
    id 32
    label "belfer"
  ]
  node [
    id 33
    label "kszta&#322;ciciel"
  ]
  node [
    id 34
    label "preceptor"
  ]
  node [
    id 35
    label "pedagog"
  ]
  node [
    id 36
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 37
    label "szkolnik"
  ]
  node [
    id 38
    label "profesor"
  ]
  node [
    id 39
    label "popularyzator"
  ]
  node [
    id 40
    label "&#347;ledziciel"
  ]
  node [
    id 41
    label "uczony"
  ]
  node [
    id 42
    label "Miczurin"
  ]
  node [
    id 43
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 44
    label "filozofia"
  ]
  node [
    id 45
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 46
    label "rozci&#261;g&#322;y"
  ]
  node [
    id 47
    label "pogl&#261;dy"
  ]
  node [
    id 48
    label "teoria_wzgl&#281;dno&#347;ci"
  ]
  node [
    id 49
    label "j&#281;zyk_programowania"
  ]
  node [
    id 50
    label "time"
  ]
  node [
    id 51
    label "cios"
  ]
  node [
    id 52
    label "chwila"
  ]
  node [
    id 53
    label "uderzenie"
  ]
  node [
    id 54
    label "blok"
  ]
  node [
    id 55
    label "shot"
  ]
  node [
    id 56
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 57
    label "struktura_geologiczna"
  ]
  node [
    id 58
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 59
    label "pr&#243;ba"
  ]
  node [
    id 60
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 61
    label "coup"
  ]
  node [
    id 62
    label "siekacz"
  ]
  node [
    id 63
    label "instrumentalizacja"
  ]
  node [
    id 64
    label "trafienie"
  ]
  node [
    id 65
    label "walka"
  ]
  node [
    id 66
    label "zdarzenie_si&#281;"
  ]
  node [
    id 67
    label "wdarcie_si&#281;"
  ]
  node [
    id 68
    label "pogorszenie"
  ]
  node [
    id 69
    label "d&#378;wi&#281;k"
  ]
  node [
    id 70
    label "poczucie"
  ]
  node [
    id 71
    label "reakcja"
  ]
  node [
    id 72
    label "contact"
  ]
  node [
    id 73
    label "stukni&#281;cie"
  ]
  node [
    id 74
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 75
    label "bat"
  ]
  node [
    id 76
    label "spowodowanie"
  ]
  node [
    id 77
    label "rush"
  ]
  node [
    id 78
    label "odbicie"
  ]
  node [
    id 79
    label "dawka"
  ]
  node [
    id 80
    label "zadanie"
  ]
  node [
    id 81
    label "&#347;ci&#281;cie"
  ]
  node [
    id 82
    label "st&#322;uczenie"
  ]
  node [
    id 83
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 84
    label "odbicie_si&#281;"
  ]
  node [
    id 85
    label "dotkni&#281;cie"
  ]
  node [
    id 86
    label "charge"
  ]
  node [
    id 87
    label "dostanie"
  ]
  node [
    id 88
    label "skrytykowanie"
  ]
  node [
    id 89
    label "zagrywka"
  ]
  node [
    id 90
    label "manewr"
  ]
  node [
    id 91
    label "nast&#261;pienie"
  ]
  node [
    id 92
    label "uderzanie"
  ]
  node [
    id 93
    label "pogoda"
  ]
  node [
    id 94
    label "stroke"
  ]
  node [
    id 95
    label "pobicie"
  ]
  node [
    id 96
    label "ruch"
  ]
  node [
    id 97
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 98
    label "flap"
  ]
  node [
    id 99
    label "dotyk"
  ]
  node [
    id 100
    label "zrobienie"
  ]
  node [
    id 101
    label "czas"
  ]
  node [
    id 102
    label "pr&#281;dki"
  ]
  node [
    id 103
    label "pocz&#261;tkowy"
  ]
  node [
    id 104
    label "najwa&#380;niejszy"
  ]
  node [
    id 105
    label "ch&#281;tny"
  ]
  node [
    id 106
    label "dzie&#324;"
  ]
  node [
    id 107
    label "dobry"
  ]
  node [
    id 108
    label "dobroczynny"
  ]
  node [
    id 109
    label "czw&#243;rka"
  ]
  node [
    id 110
    label "spokojny"
  ]
  node [
    id 111
    label "skuteczny"
  ]
  node [
    id 112
    label "&#347;mieszny"
  ]
  node [
    id 113
    label "mi&#322;y"
  ]
  node [
    id 114
    label "grzeczny"
  ]
  node [
    id 115
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 116
    label "powitanie"
  ]
  node [
    id 117
    label "dobrze"
  ]
  node [
    id 118
    label "ca&#322;y"
  ]
  node [
    id 119
    label "zwrot"
  ]
  node [
    id 120
    label "pomy&#347;lny"
  ]
  node [
    id 121
    label "moralny"
  ]
  node [
    id 122
    label "drogi"
  ]
  node [
    id 123
    label "pozytywny"
  ]
  node [
    id 124
    label "odpowiedni"
  ]
  node [
    id 125
    label "korzystny"
  ]
  node [
    id 126
    label "pos&#322;uszny"
  ]
  node [
    id 127
    label "intensywny"
  ]
  node [
    id 128
    label "szybki"
  ]
  node [
    id 129
    label "kr&#243;tki"
  ]
  node [
    id 130
    label "temperamentny"
  ]
  node [
    id 131
    label "dynamiczny"
  ]
  node [
    id 132
    label "szybko"
  ]
  node [
    id 133
    label "sprawny"
  ]
  node [
    id 134
    label "energiczny"
  ]
  node [
    id 135
    label "cz&#322;owiek"
  ]
  node [
    id 136
    label "ch&#281;tliwy"
  ]
  node [
    id 137
    label "ch&#281;tnie"
  ]
  node [
    id 138
    label "napalony"
  ]
  node [
    id 139
    label "chy&#380;y"
  ]
  node [
    id 140
    label "&#380;yczliwy"
  ]
  node [
    id 141
    label "przychylny"
  ]
  node [
    id 142
    label "gotowy"
  ]
  node [
    id 143
    label "ranek"
  ]
  node [
    id 144
    label "doba"
  ]
  node [
    id 145
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 146
    label "noc"
  ]
  node [
    id 147
    label "podwiecz&#243;r"
  ]
  node [
    id 148
    label "po&#322;udnie"
  ]
  node [
    id 149
    label "godzina"
  ]
  node [
    id 150
    label "przedpo&#322;udnie"
  ]
  node [
    id 151
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 152
    label "long_time"
  ]
  node [
    id 153
    label "wiecz&#243;r"
  ]
  node [
    id 154
    label "t&#322;usty_czwartek"
  ]
  node [
    id 155
    label "popo&#322;udnie"
  ]
  node [
    id 156
    label "walentynki"
  ]
  node [
    id 157
    label "czynienie_si&#281;"
  ]
  node [
    id 158
    label "s&#322;o&#324;ce"
  ]
  node [
    id 159
    label "rano"
  ]
  node [
    id 160
    label "tydzie&#324;"
  ]
  node [
    id 161
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 162
    label "wzej&#347;cie"
  ]
  node [
    id 163
    label "wsta&#263;"
  ]
  node [
    id 164
    label "day"
  ]
  node [
    id 165
    label "termin"
  ]
  node [
    id 166
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 167
    label "wstanie"
  ]
  node [
    id 168
    label "przedwiecz&#243;r"
  ]
  node [
    id 169
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 170
    label "Sylwester"
  ]
  node [
    id 171
    label "dzieci&#281;cy"
  ]
  node [
    id 172
    label "podstawowy"
  ]
  node [
    id 173
    label "elementarny"
  ]
  node [
    id 174
    label "pocz&#261;tkowo"
  ]
  node [
    id 175
    label "testify"
  ]
  node [
    id 176
    label "point"
  ]
  node [
    id 177
    label "przedstawi&#263;"
  ]
  node [
    id 178
    label "poda&#263;"
  ]
  node [
    id 179
    label "poinformowa&#263;"
  ]
  node [
    id 180
    label "udowodni&#263;"
  ]
  node [
    id 181
    label "spowodowa&#263;"
  ]
  node [
    id 182
    label "wyrazi&#263;"
  ]
  node [
    id 183
    label "przeszkoli&#263;"
  ]
  node [
    id 184
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 185
    label "indicate"
  ]
  node [
    id 186
    label "pom&#243;c"
  ]
  node [
    id 187
    label "inform"
  ]
  node [
    id 188
    label "zakomunikowa&#263;"
  ]
  node [
    id 189
    label "uzasadni&#263;"
  ]
  node [
    id 190
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 191
    label "oznaczy&#263;"
  ]
  node [
    id 192
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 193
    label "vent"
  ]
  node [
    id 194
    label "act"
  ]
  node [
    id 195
    label "tenis"
  ]
  node [
    id 196
    label "supply"
  ]
  node [
    id 197
    label "da&#263;"
  ]
  node [
    id 198
    label "ustawi&#263;"
  ]
  node [
    id 199
    label "siatk&#243;wka"
  ]
  node [
    id 200
    label "give"
  ]
  node [
    id 201
    label "zagra&#263;"
  ]
  node [
    id 202
    label "jedzenie"
  ]
  node [
    id 203
    label "introduce"
  ]
  node [
    id 204
    label "nafaszerowa&#263;"
  ]
  node [
    id 205
    label "zaserwowa&#263;"
  ]
  node [
    id 206
    label "ukaza&#263;"
  ]
  node [
    id 207
    label "przedstawienie"
  ]
  node [
    id 208
    label "zapozna&#263;"
  ]
  node [
    id 209
    label "express"
  ]
  node [
    id 210
    label "represent"
  ]
  node [
    id 211
    label "zaproponowa&#263;"
  ]
  node [
    id 212
    label "zademonstrowa&#263;"
  ]
  node [
    id 213
    label "typify"
  ]
  node [
    id 214
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 215
    label "opisa&#263;"
  ]
  node [
    id 216
    label "aksjomat_Pascha"
  ]
  node [
    id 217
    label "za&#322;o&#380;enie"
  ]
  node [
    id 218
    label "claim"
  ]
  node [
    id 219
    label "aksjomat_Archimedesa"
  ]
  node [
    id 220
    label "axiom"
  ]
  node [
    id 221
    label "wniosek"
  ]
  node [
    id 222
    label "pismo"
  ]
  node [
    id 223
    label "prayer"
  ]
  node [
    id 224
    label "twierdzenie"
  ]
  node [
    id 225
    label "propozycja"
  ]
  node [
    id 226
    label "my&#347;l"
  ]
  node [
    id 227
    label "motion"
  ]
  node [
    id 228
    label "wnioskowanie"
  ]
  node [
    id 229
    label "podwini&#281;cie"
  ]
  node [
    id 230
    label "zap&#322;acenie"
  ]
  node [
    id 231
    label "przyodzianie"
  ]
  node [
    id 232
    label "budowla"
  ]
  node [
    id 233
    label "pokrycie"
  ]
  node [
    id 234
    label "rozebranie"
  ]
  node [
    id 235
    label "zak&#322;adka"
  ]
  node [
    id 236
    label "struktura"
  ]
  node [
    id 237
    label "poubieranie"
  ]
  node [
    id 238
    label "infliction"
  ]
  node [
    id 239
    label "pozak&#322;adanie"
  ]
  node [
    id 240
    label "program"
  ]
  node [
    id 241
    label "przebranie"
  ]
  node [
    id 242
    label "przywdzianie"
  ]
  node [
    id 243
    label "obleczenie_si&#281;"
  ]
  node [
    id 244
    label "utworzenie"
  ]
  node [
    id 245
    label "str&#243;j"
  ]
  node [
    id 246
    label "obleczenie"
  ]
  node [
    id 247
    label "umieszczenie"
  ]
  node [
    id 248
    label "czynno&#347;&#263;"
  ]
  node [
    id 249
    label "przygotowywanie"
  ]
  node [
    id 250
    label "przymierzenie"
  ]
  node [
    id 251
    label "wyko&#324;czenie"
  ]
  node [
    id 252
    label "przygotowanie"
  ]
  node [
    id 253
    label "proposition"
  ]
  node [
    id 254
    label "przewidzenie"
  ]
  node [
    id 255
    label "poprzedzanie"
  ]
  node [
    id 256
    label "czasoprzestrze&#324;"
  ]
  node [
    id 257
    label "laba"
  ]
  node [
    id 258
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 259
    label "chronometria"
  ]
  node [
    id 260
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 261
    label "rachuba_czasu"
  ]
  node [
    id 262
    label "przep&#322;ywanie"
  ]
  node [
    id 263
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 264
    label "czasokres"
  ]
  node [
    id 265
    label "odczyt"
  ]
  node [
    id 266
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 267
    label "dzieje"
  ]
  node [
    id 268
    label "kategoria_gramatyczna"
  ]
  node [
    id 269
    label "poprzedzenie"
  ]
  node [
    id 270
    label "trawienie"
  ]
  node [
    id 271
    label "pochodzi&#263;"
  ]
  node [
    id 272
    label "period"
  ]
  node [
    id 273
    label "okres_czasu"
  ]
  node [
    id 274
    label "poprzedza&#263;"
  ]
  node [
    id 275
    label "schy&#322;ek"
  ]
  node [
    id 276
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 277
    label "odwlekanie_si&#281;"
  ]
  node [
    id 278
    label "zegar"
  ]
  node [
    id 279
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 280
    label "czwarty_wymiar"
  ]
  node [
    id 281
    label "pochodzenie"
  ]
  node [
    id 282
    label "koniugacja"
  ]
  node [
    id 283
    label "Zeitgeist"
  ]
  node [
    id 284
    label "trawi&#263;"
  ]
  node [
    id 285
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 286
    label "poprzedzi&#263;"
  ]
  node [
    id 287
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 288
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 289
    label "time_period"
  ]
  node [
    id 290
    label "mechanika"
  ]
  node [
    id 291
    label "fizyka_plazmy"
  ]
  node [
    id 292
    label "przedmiot"
  ]
  node [
    id 293
    label "interferometria"
  ]
  node [
    id 294
    label "akustyka"
  ]
  node [
    id 295
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 296
    label "fizyka_medyczna"
  ]
  node [
    id 297
    label "teoria_p&#243;l_kwantowych"
  ]
  node [
    id 298
    label "mikrofizyka"
  ]
  node [
    id 299
    label "elektrostatyka"
  ]
  node [
    id 300
    label "elektryczno&#347;&#263;"
  ]
  node [
    id 301
    label "kierunek"
  ]
  node [
    id 302
    label "teoria_pola"
  ]
  node [
    id 303
    label "agrofizyka"
  ]
  node [
    id 304
    label "fizyka_statystyczna"
  ]
  node [
    id 305
    label "fizyka_kwantowa"
  ]
  node [
    id 306
    label "optyka"
  ]
  node [
    id 307
    label "elektromagnetyzm"
  ]
  node [
    id 308
    label "dylatometria"
  ]
  node [
    id 309
    label "elektryka"
  ]
  node [
    id 310
    label "dozymetria"
  ]
  node [
    id 311
    label "elektrodynamika"
  ]
  node [
    id 312
    label "spektroskopia"
  ]
  node [
    id 313
    label "rentgenologia"
  ]
  node [
    id 314
    label "fiza"
  ]
  node [
    id 315
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 316
    label "geofizyka"
  ]
  node [
    id 317
    label "termodynamika"
  ]
  node [
    id 318
    label "fizyka_atomowa"
  ]
  node [
    id 319
    label "fizyka_teoretyczna"
  ]
  node [
    id 320
    label "elektrokinetyka"
  ]
  node [
    id 321
    label "fizyka_molekularna"
  ]
  node [
    id 322
    label "fizyka_cia&#322;a_sta&#322;ego"
  ]
  node [
    id 323
    label "pr&#243;&#380;nia"
  ]
  node [
    id 324
    label "nauka_przyrodnicza"
  ]
  node [
    id 325
    label "chemia_powierzchni"
  ]
  node [
    id 326
    label "kriofizyka"
  ]
  node [
    id 327
    label "zboczenie"
  ]
  node [
    id 328
    label "om&#243;wienie"
  ]
  node [
    id 329
    label "sponiewieranie"
  ]
  node [
    id 330
    label "discipline"
  ]
  node [
    id 331
    label "rzecz"
  ]
  node [
    id 332
    label "omawia&#263;"
  ]
  node [
    id 333
    label "kr&#261;&#380;enie"
  ]
  node [
    id 334
    label "tre&#347;&#263;"
  ]
  node [
    id 335
    label "robienie"
  ]
  node [
    id 336
    label "sponiewiera&#263;"
  ]
  node [
    id 337
    label "element"
  ]
  node [
    id 338
    label "entity"
  ]
  node [
    id 339
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 340
    label "tematyka"
  ]
  node [
    id 341
    label "w&#261;tek"
  ]
  node [
    id 342
    label "charakter"
  ]
  node [
    id 343
    label "zbaczanie"
  ]
  node [
    id 344
    label "program_nauczania"
  ]
  node [
    id 345
    label "om&#243;wi&#263;"
  ]
  node [
    id 346
    label "omawianie"
  ]
  node [
    id 347
    label "thing"
  ]
  node [
    id 348
    label "kultura"
  ]
  node [
    id 349
    label "istota"
  ]
  node [
    id 350
    label "zbacza&#263;"
  ]
  node [
    id 351
    label "zboczy&#263;"
  ]
  node [
    id 352
    label "przebieg"
  ]
  node [
    id 353
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 354
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 355
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 356
    label "praktyka"
  ]
  node [
    id 357
    label "system"
  ]
  node [
    id 358
    label "przeorientowywanie"
  ]
  node [
    id 359
    label "studia"
  ]
  node [
    id 360
    label "linia"
  ]
  node [
    id 361
    label "bok"
  ]
  node [
    id 362
    label "skr&#281;canie"
  ]
  node [
    id 363
    label "skr&#281;ca&#263;"
  ]
  node [
    id 364
    label "przeorientowywa&#263;"
  ]
  node [
    id 365
    label "orientowanie"
  ]
  node [
    id 366
    label "skr&#281;ci&#263;"
  ]
  node [
    id 367
    label "przeorientowanie"
  ]
  node [
    id 368
    label "zorientowanie"
  ]
  node [
    id 369
    label "przeorientowa&#263;"
  ]
  node [
    id 370
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 371
    label "metoda"
  ]
  node [
    id 372
    label "ty&#322;"
  ]
  node [
    id 373
    label "zorientowa&#263;"
  ]
  node [
    id 374
    label "g&#243;ra"
  ]
  node [
    id 375
    label "orientowa&#263;"
  ]
  node [
    id 376
    label "spos&#243;b"
  ]
  node [
    id 377
    label "ideologia"
  ]
  node [
    id 378
    label "orientacja"
  ]
  node [
    id 379
    label "prz&#243;d"
  ]
  node [
    id 380
    label "bearing"
  ]
  node [
    id 381
    label "skr&#281;cenie"
  ]
  node [
    id 382
    label "termodynamika_kwantowa"
  ]
  node [
    id 383
    label "termodynamika_chemiczna"
  ]
  node [
    id 384
    label "termodynamika_klasyczna"
  ]
  node [
    id 385
    label "perpetuum_mobile"
  ]
  node [
    id 386
    label "termodynamika_statystyczna"
  ]
  node [
    id 387
    label "termodynamika_techniczna"
  ]
  node [
    id 388
    label "geologia"
  ]
  node [
    id 389
    label "geodynamika"
  ]
  node [
    id 390
    label "sejsmologia"
  ]
  node [
    id 391
    label "paleomagnetyzm"
  ]
  node [
    id 392
    label "magnetometria"
  ]
  node [
    id 393
    label "nauki_o_Ziemi"
  ]
  node [
    id 394
    label "geoelektryka"
  ]
  node [
    id 395
    label "geotermika"
  ]
  node [
    id 396
    label "grawimetria"
  ]
  node [
    id 397
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 398
    label "energia"
  ]
  node [
    id 399
    label "naelektryzowanie"
  ]
  node [
    id 400
    label "elektryzowa&#263;"
  ]
  node [
    id 401
    label "zjawisko"
  ]
  node [
    id 402
    label "elektryzowanie"
  ]
  node [
    id 403
    label "naelektryzowa&#263;"
  ]
  node [
    id 404
    label "fluorescencyjna_spektroskopia_rentgenowska"
  ]
  node [
    id 405
    label "spektrometria"
  ]
  node [
    id 406
    label "spektroskopia_optyczna"
  ]
  node [
    id 407
    label "radiospektroskopia"
  ]
  node [
    id 408
    label "spektroskopia_atomowa"
  ]
  node [
    id 409
    label "spektroskopia_absorpcyjna"
  ]
  node [
    id 410
    label "spectroscopy"
  ]
  node [
    id 411
    label "spektroskopia_elektronowa"
  ]
  node [
    id 412
    label "magnetyzm"
  ]
  node [
    id 413
    label "agronomia"
  ]
  node [
    id 414
    label "nauka"
  ]
  node [
    id 415
    label "wy&#322;&#261;cznik"
  ]
  node [
    id 416
    label "mikroinstalacja"
  ]
  node [
    id 417
    label "instalacja"
  ]
  node [
    id 418
    label "rozdzielnica"
  ]
  node [
    id 419
    label "obw&#243;d"
  ]
  node [
    id 420
    label "kabel"
  ]
  node [
    id 421
    label "kontakt"
  ]
  node [
    id 422
    label "puszka"
  ]
  node [
    id 423
    label "rozszerzalno&#347;&#263;"
  ]
  node [
    id 424
    label "nauka_medyczna"
  ]
  node [
    id 425
    label "radiologia"
  ]
  node [
    id 426
    label "sejsmoakustyka"
  ]
  node [
    id 427
    label "transjent"
  ]
  node [
    id 428
    label "s&#322;yszalno&#347;&#263;"
  ]
  node [
    id 429
    label "hydroakustyka"
  ]
  node [
    id 430
    label "cecha"
  ]
  node [
    id 431
    label "mod"
  ]
  node [
    id 432
    label "wibroakustyka"
  ]
  node [
    id 433
    label "acoustics"
  ]
  node [
    id 434
    label "dosimetry"
  ]
  node [
    id 435
    label "mechanika_teoretyczna"
  ]
  node [
    id 436
    label "mechanika_gruntu"
  ]
  node [
    id 437
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 438
    label "mechanika_klasyczna"
  ]
  node [
    id 439
    label "elektromechanika"
  ]
  node [
    id 440
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 441
    label "aeromechanika"
  ]
  node [
    id 442
    label "telemechanika"
  ]
  node [
    id 443
    label "hydromechanika"
  ]
  node [
    id 444
    label "optyka_geometryczna"
  ]
  node [
    id 445
    label "irradiacja"
  ]
  node [
    id 446
    label "dioptria"
  ]
  node [
    id 447
    label "optyka_adaptacyjna"
  ]
  node [
    id 448
    label "expectation"
  ]
  node [
    id 449
    label "elektrooptyka"
  ]
  node [
    id 450
    label "optyka_nieliniowa"
  ]
  node [
    id 451
    label "optyka_elektronowa"
  ]
  node [
    id 452
    label "aberracyjny"
  ]
  node [
    id 453
    label "fotonika"
  ]
  node [
    id 454
    label "fotometria"
  ]
  node [
    id 455
    label "dioptryka"
  ]
  node [
    id 456
    label "optyka_falowa"
  ]
  node [
    id 457
    label "transmitancja"
  ]
  node [
    id 458
    label "katoptryka"
  ]
  node [
    id 459
    label "wytw&#243;r"
  ]
  node [
    id 460
    label "posta&#263;"
  ]
  node [
    id 461
    label "widzie&#263;"
  ]
  node [
    id 462
    label "optyka_kwantowa"
  ]
  node [
    id 463
    label "holografia"
  ]
  node [
    id 464
    label "patrzenie"
  ]
  node [
    id 465
    label "patrze&#263;"
  ]
  node [
    id 466
    label "decentracja"
  ]
  node [
    id 467
    label "magnetooptyka"
  ]
  node [
    id 468
    label "pojmowanie"
  ]
  node [
    id 469
    label "kolorymetria"
  ]
  node [
    id 470
    label "przestrze&#324;"
  ]
  node [
    id 471
    label "futility"
  ]
  node [
    id 472
    label "nico&#347;&#263;"
  ]
  node [
    id 473
    label "nieklasyczny"
  ]
  node [
    id 474
    label "modelowy"
  ]
  node [
    id 475
    label "klasyczno"
  ]
  node [
    id 476
    label "typowy"
  ]
  node [
    id 477
    label "zwyczajny"
  ]
  node [
    id 478
    label "staro&#380;ytny"
  ]
  node [
    id 479
    label "tradycyjny"
  ]
  node [
    id 480
    label "normatywny"
  ]
  node [
    id 481
    label "klasycznie"
  ]
  node [
    id 482
    label "tradycyjnie"
  ]
  node [
    id 483
    label "surowy"
  ]
  node [
    id 484
    label "zwyk&#322;y"
  ]
  node [
    id 485
    label "zachowawczy"
  ]
  node [
    id 486
    label "nienowoczesny"
  ]
  node [
    id 487
    label "przyj&#281;ty"
  ]
  node [
    id 488
    label "wierny"
  ]
  node [
    id 489
    label "zwyczajowy"
  ]
  node [
    id 490
    label "przeci&#281;tny"
  ]
  node [
    id 491
    label "oswojony"
  ]
  node [
    id 492
    label "zwyczajnie"
  ]
  node [
    id 493
    label "zwykle"
  ]
  node [
    id 494
    label "na&#322;o&#380;ny"
  ]
  node [
    id 495
    label "cz&#281;sty"
  ]
  node [
    id 496
    label "okre&#347;lony"
  ]
  node [
    id 497
    label "typowo"
  ]
  node [
    id 498
    label "doskona&#322;y"
  ]
  node [
    id 499
    label "pr&#243;bny"
  ]
  node [
    id 500
    label "modelowo"
  ]
  node [
    id 501
    label "specjalny"
  ]
  node [
    id 502
    label "obowi&#261;zuj&#261;cy"
  ]
  node [
    id 503
    label "normatywnie"
  ]
  node [
    id 504
    label "staro&#380;ytnie"
  ]
  node [
    id 505
    label "apadana"
  ]
  node [
    id 506
    label "dawny"
  ]
  node [
    id 507
    label "stary"
  ]
  node [
    id 508
    label "niestandardowo"
  ]
  node [
    id 509
    label "nietypowy"
  ]
  node [
    id 510
    label "nietradycyjny"
  ]
  node [
    id 511
    label "nieklasycznie"
  ]
  node [
    id 512
    label "stosowanie"
  ]
  node [
    id 513
    label "cel"
  ]
  node [
    id 514
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 515
    label "funkcja"
  ]
  node [
    id 516
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 517
    label "use"
  ]
  node [
    id 518
    label "punkt"
  ]
  node [
    id 519
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 520
    label "miejsce"
  ]
  node [
    id 521
    label "rezultat"
  ]
  node [
    id 522
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 523
    label "narobienie"
  ]
  node [
    id 524
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 525
    label "creation"
  ]
  node [
    id 526
    label "porobienie"
  ]
  node [
    id 527
    label "czyn"
  ]
  node [
    id 528
    label "supremum"
  ]
  node [
    id 529
    label "addytywno&#347;&#263;"
  ]
  node [
    id 530
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 531
    label "jednostka"
  ]
  node [
    id 532
    label "function"
  ]
  node [
    id 533
    label "matematyka"
  ]
  node [
    id 534
    label "funkcjonowanie"
  ]
  node [
    id 535
    label "praca"
  ]
  node [
    id 536
    label "rzut"
  ]
  node [
    id 537
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 538
    label "powierzanie"
  ]
  node [
    id 539
    label "dziedzina"
  ]
  node [
    id 540
    label "przeciwdziedzina"
  ]
  node [
    id 541
    label "awansowa&#263;"
  ]
  node [
    id 542
    label "stawia&#263;"
  ]
  node [
    id 543
    label "wakowa&#263;"
  ]
  node [
    id 544
    label "znaczenie"
  ]
  node [
    id 545
    label "postawi&#263;"
  ]
  node [
    id 546
    label "awansowanie"
  ]
  node [
    id 547
    label "infimum"
  ]
  node [
    id 548
    label "przejaskrawianie"
  ]
  node [
    id 549
    label "zniszczenie"
  ]
  node [
    id 550
    label "zu&#380;ywanie"
  ]
  node [
    id 551
    label "ekshumowanie"
  ]
  node [
    id 552
    label "uk&#322;ad"
  ]
  node [
    id 553
    label "jednostka_organizacyjna"
  ]
  node [
    id 554
    label "p&#322;aszczyzna"
  ]
  node [
    id 555
    label "odwadnia&#263;"
  ]
  node [
    id 556
    label "zabalsamowanie"
  ]
  node [
    id 557
    label "zesp&#243;&#322;"
  ]
  node [
    id 558
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 559
    label "odwodni&#263;"
  ]
  node [
    id 560
    label "sk&#243;ra"
  ]
  node [
    id 561
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 562
    label "staw"
  ]
  node [
    id 563
    label "ow&#322;osienie"
  ]
  node [
    id 564
    label "mi&#281;so"
  ]
  node [
    id 565
    label "zabalsamowa&#263;"
  ]
  node [
    id 566
    label "Izba_Konsyliarska"
  ]
  node [
    id 567
    label "unerwienie"
  ]
  node [
    id 568
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 569
    label "zbi&#243;r"
  ]
  node [
    id 570
    label "kremacja"
  ]
  node [
    id 571
    label "biorytm"
  ]
  node [
    id 572
    label "sekcja"
  ]
  node [
    id 573
    label "istota_&#380;ywa"
  ]
  node [
    id 574
    label "otworzy&#263;"
  ]
  node [
    id 575
    label "otwiera&#263;"
  ]
  node [
    id 576
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 577
    label "otworzenie"
  ]
  node [
    id 578
    label "materia"
  ]
  node [
    id 579
    label "pochowanie"
  ]
  node [
    id 580
    label "otwieranie"
  ]
  node [
    id 581
    label "szkielet"
  ]
  node [
    id 582
    label "tanatoplastyk"
  ]
  node [
    id 583
    label "odwadnianie"
  ]
  node [
    id 584
    label "Komitet_Region&#243;w"
  ]
  node [
    id 585
    label "odwodnienie"
  ]
  node [
    id 586
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 587
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 588
    label "pochowa&#263;"
  ]
  node [
    id 589
    label "tanatoplastyka"
  ]
  node [
    id 590
    label "balsamowa&#263;"
  ]
  node [
    id 591
    label "nieumar&#322;y"
  ]
  node [
    id 592
    label "temperatura"
  ]
  node [
    id 593
    label "balsamowanie"
  ]
  node [
    id 594
    label "ekshumowa&#263;"
  ]
  node [
    id 595
    label "l&#281;d&#378;wie"
  ]
  node [
    id 596
    label "cz&#322;onek"
  ]
  node [
    id 597
    label "pogrzeb"
  ]
  node [
    id 598
    label "Mazowsze"
  ]
  node [
    id 599
    label "odm&#322;adzanie"
  ]
  node [
    id 600
    label "&#346;wietliki"
  ]
  node [
    id 601
    label "whole"
  ]
  node [
    id 602
    label "skupienie"
  ]
  node [
    id 603
    label "The_Beatles"
  ]
  node [
    id 604
    label "odm&#322;adza&#263;"
  ]
  node [
    id 605
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 606
    label "zabudowania"
  ]
  node [
    id 607
    label "group"
  ]
  node [
    id 608
    label "zespolik"
  ]
  node [
    id 609
    label "schorzenie"
  ]
  node [
    id 610
    label "ro&#347;lina"
  ]
  node [
    id 611
    label "grupa"
  ]
  node [
    id 612
    label "Depeche_Mode"
  ]
  node [
    id 613
    label "batch"
  ]
  node [
    id 614
    label "odm&#322;odzenie"
  ]
  node [
    id 615
    label "materia&#322;"
  ]
  node [
    id 616
    label "temat"
  ]
  node [
    id 617
    label "byt"
  ]
  node [
    id 618
    label "szczeg&#243;&#322;"
  ]
  node [
    id 619
    label "ropa"
  ]
  node [
    id 620
    label "informacja"
  ]
  node [
    id 621
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 622
    label "egzemplarz"
  ]
  node [
    id 623
    label "series"
  ]
  node [
    id 624
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 625
    label "uprawianie"
  ]
  node [
    id 626
    label "praca_rolnicza"
  ]
  node [
    id 627
    label "collection"
  ]
  node [
    id 628
    label "dane"
  ]
  node [
    id 629
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 630
    label "pakiet_klimatyczny"
  ]
  node [
    id 631
    label "poj&#281;cie"
  ]
  node [
    id 632
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 633
    label "sum"
  ]
  node [
    id 634
    label "gathering"
  ]
  node [
    id 635
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 636
    label "album"
  ]
  node [
    id 637
    label "zmar&#322;y"
  ]
  node [
    id 638
    label "nekromancja"
  ]
  node [
    id 639
    label "istota_fantastyczna"
  ]
  node [
    id 640
    label "zw&#322;oki"
  ]
  node [
    id 641
    label "zakonserwowa&#263;"
  ]
  node [
    id 642
    label "poumieszczanie"
  ]
  node [
    id 643
    label "burying"
  ]
  node [
    id 644
    label "powk&#322;adanie"
  ]
  node [
    id 645
    label "burial"
  ]
  node [
    id 646
    label "w&#322;o&#380;enie"
  ]
  node [
    id 647
    label "gr&#243;b"
  ]
  node [
    id 648
    label "spocz&#281;cie"
  ]
  node [
    id 649
    label "embalm"
  ]
  node [
    id 650
    label "konserwowa&#263;"
  ]
  node [
    id 651
    label "paraszyt"
  ]
  node [
    id 652
    label "zakonserwowanie"
  ]
  node [
    id 653
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 654
    label "poumieszcza&#263;"
  ]
  node [
    id 655
    label "hide"
  ]
  node [
    id 656
    label "znie&#347;&#263;"
  ]
  node [
    id 657
    label "straci&#263;"
  ]
  node [
    id 658
    label "powk&#322;ada&#263;"
  ]
  node [
    id 659
    label "bury"
  ]
  node [
    id 660
    label "specjalista"
  ]
  node [
    id 661
    label "makija&#380;ysta"
  ]
  node [
    id 662
    label "odgrzebywa&#263;"
  ]
  node [
    id 663
    label "disinter"
  ]
  node [
    id 664
    label "odgrzeba&#263;"
  ]
  node [
    id 665
    label "popio&#322;y"
  ]
  node [
    id 666
    label "obrz&#281;d"
  ]
  node [
    id 667
    label "zabieg"
  ]
  node [
    id 668
    label "badanie"
  ]
  node [
    id 669
    label "relation"
  ]
  node [
    id 670
    label "urz&#261;d"
  ]
  node [
    id 671
    label "autopsy"
  ]
  node [
    id 672
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 673
    label "miejsce_pracy"
  ]
  node [
    id 674
    label "podsekcja"
  ]
  node [
    id 675
    label "insourcing"
  ]
  node [
    id 676
    label "ministerstwo"
  ]
  node [
    id 677
    label "orkiestra"
  ]
  node [
    id 678
    label "dzia&#322;"
  ]
  node [
    id 679
    label "odgrzebywanie"
  ]
  node [
    id 680
    label "odgrzebanie"
  ]
  node [
    id 681
    label "exhumation"
  ]
  node [
    id 682
    label "&#347;mier&#263;"
  ]
  node [
    id 683
    label "niepowodzenie"
  ]
  node [
    id 684
    label "stypa"
  ]
  node [
    id 685
    label "pusta_noc"
  ]
  node [
    id 686
    label "grabarz"
  ]
  node [
    id 687
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 688
    label "konserwowanie"
  ]
  node [
    id 689
    label "embalmment"
  ]
  node [
    id 690
    label "rytm"
  ]
  node [
    id 691
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 692
    label "tautochrona"
  ]
  node [
    id 693
    label "denga"
  ]
  node [
    id 694
    label "termoelektryczno&#347;&#263;"
  ]
  node [
    id 695
    label "zapalenie_opon_m&#243;zgowo-rdzeniowych"
  ]
  node [
    id 696
    label "oznaka"
  ]
  node [
    id 697
    label "emocja"
  ]
  node [
    id 698
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 699
    label "hotness"
  ]
  node [
    id 700
    label "choroba_bosto&#324;ska"
  ]
  node [
    id 701
    label "atmosfera"
  ]
  node [
    id 702
    label "rozpalony"
  ]
  node [
    id 703
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 704
    label "zagrza&#263;"
  ]
  node [
    id 705
    label "termoczu&#322;y"
  ]
  node [
    id 706
    label "wymiar"
  ]
  node [
    id 707
    label "&#347;ciana"
  ]
  node [
    id 708
    label "surface"
  ]
  node [
    id 709
    label "zakres"
  ]
  node [
    id 710
    label "kwadrant"
  ]
  node [
    id 711
    label "degree"
  ]
  node [
    id 712
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 713
    label "powierzchnia"
  ]
  node [
    id 714
    label "ukszta&#322;towanie"
  ]
  node [
    id 715
    label "p&#322;aszczak"
  ]
  node [
    id 716
    label "przecina&#263;"
  ]
  node [
    id 717
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 718
    label "robi&#263;"
  ]
  node [
    id 719
    label "unboxing"
  ]
  node [
    id 720
    label "zaczyna&#263;"
  ]
  node [
    id 721
    label "train"
  ]
  node [
    id 722
    label "uruchamia&#263;"
  ]
  node [
    id 723
    label "powodowa&#263;"
  ]
  node [
    id 724
    label "begin"
  ]
  node [
    id 725
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 726
    label "przeci&#261;&#263;"
  ]
  node [
    id 727
    label "udost&#281;pni&#263;"
  ]
  node [
    id 728
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 729
    label "establish"
  ]
  node [
    id 730
    label "uruchomi&#263;"
  ]
  node [
    id 731
    label "zacz&#261;&#263;"
  ]
  node [
    id 732
    label "odprowadza&#263;"
  ]
  node [
    id 733
    label "drain"
  ]
  node [
    id 734
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 735
    label "osusza&#263;"
  ]
  node [
    id 736
    label "odci&#261;ga&#263;"
  ]
  node [
    id 737
    label "odsuwa&#263;"
  ]
  node [
    id 738
    label "odprowadzanie"
  ]
  node [
    id 739
    label "powodowanie"
  ]
  node [
    id 740
    label "odci&#261;ganie"
  ]
  node [
    id 741
    label "dehydratacja"
  ]
  node [
    id 742
    label "osuszanie"
  ]
  node [
    id 743
    label "proces_chemiczny"
  ]
  node [
    id 744
    label "odsuwanie"
  ]
  node [
    id 745
    label "odsun&#261;&#263;"
  ]
  node [
    id 746
    label "odprowadzi&#263;"
  ]
  node [
    id 747
    label "osuszy&#263;"
  ]
  node [
    id 748
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 749
    label "rozk&#322;adanie"
  ]
  node [
    id 750
    label "zaczynanie"
  ]
  node [
    id 751
    label "opening"
  ]
  node [
    id 752
    label "operowanie"
  ]
  node [
    id 753
    label "udost&#281;pnianie"
  ]
  node [
    id 754
    label "przecinanie"
  ]
  node [
    id 755
    label "dehydration"
  ]
  node [
    id 756
    label "osuszenie"
  ]
  node [
    id 757
    label "odprowadzenie"
  ]
  node [
    id 758
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 759
    label "odsuni&#281;cie"
  ]
  node [
    id 760
    label "pootwieranie"
  ]
  node [
    id 761
    label "udost&#281;pnienie"
  ]
  node [
    id 762
    label "przeci&#281;cie"
  ]
  node [
    id 763
    label "zacz&#281;cie"
  ]
  node [
    id 764
    label "rozpostarcie"
  ]
  node [
    id 765
    label "rozprz&#261;c"
  ]
  node [
    id 766
    label "treaty"
  ]
  node [
    id 767
    label "systemat"
  ]
  node [
    id 768
    label "umowa"
  ]
  node [
    id 769
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 770
    label "usenet"
  ]
  node [
    id 771
    label "przestawi&#263;"
  ]
  node [
    id 772
    label "alliance"
  ]
  node [
    id 773
    label "ONZ"
  ]
  node [
    id 774
    label "NATO"
  ]
  node [
    id 775
    label "konstelacja"
  ]
  node [
    id 776
    label "o&#347;"
  ]
  node [
    id 777
    label "podsystem"
  ]
  node [
    id 778
    label "zawarcie"
  ]
  node [
    id 779
    label "zawrze&#263;"
  ]
  node [
    id 780
    label "organ"
  ]
  node [
    id 781
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 782
    label "wi&#281;&#378;"
  ]
  node [
    id 783
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 784
    label "zachowanie"
  ]
  node [
    id 785
    label "cybernetyk"
  ]
  node [
    id 786
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 787
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 788
    label "sk&#322;ad"
  ]
  node [
    id 789
    label "traktat_wersalski"
  ]
  node [
    id 790
    label "warunek_lokalowy"
  ]
  node [
    id 791
    label "plac"
  ]
  node [
    id 792
    label "location"
  ]
  node [
    id 793
    label "uwaga"
  ]
  node [
    id 794
    label "status"
  ]
  node [
    id 795
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 796
    label "rz&#261;d"
  ]
  node [
    id 797
    label "nerw"
  ]
  node [
    id 798
    label "zaty&#322;"
  ]
  node [
    id 799
    label "pupa"
  ]
  node [
    id 800
    label "strona"
  ]
  node [
    id 801
    label "documentation"
  ]
  node [
    id 802
    label "wi&#281;zozrost"
  ]
  node [
    id 803
    label "zasadzi&#263;"
  ]
  node [
    id 804
    label "punkt_odniesienia"
  ]
  node [
    id 805
    label "zasadzenie"
  ]
  node [
    id 806
    label "skeletal_system"
  ]
  node [
    id 807
    label "miednica"
  ]
  node [
    id 808
    label "szkielet_osiowy"
  ]
  node [
    id 809
    label "pas_barkowy"
  ]
  node [
    id 810
    label "ko&#347;&#263;"
  ]
  node [
    id 811
    label "konstrukcja"
  ]
  node [
    id 812
    label "dystraktor"
  ]
  node [
    id 813
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 814
    label "chrz&#261;stka"
  ]
  node [
    id 815
    label "kr&#261;&#380;ek_stawowy"
  ]
  node [
    id 816
    label "panewka"
  ]
  node [
    id 817
    label "kongruencja"
  ]
  node [
    id 818
    label "&#347;lizg_stawowy"
  ]
  node [
    id 819
    label "odprowadzalnik"
  ]
  node [
    id 820
    label "ogr&#243;d_wodny"
  ]
  node [
    id 821
    label "zbiornik_wodny"
  ]
  node [
    id 822
    label "koksartroza"
  ]
  node [
    id 823
    label "t&#281;tnica_&#380;o&#322;&#261;dkowa"
  ]
  node [
    id 824
    label "t&#281;tnica_biodrowa_zewn&#281;trzna"
  ]
  node [
    id 825
    label "pie&#324;_trzewny"
  ]
  node [
    id 826
    label "t&#281;tnica_biodrowa_wsp&#243;lna"
  ]
  node [
    id 827
    label "patroszy&#263;"
  ]
  node [
    id 828
    label "patroszenie"
  ]
  node [
    id 829
    label "gore"
  ]
  node [
    id 830
    label "t&#281;tnica_biodrowa_wewn&#281;trzna"
  ]
  node [
    id 831
    label "kiszki"
  ]
  node [
    id 832
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 833
    label "strzyc"
  ]
  node [
    id 834
    label "ostrzy&#380;enie"
  ]
  node [
    id 835
    label "strzy&#380;enie"
  ]
  node [
    id 836
    label "klata"
  ]
  node [
    id 837
    label "sze&#347;ciopak"
  ]
  node [
    id 838
    label "mi&#281;sie&#324;"
  ]
  node [
    id 839
    label "muscular_structure"
  ]
  node [
    id 840
    label "mi&#281;sie&#324;_l&#281;d&#378;wiowy"
  ]
  node [
    id 841
    label "genitalia"
  ]
  node [
    id 842
    label "plecy"
  ]
  node [
    id 843
    label "intymny"
  ]
  node [
    id 844
    label "okolica"
  ]
  node [
    id 845
    label "nerw_guziczny"
  ]
  node [
    id 846
    label "szczupak"
  ]
  node [
    id 847
    label "coating"
  ]
  node [
    id 848
    label "krupon"
  ]
  node [
    id 849
    label "harleyowiec"
  ]
  node [
    id 850
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 851
    label "kurtka"
  ]
  node [
    id 852
    label "metal"
  ]
  node [
    id 853
    label "p&#322;aszcz"
  ]
  node [
    id 854
    label "&#322;upa"
  ]
  node [
    id 855
    label "wyprze&#263;"
  ]
  node [
    id 856
    label "okrywa"
  ]
  node [
    id 857
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 858
    label "&#380;ycie"
  ]
  node [
    id 859
    label "gruczo&#322;_potowy"
  ]
  node [
    id 860
    label "lico"
  ]
  node [
    id 861
    label "wi&#243;rkownik"
  ]
  node [
    id 862
    label "mizdra"
  ]
  node [
    id 863
    label "dupa"
  ]
  node [
    id 864
    label "rockers"
  ]
  node [
    id 865
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 866
    label "surowiec"
  ]
  node [
    id 867
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 868
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 869
    label "pow&#322;oka"
  ]
  node [
    id 870
    label "zdrowie"
  ]
  node [
    id 871
    label "wyprawa"
  ]
  node [
    id 872
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 873
    label "hardrockowiec"
  ]
  node [
    id 874
    label "nask&#243;rek"
  ]
  node [
    id 875
    label "gestapowiec"
  ]
  node [
    id 876
    label "shell"
  ]
  node [
    id 877
    label "podmiot"
  ]
  node [
    id 878
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 879
    label "ptaszek"
  ]
  node [
    id 880
    label "organizacja"
  ]
  node [
    id 881
    label "element_anatomiczny"
  ]
  node [
    id 882
    label "przyrodzenie"
  ]
  node [
    id 883
    label "fiut"
  ]
  node [
    id 884
    label "shaft"
  ]
  node [
    id 885
    label "wchodzenie"
  ]
  node [
    id 886
    label "przedstawiciel"
  ]
  node [
    id 887
    label "wej&#347;cie"
  ]
  node [
    id 888
    label "skrusze&#263;"
  ]
  node [
    id 889
    label "luzowanie"
  ]
  node [
    id 890
    label "t&#322;uczenie"
  ]
  node [
    id 891
    label "wyluzowanie"
  ]
  node [
    id 892
    label "ut&#322;uczenie"
  ]
  node [
    id 893
    label "tempeh"
  ]
  node [
    id 894
    label "produkt"
  ]
  node [
    id 895
    label "krusze&#263;"
  ]
  node [
    id 896
    label "seitan"
  ]
  node [
    id 897
    label "chabanina"
  ]
  node [
    id 898
    label "luzowa&#263;"
  ]
  node [
    id 899
    label "marynata"
  ]
  node [
    id 900
    label "wyluzowa&#263;"
  ]
  node [
    id 901
    label "potrawa"
  ]
  node [
    id 902
    label "obieralnia"
  ]
  node [
    id 903
    label "panierka"
  ]
  node [
    id 904
    label "przekazywa&#263;"
  ]
  node [
    id 905
    label "zbiera&#263;"
  ]
  node [
    id 906
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 907
    label "przywraca&#263;"
  ]
  node [
    id 908
    label "dawa&#263;"
  ]
  node [
    id 909
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 910
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 911
    label "convey"
  ]
  node [
    id 912
    label "publicize"
  ]
  node [
    id 913
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 914
    label "render"
  ]
  node [
    id 915
    label "uk&#322;ada&#263;"
  ]
  node [
    id 916
    label "opracowywa&#263;"
  ]
  node [
    id 917
    label "set"
  ]
  node [
    id 918
    label "oddawa&#263;"
  ]
  node [
    id 919
    label "zmienia&#263;"
  ]
  node [
    id 920
    label "dzieli&#263;"
  ]
  node [
    id 921
    label "scala&#263;"
  ]
  node [
    id 922
    label "zestaw"
  ]
  node [
    id 923
    label "divide"
  ]
  node [
    id 924
    label "posiada&#263;"
  ]
  node [
    id 925
    label "deal"
  ]
  node [
    id 926
    label "cover"
  ]
  node [
    id 927
    label "liczy&#263;"
  ]
  node [
    id 928
    label "assign"
  ]
  node [
    id 929
    label "korzysta&#263;"
  ]
  node [
    id 930
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 931
    label "digest"
  ]
  node [
    id 932
    label "share"
  ]
  node [
    id 933
    label "iloraz"
  ]
  node [
    id 934
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 935
    label "rozdawa&#263;"
  ]
  node [
    id 936
    label "sprawowa&#263;"
  ]
  node [
    id 937
    label "dostarcza&#263;"
  ]
  node [
    id 938
    label "sacrifice"
  ]
  node [
    id 939
    label "odst&#281;powa&#263;"
  ]
  node [
    id 940
    label "sprzedawa&#263;"
  ]
  node [
    id 941
    label "reflect"
  ]
  node [
    id 942
    label "surrender"
  ]
  node [
    id 943
    label "deliver"
  ]
  node [
    id 944
    label "odpowiada&#263;"
  ]
  node [
    id 945
    label "umieszcza&#263;"
  ]
  node [
    id 946
    label "blurt_out"
  ]
  node [
    id 947
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 948
    label "przedstawia&#263;"
  ]
  node [
    id 949
    label "impart"
  ]
  node [
    id 950
    label "przejmowa&#263;"
  ]
  node [
    id 951
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 952
    label "gromadzi&#263;"
  ]
  node [
    id 953
    label "mie&#263;_miejsce"
  ]
  node [
    id 954
    label "bra&#263;"
  ]
  node [
    id 955
    label "pozyskiwa&#263;"
  ]
  node [
    id 956
    label "poci&#261;ga&#263;"
  ]
  node [
    id 957
    label "wzbiera&#263;"
  ]
  node [
    id 958
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 959
    label "meet"
  ]
  node [
    id 960
    label "dostawa&#263;"
  ]
  node [
    id 961
    label "consolidate"
  ]
  node [
    id 962
    label "congregate"
  ]
  node [
    id 963
    label "postpone"
  ]
  node [
    id 964
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 965
    label "znosi&#263;"
  ]
  node [
    id 966
    label "chroni&#263;"
  ]
  node [
    id 967
    label "darowywa&#263;"
  ]
  node [
    id 968
    label "preserve"
  ]
  node [
    id 969
    label "zachowywa&#263;"
  ]
  node [
    id 970
    label "gospodarowa&#263;"
  ]
  node [
    id 971
    label "dispose"
  ]
  node [
    id 972
    label "uczy&#263;"
  ]
  node [
    id 973
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 974
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 975
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 976
    label "przygotowywa&#263;"
  ]
  node [
    id 977
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 978
    label "tworzy&#263;"
  ]
  node [
    id 979
    label "treser"
  ]
  node [
    id 980
    label "raise"
  ]
  node [
    id 981
    label "pozostawia&#263;"
  ]
  node [
    id 982
    label "psu&#263;"
  ]
  node [
    id 983
    label "wzbudza&#263;"
  ]
  node [
    id 984
    label "wk&#322;ada&#263;"
  ]
  node [
    id 985
    label "go"
  ]
  node [
    id 986
    label "inspirowa&#263;"
  ]
  node [
    id 987
    label "wpaja&#263;"
  ]
  node [
    id 988
    label "znak"
  ]
  node [
    id 989
    label "seat"
  ]
  node [
    id 990
    label "wygrywa&#263;"
  ]
  node [
    id 991
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 992
    label "go&#347;ci&#263;"
  ]
  node [
    id 993
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 994
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 995
    label "pour"
  ]
  node [
    id 996
    label "elaborate"
  ]
  node [
    id 997
    label "pokrywa&#263;"
  ]
  node [
    id 998
    label "traci&#263;"
  ]
  node [
    id 999
    label "alternate"
  ]
  node [
    id 1000
    label "change"
  ]
  node [
    id 1001
    label "reengineering"
  ]
  node [
    id 1002
    label "zast&#281;powa&#263;"
  ]
  node [
    id 1003
    label "sprawia&#263;"
  ]
  node [
    id 1004
    label "zyskiwa&#263;"
  ]
  node [
    id 1005
    label "przechodzi&#263;"
  ]
  node [
    id 1006
    label "consort"
  ]
  node [
    id 1007
    label "jednoczy&#263;"
  ]
  node [
    id 1008
    label "work"
  ]
  node [
    id 1009
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1010
    label "podawa&#263;"
  ]
  node [
    id 1011
    label "wp&#322;aca&#263;"
  ]
  node [
    id 1012
    label "sygna&#322;"
  ]
  node [
    id 1013
    label "doprowadza&#263;"
  ]
  node [
    id 1014
    label "&#322;adowa&#263;"
  ]
  node [
    id 1015
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1016
    label "przeznacza&#263;"
  ]
  node [
    id 1017
    label "traktowa&#263;"
  ]
  node [
    id 1018
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1019
    label "obiecywa&#263;"
  ]
  node [
    id 1020
    label "tender"
  ]
  node [
    id 1021
    label "rap"
  ]
  node [
    id 1022
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 1023
    label "t&#322;uc"
  ]
  node [
    id 1024
    label "powierza&#263;"
  ]
  node [
    id 1025
    label "wpiernicza&#263;"
  ]
  node [
    id 1026
    label "exsert"
  ]
  node [
    id 1027
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1028
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 1029
    label "p&#322;aci&#263;"
  ]
  node [
    id 1030
    label "hold_out"
  ]
  node [
    id 1031
    label "nalewa&#263;"
  ]
  node [
    id 1032
    label "zezwala&#263;"
  ]
  node [
    id 1033
    label "hold"
  ]
  node [
    id 1034
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1035
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 1036
    label "relate"
  ]
  node [
    id 1037
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 1038
    label "dopieprza&#263;"
  ]
  node [
    id 1039
    label "press"
  ]
  node [
    id 1040
    label "urge"
  ]
  node [
    id 1041
    label "zbli&#380;a&#263;"
  ]
  node [
    id 1042
    label "przykrochmala&#263;"
  ]
  node [
    id 1043
    label "uderza&#263;"
  ]
  node [
    id 1044
    label "gem"
  ]
  node [
    id 1045
    label "kompozycja"
  ]
  node [
    id 1046
    label "runda"
  ]
  node [
    id 1047
    label "muzyka"
  ]
  node [
    id 1048
    label "stage_set"
  ]
  node [
    id 1049
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1050
    label "liczba"
  ]
  node [
    id 1051
    label "kategoria"
  ]
  node [
    id 1052
    label "pierwiastek"
  ]
  node [
    id 1053
    label "rozmiar"
  ]
  node [
    id 1054
    label "number"
  ]
  node [
    id 1055
    label "kwadrat_magiczny"
  ]
  node [
    id 1056
    label "wyra&#380;enie"
  ]
  node [
    id 1057
    label "elektron"
  ]
  node [
    id 1058
    label "pow&#322;oka_elektronowa"
  ]
  node [
    id 1059
    label "mikrokosmos"
  ]
  node [
    id 1060
    label "cz&#261;steczka"
  ]
  node [
    id 1061
    label "masa_atomowa"
  ]
  node [
    id 1062
    label "cz&#261;stka_elementarna"
  ]
  node [
    id 1063
    label "diadochia"
  ]
  node [
    id 1064
    label "rdze&#324;_atomowy"
  ]
  node [
    id 1065
    label "j&#261;dro_atomowe"
  ]
  node [
    id 1066
    label "liczba_atomowa"
  ]
  node [
    id 1067
    label "konfiguracja"
  ]
  node [
    id 1068
    label "cz&#261;stka"
  ]
  node [
    id 1069
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 1070
    label "substancja"
  ]
  node [
    id 1071
    label "grupa_funkcyjna"
  ]
  node [
    id 1072
    label "substancja_chemiczna"
  ]
  node [
    id 1073
    label "morfem"
  ]
  node [
    id 1074
    label "sk&#322;adnik"
  ]
  node [
    id 1075
    label "root"
  ]
  node [
    id 1076
    label "delokalizacja_elektron&#243;w"
  ]
  node [
    id 1077
    label "lepton"
  ]
  node [
    id 1078
    label "rodnik"
  ]
  node [
    id 1079
    label "stop"
  ]
  node [
    id 1080
    label "electron"
  ]
  node [
    id 1081
    label "gaz_Fermiego"
  ]
  node [
    id 1082
    label "wi&#261;zanie_kowalencyjne"
  ]
  node [
    id 1083
    label "przyroda"
  ]
  node [
    id 1084
    label "Ziemia"
  ]
  node [
    id 1085
    label "kosmos"
  ]
  node [
    id 1086
    label "miniatura"
  ]
  node [
    id 1087
    label "jon"
  ]
  node [
    id 1088
    label "krystalizowanie"
  ]
  node [
    id 1089
    label "krystalizacja"
  ]
  node [
    id 1090
    label "Rzym_Zachodni"
  ]
  node [
    id 1091
    label "ilo&#347;&#263;"
  ]
  node [
    id 1092
    label "Rzym_Wschodni"
  ]
  node [
    id 1093
    label "urz&#261;dzenie"
  ]
  node [
    id 1094
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1095
    label "equal"
  ]
  node [
    id 1096
    label "trwa&#263;"
  ]
  node [
    id 1097
    label "chodzi&#263;"
  ]
  node [
    id 1098
    label "si&#281;ga&#263;"
  ]
  node [
    id 1099
    label "stan"
  ]
  node [
    id 1100
    label "obecno&#347;&#263;"
  ]
  node [
    id 1101
    label "stand"
  ]
  node [
    id 1102
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1103
    label "uczestniczy&#263;"
  ]
  node [
    id 1104
    label "participate"
  ]
  node [
    id 1105
    label "istnie&#263;"
  ]
  node [
    id 1106
    label "pozostawa&#263;"
  ]
  node [
    id 1107
    label "zostawa&#263;"
  ]
  node [
    id 1108
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1109
    label "adhere"
  ]
  node [
    id 1110
    label "compass"
  ]
  node [
    id 1111
    label "appreciation"
  ]
  node [
    id 1112
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1113
    label "dociera&#263;"
  ]
  node [
    id 1114
    label "get"
  ]
  node [
    id 1115
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1116
    label "mierzy&#263;"
  ]
  node [
    id 1117
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1118
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1119
    label "being"
  ]
  node [
    id 1120
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1121
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1122
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1123
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1124
    label "run"
  ]
  node [
    id 1125
    label "bangla&#263;"
  ]
  node [
    id 1126
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1127
    label "przebiega&#263;"
  ]
  node [
    id 1128
    label "proceed"
  ]
  node [
    id 1129
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1130
    label "carry"
  ]
  node [
    id 1131
    label "bywa&#263;"
  ]
  node [
    id 1132
    label "dziama&#263;"
  ]
  node [
    id 1133
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1134
    label "para"
  ]
  node [
    id 1135
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1136
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1137
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1138
    label "krok"
  ]
  node [
    id 1139
    label "tryb"
  ]
  node [
    id 1140
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1141
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1142
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1143
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1144
    label "continue"
  ]
  node [
    id 1145
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1146
    label "Ohio"
  ]
  node [
    id 1147
    label "wci&#281;cie"
  ]
  node [
    id 1148
    label "Nowy_York"
  ]
  node [
    id 1149
    label "warstwa"
  ]
  node [
    id 1150
    label "samopoczucie"
  ]
  node [
    id 1151
    label "Illinois"
  ]
  node [
    id 1152
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1153
    label "state"
  ]
  node [
    id 1154
    label "Jukatan"
  ]
  node [
    id 1155
    label "Kalifornia"
  ]
  node [
    id 1156
    label "Wirginia"
  ]
  node [
    id 1157
    label "wektor"
  ]
  node [
    id 1158
    label "Goa"
  ]
  node [
    id 1159
    label "Teksas"
  ]
  node [
    id 1160
    label "Waszyngton"
  ]
  node [
    id 1161
    label "Massachusetts"
  ]
  node [
    id 1162
    label "Alaska"
  ]
  node [
    id 1163
    label "Arakan"
  ]
  node [
    id 1164
    label "Hawaje"
  ]
  node [
    id 1165
    label "Maryland"
  ]
  node [
    id 1166
    label "Michigan"
  ]
  node [
    id 1167
    label "Arizona"
  ]
  node [
    id 1168
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1169
    label "Georgia"
  ]
  node [
    id 1170
    label "poziom"
  ]
  node [
    id 1171
    label "Pensylwania"
  ]
  node [
    id 1172
    label "shape"
  ]
  node [
    id 1173
    label "Luizjana"
  ]
  node [
    id 1174
    label "Nowy_Meksyk"
  ]
  node [
    id 1175
    label "Alabama"
  ]
  node [
    id 1176
    label "Kansas"
  ]
  node [
    id 1177
    label "Oregon"
  ]
  node [
    id 1178
    label "Oklahoma"
  ]
  node [
    id 1179
    label "Floryda"
  ]
  node [
    id 1180
    label "jednostka_administracyjna"
  ]
  node [
    id 1181
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1182
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1183
    label "wytwarza&#263;"
  ]
  node [
    id 1184
    label "create"
  ]
  node [
    id 1185
    label "rola"
  ]
  node [
    id 1186
    label "organizowa&#263;"
  ]
  node [
    id 1187
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1188
    label "czyni&#263;"
  ]
  node [
    id 1189
    label "stylizowa&#263;"
  ]
  node [
    id 1190
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1191
    label "falowa&#263;"
  ]
  node [
    id 1192
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1193
    label "peddle"
  ]
  node [
    id 1194
    label "wydala&#263;"
  ]
  node [
    id 1195
    label "tentegowa&#263;"
  ]
  node [
    id 1196
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1197
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1198
    label "oszukiwa&#263;"
  ]
  node [
    id 1199
    label "ukazywa&#263;"
  ]
  node [
    id 1200
    label "przerabia&#263;"
  ]
  node [
    id 1201
    label "post&#281;powa&#263;"
  ]
  node [
    id 1202
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1203
    label "najem"
  ]
  node [
    id 1204
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1205
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1206
    label "zak&#322;ad"
  ]
  node [
    id 1207
    label "stosunek_pracy"
  ]
  node [
    id 1208
    label "benedykty&#324;ski"
  ]
  node [
    id 1209
    label "poda&#380;_pracy"
  ]
  node [
    id 1210
    label "pracowanie"
  ]
  node [
    id 1211
    label "tyrka"
  ]
  node [
    id 1212
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1213
    label "zaw&#243;d"
  ]
  node [
    id 1214
    label "tynkarski"
  ]
  node [
    id 1215
    label "pracowa&#263;"
  ]
  node [
    id 1216
    label "zmiana"
  ]
  node [
    id 1217
    label "czynnik_produkcji"
  ]
  node [
    id 1218
    label "zobowi&#261;zanie"
  ]
  node [
    id 1219
    label "kierownictwo"
  ]
  node [
    id 1220
    label "siedziba"
  ]
  node [
    id 1221
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1222
    label "uprawienie"
  ]
  node [
    id 1223
    label "kszta&#322;t"
  ]
  node [
    id 1224
    label "dialog"
  ]
  node [
    id 1225
    label "p&#322;osa"
  ]
  node [
    id 1226
    label "wykonywanie"
  ]
  node [
    id 1227
    label "plik"
  ]
  node [
    id 1228
    label "ziemia"
  ]
  node [
    id 1229
    label "ustawienie"
  ]
  node [
    id 1230
    label "scenariusz"
  ]
  node [
    id 1231
    label "pole"
  ]
  node [
    id 1232
    label "gospodarstwo"
  ]
  node [
    id 1233
    label "uprawi&#263;"
  ]
  node [
    id 1234
    label "zreinterpretowa&#263;"
  ]
  node [
    id 1235
    label "reinterpretowa&#263;"
  ]
  node [
    id 1236
    label "wrench"
  ]
  node [
    id 1237
    label "irygowanie"
  ]
  node [
    id 1238
    label "irygowa&#263;"
  ]
  node [
    id 1239
    label "zreinterpretowanie"
  ]
  node [
    id 1240
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 1241
    label "gra&#263;"
  ]
  node [
    id 1242
    label "aktorstwo"
  ]
  node [
    id 1243
    label "kostium"
  ]
  node [
    id 1244
    label "zagon"
  ]
  node [
    id 1245
    label "reinterpretowanie"
  ]
  node [
    id 1246
    label "tekst"
  ]
  node [
    id 1247
    label "zagranie"
  ]
  node [
    id 1248
    label "radlina"
  ]
  node [
    id 1249
    label "granie"
  ]
  node [
    id 1250
    label "wokalistyka"
  ]
  node [
    id 1251
    label "muza"
  ]
  node [
    id 1252
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 1253
    label "beatbox"
  ]
  node [
    id 1254
    label "komponowa&#263;"
  ]
  node [
    id 1255
    label "szko&#322;a"
  ]
  node [
    id 1256
    label "komponowanie"
  ]
  node [
    id 1257
    label "pasa&#380;"
  ]
  node [
    id 1258
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1259
    label "notacja_muzyczna"
  ]
  node [
    id 1260
    label "kontrapunkt"
  ]
  node [
    id 1261
    label "sztuka"
  ]
  node [
    id 1262
    label "instrumentalistyka"
  ]
  node [
    id 1263
    label "harmonia"
  ]
  node [
    id 1264
    label "wys&#322;uchanie"
  ]
  node [
    id 1265
    label "kapela"
  ]
  node [
    id 1266
    label "britpop"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 840
  ]
  edge [
    source 8
    target 841
  ]
  edge [
    source 8
    target 842
  ]
  edge [
    source 8
    target 843
  ]
  edge [
    source 8
    target 844
  ]
  edge [
    source 8
    target 845
  ]
  edge [
    source 8
    target 846
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 848
  ]
  edge [
    source 8
    target 849
  ]
  edge [
    source 8
    target 850
  ]
  edge [
    source 8
    target 851
  ]
  edge [
    source 8
    target 852
  ]
  edge [
    source 8
    target 853
  ]
  edge [
    source 8
    target 854
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 856
  ]
  edge [
    source 8
    target 857
  ]
  edge [
    source 8
    target 858
  ]
  edge [
    source 8
    target 859
  ]
  edge [
    source 8
    target 860
  ]
  edge [
    source 8
    target 861
  ]
  edge [
    source 8
    target 862
  ]
  edge [
    source 8
    target 863
  ]
  edge [
    source 8
    target 864
  ]
  edge [
    source 8
    target 865
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 867
  ]
  edge [
    source 8
    target 868
  ]
  edge [
    source 8
    target 869
  ]
  edge [
    source 8
    target 870
  ]
  edge [
    source 8
    target 871
  ]
  edge [
    source 8
    target 872
  ]
  edge [
    source 8
    target 873
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 875
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 876
  ]
  edge [
    source 8
    target 877
  ]
  edge [
    source 8
    target 878
  ]
  edge [
    source 8
    target 879
  ]
  edge [
    source 8
    target 880
  ]
  edge [
    source 8
    target 881
  ]
  edge [
    source 8
    target 882
  ]
  edge [
    source 8
    target 883
  ]
  edge [
    source 8
    target 884
  ]
  edge [
    source 8
    target 885
  ]
  edge [
    source 8
    target 886
  ]
  edge [
    source 8
    target 887
  ]
  edge [
    source 8
    target 888
  ]
  edge [
    source 8
    target 889
  ]
  edge [
    source 8
    target 890
  ]
  edge [
    source 8
    target 891
  ]
  edge [
    source 8
    target 892
  ]
  edge [
    source 8
    target 893
  ]
  edge [
    source 8
    target 894
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 895
  ]
  edge [
    source 8
    target 896
  ]
  edge [
    source 8
    target 897
  ]
  edge [
    source 8
    target 898
  ]
  edge [
    source 8
    target 899
  ]
  edge [
    source 8
    target 900
  ]
  edge [
    source 8
    target 901
  ]
  edge [
    source 8
    target 902
  ]
  edge [
    source 8
    target 903
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 9
    target 906
  ]
  edge [
    source 9
    target 907
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 909
  ]
  edge [
    source 9
    target 910
  ]
  edge [
    source 9
    target 911
  ]
  edge [
    source 9
    target 912
  ]
  edge [
    source 9
    target 913
  ]
  edge [
    source 9
    target 914
  ]
  edge [
    source 9
    target 915
  ]
  edge [
    source 9
    target 916
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 942
  ]
  edge [
    source 9
    target 943
  ]
  edge [
    source 9
    target 944
  ]
  edge [
    source 9
    target 945
  ]
  edge [
    source 9
    target 946
  ]
  edge [
    source 9
    target 947
  ]
  edge [
    source 9
    target 948
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 9
    target 952
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 9
    target 962
  ]
  edge [
    source 9
    target 963
  ]
  edge [
    source 9
    target 964
  ]
  edge [
    source 9
    target 965
  ]
  edge [
    source 9
    target 966
  ]
  edge [
    source 9
    target 967
  ]
  edge [
    source 9
    target 968
  ]
  edge [
    source 9
    target 969
  ]
  edge [
    source 9
    target 970
  ]
  edge [
    source 9
    target 971
  ]
  edge [
    source 9
    target 972
  ]
  edge [
    source 9
    target 973
  ]
  edge [
    source 9
    target 974
  ]
  edge [
    source 9
    target 975
  ]
  edge [
    source 9
    target 976
  ]
  edge [
    source 9
    target 977
  ]
  edge [
    source 9
    target 978
  ]
  edge [
    source 9
    target 979
  ]
  edge [
    source 9
    target 980
  ]
  edge [
    source 9
    target 981
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 982
  ]
  edge [
    source 9
    target 983
  ]
  edge [
    source 9
    target 984
  ]
  edge [
    source 9
    target 985
  ]
  edge [
    source 9
    target 986
  ]
  edge [
    source 9
    target 987
  ]
  edge [
    source 9
    target 988
  ]
  edge [
    source 9
    target 989
  ]
  edge [
    source 9
    target 990
  ]
  edge [
    source 9
    target 991
  ]
  edge [
    source 9
    target 992
  ]
  edge [
    source 9
    target 993
  ]
  edge [
    source 9
    target 994
  ]
  edge [
    source 9
    target 995
  ]
  edge [
    source 9
    target 996
  ]
  edge [
    source 9
    target 997
  ]
  edge [
    source 9
    target 998
  ]
  edge [
    source 9
    target 999
  ]
  edge [
    source 9
    target 1000
  ]
  edge [
    source 9
    target 1001
  ]
  edge [
    source 9
    target 1002
  ]
  edge [
    source 9
    target 1003
  ]
  edge [
    source 9
    target 1004
  ]
  edge [
    source 9
    target 1005
  ]
  edge [
    source 9
    target 1006
  ]
  edge [
    source 9
    target 1007
  ]
  edge [
    source 9
    target 1008
  ]
  edge [
    source 9
    target 1009
  ]
  edge [
    source 9
    target 1010
  ]
  edge [
    source 9
    target 1011
  ]
  edge [
    source 9
    target 1012
  ]
  edge [
    source 9
    target 1013
  ]
  edge [
    source 9
    target 1014
  ]
  edge [
    source 9
    target 1015
  ]
  edge [
    source 9
    target 1016
  ]
  edge [
    source 9
    target 1017
  ]
  edge [
    source 9
    target 1018
  ]
  edge [
    source 9
    target 1019
  ]
  edge [
    source 9
    target 1020
  ]
  edge [
    source 9
    target 1021
  ]
  edge [
    source 9
    target 1022
  ]
  edge [
    source 9
    target 1023
  ]
  edge [
    source 9
    target 1024
  ]
  edge [
    source 9
    target 1025
  ]
  edge [
    source 9
    target 1026
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 1027
  ]
  edge [
    source 9
    target 1028
  ]
  edge [
    source 9
    target 1029
  ]
  edge [
    source 9
    target 1030
  ]
  edge [
    source 9
    target 1031
  ]
  edge [
    source 9
    target 1032
  ]
  edge [
    source 9
    target 1033
  ]
  edge [
    source 9
    target 1034
  ]
  edge [
    source 9
    target 1035
  ]
  edge [
    source 9
    target 1036
  ]
  edge [
    source 9
    target 1037
  ]
  edge [
    source 9
    target 1038
  ]
  edge [
    source 9
    target 1039
  ]
  edge [
    source 9
    target 1040
  ]
  edge [
    source 9
    target 1041
  ]
  edge [
    source 9
    target 1042
  ]
  edge [
    source 9
    target 1043
  ]
  edge [
    source 9
    target 1044
  ]
  edge [
    source 9
    target 1045
  ]
  edge [
    source 9
    target 1046
  ]
  edge [
    source 9
    target 1047
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 1048
  ]
  edge [
    source 9
    target 1049
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 282
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1057
  ]
  edge [
    source 13
    target 1058
  ]
  edge [
    source 13
    target 1059
  ]
  edge [
    source 13
    target 1060
  ]
  edge [
    source 13
    target 1061
  ]
  edge [
    source 13
    target 1062
  ]
  edge [
    source 13
    target 1063
  ]
  edge [
    source 13
    target 1064
  ]
  edge [
    source 13
    target 1065
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 1052
  ]
  edge [
    source 13
    target 1066
  ]
  edge [
    source 13
    target 1067
  ]
  edge [
    source 13
    target 1068
  ]
  edge [
    source 13
    target 1069
  ]
  edge [
    source 13
    target 1070
  ]
  edge [
    source 13
    target 1071
  ]
  edge [
    source 13
    target 1050
  ]
  edge [
    source 13
    target 1072
  ]
  edge [
    source 13
    target 1073
  ]
  edge [
    source 13
    target 1074
  ]
  edge [
    source 13
    target 1075
  ]
  edge [
    source 13
    target 1076
  ]
  edge [
    source 13
    target 1077
  ]
  edge [
    source 13
    target 1078
  ]
  edge [
    source 13
    target 1079
  ]
  edge [
    source 13
    target 1080
  ]
  edge [
    source 13
    target 1081
  ]
  edge [
    source 13
    target 1082
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 1083
  ]
  edge [
    source 13
    target 1084
  ]
  edge [
    source 13
    target 1085
  ]
  edge [
    source 13
    target 1086
  ]
  edge [
    source 13
    target 1087
  ]
  edge [
    source 13
    target 1088
  ]
  edge [
    source 13
    target 1089
  ]
  edge [
    source 13
    target 1090
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 1091
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 1092
  ]
  edge [
    source 13
    target 1093
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 260
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 339
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 245
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 1148
  ]
  edge [
    source 14
    target 1149
  ]
  edge [
    source 14
    target 1150
  ]
  edge [
    source 14
    target 1151
  ]
  edge [
    source 14
    target 1152
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 14
    target 1178
  ]
  edge [
    source 14
    target 1179
  ]
  edge [
    source 14
    target 1180
  ]
  edge [
    source 14
    target 1181
  ]
  edge [
    source 15
    target 1182
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 1183
  ]
  edge [
    source 15
    target 1047
  ]
  edge [
    source 15
    target 1008
  ]
  edge [
    source 15
    target 1184
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 1185
  ]
  edge [
    source 15
    target 1186
  ]
  edge [
    source 15
    target 1187
  ]
  edge [
    source 15
    target 1188
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 1189
  ]
  edge [
    source 15
    target 1190
  ]
  edge [
    source 15
    target 1191
  ]
  edge [
    source 15
    target 1192
  ]
  edge [
    source 15
    target 1193
  ]
  edge [
    source 15
    target 1194
  ]
  edge [
    source 15
    target 1195
  ]
  edge [
    source 15
    target 1196
  ]
  edge [
    source 15
    target 1197
  ]
  edge [
    source 15
    target 1198
  ]
  edge [
    source 15
    target 1199
  ]
  edge [
    source 15
    target 1200
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 1201
  ]
  edge [
    source 15
    target 1202
  ]
  edge [
    source 15
    target 1203
  ]
  edge [
    source 15
    target 1204
  ]
  edge [
    source 15
    target 1205
  ]
  edge [
    source 15
    target 1206
  ]
  edge [
    source 15
    target 1207
  ]
  edge [
    source 15
    target 1208
  ]
  edge [
    source 15
    target 1209
  ]
  edge [
    source 15
    target 1210
  ]
  edge [
    source 15
    target 1211
  ]
  edge [
    source 15
    target 1212
  ]
  edge [
    source 15
    target 459
  ]
  edge [
    source 15
    target 520
  ]
  edge [
    source 15
    target 1213
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 1214
  ]
  edge [
    source 15
    target 1215
  ]
  edge [
    source 15
    target 248
  ]
  edge [
    source 15
    target 1216
  ]
  edge [
    source 15
    target 1217
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1225
  ]
  edge [
    source 15
    target 1226
  ]
  edge [
    source 15
    target 1227
  ]
  edge [
    source 15
    target 1228
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 1229
  ]
  edge [
    source 15
    target 1230
  ]
  edge [
    source 15
    target 1231
  ]
  edge [
    source 15
    target 1232
  ]
  edge [
    source 15
    target 1233
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 460
  ]
  edge [
    source 15
    target 1234
  ]
  edge [
    source 15
    target 1235
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1237
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 1238
  ]
  edge [
    source 15
    target 1239
  ]
  edge [
    source 15
    target 513
  ]
  edge [
    source 15
    target 1240
  ]
  edge [
    source 15
    target 1241
  ]
  edge [
    source 15
    target 1242
  ]
  edge [
    source 15
    target 1243
  ]
  edge [
    source 15
    target 1244
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 1245
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 1246
  ]
  edge [
    source 15
    target 1247
  ]
  edge [
    source 15
    target 1248
  ]
  edge [
    source 15
    target 1249
  ]
  edge [
    source 15
    target 1250
  ]
  edge [
    source 15
    target 292
  ]
  edge [
    source 15
    target 1251
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 1252
  ]
  edge [
    source 15
    target 1253
  ]
  edge [
    source 15
    target 1254
  ]
  edge [
    source 15
    target 1255
  ]
  edge [
    source 15
    target 1256
  ]
  edge [
    source 15
    target 1257
  ]
  edge [
    source 15
    target 1258
  ]
  edge [
    source 15
    target 1259
  ]
  edge [
    source 15
    target 1260
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 1261
  ]
  edge [
    source 15
    target 1262
  ]
  edge [
    source 15
    target 1263
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 1264
  ]
  edge [
    source 15
    target 1265
  ]
  edge [
    source 15
    target 1266
  ]
]
