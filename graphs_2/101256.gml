graph [
  node [
    id 0
    label "promet"
    origin "text"
  ]
  node [
    id 1
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 2
    label "lantanowiec"
  ]
  node [
    id 3
    label "metal"
  ]
  node [
    id 4
    label "pierwiastek"
  ]
  node [
    id 5
    label "J"
  ]
  node [
    id 6
    label "albo"
  ]
  node [
    id 7
    label "Marinskyego"
  ]
  node [
    id 8
    label "litr"
  ]
  node [
    id 9
    label "e"
  ]
  node [
    id 10
    label "Glendenina"
  ]
  node [
    id 11
    label "c&#243;rka"
  ]
  node [
    id 12
    label "dzie&#324;"
  ]
  node [
    id 13
    label "Coryella"
  ]
  node [
    id 14
    label "HR"
  ]
  node [
    id 15
    label "465"
  ]
  node [
    id 16
    label "HD"
  ]
  node [
    id 17
    label "101065"
  ]
  node [
    id 18
    label "965"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
]
