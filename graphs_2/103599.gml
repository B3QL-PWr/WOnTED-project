graph [
  node [
    id 0
    label "przychodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pomoc"
    origin "text"
  ]
  node [
    id 2
    label "logarytm"
    origin "text"
  ]
  node [
    id 3
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 4
    label "bardzo"
    origin "text"
  ]
  node [
    id 5
    label "algorytm"
    origin "text"
  ]
  node [
    id 6
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 7
    label "guzik"
    origin "text"
  ]
  node [
    id 8
    label "pilot"
    origin "text"
  ]
  node [
    id 9
    label "wcisn&#261;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "pozwoli&#263;"
    origin "text"
  ]
  node [
    id 11
    label "siebie"
    origin "text"
  ]
  node [
    id 12
    label "za&#380;artowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pr&#243;bowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 15
    label "t&#322;umaczy&#263;"
    origin "text"
  ]
  node [
    id 16
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 17
    label "nie"
    origin "text"
  ]
  node [
    id 18
    label "zdolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "matematyka"
    origin "text"
  ]
  node [
    id 20
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 21
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 22
    label "tak"
    origin "text"
  ]
  node [
    id 23
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 24
    label "pewne"
    origin "text"
  ]
  node [
    id 25
    label "do&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 26
    label "pedagogiczny"
    origin "text"
  ]
  node [
    id 27
    label "dla"
    origin "text"
  ]
  node [
    id 28
    label "jasny"
    origin "text"
  ]
  node [
    id 29
    label "u&#380;ywanie"
    origin "text"
  ]
  node [
    id 30
    label "termin"
    origin "text"
  ]
  node [
    id 31
    label "logarytmowa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "dobre"
    origin "text"
  ]
  node [
    id 33
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 34
    label "przybywa&#263;"
  ]
  node [
    id 35
    label "dochodzi&#263;"
  ]
  node [
    id 36
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 37
    label "robi&#263;"
  ]
  node [
    id 38
    label "uzyskiwa&#263;"
  ]
  node [
    id 39
    label "claim"
  ]
  node [
    id 40
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 41
    label "osi&#261;ga&#263;"
  ]
  node [
    id 42
    label "ripen"
  ]
  node [
    id 43
    label "supervene"
  ]
  node [
    id 44
    label "doczeka&#263;"
  ]
  node [
    id 45
    label "przesy&#322;ka"
  ]
  node [
    id 46
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 47
    label "doznawa&#263;"
  ]
  node [
    id 48
    label "reach"
  ]
  node [
    id 49
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 50
    label "dociera&#263;"
  ]
  node [
    id 51
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 52
    label "zachodzi&#263;"
  ]
  node [
    id 53
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 54
    label "postrzega&#263;"
  ]
  node [
    id 55
    label "orgazm"
  ]
  node [
    id 56
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 57
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 58
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 59
    label "dokoptowywa&#263;"
  ]
  node [
    id 60
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 61
    label "dolatywa&#263;"
  ]
  node [
    id 62
    label "powodowa&#263;"
  ]
  node [
    id 63
    label "submit"
  ]
  node [
    id 64
    label "get"
  ]
  node [
    id 65
    label "zyskiwa&#263;"
  ]
  node [
    id 66
    label "&#347;rodek"
  ]
  node [
    id 67
    label "darowizna"
  ]
  node [
    id 68
    label "przedmiot"
  ]
  node [
    id 69
    label "liga"
  ]
  node [
    id 70
    label "doch&#243;d"
  ]
  node [
    id 71
    label "telefon_zaufania"
  ]
  node [
    id 72
    label "pomocnik"
  ]
  node [
    id 73
    label "zgodzi&#263;"
  ]
  node [
    id 74
    label "grupa"
  ]
  node [
    id 75
    label "property"
  ]
  node [
    id 76
    label "income"
  ]
  node [
    id 77
    label "stopa_procentowa"
  ]
  node [
    id 78
    label "krzywa_Engla"
  ]
  node [
    id 79
    label "korzy&#347;&#263;"
  ]
  node [
    id 80
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 81
    label "wp&#322;yw"
  ]
  node [
    id 82
    label "zboczenie"
  ]
  node [
    id 83
    label "om&#243;wienie"
  ]
  node [
    id 84
    label "sponiewieranie"
  ]
  node [
    id 85
    label "discipline"
  ]
  node [
    id 86
    label "rzecz"
  ]
  node [
    id 87
    label "omawia&#263;"
  ]
  node [
    id 88
    label "kr&#261;&#380;enie"
  ]
  node [
    id 89
    label "tre&#347;&#263;"
  ]
  node [
    id 90
    label "robienie"
  ]
  node [
    id 91
    label "sponiewiera&#263;"
  ]
  node [
    id 92
    label "element"
  ]
  node [
    id 93
    label "entity"
  ]
  node [
    id 94
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 95
    label "tematyka"
  ]
  node [
    id 96
    label "w&#261;tek"
  ]
  node [
    id 97
    label "charakter"
  ]
  node [
    id 98
    label "zbaczanie"
  ]
  node [
    id 99
    label "program_nauczania"
  ]
  node [
    id 100
    label "om&#243;wi&#263;"
  ]
  node [
    id 101
    label "omawianie"
  ]
  node [
    id 102
    label "thing"
  ]
  node [
    id 103
    label "kultura"
  ]
  node [
    id 104
    label "istota"
  ]
  node [
    id 105
    label "zbacza&#263;"
  ]
  node [
    id 106
    label "zboczy&#263;"
  ]
  node [
    id 107
    label "punkt"
  ]
  node [
    id 108
    label "spos&#243;b"
  ]
  node [
    id 109
    label "miejsce"
  ]
  node [
    id 110
    label "abstrakcja"
  ]
  node [
    id 111
    label "czas"
  ]
  node [
    id 112
    label "chemikalia"
  ]
  node [
    id 113
    label "substancja"
  ]
  node [
    id 114
    label "kredens"
  ]
  node [
    id 115
    label "cz&#322;owiek"
  ]
  node [
    id 116
    label "zawodnik"
  ]
  node [
    id 117
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 118
    label "bylina"
  ]
  node [
    id 119
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 120
    label "gracz"
  ]
  node [
    id 121
    label "r&#281;ka"
  ]
  node [
    id 122
    label "wrzosowate"
  ]
  node [
    id 123
    label "pomagacz"
  ]
  node [
    id 124
    label "odm&#322;adzanie"
  ]
  node [
    id 125
    label "jednostka_systematyczna"
  ]
  node [
    id 126
    label "asymilowanie"
  ]
  node [
    id 127
    label "gromada"
  ]
  node [
    id 128
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 129
    label "asymilowa&#263;"
  ]
  node [
    id 130
    label "egzemplarz"
  ]
  node [
    id 131
    label "Entuzjastki"
  ]
  node [
    id 132
    label "zbi&#243;r"
  ]
  node [
    id 133
    label "kompozycja"
  ]
  node [
    id 134
    label "Terranie"
  ]
  node [
    id 135
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 136
    label "category"
  ]
  node [
    id 137
    label "pakiet_klimatyczny"
  ]
  node [
    id 138
    label "oddzia&#322;"
  ]
  node [
    id 139
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 140
    label "cz&#261;steczka"
  ]
  node [
    id 141
    label "stage_set"
  ]
  node [
    id 142
    label "type"
  ]
  node [
    id 143
    label "specgrupa"
  ]
  node [
    id 144
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 145
    label "&#346;wietliki"
  ]
  node [
    id 146
    label "odm&#322;odzenie"
  ]
  node [
    id 147
    label "Eurogrupa"
  ]
  node [
    id 148
    label "odm&#322;adza&#263;"
  ]
  node [
    id 149
    label "formacja_geologiczna"
  ]
  node [
    id 150
    label "harcerze_starsi"
  ]
  node [
    id 151
    label "przeniesienie_praw"
  ]
  node [
    id 152
    label "zapomoga"
  ]
  node [
    id 153
    label "transakcja"
  ]
  node [
    id 154
    label "dar"
  ]
  node [
    id 155
    label "zatrudni&#263;"
  ]
  node [
    id 156
    label "zgodzenie"
  ]
  node [
    id 157
    label "zgadza&#263;"
  ]
  node [
    id 158
    label "mecz_mistrzowski"
  ]
  node [
    id 159
    label "&#347;rodowisko"
  ]
  node [
    id 160
    label "arrangement"
  ]
  node [
    id 161
    label "obrona"
  ]
  node [
    id 162
    label "organizacja"
  ]
  node [
    id 163
    label "poziom"
  ]
  node [
    id 164
    label "rezerwa"
  ]
  node [
    id 165
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 166
    label "pr&#243;ba"
  ]
  node [
    id 167
    label "atak"
  ]
  node [
    id 168
    label "moneta"
  ]
  node [
    id 169
    label "union"
  ]
  node [
    id 170
    label "mantysa"
  ]
  node [
    id 171
    label "logarithm"
  ]
  node [
    id 172
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 173
    label "cecha_logarytmu"
  ]
  node [
    id 174
    label "mantissa"
  ]
  node [
    id 175
    label "w_chuj"
  ]
  node [
    id 176
    label "algorithm"
  ]
  node [
    id 177
    label "formu&#322;a"
  ]
  node [
    id 178
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 179
    label "zapis"
  ]
  node [
    id 180
    label "formularz"
  ]
  node [
    id 181
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 182
    label "sformu&#322;owanie"
  ]
  node [
    id 183
    label "kultura_duchowa"
  ]
  node [
    id 184
    label "rule"
  ]
  node [
    id 185
    label "ceremony"
  ]
  node [
    id 186
    label "zapi&#281;cie"
  ]
  node [
    id 187
    label "filobutonista"
  ]
  node [
    id 188
    label "przycisk"
  ]
  node [
    id 189
    label "zero"
  ]
  node [
    id 190
    label "filobutonistyka"
  ]
  node [
    id 191
    label "knob"
  ]
  node [
    id 192
    label "guzikarz"
  ]
  node [
    id 193
    label "pasmanteria"
  ]
  node [
    id 194
    label "r&#243;g"
  ]
  node [
    id 195
    label "interfejs"
  ]
  node [
    id 196
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 197
    label "pole"
  ]
  node [
    id 198
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 199
    label "nacisk"
  ]
  node [
    id 200
    label "wymowa"
  ]
  node [
    id 201
    label "d&#378;wi&#281;k"
  ]
  node [
    id 202
    label "wyrostek"
  ]
  node [
    id 203
    label "aut_bramkowy"
  ]
  node [
    id 204
    label "podanie"
  ]
  node [
    id 205
    label "naczynie"
  ]
  node [
    id 206
    label "linia"
  ]
  node [
    id 207
    label "instrument_d&#281;ty"
  ]
  node [
    id 208
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 209
    label "tworzywo"
  ]
  node [
    id 210
    label "poro&#380;e"
  ]
  node [
    id 211
    label "zawarto&#347;&#263;"
  ]
  node [
    id 212
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 213
    label "zbieg"
  ]
  node [
    id 214
    label "kraw&#281;d&#378;"
  ]
  node [
    id 215
    label "liczba"
  ]
  node [
    id 216
    label "ilo&#347;&#263;"
  ]
  node [
    id 217
    label "podzia&#322;ka"
  ]
  node [
    id 218
    label "ciura"
  ]
  node [
    id 219
    label "cyfra"
  ]
  node [
    id 220
    label "miernota"
  ]
  node [
    id 221
    label "g&#243;wno"
  ]
  node [
    id 222
    label "love"
  ]
  node [
    id 223
    label "brak"
  ]
  node [
    id 224
    label "str&#243;j"
  ]
  node [
    id 225
    label "haberdashery"
  ]
  node [
    id 226
    label "sklep"
  ]
  node [
    id 227
    label "ozdoba"
  ]
  node [
    id 228
    label "szmuklerz"
  ]
  node [
    id 229
    label "zamkni&#281;cie"
  ]
  node [
    id 230
    label "fastener"
  ]
  node [
    id 231
    label "zapi&#281;cie_si&#281;"
  ]
  node [
    id 232
    label "buckle"
  ]
  node [
    id 233
    label "m&#322;odziak"
  ]
  node [
    id 234
    label "kozio&#322;"
  ]
  node [
    id 235
    label "kolekcjonerstwo"
  ]
  node [
    id 236
    label "kolekcjoner"
  ]
  node [
    id 237
    label "lotnik"
  ]
  node [
    id 238
    label "przewodnik"
  ]
  node [
    id 239
    label "delfin"
  ]
  node [
    id 240
    label "briefing"
  ]
  node [
    id 241
    label "wycieczka"
  ]
  node [
    id 242
    label "nawigator"
  ]
  node [
    id 243
    label "delfinowate"
  ]
  node [
    id 244
    label "zwiastun"
  ]
  node [
    id 245
    label "urz&#261;dzenie"
  ]
  node [
    id 246
    label "kom&#243;rka"
  ]
  node [
    id 247
    label "furnishing"
  ]
  node [
    id 248
    label "zabezpieczenie"
  ]
  node [
    id 249
    label "zrobienie"
  ]
  node [
    id 250
    label "wyrz&#261;dzenie"
  ]
  node [
    id 251
    label "zagospodarowanie"
  ]
  node [
    id 252
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 253
    label "ig&#322;a"
  ]
  node [
    id 254
    label "narz&#281;dzie"
  ]
  node [
    id 255
    label "wirnik"
  ]
  node [
    id 256
    label "aparatura"
  ]
  node [
    id 257
    label "system_energetyczny"
  ]
  node [
    id 258
    label "impulsator"
  ]
  node [
    id 259
    label "mechanizm"
  ]
  node [
    id 260
    label "sprz&#281;t"
  ]
  node [
    id 261
    label "czynno&#347;&#263;"
  ]
  node [
    id 262
    label "blokowanie"
  ]
  node [
    id 263
    label "set"
  ]
  node [
    id 264
    label "zablokowanie"
  ]
  node [
    id 265
    label "przygotowanie"
  ]
  node [
    id 266
    label "komora"
  ]
  node [
    id 267
    label "j&#281;zyk"
  ]
  node [
    id 268
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 269
    label "za&#322;ogant"
  ]
  node [
    id 270
    label "urz&#261;dzenie_nawigacyjne"
  ]
  node [
    id 271
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 272
    label "przewidywanie"
  ]
  node [
    id 273
    label "oznaka"
  ]
  node [
    id 274
    label "harbinger"
  ]
  node [
    id 275
    label "obwie&#347;ciciel"
  ]
  node [
    id 276
    label "nabawianie_si&#281;"
  ]
  node [
    id 277
    label "zapowied&#378;"
  ]
  node [
    id 278
    label "declaration"
  ]
  node [
    id 279
    label "reklama"
  ]
  node [
    id 280
    label "nabawienie_si&#281;"
  ]
  node [
    id 281
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 282
    label "syn_naturalny"
  ]
  node [
    id 283
    label "ornament"
  ]
  node [
    id 284
    label "ssak_morski"
  ]
  node [
    id 285
    label "z&#281;bowce"
  ]
  node [
    id 286
    label "styl_p&#322;ywacki"
  ]
  node [
    id 287
    label "mi&#281;so&#380;erca"
  ]
  node [
    id 288
    label "nast&#281;pca_tronu"
  ]
  node [
    id 289
    label "butterfly"
  ]
  node [
    id 290
    label "&#380;o&#322;nierz"
  ]
  node [
    id 291
    label "lotnictwo_wojskowe"
  ]
  node [
    id 292
    label "awiator"
  ]
  node [
    id 293
    label "pantofel"
  ]
  node [
    id 294
    label "in&#380;ynier"
  ]
  node [
    id 295
    label "Fidel_Castro"
  ]
  node [
    id 296
    label "Anders"
  ]
  node [
    id 297
    label "opiekun"
  ]
  node [
    id 298
    label "topik"
  ]
  node [
    id 299
    label "Ko&#347;ciuszko"
  ]
  node [
    id 300
    label "przeno&#347;nik"
  ]
  node [
    id 301
    label "path"
  ]
  node [
    id 302
    label "Tito"
  ]
  node [
    id 303
    label "Saba&#322;a"
  ]
  node [
    id 304
    label "kriotron"
  ]
  node [
    id 305
    label "lider"
  ]
  node [
    id 306
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 307
    label "handbook"
  ]
  node [
    id 308
    label "Sabataj_Cwi"
  ]
  node [
    id 309
    label "Mao"
  ]
  node [
    id 310
    label "Miko&#322;ajczyk"
  ]
  node [
    id 311
    label "odprawa"
  ]
  node [
    id 312
    label "konferencja_prasowa"
  ]
  node [
    id 313
    label "informacja"
  ]
  node [
    id 314
    label "odpoczynek"
  ]
  node [
    id 315
    label "wyjazd"
  ]
  node [
    id 316
    label "chadzka"
  ]
  node [
    id 317
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 318
    label "przekaza&#263;"
  ]
  node [
    id 319
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 320
    label "press"
  ]
  node [
    id 321
    label "wci&#347;ni&#281;cie"
  ]
  node [
    id 322
    label "wciska&#263;"
  ]
  node [
    id 323
    label "nadusi&#263;"
  ]
  node [
    id 324
    label "spowodowa&#263;"
  ]
  node [
    id 325
    label "tug"
  ]
  node [
    id 326
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 327
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 328
    label "dip"
  ]
  node [
    id 329
    label "umie&#347;ci&#263;"
  ]
  node [
    id 330
    label "propagate"
  ]
  node [
    id 331
    label "wp&#322;aci&#263;"
  ]
  node [
    id 332
    label "transfer"
  ]
  node [
    id 333
    label "wys&#322;a&#263;"
  ]
  node [
    id 334
    label "give"
  ]
  node [
    id 335
    label "zrobi&#263;"
  ]
  node [
    id 336
    label "poda&#263;"
  ]
  node [
    id 337
    label "sygna&#322;"
  ]
  node [
    id 338
    label "impart"
  ]
  node [
    id 339
    label "act"
  ]
  node [
    id 340
    label "przekazanie"
  ]
  node [
    id 341
    label "invasion"
  ]
  node [
    id 342
    label "trespass"
  ]
  node [
    id 343
    label "wparcie"
  ]
  node [
    id 344
    label "spowodowanie"
  ]
  node [
    id 345
    label "odbicie"
  ]
  node [
    id 346
    label "imperativeness"
  ]
  node [
    id 347
    label "naduszenie"
  ]
  node [
    id 348
    label "spr&#281;&#380;anie"
  ]
  node [
    id 349
    label "extort"
  ]
  node [
    id 350
    label "powierza&#263;"
  ]
  node [
    id 351
    label "nacisn&#261;&#263;"
  ]
  node [
    id 352
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 353
    label "pofolgowa&#263;"
  ]
  node [
    id 354
    label "assent"
  ]
  node [
    id 355
    label "uzna&#263;"
  ]
  node [
    id 356
    label "leave"
  ]
  node [
    id 357
    label "oceni&#263;"
  ]
  node [
    id 358
    label "przyzna&#263;"
  ]
  node [
    id 359
    label "stwierdzi&#263;"
  ]
  node [
    id 360
    label "rede"
  ]
  node [
    id 361
    label "see"
  ]
  node [
    id 362
    label "permit"
  ]
  node [
    id 363
    label "joke"
  ]
  node [
    id 364
    label "jeer"
  ]
  node [
    id 365
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 366
    label "zakomunikowa&#263;"
  ]
  node [
    id 367
    label "wystawi&#263;"
  ]
  node [
    id 368
    label "post&#261;pi&#263;"
  ]
  node [
    id 369
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 370
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 371
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 372
    label "zorganizowa&#263;"
  ]
  node [
    id 373
    label "appoint"
  ]
  node [
    id 374
    label "wystylizowa&#263;"
  ]
  node [
    id 375
    label "cause"
  ]
  node [
    id 376
    label "przerobi&#263;"
  ]
  node [
    id 377
    label "nabra&#263;"
  ]
  node [
    id 378
    label "make"
  ]
  node [
    id 379
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 380
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 381
    label "wydali&#263;"
  ]
  node [
    id 382
    label "wychyli&#263;"
  ]
  node [
    id 383
    label "wskaza&#263;"
  ]
  node [
    id 384
    label "zbudowa&#263;"
  ]
  node [
    id 385
    label "wynie&#347;&#263;"
  ]
  node [
    id 386
    label "przedstawi&#263;"
  ]
  node [
    id 387
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 388
    label "pies_my&#347;liwski"
  ]
  node [
    id 389
    label "wyj&#261;&#263;"
  ]
  node [
    id 390
    label "zaproponowa&#263;"
  ]
  node [
    id 391
    label "wyrazi&#263;"
  ]
  node [
    id 392
    label "wyeksponowa&#263;"
  ]
  node [
    id 393
    label "wypisa&#263;"
  ]
  node [
    id 394
    label "wysun&#261;&#263;"
  ]
  node [
    id 395
    label "indicate"
  ]
  node [
    id 396
    label "poja&#347;nia&#263;"
  ]
  node [
    id 397
    label "u&#322;atwia&#263;"
  ]
  node [
    id 398
    label "elaborate"
  ]
  node [
    id 399
    label "suplikowa&#263;"
  ]
  node [
    id 400
    label "przek&#322;ada&#263;"
  ]
  node [
    id 401
    label "przekonywa&#263;"
  ]
  node [
    id 402
    label "interpretowa&#263;"
  ]
  node [
    id 403
    label "broni&#263;"
  ]
  node [
    id 404
    label "explain"
  ]
  node [
    id 405
    label "przedstawia&#263;"
  ]
  node [
    id 406
    label "sprawowa&#263;"
  ]
  node [
    id 407
    label "uzasadnia&#263;"
  ]
  node [
    id 408
    label "organizowa&#263;"
  ]
  node [
    id 409
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 410
    label "czyni&#263;"
  ]
  node [
    id 411
    label "stylizowa&#263;"
  ]
  node [
    id 412
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 413
    label "falowa&#263;"
  ]
  node [
    id 414
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 415
    label "peddle"
  ]
  node [
    id 416
    label "praca"
  ]
  node [
    id 417
    label "wydala&#263;"
  ]
  node [
    id 418
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 419
    label "tentegowa&#263;"
  ]
  node [
    id 420
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 421
    label "urz&#261;dza&#263;"
  ]
  node [
    id 422
    label "oszukiwa&#263;"
  ]
  node [
    id 423
    label "work"
  ]
  node [
    id 424
    label "ukazywa&#263;"
  ]
  node [
    id 425
    label "przerabia&#263;"
  ]
  node [
    id 426
    label "post&#281;powa&#263;"
  ]
  node [
    id 427
    label "gloss"
  ]
  node [
    id 428
    label "rozumie&#263;"
  ]
  node [
    id 429
    label "wykonywa&#263;"
  ]
  node [
    id 430
    label "analizowa&#263;"
  ]
  node [
    id 431
    label "odbiera&#263;"
  ]
  node [
    id 432
    label "teatr"
  ]
  node [
    id 433
    label "exhibit"
  ]
  node [
    id 434
    label "podawa&#263;"
  ]
  node [
    id 435
    label "display"
  ]
  node [
    id 436
    label "pokazywa&#263;"
  ]
  node [
    id 437
    label "demonstrowa&#263;"
  ]
  node [
    id 438
    label "przedstawienie"
  ]
  node [
    id 439
    label "zapoznawa&#263;"
  ]
  node [
    id 440
    label "opisywa&#263;"
  ]
  node [
    id 441
    label "represent"
  ]
  node [
    id 442
    label "zg&#322;asza&#263;"
  ]
  node [
    id 443
    label "typify"
  ]
  node [
    id 444
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 445
    label "attest"
  ]
  node [
    id 446
    label "stanowi&#263;"
  ]
  node [
    id 447
    label "&#322;atwi&#263;"
  ]
  node [
    id 448
    label "ease"
  ]
  node [
    id 449
    label "prosecute"
  ]
  node [
    id 450
    label "by&#263;"
  ]
  node [
    id 451
    label "estrange"
  ]
  node [
    id 452
    label "uznawa&#263;"
  ]
  node [
    id 453
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 454
    label "wole&#263;"
  ]
  node [
    id 455
    label "translate"
  ]
  node [
    id 456
    label "zmienia&#263;"
  ]
  node [
    id 457
    label "postpone"
  ]
  node [
    id 458
    label "przenosi&#263;"
  ]
  node [
    id 459
    label "prym"
  ]
  node [
    id 460
    label "wk&#322;ada&#263;"
  ]
  node [
    id 461
    label "fend"
  ]
  node [
    id 462
    label "s&#261;d"
  ]
  node [
    id 463
    label "reprezentowa&#263;"
  ]
  node [
    id 464
    label "zdawa&#263;"
  ]
  node [
    id 465
    label "czuwa&#263;"
  ]
  node [
    id 466
    label "preach"
  ]
  node [
    id 467
    label "chroni&#263;"
  ]
  node [
    id 468
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 469
    label "walczy&#263;"
  ]
  node [
    id 470
    label "resist"
  ]
  node [
    id 471
    label "adwokatowa&#263;"
  ]
  node [
    id 472
    label "rebuff"
  ]
  node [
    id 473
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 474
    label "udowadnia&#263;"
  ]
  node [
    id 475
    label "gra&#263;"
  ]
  node [
    id 476
    label "refuse"
  ]
  node [
    id 477
    label "nak&#322;ania&#263;"
  ]
  node [
    id 478
    label "argue"
  ]
  node [
    id 479
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 480
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 481
    label "artykulator"
  ]
  node [
    id 482
    label "kod"
  ]
  node [
    id 483
    label "kawa&#322;ek"
  ]
  node [
    id 484
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 485
    label "gramatyka"
  ]
  node [
    id 486
    label "stylik"
  ]
  node [
    id 487
    label "przet&#322;umaczenie"
  ]
  node [
    id 488
    label "formalizowanie"
  ]
  node [
    id 489
    label "ssanie"
  ]
  node [
    id 490
    label "ssa&#263;"
  ]
  node [
    id 491
    label "language"
  ]
  node [
    id 492
    label "liza&#263;"
  ]
  node [
    id 493
    label "napisa&#263;"
  ]
  node [
    id 494
    label "konsonantyzm"
  ]
  node [
    id 495
    label "wokalizm"
  ]
  node [
    id 496
    label "pisa&#263;"
  ]
  node [
    id 497
    label "fonetyka"
  ]
  node [
    id 498
    label "jeniec"
  ]
  node [
    id 499
    label "but"
  ]
  node [
    id 500
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 501
    label "po_koroniarsku"
  ]
  node [
    id 502
    label "t&#322;umaczenie"
  ]
  node [
    id 503
    label "m&#243;wienie"
  ]
  node [
    id 504
    label "pype&#263;"
  ]
  node [
    id 505
    label "lizanie"
  ]
  node [
    id 506
    label "pismo"
  ]
  node [
    id 507
    label "formalizowa&#263;"
  ]
  node [
    id 508
    label "organ"
  ]
  node [
    id 509
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 510
    label "rozumienie"
  ]
  node [
    id 511
    label "makroglosja"
  ]
  node [
    id 512
    label "m&#243;wi&#263;"
  ]
  node [
    id 513
    label "jama_ustna"
  ]
  node [
    id 514
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 515
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 516
    label "natural_language"
  ]
  node [
    id 517
    label "s&#322;ownictwo"
  ]
  node [
    id 518
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 519
    label "sk&#322;ada&#263;"
  ]
  node [
    id 520
    label "prosi&#263;"
  ]
  node [
    id 521
    label "b&#322;aga&#263;"
  ]
  node [
    id 522
    label "raise"
  ]
  node [
    id 523
    label "posta&#263;"
  ]
  node [
    id 524
    label "osoba"
  ]
  node [
    id 525
    label "znaczenie"
  ]
  node [
    id 526
    label "go&#347;&#263;"
  ]
  node [
    id 527
    label "ludzko&#347;&#263;"
  ]
  node [
    id 528
    label "wapniak"
  ]
  node [
    id 529
    label "os&#322;abia&#263;"
  ]
  node [
    id 530
    label "hominid"
  ]
  node [
    id 531
    label "podw&#322;adny"
  ]
  node [
    id 532
    label "os&#322;abianie"
  ]
  node [
    id 533
    label "g&#322;owa"
  ]
  node [
    id 534
    label "figura"
  ]
  node [
    id 535
    label "portrecista"
  ]
  node [
    id 536
    label "dwun&#243;g"
  ]
  node [
    id 537
    label "profanum"
  ]
  node [
    id 538
    label "mikrokosmos"
  ]
  node [
    id 539
    label "nasada"
  ]
  node [
    id 540
    label "duch"
  ]
  node [
    id 541
    label "antropochoria"
  ]
  node [
    id 542
    label "wz&#243;r"
  ]
  node [
    id 543
    label "senior"
  ]
  node [
    id 544
    label "oddzia&#322;ywanie"
  ]
  node [
    id 545
    label "Adam"
  ]
  node [
    id 546
    label "homo_sapiens"
  ]
  node [
    id 547
    label "polifag"
  ]
  node [
    id 548
    label "odwiedziny"
  ]
  node [
    id 549
    label "klient"
  ]
  node [
    id 550
    label "restauracja"
  ]
  node [
    id 551
    label "przybysz"
  ]
  node [
    id 552
    label "uczestnik"
  ]
  node [
    id 553
    label "hotel"
  ]
  node [
    id 554
    label "bratek"
  ]
  node [
    id 555
    label "sztuka"
  ]
  node [
    id 556
    label "facet"
  ]
  node [
    id 557
    label "Chocho&#322;"
  ]
  node [
    id 558
    label "Herkules_Poirot"
  ]
  node [
    id 559
    label "Edyp"
  ]
  node [
    id 560
    label "parali&#380;owa&#263;"
  ]
  node [
    id 561
    label "Harry_Potter"
  ]
  node [
    id 562
    label "Casanova"
  ]
  node [
    id 563
    label "Gargantua"
  ]
  node [
    id 564
    label "Zgredek"
  ]
  node [
    id 565
    label "Winnetou"
  ]
  node [
    id 566
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 567
    label "Dulcynea"
  ]
  node [
    id 568
    label "kategoria_gramatyczna"
  ]
  node [
    id 569
    label "person"
  ]
  node [
    id 570
    label "Sherlock_Holmes"
  ]
  node [
    id 571
    label "Quasimodo"
  ]
  node [
    id 572
    label "Plastu&#347;"
  ]
  node [
    id 573
    label "Faust"
  ]
  node [
    id 574
    label "Wallenrod"
  ]
  node [
    id 575
    label "Dwukwiat"
  ]
  node [
    id 576
    label "koniugacja"
  ]
  node [
    id 577
    label "Don_Juan"
  ]
  node [
    id 578
    label "Don_Kiszot"
  ]
  node [
    id 579
    label "Hamlet"
  ]
  node [
    id 580
    label "Werter"
  ]
  node [
    id 581
    label "Szwejk"
  ]
  node [
    id 582
    label "odk&#322;adanie"
  ]
  node [
    id 583
    label "condition"
  ]
  node [
    id 584
    label "liczenie"
  ]
  node [
    id 585
    label "stawianie"
  ]
  node [
    id 586
    label "bycie"
  ]
  node [
    id 587
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 588
    label "assay"
  ]
  node [
    id 589
    label "wskazywanie"
  ]
  node [
    id 590
    label "wyraz"
  ]
  node [
    id 591
    label "gravity"
  ]
  node [
    id 592
    label "weight"
  ]
  node [
    id 593
    label "command"
  ]
  node [
    id 594
    label "odgrywanie_roli"
  ]
  node [
    id 595
    label "cecha"
  ]
  node [
    id 596
    label "okre&#347;lanie"
  ]
  node [
    id 597
    label "wyra&#380;enie"
  ]
  node [
    id 598
    label "charakterystyka"
  ]
  node [
    id 599
    label "zaistnie&#263;"
  ]
  node [
    id 600
    label "Osjan"
  ]
  node [
    id 601
    label "wygl&#261;d"
  ]
  node [
    id 602
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 603
    label "osobowo&#347;&#263;"
  ]
  node [
    id 604
    label "wytw&#243;r"
  ]
  node [
    id 605
    label "trim"
  ]
  node [
    id 606
    label "poby&#263;"
  ]
  node [
    id 607
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 608
    label "Aspazja"
  ]
  node [
    id 609
    label "punkt_widzenia"
  ]
  node [
    id 610
    label "kompleksja"
  ]
  node [
    id 611
    label "wytrzyma&#263;"
  ]
  node [
    id 612
    label "budowa"
  ]
  node [
    id 613
    label "formacja"
  ]
  node [
    id 614
    label "pozosta&#263;"
  ]
  node [
    id 615
    label "point"
  ]
  node [
    id 616
    label "sprzeciw"
  ]
  node [
    id 617
    label "czerwona_kartka"
  ]
  node [
    id 618
    label "protestacja"
  ]
  node [
    id 619
    label "reakcja"
  ]
  node [
    id 620
    label "posiada&#263;"
  ]
  node [
    id 621
    label "potencja&#322;"
  ]
  node [
    id 622
    label "zapomnienie"
  ]
  node [
    id 623
    label "zapomina&#263;"
  ]
  node [
    id 624
    label "zapominanie"
  ]
  node [
    id 625
    label "ability"
  ]
  node [
    id 626
    label "obliczeniowo"
  ]
  node [
    id 627
    label "zapomnie&#263;"
  ]
  node [
    id 628
    label "wielko&#347;&#263;"
  ]
  node [
    id 629
    label "m&#322;ot"
  ]
  node [
    id 630
    label "znak"
  ]
  node [
    id 631
    label "drzewo"
  ]
  node [
    id 632
    label "attribute"
  ]
  node [
    id 633
    label "marka"
  ]
  node [
    id 634
    label "moc_obliczeniowa"
  ]
  node [
    id 635
    label "wiedzie&#263;"
  ]
  node [
    id 636
    label "zawiera&#263;"
  ]
  node [
    id 637
    label "mie&#263;"
  ]
  node [
    id 638
    label "support"
  ]
  node [
    id 639
    label "keep_open"
  ]
  node [
    id 640
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 641
    label "pozostawi&#263;"
  ]
  node [
    id 642
    label "wybaczy&#263;"
  ]
  node [
    id 643
    label "screw"
  ]
  node [
    id 644
    label "straci&#263;"
  ]
  node [
    id 645
    label "porzuci&#263;"
  ]
  node [
    id 646
    label "opu&#347;ci&#263;"
  ]
  node [
    id 647
    label "zabaczy&#263;"
  ]
  node [
    id 648
    label "fuck"
  ]
  node [
    id 649
    label "przesta&#263;"
  ]
  node [
    id 650
    label "stracenie"
  ]
  node [
    id 651
    label "forgetfulness"
  ]
  node [
    id 652
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 653
    label "obscurity"
  ]
  node [
    id 654
    label "zagojenie_si&#281;"
  ]
  node [
    id 655
    label "zaniedbany"
  ]
  node [
    id 656
    label "zostawienie"
  ]
  node [
    id 657
    label "zdarzenie_si&#281;"
  ]
  node [
    id 658
    label "ulecenie"
  ]
  node [
    id 659
    label "pami&#281;tanie"
  ]
  node [
    id 660
    label "zlekcewa&#380;enie"
  ]
  node [
    id 661
    label "omission"
  ]
  node [
    id 662
    label "roztargnienie"
  ]
  node [
    id 663
    label "izolacja"
  ]
  node [
    id 664
    label "przestanie"
  ]
  node [
    id 665
    label "pozostawia&#263;"
  ]
  node [
    id 666
    label "porzuca&#263;"
  ]
  node [
    id 667
    label "traci&#263;"
  ]
  node [
    id 668
    label "wybacza&#263;"
  ]
  node [
    id 669
    label "przestawa&#263;"
  ]
  node [
    id 670
    label "pomija&#263;"
  ]
  node [
    id 671
    label "zabacza&#263;"
  ]
  node [
    id 672
    label "ulatywanie"
  ]
  node [
    id 673
    label "zostawianie"
  ]
  node [
    id 674
    label "przestawanie"
  ]
  node [
    id 675
    label "lekcewa&#380;enie"
  ]
  node [
    id 676
    label "gojenie_si&#281;"
  ]
  node [
    id 677
    label "tracenie"
  ]
  node [
    id 678
    label "dzianie_si&#281;"
  ]
  node [
    id 679
    label "rachunek_operatorowy"
  ]
  node [
    id 680
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 681
    label "kryptologia"
  ]
  node [
    id 682
    label "logicyzm"
  ]
  node [
    id 683
    label "logika"
  ]
  node [
    id 684
    label "matematyka_czysta"
  ]
  node [
    id 685
    label "forsing"
  ]
  node [
    id 686
    label "supremum"
  ]
  node [
    id 687
    label "modelowanie_matematyczne"
  ]
  node [
    id 688
    label "matma"
  ]
  node [
    id 689
    label "teoria_katastrof"
  ]
  node [
    id 690
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 691
    label "kierunek"
  ]
  node [
    id 692
    label "jednostka"
  ]
  node [
    id 693
    label "fizyka_matematyczna"
  ]
  node [
    id 694
    label "teoria_graf&#243;w"
  ]
  node [
    id 695
    label "rzut"
  ]
  node [
    id 696
    label "rachunki"
  ]
  node [
    id 697
    label "topologia_algebraiczna"
  ]
  node [
    id 698
    label "matematyka_stosowana"
  ]
  node [
    id 699
    label "funkcja"
  ]
  node [
    id 700
    label "infimum"
  ]
  node [
    id 701
    label "przebieg"
  ]
  node [
    id 702
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 703
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 704
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 705
    label "praktyka"
  ]
  node [
    id 706
    label "system"
  ]
  node [
    id 707
    label "przeorientowywanie"
  ]
  node [
    id 708
    label "studia"
  ]
  node [
    id 709
    label "bok"
  ]
  node [
    id 710
    label "skr&#281;canie"
  ]
  node [
    id 711
    label "skr&#281;ca&#263;"
  ]
  node [
    id 712
    label "przeorientowywa&#263;"
  ]
  node [
    id 713
    label "orientowanie"
  ]
  node [
    id 714
    label "skr&#281;ci&#263;"
  ]
  node [
    id 715
    label "przeorientowanie"
  ]
  node [
    id 716
    label "zorientowanie"
  ]
  node [
    id 717
    label "przeorientowa&#263;"
  ]
  node [
    id 718
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 719
    label "metoda"
  ]
  node [
    id 720
    label "ty&#322;"
  ]
  node [
    id 721
    label "zorientowa&#263;"
  ]
  node [
    id 722
    label "g&#243;ra"
  ]
  node [
    id 723
    label "orientowa&#263;"
  ]
  node [
    id 724
    label "ideologia"
  ]
  node [
    id 725
    label "orientacja"
  ]
  node [
    id 726
    label "prz&#243;d"
  ]
  node [
    id 727
    label "bearing"
  ]
  node [
    id 728
    label "skr&#281;cenie"
  ]
  node [
    id 729
    label "arithmetic"
  ]
  node [
    id 730
    label "logicism"
  ]
  node [
    id 731
    label "doktryna_filozoficzna"
  ]
  node [
    id 732
    label "filozofia_matematyki"
  ]
  node [
    id 733
    label "ograniczenie"
  ]
  node [
    id 734
    label "czyn"
  ]
  node [
    id 735
    label "addytywno&#347;&#263;"
  ]
  node [
    id 736
    label "function"
  ]
  node [
    id 737
    label "zastosowanie"
  ]
  node [
    id 738
    label "funkcjonowanie"
  ]
  node [
    id 739
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 740
    label "powierzanie"
  ]
  node [
    id 741
    label "cel"
  ]
  node [
    id 742
    label "dziedzina"
  ]
  node [
    id 743
    label "przeciwdziedzina"
  ]
  node [
    id 744
    label "awansowa&#263;"
  ]
  node [
    id 745
    label "stawia&#263;"
  ]
  node [
    id 746
    label "wakowa&#263;"
  ]
  node [
    id 747
    label "postawi&#263;"
  ]
  node [
    id 748
    label "awansowanie"
  ]
  node [
    id 749
    label "dow&#243;d"
  ]
  node [
    id 750
    label "armia"
  ]
  node [
    id 751
    label "nawr&#243;t_choroby"
  ]
  node [
    id 752
    label "potomstwo"
  ]
  node [
    id 753
    label "odwzorowanie"
  ]
  node [
    id 754
    label "rysunek"
  ]
  node [
    id 755
    label "scene"
  ]
  node [
    id 756
    label "throw"
  ]
  node [
    id 757
    label "float"
  ]
  node [
    id 758
    label "projection"
  ]
  node [
    id 759
    label "injection"
  ]
  node [
    id 760
    label "blow"
  ]
  node [
    id 761
    label "ruch"
  ]
  node [
    id 762
    label "k&#322;ad"
  ]
  node [
    id 763
    label "mold"
  ]
  node [
    id 764
    label "przyswoi&#263;"
  ]
  node [
    id 765
    label "one"
  ]
  node [
    id 766
    label "poj&#281;cie"
  ]
  node [
    id 767
    label "ewoluowanie"
  ]
  node [
    id 768
    label "skala"
  ]
  node [
    id 769
    label "przyswajanie"
  ]
  node [
    id 770
    label "wyewoluowanie"
  ]
  node [
    id 771
    label "przeliczy&#263;"
  ]
  node [
    id 772
    label "wyewoluowa&#263;"
  ]
  node [
    id 773
    label "ewoluowa&#263;"
  ]
  node [
    id 774
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 775
    label "liczba_naturalna"
  ]
  node [
    id 776
    label "czynnik_biotyczny"
  ]
  node [
    id 777
    label "individual"
  ]
  node [
    id 778
    label "obiekt"
  ]
  node [
    id 779
    label "przyswaja&#263;"
  ]
  node [
    id 780
    label "przyswojenie"
  ]
  node [
    id 781
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 782
    label "starzenie_si&#281;"
  ]
  node [
    id 783
    label "przeliczanie"
  ]
  node [
    id 784
    label "przelicza&#263;"
  ]
  node [
    id 785
    label "przeliczenie"
  ]
  node [
    id 786
    label "izomorfizm"
  ]
  node [
    id 787
    label "teoremat"
  ]
  node [
    id 788
    label "logizacja"
  ]
  node [
    id 789
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 790
    label "analityka"
  ]
  node [
    id 791
    label "predykat"
  ]
  node [
    id 792
    label "rozumno&#347;&#263;"
  ]
  node [
    id 793
    label "metamatematyka"
  ]
  node [
    id 794
    label "filozofia"
  ]
  node [
    id 795
    label "operacja_logiczna"
  ]
  node [
    id 796
    label "sylogistyka"
  ]
  node [
    id 797
    label "dialektyka"
  ]
  node [
    id 798
    label "podzia&#322;_logiczny"
  ]
  node [
    id 799
    label "logistics"
  ]
  node [
    id 800
    label "informatyka"
  ]
  node [
    id 801
    label "kryptografia"
  ]
  node [
    id 802
    label "nauka"
  ]
  node [
    id 803
    label "kryptoanaliza"
  ]
  node [
    id 804
    label "nieznaczny"
  ]
  node [
    id 805
    label "pomiernie"
  ]
  node [
    id 806
    label "kr&#243;tko"
  ]
  node [
    id 807
    label "mikroskopijnie"
  ]
  node [
    id 808
    label "nieliczny"
  ]
  node [
    id 809
    label "mo&#380;liwie"
  ]
  node [
    id 810
    label "nieistotnie"
  ]
  node [
    id 811
    label "ma&#322;y"
  ]
  node [
    id 812
    label "niepowa&#380;nie"
  ]
  node [
    id 813
    label "niewa&#380;ny"
  ]
  node [
    id 814
    label "mo&#380;liwy"
  ]
  node [
    id 815
    label "zno&#347;nie"
  ]
  node [
    id 816
    label "kr&#243;tki"
  ]
  node [
    id 817
    label "nieznacznie"
  ]
  node [
    id 818
    label "drobnostkowy"
  ]
  node [
    id 819
    label "malusie&#324;ko"
  ]
  node [
    id 820
    label "mikroskopijny"
  ]
  node [
    id 821
    label "szybki"
  ]
  node [
    id 822
    label "przeci&#281;tny"
  ]
  node [
    id 823
    label "wstydliwy"
  ]
  node [
    id 824
    label "s&#322;aby"
  ]
  node [
    id 825
    label "ch&#322;opiec"
  ]
  node [
    id 826
    label "m&#322;ody"
  ]
  node [
    id 827
    label "marny"
  ]
  node [
    id 828
    label "n&#281;dznie"
  ]
  node [
    id 829
    label "nielicznie"
  ]
  node [
    id 830
    label "licho"
  ]
  node [
    id 831
    label "proporcjonalnie"
  ]
  node [
    id 832
    label "pomierny"
  ]
  node [
    id 833
    label "miernie"
  ]
  node [
    id 834
    label "du&#380;y"
  ]
  node [
    id 835
    label "mocno"
  ]
  node [
    id 836
    label "wiela"
  ]
  node [
    id 837
    label "cz&#281;sto"
  ]
  node [
    id 838
    label "wiele"
  ]
  node [
    id 839
    label "doros&#322;y"
  ]
  node [
    id 840
    label "znaczny"
  ]
  node [
    id 841
    label "niema&#322;o"
  ]
  node [
    id 842
    label "rozwini&#281;ty"
  ]
  node [
    id 843
    label "dorodny"
  ]
  node [
    id 844
    label "wa&#380;ny"
  ]
  node [
    id 845
    label "prawdziwy"
  ]
  node [
    id 846
    label "intensywny"
  ]
  node [
    id 847
    label "mocny"
  ]
  node [
    id 848
    label "silny"
  ]
  node [
    id 849
    label "przekonuj&#261;co"
  ]
  node [
    id 850
    label "powerfully"
  ]
  node [
    id 851
    label "widocznie"
  ]
  node [
    id 852
    label "szczerze"
  ]
  node [
    id 853
    label "konkretnie"
  ]
  node [
    id 854
    label "niepodwa&#380;alnie"
  ]
  node [
    id 855
    label "stabilnie"
  ]
  node [
    id 856
    label "silnie"
  ]
  node [
    id 857
    label "zdecydowanie"
  ]
  node [
    id 858
    label "strongly"
  ]
  node [
    id 859
    label "cz&#281;sty"
  ]
  node [
    id 860
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 861
    label "proszek"
  ]
  node [
    id 862
    label "tablet"
  ]
  node [
    id 863
    label "dawka"
  ]
  node [
    id 864
    label "blister"
  ]
  node [
    id 865
    label "lekarstwo"
  ]
  node [
    id 866
    label "badanie"
  ]
  node [
    id 867
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 868
    label "szko&#322;a"
  ]
  node [
    id 869
    label "obserwowanie"
  ]
  node [
    id 870
    label "wiedza"
  ]
  node [
    id 871
    label "wy&#347;wiadczenie"
  ]
  node [
    id 872
    label "wydarzenie"
  ]
  node [
    id 873
    label "znawstwo"
  ]
  node [
    id 874
    label "skill"
  ]
  node [
    id 875
    label "checkup"
  ]
  node [
    id 876
    label "spotkanie"
  ]
  node [
    id 877
    label "do&#347;wiadczanie"
  ]
  node [
    id 878
    label "zbadanie"
  ]
  node [
    id 879
    label "potraktowanie"
  ]
  node [
    id 880
    label "eksperiencja"
  ]
  node [
    id 881
    label "poczucie"
  ]
  node [
    id 882
    label "zrecenzowanie"
  ]
  node [
    id 883
    label "kontrola"
  ]
  node [
    id 884
    label "analysis"
  ]
  node [
    id 885
    label "rektalny"
  ]
  node [
    id 886
    label "ustalenie"
  ]
  node [
    id 887
    label "macanie"
  ]
  node [
    id 888
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 889
    label "usi&#322;owanie"
  ]
  node [
    id 890
    label "udowadnianie"
  ]
  node [
    id 891
    label "bia&#322;a_niedziela"
  ]
  node [
    id 892
    label "diagnostyka"
  ]
  node [
    id 893
    label "dociekanie"
  ]
  node [
    id 894
    label "rezultat"
  ]
  node [
    id 895
    label "sprawdzanie"
  ]
  node [
    id 896
    label "penetrowanie"
  ]
  node [
    id 897
    label "krytykowanie"
  ]
  node [
    id 898
    label "ustalanie"
  ]
  node [
    id 899
    label "rozpatrywanie"
  ]
  node [
    id 900
    label "investigation"
  ]
  node [
    id 901
    label "wziernikowanie"
  ]
  node [
    id 902
    label "examination"
  ]
  node [
    id 903
    label "cognition"
  ]
  node [
    id 904
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 905
    label "intelekt"
  ]
  node [
    id 906
    label "pozwolenie"
  ]
  node [
    id 907
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 908
    label "zaawansowanie"
  ]
  node [
    id 909
    label "wykszta&#322;cenie"
  ]
  node [
    id 910
    label "ekstraspekcja"
  ]
  node [
    id 911
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 912
    label "feeling"
  ]
  node [
    id 913
    label "doznanie"
  ]
  node [
    id 914
    label "smell"
  ]
  node [
    id 915
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 916
    label "opanowanie"
  ]
  node [
    id 917
    label "os&#322;upienie"
  ]
  node [
    id 918
    label "zareagowanie"
  ]
  node [
    id 919
    label "intuition"
  ]
  node [
    id 920
    label "przebiec"
  ]
  node [
    id 921
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 922
    label "motyw"
  ]
  node [
    id 923
    label "przebiegni&#281;cie"
  ]
  node [
    id 924
    label "fabu&#322;a"
  ]
  node [
    id 925
    label "udowodnienie"
  ]
  node [
    id 926
    label "przebadanie"
  ]
  node [
    id 927
    label "skontrolowanie"
  ]
  node [
    id 928
    label "rozwa&#380;enie"
  ]
  node [
    id 929
    label "validation"
  ]
  node [
    id 930
    label "delivery"
  ]
  node [
    id 931
    label "oddzia&#322;anie"
  ]
  node [
    id 932
    label "patrzenie"
  ]
  node [
    id 933
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 934
    label "doszukanie_si&#281;"
  ]
  node [
    id 935
    label "dostrzeganie"
  ]
  node [
    id 936
    label "poobserwowanie"
  ]
  node [
    id 937
    label "observation"
  ]
  node [
    id 938
    label "bocianie_gniazdo"
  ]
  node [
    id 939
    label "teren_szko&#322;y"
  ]
  node [
    id 940
    label "Mickiewicz"
  ]
  node [
    id 941
    label "kwalifikacje"
  ]
  node [
    id 942
    label "podr&#281;cznik"
  ]
  node [
    id 943
    label "absolwent"
  ]
  node [
    id 944
    label "school"
  ]
  node [
    id 945
    label "zda&#263;"
  ]
  node [
    id 946
    label "gabinet"
  ]
  node [
    id 947
    label "urszulanki"
  ]
  node [
    id 948
    label "sztuba"
  ]
  node [
    id 949
    label "&#322;awa_szkolna"
  ]
  node [
    id 950
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 951
    label "przepisa&#263;"
  ]
  node [
    id 952
    label "muzyka"
  ]
  node [
    id 953
    label "form"
  ]
  node [
    id 954
    label "klasa"
  ]
  node [
    id 955
    label "lekcja"
  ]
  node [
    id 956
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 957
    label "przepisanie"
  ]
  node [
    id 958
    label "skolaryzacja"
  ]
  node [
    id 959
    label "zdanie"
  ]
  node [
    id 960
    label "stopek"
  ]
  node [
    id 961
    label "sekretariat"
  ]
  node [
    id 962
    label "lesson"
  ]
  node [
    id 963
    label "instytucja"
  ]
  node [
    id 964
    label "niepokalanki"
  ]
  node [
    id 965
    label "siedziba"
  ]
  node [
    id 966
    label "szkolenie"
  ]
  node [
    id 967
    label "kara"
  ]
  node [
    id 968
    label "tablica"
  ]
  node [
    id 969
    label "mini&#281;cie"
  ]
  node [
    id 970
    label "zaistnienie"
  ]
  node [
    id 971
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 972
    label "przebycie"
  ]
  node [
    id 973
    label "cruise"
  ]
  node [
    id 974
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 975
    label "przep&#322;ywanie"
  ]
  node [
    id 976
    label "gathering"
  ]
  node [
    id 977
    label "zawarcie"
  ]
  node [
    id 978
    label "znajomy"
  ]
  node [
    id 979
    label "powitanie"
  ]
  node [
    id 980
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 981
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 982
    label "znalezienie"
  ]
  node [
    id 983
    label "match"
  ]
  node [
    id 984
    label "employment"
  ]
  node [
    id 985
    label "po&#380;egnanie"
  ]
  node [
    id 986
    label "gather"
  ]
  node [
    id 987
    label "spotykanie"
  ]
  node [
    id 988
    label "spotkanie_si&#281;"
  ]
  node [
    id 989
    label "zawdzi&#281;czanie"
  ]
  node [
    id 990
    label "uczynienie_dobra"
  ]
  node [
    id 991
    label "znajomo&#347;&#263;"
  ]
  node [
    id 992
    label "information"
  ]
  node [
    id 993
    label "kiperstwo"
  ]
  node [
    id 994
    label "traktowanie"
  ]
  node [
    id 995
    label "recognition"
  ]
  node [
    id 996
    label "czucie"
  ]
  node [
    id 997
    label "naukowy"
  ]
  node [
    id 998
    label "dydaktycznie"
  ]
  node [
    id 999
    label "naukowo"
  ]
  node [
    id 1000
    label "teoretyczny"
  ]
  node [
    id 1001
    label "edukacyjnie"
  ]
  node [
    id 1002
    label "scjentyficzny"
  ]
  node [
    id 1003
    label "skomplikowany"
  ]
  node [
    id 1004
    label "specjalistyczny"
  ]
  node [
    id 1005
    label "zgodny"
  ]
  node [
    id 1006
    label "intelektualny"
  ]
  node [
    id 1007
    label "specjalny"
  ]
  node [
    id 1008
    label "dydaktyczny"
  ]
  node [
    id 1009
    label "m&#261;dro&#347;ciowy"
  ]
  node [
    id 1010
    label "moralizatorsko"
  ]
  node [
    id 1011
    label "o&#347;wietlenie"
  ]
  node [
    id 1012
    label "szczery"
  ]
  node [
    id 1013
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 1014
    label "jasno"
  ]
  node [
    id 1015
    label "o&#347;wietlanie"
  ]
  node [
    id 1016
    label "przytomny"
  ]
  node [
    id 1017
    label "zrozumia&#322;y"
  ]
  node [
    id 1018
    label "niezm&#261;cony"
  ]
  node [
    id 1019
    label "bia&#322;y"
  ]
  node [
    id 1020
    label "klarowny"
  ]
  node [
    id 1021
    label "jednoznaczny"
  ]
  node [
    id 1022
    label "dobry"
  ]
  node [
    id 1023
    label "pogodny"
  ]
  node [
    id 1024
    label "skrawy"
  ]
  node [
    id 1025
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 1026
    label "przepe&#322;niony"
  ]
  node [
    id 1027
    label "szczodry"
  ]
  node [
    id 1028
    label "s&#322;uszny"
  ]
  node [
    id 1029
    label "uczciwy"
  ]
  node [
    id 1030
    label "przekonuj&#261;cy"
  ]
  node [
    id 1031
    label "prostoduszny"
  ]
  node [
    id 1032
    label "szczyry"
  ]
  node [
    id 1033
    label "czysty"
  ]
  node [
    id 1034
    label "spokojny"
  ]
  node [
    id 1035
    label "&#322;adny"
  ]
  node [
    id 1036
    label "udany"
  ]
  node [
    id 1037
    label "pozytywny"
  ]
  node [
    id 1038
    label "pogodnie"
  ]
  node [
    id 1039
    label "przyjemny"
  ]
  node [
    id 1040
    label "nienaruszony"
  ]
  node [
    id 1041
    label "doskona&#322;y"
  ]
  node [
    id 1042
    label "sprawny"
  ]
  node [
    id 1043
    label "jednoznacznie"
  ]
  node [
    id 1044
    label "okre&#347;lony"
  ]
  node [
    id 1045
    label "identyczny"
  ]
  node [
    id 1046
    label "prosty"
  ]
  node [
    id 1047
    label "pojmowalny"
  ]
  node [
    id 1048
    label "uzasadniony"
  ]
  node [
    id 1049
    label "wyja&#347;nienie"
  ]
  node [
    id 1050
    label "wyja&#347;nianie_si&#281;"
  ]
  node [
    id 1051
    label "rozja&#347;nienie"
  ]
  node [
    id 1052
    label "przeja&#347;nienie_si&#281;"
  ]
  node [
    id 1053
    label "zrozumiale"
  ]
  node [
    id 1054
    label "przeja&#347;nianie_si&#281;"
  ]
  node [
    id 1055
    label "sensowny"
  ]
  node [
    id 1056
    label "rozja&#347;nianie"
  ]
  node [
    id 1057
    label "wyja&#347;nienie_si&#281;"
  ]
  node [
    id 1058
    label "prze&#378;roczy"
  ]
  node [
    id 1059
    label "przezroczy&#347;cie"
  ]
  node [
    id 1060
    label "klarowanie"
  ]
  node [
    id 1061
    label "klarowanie_si&#281;"
  ]
  node [
    id 1062
    label "sklarowanie"
  ]
  node [
    id 1063
    label "klarownie"
  ]
  node [
    id 1064
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 1065
    label "dobroczynny"
  ]
  node [
    id 1066
    label "czw&#243;rka"
  ]
  node [
    id 1067
    label "skuteczny"
  ]
  node [
    id 1068
    label "&#347;mieszny"
  ]
  node [
    id 1069
    label "mi&#322;y"
  ]
  node [
    id 1070
    label "grzeczny"
  ]
  node [
    id 1071
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1072
    label "dobrze"
  ]
  node [
    id 1073
    label "ca&#322;y"
  ]
  node [
    id 1074
    label "zwrot"
  ]
  node [
    id 1075
    label "pomy&#347;lny"
  ]
  node [
    id 1076
    label "moralny"
  ]
  node [
    id 1077
    label "drogi"
  ]
  node [
    id 1078
    label "odpowiedni"
  ]
  node [
    id 1079
    label "korzystny"
  ]
  node [
    id 1080
    label "pos&#322;uszny"
  ]
  node [
    id 1081
    label "ja&#347;niej"
  ]
  node [
    id 1082
    label "ja&#347;nie"
  ]
  node [
    id 1083
    label "skutecznie"
  ]
  node [
    id 1084
    label "sprawnie"
  ]
  node [
    id 1085
    label "czujny"
  ]
  node [
    id 1086
    label "przytomnie"
  ]
  node [
    id 1087
    label "&#347;wiecenie"
  ]
  node [
    id 1088
    label "o&#347;wiecanie"
  ]
  node [
    id 1089
    label "light"
  ]
  node [
    id 1090
    label "prze&#347;wietlanie"
  ]
  node [
    id 1091
    label "instalacja"
  ]
  node [
    id 1092
    label "lighting"
  ]
  node [
    id 1093
    label "lighter"
  ]
  node [
    id 1094
    label "interpretacja"
  ]
  node [
    id 1095
    label "nat&#281;&#380;enie"
  ]
  node [
    id 1096
    label "carat"
  ]
  node [
    id 1097
    label "bia&#322;y_murzyn"
  ]
  node [
    id 1098
    label "Rosjanin"
  ]
  node [
    id 1099
    label "bia&#322;e"
  ]
  node [
    id 1100
    label "jasnosk&#243;ry"
  ]
  node [
    id 1101
    label "bierka_szachowa"
  ]
  node [
    id 1102
    label "bia&#322;y_taniec"
  ]
  node [
    id 1103
    label "dzia&#322;acz"
  ]
  node [
    id 1104
    label "bezbarwny"
  ]
  node [
    id 1105
    label "siwy"
  ]
  node [
    id 1106
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 1107
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1108
    label "Polak"
  ]
  node [
    id 1109
    label "medyczny"
  ]
  node [
    id 1110
    label "bia&#322;o"
  ]
  node [
    id 1111
    label "typ_orientalny"
  ]
  node [
    id 1112
    label "libera&#322;"
  ]
  node [
    id 1113
    label "&#347;nie&#380;nie"
  ]
  node [
    id 1114
    label "konserwatysta"
  ]
  node [
    id 1115
    label "&#347;nie&#380;no"
  ]
  node [
    id 1116
    label "bia&#322;as"
  ]
  node [
    id 1117
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 1118
    label "blady"
  ]
  node [
    id 1119
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 1120
    label "nacjonalista"
  ]
  node [
    id 1121
    label "stosowanie"
  ]
  node [
    id 1122
    label "u&#380;yteczny"
  ]
  node [
    id 1123
    label "przejaskrawianie"
  ]
  node [
    id 1124
    label "zniszczenie"
  ]
  node [
    id 1125
    label "relish"
  ]
  node [
    id 1126
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 1127
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1128
    label "exercise"
  ]
  node [
    id 1129
    label "zaznawanie"
  ]
  node [
    id 1130
    label "zu&#380;ywanie"
  ]
  node [
    id 1131
    label "use"
  ]
  node [
    id 1132
    label "fabrication"
  ]
  node [
    id 1133
    label "porobienie"
  ]
  node [
    id 1134
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1135
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1136
    label "creation"
  ]
  node [
    id 1137
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1138
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1139
    label "tentegowanie"
  ]
  node [
    id 1140
    label "activity"
  ]
  node [
    id 1141
    label "bezproblemowy"
  ]
  node [
    id 1142
    label "wykorzystywanie"
  ]
  node [
    id 1143
    label "wyzyskanie"
  ]
  node [
    id 1144
    label "przydanie_si&#281;"
  ]
  node [
    id 1145
    label "przydawanie_si&#281;"
  ]
  node [
    id 1146
    label "u&#380;ytecznie"
  ]
  node [
    id 1147
    label "przydatny"
  ]
  node [
    id 1148
    label "mutant"
  ]
  node [
    id 1149
    label "dobrostan"
  ]
  node [
    id 1150
    label "u&#380;ycie"
  ]
  node [
    id 1151
    label "u&#380;y&#263;"
  ]
  node [
    id 1152
    label "bawienie"
  ]
  node [
    id 1153
    label "lubo&#347;&#263;"
  ]
  node [
    id 1154
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1155
    label "prze&#380;ycie"
  ]
  node [
    id 1156
    label "aromatyczny"
  ]
  node [
    id 1157
    label "dodatek"
  ]
  node [
    id 1158
    label "g&#281;sty"
  ]
  node [
    id 1159
    label "sos"
  ]
  node [
    id 1160
    label "wear"
  ]
  node [
    id 1161
    label "destruction"
  ]
  node [
    id 1162
    label "zu&#380;ycie"
  ]
  node [
    id 1163
    label "attrition"
  ]
  node [
    id 1164
    label "zaszkodzenie"
  ]
  node [
    id 1165
    label "os&#322;abienie"
  ]
  node [
    id 1166
    label "podpalenie"
  ]
  node [
    id 1167
    label "strata"
  ]
  node [
    id 1168
    label "kondycja_fizyczna"
  ]
  node [
    id 1169
    label "spl&#261;drowanie"
  ]
  node [
    id 1170
    label "zdrowie"
  ]
  node [
    id 1171
    label "poniszczenie"
  ]
  node [
    id 1172
    label "ruin"
  ]
  node [
    id 1173
    label "stanie_si&#281;"
  ]
  node [
    id 1174
    label "poniszczenie_si&#281;"
  ]
  node [
    id 1175
    label "wydawanie"
  ]
  node [
    id 1176
    label "depreciation"
  ]
  node [
    id 1177
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1178
    label "przedstawianie"
  ]
  node [
    id 1179
    label "przesadzanie"
  ]
  node [
    id 1180
    label "nazewnictwo"
  ]
  node [
    id 1181
    label "term"
  ]
  node [
    id 1182
    label "przypadni&#281;cie"
  ]
  node [
    id 1183
    label "ekspiracja"
  ]
  node [
    id 1184
    label "przypa&#347;&#263;"
  ]
  node [
    id 1185
    label "chronogram"
  ]
  node [
    id 1186
    label "nazwa"
  ]
  node [
    id 1187
    label "wezwanie"
  ]
  node [
    id 1188
    label "patron"
  ]
  node [
    id 1189
    label "leksem"
  ]
  node [
    id 1190
    label "poprzedzanie"
  ]
  node [
    id 1191
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1192
    label "laba"
  ]
  node [
    id 1193
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1194
    label "chronometria"
  ]
  node [
    id 1195
    label "rachuba_czasu"
  ]
  node [
    id 1196
    label "czasokres"
  ]
  node [
    id 1197
    label "odczyt"
  ]
  node [
    id 1198
    label "chwila"
  ]
  node [
    id 1199
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1200
    label "dzieje"
  ]
  node [
    id 1201
    label "poprzedzenie"
  ]
  node [
    id 1202
    label "trawienie"
  ]
  node [
    id 1203
    label "pochodzi&#263;"
  ]
  node [
    id 1204
    label "period"
  ]
  node [
    id 1205
    label "okres_czasu"
  ]
  node [
    id 1206
    label "poprzedza&#263;"
  ]
  node [
    id 1207
    label "schy&#322;ek"
  ]
  node [
    id 1208
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1209
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1210
    label "zegar"
  ]
  node [
    id 1211
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1212
    label "czwarty_wymiar"
  ]
  node [
    id 1213
    label "pochodzenie"
  ]
  node [
    id 1214
    label "Zeitgeist"
  ]
  node [
    id 1215
    label "trawi&#263;"
  ]
  node [
    id 1216
    label "pogoda"
  ]
  node [
    id 1217
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1218
    label "poprzedzi&#263;"
  ]
  node [
    id 1219
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1220
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1221
    label "time_period"
  ]
  node [
    id 1222
    label "practice"
  ]
  node [
    id 1223
    label "zwyczaj"
  ]
  node [
    id 1224
    label "terminology"
  ]
  node [
    id 1225
    label "wydech"
  ]
  node [
    id 1226
    label "ekspirowanie"
  ]
  node [
    id 1227
    label "spodoba&#263;_si&#281;"
  ]
  node [
    id 1228
    label "trafi&#263;_si&#281;"
  ]
  node [
    id 1229
    label "fall"
  ]
  node [
    id 1230
    label "pa&#347;&#263;"
  ]
  node [
    id 1231
    label "dotrze&#263;"
  ]
  node [
    id 1232
    label "wypa&#347;&#263;"
  ]
  node [
    id 1233
    label "przywrze&#263;"
  ]
  node [
    id 1234
    label "rzuci&#263;_si&#281;"
  ]
  node [
    id 1235
    label "barok"
  ]
  node [
    id 1236
    label "przytulenie_si&#281;"
  ]
  node [
    id 1237
    label "spadni&#281;cie"
  ]
  node [
    id 1238
    label "okrojenie_si&#281;"
  ]
  node [
    id 1239
    label "prolapse"
  ]
  node [
    id 1240
    label "pocz&#261;tki"
  ]
  node [
    id 1241
    label "ukra&#347;&#263;"
  ]
  node [
    id 1242
    label "ukradzenie"
  ]
  node [
    id 1243
    label "idea"
  ]
  node [
    id 1244
    label "p&#322;&#243;d"
  ]
  node [
    id 1245
    label "strategia"
  ]
  node [
    id 1246
    label "background"
  ]
  node [
    id 1247
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1248
    label "podpierdoli&#263;"
  ]
  node [
    id 1249
    label "dash_off"
  ]
  node [
    id 1250
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 1251
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 1252
    label "zabra&#263;"
  ]
  node [
    id 1253
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1254
    label "overcharge"
  ]
  node [
    id 1255
    label "podpierdolenie"
  ]
  node [
    id 1256
    label "zgini&#281;cie"
  ]
  node [
    id 1257
    label "przyw&#322;aszczenie"
  ]
  node [
    id 1258
    label "larceny"
  ]
  node [
    id 1259
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1260
    label "zw&#281;dzenie"
  ]
  node [
    id 1261
    label "okradzenie"
  ]
  node [
    id 1262
    label "nakradzenie"
  ]
  node [
    id 1263
    label "byt"
  ]
  node [
    id 1264
    label "Kant"
  ]
  node [
    id 1265
    label "ideacja"
  ]
  node [
    id 1266
    label "j&#261;dro"
  ]
  node [
    id 1267
    label "systemik"
  ]
  node [
    id 1268
    label "rozprz&#261;c"
  ]
  node [
    id 1269
    label "oprogramowanie"
  ]
  node [
    id 1270
    label "systemat"
  ]
  node [
    id 1271
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1272
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1273
    label "model"
  ]
  node [
    id 1274
    label "struktura"
  ]
  node [
    id 1275
    label "usenet"
  ]
  node [
    id 1276
    label "porz&#261;dek"
  ]
  node [
    id 1277
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1278
    label "przyn&#281;ta"
  ]
  node [
    id 1279
    label "net"
  ]
  node [
    id 1280
    label "w&#281;dkarstwo"
  ]
  node [
    id 1281
    label "eratem"
  ]
  node [
    id 1282
    label "doktryna"
  ]
  node [
    id 1283
    label "pulpit"
  ]
  node [
    id 1284
    label "konstelacja"
  ]
  node [
    id 1285
    label "jednostka_geologiczna"
  ]
  node [
    id 1286
    label "o&#347;"
  ]
  node [
    id 1287
    label "podsystem"
  ]
  node [
    id 1288
    label "ryba"
  ]
  node [
    id 1289
    label "Leopard"
  ]
  node [
    id 1290
    label "Android"
  ]
  node [
    id 1291
    label "zachowanie"
  ]
  node [
    id 1292
    label "cybernetyk"
  ]
  node [
    id 1293
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1294
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1295
    label "method"
  ]
  node [
    id 1296
    label "sk&#322;ad"
  ]
  node [
    id 1297
    label "podstawa"
  ]
  node [
    id 1298
    label "oprzyrz&#261;dowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 396
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 334
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 267
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 411
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 15
    target 416
  ]
  edge [
    source 15
    target 417
  ]
  edge [
    source 15
    target 418
  ]
  edge [
    source 15
    target 419
  ]
  edge [
    source 15
    target 420
  ]
  edge [
    source 15
    target 421
  ]
  edge [
    source 15
    target 422
  ]
  edge [
    source 15
    target 423
  ]
  edge [
    source 15
    target 424
  ]
  edge [
    source 15
    target 425
  ]
  edge [
    source 15
    target 339
  ]
  edge [
    source 15
    target 426
  ]
  edge [
    source 15
    target 427
  ]
  edge [
    source 15
    target 428
  ]
  edge [
    source 15
    target 429
  ]
  edge [
    source 15
    target 430
  ]
  edge [
    source 15
    target 431
  ]
  edge [
    source 15
    target 432
  ]
  edge [
    source 15
    target 433
  ]
  edge [
    source 15
    target 434
  ]
  edge [
    source 15
    target 435
  ]
  edge [
    source 15
    target 436
  ]
  edge [
    source 15
    target 437
  ]
  edge [
    source 15
    target 438
  ]
  edge [
    source 15
    target 439
  ]
  edge [
    source 15
    target 440
  ]
  edge [
    source 15
    target 441
  ]
  edge [
    source 15
    target 442
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 444
  ]
  edge [
    source 15
    target 445
  ]
  edge [
    source 15
    target 446
  ]
  edge [
    source 15
    target 447
  ]
  edge [
    source 15
    target 448
  ]
  edge [
    source 15
    target 62
  ]
  edge [
    source 15
    target 449
  ]
  edge [
    source 15
    target 450
  ]
  edge [
    source 15
    target 451
  ]
  edge [
    source 15
    target 452
  ]
  edge [
    source 15
    target 453
  ]
  edge [
    source 15
    target 454
  ]
  edge [
    source 15
    target 455
  ]
  edge [
    source 15
    target 456
  ]
  edge [
    source 15
    target 457
  ]
  edge [
    source 15
    target 458
  ]
  edge [
    source 15
    target 459
  ]
  edge [
    source 15
    target 460
  ]
  edge [
    source 15
    target 461
  ]
  edge [
    source 15
    target 462
  ]
  edge [
    source 15
    target 463
  ]
  edge [
    source 15
    target 464
  ]
  edge [
    source 15
    target 465
  ]
  edge [
    source 15
    target 466
  ]
  edge [
    source 15
    target 467
  ]
  edge [
    source 15
    target 468
  ]
  edge [
    source 15
    target 469
  ]
  edge [
    source 15
    target 470
  ]
  edge [
    source 15
    target 471
  ]
  edge [
    source 15
    target 472
  ]
  edge [
    source 15
    target 473
  ]
  edge [
    source 15
    target 474
  ]
  edge [
    source 15
    target 475
  ]
  edge [
    source 15
    target 476
  ]
  edge [
    source 15
    target 477
  ]
  edge [
    source 15
    target 478
  ]
  edge [
    source 15
    target 479
  ]
  edge [
    source 15
    target 480
  ]
  edge [
    source 15
    target 481
  ]
  edge [
    source 15
    target 482
  ]
  edge [
    source 15
    target 483
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 484
  ]
  edge [
    source 15
    target 485
  ]
  edge [
    source 15
    target 486
  ]
  edge [
    source 15
    target 487
  ]
  edge [
    source 15
    target 488
  ]
  edge [
    source 15
    target 489
  ]
  edge [
    source 15
    target 490
  ]
  edge [
    source 15
    target 491
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 493
  ]
  edge [
    source 15
    target 494
  ]
  edge [
    source 15
    target 495
  ]
  edge [
    source 15
    target 496
  ]
  edge [
    source 15
    target 497
  ]
  edge [
    source 15
    target 498
  ]
  edge [
    source 15
    target 499
  ]
  edge [
    source 15
    target 500
  ]
  edge [
    source 15
    target 501
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 502
  ]
  edge [
    source 15
    target 503
  ]
  edge [
    source 15
    target 504
  ]
  edge [
    source 15
    target 505
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 507
  ]
  edge [
    source 15
    target 508
  ]
  edge [
    source 15
    target 509
  ]
  edge [
    source 15
    target 510
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 511
  ]
  edge [
    source 15
    target 512
  ]
  edge [
    source 15
    target 513
  ]
  edge [
    source 15
    target 514
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 515
  ]
  edge [
    source 15
    target 516
  ]
  edge [
    source 15
    target 517
  ]
  edge [
    source 15
    target 245
  ]
  edge [
    source 15
    target 518
  ]
  edge [
    source 15
    target 519
  ]
  edge [
    source 15
    target 520
  ]
  edge [
    source 15
    target 521
  ]
  edge [
    source 15
    target 522
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 536
  ]
  edge [
    source 16
    target 537
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 544
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 547
  ]
  edge [
    source 16
    target 548
  ]
  edge [
    source 16
    target 549
  ]
  edge [
    source 16
    target 550
  ]
  edge [
    source 16
    target 551
  ]
  edge [
    source 16
    target 552
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 554
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 557
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 560
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 565
  ]
  edge [
    source 16
    target 566
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 313
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 615
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 335
  ]
  edge [
    source 18
    target 643
  ]
  edge [
    source 18
    target 644
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 19
    target 679
  ]
  edge [
    source 19
    target 68
  ]
  edge [
    source 19
    target 680
  ]
  edge [
    source 19
    target 681
  ]
  edge [
    source 19
    target 682
  ]
  edge [
    source 19
    target 683
  ]
  edge [
    source 19
    target 684
  ]
  edge [
    source 19
    target 685
  ]
  edge [
    source 19
    target 686
  ]
  edge [
    source 19
    target 687
  ]
  edge [
    source 19
    target 688
  ]
  edge [
    source 19
    target 689
  ]
  edge [
    source 19
    target 690
  ]
  edge [
    source 19
    target 691
  ]
  edge [
    source 19
    target 692
  ]
  edge [
    source 19
    target 693
  ]
  edge [
    source 19
    target 694
  ]
  edge [
    source 19
    target 695
  ]
  edge [
    source 19
    target 696
  ]
  edge [
    source 19
    target 697
  ]
  edge [
    source 19
    target 698
  ]
  edge [
    source 19
    target 699
  ]
  edge [
    source 19
    target 700
  ]
  edge [
    source 19
    target 82
  ]
  edge [
    source 19
    target 83
  ]
  edge [
    source 19
    target 84
  ]
  edge [
    source 19
    target 85
  ]
  edge [
    source 19
    target 86
  ]
  edge [
    source 19
    target 87
  ]
  edge [
    source 19
    target 88
  ]
  edge [
    source 19
    target 89
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 94
  ]
  edge [
    source 19
    target 95
  ]
  edge [
    source 19
    target 96
  ]
  edge [
    source 19
    target 97
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 100
  ]
  edge [
    source 19
    target 101
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 106
  ]
  edge [
    source 19
    target 701
  ]
  edge [
    source 19
    target 702
  ]
  edge [
    source 19
    target 703
  ]
  edge [
    source 19
    target 704
  ]
  edge [
    source 19
    target 705
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 708
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 709
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 712
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 714
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 19
    target 720
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 727
  ]
  edge [
    source 19
    target 728
  ]
  edge [
    source 19
    target 729
  ]
  edge [
    source 19
    target 730
  ]
  edge [
    source 19
    target 731
  ]
  edge [
    source 19
    target 732
  ]
  edge [
    source 19
    target 733
  ]
  edge [
    source 19
    target 734
  ]
  edge [
    source 19
    target 735
  ]
  edge [
    source 19
    target 736
  ]
  edge [
    source 19
    target 737
  ]
  edge [
    source 19
    target 738
  ]
  edge [
    source 19
    target 416
  ]
  edge [
    source 19
    target 739
  ]
  edge [
    source 19
    target 740
  ]
  edge [
    source 19
    target 741
  ]
  edge [
    source 19
    target 742
  ]
  edge [
    source 19
    target 743
  ]
  edge [
    source 19
    target 744
  ]
  edge [
    source 19
    target 745
  ]
  edge [
    source 19
    target 746
  ]
  edge [
    source 19
    target 525
  ]
  edge [
    source 19
    target 747
  ]
  edge [
    source 19
    target 748
  ]
  edge [
    source 19
    target 749
  ]
  edge [
    source 19
    target 750
  ]
  edge [
    source 19
    target 751
  ]
  edge [
    source 19
    target 752
  ]
  edge [
    source 19
    target 753
  ]
  edge [
    source 19
    target 754
  ]
  edge [
    source 19
    target 755
  ]
  edge [
    source 19
    target 756
  ]
  edge [
    source 19
    target 757
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 758
  ]
  edge [
    source 19
    target 759
  ]
  edge [
    source 19
    target 760
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 761
  ]
  edge [
    source 19
    target 762
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 763
  ]
  edge [
    source 19
    target 764
  ]
  edge [
    source 19
    target 527
  ]
  edge [
    source 19
    target 765
  ]
  edge [
    source 19
    target 766
  ]
  edge [
    source 19
    target 767
  ]
  edge [
    source 19
    target 768
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 769
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 619
  ]
  edge [
    source 19
    target 771
  ]
  edge [
    source 19
    target 772
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 775
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 533
  ]
  edge [
    source 19
    target 534
  ]
  edge [
    source 19
    target 777
  ]
  edge [
    source 19
    target 535
  ]
  edge [
    source 19
    target 778
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 537
  ]
  edge [
    source 19
    target 538
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 544
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 546
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 810
  ]
  edge [
    source 20
    target 811
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 20
    target 813
  ]
  edge [
    source 20
    target 814
  ]
  edge [
    source 20
    target 815
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 20
    target 818
  ]
  edge [
    source 20
    target 819
  ]
  edge [
    source 20
    target 820
  ]
  edge [
    source 20
    target 821
  ]
  edge [
    source 20
    target 822
  ]
  edge [
    source 20
    target 823
  ]
  edge [
    source 20
    target 824
  ]
  edge [
    source 20
    target 825
  ]
  edge [
    source 20
    target 826
  ]
  edge [
    source 20
    target 827
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 830
  ]
  edge [
    source 20
    target 831
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 834
  ]
  edge [
    source 21
    target 835
  ]
  edge [
    source 21
    target 836
  ]
  edge [
    source 21
    target 837
  ]
  edge [
    source 21
    target 838
  ]
  edge [
    source 21
    target 839
  ]
  edge [
    source 21
    target 840
  ]
  edge [
    source 21
    target 841
  ]
  edge [
    source 21
    target 842
  ]
  edge [
    source 21
    target 843
  ]
  edge [
    source 21
    target 844
  ]
  edge [
    source 21
    target 845
  ]
  edge [
    source 21
    target 846
  ]
  edge [
    source 21
    target 847
  ]
  edge [
    source 21
    target 848
  ]
  edge [
    source 21
    target 849
  ]
  edge [
    source 21
    target 850
  ]
  edge [
    source 21
    target 851
  ]
  edge [
    source 21
    target 852
  ]
  edge [
    source 21
    target 853
  ]
  edge [
    source 21
    target 854
  ]
  edge [
    source 21
    target 855
  ]
  edge [
    source 21
    target 856
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 858
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 859
  ]
  edge [
    source 21
    target 860
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 861
  ]
  edge [
    source 23
    target 862
  ]
  edge [
    source 23
    target 863
  ]
  edge [
    source 23
    target 864
  ]
  edge [
    source 23
    target 865
  ]
  edge [
    source 23
    target 595
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 866
  ]
  edge [
    source 25
    target 867
  ]
  edge [
    source 25
    target 868
  ]
  edge [
    source 25
    target 869
  ]
  edge [
    source 25
    target 870
  ]
  edge [
    source 25
    target 871
  ]
  edge [
    source 25
    target 872
  ]
  edge [
    source 25
    target 588
  ]
  edge [
    source 25
    target 873
  ]
  edge [
    source 25
    target 874
  ]
  edge [
    source 25
    target 875
  ]
  edge [
    source 25
    target 876
  ]
  edge [
    source 25
    target 877
  ]
  edge [
    source 25
    target 878
  ]
  edge [
    source 25
    target 879
  ]
  edge [
    source 25
    target 880
  ]
  edge [
    source 25
    target 881
  ]
  edge [
    source 25
    target 882
  ]
  edge [
    source 25
    target 883
  ]
  edge [
    source 25
    target 884
  ]
  edge [
    source 25
    target 885
  ]
  edge [
    source 25
    target 886
  ]
  edge [
    source 25
    target 887
  ]
  edge [
    source 25
    target 888
  ]
  edge [
    source 25
    target 889
  ]
  edge [
    source 25
    target 890
  ]
  edge [
    source 25
    target 416
  ]
  edge [
    source 25
    target 891
  ]
  edge [
    source 25
    target 892
  ]
  edge [
    source 25
    target 893
  ]
  edge [
    source 25
    target 894
  ]
  edge [
    source 25
    target 895
  ]
  edge [
    source 25
    target 896
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 897
  ]
  edge [
    source 25
    target 101
  ]
  edge [
    source 25
    target 898
  ]
  edge [
    source 25
    target 899
  ]
  edge [
    source 25
    target 900
  ]
  edge [
    source 25
    target 901
  ]
  edge [
    source 25
    target 902
  ]
  edge [
    source 25
    target 903
  ]
  edge [
    source 25
    target 904
  ]
  edge [
    source 25
    target 905
  ]
  edge [
    source 25
    target 906
  ]
  edge [
    source 25
    target 907
  ]
  edge [
    source 25
    target 908
  ]
  edge [
    source 25
    target 909
  ]
  edge [
    source 25
    target 128
  ]
  edge [
    source 25
    target 910
  ]
  edge [
    source 25
    target 911
  ]
  edge [
    source 25
    target 912
  ]
  edge [
    source 25
    target 913
  ]
  edge [
    source 25
    target 914
  ]
  edge [
    source 25
    target 915
  ]
  edge [
    source 25
    target 657
  ]
  edge [
    source 25
    target 916
  ]
  edge [
    source 25
    target 917
  ]
  edge [
    source 25
    target 918
  ]
  edge [
    source 25
    target 919
  ]
  edge [
    source 25
    target 920
  ]
  edge [
    source 25
    target 97
  ]
  edge [
    source 25
    target 921
  ]
  edge [
    source 25
    target 922
  ]
  edge [
    source 25
    target 923
  ]
  edge [
    source 25
    target 924
  ]
  edge [
    source 25
    target 925
  ]
  edge [
    source 25
    target 926
  ]
  edge [
    source 25
    target 927
  ]
  edge [
    source 25
    target 928
  ]
  edge [
    source 25
    target 929
  ]
  edge [
    source 25
    target 930
  ]
  edge [
    source 25
    target 931
  ]
  edge [
    source 25
    target 249
  ]
  edge [
    source 25
    target 932
  ]
  edge [
    source 25
    target 933
  ]
  edge [
    source 25
    target 934
  ]
  edge [
    source 25
    target 935
  ]
  edge [
    source 25
    target 936
  ]
  edge [
    source 25
    target 937
  ]
  edge [
    source 25
    target 938
  ]
  edge [
    source 25
    target 939
  ]
  edge [
    source 25
    target 940
  ]
  edge [
    source 25
    target 941
  ]
  edge [
    source 25
    target 942
  ]
  edge [
    source 25
    target 943
  ]
  edge [
    source 25
    target 705
  ]
  edge [
    source 25
    target 944
  ]
  edge [
    source 25
    target 706
  ]
  edge [
    source 25
    target 945
  ]
  edge [
    source 25
    target 946
  ]
  edge [
    source 25
    target 947
  ]
  edge [
    source 25
    target 948
  ]
  edge [
    source 25
    target 949
  ]
  edge [
    source 25
    target 802
  ]
  edge [
    source 25
    target 950
  ]
  edge [
    source 25
    target 951
  ]
  edge [
    source 25
    target 952
  ]
  edge [
    source 25
    target 74
  ]
  edge [
    source 25
    target 953
  ]
  edge [
    source 25
    target 954
  ]
  edge [
    source 25
    target 955
  ]
  edge [
    source 25
    target 719
  ]
  edge [
    source 25
    target 956
  ]
  edge [
    source 25
    target 957
  ]
  edge [
    source 25
    target 111
  ]
  edge [
    source 25
    target 958
  ]
  edge [
    source 25
    target 959
  ]
  edge [
    source 25
    target 960
  ]
  edge [
    source 25
    target 961
  ]
  edge [
    source 25
    target 724
  ]
  edge [
    source 25
    target 962
  ]
  edge [
    source 25
    target 963
  ]
  edge [
    source 25
    target 964
  ]
  edge [
    source 25
    target 965
  ]
  edge [
    source 25
    target 966
  ]
  edge [
    source 25
    target 967
  ]
  edge [
    source 25
    target 968
  ]
  edge [
    source 25
    target 969
  ]
  edge [
    source 25
    target 970
  ]
  edge [
    source 25
    target 971
  ]
  edge [
    source 25
    target 972
  ]
  edge [
    source 25
    target 973
  ]
  edge [
    source 25
    target 974
  ]
  edge [
    source 25
    target 975
  ]
  edge [
    source 25
    target 976
  ]
  edge [
    source 25
    target 977
  ]
  edge [
    source 25
    target 978
  ]
  edge [
    source 25
    target 979
  ]
  edge [
    source 25
    target 980
  ]
  edge [
    source 25
    target 344
  ]
  edge [
    source 25
    target 981
  ]
  edge [
    source 25
    target 982
  ]
  edge [
    source 25
    target 983
  ]
  edge [
    source 25
    target 984
  ]
  edge [
    source 25
    target 985
  ]
  edge [
    source 25
    target 986
  ]
  edge [
    source 25
    target 987
  ]
  edge [
    source 25
    target 988
  ]
  edge [
    source 25
    target 989
  ]
  edge [
    source 25
    target 990
  ]
  edge [
    source 25
    target 991
  ]
  edge [
    source 25
    target 992
  ]
  edge [
    source 25
    target 993
  ]
  edge [
    source 25
    target 994
  ]
  edge [
    source 25
    target 995
  ]
  edge [
    source 25
    target 996
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 997
  ]
  edge [
    source 26
    target 998
  ]
  edge [
    source 26
    target 999
  ]
  edge [
    source 26
    target 1000
  ]
  edge [
    source 26
    target 1001
  ]
  edge [
    source 26
    target 1002
  ]
  edge [
    source 26
    target 1003
  ]
  edge [
    source 26
    target 1004
  ]
  edge [
    source 26
    target 1005
  ]
  edge [
    source 26
    target 1006
  ]
  edge [
    source 26
    target 1007
  ]
  edge [
    source 26
    target 1008
  ]
  edge [
    source 26
    target 1009
  ]
  edge [
    source 26
    target 1010
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1011
  ]
  edge [
    source 28
    target 1012
  ]
  edge [
    source 28
    target 1013
  ]
  edge [
    source 28
    target 1014
  ]
  edge [
    source 28
    target 1015
  ]
  edge [
    source 28
    target 1016
  ]
  edge [
    source 28
    target 1017
  ]
  edge [
    source 28
    target 1018
  ]
  edge [
    source 28
    target 1019
  ]
  edge [
    source 28
    target 1020
  ]
  edge [
    source 28
    target 1021
  ]
  edge [
    source 28
    target 1022
  ]
  edge [
    source 28
    target 1023
  ]
  edge [
    source 28
    target 1024
  ]
  edge [
    source 28
    target 1025
  ]
  edge [
    source 28
    target 1026
  ]
  edge [
    source 28
    target 1027
  ]
  edge [
    source 28
    target 1028
  ]
  edge [
    source 28
    target 1029
  ]
  edge [
    source 28
    target 1030
  ]
  edge [
    source 28
    target 1031
  ]
  edge [
    source 28
    target 1032
  ]
  edge [
    source 28
    target 852
  ]
  edge [
    source 28
    target 1033
  ]
  edge [
    source 28
    target 1034
  ]
  edge [
    source 28
    target 1035
  ]
  edge [
    source 28
    target 1036
  ]
  edge [
    source 28
    target 1037
  ]
  edge [
    source 28
    target 1038
  ]
  edge [
    source 28
    target 1039
  ]
  edge [
    source 28
    target 1040
  ]
  edge [
    source 28
    target 1041
  ]
  edge [
    source 28
    target 1042
  ]
  edge [
    source 28
    target 1043
  ]
  edge [
    source 28
    target 1044
  ]
  edge [
    source 28
    target 1045
  ]
  edge [
    source 28
    target 1046
  ]
  edge [
    source 28
    target 1047
  ]
  edge [
    source 28
    target 1048
  ]
  edge [
    source 28
    target 1049
  ]
  edge [
    source 28
    target 1050
  ]
  edge [
    source 28
    target 1051
  ]
  edge [
    source 28
    target 1052
  ]
  edge [
    source 28
    target 1053
  ]
  edge [
    source 28
    target 502
  ]
  edge [
    source 28
    target 1054
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 979
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 609
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 28
    target 1093
  ]
  edge [
    source 28
    target 344
  ]
  edge [
    source 28
    target 1094
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 595
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 1096
  ]
  edge [
    source 28
    target 1097
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 115
  ]
  edge [
    source 28
    target 1099
  ]
  edge [
    source 28
    target 1100
  ]
  edge [
    source 28
    target 1101
  ]
  edge [
    source 28
    target 1102
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1104
  ]
  edge [
    source 28
    target 1105
  ]
  edge [
    source 28
    target 1106
  ]
  edge [
    source 28
    target 1107
  ]
  edge [
    source 28
    target 1108
  ]
  edge [
    source 28
    target 1109
  ]
  edge [
    source 28
    target 1110
  ]
  edge [
    source 28
    target 1111
  ]
  edge [
    source 28
    target 1112
  ]
  edge [
    source 28
    target 1113
  ]
  edge [
    source 28
    target 1114
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 1116
  ]
  edge [
    source 28
    target 1117
  ]
  edge [
    source 28
    target 1118
  ]
  edge [
    source 28
    target 1119
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1121
  ]
  edge [
    source 29
    target 1122
  ]
  edge [
    source 29
    target 1123
  ]
  edge [
    source 29
    target 1124
  ]
  edge [
    source 29
    target 1125
  ]
  edge [
    source 29
    target 90
  ]
  edge [
    source 29
    target 1126
  ]
  edge [
    source 29
    target 1127
  ]
  edge [
    source 29
    target 1128
  ]
  edge [
    source 29
    target 1129
  ]
  edge [
    source 29
    target 1130
  ]
  edge [
    source 29
    target 261
  ]
  edge [
    source 29
    target 1131
  ]
  edge [
    source 29
    target 995
  ]
  edge [
    source 29
    target 987
  ]
  edge [
    source 29
    target 975
  ]
  edge [
    source 29
    target 996
  ]
  edge [
    source 29
    target 1132
  ]
  edge [
    source 29
    target 1133
  ]
  edge [
    source 29
    target 68
  ]
  edge [
    source 29
    target 586
  ]
  edge [
    source 29
    target 1134
  ]
  edge [
    source 29
    target 1135
  ]
  edge [
    source 29
    target 1136
  ]
  edge [
    source 29
    target 1137
  ]
  edge [
    source 29
    target 339
  ]
  edge [
    source 29
    target 1138
  ]
  edge [
    source 29
    target 1139
  ]
  edge [
    source 29
    target 1140
  ]
  edge [
    source 29
    target 1141
  ]
  edge [
    source 29
    target 872
  ]
  edge [
    source 29
    target 1142
  ]
  edge [
    source 29
    target 1143
  ]
  edge [
    source 29
    target 1144
  ]
  edge [
    source 29
    target 1145
  ]
  edge [
    source 29
    target 1146
  ]
  edge [
    source 29
    target 1147
  ]
  edge [
    source 29
    target 1148
  ]
  edge [
    source 29
    target 913
  ]
  edge [
    source 29
    target 1149
  ]
  edge [
    source 29
    target 1150
  ]
  edge [
    source 29
    target 1151
  ]
  edge [
    source 29
    target 1152
  ]
  edge [
    source 29
    target 1153
  ]
  edge [
    source 29
    target 1154
  ]
  edge [
    source 29
    target 1155
  ]
  edge [
    source 29
    target 1156
  ]
  edge [
    source 29
    target 1157
  ]
  edge [
    source 29
    target 1158
  ]
  edge [
    source 29
    target 1159
  ]
  edge [
    source 29
    target 1160
  ]
  edge [
    source 29
    target 1161
  ]
  edge [
    source 29
    target 1162
  ]
  edge [
    source 29
    target 1163
  ]
  edge [
    source 29
    target 1164
  ]
  edge [
    source 29
    target 1165
  ]
  edge [
    source 29
    target 1166
  ]
  edge [
    source 29
    target 1167
  ]
  edge [
    source 29
    target 1168
  ]
  edge [
    source 29
    target 344
  ]
  edge [
    source 29
    target 1169
  ]
  edge [
    source 29
    target 1170
  ]
  edge [
    source 29
    target 1171
  ]
  edge [
    source 29
    target 1172
  ]
  edge [
    source 29
    target 1173
  ]
  edge [
    source 29
    target 894
  ]
  edge [
    source 29
    target 1174
  ]
  edge [
    source 29
    target 1175
  ]
  edge [
    source 29
    target 1176
  ]
  edge [
    source 29
    target 1177
  ]
  edge [
    source 29
    target 1178
  ]
  edge [
    source 29
    target 1179
  ]
  edge [
    source 30
    target 1180
  ]
  edge [
    source 30
    target 1181
  ]
  edge [
    source 30
    target 1182
  ]
  edge [
    source 30
    target 1183
  ]
  edge [
    source 30
    target 1184
  ]
  edge [
    source 30
    target 1185
  ]
  edge [
    source 30
    target 111
  ]
  edge [
    source 30
    target 705
  ]
  edge [
    source 30
    target 1186
  ]
  edge [
    source 30
    target 1187
  ]
  edge [
    source 30
    target 1188
  ]
  edge [
    source 30
    target 1189
  ]
  edge [
    source 30
    target 1190
  ]
  edge [
    source 30
    target 1191
  ]
  edge [
    source 30
    target 1192
  ]
  edge [
    source 30
    target 1193
  ]
  edge [
    source 30
    target 1194
  ]
  edge [
    source 30
    target 40
  ]
  edge [
    source 30
    target 1195
  ]
  edge [
    source 30
    target 975
  ]
  edge [
    source 30
    target 867
  ]
  edge [
    source 30
    target 1196
  ]
  edge [
    source 30
    target 1197
  ]
  edge [
    source 30
    target 1198
  ]
  edge [
    source 30
    target 1199
  ]
  edge [
    source 30
    target 1200
  ]
  edge [
    source 30
    target 568
  ]
  edge [
    source 30
    target 1201
  ]
  edge [
    source 30
    target 1202
  ]
  edge [
    source 30
    target 1203
  ]
  edge [
    source 30
    target 1204
  ]
  edge [
    source 30
    target 1205
  ]
  edge [
    source 30
    target 1206
  ]
  edge [
    source 30
    target 1207
  ]
  edge [
    source 30
    target 1208
  ]
  edge [
    source 30
    target 1209
  ]
  edge [
    source 30
    target 1210
  ]
  edge [
    source 30
    target 1211
  ]
  edge [
    source 30
    target 1212
  ]
  edge [
    source 30
    target 1213
  ]
  edge [
    source 30
    target 576
  ]
  edge [
    source 30
    target 1214
  ]
  edge [
    source 30
    target 1215
  ]
  edge [
    source 30
    target 1216
  ]
  edge [
    source 30
    target 1217
  ]
  edge [
    source 30
    target 1218
  ]
  edge [
    source 30
    target 1219
  ]
  edge [
    source 30
    target 1220
  ]
  edge [
    source 30
    target 1221
  ]
  edge [
    source 30
    target 1222
  ]
  edge [
    source 30
    target 870
  ]
  edge [
    source 30
    target 873
  ]
  edge [
    source 30
    target 874
  ]
  edge [
    source 30
    target 734
  ]
  edge [
    source 30
    target 802
  ]
  edge [
    source 30
    target 1223
  ]
  edge [
    source 30
    target 880
  ]
  edge [
    source 30
    target 416
  ]
  edge [
    source 30
    target 517
  ]
  edge [
    source 30
    target 1224
  ]
  edge [
    source 30
    target 766
  ]
  edge [
    source 30
    target 1225
  ]
  edge [
    source 30
    target 1226
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 1227
  ]
  edge [
    source 30
    target 1228
  ]
  edge [
    source 30
    target 1229
  ]
  edge [
    source 30
    target 1230
  ]
  edge [
    source 30
    target 1231
  ]
  edge [
    source 30
    target 1232
  ]
  edge [
    source 30
    target 1233
  ]
  edge [
    source 30
    target 1234
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 1235
  ]
  edge [
    source 30
    target 1236
  ]
  edge [
    source 30
    target 1237
  ]
  edge [
    source 30
    target 657
  ]
  edge [
    source 30
    target 1238
  ]
  edge [
    source 30
    target 1239
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 604
  ]
  edge [
    source 33
    target 1240
  ]
  edge [
    source 33
    target 1241
  ]
  edge [
    source 33
    target 1242
  ]
  edge [
    source 33
    target 1243
  ]
  edge [
    source 33
    target 706
  ]
  edge [
    source 33
    target 68
  ]
  edge [
    source 33
    target 1244
  ]
  edge [
    source 33
    target 423
  ]
  edge [
    source 33
    target 894
  ]
  edge [
    source 33
    target 1245
  ]
  edge [
    source 33
    target 1246
  ]
  edge [
    source 33
    target 1247
  ]
  edge [
    source 33
    target 1248
  ]
  edge [
    source 33
    target 1249
  ]
  edge [
    source 33
    target 1250
  ]
  edge [
    source 33
    target 1251
  ]
  edge [
    source 33
    target 1252
  ]
  edge [
    source 33
    target 1253
  ]
  edge [
    source 33
    target 1254
  ]
  edge [
    source 33
    target 1255
  ]
  edge [
    source 33
    target 1256
  ]
  edge [
    source 33
    target 1257
  ]
  edge [
    source 33
    target 1258
  ]
  edge [
    source 33
    target 1259
  ]
  edge [
    source 33
    target 1260
  ]
  edge [
    source 33
    target 1261
  ]
  edge [
    source 33
    target 1262
  ]
  edge [
    source 33
    target 724
  ]
  edge [
    source 33
    target 1263
  ]
  edge [
    source 33
    target 905
  ]
  edge [
    source 33
    target 1264
  ]
  edge [
    source 33
    target 741
  ]
  edge [
    source 33
    target 766
  ]
  edge [
    source 33
    target 104
  ]
  edge [
    source 33
    target 1265
  ]
  edge [
    source 33
    target 1266
  ]
  edge [
    source 33
    target 1267
  ]
  edge [
    source 33
    target 1268
  ]
  edge [
    source 33
    target 1269
  ]
  edge [
    source 33
    target 1270
  ]
  edge [
    source 33
    target 1271
  ]
  edge [
    source 33
    target 128
  ]
  edge [
    source 33
    target 1272
  ]
  edge [
    source 33
    target 1273
  ]
  edge [
    source 33
    target 1274
  ]
  edge [
    source 33
    target 1275
  ]
  edge [
    source 33
    target 462
  ]
  edge [
    source 33
    target 132
  ]
  edge [
    source 33
    target 1276
  ]
  edge [
    source 33
    target 1277
  ]
  edge [
    source 33
    target 1278
  ]
  edge [
    source 33
    target 1279
  ]
  edge [
    source 33
    target 1280
  ]
  edge [
    source 33
    target 1281
  ]
  edge [
    source 33
    target 138
  ]
  edge [
    source 33
    target 1282
  ]
  edge [
    source 33
    target 1283
  ]
  edge [
    source 33
    target 1284
  ]
  edge [
    source 33
    target 1285
  ]
  edge [
    source 33
    target 1286
  ]
  edge [
    source 33
    target 1287
  ]
  edge [
    source 33
    target 719
  ]
  edge [
    source 33
    target 1288
  ]
  edge [
    source 33
    target 1289
  ]
  edge [
    source 33
    target 108
  ]
  edge [
    source 33
    target 1290
  ]
  edge [
    source 33
    target 1291
  ]
  edge [
    source 33
    target 1292
  ]
  edge [
    source 33
    target 1293
  ]
  edge [
    source 33
    target 1294
  ]
  edge [
    source 33
    target 1295
  ]
  edge [
    source 33
    target 1296
  ]
  edge [
    source 33
    target 1297
  ]
  edge [
    source 33
    target 1298
  ]
]
