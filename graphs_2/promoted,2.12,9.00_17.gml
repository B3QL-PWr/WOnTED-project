graph [
  node [
    id 0
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "badanie"
    origin "text"
  ]
  node [
    id 2
    label "archeologiczny"
    origin "text"
  ]
  node [
    id 3
    label "cmentarzysko"
    origin "text"
  ]
  node [
    id 4
    label "woj"
    origin "text"
  ]
  node [
    id 5
    label "powiat"
    origin "text"
  ]
  node [
    id 6
    label "gorzowski"
    origin "text"
  ]
  node [
    id 7
    label "dispose"
  ]
  node [
    id 8
    label "zrezygnowa&#263;"
  ]
  node [
    id 9
    label "zrobi&#263;"
  ]
  node [
    id 10
    label "cause"
  ]
  node [
    id 11
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 12
    label "wytworzy&#263;"
  ]
  node [
    id 13
    label "communicate"
  ]
  node [
    id 14
    label "przesta&#263;"
  ]
  node [
    id 15
    label "drop"
  ]
  node [
    id 16
    label "post&#261;pi&#263;"
  ]
  node [
    id 17
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 18
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 19
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 20
    label "zorganizowa&#263;"
  ]
  node [
    id 21
    label "appoint"
  ]
  node [
    id 22
    label "wystylizowa&#263;"
  ]
  node [
    id 23
    label "przerobi&#263;"
  ]
  node [
    id 24
    label "nabra&#263;"
  ]
  node [
    id 25
    label "make"
  ]
  node [
    id 26
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 27
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 28
    label "wydali&#263;"
  ]
  node [
    id 29
    label "manufacture"
  ]
  node [
    id 30
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 31
    label "model"
  ]
  node [
    id 32
    label "nada&#263;"
  ]
  node [
    id 33
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 34
    label "coating"
  ]
  node [
    id 35
    label "sko&#324;czy&#263;"
  ]
  node [
    id 36
    label "leave_office"
  ]
  node [
    id 37
    label "fail"
  ]
  node [
    id 38
    label "obserwowanie"
  ]
  node [
    id 39
    label "zrecenzowanie"
  ]
  node [
    id 40
    label "kontrola"
  ]
  node [
    id 41
    label "analysis"
  ]
  node [
    id 42
    label "rektalny"
  ]
  node [
    id 43
    label "ustalenie"
  ]
  node [
    id 44
    label "macanie"
  ]
  node [
    id 45
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 46
    label "usi&#322;owanie"
  ]
  node [
    id 47
    label "udowadnianie"
  ]
  node [
    id 48
    label "praca"
  ]
  node [
    id 49
    label "bia&#322;a_niedziela"
  ]
  node [
    id 50
    label "diagnostyka"
  ]
  node [
    id 51
    label "dociekanie"
  ]
  node [
    id 52
    label "rezultat"
  ]
  node [
    id 53
    label "sprawdzanie"
  ]
  node [
    id 54
    label "penetrowanie"
  ]
  node [
    id 55
    label "czynno&#347;&#263;"
  ]
  node [
    id 56
    label "krytykowanie"
  ]
  node [
    id 57
    label "omawianie"
  ]
  node [
    id 58
    label "ustalanie"
  ]
  node [
    id 59
    label "rozpatrywanie"
  ]
  node [
    id 60
    label "investigation"
  ]
  node [
    id 61
    label "wziernikowanie"
  ]
  node [
    id 62
    label "examination"
  ]
  node [
    id 63
    label "discussion"
  ]
  node [
    id 64
    label "dyskutowanie"
  ]
  node [
    id 65
    label "temat"
  ]
  node [
    id 66
    label "czepianie_si&#281;"
  ]
  node [
    id 67
    label "opiniowanie"
  ]
  node [
    id 68
    label "ocenianie"
  ]
  node [
    id 69
    label "zaopiniowanie"
  ]
  node [
    id 70
    label "przeszukiwanie"
  ]
  node [
    id 71
    label "docieranie"
  ]
  node [
    id 72
    label "penetration"
  ]
  node [
    id 73
    label "decyzja"
  ]
  node [
    id 74
    label "umocnienie"
  ]
  node [
    id 75
    label "appointment"
  ]
  node [
    id 76
    label "spowodowanie"
  ]
  node [
    id 77
    label "localization"
  ]
  node [
    id 78
    label "informacja"
  ]
  node [
    id 79
    label "zdecydowanie"
  ]
  node [
    id 80
    label "zrobienie"
  ]
  node [
    id 81
    label "colony"
  ]
  node [
    id 82
    label "powodowanie"
  ]
  node [
    id 83
    label "robienie"
  ]
  node [
    id 84
    label "colonization"
  ]
  node [
    id 85
    label "decydowanie"
  ]
  node [
    id 86
    label "umacnianie"
  ]
  node [
    id 87
    label "liquidation"
  ]
  node [
    id 88
    label "przemy&#347;liwanie"
  ]
  node [
    id 89
    label "dzia&#322;anie"
  ]
  node [
    id 90
    label "typ"
  ]
  node [
    id 91
    label "event"
  ]
  node [
    id 92
    label "przyczyna"
  ]
  node [
    id 93
    label "legalizacja_ponowna"
  ]
  node [
    id 94
    label "instytucja"
  ]
  node [
    id 95
    label "w&#322;adza"
  ]
  node [
    id 96
    label "perlustracja"
  ]
  node [
    id 97
    label "legalizacja_pierwotna"
  ]
  node [
    id 98
    label "activity"
  ]
  node [
    id 99
    label "bezproblemowy"
  ]
  node [
    id 100
    label "wydarzenie"
  ]
  node [
    id 101
    label "podejmowanie"
  ]
  node [
    id 102
    label "effort"
  ]
  node [
    id 103
    label "staranie_si&#281;"
  ]
  node [
    id 104
    label "essay"
  ]
  node [
    id 105
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 106
    label "redagowanie"
  ]
  node [
    id 107
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 108
    label "przymierzanie"
  ]
  node [
    id 109
    label "przymierzenie"
  ]
  node [
    id 110
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 111
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 112
    label "najem"
  ]
  node [
    id 113
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 114
    label "zak&#322;ad"
  ]
  node [
    id 115
    label "stosunek_pracy"
  ]
  node [
    id 116
    label "benedykty&#324;ski"
  ]
  node [
    id 117
    label "poda&#380;_pracy"
  ]
  node [
    id 118
    label "pracowanie"
  ]
  node [
    id 119
    label "tyrka"
  ]
  node [
    id 120
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 121
    label "wytw&#243;r"
  ]
  node [
    id 122
    label "miejsce"
  ]
  node [
    id 123
    label "zaw&#243;d"
  ]
  node [
    id 124
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 125
    label "tynkarski"
  ]
  node [
    id 126
    label "pracowa&#263;"
  ]
  node [
    id 127
    label "zmiana"
  ]
  node [
    id 128
    label "czynnik_produkcji"
  ]
  node [
    id 129
    label "zobowi&#261;zanie"
  ]
  node [
    id 130
    label "kierownictwo"
  ]
  node [
    id 131
    label "siedziba"
  ]
  node [
    id 132
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 133
    label "analizowanie"
  ]
  node [
    id 134
    label "uzasadnianie"
  ]
  node [
    id 135
    label "presentation"
  ]
  node [
    id 136
    label "pokazywanie"
  ]
  node [
    id 137
    label "endoscopy"
  ]
  node [
    id 138
    label "rozmy&#347;lanie"
  ]
  node [
    id 139
    label "quest"
  ]
  node [
    id 140
    label "dop&#322;ywanie"
  ]
  node [
    id 141
    label "examen"
  ]
  node [
    id 142
    label "diagnosis"
  ]
  node [
    id 143
    label "medycyna"
  ]
  node [
    id 144
    label "anamneza"
  ]
  node [
    id 145
    label "dotykanie"
  ]
  node [
    id 146
    label "dr&#243;b"
  ]
  node [
    id 147
    label "pomacanie"
  ]
  node [
    id 148
    label "feel"
  ]
  node [
    id 149
    label "palpation"
  ]
  node [
    id 150
    label "namacanie"
  ]
  node [
    id 151
    label "hodowanie"
  ]
  node [
    id 152
    label "patrzenie"
  ]
  node [
    id 153
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 154
    label "doszukanie_si&#281;"
  ]
  node [
    id 155
    label "dostrzeganie"
  ]
  node [
    id 156
    label "poobserwowanie"
  ]
  node [
    id 157
    label "observation"
  ]
  node [
    id 158
    label "bocianie_gniazdo"
  ]
  node [
    id 159
    label "charakterystyczny"
  ]
  node [
    id 160
    label "naukowy"
  ]
  node [
    id 161
    label "archeologicznie"
  ]
  node [
    id 162
    label "charakterystycznie"
  ]
  node [
    id 163
    label "naukowo"
  ]
  node [
    id 164
    label "szczeg&#243;lny"
  ]
  node [
    id 165
    label "wyj&#261;tkowy"
  ]
  node [
    id 166
    label "typowy"
  ]
  node [
    id 167
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 168
    label "podobny"
  ]
  node [
    id 169
    label "teoretyczny"
  ]
  node [
    id 170
    label "edukacyjnie"
  ]
  node [
    id 171
    label "scjentyficzny"
  ]
  node [
    id 172
    label "skomplikowany"
  ]
  node [
    id 173
    label "specjalistyczny"
  ]
  node [
    id 174
    label "zgodny"
  ]
  node [
    id 175
    label "intelektualny"
  ]
  node [
    id 176
    label "specjalny"
  ]
  node [
    id 177
    label "cmentarz"
  ]
  node [
    id 178
    label "cinerarium"
  ]
  node [
    id 179
    label "&#380;alnik"
  ]
  node [
    id 180
    label "park_sztywnych"
  ]
  node [
    id 181
    label "sm&#281;tarz"
  ]
  node [
    id 182
    label "pogrobowisko"
  ]
  node [
    id 183
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 184
    label "smentarz"
  ]
  node [
    id 185
    label "&#380;o&#322;nierz"
  ]
  node [
    id 186
    label "wite&#378;"
  ]
  node [
    id 187
    label "wojownik"
  ]
  node [
    id 188
    label "osada"
  ]
  node [
    id 189
    label "walcz&#261;cy"
  ]
  node [
    id 190
    label "cz&#322;owiek"
  ]
  node [
    id 191
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 192
    label "harcap"
  ]
  node [
    id 193
    label "wojsko"
  ]
  node [
    id 194
    label "elew"
  ]
  node [
    id 195
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 196
    label "demobilizowanie"
  ]
  node [
    id 197
    label "Gurkha"
  ]
  node [
    id 198
    label "zdemobilizowanie"
  ]
  node [
    id 199
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 200
    label "so&#322;dat"
  ]
  node [
    id 201
    label "demobilizowa&#263;"
  ]
  node [
    id 202
    label "mundurowy"
  ]
  node [
    id 203
    label "rota"
  ]
  node [
    id 204
    label "zdemobilizowa&#263;"
  ]
  node [
    id 205
    label "&#380;o&#322;dowy"
  ]
  node [
    id 206
    label "motyl_dzienny"
  ]
  node [
    id 207
    label "paziowate"
  ]
  node [
    id 208
    label "wojew&#243;dztwo"
  ]
  node [
    id 209
    label "jednostka_administracyjna"
  ]
  node [
    id 210
    label "gmina"
  ]
  node [
    id 211
    label "Biskupice"
  ]
  node [
    id 212
    label "radny"
  ]
  node [
    id 213
    label "urz&#261;d"
  ]
  node [
    id 214
    label "rada_gminy"
  ]
  node [
    id 215
    label "Dobro&#324;"
  ]
  node [
    id 216
    label "organizacja_religijna"
  ]
  node [
    id 217
    label "Karlsbad"
  ]
  node [
    id 218
    label "Wielka_Wie&#347;"
  ]
  node [
    id 219
    label "mikroregion"
  ]
  node [
    id 220
    label "makroregion"
  ]
  node [
    id 221
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 222
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 223
    label "pa&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
]
