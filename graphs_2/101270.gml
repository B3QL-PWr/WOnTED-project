graph [
  node [
    id 0
    label "&#322;&#281;gajny"
    origin "text"
  ]
  node [
    id 1
    label "warmi&#324;sko"
  ]
  node [
    id 2
    label "mazurski"
  ]
  node [
    id 3
    label "Olsztyn"
  ]
  node [
    id 4
    label "&#8211;"
  ]
  node [
    id 5
    label "Korsze"
  ]
  node [
    id 6
    label "jezioro"
  ]
  node [
    id 7
    label "Linowskie"
  ]
  node [
    id 8
    label "drogi"
  ]
  node [
    id 9
    label "krajowy"
  ]
  node [
    id 10
    label "nr"
  ]
  node [
    id 11
    label "16"
  ]
  node [
    id 12
    label "kapitu&#322;a"
  ]
  node [
    id 13
    label "warmi&#324;ski"
  ]
  node [
    id 14
    label "von"
  ]
  node [
    id 15
    label "Sa&#223;"
  ]
  node [
    id 16
    label "marga&#263;"
  ]
  node [
    id 17
    label "Teubern"
  ]
  node [
    id 18
    label "gospodarstwo"
  ]
  node [
    id 19
    label "ogrodniczy"
  ]
  node [
    id 20
    label "grupa"
  ]
  node [
    id 21
    label "lotos"
  ]
  node [
    id 22
    label "Franciszka"
  ]
  node [
    id 23
    label "Gollan"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
]
