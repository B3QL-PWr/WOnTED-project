graph [
  node [
    id 0
    label "kra&#347;ko"
    origin "text"
  ]
  node [
    id 1
    label "reszta"
    origin "text"
  ]
  node [
    id 2
    label "ekipa"
    origin "text"
  ]
  node [
    id 3
    label "musza"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "bardziej"
    origin "text"
  ]
  node [
    id 6
    label "postara&#263;"
    origin "text"
  ]
  node [
    id 7
    label "komentarz"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "sam"
    origin "text"
  ]
  node [
    id 10
    label "siebie"
    origin "text"
  ]
  node [
    id 11
    label "kwota"
  ]
  node [
    id 12
    label "wydanie"
  ]
  node [
    id 13
    label "remainder"
  ]
  node [
    id 14
    label "pozosta&#322;y"
  ]
  node [
    id 15
    label "wydawa&#263;"
  ]
  node [
    id 16
    label "wyda&#263;"
  ]
  node [
    id 17
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 18
    label "Rzym_Zachodni"
  ]
  node [
    id 19
    label "whole"
  ]
  node [
    id 20
    label "ilo&#347;&#263;"
  ]
  node [
    id 21
    label "element"
  ]
  node [
    id 22
    label "Rzym_Wschodni"
  ]
  node [
    id 23
    label "urz&#261;dzenie"
  ]
  node [
    id 24
    label "wynie&#347;&#263;"
  ]
  node [
    id 25
    label "pieni&#261;dze"
  ]
  node [
    id 26
    label "limit"
  ]
  node [
    id 27
    label "wynosi&#263;"
  ]
  node [
    id 28
    label "inny"
  ]
  node [
    id 29
    label "&#380;ywy"
  ]
  node [
    id 30
    label "robi&#263;"
  ]
  node [
    id 31
    label "mie&#263;_miejsce"
  ]
  node [
    id 32
    label "plon"
  ]
  node [
    id 33
    label "give"
  ]
  node [
    id 34
    label "surrender"
  ]
  node [
    id 35
    label "kojarzy&#263;"
  ]
  node [
    id 36
    label "d&#378;wi&#281;k"
  ]
  node [
    id 37
    label "impart"
  ]
  node [
    id 38
    label "dawa&#263;"
  ]
  node [
    id 39
    label "zapach"
  ]
  node [
    id 40
    label "wydawnictwo"
  ]
  node [
    id 41
    label "wiano"
  ]
  node [
    id 42
    label "produkcja"
  ]
  node [
    id 43
    label "wprowadza&#263;"
  ]
  node [
    id 44
    label "podawa&#263;"
  ]
  node [
    id 45
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 46
    label "ujawnia&#263;"
  ]
  node [
    id 47
    label "placard"
  ]
  node [
    id 48
    label "powierza&#263;"
  ]
  node [
    id 49
    label "denuncjowa&#263;"
  ]
  node [
    id 50
    label "tajemnica"
  ]
  node [
    id 51
    label "panna_na_wydaniu"
  ]
  node [
    id 52
    label "wytwarza&#263;"
  ]
  node [
    id 53
    label "train"
  ]
  node [
    id 54
    label "powierzy&#263;"
  ]
  node [
    id 55
    label "skojarzy&#263;"
  ]
  node [
    id 56
    label "zadenuncjowa&#263;"
  ]
  node [
    id 57
    label "da&#263;"
  ]
  node [
    id 58
    label "zrobi&#263;"
  ]
  node [
    id 59
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 60
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 61
    label "translate"
  ]
  node [
    id 62
    label "picture"
  ]
  node [
    id 63
    label "poda&#263;"
  ]
  node [
    id 64
    label "wprowadzi&#263;"
  ]
  node [
    id 65
    label "wytworzy&#263;"
  ]
  node [
    id 66
    label "dress"
  ]
  node [
    id 67
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 68
    label "supply"
  ]
  node [
    id 69
    label "ujawni&#263;"
  ]
  node [
    id 70
    label "delivery"
  ]
  node [
    id 71
    label "zdarzenie_si&#281;"
  ]
  node [
    id 72
    label "rendition"
  ]
  node [
    id 73
    label "egzemplarz"
  ]
  node [
    id 74
    label "impression"
  ]
  node [
    id 75
    label "publikacja"
  ]
  node [
    id 76
    label "zadenuncjowanie"
  ]
  node [
    id 77
    label "wytworzenie"
  ]
  node [
    id 78
    label "issue"
  ]
  node [
    id 79
    label "danie"
  ]
  node [
    id 80
    label "czasopismo"
  ]
  node [
    id 81
    label "podanie"
  ]
  node [
    id 82
    label "wprowadzenie"
  ]
  node [
    id 83
    label "odmiana"
  ]
  node [
    id 84
    label "ujawnienie"
  ]
  node [
    id 85
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 86
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 87
    label "zrobienie"
  ]
  node [
    id 88
    label "zesp&#243;&#322;"
  ]
  node [
    id 89
    label "grupa"
  ]
  node [
    id 90
    label "dublet"
  ]
  node [
    id 91
    label "force"
  ]
  node [
    id 92
    label "odm&#322;adzanie"
  ]
  node [
    id 93
    label "liga"
  ]
  node [
    id 94
    label "jednostka_systematyczna"
  ]
  node [
    id 95
    label "asymilowanie"
  ]
  node [
    id 96
    label "gromada"
  ]
  node [
    id 97
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 98
    label "asymilowa&#263;"
  ]
  node [
    id 99
    label "Entuzjastki"
  ]
  node [
    id 100
    label "zbi&#243;r"
  ]
  node [
    id 101
    label "kompozycja"
  ]
  node [
    id 102
    label "Terranie"
  ]
  node [
    id 103
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 104
    label "category"
  ]
  node [
    id 105
    label "pakiet_klimatyczny"
  ]
  node [
    id 106
    label "oddzia&#322;"
  ]
  node [
    id 107
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 108
    label "cz&#261;steczka"
  ]
  node [
    id 109
    label "stage_set"
  ]
  node [
    id 110
    label "type"
  ]
  node [
    id 111
    label "specgrupa"
  ]
  node [
    id 112
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 113
    label "&#346;wietliki"
  ]
  node [
    id 114
    label "odm&#322;odzenie"
  ]
  node [
    id 115
    label "Eurogrupa"
  ]
  node [
    id 116
    label "odm&#322;adza&#263;"
  ]
  node [
    id 117
    label "formacja_geologiczna"
  ]
  node [
    id 118
    label "harcerze_starsi"
  ]
  node [
    id 119
    label "Mazowsze"
  ]
  node [
    id 120
    label "skupienie"
  ]
  node [
    id 121
    label "The_Beatles"
  ]
  node [
    id 122
    label "zabudowania"
  ]
  node [
    id 123
    label "group"
  ]
  node [
    id 124
    label "zespolik"
  ]
  node [
    id 125
    label "schorzenie"
  ]
  node [
    id 126
    label "ro&#347;lina"
  ]
  node [
    id 127
    label "Depeche_Mode"
  ]
  node [
    id 128
    label "batch"
  ]
  node [
    id 129
    label "dru&#380;yna"
  ]
  node [
    id 130
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 131
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 132
    label "kaftan"
  ]
  node [
    id 133
    label "zwyci&#281;stwo"
  ]
  node [
    id 134
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 135
    label "comment"
  ]
  node [
    id 136
    label "ocena"
  ]
  node [
    id 137
    label "interpretacja"
  ]
  node [
    id 138
    label "tekst"
  ]
  node [
    id 139
    label "artyku&#322;"
  ]
  node [
    id 140
    label "audycja"
  ]
  node [
    id 141
    label "gossip"
  ]
  node [
    id 142
    label "ekscerpcja"
  ]
  node [
    id 143
    label "j&#281;zykowo"
  ]
  node [
    id 144
    label "wypowied&#378;"
  ]
  node [
    id 145
    label "redakcja"
  ]
  node [
    id 146
    label "wytw&#243;r"
  ]
  node [
    id 147
    label "pomini&#281;cie"
  ]
  node [
    id 148
    label "dzie&#322;o"
  ]
  node [
    id 149
    label "preparacja"
  ]
  node [
    id 150
    label "odmianka"
  ]
  node [
    id 151
    label "opu&#347;ci&#263;"
  ]
  node [
    id 152
    label "koniektura"
  ]
  node [
    id 153
    label "pisa&#263;"
  ]
  node [
    id 154
    label "obelga"
  ]
  node [
    id 155
    label "pogl&#261;d"
  ]
  node [
    id 156
    label "decyzja"
  ]
  node [
    id 157
    label "sofcik"
  ]
  node [
    id 158
    label "kryterium"
  ]
  node [
    id 159
    label "informacja"
  ]
  node [
    id 160
    label "appraisal"
  ]
  node [
    id 161
    label "explanation"
  ]
  node [
    id 162
    label "hermeneutyka"
  ]
  node [
    id 163
    label "spos&#243;b"
  ]
  node [
    id 164
    label "wypracowanie"
  ]
  node [
    id 165
    label "kontekst"
  ]
  node [
    id 166
    label "realizacja"
  ]
  node [
    id 167
    label "interpretation"
  ]
  node [
    id 168
    label "obja&#347;nienie"
  ]
  node [
    id 169
    label "program"
  ]
  node [
    id 170
    label "blok"
  ]
  node [
    id 171
    label "prawda"
  ]
  node [
    id 172
    label "znak_j&#281;zykowy"
  ]
  node [
    id 173
    label "nag&#322;&#243;wek"
  ]
  node [
    id 174
    label "szkic"
  ]
  node [
    id 175
    label "line"
  ]
  node [
    id 176
    label "fragment"
  ]
  node [
    id 177
    label "wyr&#243;b"
  ]
  node [
    id 178
    label "rodzajnik"
  ]
  node [
    id 179
    label "dokument"
  ]
  node [
    id 180
    label "towar"
  ]
  node [
    id 181
    label "paragraf"
  ]
  node [
    id 182
    label "gaworzy&#263;"
  ]
  node [
    id 183
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 184
    label "remark"
  ]
  node [
    id 185
    label "rozmawia&#263;"
  ]
  node [
    id 186
    label "wyra&#380;a&#263;"
  ]
  node [
    id 187
    label "umie&#263;"
  ]
  node [
    id 188
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 189
    label "dziama&#263;"
  ]
  node [
    id 190
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 191
    label "formu&#322;owa&#263;"
  ]
  node [
    id 192
    label "dysfonia"
  ]
  node [
    id 193
    label "express"
  ]
  node [
    id 194
    label "talk"
  ]
  node [
    id 195
    label "u&#380;ywa&#263;"
  ]
  node [
    id 196
    label "prawi&#263;"
  ]
  node [
    id 197
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 198
    label "powiada&#263;"
  ]
  node [
    id 199
    label "tell"
  ]
  node [
    id 200
    label "chew_the_fat"
  ]
  node [
    id 201
    label "say"
  ]
  node [
    id 202
    label "j&#281;zyk"
  ]
  node [
    id 203
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 204
    label "informowa&#263;"
  ]
  node [
    id 205
    label "wydobywa&#263;"
  ]
  node [
    id 206
    label "okre&#347;la&#263;"
  ]
  node [
    id 207
    label "korzysta&#263;"
  ]
  node [
    id 208
    label "distribute"
  ]
  node [
    id 209
    label "bash"
  ]
  node [
    id 210
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 211
    label "doznawa&#263;"
  ]
  node [
    id 212
    label "decydowa&#263;"
  ]
  node [
    id 213
    label "signify"
  ]
  node [
    id 214
    label "style"
  ]
  node [
    id 215
    label "powodowa&#263;"
  ]
  node [
    id 216
    label "komunikowa&#263;"
  ]
  node [
    id 217
    label "inform"
  ]
  node [
    id 218
    label "znaczy&#263;"
  ]
  node [
    id 219
    label "give_voice"
  ]
  node [
    id 220
    label "oznacza&#263;"
  ]
  node [
    id 221
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 222
    label "represent"
  ]
  node [
    id 223
    label "convey"
  ]
  node [
    id 224
    label "arouse"
  ]
  node [
    id 225
    label "determine"
  ]
  node [
    id 226
    label "work"
  ]
  node [
    id 227
    label "reakcja_chemiczna"
  ]
  node [
    id 228
    label "uwydatnia&#263;"
  ]
  node [
    id 229
    label "eksploatowa&#263;"
  ]
  node [
    id 230
    label "uzyskiwa&#263;"
  ]
  node [
    id 231
    label "wydostawa&#263;"
  ]
  node [
    id 232
    label "wyjmowa&#263;"
  ]
  node [
    id 233
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 234
    label "dobywa&#263;"
  ]
  node [
    id 235
    label "ocala&#263;"
  ]
  node [
    id 236
    label "excavate"
  ]
  node [
    id 237
    label "g&#243;rnictwo"
  ]
  node [
    id 238
    label "raise"
  ]
  node [
    id 239
    label "wiedzie&#263;"
  ]
  node [
    id 240
    label "can"
  ]
  node [
    id 241
    label "m&#243;c"
  ]
  node [
    id 242
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 243
    label "rozumie&#263;"
  ]
  node [
    id 244
    label "szczeka&#263;"
  ]
  node [
    id 245
    label "funkcjonowa&#263;"
  ]
  node [
    id 246
    label "mawia&#263;"
  ]
  node [
    id 247
    label "opowiada&#263;"
  ]
  node [
    id 248
    label "chatter"
  ]
  node [
    id 249
    label "niemowl&#281;"
  ]
  node [
    id 250
    label "kosmetyk"
  ]
  node [
    id 251
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 252
    label "stanowisko_archeologiczne"
  ]
  node [
    id 253
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 254
    label "artykulator"
  ]
  node [
    id 255
    label "kod"
  ]
  node [
    id 256
    label "kawa&#322;ek"
  ]
  node [
    id 257
    label "przedmiot"
  ]
  node [
    id 258
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 259
    label "gramatyka"
  ]
  node [
    id 260
    label "stylik"
  ]
  node [
    id 261
    label "przet&#322;umaczenie"
  ]
  node [
    id 262
    label "formalizowanie"
  ]
  node [
    id 263
    label "ssa&#263;"
  ]
  node [
    id 264
    label "ssanie"
  ]
  node [
    id 265
    label "language"
  ]
  node [
    id 266
    label "liza&#263;"
  ]
  node [
    id 267
    label "napisa&#263;"
  ]
  node [
    id 268
    label "konsonantyzm"
  ]
  node [
    id 269
    label "wokalizm"
  ]
  node [
    id 270
    label "fonetyka"
  ]
  node [
    id 271
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 272
    label "jeniec"
  ]
  node [
    id 273
    label "but"
  ]
  node [
    id 274
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 275
    label "po_koroniarsku"
  ]
  node [
    id 276
    label "kultura_duchowa"
  ]
  node [
    id 277
    label "t&#322;umaczenie"
  ]
  node [
    id 278
    label "m&#243;wienie"
  ]
  node [
    id 279
    label "pype&#263;"
  ]
  node [
    id 280
    label "lizanie"
  ]
  node [
    id 281
    label "pismo"
  ]
  node [
    id 282
    label "formalizowa&#263;"
  ]
  node [
    id 283
    label "organ"
  ]
  node [
    id 284
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 285
    label "rozumienie"
  ]
  node [
    id 286
    label "makroglosja"
  ]
  node [
    id 287
    label "jama_ustna"
  ]
  node [
    id 288
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 289
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 290
    label "natural_language"
  ]
  node [
    id 291
    label "s&#322;ownictwo"
  ]
  node [
    id 292
    label "dysphonia"
  ]
  node [
    id 293
    label "dysleksja"
  ]
  node [
    id 294
    label "sklep"
  ]
  node [
    id 295
    label "p&#243;&#322;ka"
  ]
  node [
    id 296
    label "firma"
  ]
  node [
    id 297
    label "stoisko"
  ]
  node [
    id 298
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 299
    label "sk&#322;ad"
  ]
  node [
    id 300
    label "obiekt_handlowy"
  ]
  node [
    id 301
    label "zaplecze"
  ]
  node [
    id 302
    label "witryna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
]
