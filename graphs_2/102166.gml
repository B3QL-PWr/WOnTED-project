graph [
  node [
    id 0
    label "centrum"
    origin "text"
  ]
  node [
    id 1
    label "zarz&#261;dzanie"
    origin "text"
  ]
  node [
    id 2
    label "kryzysowy"
    origin "text"
  ]
  node [
    id 3
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 4
    label "miejski"
    origin "text"
  ]
  node [
    id 5
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 6
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "punkt"
  ]
  node [
    id 8
    label "blok"
  ]
  node [
    id 9
    label "sejm"
  ]
  node [
    id 10
    label "centroprawica"
  ]
  node [
    id 11
    label "core"
  ]
  node [
    id 12
    label "o&#347;rodek"
  ]
  node [
    id 13
    label "Hollywood"
  ]
  node [
    id 14
    label "miejsce"
  ]
  node [
    id 15
    label "centrolew"
  ]
  node [
    id 16
    label "&#347;rodek"
  ]
  node [
    id 17
    label "warunki"
  ]
  node [
    id 18
    label "zal&#261;&#380;ek"
  ]
  node [
    id 19
    label "skupisko"
  ]
  node [
    id 20
    label "center"
  ]
  node [
    id 21
    label "instytucja"
  ]
  node [
    id 22
    label "otoczenie"
  ]
  node [
    id 23
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 24
    label "dzia&#322;"
  ]
  node [
    id 25
    label "skorupa_ziemska"
  ]
  node [
    id 26
    label "budynek"
  ]
  node [
    id 27
    label "przeszkoda"
  ]
  node [
    id 28
    label "bry&#322;a"
  ]
  node [
    id 29
    label "j&#261;kanie"
  ]
  node [
    id 30
    label "program"
  ]
  node [
    id 31
    label "square"
  ]
  node [
    id 32
    label "bloking"
  ]
  node [
    id 33
    label "kontynent"
  ]
  node [
    id 34
    label "ok&#322;adka"
  ]
  node [
    id 35
    label "zbi&#243;r"
  ]
  node [
    id 36
    label "kr&#261;g"
  ]
  node [
    id 37
    label "start"
  ]
  node [
    id 38
    label "blockage"
  ]
  node [
    id 39
    label "blokowisko"
  ]
  node [
    id 40
    label "artyku&#322;"
  ]
  node [
    id 41
    label "blokada"
  ]
  node [
    id 42
    label "whole"
  ]
  node [
    id 43
    label "stok_kontynentalny"
  ]
  node [
    id 44
    label "bajt"
  ]
  node [
    id 45
    label "barak"
  ]
  node [
    id 46
    label "zamek"
  ]
  node [
    id 47
    label "referat"
  ]
  node [
    id 48
    label "nastawnia"
  ]
  node [
    id 49
    label "obrona"
  ]
  node [
    id 50
    label "organizacja"
  ]
  node [
    id 51
    label "grupa"
  ]
  node [
    id 52
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 53
    label "dom_wielorodzinny"
  ]
  node [
    id 54
    label "zeszyt"
  ]
  node [
    id 55
    label "ram&#243;wka"
  ]
  node [
    id 56
    label "siatk&#243;wka"
  ]
  node [
    id 57
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 58
    label "block"
  ]
  node [
    id 59
    label "bie&#380;nia"
  ]
  node [
    id 60
    label "zesp&#243;&#322;"
  ]
  node [
    id 61
    label "obiekt_matematyczny"
  ]
  node [
    id 62
    label "stopie&#324;_pisma"
  ]
  node [
    id 63
    label "pozycja"
  ]
  node [
    id 64
    label "problemat"
  ]
  node [
    id 65
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 66
    label "obiekt"
  ]
  node [
    id 67
    label "point"
  ]
  node [
    id 68
    label "plamka"
  ]
  node [
    id 69
    label "przestrze&#324;"
  ]
  node [
    id 70
    label "mark"
  ]
  node [
    id 71
    label "ust&#281;p"
  ]
  node [
    id 72
    label "po&#322;o&#380;enie"
  ]
  node [
    id 73
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 74
    label "kres"
  ]
  node [
    id 75
    label "plan"
  ]
  node [
    id 76
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 77
    label "chwila"
  ]
  node [
    id 78
    label "podpunkt"
  ]
  node [
    id 79
    label "jednostka"
  ]
  node [
    id 80
    label "sprawa"
  ]
  node [
    id 81
    label "problematyka"
  ]
  node [
    id 82
    label "prosta"
  ]
  node [
    id 83
    label "wojsko"
  ]
  node [
    id 84
    label "zapunktowa&#263;"
  ]
  node [
    id 85
    label "rz&#261;d"
  ]
  node [
    id 86
    label "uwaga"
  ]
  node [
    id 87
    label "cecha"
  ]
  node [
    id 88
    label "praca"
  ]
  node [
    id 89
    label "plac"
  ]
  node [
    id 90
    label "location"
  ]
  node [
    id 91
    label "warunek_lokalowy"
  ]
  node [
    id 92
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 93
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 94
    label "cia&#322;o"
  ]
  node [
    id 95
    label "status"
  ]
  node [
    id 96
    label "koalicja"
  ]
  node [
    id 97
    label "parlament"
  ]
  node [
    id 98
    label "siedziba"
  ]
  node [
    id 99
    label "lewica"
  ]
  node [
    id 100
    label "prawica"
  ]
  node [
    id 101
    label "izba_ni&#380;sza"
  ]
  node [
    id 102
    label "zgromadzenie"
  ]
  node [
    id 103
    label "obrady"
  ]
  node [
    id 104
    label "parliament"
  ]
  node [
    id 105
    label "Los_Angeles"
  ]
  node [
    id 106
    label "dysponowanie"
  ]
  node [
    id 107
    label "nauka_humanistyczna"
  ]
  node [
    id 108
    label "dominance"
  ]
  node [
    id 109
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 110
    label "trzymanie"
  ]
  node [
    id 111
    label "pozarz&#261;dzanie"
  ]
  node [
    id 112
    label "dawanie"
  ]
  node [
    id 113
    label "polecanie"
  ]
  node [
    id 114
    label "nauka_ekonomiczna"
  ]
  node [
    id 115
    label "commission"
  ]
  node [
    id 116
    label "zadawanie"
  ]
  node [
    id 117
    label "przesadzanie"
  ]
  node [
    id 118
    label "powierzanie"
  ]
  node [
    id 119
    label "reference"
  ]
  node [
    id 120
    label "ordynowanie"
  ]
  node [
    id 121
    label "doradzanie"
  ]
  node [
    id 122
    label "przygotowywanie"
  ]
  node [
    id 123
    label "skazany"
  ]
  node [
    id 124
    label "rozporz&#261;dzanie"
  ]
  node [
    id 125
    label "namaszczenie_chorych"
  ]
  node [
    id 126
    label "dysponowanie_si&#281;"
  ]
  node [
    id 127
    label "disposal"
  ]
  node [
    id 128
    label "rozdysponowywanie"
  ]
  node [
    id 129
    label "udost&#281;pnianie"
  ]
  node [
    id 130
    label "pra&#380;enie"
  ]
  node [
    id 131
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 132
    label "urz&#261;dzanie"
  ]
  node [
    id 133
    label "przeznaczanie"
  ]
  node [
    id 134
    label "p&#322;acenie"
  ]
  node [
    id 135
    label "puszczanie_si&#281;"
  ]
  node [
    id 136
    label "dodawanie"
  ]
  node [
    id 137
    label "wyst&#281;powanie"
  ]
  node [
    id 138
    label "powodowanie"
  ]
  node [
    id 139
    label "bycie_w_posiadaniu"
  ]
  node [
    id 140
    label "communication"
  ]
  node [
    id 141
    label "&#322;adowanie"
  ]
  node [
    id 142
    label "czynno&#347;&#263;"
  ]
  node [
    id 143
    label "wyrzekanie_si&#281;"
  ]
  node [
    id 144
    label "nalewanie"
  ]
  node [
    id 145
    label "zezwalanie"
  ]
  node [
    id 146
    label "giving"
  ]
  node [
    id 147
    label "przekazywanie"
  ]
  node [
    id 148
    label "obiecywanie"
  ]
  node [
    id 149
    label "odst&#281;powanie"
  ]
  node [
    id 150
    label "strike"
  ]
  node [
    id 151
    label "dostarczanie"
  ]
  node [
    id 152
    label "wymienianie_si&#281;"
  ]
  node [
    id 153
    label "rendition"
  ]
  node [
    id 154
    label "robienie"
  ]
  node [
    id 155
    label "emission"
  ]
  node [
    id 156
    label "administration"
  ]
  node [
    id 157
    label "umo&#380;liwianie"
  ]
  node [
    id 158
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 159
    label "utrzymywanie"
  ]
  node [
    id 160
    label "wypuszczanie"
  ]
  node [
    id 161
    label "noszenie"
  ]
  node [
    id 162
    label "przetrzymanie"
  ]
  node [
    id 163
    label "przetrzymywanie"
  ]
  node [
    id 164
    label "hodowanie"
  ]
  node [
    id 165
    label "wypuszczenie"
  ]
  node [
    id 166
    label "niesienie"
  ]
  node [
    id 167
    label "utrzymanie"
  ]
  node [
    id 168
    label "zmuszanie"
  ]
  node [
    id 169
    label "podtrzymywanie"
  ]
  node [
    id 170
    label "sprawowanie"
  ]
  node [
    id 171
    label "poise"
  ]
  node [
    id 172
    label "uniemo&#380;liwianie"
  ]
  node [
    id 173
    label "dzier&#380;enie"
  ]
  node [
    id 174
    label "clasp"
  ]
  node [
    id 175
    label "zachowywanie"
  ]
  node [
    id 176
    label "detention"
  ]
  node [
    id 177
    label "potrzymanie"
  ]
  node [
    id 178
    label "retention"
  ]
  node [
    id 179
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 180
    label "z&#322;y"
  ]
  node [
    id 181
    label "trudny"
  ]
  node [
    id 182
    label "niepomy&#347;lny"
  ]
  node [
    id 183
    label "niegrzeczny"
  ]
  node [
    id 184
    label "rozgniewanie"
  ]
  node [
    id 185
    label "zdenerwowany"
  ]
  node [
    id 186
    label "&#378;le"
  ]
  node [
    id 187
    label "niemoralny"
  ]
  node [
    id 188
    label "sierdzisty"
  ]
  node [
    id 189
    label "pieski"
  ]
  node [
    id 190
    label "zez&#322;oszczenie"
  ]
  node [
    id 191
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 192
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 193
    label "z&#322;oszczenie"
  ]
  node [
    id 194
    label "gniewanie"
  ]
  node [
    id 195
    label "niekorzystny"
  ]
  node [
    id 196
    label "syf"
  ]
  node [
    id 197
    label "negatywny"
  ]
  node [
    id 198
    label "ci&#281;&#380;ko"
  ]
  node [
    id 199
    label "skomplikowany"
  ]
  node [
    id 200
    label "k&#322;opotliwy"
  ]
  node [
    id 201
    label "wymagaj&#261;cy"
  ]
  node [
    id 202
    label "mianowaniec"
  ]
  node [
    id 203
    label "okienko"
  ]
  node [
    id 204
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 205
    label "position"
  ]
  node [
    id 206
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 207
    label "stanowisko"
  ]
  node [
    id 208
    label "w&#322;adza"
  ]
  node [
    id 209
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 210
    label "organ"
  ]
  node [
    id 211
    label "miejsce_pracy"
  ]
  node [
    id 212
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 213
    label "&#321;ubianka"
  ]
  node [
    id 214
    label "Bia&#322;y_Dom"
  ]
  node [
    id 215
    label "dzia&#322;_personalny"
  ]
  node [
    id 216
    label "Kreml"
  ]
  node [
    id 217
    label "sadowisko"
  ]
  node [
    id 218
    label "cz&#322;owiek"
  ]
  node [
    id 219
    label "struktura"
  ]
  node [
    id 220
    label "panowanie"
  ]
  node [
    id 221
    label "wydolno&#347;&#263;"
  ]
  node [
    id 222
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 223
    label "prawo"
  ]
  node [
    id 224
    label "rz&#261;dzenie"
  ]
  node [
    id 225
    label "postawi&#263;"
  ]
  node [
    id 226
    label "awansowa&#263;"
  ]
  node [
    id 227
    label "wakowa&#263;"
  ]
  node [
    id 228
    label "uprawianie"
  ]
  node [
    id 229
    label "pogl&#261;d"
  ]
  node [
    id 230
    label "awansowanie"
  ]
  node [
    id 231
    label "stawia&#263;"
  ]
  node [
    id 232
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 233
    label "uk&#322;ad"
  ]
  node [
    id 234
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 235
    label "Komitet_Region&#243;w"
  ]
  node [
    id 236
    label "struktura_anatomiczna"
  ]
  node [
    id 237
    label "organogeneza"
  ]
  node [
    id 238
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 239
    label "tw&#243;r"
  ]
  node [
    id 240
    label "tkanka"
  ]
  node [
    id 241
    label "stomia"
  ]
  node [
    id 242
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 243
    label "budowa"
  ]
  node [
    id 244
    label "dekortykacja"
  ]
  node [
    id 245
    label "okolica"
  ]
  node [
    id 246
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 247
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 248
    label "Izba_Konsyliarska"
  ]
  node [
    id 249
    label "jednostka_organizacyjna"
  ]
  node [
    id 250
    label "ekran"
  ]
  node [
    id 251
    label "wype&#322;nianie"
  ]
  node [
    id 252
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 253
    label "poczta"
  ]
  node [
    id 254
    label "czas_wolny"
  ]
  node [
    id 255
    label "pulpit"
  ]
  node [
    id 256
    label "tabela"
  ]
  node [
    id 257
    label "interfejs"
  ]
  node [
    id 258
    label "otw&#243;r"
  ]
  node [
    id 259
    label "rubryka"
  ]
  node [
    id 260
    label "menad&#380;er_okien"
  ]
  node [
    id 261
    label "okno"
  ]
  node [
    id 262
    label "wype&#322;nienie"
  ]
  node [
    id 263
    label "distribution"
  ]
  node [
    id 264
    label "zakres"
  ]
  node [
    id 265
    label "poddzia&#322;"
  ]
  node [
    id 266
    label "bezdro&#380;e"
  ]
  node [
    id 267
    label "insourcing"
  ]
  node [
    id 268
    label "stopie&#324;"
  ]
  node [
    id 269
    label "wytw&#243;r"
  ]
  node [
    id 270
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 271
    label "competence"
  ]
  node [
    id 272
    label "sfera"
  ]
  node [
    id 273
    label "column"
  ]
  node [
    id 274
    label "tytu&#322;"
  ]
  node [
    id 275
    label "mandatariusz"
  ]
  node [
    id 276
    label "poj&#281;cie"
  ]
  node [
    id 277
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 278
    label "afiliowa&#263;"
  ]
  node [
    id 279
    label "establishment"
  ]
  node [
    id 280
    label "zamyka&#263;"
  ]
  node [
    id 281
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 282
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 283
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 284
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 285
    label "standard"
  ]
  node [
    id 286
    label "Fundusze_Unijne"
  ]
  node [
    id 287
    label "biuro"
  ]
  node [
    id 288
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 289
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 290
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 291
    label "zamykanie"
  ]
  node [
    id 292
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 293
    label "osoba_prawna"
  ]
  node [
    id 294
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 295
    label "typowy"
  ]
  node [
    id 296
    label "publiczny"
  ]
  node [
    id 297
    label "miejsko"
  ]
  node [
    id 298
    label "miastowy"
  ]
  node [
    id 299
    label "upublicznienie"
  ]
  node [
    id 300
    label "publicznie"
  ]
  node [
    id 301
    label "upublicznianie"
  ]
  node [
    id 302
    label "jawny"
  ]
  node [
    id 303
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 304
    label "typowo"
  ]
  node [
    id 305
    label "zwyk&#322;y"
  ]
  node [
    id 306
    label "zwyczajny"
  ]
  node [
    id 307
    label "cz&#281;sty"
  ]
  node [
    id 308
    label "nowoczesny"
  ]
  node [
    id 309
    label "obywatel"
  ]
  node [
    id 310
    label "mieszczanin"
  ]
  node [
    id 311
    label "mieszcza&#324;stwo"
  ]
  node [
    id 312
    label "charakterystycznie"
  ]
  node [
    id 313
    label "powiada&#263;"
  ]
  node [
    id 314
    label "komunikowa&#263;"
  ]
  node [
    id 315
    label "inform"
  ]
  node [
    id 316
    label "communicate"
  ]
  node [
    id 317
    label "powodowa&#263;"
  ]
  node [
    id 318
    label "mawia&#263;"
  ]
  node [
    id 319
    label "m&#243;wi&#263;"
  ]
  node [
    id 320
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 321
    label "Wroc&#322;aw"
  ]
  node [
    id 322
    label "prognoza"
  ]
  node [
    id 323
    label "meteorologiczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 287
    target 322
  ]
  edge [
    source 287
    target 323
  ]
  edge [
    source 322
    target 323
  ]
]
