graph [
  node [
    id 0
    label "dawny"
    origin "text"
  ]
  node [
    id 1
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niedu&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "amatorski"
    origin "text"
  ]
  node [
    id 6
    label "filmik"
    origin "text"
  ]
  node [
    id 7
    label "wideo"
    origin "text"
  ]
  node [
    id 8
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 9
    label "ciekawy"
    origin "text"
  ]
  node [
    id 10
    label "problem"
    origin "text"
  ]
  node [
    id 11
    label "matematyczny"
    origin "text"
  ]
  node [
    id 12
    label "wszystek"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "dost&#281;pny"
    origin "text"
  ]
  node [
    id 15
    label "najpierw"
    origin "text"
  ]
  node [
    id 16
    label "witryna"
    origin "text"
  ]
  node [
    id 17
    label "opinia"
    origin "text"
  ]
  node [
    id 18
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 19
    label "tutaj"
    origin "text"
  ]
  node [
    id 20
    label "oto"
    origin "text"
  ]
  node [
    id 21
    label "pierwszy"
    origin "text"
  ]
  node [
    id 22
    label "przeprasza&#263;"
    origin "text"
  ]
  node [
    id 23
    label "jako&#347;&#263;"
    origin "text"
  ]
  node [
    id 24
    label "w&#322;a&#347;ciwie"
    origin "text"
  ]
  node [
    id 25
    label "kiepsko&#347;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 27
    label "przestarza&#322;y"
  ]
  node [
    id 28
    label "odleg&#322;y"
  ]
  node [
    id 29
    label "przesz&#322;y"
  ]
  node [
    id 30
    label "od_dawna"
  ]
  node [
    id 31
    label "poprzedni"
  ]
  node [
    id 32
    label "dawno"
  ]
  node [
    id 33
    label "d&#322;ugoletni"
  ]
  node [
    id 34
    label "anachroniczny"
  ]
  node [
    id 35
    label "dawniej"
  ]
  node [
    id 36
    label "niegdysiejszy"
  ]
  node [
    id 37
    label "wcze&#347;niejszy"
  ]
  node [
    id 38
    label "kombatant"
  ]
  node [
    id 39
    label "stary"
  ]
  node [
    id 40
    label "poprzednio"
  ]
  node [
    id 41
    label "zestarzenie_si&#281;"
  ]
  node [
    id 42
    label "starzenie_si&#281;"
  ]
  node [
    id 43
    label "archaicznie"
  ]
  node [
    id 44
    label "zgrzybienie"
  ]
  node [
    id 45
    label "niedzisiejszy"
  ]
  node [
    id 46
    label "staro&#347;wiecki"
  ]
  node [
    id 47
    label "przestarzale"
  ]
  node [
    id 48
    label "anachronicznie"
  ]
  node [
    id 49
    label "niezgodny"
  ]
  node [
    id 50
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 51
    label "ongi&#347;"
  ]
  node [
    id 52
    label "odlegle"
  ]
  node [
    id 53
    label "delikatny"
  ]
  node [
    id 54
    label "r&#243;&#380;ny"
  ]
  node [
    id 55
    label "daleko"
  ]
  node [
    id 56
    label "s&#322;aby"
  ]
  node [
    id 57
    label "daleki"
  ]
  node [
    id 58
    label "oddalony"
  ]
  node [
    id 59
    label "obcy"
  ]
  node [
    id 60
    label "nieobecny"
  ]
  node [
    id 61
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 62
    label "ojciec"
  ]
  node [
    id 63
    label "nienowoczesny"
  ]
  node [
    id 64
    label "gruba_ryba"
  ]
  node [
    id 65
    label "staro"
  ]
  node [
    id 66
    label "m&#261;&#380;"
  ]
  node [
    id 67
    label "starzy"
  ]
  node [
    id 68
    label "dotychczasowy"
  ]
  node [
    id 69
    label "p&#243;&#378;ny"
  ]
  node [
    id 70
    label "charakterystyczny"
  ]
  node [
    id 71
    label "brat"
  ]
  node [
    id 72
    label "po_staro&#347;wiecku"
  ]
  node [
    id 73
    label "zwierzchnik"
  ]
  node [
    id 74
    label "znajomy"
  ]
  node [
    id 75
    label "starczo"
  ]
  node [
    id 76
    label "dojrza&#322;y"
  ]
  node [
    id 77
    label "wcze&#347;niej"
  ]
  node [
    id 78
    label "miniony"
  ]
  node [
    id 79
    label "ostatni"
  ]
  node [
    id 80
    label "d&#322;ugi"
  ]
  node [
    id 81
    label "wieloletni"
  ]
  node [
    id 82
    label "kiedy&#347;"
  ]
  node [
    id 83
    label "d&#322;ugotrwale"
  ]
  node [
    id 84
    label "dawnie"
  ]
  node [
    id 85
    label "wyjadacz"
  ]
  node [
    id 86
    label "weteran"
  ]
  node [
    id 87
    label "mie&#263;_miejsce"
  ]
  node [
    id 88
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 89
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 90
    label "p&#322;ywa&#263;"
  ]
  node [
    id 91
    label "run"
  ]
  node [
    id 92
    label "bangla&#263;"
  ]
  node [
    id 93
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 94
    label "przebiega&#263;"
  ]
  node [
    id 95
    label "wk&#322;ada&#263;"
  ]
  node [
    id 96
    label "proceed"
  ]
  node [
    id 97
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 98
    label "carry"
  ]
  node [
    id 99
    label "bywa&#263;"
  ]
  node [
    id 100
    label "dziama&#263;"
  ]
  node [
    id 101
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 102
    label "stara&#263;_si&#281;"
  ]
  node [
    id 103
    label "para"
  ]
  node [
    id 104
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 105
    label "str&#243;j"
  ]
  node [
    id 106
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 107
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 108
    label "krok"
  ]
  node [
    id 109
    label "tryb"
  ]
  node [
    id 110
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 111
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 112
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 113
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 114
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 115
    label "continue"
  ]
  node [
    id 116
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 117
    label "przedmiot"
  ]
  node [
    id 118
    label "kontrolowa&#263;"
  ]
  node [
    id 119
    label "sok"
  ]
  node [
    id 120
    label "krew"
  ]
  node [
    id 121
    label "wheel"
  ]
  node [
    id 122
    label "draw"
  ]
  node [
    id 123
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 124
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 125
    label "biec"
  ]
  node [
    id 126
    label "przebywa&#263;"
  ]
  node [
    id 127
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 128
    label "equal"
  ]
  node [
    id 129
    label "trwa&#263;"
  ]
  node [
    id 130
    label "si&#281;ga&#263;"
  ]
  node [
    id 131
    label "stan"
  ]
  node [
    id 132
    label "obecno&#347;&#263;"
  ]
  node [
    id 133
    label "stand"
  ]
  node [
    id 134
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 135
    label "uczestniczy&#263;"
  ]
  node [
    id 136
    label "inflict"
  ]
  node [
    id 137
    label "&#380;egna&#263;"
  ]
  node [
    id 138
    label "pozosta&#263;"
  ]
  node [
    id 139
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 140
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 141
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 142
    label "sterowa&#263;"
  ]
  node [
    id 143
    label "ciecz"
  ]
  node [
    id 144
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 145
    label "mie&#263;"
  ]
  node [
    id 146
    label "m&#243;wi&#263;"
  ]
  node [
    id 147
    label "lata&#263;"
  ]
  node [
    id 148
    label "statek"
  ]
  node [
    id 149
    label "swimming"
  ]
  node [
    id 150
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 151
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 152
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 153
    label "pracowa&#263;"
  ]
  node [
    id 154
    label "sink"
  ]
  node [
    id 155
    label "zanika&#263;"
  ]
  node [
    id 156
    label "falowa&#263;"
  ]
  node [
    id 157
    label "pair"
  ]
  node [
    id 158
    label "zesp&#243;&#322;"
  ]
  node [
    id 159
    label "odparowywanie"
  ]
  node [
    id 160
    label "gaz_cieplarniany"
  ]
  node [
    id 161
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 162
    label "poker"
  ]
  node [
    id 163
    label "moneta"
  ]
  node [
    id 164
    label "parowanie"
  ]
  node [
    id 165
    label "zbi&#243;r"
  ]
  node [
    id 166
    label "damp"
  ]
  node [
    id 167
    label "nale&#380;e&#263;"
  ]
  node [
    id 168
    label "sztuka"
  ]
  node [
    id 169
    label "odparowanie"
  ]
  node [
    id 170
    label "grupa"
  ]
  node [
    id 171
    label "odparowa&#263;"
  ]
  node [
    id 172
    label "dodatek"
  ]
  node [
    id 173
    label "jednostka_monetarna"
  ]
  node [
    id 174
    label "smoke"
  ]
  node [
    id 175
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 176
    label "odparowywa&#263;"
  ]
  node [
    id 177
    label "uk&#322;ad"
  ]
  node [
    id 178
    label "Albania"
  ]
  node [
    id 179
    label "gaz"
  ]
  node [
    id 180
    label "wyparowanie"
  ]
  node [
    id 181
    label "step"
  ]
  node [
    id 182
    label "tu&#322;&#243;w"
  ]
  node [
    id 183
    label "measurement"
  ]
  node [
    id 184
    label "action"
  ]
  node [
    id 185
    label "czyn"
  ]
  node [
    id 186
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 187
    label "ruch"
  ]
  node [
    id 188
    label "passus"
  ]
  node [
    id 189
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 190
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 191
    label "skejt"
  ]
  node [
    id 192
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 193
    label "pace"
  ]
  node [
    id 194
    label "ko&#322;o"
  ]
  node [
    id 195
    label "spos&#243;b"
  ]
  node [
    id 196
    label "modalno&#347;&#263;"
  ]
  node [
    id 197
    label "z&#261;b"
  ]
  node [
    id 198
    label "cecha"
  ]
  node [
    id 199
    label "kategoria_gramatyczna"
  ]
  node [
    id 200
    label "skala"
  ]
  node [
    id 201
    label "funkcjonowa&#263;"
  ]
  node [
    id 202
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 203
    label "koniugacja"
  ]
  node [
    id 204
    label "przekazywa&#263;"
  ]
  node [
    id 205
    label "ubiera&#263;"
  ]
  node [
    id 206
    label "odziewa&#263;"
  ]
  node [
    id 207
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 208
    label "obleka&#263;"
  ]
  node [
    id 209
    label "inspirowa&#263;"
  ]
  node [
    id 210
    label "pour"
  ]
  node [
    id 211
    label "nosi&#263;"
  ]
  node [
    id 212
    label "introduce"
  ]
  node [
    id 213
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 214
    label "wzbudza&#263;"
  ]
  node [
    id 215
    label "umieszcza&#263;"
  ]
  node [
    id 216
    label "place"
  ]
  node [
    id 217
    label "wpaja&#263;"
  ]
  node [
    id 218
    label "gorset"
  ]
  node [
    id 219
    label "zrzucenie"
  ]
  node [
    id 220
    label "znoszenie"
  ]
  node [
    id 221
    label "kr&#243;j"
  ]
  node [
    id 222
    label "struktura"
  ]
  node [
    id 223
    label "ubranie_si&#281;"
  ]
  node [
    id 224
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 225
    label "znosi&#263;"
  ]
  node [
    id 226
    label "pochodzi&#263;"
  ]
  node [
    id 227
    label "zrzuci&#263;"
  ]
  node [
    id 228
    label "pasmanteria"
  ]
  node [
    id 229
    label "pochodzenie"
  ]
  node [
    id 230
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 231
    label "odzie&#380;"
  ]
  node [
    id 232
    label "wyko&#324;czenie"
  ]
  node [
    id 233
    label "zasada"
  ]
  node [
    id 234
    label "w&#322;o&#380;enie"
  ]
  node [
    id 235
    label "garderoba"
  ]
  node [
    id 236
    label "odziewek"
  ]
  node [
    id 237
    label "cover"
  ]
  node [
    id 238
    label "popyt"
  ]
  node [
    id 239
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 240
    label "rozumie&#263;"
  ]
  node [
    id 241
    label "szczeka&#263;"
  ]
  node [
    id 242
    label "rozmawia&#263;"
  ]
  node [
    id 243
    label "post&#261;pi&#263;"
  ]
  node [
    id 244
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 245
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 246
    label "odj&#261;&#263;"
  ]
  node [
    id 247
    label "zrobi&#263;"
  ]
  node [
    id 248
    label "cause"
  ]
  node [
    id 249
    label "begin"
  ]
  node [
    id 250
    label "do"
  ]
  node [
    id 251
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 252
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 253
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 254
    label "zorganizowa&#263;"
  ]
  node [
    id 255
    label "appoint"
  ]
  node [
    id 256
    label "wystylizowa&#263;"
  ]
  node [
    id 257
    label "przerobi&#263;"
  ]
  node [
    id 258
    label "nabra&#263;"
  ]
  node [
    id 259
    label "make"
  ]
  node [
    id 260
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 261
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 262
    label "wydali&#263;"
  ]
  node [
    id 263
    label "withdraw"
  ]
  node [
    id 264
    label "zabra&#263;"
  ]
  node [
    id 265
    label "oddzieli&#263;"
  ]
  node [
    id 266
    label "policzy&#263;"
  ]
  node [
    id 267
    label "reduce"
  ]
  node [
    id 268
    label "oddali&#263;"
  ]
  node [
    id 269
    label "separate"
  ]
  node [
    id 270
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 271
    label "advance"
  ]
  node [
    id 272
    label "act"
  ]
  node [
    id 273
    label "see"
  ]
  node [
    id 274
    label "his"
  ]
  node [
    id 275
    label "d&#378;wi&#281;k"
  ]
  node [
    id 276
    label "ut"
  ]
  node [
    id 277
    label "C"
  ]
  node [
    id 278
    label "organizowa&#263;"
  ]
  node [
    id 279
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 280
    label "czyni&#263;"
  ]
  node [
    id 281
    label "give"
  ]
  node [
    id 282
    label "stylizowa&#263;"
  ]
  node [
    id 283
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 284
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 285
    label "peddle"
  ]
  node [
    id 286
    label "praca"
  ]
  node [
    id 287
    label "wydala&#263;"
  ]
  node [
    id 288
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 289
    label "tentegowa&#263;"
  ]
  node [
    id 290
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 291
    label "urz&#261;dza&#263;"
  ]
  node [
    id 292
    label "oszukiwa&#263;"
  ]
  node [
    id 293
    label "work"
  ]
  node [
    id 294
    label "ukazywa&#263;"
  ]
  node [
    id 295
    label "przerabia&#263;"
  ]
  node [
    id 296
    label "post&#281;powa&#263;"
  ]
  node [
    id 297
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 298
    label "billow"
  ]
  node [
    id 299
    label "clutter"
  ]
  node [
    id 300
    label "beckon"
  ]
  node [
    id 301
    label "powiewa&#263;"
  ]
  node [
    id 302
    label "planowa&#263;"
  ]
  node [
    id 303
    label "dostosowywa&#263;"
  ]
  node [
    id 304
    label "treat"
  ]
  node [
    id 305
    label "pozyskiwa&#263;"
  ]
  node [
    id 306
    label "ensnare"
  ]
  node [
    id 307
    label "skupia&#263;"
  ]
  node [
    id 308
    label "create"
  ]
  node [
    id 309
    label "przygotowywa&#263;"
  ]
  node [
    id 310
    label "tworzy&#263;"
  ]
  node [
    id 311
    label "standard"
  ]
  node [
    id 312
    label "wprowadza&#263;"
  ]
  node [
    id 313
    label "kopiowa&#263;"
  ]
  node [
    id 314
    label "czerpa&#263;"
  ]
  node [
    id 315
    label "dally"
  ]
  node [
    id 316
    label "mock"
  ]
  node [
    id 317
    label "decydowa&#263;"
  ]
  node [
    id 318
    label "cast"
  ]
  node [
    id 319
    label "podbija&#263;"
  ]
  node [
    id 320
    label "sprawia&#263;"
  ]
  node [
    id 321
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 322
    label "przechodzi&#263;"
  ]
  node [
    id 323
    label "wytwarza&#263;"
  ]
  node [
    id 324
    label "amend"
  ]
  node [
    id 325
    label "zalicza&#263;"
  ]
  node [
    id 326
    label "overwork"
  ]
  node [
    id 327
    label "convert"
  ]
  node [
    id 328
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 329
    label "zamienia&#263;"
  ]
  node [
    id 330
    label "zmienia&#263;"
  ]
  node [
    id 331
    label "modyfikowa&#263;"
  ]
  node [
    id 332
    label "radzi&#263;_sobie"
  ]
  node [
    id 333
    label "przetwarza&#263;"
  ]
  node [
    id 334
    label "sp&#281;dza&#263;"
  ]
  node [
    id 335
    label "stylize"
  ]
  node [
    id 336
    label "upodabnia&#263;"
  ]
  node [
    id 337
    label "nadawa&#263;"
  ]
  node [
    id 338
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 339
    label "go"
  ]
  node [
    id 340
    label "przybiera&#263;"
  ]
  node [
    id 341
    label "i&#347;&#263;"
  ]
  node [
    id 342
    label "use"
  ]
  node [
    id 343
    label "blurt_out"
  ]
  node [
    id 344
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 345
    label "usuwa&#263;"
  ]
  node [
    id 346
    label "unwrap"
  ]
  node [
    id 347
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 348
    label "pokazywa&#263;"
  ]
  node [
    id 349
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 350
    label "orzyna&#263;"
  ]
  node [
    id 351
    label "oszwabia&#263;"
  ]
  node [
    id 352
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 353
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 354
    label "cheat"
  ]
  node [
    id 355
    label "dispose"
  ]
  node [
    id 356
    label "aran&#380;owa&#263;"
  ]
  node [
    id 357
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 358
    label "odpowiada&#263;"
  ]
  node [
    id 359
    label "zabezpiecza&#263;"
  ]
  node [
    id 360
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 361
    label "doprowadza&#263;"
  ]
  node [
    id 362
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 363
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 364
    label "najem"
  ]
  node [
    id 365
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 366
    label "zak&#322;ad"
  ]
  node [
    id 367
    label "stosunek_pracy"
  ]
  node [
    id 368
    label "benedykty&#324;ski"
  ]
  node [
    id 369
    label "poda&#380;_pracy"
  ]
  node [
    id 370
    label "pracowanie"
  ]
  node [
    id 371
    label "tyrka"
  ]
  node [
    id 372
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 373
    label "wytw&#243;r"
  ]
  node [
    id 374
    label "miejsce"
  ]
  node [
    id 375
    label "zaw&#243;d"
  ]
  node [
    id 376
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 377
    label "tynkarski"
  ]
  node [
    id 378
    label "czynno&#347;&#263;"
  ]
  node [
    id 379
    label "zmiana"
  ]
  node [
    id 380
    label "czynnik_produkcji"
  ]
  node [
    id 381
    label "zobowi&#261;zanie"
  ]
  node [
    id 382
    label "kierownictwo"
  ]
  node [
    id 383
    label "siedziba"
  ]
  node [
    id 384
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 385
    label "nielicznie"
  ]
  node [
    id 386
    label "nieznacznie"
  ]
  node [
    id 387
    label "m&#322;ody"
  ]
  node [
    id 388
    label "ma&#322;o"
  ]
  node [
    id 389
    label "ma&#322;y"
  ]
  node [
    id 390
    label "nieznaczny"
  ]
  node [
    id 391
    label "pomiernie"
  ]
  node [
    id 392
    label "kr&#243;tko"
  ]
  node [
    id 393
    label "mikroskopijnie"
  ]
  node [
    id 394
    label "nieliczny"
  ]
  node [
    id 395
    label "mo&#380;liwie"
  ]
  node [
    id 396
    label "nieistotnie"
  ]
  node [
    id 397
    label "szybki"
  ]
  node [
    id 398
    label "przeci&#281;tny"
  ]
  node [
    id 399
    label "wstydliwy"
  ]
  node [
    id 400
    label "niewa&#380;ny"
  ]
  node [
    id 401
    label "ch&#322;opiec"
  ]
  node [
    id 402
    label "marny"
  ]
  node [
    id 403
    label "n&#281;dznie"
  ]
  node [
    id 404
    label "m&#322;odo"
  ]
  node [
    id 405
    label "nowy"
  ]
  node [
    id 406
    label "cz&#322;owiek"
  ]
  node [
    id 407
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 408
    label "nowo&#380;eniec"
  ]
  node [
    id 409
    label "nie&#380;onaty"
  ]
  node [
    id 410
    label "wczesny"
  ]
  node [
    id 411
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 412
    label "niezawodowy"
  ]
  node [
    id 413
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 414
    label "po_laicku"
  ]
  node [
    id 415
    label "po_amatorsku"
  ]
  node [
    id 416
    label "niezawodowo"
  ]
  node [
    id 417
    label "nierzetelny"
  ]
  node [
    id 418
    label "amatorsko"
  ]
  node [
    id 419
    label "nieprzekonuj&#261;cy"
  ]
  node [
    id 420
    label "nieporz&#261;dny"
  ]
  node [
    id 421
    label "niewiarygodny"
  ]
  node [
    id 422
    label "niedok&#322;adny"
  ]
  node [
    id 423
    label "nierzetelnie"
  ]
  node [
    id 424
    label "nietrwa&#322;y"
  ]
  node [
    id 425
    label "mizerny"
  ]
  node [
    id 426
    label "marnie"
  ]
  node [
    id 427
    label "po&#347;ledni"
  ]
  node [
    id 428
    label "niezdrowy"
  ]
  node [
    id 429
    label "z&#322;y"
  ]
  node [
    id 430
    label "nieumiej&#281;tny"
  ]
  node [
    id 431
    label "s&#322;abo"
  ]
  node [
    id 432
    label "lura"
  ]
  node [
    id 433
    label "nieudany"
  ]
  node [
    id 434
    label "s&#322;abowity"
  ]
  node [
    id 435
    label "zawodny"
  ]
  node [
    id 436
    label "&#322;agodny"
  ]
  node [
    id 437
    label "md&#322;y"
  ]
  node [
    id 438
    label "niedoskona&#322;y"
  ]
  node [
    id 439
    label "przemijaj&#261;cy"
  ]
  node [
    id 440
    label "niemocny"
  ]
  node [
    id 441
    label "niefajny"
  ]
  node [
    id 442
    label "kiepsko"
  ]
  node [
    id 443
    label "nale&#380;ny"
  ]
  node [
    id 444
    label "nale&#380;yty"
  ]
  node [
    id 445
    label "typowy"
  ]
  node [
    id 446
    label "uprawniony"
  ]
  node [
    id 447
    label "zasadniczy"
  ]
  node [
    id 448
    label "stosownie"
  ]
  node [
    id 449
    label "taki"
  ]
  node [
    id 450
    label "prawdziwy"
  ]
  node [
    id 451
    label "ten"
  ]
  node [
    id 452
    label "dobry"
  ]
  node [
    id 453
    label "artlessly"
  ]
  node [
    id 454
    label "gif"
  ]
  node [
    id 455
    label "technika"
  ]
  node [
    id 456
    label "film"
  ]
  node [
    id 457
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 458
    label "wideokaseta"
  ]
  node [
    id 459
    label "odtwarzacz"
  ]
  node [
    id 460
    label "telekomunikacja"
  ]
  node [
    id 461
    label "cywilizacja"
  ]
  node [
    id 462
    label "wiedza"
  ]
  node [
    id 463
    label "sprawno&#347;&#263;"
  ]
  node [
    id 464
    label "engineering"
  ]
  node [
    id 465
    label "fotowoltaika"
  ]
  node [
    id 466
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 467
    label "teletechnika"
  ]
  node [
    id 468
    label "mechanika_precyzyjna"
  ]
  node [
    id 469
    label "technologia"
  ]
  node [
    id 470
    label "animatronika"
  ]
  node [
    id 471
    label "odczulenie"
  ]
  node [
    id 472
    label "odczula&#263;"
  ]
  node [
    id 473
    label "blik"
  ]
  node [
    id 474
    label "odczuli&#263;"
  ]
  node [
    id 475
    label "scena"
  ]
  node [
    id 476
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 477
    label "muza"
  ]
  node [
    id 478
    label "postprodukcja"
  ]
  node [
    id 479
    label "block"
  ]
  node [
    id 480
    label "trawiarnia"
  ]
  node [
    id 481
    label "sklejarka"
  ]
  node [
    id 482
    label "uj&#281;cie"
  ]
  node [
    id 483
    label "filmoteka"
  ]
  node [
    id 484
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 485
    label "klatka"
  ]
  node [
    id 486
    label "rozbieg&#243;wka"
  ]
  node [
    id 487
    label "napisy"
  ]
  node [
    id 488
    label "ta&#347;ma"
  ]
  node [
    id 489
    label "odczulanie"
  ]
  node [
    id 490
    label "anamorfoza"
  ]
  node [
    id 491
    label "dorobek"
  ]
  node [
    id 492
    label "ty&#322;&#243;wka"
  ]
  node [
    id 493
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 494
    label "b&#322;ona"
  ]
  node [
    id 495
    label "emulsja_fotograficzna"
  ]
  node [
    id 496
    label "photograph"
  ]
  node [
    id 497
    label "czo&#322;&#243;wka"
  ]
  node [
    id 498
    label "rola"
  ]
  node [
    id 499
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 500
    label "sprz&#281;t_audio"
  ]
  node [
    id 501
    label "magnetowid"
  ]
  node [
    id 502
    label "wideoteka"
  ]
  node [
    id 503
    label "wypo&#380;yczalnia_wideo"
  ]
  node [
    id 504
    label "kaseta"
  ]
  node [
    id 505
    label "oddany"
  ]
  node [
    id 506
    label "wierny"
  ]
  node [
    id 507
    label "ofiarny"
  ]
  node [
    id 508
    label "nietuzinkowy"
  ]
  node [
    id 509
    label "intryguj&#261;cy"
  ]
  node [
    id 510
    label "ch&#281;tny"
  ]
  node [
    id 511
    label "swoisty"
  ]
  node [
    id 512
    label "interesowanie"
  ]
  node [
    id 513
    label "dziwny"
  ]
  node [
    id 514
    label "interesuj&#261;cy"
  ]
  node [
    id 515
    label "ciekawie"
  ]
  node [
    id 516
    label "indagator"
  ]
  node [
    id 517
    label "niespotykany"
  ]
  node [
    id 518
    label "nietuzinkowo"
  ]
  node [
    id 519
    label "interesuj&#261;co"
  ]
  node [
    id 520
    label "atrakcyjny"
  ]
  node [
    id 521
    label "intryguj&#261;co"
  ]
  node [
    id 522
    label "ch&#281;tliwy"
  ]
  node [
    id 523
    label "ch&#281;tnie"
  ]
  node [
    id 524
    label "napalony"
  ]
  node [
    id 525
    label "chy&#380;y"
  ]
  node [
    id 526
    label "&#380;yczliwy"
  ]
  node [
    id 527
    label "przychylny"
  ]
  node [
    id 528
    label "gotowy"
  ]
  node [
    id 529
    label "ludzko&#347;&#263;"
  ]
  node [
    id 530
    label "asymilowanie"
  ]
  node [
    id 531
    label "wapniak"
  ]
  node [
    id 532
    label "asymilowa&#263;"
  ]
  node [
    id 533
    label "os&#322;abia&#263;"
  ]
  node [
    id 534
    label "posta&#263;"
  ]
  node [
    id 535
    label "hominid"
  ]
  node [
    id 536
    label "podw&#322;adny"
  ]
  node [
    id 537
    label "os&#322;abianie"
  ]
  node [
    id 538
    label "g&#322;owa"
  ]
  node [
    id 539
    label "figura"
  ]
  node [
    id 540
    label "portrecista"
  ]
  node [
    id 541
    label "dwun&#243;g"
  ]
  node [
    id 542
    label "profanum"
  ]
  node [
    id 543
    label "mikrokosmos"
  ]
  node [
    id 544
    label "nasada"
  ]
  node [
    id 545
    label "duch"
  ]
  node [
    id 546
    label "antropochoria"
  ]
  node [
    id 547
    label "osoba"
  ]
  node [
    id 548
    label "wz&#243;r"
  ]
  node [
    id 549
    label "senior"
  ]
  node [
    id 550
    label "oddzia&#322;ywanie"
  ]
  node [
    id 551
    label "Adam"
  ]
  node [
    id 552
    label "homo_sapiens"
  ]
  node [
    id 553
    label "polifag"
  ]
  node [
    id 554
    label "dziwnie"
  ]
  node [
    id 555
    label "dziwy"
  ]
  node [
    id 556
    label "inny"
  ]
  node [
    id 557
    label "odr&#281;bny"
  ]
  node [
    id 558
    label "swoi&#347;cie"
  ]
  node [
    id 559
    label "occupation"
  ]
  node [
    id 560
    label "bycie"
  ]
  node [
    id 561
    label "dobrze"
  ]
  node [
    id 562
    label "ciekawski"
  ]
  node [
    id 563
    label "sprawa"
  ]
  node [
    id 564
    label "subiekcja"
  ]
  node [
    id 565
    label "problemat"
  ]
  node [
    id 566
    label "jajko_Kolumba"
  ]
  node [
    id 567
    label "obstruction"
  ]
  node [
    id 568
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 569
    label "problematyka"
  ]
  node [
    id 570
    label "trudno&#347;&#263;"
  ]
  node [
    id 571
    label "pierepa&#322;ka"
  ]
  node [
    id 572
    label "ambaras"
  ]
  node [
    id 573
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 574
    label "napotka&#263;"
  ]
  node [
    id 575
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 576
    label "k&#322;opotliwy"
  ]
  node [
    id 577
    label "napotkanie"
  ]
  node [
    id 578
    label "poziom"
  ]
  node [
    id 579
    label "difficulty"
  ]
  node [
    id 580
    label "obstacle"
  ]
  node [
    id 581
    label "sytuacja"
  ]
  node [
    id 582
    label "kognicja"
  ]
  node [
    id 583
    label "object"
  ]
  node [
    id 584
    label "rozprawa"
  ]
  node [
    id 585
    label "temat"
  ]
  node [
    id 586
    label "wydarzenie"
  ]
  node [
    id 587
    label "szczeg&#243;&#322;"
  ]
  node [
    id 588
    label "proposition"
  ]
  node [
    id 589
    label "przes&#322;anka"
  ]
  node [
    id 590
    label "rzecz"
  ]
  node [
    id 591
    label "idea"
  ]
  node [
    id 592
    label "k&#322;opot"
  ]
  node [
    id 593
    label "matematycznie"
  ]
  node [
    id 594
    label "dok&#322;adny"
  ]
  node [
    id 595
    label "sprecyzowanie"
  ]
  node [
    id 596
    label "dok&#322;adnie"
  ]
  node [
    id 597
    label "precyzyjny"
  ]
  node [
    id 598
    label "miliamperomierz"
  ]
  node [
    id 599
    label "precyzowanie"
  ]
  node [
    id 600
    label "rzetelny"
  ]
  node [
    id 601
    label "ca&#322;y"
  ]
  node [
    id 602
    label "jedyny"
  ]
  node [
    id 603
    label "du&#380;y"
  ]
  node [
    id 604
    label "zdr&#243;w"
  ]
  node [
    id 605
    label "calu&#347;ko"
  ]
  node [
    id 606
    label "kompletny"
  ]
  node [
    id 607
    label "&#380;ywy"
  ]
  node [
    id 608
    label "pe&#322;ny"
  ]
  node [
    id 609
    label "podobny"
  ]
  node [
    id 610
    label "ca&#322;o"
  ]
  node [
    id 611
    label "participate"
  ]
  node [
    id 612
    label "istnie&#263;"
  ]
  node [
    id 613
    label "pozostawa&#263;"
  ]
  node [
    id 614
    label "zostawa&#263;"
  ]
  node [
    id 615
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 616
    label "adhere"
  ]
  node [
    id 617
    label "compass"
  ]
  node [
    id 618
    label "korzysta&#263;"
  ]
  node [
    id 619
    label "appreciation"
  ]
  node [
    id 620
    label "osi&#261;ga&#263;"
  ]
  node [
    id 621
    label "dociera&#263;"
  ]
  node [
    id 622
    label "get"
  ]
  node [
    id 623
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 624
    label "mierzy&#263;"
  ]
  node [
    id 625
    label "u&#380;ywa&#263;"
  ]
  node [
    id 626
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 627
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 628
    label "exsert"
  ]
  node [
    id 629
    label "being"
  ]
  node [
    id 630
    label "Ohio"
  ]
  node [
    id 631
    label "wci&#281;cie"
  ]
  node [
    id 632
    label "Nowy_York"
  ]
  node [
    id 633
    label "warstwa"
  ]
  node [
    id 634
    label "samopoczucie"
  ]
  node [
    id 635
    label "Illinois"
  ]
  node [
    id 636
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 637
    label "state"
  ]
  node [
    id 638
    label "Jukatan"
  ]
  node [
    id 639
    label "Kalifornia"
  ]
  node [
    id 640
    label "Wirginia"
  ]
  node [
    id 641
    label "wektor"
  ]
  node [
    id 642
    label "Goa"
  ]
  node [
    id 643
    label "Teksas"
  ]
  node [
    id 644
    label "Waszyngton"
  ]
  node [
    id 645
    label "Massachusetts"
  ]
  node [
    id 646
    label "Alaska"
  ]
  node [
    id 647
    label "Arakan"
  ]
  node [
    id 648
    label "Hawaje"
  ]
  node [
    id 649
    label "Maryland"
  ]
  node [
    id 650
    label "punkt"
  ]
  node [
    id 651
    label "Michigan"
  ]
  node [
    id 652
    label "Arizona"
  ]
  node [
    id 653
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 654
    label "Georgia"
  ]
  node [
    id 655
    label "Pensylwania"
  ]
  node [
    id 656
    label "shape"
  ]
  node [
    id 657
    label "Luizjana"
  ]
  node [
    id 658
    label "Nowy_Meksyk"
  ]
  node [
    id 659
    label "Alabama"
  ]
  node [
    id 660
    label "ilo&#347;&#263;"
  ]
  node [
    id 661
    label "Kansas"
  ]
  node [
    id 662
    label "Oregon"
  ]
  node [
    id 663
    label "Oklahoma"
  ]
  node [
    id 664
    label "Floryda"
  ]
  node [
    id 665
    label "jednostka_administracyjna"
  ]
  node [
    id 666
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 667
    label "mo&#380;liwy"
  ]
  node [
    id 668
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 669
    label "odblokowanie_si&#281;"
  ]
  node [
    id 670
    label "zrozumia&#322;y"
  ]
  node [
    id 671
    label "dost&#281;pnie"
  ]
  node [
    id 672
    label "&#322;atwy"
  ]
  node [
    id 673
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 674
    label "przyst&#281;pnie"
  ]
  node [
    id 675
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 676
    label "&#322;atwo"
  ]
  node [
    id 677
    label "letki"
  ]
  node [
    id 678
    label "prosty"
  ]
  node [
    id 679
    label "&#322;acny"
  ]
  node [
    id 680
    label "snadny"
  ]
  node [
    id 681
    label "przyjemny"
  ]
  node [
    id 682
    label "urealnianie"
  ]
  node [
    id 683
    label "mo&#380;ebny"
  ]
  node [
    id 684
    label "umo&#380;liwianie"
  ]
  node [
    id 685
    label "zno&#347;ny"
  ]
  node [
    id 686
    label "umo&#380;liwienie"
  ]
  node [
    id 687
    label "urealnienie"
  ]
  node [
    id 688
    label "pojmowalny"
  ]
  node [
    id 689
    label "uzasadniony"
  ]
  node [
    id 690
    label "wyja&#347;nienie"
  ]
  node [
    id 691
    label "przeja&#347;nianie_si&#281;"
  ]
  node [
    id 692
    label "rozja&#347;nienie"
  ]
  node [
    id 693
    label "przeja&#347;nienie_si&#281;"
  ]
  node [
    id 694
    label "zrozumiale"
  ]
  node [
    id 695
    label "t&#322;umaczenie"
  ]
  node [
    id 696
    label "wyja&#347;nianie_si&#281;"
  ]
  node [
    id 697
    label "sensowny"
  ]
  node [
    id 698
    label "rozja&#347;nianie"
  ]
  node [
    id 699
    label "wyja&#347;nienie_si&#281;"
  ]
  node [
    id 700
    label "przyst&#281;pny"
  ]
  node [
    id 701
    label "wygodnie"
  ]
  node [
    id 702
    label "nasamprz&#243;d"
  ]
  node [
    id 703
    label "pierw"
  ]
  node [
    id 704
    label "pocz&#261;tkowo"
  ]
  node [
    id 705
    label "pierwiej"
  ]
  node [
    id 706
    label "dzieci&#281;co"
  ]
  node [
    id 707
    label "pocz&#261;tkowy"
  ]
  node [
    id 708
    label "zrazu"
  ]
  node [
    id 709
    label "priorytetowo"
  ]
  node [
    id 710
    label "szyba"
  ]
  node [
    id 711
    label "okno"
  ]
  node [
    id 712
    label "YouTube"
  ]
  node [
    id 713
    label "gablota"
  ]
  node [
    id 714
    label "sklep"
  ]
  node [
    id 715
    label "strona"
  ]
  node [
    id 716
    label "parapet"
  ]
  node [
    id 717
    label "okiennica"
  ]
  node [
    id 718
    label "interfejs"
  ]
  node [
    id 719
    label "prze&#347;wit"
  ]
  node [
    id 720
    label "pulpit"
  ]
  node [
    id 721
    label "transenna"
  ]
  node [
    id 722
    label "kwatera_okienna"
  ]
  node [
    id 723
    label "inspekt"
  ]
  node [
    id 724
    label "nora"
  ]
  node [
    id 725
    label "skrzyd&#322;o"
  ]
  node [
    id 726
    label "nadokiennik"
  ]
  node [
    id 727
    label "futryna"
  ]
  node [
    id 728
    label "lufcik"
  ]
  node [
    id 729
    label "program"
  ]
  node [
    id 730
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 731
    label "casement"
  ]
  node [
    id 732
    label "menad&#380;er_okien"
  ]
  node [
    id 733
    label "otw&#243;r"
  ]
  node [
    id 734
    label "szafka"
  ]
  node [
    id 735
    label "fura"
  ]
  node [
    id 736
    label "p&#322;&#243;d"
  ]
  node [
    id 737
    label "rezultat"
  ]
  node [
    id 738
    label "glass"
  ]
  node [
    id 739
    label "antyrama"
  ]
  node [
    id 740
    label "kartka"
  ]
  node [
    id 741
    label "logowanie"
  ]
  node [
    id 742
    label "plik"
  ]
  node [
    id 743
    label "s&#261;d"
  ]
  node [
    id 744
    label "adres_internetowy"
  ]
  node [
    id 745
    label "linia"
  ]
  node [
    id 746
    label "serwis_internetowy"
  ]
  node [
    id 747
    label "bok"
  ]
  node [
    id 748
    label "skr&#281;canie"
  ]
  node [
    id 749
    label "skr&#281;ca&#263;"
  ]
  node [
    id 750
    label "orientowanie"
  ]
  node [
    id 751
    label "skr&#281;ci&#263;"
  ]
  node [
    id 752
    label "zorientowanie"
  ]
  node [
    id 753
    label "ty&#322;"
  ]
  node [
    id 754
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 755
    label "fragment"
  ]
  node [
    id 756
    label "layout"
  ]
  node [
    id 757
    label "obiekt"
  ]
  node [
    id 758
    label "zorientowa&#263;"
  ]
  node [
    id 759
    label "pagina"
  ]
  node [
    id 760
    label "podmiot"
  ]
  node [
    id 761
    label "g&#243;ra"
  ]
  node [
    id 762
    label "orientowa&#263;"
  ]
  node [
    id 763
    label "voice"
  ]
  node [
    id 764
    label "orientacja"
  ]
  node [
    id 765
    label "prz&#243;d"
  ]
  node [
    id 766
    label "internet"
  ]
  node [
    id 767
    label "powierzchnia"
  ]
  node [
    id 768
    label "forma"
  ]
  node [
    id 769
    label "skr&#281;cenie"
  ]
  node [
    id 770
    label "p&#243;&#322;ka"
  ]
  node [
    id 771
    label "firma"
  ]
  node [
    id 772
    label "stoisko"
  ]
  node [
    id 773
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 774
    label "sk&#322;ad"
  ]
  node [
    id 775
    label "obiekt_handlowy"
  ]
  node [
    id 776
    label "zaplecze"
  ]
  node [
    id 777
    label "reputacja"
  ]
  node [
    id 778
    label "pogl&#261;d"
  ]
  node [
    id 779
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 780
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 781
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 782
    label "sofcik"
  ]
  node [
    id 783
    label "wielko&#347;&#263;"
  ]
  node [
    id 784
    label "kryterium"
  ]
  node [
    id 785
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 786
    label "ekspertyza"
  ]
  node [
    id 787
    label "informacja"
  ]
  node [
    id 788
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 789
    label "dokument"
  ]
  node [
    id 790
    label "appraisal"
  ]
  node [
    id 791
    label "charakterystyka"
  ]
  node [
    id 792
    label "m&#322;ot"
  ]
  node [
    id 793
    label "znak"
  ]
  node [
    id 794
    label "drzewo"
  ]
  node [
    id 795
    label "pr&#243;ba"
  ]
  node [
    id 796
    label "attribute"
  ]
  node [
    id 797
    label "marka"
  ]
  node [
    id 798
    label "znaczenie"
  ]
  node [
    id 799
    label "zapis"
  ]
  node [
    id 800
    label "&#347;wiadectwo"
  ]
  node [
    id 801
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 802
    label "parafa"
  ]
  node [
    id 803
    label "raport&#243;wka"
  ]
  node [
    id 804
    label "utw&#243;r"
  ]
  node [
    id 805
    label "record"
  ]
  node [
    id 806
    label "registratura"
  ]
  node [
    id 807
    label "dokumentacja"
  ]
  node [
    id 808
    label "fascyku&#322;"
  ]
  node [
    id 809
    label "artyku&#322;"
  ]
  node [
    id 810
    label "writing"
  ]
  node [
    id 811
    label "sygnatariusz"
  ]
  node [
    id 812
    label "badanie"
  ]
  node [
    id 813
    label "sketch"
  ]
  node [
    id 814
    label "ocena"
  ]
  node [
    id 815
    label "survey"
  ]
  node [
    id 816
    label "teologicznie"
  ]
  node [
    id 817
    label "belief"
  ]
  node [
    id 818
    label "zderzenie_si&#281;"
  ]
  node [
    id 819
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 820
    label "teoria_Arrheniusa"
  ]
  node [
    id 821
    label "publikacja"
  ]
  node [
    id 822
    label "obiega&#263;"
  ]
  node [
    id 823
    label "powzi&#281;cie"
  ]
  node [
    id 824
    label "dane"
  ]
  node [
    id 825
    label "obiegni&#281;cie"
  ]
  node [
    id 826
    label "sygna&#322;"
  ]
  node [
    id 827
    label "obieganie"
  ]
  node [
    id 828
    label "powzi&#261;&#263;"
  ]
  node [
    id 829
    label "obiec"
  ]
  node [
    id 830
    label "doj&#347;cie"
  ]
  node [
    id 831
    label "doj&#347;&#263;"
  ]
  node [
    id 832
    label "warunek_lokalowy"
  ]
  node [
    id 833
    label "rozmiar"
  ]
  node [
    id 834
    label "liczba"
  ]
  node [
    id 835
    label "rzadko&#347;&#263;"
  ]
  node [
    id 836
    label "zaleta"
  ]
  node [
    id 837
    label "measure"
  ]
  node [
    id 838
    label "dymensja"
  ]
  node [
    id 839
    label "poj&#281;cie"
  ]
  node [
    id 840
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 841
    label "zdolno&#347;&#263;"
  ]
  node [
    id 842
    label "potencja"
  ]
  node [
    id 843
    label "property"
  ]
  node [
    id 844
    label "pornografia_mi&#281;kka"
  ]
  node [
    id 845
    label "drobiazg"
  ]
  node [
    id 846
    label "pornografia"
  ]
  node [
    id 847
    label "czynnik"
  ]
  node [
    id 848
    label "doba"
  ]
  node [
    id 849
    label "weekend"
  ]
  node [
    id 850
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 851
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 852
    label "czas"
  ]
  node [
    id 853
    label "miesi&#261;c"
  ]
  node [
    id 854
    label "poprzedzanie"
  ]
  node [
    id 855
    label "czasoprzestrze&#324;"
  ]
  node [
    id 856
    label "laba"
  ]
  node [
    id 857
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 858
    label "chronometria"
  ]
  node [
    id 859
    label "rachuba_czasu"
  ]
  node [
    id 860
    label "przep&#322;ywanie"
  ]
  node [
    id 861
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 862
    label "czasokres"
  ]
  node [
    id 863
    label "odczyt"
  ]
  node [
    id 864
    label "chwila"
  ]
  node [
    id 865
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 866
    label "dzieje"
  ]
  node [
    id 867
    label "poprzedzenie"
  ]
  node [
    id 868
    label "trawienie"
  ]
  node [
    id 869
    label "period"
  ]
  node [
    id 870
    label "okres_czasu"
  ]
  node [
    id 871
    label "poprzedza&#263;"
  ]
  node [
    id 872
    label "schy&#322;ek"
  ]
  node [
    id 873
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 874
    label "odwlekanie_si&#281;"
  ]
  node [
    id 875
    label "zegar"
  ]
  node [
    id 876
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 877
    label "czwarty_wymiar"
  ]
  node [
    id 878
    label "Zeitgeist"
  ]
  node [
    id 879
    label "trawi&#263;"
  ]
  node [
    id 880
    label "pogoda"
  ]
  node [
    id 881
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 882
    label "poprzedzi&#263;"
  ]
  node [
    id 883
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 884
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 885
    label "time_period"
  ]
  node [
    id 886
    label "noc"
  ]
  node [
    id 887
    label "dzie&#324;"
  ]
  node [
    id 888
    label "godzina"
  ]
  node [
    id 889
    label "long_time"
  ]
  node [
    id 890
    label "jednostka_geologiczna"
  ]
  node [
    id 891
    label "niedziela"
  ]
  node [
    id 892
    label "sobota"
  ]
  node [
    id 893
    label "miech"
  ]
  node [
    id 894
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 895
    label "rok"
  ]
  node [
    id 896
    label "kalendy"
  ]
  node [
    id 897
    label "tam"
  ]
  node [
    id 898
    label "tu"
  ]
  node [
    id 899
    label "pr&#281;dki"
  ]
  node [
    id 900
    label "najwa&#380;niejszy"
  ]
  node [
    id 901
    label "dobroczynny"
  ]
  node [
    id 902
    label "czw&#243;rka"
  ]
  node [
    id 903
    label "spokojny"
  ]
  node [
    id 904
    label "skuteczny"
  ]
  node [
    id 905
    label "&#347;mieszny"
  ]
  node [
    id 906
    label "mi&#322;y"
  ]
  node [
    id 907
    label "grzeczny"
  ]
  node [
    id 908
    label "powitanie"
  ]
  node [
    id 909
    label "zwrot"
  ]
  node [
    id 910
    label "pomy&#347;lny"
  ]
  node [
    id 911
    label "moralny"
  ]
  node [
    id 912
    label "drogi"
  ]
  node [
    id 913
    label "pozytywny"
  ]
  node [
    id 914
    label "odpowiedni"
  ]
  node [
    id 915
    label "korzystny"
  ]
  node [
    id 916
    label "pos&#322;uszny"
  ]
  node [
    id 917
    label "intensywny"
  ]
  node [
    id 918
    label "kr&#243;tki"
  ]
  node [
    id 919
    label "temperamentny"
  ]
  node [
    id 920
    label "dynamiczny"
  ]
  node [
    id 921
    label "szybko"
  ]
  node [
    id 922
    label "sprawny"
  ]
  node [
    id 923
    label "energiczny"
  ]
  node [
    id 924
    label "ranek"
  ]
  node [
    id 925
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 926
    label "podwiecz&#243;r"
  ]
  node [
    id 927
    label "po&#322;udnie"
  ]
  node [
    id 928
    label "przedpo&#322;udnie"
  ]
  node [
    id 929
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 930
    label "wiecz&#243;r"
  ]
  node [
    id 931
    label "t&#322;usty_czwartek"
  ]
  node [
    id 932
    label "popo&#322;udnie"
  ]
  node [
    id 933
    label "walentynki"
  ]
  node [
    id 934
    label "czynienie_si&#281;"
  ]
  node [
    id 935
    label "s&#322;o&#324;ce"
  ]
  node [
    id 936
    label "rano"
  ]
  node [
    id 937
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 938
    label "wzej&#347;cie"
  ]
  node [
    id 939
    label "wsta&#263;"
  ]
  node [
    id 940
    label "day"
  ]
  node [
    id 941
    label "termin"
  ]
  node [
    id 942
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 943
    label "wstanie"
  ]
  node [
    id 944
    label "przedwiecz&#243;r"
  ]
  node [
    id 945
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 946
    label "Sylwester"
  ]
  node [
    id 947
    label "dzieci&#281;cy"
  ]
  node [
    id 948
    label "podstawowy"
  ]
  node [
    id 949
    label "elementarny"
  ]
  node [
    id 950
    label "sk&#322;ada&#263;"
  ]
  node [
    id 951
    label "zbiera&#263;"
  ]
  node [
    id 952
    label "przywraca&#263;"
  ]
  node [
    id 953
    label "dawa&#263;"
  ]
  node [
    id 954
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 955
    label "convey"
  ]
  node [
    id 956
    label "publicize"
  ]
  node [
    id 957
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 958
    label "render"
  ]
  node [
    id 959
    label "uk&#322;ada&#263;"
  ]
  node [
    id 960
    label "opracowywa&#263;"
  ]
  node [
    id 961
    label "set"
  ]
  node [
    id 962
    label "oddawa&#263;"
  ]
  node [
    id 963
    label "train"
  ]
  node [
    id 964
    label "dzieli&#263;"
  ]
  node [
    id 965
    label "scala&#263;"
  ]
  node [
    id 966
    label "zestaw"
  ]
  node [
    id 967
    label "warto&#347;&#263;"
  ]
  node [
    id 968
    label "quality"
  ]
  node [
    id 969
    label "co&#347;"
  ]
  node [
    id 970
    label "syf"
  ]
  node [
    id 971
    label "zrewaluowa&#263;"
  ]
  node [
    id 972
    label "zmienna"
  ]
  node [
    id 973
    label "wskazywanie"
  ]
  node [
    id 974
    label "rewaluowanie"
  ]
  node [
    id 975
    label "cel"
  ]
  node [
    id 976
    label "wskazywa&#263;"
  ]
  node [
    id 977
    label "korzy&#347;&#263;"
  ]
  node [
    id 978
    label "worth"
  ]
  node [
    id 979
    label "zrewaluowanie"
  ]
  node [
    id 980
    label "rewaluowa&#263;"
  ]
  node [
    id 981
    label "wabik"
  ]
  node [
    id 982
    label "thing"
  ]
  node [
    id 983
    label "cosik"
  ]
  node [
    id 984
    label "syphilis"
  ]
  node [
    id 985
    label "tragedia"
  ]
  node [
    id 986
    label "nieporz&#261;dek"
  ]
  node [
    id 987
    label "kr&#281;tek_blady"
  ]
  node [
    id 988
    label "krosta"
  ]
  node [
    id 989
    label "choroba_dworska"
  ]
  node [
    id 990
    label "choroba_bakteryjna"
  ]
  node [
    id 991
    label "zabrudzenie"
  ]
  node [
    id 992
    label "substancja"
  ]
  node [
    id 993
    label "choroba_weneryczna"
  ]
  node [
    id 994
    label "ki&#322;a_wrodzona"
  ]
  node [
    id 995
    label "szankier_twardy"
  ]
  node [
    id 996
    label "spot"
  ]
  node [
    id 997
    label "zanieczyszczenie"
  ]
  node [
    id 998
    label "charakterystycznie"
  ]
  node [
    id 999
    label "nale&#380;nie"
  ]
  node [
    id 1000
    label "stosowny"
  ]
  node [
    id 1001
    label "nale&#380;ycie"
  ]
  node [
    id 1002
    label "prawdziwie"
  ]
  node [
    id 1003
    label "szczero"
  ]
  node [
    id 1004
    label "podobnie"
  ]
  node [
    id 1005
    label "zgodnie"
  ]
  node [
    id 1006
    label "naprawd&#281;"
  ]
  node [
    id 1007
    label "szczerze"
  ]
  node [
    id 1008
    label "truly"
  ]
  node [
    id 1009
    label "rzeczywisty"
  ]
  node [
    id 1010
    label "przystojnie"
  ]
  node [
    id 1011
    label "zadowalaj&#261;co"
  ]
  node [
    id 1012
    label "rz&#261;dnie"
  ]
  node [
    id 1013
    label "typowo"
  ]
  node [
    id 1014
    label "wyj&#261;tkowo"
  ]
  node [
    id 1015
    label "szczeg&#243;lnie"
  ]
  node [
    id 1016
    label "odpowiednio"
  ]
  node [
    id 1017
    label "dobroczynnie"
  ]
  node [
    id 1018
    label "moralnie"
  ]
  node [
    id 1019
    label "korzystnie"
  ]
  node [
    id 1020
    label "pozytywnie"
  ]
  node [
    id 1021
    label "lepiej"
  ]
  node [
    id 1022
    label "wiele"
  ]
  node [
    id 1023
    label "skutecznie"
  ]
  node [
    id 1024
    label "pomy&#347;lnie"
  ]
  node [
    id 1025
    label "denno&#347;&#263;"
  ]
  node [
    id 1026
    label "marno&#347;&#263;"
  ]
  node [
    id 1027
    label "punctiliously"
  ]
  node [
    id 1028
    label "meticulously"
  ]
  node [
    id 1029
    label "precyzyjnie"
  ]
  node [
    id 1030
    label "rzetelnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 395
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 293
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 482
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 373
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 707
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 510
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 452
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 413
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 561
  ]
  edge [
    source 21
    target 601
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 397
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 406
  ]
  edge [
    source 21
    target 522
  ]
  edge [
    source 21
    target 523
  ]
  edge [
    source 21
    target 524
  ]
  edge [
    source 21
    target 525
  ]
  edge [
    source 21
    target 526
  ]
  edge [
    source 21
    target 527
  ]
  edge [
    source 21
    target 528
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 848
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 852
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 704
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 950
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 951
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 952
  ]
  edge [
    source 22
    target 953
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 22
    target 954
  ]
  edge [
    source 22
    target 955
  ]
  edge [
    source 22
    target 956
  ]
  edge [
    source 22
    target 957
  ]
  edge [
    source 22
    target 958
  ]
  edge [
    source 22
    target 959
  ]
  edge [
    source 22
    target 960
  ]
  edge [
    source 22
    target 961
  ]
  edge [
    source 22
    target 962
  ]
  edge [
    source 22
    target 963
  ]
  edge [
    source 22
    target 330
  ]
  edge [
    source 22
    target 964
  ]
  edge [
    source 22
    target 965
  ]
  edge [
    source 22
    target 966
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 967
  ]
  edge [
    source 23
    target 968
  ]
  edge [
    source 23
    target 969
  ]
  edge [
    source 23
    target 637
  ]
  edge [
    source 23
    target 970
  ]
  edge [
    source 23
    target 833
  ]
  edge [
    source 23
    target 971
  ]
  edge [
    source 23
    target 972
  ]
  edge [
    source 23
    target 973
  ]
  edge [
    source 23
    target 974
  ]
  edge [
    source 23
    target 975
  ]
  edge [
    source 23
    target 976
  ]
  edge [
    source 23
    target 977
  ]
  edge [
    source 23
    target 839
  ]
  edge [
    source 23
    target 978
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 980
  ]
  edge [
    source 23
    target 981
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 982
  ]
  edge [
    source 23
    target 983
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 989
  ]
  edge [
    source 23
    target 990
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 992
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 993
  ]
  edge [
    source 23
    target 994
  ]
  edge [
    source 23
    target 995
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 997
  ]
  edge [
    source 23
    target 429
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 999
  ]
  edge [
    source 24
    target 1000
  ]
  edge [
    source 24
    target 561
  ]
  edge [
    source 24
    target 413
  ]
  edge [
    source 24
    target 1001
  ]
  edge [
    source 24
    target 1002
  ]
  edge [
    source 24
    target 443
  ]
  edge [
    source 24
    target 1003
  ]
  edge [
    source 24
    target 1004
  ]
  edge [
    source 24
    target 1005
  ]
  edge [
    source 24
    target 1006
  ]
  edge [
    source 24
    target 1007
  ]
  edge [
    source 24
    target 1008
  ]
  edge [
    source 24
    target 450
  ]
  edge [
    source 24
    target 1009
  ]
  edge [
    source 24
    target 1010
  ]
  edge [
    source 24
    target 444
  ]
  edge [
    source 24
    target 1011
  ]
  edge [
    source 24
    target 1012
  ]
  edge [
    source 24
    target 1013
  ]
  edge [
    source 24
    target 1014
  ]
  edge [
    source 24
    target 1015
  ]
  edge [
    source 24
    target 70
  ]
  edge [
    source 24
    target 1016
  ]
  edge [
    source 24
    target 1017
  ]
  edge [
    source 24
    target 1018
  ]
  edge [
    source 24
    target 1019
  ]
  edge [
    source 24
    target 1020
  ]
  edge [
    source 24
    target 1021
  ]
  edge [
    source 24
    target 1022
  ]
  edge [
    source 24
    target 1023
  ]
  edge [
    source 24
    target 1024
  ]
  edge [
    source 24
    target 452
  ]
  edge [
    source 24
    target 445
  ]
  edge [
    source 24
    target 446
  ]
  edge [
    source 24
    target 447
  ]
  edge [
    source 24
    target 448
  ]
  edge [
    source 24
    target 449
  ]
  edge [
    source 24
    target 451
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 967
  ]
  edge [
    source 25
    target 1025
  ]
  edge [
    source 25
    target 833
  ]
  edge [
    source 25
    target 980
  ]
  edge [
    source 25
    target 971
  ]
  edge [
    source 25
    target 972
  ]
  edge [
    source 25
    target 973
  ]
  edge [
    source 25
    target 974
  ]
  edge [
    source 25
    target 975
  ]
  edge [
    source 25
    target 976
  ]
  edge [
    source 25
    target 977
  ]
  edge [
    source 25
    target 839
  ]
  edge [
    source 25
    target 978
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 979
  ]
  edge [
    source 25
    target 981
  ]
  edge [
    source 25
    target 715
  ]
  edge [
    source 25
    target 1026
  ]
  edge [
    source 26
    target 596
  ]
  edge [
    source 26
    target 1027
  ]
  edge [
    source 26
    target 1028
  ]
  edge [
    source 26
    target 1029
  ]
  edge [
    source 26
    target 594
  ]
  edge [
    source 26
    target 1030
  ]
]
