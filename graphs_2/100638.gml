graph [
  node [
    id 0
    label "koran"
    origin "text"
  ]
  node [
    id 1
    label "jasno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "poranek"
    origin "text"
  ]
  node [
    id 3
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 4
    label "&#347;wieci&#263;"
  ]
  node [
    id 5
    label "wpadni&#281;cie"
  ]
  node [
    id 6
    label "zjawisko"
  ]
  node [
    id 7
    label "fotokataliza"
  ]
  node [
    id 8
    label "wpa&#347;&#263;"
  ]
  node [
    id 9
    label "promieniowanie_optyczne"
  ]
  node [
    id 10
    label "ja&#347;nia"
  ]
  node [
    id 11
    label "klarowno&#347;&#263;"
  ]
  node [
    id 12
    label "light"
  ]
  node [
    id 13
    label "jednoznaczno&#347;&#263;"
  ]
  node [
    id 14
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 15
    label "wpada&#263;"
  ]
  node [
    id 16
    label "ton"
  ]
  node [
    id 17
    label "kontrast"
  ]
  node [
    id 18
    label "przy&#263;mienie"
  ]
  node [
    id 19
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 20
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 21
    label "&#347;wiecenie"
  ]
  node [
    id 22
    label "przy&#263;mi&#263;"
  ]
  node [
    id 23
    label "pogodno&#347;&#263;"
  ]
  node [
    id 24
    label "promie&#324;"
  ]
  node [
    id 25
    label "polish"
  ]
  node [
    id 26
    label "wpadanie"
  ]
  node [
    id 27
    label "przy&#263;miewanie"
  ]
  node [
    id 28
    label "okre&#347;lono&#347;&#263;"
  ]
  node [
    id 29
    label "zrozumia&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "cecha"
  ]
  node [
    id 31
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 32
    label "wyraz"
  ]
  node [
    id 33
    label "atmosfera"
  ]
  node [
    id 34
    label "bezproblemowo&#347;&#263;"
  ]
  node [
    id 35
    label "lucidity"
  ]
  node [
    id 36
    label "faktura"
  ]
  node [
    id 37
    label "przenikalno&#347;&#263;"
  ]
  node [
    id 38
    label "brzmienie"
  ]
  node [
    id 39
    label "proces"
  ]
  node [
    id 40
    label "boski"
  ]
  node [
    id 41
    label "krajobraz"
  ]
  node [
    id 42
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 43
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 44
    label "przywidzenie"
  ]
  node [
    id 45
    label "presence"
  ]
  node [
    id 46
    label "charakter"
  ]
  node [
    id 47
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 48
    label "wieloton"
  ]
  node [
    id 49
    label "tu&#324;czyk"
  ]
  node [
    id 50
    label "d&#378;wi&#281;k"
  ]
  node [
    id 51
    label "zabarwienie"
  ]
  node [
    id 52
    label "interwa&#322;"
  ]
  node [
    id 53
    label "modalizm"
  ]
  node [
    id 54
    label "ubarwienie"
  ]
  node [
    id 55
    label "note"
  ]
  node [
    id 56
    label "formality"
  ]
  node [
    id 57
    label "glinka"
  ]
  node [
    id 58
    label "jednostka"
  ]
  node [
    id 59
    label "sound"
  ]
  node [
    id 60
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 61
    label "zwyczaj"
  ]
  node [
    id 62
    label "neoproterozoik"
  ]
  node [
    id 63
    label "solmizacja"
  ]
  node [
    id 64
    label "seria"
  ]
  node [
    id 65
    label "tone"
  ]
  node [
    id 66
    label "kolorystyka"
  ]
  node [
    id 67
    label "r&#243;&#380;nica"
  ]
  node [
    id 68
    label "akcent"
  ]
  node [
    id 69
    label "repetycja"
  ]
  node [
    id 70
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 71
    label "heksachord"
  ]
  node [
    id 72
    label "rejestr"
  ]
  node [
    id 73
    label "stosunek"
  ]
  node [
    id 74
    label "preparat"
  ]
  node [
    id 75
    label "chwyt"
  ]
  node [
    id 76
    label "prze&#347;wietlenie"
  ]
  node [
    id 77
    label "ultrasonografia"
  ]
  node [
    id 78
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 79
    label "strike"
  ]
  node [
    id 80
    label "zaziera&#263;"
  ]
  node [
    id 81
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 82
    label "czu&#263;"
  ]
  node [
    id 83
    label "spotyka&#263;"
  ]
  node [
    id 84
    label "drop"
  ]
  node [
    id 85
    label "pogo"
  ]
  node [
    id 86
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 87
    label "rzecz"
  ]
  node [
    id 88
    label "ogrom"
  ]
  node [
    id 89
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 90
    label "zapach"
  ]
  node [
    id 91
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 92
    label "popada&#263;"
  ]
  node [
    id 93
    label "odwiedza&#263;"
  ]
  node [
    id 94
    label "wymy&#347;la&#263;"
  ]
  node [
    id 95
    label "przypomina&#263;"
  ]
  node [
    id 96
    label "ujmowa&#263;"
  ]
  node [
    id 97
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 98
    label "&#347;wiat&#322;o"
  ]
  node [
    id 99
    label "fall"
  ]
  node [
    id 100
    label "chowa&#263;"
  ]
  node [
    id 101
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 102
    label "demaskowa&#263;"
  ]
  node [
    id 103
    label "ulega&#263;"
  ]
  node [
    id 104
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 105
    label "emocja"
  ]
  node [
    id 106
    label "flatten"
  ]
  node [
    id 107
    label "darken"
  ]
  node [
    id 108
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 109
    label "os&#322;abi&#263;"
  ]
  node [
    id 110
    label "addle"
  ]
  node [
    id 111
    label "przygasi&#263;"
  ]
  node [
    id 112
    label "wymy&#347;lenie"
  ]
  node [
    id 113
    label "spotkanie"
  ]
  node [
    id 114
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 115
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 116
    label "ulegni&#281;cie"
  ]
  node [
    id 117
    label "collapse"
  ]
  node [
    id 118
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 119
    label "poniesienie"
  ]
  node [
    id 120
    label "ciecz"
  ]
  node [
    id 121
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 122
    label "odwiedzenie"
  ]
  node [
    id 123
    label "uderzenie"
  ]
  node [
    id 124
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 125
    label "rzeka"
  ]
  node [
    id 126
    label "postrzeganie"
  ]
  node [
    id 127
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 128
    label "dostanie_si&#281;"
  ]
  node [
    id 129
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 130
    label "release"
  ]
  node [
    id 131
    label "rozbicie_si&#281;"
  ]
  node [
    id 132
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 133
    label "g&#243;rowanie"
  ]
  node [
    id 134
    label "os&#322;abianie"
  ]
  node [
    id 135
    label "przy&#263;miony"
  ]
  node [
    id 136
    label "&#263;mienie"
  ]
  node [
    id 137
    label "signal"
  ]
  node [
    id 138
    label "pojawianie_si&#281;"
  ]
  node [
    id 139
    label "reakcja_chemiczna"
  ]
  node [
    id 140
    label "katalizator"
  ]
  node [
    id 141
    label "kataliza"
  ]
  node [
    id 142
    label "gleam"
  ]
  node [
    id 143
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 144
    label "os&#322;abienie"
  ]
  node [
    id 145
    label "przewy&#380;szenie"
  ]
  node [
    id 146
    label "mystification"
  ]
  node [
    id 147
    label "gorze&#263;"
  ]
  node [
    id 148
    label "o&#347;wietla&#263;"
  ]
  node [
    id 149
    label "kierowa&#263;"
  ]
  node [
    id 150
    label "kolor"
  ]
  node [
    id 151
    label "flash"
  ]
  node [
    id 152
    label "czuwa&#263;"
  ]
  node [
    id 153
    label "radiance"
  ]
  node [
    id 154
    label "wyr&#243;&#380;nia&#263;_si&#281;"
  ]
  node [
    id 155
    label "tryska&#263;"
  ]
  node [
    id 156
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 157
    label "smoulder"
  ]
  node [
    id 158
    label "gra&#263;"
  ]
  node [
    id 159
    label "emanowa&#263;"
  ]
  node [
    id 160
    label "zarysowywa&#263;_si&#281;"
  ]
  node [
    id 161
    label "ridicule"
  ]
  node [
    id 162
    label "tli&#263;_si&#281;"
  ]
  node [
    id 163
    label "bi&#263;_po_oczach"
  ]
  node [
    id 164
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 165
    label "g&#243;rowa&#263;"
  ]
  node [
    id 166
    label "os&#322;abia&#263;"
  ]
  node [
    id 167
    label "eclipse"
  ]
  node [
    id 168
    label "dim"
  ]
  node [
    id 169
    label "o&#347;wietlanie"
  ]
  node [
    id 170
    label "w&#322;&#261;czanie"
  ]
  node [
    id 171
    label "bycie"
  ]
  node [
    id 172
    label "zarysowywanie_si&#281;"
  ]
  node [
    id 173
    label "zapalanie"
  ]
  node [
    id 174
    label "ignition"
  ]
  node [
    id 175
    label "za&#347;wiecenie"
  ]
  node [
    id 176
    label "limelight"
  ]
  node [
    id 177
    label "palenie"
  ]
  node [
    id 178
    label "po&#347;wiecenie"
  ]
  node [
    id 179
    label "uleganie"
  ]
  node [
    id 180
    label "dostawanie_si&#281;"
  ]
  node [
    id 181
    label "odwiedzanie"
  ]
  node [
    id 182
    label "spotykanie"
  ]
  node [
    id 183
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 184
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 185
    label "wymy&#347;lanie"
  ]
  node [
    id 186
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 187
    label "ingress"
  ]
  node [
    id 188
    label "dzianie_si&#281;"
  ]
  node [
    id 189
    label "wp&#322;ywanie"
  ]
  node [
    id 190
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 191
    label "overlap"
  ]
  node [
    id 192
    label "wkl&#281;sanie"
  ]
  node [
    id 193
    label "ulec"
  ]
  node [
    id 194
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 195
    label "fall_upon"
  ]
  node [
    id 196
    label "ponie&#347;&#263;"
  ]
  node [
    id 197
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 198
    label "uderzy&#263;"
  ]
  node [
    id 199
    label "wymy&#347;li&#263;"
  ]
  node [
    id 200
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 201
    label "decline"
  ]
  node [
    id 202
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 203
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 204
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 205
    label "spotka&#263;"
  ]
  node [
    id 206
    label "odwiedzi&#263;"
  ]
  node [
    id 207
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 208
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 209
    label "wyrostek"
  ]
  node [
    id 210
    label "pi&#243;rko"
  ]
  node [
    id 211
    label "strumie&#324;"
  ]
  node [
    id 212
    label "odcinek"
  ]
  node [
    id 213
    label "zapowied&#378;"
  ]
  node [
    id 214
    label "odrobina"
  ]
  node [
    id 215
    label "rozeta"
  ]
  node [
    id 216
    label "dzie&#324;"
  ]
  node [
    id 217
    label "blady_&#347;wit"
  ]
  node [
    id 218
    label "podkurek"
  ]
  node [
    id 219
    label "ranek"
  ]
  node [
    id 220
    label "posi&#322;ek"
  ]
  node [
    id 221
    label "Popielec"
  ]
  node [
    id 222
    label "doba"
  ]
  node [
    id 223
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 224
    label "noc"
  ]
  node [
    id 225
    label "podwiecz&#243;r"
  ]
  node [
    id 226
    label "po&#322;udnie"
  ]
  node [
    id 227
    label "godzina"
  ]
  node [
    id 228
    label "przedpo&#322;udnie"
  ]
  node [
    id 229
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 230
    label "long_time"
  ]
  node [
    id 231
    label "wiecz&#243;r"
  ]
  node [
    id 232
    label "t&#322;usty_czwartek"
  ]
  node [
    id 233
    label "popo&#322;udnie"
  ]
  node [
    id 234
    label "walentynki"
  ]
  node [
    id 235
    label "czynienie_si&#281;"
  ]
  node [
    id 236
    label "s&#322;o&#324;ce"
  ]
  node [
    id 237
    label "rano"
  ]
  node [
    id 238
    label "tydzie&#324;"
  ]
  node [
    id 239
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 240
    label "wzej&#347;cie"
  ]
  node [
    id 241
    label "czas"
  ]
  node [
    id 242
    label "wsta&#263;"
  ]
  node [
    id 243
    label "day"
  ]
  node [
    id 244
    label "termin"
  ]
  node [
    id 245
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 246
    label "wstanie"
  ]
  node [
    id 247
    label "przedwiecz&#243;r"
  ]
  node [
    id 248
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 249
    label "Sylwester"
  ]
  node [
    id 250
    label "Koran"
  ]
  node [
    id 251
    label "Surah"
  ]
  node [
    id 252
    label "Ada"
  ]
  node [
    id 253
    label "Duh&#226;"
  ]
  node [
    id 254
    label "na"
  ]
  node [
    id 255
    label "wsch&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 236
    target 254
  ]
  edge [
    source 236
    target 255
  ]
  edge [
    source 251
    target 252
  ]
  edge [
    source 251
    target 253
  ]
  edge [
    source 252
    target 253
  ]
  edge [
    source 254
    target 255
  ]
]
