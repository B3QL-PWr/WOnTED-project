graph [
  node [
    id 0
    label "niesamowity"
    origin "text"
  ]
  node [
    id 1
    label "niesamowicie"
  ]
  node [
    id 2
    label "niezwyk&#322;y"
  ]
  node [
    id 3
    label "niezwykle"
  ]
  node [
    id 4
    label "inny"
  ]
  node [
    id 5
    label "niesamowito"
  ]
  node [
    id 6
    label "bardzo"
  ]
  node [
    id 7
    label "stara"
  ]
  node [
    id 8
    label "miasto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 7
    target 8
  ]
]
