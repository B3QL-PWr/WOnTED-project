graph [
  node [
    id 0
    label "przez"
    origin "text"
  ]
  node [
    id 1
    label "wiele"
    origin "text"
  ]
  node [
    id 2
    label "godzina"
    origin "text"
  ]
  node [
    id 3
    label "poci&#261;g"
    origin "text"
  ]
  node [
    id 4
    label "krak"
    origin "text"
  ]
  node [
    id 5
    label "sta&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "peron"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 9
    label "wjecha&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wiela"
  ]
  node [
    id 11
    label "du&#380;y"
  ]
  node [
    id 12
    label "du&#380;o"
  ]
  node [
    id 13
    label "doros&#322;y"
  ]
  node [
    id 14
    label "znaczny"
  ]
  node [
    id 15
    label "niema&#322;o"
  ]
  node [
    id 16
    label "rozwini&#281;ty"
  ]
  node [
    id 17
    label "dorodny"
  ]
  node [
    id 18
    label "wa&#380;ny"
  ]
  node [
    id 19
    label "prawdziwy"
  ]
  node [
    id 20
    label "time"
  ]
  node [
    id 21
    label "doba"
  ]
  node [
    id 22
    label "p&#243;&#322;godzina"
  ]
  node [
    id 23
    label "jednostka_czasu"
  ]
  node [
    id 24
    label "czas"
  ]
  node [
    id 25
    label "minuta"
  ]
  node [
    id 26
    label "kwadrans"
  ]
  node [
    id 27
    label "poprzedzanie"
  ]
  node [
    id 28
    label "czasoprzestrze&#324;"
  ]
  node [
    id 29
    label "laba"
  ]
  node [
    id 30
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 31
    label "chronometria"
  ]
  node [
    id 32
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 33
    label "rachuba_czasu"
  ]
  node [
    id 34
    label "przep&#322;ywanie"
  ]
  node [
    id 35
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 36
    label "czasokres"
  ]
  node [
    id 37
    label "odczyt"
  ]
  node [
    id 38
    label "chwila"
  ]
  node [
    id 39
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 40
    label "dzieje"
  ]
  node [
    id 41
    label "kategoria_gramatyczna"
  ]
  node [
    id 42
    label "poprzedzenie"
  ]
  node [
    id 43
    label "trawienie"
  ]
  node [
    id 44
    label "pochodzi&#263;"
  ]
  node [
    id 45
    label "period"
  ]
  node [
    id 46
    label "okres_czasu"
  ]
  node [
    id 47
    label "poprzedza&#263;"
  ]
  node [
    id 48
    label "schy&#322;ek"
  ]
  node [
    id 49
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 50
    label "odwlekanie_si&#281;"
  ]
  node [
    id 51
    label "zegar"
  ]
  node [
    id 52
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 53
    label "czwarty_wymiar"
  ]
  node [
    id 54
    label "pochodzenie"
  ]
  node [
    id 55
    label "koniugacja"
  ]
  node [
    id 56
    label "Zeitgeist"
  ]
  node [
    id 57
    label "trawi&#263;"
  ]
  node [
    id 58
    label "pogoda"
  ]
  node [
    id 59
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 60
    label "poprzedzi&#263;"
  ]
  node [
    id 61
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 62
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 63
    label "time_period"
  ]
  node [
    id 64
    label "zapis"
  ]
  node [
    id 65
    label "sekunda"
  ]
  node [
    id 66
    label "jednostka"
  ]
  node [
    id 67
    label "stopie&#324;"
  ]
  node [
    id 68
    label "design"
  ]
  node [
    id 69
    label "tydzie&#324;"
  ]
  node [
    id 70
    label "noc"
  ]
  node [
    id 71
    label "dzie&#324;"
  ]
  node [
    id 72
    label "long_time"
  ]
  node [
    id 73
    label "jednostka_geologiczna"
  ]
  node [
    id 74
    label "pojazd_kolejowy"
  ]
  node [
    id 75
    label "wagon"
  ]
  node [
    id 76
    label "cug"
  ]
  node [
    id 77
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 78
    label "lokomotywa"
  ]
  node [
    id 79
    label "tender"
  ]
  node [
    id 80
    label "kolej"
  ]
  node [
    id 81
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 82
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 83
    label "statek"
  ]
  node [
    id 84
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 85
    label "okr&#281;t"
  ]
  node [
    id 86
    label "karton"
  ]
  node [
    id 87
    label "czo&#322;ownica"
  ]
  node [
    id 88
    label "harmonijka"
  ]
  node [
    id 89
    label "tramwaj"
  ]
  node [
    id 90
    label "klasa"
  ]
  node [
    id 91
    label "ciuchcia"
  ]
  node [
    id 92
    label "pojazd_trakcyjny"
  ]
  node [
    id 93
    label "para"
  ]
  node [
    id 94
    label "pr&#261;d"
  ]
  node [
    id 95
    label "draft"
  ]
  node [
    id 96
    label "stan"
  ]
  node [
    id 97
    label "&#347;l&#261;ski"
  ]
  node [
    id 98
    label "ci&#261;g"
  ]
  node [
    id 99
    label "zaprz&#281;g"
  ]
  node [
    id 100
    label "droga"
  ]
  node [
    id 101
    label "trakcja"
  ]
  node [
    id 102
    label "run"
  ]
  node [
    id 103
    label "blokada"
  ]
  node [
    id 104
    label "kolejno&#347;&#263;"
  ]
  node [
    id 105
    label "tor"
  ]
  node [
    id 106
    label "linia"
  ]
  node [
    id 107
    label "proces"
  ]
  node [
    id 108
    label "pocz&#261;tek"
  ]
  node [
    id 109
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 110
    label "cedu&#322;a"
  ]
  node [
    id 111
    label "nast&#281;pstwo"
  ]
  node [
    id 112
    label "regularny"
  ]
  node [
    id 113
    label "jednakowy"
  ]
  node [
    id 114
    label "zwyk&#322;y"
  ]
  node [
    id 115
    label "stale"
  ]
  node [
    id 116
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 117
    label "mundurowanie"
  ]
  node [
    id 118
    label "zr&#243;wnanie"
  ]
  node [
    id 119
    label "taki&#380;"
  ]
  node [
    id 120
    label "mundurowa&#263;"
  ]
  node [
    id 121
    label "jednakowo"
  ]
  node [
    id 122
    label "zr&#243;wnywanie"
  ]
  node [
    id 123
    label "identyczny"
  ]
  node [
    id 124
    label "przeci&#281;tny"
  ]
  node [
    id 125
    label "zwyczajnie"
  ]
  node [
    id 126
    label "zwykle"
  ]
  node [
    id 127
    label "cz&#281;sty"
  ]
  node [
    id 128
    label "okre&#347;lony"
  ]
  node [
    id 129
    label "zorganizowany"
  ]
  node [
    id 130
    label "powtarzalny"
  ]
  node [
    id 131
    label "regularnie"
  ]
  node [
    id 132
    label "harmonijny"
  ]
  node [
    id 133
    label "zawsze"
  ]
  node [
    id 134
    label "Rzym_Zachodni"
  ]
  node [
    id 135
    label "whole"
  ]
  node [
    id 136
    label "ilo&#347;&#263;"
  ]
  node [
    id 137
    label "element"
  ]
  node [
    id 138
    label "Rzym_Wschodni"
  ]
  node [
    id 139
    label "urz&#261;dzenie"
  ]
  node [
    id 140
    label "r&#243;&#380;niczka"
  ]
  node [
    id 141
    label "&#347;rodowisko"
  ]
  node [
    id 142
    label "przedmiot"
  ]
  node [
    id 143
    label "materia"
  ]
  node [
    id 144
    label "szambo"
  ]
  node [
    id 145
    label "aspo&#322;eczny"
  ]
  node [
    id 146
    label "component"
  ]
  node [
    id 147
    label "szkodnik"
  ]
  node [
    id 148
    label "gangsterski"
  ]
  node [
    id 149
    label "poj&#281;cie"
  ]
  node [
    id 150
    label "underworld"
  ]
  node [
    id 151
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 152
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 153
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 154
    label "rozmiar"
  ]
  node [
    id 155
    label "part"
  ]
  node [
    id 156
    label "kom&#243;rka"
  ]
  node [
    id 157
    label "furnishing"
  ]
  node [
    id 158
    label "zabezpieczenie"
  ]
  node [
    id 159
    label "zrobienie"
  ]
  node [
    id 160
    label "wyrz&#261;dzenie"
  ]
  node [
    id 161
    label "zagospodarowanie"
  ]
  node [
    id 162
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 163
    label "ig&#322;a"
  ]
  node [
    id 164
    label "narz&#281;dzie"
  ]
  node [
    id 165
    label "wirnik"
  ]
  node [
    id 166
    label "aparatura"
  ]
  node [
    id 167
    label "system_energetyczny"
  ]
  node [
    id 168
    label "impulsator"
  ]
  node [
    id 169
    label "mechanizm"
  ]
  node [
    id 170
    label "sprz&#281;t"
  ]
  node [
    id 171
    label "czynno&#347;&#263;"
  ]
  node [
    id 172
    label "blokowanie"
  ]
  node [
    id 173
    label "set"
  ]
  node [
    id 174
    label "zablokowanie"
  ]
  node [
    id 175
    label "przygotowanie"
  ]
  node [
    id 176
    label "komora"
  ]
  node [
    id 177
    label "j&#281;zyk"
  ]
  node [
    id 178
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 179
    label "dworzec"
  ]
  node [
    id 180
    label "stanowisko"
  ]
  node [
    id 181
    label "po&#322;o&#380;enie"
  ]
  node [
    id 182
    label "punkt"
  ]
  node [
    id 183
    label "pogl&#261;d"
  ]
  node [
    id 184
    label "wojsko"
  ]
  node [
    id 185
    label "awansowa&#263;"
  ]
  node [
    id 186
    label "stawia&#263;"
  ]
  node [
    id 187
    label "uprawianie"
  ]
  node [
    id 188
    label "wakowa&#263;"
  ]
  node [
    id 189
    label "powierzanie"
  ]
  node [
    id 190
    label "postawi&#263;"
  ]
  node [
    id 191
    label "miejsce"
  ]
  node [
    id 192
    label "awansowanie"
  ]
  node [
    id 193
    label "praca"
  ]
  node [
    id 194
    label "poczekalnia"
  ]
  node [
    id 195
    label "majdaniarz"
  ]
  node [
    id 196
    label "stacja"
  ]
  node [
    id 197
    label "przechowalnia"
  ]
  node [
    id 198
    label "hala"
  ]
  node [
    id 199
    label "by&#263;"
  ]
  node [
    id 200
    label "gotowy"
  ]
  node [
    id 201
    label "might"
  ]
  node [
    id 202
    label "uprawi&#263;"
  ]
  node [
    id 203
    label "public_treasury"
  ]
  node [
    id 204
    label "pole"
  ]
  node [
    id 205
    label "obrobi&#263;"
  ]
  node [
    id 206
    label "nietrze&#378;wy"
  ]
  node [
    id 207
    label "czekanie"
  ]
  node [
    id 208
    label "martwy"
  ]
  node [
    id 209
    label "bliski"
  ]
  node [
    id 210
    label "gotowo"
  ]
  node [
    id 211
    label "przygotowywanie"
  ]
  node [
    id 212
    label "dyspozycyjny"
  ]
  node [
    id 213
    label "zalany"
  ]
  node [
    id 214
    label "nieuchronny"
  ]
  node [
    id 215
    label "doj&#347;cie"
  ]
  node [
    id 216
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 217
    label "mie&#263;_miejsce"
  ]
  node [
    id 218
    label "equal"
  ]
  node [
    id 219
    label "trwa&#263;"
  ]
  node [
    id 220
    label "chodzi&#263;"
  ]
  node [
    id 221
    label "si&#281;ga&#263;"
  ]
  node [
    id 222
    label "obecno&#347;&#263;"
  ]
  node [
    id 223
    label "stand"
  ]
  node [
    id 224
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 225
    label "uczestniczy&#263;"
  ]
  node [
    id 226
    label "wkroczy&#263;"
  ]
  node [
    id 227
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 228
    label "move"
  ]
  node [
    id 229
    label "spell"
  ]
  node [
    id 230
    label "wsun&#261;&#263;"
  ]
  node [
    id 231
    label "intervene"
  ]
  node [
    id 232
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 233
    label "powiedzie&#263;"
  ]
  node [
    id 234
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 235
    label "wsun&#261;&#263;_si&#281;"
  ]
  node [
    id 236
    label "przekroczy&#263;"
  ]
  node [
    id 237
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 238
    label "wpa&#347;&#263;"
  ]
  node [
    id 239
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 240
    label "skrytykowa&#263;"
  ]
  node [
    id 241
    label "doj&#347;&#263;"
  ]
  node [
    id 242
    label "zacz&#261;&#263;"
  ]
  node [
    id 243
    label "post&#261;pi&#263;"
  ]
  node [
    id 244
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 245
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 246
    label "odj&#261;&#263;"
  ]
  node [
    id 247
    label "zrobi&#263;"
  ]
  node [
    id 248
    label "cause"
  ]
  node [
    id 249
    label "introduce"
  ]
  node [
    id 250
    label "begin"
  ]
  node [
    id 251
    label "do"
  ]
  node [
    id 252
    label "sta&#263;_si&#281;"
  ]
  node [
    id 253
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 254
    label "supervene"
  ]
  node [
    id 255
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 256
    label "zaj&#347;&#263;"
  ]
  node [
    id 257
    label "catch"
  ]
  node [
    id 258
    label "get"
  ]
  node [
    id 259
    label "bodziec"
  ]
  node [
    id 260
    label "informacja"
  ]
  node [
    id 261
    label "przesy&#322;ka"
  ]
  node [
    id 262
    label "dodatek"
  ]
  node [
    id 263
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 264
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 265
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 266
    label "heed"
  ]
  node [
    id 267
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 268
    label "spowodowa&#263;"
  ]
  node [
    id 269
    label "dozna&#263;"
  ]
  node [
    id 270
    label "dokoptowa&#263;"
  ]
  node [
    id 271
    label "postrzega&#263;"
  ]
  node [
    id 272
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 273
    label "orgazm"
  ]
  node [
    id 274
    label "dolecie&#263;"
  ]
  node [
    id 275
    label "drive"
  ]
  node [
    id 276
    label "dotrze&#263;"
  ]
  node [
    id 277
    label "uzyska&#263;"
  ]
  node [
    id 278
    label "dop&#322;ata"
  ]
  node [
    id 279
    label "become"
  ]
  node [
    id 280
    label "profit"
  ]
  node [
    id 281
    label "score"
  ]
  node [
    id 282
    label "make"
  ]
  node [
    id 283
    label "work"
  ]
  node [
    id 284
    label "chemia"
  ]
  node [
    id 285
    label "reakcja_chemiczna"
  ]
  node [
    id 286
    label "act"
  ]
  node [
    id 287
    label "discover"
  ]
  node [
    id 288
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 289
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 290
    label "wydoby&#263;"
  ]
  node [
    id 291
    label "poda&#263;"
  ]
  node [
    id 292
    label "okre&#347;li&#263;"
  ]
  node [
    id 293
    label "express"
  ]
  node [
    id 294
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 295
    label "wyrazi&#263;"
  ]
  node [
    id 296
    label "rzekn&#261;&#263;"
  ]
  node [
    id 297
    label "unwrap"
  ]
  node [
    id 298
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 299
    label "convey"
  ]
  node [
    id 300
    label "zaopiniowa&#263;"
  ]
  node [
    id 301
    label "review"
  ]
  node [
    id 302
    label "oceni&#263;"
  ]
  node [
    id 303
    label "motivate"
  ]
  node [
    id 304
    label "zaj&#261;&#263;"
  ]
  node [
    id 305
    label "induct"
  ]
  node [
    id 306
    label "wej&#347;&#263;"
  ]
  node [
    id 307
    label "poruszy&#263;"
  ]
  node [
    id 308
    label "zje&#347;&#263;"
  ]
  node [
    id 309
    label "umie&#347;ci&#263;"
  ]
  node [
    id 310
    label "insert"
  ]
  node [
    id 311
    label "strike"
  ]
  node [
    id 312
    label "ulec"
  ]
  node [
    id 313
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 314
    label "collapse"
  ]
  node [
    id 315
    label "rzecz"
  ]
  node [
    id 316
    label "d&#378;wi&#281;k"
  ]
  node [
    id 317
    label "fall_upon"
  ]
  node [
    id 318
    label "ponie&#347;&#263;"
  ]
  node [
    id 319
    label "ogrom"
  ]
  node [
    id 320
    label "zapach"
  ]
  node [
    id 321
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 322
    label "uderzy&#263;"
  ]
  node [
    id 323
    label "wymy&#347;li&#263;"
  ]
  node [
    id 324
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 325
    label "wpada&#263;"
  ]
  node [
    id 326
    label "decline"
  ]
  node [
    id 327
    label "&#347;wiat&#322;o"
  ]
  node [
    id 328
    label "fall"
  ]
  node [
    id 329
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 330
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 331
    label "emocja"
  ]
  node [
    id 332
    label "spotka&#263;"
  ]
  node [
    id 333
    label "odwiedzi&#263;"
  ]
  node [
    id 334
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 335
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 336
    label "ograniczenie"
  ]
  node [
    id 337
    label "przeby&#263;"
  ]
  node [
    id 338
    label "open"
  ]
  node [
    id 339
    label "min&#261;&#263;"
  ]
  node [
    id 340
    label "cut"
  ]
  node [
    id 341
    label "pique"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
]
