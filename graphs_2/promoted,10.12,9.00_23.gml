graph [
  node [
    id 0
    label "nadwy&#380;ka"
    origin "text"
  ]
  node [
    id 1
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 2
    label "chiny"
    origin "text"
  ]
  node [
    id 3
    label "indie"
    origin "text"
  ]
  node [
    id 4
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "sytuacja"
    origin "text"
  ]
  node [
    id 6
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 7
    label "tamten"
    origin "text"
  ]
  node [
    id 8
    label "kraj"
    origin "text"
  ]
  node [
    id 9
    label "z_nawi&#261;zk&#261;"
  ]
  node [
    id 10
    label "nadmiar"
  ]
  node [
    id 11
    label "przekarmianie"
  ]
  node [
    id 12
    label "przekarmi&#263;"
  ]
  node [
    id 13
    label "ilo&#347;&#263;"
  ]
  node [
    id 14
    label "przekarmia&#263;"
  ]
  node [
    id 15
    label "surfeit"
  ]
  node [
    id 16
    label "przekarmienie"
  ]
  node [
    id 17
    label "zbywa&#263;"
  ]
  node [
    id 18
    label "doros&#322;y"
  ]
  node [
    id 19
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "ojciec"
  ]
  node [
    id 22
    label "jegomo&#347;&#263;"
  ]
  node [
    id 23
    label "andropauza"
  ]
  node [
    id 24
    label "pa&#324;stwo"
  ]
  node [
    id 25
    label "bratek"
  ]
  node [
    id 26
    label "samiec"
  ]
  node [
    id 27
    label "ch&#322;opina"
  ]
  node [
    id 28
    label "twardziel"
  ]
  node [
    id 29
    label "androlog"
  ]
  node [
    id 30
    label "m&#261;&#380;"
  ]
  node [
    id 31
    label "Katar"
  ]
  node [
    id 32
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 33
    label "Libia"
  ]
  node [
    id 34
    label "Gwatemala"
  ]
  node [
    id 35
    label "Afganistan"
  ]
  node [
    id 36
    label "Ekwador"
  ]
  node [
    id 37
    label "Tad&#380;ykistan"
  ]
  node [
    id 38
    label "Bhutan"
  ]
  node [
    id 39
    label "Argentyna"
  ]
  node [
    id 40
    label "D&#380;ibuti"
  ]
  node [
    id 41
    label "Wenezuela"
  ]
  node [
    id 42
    label "Ukraina"
  ]
  node [
    id 43
    label "Gabon"
  ]
  node [
    id 44
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 45
    label "Rwanda"
  ]
  node [
    id 46
    label "Liechtenstein"
  ]
  node [
    id 47
    label "organizacja"
  ]
  node [
    id 48
    label "Sri_Lanka"
  ]
  node [
    id 49
    label "Madagaskar"
  ]
  node [
    id 50
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 51
    label "Tonga"
  ]
  node [
    id 52
    label "Kongo"
  ]
  node [
    id 53
    label "Bangladesz"
  ]
  node [
    id 54
    label "Kanada"
  ]
  node [
    id 55
    label "Wehrlen"
  ]
  node [
    id 56
    label "Algieria"
  ]
  node [
    id 57
    label "Surinam"
  ]
  node [
    id 58
    label "Chile"
  ]
  node [
    id 59
    label "Sahara_Zachodnia"
  ]
  node [
    id 60
    label "Uganda"
  ]
  node [
    id 61
    label "W&#281;gry"
  ]
  node [
    id 62
    label "Birma"
  ]
  node [
    id 63
    label "Kazachstan"
  ]
  node [
    id 64
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 65
    label "Armenia"
  ]
  node [
    id 66
    label "Tuwalu"
  ]
  node [
    id 67
    label "Timor_Wschodni"
  ]
  node [
    id 68
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 69
    label "Izrael"
  ]
  node [
    id 70
    label "Estonia"
  ]
  node [
    id 71
    label "Komory"
  ]
  node [
    id 72
    label "Kamerun"
  ]
  node [
    id 73
    label "Haiti"
  ]
  node [
    id 74
    label "Belize"
  ]
  node [
    id 75
    label "Sierra_Leone"
  ]
  node [
    id 76
    label "Luksemburg"
  ]
  node [
    id 77
    label "USA"
  ]
  node [
    id 78
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 79
    label "Barbados"
  ]
  node [
    id 80
    label "San_Marino"
  ]
  node [
    id 81
    label "Bu&#322;garia"
  ]
  node [
    id 82
    label "Wietnam"
  ]
  node [
    id 83
    label "Indonezja"
  ]
  node [
    id 84
    label "Malawi"
  ]
  node [
    id 85
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 86
    label "Francja"
  ]
  node [
    id 87
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 88
    label "partia"
  ]
  node [
    id 89
    label "Zambia"
  ]
  node [
    id 90
    label "Angola"
  ]
  node [
    id 91
    label "Grenada"
  ]
  node [
    id 92
    label "Nepal"
  ]
  node [
    id 93
    label "Panama"
  ]
  node [
    id 94
    label "Rumunia"
  ]
  node [
    id 95
    label "Czarnog&#243;ra"
  ]
  node [
    id 96
    label "Malediwy"
  ]
  node [
    id 97
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 98
    label "S&#322;owacja"
  ]
  node [
    id 99
    label "para"
  ]
  node [
    id 100
    label "Egipt"
  ]
  node [
    id 101
    label "zwrot"
  ]
  node [
    id 102
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 103
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 104
    label "Kolumbia"
  ]
  node [
    id 105
    label "Mozambik"
  ]
  node [
    id 106
    label "Laos"
  ]
  node [
    id 107
    label "Burundi"
  ]
  node [
    id 108
    label "Suazi"
  ]
  node [
    id 109
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 110
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 111
    label "Czechy"
  ]
  node [
    id 112
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 113
    label "Wyspy_Marshalla"
  ]
  node [
    id 114
    label "Trynidad_i_Tobago"
  ]
  node [
    id 115
    label "Dominika"
  ]
  node [
    id 116
    label "Palau"
  ]
  node [
    id 117
    label "Syria"
  ]
  node [
    id 118
    label "Gwinea_Bissau"
  ]
  node [
    id 119
    label "Liberia"
  ]
  node [
    id 120
    label "Zimbabwe"
  ]
  node [
    id 121
    label "Polska"
  ]
  node [
    id 122
    label "Jamajka"
  ]
  node [
    id 123
    label "Dominikana"
  ]
  node [
    id 124
    label "Senegal"
  ]
  node [
    id 125
    label "Gruzja"
  ]
  node [
    id 126
    label "Togo"
  ]
  node [
    id 127
    label "Chorwacja"
  ]
  node [
    id 128
    label "Meksyk"
  ]
  node [
    id 129
    label "Macedonia"
  ]
  node [
    id 130
    label "Gujana"
  ]
  node [
    id 131
    label "Zair"
  ]
  node [
    id 132
    label "Albania"
  ]
  node [
    id 133
    label "Kambod&#380;a"
  ]
  node [
    id 134
    label "Mauritius"
  ]
  node [
    id 135
    label "Monako"
  ]
  node [
    id 136
    label "Gwinea"
  ]
  node [
    id 137
    label "Mali"
  ]
  node [
    id 138
    label "Nigeria"
  ]
  node [
    id 139
    label "Kostaryka"
  ]
  node [
    id 140
    label "Hanower"
  ]
  node [
    id 141
    label "Paragwaj"
  ]
  node [
    id 142
    label "W&#322;ochy"
  ]
  node [
    id 143
    label "Wyspy_Salomona"
  ]
  node [
    id 144
    label "Seszele"
  ]
  node [
    id 145
    label "Hiszpania"
  ]
  node [
    id 146
    label "Boliwia"
  ]
  node [
    id 147
    label "Kirgistan"
  ]
  node [
    id 148
    label "Irlandia"
  ]
  node [
    id 149
    label "Czad"
  ]
  node [
    id 150
    label "Irak"
  ]
  node [
    id 151
    label "Lesoto"
  ]
  node [
    id 152
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 153
    label "Malta"
  ]
  node [
    id 154
    label "Andora"
  ]
  node [
    id 155
    label "Chiny"
  ]
  node [
    id 156
    label "Filipiny"
  ]
  node [
    id 157
    label "Antarktis"
  ]
  node [
    id 158
    label "Niemcy"
  ]
  node [
    id 159
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 160
    label "Brazylia"
  ]
  node [
    id 161
    label "terytorium"
  ]
  node [
    id 162
    label "Nikaragua"
  ]
  node [
    id 163
    label "Pakistan"
  ]
  node [
    id 164
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 165
    label "Kenia"
  ]
  node [
    id 166
    label "Niger"
  ]
  node [
    id 167
    label "Tunezja"
  ]
  node [
    id 168
    label "Portugalia"
  ]
  node [
    id 169
    label "Fid&#380;i"
  ]
  node [
    id 170
    label "Maroko"
  ]
  node [
    id 171
    label "Botswana"
  ]
  node [
    id 172
    label "Tajlandia"
  ]
  node [
    id 173
    label "Australia"
  ]
  node [
    id 174
    label "Burkina_Faso"
  ]
  node [
    id 175
    label "interior"
  ]
  node [
    id 176
    label "Benin"
  ]
  node [
    id 177
    label "Tanzania"
  ]
  node [
    id 178
    label "Indie"
  ]
  node [
    id 179
    label "&#321;otwa"
  ]
  node [
    id 180
    label "Kiribati"
  ]
  node [
    id 181
    label "Antigua_i_Barbuda"
  ]
  node [
    id 182
    label "Rodezja"
  ]
  node [
    id 183
    label "Cypr"
  ]
  node [
    id 184
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 185
    label "Peru"
  ]
  node [
    id 186
    label "Austria"
  ]
  node [
    id 187
    label "Urugwaj"
  ]
  node [
    id 188
    label "Jordania"
  ]
  node [
    id 189
    label "Grecja"
  ]
  node [
    id 190
    label "Azerbejd&#380;an"
  ]
  node [
    id 191
    label "Turcja"
  ]
  node [
    id 192
    label "Samoa"
  ]
  node [
    id 193
    label "Sudan"
  ]
  node [
    id 194
    label "Oman"
  ]
  node [
    id 195
    label "ziemia"
  ]
  node [
    id 196
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 197
    label "Uzbekistan"
  ]
  node [
    id 198
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 199
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 200
    label "Honduras"
  ]
  node [
    id 201
    label "Mongolia"
  ]
  node [
    id 202
    label "Portoryko"
  ]
  node [
    id 203
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 204
    label "Serbia"
  ]
  node [
    id 205
    label "Tajwan"
  ]
  node [
    id 206
    label "Wielka_Brytania"
  ]
  node [
    id 207
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 208
    label "Liban"
  ]
  node [
    id 209
    label "Japonia"
  ]
  node [
    id 210
    label "Ghana"
  ]
  node [
    id 211
    label "Bahrajn"
  ]
  node [
    id 212
    label "Belgia"
  ]
  node [
    id 213
    label "Etiopia"
  ]
  node [
    id 214
    label "Mikronezja"
  ]
  node [
    id 215
    label "Kuwejt"
  ]
  node [
    id 216
    label "grupa"
  ]
  node [
    id 217
    label "Bahamy"
  ]
  node [
    id 218
    label "Rosja"
  ]
  node [
    id 219
    label "Mo&#322;dawia"
  ]
  node [
    id 220
    label "Litwa"
  ]
  node [
    id 221
    label "S&#322;owenia"
  ]
  node [
    id 222
    label "Szwajcaria"
  ]
  node [
    id 223
    label "Erytrea"
  ]
  node [
    id 224
    label "Kuba"
  ]
  node [
    id 225
    label "Arabia_Saudyjska"
  ]
  node [
    id 226
    label "granica_pa&#324;stwa"
  ]
  node [
    id 227
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 228
    label "Malezja"
  ]
  node [
    id 229
    label "Korea"
  ]
  node [
    id 230
    label "Jemen"
  ]
  node [
    id 231
    label "Nowa_Zelandia"
  ]
  node [
    id 232
    label "Namibia"
  ]
  node [
    id 233
    label "Nauru"
  ]
  node [
    id 234
    label "holoarktyka"
  ]
  node [
    id 235
    label "Brunei"
  ]
  node [
    id 236
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 237
    label "Khitai"
  ]
  node [
    id 238
    label "Mauretania"
  ]
  node [
    id 239
    label "Iran"
  ]
  node [
    id 240
    label "Gambia"
  ]
  node [
    id 241
    label "Somalia"
  ]
  node [
    id 242
    label "Holandia"
  ]
  node [
    id 243
    label "Turkmenistan"
  ]
  node [
    id 244
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 245
    label "Salwador"
  ]
  node [
    id 246
    label "wydoro&#347;lenie"
  ]
  node [
    id 247
    label "du&#380;y"
  ]
  node [
    id 248
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 249
    label "doro&#347;lenie"
  ]
  node [
    id 250
    label "&#378;ra&#322;y"
  ]
  node [
    id 251
    label "doro&#347;le"
  ]
  node [
    id 252
    label "senior"
  ]
  node [
    id 253
    label "dojrzale"
  ]
  node [
    id 254
    label "wapniak"
  ]
  node [
    id 255
    label "dojrza&#322;y"
  ]
  node [
    id 256
    label "m&#261;dry"
  ]
  node [
    id 257
    label "doletni"
  ]
  node [
    id 258
    label "ludzko&#347;&#263;"
  ]
  node [
    id 259
    label "asymilowanie"
  ]
  node [
    id 260
    label "asymilowa&#263;"
  ]
  node [
    id 261
    label "os&#322;abia&#263;"
  ]
  node [
    id 262
    label "posta&#263;"
  ]
  node [
    id 263
    label "hominid"
  ]
  node [
    id 264
    label "podw&#322;adny"
  ]
  node [
    id 265
    label "os&#322;abianie"
  ]
  node [
    id 266
    label "g&#322;owa"
  ]
  node [
    id 267
    label "figura"
  ]
  node [
    id 268
    label "portrecista"
  ]
  node [
    id 269
    label "dwun&#243;g"
  ]
  node [
    id 270
    label "profanum"
  ]
  node [
    id 271
    label "mikrokosmos"
  ]
  node [
    id 272
    label "nasada"
  ]
  node [
    id 273
    label "duch"
  ]
  node [
    id 274
    label "antropochoria"
  ]
  node [
    id 275
    label "osoba"
  ]
  node [
    id 276
    label "wz&#243;r"
  ]
  node [
    id 277
    label "oddzia&#322;ywanie"
  ]
  node [
    id 278
    label "Adam"
  ]
  node [
    id 279
    label "homo_sapiens"
  ]
  node [
    id 280
    label "polifag"
  ]
  node [
    id 281
    label "pami&#281;&#263;"
  ]
  node [
    id 282
    label "powierzchnia_zr&#243;wnania"
  ]
  node [
    id 283
    label "zapalenie"
  ]
  node [
    id 284
    label "drewno_wt&#243;rne"
  ]
  node [
    id 285
    label "heartwood"
  ]
  node [
    id 286
    label "monolit"
  ]
  node [
    id 287
    label "formacja_skalna"
  ]
  node [
    id 288
    label "klaster_dyskowy"
  ]
  node [
    id 289
    label "komputer"
  ]
  node [
    id 290
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 291
    label "hard_disc"
  ]
  node [
    id 292
    label "&#347;mia&#322;ek"
  ]
  node [
    id 293
    label "kszta&#322;ciciel"
  ]
  node [
    id 294
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 295
    label "kuwada"
  ]
  node [
    id 296
    label "tworzyciel"
  ]
  node [
    id 297
    label "rodzice"
  ]
  node [
    id 298
    label "&#347;w"
  ]
  node [
    id 299
    label "pomys&#322;odawca"
  ]
  node [
    id 300
    label "rodzic"
  ]
  node [
    id 301
    label "wykonawca"
  ]
  node [
    id 302
    label "ojczym"
  ]
  node [
    id 303
    label "przodek"
  ]
  node [
    id 304
    label "papa"
  ]
  node [
    id 305
    label "zakonnik"
  ]
  node [
    id 306
    label "stary"
  ]
  node [
    id 307
    label "kochanek"
  ]
  node [
    id 308
    label "fio&#322;ek"
  ]
  node [
    id 309
    label "facet"
  ]
  node [
    id 310
    label "brat"
  ]
  node [
    id 311
    label "zwierz&#281;"
  ]
  node [
    id 312
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 313
    label "ma&#322;&#380;onek"
  ]
  node [
    id 314
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 315
    label "m&#243;j"
  ]
  node [
    id 316
    label "ch&#322;op"
  ]
  node [
    id 317
    label "pan_m&#322;ody"
  ]
  node [
    id 318
    label "&#347;lubny"
  ]
  node [
    id 319
    label "pan_domu"
  ]
  node [
    id 320
    label "pan_i_w&#322;adca"
  ]
  node [
    id 321
    label "mo&#347;&#263;"
  ]
  node [
    id 322
    label "specjalista"
  ]
  node [
    id 323
    label "&#380;ycie"
  ]
  node [
    id 324
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 325
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 326
    label "pour"
  ]
  node [
    id 327
    label "zasila&#263;"
  ]
  node [
    id 328
    label "determine"
  ]
  node [
    id 329
    label "ciek_wodny"
  ]
  node [
    id 330
    label "kapita&#322;"
  ]
  node [
    id 331
    label "work"
  ]
  node [
    id 332
    label "dochodzi&#263;"
  ]
  node [
    id 333
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 334
    label "flow"
  ]
  node [
    id 335
    label "nail"
  ]
  node [
    id 336
    label "statek"
  ]
  node [
    id 337
    label "dociera&#263;"
  ]
  node [
    id 338
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 339
    label "przybywa&#263;"
  ]
  node [
    id 340
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 341
    label "robi&#263;"
  ]
  node [
    id 342
    label "uzyskiwa&#263;"
  ]
  node [
    id 343
    label "claim"
  ]
  node [
    id 344
    label "osi&#261;ga&#263;"
  ]
  node [
    id 345
    label "ripen"
  ]
  node [
    id 346
    label "supervene"
  ]
  node [
    id 347
    label "doczeka&#263;"
  ]
  node [
    id 348
    label "przesy&#322;ka"
  ]
  node [
    id 349
    label "doznawa&#263;"
  ]
  node [
    id 350
    label "reach"
  ]
  node [
    id 351
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 352
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 353
    label "zachodzi&#263;"
  ]
  node [
    id 354
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 355
    label "postrzega&#263;"
  ]
  node [
    id 356
    label "orgazm"
  ]
  node [
    id 357
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 358
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 359
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 360
    label "dokoptowywa&#263;"
  ]
  node [
    id 361
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 362
    label "dolatywa&#263;"
  ]
  node [
    id 363
    label "powodowa&#263;"
  ]
  node [
    id 364
    label "submit"
  ]
  node [
    id 365
    label "dostarcza&#263;"
  ]
  node [
    id 366
    label "digest"
  ]
  node [
    id 367
    label "absolutorium"
  ]
  node [
    id 368
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 369
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 370
    label "&#347;rodowisko"
  ]
  node [
    id 371
    label "nap&#322;ywanie"
  ]
  node [
    id 372
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 373
    label "zaleta"
  ]
  node [
    id 374
    label "mienie"
  ]
  node [
    id 375
    label "podupada&#263;"
  ]
  node [
    id 376
    label "podupadanie"
  ]
  node [
    id 377
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 378
    label "kwestor"
  ]
  node [
    id 379
    label "zas&#243;b"
  ]
  node [
    id 380
    label "supernadz&#243;r"
  ]
  node [
    id 381
    label "uruchomienie"
  ]
  node [
    id 382
    label "uruchamia&#263;"
  ]
  node [
    id 383
    label "kapitalista"
  ]
  node [
    id 384
    label "uruchamianie"
  ]
  node [
    id 385
    label "czynnik_produkcji"
  ]
  node [
    id 386
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 387
    label "warunki"
  ]
  node [
    id 388
    label "szczeg&#243;&#322;"
  ]
  node [
    id 389
    label "state"
  ]
  node [
    id 390
    label "motyw"
  ]
  node [
    id 391
    label "realia"
  ]
  node [
    id 392
    label "sk&#322;adnik"
  ]
  node [
    id 393
    label "wydarzenie"
  ]
  node [
    id 394
    label "status"
  ]
  node [
    id 395
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 396
    label "niuansowa&#263;"
  ]
  node [
    id 397
    label "element"
  ]
  node [
    id 398
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 399
    label "zniuansowa&#263;"
  ]
  node [
    id 400
    label "fraza"
  ]
  node [
    id 401
    label "temat"
  ]
  node [
    id 402
    label "melodia"
  ]
  node [
    id 403
    label "cecha"
  ]
  node [
    id 404
    label "przyczyna"
  ]
  node [
    id 405
    label "ozdoba"
  ]
  node [
    id 406
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 407
    label "message"
  ]
  node [
    id 408
    label "kontekst"
  ]
  node [
    id 409
    label "spo&#322;ecznie"
  ]
  node [
    id 410
    label "publiczny"
  ]
  node [
    id 411
    label "niepubliczny"
  ]
  node [
    id 412
    label "publicznie"
  ]
  node [
    id 413
    label "upublicznianie"
  ]
  node [
    id 414
    label "jawny"
  ]
  node [
    id 415
    label "upublicznienie"
  ]
  node [
    id 416
    label "Mazowsze"
  ]
  node [
    id 417
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 418
    label "Anglia"
  ]
  node [
    id 419
    label "Amazonia"
  ]
  node [
    id 420
    label "Bordeaux"
  ]
  node [
    id 421
    label "Naddniestrze"
  ]
  node [
    id 422
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 423
    label "Europa_Zachodnia"
  ]
  node [
    id 424
    label "Armagnac"
  ]
  node [
    id 425
    label "Amhara"
  ]
  node [
    id 426
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 427
    label "Zamojszczyzna"
  ]
  node [
    id 428
    label "Turkiestan"
  ]
  node [
    id 429
    label "Ma&#322;opolska"
  ]
  node [
    id 430
    label "Noworosja"
  ]
  node [
    id 431
    label "Lubelszczyzna"
  ]
  node [
    id 432
    label "Mezoameryka"
  ]
  node [
    id 433
    label "Ba&#322;kany"
  ]
  node [
    id 434
    label "Kurdystan"
  ]
  node [
    id 435
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 436
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 437
    label "Baszkiria"
  ]
  node [
    id 438
    label "Szkocja"
  ]
  node [
    id 439
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 440
    label "Tonkin"
  ]
  node [
    id 441
    label "Maghreb"
  ]
  node [
    id 442
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 443
    label "Nadrenia"
  ]
  node [
    id 444
    label "Podhale"
  ]
  node [
    id 445
    label "Wielkopolska"
  ]
  node [
    id 446
    label "Zabajkale"
  ]
  node [
    id 447
    label "Apulia"
  ]
  node [
    id 448
    label "brzeg"
  ]
  node [
    id 449
    label "Bojkowszczyzna"
  ]
  node [
    id 450
    label "Kujawy"
  ]
  node [
    id 451
    label "Liguria"
  ]
  node [
    id 452
    label "Pamir"
  ]
  node [
    id 453
    label "Indochiny"
  ]
  node [
    id 454
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 455
    label "Polinezja"
  ]
  node [
    id 456
    label "Kurpie"
  ]
  node [
    id 457
    label "Podlasie"
  ]
  node [
    id 458
    label "S&#261;decczyzna"
  ]
  node [
    id 459
    label "Umbria"
  ]
  node [
    id 460
    label "Karaiby"
  ]
  node [
    id 461
    label "Ukraina_Zachodnia"
  ]
  node [
    id 462
    label "Kielecczyzna"
  ]
  node [
    id 463
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 464
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 465
    label "Kalabria"
  ]
  node [
    id 466
    label "Skandynawia"
  ]
  node [
    id 467
    label "Bory_Tucholskie"
  ]
  node [
    id 468
    label "Huculszczyzna"
  ]
  node [
    id 469
    label "Tyrol"
  ]
  node [
    id 470
    label "Turyngia"
  ]
  node [
    id 471
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 472
    label "jednostka_administracyjna"
  ]
  node [
    id 473
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 474
    label "Hercegowina"
  ]
  node [
    id 475
    label "Lotaryngia"
  ]
  node [
    id 476
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 477
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 478
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 479
    label "Walia"
  ]
  node [
    id 480
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 481
    label "Opolskie"
  ]
  node [
    id 482
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 483
    label "Kampania"
  ]
  node [
    id 484
    label "Sand&#380;ak"
  ]
  node [
    id 485
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 486
    label "Syjon"
  ]
  node [
    id 487
    label "Kabylia"
  ]
  node [
    id 488
    label "Lombardia"
  ]
  node [
    id 489
    label "Warmia"
  ]
  node [
    id 490
    label "Kaszmir"
  ]
  node [
    id 491
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 492
    label "&#321;&#243;dzkie"
  ]
  node [
    id 493
    label "Kaukaz"
  ]
  node [
    id 494
    label "Europa_Wschodnia"
  ]
  node [
    id 495
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 496
    label "Biskupizna"
  ]
  node [
    id 497
    label "Afryka_Wschodnia"
  ]
  node [
    id 498
    label "Podkarpacie"
  ]
  node [
    id 499
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 500
    label "obszar"
  ]
  node [
    id 501
    label "Afryka_Zachodnia"
  ]
  node [
    id 502
    label "Toskania"
  ]
  node [
    id 503
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 504
    label "Podbeskidzie"
  ]
  node [
    id 505
    label "Bo&#347;nia"
  ]
  node [
    id 506
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 507
    label "Oceania"
  ]
  node [
    id 508
    label "Pomorze_Zachodnie"
  ]
  node [
    id 509
    label "Powi&#347;le"
  ]
  node [
    id 510
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 511
    label "&#321;emkowszczyzna"
  ]
  node [
    id 512
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 513
    label "Kaszuby"
  ]
  node [
    id 514
    label "Ko&#322;yma"
  ]
  node [
    id 515
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 516
    label "Szlezwik"
  ]
  node [
    id 517
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 518
    label "Polesie"
  ]
  node [
    id 519
    label "Kerala"
  ]
  node [
    id 520
    label "Mazury"
  ]
  node [
    id 521
    label "Palestyna"
  ]
  node [
    id 522
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 523
    label "Lauda"
  ]
  node [
    id 524
    label "Azja_Wschodnia"
  ]
  node [
    id 525
    label "Zakarpacie"
  ]
  node [
    id 526
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 527
    label "Galicja"
  ]
  node [
    id 528
    label "Lubuskie"
  ]
  node [
    id 529
    label "Laponia"
  ]
  node [
    id 530
    label "Yorkshire"
  ]
  node [
    id 531
    label "Bawaria"
  ]
  node [
    id 532
    label "Zag&#243;rze"
  ]
  node [
    id 533
    label "Andaluzja"
  ]
  node [
    id 534
    label "&#379;ywiecczyzna"
  ]
  node [
    id 535
    label "Oksytania"
  ]
  node [
    id 536
    label "Opolszczyzna"
  ]
  node [
    id 537
    label "Kociewie"
  ]
  node [
    id 538
    label "Lasko"
  ]
  node [
    id 539
    label "woda"
  ]
  node [
    id 540
    label "linia"
  ]
  node [
    id 541
    label "zbi&#243;r"
  ]
  node [
    id 542
    label "ekoton"
  ]
  node [
    id 543
    label "str&#261;d"
  ]
  node [
    id 544
    label "koniec"
  ]
  node [
    id 545
    label "plantowa&#263;"
  ]
  node [
    id 546
    label "zapadnia"
  ]
  node [
    id 547
    label "budynek"
  ]
  node [
    id 548
    label "skorupa_ziemska"
  ]
  node [
    id 549
    label "glinowanie"
  ]
  node [
    id 550
    label "martwica"
  ]
  node [
    id 551
    label "teren"
  ]
  node [
    id 552
    label "litosfera"
  ]
  node [
    id 553
    label "penetrator"
  ]
  node [
    id 554
    label "glinowa&#263;"
  ]
  node [
    id 555
    label "domain"
  ]
  node [
    id 556
    label "podglebie"
  ]
  node [
    id 557
    label "kompleks_sorpcyjny"
  ]
  node [
    id 558
    label "miejsce"
  ]
  node [
    id 559
    label "kort"
  ]
  node [
    id 560
    label "pojazd"
  ]
  node [
    id 561
    label "powierzchnia"
  ]
  node [
    id 562
    label "pr&#243;chnica"
  ]
  node [
    id 563
    label "pomieszczenie"
  ]
  node [
    id 564
    label "ryzosfera"
  ]
  node [
    id 565
    label "p&#322;aszczyzna"
  ]
  node [
    id 566
    label "dotleni&#263;"
  ]
  node [
    id 567
    label "glej"
  ]
  node [
    id 568
    label "posadzka"
  ]
  node [
    id 569
    label "geosystem"
  ]
  node [
    id 570
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 571
    label "przestrze&#324;"
  ]
  node [
    id 572
    label "podmiot"
  ]
  node [
    id 573
    label "jednostka_organizacyjna"
  ]
  node [
    id 574
    label "struktura"
  ]
  node [
    id 575
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 576
    label "TOPR"
  ]
  node [
    id 577
    label "endecki"
  ]
  node [
    id 578
    label "zesp&#243;&#322;"
  ]
  node [
    id 579
    label "od&#322;am"
  ]
  node [
    id 580
    label "przedstawicielstwo"
  ]
  node [
    id 581
    label "Cepelia"
  ]
  node [
    id 582
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 583
    label "ZBoWiD"
  ]
  node [
    id 584
    label "organization"
  ]
  node [
    id 585
    label "centrala"
  ]
  node [
    id 586
    label "GOPR"
  ]
  node [
    id 587
    label "ZOMO"
  ]
  node [
    id 588
    label "ZMP"
  ]
  node [
    id 589
    label "komitet_koordynacyjny"
  ]
  node [
    id 590
    label "przybud&#243;wka"
  ]
  node [
    id 591
    label "boj&#243;wka"
  ]
  node [
    id 592
    label "p&#243;&#322;noc"
  ]
  node [
    id 593
    label "Kosowo"
  ]
  node [
    id 594
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 595
    label "Zab&#322;ocie"
  ]
  node [
    id 596
    label "zach&#243;d"
  ]
  node [
    id 597
    label "po&#322;udnie"
  ]
  node [
    id 598
    label "Pow&#261;zki"
  ]
  node [
    id 599
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 600
    label "Piotrowo"
  ]
  node [
    id 601
    label "Olszanica"
  ]
  node [
    id 602
    label "Ruda_Pabianicka"
  ]
  node [
    id 603
    label "holarktyka"
  ]
  node [
    id 604
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 605
    label "Ludwin&#243;w"
  ]
  node [
    id 606
    label "Arktyka"
  ]
  node [
    id 607
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 608
    label "Zabu&#380;e"
  ]
  node [
    id 609
    label "antroposfera"
  ]
  node [
    id 610
    label "Neogea"
  ]
  node [
    id 611
    label "Syberia_Zachodnia"
  ]
  node [
    id 612
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 613
    label "zakres"
  ]
  node [
    id 614
    label "pas_planetoid"
  ]
  node [
    id 615
    label "Syberia_Wschodnia"
  ]
  node [
    id 616
    label "Antarktyka"
  ]
  node [
    id 617
    label "Rakowice"
  ]
  node [
    id 618
    label "akrecja"
  ]
  node [
    id 619
    label "wymiar"
  ]
  node [
    id 620
    label "&#321;&#281;g"
  ]
  node [
    id 621
    label "Kresy_Zachodnie"
  ]
  node [
    id 622
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 623
    label "wsch&#243;d"
  ]
  node [
    id 624
    label "Notogea"
  ]
  node [
    id 625
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 626
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 627
    label "Pend&#380;ab"
  ]
  node [
    id 628
    label "funt_liba&#324;ski"
  ]
  node [
    id 629
    label "strefa_euro"
  ]
  node [
    id 630
    label "Pozna&#324;"
  ]
  node [
    id 631
    label "lira_malta&#324;ska"
  ]
  node [
    id 632
    label "Gozo"
  ]
  node [
    id 633
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 634
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 635
    label "dolar_namibijski"
  ]
  node [
    id 636
    label "milrejs"
  ]
  node [
    id 637
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 638
    label "NATO"
  ]
  node [
    id 639
    label "escudo_portugalskie"
  ]
  node [
    id 640
    label "dolar_bahamski"
  ]
  node [
    id 641
    label "Wielka_Bahama"
  ]
  node [
    id 642
    label "dolar_liberyjski"
  ]
  node [
    id 643
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 644
    label "riel"
  ]
  node [
    id 645
    label "Karelia"
  ]
  node [
    id 646
    label "Mari_El"
  ]
  node [
    id 647
    label "Inguszetia"
  ]
  node [
    id 648
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 649
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 650
    label "Udmurcja"
  ]
  node [
    id 651
    label "Newa"
  ]
  node [
    id 652
    label "&#321;adoga"
  ]
  node [
    id 653
    label "Czeczenia"
  ]
  node [
    id 654
    label "Anadyr"
  ]
  node [
    id 655
    label "Syberia"
  ]
  node [
    id 656
    label "Tatarstan"
  ]
  node [
    id 657
    label "Wszechrosja"
  ]
  node [
    id 658
    label "Azja"
  ]
  node [
    id 659
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 660
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 661
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 662
    label "Witim"
  ]
  node [
    id 663
    label "Kamczatka"
  ]
  node [
    id 664
    label "Jama&#322;"
  ]
  node [
    id 665
    label "Dagestan"
  ]
  node [
    id 666
    label "Tuwa"
  ]
  node [
    id 667
    label "car"
  ]
  node [
    id 668
    label "Komi"
  ]
  node [
    id 669
    label "Czuwaszja"
  ]
  node [
    id 670
    label "Chakasja"
  ]
  node [
    id 671
    label "Perm"
  ]
  node [
    id 672
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 673
    label "Ajon"
  ]
  node [
    id 674
    label "Adygeja"
  ]
  node [
    id 675
    label "Dniepr"
  ]
  node [
    id 676
    label "rubel_rosyjski"
  ]
  node [
    id 677
    label "Don"
  ]
  node [
    id 678
    label "Mordowia"
  ]
  node [
    id 679
    label "s&#322;owianofilstwo"
  ]
  node [
    id 680
    label "lew"
  ]
  node [
    id 681
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 682
    label "Dobrud&#380;a"
  ]
  node [
    id 683
    label "Unia_Europejska"
  ]
  node [
    id 684
    label "lira_izraelska"
  ]
  node [
    id 685
    label "szekel"
  ]
  node [
    id 686
    label "Galilea"
  ]
  node [
    id 687
    label "Judea"
  ]
  node [
    id 688
    label "Luksemburgia"
  ]
  node [
    id 689
    label "frank_belgijski"
  ]
  node [
    id 690
    label "Limburgia"
  ]
  node [
    id 691
    label "Walonia"
  ]
  node [
    id 692
    label "Brabancja"
  ]
  node [
    id 693
    label "Flandria"
  ]
  node [
    id 694
    label "Niderlandy"
  ]
  node [
    id 695
    label "dinar_iracki"
  ]
  node [
    id 696
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 697
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 698
    label "szyling_ugandyjski"
  ]
  node [
    id 699
    label "dolar_jamajski"
  ]
  node [
    id 700
    label "kafar"
  ]
  node [
    id 701
    label "ringgit"
  ]
  node [
    id 702
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 703
    label "Borneo"
  ]
  node [
    id 704
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 705
    label "dolar_surinamski"
  ]
  node [
    id 706
    label "funt_suda&#324;ski"
  ]
  node [
    id 707
    label "dolar_guja&#324;ski"
  ]
  node [
    id 708
    label "Manica"
  ]
  node [
    id 709
    label "escudo_mozambickie"
  ]
  node [
    id 710
    label "Cabo_Delgado"
  ]
  node [
    id 711
    label "Inhambane"
  ]
  node [
    id 712
    label "Maputo"
  ]
  node [
    id 713
    label "Gaza"
  ]
  node [
    id 714
    label "Niasa"
  ]
  node [
    id 715
    label "Nampula"
  ]
  node [
    id 716
    label "metical"
  ]
  node [
    id 717
    label "Sahara"
  ]
  node [
    id 718
    label "inti"
  ]
  node [
    id 719
    label "sol"
  ]
  node [
    id 720
    label "kip"
  ]
  node [
    id 721
    label "Pireneje"
  ]
  node [
    id 722
    label "euro"
  ]
  node [
    id 723
    label "kwacha_zambijska"
  ]
  node [
    id 724
    label "tugrik"
  ]
  node [
    id 725
    label "Buriaci"
  ]
  node [
    id 726
    label "ajmak"
  ]
  node [
    id 727
    label "balboa"
  ]
  node [
    id 728
    label "Ameryka_Centralna"
  ]
  node [
    id 729
    label "dolar"
  ]
  node [
    id 730
    label "gulden"
  ]
  node [
    id 731
    label "Zelandia"
  ]
  node [
    id 732
    label "manat_turkme&#324;ski"
  ]
  node [
    id 733
    label "dolar_Tuvalu"
  ]
  node [
    id 734
    label "zair"
  ]
  node [
    id 735
    label "Katanga"
  ]
  node [
    id 736
    label "frank_szwajcarski"
  ]
  node [
    id 737
    label "Jukatan"
  ]
  node [
    id 738
    label "dolar_Belize"
  ]
  node [
    id 739
    label "colon"
  ]
  node [
    id 740
    label "Dyja"
  ]
  node [
    id 741
    label "korona_czeska"
  ]
  node [
    id 742
    label "Izera"
  ]
  node [
    id 743
    label "ugija"
  ]
  node [
    id 744
    label "szyling_kenijski"
  ]
  node [
    id 745
    label "Nachiczewan"
  ]
  node [
    id 746
    label "manat_azerski"
  ]
  node [
    id 747
    label "Karabach"
  ]
  node [
    id 748
    label "Bengal"
  ]
  node [
    id 749
    label "taka"
  ]
  node [
    id 750
    label "Ocean_Spokojny"
  ]
  node [
    id 751
    label "dolar_Kiribati"
  ]
  node [
    id 752
    label "peso_filipi&#324;skie"
  ]
  node [
    id 753
    label "Cebu"
  ]
  node [
    id 754
    label "Atlantyk"
  ]
  node [
    id 755
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 756
    label "Ulster"
  ]
  node [
    id 757
    label "funt_irlandzki"
  ]
  node [
    id 758
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 759
    label "cedi"
  ]
  node [
    id 760
    label "ariary"
  ]
  node [
    id 761
    label "Ocean_Indyjski"
  ]
  node [
    id 762
    label "frank_malgaski"
  ]
  node [
    id 763
    label "Estremadura"
  ]
  node [
    id 764
    label "Kastylia"
  ]
  node [
    id 765
    label "Rzym_Zachodni"
  ]
  node [
    id 766
    label "Aragonia"
  ]
  node [
    id 767
    label "hacjender"
  ]
  node [
    id 768
    label "Asturia"
  ]
  node [
    id 769
    label "Baskonia"
  ]
  node [
    id 770
    label "Majorka"
  ]
  node [
    id 771
    label "Walencja"
  ]
  node [
    id 772
    label "peseta"
  ]
  node [
    id 773
    label "Katalonia"
  ]
  node [
    id 774
    label "peso_chilijskie"
  ]
  node [
    id 775
    label "Indie_Zachodnie"
  ]
  node [
    id 776
    label "Sikkim"
  ]
  node [
    id 777
    label "Asam"
  ]
  node [
    id 778
    label "rupia_indyjska"
  ]
  node [
    id 779
    label "Indie_Portugalskie"
  ]
  node [
    id 780
    label "Indie_Wschodnie"
  ]
  node [
    id 781
    label "Bollywood"
  ]
  node [
    id 782
    label "jen"
  ]
  node [
    id 783
    label "jinja"
  ]
  node [
    id 784
    label "Okinawa"
  ]
  node [
    id 785
    label "Japonica"
  ]
  node [
    id 786
    label "Rugia"
  ]
  node [
    id 787
    label "Saksonia"
  ]
  node [
    id 788
    label "Dolna_Saksonia"
  ]
  node [
    id 789
    label "Anglosas"
  ]
  node [
    id 790
    label "Hesja"
  ]
  node [
    id 791
    label "Wirtembergia"
  ]
  node [
    id 792
    label "Po&#322;abie"
  ]
  node [
    id 793
    label "Germania"
  ]
  node [
    id 794
    label "Frankonia"
  ]
  node [
    id 795
    label "Badenia"
  ]
  node [
    id 796
    label "Holsztyn"
  ]
  node [
    id 797
    label "marka"
  ]
  node [
    id 798
    label "Brandenburgia"
  ]
  node [
    id 799
    label "Szwabia"
  ]
  node [
    id 800
    label "Niemcy_Zachodnie"
  ]
  node [
    id 801
    label "Westfalia"
  ]
  node [
    id 802
    label "Helgoland"
  ]
  node [
    id 803
    label "Karlsbad"
  ]
  node [
    id 804
    label "Niemcy_Wschodnie"
  ]
  node [
    id 805
    label "Piemont"
  ]
  node [
    id 806
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 807
    label "Sardynia"
  ]
  node [
    id 808
    label "Italia"
  ]
  node [
    id 809
    label "Ok&#281;cie"
  ]
  node [
    id 810
    label "Karyntia"
  ]
  node [
    id 811
    label "Romania"
  ]
  node [
    id 812
    label "Warszawa"
  ]
  node [
    id 813
    label "lir"
  ]
  node [
    id 814
    label "Sycylia"
  ]
  node [
    id 815
    label "Dacja"
  ]
  node [
    id 816
    label "lej_rumu&#324;ski"
  ]
  node [
    id 817
    label "Siedmiogr&#243;d"
  ]
  node [
    id 818
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 819
    label "funt_syryjski"
  ]
  node [
    id 820
    label "alawizm"
  ]
  node [
    id 821
    label "frank_rwandyjski"
  ]
  node [
    id 822
    label "dinar_Bahrajnu"
  ]
  node [
    id 823
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 824
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 825
    label "frank_luksemburski"
  ]
  node [
    id 826
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 827
    label "peso_kuba&#324;skie"
  ]
  node [
    id 828
    label "frank_monakijski"
  ]
  node [
    id 829
    label "dinar_algierski"
  ]
  node [
    id 830
    label "Wojwodina"
  ]
  node [
    id 831
    label "dinar_serbski"
  ]
  node [
    id 832
    label "Orinoko"
  ]
  node [
    id 833
    label "boliwar"
  ]
  node [
    id 834
    label "tenge"
  ]
  node [
    id 835
    label "frank_alba&#324;ski"
  ]
  node [
    id 836
    label "lek"
  ]
  node [
    id 837
    label "dolar_Barbadosu"
  ]
  node [
    id 838
    label "Antyle"
  ]
  node [
    id 839
    label "kyat"
  ]
  node [
    id 840
    label "Arakan"
  ]
  node [
    id 841
    label "c&#243;rdoba"
  ]
  node [
    id 842
    label "Paros"
  ]
  node [
    id 843
    label "Epir"
  ]
  node [
    id 844
    label "panhellenizm"
  ]
  node [
    id 845
    label "Eubea"
  ]
  node [
    id 846
    label "Rodos"
  ]
  node [
    id 847
    label "Achaja"
  ]
  node [
    id 848
    label "Termopile"
  ]
  node [
    id 849
    label "Attyka"
  ]
  node [
    id 850
    label "Hellada"
  ]
  node [
    id 851
    label "Etolia"
  ]
  node [
    id 852
    label "palestra"
  ]
  node [
    id 853
    label "Kreta"
  ]
  node [
    id 854
    label "drachma"
  ]
  node [
    id 855
    label "Olimp"
  ]
  node [
    id 856
    label "Tesalia"
  ]
  node [
    id 857
    label "Peloponez"
  ]
  node [
    id 858
    label "Eolia"
  ]
  node [
    id 859
    label "Beocja"
  ]
  node [
    id 860
    label "Parnas"
  ]
  node [
    id 861
    label "Lesbos"
  ]
  node [
    id 862
    label "Mariany"
  ]
  node [
    id 863
    label "Salzburg"
  ]
  node [
    id 864
    label "Rakuzy"
  ]
  node [
    id 865
    label "konsulent"
  ]
  node [
    id 866
    label "szyling_austryjacki"
  ]
  node [
    id 867
    label "birr"
  ]
  node [
    id 868
    label "negus"
  ]
  node [
    id 869
    label "Jawa"
  ]
  node [
    id 870
    label "Sumatra"
  ]
  node [
    id 871
    label "rupia_indonezyjska"
  ]
  node [
    id 872
    label "Nowa_Gwinea"
  ]
  node [
    id 873
    label "Moluki"
  ]
  node [
    id 874
    label "boliviano"
  ]
  node [
    id 875
    label "Pikardia"
  ]
  node [
    id 876
    label "Masyw_Centralny"
  ]
  node [
    id 877
    label "Akwitania"
  ]
  node [
    id 878
    label "Alzacja"
  ]
  node [
    id 879
    label "Sekwana"
  ]
  node [
    id 880
    label "Langwedocja"
  ]
  node [
    id 881
    label "Martynika"
  ]
  node [
    id 882
    label "Bretania"
  ]
  node [
    id 883
    label "Sabaudia"
  ]
  node [
    id 884
    label "Korsyka"
  ]
  node [
    id 885
    label "Normandia"
  ]
  node [
    id 886
    label "Gaskonia"
  ]
  node [
    id 887
    label "Burgundia"
  ]
  node [
    id 888
    label "frank_francuski"
  ]
  node [
    id 889
    label "Wandea"
  ]
  node [
    id 890
    label "Prowansja"
  ]
  node [
    id 891
    label "Gwadelupa"
  ]
  node [
    id 892
    label "somoni"
  ]
  node [
    id 893
    label "Melanezja"
  ]
  node [
    id 894
    label "dolar_Fid&#380;i"
  ]
  node [
    id 895
    label "funt_cypryjski"
  ]
  node [
    id 896
    label "Afrodyzje"
  ]
  node [
    id 897
    label "peso_dominika&#324;skie"
  ]
  node [
    id 898
    label "Fryburg"
  ]
  node [
    id 899
    label "Bazylea"
  ]
  node [
    id 900
    label "Alpy"
  ]
  node [
    id 901
    label "Helwecja"
  ]
  node [
    id 902
    label "Berno"
  ]
  node [
    id 903
    label "sum"
  ]
  node [
    id 904
    label "Karaka&#322;pacja"
  ]
  node [
    id 905
    label "Windawa"
  ]
  node [
    id 906
    label "&#322;at"
  ]
  node [
    id 907
    label "Kurlandia"
  ]
  node [
    id 908
    label "Liwonia"
  ]
  node [
    id 909
    label "rubel_&#322;otewski"
  ]
  node [
    id 910
    label "Inflanty"
  ]
  node [
    id 911
    label "Wile&#324;szczyzna"
  ]
  node [
    id 912
    label "&#379;mud&#378;"
  ]
  node [
    id 913
    label "lit"
  ]
  node [
    id 914
    label "frank_tunezyjski"
  ]
  node [
    id 915
    label "dinar_tunezyjski"
  ]
  node [
    id 916
    label "lempira"
  ]
  node [
    id 917
    label "korona_w&#281;gierska"
  ]
  node [
    id 918
    label "forint"
  ]
  node [
    id 919
    label "Lipt&#243;w"
  ]
  node [
    id 920
    label "dong"
  ]
  node [
    id 921
    label "Annam"
  ]
  node [
    id 922
    label "lud"
  ]
  node [
    id 923
    label "frank_kongijski"
  ]
  node [
    id 924
    label "szyling_somalijski"
  ]
  node [
    id 925
    label "cruzado"
  ]
  node [
    id 926
    label "real"
  ]
  node [
    id 927
    label "Podole"
  ]
  node [
    id 928
    label "Wsch&#243;d"
  ]
  node [
    id 929
    label "Naddnieprze"
  ]
  node [
    id 930
    label "Ma&#322;orosja"
  ]
  node [
    id 931
    label "Wo&#322;y&#324;"
  ]
  node [
    id 932
    label "Nadbu&#380;e"
  ]
  node [
    id 933
    label "hrywna"
  ]
  node [
    id 934
    label "Zaporo&#380;e"
  ]
  node [
    id 935
    label "Krym"
  ]
  node [
    id 936
    label "Dniestr"
  ]
  node [
    id 937
    label "Przykarpacie"
  ]
  node [
    id 938
    label "Kozaczyzna"
  ]
  node [
    id 939
    label "karbowaniec"
  ]
  node [
    id 940
    label "Tasmania"
  ]
  node [
    id 941
    label "Nowy_&#346;wiat"
  ]
  node [
    id 942
    label "dolar_australijski"
  ]
  node [
    id 943
    label "gourde"
  ]
  node [
    id 944
    label "escudo_angolskie"
  ]
  node [
    id 945
    label "kwanza"
  ]
  node [
    id 946
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 947
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 948
    label "Ad&#380;aria"
  ]
  node [
    id 949
    label "lari"
  ]
  node [
    id 950
    label "naira"
  ]
  node [
    id 951
    label "Ohio"
  ]
  node [
    id 952
    label "P&#243;&#322;noc"
  ]
  node [
    id 953
    label "Nowy_York"
  ]
  node [
    id 954
    label "Illinois"
  ]
  node [
    id 955
    label "Po&#322;udnie"
  ]
  node [
    id 956
    label "Kalifornia"
  ]
  node [
    id 957
    label "Wirginia"
  ]
  node [
    id 958
    label "Teksas"
  ]
  node [
    id 959
    label "Waszyngton"
  ]
  node [
    id 960
    label "zielona_karta"
  ]
  node [
    id 961
    label "Massachusetts"
  ]
  node [
    id 962
    label "Alaska"
  ]
  node [
    id 963
    label "Hawaje"
  ]
  node [
    id 964
    label "Maryland"
  ]
  node [
    id 965
    label "Michigan"
  ]
  node [
    id 966
    label "Arizona"
  ]
  node [
    id 967
    label "Georgia"
  ]
  node [
    id 968
    label "stan_wolny"
  ]
  node [
    id 969
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 970
    label "Pensylwania"
  ]
  node [
    id 971
    label "Luizjana"
  ]
  node [
    id 972
    label "Nowy_Meksyk"
  ]
  node [
    id 973
    label "Wuj_Sam"
  ]
  node [
    id 974
    label "Alabama"
  ]
  node [
    id 975
    label "Kansas"
  ]
  node [
    id 976
    label "Oregon"
  ]
  node [
    id 977
    label "Zach&#243;d"
  ]
  node [
    id 978
    label "Floryda"
  ]
  node [
    id 979
    label "Oklahoma"
  ]
  node [
    id 980
    label "Hudson"
  ]
  node [
    id 981
    label "som"
  ]
  node [
    id 982
    label "peso_urugwajskie"
  ]
  node [
    id 983
    label "denar_macedo&#324;ski"
  ]
  node [
    id 984
    label "dolar_Brunei"
  ]
  node [
    id 985
    label "rial_ira&#324;ski"
  ]
  node [
    id 986
    label "mu&#322;&#322;a"
  ]
  node [
    id 987
    label "Persja"
  ]
  node [
    id 988
    label "d&#380;amahirijja"
  ]
  node [
    id 989
    label "dinar_libijski"
  ]
  node [
    id 990
    label "nakfa"
  ]
  node [
    id 991
    label "rial_katarski"
  ]
  node [
    id 992
    label "quetzal"
  ]
  node [
    id 993
    label "won"
  ]
  node [
    id 994
    label "rial_jeme&#324;ski"
  ]
  node [
    id 995
    label "peso_argenty&#324;skie"
  ]
  node [
    id 996
    label "guarani"
  ]
  node [
    id 997
    label "perper"
  ]
  node [
    id 998
    label "dinar_kuwejcki"
  ]
  node [
    id 999
    label "dalasi"
  ]
  node [
    id 1000
    label "dolar_Zimbabwe"
  ]
  node [
    id 1001
    label "Szantung"
  ]
  node [
    id 1002
    label "Chiny_Zachodnie"
  ]
  node [
    id 1003
    label "Kuantung"
  ]
  node [
    id 1004
    label "D&#380;ungaria"
  ]
  node [
    id 1005
    label "yuan"
  ]
  node [
    id 1006
    label "Hongkong"
  ]
  node [
    id 1007
    label "Chiny_Wschodnie"
  ]
  node [
    id 1008
    label "Guangdong"
  ]
  node [
    id 1009
    label "Junnan"
  ]
  node [
    id 1010
    label "Mand&#380;uria"
  ]
  node [
    id 1011
    label "Syczuan"
  ]
  node [
    id 1012
    label "Pa&#322;uki"
  ]
  node [
    id 1013
    label "Wolin"
  ]
  node [
    id 1014
    label "z&#322;oty"
  ]
  node [
    id 1015
    label "So&#322;a"
  ]
  node [
    id 1016
    label "Krajna"
  ]
  node [
    id 1017
    label "Suwalszczyzna"
  ]
  node [
    id 1018
    label "barwy_polskie"
  ]
  node [
    id 1019
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1020
    label "Kaczawa"
  ]
  node [
    id 1021
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1022
    label "Wis&#322;a"
  ]
  node [
    id 1023
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1024
    label "lira_turecka"
  ]
  node [
    id 1025
    label "Azja_Mniejsza"
  ]
  node [
    id 1026
    label "Ujgur"
  ]
  node [
    id 1027
    label "kuna"
  ]
  node [
    id 1028
    label "dram"
  ]
  node [
    id 1029
    label "tala"
  ]
  node [
    id 1030
    label "korona_s&#322;owacka"
  ]
  node [
    id 1031
    label "Turiec"
  ]
  node [
    id 1032
    label "Himalaje"
  ]
  node [
    id 1033
    label "rupia_nepalska"
  ]
  node [
    id 1034
    label "frank_gwinejski"
  ]
  node [
    id 1035
    label "korona_esto&#324;ska"
  ]
  node [
    id 1036
    label "marka_esto&#324;ska"
  ]
  node [
    id 1037
    label "Quebec"
  ]
  node [
    id 1038
    label "dolar_kanadyjski"
  ]
  node [
    id 1039
    label "Nowa_Fundlandia"
  ]
  node [
    id 1040
    label "Zanzibar"
  ]
  node [
    id 1041
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1042
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1043
    label "&#346;wite&#378;"
  ]
  node [
    id 1044
    label "peso_kolumbijskie"
  ]
  node [
    id 1045
    label "Synaj"
  ]
  node [
    id 1046
    label "paraszyt"
  ]
  node [
    id 1047
    label "funt_egipski"
  ]
  node [
    id 1048
    label "szach"
  ]
  node [
    id 1049
    label "Baktria"
  ]
  node [
    id 1050
    label "afgani"
  ]
  node [
    id 1051
    label "baht"
  ]
  node [
    id 1052
    label "tolar"
  ]
  node [
    id 1053
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1054
    label "Gagauzja"
  ]
  node [
    id 1055
    label "moszaw"
  ]
  node [
    id 1056
    label "Kanaan"
  ]
  node [
    id 1057
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1058
    label "Jerozolima"
  ]
  node [
    id 1059
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1060
    label "Wiktoria"
  ]
  node [
    id 1061
    label "Guernsey"
  ]
  node [
    id 1062
    label "Conrad"
  ]
  node [
    id 1063
    label "funt_szterling"
  ]
  node [
    id 1064
    label "Portland"
  ]
  node [
    id 1065
    label "El&#380;bieta_I"
  ]
  node [
    id 1066
    label "Kornwalia"
  ]
  node [
    id 1067
    label "Dolna_Frankonia"
  ]
  node [
    id 1068
    label "Karpaty"
  ]
  node [
    id 1069
    label "Beskid_Niski"
  ]
  node [
    id 1070
    label "Mariensztat"
  ]
  node [
    id 1071
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1072
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1073
    label "Paj&#281;czno"
  ]
  node [
    id 1074
    label "Mogielnica"
  ]
  node [
    id 1075
    label "Gop&#322;o"
  ]
  node [
    id 1076
    label "Moza"
  ]
  node [
    id 1077
    label "Poprad"
  ]
  node [
    id 1078
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1079
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1080
    label "Bojanowo"
  ]
  node [
    id 1081
    label "Obra"
  ]
  node [
    id 1082
    label "Wilkowo_Polskie"
  ]
  node [
    id 1083
    label "Dobra"
  ]
  node [
    id 1084
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1085
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1086
    label "Etruria"
  ]
  node [
    id 1087
    label "Rumelia"
  ]
  node [
    id 1088
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1089
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1090
    label "Abchazja"
  ]
  node [
    id 1091
    label "Sarmata"
  ]
  node [
    id 1092
    label "Eurazja"
  ]
  node [
    id 1093
    label "Tatry"
  ]
  node [
    id 1094
    label "Podtatrze"
  ]
  node [
    id 1095
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1096
    label "jezioro"
  ]
  node [
    id 1097
    label "&#346;l&#261;sk"
  ]
  node [
    id 1098
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1099
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1100
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1101
    label "Austro-W&#281;gry"
  ]
  node [
    id 1102
    label "funt_szkocki"
  ]
  node [
    id 1103
    label "Kaledonia"
  ]
  node [
    id 1104
    label "Biskupice"
  ]
  node [
    id 1105
    label "Iwanowice"
  ]
  node [
    id 1106
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1107
    label "Rogo&#378;nik"
  ]
  node [
    id 1108
    label "Ropa"
  ]
  node [
    id 1109
    label "Buriacja"
  ]
  node [
    id 1110
    label "Rozewie"
  ]
  node [
    id 1111
    label "Norwegia"
  ]
  node [
    id 1112
    label "Szwecja"
  ]
  node [
    id 1113
    label "Finlandia"
  ]
  node [
    id 1114
    label "Aruba"
  ]
  node [
    id 1115
    label "Kajmany"
  ]
  node [
    id 1116
    label "Anguilla"
  ]
  node [
    id 1117
    label "Amazonka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 840
  ]
  edge [
    source 8
    target 841
  ]
  edge [
    source 8
    target 842
  ]
  edge [
    source 8
    target 843
  ]
  edge [
    source 8
    target 844
  ]
  edge [
    source 8
    target 845
  ]
  edge [
    source 8
    target 846
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 848
  ]
  edge [
    source 8
    target 849
  ]
  edge [
    source 8
    target 850
  ]
  edge [
    source 8
    target 851
  ]
  edge [
    source 8
    target 852
  ]
  edge [
    source 8
    target 853
  ]
  edge [
    source 8
    target 854
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 856
  ]
  edge [
    source 8
    target 857
  ]
  edge [
    source 8
    target 858
  ]
  edge [
    source 8
    target 859
  ]
  edge [
    source 8
    target 860
  ]
  edge [
    source 8
    target 861
  ]
  edge [
    source 8
    target 862
  ]
  edge [
    source 8
    target 863
  ]
  edge [
    source 8
    target 864
  ]
  edge [
    source 8
    target 865
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 867
  ]
  edge [
    source 8
    target 868
  ]
  edge [
    source 8
    target 869
  ]
  edge [
    source 8
    target 870
  ]
  edge [
    source 8
    target 871
  ]
  edge [
    source 8
    target 872
  ]
  edge [
    source 8
    target 873
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 875
  ]
  edge [
    source 8
    target 876
  ]
  edge [
    source 8
    target 877
  ]
  edge [
    source 8
    target 878
  ]
  edge [
    source 8
    target 879
  ]
  edge [
    source 8
    target 880
  ]
  edge [
    source 8
    target 881
  ]
  edge [
    source 8
    target 882
  ]
  edge [
    source 8
    target 883
  ]
  edge [
    source 8
    target 884
  ]
  edge [
    source 8
    target 885
  ]
  edge [
    source 8
    target 886
  ]
  edge [
    source 8
    target 887
  ]
  edge [
    source 8
    target 888
  ]
  edge [
    source 8
    target 889
  ]
  edge [
    source 8
    target 890
  ]
  edge [
    source 8
    target 891
  ]
  edge [
    source 8
    target 892
  ]
  edge [
    source 8
    target 893
  ]
  edge [
    source 8
    target 894
  ]
  edge [
    source 8
    target 895
  ]
  edge [
    source 8
    target 896
  ]
  edge [
    source 8
    target 897
  ]
  edge [
    source 8
    target 898
  ]
  edge [
    source 8
    target 899
  ]
  edge [
    source 8
    target 900
  ]
  edge [
    source 8
    target 901
  ]
  edge [
    source 8
    target 902
  ]
  edge [
    source 8
    target 903
  ]
  edge [
    source 8
    target 904
  ]
  edge [
    source 8
    target 905
  ]
  edge [
    source 8
    target 906
  ]
  edge [
    source 8
    target 907
  ]
  edge [
    source 8
    target 908
  ]
  edge [
    source 8
    target 909
  ]
  edge [
    source 8
    target 910
  ]
  edge [
    source 8
    target 911
  ]
  edge [
    source 8
    target 912
  ]
  edge [
    source 8
    target 913
  ]
  edge [
    source 8
    target 914
  ]
  edge [
    source 8
    target 915
  ]
  edge [
    source 8
    target 916
  ]
  edge [
    source 8
    target 917
  ]
  edge [
    source 8
    target 918
  ]
  edge [
    source 8
    target 919
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 921
  ]
  edge [
    source 8
    target 922
  ]
  edge [
    source 8
    target 923
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 925
  ]
  edge [
    source 8
    target 926
  ]
  edge [
    source 8
    target 927
  ]
  edge [
    source 8
    target 928
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 930
  ]
  edge [
    source 8
    target 931
  ]
  edge [
    source 8
    target 932
  ]
  edge [
    source 8
    target 933
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 8
    target 937
  ]
  edge [
    source 8
    target 938
  ]
  edge [
    source 8
    target 939
  ]
  edge [
    source 8
    target 940
  ]
  edge [
    source 8
    target 941
  ]
  edge [
    source 8
    target 942
  ]
  edge [
    source 8
    target 943
  ]
  edge [
    source 8
    target 944
  ]
  edge [
    source 8
    target 945
  ]
  edge [
    source 8
    target 946
  ]
  edge [
    source 8
    target 947
  ]
  edge [
    source 8
    target 948
  ]
  edge [
    source 8
    target 949
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 951
  ]
  edge [
    source 8
    target 952
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 954
  ]
  edge [
    source 8
    target 955
  ]
  edge [
    source 8
    target 956
  ]
  edge [
    source 8
    target 957
  ]
  edge [
    source 8
    target 958
  ]
  edge [
    source 8
    target 959
  ]
  edge [
    source 8
    target 960
  ]
  edge [
    source 8
    target 961
  ]
  edge [
    source 8
    target 962
  ]
  edge [
    source 8
    target 963
  ]
  edge [
    source 8
    target 964
  ]
  edge [
    source 8
    target 965
  ]
  edge [
    source 8
    target 966
  ]
  edge [
    source 8
    target 967
  ]
  edge [
    source 8
    target 968
  ]
  edge [
    source 8
    target 969
  ]
  edge [
    source 8
    target 970
  ]
  edge [
    source 8
    target 971
  ]
  edge [
    source 8
    target 972
  ]
  edge [
    source 8
    target 973
  ]
  edge [
    source 8
    target 974
  ]
  edge [
    source 8
    target 975
  ]
  edge [
    source 8
    target 976
  ]
  edge [
    source 8
    target 977
  ]
  edge [
    source 8
    target 978
  ]
  edge [
    source 8
    target 979
  ]
  edge [
    source 8
    target 980
  ]
  edge [
    source 8
    target 981
  ]
  edge [
    source 8
    target 982
  ]
  edge [
    source 8
    target 983
  ]
  edge [
    source 8
    target 984
  ]
  edge [
    source 8
    target 985
  ]
  edge [
    source 8
    target 986
  ]
  edge [
    source 8
    target 987
  ]
  edge [
    source 8
    target 988
  ]
  edge [
    source 8
    target 989
  ]
  edge [
    source 8
    target 990
  ]
  edge [
    source 8
    target 991
  ]
  edge [
    source 8
    target 992
  ]
  edge [
    source 8
    target 993
  ]
  edge [
    source 8
    target 994
  ]
  edge [
    source 8
    target 995
  ]
  edge [
    source 8
    target 996
  ]
  edge [
    source 8
    target 997
  ]
  edge [
    source 8
    target 998
  ]
  edge [
    source 8
    target 999
  ]
  edge [
    source 8
    target 1000
  ]
  edge [
    source 8
    target 1001
  ]
  edge [
    source 8
    target 1002
  ]
  edge [
    source 8
    target 1003
  ]
  edge [
    source 8
    target 1004
  ]
  edge [
    source 8
    target 1005
  ]
  edge [
    source 8
    target 1006
  ]
  edge [
    source 8
    target 1007
  ]
  edge [
    source 8
    target 1008
  ]
  edge [
    source 8
    target 1009
  ]
  edge [
    source 8
    target 1010
  ]
  edge [
    source 8
    target 1011
  ]
  edge [
    source 8
    target 1012
  ]
  edge [
    source 8
    target 1013
  ]
  edge [
    source 8
    target 1014
  ]
  edge [
    source 8
    target 1015
  ]
  edge [
    source 8
    target 1016
  ]
  edge [
    source 8
    target 1017
  ]
  edge [
    source 8
    target 1018
  ]
  edge [
    source 8
    target 1019
  ]
  edge [
    source 8
    target 1020
  ]
  edge [
    source 8
    target 1021
  ]
  edge [
    source 8
    target 1022
  ]
  edge [
    source 8
    target 1023
  ]
  edge [
    source 8
    target 1024
  ]
  edge [
    source 8
    target 1025
  ]
  edge [
    source 8
    target 1026
  ]
  edge [
    source 8
    target 1027
  ]
  edge [
    source 8
    target 1028
  ]
  edge [
    source 8
    target 1029
  ]
  edge [
    source 8
    target 1030
  ]
  edge [
    source 8
    target 1031
  ]
  edge [
    source 8
    target 1032
  ]
  edge [
    source 8
    target 1033
  ]
  edge [
    source 8
    target 1034
  ]
  edge [
    source 8
    target 1035
  ]
  edge [
    source 8
    target 1036
  ]
  edge [
    source 8
    target 1037
  ]
  edge [
    source 8
    target 1038
  ]
  edge [
    source 8
    target 1039
  ]
  edge [
    source 8
    target 1040
  ]
  edge [
    source 8
    target 1041
  ]
  edge [
    source 8
    target 1042
  ]
  edge [
    source 8
    target 1043
  ]
  edge [
    source 8
    target 1044
  ]
  edge [
    source 8
    target 1045
  ]
  edge [
    source 8
    target 1046
  ]
  edge [
    source 8
    target 1047
  ]
  edge [
    source 8
    target 1048
  ]
  edge [
    source 8
    target 1049
  ]
  edge [
    source 8
    target 1050
  ]
  edge [
    source 8
    target 1051
  ]
  edge [
    source 8
    target 1052
  ]
  edge [
    source 8
    target 1053
  ]
  edge [
    source 8
    target 1054
  ]
  edge [
    source 8
    target 1055
  ]
  edge [
    source 8
    target 1056
  ]
  edge [
    source 8
    target 1057
  ]
  edge [
    source 8
    target 1058
  ]
  edge [
    source 8
    target 1059
  ]
  edge [
    source 8
    target 1060
  ]
  edge [
    source 8
    target 1061
  ]
  edge [
    source 8
    target 1062
  ]
  edge [
    source 8
    target 1063
  ]
  edge [
    source 8
    target 1064
  ]
  edge [
    source 8
    target 1065
  ]
  edge [
    source 8
    target 1066
  ]
  edge [
    source 8
    target 1067
  ]
  edge [
    source 8
    target 1068
  ]
  edge [
    source 8
    target 1069
  ]
  edge [
    source 8
    target 1070
  ]
  edge [
    source 8
    target 1071
  ]
  edge [
    source 8
    target 1072
  ]
  edge [
    source 8
    target 1073
  ]
  edge [
    source 8
    target 1074
  ]
  edge [
    source 8
    target 1075
  ]
  edge [
    source 8
    target 1076
  ]
  edge [
    source 8
    target 1077
  ]
  edge [
    source 8
    target 1078
  ]
  edge [
    source 8
    target 1079
  ]
  edge [
    source 8
    target 1080
  ]
  edge [
    source 8
    target 1081
  ]
  edge [
    source 8
    target 1082
  ]
  edge [
    source 8
    target 1083
  ]
  edge [
    source 8
    target 1084
  ]
  edge [
    source 8
    target 1085
  ]
  edge [
    source 8
    target 1086
  ]
  edge [
    source 8
    target 1087
  ]
  edge [
    source 8
    target 1088
  ]
  edge [
    source 8
    target 1089
  ]
  edge [
    source 8
    target 1090
  ]
  edge [
    source 8
    target 1091
  ]
  edge [
    source 8
    target 1092
  ]
  edge [
    source 8
    target 1093
  ]
  edge [
    source 8
    target 1094
  ]
  edge [
    source 8
    target 1095
  ]
  edge [
    source 8
    target 1096
  ]
  edge [
    source 8
    target 1097
  ]
  edge [
    source 8
    target 1098
  ]
  edge [
    source 8
    target 1099
  ]
  edge [
    source 8
    target 1100
  ]
  edge [
    source 8
    target 1101
  ]
  edge [
    source 8
    target 1102
  ]
  edge [
    source 8
    target 1103
  ]
  edge [
    source 8
    target 1104
  ]
  edge [
    source 8
    target 1105
  ]
  edge [
    source 8
    target 1106
  ]
  edge [
    source 8
    target 1107
  ]
  edge [
    source 8
    target 1108
  ]
  edge [
    source 8
    target 1109
  ]
  edge [
    source 8
    target 1110
  ]
  edge [
    source 8
    target 1111
  ]
  edge [
    source 8
    target 1112
  ]
  edge [
    source 8
    target 1113
  ]
  edge [
    source 8
    target 1114
  ]
  edge [
    source 8
    target 1115
  ]
  edge [
    source 8
    target 1116
  ]
  edge [
    source 8
    target 1117
  ]
]
