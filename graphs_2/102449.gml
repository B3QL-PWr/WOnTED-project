graph [
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "skutecznie"
    origin "text"
  ]
  node [
    id 2
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "projekt"
    origin "text"
  ]
  node [
    id 4
    label "typ"
    origin "text"
  ]
  node [
    id 5
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 6
    label "zobo"
  ]
  node [
    id 7
    label "yakalo"
  ]
  node [
    id 8
    label "byd&#322;o"
  ]
  node [
    id 9
    label "dzo"
  ]
  node [
    id 10
    label "kr&#281;torogie"
  ]
  node [
    id 11
    label "zbi&#243;r"
  ]
  node [
    id 12
    label "g&#322;owa"
  ]
  node [
    id 13
    label "czochrad&#322;o"
  ]
  node [
    id 14
    label "posp&#243;lstwo"
  ]
  node [
    id 15
    label "kraal"
  ]
  node [
    id 16
    label "livestock"
  ]
  node [
    id 17
    label "prze&#380;uwacz"
  ]
  node [
    id 18
    label "zebu"
  ]
  node [
    id 19
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 20
    label "bizon"
  ]
  node [
    id 21
    label "byd&#322;o_domowe"
  ]
  node [
    id 22
    label "dobrze"
  ]
  node [
    id 23
    label "skuteczny"
  ]
  node [
    id 24
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 25
    label "odpowiednio"
  ]
  node [
    id 26
    label "dobroczynnie"
  ]
  node [
    id 27
    label "moralnie"
  ]
  node [
    id 28
    label "korzystnie"
  ]
  node [
    id 29
    label "pozytywnie"
  ]
  node [
    id 30
    label "lepiej"
  ]
  node [
    id 31
    label "wiele"
  ]
  node [
    id 32
    label "pomy&#347;lnie"
  ]
  node [
    id 33
    label "dobry"
  ]
  node [
    id 34
    label "poskutkowanie"
  ]
  node [
    id 35
    label "sprawny"
  ]
  node [
    id 36
    label "skutkowanie"
  ]
  node [
    id 37
    label "&#380;y&#263;"
  ]
  node [
    id 38
    label "robi&#263;"
  ]
  node [
    id 39
    label "kierowa&#263;"
  ]
  node [
    id 40
    label "g&#243;rowa&#263;"
  ]
  node [
    id 41
    label "tworzy&#263;"
  ]
  node [
    id 42
    label "krzywa"
  ]
  node [
    id 43
    label "linia_melodyczna"
  ]
  node [
    id 44
    label "control"
  ]
  node [
    id 45
    label "string"
  ]
  node [
    id 46
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 47
    label "ukierunkowywa&#263;"
  ]
  node [
    id 48
    label "sterowa&#263;"
  ]
  node [
    id 49
    label "kre&#347;li&#263;"
  ]
  node [
    id 50
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 51
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 52
    label "message"
  ]
  node [
    id 53
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 54
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 55
    label "eksponowa&#263;"
  ]
  node [
    id 56
    label "navigate"
  ]
  node [
    id 57
    label "manipulate"
  ]
  node [
    id 58
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 59
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 60
    label "przesuwa&#263;"
  ]
  node [
    id 61
    label "partner"
  ]
  node [
    id 62
    label "prowadzenie"
  ]
  node [
    id 63
    label "powodowa&#263;"
  ]
  node [
    id 64
    label "mie&#263;_miejsce"
  ]
  node [
    id 65
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 66
    label "motywowa&#263;"
  ]
  node [
    id 67
    label "act"
  ]
  node [
    id 68
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 69
    label "organizowa&#263;"
  ]
  node [
    id 70
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 71
    label "czyni&#263;"
  ]
  node [
    id 72
    label "give"
  ]
  node [
    id 73
    label "stylizowa&#263;"
  ]
  node [
    id 74
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 75
    label "falowa&#263;"
  ]
  node [
    id 76
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 77
    label "peddle"
  ]
  node [
    id 78
    label "praca"
  ]
  node [
    id 79
    label "wydala&#263;"
  ]
  node [
    id 80
    label "tentegowa&#263;"
  ]
  node [
    id 81
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 82
    label "urz&#261;dza&#263;"
  ]
  node [
    id 83
    label "oszukiwa&#263;"
  ]
  node [
    id 84
    label "work"
  ]
  node [
    id 85
    label "ukazywa&#263;"
  ]
  node [
    id 86
    label "przerabia&#263;"
  ]
  node [
    id 87
    label "post&#281;powa&#263;"
  ]
  node [
    id 88
    label "draw"
  ]
  node [
    id 89
    label "clear"
  ]
  node [
    id 90
    label "usuwa&#263;"
  ]
  node [
    id 91
    label "report"
  ]
  node [
    id 92
    label "rysowa&#263;"
  ]
  node [
    id 93
    label "describe"
  ]
  node [
    id 94
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 95
    label "przedstawia&#263;"
  ]
  node [
    id 96
    label "delineate"
  ]
  node [
    id 97
    label "pope&#322;nia&#263;"
  ]
  node [
    id 98
    label "wytwarza&#263;"
  ]
  node [
    id 99
    label "get"
  ]
  node [
    id 100
    label "consist"
  ]
  node [
    id 101
    label "stanowi&#263;"
  ]
  node [
    id 102
    label "raise"
  ]
  node [
    id 103
    label "podkre&#347;la&#263;"
  ]
  node [
    id 104
    label "demonstrowa&#263;"
  ]
  node [
    id 105
    label "unwrap"
  ]
  node [
    id 106
    label "napromieniowywa&#263;"
  ]
  node [
    id 107
    label "trzyma&#263;"
  ]
  node [
    id 108
    label "manipulowa&#263;"
  ]
  node [
    id 109
    label "wysy&#322;a&#263;"
  ]
  node [
    id 110
    label "zwierzchnik"
  ]
  node [
    id 111
    label "ustawia&#263;"
  ]
  node [
    id 112
    label "przeznacza&#263;"
  ]
  node [
    id 113
    label "match"
  ]
  node [
    id 114
    label "administrowa&#263;"
  ]
  node [
    id 115
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 116
    label "order"
  ]
  node [
    id 117
    label "indicate"
  ]
  node [
    id 118
    label "undertaking"
  ]
  node [
    id 119
    label "base_on_balls"
  ]
  node [
    id 120
    label "wyprzedza&#263;"
  ]
  node [
    id 121
    label "wygrywa&#263;"
  ]
  node [
    id 122
    label "chop"
  ]
  node [
    id 123
    label "przekracza&#263;"
  ]
  node [
    id 124
    label "treat"
  ]
  node [
    id 125
    label "zaspokaja&#263;"
  ]
  node [
    id 126
    label "suffice"
  ]
  node [
    id 127
    label "zaspakaja&#263;"
  ]
  node [
    id 128
    label "uprawia&#263;_seks"
  ]
  node [
    id 129
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 130
    label "serve"
  ]
  node [
    id 131
    label "dostosowywa&#263;"
  ]
  node [
    id 132
    label "estrange"
  ]
  node [
    id 133
    label "transfer"
  ]
  node [
    id 134
    label "translate"
  ]
  node [
    id 135
    label "go"
  ]
  node [
    id 136
    label "zmienia&#263;"
  ]
  node [
    id 137
    label "postpone"
  ]
  node [
    id 138
    label "przestawia&#263;"
  ]
  node [
    id 139
    label "rusza&#263;"
  ]
  node [
    id 140
    label "przenosi&#263;"
  ]
  node [
    id 141
    label "marshal"
  ]
  node [
    id 142
    label "wyznacza&#263;"
  ]
  node [
    id 143
    label "nadawa&#263;"
  ]
  node [
    id 144
    label "shape"
  ]
  node [
    id 145
    label "istnie&#263;"
  ]
  node [
    id 146
    label "pause"
  ]
  node [
    id 147
    label "stay"
  ]
  node [
    id 148
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 149
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 150
    label "dane"
  ]
  node [
    id 151
    label "klawisz"
  ]
  node [
    id 152
    label "figura_geometryczna"
  ]
  node [
    id 153
    label "linia"
  ]
  node [
    id 154
    label "poprowadzi&#263;"
  ]
  node [
    id 155
    label "curvature"
  ]
  node [
    id 156
    label "curve"
  ]
  node [
    id 157
    label "wystawa&#263;"
  ]
  node [
    id 158
    label "sprout"
  ]
  node [
    id 159
    label "dysponowanie"
  ]
  node [
    id 160
    label "sterowanie"
  ]
  node [
    id 161
    label "powodowanie"
  ]
  node [
    id 162
    label "management"
  ]
  node [
    id 163
    label "kierowanie"
  ]
  node [
    id 164
    label "ukierunkowywanie"
  ]
  node [
    id 165
    label "przywodzenie"
  ]
  node [
    id 166
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 167
    label "doprowadzanie"
  ]
  node [
    id 168
    label "kre&#347;lenie"
  ]
  node [
    id 169
    label "lead"
  ]
  node [
    id 170
    label "eksponowanie"
  ]
  node [
    id 171
    label "robienie"
  ]
  node [
    id 172
    label "prowadzanie"
  ]
  node [
    id 173
    label "wprowadzanie"
  ]
  node [
    id 174
    label "doprowadzenie"
  ]
  node [
    id 175
    label "poprowadzenie"
  ]
  node [
    id 176
    label "kszta&#322;towanie"
  ]
  node [
    id 177
    label "aim"
  ]
  node [
    id 178
    label "zwracanie"
  ]
  node [
    id 179
    label "przecinanie"
  ]
  node [
    id 180
    label "czynno&#347;&#263;"
  ]
  node [
    id 181
    label "ta&#324;czenie"
  ]
  node [
    id 182
    label "przewy&#380;szanie"
  ]
  node [
    id 183
    label "g&#243;rowanie"
  ]
  node [
    id 184
    label "zaprowadzanie"
  ]
  node [
    id 185
    label "dawanie"
  ]
  node [
    id 186
    label "trzymanie"
  ]
  node [
    id 187
    label "oprowadzanie"
  ]
  node [
    id 188
    label "wprowadzenie"
  ]
  node [
    id 189
    label "drive"
  ]
  node [
    id 190
    label "oprowadzenie"
  ]
  node [
    id 191
    label "przeci&#281;cie"
  ]
  node [
    id 192
    label "przeci&#261;ganie"
  ]
  node [
    id 193
    label "pozarz&#261;dzanie"
  ]
  node [
    id 194
    label "granie"
  ]
  node [
    id 195
    label "pracownik"
  ]
  node [
    id 196
    label "przedsi&#281;biorca"
  ]
  node [
    id 197
    label "cz&#322;owiek"
  ]
  node [
    id 198
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 199
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 200
    label "kolaborator"
  ]
  node [
    id 201
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 202
    label "sp&#243;lnik"
  ]
  node [
    id 203
    label "aktor"
  ]
  node [
    id 204
    label "uczestniczenie"
  ]
  node [
    id 205
    label "intencja"
  ]
  node [
    id 206
    label "plan"
  ]
  node [
    id 207
    label "device"
  ]
  node [
    id 208
    label "program_u&#380;ytkowy"
  ]
  node [
    id 209
    label "pomys&#322;"
  ]
  node [
    id 210
    label "dokumentacja"
  ]
  node [
    id 211
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 212
    label "agreement"
  ]
  node [
    id 213
    label "dokument"
  ]
  node [
    id 214
    label "wytw&#243;r"
  ]
  node [
    id 215
    label "thinking"
  ]
  node [
    id 216
    label "model"
  ]
  node [
    id 217
    label "punkt"
  ]
  node [
    id 218
    label "rysunek"
  ]
  node [
    id 219
    label "miejsce_pracy"
  ]
  node [
    id 220
    label "przestrze&#324;"
  ]
  node [
    id 221
    label "obraz"
  ]
  node [
    id 222
    label "reprezentacja"
  ]
  node [
    id 223
    label "dekoracja"
  ]
  node [
    id 224
    label "perspektywa"
  ]
  node [
    id 225
    label "ekscerpcja"
  ]
  node [
    id 226
    label "materia&#322;"
  ]
  node [
    id 227
    label "operat"
  ]
  node [
    id 228
    label "kosztorys"
  ]
  node [
    id 229
    label "zapis"
  ]
  node [
    id 230
    label "&#347;wiadectwo"
  ]
  node [
    id 231
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 232
    label "parafa"
  ]
  node [
    id 233
    label "plik"
  ]
  node [
    id 234
    label "raport&#243;wka"
  ]
  node [
    id 235
    label "utw&#243;r"
  ]
  node [
    id 236
    label "record"
  ]
  node [
    id 237
    label "fascyku&#322;"
  ]
  node [
    id 238
    label "registratura"
  ]
  node [
    id 239
    label "artyku&#322;"
  ]
  node [
    id 240
    label "writing"
  ]
  node [
    id 241
    label "sygnatariusz"
  ]
  node [
    id 242
    label "pocz&#261;tki"
  ]
  node [
    id 243
    label "ukra&#347;&#263;"
  ]
  node [
    id 244
    label "ukradzenie"
  ]
  node [
    id 245
    label "idea"
  ]
  node [
    id 246
    label "system"
  ]
  node [
    id 247
    label "facet"
  ]
  node [
    id 248
    label "jednostka_systematyczna"
  ]
  node [
    id 249
    label "kr&#243;lestwo"
  ]
  node [
    id 250
    label "autorament"
  ]
  node [
    id 251
    label "variety"
  ]
  node [
    id 252
    label "antycypacja"
  ]
  node [
    id 253
    label "przypuszczenie"
  ]
  node [
    id 254
    label "cynk"
  ]
  node [
    id 255
    label "obstawia&#263;"
  ]
  node [
    id 256
    label "gromada"
  ]
  node [
    id 257
    label "sztuka"
  ]
  node [
    id 258
    label "rezultat"
  ]
  node [
    id 259
    label "design"
  ]
  node [
    id 260
    label "pob&#243;r"
  ]
  node [
    id 261
    label "wojsko"
  ]
  node [
    id 262
    label "type"
  ]
  node [
    id 263
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 264
    label "wygl&#261;d"
  ]
  node [
    id 265
    label "pogl&#261;d"
  ]
  node [
    id 266
    label "proces"
  ]
  node [
    id 267
    label "zapowied&#378;"
  ]
  node [
    id 268
    label "upodobnienie"
  ]
  node [
    id 269
    label "zjawisko"
  ]
  node [
    id 270
    label "narracja"
  ]
  node [
    id 271
    label "prediction"
  ]
  node [
    id 272
    label "pr&#243;bowanie"
  ]
  node [
    id 273
    label "rola"
  ]
  node [
    id 274
    label "przedmiot"
  ]
  node [
    id 275
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 276
    label "realizacja"
  ]
  node [
    id 277
    label "scena"
  ]
  node [
    id 278
    label "didaskalia"
  ]
  node [
    id 279
    label "czyn"
  ]
  node [
    id 280
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 281
    label "environment"
  ]
  node [
    id 282
    label "head"
  ]
  node [
    id 283
    label "scenariusz"
  ]
  node [
    id 284
    label "egzemplarz"
  ]
  node [
    id 285
    label "jednostka"
  ]
  node [
    id 286
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 287
    label "kultura_duchowa"
  ]
  node [
    id 288
    label "fortel"
  ]
  node [
    id 289
    label "theatrical_performance"
  ]
  node [
    id 290
    label "ambala&#380;"
  ]
  node [
    id 291
    label "sprawno&#347;&#263;"
  ]
  node [
    id 292
    label "kobieta"
  ]
  node [
    id 293
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 294
    label "Faust"
  ]
  node [
    id 295
    label "scenografia"
  ]
  node [
    id 296
    label "ods&#322;ona"
  ]
  node [
    id 297
    label "turn"
  ]
  node [
    id 298
    label "pokaz"
  ]
  node [
    id 299
    label "ilo&#347;&#263;"
  ]
  node [
    id 300
    label "przedstawienie"
  ]
  node [
    id 301
    label "przedstawi&#263;"
  ]
  node [
    id 302
    label "Apollo"
  ]
  node [
    id 303
    label "kultura"
  ]
  node [
    id 304
    label "przedstawianie"
  ]
  node [
    id 305
    label "towar"
  ]
  node [
    id 306
    label "bratek"
  ]
  node [
    id 307
    label "datum"
  ]
  node [
    id 308
    label "poszlaka"
  ]
  node [
    id 309
    label "dopuszczenie"
  ]
  node [
    id 310
    label "teoria"
  ]
  node [
    id 311
    label "conjecture"
  ]
  node [
    id 312
    label "koniektura"
  ]
  node [
    id 313
    label "ludzko&#347;&#263;"
  ]
  node [
    id 314
    label "asymilowanie"
  ]
  node [
    id 315
    label "wapniak"
  ]
  node [
    id 316
    label "asymilowa&#263;"
  ]
  node [
    id 317
    label "os&#322;abia&#263;"
  ]
  node [
    id 318
    label "posta&#263;"
  ]
  node [
    id 319
    label "hominid"
  ]
  node [
    id 320
    label "podw&#322;adny"
  ]
  node [
    id 321
    label "os&#322;abianie"
  ]
  node [
    id 322
    label "figura"
  ]
  node [
    id 323
    label "portrecista"
  ]
  node [
    id 324
    label "dwun&#243;g"
  ]
  node [
    id 325
    label "profanum"
  ]
  node [
    id 326
    label "mikrokosmos"
  ]
  node [
    id 327
    label "nasada"
  ]
  node [
    id 328
    label "duch"
  ]
  node [
    id 329
    label "antropochoria"
  ]
  node [
    id 330
    label "osoba"
  ]
  node [
    id 331
    label "wz&#243;r"
  ]
  node [
    id 332
    label "senior"
  ]
  node [
    id 333
    label "oddzia&#322;ywanie"
  ]
  node [
    id 334
    label "Adam"
  ]
  node [
    id 335
    label "homo_sapiens"
  ]
  node [
    id 336
    label "polifag"
  ]
  node [
    id 337
    label "tip-off"
  ]
  node [
    id 338
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 339
    label "tip"
  ]
  node [
    id 340
    label "sygna&#322;"
  ]
  node [
    id 341
    label "metal_kolorowy"
  ]
  node [
    id 342
    label "mikroelement"
  ]
  node [
    id 343
    label "cynkowiec"
  ]
  node [
    id 344
    label "ubezpiecza&#263;"
  ]
  node [
    id 345
    label "venture"
  ]
  node [
    id 346
    label "przewidywa&#263;"
  ]
  node [
    id 347
    label "zapewnia&#263;"
  ]
  node [
    id 348
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 349
    label "typowa&#263;"
  ]
  node [
    id 350
    label "ochrona"
  ]
  node [
    id 351
    label "zastawia&#263;"
  ]
  node [
    id 352
    label "budowa&#263;"
  ]
  node [
    id 353
    label "zajmowa&#263;"
  ]
  node [
    id 354
    label "obejmowa&#263;"
  ]
  node [
    id 355
    label "os&#322;ania&#263;"
  ]
  node [
    id 356
    label "otacza&#263;"
  ]
  node [
    id 357
    label "broni&#263;"
  ]
  node [
    id 358
    label "powierza&#263;"
  ]
  node [
    id 359
    label "bramka"
  ]
  node [
    id 360
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 361
    label "frame"
  ]
  node [
    id 362
    label "dzia&#322;anie"
  ]
  node [
    id 363
    label "event"
  ]
  node [
    id 364
    label "przyczyna"
  ]
  node [
    id 365
    label "jednostka_administracyjna"
  ]
  node [
    id 366
    label "zoologia"
  ]
  node [
    id 367
    label "skupienie"
  ]
  node [
    id 368
    label "stage_set"
  ]
  node [
    id 369
    label "tribe"
  ]
  node [
    id 370
    label "hurma"
  ]
  node [
    id 371
    label "grupa"
  ]
  node [
    id 372
    label "botanika"
  ]
  node [
    id 373
    label "ro&#347;liny"
  ]
  node [
    id 374
    label "grzyby"
  ]
  node [
    id 375
    label "Arktogea"
  ]
  node [
    id 376
    label "prokarioty"
  ]
  node [
    id 377
    label "zwierz&#281;ta"
  ]
  node [
    id 378
    label "domena"
  ]
  node [
    id 379
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 380
    label "protisty"
  ]
  node [
    id 381
    label "pa&#324;stwo"
  ]
  node [
    id 382
    label "terytorium"
  ]
  node [
    id 383
    label "kategoria_systematyczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
]
