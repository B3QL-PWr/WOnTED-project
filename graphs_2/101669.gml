graph [
  node [
    id 0
    label "przechodzi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "pytanie"
    origin "text"
  ]
  node [
    id 3
    label "odpowiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ten"
    origin "text"
  ]
  node [
    id 7
    label "bezpo&#347;redni"
    origin "text"
  ]
  node [
    id 8
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 9
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 12
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 13
    label "taki"
    origin "text"
  ]
  node [
    id 14
    label "sytuacja"
    origin "text"
  ]
  node [
    id 15
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 16
    label "kto"
    origin "text"
  ]
  node [
    id 17
    label "za&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 18
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 19
    label "skarga"
    origin "text"
  ]
  node [
    id 20
    label "kasacyjny"
    origin "text"
  ]
  node [
    id 21
    label "odpis"
    origin "text"
  ]
  node [
    id 22
    label "wezwa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 24
    label "tylko"
    origin "text"
  ]
  node [
    id 25
    label "uniemo&#380;liwia&#263;"
    origin "text"
  ]
  node [
    id 26
    label "nadanie"
    origin "text"
  ]
  node [
    id 27
    label "prawid&#322;owy"
    origin "text"
  ]
  node [
    id 28
    label "bieg"
    origin "text"
  ]
  node [
    id 29
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 30
    label "tak"
    origin "text"
  ]
  node [
    id 31
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 32
    label "zapomnie&#263;"
    origin "text"
  ]
  node [
    id 33
    label "napisa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "sam"
    origin "text"
  ]
  node [
    id 36
    label "wniosek"
    origin "text"
  ]
  node [
    id 37
    label "rozpatrzenie"
    origin "text"
  ]
  node [
    id 38
    label "prosty"
    origin "text"
  ]
  node [
    id 39
    label "powinien"
    origin "text"
  ]
  node [
    id 40
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 41
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 42
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 43
    label "s&#322;ysze&#263;by&#263;"
    origin "text"
  ]
  node [
    id 44
    label "dzisiejszy"
    origin "text"
  ]
  node [
    id 45
    label "dyskusja"
    origin "text"
  ]
  node [
    id 46
    label "wszystek"
    origin "text"
  ]
  node [
    id 47
    label "klub"
    origin "text"
  ]
  node [
    id 48
    label "popiera&#263;"
    origin "text"
  ]
  node [
    id 49
    label "nowelizacja"
    origin "text"
  ]
  node [
    id 50
    label "te&#380;"
    origin "text"
  ]
  node [
    id 51
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 52
    label "wszyscy"
    origin "text"
  ]
  node [
    id 53
    label "tym"
    origin "text"
  ]
  node [
    id 54
    label "moment"
    origin "text"
  ]
  node [
    id 55
    label "podzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 56
    label "wystarczy&#263;"
    origin "text"
  ]
  node [
    id 57
    label "komentarz"
    origin "text"
  ]
  node [
    id 58
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 59
    label "mie&#263;_miejsce"
  ]
  node [
    id 60
    label "move"
  ]
  node [
    id 61
    label "zaczyna&#263;"
  ]
  node [
    id 62
    label "przebywa&#263;"
  ]
  node [
    id 63
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 64
    label "conflict"
  ]
  node [
    id 65
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "mija&#263;"
  ]
  node [
    id 67
    label "proceed"
  ]
  node [
    id 68
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 69
    label "go"
  ]
  node [
    id 70
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 71
    label "saturate"
  ]
  node [
    id 72
    label "i&#347;&#263;"
  ]
  node [
    id 73
    label "doznawa&#263;"
  ]
  node [
    id 74
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 75
    label "przestawa&#263;"
  ]
  node [
    id 76
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 77
    label "pass"
  ]
  node [
    id 78
    label "zalicza&#263;"
  ]
  node [
    id 79
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 80
    label "zmienia&#263;"
  ]
  node [
    id 81
    label "test"
  ]
  node [
    id 82
    label "podlega&#263;"
  ]
  node [
    id 83
    label "przerabia&#263;"
  ]
  node [
    id 84
    label "continue"
  ]
  node [
    id 85
    label "traci&#263;"
  ]
  node [
    id 86
    label "alternate"
  ]
  node [
    id 87
    label "change"
  ]
  node [
    id 88
    label "reengineering"
  ]
  node [
    id 89
    label "zast&#281;powa&#263;"
  ]
  node [
    id 90
    label "sprawia&#263;"
  ]
  node [
    id 91
    label "zyskiwa&#263;"
  ]
  node [
    id 92
    label "&#380;y&#263;"
  ]
  node [
    id 93
    label "coating"
  ]
  node [
    id 94
    label "determine"
  ]
  node [
    id 95
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 96
    label "ko&#324;czy&#263;"
  ]
  node [
    id 97
    label "finish_up"
  ]
  node [
    id 98
    label "lecie&#263;"
  ]
  node [
    id 99
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 100
    label "bangla&#263;"
  ]
  node [
    id 101
    label "trace"
  ]
  node [
    id 102
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 103
    label "impart"
  ]
  node [
    id 104
    label "try"
  ]
  node [
    id 105
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 106
    label "boost"
  ]
  node [
    id 107
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 108
    label "dziama&#263;"
  ]
  node [
    id 109
    label "blend"
  ]
  node [
    id 110
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 111
    label "draw"
  ]
  node [
    id 112
    label "wyrusza&#263;"
  ]
  node [
    id 113
    label "bie&#380;e&#263;"
  ]
  node [
    id 114
    label "dzia&#263;_si&#281;"
  ]
  node [
    id 115
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 116
    label "tryb"
  ]
  node [
    id 117
    label "czas"
  ]
  node [
    id 118
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 119
    label "atakowa&#263;"
  ]
  node [
    id 120
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 121
    label "describe"
  ]
  node [
    id 122
    label "post&#281;powa&#263;"
  ]
  node [
    id 123
    label "tkwi&#263;"
  ]
  node [
    id 124
    label "istnie&#263;"
  ]
  node [
    id 125
    label "pause"
  ]
  node [
    id 126
    label "hesitate"
  ]
  node [
    id 127
    label "trwa&#263;"
  ]
  node [
    id 128
    label "base_on_balls"
  ]
  node [
    id 129
    label "omija&#263;"
  ]
  node [
    id 130
    label "hurt"
  ]
  node [
    id 131
    label "czu&#263;"
  ]
  node [
    id 132
    label "zale&#380;e&#263;"
  ]
  node [
    id 133
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 134
    label "bra&#263;"
  ]
  node [
    id 135
    label "mark"
  ]
  node [
    id 136
    label "number"
  ]
  node [
    id 137
    label "stwierdza&#263;"
  ]
  node [
    id 138
    label "odb&#281;bnia&#263;"
  ]
  node [
    id 139
    label "wlicza&#263;"
  ]
  node [
    id 140
    label "przyporz&#261;dkowywa&#263;"
  ]
  node [
    id 141
    label "odejmowa&#263;"
  ]
  node [
    id 142
    label "bankrupt"
  ]
  node [
    id 143
    label "open"
  ]
  node [
    id 144
    label "set_about"
  ]
  node [
    id 145
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 146
    label "begin"
  ]
  node [
    id 147
    label "goban"
  ]
  node [
    id 148
    label "gra_planszowa"
  ]
  node [
    id 149
    label "sport_umys&#322;owy"
  ]
  node [
    id 150
    label "chi&#324;ski"
  ]
  node [
    id 151
    label "cover"
  ]
  node [
    id 152
    label "robi&#263;"
  ]
  node [
    id 153
    label "wytwarza&#263;"
  ]
  node [
    id 154
    label "amend"
  ]
  node [
    id 155
    label "overwork"
  ]
  node [
    id 156
    label "convert"
  ]
  node [
    id 157
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 158
    label "zamienia&#263;"
  ]
  node [
    id 159
    label "modyfikowa&#263;"
  ]
  node [
    id 160
    label "radzi&#263;_sobie"
  ]
  node [
    id 161
    label "pracowa&#263;"
  ]
  node [
    id 162
    label "przetwarza&#263;"
  ]
  node [
    id 163
    label "sp&#281;dza&#263;"
  ]
  node [
    id 164
    label "badanie"
  ]
  node [
    id 165
    label "do&#347;wiadczenie"
  ]
  node [
    id 166
    label "narz&#281;dzie"
  ]
  node [
    id 167
    label "przechodzenie"
  ]
  node [
    id 168
    label "quiz"
  ]
  node [
    id 169
    label "sprawdzian"
  ]
  node [
    id 170
    label "arkusz"
  ]
  node [
    id 171
    label "nast&#281;pnie"
  ]
  node [
    id 172
    label "inny"
  ]
  node [
    id 173
    label "nastopny"
  ]
  node [
    id 174
    label "kolejno"
  ]
  node [
    id 175
    label "kt&#243;ry&#347;"
  ]
  node [
    id 176
    label "osobno"
  ]
  node [
    id 177
    label "r&#243;&#380;ny"
  ]
  node [
    id 178
    label "inszy"
  ]
  node [
    id 179
    label "inaczej"
  ]
  node [
    id 180
    label "sprawa"
  ]
  node [
    id 181
    label "wypytanie"
  ]
  node [
    id 182
    label "egzaminowanie"
  ]
  node [
    id 183
    label "zwracanie_si&#281;"
  ]
  node [
    id 184
    label "wywo&#322;ywanie"
  ]
  node [
    id 185
    label "rozpytywanie"
  ]
  node [
    id 186
    label "wypowiedzenie"
  ]
  node [
    id 187
    label "wypowied&#378;"
  ]
  node [
    id 188
    label "problemat"
  ]
  node [
    id 189
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 190
    label "problematyka"
  ]
  node [
    id 191
    label "zadanie"
  ]
  node [
    id 192
    label "odpowiada&#263;"
  ]
  node [
    id 193
    label "przes&#322;uchiwanie"
  ]
  node [
    id 194
    label "question"
  ]
  node [
    id 195
    label "sprawdzanie"
  ]
  node [
    id 196
    label "odpowiadanie"
  ]
  node [
    id 197
    label "survey"
  ]
  node [
    id 198
    label "pos&#322;uchanie"
  ]
  node [
    id 199
    label "s&#261;d"
  ]
  node [
    id 200
    label "sparafrazowanie"
  ]
  node [
    id 201
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 202
    label "strawestowa&#263;"
  ]
  node [
    id 203
    label "sparafrazowa&#263;"
  ]
  node [
    id 204
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 205
    label "trawestowa&#263;"
  ]
  node [
    id 206
    label "sformu&#322;owanie"
  ]
  node [
    id 207
    label "parafrazowanie"
  ]
  node [
    id 208
    label "ozdobnik"
  ]
  node [
    id 209
    label "delimitacja"
  ]
  node [
    id 210
    label "parafrazowa&#263;"
  ]
  node [
    id 211
    label "stylizacja"
  ]
  node [
    id 212
    label "komunikat"
  ]
  node [
    id 213
    label "trawestowanie"
  ]
  node [
    id 214
    label "strawestowanie"
  ]
  node [
    id 215
    label "rezultat"
  ]
  node [
    id 216
    label "konwersja"
  ]
  node [
    id 217
    label "notice"
  ]
  node [
    id 218
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 219
    label "przepowiedzenie"
  ]
  node [
    id 220
    label "rozwi&#261;zanie"
  ]
  node [
    id 221
    label "generowa&#263;"
  ]
  node [
    id 222
    label "wydanie"
  ]
  node [
    id 223
    label "message"
  ]
  node [
    id 224
    label "generowanie"
  ]
  node [
    id 225
    label "wydobycie"
  ]
  node [
    id 226
    label "zwerbalizowanie"
  ]
  node [
    id 227
    label "szyk"
  ]
  node [
    id 228
    label "notification"
  ]
  node [
    id 229
    label "powiedzenie"
  ]
  node [
    id 230
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 231
    label "denunciation"
  ]
  node [
    id 232
    label "wyra&#380;enie"
  ]
  node [
    id 233
    label "zaj&#281;cie"
  ]
  node [
    id 234
    label "yield"
  ]
  node [
    id 235
    label "zbi&#243;r"
  ]
  node [
    id 236
    label "zaszkodzenie"
  ]
  node [
    id 237
    label "za&#322;o&#380;enie"
  ]
  node [
    id 238
    label "duty"
  ]
  node [
    id 239
    label "powierzanie"
  ]
  node [
    id 240
    label "work"
  ]
  node [
    id 241
    label "problem"
  ]
  node [
    id 242
    label "przepisanie"
  ]
  node [
    id 243
    label "nakarmienie"
  ]
  node [
    id 244
    label "przepisa&#263;"
  ]
  node [
    id 245
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 246
    label "czynno&#347;&#263;"
  ]
  node [
    id 247
    label "zobowi&#261;zanie"
  ]
  node [
    id 248
    label "kognicja"
  ]
  node [
    id 249
    label "object"
  ]
  node [
    id 250
    label "rozprawa"
  ]
  node [
    id 251
    label "temat"
  ]
  node [
    id 252
    label "wydarzenie"
  ]
  node [
    id 253
    label "szczeg&#243;&#322;"
  ]
  node [
    id 254
    label "proposition"
  ]
  node [
    id 255
    label "przes&#322;anka"
  ]
  node [
    id 256
    label "rzecz"
  ]
  node [
    id 257
    label "idea"
  ]
  node [
    id 258
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 259
    label "ustalenie"
  ]
  node [
    id 260
    label "redagowanie"
  ]
  node [
    id 261
    label "ustalanie"
  ]
  node [
    id 262
    label "dociekanie"
  ]
  node [
    id 263
    label "robienie"
  ]
  node [
    id 264
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 265
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 266
    label "investigation"
  ]
  node [
    id 267
    label "macanie"
  ]
  node [
    id 268
    label "usi&#322;owanie"
  ]
  node [
    id 269
    label "penetrowanie"
  ]
  node [
    id 270
    label "przymierzanie"
  ]
  node [
    id 271
    label "przymierzenie"
  ]
  node [
    id 272
    label "examination"
  ]
  node [
    id 273
    label "wypytywanie"
  ]
  node [
    id 274
    label "zbadanie"
  ]
  node [
    id 275
    label "react"
  ]
  node [
    id 276
    label "dawa&#263;"
  ]
  node [
    id 277
    label "ponosi&#263;"
  ]
  node [
    id 278
    label "report"
  ]
  node [
    id 279
    label "equate"
  ]
  node [
    id 280
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 281
    label "answer"
  ]
  node [
    id 282
    label "powodowa&#263;"
  ]
  node [
    id 283
    label "tone"
  ]
  node [
    id 284
    label "contend"
  ]
  node [
    id 285
    label "reagowa&#263;"
  ]
  node [
    id 286
    label "reagowanie"
  ]
  node [
    id 287
    label "dawanie"
  ]
  node [
    id 288
    label "powodowanie"
  ]
  node [
    id 289
    label "bycie"
  ]
  node [
    id 290
    label "pokutowanie"
  ]
  node [
    id 291
    label "odpowiedzialny"
  ]
  node [
    id 292
    label "winny"
  ]
  node [
    id 293
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 294
    label "picie_piwa"
  ]
  node [
    id 295
    label "odpowiedni"
  ]
  node [
    id 296
    label "parry"
  ]
  node [
    id 297
    label "fit"
  ]
  node [
    id 298
    label "dzianie_si&#281;"
  ]
  node [
    id 299
    label "rendition"
  ]
  node [
    id 300
    label "ponoszenie"
  ]
  node [
    id 301
    label "rozmawianie"
  ]
  node [
    id 302
    label "faza"
  ]
  node [
    id 303
    label "podchodzi&#263;"
  ]
  node [
    id 304
    label "&#263;wiczenie"
  ]
  node [
    id 305
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 306
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 307
    label "praca_pisemna"
  ]
  node [
    id 308
    label "kontrola"
  ]
  node [
    id 309
    label "dydaktyka"
  ]
  node [
    id 310
    label "pr&#243;ba"
  ]
  node [
    id 311
    label "przepytywanie"
  ]
  node [
    id 312
    label "oznajmianie"
  ]
  node [
    id 313
    label "wzywanie"
  ]
  node [
    id 314
    label "development"
  ]
  node [
    id 315
    label "exploitation"
  ]
  node [
    id 316
    label "zdawanie"
  ]
  node [
    id 317
    label "w&#322;&#261;czanie"
  ]
  node [
    id 318
    label "s&#322;uchanie"
  ]
  node [
    id 319
    label "zareagowa&#263;"
  ]
  node [
    id 320
    label "ponie&#347;&#263;"
  ]
  node [
    id 321
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 322
    label "da&#263;"
  ]
  node [
    id 323
    label "copy"
  ]
  node [
    id 324
    label "picture"
  ]
  node [
    id 325
    label "tax_return"
  ]
  node [
    id 326
    label "spowodowa&#263;"
  ]
  node [
    id 327
    label "bekn&#261;&#263;"
  ]
  node [
    id 328
    label "agreement"
  ]
  node [
    id 329
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 330
    label "riot"
  ]
  node [
    id 331
    label "carry"
  ]
  node [
    id 332
    label "dozna&#263;"
  ]
  node [
    id 333
    label "nak&#322;oni&#263;"
  ]
  node [
    id 334
    label "wst&#261;pi&#263;"
  ]
  node [
    id 335
    label "porwa&#263;"
  ]
  node [
    id 336
    label "uda&#263;_si&#281;"
  ]
  node [
    id 337
    label "powierzy&#263;"
  ]
  node [
    id 338
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 339
    label "give"
  ]
  node [
    id 340
    label "obieca&#263;"
  ]
  node [
    id 341
    label "pozwoli&#263;"
  ]
  node [
    id 342
    label "odst&#261;pi&#263;"
  ]
  node [
    id 343
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 344
    label "przywali&#263;"
  ]
  node [
    id 345
    label "wyrzec_si&#281;"
  ]
  node [
    id 346
    label "sztachn&#261;&#263;"
  ]
  node [
    id 347
    label "rap"
  ]
  node [
    id 348
    label "feed"
  ]
  node [
    id 349
    label "zrobi&#263;"
  ]
  node [
    id 350
    label "convey"
  ]
  node [
    id 351
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 352
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 353
    label "testify"
  ]
  node [
    id 354
    label "udost&#281;pni&#263;"
  ]
  node [
    id 355
    label "przeznaczy&#263;"
  ]
  node [
    id 356
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 357
    label "zada&#263;"
  ]
  node [
    id 358
    label "dress"
  ]
  node [
    id 359
    label "dostarczy&#263;"
  ]
  node [
    id 360
    label "przekaza&#263;"
  ]
  node [
    id 361
    label "supply"
  ]
  node [
    id 362
    label "doda&#263;"
  ]
  node [
    id 363
    label "zap&#322;aci&#263;"
  ]
  node [
    id 364
    label "sta&#263;_si&#281;"
  ]
  node [
    id 365
    label "act"
  ]
  node [
    id 366
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 367
    label "koza"
  ]
  node [
    id 368
    label "wydoby&#263;"
  ]
  node [
    id 369
    label "daniel"
  ]
  node [
    id 370
    label "baran"
  ]
  node [
    id 371
    label "kszyk"
  ]
  node [
    id 372
    label "owca_domowa"
  ]
  node [
    id 373
    label "bleat"
  ]
  node [
    id 374
    label "dok&#322;adnie"
  ]
  node [
    id 375
    label "punctiliously"
  ]
  node [
    id 376
    label "meticulously"
  ]
  node [
    id 377
    label "precyzyjnie"
  ]
  node [
    id 378
    label "dok&#322;adny"
  ]
  node [
    id 379
    label "rzetelnie"
  ]
  node [
    id 380
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 381
    label "equal"
  ]
  node [
    id 382
    label "chodzi&#263;"
  ]
  node [
    id 383
    label "si&#281;ga&#263;"
  ]
  node [
    id 384
    label "stan"
  ]
  node [
    id 385
    label "obecno&#347;&#263;"
  ]
  node [
    id 386
    label "stand"
  ]
  node [
    id 387
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 388
    label "uczestniczy&#263;"
  ]
  node [
    id 389
    label "participate"
  ]
  node [
    id 390
    label "pozostawa&#263;"
  ]
  node [
    id 391
    label "zostawa&#263;"
  ]
  node [
    id 392
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 393
    label "adhere"
  ]
  node [
    id 394
    label "compass"
  ]
  node [
    id 395
    label "korzysta&#263;"
  ]
  node [
    id 396
    label "appreciation"
  ]
  node [
    id 397
    label "osi&#261;ga&#263;"
  ]
  node [
    id 398
    label "dociera&#263;"
  ]
  node [
    id 399
    label "get"
  ]
  node [
    id 400
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 401
    label "mierzy&#263;"
  ]
  node [
    id 402
    label "u&#380;ywa&#263;"
  ]
  node [
    id 403
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 404
    label "exsert"
  ]
  node [
    id 405
    label "being"
  ]
  node [
    id 406
    label "cecha"
  ]
  node [
    id 407
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 408
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 409
    label "p&#322;ywa&#263;"
  ]
  node [
    id 410
    label "run"
  ]
  node [
    id 411
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 412
    label "przebiega&#263;"
  ]
  node [
    id 413
    label "wk&#322;ada&#263;"
  ]
  node [
    id 414
    label "bywa&#263;"
  ]
  node [
    id 415
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 416
    label "stara&#263;_si&#281;"
  ]
  node [
    id 417
    label "para"
  ]
  node [
    id 418
    label "str&#243;j"
  ]
  node [
    id 419
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 420
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 421
    label "krok"
  ]
  node [
    id 422
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 423
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 424
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 425
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 426
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 427
    label "Ohio"
  ]
  node [
    id 428
    label "wci&#281;cie"
  ]
  node [
    id 429
    label "Nowy_York"
  ]
  node [
    id 430
    label "warstwa"
  ]
  node [
    id 431
    label "samopoczucie"
  ]
  node [
    id 432
    label "Illinois"
  ]
  node [
    id 433
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 434
    label "state"
  ]
  node [
    id 435
    label "Jukatan"
  ]
  node [
    id 436
    label "Kalifornia"
  ]
  node [
    id 437
    label "Wirginia"
  ]
  node [
    id 438
    label "wektor"
  ]
  node [
    id 439
    label "Goa"
  ]
  node [
    id 440
    label "Teksas"
  ]
  node [
    id 441
    label "Waszyngton"
  ]
  node [
    id 442
    label "miejsce"
  ]
  node [
    id 443
    label "Massachusetts"
  ]
  node [
    id 444
    label "Alaska"
  ]
  node [
    id 445
    label "Arakan"
  ]
  node [
    id 446
    label "Hawaje"
  ]
  node [
    id 447
    label "Maryland"
  ]
  node [
    id 448
    label "punkt"
  ]
  node [
    id 449
    label "Michigan"
  ]
  node [
    id 450
    label "Arizona"
  ]
  node [
    id 451
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 452
    label "Georgia"
  ]
  node [
    id 453
    label "poziom"
  ]
  node [
    id 454
    label "Pensylwania"
  ]
  node [
    id 455
    label "shape"
  ]
  node [
    id 456
    label "Luizjana"
  ]
  node [
    id 457
    label "Nowy_Meksyk"
  ]
  node [
    id 458
    label "Alabama"
  ]
  node [
    id 459
    label "ilo&#347;&#263;"
  ]
  node [
    id 460
    label "Kansas"
  ]
  node [
    id 461
    label "Oregon"
  ]
  node [
    id 462
    label "Oklahoma"
  ]
  node [
    id 463
    label "Floryda"
  ]
  node [
    id 464
    label "jednostka_administracyjna"
  ]
  node [
    id 465
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 466
    label "okre&#347;lony"
  ]
  node [
    id 467
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 468
    label "wiadomy"
  ]
  node [
    id 469
    label "bliski"
  ]
  node [
    id 470
    label "bezpo&#347;rednio"
  ]
  node [
    id 471
    label "szczery"
  ]
  node [
    id 472
    label "szczodry"
  ]
  node [
    id 473
    label "s&#322;uszny"
  ]
  node [
    id 474
    label "uczciwy"
  ]
  node [
    id 475
    label "przekonuj&#261;cy"
  ]
  node [
    id 476
    label "prostoduszny"
  ]
  node [
    id 477
    label "szczyry"
  ]
  node [
    id 478
    label "szczerze"
  ]
  node [
    id 479
    label "czysty"
  ]
  node [
    id 480
    label "blisko"
  ]
  node [
    id 481
    label "cz&#322;owiek"
  ]
  node [
    id 482
    label "znajomy"
  ]
  node [
    id 483
    label "zwi&#261;zany"
  ]
  node [
    id 484
    label "przesz&#322;y"
  ]
  node [
    id 485
    label "silny"
  ]
  node [
    id 486
    label "zbli&#380;enie"
  ]
  node [
    id 487
    label "kr&#243;tki"
  ]
  node [
    id 488
    label "oddalony"
  ]
  node [
    id 489
    label "nieodleg&#322;y"
  ]
  node [
    id 490
    label "przysz&#322;y"
  ]
  node [
    id 491
    label "gotowy"
  ]
  node [
    id 492
    label "ma&#322;y"
  ]
  node [
    id 493
    label "kwota"
  ]
  node [
    id 494
    label "&#347;lad"
  ]
  node [
    id 495
    label "zjawisko"
  ]
  node [
    id 496
    label "lobbysta"
  ]
  node [
    id 497
    label "doch&#243;d_narodowy"
  ]
  node [
    id 498
    label "dzia&#322;anie"
  ]
  node [
    id 499
    label "typ"
  ]
  node [
    id 500
    label "event"
  ]
  node [
    id 501
    label "przyczyna"
  ]
  node [
    id 502
    label "proces"
  ]
  node [
    id 503
    label "boski"
  ]
  node [
    id 504
    label "krajobraz"
  ]
  node [
    id 505
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 506
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 507
    label "przywidzenie"
  ]
  node [
    id 508
    label "presence"
  ]
  node [
    id 509
    label "charakter"
  ]
  node [
    id 510
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 511
    label "wynie&#347;&#263;"
  ]
  node [
    id 512
    label "pieni&#261;dze"
  ]
  node [
    id 513
    label "limit"
  ]
  node [
    id 514
    label "wynosi&#263;"
  ]
  node [
    id 515
    label "przedstawiciel"
  ]
  node [
    id 516
    label "grupa_nacisku"
  ]
  node [
    id 517
    label "sznurowanie"
  ]
  node [
    id 518
    label "odrobina"
  ]
  node [
    id 519
    label "skutek"
  ]
  node [
    id 520
    label "sznurowa&#263;"
  ]
  node [
    id 521
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 522
    label "attribute"
  ]
  node [
    id 523
    label "odcisk"
  ]
  node [
    id 524
    label "raj_utracony"
  ]
  node [
    id 525
    label "umieranie"
  ]
  node [
    id 526
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 527
    label "prze&#380;ywanie"
  ]
  node [
    id 528
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 529
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 530
    label "po&#322;&#243;g"
  ]
  node [
    id 531
    label "umarcie"
  ]
  node [
    id 532
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 533
    label "subsistence"
  ]
  node [
    id 534
    label "power"
  ]
  node [
    id 535
    label "okres_noworodkowy"
  ]
  node [
    id 536
    label "prze&#380;ycie"
  ]
  node [
    id 537
    label "wiek_matuzalemowy"
  ]
  node [
    id 538
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 539
    label "entity"
  ]
  node [
    id 540
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 541
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 542
    label "do&#380;ywanie"
  ]
  node [
    id 543
    label "byt"
  ]
  node [
    id 544
    label "dzieci&#324;stwo"
  ]
  node [
    id 545
    label "andropauza"
  ]
  node [
    id 546
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 547
    label "rozw&#243;j"
  ]
  node [
    id 548
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 549
    label "menopauza"
  ]
  node [
    id 550
    label "&#347;mier&#263;"
  ]
  node [
    id 551
    label "koleje_losu"
  ]
  node [
    id 552
    label "zegar_biologiczny"
  ]
  node [
    id 553
    label "szwung"
  ]
  node [
    id 554
    label "przebywanie"
  ]
  node [
    id 555
    label "warunki"
  ]
  node [
    id 556
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 557
    label "niemowl&#281;ctwo"
  ]
  node [
    id 558
    label "&#380;ywy"
  ]
  node [
    id 559
    label "life"
  ]
  node [
    id 560
    label "staro&#347;&#263;"
  ]
  node [
    id 561
    label "energy"
  ]
  node [
    id 562
    label "wra&#380;enie"
  ]
  node [
    id 563
    label "przej&#347;cie"
  ]
  node [
    id 564
    label "doznanie"
  ]
  node [
    id 565
    label "poradzenie_sobie"
  ]
  node [
    id 566
    label "przetrwanie"
  ]
  node [
    id 567
    label "survival"
  ]
  node [
    id 568
    label "wytrzymywanie"
  ]
  node [
    id 569
    label "zaznawanie"
  ]
  node [
    id 570
    label "trwanie"
  ]
  node [
    id 571
    label "obejrzenie"
  ]
  node [
    id 572
    label "widzenie"
  ]
  node [
    id 573
    label "urzeczywistnianie"
  ]
  node [
    id 574
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 575
    label "produkowanie"
  ]
  node [
    id 576
    label "przeszkodzenie"
  ]
  node [
    id 577
    label "znikni&#281;cie"
  ]
  node [
    id 578
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 579
    label "przeszkadzanie"
  ]
  node [
    id 580
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 581
    label "wyprodukowanie"
  ]
  node [
    id 582
    label "utrzymywanie"
  ]
  node [
    id 583
    label "subsystencja"
  ]
  node [
    id 584
    label "utrzyma&#263;"
  ]
  node [
    id 585
    label "egzystencja"
  ]
  node [
    id 586
    label "wy&#380;ywienie"
  ]
  node [
    id 587
    label "ontologicznie"
  ]
  node [
    id 588
    label "utrzymanie"
  ]
  node [
    id 589
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 590
    label "potencja"
  ]
  node [
    id 591
    label "utrzymywa&#263;"
  ]
  node [
    id 592
    label "status"
  ]
  node [
    id 593
    label "poprzedzanie"
  ]
  node [
    id 594
    label "czasoprzestrze&#324;"
  ]
  node [
    id 595
    label "laba"
  ]
  node [
    id 596
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 597
    label "chronometria"
  ]
  node [
    id 598
    label "rachuba_czasu"
  ]
  node [
    id 599
    label "przep&#322;ywanie"
  ]
  node [
    id 600
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 601
    label "czasokres"
  ]
  node [
    id 602
    label "odczyt"
  ]
  node [
    id 603
    label "chwila"
  ]
  node [
    id 604
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 605
    label "dzieje"
  ]
  node [
    id 606
    label "kategoria_gramatyczna"
  ]
  node [
    id 607
    label "poprzedzenie"
  ]
  node [
    id 608
    label "trawienie"
  ]
  node [
    id 609
    label "pochodzi&#263;"
  ]
  node [
    id 610
    label "period"
  ]
  node [
    id 611
    label "okres_czasu"
  ]
  node [
    id 612
    label "poprzedza&#263;"
  ]
  node [
    id 613
    label "schy&#322;ek"
  ]
  node [
    id 614
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 615
    label "odwlekanie_si&#281;"
  ]
  node [
    id 616
    label "zegar"
  ]
  node [
    id 617
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 618
    label "czwarty_wymiar"
  ]
  node [
    id 619
    label "pochodzenie"
  ]
  node [
    id 620
    label "koniugacja"
  ]
  node [
    id 621
    label "Zeitgeist"
  ]
  node [
    id 622
    label "trawi&#263;"
  ]
  node [
    id 623
    label "pogoda"
  ]
  node [
    id 624
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 625
    label "poprzedzi&#263;"
  ]
  node [
    id 626
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 627
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 628
    label "time_period"
  ]
  node [
    id 629
    label "ocieranie_si&#281;"
  ]
  node [
    id 630
    label "otoczenie_si&#281;"
  ]
  node [
    id 631
    label "posiedzenie"
  ]
  node [
    id 632
    label "otarcie_si&#281;"
  ]
  node [
    id 633
    label "atakowanie"
  ]
  node [
    id 634
    label "otaczanie_si&#281;"
  ]
  node [
    id 635
    label "wyj&#347;cie"
  ]
  node [
    id 636
    label "zmierzanie"
  ]
  node [
    id 637
    label "residency"
  ]
  node [
    id 638
    label "sojourn"
  ]
  node [
    id 639
    label "wychodzenie"
  ]
  node [
    id 640
    label "tkwienie"
  ]
  node [
    id 641
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 642
    label "absolutorium"
  ]
  node [
    id 643
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 644
    label "activity"
  ]
  node [
    id 645
    label "ton"
  ]
  node [
    id 646
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 647
    label "odumarcie"
  ]
  node [
    id 648
    label "przestanie"
  ]
  node [
    id 649
    label "martwy"
  ]
  node [
    id 650
    label "dysponowanie_si&#281;"
  ]
  node [
    id 651
    label "pomarcie"
  ]
  node [
    id 652
    label "die"
  ]
  node [
    id 653
    label "sko&#324;czenie"
  ]
  node [
    id 654
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 655
    label "zdechni&#281;cie"
  ]
  node [
    id 656
    label "zabicie"
  ]
  node [
    id 657
    label "korkowanie"
  ]
  node [
    id 658
    label "death"
  ]
  node [
    id 659
    label "zabijanie"
  ]
  node [
    id 660
    label "przestawanie"
  ]
  node [
    id 661
    label "odumieranie"
  ]
  node [
    id 662
    label "zdychanie"
  ]
  node [
    id 663
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 664
    label "zanikanie"
  ]
  node [
    id 665
    label "ko&#324;czenie"
  ]
  node [
    id 666
    label "nieuleczalnie_chory"
  ]
  node [
    id 667
    label "ciekawy"
  ]
  node [
    id 668
    label "szybki"
  ]
  node [
    id 669
    label "&#380;ywotny"
  ]
  node [
    id 670
    label "naturalny"
  ]
  node [
    id 671
    label "&#380;ywo"
  ]
  node [
    id 672
    label "o&#380;ywianie"
  ]
  node [
    id 673
    label "g&#322;&#281;boki"
  ]
  node [
    id 674
    label "wyra&#378;ny"
  ]
  node [
    id 675
    label "czynny"
  ]
  node [
    id 676
    label "aktualny"
  ]
  node [
    id 677
    label "zgrabny"
  ]
  node [
    id 678
    label "prawdziwy"
  ]
  node [
    id 679
    label "realistyczny"
  ]
  node [
    id 680
    label "energiczny"
  ]
  node [
    id 681
    label "procedura"
  ]
  node [
    id 682
    label "proces_biologiczny"
  ]
  node [
    id 683
    label "z&#322;ote_czasy"
  ]
  node [
    id 684
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 685
    label "process"
  ]
  node [
    id 686
    label "cycle"
  ]
  node [
    id 687
    label "defenestracja"
  ]
  node [
    id 688
    label "agonia"
  ]
  node [
    id 689
    label "kres"
  ]
  node [
    id 690
    label "mogi&#322;a"
  ]
  node [
    id 691
    label "kres_&#380;ycia"
  ]
  node [
    id 692
    label "upadek"
  ]
  node [
    id 693
    label "szeol"
  ]
  node [
    id 694
    label "pogrzebanie"
  ]
  node [
    id 695
    label "istota_nadprzyrodzona"
  ]
  node [
    id 696
    label "&#380;a&#322;oba"
  ]
  node [
    id 697
    label "pogrzeb"
  ]
  node [
    id 698
    label "majority"
  ]
  node [
    id 699
    label "wiek"
  ]
  node [
    id 700
    label "osiemnastoletni"
  ]
  node [
    id 701
    label "age"
  ]
  node [
    id 702
    label "zlec"
  ]
  node [
    id 703
    label "zlegni&#281;cie"
  ]
  node [
    id 704
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 705
    label "dzieci&#281;ctwo"
  ]
  node [
    id 706
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 707
    label "kobieta"
  ]
  node [
    id 708
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 709
    label "przekwitanie"
  ]
  node [
    id 710
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 711
    label "adolescence"
  ]
  node [
    id 712
    label "zielone_lata"
  ]
  node [
    id 713
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 714
    label "energia"
  ]
  node [
    id 715
    label "zapa&#322;"
  ]
  node [
    id 716
    label "artyleria"
  ]
  node [
    id 717
    label "laweta"
  ]
  node [
    id 718
    label "cannon"
  ]
  node [
    id 719
    label "waln&#261;&#263;"
  ]
  node [
    id 720
    label "bateria_artylerii"
  ]
  node [
    id 721
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 722
    label "oporopowrotnik"
  ]
  node [
    id 723
    label "przedmuchiwacz"
  ]
  node [
    id 724
    label "bateria"
  ]
  node [
    id 725
    label "bro&#324;"
  ]
  node [
    id 726
    label "amunicja"
  ]
  node [
    id 727
    label "karta_przetargowa"
  ]
  node [
    id 728
    label "rozbroi&#263;"
  ]
  node [
    id 729
    label "rozbrojenie"
  ]
  node [
    id 730
    label "osprz&#281;t"
  ]
  node [
    id 731
    label "uzbrojenie"
  ]
  node [
    id 732
    label "przyrz&#261;d"
  ]
  node [
    id 733
    label "rozbrajanie"
  ]
  node [
    id 734
    label "rozbraja&#263;"
  ]
  node [
    id 735
    label "or&#281;&#380;"
  ]
  node [
    id 736
    label "urz&#261;dzenie"
  ]
  node [
    id 737
    label "przyczepa"
  ]
  node [
    id 738
    label "podstawa"
  ]
  node [
    id 739
    label "lemiesz"
  ]
  node [
    id 740
    label "powrotnik"
  ]
  node [
    id 741
    label "formacja"
  ]
  node [
    id 742
    label "armia"
  ]
  node [
    id 743
    label "artillery"
  ]
  node [
    id 744
    label "munition"
  ]
  node [
    id 745
    label "zaw&#243;r"
  ]
  node [
    id 746
    label "kolekcja"
  ]
  node [
    id 747
    label "oficer_ogniowy"
  ]
  node [
    id 748
    label "dywizjon_artylerii"
  ]
  node [
    id 749
    label "kran"
  ]
  node [
    id 750
    label "pododdzia&#322;"
  ]
  node [
    id 751
    label "cell"
  ]
  node [
    id 752
    label "pluton"
  ]
  node [
    id 753
    label "odkr&#281;ca&#263;_wod&#281;"
  ]
  node [
    id 754
    label "odkr&#281;ci&#263;_wod&#281;"
  ]
  node [
    id 755
    label "dzia&#322;obitnia"
  ]
  node [
    id 756
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 757
    label "sygn&#261;&#263;"
  ]
  node [
    id 758
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 759
    label "peddle"
  ]
  node [
    id 760
    label "jebn&#261;&#263;"
  ]
  node [
    id 761
    label "fall"
  ]
  node [
    id 762
    label "uderzy&#263;"
  ]
  node [
    id 763
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 764
    label "zmieni&#263;"
  ]
  node [
    id 765
    label "majdn&#261;&#263;"
  ]
  node [
    id 766
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 767
    label "paln&#261;&#263;"
  ]
  node [
    id 768
    label "strzeli&#263;"
  ]
  node [
    id 769
    label "lumber"
  ]
  node [
    id 770
    label "jaki&#347;"
  ]
  node [
    id 771
    label "przyzwoity"
  ]
  node [
    id 772
    label "jako&#347;"
  ]
  node [
    id 773
    label "jako_tako"
  ]
  node [
    id 774
    label "niez&#322;y"
  ]
  node [
    id 775
    label "dziwny"
  ]
  node [
    id 776
    label "charakterystyczny"
  ]
  node [
    id 777
    label "motyw"
  ]
  node [
    id 778
    label "realia"
  ]
  node [
    id 779
    label "sk&#322;adnik"
  ]
  node [
    id 780
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 781
    label "niuansowa&#263;"
  ]
  node [
    id 782
    label "element"
  ]
  node [
    id 783
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 784
    label "zniuansowa&#263;"
  ]
  node [
    id 785
    label "fraza"
  ]
  node [
    id 786
    label "melodia"
  ]
  node [
    id 787
    label "ozdoba"
  ]
  node [
    id 788
    label "kontekst"
  ]
  node [
    id 789
    label "posta&#263;"
  ]
  node [
    id 790
    label "osoba"
  ]
  node [
    id 791
    label "znaczenie"
  ]
  node [
    id 792
    label "go&#347;&#263;"
  ]
  node [
    id 793
    label "ludzko&#347;&#263;"
  ]
  node [
    id 794
    label "asymilowanie"
  ]
  node [
    id 795
    label "wapniak"
  ]
  node [
    id 796
    label "asymilowa&#263;"
  ]
  node [
    id 797
    label "os&#322;abia&#263;"
  ]
  node [
    id 798
    label "hominid"
  ]
  node [
    id 799
    label "podw&#322;adny"
  ]
  node [
    id 800
    label "os&#322;abianie"
  ]
  node [
    id 801
    label "g&#322;owa"
  ]
  node [
    id 802
    label "figura"
  ]
  node [
    id 803
    label "portrecista"
  ]
  node [
    id 804
    label "dwun&#243;g"
  ]
  node [
    id 805
    label "profanum"
  ]
  node [
    id 806
    label "mikrokosmos"
  ]
  node [
    id 807
    label "nasada"
  ]
  node [
    id 808
    label "duch"
  ]
  node [
    id 809
    label "antropochoria"
  ]
  node [
    id 810
    label "wz&#243;r"
  ]
  node [
    id 811
    label "senior"
  ]
  node [
    id 812
    label "oddzia&#322;ywanie"
  ]
  node [
    id 813
    label "Adam"
  ]
  node [
    id 814
    label "homo_sapiens"
  ]
  node [
    id 815
    label "polifag"
  ]
  node [
    id 816
    label "odwiedziny"
  ]
  node [
    id 817
    label "klient"
  ]
  node [
    id 818
    label "restauracja"
  ]
  node [
    id 819
    label "przybysz"
  ]
  node [
    id 820
    label "uczestnik"
  ]
  node [
    id 821
    label "hotel"
  ]
  node [
    id 822
    label "bratek"
  ]
  node [
    id 823
    label "sztuka"
  ]
  node [
    id 824
    label "facet"
  ]
  node [
    id 825
    label "Chocho&#322;"
  ]
  node [
    id 826
    label "Herkules_Poirot"
  ]
  node [
    id 827
    label "Edyp"
  ]
  node [
    id 828
    label "parali&#380;owa&#263;"
  ]
  node [
    id 829
    label "Harry_Potter"
  ]
  node [
    id 830
    label "Casanova"
  ]
  node [
    id 831
    label "Zgredek"
  ]
  node [
    id 832
    label "Gargantua"
  ]
  node [
    id 833
    label "Winnetou"
  ]
  node [
    id 834
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 835
    label "Dulcynea"
  ]
  node [
    id 836
    label "person"
  ]
  node [
    id 837
    label "Plastu&#347;"
  ]
  node [
    id 838
    label "Quasimodo"
  ]
  node [
    id 839
    label "Sherlock_Holmes"
  ]
  node [
    id 840
    label "Faust"
  ]
  node [
    id 841
    label "Wallenrod"
  ]
  node [
    id 842
    label "Dwukwiat"
  ]
  node [
    id 843
    label "Don_Juan"
  ]
  node [
    id 844
    label "Don_Kiszot"
  ]
  node [
    id 845
    label "Hamlet"
  ]
  node [
    id 846
    label "Werter"
  ]
  node [
    id 847
    label "istota"
  ]
  node [
    id 848
    label "Szwejk"
  ]
  node [
    id 849
    label "odk&#322;adanie"
  ]
  node [
    id 850
    label "condition"
  ]
  node [
    id 851
    label "liczenie"
  ]
  node [
    id 852
    label "stawianie"
  ]
  node [
    id 853
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 854
    label "assay"
  ]
  node [
    id 855
    label "wskazywanie"
  ]
  node [
    id 856
    label "wyraz"
  ]
  node [
    id 857
    label "gravity"
  ]
  node [
    id 858
    label "weight"
  ]
  node [
    id 859
    label "command"
  ]
  node [
    id 860
    label "odgrywanie_roli"
  ]
  node [
    id 861
    label "informacja"
  ]
  node [
    id 862
    label "okre&#347;lanie"
  ]
  node [
    id 863
    label "charakterystyka"
  ]
  node [
    id 864
    label "zaistnie&#263;"
  ]
  node [
    id 865
    label "Osjan"
  ]
  node [
    id 866
    label "wygl&#261;d"
  ]
  node [
    id 867
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 868
    label "osobowo&#347;&#263;"
  ]
  node [
    id 869
    label "wytw&#243;r"
  ]
  node [
    id 870
    label "trim"
  ]
  node [
    id 871
    label "poby&#263;"
  ]
  node [
    id 872
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 873
    label "Aspazja"
  ]
  node [
    id 874
    label "punkt_widzenia"
  ]
  node [
    id 875
    label "kompleksja"
  ]
  node [
    id 876
    label "wytrzyma&#263;"
  ]
  node [
    id 877
    label "budowa"
  ]
  node [
    id 878
    label "pozosta&#263;"
  ]
  node [
    id 879
    label "point"
  ]
  node [
    id 880
    label "przedstawienie"
  ]
  node [
    id 881
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 882
    label "annex"
  ]
  node [
    id 883
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 884
    label "catch"
  ]
  node [
    id 885
    label "dokoptowa&#263;"
  ]
  node [
    id 886
    label "articulation"
  ]
  node [
    id 887
    label "fakt"
  ]
  node [
    id 888
    label "czyn"
  ]
  node [
    id 889
    label "ilustracja"
  ]
  node [
    id 890
    label "materia&#322;"
  ]
  node [
    id 891
    label "szata_graficzna"
  ]
  node [
    id 892
    label "photograph"
  ]
  node [
    id 893
    label "obrazek"
  ]
  node [
    id 894
    label "bia&#322;e_plamy"
  ]
  node [
    id 895
    label "funkcja"
  ]
  node [
    id 896
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 897
    label "cz&#322;onek"
  ]
  node [
    id 898
    label "substytuowa&#263;"
  ]
  node [
    id 899
    label "substytuowanie"
  ]
  node [
    id 900
    label "zast&#281;pca"
  ]
  node [
    id 901
    label "request"
  ]
  node [
    id 902
    label "akt_oskar&#380;enia"
  ]
  node [
    id 903
    label "skwierk"
  ]
  node [
    id 904
    label "zawiadomienie"
  ]
  node [
    id 905
    label "odwo&#322;anie"
  ]
  node [
    id 906
    label "announcement"
  ]
  node [
    id 907
    label "poinformowanie"
  ]
  node [
    id 908
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 909
    label "reference"
  ]
  node [
    id 910
    label "uniewa&#380;nienie"
  ]
  node [
    id 911
    label "retraction"
  ]
  node [
    id 912
    label "mention"
  ]
  node [
    id 913
    label "odprawienie"
  ]
  node [
    id 914
    label "d&#378;wi&#281;k"
  ]
  node [
    id 915
    label "&#380;ale"
  ]
  node [
    id 916
    label "&#347;wiergot"
  ]
  node [
    id 917
    label "apelacyjny"
  ]
  node [
    id 918
    label "appellate"
  ]
  node [
    id 919
    label "duplikat"
  ]
  node [
    id 920
    label "dokument"
  ]
  node [
    id 921
    label "kopia"
  ]
  node [
    id 922
    label "transcript"
  ]
  node [
    id 923
    label "nakaza&#263;"
  ]
  node [
    id 924
    label "invite"
  ]
  node [
    id 925
    label "zach&#281;ci&#263;"
  ]
  node [
    id 926
    label "poinformowa&#263;"
  ]
  node [
    id 927
    label "przewo&#322;a&#263;"
  ]
  node [
    id 928
    label "adduce"
  ]
  node [
    id 929
    label "ask"
  ]
  node [
    id 930
    label "poprosi&#263;"
  ]
  node [
    id 931
    label "inform"
  ]
  node [
    id 932
    label "zakomunikowa&#263;"
  ]
  node [
    id 933
    label "poleci&#263;"
  ]
  node [
    id 934
    label "order"
  ]
  node [
    id 935
    label "zapakowa&#263;"
  ]
  node [
    id 936
    label "zaproponowa&#263;"
  ]
  node [
    id 937
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 938
    label "pozyska&#263;"
  ]
  node [
    id 939
    label "zepsu&#263;"
  ]
  node [
    id 940
    label "przywo&#322;a&#263;"
  ]
  node [
    id 941
    label "wywo&#322;a&#263;"
  ]
  node [
    id 942
    label "przeszkadza&#263;"
  ]
  node [
    id 943
    label "anticipate"
  ]
  node [
    id 944
    label "transgress"
  ]
  node [
    id 945
    label "wadzi&#263;"
  ]
  node [
    id 946
    label "utrudnia&#263;"
  ]
  node [
    id 947
    label "broadcast"
  ]
  node [
    id 948
    label "spowodowanie"
  ]
  node [
    id 949
    label "zdarzenie_si&#281;"
  ]
  node [
    id 950
    label "nazwanie"
  ]
  node [
    id 951
    label "przes&#322;anie"
  ]
  node [
    id 952
    label "akt"
  ]
  node [
    id 953
    label "przyznanie"
  ]
  node [
    id 954
    label "denomination"
  ]
  node [
    id 955
    label "danie"
  ]
  node [
    id 956
    label "confession"
  ]
  node [
    id 957
    label "stwierdzenie"
  ]
  node [
    id 958
    label "recognition"
  ]
  node [
    id 959
    label "oznajmienie"
  ]
  node [
    id 960
    label "campaign"
  ]
  node [
    id 961
    label "causing"
  ]
  node [
    id 962
    label "bezproblemowy"
  ]
  node [
    id 963
    label "pismo"
  ]
  node [
    id 964
    label "przekazanie"
  ]
  node [
    id 965
    label "forward"
  ]
  node [
    id 966
    label "p&#243;j&#347;cie"
  ]
  node [
    id 967
    label "bed"
  ]
  node [
    id 968
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 969
    label "podnieci&#263;"
  ]
  node [
    id 970
    label "scena"
  ]
  node [
    id 971
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 972
    label "numer"
  ]
  node [
    id 973
    label "po&#380;ycie"
  ]
  node [
    id 974
    label "poj&#281;cie"
  ]
  node [
    id 975
    label "podniecenie"
  ]
  node [
    id 976
    label "nago&#347;&#263;"
  ]
  node [
    id 977
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 978
    label "fascyku&#322;"
  ]
  node [
    id 979
    label "seks"
  ]
  node [
    id 980
    label "podniecanie"
  ]
  node [
    id 981
    label "imisja"
  ]
  node [
    id 982
    label "zwyczaj"
  ]
  node [
    id 983
    label "rozmna&#380;anie"
  ]
  node [
    id 984
    label "ruch_frykcyjny"
  ]
  node [
    id 985
    label "ontologia"
  ]
  node [
    id 986
    label "na_pieska"
  ]
  node [
    id 987
    label "pozycja_misjonarska"
  ]
  node [
    id 988
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 989
    label "fragment"
  ]
  node [
    id 990
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 991
    label "z&#322;&#261;czenie"
  ]
  node [
    id 992
    label "gra_wst&#281;pna"
  ]
  node [
    id 993
    label "erotyka"
  ]
  node [
    id 994
    label "urzeczywistnienie"
  ]
  node [
    id 995
    label "baraszki"
  ]
  node [
    id 996
    label "certificate"
  ]
  node [
    id 997
    label "po&#380;&#261;danie"
  ]
  node [
    id 998
    label "wzw&#243;d"
  ]
  node [
    id 999
    label "arystotelizm"
  ]
  node [
    id 1000
    label "podnieca&#263;"
  ]
  node [
    id 1001
    label "term"
  ]
  node [
    id 1002
    label "leksem"
  ]
  node [
    id 1003
    label "wezwanie"
  ]
  node [
    id 1004
    label "patron"
  ]
  node [
    id 1005
    label "symetryczny"
  ]
  node [
    id 1006
    label "prawid&#322;owo"
  ]
  node [
    id 1007
    label "po&#380;&#261;dany"
  ]
  node [
    id 1008
    label "dobry"
  ]
  node [
    id 1009
    label "s&#322;usznie"
  ]
  node [
    id 1010
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1011
    label "zasadny"
  ]
  node [
    id 1012
    label "nale&#380;yty"
  ]
  node [
    id 1013
    label "solidny"
  ]
  node [
    id 1014
    label "dobroczynny"
  ]
  node [
    id 1015
    label "czw&#243;rka"
  ]
  node [
    id 1016
    label "spokojny"
  ]
  node [
    id 1017
    label "skuteczny"
  ]
  node [
    id 1018
    label "&#347;mieszny"
  ]
  node [
    id 1019
    label "mi&#322;y"
  ]
  node [
    id 1020
    label "grzeczny"
  ]
  node [
    id 1021
    label "powitanie"
  ]
  node [
    id 1022
    label "dobrze"
  ]
  node [
    id 1023
    label "ca&#322;y"
  ]
  node [
    id 1024
    label "zwrot"
  ]
  node [
    id 1025
    label "pomy&#347;lny"
  ]
  node [
    id 1026
    label "moralny"
  ]
  node [
    id 1027
    label "drogi"
  ]
  node [
    id 1028
    label "pozytywny"
  ]
  node [
    id 1029
    label "korzystny"
  ]
  node [
    id 1030
    label "pos&#322;uszny"
  ]
  node [
    id 1031
    label "symetrycznie"
  ]
  node [
    id 1032
    label "bystrzyca"
  ]
  node [
    id 1033
    label "wy&#347;cig"
  ]
  node [
    id 1034
    label "parametr"
  ]
  node [
    id 1035
    label "roll"
  ]
  node [
    id 1036
    label "linia"
  ]
  node [
    id 1037
    label "kierunek"
  ]
  node [
    id 1038
    label "d&#261;&#380;enie"
  ]
  node [
    id 1039
    label "przedbieg"
  ]
  node [
    id 1040
    label "konkurencja"
  ]
  node [
    id 1041
    label "pr&#261;d"
  ]
  node [
    id 1042
    label "ciek_wodny"
  ]
  node [
    id 1043
    label "kurs"
  ]
  node [
    id 1044
    label "syfon"
  ]
  node [
    id 1045
    label "pozycja"
  ]
  node [
    id 1046
    label "ruch"
  ]
  node [
    id 1047
    label "czo&#322;&#243;wka"
  ]
  node [
    id 1048
    label "mechanika"
  ]
  node [
    id 1049
    label "poruszenie"
  ]
  node [
    id 1050
    label "movement"
  ]
  node [
    id 1051
    label "myk"
  ]
  node [
    id 1052
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1053
    label "travel"
  ]
  node [
    id 1054
    label "kanciasty"
  ]
  node [
    id 1055
    label "commercial_enterprise"
  ]
  node [
    id 1056
    label "model"
  ]
  node [
    id 1057
    label "strumie&#324;"
  ]
  node [
    id 1058
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1059
    label "taktyka"
  ]
  node [
    id 1060
    label "apraksja"
  ]
  node [
    id 1061
    label "natural_process"
  ]
  node [
    id 1062
    label "d&#322;ugi"
  ]
  node [
    id 1063
    label "dyssypacja_energii"
  ]
  node [
    id 1064
    label "tumult"
  ]
  node [
    id 1065
    label "stopek"
  ]
  node [
    id 1066
    label "zmiana"
  ]
  node [
    id 1067
    label "manewr"
  ]
  node [
    id 1068
    label "lokomocja"
  ]
  node [
    id 1069
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1070
    label "komunikacja"
  ]
  node [
    id 1071
    label "drift"
  ]
  node [
    id 1072
    label "przebieg"
  ]
  node [
    id 1073
    label "legislacyjnie"
  ]
  node [
    id 1074
    label "nast&#281;pstwo"
  ]
  node [
    id 1075
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1076
    label "finisz"
  ]
  node [
    id 1077
    label "Formu&#322;a_1"
  ]
  node [
    id 1078
    label "zmagania"
  ]
  node [
    id 1079
    label "contest"
  ]
  node [
    id 1080
    label "celownik"
  ]
  node [
    id 1081
    label "lista_startowa"
  ]
  node [
    id 1082
    label "torowiec"
  ]
  node [
    id 1083
    label "start"
  ]
  node [
    id 1084
    label "rywalizacja"
  ]
  node [
    id 1085
    label "start_lotny"
  ]
  node [
    id 1086
    label "racing"
  ]
  node [
    id 1087
    label "prolog"
  ]
  node [
    id 1088
    label "lotny_finisz"
  ]
  node [
    id 1089
    label "zawody"
  ]
  node [
    id 1090
    label "premia_g&#243;rska"
  ]
  node [
    id 1091
    label "interakcja"
  ]
  node [
    id 1092
    label "firma"
  ]
  node [
    id 1093
    label "dyscyplina_sportowa"
  ]
  node [
    id 1094
    label "dob&#243;r_naturalny"
  ]
  node [
    id 1095
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1096
    label "zwy&#380;kowanie"
  ]
  node [
    id 1097
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1098
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1099
    label "zaj&#281;cia"
  ]
  node [
    id 1100
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1101
    label "trasa"
  ]
  node [
    id 1102
    label "rok"
  ]
  node [
    id 1103
    label "przeorientowywanie"
  ]
  node [
    id 1104
    label "przejazd"
  ]
  node [
    id 1105
    label "przeorientowywa&#263;"
  ]
  node [
    id 1106
    label "nauka"
  ]
  node [
    id 1107
    label "grupa"
  ]
  node [
    id 1108
    label "przeorientowanie"
  ]
  node [
    id 1109
    label "klasa"
  ]
  node [
    id 1110
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1111
    label "przeorientowa&#263;"
  ]
  node [
    id 1112
    label "manner"
  ]
  node [
    id 1113
    label "course"
  ]
  node [
    id 1114
    label "passage"
  ]
  node [
    id 1115
    label "zni&#380;kowanie"
  ]
  node [
    id 1116
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1117
    label "seria"
  ]
  node [
    id 1118
    label "stawka"
  ]
  node [
    id 1119
    label "way"
  ]
  node [
    id 1120
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1121
    label "spos&#243;b"
  ]
  node [
    id 1122
    label "deprecjacja"
  ]
  node [
    id 1123
    label "cedu&#322;a"
  ]
  node [
    id 1124
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1125
    label "drive"
  ]
  node [
    id 1126
    label "bearing"
  ]
  node [
    id 1127
    label "Lira"
  ]
  node [
    id 1128
    label "intencja"
  ]
  node [
    id 1129
    label "post&#281;powanie"
  ]
  node [
    id 1130
    label "kszta&#322;t"
  ]
  node [
    id 1131
    label "poprowadzi&#263;"
  ]
  node [
    id 1132
    label "cord"
  ]
  node [
    id 1133
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1134
    label "tract"
  ]
  node [
    id 1135
    label "materia&#322;_zecerski"
  ]
  node [
    id 1136
    label "curve"
  ]
  node [
    id 1137
    label "figura_geometryczna"
  ]
  node [
    id 1138
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1139
    label "jard"
  ]
  node [
    id 1140
    label "szczep"
  ]
  node [
    id 1141
    label "phreaker"
  ]
  node [
    id 1142
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1143
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1144
    label "prowadzi&#263;"
  ]
  node [
    id 1145
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1146
    label "access"
  ]
  node [
    id 1147
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 1148
    label "billing"
  ]
  node [
    id 1149
    label "granica"
  ]
  node [
    id 1150
    label "szpaler"
  ]
  node [
    id 1151
    label "sztrych"
  ]
  node [
    id 1152
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1153
    label "drzewo_genealogiczne"
  ]
  node [
    id 1154
    label "transporter"
  ]
  node [
    id 1155
    label "line"
  ]
  node [
    id 1156
    label "przew&#243;d"
  ]
  node [
    id 1157
    label "granice"
  ]
  node [
    id 1158
    label "kontakt"
  ]
  node [
    id 1159
    label "rz&#261;d"
  ]
  node [
    id 1160
    label "przewo&#378;nik"
  ]
  node [
    id 1161
    label "przystanek"
  ]
  node [
    id 1162
    label "linijka"
  ]
  node [
    id 1163
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1164
    label "coalescence"
  ]
  node [
    id 1165
    label "Ural"
  ]
  node [
    id 1166
    label "prowadzenie"
  ]
  node [
    id 1167
    label "tekst"
  ]
  node [
    id 1168
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1169
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1170
    label "koniec"
  ]
  node [
    id 1171
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1172
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1173
    label "przep&#322;yw"
  ]
  node [
    id 1174
    label "ideologia"
  ]
  node [
    id 1175
    label "apparent_motion"
  ]
  node [
    id 1176
    label "przyp&#322;yw"
  ]
  node [
    id 1177
    label "metoda"
  ]
  node [
    id 1178
    label "electricity"
  ]
  node [
    id 1179
    label "dreszcz"
  ]
  node [
    id 1180
    label "praktyka"
  ]
  node [
    id 1181
    label "system"
  ]
  node [
    id 1182
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1183
    label "debit"
  ]
  node [
    id 1184
    label "druk"
  ]
  node [
    id 1185
    label "wydawa&#263;"
  ]
  node [
    id 1186
    label "szermierka"
  ]
  node [
    id 1187
    label "spis"
  ]
  node [
    id 1188
    label "wyda&#263;"
  ]
  node [
    id 1189
    label "ustawienie"
  ]
  node [
    id 1190
    label "publikacja"
  ]
  node [
    id 1191
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 1192
    label "adres"
  ]
  node [
    id 1193
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1194
    label "rozmieszczenie"
  ]
  node [
    id 1195
    label "redaktor"
  ]
  node [
    id 1196
    label "awansowa&#263;"
  ]
  node [
    id 1197
    label "wojsko"
  ]
  node [
    id 1198
    label "awans"
  ]
  node [
    id 1199
    label "awansowanie"
  ]
  node [
    id 1200
    label "poster"
  ]
  node [
    id 1201
    label "le&#380;e&#263;"
  ]
  node [
    id 1202
    label "ko&#322;o"
  ]
  node [
    id 1203
    label "modalno&#347;&#263;"
  ]
  node [
    id 1204
    label "z&#261;b"
  ]
  node [
    id 1205
    label "skala"
  ]
  node [
    id 1206
    label "funkcjonowa&#263;"
  ]
  node [
    id 1207
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1208
    label "wymiar"
  ]
  node [
    id 1209
    label "zmienna"
  ]
  node [
    id 1210
    label "wielko&#347;&#263;"
  ]
  node [
    id 1211
    label "studia"
  ]
  node [
    id 1212
    label "bok"
  ]
  node [
    id 1213
    label "skr&#281;canie"
  ]
  node [
    id 1214
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1215
    label "orientowanie"
  ]
  node [
    id 1216
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1217
    label "zorientowanie"
  ]
  node [
    id 1218
    label "ty&#322;"
  ]
  node [
    id 1219
    label "zorientowa&#263;"
  ]
  node [
    id 1220
    label "g&#243;ra"
  ]
  node [
    id 1221
    label "orientowa&#263;"
  ]
  node [
    id 1222
    label "orientacja"
  ]
  node [
    id 1223
    label "prz&#243;d"
  ]
  node [
    id 1224
    label "skr&#281;cenie"
  ]
  node [
    id 1225
    label "facylitator"
  ]
  node [
    id 1226
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1227
    label "metodyka"
  ]
  node [
    id 1228
    label "brak"
  ]
  node [
    id 1229
    label "alpinizm"
  ]
  node [
    id 1230
    label "wst&#281;p"
  ]
  node [
    id 1231
    label "elita"
  ]
  node [
    id 1232
    label "film"
  ]
  node [
    id 1233
    label "rajd"
  ]
  node [
    id 1234
    label "poligrafia"
  ]
  node [
    id 1235
    label "latarka_czo&#322;owa"
  ]
  node [
    id 1236
    label "&#347;ciana"
  ]
  node [
    id 1237
    label "zderzenie"
  ]
  node [
    id 1238
    label "front"
  ]
  node [
    id 1239
    label "eliminacje"
  ]
  node [
    id 1240
    label "nurt"
  ]
  node [
    id 1241
    label "butelka"
  ]
  node [
    id 1242
    label "tunel"
  ]
  node [
    id 1243
    label "korytarz"
  ]
  node [
    id 1244
    label "przeszkoda"
  ]
  node [
    id 1245
    label "sump"
  ]
  node [
    id 1246
    label "utrudnienie"
  ]
  node [
    id 1247
    label "kanalizacja"
  ]
  node [
    id 1248
    label "kana&#322;"
  ]
  node [
    id 1249
    label "muszla"
  ]
  node [
    id 1250
    label "rura"
  ]
  node [
    id 1251
    label "pozostawi&#263;"
  ]
  node [
    id 1252
    label "wybaczy&#263;"
  ]
  node [
    id 1253
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1254
    label "screw"
  ]
  node [
    id 1255
    label "straci&#263;"
  ]
  node [
    id 1256
    label "porzuci&#263;"
  ]
  node [
    id 1257
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1258
    label "zabaczy&#263;"
  ]
  node [
    id 1259
    label "fuck"
  ]
  node [
    id 1260
    label "przesta&#263;"
  ]
  node [
    id 1261
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1262
    label "drop"
  ]
  node [
    id 1263
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1264
    label "leave_office"
  ]
  node [
    id 1265
    label "fail"
  ]
  node [
    id 1266
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1267
    label "obni&#380;y&#263;"
  ]
  node [
    id 1268
    label "zostawi&#263;"
  ]
  node [
    id 1269
    label "potani&#263;"
  ]
  node [
    id 1270
    label "evacuate"
  ]
  node [
    id 1271
    label "humiliate"
  ]
  node [
    id 1272
    label "leave"
  ]
  node [
    id 1273
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1274
    label "authorize"
  ]
  node [
    id 1275
    label "omin&#261;&#263;"
  ]
  node [
    id 1276
    label "unwrap"
  ]
  node [
    id 1277
    label "doprowadzi&#263;"
  ]
  node [
    id 1278
    label "skrzywdzi&#263;"
  ]
  node [
    id 1279
    label "shove"
  ]
  node [
    id 1280
    label "zaplanowa&#263;"
  ]
  node [
    id 1281
    label "shelve"
  ]
  node [
    id 1282
    label "zachowa&#263;"
  ]
  node [
    id 1283
    label "wyznaczy&#263;"
  ]
  node [
    id 1284
    label "liszy&#263;"
  ]
  node [
    id 1285
    label "zerwa&#263;"
  ]
  node [
    id 1286
    label "release"
  ]
  node [
    id 1287
    label "stworzy&#263;"
  ]
  node [
    id 1288
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1289
    label "zabra&#263;"
  ]
  node [
    id 1290
    label "zrezygnowa&#263;"
  ]
  node [
    id 1291
    label "permit"
  ]
  node [
    id 1292
    label "stracenie"
  ]
  node [
    id 1293
    label "zabi&#263;"
  ]
  node [
    id 1294
    label "forfeit"
  ]
  node [
    id 1295
    label "wytraci&#263;"
  ]
  node [
    id 1296
    label "waste"
  ]
  node [
    id 1297
    label "przegra&#263;"
  ]
  node [
    id 1298
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 1299
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 1300
    label "execute"
  ]
  node [
    id 1301
    label "post&#261;pi&#263;"
  ]
  node [
    id 1302
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1303
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1304
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1305
    label "zorganizowa&#263;"
  ]
  node [
    id 1306
    label "appoint"
  ]
  node [
    id 1307
    label "wystylizowa&#263;"
  ]
  node [
    id 1308
    label "cause"
  ]
  node [
    id 1309
    label "przerobi&#263;"
  ]
  node [
    id 1310
    label "nabra&#263;"
  ]
  node [
    id 1311
    label "make"
  ]
  node [
    id 1312
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1313
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1314
    label "wydali&#263;"
  ]
  node [
    id 1315
    label "udzieli&#263;"
  ]
  node [
    id 1316
    label "pu&#347;ci&#263;_p&#322;azem"
  ]
  node [
    id 1317
    label "posiada&#263;"
  ]
  node [
    id 1318
    label "potencja&#322;"
  ]
  node [
    id 1319
    label "zapomina&#263;"
  ]
  node [
    id 1320
    label "zapomnienie"
  ]
  node [
    id 1321
    label "zapominanie"
  ]
  node [
    id 1322
    label "ability"
  ]
  node [
    id 1323
    label "obliczeniowo"
  ]
  node [
    id 1324
    label "read"
  ]
  node [
    id 1325
    label "styl"
  ]
  node [
    id 1326
    label "postawi&#263;"
  ]
  node [
    id 1327
    label "write"
  ]
  node [
    id 1328
    label "donie&#347;&#263;"
  ]
  node [
    id 1329
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1330
    label "prasa"
  ]
  node [
    id 1331
    label "zafundowa&#263;"
  ]
  node [
    id 1332
    label "budowla"
  ]
  node [
    id 1333
    label "plant"
  ]
  node [
    id 1334
    label "uruchomi&#263;"
  ]
  node [
    id 1335
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 1336
    label "obra&#263;"
  ]
  node [
    id 1337
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 1338
    label "obstawi&#263;"
  ]
  node [
    id 1339
    label "post"
  ]
  node [
    id 1340
    label "oceni&#263;"
  ]
  node [
    id 1341
    label "stanowisko"
  ]
  node [
    id 1342
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 1343
    label "uczyni&#263;"
  ]
  node [
    id 1344
    label "znak"
  ]
  node [
    id 1345
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1346
    label "wytworzy&#263;"
  ]
  node [
    id 1347
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 1348
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1349
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 1350
    label "set"
  ]
  node [
    id 1351
    label "wskaza&#263;"
  ]
  node [
    id 1352
    label "przyzna&#263;"
  ]
  node [
    id 1353
    label "przedstawi&#263;"
  ]
  node [
    id 1354
    label "establish"
  ]
  node [
    id 1355
    label "stawi&#263;"
  ]
  node [
    id 1356
    label "create"
  ]
  node [
    id 1357
    label "specjalista_od_public_relations"
  ]
  node [
    id 1358
    label "wizerunek"
  ]
  node [
    id 1359
    label "przygotowa&#263;"
  ]
  node [
    id 1360
    label "przytacha&#263;"
  ]
  node [
    id 1361
    label "zanie&#347;&#263;"
  ]
  node [
    id 1362
    label "denounce"
  ]
  node [
    id 1363
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 1364
    label "serve"
  ]
  node [
    id 1365
    label "trzonek"
  ]
  node [
    id 1366
    label "reakcja"
  ]
  node [
    id 1367
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1368
    label "zachowanie"
  ]
  node [
    id 1369
    label "stylik"
  ]
  node [
    id 1370
    label "handle"
  ]
  node [
    id 1371
    label "stroke"
  ]
  node [
    id 1372
    label "natural_language"
  ]
  node [
    id 1373
    label "pisa&#263;"
  ]
  node [
    id 1374
    label "kanon"
  ]
  node [
    id 1375
    label "behawior"
  ]
  node [
    id 1376
    label "zesp&#243;&#322;"
  ]
  node [
    id 1377
    label "t&#322;oczysko"
  ]
  node [
    id 1378
    label "depesza"
  ]
  node [
    id 1379
    label "maszyna"
  ]
  node [
    id 1380
    label "media"
  ]
  node [
    id 1381
    label "czasopismo"
  ]
  node [
    id 1382
    label "dziennikarz_prasowy"
  ]
  node [
    id 1383
    label "kiosk"
  ]
  node [
    id 1384
    label "maszyna_rolnicza"
  ]
  node [
    id 1385
    label "gazeta"
  ]
  node [
    id 1386
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1387
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1388
    label "wn&#281;trze"
  ]
  node [
    id 1389
    label "wiedza"
  ]
  node [
    id 1390
    label "obiega&#263;"
  ]
  node [
    id 1391
    label "powzi&#281;cie"
  ]
  node [
    id 1392
    label "dane"
  ]
  node [
    id 1393
    label "obiegni&#281;cie"
  ]
  node [
    id 1394
    label "sygna&#322;"
  ]
  node [
    id 1395
    label "obieganie"
  ]
  node [
    id 1396
    label "powzi&#261;&#263;"
  ]
  node [
    id 1397
    label "obiec"
  ]
  node [
    id 1398
    label "doj&#347;cie"
  ]
  node [
    id 1399
    label "doj&#347;&#263;"
  ]
  node [
    id 1400
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1401
    label "superego"
  ]
  node [
    id 1402
    label "psychika"
  ]
  node [
    id 1403
    label "wyraz_pochodny"
  ]
  node [
    id 1404
    label "zboczenie"
  ]
  node [
    id 1405
    label "om&#243;wienie"
  ]
  node [
    id 1406
    label "omawia&#263;"
  ]
  node [
    id 1407
    label "forum"
  ]
  node [
    id 1408
    label "topik"
  ]
  node [
    id 1409
    label "tematyka"
  ]
  node [
    id 1410
    label "w&#261;tek"
  ]
  node [
    id 1411
    label "zbaczanie"
  ]
  node [
    id 1412
    label "forma"
  ]
  node [
    id 1413
    label "om&#243;wi&#263;"
  ]
  node [
    id 1414
    label "omawianie"
  ]
  node [
    id 1415
    label "otoczka"
  ]
  node [
    id 1416
    label "zbacza&#263;"
  ]
  node [
    id 1417
    label "zboczy&#263;"
  ]
  node [
    id 1418
    label "sklep"
  ]
  node [
    id 1419
    label "p&#243;&#322;ka"
  ]
  node [
    id 1420
    label "stoisko"
  ]
  node [
    id 1421
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 1422
    label "sk&#322;ad"
  ]
  node [
    id 1423
    label "obiekt_handlowy"
  ]
  node [
    id 1424
    label "zaplecze"
  ]
  node [
    id 1425
    label "witryna"
  ]
  node [
    id 1426
    label "prayer"
  ]
  node [
    id 1427
    label "twierdzenie"
  ]
  node [
    id 1428
    label "propozycja"
  ]
  node [
    id 1429
    label "my&#347;l"
  ]
  node [
    id 1430
    label "motion"
  ]
  node [
    id 1431
    label "wnioskowanie"
  ]
  node [
    id 1432
    label "szko&#322;a"
  ]
  node [
    id 1433
    label "p&#322;&#243;d"
  ]
  node [
    id 1434
    label "thinking"
  ]
  node [
    id 1435
    label "umys&#322;"
  ]
  node [
    id 1436
    label "political_orientation"
  ]
  node [
    id 1437
    label "pomys&#322;"
  ]
  node [
    id 1438
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1439
    label "fantomatyka"
  ]
  node [
    id 1440
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1441
    label "alternatywa_Fredholma"
  ]
  node [
    id 1442
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1443
    label "teoria"
  ]
  node [
    id 1444
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1445
    label "paradoks_Leontiefa"
  ]
  node [
    id 1446
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1447
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1448
    label "teza"
  ]
  node [
    id 1449
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1450
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1451
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1452
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1453
    label "twierdzenie_Maya"
  ]
  node [
    id 1454
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1455
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1456
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1457
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1458
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1459
    label "zapewnianie"
  ]
  node [
    id 1460
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1461
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1462
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1463
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1464
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1465
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1466
    label "twierdzenie_Cevy"
  ]
  node [
    id 1467
    label "twierdzenie_Pascala"
  ]
  node [
    id 1468
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1469
    label "komunikowanie"
  ]
  node [
    id 1470
    label "zasada"
  ]
  node [
    id 1471
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1472
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1473
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1474
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1475
    label "psychotest"
  ]
  node [
    id 1476
    label "wk&#322;ad"
  ]
  node [
    id 1477
    label "handwriting"
  ]
  node [
    id 1478
    label "przekaz"
  ]
  node [
    id 1479
    label "dzie&#322;o"
  ]
  node [
    id 1480
    label "paleograf"
  ]
  node [
    id 1481
    label "interpunkcja"
  ]
  node [
    id 1482
    label "dzia&#322;"
  ]
  node [
    id 1483
    label "grafia"
  ]
  node [
    id 1484
    label "egzemplarz"
  ]
  node [
    id 1485
    label "communication"
  ]
  node [
    id 1486
    label "script"
  ]
  node [
    id 1487
    label "zajawka"
  ]
  node [
    id 1488
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1489
    label "list"
  ]
  node [
    id 1490
    label "Zwrotnica"
  ]
  node [
    id 1491
    label "ok&#322;adka"
  ]
  node [
    id 1492
    label "ortografia"
  ]
  node [
    id 1493
    label "letter"
  ]
  node [
    id 1494
    label "paleografia"
  ]
  node [
    id 1495
    label "j&#281;zyk"
  ]
  node [
    id 1496
    label "proposal"
  ]
  node [
    id 1497
    label "proszenie"
  ]
  node [
    id 1498
    label "dochodzenie"
  ]
  node [
    id 1499
    label "proces_my&#347;lowy"
  ]
  node [
    id 1500
    label "lead"
  ]
  node [
    id 1501
    label "konkluzja"
  ]
  node [
    id 1502
    label "sk&#322;adanie"
  ]
  node [
    id 1503
    label "probe"
  ]
  node [
    id 1504
    label "przemy&#347;lenie"
  ]
  node [
    id 1505
    label "consideration"
  ]
  node [
    id 1506
    label "pomy&#347;lenie"
  ]
  node [
    id 1507
    label "namy&#347;lenie_si&#281;"
  ]
  node [
    id 1508
    label "skromny"
  ]
  node [
    id 1509
    label "po_prostu"
  ]
  node [
    id 1510
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 1511
    label "rozprostowanie"
  ]
  node [
    id 1512
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 1513
    label "prosto"
  ]
  node [
    id 1514
    label "prostowanie_si&#281;"
  ]
  node [
    id 1515
    label "niepozorny"
  ]
  node [
    id 1516
    label "cios"
  ]
  node [
    id 1517
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 1518
    label "naiwny"
  ]
  node [
    id 1519
    label "&#322;atwy"
  ]
  node [
    id 1520
    label "prostowanie"
  ]
  node [
    id 1521
    label "zwyk&#322;y"
  ]
  node [
    id 1522
    label "&#322;atwo"
  ]
  node [
    id 1523
    label "skromnie"
  ]
  node [
    id 1524
    label "elementarily"
  ]
  node [
    id 1525
    label "niepozornie"
  ]
  node [
    id 1526
    label "naturalnie"
  ]
  node [
    id 1527
    label "kszta&#322;towanie"
  ]
  node [
    id 1528
    label "korygowanie"
  ]
  node [
    id 1529
    label "rozk&#322;adanie"
  ]
  node [
    id 1530
    label "correction"
  ]
  node [
    id 1531
    label "adjustment"
  ]
  node [
    id 1532
    label "rozpostarcie"
  ]
  node [
    id 1533
    label "erecting"
  ]
  node [
    id 1534
    label "ukszta&#322;towanie"
  ]
  node [
    id 1535
    label "szaraczek"
  ]
  node [
    id 1536
    label "zwyczajny"
  ]
  node [
    id 1537
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 1538
    label "wstydliwy"
  ]
  node [
    id 1539
    label "niewa&#380;ny"
  ]
  node [
    id 1540
    label "niewymy&#347;lny"
  ]
  node [
    id 1541
    label "prawy"
  ]
  node [
    id 1542
    label "zrozumia&#322;y"
  ]
  node [
    id 1543
    label "immanentny"
  ]
  node [
    id 1544
    label "bezsporny"
  ]
  node [
    id 1545
    label "organicznie"
  ]
  node [
    id 1546
    label "pierwotny"
  ]
  node [
    id 1547
    label "neutralny"
  ]
  node [
    id 1548
    label "normalny"
  ]
  node [
    id 1549
    label "rzeczywisty"
  ]
  node [
    id 1550
    label "naiwnie"
  ]
  node [
    id 1551
    label "poczciwy"
  ]
  node [
    id 1552
    label "g&#322;upi"
  ]
  node [
    id 1553
    label "letki"
  ]
  node [
    id 1554
    label "&#322;acny"
  ]
  node [
    id 1555
    label "snadny"
  ]
  node [
    id 1556
    label "przyjemny"
  ]
  node [
    id 1557
    label "prostodusznie"
  ]
  node [
    id 1558
    label "przeci&#281;tny"
  ]
  node [
    id 1559
    label "zwyczajnie"
  ]
  node [
    id 1560
    label "zwykle"
  ]
  node [
    id 1561
    label "cz&#281;sty"
  ]
  node [
    id 1562
    label "blok"
  ]
  node [
    id 1563
    label "time"
  ]
  node [
    id 1564
    label "shot"
  ]
  node [
    id 1565
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 1566
    label "uderzenie"
  ]
  node [
    id 1567
    label "struktura_geologiczna"
  ]
  node [
    id 1568
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 1569
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 1570
    label "coup"
  ]
  node [
    id 1571
    label "siekacz"
  ]
  node [
    id 1572
    label "due"
  ]
  node [
    id 1573
    label "musie&#263;"
  ]
  node [
    id 1574
    label "need"
  ]
  node [
    id 1575
    label "pragn&#261;&#263;"
  ]
  node [
    id 1576
    label "take_care"
  ]
  node [
    id 1577
    label "troska&#263;_si&#281;"
  ]
  node [
    id 1578
    label "deliver"
  ]
  node [
    id 1579
    label "rozpatrywa&#263;"
  ]
  node [
    id 1580
    label "zamierza&#263;"
  ]
  node [
    id 1581
    label "argue"
  ]
  node [
    id 1582
    label "os&#261;dza&#263;"
  ]
  node [
    id 1583
    label "strike"
  ]
  node [
    id 1584
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1585
    label "znajdowa&#263;"
  ]
  node [
    id 1586
    label "hold"
  ]
  node [
    id 1587
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 1588
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 1589
    label "przeprowadza&#263;"
  ]
  node [
    id 1590
    label "consider"
  ]
  node [
    id 1591
    label "volunteer"
  ]
  node [
    id 1592
    label "organizowa&#263;"
  ]
  node [
    id 1593
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1594
    label "czyni&#263;"
  ]
  node [
    id 1595
    label "stylizowa&#263;"
  ]
  node [
    id 1596
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1597
    label "falowa&#263;"
  ]
  node [
    id 1598
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1599
    label "praca"
  ]
  node [
    id 1600
    label "wydala&#263;"
  ]
  node [
    id 1601
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1602
    label "tentegowa&#263;"
  ]
  node [
    id 1603
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1604
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1605
    label "oszukiwa&#263;"
  ]
  node [
    id 1606
    label "ukazywa&#263;"
  ]
  node [
    id 1607
    label "wpadni&#281;cie"
  ]
  node [
    id 1608
    label "regestr"
  ]
  node [
    id 1609
    label "wpa&#347;&#263;"
  ]
  node [
    id 1610
    label "note"
  ]
  node [
    id 1611
    label "partia"
  ]
  node [
    id 1612
    label "&#347;piewak_operowy"
  ]
  node [
    id 1613
    label "onomatopeja"
  ]
  node [
    id 1614
    label "decyzja"
  ]
  node [
    id 1615
    label "linia_melodyczna"
  ]
  node [
    id 1616
    label "sound"
  ]
  node [
    id 1617
    label "opinion"
  ]
  node [
    id 1618
    label "wpada&#263;"
  ]
  node [
    id 1619
    label "nakaz"
  ]
  node [
    id 1620
    label "matowie&#263;"
  ]
  node [
    id 1621
    label "foniatra"
  ]
  node [
    id 1622
    label "ch&#243;rzysta"
  ]
  node [
    id 1623
    label "mutacja"
  ]
  node [
    id 1624
    label "&#347;piewaczka"
  ]
  node [
    id 1625
    label "zmatowienie"
  ]
  node [
    id 1626
    label "wokal"
  ]
  node [
    id 1627
    label "emisja"
  ]
  node [
    id 1628
    label "zmatowie&#263;"
  ]
  node [
    id 1629
    label "&#347;piewak"
  ]
  node [
    id 1630
    label "matowienie"
  ]
  node [
    id 1631
    label "brzmienie"
  ]
  node [
    id 1632
    label "wpadanie"
  ]
  node [
    id 1633
    label "odm&#322;adzanie"
  ]
  node [
    id 1634
    label "liga"
  ]
  node [
    id 1635
    label "jednostka_systematyczna"
  ]
  node [
    id 1636
    label "gromada"
  ]
  node [
    id 1637
    label "Entuzjastki"
  ]
  node [
    id 1638
    label "kompozycja"
  ]
  node [
    id 1639
    label "Terranie"
  ]
  node [
    id 1640
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1641
    label "category"
  ]
  node [
    id 1642
    label "pakiet_klimatyczny"
  ]
  node [
    id 1643
    label "oddzia&#322;"
  ]
  node [
    id 1644
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1645
    label "cz&#261;steczka"
  ]
  node [
    id 1646
    label "stage_set"
  ]
  node [
    id 1647
    label "type"
  ]
  node [
    id 1648
    label "specgrupa"
  ]
  node [
    id 1649
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1650
    label "&#346;wietliki"
  ]
  node [
    id 1651
    label "odm&#322;odzenie"
  ]
  node [
    id 1652
    label "Eurogrupa"
  ]
  node [
    id 1653
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1654
    label "formacja_geologiczna"
  ]
  node [
    id 1655
    label "harcerze_starsi"
  ]
  node [
    id 1656
    label "statement"
  ]
  node [
    id 1657
    label "polecenie"
  ]
  node [
    id 1658
    label "bodziec"
  ]
  node [
    id 1659
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 1660
    label "management"
  ]
  node [
    id 1661
    label "resolution"
  ]
  node [
    id 1662
    label "zdecydowanie"
  ]
  node [
    id 1663
    label "Bund"
  ]
  node [
    id 1664
    label "PPR"
  ]
  node [
    id 1665
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1666
    label "wybranek"
  ]
  node [
    id 1667
    label "Jakobici"
  ]
  node [
    id 1668
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1669
    label "SLD"
  ]
  node [
    id 1670
    label "Razem"
  ]
  node [
    id 1671
    label "PiS"
  ]
  node [
    id 1672
    label "package"
  ]
  node [
    id 1673
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1674
    label "Kuomintang"
  ]
  node [
    id 1675
    label "ZSL"
  ]
  node [
    id 1676
    label "organizacja"
  ]
  node [
    id 1677
    label "AWS"
  ]
  node [
    id 1678
    label "gra"
  ]
  node [
    id 1679
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1680
    label "game"
  ]
  node [
    id 1681
    label "PO"
  ]
  node [
    id 1682
    label "si&#322;a"
  ]
  node [
    id 1683
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1684
    label "niedoczas"
  ]
  node [
    id 1685
    label "Federali&#347;ci"
  ]
  node [
    id 1686
    label "PSL"
  ]
  node [
    id 1687
    label "Wigowie"
  ]
  node [
    id 1688
    label "ZChN"
  ]
  node [
    id 1689
    label "egzekutywa"
  ]
  node [
    id 1690
    label "aktyw"
  ]
  node [
    id 1691
    label "wybranka"
  ]
  node [
    id 1692
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1693
    label "unit"
  ]
  node [
    id 1694
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 1695
    label "muzyk"
  ]
  node [
    id 1696
    label "phone"
  ]
  node [
    id 1697
    label "intonacja"
  ]
  node [
    id 1698
    label "modalizm"
  ]
  node [
    id 1699
    label "nadlecenie"
  ]
  node [
    id 1700
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1701
    label "solmizacja"
  ]
  node [
    id 1702
    label "dobiec"
  ]
  node [
    id 1703
    label "transmiter"
  ]
  node [
    id 1704
    label "heksachord"
  ]
  node [
    id 1705
    label "akcent"
  ]
  node [
    id 1706
    label "repetycja"
  ]
  node [
    id 1707
    label "pogl&#261;d"
  ]
  node [
    id 1708
    label "stawia&#263;"
  ]
  node [
    id 1709
    label "uprawianie"
  ]
  node [
    id 1710
    label "wakowa&#263;"
  ]
  node [
    id 1711
    label "Mazowsze"
  ]
  node [
    id 1712
    label "whole"
  ]
  node [
    id 1713
    label "skupienie"
  ]
  node [
    id 1714
    label "The_Beatles"
  ]
  node [
    id 1715
    label "zabudowania"
  ]
  node [
    id 1716
    label "group"
  ]
  node [
    id 1717
    label "zespolik"
  ]
  node [
    id 1718
    label "schorzenie"
  ]
  node [
    id 1719
    label "ro&#347;lina"
  ]
  node [
    id 1720
    label "Depeche_Mode"
  ]
  node [
    id 1721
    label "batch"
  ]
  node [
    id 1722
    label "decline"
  ]
  node [
    id 1723
    label "kolor"
  ]
  node [
    id 1724
    label "niszczy&#263;_si&#281;"
  ]
  node [
    id 1725
    label "dematerializowa&#263;_si&#281;"
  ]
  node [
    id 1726
    label "tarnish"
  ]
  node [
    id 1727
    label "przype&#322;za&#263;"
  ]
  node [
    id 1728
    label "bledn&#261;&#263;"
  ]
  node [
    id 1729
    label "burze&#263;"
  ]
  node [
    id 1730
    label "zbledn&#261;&#263;"
  ]
  node [
    id 1731
    label "pale"
  ]
  node [
    id 1732
    label "zniszczy&#263;_si&#281;"
  ]
  node [
    id 1733
    label "przype&#322;zn&#261;&#263;"
  ]
  node [
    id 1734
    label "laryngolog"
  ]
  node [
    id 1735
    label "expense"
  ]
  node [
    id 1736
    label "introdukcja"
  ]
  node [
    id 1737
    label "wydobywanie"
  ]
  node [
    id 1738
    label "przesy&#322;"
  ]
  node [
    id 1739
    label "consequence"
  ]
  node [
    id 1740
    label "wydzielanie"
  ]
  node [
    id 1741
    label "zniszczenie_si&#281;"
  ]
  node [
    id 1742
    label "przype&#322;&#378;ni&#281;cie"
  ]
  node [
    id 1743
    label "zja&#347;nienie"
  ]
  node [
    id 1744
    label "odbarwienie_si&#281;"
  ]
  node [
    id 1745
    label "przyt&#322;umiony"
  ]
  node [
    id 1746
    label "matowy"
  ]
  node [
    id 1747
    label "zmienienie"
  ]
  node [
    id 1748
    label "stanie_si&#281;"
  ]
  node [
    id 1749
    label "zga&#347;ni&#281;cie"
  ]
  node [
    id 1750
    label "wyblak&#322;y"
  ]
  node [
    id 1751
    label "variation"
  ]
  node [
    id 1752
    label "zaburzenie"
  ]
  node [
    id 1753
    label "operator"
  ]
  node [
    id 1754
    label "odmiana"
  ]
  node [
    id 1755
    label "variety"
  ]
  node [
    id 1756
    label "proces_fizjologiczny"
  ]
  node [
    id 1757
    label "zamiana"
  ]
  node [
    id 1758
    label "mutagenny"
  ]
  node [
    id 1759
    label "gen"
  ]
  node [
    id 1760
    label "ga&#347;ni&#281;cie"
  ]
  node [
    id 1761
    label "stawanie_si&#281;"
  ]
  node [
    id 1762
    label "burzenie"
  ]
  node [
    id 1763
    label "odbarwianie_si&#281;"
  ]
  node [
    id 1764
    label "przype&#322;zanie"
  ]
  node [
    id 1765
    label "ja&#347;nienie"
  ]
  node [
    id 1766
    label "niszczenie_si&#281;"
  ]
  node [
    id 1767
    label "choreuta"
  ]
  node [
    id 1768
    label "ch&#243;r"
  ]
  node [
    id 1769
    label "zaziera&#263;"
  ]
  node [
    id 1770
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1771
    label "spotyka&#263;"
  ]
  node [
    id 1772
    label "pogo"
  ]
  node [
    id 1773
    label "ogrom"
  ]
  node [
    id 1774
    label "zapach"
  ]
  node [
    id 1775
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 1776
    label "popada&#263;"
  ]
  node [
    id 1777
    label "odwiedza&#263;"
  ]
  node [
    id 1778
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1779
    label "przypomina&#263;"
  ]
  node [
    id 1780
    label "ujmowa&#263;"
  ]
  node [
    id 1781
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 1782
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1783
    label "chowa&#263;"
  ]
  node [
    id 1784
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 1785
    label "demaskowa&#263;"
  ]
  node [
    id 1786
    label "ulega&#263;"
  ]
  node [
    id 1787
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 1788
    label "emocja"
  ]
  node [
    id 1789
    label "flatten"
  ]
  node [
    id 1790
    label "delivery"
  ]
  node [
    id 1791
    label "impression"
  ]
  node [
    id 1792
    label "zadenuncjowanie"
  ]
  node [
    id 1793
    label "reszta"
  ]
  node [
    id 1794
    label "wytworzenie"
  ]
  node [
    id 1795
    label "issue"
  ]
  node [
    id 1796
    label "podanie"
  ]
  node [
    id 1797
    label "wprowadzenie"
  ]
  node [
    id 1798
    label "ujawnienie"
  ]
  node [
    id 1799
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 1800
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1801
    label "zrobienie"
  ]
  node [
    id 1802
    label "plon"
  ]
  node [
    id 1803
    label "surrender"
  ]
  node [
    id 1804
    label "kojarzy&#263;"
  ]
  node [
    id 1805
    label "wydawnictwo"
  ]
  node [
    id 1806
    label "wiano"
  ]
  node [
    id 1807
    label "produkcja"
  ]
  node [
    id 1808
    label "wprowadza&#263;"
  ]
  node [
    id 1809
    label "podawa&#263;"
  ]
  node [
    id 1810
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1811
    label "ujawnia&#263;"
  ]
  node [
    id 1812
    label "placard"
  ]
  node [
    id 1813
    label "powierza&#263;"
  ]
  node [
    id 1814
    label "denuncjowa&#263;"
  ]
  node [
    id 1815
    label "tajemnica"
  ]
  node [
    id 1816
    label "panna_na_wydaniu"
  ]
  node [
    id 1817
    label "train"
  ]
  node [
    id 1818
    label "wymy&#347;lenie"
  ]
  node [
    id 1819
    label "spotkanie"
  ]
  node [
    id 1820
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 1821
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 1822
    label "ulegni&#281;cie"
  ]
  node [
    id 1823
    label "collapse"
  ]
  node [
    id 1824
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1825
    label "poniesienie"
  ]
  node [
    id 1826
    label "ciecz"
  ]
  node [
    id 1827
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1828
    label "odwiedzenie"
  ]
  node [
    id 1829
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 1830
    label "rzeka"
  ]
  node [
    id 1831
    label "postrzeganie"
  ]
  node [
    id 1832
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 1833
    label "dostanie_si&#281;"
  ]
  node [
    id 1834
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1835
    label "rozbicie_si&#281;"
  ]
  node [
    id 1836
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 1837
    label "skojarzy&#263;"
  ]
  node [
    id 1838
    label "zadenuncjowa&#263;"
  ]
  node [
    id 1839
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 1840
    label "translate"
  ]
  node [
    id 1841
    label "poda&#263;"
  ]
  node [
    id 1842
    label "wprowadzi&#263;"
  ]
  node [
    id 1843
    label "ujawni&#263;"
  ]
  node [
    id 1844
    label "uleganie"
  ]
  node [
    id 1845
    label "dostawanie_si&#281;"
  ]
  node [
    id 1846
    label "odwiedzanie"
  ]
  node [
    id 1847
    label "spotykanie"
  ]
  node [
    id 1848
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1849
    label "wymy&#347;lanie"
  ]
  node [
    id 1850
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 1851
    label "ingress"
  ]
  node [
    id 1852
    label "wp&#322;ywanie"
  ]
  node [
    id 1853
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 1854
    label "overlap"
  ]
  node [
    id 1855
    label "wkl&#281;sanie"
  ]
  node [
    id 1856
    label "ulec"
  ]
  node [
    id 1857
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 1858
    label "fall_upon"
  ]
  node [
    id 1859
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 1860
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1861
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 1862
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1863
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 1864
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1865
    label "spotka&#263;"
  ]
  node [
    id 1866
    label "odwiedzi&#263;"
  ]
  node [
    id 1867
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 1868
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 1869
    label "catalog"
  ]
  node [
    id 1870
    label "stock"
  ]
  node [
    id 1871
    label "sumariusz"
  ]
  node [
    id 1872
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 1873
    label "check"
  ]
  node [
    id 1874
    label "book"
  ]
  node [
    id 1875
    label "figurowa&#263;"
  ]
  node [
    id 1876
    label "rejestr"
  ]
  node [
    id 1877
    label "wyliczanka"
  ]
  node [
    id 1878
    label "instrumentacja_g&#322;oskowa"
  ]
  node [
    id 1879
    label "figura_s&#322;&#243;w"
  ]
  node [
    id 1880
    label "&#347;piew"
  ]
  node [
    id 1881
    label "wyra&#380;anie"
  ]
  node [
    id 1882
    label "wydawanie"
  ]
  node [
    id 1883
    label "spirit"
  ]
  node [
    id 1884
    label "kolorystyka"
  ]
  node [
    id 1885
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1886
    label "dzisiejszo"
  ]
  node [
    id 1887
    label "jednoczesny"
  ]
  node [
    id 1888
    label "unowocze&#347;nianie"
  ]
  node [
    id 1889
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 1890
    label "tera&#378;niejszy"
  ]
  node [
    id 1891
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 1892
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 1893
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 1894
    label "rozmowa"
  ]
  node [
    id 1895
    label "sympozjon"
  ]
  node [
    id 1896
    label "conference"
  ]
  node [
    id 1897
    label "cisza"
  ]
  node [
    id 1898
    label "odpowied&#378;"
  ]
  node [
    id 1899
    label "rozhowor"
  ]
  node [
    id 1900
    label "discussion"
  ]
  node [
    id 1901
    label "esej"
  ]
  node [
    id 1902
    label "sympozjarcha"
  ]
  node [
    id 1903
    label "rozrywka"
  ]
  node [
    id 1904
    label "symposium"
  ]
  node [
    id 1905
    label "przyj&#281;cie"
  ]
  node [
    id 1906
    label "utw&#243;r"
  ]
  node [
    id 1907
    label "konferencja"
  ]
  node [
    id 1908
    label "jedyny"
  ]
  node [
    id 1909
    label "du&#380;y"
  ]
  node [
    id 1910
    label "zdr&#243;w"
  ]
  node [
    id 1911
    label "calu&#347;ko"
  ]
  node [
    id 1912
    label "kompletny"
  ]
  node [
    id 1913
    label "pe&#322;ny"
  ]
  node [
    id 1914
    label "podobny"
  ]
  node [
    id 1915
    label "ca&#322;o"
  ]
  node [
    id 1916
    label "od&#322;am"
  ]
  node [
    id 1917
    label "siedziba"
  ]
  node [
    id 1918
    label "society"
  ]
  node [
    id 1919
    label "bar"
  ]
  node [
    id 1920
    label "jakobini"
  ]
  node [
    id 1921
    label "lokal"
  ]
  node [
    id 1922
    label "stowarzyszenie"
  ]
  node [
    id 1923
    label "klubista"
  ]
  node [
    id 1924
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 1925
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1926
    label "Chewra_Kadisza"
  ]
  node [
    id 1927
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 1928
    label "Rotary_International"
  ]
  node [
    id 1929
    label "fabianie"
  ]
  node [
    id 1930
    label "Eleusis"
  ]
  node [
    id 1931
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 1932
    label "Monar"
  ]
  node [
    id 1933
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 1934
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 1935
    label "&#321;ubianka"
  ]
  node [
    id 1936
    label "miejsce_pracy"
  ]
  node [
    id 1937
    label "dzia&#322;_personalny"
  ]
  node [
    id 1938
    label "Kreml"
  ]
  node [
    id 1939
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1940
    label "budynek"
  ]
  node [
    id 1941
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1942
    label "sadowisko"
  ]
  node [
    id 1943
    label "kawa&#322;"
  ]
  node [
    id 1944
    label "bry&#322;a"
  ]
  node [
    id 1945
    label "section"
  ]
  node [
    id 1946
    label "gastronomia"
  ]
  node [
    id 1947
    label "zak&#322;ad"
  ]
  node [
    id 1948
    label "lada"
  ]
  node [
    id 1949
    label "blat"
  ]
  node [
    id 1950
    label "berylowiec"
  ]
  node [
    id 1951
    label "milibar"
  ]
  node [
    id 1952
    label "kawiarnia"
  ]
  node [
    id 1953
    label "buffet"
  ]
  node [
    id 1954
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1955
    label "mikrobar"
  ]
  node [
    id 1956
    label "pomaga&#263;"
  ]
  node [
    id 1957
    label "unbosom"
  ]
  node [
    id 1958
    label "uzasadnia&#263;"
  ]
  node [
    id 1959
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1960
    label "explain"
  ]
  node [
    id 1961
    label "aid"
  ]
  node [
    id 1962
    label "u&#322;atwia&#263;"
  ]
  node [
    id 1963
    label "concur"
  ]
  node [
    id 1964
    label "sprzyja&#263;"
  ]
  node [
    id 1965
    label "skutkowa&#263;"
  ]
  node [
    id 1966
    label "digest"
  ]
  node [
    id 1967
    label "Warszawa"
  ]
  node [
    id 1968
    label "back"
  ]
  node [
    id 1969
    label "modyfikacja"
  ]
  node [
    id 1970
    label "ustawa"
  ]
  node [
    id 1971
    label "story"
  ]
  node [
    id 1972
    label "amendment"
  ]
  node [
    id 1973
    label "modification"
  ]
  node [
    id 1974
    label "przer&#243;bka"
  ]
  node [
    id 1975
    label "przystosowanie"
  ]
  node [
    id 1976
    label "Karta_Nauczyciela"
  ]
  node [
    id 1977
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 1978
    label "przej&#347;&#263;"
  ]
  node [
    id 1979
    label "charter"
  ]
  node [
    id 1980
    label "marc&#243;wka"
  ]
  node [
    id 1981
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 1982
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1983
    label "chron"
  ]
  node [
    id 1984
    label "minute"
  ]
  node [
    id 1985
    label "jednostka_geologiczna"
  ]
  node [
    id 1986
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1987
    label "sprzeciwi&#263;_si&#281;"
  ]
  node [
    id 1988
    label "thank"
  ]
  node [
    id 1989
    label "opracowa&#263;"
  ]
  node [
    id 1990
    label "marshal"
  ]
  node [
    id 1991
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 1992
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 1993
    label "fold"
  ]
  node [
    id 1994
    label "zebra&#263;"
  ]
  node [
    id 1995
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1996
    label "jell"
  ]
  node [
    id 1997
    label "frame"
  ]
  node [
    id 1998
    label "scali&#263;"
  ]
  node [
    id 1999
    label "odda&#263;"
  ]
  node [
    id 2000
    label "pay"
  ]
  node [
    id 2001
    label "zestaw"
  ]
  node [
    id 2002
    label "suffice"
  ]
  node [
    id 2003
    label "stan&#261;&#263;"
  ]
  node [
    id 2004
    label "zaspokoi&#263;"
  ]
  node [
    id 2005
    label "dosta&#263;"
  ]
  node [
    id 2006
    label "satisfy"
  ]
  node [
    id 2007
    label "p&#243;j&#347;&#263;_do_&#322;&#243;&#380;ka"
  ]
  node [
    id 2008
    label "napoi&#263;_si&#281;"
  ]
  node [
    id 2009
    label "zadowoli&#263;"
  ]
  node [
    id 2010
    label "reserve"
  ]
  node [
    id 2011
    label "originate"
  ]
  node [
    id 2012
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 2013
    label "zatrzyma&#263;_si&#281;"
  ]
  node [
    id 2014
    label "przyby&#263;"
  ]
  node [
    id 2015
    label "obj&#261;&#263;"
  ]
  node [
    id 2016
    label "unie&#347;&#263;_si&#281;"
  ]
  node [
    id 2017
    label "przyj&#261;&#263;"
  ]
  node [
    id 2018
    label "zosta&#263;"
  ]
  node [
    id 2019
    label "zapanowa&#263;"
  ]
  node [
    id 2020
    label "develop"
  ]
  node [
    id 2021
    label "nabawienie_si&#281;"
  ]
  node [
    id 2022
    label "obskoczy&#263;"
  ]
  node [
    id 2023
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 2024
    label "zwiastun"
  ]
  node [
    id 2025
    label "doczeka&#263;"
  ]
  node [
    id 2026
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 2027
    label "kupi&#263;"
  ]
  node [
    id 2028
    label "wysta&#263;"
  ]
  node [
    id 2029
    label "wzi&#261;&#263;"
  ]
  node [
    id 2030
    label "naby&#263;"
  ]
  node [
    id 2031
    label "nabawianie_si&#281;"
  ]
  node [
    id 2032
    label "range"
  ]
  node [
    id 2033
    label "uzyska&#263;"
  ]
  node [
    id 2034
    label "comment"
  ]
  node [
    id 2035
    label "ocena"
  ]
  node [
    id 2036
    label "interpretacja"
  ]
  node [
    id 2037
    label "artyku&#322;"
  ]
  node [
    id 2038
    label "audycja"
  ]
  node [
    id 2039
    label "gossip"
  ]
  node [
    id 2040
    label "ekscerpcja"
  ]
  node [
    id 2041
    label "j&#281;zykowo"
  ]
  node [
    id 2042
    label "redakcja"
  ]
  node [
    id 2043
    label "pomini&#281;cie"
  ]
  node [
    id 2044
    label "preparacja"
  ]
  node [
    id 2045
    label "odmianka"
  ]
  node [
    id 2046
    label "koniektura"
  ]
  node [
    id 2047
    label "obelga"
  ]
  node [
    id 2048
    label "sofcik"
  ]
  node [
    id 2049
    label "kryterium"
  ]
  node [
    id 2050
    label "appraisal"
  ]
  node [
    id 2051
    label "explanation"
  ]
  node [
    id 2052
    label "hermeneutyka"
  ]
  node [
    id 2053
    label "wypracowanie"
  ]
  node [
    id 2054
    label "realizacja"
  ]
  node [
    id 2055
    label "interpretation"
  ]
  node [
    id 2056
    label "obja&#347;nienie"
  ]
  node [
    id 2057
    label "program"
  ]
  node [
    id 2058
    label "prawda"
  ]
  node [
    id 2059
    label "znak_j&#281;zykowy"
  ]
  node [
    id 2060
    label "nag&#322;&#243;wek"
  ]
  node [
    id 2061
    label "szkic"
  ]
  node [
    id 2062
    label "wyr&#243;b"
  ]
  node [
    id 2063
    label "rodzajnik"
  ]
  node [
    id 2064
    label "towar"
  ]
  node [
    id 2065
    label "paragraf"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 253
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 252
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 406
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 787
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 223
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 15
    target 481
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 15
    target 849
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 852
  ]
  edge [
    source 15
    target 289
  ]
  edge [
    source 15
    target 853
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 856
  ]
  edge [
    source 15
    target 857
  ]
  edge [
    source 15
    target 858
  ]
  edge [
    source 15
    target 859
  ]
  edge [
    source 15
    target 860
  ]
  edge [
    source 15
    target 861
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 232
  ]
  edge [
    source 15
    target 863
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 349
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 17
    target 57
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 515
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 252
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 365
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 917
  ]
  edge [
    source 20
    target 918
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 57
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 38
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 923
  ]
  edge [
    source 22
    target 924
  ]
  edge [
    source 22
    target 925
  ]
  edge [
    source 22
    target 926
  ]
  edge [
    source 22
    target 927
  ]
  edge [
    source 22
    target 928
  ]
  edge [
    source 22
    target 929
  ]
  edge [
    source 22
    target 930
  ]
  edge [
    source 22
    target 931
  ]
  edge [
    source 22
    target 932
  ]
  edge [
    source 22
    target 933
  ]
  edge [
    source 22
    target 934
  ]
  edge [
    source 22
    target 935
  ]
  edge [
    source 22
    target 936
  ]
  edge [
    source 22
    target 937
  ]
  edge [
    source 22
    target 938
  ]
  edge [
    source 22
    target 939
  ]
  edge [
    source 22
    target 940
  ]
  edge [
    source 22
    target 941
  ]
  edge [
    source 22
    target 39
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 45
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 942
  ]
  edge [
    source 25
    target 943
  ]
  edge [
    source 25
    target 944
  ]
  edge [
    source 25
    target 945
  ]
  edge [
    source 25
    target 946
  ]
  edge [
    source 25
    target 45
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 947
  ]
  edge [
    source 26
    target 948
  ]
  edge [
    source 26
    target 949
  ]
  edge [
    source 26
    target 950
  ]
  edge [
    source 26
    target 951
  ]
  edge [
    source 26
    target 952
  ]
  edge [
    source 26
    target 953
  ]
  edge [
    source 26
    target 954
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 955
  ]
  edge [
    source 26
    target 956
  ]
  edge [
    source 26
    target 957
  ]
  edge [
    source 26
    target 958
  ]
  edge [
    source 26
    target 959
  ]
  edge [
    source 26
    target 960
  ]
  edge [
    source 26
    target 961
  ]
  edge [
    source 26
    target 644
  ]
  edge [
    source 26
    target 962
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 26
    target 963
  ]
  edge [
    source 26
    target 964
  ]
  edge [
    source 26
    target 965
  ]
  edge [
    source 26
    target 223
  ]
  edge [
    source 26
    target 791
  ]
  edge [
    source 26
    target 966
  ]
  edge [
    source 26
    target 967
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 968
  ]
  edge [
    source 26
    target 969
  ]
  edge [
    source 26
    target 970
  ]
  edge [
    source 26
    target 971
  ]
  edge [
    source 26
    target 972
  ]
  edge [
    source 26
    target 973
  ]
  edge [
    source 26
    target 974
  ]
  edge [
    source 26
    target 975
  ]
  edge [
    source 26
    target 976
  ]
  edge [
    source 26
    target 977
  ]
  edge [
    source 26
    target 978
  ]
  edge [
    source 26
    target 979
  ]
  edge [
    source 26
    target 980
  ]
  edge [
    source 26
    target 981
  ]
  edge [
    source 26
    target 982
  ]
  edge [
    source 26
    target 983
  ]
  edge [
    source 26
    target 984
  ]
  edge [
    source 26
    target 985
  ]
  edge [
    source 26
    target 986
  ]
  edge [
    source 26
    target 987
  ]
  edge [
    source 26
    target 988
  ]
  edge [
    source 26
    target 989
  ]
  edge [
    source 26
    target 990
  ]
  edge [
    source 26
    target 991
  ]
  edge [
    source 26
    target 992
  ]
  edge [
    source 26
    target 993
  ]
  edge [
    source 26
    target 994
  ]
  edge [
    source 26
    target 995
  ]
  edge [
    source 26
    target 996
  ]
  edge [
    source 26
    target 997
  ]
  edge [
    source 26
    target 998
  ]
  edge [
    source 26
    target 895
  ]
  edge [
    source 26
    target 365
  ]
  edge [
    source 26
    target 920
  ]
  edge [
    source 26
    target 999
  ]
  edge [
    source 26
    target 1000
  ]
  edge [
    source 26
    target 1001
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 1002
  ]
  edge [
    source 26
    target 1003
  ]
  edge [
    source 26
    target 1004
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 473
  ]
  edge [
    source 27
    target 1005
  ]
  edge [
    source 27
    target 1006
  ]
  edge [
    source 27
    target 1007
  ]
  edge [
    source 27
    target 1008
  ]
  edge [
    source 27
    target 1009
  ]
  edge [
    source 27
    target 1010
  ]
  edge [
    source 27
    target 1011
  ]
  edge [
    source 27
    target 1012
  ]
  edge [
    source 27
    target 678
  ]
  edge [
    source 27
    target 1013
  ]
  edge [
    source 27
    target 1014
  ]
  edge [
    source 27
    target 1015
  ]
  edge [
    source 27
    target 1016
  ]
  edge [
    source 27
    target 1017
  ]
  edge [
    source 27
    target 1018
  ]
  edge [
    source 27
    target 1019
  ]
  edge [
    source 27
    target 1020
  ]
  edge [
    source 27
    target 1021
  ]
  edge [
    source 27
    target 1022
  ]
  edge [
    source 27
    target 1023
  ]
  edge [
    source 27
    target 1024
  ]
  edge [
    source 27
    target 1025
  ]
  edge [
    source 27
    target 1026
  ]
  edge [
    source 27
    target 1027
  ]
  edge [
    source 27
    target 1028
  ]
  edge [
    source 27
    target 295
  ]
  edge [
    source 27
    target 1029
  ]
  edge [
    source 27
    target 1030
  ]
  edge [
    source 27
    target 1031
  ]
  edge [
    source 28
    target 1032
  ]
  edge [
    source 28
    target 1033
  ]
  edge [
    source 28
    target 686
  ]
  edge [
    source 28
    target 1034
  ]
  edge [
    source 28
    target 1035
  ]
  edge [
    source 28
    target 1036
  ]
  edge [
    source 28
    target 681
  ]
  edge [
    source 28
    target 1037
  ]
  edge [
    source 28
    target 502
  ]
  edge [
    source 28
    target 1038
  ]
  edge [
    source 28
    target 1039
  ]
  edge [
    source 28
    target 1040
  ]
  edge [
    source 28
    target 1041
  ]
  edge [
    source 28
    target 1042
  ]
  edge [
    source 28
    target 1043
  ]
  edge [
    source 28
    target 116
  ]
  edge [
    source 28
    target 1044
  ]
  edge [
    source 28
    target 1045
  ]
  edge [
    source 28
    target 1046
  ]
  edge [
    source 28
    target 684
  ]
  edge [
    source 28
    target 1047
  ]
  edge [
    source 28
    target 1048
  ]
  edge [
    source 28
    target 582
  ]
  edge [
    source 28
    target 60
  ]
  edge [
    source 28
    target 1049
  ]
  edge [
    source 28
    target 1050
  ]
  edge [
    source 28
    target 1051
  ]
  edge [
    source 28
    target 584
  ]
  edge [
    source 28
    target 1052
  ]
  edge [
    source 28
    target 495
  ]
  edge [
    source 28
    target 588
  ]
  edge [
    source 28
    target 1053
  ]
  edge [
    source 28
    target 1054
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 487
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 532
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 591
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 246
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 28
    target 820
  ]
  edge [
    source 28
    target 1093
  ]
  edge [
    source 28
    target 1094
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 1096
  ]
  edge [
    source 28
    target 1097
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 1099
  ]
  edge [
    source 28
    target 1100
  ]
  edge [
    source 28
    target 1101
  ]
  edge [
    source 28
    target 1102
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1104
  ]
  edge [
    source 28
    target 1105
  ]
  edge [
    source 28
    target 1106
  ]
  edge [
    source 28
    target 1107
  ]
  edge [
    source 28
    target 1108
  ]
  edge [
    source 28
    target 1109
  ]
  edge [
    source 28
    target 1110
  ]
  edge [
    source 28
    target 1111
  ]
  edge [
    source 28
    target 1112
  ]
  edge [
    source 28
    target 1113
  ]
  edge [
    source 28
    target 1114
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 1116
  ]
  edge [
    source 28
    target 1117
  ]
  edge [
    source 28
    target 1118
  ]
  edge [
    source 28
    target 1119
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 28
    target 1121
  ]
  edge [
    source 28
    target 1122
  ]
  edge [
    source 28
    target 1123
  ]
  edge [
    source 28
    target 1124
  ]
  edge [
    source 28
    target 1125
  ]
  edge [
    source 28
    target 1126
  ]
  edge [
    source 28
    target 1127
  ]
  edge [
    source 28
    target 1128
  ]
  edge [
    source 28
    target 960
  ]
  edge [
    source 28
    target 1129
  ]
  edge [
    source 28
    target 1130
  ]
  edge [
    source 28
    target 742
  ]
  edge [
    source 28
    target 481
  ]
  edge [
    source 28
    target 1131
  ]
  edge [
    source 28
    target 1132
  ]
  edge [
    source 28
    target 406
  ]
  edge [
    source 28
    target 1133
  ]
  edge [
    source 28
    target 1134
  ]
  edge [
    source 28
    target 1135
  ]
  edge [
    source 28
    target 756
  ]
  edge [
    source 28
    target 1136
  ]
  edge [
    source 28
    target 1137
  ]
  edge [
    source 28
    target 866
  ]
  edge [
    source 28
    target 235
  ]
  edge [
    source 28
    target 1138
  ]
  edge [
    source 28
    target 1139
  ]
  edge [
    source 28
    target 1140
  ]
  edge [
    source 28
    target 1141
  ]
  edge [
    source 28
    target 1142
  ]
  edge [
    source 28
    target 1143
  ]
  edge [
    source 28
    target 1144
  ]
  edge [
    source 28
    target 1145
  ]
  edge [
    source 28
    target 1146
  ]
  edge [
    source 28
    target 1147
  ]
  edge [
    source 28
    target 1148
  ]
  edge [
    source 28
    target 1149
  ]
  edge [
    source 28
    target 1150
  ]
  edge [
    source 28
    target 1151
  ]
  edge [
    source 28
    target 1152
  ]
  edge [
    source 28
    target 1153
  ]
  edge [
    source 28
    target 1154
  ]
  edge [
    source 28
    target 1155
  ]
  edge [
    source 28
    target 989
  ]
  edge [
    source 28
    target 875
  ]
  edge [
    source 28
    target 1156
  ]
  edge [
    source 28
    target 877
  ]
  edge [
    source 28
    target 1157
  ]
  edge [
    source 28
    target 1158
  ]
  edge [
    source 28
    target 1159
  ]
  edge [
    source 28
    target 1160
  ]
  edge [
    source 28
    target 1161
  ]
  edge [
    source 28
    target 1162
  ]
  edge [
    source 28
    target 1163
  ]
  edge [
    source 28
    target 1164
  ]
  edge [
    source 28
    target 1165
  ]
  edge [
    source 28
    target 879
  ]
  edge [
    source 28
    target 1166
  ]
  edge [
    source 28
    target 1167
  ]
  edge [
    source 28
    target 1168
  ]
  edge [
    source 28
    target 1169
  ]
  edge [
    source 28
    target 1170
  ]
  edge [
    source 28
    target 1171
  ]
  edge [
    source 28
    target 1172
  ]
  edge [
    source 28
    target 714
  ]
  edge [
    source 28
    target 1173
  ]
  edge [
    source 28
    target 1174
  ]
  edge [
    source 28
    target 1175
  ]
  edge [
    source 28
    target 1176
  ]
  edge [
    source 28
    target 1177
  ]
  edge [
    source 28
    target 1178
  ]
  edge [
    source 28
    target 1179
  ]
  edge [
    source 28
    target 1180
  ]
  edge [
    source 28
    target 1181
  ]
  edge [
    source 28
    target 1182
  ]
  edge [
    source 28
    target 1183
  ]
  edge [
    source 28
    target 1184
  ]
  edge [
    source 28
    target 407
  ]
  edge [
    source 28
    target 891
  ]
  edge [
    source 28
    target 1185
  ]
  edge [
    source 28
    target 1186
  ]
  edge [
    source 28
    target 1187
  ]
  edge [
    source 28
    target 1188
  ]
  edge [
    source 28
    target 1189
  ]
  edge [
    source 28
    target 1190
  ]
  edge [
    source 28
    target 592
  ]
  edge [
    source 28
    target 442
  ]
  edge [
    source 28
    target 1191
  ]
  edge [
    source 28
    target 1192
  ]
  edge [
    source 28
    target 1193
  ]
  edge [
    source 28
    target 1194
  ]
  edge [
    source 28
    target 1195
  ]
  edge [
    source 28
    target 1196
  ]
  edge [
    source 28
    target 1197
  ]
  edge [
    source 28
    target 791
  ]
  edge [
    source 28
    target 1198
  ]
  edge [
    source 28
    target 1199
  ]
  edge [
    source 28
    target 1200
  ]
  edge [
    source 28
    target 1201
  ]
  edge [
    source 28
    target 1202
  ]
  edge [
    source 28
    target 1203
  ]
  edge [
    source 28
    target 1204
  ]
  edge [
    source 28
    target 606
  ]
  edge [
    source 28
    target 1205
  ]
  edge [
    source 28
    target 1206
  ]
  edge [
    source 28
    target 1207
  ]
  edge [
    source 28
    target 620
  ]
  edge [
    source 28
    target 1208
  ]
  edge [
    source 28
    target 1209
  ]
  edge [
    source 28
    target 863
  ]
  edge [
    source 28
    target 1210
  ]
  edge [
    source 28
    target 1211
  ]
  edge [
    source 28
    target 1212
  ]
  edge [
    source 28
    target 1213
  ]
  edge [
    source 28
    target 1214
  ]
  edge [
    source 28
    target 1215
  ]
  edge [
    source 28
    target 1216
  ]
  edge [
    source 28
    target 1217
  ]
  edge [
    source 28
    target 1218
  ]
  edge [
    source 28
    target 1219
  ]
  edge [
    source 28
    target 1220
  ]
  edge [
    source 28
    target 1221
  ]
  edge [
    source 28
    target 1222
  ]
  edge [
    source 28
    target 1223
  ]
  edge [
    source 28
    target 1224
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 1225
  ]
  edge [
    source 28
    target 1226
  ]
  edge [
    source 28
    target 1227
  ]
  edge [
    source 28
    target 1228
  ]
  edge [
    source 28
    target 890
  ]
  edge [
    source 28
    target 1229
  ]
  edge [
    source 28
    target 1230
  ]
  edge [
    source 28
    target 1231
  ]
  edge [
    source 28
    target 1232
  ]
  edge [
    source 28
    target 1233
  ]
  edge [
    source 28
    target 1234
  ]
  edge [
    source 28
    target 750
  ]
  edge [
    source 28
    target 1235
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 1237
  ]
  edge [
    source 28
    target 1238
  ]
  edge [
    source 28
    target 1239
  ]
  edge [
    source 28
    target 1240
  ]
  edge [
    source 28
    target 1241
  ]
  edge [
    source 28
    target 1242
  ]
  edge [
    source 28
    target 1243
  ]
  edge [
    source 28
    target 1244
  ]
  edge [
    source 28
    target 1245
  ]
  edge [
    source 28
    target 1246
  ]
  edge [
    source 28
    target 1247
  ]
  edge [
    source 28
    target 1248
  ]
  edge [
    source 28
    target 1249
  ]
  edge [
    source 28
    target 1250
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 29
    target 56
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1251
  ]
  edge [
    source 32
    target 1252
  ]
  edge [
    source 32
    target 1253
  ]
  edge [
    source 32
    target 349
  ]
  edge [
    source 32
    target 1254
  ]
  edge [
    source 32
    target 1255
  ]
  edge [
    source 32
    target 1256
  ]
  edge [
    source 32
    target 1257
  ]
  edge [
    source 32
    target 1258
  ]
  edge [
    source 32
    target 1259
  ]
  edge [
    source 32
    target 1260
  ]
  edge [
    source 32
    target 1261
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 32
    target 1262
  ]
  edge [
    source 32
    target 1263
  ]
  edge [
    source 32
    target 1264
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1167
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 1188
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 103
  ]
  edge [
    source 32
    target 322
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 326
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 329
  ]
  edge [
    source 32
    target 360
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 32
    target 1297
  ]
  edge [
    source 32
    target 1298
  ]
  edge [
    source 32
    target 1299
  ]
  edge [
    source 32
    target 1300
  ]
  edge [
    source 32
    target 1301
  ]
  edge [
    source 32
    target 1302
  ]
  edge [
    source 32
    target 1303
  ]
  edge [
    source 32
    target 1304
  ]
  edge [
    source 32
    target 1305
  ]
  edge [
    source 32
    target 1306
  ]
  edge [
    source 32
    target 1307
  ]
  edge [
    source 32
    target 1308
  ]
  edge [
    source 32
    target 1309
  ]
  edge [
    source 32
    target 1310
  ]
  edge [
    source 32
    target 1311
  ]
  edge [
    source 32
    target 1312
  ]
  edge [
    source 32
    target 1313
  ]
  edge [
    source 32
    target 1314
  ]
  edge [
    source 32
    target 339
  ]
  edge [
    source 32
    target 1315
  ]
  edge [
    source 32
    target 1316
  ]
  edge [
    source 32
    target 1317
  ]
  edge [
    source 32
    target 406
  ]
  edge [
    source 32
    target 1318
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1287
  ]
  edge [
    source 33
    target 1324
  ]
  edge [
    source 33
    target 1325
  ]
  edge [
    source 33
    target 1326
  ]
  edge [
    source 33
    target 1327
  ]
  edge [
    source 33
    target 1328
  ]
  edge [
    source 33
    target 1329
  ]
  edge [
    source 33
    target 1330
  ]
  edge [
    source 33
    target 1331
  ]
  edge [
    source 33
    target 1332
  ]
  edge [
    source 33
    target 1188
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 1333
  ]
  edge [
    source 33
    target 1334
  ]
  edge [
    source 33
    target 1335
  ]
  edge [
    source 33
    target 1251
  ]
  edge [
    source 33
    target 1336
  ]
  edge [
    source 33
    target 759
  ]
  edge [
    source 33
    target 1337
  ]
  edge [
    source 33
    target 1338
  ]
  edge [
    source 33
    target 764
  ]
  edge [
    source 33
    target 1339
  ]
  edge [
    source 33
    target 1283
  ]
  edge [
    source 33
    target 1340
  ]
  edge [
    source 33
    target 1341
  ]
  edge [
    source 33
    target 1342
  ]
  edge [
    source 33
    target 1343
  ]
  edge [
    source 33
    target 1344
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 1345
  ]
  edge [
    source 33
    target 1346
  ]
  edge [
    source 33
    target 1347
  ]
  edge [
    source 33
    target 1348
  ]
  edge [
    source 33
    target 1349
  ]
  edge [
    source 33
    target 1350
  ]
  edge [
    source 33
    target 1351
  ]
  edge [
    source 33
    target 1352
  ]
  edge [
    source 33
    target 368
  ]
  edge [
    source 33
    target 1353
  ]
  edge [
    source 33
    target 1354
  ]
  edge [
    source 33
    target 1355
  ]
  edge [
    source 33
    target 1356
  ]
  edge [
    source 33
    target 1357
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 1358
  ]
  edge [
    source 33
    target 1359
  ]
  edge [
    source 33
    target 932
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 33
    target 1360
  ]
  edge [
    source 33
    target 234
  ]
  edge [
    source 33
    target 1361
  ]
  edge [
    source 33
    target 931
  ]
  edge [
    source 33
    target 926
  ]
  edge [
    source 33
    target 399
  ]
  edge [
    source 33
    target 881
  ]
  edge [
    source 33
    target 1362
  ]
  edge [
    source 33
    target 1363
  ]
  edge [
    source 33
    target 1364
  ]
  edge [
    source 33
    target 1365
  ]
  edge [
    source 33
    target 1366
  ]
  edge [
    source 33
    target 166
  ]
  edge [
    source 33
    target 1121
  ]
  edge [
    source 33
    target 235
  ]
  edge [
    source 33
    target 1367
  ]
  edge [
    source 33
    target 1368
  ]
  edge [
    source 33
    target 1369
  ]
  edge [
    source 33
    target 1093
  ]
  edge [
    source 33
    target 1370
  ]
  edge [
    source 33
    target 1371
  ]
  edge [
    source 33
    target 1155
  ]
  edge [
    source 33
    target 509
  ]
  edge [
    source 33
    target 1372
  ]
  edge [
    source 33
    target 1373
  ]
  edge [
    source 33
    target 1374
  ]
  edge [
    source 33
    target 1375
  ]
  edge [
    source 33
    target 1376
  ]
  edge [
    source 33
    target 1377
  ]
  edge [
    source 33
    target 1378
  ]
  edge [
    source 33
    target 1379
  ]
  edge [
    source 33
    target 1380
  ]
  edge [
    source 33
    target 1381
  ]
  edge [
    source 33
    target 1382
  ]
  edge [
    source 33
    target 1383
  ]
  edge [
    source 33
    target 1384
  ]
  edge [
    source 33
    target 1385
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 57
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 853
  ]
  edge [
    source 34
    target 251
  ]
  edge [
    source 34
    target 847
  ]
  edge [
    source 34
    target 861
  ]
  edge [
    source 34
    target 1386
  ]
  edge [
    source 34
    target 459
  ]
  edge [
    source 34
    target 1387
  ]
  edge [
    source 34
    target 1388
  ]
  edge [
    source 34
    target 448
  ]
  edge [
    source 34
    target 1190
  ]
  edge [
    source 34
    target 1389
  ]
  edge [
    source 34
    target 1390
  ]
  edge [
    source 34
    target 1391
  ]
  edge [
    source 34
    target 1392
  ]
  edge [
    source 34
    target 1393
  ]
  edge [
    source 34
    target 1394
  ]
  edge [
    source 34
    target 1395
  ]
  edge [
    source 34
    target 1396
  ]
  edge [
    source 34
    target 1397
  ]
  edge [
    source 34
    target 1398
  ]
  edge [
    source 34
    target 1399
  ]
  edge [
    source 34
    target 1400
  ]
  edge [
    source 34
    target 1401
  ]
  edge [
    source 34
    target 1402
  ]
  edge [
    source 34
    target 791
  ]
  edge [
    source 34
    target 509
  ]
  edge [
    source 34
    target 406
  ]
  edge [
    source 34
    target 180
  ]
  edge [
    source 34
    target 1403
  ]
  edge [
    source 34
    target 1404
  ]
  edge [
    source 34
    target 1405
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 1406
  ]
  edge [
    source 34
    target 785
  ]
  edge [
    source 34
    target 539
  ]
  edge [
    source 34
    target 1407
  ]
  edge [
    source 34
    target 1408
  ]
  edge [
    source 34
    target 1409
  ]
  edge [
    source 34
    target 1410
  ]
  edge [
    source 34
    target 1411
  ]
  edge [
    source 34
    target 1412
  ]
  edge [
    source 34
    target 1413
  ]
  edge [
    source 34
    target 1414
  ]
  edge [
    source 34
    target 786
  ]
  edge [
    source 34
    target 1415
  ]
  edge [
    source 34
    target 1416
  ]
  edge [
    source 34
    target 1417
  ]
  edge [
    source 35
    target 1418
  ]
  edge [
    source 35
    target 1419
  ]
  edge [
    source 35
    target 1092
  ]
  edge [
    source 35
    target 1420
  ]
  edge [
    source 35
    target 1421
  ]
  edge [
    source 35
    target 1422
  ]
  edge [
    source 35
    target 1423
  ]
  edge [
    source 35
    target 1424
  ]
  edge [
    source 35
    target 1425
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 963
  ]
  edge [
    source 36
    target 1426
  ]
  edge [
    source 36
    target 1427
  ]
  edge [
    source 36
    target 1428
  ]
  edge [
    source 36
    target 1429
  ]
  edge [
    source 36
    target 1430
  ]
  edge [
    source 36
    target 1431
  ]
  edge [
    source 36
    target 199
  ]
  edge [
    source 36
    target 1432
  ]
  edge [
    source 36
    target 869
  ]
  edge [
    source 36
    target 1433
  ]
  edge [
    source 36
    target 1434
  ]
  edge [
    source 36
    target 1435
  ]
  edge [
    source 36
    target 1436
  ]
  edge [
    source 36
    target 847
  ]
  edge [
    source 36
    target 1437
  ]
  edge [
    source 36
    target 1438
  ]
  edge [
    source 36
    target 257
  ]
  edge [
    source 36
    target 1181
  ]
  edge [
    source 36
    target 1439
  ]
  edge [
    source 36
    target 1440
  ]
  edge [
    source 36
    target 1441
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 1442
  ]
  edge [
    source 36
    target 1443
  ]
  edge [
    source 36
    target 1444
  ]
  edge [
    source 36
    target 1445
  ]
  edge [
    source 36
    target 1446
  ]
  edge [
    source 36
    target 1447
  ]
  edge [
    source 36
    target 1448
  ]
  edge [
    source 36
    target 1449
  ]
  edge [
    source 36
    target 1450
  ]
  edge [
    source 36
    target 1451
  ]
  edge [
    source 36
    target 1452
  ]
  edge [
    source 36
    target 1453
  ]
  edge [
    source 36
    target 1454
  ]
  edge [
    source 36
    target 1455
  ]
  edge [
    source 36
    target 1456
  ]
  edge [
    source 36
    target 1457
  ]
  edge [
    source 36
    target 1458
  ]
  edge [
    source 36
    target 1459
  ]
  edge [
    source 36
    target 1460
  ]
  edge [
    source 36
    target 1461
  ]
  edge [
    source 36
    target 1462
  ]
  edge [
    source 36
    target 1463
  ]
  edge [
    source 36
    target 1464
  ]
  edge [
    source 36
    target 1465
  ]
  edge [
    source 36
    target 1466
  ]
  edge [
    source 36
    target 1467
  ]
  edge [
    source 36
    target 254
  ]
  edge [
    source 36
    target 1468
  ]
  edge [
    source 36
    target 1469
  ]
  edge [
    source 36
    target 1470
  ]
  edge [
    source 36
    target 1471
  ]
  edge [
    source 36
    target 1472
  ]
  edge [
    source 36
    target 1473
  ]
  edge [
    source 36
    target 1474
  ]
  edge [
    source 36
    target 1475
  ]
  edge [
    source 36
    target 1476
  ]
  edge [
    source 36
    target 1477
  ]
  edge [
    source 36
    target 1478
  ]
  edge [
    source 36
    target 1479
  ]
  edge [
    source 36
    target 1480
  ]
  edge [
    source 36
    target 1481
  ]
  edge [
    source 36
    target 406
  ]
  edge [
    source 36
    target 1482
  ]
  edge [
    source 36
    target 1483
  ]
  edge [
    source 36
    target 1484
  ]
  edge [
    source 36
    target 1485
  ]
  edge [
    source 36
    target 1486
  ]
  edge [
    source 36
    target 1487
  ]
  edge [
    source 36
    target 1488
  ]
  edge [
    source 36
    target 1489
  ]
  edge [
    source 36
    target 1192
  ]
  edge [
    source 36
    target 1490
  ]
  edge [
    source 36
    target 1381
  ]
  edge [
    source 36
    target 1491
  ]
  edge [
    source 36
    target 1492
  ]
  edge [
    source 36
    target 1493
  ]
  edge [
    source 36
    target 1070
  ]
  edge [
    source 36
    target 1494
  ]
  edge [
    source 36
    target 1495
  ]
  edge [
    source 36
    target 920
  ]
  edge [
    source 36
    target 1330
  ]
  edge [
    source 36
    target 1496
  ]
  edge [
    source 36
    target 1497
  ]
  edge [
    source 36
    target 1498
  ]
  edge [
    source 36
    target 1499
  ]
  edge [
    source 36
    target 1500
  ]
  edge [
    source 36
    target 1501
  ]
  edge [
    source 36
    target 1502
  ]
  edge [
    source 36
    target 255
  ]
  edge [
    source 37
    target 1503
  ]
  edge [
    source 37
    target 1504
  ]
  edge [
    source 37
    target 1429
  ]
  edge [
    source 37
    target 1505
  ]
  edge [
    source 37
    target 1506
  ]
  edge [
    source 37
    target 1507
  ]
  edge [
    source 38
    target 1508
  ]
  edge [
    source 38
    target 1509
  ]
  edge [
    source 38
    target 670
  ]
  edge [
    source 38
    target 1510
  ]
  edge [
    source 38
    target 1511
  ]
  edge [
    source 38
    target 1512
  ]
  edge [
    source 38
    target 1513
  ]
  edge [
    source 38
    target 1514
  ]
  edge [
    source 38
    target 1515
  ]
  edge [
    source 38
    target 1516
  ]
  edge [
    source 38
    target 476
  ]
  edge [
    source 38
    target 1517
  ]
  edge [
    source 38
    target 1518
  ]
  edge [
    source 38
    target 1519
  ]
  edge [
    source 38
    target 1520
  ]
  edge [
    source 38
    target 1521
  ]
  edge [
    source 38
    target 1522
  ]
  edge [
    source 38
    target 1523
  ]
  edge [
    source 38
    target 470
  ]
  edge [
    source 38
    target 1524
  ]
  edge [
    source 38
    target 1525
  ]
  edge [
    source 38
    target 1526
  ]
  edge [
    source 38
    target 1527
  ]
  edge [
    source 38
    target 1528
  ]
  edge [
    source 38
    target 1529
  ]
  edge [
    source 38
    target 1530
  ]
  edge [
    source 38
    target 1531
  ]
  edge [
    source 38
    target 1532
  ]
  edge [
    source 38
    target 1533
  ]
  edge [
    source 38
    target 1534
  ]
  edge [
    source 38
    target 1535
  ]
  edge [
    source 38
    target 1536
  ]
  edge [
    source 38
    target 1537
  ]
  edge [
    source 38
    target 1020
  ]
  edge [
    source 38
    target 1538
  ]
  edge [
    source 38
    target 1539
  ]
  edge [
    source 38
    target 1540
  ]
  edge [
    source 38
    target 492
  ]
  edge [
    source 38
    target 471
  ]
  edge [
    source 38
    target 1541
  ]
  edge [
    source 38
    target 1542
  ]
  edge [
    source 38
    target 1543
  ]
  edge [
    source 38
    target 1544
  ]
  edge [
    source 38
    target 1545
  ]
  edge [
    source 38
    target 1546
  ]
  edge [
    source 38
    target 1547
  ]
  edge [
    source 38
    target 1548
  ]
  edge [
    source 38
    target 1549
  ]
  edge [
    source 38
    target 1550
  ]
  edge [
    source 38
    target 1551
  ]
  edge [
    source 38
    target 1552
  ]
  edge [
    source 38
    target 1553
  ]
  edge [
    source 38
    target 1554
  ]
  edge [
    source 38
    target 1555
  ]
  edge [
    source 38
    target 1556
  ]
  edge [
    source 38
    target 1557
  ]
  edge [
    source 38
    target 1558
  ]
  edge [
    source 38
    target 1559
  ]
  edge [
    source 38
    target 1560
  ]
  edge [
    source 38
    target 1561
  ]
  edge [
    source 38
    target 466
  ]
  edge [
    source 38
    target 1562
  ]
  edge [
    source 38
    target 1563
  ]
  edge [
    source 38
    target 1564
  ]
  edge [
    source 38
    target 1565
  ]
  edge [
    source 38
    target 1566
  ]
  edge [
    source 38
    target 1567
  ]
  edge [
    source 38
    target 1568
  ]
  edge [
    source 38
    target 310
  ]
  edge [
    source 38
    target 1569
  ]
  edge [
    source 38
    target 1570
  ]
  edge [
    source 38
    target 1571
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 39
    target 56
  ]
  edge [
    source 39
    target 1572
  ]
  edge [
    source 39
    target 1573
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 152
  ]
  edge [
    source 40
    target 1576
  ]
  edge [
    source 40
    target 1577
  ]
  edge [
    source 40
    target 1578
  ]
  edge [
    source 40
    target 1579
  ]
  edge [
    source 40
    target 1580
  ]
  edge [
    source 40
    target 1581
  ]
  edge [
    source 40
    target 1582
  ]
  edge [
    source 40
    target 1583
  ]
  edge [
    source 40
    target 1584
  ]
  edge [
    source 40
    target 282
  ]
  edge [
    source 40
    target 1585
  ]
  edge [
    source 40
    target 1586
  ]
  edge [
    source 40
    target 1587
  ]
  edge [
    source 40
    target 1588
  ]
  edge [
    source 40
    target 1589
  ]
  edge [
    source 40
    target 1590
  ]
  edge [
    source 40
    target 1591
  ]
  edge [
    source 40
    target 1592
  ]
  edge [
    source 40
    target 1593
  ]
  edge [
    source 40
    target 1594
  ]
  edge [
    source 40
    target 339
  ]
  edge [
    source 40
    target 1595
  ]
  edge [
    source 40
    target 1596
  ]
  edge [
    source 40
    target 1597
  ]
  edge [
    source 40
    target 1598
  ]
  edge [
    source 40
    target 759
  ]
  edge [
    source 40
    target 1599
  ]
  edge [
    source 40
    target 1600
  ]
  edge [
    source 40
    target 1601
  ]
  edge [
    source 40
    target 1602
  ]
  edge [
    source 40
    target 1603
  ]
  edge [
    source 40
    target 1604
  ]
  edge [
    source 40
    target 1605
  ]
  edge [
    source 40
    target 240
  ]
  edge [
    source 40
    target 1606
  ]
  edge [
    source 40
    target 83
  ]
  edge [
    source 40
    target 365
  ]
  edge [
    source 40
    target 122
  ]
  edge [
    source 40
    target 57
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1376
  ]
  edge [
    source 41
    target 1607
  ]
  edge [
    source 41
    target 1185
  ]
  edge [
    source 41
    target 1608
  ]
  edge [
    source 41
    target 495
  ]
  edge [
    source 41
    target 1253
  ]
  edge [
    source 41
    target 1188
  ]
  edge [
    source 41
    target 1609
  ]
  edge [
    source 41
    target 914
  ]
  edge [
    source 41
    target 1610
  ]
  edge [
    source 41
    target 1611
  ]
  edge [
    source 41
    target 1612
  ]
  edge [
    source 41
    target 1613
  ]
  edge [
    source 41
    target 1614
  ]
  edge [
    source 41
    target 1615
  ]
  edge [
    source 41
    target 1616
  ]
  edge [
    source 41
    target 1617
  ]
  edge [
    source 41
    target 1107
  ]
  edge [
    source 41
    target 1618
  ]
  edge [
    source 41
    target 1619
  ]
  edge [
    source 41
    target 1620
  ]
  edge [
    source 41
    target 1621
  ]
  edge [
    source 41
    target 1341
  ]
  edge [
    source 41
    target 1622
  ]
  edge [
    source 41
    target 1623
  ]
  edge [
    source 41
    target 1624
  ]
  edge [
    source 41
    target 1625
  ]
  edge [
    source 41
    target 222
  ]
  edge [
    source 41
    target 1626
  ]
  edge [
    source 41
    target 187
  ]
  edge [
    source 41
    target 1627
  ]
  edge [
    source 41
    target 1628
  ]
  edge [
    source 41
    target 1629
  ]
  edge [
    source 41
    target 1630
  ]
  edge [
    source 41
    target 1631
  ]
  edge [
    source 41
    target 1632
  ]
  edge [
    source 41
    target 1317
  ]
  edge [
    source 41
    target 406
  ]
  edge [
    source 41
    target 1318
  ]
  edge [
    source 41
    target 1320
  ]
  edge [
    source 41
    target 1319
  ]
  edge [
    source 41
    target 1321
  ]
  edge [
    source 41
    target 1322
  ]
  edge [
    source 41
    target 1323
  ]
  edge [
    source 41
    target 1633
  ]
  edge [
    source 41
    target 1634
  ]
  edge [
    source 41
    target 1635
  ]
  edge [
    source 41
    target 794
  ]
  edge [
    source 41
    target 1636
  ]
  edge [
    source 41
    target 589
  ]
  edge [
    source 41
    target 796
  ]
  edge [
    source 41
    target 1484
  ]
  edge [
    source 41
    target 1637
  ]
  edge [
    source 41
    target 235
  ]
  edge [
    source 41
    target 1638
  ]
  edge [
    source 41
    target 1639
  ]
  edge [
    source 41
    target 1640
  ]
  edge [
    source 41
    target 1641
  ]
  edge [
    source 41
    target 1642
  ]
  edge [
    source 41
    target 1643
  ]
  edge [
    source 41
    target 1644
  ]
  edge [
    source 41
    target 1645
  ]
  edge [
    source 41
    target 1646
  ]
  edge [
    source 41
    target 1647
  ]
  edge [
    source 41
    target 1648
  ]
  edge [
    source 41
    target 1649
  ]
  edge [
    source 41
    target 1650
  ]
  edge [
    source 41
    target 1651
  ]
  edge [
    source 41
    target 1652
  ]
  edge [
    source 41
    target 1653
  ]
  edge [
    source 41
    target 1654
  ]
  edge [
    source 41
    target 1655
  ]
  edge [
    source 41
    target 1656
  ]
  edge [
    source 41
    target 1657
  ]
  edge [
    source 41
    target 1658
  ]
  edge [
    source 41
    target 502
  ]
  edge [
    source 41
    target 503
  ]
  edge [
    source 41
    target 504
  ]
  edge [
    source 41
    target 505
  ]
  edge [
    source 41
    target 506
  ]
  edge [
    source 41
    target 507
  ]
  edge [
    source 41
    target 508
  ]
  edge [
    source 41
    target 509
  ]
  edge [
    source 41
    target 510
  ]
  edge [
    source 41
    target 1659
  ]
  edge [
    source 41
    target 1660
  ]
  edge [
    source 41
    target 1661
  ]
  edge [
    source 41
    target 869
  ]
  edge [
    source 41
    target 1662
  ]
  edge [
    source 41
    target 920
  ]
  edge [
    source 41
    target 1663
  ]
  edge [
    source 41
    target 1664
  ]
  edge [
    source 41
    target 1665
  ]
  edge [
    source 41
    target 1666
  ]
  edge [
    source 41
    target 1667
  ]
  edge [
    source 41
    target 1668
  ]
  edge [
    source 41
    target 1669
  ]
  edge [
    source 41
    target 1670
  ]
  edge [
    source 41
    target 1671
  ]
  edge [
    source 41
    target 1672
  ]
  edge [
    source 41
    target 1673
  ]
  edge [
    source 41
    target 1674
  ]
  edge [
    source 41
    target 1675
  ]
  edge [
    source 41
    target 1676
  ]
  edge [
    source 41
    target 1677
  ]
  edge [
    source 41
    target 1678
  ]
  edge [
    source 41
    target 1679
  ]
  edge [
    source 41
    target 1680
  ]
  edge [
    source 41
    target 1562
  ]
  edge [
    source 41
    target 890
  ]
  edge [
    source 41
    target 1681
  ]
  edge [
    source 41
    target 1682
  ]
  edge [
    source 41
    target 1683
  ]
  edge [
    source 41
    target 1684
  ]
  edge [
    source 41
    target 1685
  ]
  edge [
    source 41
    target 1686
  ]
  edge [
    source 41
    target 1687
  ]
  edge [
    source 41
    target 1688
  ]
  edge [
    source 41
    target 1689
  ]
  edge [
    source 41
    target 1690
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1171
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 1117
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 1182
  ]
  edge [
    source 41
    target 448
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1197
  ]
  edge [
    source 41
    target 1196
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 239
  ]
  edge [
    source 41
    target 1326
  ]
  edge [
    source 41
    target 442
  ]
  edge [
    source 41
    target 1199
  ]
  edge [
    source 41
    target 1599
  ]
  edge [
    source 41
    target 198
  ]
  edge [
    source 41
    target 199
  ]
  edge [
    source 41
    target 200
  ]
  edge [
    source 41
    target 202
  ]
  edge [
    source 41
    target 201
  ]
  edge [
    source 41
    target 205
  ]
  edge [
    source 41
    target 203
  ]
  edge [
    source 41
    target 204
  ]
  edge [
    source 41
    target 206
  ]
  edge [
    source 41
    target 207
  ]
  edge [
    source 41
    target 208
  ]
  edge [
    source 41
    target 209
  ]
  edge [
    source 41
    target 210
  ]
  edge [
    source 41
    target 211
  ]
  edge [
    source 41
    target 212
  ]
  edge [
    source 41
    target 213
  ]
  edge [
    source 41
    target 214
  ]
  edge [
    source 41
    target 215
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 115
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 41
    target 364
  ]
  edge [
    source 41
    target 1730
  ]
  edge [
    source 41
    target 1731
  ]
  edge [
    source 41
    target 1732
  ]
  edge [
    source 41
    target 1733
  ]
  edge [
    source 41
    target 1734
  ]
  edge [
    source 41
    target 1190
  ]
  edge [
    source 41
    target 1735
  ]
  edge [
    source 41
    target 1736
  ]
  edge [
    source 41
    target 1737
  ]
  edge [
    source 41
    target 1738
  ]
  edge [
    source 41
    target 1739
  ]
  edge [
    source 41
    target 1740
  ]
  edge [
    source 41
    target 1741
  ]
  edge [
    source 41
    target 1742
  ]
  edge [
    source 41
    target 1743
  ]
  edge [
    source 41
    target 1744
  ]
  edge [
    source 41
    target 948
  ]
  edge [
    source 41
    target 1745
  ]
  edge [
    source 41
    target 1746
  ]
  edge [
    source 41
    target 1747
  ]
  edge [
    source 41
    target 1748
  ]
  edge [
    source 41
    target 246
  ]
  edge [
    source 41
    target 1749
  ]
  edge [
    source 41
    target 1750
  ]
  edge [
    source 41
    target 1403
  ]
  edge [
    source 41
    target 1751
  ]
  edge [
    source 41
    target 1752
  ]
  edge [
    source 41
    target 87
  ]
  edge [
    source 41
    target 1753
  ]
  edge [
    source 41
    target 1754
  ]
  edge [
    source 41
    target 1755
  ]
  edge [
    source 41
    target 1756
  ]
  edge [
    source 41
    target 1757
  ]
  edge [
    source 41
    target 1758
  ]
  edge [
    source 41
    target 528
  ]
  edge [
    source 41
    target 1759
  ]
  edge [
    source 41
    target 1760
  ]
  edge [
    source 41
    target 1761
  ]
  edge [
    source 41
    target 1762
  ]
  edge [
    source 41
    target 578
  ]
  edge [
    source 41
    target 1763
  ]
  edge [
    source 41
    target 1764
  ]
  edge [
    source 41
    target 1765
  ]
  edge [
    source 41
    target 1766
  ]
  edge [
    source 41
    target 1767
  ]
  edge [
    source 41
    target 1768
  ]
  edge [
    source 41
    target 58
  ]
  edge [
    source 41
    target 1583
  ]
  edge [
    source 41
    target 1769
  ]
  edge [
    source 41
    target 1770
  ]
  edge [
    source 41
    target 131
  ]
  edge [
    source 41
    target 1771
  ]
  edge [
    source 41
    target 1262
  ]
  edge [
    source 41
    target 1772
  ]
  edge [
    source 41
    target 65
  ]
  edge [
    source 41
    target 256
  ]
  edge [
    source 41
    target 1773
  ]
  edge [
    source 41
    target 68
  ]
  edge [
    source 41
    target 1774
  ]
  edge [
    source 41
    target 1775
  ]
  edge [
    source 41
    target 1776
  ]
  edge [
    source 41
    target 1777
  ]
  edge [
    source 41
    target 1778
  ]
  edge [
    source 41
    target 1779
  ]
  edge [
    source 41
    target 1780
  ]
  edge [
    source 41
    target 1781
  ]
  edge [
    source 41
    target 1782
  ]
  edge [
    source 41
    target 761
  ]
  edge [
    source 41
    target 1783
  ]
  edge [
    source 41
    target 1784
  ]
  edge [
    source 41
    target 1785
  ]
  edge [
    source 41
    target 1786
  ]
  edge [
    source 41
    target 1787
  ]
  edge [
    source 41
    target 1788
  ]
  edge [
    source 41
    target 1789
  ]
  edge [
    source 41
    target 1790
  ]
  edge [
    source 41
    target 949
  ]
  edge [
    source 41
    target 299
  ]
  edge [
    source 41
    target 1791
  ]
  edge [
    source 41
    target 1792
  ]
  edge [
    source 41
    target 1793
  ]
  edge [
    source 41
    target 1794
  ]
  edge [
    source 41
    target 1795
  ]
  edge [
    source 41
    target 955
  ]
  edge [
    source 41
    target 1381
  ]
  edge [
    source 41
    target 1796
  ]
  edge [
    source 41
    target 1797
  ]
  edge [
    source 41
    target 1798
  ]
  edge [
    source 41
    target 1799
  ]
  edge [
    source 41
    target 1800
  ]
  edge [
    source 41
    target 736
  ]
  edge [
    source 41
    target 1801
  ]
  edge [
    source 41
    target 152
  ]
  edge [
    source 41
    target 59
  ]
  edge [
    source 41
    target 1802
  ]
  edge [
    source 41
    target 339
  ]
  edge [
    source 41
    target 1803
  ]
  edge [
    source 41
    target 1804
  ]
  edge [
    source 41
    target 103
  ]
  edge [
    source 41
    target 276
  ]
  edge [
    source 41
    target 1805
  ]
  edge [
    source 41
    target 1806
  ]
  edge [
    source 41
    target 1807
  ]
  edge [
    source 41
    target 1808
  ]
  edge [
    source 41
    target 1809
  ]
  edge [
    source 41
    target 1810
  ]
  edge [
    source 41
    target 1811
  ]
  edge [
    source 41
    target 1812
  ]
  edge [
    source 41
    target 1813
  ]
  edge [
    source 41
    target 1814
  ]
  edge [
    source 41
    target 1815
  ]
  edge [
    source 41
    target 1816
  ]
  edge [
    source 41
    target 153
  ]
  edge [
    source 41
    target 1817
  ]
  edge [
    source 41
    target 1818
  ]
  edge [
    source 41
    target 1819
  ]
  edge [
    source 41
    target 1820
  ]
  edge [
    source 41
    target 1821
  ]
  edge [
    source 41
    target 1822
  ]
  edge [
    source 41
    target 1823
  ]
  edge [
    source 41
    target 1824
  ]
  edge [
    source 41
    target 1825
  ]
  edge [
    source 41
    target 1826
  ]
  edge [
    source 41
    target 1827
  ]
  edge [
    source 41
    target 1828
  ]
  edge [
    source 41
    target 1566
  ]
  edge [
    source 41
    target 1829
  ]
  edge [
    source 41
    target 1830
  ]
  edge [
    source 41
    target 1831
  ]
  edge [
    source 41
    target 1832
  ]
  edge [
    source 41
    target 1833
  ]
  edge [
    source 41
    target 1834
  ]
  edge [
    source 41
    target 1286
  ]
  edge [
    source 41
    target 1835
  ]
  edge [
    source 41
    target 1836
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 512
  ]
  edge [
    source 41
    target 1837
  ]
  edge [
    source 41
    target 1838
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 1839
  ]
  edge [
    source 41
    target 937
  ]
  edge [
    source 41
    target 1840
  ]
  edge [
    source 41
    target 324
  ]
  edge [
    source 41
    target 1841
  ]
  edge [
    source 41
    target 1842
  ]
  edge [
    source 41
    target 1346
  ]
  edge [
    source 41
    target 358
  ]
  edge [
    source 41
    target 329
  ]
  edge [
    source 41
    target 361
  ]
  edge [
    source 41
    target 1843
  ]
  edge [
    source 41
    target 1844
  ]
  edge [
    source 41
    target 1845
  ]
  edge [
    source 41
    target 1846
  ]
  edge [
    source 41
    target 1847
  ]
  edge [
    source 41
    target 641
  ]
  edge [
    source 41
    target 1848
  ]
  edge [
    source 41
    target 1849
  ]
  edge [
    source 41
    target 1850
  ]
  edge [
    source 41
    target 1851
  ]
  edge [
    source 41
    target 298
  ]
  edge [
    source 41
    target 1852
  ]
  edge [
    source 41
    target 1853
  ]
  edge [
    source 41
    target 1854
  ]
  edge [
    source 41
    target 1855
  ]
  edge [
    source 41
    target 1856
  ]
  edge [
    source 41
    target 1857
  ]
  edge [
    source 41
    target 1858
  ]
  edge [
    source 41
    target 320
  ]
  edge [
    source 41
    target 1859
  ]
  edge [
    source 41
    target 762
  ]
  edge [
    source 41
    target 1860
  ]
  edge [
    source 41
    target 1861
  ]
  edge [
    source 41
    target 1862
  ]
  edge [
    source 41
    target 1863
  ]
  edge [
    source 41
    target 1864
  ]
  edge [
    source 41
    target 1865
  ]
  edge [
    source 41
    target 1866
  ]
  edge [
    source 41
    target 1867
  ]
  edge [
    source 41
    target 1868
  ]
  edge [
    source 41
    target 1869
  ]
  edge [
    source 41
    target 1870
  ]
  edge [
    source 41
    target 1045
  ]
  edge [
    source 41
    target 1871
  ]
  edge [
    source 41
    target 1872
  ]
  edge [
    source 41
    target 1187
  ]
  edge [
    source 41
    target 1167
  ]
  edge [
    source 41
    target 1873
  ]
  edge [
    source 41
    target 1874
  ]
  edge [
    source 41
    target 1875
  ]
  edge [
    source 41
    target 1876
  ]
  edge [
    source 41
    target 1877
  ]
  edge [
    source 41
    target 1878
  ]
  edge [
    source 41
    target 1002
  ]
  edge [
    source 41
    target 1879
  ]
  edge [
    source 41
    target 1880
  ]
  edge [
    source 41
    target 1881
  ]
  edge [
    source 41
    target 283
  ]
  edge [
    source 41
    target 1882
  ]
  edge [
    source 41
    target 1883
  ]
  edge [
    source 41
    target 1884
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1885
  ]
  edge [
    source 44
    target 1886
  ]
  edge [
    source 44
    target 1887
  ]
  edge [
    source 44
    target 1888
  ]
  edge [
    source 44
    target 1889
  ]
  edge [
    source 44
    target 1890
  ]
  edge [
    source 44
    target 1891
  ]
  edge [
    source 44
    target 1892
  ]
  edge [
    source 44
    target 1893
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1894
  ]
  edge [
    source 45
    target 1895
  ]
  edge [
    source 45
    target 1896
  ]
  edge [
    source 45
    target 1897
  ]
  edge [
    source 45
    target 1898
  ]
  edge [
    source 45
    target 1899
  ]
  edge [
    source 45
    target 1900
  ]
  edge [
    source 45
    target 246
  ]
  edge [
    source 45
    target 1901
  ]
  edge [
    source 45
    target 1902
  ]
  edge [
    source 45
    target 235
  ]
  edge [
    source 45
    target 302
  ]
  edge [
    source 45
    target 1903
  ]
  edge [
    source 45
    target 1904
  ]
  edge [
    source 45
    target 1905
  ]
  edge [
    source 45
    target 1906
  ]
  edge [
    source 45
    target 1907
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1023
  ]
  edge [
    source 46
    target 1908
  ]
  edge [
    source 46
    target 1909
  ]
  edge [
    source 46
    target 1910
  ]
  edge [
    source 46
    target 1911
  ]
  edge [
    source 46
    target 1912
  ]
  edge [
    source 46
    target 558
  ]
  edge [
    source 46
    target 1913
  ]
  edge [
    source 46
    target 1914
  ]
  edge [
    source 46
    target 1915
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1916
  ]
  edge [
    source 47
    target 1917
  ]
  edge [
    source 47
    target 1918
  ]
  edge [
    source 47
    target 1919
  ]
  edge [
    source 47
    target 1920
  ]
  edge [
    source 47
    target 1921
  ]
  edge [
    source 47
    target 1922
  ]
  edge [
    source 47
    target 1923
  ]
  edge [
    source 47
    target 1924
  ]
  edge [
    source 47
    target 1925
  ]
  edge [
    source 47
    target 1926
  ]
  edge [
    source 47
    target 1676
  ]
  edge [
    source 47
    target 1927
  ]
  edge [
    source 47
    target 1928
  ]
  edge [
    source 47
    target 1929
  ]
  edge [
    source 47
    target 1930
  ]
  edge [
    source 47
    target 1931
  ]
  edge [
    source 47
    target 1932
  ]
  edge [
    source 47
    target 1933
  ]
  edge [
    source 47
    target 1107
  ]
  edge [
    source 47
    target 1934
  ]
  edge [
    source 47
    target 1935
  ]
  edge [
    source 47
    target 1936
  ]
  edge [
    source 47
    target 1937
  ]
  edge [
    source 47
    target 1938
  ]
  edge [
    source 47
    target 1939
  ]
  edge [
    source 47
    target 1940
  ]
  edge [
    source 47
    target 442
  ]
  edge [
    source 47
    target 1941
  ]
  edge [
    source 47
    target 1942
  ]
  edge [
    source 47
    target 1943
  ]
  edge [
    source 47
    target 1944
  ]
  edge [
    source 47
    target 989
  ]
  edge [
    source 47
    target 1567
  ]
  edge [
    source 47
    target 1482
  ]
  edge [
    source 47
    target 1945
  ]
  edge [
    source 47
    target 1946
  ]
  edge [
    source 47
    target 1947
  ]
  edge [
    source 47
    target 897
  ]
  edge [
    source 47
    target 1948
  ]
  edge [
    source 47
    target 1949
  ]
  edge [
    source 47
    target 1950
  ]
  edge [
    source 47
    target 1951
  ]
  edge [
    source 47
    target 1952
  ]
  edge [
    source 47
    target 1953
  ]
  edge [
    source 47
    target 1954
  ]
  edge [
    source 47
    target 1955
  ]
  edge [
    source 48
    target 1956
  ]
  edge [
    source 48
    target 1957
  ]
  edge [
    source 48
    target 1958
  ]
  edge [
    source 48
    target 1959
  ]
  edge [
    source 48
    target 1960
  ]
  edge [
    source 48
    target 59
  ]
  edge [
    source 48
    target 152
  ]
  edge [
    source 48
    target 1961
  ]
  edge [
    source 48
    target 1962
  ]
  edge [
    source 48
    target 1963
  ]
  edge [
    source 48
    target 1964
  ]
  edge [
    source 48
    target 1965
  ]
  edge [
    source 48
    target 1966
  ]
  edge [
    source 48
    target 1967
  ]
  edge [
    source 48
    target 282
  ]
  edge [
    source 48
    target 1968
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1969
  ]
  edge [
    source 49
    target 1970
  ]
  edge [
    source 49
    target 1971
  ]
  edge [
    source 49
    target 1972
  ]
  edge [
    source 49
    target 1973
  ]
  edge [
    source 49
    target 1403
  ]
  edge [
    source 49
    target 1974
  ]
  edge [
    source 49
    target 1975
  ]
  edge [
    source 49
    target 1066
  ]
  edge [
    source 49
    target 1976
  ]
  edge [
    source 49
    target 563
  ]
  edge [
    source 49
    target 1977
  ]
  edge [
    source 49
    target 952
  ]
  edge [
    source 49
    target 1978
  ]
  edge [
    source 49
    target 1979
  ]
  edge [
    source 49
    target 1980
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 1563
  ]
  edge [
    source 54
    target 1981
  ]
  edge [
    source 54
    target 611
  ]
  edge [
    source 54
    target 1982
  ]
  edge [
    source 54
    target 989
  ]
  edge [
    source 54
    target 1983
  ]
  edge [
    source 54
    target 1984
  ]
  edge [
    source 54
    target 1985
  ]
  edge [
    source 54
    target 1171
  ]
  edge [
    source 54
    target 1906
  ]
  edge [
    source 54
    target 699
  ]
  edge [
    source 55
    target 1986
  ]
  edge [
    source 55
    target 1987
  ]
  edge [
    source 55
    target 1988
  ]
  edge [
    source 55
    target 1722
  ]
  edge [
    source 55
    target 1989
  ]
  edge [
    source 55
    target 339
  ]
  edge [
    source 55
    target 1610
  ]
  edge [
    source 55
    target 322
  ]
  edge [
    source 55
    target 1990
  ]
  edge [
    source 55
    target 764
  ]
  edge [
    source 55
    target 1991
  ]
  edge [
    source 55
    target 1992
  ]
  edge [
    source 55
    target 1993
  ]
  edge [
    source 55
    target 1342
  ]
  edge [
    source 55
    target 1994
  ]
  edge [
    source 55
    target 1995
  ]
  edge [
    source 55
    target 326
  ]
  edge [
    source 55
    target 1996
  ]
  edge [
    source 55
    target 1997
  ]
  edge [
    source 55
    target 360
  ]
  edge [
    source 55
    target 1350
  ]
  edge [
    source 55
    target 1998
  ]
  edge [
    source 55
    target 1999
  ]
  edge [
    source 55
    target 1169
  ]
  edge [
    source 55
    target 2000
  ]
  edge [
    source 55
    target 2001
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 2002
  ]
  edge [
    source 56
    target 326
  ]
  edge [
    source 56
    target 2003
  ]
  edge [
    source 56
    target 2004
  ]
  edge [
    source 56
    target 2005
  ]
  edge [
    source 56
    target 329
  ]
  edge [
    source 56
    target 365
  ]
  edge [
    source 56
    target 2006
  ]
  edge [
    source 56
    target 2007
  ]
  edge [
    source 56
    target 2008
  ]
  edge [
    source 56
    target 2009
  ]
  edge [
    source 56
    target 1364
  ]
  edge [
    source 56
    target 2010
  ]
  edge [
    source 56
    target 352
  ]
  edge [
    source 56
    target 2011
  ]
  edge [
    source 56
    target 2012
  ]
  edge [
    source 56
    target 2013
  ]
  edge [
    source 56
    target 2014
  ]
  edge [
    source 56
    target 2015
  ]
  edge [
    source 56
    target 2016
  ]
  edge [
    source 56
    target 2017
  ]
  edge [
    source 56
    target 764
  ]
  edge [
    source 56
    target 2018
  ]
  edge [
    source 56
    target 1260
  ]
  edge [
    source 56
    target 2019
  ]
  edge [
    source 56
    target 2020
  ]
  edge [
    source 56
    target 1718
  ]
  edge [
    source 56
    target 2021
  ]
  edge [
    source 56
    target 2022
  ]
  edge [
    source 56
    target 2023
  ]
  edge [
    source 56
    target 884
  ]
  edge [
    source 56
    target 349
  ]
  edge [
    source 56
    target 399
  ]
  edge [
    source 56
    target 2024
  ]
  edge [
    source 56
    target 2025
  ]
  edge [
    source 56
    target 2026
  ]
  edge [
    source 56
    target 2027
  ]
  edge [
    source 56
    target 2028
  ]
  edge [
    source 56
    target 2029
  ]
  edge [
    source 56
    target 2030
  ]
  edge [
    source 56
    target 2031
  ]
  edge [
    source 56
    target 2032
  ]
  edge [
    source 56
    target 2033
  ]
  edge [
    source 57
    target 2034
  ]
  edge [
    source 57
    target 2035
  ]
  edge [
    source 57
    target 2036
  ]
  edge [
    source 57
    target 1167
  ]
  edge [
    source 57
    target 2037
  ]
  edge [
    source 57
    target 2038
  ]
  edge [
    source 57
    target 2039
  ]
  edge [
    source 57
    target 2040
  ]
  edge [
    source 57
    target 2041
  ]
  edge [
    source 57
    target 187
  ]
  edge [
    source 57
    target 2042
  ]
  edge [
    source 57
    target 869
  ]
  edge [
    source 57
    target 2043
  ]
  edge [
    source 57
    target 1479
  ]
  edge [
    source 57
    target 2044
  ]
  edge [
    source 57
    target 2045
  ]
  edge [
    source 57
    target 1257
  ]
  edge [
    source 57
    target 2046
  ]
  edge [
    source 57
    target 1373
  ]
  edge [
    source 57
    target 2047
  ]
  edge [
    source 57
    target 1707
  ]
  edge [
    source 57
    target 1614
  ]
  edge [
    source 57
    target 2048
  ]
  edge [
    source 57
    target 2049
  ]
  edge [
    source 57
    target 861
  ]
  edge [
    source 57
    target 2050
  ]
  edge [
    source 57
    target 2051
  ]
  edge [
    source 57
    target 2052
  ]
  edge [
    source 57
    target 1121
  ]
  edge [
    source 57
    target 2053
  ]
  edge [
    source 57
    target 788
  ]
  edge [
    source 57
    target 2054
  ]
  edge [
    source 57
    target 2055
  ]
  edge [
    source 57
    target 2056
  ]
  edge [
    source 57
    target 2057
  ]
  edge [
    source 57
    target 1562
  ]
  edge [
    source 57
    target 2058
  ]
  edge [
    source 57
    target 2059
  ]
  edge [
    source 57
    target 2060
  ]
  edge [
    source 57
    target 2061
  ]
  edge [
    source 57
    target 1155
  ]
  edge [
    source 57
    target 989
  ]
  edge [
    source 57
    target 2062
  ]
  edge [
    source 57
    target 2063
  ]
  edge [
    source 57
    target 920
  ]
  edge [
    source 57
    target 2064
  ]
  edge [
    source 57
    target 2065
  ]
]
