graph [
  node [
    id 0
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 1
    label "tema"
    origin "text"
  ]
  node [
    id 2
    label "wina"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "moja"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;odo&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "tychy"
    origin "text"
  ]
  node [
    id 7
    label "gr&#243;b"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "kraj"
    origin "text"
  ]
  node [
    id 10
    label "mno&#380;y&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ten"
    origin "text"
  ]
  node [
    id 12
    label "wieczny"
    origin "text"
  ]
  node [
    id 13
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 14
    label "samotno&#347;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "Cesarstwo_Bizanty&#324;skie"
  ]
  node [
    id 16
    label "jednostka_administracyjna"
  ]
  node [
    id 17
    label "lutnia"
  ]
  node [
    id 18
    label "konsekwencja"
  ]
  node [
    id 19
    label "wstyd"
  ]
  node [
    id 20
    label "guilt"
  ]
  node [
    id 21
    label "odczuwa&#263;"
  ]
  node [
    id 22
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 23
    label "skrupienie_si&#281;"
  ]
  node [
    id 24
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 26
    label "odczucie"
  ]
  node [
    id 27
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 28
    label "koszula_Dejaniry"
  ]
  node [
    id 29
    label "odczuwanie"
  ]
  node [
    id 30
    label "event"
  ]
  node [
    id 31
    label "rezultat"
  ]
  node [
    id 32
    label "skrupianie_si&#281;"
  ]
  node [
    id 33
    label "odczu&#263;"
  ]
  node [
    id 34
    label "chordofon_szarpany"
  ]
  node [
    id 35
    label "srom"
  ]
  node [
    id 36
    label "emocja"
  ]
  node [
    id 37
    label "dishonor"
  ]
  node [
    id 38
    label "konfuzja"
  ]
  node [
    id 39
    label "shame"
  ]
  node [
    id 40
    label "g&#322;upio"
  ]
  node [
    id 41
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 42
    label "mie&#263;_miejsce"
  ]
  node [
    id 43
    label "equal"
  ]
  node [
    id 44
    label "trwa&#263;"
  ]
  node [
    id 45
    label "chodzi&#263;"
  ]
  node [
    id 46
    label "si&#281;ga&#263;"
  ]
  node [
    id 47
    label "stan"
  ]
  node [
    id 48
    label "obecno&#347;&#263;"
  ]
  node [
    id 49
    label "stand"
  ]
  node [
    id 50
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "uczestniczy&#263;"
  ]
  node [
    id 52
    label "participate"
  ]
  node [
    id 53
    label "robi&#263;"
  ]
  node [
    id 54
    label "istnie&#263;"
  ]
  node [
    id 55
    label "pozostawa&#263;"
  ]
  node [
    id 56
    label "zostawa&#263;"
  ]
  node [
    id 57
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 58
    label "adhere"
  ]
  node [
    id 59
    label "compass"
  ]
  node [
    id 60
    label "korzysta&#263;"
  ]
  node [
    id 61
    label "appreciation"
  ]
  node [
    id 62
    label "osi&#261;ga&#263;"
  ]
  node [
    id 63
    label "dociera&#263;"
  ]
  node [
    id 64
    label "get"
  ]
  node [
    id 65
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 66
    label "mierzy&#263;"
  ]
  node [
    id 67
    label "u&#380;ywa&#263;"
  ]
  node [
    id 68
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 69
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 70
    label "exsert"
  ]
  node [
    id 71
    label "being"
  ]
  node [
    id 72
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 73
    label "cecha"
  ]
  node [
    id 74
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 75
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 76
    label "p&#322;ywa&#263;"
  ]
  node [
    id 77
    label "run"
  ]
  node [
    id 78
    label "bangla&#263;"
  ]
  node [
    id 79
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 80
    label "przebiega&#263;"
  ]
  node [
    id 81
    label "wk&#322;ada&#263;"
  ]
  node [
    id 82
    label "proceed"
  ]
  node [
    id 83
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 84
    label "carry"
  ]
  node [
    id 85
    label "bywa&#263;"
  ]
  node [
    id 86
    label "dziama&#263;"
  ]
  node [
    id 87
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 88
    label "stara&#263;_si&#281;"
  ]
  node [
    id 89
    label "para"
  ]
  node [
    id 90
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 91
    label "str&#243;j"
  ]
  node [
    id 92
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 93
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 94
    label "krok"
  ]
  node [
    id 95
    label "tryb"
  ]
  node [
    id 96
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 97
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 98
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 99
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 100
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 101
    label "continue"
  ]
  node [
    id 102
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 103
    label "Ohio"
  ]
  node [
    id 104
    label "wci&#281;cie"
  ]
  node [
    id 105
    label "Nowy_York"
  ]
  node [
    id 106
    label "warstwa"
  ]
  node [
    id 107
    label "samopoczucie"
  ]
  node [
    id 108
    label "Illinois"
  ]
  node [
    id 109
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 110
    label "state"
  ]
  node [
    id 111
    label "Jukatan"
  ]
  node [
    id 112
    label "Kalifornia"
  ]
  node [
    id 113
    label "Wirginia"
  ]
  node [
    id 114
    label "wektor"
  ]
  node [
    id 115
    label "Goa"
  ]
  node [
    id 116
    label "Teksas"
  ]
  node [
    id 117
    label "Waszyngton"
  ]
  node [
    id 118
    label "miejsce"
  ]
  node [
    id 119
    label "Massachusetts"
  ]
  node [
    id 120
    label "Alaska"
  ]
  node [
    id 121
    label "Arakan"
  ]
  node [
    id 122
    label "Hawaje"
  ]
  node [
    id 123
    label "Maryland"
  ]
  node [
    id 124
    label "punkt"
  ]
  node [
    id 125
    label "Michigan"
  ]
  node [
    id 126
    label "Arizona"
  ]
  node [
    id 127
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 128
    label "Georgia"
  ]
  node [
    id 129
    label "poziom"
  ]
  node [
    id 130
    label "Pensylwania"
  ]
  node [
    id 131
    label "shape"
  ]
  node [
    id 132
    label "Luizjana"
  ]
  node [
    id 133
    label "Nowy_Meksyk"
  ]
  node [
    id 134
    label "Alabama"
  ]
  node [
    id 135
    label "ilo&#347;&#263;"
  ]
  node [
    id 136
    label "Kansas"
  ]
  node [
    id 137
    label "Oregon"
  ]
  node [
    id 138
    label "Oklahoma"
  ]
  node [
    id 139
    label "Floryda"
  ]
  node [
    id 140
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 141
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 142
    label "adolescence"
  ]
  node [
    id 143
    label "wiek"
  ]
  node [
    id 144
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 145
    label "zielone_lata"
  ]
  node [
    id 146
    label "period"
  ]
  node [
    id 147
    label "choroba_wieku"
  ]
  node [
    id 148
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 149
    label "chron"
  ]
  node [
    id 150
    label "czas"
  ]
  node [
    id 151
    label "rok"
  ]
  node [
    id 152
    label "long_time"
  ]
  node [
    id 153
    label "jednostka_geologiczna"
  ]
  node [
    id 154
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 155
    label "raj_utracony"
  ]
  node [
    id 156
    label "umieranie"
  ]
  node [
    id 157
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 158
    label "prze&#380;ywanie"
  ]
  node [
    id 159
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 160
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 161
    label "po&#322;&#243;g"
  ]
  node [
    id 162
    label "umarcie"
  ]
  node [
    id 163
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 164
    label "subsistence"
  ]
  node [
    id 165
    label "power"
  ]
  node [
    id 166
    label "okres_noworodkowy"
  ]
  node [
    id 167
    label "prze&#380;ycie"
  ]
  node [
    id 168
    label "wiek_matuzalemowy"
  ]
  node [
    id 169
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 170
    label "entity"
  ]
  node [
    id 171
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 172
    label "do&#380;ywanie"
  ]
  node [
    id 173
    label "byt"
  ]
  node [
    id 174
    label "andropauza"
  ]
  node [
    id 175
    label "dzieci&#324;stwo"
  ]
  node [
    id 176
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 177
    label "rozw&#243;j"
  ]
  node [
    id 178
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 179
    label "menopauza"
  ]
  node [
    id 180
    label "&#347;mier&#263;"
  ]
  node [
    id 181
    label "koleje_losu"
  ]
  node [
    id 182
    label "bycie"
  ]
  node [
    id 183
    label "zegar_biologiczny"
  ]
  node [
    id 184
    label "szwung"
  ]
  node [
    id 185
    label "przebywanie"
  ]
  node [
    id 186
    label "warunki"
  ]
  node [
    id 187
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 188
    label "niemowl&#281;ctwo"
  ]
  node [
    id 189
    label "&#380;ywy"
  ]
  node [
    id 190
    label "life"
  ]
  node [
    id 191
    label "staro&#347;&#263;"
  ]
  node [
    id 192
    label "energy"
  ]
  node [
    id 193
    label "podobie&#324;stwo"
  ]
  node [
    id 194
    label "sympatyczno&#347;&#263;"
  ]
  node [
    id 195
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 196
    label "szczeg&#243;lno&#347;&#263;"
  ]
  node [
    id 197
    label "youngness"
  ]
  node [
    id 198
    label "defenestracja"
  ]
  node [
    id 199
    label "agonia"
  ]
  node [
    id 200
    label "spocz&#261;&#263;"
  ]
  node [
    id 201
    label "spocz&#281;cie"
  ]
  node [
    id 202
    label "kres"
  ]
  node [
    id 203
    label "mogi&#322;a"
  ]
  node [
    id 204
    label "pochowanie"
  ]
  node [
    id 205
    label "kres_&#380;ycia"
  ]
  node [
    id 206
    label "spoczywa&#263;"
  ]
  node [
    id 207
    label "szeol"
  ]
  node [
    id 208
    label "pogrzebanie"
  ]
  node [
    id 209
    label "chowanie"
  ]
  node [
    id 210
    label "park_sztywnych"
  ]
  node [
    id 211
    label "pomnik"
  ]
  node [
    id 212
    label "nagrobek"
  ]
  node [
    id 213
    label "&#380;a&#322;oba"
  ]
  node [
    id 214
    label "prochowisko"
  ]
  node [
    id 215
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 216
    label "spoczywanie"
  ]
  node [
    id 217
    label "zabicie"
  ]
  node [
    id 218
    label "ostatnie_podrygi"
  ]
  node [
    id 219
    label "dzia&#322;anie"
  ]
  node [
    id 220
    label "chwila"
  ]
  node [
    id 221
    label "koniec"
  ]
  node [
    id 222
    label "death"
  ]
  node [
    id 223
    label "upadek"
  ]
  node [
    id 224
    label "zmierzch"
  ]
  node [
    id 225
    label "nieuleczalnie_chory"
  ]
  node [
    id 226
    label "epitaph"
  ]
  node [
    id 227
    label "wiersz"
  ]
  node [
    id 228
    label "p&#322;yta"
  ]
  node [
    id 229
    label "dow&#243;d"
  ]
  node [
    id 230
    label "&#347;wiadectwo"
  ]
  node [
    id 231
    label "dzie&#322;o"
  ]
  node [
    id 232
    label "cok&#243;&#322;"
  ]
  node [
    id 233
    label "rzecz"
  ]
  node [
    id 234
    label "za&#347;wiaty"
  ]
  node [
    id 235
    label "piek&#322;o"
  ]
  node [
    id 236
    label "judaizm"
  ]
  node [
    id 237
    label "destruction"
  ]
  node [
    id 238
    label "zabrzmienie"
  ]
  node [
    id 239
    label "skrzywdzenie"
  ]
  node [
    id 240
    label "pozabijanie"
  ]
  node [
    id 241
    label "zniszczenie"
  ]
  node [
    id 242
    label "zaszkodzenie"
  ]
  node [
    id 243
    label "usuni&#281;cie"
  ]
  node [
    id 244
    label "spowodowanie"
  ]
  node [
    id 245
    label "killing"
  ]
  node [
    id 246
    label "zdarzenie_si&#281;"
  ]
  node [
    id 247
    label "czyn"
  ]
  node [
    id 248
    label "granie"
  ]
  node [
    id 249
    label "zamkni&#281;cie"
  ]
  node [
    id 250
    label "compaction"
  ]
  node [
    id 251
    label "&#380;al"
  ]
  node [
    id 252
    label "paznokie&#263;"
  ]
  node [
    id 253
    label "symbol"
  ]
  node [
    id 254
    label "kir"
  ]
  node [
    id 255
    label "brud"
  ]
  node [
    id 256
    label "wyrzucenie"
  ]
  node [
    id 257
    label "defenestration"
  ]
  node [
    id 258
    label "zaj&#347;cie"
  ]
  node [
    id 259
    label "burying"
  ]
  node [
    id 260
    label "zasypanie"
  ]
  node [
    id 261
    label "zw&#322;oki"
  ]
  node [
    id 262
    label "burial"
  ]
  node [
    id 263
    label "w&#322;o&#380;enie"
  ]
  node [
    id 264
    label "porobienie"
  ]
  node [
    id 265
    label "uniemo&#380;liwienie"
  ]
  node [
    id 266
    label "po&#322;o&#380;enie"
  ]
  node [
    id 267
    label "odpoczywanie"
  ]
  node [
    id 268
    label "poumieszczanie"
  ]
  node [
    id 269
    label "powk&#322;adanie"
  ]
  node [
    id 270
    label "umieszczanie"
  ]
  node [
    id 271
    label "potrzymanie"
  ]
  node [
    id 272
    label "dochowanie_si&#281;"
  ]
  node [
    id 273
    label "wk&#322;adanie"
  ]
  node [
    id 274
    label "concealment"
  ]
  node [
    id 275
    label "ukrywanie"
  ]
  node [
    id 276
    label "zmar&#322;y"
  ]
  node [
    id 277
    label "sk&#322;adanie"
  ]
  node [
    id 278
    label "opiekowanie_si&#281;"
  ]
  node [
    id 279
    label "zachowywanie"
  ]
  node [
    id 280
    label "education"
  ]
  node [
    id 281
    label "czucie"
  ]
  node [
    id 282
    label "clasp"
  ]
  node [
    id 283
    label "wychowywanie_si&#281;"
  ]
  node [
    id 284
    label "przetrzymywanie"
  ]
  node [
    id 285
    label "boarding"
  ]
  node [
    id 286
    label "niewidoczny"
  ]
  node [
    id 287
    label "hodowanie"
  ]
  node [
    id 288
    label "lie"
  ]
  node [
    id 289
    label "odpoczywa&#263;"
  ]
  node [
    id 290
    label "nadzieja"
  ]
  node [
    id 291
    label "remainder"
  ]
  node [
    id 292
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 293
    label "stan&#261;&#263;"
  ]
  node [
    id 294
    label "zacz&#261;&#263;"
  ]
  node [
    id 295
    label "znalezienie_si&#281;"
  ]
  node [
    id 296
    label "usi&#261;dni&#281;cie"
  ]
  node [
    id 297
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 298
    label "Katar"
  ]
  node [
    id 299
    label "Mazowsze"
  ]
  node [
    id 300
    label "Libia"
  ]
  node [
    id 301
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 302
    label "Gwatemala"
  ]
  node [
    id 303
    label "Anglia"
  ]
  node [
    id 304
    label "Amazonia"
  ]
  node [
    id 305
    label "Ekwador"
  ]
  node [
    id 306
    label "Afganistan"
  ]
  node [
    id 307
    label "Bordeaux"
  ]
  node [
    id 308
    label "Tad&#380;ykistan"
  ]
  node [
    id 309
    label "Bhutan"
  ]
  node [
    id 310
    label "Argentyna"
  ]
  node [
    id 311
    label "D&#380;ibuti"
  ]
  node [
    id 312
    label "Wenezuela"
  ]
  node [
    id 313
    label "Gabon"
  ]
  node [
    id 314
    label "Ukraina"
  ]
  node [
    id 315
    label "Naddniestrze"
  ]
  node [
    id 316
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 317
    label "Europa_Zachodnia"
  ]
  node [
    id 318
    label "Armagnac"
  ]
  node [
    id 319
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 320
    label "Rwanda"
  ]
  node [
    id 321
    label "Liechtenstein"
  ]
  node [
    id 322
    label "Amhara"
  ]
  node [
    id 323
    label "organizacja"
  ]
  node [
    id 324
    label "Sri_Lanka"
  ]
  node [
    id 325
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 326
    label "Zamojszczyzna"
  ]
  node [
    id 327
    label "Madagaskar"
  ]
  node [
    id 328
    label "Kongo"
  ]
  node [
    id 329
    label "Tonga"
  ]
  node [
    id 330
    label "Bangladesz"
  ]
  node [
    id 331
    label "Kanada"
  ]
  node [
    id 332
    label "Turkiestan"
  ]
  node [
    id 333
    label "Wehrlen"
  ]
  node [
    id 334
    label "Ma&#322;opolska"
  ]
  node [
    id 335
    label "Algieria"
  ]
  node [
    id 336
    label "Noworosja"
  ]
  node [
    id 337
    label "Uganda"
  ]
  node [
    id 338
    label "Surinam"
  ]
  node [
    id 339
    label "Sahara_Zachodnia"
  ]
  node [
    id 340
    label "Chile"
  ]
  node [
    id 341
    label "Lubelszczyzna"
  ]
  node [
    id 342
    label "W&#281;gry"
  ]
  node [
    id 343
    label "Mezoameryka"
  ]
  node [
    id 344
    label "Birma"
  ]
  node [
    id 345
    label "Ba&#322;kany"
  ]
  node [
    id 346
    label "Kurdystan"
  ]
  node [
    id 347
    label "Kazachstan"
  ]
  node [
    id 348
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 349
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 350
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 351
    label "Armenia"
  ]
  node [
    id 352
    label "Tuwalu"
  ]
  node [
    id 353
    label "Timor_Wschodni"
  ]
  node [
    id 354
    label "Baszkiria"
  ]
  node [
    id 355
    label "Szkocja"
  ]
  node [
    id 356
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 357
    label "Tonkin"
  ]
  node [
    id 358
    label "Maghreb"
  ]
  node [
    id 359
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 360
    label "Izrael"
  ]
  node [
    id 361
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 362
    label "Nadrenia"
  ]
  node [
    id 363
    label "Estonia"
  ]
  node [
    id 364
    label "Komory"
  ]
  node [
    id 365
    label "Podhale"
  ]
  node [
    id 366
    label "Wielkopolska"
  ]
  node [
    id 367
    label "Zabajkale"
  ]
  node [
    id 368
    label "Kamerun"
  ]
  node [
    id 369
    label "Haiti"
  ]
  node [
    id 370
    label "Belize"
  ]
  node [
    id 371
    label "Sierra_Leone"
  ]
  node [
    id 372
    label "Apulia"
  ]
  node [
    id 373
    label "Luksemburg"
  ]
  node [
    id 374
    label "brzeg"
  ]
  node [
    id 375
    label "USA"
  ]
  node [
    id 376
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 377
    label "Barbados"
  ]
  node [
    id 378
    label "San_Marino"
  ]
  node [
    id 379
    label "Bu&#322;garia"
  ]
  node [
    id 380
    label "Indonezja"
  ]
  node [
    id 381
    label "Wietnam"
  ]
  node [
    id 382
    label "Bojkowszczyzna"
  ]
  node [
    id 383
    label "Malawi"
  ]
  node [
    id 384
    label "Francja"
  ]
  node [
    id 385
    label "Zambia"
  ]
  node [
    id 386
    label "Kujawy"
  ]
  node [
    id 387
    label "Angola"
  ]
  node [
    id 388
    label "Liguria"
  ]
  node [
    id 389
    label "Grenada"
  ]
  node [
    id 390
    label "Pamir"
  ]
  node [
    id 391
    label "Nepal"
  ]
  node [
    id 392
    label "Panama"
  ]
  node [
    id 393
    label "Rumunia"
  ]
  node [
    id 394
    label "Indochiny"
  ]
  node [
    id 395
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 396
    label "Polinezja"
  ]
  node [
    id 397
    label "Kurpie"
  ]
  node [
    id 398
    label "Podlasie"
  ]
  node [
    id 399
    label "S&#261;decczyzna"
  ]
  node [
    id 400
    label "Umbria"
  ]
  node [
    id 401
    label "Czarnog&#243;ra"
  ]
  node [
    id 402
    label "Malediwy"
  ]
  node [
    id 403
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 404
    label "S&#322;owacja"
  ]
  node [
    id 405
    label "Karaiby"
  ]
  node [
    id 406
    label "Ukraina_Zachodnia"
  ]
  node [
    id 407
    label "Kielecczyzna"
  ]
  node [
    id 408
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 409
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 410
    label "Egipt"
  ]
  node [
    id 411
    label "Kalabria"
  ]
  node [
    id 412
    label "Kolumbia"
  ]
  node [
    id 413
    label "Mozambik"
  ]
  node [
    id 414
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 415
    label "Laos"
  ]
  node [
    id 416
    label "Burundi"
  ]
  node [
    id 417
    label "Suazi"
  ]
  node [
    id 418
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 419
    label "Czechy"
  ]
  node [
    id 420
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 421
    label "Wyspy_Marshalla"
  ]
  node [
    id 422
    label "Dominika"
  ]
  node [
    id 423
    label "Trynidad_i_Tobago"
  ]
  node [
    id 424
    label "Syria"
  ]
  node [
    id 425
    label "Palau"
  ]
  node [
    id 426
    label "Skandynawia"
  ]
  node [
    id 427
    label "Gwinea_Bissau"
  ]
  node [
    id 428
    label "Liberia"
  ]
  node [
    id 429
    label "Jamajka"
  ]
  node [
    id 430
    label "Zimbabwe"
  ]
  node [
    id 431
    label "Polska"
  ]
  node [
    id 432
    label "Bory_Tucholskie"
  ]
  node [
    id 433
    label "Huculszczyzna"
  ]
  node [
    id 434
    label "Tyrol"
  ]
  node [
    id 435
    label "Turyngia"
  ]
  node [
    id 436
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 437
    label "Dominikana"
  ]
  node [
    id 438
    label "Senegal"
  ]
  node [
    id 439
    label "Togo"
  ]
  node [
    id 440
    label "Gujana"
  ]
  node [
    id 441
    label "Albania"
  ]
  node [
    id 442
    label "Zair"
  ]
  node [
    id 443
    label "Meksyk"
  ]
  node [
    id 444
    label "Gruzja"
  ]
  node [
    id 445
    label "Macedonia"
  ]
  node [
    id 446
    label "Kambod&#380;a"
  ]
  node [
    id 447
    label "Chorwacja"
  ]
  node [
    id 448
    label "Monako"
  ]
  node [
    id 449
    label "Mauritius"
  ]
  node [
    id 450
    label "Gwinea"
  ]
  node [
    id 451
    label "Mali"
  ]
  node [
    id 452
    label "Nigeria"
  ]
  node [
    id 453
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 454
    label "Hercegowina"
  ]
  node [
    id 455
    label "Kostaryka"
  ]
  node [
    id 456
    label "Lotaryngia"
  ]
  node [
    id 457
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 458
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 459
    label "Hanower"
  ]
  node [
    id 460
    label "Paragwaj"
  ]
  node [
    id 461
    label "W&#322;ochy"
  ]
  node [
    id 462
    label "Seszele"
  ]
  node [
    id 463
    label "Wyspy_Salomona"
  ]
  node [
    id 464
    label "Hiszpania"
  ]
  node [
    id 465
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 466
    label "Walia"
  ]
  node [
    id 467
    label "Boliwia"
  ]
  node [
    id 468
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 469
    label "Opolskie"
  ]
  node [
    id 470
    label "Kirgistan"
  ]
  node [
    id 471
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 472
    label "Irlandia"
  ]
  node [
    id 473
    label "Kampania"
  ]
  node [
    id 474
    label "Czad"
  ]
  node [
    id 475
    label "Irak"
  ]
  node [
    id 476
    label "Lesoto"
  ]
  node [
    id 477
    label "Malta"
  ]
  node [
    id 478
    label "Andora"
  ]
  node [
    id 479
    label "Sand&#380;ak"
  ]
  node [
    id 480
    label "Chiny"
  ]
  node [
    id 481
    label "Filipiny"
  ]
  node [
    id 482
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 483
    label "Syjon"
  ]
  node [
    id 484
    label "Niemcy"
  ]
  node [
    id 485
    label "Kabylia"
  ]
  node [
    id 486
    label "Lombardia"
  ]
  node [
    id 487
    label "Warmia"
  ]
  node [
    id 488
    label "Nikaragua"
  ]
  node [
    id 489
    label "Pakistan"
  ]
  node [
    id 490
    label "Brazylia"
  ]
  node [
    id 491
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 492
    label "Kaszmir"
  ]
  node [
    id 493
    label "Maroko"
  ]
  node [
    id 494
    label "Portugalia"
  ]
  node [
    id 495
    label "Niger"
  ]
  node [
    id 496
    label "Kenia"
  ]
  node [
    id 497
    label "Botswana"
  ]
  node [
    id 498
    label "Fid&#380;i"
  ]
  node [
    id 499
    label "Tunezja"
  ]
  node [
    id 500
    label "Australia"
  ]
  node [
    id 501
    label "Tajlandia"
  ]
  node [
    id 502
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 503
    label "&#321;&#243;dzkie"
  ]
  node [
    id 504
    label "Kaukaz"
  ]
  node [
    id 505
    label "Burkina_Faso"
  ]
  node [
    id 506
    label "Tanzania"
  ]
  node [
    id 507
    label "Benin"
  ]
  node [
    id 508
    label "Europa_Wschodnia"
  ]
  node [
    id 509
    label "interior"
  ]
  node [
    id 510
    label "Indie"
  ]
  node [
    id 511
    label "&#321;otwa"
  ]
  node [
    id 512
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 513
    label "Biskupizna"
  ]
  node [
    id 514
    label "Kiribati"
  ]
  node [
    id 515
    label "Antigua_i_Barbuda"
  ]
  node [
    id 516
    label "Rodezja"
  ]
  node [
    id 517
    label "Afryka_Wschodnia"
  ]
  node [
    id 518
    label "Cypr"
  ]
  node [
    id 519
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 520
    label "Podkarpacie"
  ]
  node [
    id 521
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 522
    label "obszar"
  ]
  node [
    id 523
    label "Peru"
  ]
  node [
    id 524
    label "Afryka_Zachodnia"
  ]
  node [
    id 525
    label "Toskania"
  ]
  node [
    id 526
    label "Austria"
  ]
  node [
    id 527
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 528
    label "Urugwaj"
  ]
  node [
    id 529
    label "Podbeskidzie"
  ]
  node [
    id 530
    label "Jordania"
  ]
  node [
    id 531
    label "Bo&#347;nia"
  ]
  node [
    id 532
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 533
    label "Grecja"
  ]
  node [
    id 534
    label "Azerbejd&#380;an"
  ]
  node [
    id 535
    label "Oceania"
  ]
  node [
    id 536
    label "Turcja"
  ]
  node [
    id 537
    label "Pomorze_Zachodnie"
  ]
  node [
    id 538
    label "Samoa"
  ]
  node [
    id 539
    label "Powi&#347;le"
  ]
  node [
    id 540
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 541
    label "ziemia"
  ]
  node [
    id 542
    label "Sudan"
  ]
  node [
    id 543
    label "Oman"
  ]
  node [
    id 544
    label "&#321;emkowszczyzna"
  ]
  node [
    id 545
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 546
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 547
    label "Uzbekistan"
  ]
  node [
    id 548
    label "Portoryko"
  ]
  node [
    id 549
    label "Honduras"
  ]
  node [
    id 550
    label "Mongolia"
  ]
  node [
    id 551
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 552
    label "Kaszuby"
  ]
  node [
    id 553
    label "Ko&#322;yma"
  ]
  node [
    id 554
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 555
    label "Szlezwik"
  ]
  node [
    id 556
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 557
    label "Serbia"
  ]
  node [
    id 558
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 559
    label "Tajwan"
  ]
  node [
    id 560
    label "Wielka_Brytania"
  ]
  node [
    id 561
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 562
    label "Liban"
  ]
  node [
    id 563
    label "Japonia"
  ]
  node [
    id 564
    label "Ghana"
  ]
  node [
    id 565
    label "Belgia"
  ]
  node [
    id 566
    label "Bahrajn"
  ]
  node [
    id 567
    label "Mikronezja"
  ]
  node [
    id 568
    label "Etiopia"
  ]
  node [
    id 569
    label "Polesie"
  ]
  node [
    id 570
    label "Kuwejt"
  ]
  node [
    id 571
    label "Kerala"
  ]
  node [
    id 572
    label "Mazury"
  ]
  node [
    id 573
    label "Bahamy"
  ]
  node [
    id 574
    label "Rosja"
  ]
  node [
    id 575
    label "Mo&#322;dawia"
  ]
  node [
    id 576
    label "Palestyna"
  ]
  node [
    id 577
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 578
    label "Lauda"
  ]
  node [
    id 579
    label "Azja_Wschodnia"
  ]
  node [
    id 580
    label "Litwa"
  ]
  node [
    id 581
    label "S&#322;owenia"
  ]
  node [
    id 582
    label "Szwajcaria"
  ]
  node [
    id 583
    label "Erytrea"
  ]
  node [
    id 584
    label "Zakarpacie"
  ]
  node [
    id 585
    label "Arabia_Saudyjska"
  ]
  node [
    id 586
    label "Kuba"
  ]
  node [
    id 587
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 588
    label "Galicja"
  ]
  node [
    id 589
    label "Lubuskie"
  ]
  node [
    id 590
    label "Laponia"
  ]
  node [
    id 591
    label "granica_pa&#324;stwa"
  ]
  node [
    id 592
    label "Malezja"
  ]
  node [
    id 593
    label "Korea"
  ]
  node [
    id 594
    label "Yorkshire"
  ]
  node [
    id 595
    label "Bawaria"
  ]
  node [
    id 596
    label "Zag&#243;rze"
  ]
  node [
    id 597
    label "Jemen"
  ]
  node [
    id 598
    label "Nowa_Zelandia"
  ]
  node [
    id 599
    label "Andaluzja"
  ]
  node [
    id 600
    label "Namibia"
  ]
  node [
    id 601
    label "Nauru"
  ]
  node [
    id 602
    label "&#379;ywiecczyzna"
  ]
  node [
    id 603
    label "Brunei"
  ]
  node [
    id 604
    label "Oksytania"
  ]
  node [
    id 605
    label "Opolszczyzna"
  ]
  node [
    id 606
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 607
    label "Kociewie"
  ]
  node [
    id 608
    label "Khitai"
  ]
  node [
    id 609
    label "Mauretania"
  ]
  node [
    id 610
    label "Iran"
  ]
  node [
    id 611
    label "Gambia"
  ]
  node [
    id 612
    label "Somalia"
  ]
  node [
    id 613
    label "Holandia"
  ]
  node [
    id 614
    label "Lasko"
  ]
  node [
    id 615
    label "Turkmenistan"
  ]
  node [
    id 616
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 617
    label "Salwador"
  ]
  node [
    id 618
    label "woda"
  ]
  node [
    id 619
    label "linia"
  ]
  node [
    id 620
    label "zbi&#243;r"
  ]
  node [
    id 621
    label "ekoton"
  ]
  node [
    id 622
    label "str&#261;d"
  ]
  node [
    id 623
    label "plantowa&#263;"
  ]
  node [
    id 624
    label "zapadnia"
  ]
  node [
    id 625
    label "budynek"
  ]
  node [
    id 626
    label "skorupa_ziemska"
  ]
  node [
    id 627
    label "glinowanie"
  ]
  node [
    id 628
    label "martwica"
  ]
  node [
    id 629
    label "teren"
  ]
  node [
    id 630
    label "litosfera"
  ]
  node [
    id 631
    label "penetrator"
  ]
  node [
    id 632
    label "glinowa&#263;"
  ]
  node [
    id 633
    label "domain"
  ]
  node [
    id 634
    label "podglebie"
  ]
  node [
    id 635
    label "kompleks_sorpcyjny"
  ]
  node [
    id 636
    label "kort"
  ]
  node [
    id 637
    label "czynnik_produkcji"
  ]
  node [
    id 638
    label "pojazd"
  ]
  node [
    id 639
    label "powierzchnia"
  ]
  node [
    id 640
    label "pr&#243;chnica"
  ]
  node [
    id 641
    label "pomieszczenie"
  ]
  node [
    id 642
    label "ryzosfera"
  ]
  node [
    id 643
    label "p&#322;aszczyzna"
  ]
  node [
    id 644
    label "dotleni&#263;"
  ]
  node [
    id 645
    label "glej"
  ]
  node [
    id 646
    label "pa&#324;stwo"
  ]
  node [
    id 647
    label "posadzka"
  ]
  node [
    id 648
    label "geosystem"
  ]
  node [
    id 649
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 650
    label "przestrze&#324;"
  ]
  node [
    id 651
    label "podmiot"
  ]
  node [
    id 652
    label "jednostka_organizacyjna"
  ]
  node [
    id 653
    label "struktura"
  ]
  node [
    id 654
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 655
    label "TOPR"
  ]
  node [
    id 656
    label "endecki"
  ]
  node [
    id 657
    label "zesp&#243;&#322;"
  ]
  node [
    id 658
    label "przedstawicielstwo"
  ]
  node [
    id 659
    label "od&#322;am"
  ]
  node [
    id 660
    label "Cepelia"
  ]
  node [
    id 661
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 662
    label "ZBoWiD"
  ]
  node [
    id 663
    label "organization"
  ]
  node [
    id 664
    label "centrala"
  ]
  node [
    id 665
    label "GOPR"
  ]
  node [
    id 666
    label "ZOMO"
  ]
  node [
    id 667
    label "ZMP"
  ]
  node [
    id 668
    label "komitet_koordynacyjny"
  ]
  node [
    id 669
    label "przybud&#243;wka"
  ]
  node [
    id 670
    label "boj&#243;wka"
  ]
  node [
    id 671
    label "p&#243;&#322;noc"
  ]
  node [
    id 672
    label "Kosowo"
  ]
  node [
    id 673
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 674
    label "Zab&#322;ocie"
  ]
  node [
    id 675
    label "zach&#243;d"
  ]
  node [
    id 676
    label "po&#322;udnie"
  ]
  node [
    id 677
    label "Pow&#261;zki"
  ]
  node [
    id 678
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 679
    label "Piotrowo"
  ]
  node [
    id 680
    label "Olszanica"
  ]
  node [
    id 681
    label "Ruda_Pabianicka"
  ]
  node [
    id 682
    label "holarktyka"
  ]
  node [
    id 683
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 684
    label "Ludwin&#243;w"
  ]
  node [
    id 685
    label "Arktyka"
  ]
  node [
    id 686
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 687
    label "Zabu&#380;e"
  ]
  node [
    id 688
    label "antroposfera"
  ]
  node [
    id 689
    label "Neogea"
  ]
  node [
    id 690
    label "terytorium"
  ]
  node [
    id 691
    label "Syberia_Zachodnia"
  ]
  node [
    id 692
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 693
    label "zakres"
  ]
  node [
    id 694
    label "pas_planetoid"
  ]
  node [
    id 695
    label "Syberia_Wschodnia"
  ]
  node [
    id 696
    label "Antarktyka"
  ]
  node [
    id 697
    label "Rakowice"
  ]
  node [
    id 698
    label "akrecja"
  ]
  node [
    id 699
    label "wymiar"
  ]
  node [
    id 700
    label "&#321;&#281;g"
  ]
  node [
    id 701
    label "Kresy_Zachodnie"
  ]
  node [
    id 702
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 703
    label "wsch&#243;d"
  ]
  node [
    id 704
    label "Notogea"
  ]
  node [
    id 705
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 706
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 707
    label "Pend&#380;ab"
  ]
  node [
    id 708
    label "funt_liba&#324;ski"
  ]
  node [
    id 709
    label "strefa_euro"
  ]
  node [
    id 710
    label "Pozna&#324;"
  ]
  node [
    id 711
    label "lira_malta&#324;ska"
  ]
  node [
    id 712
    label "Gozo"
  ]
  node [
    id 713
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 714
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 715
    label "dolar_namibijski"
  ]
  node [
    id 716
    label "milrejs"
  ]
  node [
    id 717
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 718
    label "NATO"
  ]
  node [
    id 719
    label "escudo_portugalskie"
  ]
  node [
    id 720
    label "dolar_bahamski"
  ]
  node [
    id 721
    label "Wielka_Bahama"
  ]
  node [
    id 722
    label "dolar_liberyjski"
  ]
  node [
    id 723
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 724
    label "riel"
  ]
  node [
    id 725
    label "Karelia"
  ]
  node [
    id 726
    label "Mari_El"
  ]
  node [
    id 727
    label "Inguszetia"
  ]
  node [
    id 728
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 729
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 730
    label "Udmurcja"
  ]
  node [
    id 731
    label "Newa"
  ]
  node [
    id 732
    label "&#321;adoga"
  ]
  node [
    id 733
    label "Czeczenia"
  ]
  node [
    id 734
    label "Anadyr"
  ]
  node [
    id 735
    label "Syberia"
  ]
  node [
    id 736
    label "Tatarstan"
  ]
  node [
    id 737
    label "Wszechrosja"
  ]
  node [
    id 738
    label "Azja"
  ]
  node [
    id 739
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 740
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 741
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 742
    label "Witim"
  ]
  node [
    id 743
    label "Kamczatka"
  ]
  node [
    id 744
    label "Jama&#322;"
  ]
  node [
    id 745
    label "Dagestan"
  ]
  node [
    id 746
    label "Tuwa"
  ]
  node [
    id 747
    label "car"
  ]
  node [
    id 748
    label "Komi"
  ]
  node [
    id 749
    label "Czuwaszja"
  ]
  node [
    id 750
    label "Chakasja"
  ]
  node [
    id 751
    label "Perm"
  ]
  node [
    id 752
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 753
    label "Ajon"
  ]
  node [
    id 754
    label "Adygeja"
  ]
  node [
    id 755
    label "Dniepr"
  ]
  node [
    id 756
    label "rubel_rosyjski"
  ]
  node [
    id 757
    label "Don"
  ]
  node [
    id 758
    label "Mordowia"
  ]
  node [
    id 759
    label "s&#322;owianofilstwo"
  ]
  node [
    id 760
    label "lew"
  ]
  node [
    id 761
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 762
    label "Dobrud&#380;a"
  ]
  node [
    id 763
    label "Unia_Europejska"
  ]
  node [
    id 764
    label "lira_izraelska"
  ]
  node [
    id 765
    label "szekel"
  ]
  node [
    id 766
    label "Galilea"
  ]
  node [
    id 767
    label "Judea"
  ]
  node [
    id 768
    label "Luksemburgia"
  ]
  node [
    id 769
    label "frank_belgijski"
  ]
  node [
    id 770
    label "Limburgia"
  ]
  node [
    id 771
    label "Brabancja"
  ]
  node [
    id 772
    label "Walonia"
  ]
  node [
    id 773
    label "Flandria"
  ]
  node [
    id 774
    label "Niderlandy"
  ]
  node [
    id 775
    label "dinar_iracki"
  ]
  node [
    id 776
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 777
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 778
    label "szyling_ugandyjski"
  ]
  node [
    id 779
    label "kafar"
  ]
  node [
    id 780
    label "dolar_jamajski"
  ]
  node [
    id 781
    label "ringgit"
  ]
  node [
    id 782
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 783
    label "Borneo"
  ]
  node [
    id 784
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 785
    label "dolar_surinamski"
  ]
  node [
    id 786
    label "funt_suda&#324;ski"
  ]
  node [
    id 787
    label "dolar_guja&#324;ski"
  ]
  node [
    id 788
    label "Manica"
  ]
  node [
    id 789
    label "escudo_mozambickie"
  ]
  node [
    id 790
    label "Cabo_Delgado"
  ]
  node [
    id 791
    label "Inhambane"
  ]
  node [
    id 792
    label "Maputo"
  ]
  node [
    id 793
    label "Gaza"
  ]
  node [
    id 794
    label "Niasa"
  ]
  node [
    id 795
    label "Nampula"
  ]
  node [
    id 796
    label "metical"
  ]
  node [
    id 797
    label "Sahara"
  ]
  node [
    id 798
    label "inti"
  ]
  node [
    id 799
    label "sol"
  ]
  node [
    id 800
    label "kip"
  ]
  node [
    id 801
    label "Pireneje"
  ]
  node [
    id 802
    label "euro"
  ]
  node [
    id 803
    label "kwacha_zambijska"
  ]
  node [
    id 804
    label "Buriaci"
  ]
  node [
    id 805
    label "tugrik"
  ]
  node [
    id 806
    label "ajmak"
  ]
  node [
    id 807
    label "balboa"
  ]
  node [
    id 808
    label "Ameryka_Centralna"
  ]
  node [
    id 809
    label "dolar"
  ]
  node [
    id 810
    label "gulden"
  ]
  node [
    id 811
    label "Zelandia"
  ]
  node [
    id 812
    label "manat_turkme&#324;ski"
  ]
  node [
    id 813
    label "dolar_Tuvalu"
  ]
  node [
    id 814
    label "zair"
  ]
  node [
    id 815
    label "Katanga"
  ]
  node [
    id 816
    label "frank_szwajcarski"
  ]
  node [
    id 817
    label "dolar_Belize"
  ]
  node [
    id 818
    label "colon"
  ]
  node [
    id 819
    label "Dyja"
  ]
  node [
    id 820
    label "korona_czeska"
  ]
  node [
    id 821
    label "Izera"
  ]
  node [
    id 822
    label "ugija"
  ]
  node [
    id 823
    label "szyling_kenijski"
  ]
  node [
    id 824
    label "Nachiczewan"
  ]
  node [
    id 825
    label "manat_azerski"
  ]
  node [
    id 826
    label "Karabach"
  ]
  node [
    id 827
    label "Bengal"
  ]
  node [
    id 828
    label "taka"
  ]
  node [
    id 829
    label "Ocean_Spokojny"
  ]
  node [
    id 830
    label "dolar_Kiribati"
  ]
  node [
    id 831
    label "peso_filipi&#324;skie"
  ]
  node [
    id 832
    label "Cebu"
  ]
  node [
    id 833
    label "Atlantyk"
  ]
  node [
    id 834
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 835
    label "Ulster"
  ]
  node [
    id 836
    label "funt_irlandzki"
  ]
  node [
    id 837
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 838
    label "cedi"
  ]
  node [
    id 839
    label "ariary"
  ]
  node [
    id 840
    label "Ocean_Indyjski"
  ]
  node [
    id 841
    label "frank_malgaski"
  ]
  node [
    id 842
    label "Estremadura"
  ]
  node [
    id 843
    label "Kastylia"
  ]
  node [
    id 844
    label "Rzym_Zachodni"
  ]
  node [
    id 845
    label "Aragonia"
  ]
  node [
    id 846
    label "hacjender"
  ]
  node [
    id 847
    label "Asturia"
  ]
  node [
    id 848
    label "Baskonia"
  ]
  node [
    id 849
    label "Majorka"
  ]
  node [
    id 850
    label "Walencja"
  ]
  node [
    id 851
    label "peseta"
  ]
  node [
    id 852
    label "Katalonia"
  ]
  node [
    id 853
    label "peso_chilijskie"
  ]
  node [
    id 854
    label "Indie_Zachodnie"
  ]
  node [
    id 855
    label "Sikkim"
  ]
  node [
    id 856
    label "Asam"
  ]
  node [
    id 857
    label "rupia_indyjska"
  ]
  node [
    id 858
    label "Indie_Portugalskie"
  ]
  node [
    id 859
    label "Indie_Wschodnie"
  ]
  node [
    id 860
    label "Bollywood"
  ]
  node [
    id 861
    label "jen"
  ]
  node [
    id 862
    label "jinja"
  ]
  node [
    id 863
    label "Okinawa"
  ]
  node [
    id 864
    label "Japonica"
  ]
  node [
    id 865
    label "Rugia"
  ]
  node [
    id 866
    label "Saksonia"
  ]
  node [
    id 867
    label "Dolna_Saksonia"
  ]
  node [
    id 868
    label "Anglosas"
  ]
  node [
    id 869
    label "Hesja"
  ]
  node [
    id 870
    label "Wirtembergia"
  ]
  node [
    id 871
    label "Po&#322;abie"
  ]
  node [
    id 872
    label "Germania"
  ]
  node [
    id 873
    label "Frankonia"
  ]
  node [
    id 874
    label "Badenia"
  ]
  node [
    id 875
    label "Holsztyn"
  ]
  node [
    id 876
    label "marka"
  ]
  node [
    id 877
    label "Szwabia"
  ]
  node [
    id 878
    label "Brandenburgia"
  ]
  node [
    id 879
    label "Niemcy_Zachodnie"
  ]
  node [
    id 880
    label "Westfalia"
  ]
  node [
    id 881
    label "Helgoland"
  ]
  node [
    id 882
    label "Karlsbad"
  ]
  node [
    id 883
    label "Niemcy_Wschodnie"
  ]
  node [
    id 884
    label "Piemont"
  ]
  node [
    id 885
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 886
    label "Sardynia"
  ]
  node [
    id 887
    label "Italia"
  ]
  node [
    id 888
    label "Ok&#281;cie"
  ]
  node [
    id 889
    label "Karyntia"
  ]
  node [
    id 890
    label "Romania"
  ]
  node [
    id 891
    label "Warszawa"
  ]
  node [
    id 892
    label "lir"
  ]
  node [
    id 893
    label "Sycylia"
  ]
  node [
    id 894
    label "Dacja"
  ]
  node [
    id 895
    label "lej_rumu&#324;ski"
  ]
  node [
    id 896
    label "Siedmiogr&#243;d"
  ]
  node [
    id 897
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 898
    label "funt_syryjski"
  ]
  node [
    id 899
    label "alawizm"
  ]
  node [
    id 900
    label "frank_rwandyjski"
  ]
  node [
    id 901
    label "dinar_Bahrajnu"
  ]
  node [
    id 902
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 903
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 904
    label "frank_luksemburski"
  ]
  node [
    id 905
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 906
    label "peso_kuba&#324;skie"
  ]
  node [
    id 907
    label "frank_monakijski"
  ]
  node [
    id 908
    label "dinar_algierski"
  ]
  node [
    id 909
    label "Wojwodina"
  ]
  node [
    id 910
    label "dinar_serbski"
  ]
  node [
    id 911
    label "Orinoko"
  ]
  node [
    id 912
    label "boliwar"
  ]
  node [
    id 913
    label "tenge"
  ]
  node [
    id 914
    label "lek"
  ]
  node [
    id 915
    label "frank_alba&#324;ski"
  ]
  node [
    id 916
    label "dolar_Barbadosu"
  ]
  node [
    id 917
    label "Antyle"
  ]
  node [
    id 918
    label "kyat"
  ]
  node [
    id 919
    label "c&#243;rdoba"
  ]
  node [
    id 920
    label "Paros"
  ]
  node [
    id 921
    label "Epir"
  ]
  node [
    id 922
    label "panhellenizm"
  ]
  node [
    id 923
    label "Eubea"
  ]
  node [
    id 924
    label "Rodos"
  ]
  node [
    id 925
    label "Achaja"
  ]
  node [
    id 926
    label "Termopile"
  ]
  node [
    id 927
    label "Attyka"
  ]
  node [
    id 928
    label "Hellada"
  ]
  node [
    id 929
    label "Etolia"
  ]
  node [
    id 930
    label "palestra"
  ]
  node [
    id 931
    label "Kreta"
  ]
  node [
    id 932
    label "drachma"
  ]
  node [
    id 933
    label "Olimp"
  ]
  node [
    id 934
    label "Tesalia"
  ]
  node [
    id 935
    label "Peloponez"
  ]
  node [
    id 936
    label "Eolia"
  ]
  node [
    id 937
    label "Beocja"
  ]
  node [
    id 938
    label "Parnas"
  ]
  node [
    id 939
    label "Lesbos"
  ]
  node [
    id 940
    label "Mariany"
  ]
  node [
    id 941
    label "Salzburg"
  ]
  node [
    id 942
    label "Rakuzy"
  ]
  node [
    id 943
    label "konsulent"
  ]
  node [
    id 944
    label "szyling_austryjacki"
  ]
  node [
    id 945
    label "birr"
  ]
  node [
    id 946
    label "negus"
  ]
  node [
    id 947
    label "Jawa"
  ]
  node [
    id 948
    label "Sumatra"
  ]
  node [
    id 949
    label "rupia_indonezyjska"
  ]
  node [
    id 950
    label "Nowa_Gwinea"
  ]
  node [
    id 951
    label "Moluki"
  ]
  node [
    id 952
    label "boliviano"
  ]
  node [
    id 953
    label "Pikardia"
  ]
  node [
    id 954
    label "Masyw_Centralny"
  ]
  node [
    id 955
    label "Akwitania"
  ]
  node [
    id 956
    label "Alzacja"
  ]
  node [
    id 957
    label "Sekwana"
  ]
  node [
    id 958
    label "Langwedocja"
  ]
  node [
    id 959
    label "Martynika"
  ]
  node [
    id 960
    label "Bretania"
  ]
  node [
    id 961
    label "Sabaudia"
  ]
  node [
    id 962
    label "Korsyka"
  ]
  node [
    id 963
    label "Normandia"
  ]
  node [
    id 964
    label "Gaskonia"
  ]
  node [
    id 965
    label "Burgundia"
  ]
  node [
    id 966
    label "frank_francuski"
  ]
  node [
    id 967
    label "Wandea"
  ]
  node [
    id 968
    label "Prowansja"
  ]
  node [
    id 969
    label "Gwadelupa"
  ]
  node [
    id 970
    label "somoni"
  ]
  node [
    id 971
    label "Melanezja"
  ]
  node [
    id 972
    label "dolar_Fid&#380;i"
  ]
  node [
    id 973
    label "funt_cypryjski"
  ]
  node [
    id 974
    label "Afrodyzje"
  ]
  node [
    id 975
    label "peso_dominika&#324;skie"
  ]
  node [
    id 976
    label "Fryburg"
  ]
  node [
    id 977
    label "Bazylea"
  ]
  node [
    id 978
    label "Alpy"
  ]
  node [
    id 979
    label "Helwecja"
  ]
  node [
    id 980
    label "Berno"
  ]
  node [
    id 981
    label "sum"
  ]
  node [
    id 982
    label "Karaka&#322;pacja"
  ]
  node [
    id 983
    label "Windawa"
  ]
  node [
    id 984
    label "&#322;at"
  ]
  node [
    id 985
    label "Kurlandia"
  ]
  node [
    id 986
    label "Liwonia"
  ]
  node [
    id 987
    label "rubel_&#322;otewski"
  ]
  node [
    id 988
    label "Inflanty"
  ]
  node [
    id 989
    label "Wile&#324;szczyzna"
  ]
  node [
    id 990
    label "&#379;mud&#378;"
  ]
  node [
    id 991
    label "lit"
  ]
  node [
    id 992
    label "frank_tunezyjski"
  ]
  node [
    id 993
    label "dinar_tunezyjski"
  ]
  node [
    id 994
    label "lempira"
  ]
  node [
    id 995
    label "korona_w&#281;gierska"
  ]
  node [
    id 996
    label "forint"
  ]
  node [
    id 997
    label "Lipt&#243;w"
  ]
  node [
    id 998
    label "dong"
  ]
  node [
    id 999
    label "Annam"
  ]
  node [
    id 1000
    label "lud"
  ]
  node [
    id 1001
    label "frank_kongijski"
  ]
  node [
    id 1002
    label "szyling_somalijski"
  ]
  node [
    id 1003
    label "cruzado"
  ]
  node [
    id 1004
    label "real"
  ]
  node [
    id 1005
    label "Podole"
  ]
  node [
    id 1006
    label "Wsch&#243;d"
  ]
  node [
    id 1007
    label "Naddnieprze"
  ]
  node [
    id 1008
    label "Ma&#322;orosja"
  ]
  node [
    id 1009
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1010
    label "Nadbu&#380;e"
  ]
  node [
    id 1011
    label "hrywna"
  ]
  node [
    id 1012
    label "Zaporo&#380;e"
  ]
  node [
    id 1013
    label "Krym"
  ]
  node [
    id 1014
    label "Dniestr"
  ]
  node [
    id 1015
    label "Przykarpacie"
  ]
  node [
    id 1016
    label "Kozaczyzna"
  ]
  node [
    id 1017
    label "karbowaniec"
  ]
  node [
    id 1018
    label "Tasmania"
  ]
  node [
    id 1019
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1020
    label "dolar_australijski"
  ]
  node [
    id 1021
    label "gourde"
  ]
  node [
    id 1022
    label "escudo_angolskie"
  ]
  node [
    id 1023
    label "kwanza"
  ]
  node [
    id 1024
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1025
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1026
    label "Ad&#380;aria"
  ]
  node [
    id 1027
    label "lari"
  ]
  node [
    id 1028
    label "naira"
  ]
  node [
    id 1029
    label "P&#243;&#322;noc"
  ]
  node [
    id 1030
    label "Po&#322;udnie"
  ]
  node [
    id 1031
    label "zielona_karta"
  ]
  node [
    id 1032
    label "stan_wolny"
  ]
  node [
    id 1033
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1034
    label "Wuj_Sam"
  ]
  node [
    id 1035
    label "Zach&#243;d"
  ]
  node [
    id 1036
    label "Hudson"
  ]
  node [
    id 1037
    label "som"
  ]
  node [
    id 1038
    label "peso_urugwajskie"
  ]
  node [
    id 1039
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1040
    label "dolar_Brunei"
  ]
  node [
    id 1041
    label "rial_ira&#324;ski"
  ]
  node [
    id 1042
    label "mu&#322;&#322;a"
  ]
  node [
    id 1043
    label "Persja"
  ]
  node [
    id 1044
    label "d&#380;amahirijja"
  ]
  node [
    id 1045
    label "dinar_libijski"
  ]
  node [
    id 1046
    label "nakfa"
  ]
  node [
    id 1047
    label "rial_katarski"
  ]
  node [
    id 1048
    label "quetzal"
  ]
  node [
    id 1049
    label "won"
  ]
  node [
    id 1050
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1051
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1052
    label "guarani"
  ]
  node [
    id 1053
    label "perper"
  ]
  node [
    id 1054
    label "dinar_kuwejcki"
  ]
  node [
    id 1055
    label "dalasi"
  ]
  node [
    id 1056
    label "dolar_Zimbabwe"
  ]
  node [
    id 1057
    label "Szantung"
  ]
  node [
    id 1058
    label "Chiny_Zachodnie"
  ]
  node [
    id 1059
    label "Kuantung"
  ]
  node [
    id 1060
    label "D&#380;ungaria"
  ]
  node [
    id 1061
    label "yuan"
  ]
  node [
    id 1062
    label "Hongkong"
  ]
  node [
    id 1063
    label "Chiny_Wschodnie"
  ]
  node [
    id 1064
    label "Guangdong"
  ]
  node [
    id 1065
    label "Junnan"
  ]
  node [
    id 1066
    label "Mand&#380;uria"
  ]
  node [
    id 1067
    label "Syczuan"
  ]
  node [
    id 1068
    label "Pa&#322;uki"
  ]
  node [
    id 1069
    label "Wolin"
  ]
  node [
    id 1070
    label "z&#322;oty"
  ]
  node [
    id 1071
    label "So&#322;a"
  ]
  node [
    id 1072
    label "Suwalszczyzna"
  ]
  node [
    id 1073
    label "Krajna"
  ]
  node [
    id 1074
    label "barwy_polskie"
  ]
  node [
    id 1075
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1076
    label "Kaczawa"
  ]
  node [
    id 1077
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1078
    label "Wis&#322;a"
  ]
  node [
    id 1079
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1080
    label "Ujgur"
  ]
  node [
    id 1081
    label "Azja_Mniejsza"
  ]
  node [
    id 1082
    label "lira_turecka"
  ]
  node [
    id 1083
    label "kuna"
  ]
  node [
    id 1084
    label "dram"
  ]
  node [
    id 1085
    label "tala"
  ]
  node [
    id 1086
    label "korona_s&#322;owacka"
  ]
  node [
    id 1087
    label "Turiec"
  ]
  node [
    id 1088
    label "Himalaje"
  ]
  node [
    id 1089
    label "rupia_nepalska"
  ]
  node [
    id 1090
    label "frank_gwinejski"
  ]
  node [
    id 1091
    label "korona_esto&#324;ska"
  ]
  node [
    id 1092
    label "marka_esto&#324;ska"
  ]
  node [
    id 1093
    label "Quebec"
  ]
  node [
    id 1094
    label "dolar_kanadyjski"
  ]
  node [
    id 1095
    label "Nowa_Fundlandia"
  ]
  node [
    id 1096
    label "Zanzibar"
  ]
  node [
    id 1097
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1098
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1099
    label "&#346;wite&#378;"
  ]
  node [
    id 1100
    label "peso_kolumbijskie"
  ]
  node [
    id 1101
    label "Synaj"
  ]
  node [
    id 1102
    label "paraszyt"
  ]
  node [
    id 1103
    label "funt_egipski"
  ]
  node [
    id 1104
    label "szach"
  ]
  node [
    id 1105
    label "Baktria"
  ]
  node [
    id 1106
    label "afgani"
  ]
  node [
    id 1107
    label "baht"
  ]
  node [
    id 1108
    label "tolar"
  ]
  node [
    id 1109
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1110
    label "Gagauzja"
  ]
  node [
    id 1111
    label "moszaw"
  ]
  node [
    id 1112
    label "Kanaan"
  ]
  node [
    id 1113
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1114
    label "Jerozolima"
  ]
  node [
    id 1115
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1116
    label "Wiktoria"
  ]
  node [
    id 1117
    label "Guernsey"
  ]
  node [
    id 1118
    label "Conrad"
  ]
  node [
    id 1119
    label "funt_szterling"
  ]
  node [
    id 1120
    label "Portland"
  ]
  node [
    id 1121
    label "El&#380;bieta_I"
  ]
  node [
    id 1122
    label "Kornwalia"
  ]
  node [
    id 1123
    label "Dolna_Frankonia"
  ]
  node [
    id 1124
    label "Karpaty"
  ]
  node [
    id 1125
    label "Beskid_Niski"
  ]
  node [
    id 1126
    label "Mariensztat"
  ]
  node [
    id 1127
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1128
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1129
    label "Paj&#281;czno"
  ]
  node [
    id 1130
    label "Mogielnica"
  ]
  node [
    id 1131
    label "Gop&#322;o"
  ]
  node [
    id 1132
    label "Moza"
  ]
  node [
    id 1133
    label "Poprad"
  ]
  node [
    id 1134
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1135
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1136
    label "Bojanowo"
  ]
  node [
    id 1137
    label "Obra"
  ]
  node [
    id 1138
    label "Wilkowo_Polskie"
  ]
  node [
    id 1139
    label "Dobra"
  ]
  node [
    id 1140
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1141
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1142
    label "Etruria"
  ]
  node [
    id 1143
    label "Rumelia"
  ]
  node [
    id 1144
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1145
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1146
    label "Abchazja"
  ]
  node [
    id 1147
    label "Sarmata"
  ]
  node [
    id 1148
    label "Eurazja"
  ]
  node [
    id 1149
    label "Tatry"
  ]
  node [
    id 1150
    label "Podtatrze"
  ]
  node [
    id 1151
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1152
    label "jezioro"
  ]
  node [
    id 1153
    label "&#346;l&#261;sk"
  ]
  node [
    id 1154
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1155
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1156
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1157
    label "Austro-W&#281;gry"
  ]
  node [
    id 1158
    label "funt_szkocki"
  ]
  node [
    id 1159
    label "Kaledonia"
  ]
  node [
    id 1160
    label "Biskupice"
  ]
  node [
    id 1161
    label "Iwanowice"
  ]
  node [
    id 1162
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1163
    label "Rogo&#378;nik"
  ]
  node [
    id 1164
    label "Ropa"
  ]
  node [
    id 1165
    label "Buriacja"
  ]
  node [
    id 1166
    label "Rozewie"
  ]
  node [
    id 1167
    label "Norwegia"
  ]
  node [
    id 1168
    label "Szwecja"
  ]
  node [
    id 1169
    label "Finlandia"
  ]
  node [
    id 1170
    label "Aruba"
  ]
  node [
    id 1171
    label "Kajmany"
  ]
  node [
    id 1172
    label "Anguilla"
  ]
  node [
    id 1173
    label "Amazonka"
  ]
  node [
    id 1174
    label "augment"
  ]
  node [
    id 1175
    label "liczy&#263;"
  ]
  node [
    id 1176
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1177
    label "circulate"
  ]
  node [
    id 1178
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1179
    label "zmienia&#263;"
  ]
  node [
    id 1180
    label "increase"
  ]
  node [
    id 1181
    label "report"
  ]
  node [
    id 1182
    label "dyskalkulia"
  ]
  node [
    id 1183
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1184
    label "wynagrodzenie"
  ]
  node [
    id 1185
    label "wymienia&#263;"
  ]
  node [
    id 1186
    label "posiada&#263;"
  ]
  node [
    id 1187
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1188
    label "wycenia&#263;"
  ]
  node [
    id 1189
    label "bra&#263;"
  ]
  node [
    id 1190
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1191
    label "rachowa&#263;"
  ]
  node [
    id 1192
    label "count"
  ]
  node [
    id 1193
    label "tell"
  ]
  node [
    id 1194
    label "odlicza&#263;"
  ]
  node [
    id 1195
    label "dodawa&#263;"
  ]
  node [
    id 1196
    label "wyznacza&#263;"
  ]
  node [
    id 1197
    label "admit"
  ]
  node [
    id 1198
    label "policza&#263;"
  ]
  node [
    id 1199
    label "okre&#347;la&#263;"
  ]
  node [
    id 1200
    label "samog&#322;oska"
  ]
  node [
    id 1201
    label "okre&#347;lony"
  ]
  node [
    id 1202
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1203
    label "wiadomy"
  ]
  node [
    id 1204
    label "sta&#322;y"
  ]
  node [
    id 1205
    label "ci&#261;g&#322;y"
  ]
  node [
    id 1206
    label "pradawny"
  ]
  node [
    id 1207
    label "wiecznie"
  ]
  node [
    id 1208
    label "trwa&#322;y"
  ]
  node [
    id 1209
    label "ci&#261;gle"
  ]
  node [
    id 1210
    label "nieprzerwany"
  ]
  node [
    id 1211
    label "nieustanny"
  ]
  node [
    id 1212
    label "przestarza&#322;y"
  ]
  node [
    id 1213
    label "odleg&#322;y"
  ]
  node [
    id 1214
    label "przesz&#322;y"
  ]
  node [
    id 1215
    label "niegdysiejszy"
  ]
  node [
    id 1216
    label "staro&#380;ytny"
  ]
  node [
    id 1217
    label "pradawno"
  ]
  node [
    id 1218
    label "stary"
  ]
  node [
    id 1219
    label "regularny"
  ]
  node [
    id 1220
    label "jednakowy"
  ]
  node [
    id 1221
    label "zwyk&#322;y"
  ]
  node [
    id 1222
    label "stale"
  ]
  node [
    id 1223
    label "trwale"
  ]
  node [
    id 1224
    label "mocny"
  ]
  node [
    id 1225
    label "umocnienie"
  ]
  node [
    id 1226
    label "ustalanie_si&#281;"
  ]
  node [
    id 1227
    label "ustalenie_si&#281;"
  ]
  node [
    id 1228
    label "nieruchomy"
  ]
  node [
    id 1229
    label "utrwalenie_si&#281;"
  ]
  node [
    id 1230
    label "umacnianie"
  ]
  node [
    id 1231
    label "utrwalanie_si&#281;"
  ]
  node [
    id 1232
    label "trwanie"
  ]
  node [
    id 1233
    label "wra&#380;enie"
  ]
  node [
    id 1234
    label "przej&#347;cie"
  ]
  node [
    id 1235
    label "doznanie"
  ]
  node [
    id 1236
    label "poradzenie_sobie"
  ]
  node [
    id 1237
    label "przetrwanie"
  ]
  node [
    id 1238
    label "survival"
  ]
  node [
    id 1239
    label "przechodzenie"
  ]
  node [
    id 1240
    label "wytrzymywanie"
  ]
  node [
    id 1241
    label "zaznawanie"
  ]
  node [
    id 1242
    label "obejrzenie"
  ]
  node [
    id 1243
    label "widzenie"
  ]
  node [
    id 1244
    label "urzeczywistnianie"
  ]
  node [
    id 1245
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1246
    label "produkowanie"
  ]
  node [
    id 1247
    label "przeszkodzenie"
  ]
  node [
    id 1248
    label "znikni&#281;cie"
  ]
  node [
    id 1249
    label "robienie"
  ]
  node [
    id 1250
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1251
    label "przeszkadzanie"
  ]
  node [
    id 1252
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1253
    label "wyprodukowanie"
  ]
  node [
    id 1254
    label "utrzymywanie"
  ]
  node [
    id 1255
    label "subsystencja"
  ]
  node [
    id 1256
    label "utrzyma&#263;"
  ]
  node [
    id 1257
    label "egzystencja"
  ]
  node [
    id 1258
    label "wy&#380;ywienie"
  ]
  node [
    id 1259
    label "ontologicznie"
  ]
  node [
    id 1260
    label "utrzymanie"
  ]
  node [
    id 1261
    label "potencja"
  ]
  node [
    id 1262
    label "utrzymywa&#263;"
  ]
  node [
    id 1263
    label "status"
  ]
  node [
    id 1264
    label "sytuacja"
  ]
  node [
    id 1265
    label "poprzedzanie"
  ]
  node [
    id 1266
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1267
    label "laba"
  ]
  node [
    id 1268
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1269
    label "chronometria"
  ]
  node [
    id 1270
    label "rachuba_czasu"
  ]
  node [
    id 1271
    label "przep&#322;ywanie"
  ]
  node [
    id 1272
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1273
    label "czasokres"
  ]
  node [
    id 1274
    label "odczyt"
  ]
  node [
    id 1275
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1276
    label "dzieje"
  ]
  node [
    id 1277
    label "kategoria_gramatyczna"
  ]
  node [
    id 1278
    label "poprzedzenie"
  ]
  node [
    id 1279
    label "trawienie"
  ]
  node [
    id 1280
    label "pochodzi&#263;"
  ]
  node [
    id 1281
    label "okres_czasu"
  ]
  node [
    id 1282
    label "poprzedza&#263;"
  ]
  node [
    id 1283
    label "schy&#322;ek"
  ]
  node [
    id 1284
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1285
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1286
    label "zegar"
  ]
  node [
    id 1287
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1288
    label "czwarty_wymiar"
  ]
  node [
    id 1289
    label "pochodzenie"
  ]
  node [
    id 1290
    label "koniugacja"
  ]
  node [
    id 1291
    label "Zeitgeist"
  ]
  node [
    id 1292
    label "trawi&#263;"
  ]
  node [
    id 1293
    label "pogoda"
  ]
  node [
    id 1294
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1295
    label "poprzedzi&#263;"
  ]
  node [
    id 1296
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1297
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1298
    label "time_period"
  ]
  node [
    id 1299
    label "ocieranie_si&#281;"
  ]
  node [
    id 1300
    label "otoczenie_si&#281;"
  ]
  node [
    id 1301
    label "posiedzenie"
  ]
  node [
    id 1302
    label "otarcie_si&#281;"
  ]
  node [
    id 1303
    label "atakowanie"
  ]
  node [
    id 1304
    label "otaczanie_si&#281;"
  ]
  node [
    id 1305
    label "wyj&#347;cie"
  ]
  node [
    id 1306
    label "zmierzanie"
  ]
  node [
    id 1307
    label "residency"
  ]
  node [
    id 1308
    label "sojourn"
  ]
  node [
    id 1309
    label "wychodzenie"
  ]
  node [
    id 1310
    label "tkwienie"
  ]
  node [
    id 1311
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1312
    label "absolutorium"
  ]
  node [
    id 1313
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1314
    label "activity"
  ]
  node [
    id 1315
    label "ton"
  ]
  node [
    id 1316
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 1317
    label "korkowanie"
  ]
  node [
    id 1318
    label "zabijanie"
  ]
  node [
    id 1319
    label "martwy"
  ]
  node [
    id 1320
    label "przestawanie"
  ]
  node [
    id 1321
    label "odumieranie"
  ]
  node [
    id 1322
    label "zdychanie"
  ]
  node [
    id 1323
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1324
    label "zanikanie"
  ]
  node [
    id 1325
    label "ko&#324;czenie"
  ]
  node [
    id 1326
    label "ciekawy"
  ]
  node [
    id 1327
    label "szybki"
  ]
  node [
    id 1328
    label "&#380;ywotny"
  ]
  node [
    id 1329
    label "naturalny"
  ]
  node [
    id 1330
    label "&#380;ywo"
  ]
  node [
    id 1331
    label "cz&#322;owiek"
  ]
  node [
    id 1332
    label "o&#380;ywianie"
  ]
  node [
    id 1333
    label "silny"
  ]
  node [
    id 1334
    label "g&#322;&#281;boki"
  ]
  node [
    id 1335
    label "wyra&#378;ny"
  ]
  node [
    id 1336
    label "czynny"
  ]
  node [
    id 1337
    label "aktualny"
  ]
  node [
    id 1338
    label "zgrabny"
  ]
  node [
    id 1339
    label "prawdziwy"
  ]
  node [
    id 1340
    label "realistyczny"
  ]
  node [
    id 1341
    label "energiczny"
  ]
  node [
    id 1342
    label "odumarcie"
  ]
  node [
    id 1343
    label "przestanie"
  ]
  node [
    id 1344
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1345
    label "pomarcie"
  ]
  node [
    id 1346
    label "die"
  ]
  node [
    id 1347
    label "sko&#324;czenie"
  ]
  node [
    id 1348
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1349
    label "zdechni&#281;cie"
  ]
  node [
    id 1350
    label "procedura"
  ]
  node [
    id 1351
    label "proces"
  ]
  node [
    id 1352
    label "proces_biologiczny"
  ]
  node [
    id 1353
    label "z&#322;ote_czasy"
  ]
  node [
    id 1354
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1355
    label "process"
  ]
  node [
    id 1356
    label "cycle"
  ]
  node [
    id 1357
    label "rozwi&#261;zanie"
  ]
  node [
    id 1358
    label "zlec"
  ]
  node [
    id 1359
    label "zlegni&#281;cie"
  ]
  node [
    id 1360
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1361
    label "pogrzeb"
  ]
  node [
    id 1362
    label "majority"
  ]
  node [
    id 1363
    label "osiemnastoletni"
  ]
  node [
    id 1364
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1365
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1366
    label "age"
  ]
  node [
    id 1367
    label "kobieta"
  ]
  node [
    id 1368
    label "przekwitanie"
  ]
  node [
    id 1369
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 1370
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1371
    label "energia"
  ]
  node [
    id 1372
    label "zapa&#322;"
  ]
  node [
    id 1373
    label "samota"
  ]
  node [
    id 1374
    label "isolation"
  ]
  node [
    id 1375
    label "izolacja"
  ]
  node [
    id 1376
    label "odczucia"
  ]
  node [
    id 1377
    label "zmys&#322;"
  ]
  node [
    id 1378
    label "zjawisko"
  ]
  node [
    id 1379
    label "przeczulica"
  ]
  node [
    id 1380
    label "poczucie"
  ]
  node [
    id 1381
    label "reakcja"
  ]
  node [
    id 1382
    label "l&#281;k_separacyjny"
  ]
  node [
    id 1383
    label "insulant"
  ]
  node [
    id 1384
    label "insulation"
  ]
  node [
    id 1385
    label "ochrona"
  ]
  node [
    id 1386
    label "separation"
  ]
  node [
    id 1387
    label "samotnia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 9
    target 896
  ]
  edge [
    source 9
    target 897
  ]
  edge [
    source 9
    target 898
  ]
  edge [
    source 9
    target 899
  ]
  edge [
    source 9
    target 900
  ]
  edge [
    source 9
    target 901
  ]
  edge [
    source 9
    target 902
  ]
  edge [
    source 9
    target 903
  ]
  edge [
    source 9
    target 904
  ]
  edge [
    source 9
    target 905
  ]
  edge [
    source 9
    target 906
  ]
  edge [
    source 9
    target 907
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 909
  ]
  edge [
    source 9
    target 910
  ]
  edge [
    source 9
    target 911
  ]
  edge [
    source 9
    target 912
  ]
  edge [
    source 9
    target 913
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 914
  ]
  edge [
    source 9
    target 915
  ]
  edge [
    source 9
    target 916
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 942
  ]
  edge [
    source 9
    target 943
  ]
  edge [
    source 9
    target 944
  ]
  edge [
    source 9
    target 945
  ]
  edge [
    source 9
    target 946
  ]
  edge [
    source 9
    target 947
  ]
  edge [
    source 9
    target 948
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 9
    target 952
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 9
    target 962
  ]
  edge [
    source 9
    target 963
  ]
  edge [
    source 9
    target 964
  ]
  edge [
    source 9
    target 965
  ]
  edge [
    source 9
    target 966
  ]
  edge [
    source 9
    target 967
  ]
  edge [
    source 9
    target 968
  ]
  edge [
    source 9
    target 969
  ]
  edge [
    source 9
    target 970
  ]
  edge [
    source 9
    target 971
  ]
  edge [
    source 9
    target 972
  ]
  edge [
    source 9
    target 973
  ]
  edge [
    source 9
    target 974
  ]
  edge [
    source 9
    target 975
  ]
  edge [
    source 9
    target 976
  ]
  edge [
    source 9
    target 977
  ]
  edge [
    source 9
    target 978
  ]
  edge [
    source 9
    target 979
  ]
  edge [
    source 9
    target 980
  ]
  edge [
    source 9
    target 981
  ]
  edge [
    source 9
    target 982
  ]
  edge [
    source 9
    target 983
  ]
  edge [
    source 9
    target 984
  ]
  edge [
    source 9
    target 985
  ]
  edge [
    source 9
    target 986
  ]
  edge [
    source 9
    target 987
  ]
  edge [
    source 9
    target 988
  ]
  edge [
    source 9
    target 989
  ]
  edge [
    source 9
    target 990
  ]
  edge [
    source 9
    target 991
  ]
  edge [
    source 9
    target 992
  ]
  edge [
    source 9
    target 993
  ]
  edge [
    source 9
    target 994
  ]
  edge [
    source 9
    target 995
  ]
  edge [
    source 9
    target 996
  ]
  edge [
    source 9
    target 997
  ]
  edge [
    source 9
    target 998
  ]
  edge [
    source 9
    target 999
  ]
  edge [
    source 9
    target 1000
  ]
  edge [
    source 9
    target 1001
  ]
  edge [
    source 9
    target 1002
  ]
  edge [
    source 9
    target 1003
  ]
  edge [
    source 9
    target 1004
  ]
  edge [
    source 9
    target 1005
  ]
  edge [
    source 9
    target 1006
  ]
  edge [
    source 9
    target 1007
  ]
  edge [
    source 9
    target 1008
  ]
  edge [
    source 9
    target 1009
  ]
  edge [
    source 9
    target 1010
  ]
  edge [
    source 9
    target 1011
  ]
  edge [
    source 9
    target 1012
  ]
  edge [
    source 9
    target 1013
  ]
  edge [
    source 9
    target 1014
  ]
  edge [
    source 9
    target 1015
  ]
  edge [
    source 9
    target 1016
  ]
  edge [
    source 9
    target 1017
  ]
  edge [
    source 9
    target 1018
  ]
  edge [
    source 9
    target 1019
  ]
  edge [
    source 9
    target 1020
  ]
  edge [
    source 9
    target 1021
  ]
  edge [
    source 9
    target 1022
  ]
  edge [
    source 9
    target 1023
  ]
  edge [
    source 9
    target 1024
  ]
  edge [
    source 9
    target 1025
  ]
  edge [
    source 9
    target 1026
  ]
  edge [
    source 9
    target 1027
  ]
  edge [
    source 9
    target 1028
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 1029
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 1030
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 1031
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 1032
  ]
  edge [
    source 9
    target 1033
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 1034
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 1035
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 1036
  ]
  edge [
    source 9
    target 1037
  ]
  edge [
    source 9
    target 1038
  ]
  edge [
    source 9
    target 1039
  ]
  edge [
    source 9
    target 1040
  ]
  edge [
    source 9
    target 1041
  ]
  edge [
    source 9
    target 1042
  ]
  edge [
    source 9
    target 1043
  ]
  edge [
    source 9
    target 1044
  ]
  edge [
    source 9
    target 1045
  ]
  edge [
    source 9
    target 1046
  ]
  edge [
    source 9
    target 1047
  ]
  edge [
    source 9
    target 1048
  ]
  edge [
    source 9
    target 1049
  ]
  edge [
    source 9
    target 1050
  ]
  edge [
    source 9
    target 1051
  ]
  edge [
    source 9
    target 1052
  ]
  edge [
    source 9
    target 1053
  ]
  edge [
    source 9
    target 1054
  ]
  edge [
    source 9
    target 1055
  ]
  edge [
    source 9
    target 1056
  ]
  edge [
    source 9
    target 1057
  ]
  edge [
    source 9
    target 1058
  ]
  edge [
    source 9
    target 1059
  ]
  edge [
    source 9
    target 1060
  ]
  edge [
    source 9
    target 1061
  ]
  edge [
    source 9
    target 1062
  ]
  edge [
    source 9
    target 1063
  ]
  edge [
    source 9
    target 1064
  ]
  edge [
    source 9
    target 1065
  ]
  edge [
    source 9
    target 1066
  ]
  edge [
    source 9
    target 1067
  ]
  edge [
    source 9
    target 1068
  ]
  edge [
    source 9
    target 1069
  ]
  edge [
    source 9
    target 1070
  ]
  edge [
    source 9
    target 1071
  ]
  edge [
    source 9
    target 1072
  ]
  edge [
    source 9
    target 1073
  ]
  edge [
    source 9
    target 1074
  ]
  edge [
    source 9
    target 1075
  ]
  edge [
    source 9
    target 1076
  ]
  edge [
    source 9
    target 1077
  ]
  edge [
    source 9
    target 1078
  ]
  edge [
    source 9
    target 1079
  ]
  edge [
    source 9
    target 1080
  ]
  edge [
    source 9
    target 1081
  ]
  edge [
    source 9
    target 1082
  ]
  edge [
    source 9
    target 1083
  ]
  edge [
    source 9
    target 1084
  ]
  edge [
    source 9
    target 1085
  ]
  edge [
    source 9
    target 1086
  ]
  edge [
    source 9
    target 1087
  ]
  edge [
    source 9
    target 1088
  ]
  edge [
    source 9
    target 1089
  ]
  edge [
    source 9
    target 1090
  ]
  edge [
    source 9
    target 1091
  ]
  edge [
    source 9
    target 1092
  ]
  edge [
    source 9
    target 1093
  ]
  edge [
    source 9
    target 1094
  ]
  edge [
    source 9
    target 1095
  ]
  edge [
    source 9
    target 1096
  ]
  edge [
    source 9
    target 1097
  ]
  edge [
    source 9
    target 1098
  ]
  edge [
    source 9
    target 1099
  ]
  edge [
    source 9
    target 1100
  ]
  edge [
    source 9
    target 1101
  ]
  edge [
    source 9
    target 1102
  ]
  edge [
    source 9
    target 1103
  ]
  edge [
    source 9
    target 1104
  ]
  edge [
    source 9
    target 1105
  ]
  edge [
    source 9
    target 1106
  ]
  edge [
    source 9
    target 1107
  ]
  edge [
    source 9
    target 1108
  ]
  edge [
    source 9
    target 1109
  ]
  edge [
    source 9
    target 1110
  ]
  edge [
    source 9
    target 1111
  ]
  edge [
    source 9
    target 1112
  ]
  edge [
    source 9
    target 1113
  ]
  edge [
    source 9
    target 1114
  ]
  edge [
    source 9
    target 1115
  ]
  edge [
    source 9
    target 1116
  ]
  edge [
    source 9
    target 1117
  ]
  edge [
    source 9
    target 1118
  ]
  edge [
    source 9
    target 1119
  ]
  edge [
    source 9
    target 1120
  ]
  edge [
    source 9
    target 1121
  ]
  edge [
    source 9
    target 1122
  ]
  edge [
    source 9
    target 1123
  ]
  edge [
    source 9
    target 1124
  ]
  edge [
    source 9
    target 1125
  ]
  edge [
    source 9
    target 1126
  ]
  edge [
    source 9
    target 1127
  ]
  edge [
    source 9
    target 1128
  ]
  edge [
    source 9
    target 1129
  ]
  edge [
    source 9
    target 1130
  ]
  edge [
    source 9
    target 1131
  ]
  edge [
    source 9
    target 1132
  ]
  edge [
    source 9
    target 1133
  ]
  edge [
    source 9
    target 1134
  ]
  edge [
    source 9
    target 1135
  ]
  edge [
    source 9
    target 1136
  ]
  edge [
    source 9
    target 1137
  ]
  edge [
    source 9
    target 1138
  ]
  edge [
    source 9
    target 1139
  ]
  edge [
    source 9
    target 1140
  ]
  edge [
    source 9
    target 1141
  ]
  edge [
    source 9
    target 1142
  ]
  edge [
    source 9
    target 1143
  ]
  edge [
    source 9
    target 1144
  ]
  edge [
    source 9
    target 1145
  ]
  edge [
    source 9
    target 1146
  ]
  edge [
    source 9
    target 1147
  ]
  edge [
    source 9
    target 1148
  ]
  edge [
    source 9
    target 1149
  ]
  edge [
    source 9
    target 1150
  ]
  edge [
    source 9
    target 1151
  ]
  edge [
    source 9
    target 1152
  ]
  edge [
    source 9
    target 1153
  ]
  edge [
    source 9
    target 1154
  ]
  edge [
    source 9
    target 1155
  ]
  edge [
    source 9
    target 1156
  ]
  edge [
    source 9
    target 1157
  ]
  edge [
    source 9
    target 1158
  ]
  edge [
    source 9
    target 1159
  ]
  edge [
    source 9
    target 1160
  ]
  edge [
    source 9
    target 1161
  ]
  edge [
    source 9
    target 1162
  ]
  edge [
    source 9
    target 1163
  ]
  edge [
    source 9
    target 1164
  ]
  edge [
    source 9
    target 1165
  ]
  edge [
    source 9
    target 1166
  ]
  edge [
    source 9
    target 1167
  ]
  edge [
    source 9
    target 1168
  ]
  edge [
    source 9
    target 1169
  ]
  edge [
    source 9
    target 1170
  ]
  edge [
    source 9
    target 1171
  ]
  edge [
    source 9
    target 1172
  ]
  edge [
    source 9
    target 1173
  ]
  edge [
    source 10
    target 1174
  ]
  edge [
    source 10
    target 1175
  ]
  edge [
    source 10
    target 1176
  ]
  edge [
    source 10
    target 1177
  ]
  edge [
    source 10
    target 1178
  ]
  edge [
    source 10
    target 1179
  ]
  edge [
    source 10
    target 1180
  ]
  edge [
    source 10
    target 1181
  ]
  edge [
    source 10
    target 1182
  ]
  edge [
    source 10
    target 1183
  ]
  edge [
    source 10
    target 1184
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 1185
  ]
  edge [
    source 10
    target 1186
  ]
  edge [
    source 10
    target 1187
  ]
  edge [
    source 10
    target 1188
  ]
  edge [
    source 10
    target 1189
  ]
  edge [
    source 10
    target 1190
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 1191
  ]
  edge [
    source 10
    target 1192
  ]
  edge [
    source 10
    target 1193
  ]
  edge [
    source 10
    target 1194
  ]
  edge [
    source 10
    target 1195
  ]
  edge [
    source 10
    target 1196
  ]
  edge [
    source 10
    target 1197
  ]
  edge [
    source 10
    target 1198
  ]
  edge [
    source 10
    target 1199
  ]
  edge [
    source 10
    target 1200
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1201
  ]
  edge [
    source 11
    target 1202
  ]
  edge [
    source 11
    target 1203
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 1229
  ]
  edge [
    source 12
    target 1230
  ]
  edge [
    source 12
    target 1231
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 1232
  ]
  edge [
    source 13
    target 1233
  ]
  edge [
    source 13
    target 1234
  ]
  edge [
    source 13
    target 1235
  ]
  edge [
    source 13
    target 1236
  ]
  edge [
    source 13
    target 1237
  ]
  edge [
    source 13
    target 1238
  ]
  edge [
    source 13
    target 1239
  ]
  edge [
    source 13
    target 1240
  ]
  edge [
    source 13
    target 1241
  ]
  edge [
    source 13
    target 1242
  ]
  edge [
    source 13
    target 1243
  ]
  edge [
    source 13
    target 1244
  ]
  edge [
    source 13
    target 1245
  ]
  edge [
    source 13
    target 1246
  ]
  edge [
    source 13
    target 1247
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 1248
  ]
  edge [
    source 13
    target 1249
  ]
  edge [
    source 13
    target 1250
  ]
  edge [
    source 13
    target 1251
  ]
  edge [
    source 13
    target 1252
  ]
  edge [
    source 13
    target 1253
  ]
  edge [
    source 13
    target 1254
  ]
  edge [
    source 13
    target 1255
  ]
  edge [
    source 13
    target 1256
  ]
  edge [
    source 13
    target 1257
  ]
  edge [
    source 13
    target 1258
  ]
  edge [
    source 13
    target 1259
  ]
  edge [
    source 13
    target 1260
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 1261
  ]
  edge [
    source 13
    target 1262
  ]
  edge [
    source 13
    target 1263
  ]
  edge [
    source 13
    target 1264
  ]
  edge [
    source 13
    target 1265
  ]
  edge [
    source 13
    target 1266
  ]
  edge [
    source 13
    target 1267
  ]
  edge [
    source 13
    target 1268
  ]
  edge [
    source 13
    target 1269
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 1270
  ]
  edge [
    source 13
    target 1271
  ]
  edge [
    source 13
    target 1272
  ]
  edge [
    source 13
    target 1273
  ]
  edge [
    source 13
    target 1274
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 1275
  ]
  edge [
    source 13
    target 1276
  ]
  edge [
    source 13
    target 1277
  ]
  edge [
    source 13
    target 1278
  ]
  edge [
    source 13
    target 1279
  ]
  edge [
    source 13
    target 1280
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 1281
  ]
  edge [
    source 13
    target 1282
  ]
  edge [
    source 13
    target 1283
  ]
  edge [
    source 13
    target 1284
  ]
  edge [
    source 13
    target 1285
  ]
  edge [
    source 13
    target 1286
  ]
  edge [
    source 13
    target 1287
  ]
  edge [
    source 13
    target 1288
  ]
  edge [
    source 13
    target 1289
  ]
  edge [
    source 13
    target 1290
  ]
  edge [
    source 13
    target 1291
  ]
  edge [
    source 13
    target 1292
  ]
  edge [
    source 13
    target 1293
  ]
  edge [
    source 13
    target 1294
  ]
  edge [
    source 13
    target 1295
  ]
  edge [
    source 13
    target 1296
  ]
  edge [
    source 13
    target 1297
  ]
  edge [
    source 13
    target 1298
  ]
  edge [
    source 13
    target 1299
  ]
  edge [
    source 13
    target 1300
  ]
  edge [
    source 13
    target 1301
  ]
  edge [
    source 13
    target 1302
  ]
  edge [
    source 13
    target 1303
  ]
  edge [
    source 13
    target 1304
  ]
  edge [
    source 13
    target 1305
  ]
  edge [
    source 13
    target 1306
  ]
  edge [
    source 13
    target 1307
  ]
  edge [
    source 13
    target 1308
  ]
  edge [
    source 13
    target 1309
  ]
  edge [
    source 13
    target 1310
  ]
  edge [
    source 13
    target 1311
  ]
  edge [
    source 13
    target 1312
  ]
  edge [
    source 13
    target 1313
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 1314
  ]
  edge [
    source 13
    target 1315
  ]
  edge [
    source 13
    target 1316
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 1317
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 1318
  ]
  edge [
    source 13
    target 1319
  ]
  edge [
    source 13
    target 1320
  ]
  edge [
    source 13
    target 1321
  ]
  edge [
    source 13
    target 1322
  ]
  edge [
    source 13
    target 47
  ]
  edge [
    source 13
    target 1323
  ]
  edge [
    source 13
    target 1324
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 1334
  ]
  edge [
    source 13
    target 1335
  ]
  edge [
    source 13
    target 1336
  ]
  edge [
    source 13
    target 1337
  ]
  edge [
    source 13
    target 1338
  ]
  edge [
    source 13
    target 1339
  ]
  edge [
    source 13
    target 1340
  ]
  edge [
    source 13
    target 1341
  ]
  edge [
    source 13
    target 1342
  ]
  edge [
    source 13
    target 1343
  ]
  edge [
    source 13
    target 1344
  ]
  edge [
    source 13
    target 1345
  ]
  edge [
    source 13
    target 1346
  ]
  edge [
    source 13
    target 1347
  ]
  edge [
    source 13
    target 1348
  ]
  edge [
    source 13
    target 1349
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 1350
  ]
  edge [
    source 13
    target 1351
  ]
  edge [
    source 13
    target 1352
  ]
  edge [
    source 13
    target 1353
  ]
  edge [
    source 13
    target 1354
  ]
  edge [
    source 13
    target 1355
  ]
  edge [
    source 13
    target 1356
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 1357
  ]
  edge [
    source 13
    target 1358
  ]
  edge [
    source 13
    target 1359
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 1360
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 1361
  ]
  edge [
    source 13
    target 1362
  ]
  edge [
    source 13
    target 1363
  ]
  edge [
    source 13
    target 1364
  ]
  edge [
    source 13
    target 1365
  ]
  edge [
    source 13
    target 1366
  ]
  edge [
    source 13
    target 1367
  ]
  edge [
    source 13
    target 1368
  ]
  edge [
    source 13
    target 1369
  ]
  edge [
    source 13
    target 1370
  ]
  edge [
    source 13
    target 1371
  ]
  edge [
    source 13
    target 1372
  ]
  edge [
    source 14
    target 1233
  ]
  edge [
    source 14
    target 1373
  ]
  edge [
    source 14
    target 1374
  ]
  edge [
    source 14
    target 1375
  ]
  edge [
    source 14
    target 1376
  ]
  edge [
    source 14
    target 1351
  ]
  edge [
    source 14
    target 1377
  ]
  edge [
    source 14
    target 1378
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 1379
  ]
  edge [
    source 14
    target 1380
  ]
  edge [
    source 14
    target 1381
  ]
  edge [
    source 14
    target 1382
  ]
  edge [
    source 14
    target 1254
  ]
  edge [
    source 14
    target 47
  ]
  edge [
    source 14
    target 1383
  ]
  edge [
    source 14
    target 1384
  ]
  edge [
    source 14
    target 1385
  ]
  edge [
    source 14
    target 1386
  ]
  edge [
    source 14
    target 1387
  ]
]
