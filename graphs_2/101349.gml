graph [
  node [
    id 0
    label "rudawka"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "sok&#243;lski"
    origin "text"
  ]
  node [
    id 3
    label "woda"
  ]
  node [
    id 4
    label "bagno"
  ]
  node [
    id 5
    label "dotleni&#263;"
  ]
  node [
    id 6
    label "spi&#281;trza&#263;"
  ]
  node [
    id 7
    label "spi&#281;trzenie"
  ]
  node [
    id 8
    label "utylizator"
  ]
  node [
    id 9
    label "obiekt_naturalny"
  ]
  node [
    id 10
    label "p&#322;ycizna"
  ]
  node [
    id 11
    label "nabranie"
  ]
  node [
    id 12
    label "Waruna"
  ]
  node [
    id 13
    label "przyroda"
  ]
  node [
    id 14
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 15
    label "przybieranie"
  ]
  node [
    id 16
    label "uci&#261;g"
  ]
  node [
    id 17
    label "bombast"
  ]
  node [
    id 18
    label "fala"
  ]
  node [
    id 19
    label "kryptodepresja"
  ]
  node [
    id 20
    label "water"
  ]
  node [
    id 21
    label "wysi&#281;k"
  ]
  node [
    id 22
    label "pustka"
  ]
  node [
    id 23
    label "ciecz"
  ]
  node [
    id 24
    label "przybrze&#380;e"
  ]
  node [
    id 25
    label "nap&#243;j"
  ]
  node [
    id 26
    label "spi&#281;trzanie"
  ]
  node [
    id 27
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 28
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 29
    label "bicie"
  ]
  node [
    id 30
    label "klarownik"
  ]
  node [
    id 31
    label "chlastanie"
  ]
  node [
    id 32
    label "woda_s&#322;odka"
  ]
  node [
    id 33
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 34
    label "nabra&#263;"
  ]
  node [
    id 35
    label "chlasta&#263;"
  ]
  node [
    id 36
    label "uj&#281;cie_wody"
  ]
  node [
    id 37
    label "zrzut"
  ]
  node [
    id 38
    label "wypowied&#378;"
  ]
  node [
    id 39
    label "wodnik"
  ]
  node [
    id 40
    label "pojazd"
  ]
  node [
    id 41
    label "l&#243;d"
  ]
  node [
    id 42
    label "wybrze&#380;e"
  ]
  node [
    id 43
    label "deklamacja"
  ]
  node [
    id 44
    label "tlenek"
  ]
  node [
    id 45
    label "szuwar"
  ]
  node [
    id 46
    label "teren"
  ]
  node [
    id 47
    label "smr&#243;d"
  ]
  node [
    id 48
    label "bajor"
  ]
  node [
    id 49
    label "krzew"
  ]
  node [
    id 50
    label "wrzosowate"
  ]
  node [
    id 51
    label "pasztet"
  ]
  node [
    id 52
    label "moczar"
  ]
  node [
    id 53
    label "mire"
  ]
  node [
    id 54
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 55
    label "kloaka"
  ]
  node [
    id 56
    label "wojew&#243;dztwo"
  ]
  node [
    id 57
    label "jednostka_administracyjna"
  ]
  node [
    id 58
    label "gmina"
  ]
  node [
    id 59
    label "Biskupice"
  ]
  node [
    id 60
    label "radny"
  ]
  node [
    id 61
    label "urz&#261;d"
  ]
  node [
    id 62
    label "rada_gminy"
  ]
  node [
    id 63
    label "Dobro&#324;"
  ]
  node [
    id 64
    label "organizacja_religijna"
  ]
  node [
    id 65
    label "Karlsbad"
  ]
  node [
    id 66
    label "Wielka_Wie&#347;"
  ]
  node [
    id 67
    label "mikroregion"
  ]
  node [
    id 68
    label "makroregion"
  ]
  node [
    id 69
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 70
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 71
    label "pa&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
]
